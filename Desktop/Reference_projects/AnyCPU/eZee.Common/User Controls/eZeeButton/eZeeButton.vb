Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Drawing.Drawing2D
Imports System.Drawing.Text
Imports PushButtonState = System.Windows.Forms.VisualStyles.PushButtonState

''' <summary>
''' Represents a glass button control.
''' </summary>
<ToolboxBitmap(GetType(eZeeButton)), ToolboxItem(True), ToolboxItemFilter("System.Windows.Forms"), Description("Raises an event when the user clicks it.")> _
Partial Public Class eZeeButton
    Inherits Button

#Region " Constructors "

    ''' <summary>
    ''' Initializes a new instance of the eZeeButton class.
    ''' </summary>
    Public Sub New()
        InitializeComponent()
        x_timer.Interval = animationLength \ framesCount
        MyBase.BackColor = Color.Transparent
        BackColor = Color.Black
        ForeColor = Color.White
        OuterBorderColor = Color.White
        InnerBorderColor = Color.Black
        ShineColor = Color.White
        GlowColor = Color.FromArgb(-7488001) 'unchecked((int)(0xFF8DBDFF)));
        SetStyle(ControlStyles.AllPaintingInWmPaint Or ControlStyles.OptimizedDoubleBuffer Or ControlStyles.ResizeRedraw Or ControlStyles.SupportsTransparentBackColor Or ControlStyles.UserPaint, True)
        SetStyle(ControlStyles.Opaque, False)
    End Sub

#End Region

#Region " Fields and Properties "

    Private backColor_Renamed As Color
    ''' <summary>
    ''' Gets or sets the background color of the control.
    ''' </summary>
    ''' <returns>A <see cref="T:System.Drawing.Color" /> value representing the background color.</returns>
    <DefaultValue(GetType(Color), "Black")> _
    Public Overridable Shadows Property BackColor() As Color
        Get
            Return backColor_Renamed
        End Get
        Set(ByVal value As Color)
            If (Not backColor_Renamed.Equals(value)) Then
                backColor_Renamed = value
                UseVisualStyleBackColor = False
                CreateFrames()
                OnBackColorChanged(EventArgs.Empty)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the foreground color of the control.
    ''' </summary>
    ''' <returns>The foreground <see cref="T:System.Drawing.Color" /> of the control.</returns>
    <DefaultValue(GetType(Color), "White")> _
    Public Overridable Shadows Property ForeColor() As Color
        Get
            Return MyBase.ForeColor
        End Get
        Set(ByVal value As Color)
            MyBase.ForeColor = value
        End Set
    End Property

    Private innerBorderColor_Renamed As Color
    ''' <summary>
    ''' Gets or sets the inner border color of the control.
    ''' </summary>
    ''' <returns>A <see cref="T:System.Drawing.Color" /> value representing the color of the inner border.</returns>
    <DefaultValue(GetType(Color), "Black"), Category("Appearance"), Description("The inner border color of the control.")> _
    Public Overridable Property InnerBorderColor() As Color
        Get
            Return innerBorderColor_Renamed
        End Get
        Set(ByVal value As Color)
            If innerBorderColor_Renamed <> value Then
                innerBorderColor_Renamed = value
                CreateFrames()
                If IsHandleCreated Then
                    Invalidate()
                End If
                OnInnerBorderColorChanged(EventArgs.Empty)
            End If
        End Set
    End Property

    Private outerBorderColor_Renamed As Color
    ''' <summary>
    ''' Gets or sets the outer border color of the control.
    ''' </summary>
    ''' <returns>A <see cref="T:System.Drawing.Color" /> value representing the color of the outer border.</returns>
    <DefaultValue(GetType(Color), "White"), Category("Appearance"), Description("The outer border color of the control.")> _
    Public Overridable Property OuterBorderColor() As Color
        Get
            Return outerBorderColor_Renamed
        End Get
        Set(ByVal value As Color)
            If outerBorderColor_Renamed <> value Then
                outerBorderColor_Renamed = value
                CreateFrames()
                If IsHandleCreated Then
                    Invalidate()
                End If
                OnOuterBorderColorChanged(EventArgs.Empty)
            End If
        End Set
    End Property

    Private shineColor_Renamed As Color
    ''' <summary>
    ''' Gets or sets the shine color of the control.
    ''' </summary>
    ''' <returns>A <see cref="T:System.Drawing.Color" /> value representing the shine color.</returns>
    <DefaultValue(GetType(Color), "White"), Category("Appearance"), Description("The shine color of the control.")> _
    Public Overridable Property ShineColor() As Color
        Get
            Return shineColor_Renamed
        End Get
        Set(ByVal value As Color)
            If shineColor_Renamed <> value Then
                shineColor_Renamed = value
                CreateFrames()
                If IsHandleCreated Then
                    Invalidate()
                End If
                OnShineColorChanged(EventArgs.Empty)
            End If
        End Set
    End Property

    Private glowColor_Renamed As Color
    ''' <summary>
    ''' Gets or sets the glow color of the control.
    ''' </summary>
    ''' <returns>A <see cref="T:System.Drawing.Color" /> value representing the glow color.</returns>
    <DefaultValue(GetType(Color), "255,141,189,255"), Category("Appearance"), Description("The glow color of the control.")> _
    Public Overridable Property GlowColor() As Color
        Get
            Return glowColor_Renamed
        End Get
        Set(ByVal value As Color)
            If glowColor_Renamed <> value Then
                glowColor_Renamed = value
                CreateFrames()
                If IsHandleCreated Then
                    Invalidate()
                End If
                OnGlowColorChanged(EventArgs.Empty)
            End If
        End Set
    End Property


    Private gblnStretchImage As Boolean
    <DefaultValue(False), Category("Appearance")> _
    Public Overridable Property StretchImage() As Boolean
        Get
            Return gblnStretchImage
        End Get
        Set(ByVal value As Boolean)
            If gblnStretchImage <> value Then
                gblnStretchImage = value
                CreateFrames()
                If IsHandleCreated Then
                    Invalidate()
                End If
            End If
        End Set
    End Property

    Private isHovered As Boolean
    Private isFocused As Boolean
    Private isFocusedByKey As Boolean
    Private isKeyDown As Boolean
    Private isMouseDown As Boolean
    Private ReadOnly Property isPressed() As Boolean
        Get
            Return isKeyDown OrElse (isMouseDown AndAlso isHovered)
        End Get
    End Property

    ''' <summary>
    ''' Gets the state of the button control.
    ''' </summary>
    ''' <value>The state of the button control.</value>
    <Browsable(False)> _
    Public ReadOnly Property State() As PushButtonState
        Get
            If (Not Enabled) Then
                Return PushButtonState.Disabled
            End If
            If isPressed Then
                Return PushButtonState.Pressed
            End If
            If isHovered Then
                Return PushButtonState.Hot
            End If
            If isFocused OrElse IsDefault Then
                Return PushButtonState.Default
            End If
            Return PushButtonState.Normal
        End Get
    End Property

#End Region

#Region " Events "

    ''' <summary>Occurs when the value of the <see cref="P:Glass.GlassButton.InnerBorderColor" /> property changes.</summary>
    <Description("Event raised when the value of the InnerBorderColor property is changed."), Category("Property Changed")> _
    Public Event InnerBorderColorChanged As EventHandler

    ''' <summary>
    ''' Raises the <see cref="E:Glass.GlassButton.InnerBorderColorChanged" /> event.
    ''' </summary>
    ''' <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
    Protected Overridable Sub OnInnerBorderColorChanged(ByVal e As EventArgs)
        If Not InnerBorderColorChangedEvent Is Nothing Then
            RaiseEvent InnerBorderColorChanged(Me, e)
        End If
    End Sub

    ''' <summary>Occurs when the value of the <see cref="P:Glass.GlassButton.OuterBorderColor" /> property changes.</summary>
    <Description("Event raised when the value of the OuterBorderColor property is changed."), Category("Property Changed")> _
    Public Event OuterBorderColorChanged As EventHandler

    ''' <summary>
    ''' Raises the <see cref="E:Glass.GlassButton.OuterBorderColorChanged" /> event.
    ''' </summary>
    ''' <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
    Protected Overridable Sub OnOuterBorderColorChanged(ByVal e As EventArgs)
        If Not OuterBorderColorChangedEvent Is Nothing Then
            RaiseEvent OuterBorderColorChanged(Me, e)
        End If
    End Sub

    ''' <summary>Occurs when the value of the <see cref="P:Glass.GlassButton.ShineColor" /> property changes.</summary>
    <Description("Event raised when the value of the ShineColor property is changed."), Category("Property Changed")> _
    Public Event ShineColorChanged As EventHandler

    ''' <summary>
    ''' Raises the <see cref="E:Glass.GlassButton.ShineColorChanged" /> event.
    ''' </summary>
    ''' <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
    Protected Overridable Sub OnShineColorChanged(ByVal e As EventArgs)
        If Not ShineColorChangedEvent Is Nothing Then
            RaiseEvent ShineColorChanged(Me, e)
        End If
    End Sub

    ''' <summary>Occurs when the value of the <see cref="P:Glass.GlassButton.GlowColor" /> property changes.</summary>
    <Description("Event raised when the value of the GlowColor property is changed."), Category("Property Changed")> _
    Public Event GlowColorChanged As EventHandler

    ''' <summary>
    ''' Raises the <see cref="E:Glass.GlassButton.GlowColorChanged" /> event.
    ''' </summary>
    ''' <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
    Protected Overridable Sub OnGlowColorChanged(ByVal e As EventArgs)
        If Not GlowColorChangedEvent Is Nothing Then
            RaiseEvent InnerBorderColorChanged(Me, e)
        End If
    End Sub

#End Region

#Region " Overrided Methods "

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.SizeChanged" /> event.
    ''' </summary>
    ''' <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        CreateFrames()
        MyBase.OnSizeChanged(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.Click" /> event.
    ''' </summary>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Protected Overrides Sub OnClick(ByVal e As EventArgs)
        isMouseDown = False
        isKeyDown = isMouseDown
        MyBase.OnClick(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.Enter" /> event.
    ''' </summary>
    ''' <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnEnter(ByVal e As EventArgs)
        isFocusedByKey = True
        isFocused = isFocusedByKey
        MyBase.OnEnter(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.Leave" /> event.
    ''' </summary>
    ''' <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnLeave(ByVal e As EventArgs)
        MyBase.OnLeave(e)
        isMouseDown = False
        isKeyDown = isMouseDown
        isFocusedByKey = isKeyDown
        isFocused = isFocusedByKey
        Invalidate()
    End Sub

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.ButtonBase.OnKeyUp(System.Windows.Forms.KeyEventArgs)" /> event.
    ''' </summary>
    ''' <param name="kevent">A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnKeyDown(ByVal kevent As KeyEventArgs)
        If kevent.KeyCode = Keys.Space Then
            isKeyDown = True
            Invalidate()
        End If
        MyBase.OnKeyDown(kevent)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.ButtonBase.OnKeyUp(System.Windows.Forms.KeyEventArgs)" /> event.
    ''' </summary>
    ''' <param name="kevent">A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnKeyUp(ByVal kevent As KeyEventArgs)
        If isKeyDown AndAlso kevent.KeyCode = Keys.Space Then
            isKeyDown = False
            Invalidate()
        End If
        MyBase.OnKeyUp(kevent)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event.
    ''' </summary>
    ''' <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        If (Not isMouseDown) AndAlso e.Button = Windows.Forms.MouseButtons.Left Then
            isMouseDown = True
            isFocusedByKey = False
            Invalidate()
        End If
        MyBase.OnMouseDown(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event.
    ''' </summary>
    ''' <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnMouseUp(ByVal e As MouseEventArgs)
        If isMouseDown Then
            isMouseDown = False
            Invalidate()
        End If
        MyBase.OnMouseUp(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.Control.OnMouseMove(System.Windows.Forms.MouseEventArgs)" /> event.
    ''' </summary>
    ''' <param name="mevent">A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnMouseMove(ByVal mevent As MouseEventArgs)
        MyBase.OnMouseMove(mevent)
        If mevent.Button <> Windows.Forms.MouseButtons.None Then
            If (Not ClientRectangle.Contains(mevent.X, mevent.Y)) Then
                If isHovered Then
                    isHovered = False
                    Invalidate()
                End If
            ElseIf (Not isHovered) Then
                isHovered = True
                Invalidate()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.MouseEnter" /> event.
    ''' </summary>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        isHovered = True
        FadeIn()
        Invalidate()
        MyBase.OnMouseEnter(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Control.MouseLeave" /> event.
    ''' </summary>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        isHovered = False
        FadeOut()
        Invalidate()
        MyBase.OnMouseLeave(e)
    End Sub

#End Region

#Region " Painting "

    ''' <summary>
    ''' Raises the <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" /> event.
    ''' </summary>
    ''' <param name="pevent">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnPaint(ByVal pevent As PaintEventArgs)
        DrawButtonBackgroundFromBuffer(pevent.Graphics)
        DrawForegroundFromButton(pevent)
        DrawButtonForeground(pevent.Graphics)
    End Sub

    Private Sub DrawButtonBackgroundFromBuffer(ByVal graphics As Graphics)
        Dim frame As Integer
        If (Not Enabled) Then
            frame = FRAME_DISABLED
        ElseIf isPressed Then
            frame = FRAME_PRESSED
        ElseIf (Not isAnimating) AndAlso currentFrame = 0 Then
            frame = FRAME_NORMAL
        Else
            If (Not HasAnimationFrames) Then
                CreateFrames(True)
            End If
            frame = FRAME_ANIMATED + currentFrame
        End If
        If frames Is Nothing Then
            CreateFrames()
        End If
        graphics.DrawImage(frames(frame), Point.Empty)
    End Sub

    Public Function CreateBackgroundFrame(ByVal pressed As Boolean, ByVal hovered As Boolean, ByVal animating As Boolean, ByVal enabled As Boolean, ByVal glowOpacity As Single) As Image
        Dim rect As Rectangle = ClientRectangle
        If rect.Width <= 0 Then
            rect.Width = 1
        End If
        If rect.Height <= 0 Then
            rect.Height = 1
        End If
        Dim img As Image = New Bitmap(rect.Width, rect.Height)
        Using g As Graphics = Graphics.FromImage(img)
            g.Clear(Color.Transparent)
            DrawButtonBackground(g, rect, pressed, hovered, animating, enabled, outerBorderColor_Renamed, backColor_Renamed, glowColor_Renamed, shineColor_Renamed, innerBorderColor_Renamed, glowOpacity)
        End Using
        Return img
    End Function

    Private Shared Sub DrawButtonBackground(ByVal g As Graphics, ByVal rectangle As Rectangle, ByVal pressed As Boolean, ByVal hovered As Boolean, ByVal animating As Boolean, ByVal enabled As Boolean, ByVal outerBorderColor_Renamed As Color, ByVal backColor_Renamed As Color, ByVal glowColor_Renamed As Color, ByVal shineColor_Renamed As Color, ByVal innerBorderColor_Renamed As Color, ByVal glowOpacity As Single)
        Dim sm As SmoothingMode = g.SmoothingMode
        g.SmoothingMode = SmoothingMode.AntiAlias

        '#region " white border "
        Dim rect As Rectangle = rectangle
        rect.Width -= 1
        rect.Height -= 1
        Using bw As GraphicsPath = CreateRoundRectangle(rect, 4)
            Using p As Pen = New Pen(outerBorderColor_Renamed)
                g.DrawPath(p, bw)
            End Using
        End Using
        '			#End Region

        rect.X += 1
        rect.Y += 1
        rect.Width -= 2
        rect.Height -= 2
        Dim rect2 As Rectangle = rect
        rect2.Height >>= 1

        '#region " content "
        Using bb As GraphicsPath = CreateRoundRectangle(rect, 2)
            Dim opacity As Integer
            If pressed Then
                opacity = &HCC
            Else
                opacity = &H7F
            End If
            Using br As Brush = New SolidBrush(Color.FromArgb(opacity, backColor_Renamed))
                g.FillPath(br, bb)
            End Using
        End Using
        '			#End Region

        '#region " glow "
        If (hovered OrElse animating) AndAlso (Not pressed) Then
            Using clip As GraphicsPath = CreateRoundRectangle(rect, 2)
                g.SetClip(clip, CombineMode.Intersect)
                Using brad As GraphicsPath = CreateBottomRadialPath(rect)
                    Using pgr As PathGradientBrush = New PathGradientBrush(brad)
                        'TODO: INSTANT VB TODO TASK: There is no equivalent to an 'unchecked' block in VB.NET
                        '							unchecked
                        Dim opacity As Integer = CInt(&HB2 * glowOpacity + 0.5F)
                        Dim bounds As RectangleF = brad.GetBounds()
                        pgr.CenterPoint = New PointF((bounds.Left + bounds.Right) / 2.0F, (bounds.Top + bounds.Bottom) / 2.0F)
                        pgr.CenterColor = Color.FromArgb(opacity, glowColor_Renamed)
                        pgr.SurroundColors = New Color() {Color.FromArgb(0, glowColor_Renamed)}
                        'TODO: INSTANT VB TODO TASK: End of the original C# 'unchecked' block
                        g.FillPath(pgr, brad)
                    End Using
                End Using
                g.ResetClip()
            End Using
        End If
        '			#End Region

        '#region " shine "
        If rect2.Width > 0 AndAlso rect2.Height > 0 Then
            rect2.Height += 1
            Using bh As GraphicsPath = CreateTopRoundRectangle(rect2, 2)
                rect2.Height += 1
                Dim opacity As Integer = &H99
                If pressed Or (Not enabled) Then
                    opacity = CInt(Fix(0.4F * opacity + 0.5F))
                End If
                Using br As LinearGradientBrush = New LinearGradientBrush(rect2, Color.FromArgb(opacity, shineColor_Renamed), Color.FromArgb(opacity \ 3, shineColor_Renamed), LinearGradientMode.Vertical)
                    g.FillPath(br, bh)
                End Using
            End Using
            rect2.Height -= 2
        End If
        '			#End Region

        '#region " black border "
        Using bb As GraphicsPath = CreateRoundRectangle(rect, 3)
            Using p As Pen = New Pen(innerBorderColor_Renamed)
                g.DrawPath(p, bb)
            End Using
        End Using
        '			#End Region

        g.SmoothingMode = sm
    End Sub

    Private Sub DrawButtonForeground(ByVal g As Graphics)
        If Focused AndAlso ShowFocusCues Then
            Dim rect As Rectangle = ClientRectangle
            rect.Inflate(-4, -4)
            ControlPaint.DrawFocusRectangle(g, rect)
        End If
    End Sub

    Private Function ShrinkImage(ByVal picture As Image, ByVal img_size As Size, Optional ByVal anti_alias As Boolean = False) As Image
        If picture Is Nothing Then Return Nothing
        ' Make the destination Bitmap.
        Dim to_bm As New Bitmap(img_size.Width, img_size.Height)
        ' Copy the image.
        Using gr As Graphics = Graphics.FromImage(to_bm)


            If anti_alias Then gr.InterpolationMode = _
                Drawing2D.InterpolationMode.HighQualityBilinear

            gr.DrawImage(picture, 0, 0, img_size.Width - 1, img_size.Height - 1)
        End Using

        ' Display the result.
        Return to_bm

    End Function

    Private imageButton As Button
    Private Sub DrawForegroundFromButton(ByVal pevent As PaintEventArgs)
        If imageButton Is Nothing Then
            imageButton = New Button()
            imageButton.Parent = New TransparentControl()
            imageButton.BackColor = Color.Transparent
            imageButton.FlatAppearance.BorderSize = 0
            imageButton.FlatStyle = FlatStyle.Flat
        End If
        imageButton.AutoEllipsis = AutoEllipsis
        If Enabled Then
            imageButton.ForeColor = ForeColor
        Else
            imageButton.ForeColor = Color.FromArgb((3 * ForeColor.R + backColor_Renamed.R) >> 2, (3 * ForeColor.G + backColor_Renamed.G) >> 2, (3 * ForeColor.B + backColor_Renamed.B) >> 2)
        End If

        imageButton.Font = Font
        imageButton.RightToLeft = RightToLeft
        If gblnStretchImage Then
            imageButton.Image = ShrinkImage(Image, Size, True)
        Else
            imageButton.Image = Image
        End If
        imageButton.ImageAlign = ImageAlign
        imageButton.ImageIndex = ImageIndex
        imageButton.ImageKey = ImageKey
        imageButton.ImageList = ImageList
        imageButton.Padding = Padding
        imageButton.Size = Size
        imageButton.Text = Text
        imageButton.TextAlign = TextAlign
        imageButton.TextImageRelation = TextImageRelation
        imageButton.UseCompatibleTextRendering = UseCompatibleTextRendering
        imageButton.UseMnemonic = UseMnemonic
        InvokePaint(imageButton, pevent)
    End Sub

    Private Class TransparentControl
        Inherits Control
        Protected Overrides Sub OnPaintBackground(ByVal pevent As PaintEventArgs)
        End Sub
        Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        End Sub
    End Class

    Private Shared Function CreateRoundRectangle(ByVal rectangle As Rectangle, ByVal radius As Integer) As GraphicsPath
        Dim path As GraphicsPath = New GraphicsPath()
        Dim l As Integer = rectangle.Left
        Dim t As Integer = rectangle.Top
        Dim w As Integer = rectangle.Width
        Dim h As Integer = rectangle.Height
        Dim d As Integer = radius << 1
        path.AddArc(l, t, d, d, 180, 90) ' topleft
        path.AddLine(l + radius, t, l + w - radius, t) ' top
        path.AddArc(l + w - d, t, d, d, 270, 90) ' topright
        path.AddLine(l + w, t + radius, l + w, t + h - radius) ' right
        path.AddArc(l + w - d, t + h - d, d, d, 0, 90) ' bottomright
        path.AddLine(l + w - radius, t + h, l + radius, t + h) ' bottom
        path.AddArc(l, t + h - d, d, d, 90, 90) ' bottomleft
        path.AddLine(l, t + h - radius, l, t + radius) ' left
        path.CloseFigure()
        Return path
    End Function

    Private Shared Function CreateTopRoundRectangle(ByVal rectangle As Rectangle, ByVal radius As Integer) As GraphicsPath
        Dim path As GraphicsPath = New GraphicsPath()
        Dim l As Integer = rectangle.Left
        Dim t As Integer = rectangle.Top
        Dim w As Integer = rectangle.Width
        Dim h As Integer = rectangle.Height
        Dim d As Integer = radius << 1
        path.AddArc(l, t, d, d, 180, 90) ' topleft
        path.AddLine(l + radius, t, l + w - radius, t) ' top
        path.AddArc(l + w - d, t, d, d, 270, 90) ' topright
        path.AddLine(l + w, t + radius, l + w, t + h) ' right
        path.AddLine(l + w, t + h, l, t + h) ' bottom
        path.AddLine(l, t + h, l, t + radius) ' left
        path.CloseFigure()
        Return path
    End Function

    Private Shared Function CreateBottomRadialPath(ByVal rectangle As Rectangle) As GraphicsPath
        Dim path As GraphicsPath = New GraphicsPath()
        Dim rect As RectangleF = rectangle
        rect.X -= rect.Width * 0.35F
        rect.Y -= rect.Height * 0.15F
        rect.Width *= 1.7F
        rect.Height *= 2.3F
        path.AddEllipse(rect)
        path.CloseFigure()
        Return path
    End Function

#End Region

#Region " Unused Properties & Events "

    ''' <summary>This property is not relevant for this class.</summary>
    ''' <returns>This property is not relevant for this class.</returns>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)> _
    Public Shadows ReadOnly Property FlatAppearance() As FlatButtonAppearance
        Get
            Return MyBase.FlatAppearance
        End Get
    End Property

    ''' <summary>This property is not relevant for this class.</summary>
    ''' <returns>This property is not relevant for this class.</returns>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)> _
    Public Shadows Property FlatStyle() As FlatStyle
        Get
            Return MyBase.FlatStyle
        End Get
        Set(ByVal value As FlatStyle)
            MyBase.FlatStyle = value
        End Set
    End Property

    ''' <summary>This property is not relevant for this class.</summary>
    ''' <returns>This property is not relevant for this class.</returns>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)> _
    Public Shadows Property UseVisualStyleBackColor() As Boolean
        Get
            Return MyBase.UseVisualStyleBackColor
        End Get
        Set(ByVal value As Boolean)
            MyBase.UseVisualStyleBackColor = value
        End Set
    End Property

#End Region

#Region " Animation Support "

    Private frames As List(Of Image)

    Private Const FRAME_DISABLED As Integer = 0
    Private Const FRAME_PRESSED As Integer = 1
    Private Const FRAME_NORMAL As Integer = 2
    Private Const FRAME_ANIMATED As Integer = 3

    Private ReadOnly Property HasAnimationFrames() As Boolean
        Get
            Return Not frames Is Nothing AndAlso frames.Count > FRAME_ANIMATED
        End Get
    End Property

    Private Sub CreateFrames()
        CreateFrames(False)
    End Sub

    Private Sub CreateFrames(ByVal withAnimationFrames As Boolean)
        DestroyFrames()
        If (Not IsHandleCreated) Then
            Return
        End If
        If frames Is Nothing Then
            frames = New List(Of Image)()
        End If
        frames.Add(CreateBackgroundFrame(False, False, False, False, 0))
        frames.Add(CreateBackgroundFrame(True, True, False, True, 0))
        frames.Add(CreateBackgroundFrame(False, False, False, True, 0))
        If (Not withAnimationFrames) Then
            Return
        End If
        For i As Integer = 0 To framesCount - 1
            frames.Add(CreateBackgroundFrame(False, True, True, True, CSng(i) / (framesCount - 1.0F)))
        Next i
    End Sub

    Private Sub DestroyFrames()
        If Not frames Is Nothing Then
            Do While frames.Count > 0
                frames(frames.Count - 1).Dispose()
                frames.RemoveAt(frames.Count - 1)
            Loop
        End If
    End Sub

    Private Const animationLength As Integer = 300
    Private Const framesCount As Integer = 10
    Private currentFrame As Integer
    Private direction As Integer

    Private ReadOnly Property isAnimating() As Boolean
        Get
            Return direction <> 0
        End Get
    End Property

    Private Sub FadeIn()
        direction = 1
        x_timer.Enabled = True
    End Sub

    Private Sub FadeOut()
        direction = -1
        x_timer.Enabled = True
    End Sub

    Private Sub timer_Tick(ByVal sender As Object, ByVal e As EventArgs) Handles x_timer.Tick
        If (Not x_timer.Enabled) Then
            Return
        End If
        Refresh()
        currentFrame += direction
        If currentFrame = -1 Then
            currentFrame = 0
            x_timer.Enabled = False
            direction = 0
            Return
        End If
        If currentFrame = framesCount Then
            currentFrame = framesCount - 1
            x_timer.Enabled = False
            direction = 0
        End If
    End Sub

#End Region
End Class

