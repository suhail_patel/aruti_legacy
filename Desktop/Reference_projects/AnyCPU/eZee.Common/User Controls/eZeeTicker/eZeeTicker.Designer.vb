<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeTicker
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.htmlLabel1 = New System.Windows.Forms.WebBrowser
        Me.panel1 = New System.Windows.Forms.Panel
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'htmlLabel1
        '
        Me.htmlLabel1.IsWebBrowserContextMenuEnabled = False
        Me.htmlLabel1.Location = New System.Drawing.Point(3, 3)
        Me.htmlLabel1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.htmlLabel1.Name = "htmlLabel1"
        Me.htmlLabel1.ScrollBarsEnabled = False
        Me.htmlLabel1.Size = New System.Drawing.Size(545, 47)
        Me.htmlLabel1.TabIndex = 2
        Me.htmlLabel1.WebBrowserShortcutsEnabled = False
        '
        'panel1
        '
        Me.panel1.Controls.Add(Me.htmlLabel1)
        Me.panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panel1.Location = New System.Drawing.Point(0, 0)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(548, 53)
        Me.panel1.TabIndex = 1
        '
        'Timer1
        '
        '
        'eZeeTicker
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.panel1)
        Me.Name = "eZeeTicker"
        Me.Size = New System.Drawing.Size(548, 53)
        Me.panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents htmlLabel1 As System.Windows.Forms.WebBrowser
    Private WithEvents panel1 As System.Windows.Forms.Panel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer

End Class
