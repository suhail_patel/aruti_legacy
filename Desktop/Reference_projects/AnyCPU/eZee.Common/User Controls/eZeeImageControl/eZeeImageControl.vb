Imports system.Windows.Forms
Imports system.Drawing

Imports System.ComponentModel

Public Class eZeeImageControl
    Private ReadOnly mstrModuleName As String = "eZeeImageControl"

    Private obj_Frm As New eZeeImageControl_Preview
    Private m_Image As Image = Nothing
    Private m_FileChange As Boolean = False

    Public Enum PreviewAt
        TOP = 1
        BOTTOM = 2
        LEFT = 3
        RIGHT = 4
    End Enum

#Region " Property "

    Private mstrFileName As String = ""
    Private mstrFilePath As String = ""
    Private mstrMsg_Configuration As String = "Please set image path from Configuration." & vbCrLf & _
                        "You can set it from Configuration-> Menu-> Configuration Options-> General Tab."

    Private mstrMsg_FileNotFound As String = "Image file used is not found on specified path, the file is deleted or renamed."

    Private mblnShowCapture As Boolean = False
    Private mblnHideControls As Boolean = False
    Private mblnShowPreview As Boolean = True
    Private mShowAt As PreviewAt = PreviewAt.BOTTOM
    Private mblnFileMode As Boolean = True

    <Category("Custom"), DefaultValue(PreviewAt.BOTTOM)> _
    Public Property _ShowAt() As PreviewAt
        Get
            Return mShowAt
        End Get
        Set(ByVal value As PreviewAt)
            mShowAt = value
        End Set
    End Property

    <Category("Custom"), DefaultValue(True)> _
    Public Property _ShowPreview() As Boolean
        Get
            Return mblnShowPreview
        End Get
        Set(ByVal value As Boolean)
            mblnShowPreview = value
        End Set
    End Property

    <Category("Custom"), DefaultValue(False)> _
    Public Property _ShowCapture() As Boolean
        Get
            Return mblnShowCapture
        End Get
        Set(ByVal value As Boolean)
            mblnShowCapture = value
            objbtnCapture.Visible = mblnShowCapture
        End Set
    End Property

    <Category("Custom"), DefaultValue(False)> _
    Public Property _HideControls() As Boolean
        Get
            Return mblnHideControls
        End Get
        Set(ByVal value As Boolean)
            mblnHideControls = value
            objbtnCapture.Visible = Not mblnHideControls And mblnShowCapture
            objbtnAdd.Visible = Not mblnHideControls
            objbtnDelete.Visible = Not mblnHideControls
            objbtnRefresh.Visible = Not mblnHideControls
        End Set
    End Property

    <Category("Custom"), Browsable(False), DefaultValue("")> _
    Public Property _FileName() As String
        Get
            If mstrFileName = "" And m_Image IsNot Nothing Then
                mstrFileName = Guid.NewGuid.ToString & ".jpg"
            End If
            Return mstrFileName
        End Get
        Set(ByVal value As String)
            mstrFileName = IO.Path.GetFileName(value)
            Call objbtnRefresh_Click(Nothing, Nothing)
        End Set
    End Property

    <Category("Custom"), Browsable(False), DefaultValue("")> _
    Public Property _FilePath() As String
        Get
            Return mstrFilePath
        End Get
        Set(ByVal value As String)
            mstrFilePath = value
        End Set
    End Property

    <Category("Error Messages"), Browsable(False), DefaultValue("")> _
    Public WriteOnly Property _Msg_Configuration() As String
        Set(ByVal value As String)
            mstrMsg_Configuration = value
        End Set
    End Property

    <Category("Error Messages"), Browsable(False), DefaultValue("")> _
    Public WriteOnly Property _Msg_FileNotFound() As String
        Set(ByVal value As String)
            mstrMsg_FileNotFound = value
        End Set
    End Property

    <Category("Custom")> _
    Public Property _Image() As Image
        Get
            Return m_Image
        End Get
        Set(ByVal value As Image)
            m_Image = value
            'If m_Image Is Nothing Then
            '    objlblNoImage.Visible = True
            'Else
            '    objlblNoImage.Visible = False
            'End If
            'Call objbtnRefresh_Click(Nothing, Nothing)
            Call SetImage()
        End Set
    End Property

    ''' <summary>
    ''' There are 2 mode of this control. 
    ''' File mode = True, save image to image path, Database Mode
    ''' File mode = False Database Mode
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _FileMode() As Boolean
        Get
            Return mblnFileMode
        End Get
        Set(ByVal value As Boolean)
            mblnFileMode = value
            objbtnRefresh.Visible = mblnFileMode
        End Set
    End Property
#End Region

#Region " Public Function "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        objbtnCapture.Visible = mblnShowCapture
    End Sub

    Public Function GetImageFile() As String
        Return IIf(mstrFilePath.EndsWith("\"), mstrFilePath, mstrFilePath + "\") + mstrFileName
    End Function

    Public Function SaveImage() As Boolean
        If Not m_FileChange Then Return True
        Try
            If Not CheckValidPath() Then Return False

            If m_Image Is Nothing Then
                If My.Computer.FileSystem.FileExists(GetImageFile) Then
                    My.Computer.FileSystem.DeleteFile(GetImageFile)
                End If
                mstrFileName = ""
            Else
                If mstrFileName = "" Then
                    mstrFileName = Guid.NewGuid.ToString & ".png"
                End If
                m_Image.Save(GetImageFile, System.Drawing.Imaging.ImageFormat.Png)
            End If

            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
        End Try
    End Function

#End Region

#Region " Private Function "

    Private Function CheckValidPath() As Boolean
        If mblnFileMode AndAlso Not IO.Directory.Exists(mstrFilePath) Then
            'eZeeMsgBox.Show(mstrMsg_Configuration, enMsgBoxStyle.Information)
            MsgBox(mstrMsg_Configuration, MsgBoxStyle.Information)
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub SetImage()
        Try
            If m_Image Is Nothing Then
                If Not objPicImage.Image Is Nothing Then
                    objPicImage.Image.Dispose()
                    objPicImage.Image = Nothing
                End If

                If Not obj_Frm._Image Is Nothing Then
                    obj_Frm._Image.Dispose()
                    obj_Frm._Image = Nothing
                End If

                objPicImage.BackgroundImage = Global.eZee.Common.My.Resources.Resources.No_Image
            Else
                If m_Image.Size.Height > objPicImage.Height Or m_Image.Size.Width > objPicImage.Width Then
                    objPicImage.SizeMode = PictureBoxSizeMode.StretchImage
                Else
                    objPicImage.SizeMode = PictureBoxSizeMode.CenterImage
                End If

                objPicImage.Image = m_Image
                obj_Frm._Image = m_Image

                objPicImage.BackgroundImage = Nothing
            End If
        Catch ex As Exception
        Finally
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try
    End Sub

    Private Sub SetImageFromFile(ByVal strFile As String)
        Try
            If Not m_Image Is Nothing Then
                m_Image.Dispose()
                m_Image = Nothing
            End If

            If strFile <> "" And My.Computer.FileSystem.FileExists(strFile) Then
                m_Image = ImageClone(Drawing.Image.FromFile(strFile))
            End If

            Call SetImage()
        Catch ex As Exception
        Finally
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try
    End Sub

    Private Function ImageClone(ByVal pImg As Image) As Image
        Dim TmpBitmap As Image = New Drawing.Bitmap(pImg)
        Dim TmpBitmapGr As Graphics = Graphics.FromImage(TmpBitmap)
        TmpBitmapGr.DrawImage(pImg, New Rectangle(0, 0, pImg.Width, pImg.Height))

        Return TmpBitmap
    End Function

#End Region

#Region " Button "

    Private Sub objbtnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnRefresh.Click
        If Not mblnFileMode Then Exit Sub
        If mstrFileName <> "" And Not My.Computer.FileSystem.FileExists(GetImageFile) Then
            m_FileChange = False
            'eZeeMsgBox.Show(mstrMsg_FileNotFound, enMsgBoxStyle.Information)
            MsgBox(mstrMsg_FileNotFound, MsgBoxStyle.Information)
        End If
        Call SetImageFromFile(GetImageFile)
    End Sub

    Private Sub objbtnCapture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnCapture.Click
        If Not CheckValidPath() Then Exit Sub

        Dim objfrm As New eZeeImageControl_Capture
        Try
            objfrm._Image = m_Image

            If objfrm.DisplayDialog() Then
                m_Image = objfrm._Image
                m_FileChange = True
                Call SetImage()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objfrm.Dispose()
            objfrm = Nothing

            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try
    End Sub

    Private Sub objbtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdd.Click
        Try
            If Not CheckValidPath() Then Exit Sub

            ofdImageDialog.Filter = "All Image Formats (*.bmp;*.jpg;*.jpeg;*.gif;*.tif,*.png)|*.bmp;*.jpg;*.jpeg;*.gif;*.tif;*.png|Bitmaps (*.bmp)|*.bmp|GIFs (*.gif)|*.gif|JPEGs (*.jpg)|*.jpg;*.jpeg|TIFs (*.tif)|*.tif|PNGs (*.png)|*.png"

            If (ofdImageDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                Call SetImageFromFile(ofdImageDialog.FileName)
            End If

            m_FileChange = True

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub objbtnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDelete.Click
        Try
            Call SetImageFromFile("")
            m_FileChange = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " Preview "
    Private Sub objPicImage_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objPicImage.MouseEnter

        If Not mblnShowPreview Then Exit Sub

        If Not m_Image Is Nothing Then
            obj_Frm.Show()
            Select Case mShowAt
                Case PreviewAt.TOP
                    obj_Frm.Top = Me.ParentForm.Top + Me.Top - obj_Frm.Height + 20
                    obj_Frm.Left = Me.ParentForm.Left + Me.Left - Me.Width / 4
                Case PreviewAt.BOTTOM
                    obj_Frm.Top = Me.ParentForm.Top + Me.Top + Me.Height + 40
                    obj_Frm.Left = Me.ParentForm.Left + Me.Left - Me.Width / 4
                Case PreviewAt.LEFT
                    obj_Frm.Top = Me.ParentForm.Top + Me.Top - Me.Height / 4
                    obj_Frm.Left = Me.ParentForm.Left + Me.Left - obj_Frm.Width
                Case PreviewAt.RIGHT
                    obj_Frm.Top = Me.ParentForm.Top + Me.Parent.Top + Me.Top - Me.Height / 4
                    obj_Frm.Left = Me.ParentForm.Left + Me.Parent.Left + Me.Left + Me.Width - 20
                Case Else
                    obj_Frm.Top = Me.ParentForm.Top + Me.Top - Me.Height / 4
                    obj_Frm.Left = Me.ParentForm.Left + Me.Left - obj_Frm.Width
            End Select
        End If
    End Sub

    Private Sub objPicImage_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objPicImage.MouseLeave
        If Not mblnShowPreview Then Exit Sub
        If Not m_Image Is Nothing Then
            obj_Frm.Hide()
        End If
    End Sub
#End Region

End Class
