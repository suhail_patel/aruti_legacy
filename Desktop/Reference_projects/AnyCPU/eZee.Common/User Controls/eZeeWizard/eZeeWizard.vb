Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Windows.Forms
Imports System.Windows.Forms.Design
Imports System.ComponentModel.Design


''' Represents an extentable wizard control with basic page navigation functionality. 

<Designer(GetType(eZeeWizard.WizardDesigner))> _
Public Class eZeeWizard
    Inherits System.Windows.Forms.UserControl

#Region "Consts"
    Private Const FOOTER_AREA_HEIGHT As Integer = 48
    Private ReadOnly offsetCancel As New Point(84, 36)
    Private ReadOnly offsetNext As New Point(168, 36)
    'Private ReadOnly offsetBack As New Point(244, 36)
    Private ReadOnly offsetBack As New Point(252, 36)

    'private readonly Point offsetCancel = new Point(95, 36); 
    'private readonly Point offsetNext = new Point(195, 36); 
    'private readonly Point offsetBack = new Point(290, 36); 
#End Region

#Region "Fields"
    Private m_selectedPage As eZeeWizardPage = Nothing
    Private m_pages As WizardPagesCollection = Nothing
    Private m_headerImage As Image = Nothing
    Private m_welcomeImage As Image = Nothing
    Private m_headerFont As Font = Nothing
    Private m_headerTitleFont As Font = Nothing
    Private m_welcomeFont As Font = Nothing
    Private m_welcomeTitleFont As Font = Nothing
#End Region

#Region "Constructor&Dispose"

    ''' Creates a new instance of the <see cref="eZeeWizard"/> class. 

    Public Sub New()
        ' call required by designer 
        Me.InitializeComponent()

        ' reset control style to improove rendering (reduce flicker) 
        MyBase.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        MyBase.SetStyle(ControlStyles.DoubleBuffer, True)
        MyBase.SetStyle(ControlStyles.ResizeRedraw, True)
        MyBase.SetStyle(ControlStyles.UserPaint, True)

        ' reset dock style 
        MyBase.Dock = DockStyle.Fill

        ' init pages collection 
        Me.m_pages = New WizardPagesCollection(Me)
       
    End Sub

#End Region

#Region "Properties"

    ''' Gets or sets which edge of the parent container a control is docked to. 

    <DefaultValue(DockStyle.Fill)> _
    <Category("Layout")> _
    <Description("Gets or sets which edge of the parent container a control is docked to.")> _
    Public Shadows Property Dock() As DockStyle
        Get
            Return MyBase.Dock
        End Get
        Set(ByVal value As DockStyle)
            MyBase.Dock = value
        End Set
    End Property


    ''' Gets the collection of wizard pages in this tab control. 

    <Category("Wizard")> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Content)> _
    <Description("Gets the collection of wizard pages in this tab control.")> _
    Public ReadOnly Property Pages() As WizardPagesCollection
        Get
            Return Me.m_pages
        End Get
    End Property


    ''' Gets or sets the currently-selected wizard page. 

    <Browsable(False)> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property SelectedPage() As eZeeWizardPage
        Get
            Return Me.m_selectedPage
        End Get
        Set(ByVal value As eZeeWizardPage)
            ' select new page 
            Me.ActivatePage(value)
        End Set
    End Property


    ''' Gets or sets the currently-selected wizard page by index. 

    <Browsable(False)> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Friend Property SelectedIndex() As Integer
        Get
            Return Me.m_pages.IndexOf(Me.m_selectedPage)
        End Get
        Set(ByVal value As Integer)
            ' check if there are any pages 
            If Me.m_pages.Count = 0 Then
                ' reset invalid index 
                Me.ActivatePage(-1)
                Return
            End If

            ' validate page index 
            If value < -1 OrElse value >= Me.m_pages.Count Then
                Throw New ArgumentOutOfRangeException("SelectedIndex", value, "The page index must be between 0 and " + Convert.ToString(Me.m_pages.Count - 1))
            End If

            ' select new page 
            Me.ActivatePage(value)
        End Set
    End Property


    ''' Gets or sets the image displayed on the header of the standard pages. 

    <DefaultValue("")> _
    <Category("Wizard")> _
    <Description("Gets or sets the image displayed on the header of the standard pages.")> _
    Public Property HeaderImage() As Image
        Get
            Return Me.m_headerImage
        End Get
        Set(ByVal value As Image)
            If Me.m_headerImage IsNot value Then
                Me.m_headerImage = value
                Me.Invalidate()
            End If
        End Set
    End Property


    ''' Gets or sets the image displayed on the welcome and finish pages. 

    <DefaultValue("")> _
    <Category("Wizard")> _
    <Description("Gets or sets the image displayed on the welcome and finish pages.")> _
    Public Property WelcomeImage() As Image
        Get
            Return Me.m_welcomeImage
        End Get
        Set(ByVal value As Image)
            If Me.m_welcomeImage IsNot value Then
                Me.m_welcomeImage = value
                Me.Invalidate()
            End If
        End Set
    End Property


    ''' Gets or sets the font used to display the description of a standard page. 

    <Category("Appearance")> _
    <Description("Gets or sets the font used to display the description of a standard page.")> _
    Public Property HeaderFont() As Font
        Get
            If Me.m_headerFont Is Nothing Then
                Return Me.Font
            Else
                Return Me.m_headerFont
            End If
        End Get
        Set(ByVal value As Font)
            If Me.m_headerFont IsNot value Then
                Me.m_headerFont = value
                Me.Invalidate()
            End If
        End Set
    End Property
    Protected Function ShouldSerializeHeaderFont() As Boolean
        Return Me.m_headerFont IsNot Nothing
    End Function


    ''' Gets or sets the font used to display the title of a standard page. 

    <Category("Appearance")> _
    <Description("Gets or sets the font used to display the title of a standard page.")> _
    Public Property HeaderTitleFont() As Font
        Get
            If Me.m_headerTitleFont Is Nothing Then
                Return New Font(Me.Font.FontFamily, Me.Font.Size + 2, FontStyle.Bold)
            Else
                Return Me.m_headerTitleFont
            End If
        End Get
        Set(ByVal value As Font)
            If Me.m_headerTitleFont IsNot value Then
                Me.m_headerTitleFont = value
                Me.Invalidate()
            End If
        End Set
    End Property
    Protected Function ShouldSerializeHeaderTitleFont() As Boolean
        Return Me.m_headerTitleFont IsNot Nothing
    End Function


    ''' Gets or sets the font used to display the description of a welcome of finish page. 

    <Category("Appearance")> _
    <Description("Gets or sets the font used to display the description of a welcome of finish page.")> _
    Public Property WelcomeFont() As Font
        Get
            If Me.m_welcomeFont Is Nothing Then
                Return Me.Font
            Else
                Return Me.m_welcomeFont
            End If
        End Get
        Set(ByVal value As Font)
            If Me.m_welcomeFont IsNot value Then
                Me.m_welcomeFont = value
                Me.Invalidate()
            End If
        End Set
    End Property
    Protected Function ShouldSerializeWelcomeFont() As Boolean
        Return Me.m_welcomeFont IsNot Nothing
    End Function


    ''' Gets or sets the font used to display the title of a welcome of finish page. 

    <Category("Appearance")> _
    <Description("Gets or sets the font used to display the title of a welcome of finish page.")> _
    Public Property WelcomeTitleFont() As Font
        Get
            If Me.m_welcomeTitleFont Is Nothing Then
                Return New Font(Me.Font.FontFamily, Me.Font.Size + 10, FontStyle.Bold)
            Else
                Return Me.m_welcomeTitleFont
            End If
        End Get
        Set(ByVal value As Font)
            If Me.m_welcomeTitleFont IsNot value Then
                Me.m_welcomeTitleFont = value
                Me.Invalidate()
            End If
        End Set
    End Property
    Protected Function ShouldSerializeWelcomeTitleFont() As Boolean
        Return Me.m_welcomeTitleFont IsNot Nothing
    End Function


    ''' Gets or sets the enabled state of the Next button. 

    <Browsable(False)> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property NextEnabled() As Boolean
        Get
            Return Me.objbuttonNext.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.objbuttonNext.Enabled = value
        End Set
    End Property


    ''' Gets or sets the enabled state of the back button. 

    <Browsable(False)> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property BackEnabled() As Boolean
        Get
            Return Me.objbuttonBack.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.objbuttonBack.Enabled = value
        End Set
    End Property


    ''' Gets or sets the enabled state of the cancel button. 

    <Browsable(False)> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property CancelEnabled() As Boolean
        Get
            Return Me.objbuttonCancel.Enabled
        End Get
        Set(ByVal value As Boolean)

            Me.objbuttonCancel.Enabled = value
        End Set
    End Property


    ''' Gets or sets the text displayed by the Next button. 
    <Browsable(False)> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property NextText() As String
        Get
            Return Me.objbuttonNext.Text
        End Get
        Set(ByVal value As String)
            Me.objbuttonNext.Text = value
        End Set
    End Property


    ''' Gets or sets the text displayed by the back button. 
    <Browsable(False)> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property BackText() As String
        Get
            Return Me.objbuttonBack.Text
        End Get
        Set(ByVal value As String)
            Me.objbuttonBack.Text = value
        End Set
    End Property

    Private mstrCancelText As String = "Cancel"
    ''' Gets or sets the text displayed by the cancel button. 
    <Browsable(False)> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property CancelText() As String
        Get
            Return mstrCancelText
        End Get
        Set(ByVal value As String)
            If Me.m_selectedPage.Style <> eZeeWizardPageStyle.Finish Then
                Me.objbuttonCancel.Text = value
            End If
            mstrCancelText = value
        End Set
    End Property

    Dim mstrFinishText As String = "Finish"
    <Browsable(False)> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
   Public Property FinishText() As String
        Get
            Return mstrFinishText
        End Get
        Set(ByVal value As String)
            If Me.m_selectedPage.Style = eZeeWizardPageStyle.Finish Then
                Me.objbuttonCancel.Text = value
            End If

            mstrFinishText = value
        End Set
    End Property
#End Region

#Region "Methods"

    ''' Swithes forward to next wizard page. 

    Public Sub [Next]()
        ' check if we're on the last page (finish) 
        If Me.SelectedIndex = Me.m_pages.Count - 1 Then
            Me.objbuttonNext.Enabled = False
        Else
            ' handle page switch 
            Me.OnBeforeSwitchPages(New BeforeSwitchPagesEventArgs(Me.SelectedIndex, Me.SelectedIndex + 1))
        End If
    End Sub


    ''' Swithes backward to previous wizard page. 

    Public Sub Back()
        If Me.SelectedIndex = 0 Then
            Me.objbuttonBack.Enabled = False
        Else
            ' handle page switch 
            Me.OnBeforeSwitchPages(New BeforeSwitchPagesEventArgs(Me.SelectedIndex, Me.SelectedIndex - 1))
        End If
    End Sub


    ''' Activates the specified wizard bage. 
    ''' <param name="index">An Integer value representing the zero-based index of the page to be activated.</param> 
    Private Sub ActivatePage(ByVal index As Integer)
        ' check if new page is invalid 
        If index < 0 OrElse index >= Me.m_pages.Count Then
            ' filter out 
            Return
        End If

        ' get new page 
        Dim page As eZeeWizardPage = DirectCast(Me.m_pages(index), eZeeWizardPage)

        ' activate page 
        Me.ActivatePage(page)
    End Sub


    ''' Activates the specified wizard bage. 
    ''' <param name="page">A WizardPage object representing the page to be activated.</param> 
    Private Sub ActivatePage(ByVal page As eZeeWizardPage)
        ' validate given page 
        If Me.m_pages.Contains(page) = False Then
            ' filter out 
            Return
        End If

        ' deactivate current page 
        If Me.m_selectedPage IsNot Nothing Then
            Me.m_selectedPage.Visible = False
        End If

        ' activate new page 
        Me.m_selectedPage = page

        If Me.m_selectedPage IsNot Nothing Then
            'Ensure that this panel displays inside the wizard 
            Me.m_selectedPage.Parent = Me
            If Me.Contains(Me.m_selectedPage) = False Then
                Me.Container.Add(Me.m_selectedPage)
            End If
            If Me.m_selectedPage.Style = eZeeWizardPageStyle.Finish Then
                Me.objbuttonCancel.Text = mstrFinishText
                Me.objbuttonCancel.DialogResult = DialogResult.OK
            Else
                Me.objbuttonCancel.Text = mstrCancelText
                Me.objbuttonCancel.DialogResult = DialogResult.Cancel
            End If

            'Make it fill the space 
            Me.m_selectedPage.SetBounds(0, 0, Me.Width, Me.Height - FOOTER_AREA_HEIGHT)
            Me.m_selectedPage.Visible = True
            Me.m_selectedPage.BringToFront()
            Me.FocusFirstTabIndex(Me.m_selectedPage)
        End If

        'What should the back button say 
        If Me.SelectedIndex > 0 Then
            objbuttonBack.Enabled = True
        Else
            objbuttonBack.Enabled = False
        End If

        'What should the Next button say 
        If Me.SelectedIndex < Me.m_pages.Count - 1 Then
            Me.objbuttonNext.Enabled = True
        Else
            If Me.DesignMode = False Then
                ' at runtime disable back button (we finished; there's no point going back) 
                objbuttonBack.Enabled = False
            End If
            Me.objbuttonNext.Enabled = False
        End If

        ' refresh 
        If Me.m_selectedPage IsNot Nothing Then
            Me.m_selectedPage.Invalidate()
        Else
            Me.Invalidate()
        End If
    End Sub


    ''' Focus the control with a lowest tab index in the given container. 
    ''' <param name="container">A Control object to pe processed.</param> 
    Private Sub FocusFirstTabIndex(ByVal container As Control)
        ' init search result varialble 
        Dim searchResult As Control = Nothing

        ' find the control with the lowest tab index 
        For Each control As Control In container.Controls
            If control.CanFocus AndAlso (searchResult Is Nothing OrElse control.TabIndex < searchResult.TabIndex) Then
                searchResult = control
            End If
        Next

        ' check if anything searchResult 
        If searchResult IsNot Nothing Then
            ' focus found control 
            searchResult.Focus()
        Else
            ' focus the container 
            container.Focus()
        End If
    End Sub


    ''' Raises the SwitchPages event. 
    ''' <param name="e">A WizardPageEventArgs object that holds event data.</param> 
    Protected Overridable Sub OnBeforeSwitchPages(ByVal e As BeforeSwitchPagesEventArgs)
        ' check if there are subscribers 
        'If Me.BeforeSwitchPages Is Nothing Then
        ' raise BeforeSwitchPages event 
        RaiseEvent BeforeSwitchPages(Me, e)
        'End If

        ' check if user canceled 
        If e.Cancel Then
            ' filter 
            Return
        End If

        ' activate new page 
        Me.ActivatePage(e.NewIndex)

        ' raise the after event 
        Me.OnAfterSwitchPages(TryCast(e, AfterSwitchPagesEventArgs))
    End Sub


    ''' Raises the SwitchPages event. 
    ''' <param name="e">A WizardPageEventArgs object that holds event data.</param> 
    Protected Overridable Sub OnAfterSwitchPages(ByVal e As AfterSwitchPagesEventArgs)
        ' check if there are subscribers 
        'If Me.AfterSwitchPages IsNot Nothing Then
        ' raise AfterSwitchPages event 
        RaiseEvent AfterSwitchPages(Me, e)
        'End If
    End Sub



    ''' Raises the Cancel event. 
    ''' <param name="e">A CancelEventArgs object that holds event data.</param> 
    Protected Overridable Sub OnCancel(ByVal e As CancelEventArgs)
        ' check if there are subscribers 
        'If Me.Cancel IsNot Nothing Then
        ' raise Cancel event 
        RaiseEvent Cancel(Me, e)
        'End If

        ' check if user canceled 
        If e.Cancel Then
            ' cancel closing (when ShowDialog is used) 
            Me.ParentForm.DialogResult = DialogResult.None
        Else
            ' ensure parent form is closed (even when ShowDialog is not used) 
            Me.ParentForm.Close()
        End If
    End Sub


    ''' Raises the Finish event. 
    ''' <param name="e">A EventArgs object that holds event data.</param> 
    Protected Overridable Sub OnFinish(ByVal e As EventArgs)
        ' check if there are subscribers 
        'If Me.Finish IsNot Nothing Then
        ' raise Finish event 
        RaiseEvent Finish(Me, e)
        'End If

        ' ensure parent form is closed (even when ShowDialog is not used) 
        Me.ParentForm.Close()
    End Sub

    ''' Raises the Load event. 
    Protected Overrides Sub OnLoad(ByVal e As EventArgs)
        ' raise the Load event 
        MyBase.OnLoad(e)

        ' activate first page, if exists 
        If Me.m_pages.Count > 0 Then
            If Not m_pages.Item(0).IsDisposed Then
                Me.ActivatePage(0)
            End If
        End If
    End Sub

    ''' Raises the Resize event. 
    Protected Overloads Overrides Sub OnResize(ByVal e As EventArgs)
        ' raise the Resize event 
        MyBase.OnResize(e)

        ' resize the selected page to fit the wizard 
        If Me.m_selectedPage IsNot Nothing Then
            Me.m_selectedPage.SetBounds(0, 0, Me.Width, Me.Height - FOOTER_AREA_HEIGHT)
        End If

        ' position navigation buttons 
        Me.objbuttonCancel.Location = New Point(Me.Width - Me.offsetCancel.X, Me.Height - Me.offsetCancel.Y)
        Me.objbuttonNext.Location = New Point(Me.Width - Me.offsetNext.X, Me.Height - Me.offsetNext.Y)
        Me.objbuttonBack.Location = New Point(Me.Width - Me.offsetBack.X, Me.Height - Me.offsetBack.Y)
    End Sub


    ''' Raises the Paint event. 

    Protected Overloads Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        ' raise the Paint event 
        MyBase.OnPaint(e)

        Dim bottomRect As Rectangle = Me.ClientRectangle
        bottomRect.Y = Me.Height - FOOTER_AREA_HEIGHT
        bottomRect.Height = FOOTER_AREA_HEIGHT
        ControlPaint.DrawBorder3D(e.Graphics, bottomRect, Border3DStyle.Etched, Border3DSide.Top)
    End Sub


    ''' Raises the ControlAdded event. 

    Protected Overloads Overrides Sub OnControlAdded(ByVal e As ControlEventArgs)
        ' prevent other controls from being added directly to the wizard 
        If TypeOf e.Control Is eZeeWizardPage = False AndAlso e.Control IsNot Me.objbuttonCancel AndAlso e.Control IsNot Me.objbuttonNext AndAlso e.Control IsNot Me.objbuttonBack Then
            ' add the control to the selected page 
            If Me.m_selectedPage IsNot Nothing Then
                Me.m_selectedPage.Controls.Add(e.Control)
            End If
        Else
            ' raise the ControlAdded event 
            MyBase.OnControlAdded(e)
        End If
    End Sub

#End Region

#Region "Events"

    ''' Occurs before the wizard pages are switched, giving the user a chance to validate. 

    <Category("Wizard")> _
    <Description("Occurs before the wizard pages are switched, giving the user a chance to validate.")> _
    Public Event BeforeSwitchPages As BeforeSwitchPagesEventHandler

    ''' Occurs after the wizard pages are switched, giving the user a chance to setup the new page. 

    <Category("Wizard")> _
    <Description("Occurs after the wizard pages are switched, giving the user a chance to setup the new page.")> _
    Public Event AfterSwitchPages As AfterSwitchPagesEventHandler

    ''' Occurs when wizard is canceled, giving the user a chance to validate. 

    <Category("Wizard")> _
    <Description("Occurs when wizard is canceled, giving the user a chance to validate.")> _
    Public Event Cancel As CancelEventHandler

    ''' Occurs when wizard is finished, giving the user a chance to do extra stuff. 

    <Category("Wizard")> _
    <Description("Occurs when wizard is finished, giving the user a chance to do extra stuff.")> _
    Public Event Finish As EventHandler

    ''' Represents the method that will handle the BeforeSwitchPages event of the Wizard control. 

    Public Delegate Sub BeforeSwitchPagesEventHandler(ByVal sender As Object, ByVal e As BeforeSwitchPagesEventArgs)

    ''' Represents the method that will handle the AfterSwitchPages event of the Wizard control. 

    Public Delegate Sub AfterSwitchPagesEventHandler(ByVal sender As Object, ByVal e As AfterSwitchPagesEventArgs)
#End Region

#Region "Events handlers"

    ''' Handles the Click event of objbuttonNext. 

    Private Sub buttonNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbuttonNext.Click
        Me.[Next]()
    End Sub


    ''' Handles the Click event of objbuttonBack. 

    Private Sub buttonBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbuttonBack.Click
        Me.Back()
    End Sub


    ''' Handles the Click event of objbuttonCancel. 

    Private Sub buttonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbuttonCancel.Click
        ' check if button is cancel mode 
        If Me.objbuttonCancel.DialogResult = DialogResult.Cancel Then
            Me.OnCancel(New CancelEventArgs())
        ElseIf Me.objbuttonCancel.DialogResult = DialogResult.OK Then
            ' check if button is finish mode 
            Me.OnFinish(EventArgs.Empty)
        End If
    End Sub
#End Region

#Region "Inner classes"

    ''' Represents a designer for the wizard control. 

    Friend Class WizardDesigner
        Inherits ParentControlDesigner

#Region "Methods"

        ''' Overrides the handling of Mouse clicks to allow back-next to work in the designer. 
        ''' <param name="msg">A Message value.</param> 
        Protected Overloads Overrides Sub WndProc(ByRef msg As Message)
            ' declare PInvoke constants 
            Const WM_LBUTTONDOWN As Integer = 513
            Const WM_LBUTTONDBLCLK As Integer = 515

            ' check message 
            If msg.Msg = WM_LBUTTONDOWN OrElse msg.Msg = WM_LBUTTONDBLCLK Then
                ' get the control under the mouse 
                Dim ss As ISelectionService = DirectCast(GetService(GetType(ISelectionService)), ISelectionService)

                If TypeOf ss.PrimarySelection Is eZeeWizard Then
                    Dim wizard As eZeeWizard = DirectCast(ss.PrimarySelection, eZeeWizard)

                    ' extract the mouse position 
                    Dim xPos As Integer = CShort((CInt(msg.LParam) And 65535))
                    Dim yPos As Integer = CShort(((CInt(msg.LParam) And 4294901760) >> 16))
                    Dim mousePos As New Point(xPos, yPos)

                    If msg.HWnd = wizard.objbuttonNext.Handle Then
                        If wizard.objbuttonNext.Enabled AndAlso wizard.objbuttonNext.ClientRectangle.Contains(mousePos) Then
                            'Press the button 
                            wizard.[Next]()
                        End If
                    ElseIf msg.HWnd = wizard.objbuttonBack.Handle Then
                        If wizard.objbuttonBack.Enabled AndAlso wizard.objbuttonBack.ClientRectangle.Contains(mousePos) Then
                            'Press the button 
                            wizard.Back()
                        End If
                    End If

                    ' filter message 
                    Return
                End If
            End If

            ' forward message 
            MyBase.WndProc(msg)
        End Sub


        ''' Prevents the grid from being drawn on the Wizard. 

        Protected Overloads ReadOnly Property DrawGrid() As Boolean
            Get
                Return False
            End Get
        End Property
#End Region

    End Class


    ''' Provides data for the AfterSwitchPages event of the Wizard control. 

    Public Class AfterSwitchPagesEventArgs
        Inherits EventArgs

#Region "Fields"
        Private m_oldIndex As Integer
        Protected m_newIndex As Integer
#End Region

#Region "Constructor"

        ''' Creates a new instance of the WizardPageEventArgs class. 
        ''' <param name="oldIndex">An integer value representing the index of the old page.</param> 
        ''' <param name="newIndex">An integer value representing the index of the new page.</param> 
        Friend Sub New(ByVal oldIndex As Integer, ByVal newIndex As Integer)
            Me.m_oldIndex = oldIndex
            Me.m_newIndex = newIndex
        End Sub

#End Region

#Region "Properties"

        ''' Gets the index of the old page. 

        Public ReadOnly Property OldIndex() As Integer
            Get
                Return Me.m_oldIndex
            End Get
        End Property


        ''' Gets or sets the index of the new page. 

        Public ReadOnly Property NewIndex() As Integer
            Get
                Return Me.m_newIndex
            End Get
        End Property
#End Region

    End Class


    ''' Provides data for the BeforeSwitchPages event of the Wizard control. 

    Public Class BeforeSwitchPagesEventArgs
        Inherits AfterSwitchPagesEventArgs

#Region "Fields"
        Private m_cancel As Boolean = False
#End Region

#Region "Constructor"

        ''' Creates a new instance of the WizardPageEventArgs class. 
        ''' <param name="oldIndex">An integer value representing the index of the old page.</param> 
        ''' <param name="newIndex">An integer value representing the index of the new page.</param> 
        Friend Sub New(ByVal oldIndex As Integer, ByVal newIndex As Integer)
            ' nothing 
            MyBase.New(oldIndex, newIndex)
        End Sub

#End Region

#Region "Properties"

        ''' Indicates whether the page switch should be canceled. 

        Public Property Cancel() As Boolean
            Get
                Return Me.m_cancel
            End Get
            Set(ByVal value As Boolean)
                Me.m_cancel = value
            End Set
        End Property


        ''' Gets or sets the index of the new page. 

        Public Shadows Property NewIndex() As Integer
            Get
                Return MyBase.NewIndex
            End Get
            Set(ByVal value As Integer)
                'MyBase.NewIndex = value
            End Set
        End Property
#End Region


    End Class
#End Region


End Class

