//************************************************************************************************************************************
//Class Name : clsCSVData.vb
//Purpose    : 
//Date       : 05 Feb 2008
//Written By : Naimish
//Modified   :	
//************************************************************************************************************************************
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System;

public class clsCSVData : IDisposable
{

	private DataSet dsCSV;
	private char mSeparator = ',';
	private char mTextQualifier = '\"';
	private string[] mData;
	private bool mHeader;
         
    private Regex regQuote = new Regex("^(\\x22)(.*)(\\x22)(\\s*,)(.*)$", RegexOptions.IgnoreCase | RegexOptions.RightToLeft);
    
	private Regex regNormal = new Regex("^([^,]*)(\\s*,)(.*)$", RegexOptions.IgnoreCase | RegexOptions.RightToLeft);
	private Regex regQuoteLast = new Regex("^(\\x22)([\\x22*]{2,})(\\x22)$", RegexOptions.IgnoreCase);
	private Regex regNormalLast = new Regex("^.*$", RegexOptions.IgnoreCase);

	protected bool Disposed;

#region  Load CSV 
	//
	// Load CSV
	//
	public void LoadCSV(string CSVFile)
	{
		LoadCSV(CSVFile, false);
	}

	//
	// Load CSV - Has Header
	//
	public void LoadCSV(string CSVFile, bool HasHeader)
	{
		mHeader = HasHeader;
		SetupRegEx();

		if (File.Exists(CSVFile) == false)
		{
			throw new Exception(CSVFile + " does not exist.");
		}

		if (dsCSV != null)
		{
			dsCSV.Clear();
			dsCSV.Tables.Clear();
			dsCSV.Dispose();
			dsCSV = null;
		}

		dsCSV = new DataSet("CSV");
		dsCSV.Tables.Add("CSVData");

		StreamReader sr = new StreamReader(CSVFile);
		int idx = 0;
		bool bFirstLine = true;
		DataRow dr = null;

		while (sr.Peek() > -1)
		{
			ProcessLine(sr.ReadLine());

			//
			// Create Columns
			//
			if (bFirstLine == true)
			{
				for (idx = 0; idx <= mData.GetUpperBound(0); idx++)
				{
					if (mHeader == true)
					{
						dsCSV.Tables["CSVData"].Columns.Add(mData[idx], typeof(string));
					}
					else
					{
						dsCSV.Tables["CSVData"].Columns.Add("Column" + idx, typeof(string));
					}
				}
			}

			//
			// Add Data
			//
			if (! (bFirstLine == true & mHeader == true))
			{
				dr = dsCSV.Tables["CSVData"].NewRow();

				for (idx = 0; idx <= mData.GetUpperBound(0); idx++)
				{
					dr[idx] = mData[idx];
				}

				dsCSV.Tables["CSVData"].Rows.Add(dr);
				dsCSV.AcceptChanges();
			}

			bFirstLine = false;
		}

		sr.Close();
	}

	//
	// Load CSV with custom separator
	//
	public void LoadCSV(string CSVFile, char Separator)
	{
		LoadCSV(CSVFile, Separator, false);
	}

	//
	// Load CSV with custom separator and Has Header
	//
	public void LoadCSV(string CSVFile, char Separator, bool HasHeader)
	{
		mSeparator = Separator;
		try
		{
			LoadCSV(CSVFile, HasHeader);
		}
		catch (Exception ex)
		{
			throw new Exception("CSV Error", ex);
		}
	}

	//
	// Load CSV with custom separator and text qualifier
	//
	public void LoadCSV(string CSVFile, char Separator, char TxtQualifier)
	{
		LoadCSV(CSVFile, Separator, TxtQualifier, false);
	}

	//
	// Load CSV with custom separator and text qualifier
	//
	public void LoadCSV(string CSVFile, char Separator, char TxtQualifier, bool HasHeader)
	{
		mSeparator = Separator;
		mTextQualifier = TxtQualifier;
		try
		{
			LoadCSV(CSVFile, HasHeader);
		}
		catch (Exception ex)
		{
			throw new Exception("CSV Error", ex);
		}
	}
#endregion

#region  Process Line 
	//
	// Process Line
	//
	private void ProcessLine(string sLine)
	{
		string sData = null;
		//Dim iSep As Integer
		//Dim iQuote As String
		Match m = null;
		int idx = 0;
		MatchCollection mc = null;

		mData = null;
		sLine = sLine.Replace("\t", "    "); //Replace tab with 4 spaces
		sLine = sLine.Trim();

		while (sLine.Length > 0)
		{
			sData = "";

			if (regQuote.IsMatch(sLine))
			{
				mc = regQuote.Matches(sLine);
				//
				// "text",<rest of the line>
				//
				m = regQuote.Match(sLine);
				sData = m.Groups[2].Value;
				sLine = m.Groups[5].Value;
			}
			else if (regQuoteLast.IsMatch(sLine))
			{
				//
				// "text"
				//
				m = regQuoteLast.Match(sLine);
				sData = m.Groups[2].Value;
				sLine = "";
			}
			else if (regNormal.IsMatch(sLine))
			{
				//
				// text,<rest of the line>
				//
				m = regNormal.Match(sLine);
				sData = m.Groups[1].Value;
				sLine = m.Groups[3].Value;
			}
			else if (regNormalLast.IsMatch(sLine))
			{
				//
				// text
				//
				m = regNormalLast.Match(sLine);
				sData = m.Groups[0].Value;
				sLine = "";
			}
			else
			{
				//
				// ERROR!!!!!
				//
				sData = "";
				sLine = "";
			}

			sData = sData.Trim();
			sLine = sLine.Trim();

			if (mData == null)
			{
				mData = new string[1];
				idx = 0;
			}
			else
			{
				idx = mData.GetUpperBound(0) + 1;
				Array.Resize(ref mData, idx + 1);
			}

			mData[idx] = sData;
		}
	}
#endregion

#region  Regular Expressions 
	//
	// Set up Regular Expressions
	//
	private void SetupRegEx()
	{
		string sQuote = "^(%Q)(.*)(%Q)(\\s*%S)(.*)$";
		string sNormal = "^([^%S]*)(\\s*%S)(.*)$";
		string sQuoteLast = "^(%Q)(.*)(%Q$)";
		string sNormalLast = "^.*$";
		string sSep = null;
		string sQual = null;

		if (regQuote != null)
		{
			regQuote = null;
		}
		if (regNormal != null)
		{
			regNormal = null;
		}
		if (regQuoteLast != null)
		{
			regQuoteLast = null;
		}
		if (regNormalLast != null)
		{
			regNormalLast = null;
		}

		sSep = mSeparator.ToString();
        sQual = mTextQualifier.ToString();

		if ((".$^{[(|)]}*+?\\".IndexOf(sSep, 0) + 1) > 0)
		{
			sSep = "\\" + sSep;
		}
		if ((".$^{[(|)]}*+?\\".IndexOf(sQual, 0) + 1) > 0)
		{
			sQual = "\\" + sQual;
		}

		sQuote = sQuote.Replace("%S", sSep);
		sQuote = sQuote.Replace("%Q", sQual);
		sNormal = sNormal.Replace("%S", sSep);
		sQuoteLast = sQuoteLast.Replace("%Q", sQual);

		regQuote = new Regex(sQuote, RegexOptions.IgnoreCase | RegexOptions.RightToLeft);
		regNormal = new Regex(sNormal, RegexOptions.IgnoreCase | RegexOptions.RightToLeft);
		regQuoteLast = new Regex(sQuoteLast, RegexOptions.IgnoreCase | RegexOptions.RightToLeft);
		regNormalLast = new Regex(sNormalLast, RegexOptions.IgnoreCase | RegexOptions.RightToLeft);
	}
#endregion

#region  Save As 
	//
	// Save data as XML
	//
	public void SaveAsXML(string sXMLFile)
	{
		if (dsCSV == null)
		{
			return;
		}
		dsCSV.WriteXml(sXMLFile);
	}

	//
	// Save data as CSV
	//
	public void SaveAsCSV(string sCSVFile)
	{
		if (dsCSV == null)
		{
			return;
		}

		string sLine = null;
		StreamWriter sw = new StreamWriter(sCSVFile);
		int iCol = 0;

		foreach (DataRow dr in dsCSV.Tables["CSVData"].Rows)
		{
			sLine = "";
			for (iCol = 0; iCol < dsCSV.Tables["CSVData"].Columns.Count; iCol++)
			{
				if (sLine.Length > 0)
				{
					sLine += mSeparator;
				}
				if (! (dr[iCol] == DBNull.Value))
				{
					if ((dr[iCol].ToString().IndexOf(mSeparator, 0) + 1) > 0)
					{
						sLine += mTextQualifier + dr[iCol].ToString() + mTextQualifier;
					}
					else
					{
						sLine += dr[iCol];
					}
				}
			}

			sw.WriteLine(sLine);
		}

		sw.Flush();
		sw.Close();
		sw = null;
	}

	//
	// Save data as CSV
	//
	public void SaveAsCSV(DataSet CSVDataSet, string sCSVFile)
	{
		if (CSVDataSet == null)
		{
			return;
		}

		string sLine = "";
		StreamWriter sw = new StreamWriter(sCSVFile);
		int iCol = 0;

		sLine = "";
		foreach (DataColumn col in CSVDataSet.Tables[0].Columns)
		{
			if (sLine.Length > 0)
			{
				sLine += mSeparator;
			}
			if ((col.ColumnName.IndexOf(mSeparator, 0) + 1) > 0)
			{
				sLine += mTextQualifier + col.ColumnName + mTextQualifier;
			}
			else
			{
				sLine += col.ColumnName;
			}
		}

		sw.WriteLine(sLine);

		foreach (DataRow dr in CSVDataSet.Tables[0].Rows)
		{
			sLine = "";
			for (iCol = 0; iCol < CSVDataSet.Tables[0].Columns.Count; iCol++)
			{
				if (sLine.Length > 0)
				{
					sLine += mSeparator;
				}
				if (! (dr[iCol] == DBNull.Value))
				{
					if ((dr[iCol].ToString().IndexOf(mSeparator, 0) + 1) > 0)
					{
						sLine += mTextQualifier + dr[iCol].ToString()  + mTextQualifier;
					}
					else
					{
						sLine += dr[iCol];
					}
				}
			}

			sw.WriteLine(sLine);
		}

		sw.Flush();
		sw.Close();
		sw = null;
	}
#endregion

#region  Properties 
	//
	// Separator Property
	//
	public char Separator
	{
		get
		{
			return mSeparator;
		}
		set
		{
			mSeparator = value;
			SetupRegEx();
		}
	}

	//
	// Qualifier Property
	//
	public char TextQualifier
	{
		get
		{
			return mTextQualifier;
		}
		set
		{
			mTextQualifier = value;
			SetupRegEx();
		}
	}

	//
	// Dataset Property
	//
	public DataSet CSVDataSet
	{
		get
		{
			return dsCSV;
		}
	}
#endregion

#region  Dispose and Finalize 
	//
	// Dispose
	//
	public void Dispose()
	{
		Dispose(true);
	}

	protected virtual void Dispose(bool disposing)
	{
		if (Disposed)
		{
			return;
		}

		if (disposing)
		{
			Disposed = true;

			GC.SuppressFinalize(this);
		}

		if (dsCSV != null)
		{
			dsCSV.Clear();
			dsCSV.Tables.Clear();
			dsCSV.Dispose();
			dsCSV = null;
		}
	}

	//
	// Finalize
	//
	~clsCSVData()
	{
		Dispose(false);
	}
#endregion

}
