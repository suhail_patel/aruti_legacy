namespace sqlExpressSetup
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtScript1 = new System.Windows.Forms.RichTextBox();
            this.txtScript2 = new System.Windows.Forms.RichTextBox();
            this.txtScript3 = new System.Windows.Forms.RichTextBox();
            this.txtScript4 = new System.Windows.Forms.RichTextBox();
            this.txtScript5 = new System.Windows.Forms.RichTextBox();
            this.txtScript10 = new System.Windows.Forms.RichTextBox();
            this.txtScript9 = new System.Windows.Forms.RichTextBox();
            this.txtScript8 = new System.Windows.Forms.RichTextBox();
            this.txtScript7 = new System.Windows.Forms.RichTextBox();
            this.txtScript6 = new System.Windows.Forms.RichTextBox();
            this.pgbInstall = new System.Windows.Forms.ProgressBar();
            this.lblTest = new System.Windows.Forms.Label();
            this.tmrtimer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Instance Name";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 143);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(276, 117);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SQL Express";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(101, 75);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 27);
            this.button1.TabIndex = 4;
            this.button1.Text = "Install";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(101, 49);
            this.textBox2.Name = "textBox2";
            this.textBox2.PasswordChar = '*';
            this.textBox2.Size = new System.Drawing.Size(133, 20);
            this.textBox2.TabIndex = 3;
            this.textBox2.Text = "pRofessionalaRuti999";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "SA Password";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(101, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(133, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "aPayroll";
            // 
            // txtScript1
            // 
            this.txtScript1.Location = new System.Drawing.Point(12, 135);
            this.txtScript1.Name = "txtScript1";
            this.txtScript1.Size = new System.Drawing.Size(276, 116);
            this.txtScript1.TabIndex = 2;
            this.txtScript1.Text = resources.GetString("txtScript1.Text");
            this.txtScript1.Visible = false;
            this.txtScript1.WordWrap = false;
            // 
            // txtScript2
            // 
            this.txtScript2.Location = new System.Drawing.Point(12, 257);
            this.txtScript2.Name = "txtScript2";
            this.txtScript2.Size = new System.Drawing.Size(276, 116);
            this.txtScript2.TabIndex = 3;
            this.txtScript2.Text = resources.GetString("txtScript2.Text");
            this.txtScript2.Visible = false;
            // 
            // txtScript3
            // 
            this.txtScript3.Location = new System.Drawing.Point(12, 380);
            this.txtScript3.Name = "txtScript3";
            this.txtScript3.Size = new System.Drawing.Size(276, 116);
            this.txtScript3.TabIndex = 4;
            this.txtScript3.Text = resources.GetString("txtScript3.Text");
            this.txtScript3.Visible = false;
            // 
            // txtScript4
            // 
            this.txtScript4.Location = new System.Drawing.Point(12, 502);
            this.txtScript4.Name = "txtScript4";
            this.txtScript4.Size = new System.Drawing.Size(276, 116);
            this.txtScript4.TabIndex = 5;
            this.txtScript4.Text = resources.GetString("txtScript4.Text");
            this.txtScript4.Visible = false;
            // 
            // txtScript5
            // 
            this.txtScript5.Location = new System.Drawing.Point(11, 624);
            this.txtScript5.Name = "txtScript5";
            this.txtScript5.Size = new System.Drawing.Size(276, 116);
            this.txtScript5.TabIndex = 6;
            this.txtScript5.Text = resources.GetString("txtScript5.Text");
            this.txtScript5.Visible = false;
            // 
            // txtScript10
            // 
            this.txtScript10.Location = new System.Drawing.Point(293, 624);
            this.txtScript10.Name = "txtScript10";
            this.txtScript10.Size = new System.Drawing.Size(276, 116);
            this.txtScript10.TabIndex = 11;
            this.txtScript10.Text = resources.GetString("txtScript10.Text");
            this.txtScript10.Visible = false;
            // 
            // txtScript9
            // 
            this.txtScript9.Location = new System.Drawing.Point(294, 502);
            this.txtScript9.Name = "txtScript9";
            this.txtScript9.Size = new System.Drawing.Size(276, 116);
            this.txtScript9.TabIndex = 10;
            this.txtScript9.Text = resources.GetString("txtScript9.Text");
            this.txtScript9.Visible = false;
            // 
            // txtScript8
            // 
            this.txtScript8.Location = new System.Drawing.Point(294, 380);
            this.txtScript8.Name = "txtScript8";
            this.txtScript8.Size = new System.Drawing.Size(276, 116);
            this.txtScript8.TabIndex = 9;
            this.txtScript8.Text = resources.GetString("txtScript8.Text");
            this.txtScript8.Visible = false;
            // 
            // txtScript7
            // 
            this.txtScript7.Location = new System.Drawing.Point(294, 257);
            this.txtScript7.Name = "txtScript7";
            this.txtScript7.Size = new System.Drawing.Size(276, 116);
            this.txtScript7.TabIndex = 8;
            this.txtScript7.Text = resources.GetString("txtScript7.Text");
            this.txtScript7.Visible = false;
            // 
            // txtScript6
            // 
            this.txtScript6.Location = new System.Drawing.Point(294, 135);
            this.txtScript6.Name = "txtScript6";
            this.txtScript6.Size = new System.Drawing.Size(276, 116);
            this.txtScript6.TabIndex = 7;
            this.txtScript6.Text = resources.GetString("txtScript6.Text");
            this.txtScript6.Visible = false;
            this.txtScript6.WordWrap = false;
            // 
            // pgbInstall
            // 
            this.pgbInstall.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pgbInstall.Location = new System.Drawing.Point(0, 37);
            this.pgbInstall.MarqueeAnimationSpeed = 50;
            this.pgbInstall.Name = "pgbInstall";
            this.pgbInstall.Size = new System.Drawing.Size(329, 16);
            this.pgbInstall.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pgbInstall.TabIndex = 12;
            // 
            // lblTest
            // 
            this.lblTest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTest.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTest.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblTest.Location = new System.Drawing.Point(0, 0);
            this.lblTest.Name = "lblTest";
            this.lblTest.Size = new System.Drawing.Size(329, 43);
            this.lblTest.TabIndex = 13;
            this.lblTest.Text = "Please wait, Database server installation in progress...";
            this.lblTest.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrtimer1
            // 
            this.tmrtimer1.Enabled = true;
            this.tmrtimer1.Tick += new System.EventHandler(this.tmrtimer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 53);
            this.Controls.Add(this.pgbInstall);
            this.Controls.Add(this.txtScript10);
            this.Controls.Add(this.txtScript9);
            this.Controls.Add(this.txtScript8);
            this.Controls.Add(this.txtScript7);
            this.Controls.Add(this.txtScript6);
            this.Controls.Add(this.txtScript5);
            this.Controls.Add(this.txtScript4);
            this.Controls.Add(this.txtScript3);
            this.Controls.Add(this.txtScript2);
            this.Controls.Add(this.txtScript1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblTest);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SQL Express Setup";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox txtScript1;
        private System.Windows.Forms.RichTextBox txtScript2;
        private System.Windows.Forms.RichTextBox txtScript3;
        private System.Windows.Forms.RichTextBox txtScript4;
        private System.Windows.Forms.RichTextBox txtScript5;
        private System.Windows.Forms.RichTextBox txtScript10;
        private System.Windows.Forms.RichTextBox txtScript9;
        private System.Windows.Forms.RichTextBox txtScript8;
        private System.Windows.Forms.RichTextBox txtScript7;
        private System.Windows.Forms.RichTextBox txtScript6;
        private System.Windows.Forms.ProgressBar pgbInstall;
        private System.Windows.Forms.Label lblTest;
        private System.Windows.Forms.Timer tmrtimer1;
    }
}

