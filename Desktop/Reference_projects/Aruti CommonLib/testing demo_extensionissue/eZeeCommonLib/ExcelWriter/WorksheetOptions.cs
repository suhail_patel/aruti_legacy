﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetOptions : IWriter, IReader, ICodeWriter
    {
        private int _activePane = -2147483648;
        private bool _fitToPage;
        private bool _freezePanes;
        private string _gridLineColor;
        private int _leftColumnRightPane = -2147483648;
        private WorksheetPageSetup _pageSetup;
        private WorksheetPrintOptions _print;
        private bool _protectObjects;
        private bool _protectScenarios;
        private bool _selected;
        private int _splitHorizontal = -2147483648;
        private int _splitVertical = -2147483648;
        private int _topRowBottomPane = -2147483648;
        private int _topRowVisible = -2147483648;
        private string _viewableRange;

        internal WorksheetOptions()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            Util.AddComment(method, "Options");
            if (this._selected)
            {
                Util.AddAssignment(method, targetObject, "Selected", this._selected);
            }
            if (this._topRowVisible != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "TopRowVisible", this._topRowVisible);
            }
            if (this._freezePanes)
            {
                Util.AddAssignment(method, targetObject, "FreezePanes", this._freezePanes);
            }
            if (this._fitToPage)
            {
                Util.AddAssignment(method, targetObject, "FitToPage", this._fitToPage);
            }
            if (this._splitHorizontal != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "SplitHorizontal", this._splitHorizontal);
            }
            if (this._topRowBottomPane != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "TopRowBottomPane", this._topRowBottomPane);
            }
            if (this._splitVertical != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "SplitVertical", this._splitVertical);
            }
            if (this._leftColumnRightPane != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "LeftColumnRightPane", this._leftColumnRightPane);
            }
            if (this._activePane != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "ActivePane", this._activePane);
            }
            if (this._viewableRange != null)
            {
                Util.AddAssignment(method, targetObject, "ViewableRange", this._viewableRange);
            }
            if (this._gridLineColor != null)
            {
                Util.AddAssignment(method, targetObject, "GridlineColor", this._gridLineColor);
            }
            Util.AddAssignment(method, targetObject, "ProtectObjects", this._protectObjects);
            Util.AddAssignment(method, targetObject, "ProtectScenarios", this._protectScenarios);
            if (this._pageSetup != null)
            {
                Util.Traverse(type, this._pageSetup, method, targetObject, "PageSetup");
            }
            if (this._print != null)
            {
                Util.Traverse(type, this._print, method, targetObject, "Print");
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (Util.IsElement(element2, "Selected", "urn:schemas-microsoft-com:office:excel"))
                    {
                        this._selected = true;
                    }
                    else
                    {
                        if (Util.IsElement(element2, "TopRowVisible", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._topRowVisible = Util.GetAttribute(element2, "TopRowVisible", "urn:schemas-microsoft-com:office:excel", -2147483648);
                            continue;
                        }
                        if (Util.IsElement(element2, "FreezePanes", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._freezePanes = true;
                            continue;
                        }
                        if (Util.IsElement(element2, "SplitHorizontal", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._splitHorizontal = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "TopRowBottomPane", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._topRowBottomPane = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "SplitVertical", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._splitVertical = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "LeftColumnRightPane", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._leftColumnRightPane = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "ActivePane", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._activePane = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "ViewableRange", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._viewableRange = element2.InnerText;
                            continue;
                        }
                        if (Util.IsElement(element2, "GridlineColor", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._gridLineColor = element2.InnerText;
                            continue;
                        }
                        if (Util.IsElement(element2, "ProtectObjects", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._protectObjects = bool.Parse(element2.InnerText);
                            continue;
                        }
                        if (Util.IsElement(element2, "ProtectScenarios", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._protectScenarios = bool.Parse(element2.InnerText);
                            continue;
                        }
                        if (Util.IsElement(element2, "FitToPage", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._fitToPage = true;
                            continue;
                        }
                        if (WorksheetPageSetup.IsElement(element2))
                        {
                            ((IReader) this.PageSetup).ReadXml(element2);
                            continue;
                        }
                        if (WorksheetPrintOptions.IsElement(element2))
                        {
                            ((IReader) this.Print).ReadXml(element2);
                        }
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "WorksheetOptions", "urn:schemas-microsoft-com:office:excel");
            if (this._selected)
            {
                writer.WriteElementString("Selected", "urn:schemas-microsoft-com:office:excel", "");
            }
            if (this._fitToPage)
            {
                writer.WriteElementString("FitToPage", "urn:schemas-microsoft-com:office:excel", "");
            }
            if (this._topRowVisible != -2147483648)
            {
                writer.WriteElementString("TopRowVisible", "urn:schemas-microsoft-com:office:excel", this._topRowVisible.ToString());
            }
            if (this._freezePanes)
            {
                writer.WriteElementString("FreezePanes", "urn:schemas-microsoft-com:office:excel", "");
            }
            if (this._splitHorizontal != -2147483648)
            {
                writer.WriteElementString("SplitHorizontal", "urn:schemas-microsoft-com:office:excel", this._splitHorizontal.ToString());
            }
            if (this._topRowBottomPane != -2147483648)
            {
                writer.WriteElementString("TopRowBottomPane", "urn:schemas-microsoft-com:office:excel", this._topRowBottomPane.ToString());
            }
            if (this._splitVertical != -2147483648)
            {
                writer.WriteElementString("SplitVertical", "urn:schemas-microsoft-com:office:excel", this._splitVertical.ToString());
            }
            if (this._leftColumnRightPane != -2147483648)
            {
                writer.WriteElementString("LeftColumnRightPane", "urn:schemas-microsoft-com:office:excel", this._leftColumnRightPane.ToString());
            }
            if (this._activePane != -2147483648)
            {
                writer.WriteElementString("ActivePane", "urn:schemas-microsoft-com:office:excel", this._activePane.ToString());
            }
            if (this._viewableRange != null)
            {
                writer.WriteElementString("ViewableRange", "urn:schemas-microsoft-com:office:excel", this._viewableRange);
            }
            if (this._gridLineColor != null)
            {
                writer.WriteElementString("GridlineColor", "urn:schemas-microsoft-com:office:excel", this._gridLineColor);
            }
            writer.WriteElementString("ProtectObjects", "urn:schemas-microsoft-com:office:excel", this._protectObjects.ToString());
            writer.WriteElementString("ProtectScenarios", "urn:schemas-microsoft-com:office:excel", this._protectScenarios.ToString());
            if (this._pageSetup != null)
            {
                ((IWriter) this._pageSetup).WriteXml(writer);
            }
            if (this._print != null)
            {
                ((IWriter) this._print).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "WorksheetOptions", "urn:schemas-microsoft-com:office:excel");
        }

        public int ActivePane
        {
            get
            {
                return this._activePane;
            }
            set
            {
                this._activePane = value;
            }
        }

        public bool FitToPage
        {
            get
            {
                return this._fitToPage;
            }
            set
            {
                this._fitToPage = value;
            }
        }

        public bool FreezePanes
        {
            get
            {
                return this._freezePanes;
            }
            set
            {
                this._freezePanes = value;
            }
        }

        public string GridLineColor
        {
            get
            {
                return this._gridLineColor;
            }
            set
            {
                this._gridLineColor = value;
            }
        }

        public int LeftColumnRightPane
        {
            get
            {
                return this._leftColumnRightPane;
            }
            set
            {
                this._leftColumnRightPane = value;
            }
        }

        public WorksheetPageSetup PageSetup
        {
            get
            {
                if (this._pageSetup == null)
                {
                    this._pageSetup = new WorksheetPageSetup();
                }
                return this._pageSetup;
            }
        }

        public WorksheetPrintOptions Print
        {
            get
            {
                if (this._print == null)
                {
                    this._print = new WorksheetPrintOptions();
                }
                return this._print;
            }
        }

        public bool ProtectObjects
        {
            get
            {
                return this._protectObjects;
            }
            set
            {
                this._protectObjects = value;
            }
        }

        public bool ProtectScenarios
        {
            get
            {
                return this._protectScenarios;
            }
            set
            {
                this._protectScenarios = value;
            }
        }

        public bool Selected
        {
            get
            {
                return this._selected;
            }
            set
            {
                this._selected = value;
            }
        }

        public int SplitHorizontal
        {
            get
            {
                return this._splitHorizontal;
            }
            set
            {
                this._splitHorizontal = value;
            }
        }

        public int SplitVertical
        {
            get
            {
                return this._splitVertical;
            }
            set
            {
                this._splitVertical = value;
            }
        }

        public int TopRowBottomPane
        {
            get
            {
                return this._topRowBottomPane;
            }
            set
            {
                this._topRowBottomPane = value;
            }
        }

        public int TopRowVisible
        {
            get
            {
                return this._topRowVisible;
            }
            set
            {
                this._topRowVisible = value;
            }
        }

        public string ViewableRange
        {
            get
            {
                return this._viewableRange;
            }
            set
            {
                this._viewableRange = value;
            }
        }
    }
}

