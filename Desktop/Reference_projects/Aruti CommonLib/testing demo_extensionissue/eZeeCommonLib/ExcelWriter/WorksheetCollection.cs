﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections;
    using System.Globalization;
    using System.Reflection;
    using System.Xml;

    public sealed class WorksheetCollection : CollectionBase, IWriter, ICodeWriter
    {
        internal WorksheetCollection()
        {
        }

        public int Add(Worksheet sheet)
        {
            return base.InnerList.Add(sheet);
        }

        public Worksheet Add(string name)
        {
            Worksheet sheet = new Worksheet(name);
            this.Add(sheet);
            return sheet;
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                Worksheet worksheet = this[i];
                string str = Util.CreateSafeName(worksheet.Name, "Sheet");
                CodeMemberMethod method2 = new CodeMemberMethod();
                method2.Name = "GenerateWorksheet" + str;
                method2.Parameters.Add(new CodeParameterDeclarationExpression(typeof(WorksheetCollection), "sheets"));
                Worksheet.cellDeclaration = null;
                Util.AddComment(method, "Generate " + worksheet.Name + " Worksheet");
                method.Statements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), method2.Name, new CodeExpression[] { targetObject }));
                type.Members.Add(method2);
                CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(Worksheet), "sheet", new CodeMethodInvokeExpression(new CodeVariableReferenceExpression("sheets"), "Add", new CodeExpression[] { new CodePrimitiveExpression(str) }));
                method2.Statements.Add(statement);
                ((ICodeWriter) worksheet).WriteTo(type, method2, new CodeVariableReferenceExpression("sheet"));
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(Worksheet item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(Worksheet[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(Worksheet item)
        {
            return base.InnerList.IndexOf(item);
        }

        public int IndexOf(string name)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                if (string.Compare(((Worksheet) base.InnerList[i]).Name, name, true, CultureInfo.InvariantCulture) == 0)
                {
                    return i;
                }
            }
            return -1;
        }

        public void Insert(int index, Worksheet sheet)
        {
            base.InnerList.Insert(index, sheet);
        }

        public void Remove(Worksheet item)
        {
            base.InnerList.Remove(item);
        }

        public object[] ToArray()
        {
            return base.InnerList.ToArray();
        }

        public Worksheet this[string name]
        {
            get
            {
                int index = this.IndexOf(name);
                if (index == -1)
                {
                    throw new ArgumentException("The specified worksheet " + name + " does not exists in the collection");
                }
                return (Worksheet) base.InnerList[index];
            }
            set
            {
                int index = this.IndexOf(name);
                if (index == -1)
                {
                    throw new ArgumentException("The specified worksheet " + name + " does not exists in the collection");
                }
                base.InnerList[index] = value;
            }
        }

        public Worksheet this[int index]
        {
            get
            {
                return (Worksheet) base.InnerList[index];
            }
            set
            {
                base.InnerList[index] = value;
            }
        }
    }
}

