﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections;
    using System.Globalization;
    using System.Reflection;
    using System.Xml;

    public sealed class ExcelLinksCollection : CollectionBase, IWriter, ICodeWriter
    {
        internal ExcelLinksCollection()
        {
        }

        public SupBook Add()
        {
            SupBook link = new SupBook();
            this.Add(link);
            return link;
        }

        public int Add(SupBook link)
        {
            return base.InnerList.Add(link);
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                SupBook book = this[i];
                string name = "SupBook" + i.ToString();
                CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(SupBook), name, new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "Links"), "Add", new CodeExpression[0]));
                method.Statements.Add(statement);
                ((ICodeWriter) book).WriteTo(type, method, new CodeVariableReferenceExpression(name));
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(SupBook link)
        {
            return base.InnerList.Contains(link);
        }

        public void CopyTo(SupBook[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(SupBook item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, SupBook item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(string item)
        {
            base.InnerList.Remove(item);
        }

        public SupBook this[int index]
        {
            get
            {
                return (SupBook) base.InnerList[index];
            }
        }
    }
}

