﻿namespace ExcelWriter
{
    using System;

    public enum StyleVerticalAlignment
    {
        Automatic,
        Top,
        Bottom,
        Center,
        Justify,
        Distributed,
        JustifyDistributed
    }
}

