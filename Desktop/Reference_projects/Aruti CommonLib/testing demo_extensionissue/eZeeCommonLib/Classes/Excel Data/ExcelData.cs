using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Collections;

public class ExcelData
{
    private OleDbConnection ExcelConnection;
    private Dictionary<string, string> DataTypes;

    public ExcelData()
    {
        DataTypes = new Dictionary<string, string>();
        DataTypes.Add("String", "text");
        DataTypes.Add("Int32", "int");
        DataTypes.Add("Int16", "int");
        DataTypes.Add("Int64", "int");
        DataTypes.Add("Boolean", "bit");
        DataTypes.Add("DateTime", "datetime");
        DataTypes.Add("Decimal", "decimal");
        DataTypes.Add("Float", "float");
        DataTypes.Add("Double", "float");
        DataTypes.Add("Byte[]", "image");
    }

     ~ExcelData()
    {
	    ExcelConnection = null;
    }

    public DataSet Import(string strFilePath, string strSheetName)
    {
        DataSet ds = new DataSet();
        try
        {

            //ExcelConnection = new OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0; " + "data source='" + strFilePath + " '; " + "Extended Properties=Excel 8.0;");
            ExcelConnection = new OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0; " + "data source='" + strFilePath + " '; " + "Extended Properties='Excel 8.0;HDR=Yes;IMEX=1';");

            ExcelConnection.Open();
            OleDbDataAdapter da = new OleDbDataAdapter("SELECT * FROM [" + strSheetName + "$]", ExcelConnection);

            da.Fill(ds, strSheetName);

            ExcelConnection.Close();
        }
        catch (Exception ex)
        {
            Logger(ex.Message);
            throw ex;
        }
        return ds;
    }

    public DataSet Import(string strFilePath)
    {
        DataSet ds = new DataSet();
        ArrayList strTable = new ArrayList();

        try
        {
            //ExcelConnection = new OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0; " + "data source='" + strFilePath + " '; " + "Extended Properties=Excel 8.0;");
            ExcelConnection = new OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0; " + "data source='" + strFilePath + " '; " + "Extended Properties='Excel 8.0;HDR=Yes;IMEX=1';");
            ExcelConnection.Open();

            DataTable table = ExcelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, null });
            foreach (DataRow dtRow in table.Rows)
            {
                if (dtRow["TABLE_TYPE"].ToString() == "TABLE" & dtRow["TABLE_NAME"].ToString().EndsWith("$"))
                {
                    OleDbDataAdapter da = new OleDbDataAdapter("SELECT * FROM [" + dtRow["TABLE_NAME"] + "]", ExcelConnection);
                    da.Fill( ds, dtRow["TABLE_NAME"].ToString());
                }
            }

            ExcelConnection.Close();

            return ds;

        }
        catch (Exception ex)
        {
            Logger(ex.Message);
            throw ex;
        }
    }
    
    public int Export(string strFilePath, DataSet ds)
    {
        int intReturn = 0;
        try
        {
            //Dim wr As New System.IO.BinaryWriter(New System.IO.FileStream(strFilePath & "\" & ds.DataSetName & ".xls", IO.FileMode.Append))
            //Dim br() As Byte = My.Resources.Data
            //wr.Write(br)
            //wr.Close()

            ExcelConnection = new OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0; " + "data source='" + strFilePath + "'; " + "Extended Properties=Excel 8.0;");
            ExcelConnection.Open();

            string tableName = null;
            string strQry = null;
            OleDbCommand cmd = null;

            foreach (DataTable dt in ds.Tables)
            {
                //tableName = ((dt.TableName.Equals("")) ? "Table" : dt.TableName);
                if (dt.TableName.Equals("")==true)
                    tableName = "Table" ;
                else
                    tableName = dt.TableName;
                
                strQry = "CREATE TABLE [" + tableName + "] (";
               
                //For i As Integer = 0 To dt.Columns.Count - 1
                //    If i = dt.Columns.Count - 1 Then
                //        strQry &= dt.Columns(i).ColumnName & " Text)"
                //    Else
                //        strQry &= dt.Columns(i).ColumnName & " Text,"
                //    End If
                //Next


                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (i == dt.Columns.Count - 1)
                    {
                        strQry += "[" + dt.Columns[i].ColumnName + "] " + DataTypes[dt.Columns[i].DataType.Name] + ")";
                    }
                    else
                    {
                        strQry += "[" + dt.Columns[i].ColumnName + "] " + DataTypes[dt.Columns[i].DataType.Name] + ",";
                    }
                }


                cmd = new OleDbCommand(strQry, ExcelConnection);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Logger(ex.Message + " Query: " + strQry);
                }

                if (dt.Rows.Count > 0)
                {

                    strQry = "INSERT INTO [" + tableName + "$] VALUES(";
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (i == dt.Columns.Count - 1)
                        {
                            strQry += "@p" + i + ")";
                        }
                        else
                        {
                            strQry += "@p" + i + ",";
                        }
                    }

                    cmd = new OleDbCommand(strQry, ExcelConnection);
                    foreach (DataRow dRow in dt.Rows)
                    {
                        cmd.Parameters.Clear();
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            cmd.Parameters.Add(new OleDbParameter("@p" + i, dRow[i]));
                        }
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            Logger(ex.Message + " Query: " + strQry);
                        }
                    }
                }
            }
            ExcelConnection.Close();
        }
        catch (Exception ex)
        {
            Logger(ex.Message);
            throw ex;
        }
        return intReturn;
    }

    private void Logger(string log)
    {
        try
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter("Ex.txt",true );
            file.WriteLine(log);
            file.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
