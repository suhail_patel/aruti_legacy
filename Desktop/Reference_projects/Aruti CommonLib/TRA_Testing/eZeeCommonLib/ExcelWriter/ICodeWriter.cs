﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;

    public interface ICodeWriter
    {
        void WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject);
    }
}

