﻿namespace ExcelWriter.Schemas
{
    using ExcelWriter;
    using System;
    using System.Xml;

    public sealed class Attribute : IWriter
    {
        private string _type;

        public Attribute()
        {
        }

        public Attribute(string type)
        {
            this._type = type;
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "attribute", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
            if (this._type != null)
            {
                writer.WriteAttributeString("s", "type", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882", this._type);
            }
            writer.WriteEndElement();
        }

        public string Type
        {
            get
            {
                return this._type;
            }
            set
            {
                this._type = value;
            }
        }
    }
}

