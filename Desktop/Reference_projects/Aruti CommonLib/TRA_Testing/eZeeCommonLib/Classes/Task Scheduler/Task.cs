using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using System.Runtime.InteropServices;
using TaskScheduler.Definition;

namespace TaskScheduler
{
	public sealed class Task : System.MarshalByRefObject, IDisposable
	{

#region  Fields 
		private ITask m_ITaskObj;
		private string m_Name;
#endregion

#region  Enums 

		public enum Status: int
		{
			Ready = 0X41300,
			Running = 0X41301,
			Disabled = 0X41302,
			HasNotRun = 0X41303,
			NoMoreRuns = 0X41304,
			NotScheduled = 0X41305,
			Terminated = 0X41306,
			NoValidTriggers = 0X41307,
			EventTrigger = 0X41308
		}

		[Flags()]
		public enum TaskFlags: int
		{
			Interactive = 0X1,
			DeleteWhenDone = 0X2,
			Disabled = 0X4,
			StartOnlyIfIdle = 0X10,
			KillOnIdleEnd = 0X20,
			DontStartIfOnBatteries = 0X40,
			KillIfGoingOnBatteries = 0X80,
			RunOnlyIfDocked = 0X100,
			Hidden = 0X200,
			RunIfConnectedToInternet = 0X400,
			RestartOnIdleResume = 0X800
		}

#endregion

#region  Events 

		internal delegate void SaveChangesEventHandler();
		internal event SaveChangesEventHandler SaveChanges;

#endregion

#region  Constructors 

		internal Task(string name, ITask task)
		{
			m_Name = name;
			m_ITaskObj = task;
		}

#endregion

#region IDisposable Members
        ~Task()
        {
            this.Dispose();
        }
        public void Dispose()
        {
            if (m_ITaskObj != null)
            {

                // Release the COM object
                Marshal.ReleaseComObject(m_ITaskObj);
                m_ITaskObj = null;

                // Supress Finalize call
                GC.SuppressFinalize(this);
            }
        }

        #endregion

#region  Public Properties 

		public string ApplicationName
		{
			get
			{
				return m_ITaskObj.GetApplicationName();
			}
			set
			{
				m_ITaskObj.SetApplicationName(value);
			}
		}

		public string Parameters
		{
			get
			{
				return m_ITaskObj.GetParameters();
			}
			set
			{
				m_ITaskObj.SetParameters(value);
			}
		}

		public string CommandLine
		{
			get
			{
				return m_ITaskObj.GetParameters();
			}
			set
			{
				m_ITaskObj.SetParameters(value);
			}
		}

		public string Comment
		{
			get
			{
				return m_ITaskObj.GetComment();
			}
			set
			{
				m_ITaskObj.SetComment(value);
			}
		}

		public string Creator
		{
			get
			{
				return m_ITaskObj.GetCreator();
			}
			set
			{
				m_ITaskObj.SetCreator(value);
			}
		}

		public short ErrorRetryCount
		{
			get
			{
				return m_ITaskObj.GetErrorRetryCount();
			}
			set
			{
				m_ITaskObj.SetErrorRetryCount(value);
			}
		}

		public short ErrorRetryInterval
		{
			get
			{
				return m_ITaskObj.GetErrorRetryInterval();
			}
			set
			{
				m_ITaskObj.SetErrorRetryInterval(value);
			}
		}

		public int ExitCode
		{
			get
			{
				return m_ITaskObj.GetExitCode();
			}
		}

		public Task.TaskFlags Flags
		{
			get
			{
				return m_ITaskObj.GetFlags();
			}
			set
			{
				m_ITaskObj.SetFlags((int)value);
			}
		}

		public short IdleWait
		{
			get
			{
				return m_ITaskObj.GetIdleWait();
			}
			set
			{
				m_ITaskObj.SetIdleWait(value);
			}
		}

		public int MaxRunTime
		{
			get
			{
				return m_ITaskObj.GetMaxRunTime();
			}
			set
			{
				m_ITaskObj.SetMaxRunTime(value);
			}
		}

		public int Priority
		{
			get
			{
				return m_ITaskObj.GetPriority();
			}
			set
			{
				m_ITaskObj.SetPriority(value);
			}
		}

		public Task.Status State
		{
			get
			{
				return m_ITaskObj.GetStatus();
			}
		}

		public string WorkingDirectory
		{
			get
			{
				return m_ITaskObj.GetWorkingDirectory();
			}
			set
			{
				m_ITaskObj.SetWorkingDirectory(value);
			}
		}

		public TriggerCollection Triggers
		{
			get
			{
				return new TriggerCollection(this);
			}
		}

		public System.DateTime LastRunTime
		{
			get
			{
				SYSTEMTIME ST = new SYSTEMTIME();
					double S = 0;

				ST = m_ITaskObj.GetMostRecentRunTime();
				DateConvert.SystemTimeToVariantTime(ref ST, ref S);

				return System.DateTime.FromOADate(S);
			}
		}

		public System.DateTime NextRunTime
		{
			get
			{
				SYSTEMTIME ST = new SYSTEMTIME();
					double S = 0;

                ST = m_ITaskObj.GetNextRunTime();
                DateConvert.SystemTimeToVariantTime(ref ST, ref S);
                return System.DateTime.FromOADate(S);
			}
		}

		public string FileName
		{
			get
			{
				System.Runtime.InteropServices.ComTypes.IPersistFile PF = null;
				PF = (System.Runtime.InteropServices.ComTypes.IPersistFile)m_ITaskObj;
				string strReturn = string.Empty;
				PF.GetCurFile(out strReturn);
				return strReturn;
			}
		}

		public string Name
		{
			get
			{
				return m_Name;
			}
		}

#endregion

#region  Public Methods 

		public void Save()
		{
			System.Runtime.InteropServices.ComTypes.IPersistFile PF = null;
			PF = (System.Runtime.InteropServices.ComTypes.IPersistFile)m_ITaskObj;
			if (SaveChanges != null)
				SaveChanges();
			PF.Save(null, false);
		}

		public void Save(string filename)
		{
			System.Runtime.InteropServices.ComTypes.IPersistFile PF = null;
			PF = (System.Runtime.InteropServices.ComTypes.IPersistFile)m_ITaskObj;
			if (SaveChanges != null)
				SaveChanges();
			PF.Save(filename, true);
		}

		public void SetAccountInfo(string accountName, string password)
		{
			m_ITaskObj.SetAccountInformation(accountName, password);
		}

		public string GetAccountInfo()
		{
			return m_ITaskObj.GetAccountInformation();
		}

		public void Run()
		{
			m_ITaskObj.Run();
		}

		public void Terminate()
		{
			m_ITaskObj.Terminate();
		}

		public void ShowProperties()
		{
			m_ITaskObj.EditWorkItem(new IntPtr(0),0);
		}

		public void ShowProperties(System.Windows.Forms.IWin32Window parentWindow)
		{
			m_ITaskObj.EditWorkItem(parentWindow.Handle,0);
		}

       
#endregion

#region  Friend Properties 

		internal ITask ITaskObj
		{
			get
			{
				return m_ITaskObj;
			}
		}

#endregion
    }
}
