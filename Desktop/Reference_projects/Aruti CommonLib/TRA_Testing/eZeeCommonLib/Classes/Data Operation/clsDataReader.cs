using System.Xml;
using System.IO;
using System.Configuration;
using System.Windows.Forms;
using System.Drawing;
using System.Data.SqlClient;
using System;
using System.Data;

namespace eZeeCommonLib
{
    public class clsDataReader : IDisposable
    {


        private SqlCommand oCmd;
        public string ErrorMessage = "";
        public string ErrorNumber = "0";

        public int mintFolioTranUnkid = 0;

        private bool disposedValue = false; // To detect redundant calls

        public clsDataReader()
        {
            oCmd = new SqlCommand();
            try
            {
                oCmd.Connection = modGlobal.gConn;

                if (modGlobal.gblnBindTransaction == true)
                {
                    oCmd.Transaction = modGlobal.gsqlTransaction;
                }

            }
            catch (SqlException oSqlEx)
            {
                ErrorMessage = "SqlException:- " + oSqlEx.Message;
                ErrorNumber = oSqlEx.Number.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- " + ex.Message;
            }
        }


        #region  Parameters

        /// <summary>
        /// Removes all the System.Data.SqlClient.SqlParameter objects from the System.Data.SqlClient.SqlParameterCollection.
        /// </summary>
        public void ClearParameters()
        {
            try
            {
                oCmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Removes the specified System.Data.SqlClient.SqlParameter from the collection.
        /// </summary>
        /// <param name="parameterName">The name of the parameter to retrieve. </param>
        public void RemoveParameters(string parameterName)
        {
            try
            {
                oCmd.Parameters.Remove(oCmd.Parameters[parameterName]);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.String that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>

        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, string oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        //ORIGINAL LINE: Public Sub AddParameter(ByVal strName As String, ByVal oSqlDbType As SqlDbType, ByVal iSize As Integer, ByVal oValue As String, Optional ByVal oParamDirection As ParameterDirection = ParameterDirection.Input)
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, string oValue, ParameterDirection oParamDirection)
        {


            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, iSize);

            try
            {
                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

            }
            catch (SqlException oSqlEx)
            {
                ErrorMessage = "SqlException:- [" + strName + "]" + oSqlEx.Message;
                ErrorNumber = oSqlEx.Number.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- [" + strName + "]" + ex.Message;
            }
            finally
            {
                oSqlParameter = null;
            }
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An Array of System.Byte that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>

        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, byte[] oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        //ORIGINAL LINE: Public Sub AddParameter(ByVal strName As String, ByVal oSqlDbType As SqlDbType, ByVal iSize As Integer, ByVal oValue As Byte(), Optional ByVal oParamDirection As ParameterDirection = ParameterDirection.Input)
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, byte[] oValue, ParameterDirection oParamDirection)
        {

            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, iSize);
            try
            {

                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

            }
            catch (SqlException oSqlEx)
            {
                ErrorMessage = "SqlException:- [" + strName + "]" + oSqlEx.Message;
                ErrorNumber = oSqlEx.Number.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- [" + strName + "]" + ex.Message;
            }
            finally
            {
                oSqlParameter = null;
            }

        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Boolean that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>

        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, bool oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        //ORIGINAL LINE: Public Sub AddParameter(ByVal strName As String, ByVal oSqlDbType As SqlDbType, ByVal iSize As Integer, ByVal oValue As Boolean, Optional ByVal oParamDirection As ParameterDirection = ParameterDirection.Input)
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, bool oValue, ParameterDirection oParamDirection)
        {

            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, System.Convert.ToInt32(iSize));

            try
            {
                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

            }
            catch (SqlException oSqlEx)
            {
                ErrorMessage = "SqlException:- [" + strName + "]" + oSqlEx.Message;
                ErrorNumber = oSqlEx.Number.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- [" + strName + "]" + ex.Message;
            }
            finally
            {
                oSqlParameter = null;
            }
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.DBNull that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>

        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, DBNull oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        //ORIGINAL LINE: Public Sub AddParameter(ByVal strName As String, ByVal oSqlDbType As SqlDbType, ByVal iSize As Integer, ByVal oValue As DBNull, Optional ByVal oParamDirection As ParameterDirection = ParameterDirection.Input)
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, DBNull oValue, ParameterDirection oParamDirection)
        {

            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, iSize);

            try
            {
                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);
            }
            catch (SqlException oSqlEx)
            {
                ErrorMessage = "SqlException:- " + oSqlEx.Message;
                ErrorNumber = oSqlEx.Number.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- " + ex.Message;
            }
            finally
            {
                oSqlParameter = null;
            }
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Image that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>

        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, Image oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        //ORIGINAL LINE: Public Sub AddParameter(ByVal strName As String, ByVal oSqlDbType As SqlDbType, ByVal iSize As Integer, ByVal oValue As Image, Optional ByVal oParamDirection As ParameterDirection = ParameterDirection.Input)
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, Image oValue, ParameterDirection oParamDirection)
        {

            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, System.Convert.ToInt32(iSize));

            try
            {

                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

            }
            catch (SqlException oSqlEx)
            {
                ErrorMessage = "SqlException:- " + oSqlEx.Message;
                ErrorNumber = oSqlEx.Number.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- " + ex.Message;
            }
            finally
            {
                oSqlParameter = null;
            }
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Guid that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>

        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, Guid oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        //ORIGINAL LINE: Public Sub AddParameter(ByVal strName As String, ByVal oSqlDbType As SqlDbType, ByVal iSize As Integer, ByVal oValue As Guid, Optional ByVal oParamDirection As ParameterDirection = ParameterDirection.Input)
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, Guid oValue, ParameterDirection oParamDirection)
        {

            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, System.Convert.ToInt32(iSize));

            try
            {

                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

            }
            catch (SqlException oSqlEx)
            {
                ErrorMessage = "SqlException:- " + oSqlEx.Message;
                ErrorNumber = oSqlEx.Number.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- " + ex.Message;
            }
            finally
            {
                oSqlParameter = null;
            }
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Integer that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>

        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, int oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        //ORIGINAL LINE: Public Sub AddParameter(ByVal strName As String, ByVal oSqlDbType As SqlDbType, ByVal iSize As Integer, ByVal oValue As Integer, Optional ByVal oParamDirection As ParameterDirection = ParameterDirection.Input)
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, int oValue, ParameterDirection oParamDirection)
        {

            AddParameter(strName, oSqlDbType, iSize, oValue.ToString(), oParamDirection);

        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Double that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>

        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, double oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        //ORIGINAL LINE: Public Sub AddParameter(ByVal strName As String, ByVal oSqlDbType As SqlDbType, ByVal iSize As Integer, ByVal oValue As Double, Optional ByVal oParamDirection As ParameterDirection = ParameterDirection.Input)
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, double oValue, ParameterDirection oParamDirection)
        {

            AddParameter(strName, oSqlDbType, iSize, oValue.ToString(), oParamDirection);

        }
        #endregion

        public SqlDataReader ExecReader(string strQuery)
        {
            //SqlDataAdapter oDa = null;
            SqlDataReader oReader = null;
            try
            {
                if (!(this.ErrorMessage.Equals("")))
                {
                    throw new Exception(this.ErrorNumber + " " + this.ErrorMessage);
                }

                oCmd = new SqlCommand();

                oCmd.Connection = modGlobal.gConn;
                oCmd.CommandText = strQuery;
                oCmd.CommandType = CommandType.Text;

                oReader = oCmd.ExecuteReader();

                return oReader;

            }
            catch (SqlException exSql)
            {
                this.ErrorMessage = "SqlException: " + exSql.Message + " // SqlErrorNumber:- " + exSql.Number.ToString();
                this.ErrorNumber = exSql.Number.ToString();

                return null;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = "Not SQLlException: " + ex.Message;

                return null;
            }
        }

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    // TODO: free unmanaged resources when explicitly called
                }

                // TODO: free shared unmanaged resources
            }
            this.disposedValue = true;
        }

        #region  IDisposable Support
        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}