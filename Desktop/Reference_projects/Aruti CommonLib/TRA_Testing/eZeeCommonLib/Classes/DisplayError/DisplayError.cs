
namespace eZeeCommonLib
{
    public class DisplayError
    {

        /// <summary>
        /// For display error message in proper format.
        /// </summary>
        public static void Show(string errNum, string errMessage, string strProcName, string strModuleName)
        {
            //MessageBox.Show(errNum & " : " & errMessage & vbCrLf & "Proc Name : " & strProcName & " - " & strModuleName, "", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
            eZeeMsgBox.Show(errNum + " : " + errMessage + System.Environment.NewLine + "Proc Name : " + strProcName + " - " + strModuleName, enMsgBoxStyle.Critical);
            //throw new System.Exception(errNum + " : " + errMessage + System.Environment.NewLine + "Proc Name : " + strProcName + " - " + strModuleName);

            //System.IO.StreamWriter oWrite = null;

            //oWrite = System.IO.File.AppendText(System.IO.Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory) + "/ErrorLog.txt");
            //oWrite.WriteLine(System.DateTime.Now.ToString() + " -- " + errNum + " : " + errMessage + System.Environment.NewLine + "Proc Name : " + strProcName + " - " + strModuleName);
            //oWrite.Flush();
            //oWrite.Close();

        }
    }
}