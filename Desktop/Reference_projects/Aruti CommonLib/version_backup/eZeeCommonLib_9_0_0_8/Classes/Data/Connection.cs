using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Xml;
using System.IO;
using System.Data.Odbc;
using System.Configuration;

namespace eZee.Data.Manager
{
	/// <summary>
	/// Summary description for Cnn.
	/// </summary>
	public class Connection
	{
        private string ODBCString;
        private string OLEDBString;

             
        /// <summary>
        /// Parameterized constructer to  set the connection string for dbf database.
        /// It receive the application.startuppath.
        /// Which should be set while making the object of this class.
        /// </summary>
        /// <param name="mvarSysProjPath"></param>
        public void Dbf_Connection(string ApplicationPath)
		{
            this.ODBCString = "Provider=MSDASQL/SQLServer ODBC;Driver={Microsoft Visual FoxPro Driver};" + "SourceType=DBF;SourceDB=" + ApplicationPath + ";InternetTimeout=300000;Transact Updates=True";
            this.OLEDBString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + ApplicationPath + ";Extended Properties=dBASE IV;User ID=Admin;Password=;";
		}

        public void Excel_Connection(string ApplicationPath, string FileName)
		{
            this.ODBCString = "Driver={Microsoft Excel Driver (*.xls)};DriverId=790;Dbq='" + System.IO.Path.Combine(ApplicationPath, FileName) + "';DefaultDir='" + ApplicationPath + "';";
            this.OLEDBString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + System.IO.Path.Combine(ApplicationPath, FileName) + "';Extended Properties='Excel 8.0;HDR=Yes;IMEX=1';";
		}

        public void Access_Connection(string ApplicationPath, string FileName)
        {
            this.Access_Connection(ApplicationPath,FileName,"");
        }

        public void Access_Connection(string ApplicationPath, string FileName, string Password)
        {
            this.ODBCString = "Driver={Microsoft Access Driver (*.mdb)};Dbq=" + System.IO.Path.Combine(ApplicationPath, FileName) + ";Exclusive=1;Uid=admin;Pwd=" + Password + ";";
            this.OLEDBString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + System.IO.Path.Combine(ApplicationPath, FileName) + ";Jet OLEDB:Database Password=" + Password + ";";
        }

        /// <summary>
        /// It returns the dbf connecton string.
        /// </summary>
        /// <returns>It returns the dbf connecton string.</returns>
        public string Odbc_CnnString
        {
            get
            {
                return this.ODBCString;
            }
		}

        /// <summary>
        /// It returns the dbf connecton string.
        /// </summary>
        /// <returns>It returns the dbf connecton string.</returns>
        public string Oledb_CnnString 
        {
            get
            {
                return this.OLEDBString;
            }
        }
	}
}
