using System;
using System.Collections.Generic;
using System.Text;

public enum enMsgBoxStyle : int
{
    OkOnly = 0,
    OkCancel = 1,
    AbortRetryIgnore = 2,
    YesNoCancel = 4,
    YesNo = 8,
    Information = 16,
    Critical = 32,
    Question = 64,
    Exclamation = 128
}

class eZeeMessageBox : System.Windows.Forms.Form
{
 
    public eZeeMessageBox()
    {
        InitializeComponent();
    }


    internal System.Windows.Forms.PictureBox picImage;
    internal System.Windows.Forms.Label lblMessage;
    internal System.Windows.Forms.Button button1;
    internal System.Windows.Forms.Panel pnlMain;
    internal System.Windows.Forms.CheckBox chkCheckBox;
    internal System.Windows.Forms.Button button2;
    internal System.Windows.Forms.Panel pnlFooter;
    private System.Windows.Forms.Panel pnlSideIcon;
    internal System.Windows.Forms.Button button3;

    private void InitializeComponent()
    {
        this.picImage = new System.Windows.Forms.PictureBox();
        this.lblMessage = new System.Windows.Forms.Label();
        this.button1 = new System.Windows.Forms.Button();
        this.pnlMain = new System.Windows.Forms.Panel();
        this.chkCheckBox = new System.Windows.Forms.CheckBox();
        this.button2 = new System.Windows.Forms.Button();
        this.pnlFooter = new System.Windows.Forms.Panel();
        this.button3 = new System.Windows.Forms.Button();
        this.pnlSideIcon = new System.Windows.Forms.Panel();
        ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
        this.pnlMain.SuspendLayout();
        this.pnlFooter.SuspendLayout();
        this.pnlSideIcon.SuspendLayout();
        this.SuspendLayout();
        // 
        // picImage
        // 
        this.picImage.Image = global::eZeeCommonLib.Properties.Resources.critical;
        this.picImage.Location = new System.Drawing.Point(8, 8);
        this.picImage.Name = "picImage";
        this.picImage.Size = new System.Drawing.Size(48, 48);
        this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
        this.picImage.TabIndex = 4;
        this.picImage.TabStop = false;
        // 
        // lblMessage
        // 
        this.lblMessage.AutoSize = true;
        this.lblMessage.Location = new System.Drawing.Point(8, 12);
        this.lblMessage.MaximumSize = new System.Drawing.Size(600, 550);
        this.lblMessage.Name = "lblMessage";
        this.lblMessage.Size = new System.Drawing.Size(49, 13);
        this.lblMessage.TabIndex = 0;
        this.lblMessage.Text = "Message";
        // 
        // button1
        // 
        this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        this.button1.Location = new System.Drawing.Point(200, 11);
        this.button1.Name = "button1";
        this.button1.Size = new System.Drawing.Size(88, 29);
        this.button1.TabIndex = 2;
        this.button1.UseVisualStyleBackColor = true;
        // 
        // pnlMain
        // 
        this.pnlMain.AutoSize = true;
        this.pnlMain.Controls.Add(this.lblMessage);
        this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
        this.pnlMain.Location = new System.Drawing.Point(66, 0);
        this.pnlMain.Name = "pnlMain";
        this.pnlMain.Padding = new System.Windows.Forms.Padding(6);
        this.pnlMain.Size = new System.Drawing.Size(234, 69);
        this.pnlMain.TabIndex = 0;
        // 
        // chkCheckBox
        // 
        this.chkCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
        this.chkCheckBox.AutoSize = true;
        this.chkCheckBox.Location = new System.Drawing.Point(12, 17);
        this.chkCheckBox.Name = "chkCheckBox";
        this.chkCheckBox.Size = new System.Drawing.Size(110, 17);
        this.chkCheckBox.TabIndex = 3;
        this.chkCheckBox.Text = "Don\'t Show Again";
        this.chkCheckBox.UseVisualStyleBackColor = true;
        this.chkCheckBox.Visible = false;
        // 
        // button2
        // 
        this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        this.button2.Location = new System.Drawing.Point(106, 11);
        this.button2.Name = "button2";
        this.button2.Size = new System.Drawing.Size(88, 29);
        this.button2.TabIndex = 1;
        this.button2.UseVisualStyleBackColor = true;
        // 
        // pnlFooter
        // 
        this.pnlFooter.BackColor = System.Drawing.Color.Gainsboro;
        this.pnlFooter.Controls.Add(this.button1);
        this.pnlFooter.Controls.Add(this.chkCheckBox);
        this.pnlFooter.Controls.Add(this.button2);
        this.pnlFooter.Controls.Add(this.button3);
        this.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
        this.pnlFooter.Location = new System.Drawing.Point(0, 69);
        this.pnlFooter.Margin = new System.Windows.Forms.Padding(0);
        this.pnlFooter.Name = "pnlFooter";
        this.pnlFooter.Size = new System.Drawing.Size(300, 50);
        this.pnlFooter.TabIndex = 1;
        // 
        // button3
        // 
        this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        this.button3.Location = new System.Drawing.Point(12, 11);
        this.button3.Name = "button3";
        this.button3.Size = new System.Drawing.Size(88, 29);
        this.button3.TabIndex = 0;
        this.button3.UseVisualStyleBackColor = true;
        // 
        // pnlSideIcon
        // 
        this.pnlSideIcon.Controls.Add(this.picImage);
        this.pnlSideIcon.Dock = System.Windows.Forms.DockStyle.Left;
        this.pnlSideIcon.Location = new System.Drawing.Point(0, 0);
        this.pnlSideIcon.Name = "pnlSideIcon";
        this.pnlSideIcon.Size = new System.Drawing.Size(66, 69);
        this.pnlSideIcon.TabIndex = 5;
        // 
        // eZeeMessageBox
        // 
        this.AutoSize = true;
        this.BackColor = System.Drawing.Color.White;
        this.ClientSize = new System.Drawing.Size(300, 119);
        this.ControlBox = false;
        this.Controls.Add(this.pnlMain);
        this.Controls.Add(this.pnlSideIcon);
        this.Controls.Add(this.pnlFooter);
        this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.KeyPreview = true;
        this.MaximizeBox = false;
        this.MaximumSize = new System.Drawing.Size(800, 600);
        this.MinimizeBox = false;
        this.Name = "eZeeMessageBox";
        this.ShowIcon = false;
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "Message Box";
        this.Load += new System.EventHandler(this.eZeeMessageBox_Load);
        this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.eZeeMessageBox_KeyUp);
        ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
        this.pnlMain.ResumeLayout(false);
        this.pnlMain.PerformLayout();
        this.pnlFooter.ResumeLayout(false);
        this.pnlFooter.PerformLayout();
        this.pnlSideIcon.ResumeLayout(false);
        this.ResumeLayout(false);
        this.PerformLayout();

    }
    
    private string m_strMessage = "";
    private enMsgBoxStyle m_enMsgBoxStyle = enMsgBoxStyle.OkOnly;
    private string m_strTitle = null;
    private bool m_blnShowCheckBox = false;
    private int m_intAutoclose = 0;

    //private System.Windows.Forms.DialogResult m_Result = 0;

    public bool Chekced
    {
        get
        {
            return chkCheckBox.Checked;
        }
    }


     public System.Windows.Forms.DialogResult Display(string Message,enMsgBoxStyle oenMsgBoxStyle, string Title, bool showCheckBox, int AutoCloseAfter)
    {

        m_strMessage = Message;
        m_enMsgBoxStyle = oenMsgBoxStyle;
        m_strTitle = Title;
        if (m_strTitle == "" || m_strTitle == null)
        {
            m_strTitle = eZeeMsgBox.MessageBoxButton.DefaultTitle;
        }
        this.Text = m_strTitle;

        m_blnShowCheckBox = showCheckBox;
        m_intAutoclose = AutoCloseAfter;

        int minWidth = 306;
        //this.Size = new System.Drawing.Size(302, 135);
        //#region Size
        //if (eZeeMsgBox.MessageBoxButton.Show_Big_Style == true)
        //{
        //    this.Size = new System.Drawing.Size(123, 173);
        //    pnlFooter.Size = new System.Drawing.Size(115, 50);

        //    button1.Size = new System.Drawing.Size(88, 30);
        //    button1.Location = new System.Drawing.Point(this.Size.Width - button1.Width - 16, 10);
        //    button2.Size = button1.Size;
        //    button2.Location = new System.Drawing.Point(button1.Location.X - button2.Width - 8, 10);
        //    button3.Size = button1.Size;
        //    button3.Location = new System.Drawing.Point(button2.Location.X - button3.Width - 8, 10);
        //    //picImage.Size = new System.Drawing.Size(64, 64);
        //    //pnlSideIcon.Size = new System.Drawing.Size(76, 76);

        //    chkCheckBox.Location = new System.Drawing.Point(8, 17);
        //}
        //else
        //{
        //    this.Size = new System.Drawing.Size(121, 131);
        //    pnlFooter.Size = new System.Drawing.Size(115, 40);
        //    button1.Size = new System.Drawing.Size(75, 23);
        //    button1.Location = new System.Drawing.Point(this.Size.Width - button1.Width - 16, 9);
        //    button2.Size = button1.Size;
        //    button2.Location = new System.Drawing.Point(button1.Location.X - button2.Width - 8, 9);
        //    button3.Size = button1.Size;
        //    button3.Location = new System.Drawing.Point(button2.Location.X - button3.Width - 8, 9);
        //    //picImage.Size = new System.Drawing.Size(32, 32);
        //    //pnlSideIcon.Size = new System.Drawing.Size(44, 44);

        //    chkCheckBox.Location = new System.Drawing.Point(8, 12);
        //}
        //#endregion

        #region  Image Setting
        bool blnImage = false;
        if (m_enMsgBoxStyle >= enMsgBoxStyle.Exclamation)
        {
            if (blnImage == false)
                //if (eZeeMsgBox.MessageBoxButton.Show_Big_Style == true)
                //    picImage.Image = eZeeCommonLib.Properties.Resources.exclamation;
                //else
                    picImage.Image = eZeeCommonLib.Properties.Resources.exclamation;

            blnImage = true;
            m_enMsgBoxStyle -= enMsgBoxStyle.Exclamation;
        }

        if (m_enMsgBoxStyle >= enMsgBoxStyle.Question)
        {
            if (blnImage == false)
                //if (eZeeMsgBox.MessageBoxButton.Show_Big_Style == true)
                //    picImage.Image = eZeeCommonLib.Properties.Resources.question;
                //else
                    picImage.Image = eZeeCommonLib.Properties.Resources.question;
            blnImage = true;
            m_enMsgBoxStyle -= enMsgBoxStyle.Question;
        }

        if (m_enMsgBoxStyle >= enMsgBoxStyle.Critical)
        {
            if (blnImage == false)
                //if (eZeeMsgBox.MessageBoxButton.Show_Big_Style == true)
                //    picImage.Image = eZeeCommonLib.Properties.Resources.critical;
                //else
                    picImage.Image = eZeeCommonLib.Properties.Resources.critical;

            if (blnImage == false)

                blnImage = true;
            m_enMsgBoxStyle -= enMsgBoxStyle.Critical;
        }

        if (m_enMsgBoxStyle >= enMsgBoxStyle.Information)
        {
            if (blnImage == false)
                //if (eZeeMsgBox.MessageBoxButton.Show_Big_Style == true)
                //    picImage.Image = eZeeCommonLib.Properties.Resources.information;
                //else
                    picImage.Image = eZeeCommonLib.Properties.Resources.information;

            blnImage = true;
            m_enMsgBoxStyle -= enMsgBoxStyle.Information;
        }

        if (blnImage == false)
        {
            picImage.Image = null;
            pnlSideIcon.Visible = false;
            pnlMain.Location = new System.Drawing.Point(50, 0);
        }
        #endregion

        #region Check box
        if (m_blnShowCheckBox == true)
        {
            chkCheckBox.Text = eZeeMsgBox.MessageBoxButton.Dont_Show_Again;
            chkCheckBox.Visible = m_blnShowCheckBox;

        }
        else
        {
            chkCheckBox.Text = "";
        }
        #endregion


        #region  Button Setting
        if (m_enMsgBoxStyle >= enMsgBoxStyle.YesNo)
        {
            button1.Visible = true;
            button1.Text = eZeeMsgBox.MessageBoxButton.No;
            button1.DialogResult = System.Windows.Forms.DialogResult.No;

            button2.Visible = true;
            button2.Text = eZeeMsgBox.MessageBoxButton.Yes;
            button2.DialogResult = System.Windows.Forms.DialogResult.Yes;
            button2.Focus();

            button3.Visible = false;

            this.AcceptButton = button2;
            this.CancelButton = button1;

            if (chkCheckBox.Width > 88)
                minWidth += (chkCheckBox.Width + 8 - 88);
        }
        else if (m_enMsgBoxStyle >= enMsgBoxStyle.YesNoCancel)
        {
            button1.Visible = true;
            button1.Text = eZeeMsgBox.MessageBoxButton.Cancel;
            button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            button2.Visible = true;
            button2.Text = eZeeMsgBox.MessageBoxButton.No;
            button2.DialogResult = System.Windows.Forms.DialogResult.No;

            button3.Visible = true;
            button3.Text = eZeeMsgBox.MessageBoxButton.Yes;
            button3.DialogResult = System.Windows.Forms.DialogResult.Yes;
            button3.Focus();

            this.AcceptButton = button3;
            this.CancelButton = button1;
            if (chkCheckBox.Width > 15)
                minWidth += (chkCheckBox.Width + 8 - 0);

        }
        else if (m_enMsgBoxStyle >= enMsgBoxStyle.AbortRetryIgnore)
        {
            button1.Visible = true;
            button1.Text = eZeeMsgBox.MessageBoxButton.Ignore;
            button1.DialogResult = System.Windows.Forms.DialogResult.Ignore;

            button2.Visible = true;
            button2.Text = eZeeMsgBox.MessageBoxButton.Retry;
            button2.DialogResult = System.Windows.Forms.DialogResult.Retry;
            button2.Focus();

            button3.Visible = true;
            button3.Text = eZeeMsgBox.MessageBoxButton.Abort;
            button3.DialogResult = System.Windows.Forms.DialogResult.Abort;

            this.AcceptButton = button3;
            this.CancelButton = button1;
            if (chkCheckBox.Width > 15)
                minWidth += (chkCheckBox.Width + 8 - 0);
        }
        else if (m_enMsgBoxStyle >= enMsgBoxStyle.OkCancel)
        {
            button1.Visible = true;
            button1.Text = eZeeMsgBox.MessageBoxButton.Cancel;
            button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            button2.Visible = true;
            button2.Text = eZeeMsgBox.MessageBoxButton.Ok;
            button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            button2.Focus();

            button3.Visible = false;

            this.AcceptButton = button2;
            this.CancelButton = button1;
            if (chkCheckBox.Width > 88)
                minWidth += (chkCheckBox.Width + 8 - 88);
        }
        else
        {
            button1.Visible = true;
            button1.Text = eZeeMsgBox.MessageBoxButton.Ok;
            button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            button1.Focus();

            button2.Visible = false;
            button3.Visible = false;

            this.AcceptButton = button1;
            this.CancelButton = button1;
            if (chkCheckBox.Width > 182)
                minWidth += (chkCheckBox.Width + 8 - 182);
        }
        #endregion

        //lblMessage.Font = eZeeMsgBox.MessageBoxButton.Message_Font;
        lblMessage.Text = m_strMessage;

        if (this.Width < minWidth)
        {
            this.Width = minWidth;
        }

        return this.ShowDialog();

    }

    private void eZeeMessageBox_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
    {
        if (e.Control & e.KeyCode == System.Windows.Forms.Keys.C)
        {
            string msg ="";
            msg += "\r\n---------------------\r\n"  ;
            msg += m_strTitle;
            msg += "\r\n---------------------\r\n" ;
            msg += m_strMessage;
            msg += "\r\n---------------------\r\n" ;
            System.Windows.Forms.Clipboard.Clear();
            System.Windows.Forms.Clipboard.SetDataObject(new System.Windows.Forms.DataObject(msg));
        }
    }

    private void eZeeMessageBox_Load(object sender, EventArgs e)
    {
        if (m_intAutoclose > 0)
        {
            this.tmrAutoClose = new System.Windows.Forms.Timer();
            tmrAutoClose.Tick += new EventHandler(tmrAutoClose_Tick);

            tmrAutoClose.Interval = 1000;
            tmrAutoClose.Enabled = true;

            if (this.RightToLeft == System.Windows.Forms.RightToLeft.Yes)
                this.Text = "(" + m_intAutoclose.ToString() + ") " + m_strTitle;
            else
                this.Text = m_strTitle + " (" + m_intAutoclose.ToString() + ")";

            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        }
    }

    #region Timer
    private System.Windows.Forms.Timer tmrAutoClose;
    private void tmrAutoClose_Tick(object sender, EventArgs e)
    {
        if (m_intAutoclose <= 0)
        {
            tmrAutoClose.Enabled = false;
            this.DialogResult = this.AcceptButton.DialogResult;
        }
        else
        {
            m_intAutoclose -= 1;
            if (this.RightToLeft == System.Windows.Forms.RightToLeft.Yes)
                this.Text = "(" + m_intAutoclose.ToString() + ") " + m_strTitle;
            else
                this.Text = m_strTitle + " (" + m_intAutoclose.ToString() + ")";

        }
    }
    #endregion
                 
}
