//************************************************************************************************************************************
//Class Name : clsMain.vb [DataOperation]
//Purpose    : 
//Date       : 
//Written By : Jitu
//Modified   : Naimish
//************************************************************************************************************************************
using Microsoft.SqlServer;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Collections;
using System.Data;
using System;
namespace eZeeCommonLib
{

    public class eZeeDatabase
    {
        private readonly string mstrModuleName = "Class Database";

        private string strServerName = "(Local)";
        private string strInstName = "SQLEXPRESS";
        private static string strDatabaseName = "hrmsConfiguration";

        /// <summary>
        /// A SQL Database Server Name.
        /// </summary>
        /// <value>String</value>
        /// <returns>String</returns>
        /// <remarks></remarks>
        public string ServerName
        {
            get
            {
                return strServerName;
            }
            set
            {
                strServerName = value;
            }
        }

        //''' <summary>
        //''' A SQL Database Server Instance Name.
        //''' </summary>
        //''' <value>String</value>
        //''' <returns>String</returns>
        //''' <remarks></remarks>
        //Public Property InstanceName() As String
        //    Get
        //        Return strInstName
        //    End Get
        //    Set(ByVal value As String)
        //        strInstName = value
        //    End Set
        //End Property

        //''' <summary>
        //''' A SQL Database Name.
        //''' </summary>
        //''' <value>String</value>
        //''' <returns>String</returns>
        //''' <remarks></remarks>
        //Public Property DatabaseName() As String
        //    Get
        //        Return strDatabaseName
        //    End Get
        //    Set(ByVal value As String)
        //        strDatabaseName = value
        //    End Set
        //End Property

        /// <summary>
        /// For create database connection.
        /// Modify By: Naimish
        /// </summary>
        public bool Connect()
        {
            string strConnection = null;
            string strs = null;

            try
            {

                strInstName = "aPayroll";

                strs = "pRofessionalaRuti999";
                //strDatabaseName = "PMS_NextGen";

                //Jitu (11 Sep 2009) --Start
                //strConnection = "Data Source=" + strServerName + "\\" + strInstName + ";" + "Initial Catalog = " + strDatabaseName + ";" + "Persist Security Info=True;" + "User ID = sa;" + "Password=" + strs + "";
                strConnection = "Data Source=" + strServerName + "\\" + strInstName + ";" + "Persist Security Info=True;" + "User ID = sa;" + "Password=" + strs + "";
                //Jitu (11 Sep 2009) --End

                modGlobal.gblnIsReConnect = true;

                if (modGlobal.gsqlTransaction != null)
                {
                    modGlobal.gsqlTransaction.Rollback();
                    modGlobal.gsqlTransaction.Dispose();
                }

                if (modGlobal.gConn != null)
                {
                    System.Data.SqlClient.SqlConnection.ClearAllPools();
                    modGlobal.gConn.Close();
                    modGlobal.gConn.Dispose();
                    modGlobal.gConn = null;
                }

                modGlobal.gblnBindTransaction = false;

                modGlobal.gConn = new System.Data.SqlClient.SqlConnection();
                if (modGlobal.gConn.State != ConnectionState.Closed)
                {
                    modGlobal.gConn.Close();
                }
                modGlobal.gConn.ConnectionString = strConnection;

                modGlobal.gConn.Open();

                modGlobal.gblnIsReConnect = false;
                //Naimish (06 Mar 2008) -- Start
                modGlobal.createFormatString();
                //Naimish (06 Mar 2008) -- End

                //"8AC5-AE72-0464-06BA-B0C5-DC4D-C44E-7D0F"
                //"C5CA-6A01-15EF-2B6D-3CEA-9743-11CD-BF14"
                //eCZ/JOfqhmLwnQ2dPPGFtUQwP4TVOPm1UsYB3P+/Ghdz+hwXQ6L+eZCgYsAJ1m5s9xbkz+uDXSyK758MkeAlFc3L6mU8wU63QNLlaLx8S8o=
                //Dim str As String = clsSecurity.Encrypt("C5CA-6A01-15EF-2B6D-3CEA-9743-11CD-BF14", "Elegant")

                //Naimish
                //string str = "X/tE1qiAEnnYbobxbr935hZLXdQzVj8SMlfCHzfwf0AJcLq/o2rRY0UbnByjI1j9G2M/LHPqCPXxFGPTbWu7HZCurZQip4o2dzRKGRQWBMI=";
                //str = clsSecurity.Decrypt(str, "Elegant");
                //Elegant.Ui.RibbonLicenser.LicenseKey = str;

                //Divelements.SandRibbon.Ribbon.ActivateProduct("2219|VjSsTFkVUgMHvrR+RZuPZtmG+0s=");
                return true;

            }
            catch (System.Exception ex)
            {
                string strErrMsg = null;
                strErrMsg = ex.Message + "\r" + "[Connect," + mstrModuleName + "]";
                throw new Exception(strErrMsg);
            }
        }


        /// <summary>
        /// Get Database backup path of EZEENEXTGEN instance from registry.
        /// </summary>
        /// <returns>String, Database backup path of EZEENEXTGEN instance.</returns>
        /// <remarks></remarks>
        public string GetBackupPath()
        {
            object strSubKey = "";
            object strBackUpPath = "";
            clsRegistry objReg = new clsRegistry();

            try
            {
                objReg.getValue( objReg.HKeyLocalMachine, "Software\\\\Microsoft\\\\Microsoft SQL Server\\\\Instance Names\\\\SQL\\\\", "aPayroll", ref strSubKey );
                objReg.getValue( objReg.HKeyLocalMachine , "Software\\\\Microsoft\\\\Microsoft SQL Server\\\\" + strSubKey.ToString() + "\\\\MSSQLServer\\\\", "BackupDirectory", ref strBackUpPath);

                return strBackUpPath.ToString();

            }
            catch (Exception ex)
            {
                ex.ToString();
                return "";
            }
            finally
            {
                objReg = null;
            }
        }

        // Adds an ACL entry on the specified directory for the specified account.
        public void AddDirectorySecurity(string FileName, string Account, System.Security.AccessControl.FileSystemRights Rights, System.Security.AccessControl.AccessControlType ControlType)
        {
            // Create a new DirectoryInfoobject.
            System.IO.DirectoryInfo dInfo = new System.IO.DirectoryInfo(FileName);

            // Get a DirectorySecurity object that represents the 
            // current security settings.
            System.Security.AccessControl.DirectorySecurity dSecurity = dInfo.GetAccessControl();

            // Add the FileSystemAccessRule to the security settings. 
            dSecurity.AddAccessRule(new System.Security.AccessControl.FileSystemAccessRule(Account, Rights, ControlType));

            // Set the new access settings.
            dInfo.SetAccessControl(dSecurity);

        }

        //Sohail (26 Apr 2013) - Start
        /// <summary>
        /// Backup a active database file.
        /// It's only work on server machine.
        /// </summary>
        //public string Backup(string strFilePath)
        //{

        //    Server objServer = null;
        //    ServerConnection objServerConn = null;
        //    BackupDeviceItem objBackupDeviceItem = null;
        //    Microsoft.SqlServer.Management.Smo.Backup objBackup = null;

        //    string strBackUpPath = "";
        //    string strBackUpFile = "";
        //    string DatabaseName = null; 
        //    try
        //    {
        //        //***** Check Backup Path File is Exists 
        //        if (!(System.IO.Directory.Exists(strFilePath)))
        //        {
        //            throw new Exception("Invalid database back path.");
        //        }

        //        //***** Connect to server
        //        objServerConn = new ServerConnection(strServerName + "\\aPayroll", "sa", "pRofessionalaRuti999");
        //        objServer = new Server(objServerConn);

        //        DatabaseName = modGlobal.gConn.Database.ToString();

        //        //***** Move File to SQL Backup Folder (Note: For some windows folder rights we do it.)
        //        strBackUpPath = objServer.Settings.BackupDirectory;
        //        if (strBackUpPath == "")
        //        {
        //            throw new Exception("Database not found!");
        //        }


        //        strBackUpFile = string.Format("{0}_{1}.dat", DatabaseName, System.DateTime.Now.ToString("HHmmss_yyyyMMdd"));

        //        //***** Create Backup Media
        //        objBackupDeviceItem = new BackupDeviceItem(strBackUpFile, DeviceType.File);
        //        string strs = "pRofessionalaRuti999"; //<0lling50t@ng0261<h@r|y
                               

        //        //***** Create Backup Process
        //        objBackup = new Microsoft.SqlServer.Management.Smo.Backup();
        //        objBackup.Database = modGlobal.gConn.Database;
        //        objBackup.Devices.Add(objBackupDeviceItem);
        //        objBackup.Initialize = true;
        //        objBackup.Action = BackupActionType.Database;
        //        objBackup.BackupSetName = "eZee";
        //        objBackup.BackupSetDescription = "Full Database Backup";
        //        objBackup.SetPassword(strs);
        //        objBackup.LogTruncation = BackupTruncateLogType.NoTruncate;
        //        objBackup.Complete += new ServerMessageEventHandler(_Completed);
        //        objBackup.PercentComplete += new PercentCompleteEventHandler(_PercentComplete);

        //        //***** Start Backup Process             
        //        objBackup.SqlBackup(objServer);
        //        objBackup.Wait();

        //        //***** Move Bakup File to Given Path
        //        System.IO.File.Copy(System.IO.Path.Combine(strBackUpPath, objBackupDeviceItem.Name) ,
        //                    System.IO.Path.Combine(strFilePath, objBackupDeviceItem.Name));

        //        //***** Delete Temp Bakup File
        //        System.IO.File.Delete(System.IO.Path.Combine(strBackUpPath, objBackupDeviceItem.Name));
        //        //Jitu (22 Jul 2009) --End

        //        return System.IO.Path.Combine(strFilePath, objBackupDeviceItem.Name);
        //    }
        //    catch (SmoException exSQL)
        //    {
        //        throw exSQL;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        objServer = null;
        //        objServerConn = null;
        //        objBackupDeviceItem = null;
        //        objBackup = null;
        //    }
        //}
        
        public string Backup(string strFilePath)
        {

            Server objServer = null;
            ServerConnection objServerConn = null;
            BackupDeviceItem objBackupDeviceItem = null;
            Microsoft.SqlServer.Management.Smo.Backup objBackup = null;

            string strBackUpPath = "";
            string strBackUpFile = "";
            string DatabaseName = null; 
            try
            {
                //***** Check Backup Path File is Exists 
                if (!(System.IO.Directory.Exists(strFilePath)))
                {
                    throw new Exception("Invalid database back path.");
                }

                //***** Connect to server
                objServerConn = new ServerConnection(strServerName + "\\aPayroll", "sa", "pRofessionalaRuti999");
                objServer = new Server(objServerConn);

                DatabaseName = modGlobal.gConn.Database.ToString();

                //***** Move File to SQL Backup Folder (Note: For some windows folder rights we do it.)
                strBackUpPath = objServer.Settings.BackupDirectory;
                if (strBackUpPath == "")
                {
                    throw new Exception("Database not found!");
                }


                strBackUpFile = string.Format("{0}_{1}.dat", DatabaseName, System.DateTime.Now.ToString("HHmmss_yyyyMMdd"));

                //***** Create Backup Media
                objBackupDeviceItem = new BackupDeviceItem(strBackUpFile, DeviceType.File);
                string strs = "pRofessionalaRuti999"; //<0lling50t@ng0261<h@r|y
                               

                //***** Create Backup Process
                objBackup = new Microsoft.SqlServer.Management.Smo.Backup();
                objBackup.Database = modGlobal.gConn.Database;
                objBackup.Devices.Add(objBackupDeviceItem);
                objBackup.Initialize = true;
                objBackup.Action = BackupActionType.Database;
                objBackup.BackupSetName = "eZee";
                objBackup.BackupSetDescription = "Full Database Backup";
                objBackup.SetPassword(strs);
                objBackup.LogTruncation = BackupTruncateLogType.NoTruncate;
                objBackup.Complete += new ServerMessageEventHandler(_Completed);
                objBackup.PercentComplete += new PercentCompleteEventHandler(_PercentComplete);

                //***** Start Backup Process             
                objBackup.SqlBackup(objServer);
                objBackup.Wait();

                //Sohail (02 May 2013) -- Start
                //   *** TO PREVENT BACKUP AT CLIENT MACHINE PATH, LEAVE IT ON SQL SERVER APPLICATION FOLDER PATH ON SERVER MACHINE
                ////***** Move Bakup File to Given Path
                //System.IO.File.Copy(System.IO.Path.Combine(strBackUpPath, objBackupDeviceItem.Name),
                //            System.IO.Path.Combine(strFilePath, objBackupDeviceItem.Name));

                ////***** Delete Temp Bakup File
                //System.IO.File.Delete(System.IO.Path.Combine(strBackUpPath, objBackupDeviceItem.Name));
                //***** Move Bakup File to Given Path
                if (System.IO.Directory.Exists(strFilePath) == true && System.IO.File.Exists(System.IO.Path.Combine(strBackUpPath, objBackupDeviceItem.Name)) == true)
                {
                System.IO.File.Copy(System.IO.Path.Combine(strBackUpPath, objBackupDeviceItem.Name) ,
                            System.IO.Path.Combine(strFilePath, objBackupDeviceItem.Name));

                //***** Delete Temp Bakup File
                System.IO.File.Delete(System.IO.Path.Combine(strBackUpPath, objBackupDeviceItem.Name));

                    return System.IO.Path.Combine(strFilePath, objBackupDeviceItem.Name);
                }
                else
                {
                    return System.IO.Path.Combine(strBackUpPath, objBackupDeviceItem.Name);
                }
                //Sohail (02 May 2013) --End
                //Jitu (22 Jul 2009) --End

                //Sohail (02 May 2013) --Start
                //return System.IO.Path.Combine(strFilePath, objBackupDeviceItem.Name);
                //Sohail (02 May 2013) --End
            }
            catch (SmoException exSQL)
            {
                throw exSQL;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objServer = null;
                objServerConn = null;
                objBackupDeviceItem = null;
                objBackup = null;
            }
        }
        //Sohail (26 Apr 2013) - End
        public void Restore(string strFileNamepath)
        {
            Server objServer = null;
            ServerConnection objServerConn = null;
            BackupDeviceItem objBackupDevice = null;
            string strBackUpPath = "";
            
            string ToDatabaseName = null; 
            Database ToDatabase = null;
            DataFile ToLogicalData = null;
            LogFile ToLogicalLog = null;

            Microsoft.SqlServer.Management.Smo.Restore objRestore = null;
            try
            {
                //***** Check Backup File is Exists 
                if (!(System.IO.File.Exists(strFileNamepath)))
                {
                    throw new Exception("Invalid database back up file to CD or flash drive.");
                }
                //***** Connect to server
                objServerConn = new ServerConnection(strServerName + "\\aPayroll", "sa", "pRofessionalaRuti999");
                objServer = new Server(objServerConn);

                //***** Move File to SQL Backup Folder (Note: For some windows folder rights we do it.)
                strBackUpPath = objServer.Settings.BackupDirectory;
                strBackUpPath += "\\newBackUp.Bak";
                System.IO.File.Copy(strFileNamepath, strBackUpPath, true);


                //***** Find Current Connection's File Information
                ToDatabaseName = modGlobal.gConn.Database.ToString();
                ToDatabase = objServer.Databases[ToDatabaseName];
                ToLogicalData = ToDatabase.FileGroups[0].Files[0];
                ToLogicalLog = ToDatabase.LogFiles[0];
                                           
                
                //***** Create Backup Device For Restore
                objBackupDevice = new BackupDeviceItem(strBackUpPath, DeviceType.File);
                string strs = "pRofessionalaRuti999"; //<0lling50t@ng0261<h@r|y

 
                //***** Create Retore Process
                objRestore = new Microsoft.SqlServer.Management.Smo.Restore();
                objRestore.Database = ToDatabaseName;
                objRestore.Devices.Add(objBackupDevice);
                objRestore.SetPassword(strs);

                DataTable dtFileList = objRestore.ReadFileList(objServer);
                string dbLogicalName = dtFileList.Rows[0][0].ToString();
                string dbPhysicalName = dtFileList.Rows[0][1].ToString();
                string logLogicalName = dtFileList.Rows[1][0].ToString();
                string logPhysicalName = dtFileList.Rows[1][1].ToString();

                //***** Find the Physical data and log files 
                RelocateFile reloData = new RelocateFile(dbLogicalName, ToLogicalData.FileName);
                RelocateFile reloLog = new RelocateFile(logLogicalName, ToLogicalLog.FileName);
                           
                objRestore.RelocateFiles.Add(reloData);
                objRestore.RelocateFiles.Add(reloLog);

                objRestore.ReplaceDatabase = true;
                objRestore.Complete += new ServerMessageEventHandler(_Completed);
                objRestore.PercentComplete += new PercentCompleteEventHandler(_PercentComplete);

                //***** Verify Restore Process
                string strErrMsg = "";
                if (!(objRestore.SqlVerify(objServer, out strErrMsg)))
                {
                    throw new Exception(strErrMsg);
                }

                if (strErrMsg + "" != "")
                {
                    throw new Exception(strErrMsg);
                }

                //***** Start Retore Process -- Start
                objServer.KillAllProcesses(ToDatabaseName);
                objRestore.SqlRestore(objServer);
                objRestore.Wait();
                //***** Start Retore Process -- End

                //***** Delete Old BacupFile
                System.IO.File.Delete(strBackUpPath);

                //***** Setdatabase Online 
                //Note: Restore process change database, After restore we restart application
                //and all process work on old connection data, so set new database to active mode.
                ToDatabase = objServer.Databases[ToDatabaseName];
                ToDatabase.SetOnline();

            }
            catch (Microsoft.SqlServer.Management.Smo.FailedOperationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objServer = null;
                objServerConn = null;
                objBackupDevice = null;
                objRestore = null;
                ToDatabase = null;
            }
        }

        #region  Bakup Restore Events

        public event ServerMessageEventHandler Completed;
        public event PercentCompleteEventHandler PercentComplete;

        private void _Completed(object sender, ServerMessageEventArgs e)
        {
            if (Completed != null)
                Completed(sender, e);
        }

        private void _PercentComplete(object sender, PercentCompleteEventArgs e)
        {
            if (PercentComplete != null)
                PercentComplete(sender, e);
        }

        #endregion

        public void fillDatabaseList(System.Windows.Forms.ListView lvSource)
        {
            try
            {
                Server objServer = null;
                ServerConnection objServerConn = null;
                Microsoft.SqlServer.Management.Smo.Database objDatabase = new Microsoft.SqlServer.Management.Smo.Database();

                objServerConn = new ServerConnection(modGlobal.gConn);
                objServer = new Server(objServerConn);

                System.Windows.Forms.ListViewItem lstItem = null;

                int nCount = 0;
                for (nCount = 0; nCount < objServer.Databases.Count; nCount++)
                {
                    objDatabase = objServer.Databases[nCount];
                    if (objDatabase.Name.Length < 4)
                    {
                        continue;

                    }
                    else  
                    if (objDatabase.Name.Substring(0, 4) == "hrms")
                    {
                        lstItem = new System.Windows.Forms.ListViewItem();
                        lstItem.Text = objDatabase.Name;
                        lvSource.Items.Add(lstItem);
                    }
                }
                }
            
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ArrayList fillDatabaseList()
        {
            try
            {
                Server objServer = null;
                ServerConnection objServerConn = null;
                Microsoft.SqlServer.Management.Smo.Database objDatabase = new Microsoft.SqlServer.Management.Smo.Database();

                objServerConn = new ServerConnection(modGlobal.gConn);
                objServer = new Server(objServerConn);

                ArrayList arrDatabase = new ArrayList();

                int nCount = 0;
                for (nCount = 0; nCount < objServer.Databases.Count; nCount++)
                {
                    objDatabase = objServer.Databases[nCount];
                    if (objDatabase.Name.Length < 4)
                    {
                        continue ;
                         
                    } else  
                    {
                    if (objDatabase.Name.Substring(0, 4) == "hrms")
                    {
                        arrDatabase.Add(objDatabase.Name);
                    }
                    }


                    
                }

                return arrDatabase;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ArrayList fillTranDatabaseList()
        {
            try
            {
                Server objServer = null;
                ServerConnection objServerConn = null;
                Microsoft.SqlServer.Management.Smo.Database objDatabase = new Microsoft.SqlServer.Management.Smo.Database();

                objServerConn = new ServerConnection(modGlobal.gConn);
                objServer = new Server(objServerConn);

                ArrayList arrDatabase = new ArrayList();
                
                int nCount = 0;
                for (nCount = 0; nCount < objServer.Databases.Count; nCount++)
                {
                    objDatabase = objServer.Databases[nCount];
                    if (objDatabase.Name.Length < 4)
                    {
                        continue;

                    }
                    else
                    {
                        if (objDatabase.Name.Substring(0, 4).ToUpper() == "TRAN")
                        {
                            arrDatabase.Add(objDatabase.Name);
                        }
                    }
                }

                return arrDatabase;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void change_database(string DatabaseName)
        {
            if (DatabaseName != "")
                strDatabaseName = DatabaseName;
            
            if (strDatabaseName != "")
                modGlobal.gConn.ChangeDatabase(strDatabaseName);
        }
        //Sohail (26 Apr 2013) - Start
        public static string current_database()
        {
            return modGlobal.gConn.Database.ToString();
        }
        //Sohail (26 Apr 2013) - End
    }
}