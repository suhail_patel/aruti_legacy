using System.IO;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using System;

public class RawPrinting
{
	public static string m_DocumentName = "RawPrinting";

#region  Properties 
	public string _DocumentName
	{
		set
		{
			m_DocumentName = value;
		}
	}
#endregion

#region  Structure declarions 
	[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Unicode)]
	public struct DOCINFOW
	{
		[MarshalAs(UnmanagedType.LPWStr)]
		public string pDocName;
		[MarshalAs(UnmanagedType.LPWStr)]
		public string pOutputFile;
		[MarshalAs(UnmanagedType.LPWStr)]
		public string pDataType;
	}
#endregion

#region  API declarions 
	[DllImport("winspool.Drv", EntryPoint="OpenPrinterW", SetLastError=true, CharSet=CharSet.Unicode, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
	private extern static bool OpenPrinter(string src, ref IntPtr hPrinter, int pd);

	[DllImport("winspool.Drv", EntryPoint="ClosePrinter", SetLastError=true, CharSet=CharSet.Unicode, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
	private extern static bool ClosePrinter(IntPtr hPrinter);

	[DllImport("winspool.Drv", EntryPoint="StartDocPrinterW", SetLastError=true, CharSet=CharSet.Unicode, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
	private extern static bool StartDocPrinter(IntPtr hPrinter, Int32 level, ref DOCINFOW pDI);

	[DllImport("winspool.Drv", EntryPoint="EndDocPrinter", SetLastError=true, CharSet=CharSet.Unicode, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
	private extern static bool EndDocPrinter(IntPtr hPrinter);

	[DllImport("winspool.Drv", EntryPoint="StartPagePrinter", SetLastError=true, CharSet=CharSet.Unicode, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
	private extern static bool StartPagePrinter(IntPtr hPrinter);

	[DllImport("winspool.Drv", EntryPoint="EndPagePrinter", SetLastError=true, CharSet=CharSet.Unicode, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
	private extern static bool EndPagePrinter(IntPtr hPrinter);

	[DllImport("winspool.Drv", EntryPoint="WritePrinter", SetLastError=true, CharSet=CharSet.Unicode, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
	private extern static bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, ref Int32 dwWritten);
#endregion

	/// <summary>
	/// Direct print a bytes to given printer.
	/// </summary>
	/// <param name="szPrinterName">A Printer name where to print raw data.</param>
	/// <param name="pBytes">A Bytes which are to be print.</param>
	/// <param name="dwCount">Length of Bytes.</param>
	/// <returns>Return Status data will be print or fail.</returns>
	public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
	{

		IntPtr hPrinter = new IntPtr(); // The printer handle.
		Int32 dwError = 0; // Last error - in case there was trouble.
		DOCINFOW di = new DOCINFOW(); // Describes your document (name, port, data type).
		Int32 dwWritten = 0; // The number of bytes written by WritePrinter().
		bool bSuccess = false; // Your success code.

		// Set up the DOCINFO structure.
		di.pDocName = m_DocumentName;
		di.pDataType = "RAW";

		// Assume failure unless you specifically succeed.
		bSuccess = false;
		if (OpenPrinter(szPrinterName, ref hPrinter, 0))
		{
			if (StartDocPrinter(hPrinter, 1, ref di))
			{
				if (StartPagePrinter(hPrinter))
				{
					// Write your printer-specific bytes to the printer.
					bSuccess = WritePrinter(hPrinter, pBytes, dwCount, ref dwWritten);
					EndPagePrinter(hPrinter);
				}
				EndDocPrinter(hPrinter);
			}
			ClosePrinter(hPrinter);
		}

		// If you did not succeed, GetLastError may give more information
		// about why not.
		if (bSuccess == false)
		{
			dwError = Marshal.GetLastWin32Error();
		}
		return bSuccess;
	} // SendBytesToPrinter()

	/// <summary>
	/// Direct print a file to given printer.
	/// </summary>
	/// <param name="szPrinterName">A Printer name where to print raw data.</param>
	/// <param name="szFileName">A file name which to-be print.</param>
	/// <returns>Return Status data will be print or fail.</returns>
	public static bool SendFileToPrinter(string szPrinterName, string szFileName)
	{
		// Open the file.
		FileStream fs = new FileStream(szFileName, FileMode.Open);
		// Create a BinaryReader on the file.
		BinaryReader br = new BinaryReader(fs);
		// Dim an array of bytes large enough to hold the file's contents.
		byte[] bytes = new byte[fs.Length + 1];
		bool bSuccess = false;
		// Your unmanaged pointer
		IntPtr pUnmanagedBytes = new IntPtr();
        bytes = br.ReadBytes((int)fs.Length);
        pUnmanagedBytes = Marshal.AllocCoTaskMem((int)fs.Length);
        Marshal.Copy(bytes, 0, pUnmanagedBytes, (int)fs.Length);
		// Send the unmanaged bytes to the printer.
        bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, (int)fs.Length);
		// Free the unmanaged memory that you allocated earlier.
		Marshal.FreeCoTaskMem(pUnmanagedBytes);
		fs.Close();
		fs = null;
		return bSuccess;
	} // SendFileToPrinter()

	/// <summary>
	/// Direct print a raw sting to given printer.
	/// </summary>
	/// <param name="szPrinterName">A Printer name where to print raw data.</param>
	/// <param name="szString">A string which to-be print.</param>
	/// <returns>Return Status data will be print or fail.</returns>
	public static bool SendStringToPrinter(string szPrinterName, string szString)
	{
		IntPtr pBytes = new IntPtr();
		Int32 dwCount = 0;
		bool bSuccess = false;
		dwCount = szString.Length;
		pBytes = Marshal.StringToCoTaskMemAnsi(szString);
		bSuccess = SendBytesToPrinter(szPrinterName, pBytes, dwCount);
		Marshal.FreeCoTaskMem(pBytes);

		return bSuccess;
	}

	public static IntPtr StrToIntPtr(string szString)
	{

		IntPtr pBytes = new IntPtr();
		Int32 dwCount = 0;
		try
		{
			dwCount = szString.Length;
			pBytes = Marshal.StringToCoTaskMemAnsi(szString);

			return pBytes;

		}
		finally
		{
			Marshal.FreeCoTaskMem(pBytes);
		}
	} //StrToIntPtr

	public static bool Open_CashDrawer(string sxCashDrawerName)
	{
		string strKey = (char)(27) + "p" + "\0" + (char)(25) + (char)(250);
		return SendStringToPrinter(sxCashDrawerName, strKey);
	}

}


