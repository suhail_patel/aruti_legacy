﻿namespace ExcelWriter
{
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Xml;

    public sealed class PivotFieldCollection : CollectionBase, IWriter
    {
        internal PivotFieldCollection()
        {
        }

        public PivotField Add()
        {
            PivotField pivotField = new PivotField();
            this.Add(pivotField);
            return pivotField;
        }

        public int Add(PivotField pivotField)
        {
            return base.InnerList.Add(pivotField);
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(PivotField item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(PivotField[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(PivotField item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, PivotField field)
        {
            base.InnerList.Insert(index, field);
        }

        public void Remove(PivotField field)
        {
            base.InnerList.Remove(field);
        }

        public PivotField this[int index]
        {
            get
            {
                return (PivotField) base.InnerList[index];
            }
        }
    }
}

