﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class SpreadSheetComponentOptions : IWriter, IReader, ICodeWriter
    {
        private bool _doNotEnableResize;
        private string _maxHeight;
        private string _maxWidth;
        private int _nextSheetNumber = -2147483648;
        private bool _preventPropBrowser;
        private bool _spreadsheetAutoFit;
        private SpreadSheetToolbar _toolbar;

        internal SpreadSheetComponentOptions()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._toolbar != null)
            {
                ((ICodeWriter) this._toolbar).WriteTo(type, method, new CodePropertyReferenceExpression(targetObject, "Toolbar"));
            }
            if (this._nextSheetNumber != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "NextSheetNumber", this._nextSheetNumber);
            }
            if (this._spreadsheetAutoFit)
            {
                Util.AddAssignment(method, targetObject, "SpreadsheetAutoFit", this._spreadsheetAutoFit);
            }
            if (this._doNotEnableResize)
            {
                Util.AddAssignment(method, targetObject, "DoNotEnableResize", this._doNotEnableResize);
            }
            if (this._preventPropBrowser)
            {
                Util.AddAssignment(method, targetObject, "PreventPropBrowser", this._preventPropBrowser);
            }
            if (this._maxHeight != null)
            {
                Util.AddAssignment(method, targetObject, "MaxHeight", this._maxHeight);
            }
            if (this._maxWidth != null)
            {
                Util.AddAssignment(method, targetObject, "MaxWidth", this._maxWidth);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (SpreadSheetToolbar.IsElement(element2))
                    {
                        ((IReader) this.Toolbar).ReadXml(element2);
                    }
                    else
                    {
                        if (Util.IsElement(element2, "NextSheetNumber", "urn:schemas-microsoft-com:office:component:spreadsheet"))
                        {
                            this._nextSheetNumber = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "SpreadsheetAutoFit", "urn:schemas-microsoft-com:office:component:spreadsheet"))
                        {
                            this._spreadsheetAutoFit = true;
                            continue;
                        }
                        if (Util.IsElement(element2, "DoNotEnableResize", "urn:schemas-microsoft-com:office:component:spreadsheet"))
                        {
                            this._doNotEnableResize = true;
                            continue;
                        }
                        if (Util.IsElement(element2, "PreventPropBrowser", "urn:schemas-microsoft-com:office:component:spreadsheet"))
                        {
                            this._preventPropBrowser = true;
                            continue;
                        }
                        if (Util.IsElement(element2, "MaxHeight", "urn:schemas-microsoft-com:office:component:spreadsheet"))
                        {
                            this._maxHeight = element2.InnerText;
                            continue;
                        }
                        if (Util.IsElement(element2, "MaxWidth", "urn:schemas-microsoft-com:office:component:spreadsheet"))
                        {
                            this._maxWidth = element2.InnerText;
                        }
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("c", "ComponentOptions", "urn:schemas-microsoft-com:office:component:spreadsheet");
            if (this._toolbar != null)
            {
                ((IWriter) this._toolbar).WriteXml(writer);
            }
            if (this._nextSheetNumber != -2147483648)
            {
                writer.WriteElementString("NextSheetNumber", "urn:schemas-microsoft-com:office:component:spreadsheet", this._nextSheetNumber.ToString());
            }
            if (this._spreadsheetAutoFit)
            {
                writer.WriteElementString("SpreadsheetAutoFit", "urn:schemas-microsoft-com:office:component:spreadsheet", "");
            }
            if (this._doNotEnableResize)
            {
                writer.WriteElementString("DoNotEnableResize", "urn:schemas-microsoft-com:office:component:spreadsheet", "");
            }
            if (this._preventPropBrowser)
            {
                writer.WriteElementString("PreventPropBrowser", "urn:schemas-microsoft-com:office:component:spreadsheet", "");
            }
            if (this._maxHeight != null)
            {
                writer.WriteElementString("MaxHeight", "urn:schemas-microsoft-com:office:component:spreadsheet", this._maxHeight);
            }
            if (this._maxWidth != null)
            {
                writer.WriteElementString("MaxWidth", "urn:schemas-microsoft-com:office:component:spreadsheet", this._maxWidth);
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "ComponentOptions", "urn:schemas-microsoft-com:office:component:spreadsheet");
        }

        public bool DoNotEnableResize
        {
            get
            {
                return this._doNotEnableResize;
            }
            set
            {
                this._doNotEnableResize = value;
            }
        }

        public string MaxHeight
        {
            get
            {
                return this._maxHeight;
            }
            set
            {
                this._maxHeight = value;
            }
        }

        public string MaxWidth
        {
            get
            {
                return this._maxWidth;
            }
            set
            {
                this._maxWidth = value;
            }
        }

        public int NextSheetNumber
        {
            get
            {
                return this._nextSheetNumber;
            }
            set
            {
                this._nextSheetNumber = value;
            }
        }

        public bool PreventPropBrowser
        {
            get
            {
                return this._preventPropBrowser;
            }
            set
            {
                this._preventPropBrowser = value;
            }
        }

        public bool SpreadsheetAutoFit
        {
            get
            {
                return this._spreadsheetAutoFit;
            }
            set
            {
                this._spreadsheetAutoFit = value;
            }
        }

        public SpreadSheetToolbar Toolbar
        {
            get
            {
                if (this._toolbar == null)
                {
                    this._toolbar = new SpreadSheetToolbar();
                }
                return this._toolbar;
            }
        }
    }
}

