﻿namespace ExcelWriter
{
    using System;

    public enum PrintErrorsOption
    {
        Displayed,
        Blank,
        Dash,
        NA
    }
}

