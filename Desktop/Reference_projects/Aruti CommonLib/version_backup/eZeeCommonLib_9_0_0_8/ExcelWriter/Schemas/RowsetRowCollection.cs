﻿namespace ExcelWriter.Schemas
{
    using ExcelWriter;
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Xml;

    public sealed class RowsetRowCollection : CollectionBase, IWriter
    {
        internal RowsetRowCollection()
        {
        }

        public RowsetRow Add()
        {
            RowsetRow row = new RowsetRow();
            this.Add(row);
            return row;
        }

        public int Add(RowsetRow row)
        {
            return base.InnerList.Add(row);
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(RowsetRow item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(RowsetRow[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(RowsetRow item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, RowsetRow item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(RowsetRow item)
        {
            base.InnerList.Remove(item);
        }

        public RowsetRow this[int index]
        {
            get
            {
                return (RowsetRow) base.InnerList[index];
            }
        }
    }
}

