﻿namespace ExcelWriter
{
    using System;

    public enum LineStyleOption
    {
        NotSet,
        Continuous,
        Dot,
        Dash,
        DashDot,
        DashDotDot,
        SlantDashDot,
        Double
    }
}

