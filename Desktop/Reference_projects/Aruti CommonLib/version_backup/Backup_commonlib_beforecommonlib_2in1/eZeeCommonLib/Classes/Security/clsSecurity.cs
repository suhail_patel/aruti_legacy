using System.Security.Cryptography;
using System.IO;
using System.Text;
using System;
using System.Windows.Forms;
namespace eZeeCommonLib
{
    /// <summary>
    /// Encrypttion - Decryption Class use for encrypt decrypt string for security purpose.
    /// </summary>
    /// <remarks></remarks>
    public class clsSecurity
    {

        private static byte[] bytIV = { 21, 54, 9, 13, 78, 19, 62, 45, 121, 195, 245, 35, 5, 1, 121, 212 };

        /// <summary>
        /// Encrypt a given string with key
        /// </summary>
        /// <param name="vstrTextToBeEncrypted"> A string value which will be encrypt.</param>
        /// <param name="vstrEncryptionKey">A encryption key user for encryption process.</param>
        /// <returns>String</returns>
        /// <remarks></remarks>
        public static string Encrypt(string vstrTextToBeEncrypted, string vstrEncryptionKey)
        {

            byte[] bytValue = null;
            byte[] bytKey = null;
            byte[] bytEncoded = null;

            //int intLength = 0;
            //int intRemaining = 0;

            MemoryStream objMemoryStream = new MemoryStream();
            CryptoStream objCryptoStream = null;
            RijndaelManaged objRijndaelManaged = null;
            try
            {
                vstrTextToBeEncrypted = StripNullCharacters(vstrTextToBeEncrypted);
                bytValue = Encoding.Unicode.GetBytes(vstrTextToBeEncrypted.ToCharArray());

                //intLength = vstrEncryptionKey.Length;

                //if (intLength >= 32)
                //{
                //    vstrEncryptionKey = vstrEncryptionKey.Substring(0, 32);
                //}
                //else
                //{
                //    //intLength = vstrEncryptionKey.Length;
                //    //intRemaining = 32 - intLength;
                //    //vstrEncryptionKey = vstrEncryptionKey + Microsoft.VisualBasic.Strings.StrDup(intRemaining, "X");
                //}

                vstrEncryptionKey = vstrEncryptionKey.PadRight(32, 'X');

                bytKey = Encoding.ASCII.GetBytes(vstrEncryptionKey.ToCharArray());

                objRijndaelManaged = new RijndaelManaged();

                objCryptoStream = new CryptoStream(objMemoryStream, objRijndaelManaged.CreateEncryptor(bytKey, bytIV), CryptoStreamMode.Write);
                objCryptoStream.Write(bytValue, 0, bytValue.Length);

                objCryptoStream.FlushFinalBlock();

                bytEncoded = objMemoryStream.ToArray();

                return Convert.ToBase64String(bytEncoded);

            }
            catch
            {
                return "";
            }
            finally
            {
                bytValue = null;
                bytKey = null;
                bytEncoded = null;

                if (objMemoryStream != null)
                {
                    objMemoryStream.Close();
                    //objMemoryStream.Dispose()
                }
                objMemoryStream = null;

                if (objCryptoStream != null)
                {
                    objCryptoStream.Close();
                    //objCryptoStream.Dispose()
                }
                objCryptoStream = null;

                if (objRijndaelManaged != null)
                {
                    objRijndaelManaged.Clear();
                }
                objRijndaelManaged = null;
            }

        }

        /// <summary>
        /// Decrypted string convert from Encrypted string.
        /// </summary>
        /// <param name="vstrStringToBeDecrypted"> A encrypted string value which will be decrypt.</param>
        /// <param name="vstrDecryptionKey">A encryption key user for encryption process.</param>
        /// <returns>String</returns>
        /// <remarks></remarks>
        public static string Decrypt(string vstrStringToBeDecrypted, string vstrDecryptionKey)
        {

            byte[] bytDataToBeDecrypted = null;
            byte[] bytTemp = null;
            byte[] bytDecryptionKey = null;

            //int intLength = 0;
            //int intRemaining = 0;

            RijndaelManaged objRijndaelManaged = new RijndaelManaged();
            MemoryStream objMemoryStream = null;
            CryptoStream objCryptoStream = null;

            string strReturnString = string.Empty;

            try
            {
                bytDataToBeDecrypted = Convert.FromBase64String(vstrStringToBeDecrypted);

                //intLength = vstrDecryptionKey.Length;

                //if (intLength >= 32)
                //{
                //    vstrDecryptionKey = vstrDecryptionKey.Substring(0, 32);
                //}
                //else
                //{
                //    intLength = vstrDecryptionKey.Length;
                //    intRemaining = 32 - intLength;
                //    vstrDecryptionKey = vstrDecryptionKey + Microsoft.VisualBasic.Strings.StrDup(intRemaining, "X");
                //}
                vstrDecryptionKey = vstrDecryptionKey.PadRight(32, 'X');
                bytDecryptionKey = Encoding.ASCII.GetBytes(vstrDecryptionKey.ToCharArray());

                bytTemp = new byte[bytDataToBeDecrypted.Length + 1];

                objMemoryStream = new MemoryStream(bytDataToBeDecrypted);

                objCryptoStream = new CryptoStream(objMemoryStream, objRijndaelManaged.CreateDecryptor(bytDecryptionKey, bytIV), CryptoStreamMode.Read);
                objCryptoStream.Read(bytTemp, 0, bytTemp.Length);

                //objCryptoStream.FlushFinalBlock()

                //objMemoryStream.Close()
                //objCryptoStream.Close()

                //Return StripNullCharacters(Encoding.Unicode.GetString(bytTemp))
                return StripNullCharacters(Encoding.Unicode.GetString(bytTemp, 0, bytTemp.Length));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return "";
            }
            finally
            {
                bytDataToBeDecrypted = null;
                bytTemp = null;
                bytDecryptionKey = null;

                if (objMemoryStream != null)
                {
                    objMemoryStream.Close();
                    //objMemoryStream.Dispose()
                }
                objMemoryStream = null;

                if (objCryptoStream != null)
                {
                    objCryptoStream.Close();
                    //objCryptoStream.Dispose()
                }
                objCryptoStream = null;

                if (objRijndaelManaged != null)
                {
                    objRijndaelManaged.Clear();
                }
                objRijndaelManaged = null;
            }
        }

        private static string StripNullCharacters(string vstrStringWithNulls)
        {
            int intPosition = 0;
            string strStringWithOutNulls = null;
            intPosition = 1;
            strStringWithOutNulls = vstrStringWithNulls;

            while (intPosition > 0)
            {
                intPosition = (vstrStringWithNulls.IndexOf("\0", intPosition - 1) + 1);

                if (intPosition > 0)
                {
                    strStringWithOutNulls = strStringWithOutNulls.Substring(0, intPosition - 1) + strStringWithOutNulls.Substring(strStringWithOutNulls.Length - (strStringWithOutNulls.Length - intPosition));
                }

                if (intPosition > strStringWithOutNulls.Length)
                {
                    break;
                }
            }

            return strStringWithOutNulls;
        }

    }
}