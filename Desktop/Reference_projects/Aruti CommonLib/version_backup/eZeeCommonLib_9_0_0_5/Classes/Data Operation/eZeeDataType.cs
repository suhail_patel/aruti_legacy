using System.Drawing;
using System;

namespace eZeeCommonLib
{
    public enum enDataType : int
    {
        ALIAS_SIZE = 5,
        NAME_SIZE = 255,
        DESC_SIZE = 1000,
        ADD_SIZE = 255,
        CITY_SIZE = 100,
        STATE_SIZE = 100,
        ZIPCODE_SIZE = 15,
        COUNTRY_SIZE = 100,
        EMAIL_SIZE = 100,
        FAX_SIZE = 20,
        PHONE_SIZE = 15,
        WEBSITE_SIZE = 255,
        GENDER_SIZE = 10,
        CARDNO_SIZE = 50,
        EXPDATE_SIZE = 10,
        REGISTRATION_SIZE = 20,
        VOUCHERNO_SIZE = 25,
        GUID_SIZE = 16,
        KEYNAME_SIZE = 255,
        KEYVALUE_SIZE = 50,
        IP_SIZE = 15,

        INT_SIZE = 4,
        FLOAT_SIZE = 8,
        MONEY_SIZE = 8,
        DATETIME_SIZE = 17,
        IMAGE_SIZE = 16,
        BIT_SIZE = 1,
        DECIMAL_SIZE = 36
        
    }


    public class eZeeDataType
    {

        #region  Data Field Size Constant

        /// <summary>
        /// 5
        /// </summary>
        public static int ALIAS_SIZE
        {
            get
            {
                return 5;
            }
        }

        /// <summary>
        /// 255
        /// </summary>
        public static int NAME_SIZE
        {
            get
            {
                return 255;
            }
        }

        /// <summary>
        /// 1000
        /// </summary>
        public static int DESC_SIZE
        {
            get
            {
                return 1000;
            }
        }

        /// <summary>
        /// 255
        /// </summary>
        public static int ADD_SIZE
        {
            get
            {
                return 255;
            }
        }

        /// <summary>
        /// 100
        /// </summary>
        public static int CITY_SIZE
        {
            get
            {
                return 100;
            }
        }

        /// <summary>
        /// 100
        /// </summary>
        public static int STATE_SIZE
        {
            get
            {
                return 100;
            }
        }
        
        /// <summary>
        /// 15
        /// </summary>
        public static int ZIPCODE_SIZE
        {
            get
            {
                return 15;
            }
        }

        /// <summary>
        /// 100
        /// </summary>
        public static int COUNTRY_SIZE
        {
            get
            {
                return 100;
            }
        }

        /// <summary>
        /// 100
        /// </summary>
        public static int EMAIL_SIZE
        {
            get
            {
                return 100;
            }
        }

        /// <summary>
        /// 20
        /// </summary>
        public static int FAX_SIZE
        {
            get
            {
                return 20;
            }
        }

        /// <summary>
        /// 15
        /// </summary>
        public static int PHONE_SIZE
        {
            get
            {
                return 15;
            }
        }

        /// <summary>
        /// 255
        /// </summary>
        public static int WEBSITE_SIZE
        {
            get
            {
                return 255;
            }
        }

        /// <summary>
        /// 10
        /// </summary>
        public static int GENDER_SIZE
        {
            get
            {
                return 10;
            }
        }

        /// <summary>
        /// 50
        /// </summary>
        public static int CARDNO_SIZE
        {
            get
            {
                return 50;
            }
        }

        /// <summary>
        /// 10
        /// </summary>
        public static int EXPDATE_SIZE
        {
            get
            {
                return 10;
            }
        }

        /// <summary>
        /// 20
        /// </summary>
        public static int REGISTRATION_SIZE
        {
            get
            {
                return 20;
            }
        }

        /// <summary>
        /// 25
        /// </summary>
        public static int VOUCHERNO_SIZE
        {
            get
            {
                return 25;
            }
        }

        /// <summary>
        /// 16
        /// </summary>
        public static int GUID_SIZE
        {
            get
            {
                return 16;
            }
        }

        /// <summary>
        /// 255
        /// </summary>
        public static int KEYNAME_SIZE
        {
            get
            {
                return 255;
            }
        }

        /// <summary>
        /// 50
        /// </summary>
        public static int KEYVALUE_SIZE
        {
            get
            {
                return 50;
            }
        }

        /// <summary>
        /// 15
        /// </summary>
        public static int IP_SIZE
        {
            get
            {
                return 15;
            }
        }

        
        #endregion

        #region  Data Type Size Constant
        /// <summary>
        /// 4
        /// </summary>
        public static int INT_SIZE
        {
            get
            {
                return 4;
            }
        }

        /// <summary>
        /// 8
        /// </summary>
        public static int FLOAT_SIZE
        {
            get
            {
                return 8;
            }
        }

        /// <summary>
        /// 8
        /// </summary>
        public static int MONEY_SIZE
        {
            get
            {
                return 8;
            }
        }

        /// <summary>
        /// 17
        /// </summary>
        public static int DATETIME_SIZE
        {
            get
            {
                return 17;
            }
        }

        /// <summary>
        /// 16
        /// </summary>
        public static int IMAGE_SIZE
        {
            get
            {
                return 16;
            }
        }

        /// <summary>
        /// 1
        /// </summary>
        public static int BIT_SIZE
        {
            get
            {
                return 1;
            }
        }

        public static int DECIMAL_SIZE
        {
            get
            {
                return 36;
            }

        }

        #endregion

        /// <summary>
        /// Convert database image data field value into image.
        /// </summary>
        /// <param name="Expression">A image data field value.</param> 
        /// <returns>A image which is converted from image data field value.</returns>
        public static Image data2Image(object Expression)
        {
            try
            {
                if (System.Convert.IsDBNull(Expression))
                {
                    return null;
                }
                else
                {
                    byte[] imgContent = (byte[])Expression;
                    System.IO.MemoryStream stream = new System.IO.MemoryStream(imgContent);
                    Image image = Image.FromStream(stream);
                    return image;
                }
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show ( ex.Message + "[data2Image-eZeeDataType]");
                return null;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Convert image into database image data field value.
        /// </summary>
        /// <param name="Expression">A image.</param> 
        /// <returns>A image data field value which is converted from image.</returns>
        public static byte[] image2Data(Image Expression)
        {
            try
            {
                if (Expression == null)
                {
                    return null;
                }
                else
                {
                    
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    //pression.RawFormat 
                    Expression.Save( stream,System.Drawing.Imaging.ImageFormat.Png);
                    byte[] imgContent = stream.ToArray();

                    return imgContent;
                }

            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message + "[image2Data-eZeeDataType]");
                return null;
            }
            finally
            {

            }
        }

        public static Image ResizeImage(Image picture, Size img_size )
        {
            if (picture == null)
            {
                return null;
            }
            IntPtr inp = new IntPtr();

            return picture.GetThumbnailImage(img_size.Width,img_size.Height, null, inp);
        }

    }
}