﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetStyleBorder : IWriter, IReader, ICodeWriter
    {
        private string _color;
        private LineStyleOption _lineStyle;
        private StylePosition _position;
        private int _weight = -1;

        internal WorksheetStyleBorder()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._position != StylePosition.NotSet)
            {
                Util.AddAssignment(method, targetObject, "Position", this._position);
            }
            if (this._weight != -1)
            {
                Util.AddAssignment(method, targetObject, "Weight", this._weight);
            }
            if (this._color != null)
            {
                Util.AddAssignment(method, targetObject, "Color", this._color);
            }
            if (this._lineStyle != LineStyleOption.NotSet)
            {
                Util.AddAssignment(method, targetObject, "LineStyle", this._lineStyle);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._color = Util.GetAttribute(element, "Color", "urn:schemas-microsoft-com:office:spreadsheet");
            string attribute = element.GetAttribute("Position", "urn:schemas-microsoft-com:office:spreadsheet");
            if ((attribute != null) && (attribute.Length != 0))
            {
                this._position = (StylePosition) Enum.Parse(typeof(StylePosition), attribute, true);
            }
            this._weight = Util.GetAttribute(element, "Weight", "urn:schemas-microsoft-com:office:spreadsheet", -1);
            this._color = Util.GetAttribute(element, "Color", "urn:schemas-microsoft-com:office:spreadsheet");
            attribute = element.GetAttribute("LineStyle", "urn:schemas-microsoft-com:office:spreadsheet");
            if ((attribute != null) && (attribute.Length != 0))
            {
                this._lineStyle = (LineStyleOption) Enum.Parse(typeof(LineStyleOption), attribute, true);
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Border", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._color != null)
            {
                writer.WriteAttributeString("s", "Color", "urn:schemas-microsoft-com:office:spreadsheet", this._color);
            }
            if (this._position != StylePosition.NotSet)
            {
                writer.WriteAttributeString("s", "Position", "urn:schemas-microsoft-com:office:spreadsheet", this._position.ToString());
            }
            if (this._lineStyle != LineStyleOption.NotSet)
            {
                writer.WriteAttributeString("s", "LineStyle", "urn:schemas-microsoft-com:office:spreadsheet", this._lineStyle.ToString());
            }
            if (this._weight != -1)
            {
                writer.WriteAttributeString("s", "Weight", "urn:schemas-microsoft-com:office:spreadsheet", this._weight.ToString());
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Border", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        internal int IsSpecial()
        {
            if (((this._position == StylePosition.NotSet) || (this._weight == -1)) || (this._lineStyle == LineStyleOption.NotSet))
            {
                return 2;
            }
            if (this._color == null)
            {
                return 0;
            }
            return 1;
        }

        public string Color
        {
            get
            {
                return this._color;
            }
            set
            {
                this._color = value;
            }
        }

        public LineStyleOption LineStyle
        {
            get
            {
                return this._lineStyle;
            }
            set
            {
                this._lineStyle = value;
            }
        }

        public StylePosition Position
        {
            get
            {
                return this._position;
            }
            set
            {
                this._position = value;
            }
        }

        public int Weight
        {
            get
            {
                return this._weight;
            }
            set
            {
                if (value >= 3)
                {
                    this._weight = 3;
                }
                else if (value <= 0)
                {
                    this._weight = 0;
                }
                else
                {
                    this._weight = value;
                }
            }
        }
    }
}

