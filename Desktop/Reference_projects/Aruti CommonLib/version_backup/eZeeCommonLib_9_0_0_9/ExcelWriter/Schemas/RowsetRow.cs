﻿namespace ExcelWriter.Schemas
{
    using ExcelWriter;
    using System;
    using System.Xml;

    public sealed class RowsetRow : IWriter
    {
        private RowsetColumnCollection _columns;

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("z", "row", "#RowsetSchema");
            if (this._columns != null)
            {
                ((IWriter) this._columns).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public RowsetColumnCollection Columns
        {
            get
            {
                if (this._columns == null)
                {
                    this._columns = new RowsetColumnCollection();
                }
                return this._columns;
            }
        }
    }
}

