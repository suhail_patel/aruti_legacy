﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetStyleAlignment : IWriter, IReader, ICodeWriter
    {
        private StyleHorizontalAlignment _horizontal;
        private int _indent;
        private StyleReadingOrder _readingOrder;
        private int _rotate;
        private bool _shrinkToFit;
        private StyleVerticalAlignment _vertical;
        private bool _verticalText;
        private bool _wrapText;

        internal WorksheetStyleAlignment()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._horizontal != StyleHorizontalAlignment.Automatic)
            {
                Util.AddAssignment(method, targetObject, "Horizontal", this._horizontal);
            }
            if (this._indent != 0)
            {
                Util.AddAssignment(method, targetObject, "Indent", this._indent);
            }
            if (this._rotate != 0)
            {
                Util.AddAssignment(method, targetObject, "Rotate", this._rotate);
            }
            if (this._shrinkToFit)
            {
                Util.AddAssignment(method, targetObject, "ShrinkToFit", this._shrinkToFit);
            }
            if (this._vertical != StyleVerticalAlignment.Automatic)
            {
                Util.AddAssignment(method, targetObject, "Vertical", this._vertical);
            }
            if (this._verticalText)
            {
                Util.AddAssignment(method, targetObject, "VerticalText", this._verticalText);
            }
            if (this._wrapText)
            {
                Util.AddAssignment(method, targetObject, "WrapText", this._wrapText);
            }
            if (this._readingOrder != StyleReadingOrder.NotSet)
            {
                Util.AddAssignment(method, targetObject, "ReadingOrder", this._readingOrder);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            string attribute = element.GetAttribute("Horizontal", "urn:schemas-microsoft-com:office:spreadsheet");
            if ((attribute != null) && (attribute.Length != 0))
            {
                this._horizontal = (StyleHorizontalAlignment) Enum.Parse(typeof(StyleHorizontalAlignment), attribute, true);
            }
            this._indent = Util.GetAttribute(element, "Indent", "urn:schemas-microsoft-com:office:spreadsheet", 0);
            this._rotate = Util.GetAttribute(element, "Rotate", "urn:schemas-microsoft-com:office:spreadsheet", 0);
            this._shrinkToFit = Util.GetAttribute(element, "ShrinkToFit", "urn:schemas-microsoft-com:office:spreadsheet", false);
            attribute = element.GetAttribute("Vertical", "urn:schemas-microsoft-com:office:spreadsheet");
            if ((attribute != null) && (attribute.Length != 0))
            {
                this._vertical = (StyleVerticalAlignment) Enum.Parse(typeof(StyleVerticalAlignment), attribute, true);
            }
            attribute = element.GetAttribute("ReadingOrder", "urn:schemas-microsoft-com:office:spreadsheet");
            if ((attribute != null) && (attribute.Length != 0))
            {
                this._readingOrder = (StyleReadingOrder) Enum.Parse(typeof(StyleReadingOrder), attribute, true);
            }
            this._verticalText = element.GetAttribute("VerticalText", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
            this._wrapText = element.GetAttribute("WrapText", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Alignment", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._horizontal != StyleHorizontalAlignment.Automatic)
            {
                writer.WriteAttributeString("Horizontal", "urn:schemas-microsoft-com:office:spreadsheet", this._horizontal.ToString());
            }
            if (this._indent != 0)
            {
                writer.WriteAttributeString("Indent", "urn:schemas-microsoft-com:office:spreadsheet", this._indent.ToString());
            }
            if (this._rotate != 0)
            {
                writer.WriteAttributeString("Rotate", "urn:schemas-microsoft-com:office:spreadsheet", this._rotate.ToString());
            }
            if (this._shrinkToFit)
            {
                writer.WriteAttributeString("ShrinkToFit", "urn:schemas-microsoft-com:office:spreadsheet", "1");
            }
            if (this._vertical != StyleVerticalAlignment.Automatic)
            {
                writer.WriteAttributeString("Vertical", "urn:schemas-microsoft-com:office:spreadsheet", this._vertical.ToString());
            }
            if (this._verticalText)
            {
                writer.WriteAttributeString("VerticalText", "urn:schemas-microsoft-com:office:spreadsheet", "1");
            }
            if (this._wrapText)
            {
                writer.WriteAttributeString("WrapText", "urn:schemas-microsoft-com:office:spreadsheet", "1");
            }
            if (this._readingOrder != StyleReadingOrder.NotSet)
            {
                writer.WriteAttributeString("ReadingOrder", "urn:schemas-microsoft-com:office:spreadsheet", this._readingOrder.ToString());
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Alignment", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public StyleHorizontalAlignment Horizontal
        {
            get
            {
                return this._horizontal;
            }
            set
            {
                this._horizontal = value;
            }
        }

        public int Indent
        {
            get
            {
                return this._indent;
            }
            set
            {
                this._indent = value;
            }
        }

        public StyleReadingOrder ReadingOrder
        {
            get
            {
                return this._readingOrder;
            }
            set
            {
                this._readingOrder = value;
            }
        }

        public int Rotate
        {
            get
            {
                return this._rotate;
            }
            set
            {
                this._rotate = value;
            }
        }

        public bool ShrinkToFit
        {
            get
            {
                return this._shrinkToFit;
            }
            set
            {
                this._shrinkToFit = value;
            }
        }

        public StyleVerticalAlignment Vertical
        {
            get
            {
                return this._vertical;
            }
            set
            {
                this._vertical = value;
            }
        }

        public bool VerticalText
        {
            get
            {
                return this._verticalText;
            }
            set
            {
                this._verticalText = value;
            }
        }

        public bool WrapText
        {
            get
            {
                return this._wrapText;
            }
            set
            {
                this._wrapText = value;
            }
        }
    }
}

