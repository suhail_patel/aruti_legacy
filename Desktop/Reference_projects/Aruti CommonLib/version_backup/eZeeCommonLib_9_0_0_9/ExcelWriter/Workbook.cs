﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.ComponentModel;
    using System.IO;
    using System.Text;
    using System.Xml;

    public sealed class Workbook : IWriter, IReader, ICodeWriter
    {
        private DocumentProperties _documentProperties;
        private ExcelWriter.ExcelWorkbook _excelWorkbook;
        private bool _generateExcelProcessingInstruction = true;
        private WorksheetNamedRangeCollection _names;
        private ExcelWriter.PivotCache _pivotCache;
        private ExcelWriter.SpreadSheetComponentOptions _spreadSheetComponentOptions;
        private WorksheetStyleCollection _styles;
        private WorksheetCollection _worksheets;

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._documentProperties != null)
            {
                Util.Traverse(type, this._documentProperties, method, targetObject, "Properties");
            }
            if (this._spreadSheetComponentOptions != null)
            {
                Util.Traverse(type, this._spreadSheetComponentOptions, method, targetObject, "SpreadSheetComponentOptions");
            }
            if (this._excelWorkbook != null)
            {
                Util.Traverse(type, this._excelWorkbook, method, targetObject, "ExcelWorkbook");
            }
            if (this._styles != null)
            {
                Util.Traverse(type, this._styles, method, targetObject, "Styles");
            }
            if (this._names != null)
            {
                Util.Traverse(type, this._names, method, targetObject, "Names");
            }
            if (this._worksheets != null)
            {
                Util.Traverse(type, this._worksheets, method, targetObject, "Worksheets");
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new InvalidOperationException("The specified Xml is not a valid Workbook.\nElement Name:" + element.Name);
            }
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (DocumentProperties.IsElement(element2))
                    {
                        ((IReader) this.Properties).ReadXml(element2);
                    }
                    else
                    {
                        if (ExcelWriter.SpreadSheetComponentOptions.IsElement(element2))
                        {
                            ((IReader) this.SpreadSheetComponentOptions).ReadXml(element2);
                            continue;
                        }
                        if (ExcelWriter.ExcelWorkbook.IsElement(element2))
                        {
                            ((IReader) this.ExcelWorkbook).ReadXml(element2);
                            continue;
                        }
                        if (WorksheetStyleCollection.IsElement(element2))
                        {
                            ((IReader) this.Styles).ReadXml(element2);
                            continue;
                        }
                        if (WorksheetNamedRangeCollection.IsElement(element2))
                        {
                            ((IReader) this.Names).ReadXml(element2);
                            continue;
                        }
                        if (Worksheet.IsElement(element2))
                        {
                            Worksheet sheet = new Worksheet(null);
                            ((IReader) sheet).ReadXml(element2);
                            this.Worksheets.Add(sheet);
                        }
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Workbook", "urn:schemas-microsoft-com:office:spreadsheet");
            writer.WriteAttributeString("xmlns", "x", null, "urn:schemas-microsoft-com:office:excel");
            writer.WriteAttributeString("xmlns", "o", null, "urn:schemas-microsoft-com:office:office");
            if (this._documentProperties != null)
            {
                ((IWriter) this._documentProperties).WriteXml(writer);
            }
            if (this._spreadSheetComponentOptions != null)
            {
                ((IWriter) this._spreadSheetComponentOptions).WriteXml(writer);
            }
            if (this._excelWorkbook != null)
            {
                ((IWriter) this._excelWorkbook).WriteXml(writer);
            }
            if (this._styles != null)
            {
                ((IWriter) this._styles).WriteXml(writer);
            }
            if (this._names != null)
            {
                ((IWriter) this._names).WriteXml(writer);
            }
            if (this._worksheets != null)
            {
                ((IWriter) this._worksheets).WriteXml(writer);
            }
            if (this._pivotCache != null)
            {
                ((IWriter) this._pivotCache).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Workbook", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public void Load(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            if (stream.Position >= stream.Length)
            {
                stream.Position = 0L;
            }
            XmlDocument document = new XmlDocument();
            document.Load(stream);
            ((IReader) this).ReadXml(document.DocumentElement);
        }

        public void Load(string filename)
        {
            FileStream stream = null;
            try
            {
                stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                this.Load(stream);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }
        }

        public void Save(Stream stream)
        {
            if (this.Worksheets.Count == 0)
            {
                this.Worksheets.Add("Sheet 1");
            }
            XmlTextWriter writer = new XmlTextWriter(stream, Encoding.UTF8);
            writer.Namespaces = true;
            writer.Formatting = Formatting.Indented;
            writer.WriteProcessingInstruction("xml", "version='1.0'");
            if (this._generateExcelProcessingInstruction)
            {
                writer.WriteProcessingInstruction("mso-application", "progid='Excel.Sheet'");
            }
            ((IWriter) this).WriteXml(writer);
            writer.Flush();
        }

        public void Save(string filename)
        {
            FileStream stream = null;
            try
            {
                stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
                this.Save(stream);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }
        }

        public ExcelWriter.ExcelWorkbook ExcelWorkbook
        {
            get
            {
                if (this._excelWorkbook == null)
                {
                    this._excelWorkbook = new ExcelWriter.ExcelWorkbook();
                }
                return this._excelWorkbook;
            }
        }

        public bool GenerateExcelProcessingInstruction
        {
            get
            {
                return this._generateExcelProcessingInstruction;
            }
            set
            {
                this._generateExcelProcessingInstruction = value;
            }
        }

        public WorksheetNamedRangeCollection Names
        {
            get
            {
                if (this._names == null)
                {
                    this._names = new WorksheetNamedRangeCollection();
                }
                return this._names;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public ExcelWriter.PivotCache PivotCache
        {
            get
            {
                if (this._pivotCache == null)
                {
                    this._pivotCache = new ExcelWriter.PivotCache();
                }
                return this._pivotCache;
            }
        }

        public DocumentProperties Properties
        {
            get
            {
                if (this._documentProperties == null)
                {
                    this._documentProperties = new DocumentProperties();
                }
                return this._documentProperties;
            }
        }

        public ExcelWriter.SpreadSheetComponentOptions SpreadSheetComponentOptions
        {
            get
            {
                if (this._spreadSheetComponentOptions == null)
                {
                    this._spreadSheetComponentOptions = new ExcelWriter.SpreadSheetComponentOptions();
                }
                return this._spreadSheetComponentOptions;
            }
        }

        public WorksheetStyleCollection Styles
        {
            get
            {
                if (this._styles == null)
                {
                    this._styles = new WorksheetStyleCollection(this);
                }
                return this._styles;
            }
        }

        public WorksheetCollection Worksheets
        {
            get
            {
                if (this._worksheets == null)
                {
                    this._worksheets = new WorksheetCollection();
                }
                return this._worksheets;
            }
        }
    }
}

