using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using TaskScheduler.Definition;

namespace TaskScheduler
{

	public sealed class TriggerCollection : System.Collections.ReadOnlyCollectionBase, System.IDisposable
	{

#region  Fields 

		private Task m_Task;

#endregion

#region  Constructors 

		internal TriggerCollection(Task parentTask)
		{
			m_Task = parentTask;
			Refresh();
		}

#endregion

#region  Protected Methods 

    ~TriggerCollection()
    {
        this.Dispose();
    }

    public void Dispose()
    {
        if (base.InnerList.Count > 0)
        {
            base.InnerList.Clear();
        }

        // Supress Finalize call
        GC.SuppressFinalize(this);
    }

#endregion

#region  Public Properties 

		public Trigger this[int index]
		{
			get
			{
				return (Trigger)(base.InnerList[index]);
			}
		}

#endregion

#region  Public Methods 

		public Trigger Add()
		{
			short Index = 0;
			ITaskTrigger Trigger = null;

			// Create the new ITaskTrigger
			Trigger = m_Task.ITaskObj.CreateTrigger(out Index);

			// Refresh the collection
			this.Refresh();

			// Create the Trigger object
			return new Trigger(Trigger, m_Task, Index);

		}

		public void Remove(short index)
		{

			// Remove the trigger
			m_Task.ITaskObj.DeleteTrigger(index);

			// Refresh the collection
			this.Refresh();

		}

		public void Refresh()
		{

			short lIdx = 0;
			ITask oTask = null;
			ITaskTrigger oTaskTrigger = null;

			// Clear the collection
			base.InnerList.Clear();

			// Get the ITask object from
			// the parent Task
			oTask = m_Task.ITaskObj;

			// Enumerate all triggers
			for (lIdx = 0; lIdx <= oTask.GetTriggerCount() - 1; lIdx++)
			{

				// Get the ITaskTrigger object
				oTaskTrigger = oTask.GetTrigger(lIdx);

				// Create and add a trigger object
				// for the current ITaskTrigger
				base.InnerList.Add(new Trigger(oTaskTrigger, m_Task, lIdx));

			}

		}

#endregion

	}

}
