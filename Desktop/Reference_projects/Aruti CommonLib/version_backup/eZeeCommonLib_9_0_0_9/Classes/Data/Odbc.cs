using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Drawing;

namespace eZee.Data.Manager
{
    public class Odbc
    {
        internal Connection cnn;

        public Odbc(Connection Connection)
        {
            cnn = Connection;
        }

        #region Execute
        public void ExecNonQuery(string DbfQuery)
        {
            string strConnect = cnn.Odbc_CnnString; 

            OdbcConnection con = new OdbcConnection(strConnect);

            OdbcCommand cmd = new OdbcCommand();

            foreach (OdbcParameter param in oParameterCollection)
            {
                cmd.Parameters.Add(param);
            }

            cmd.Connection = con;

            try
            {
                con.Open();
                cmd.CommandText = DbfQuery;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                con.Close();
            }
        }

        public DataSet ExecQuery(string strQuery, string strTableName)
        {
            string strConnect = cnn.Odbc_CnnString;

            OdbcConnection con = new OdbcConnection(strConnect);

            OdbcCommand cmd = new OdbcCommand();
            OdbcDataAdapter myAdapter = new OdbcDataAdapter();

            DataSet dsreturn = new DataSet();

            foreach (OdbcParameter param in oParameterCollection)
            {
                cmd.Parameters.Add(param);
            }
            cmd.CommandText = strQuery;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;

            try
            {
                con.Open();
                myAdapter.SelectCommand = cmd;
                myAdapter.Fill(dsreturn, strTableName);

                return dsreturn;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                con.Close();
            }
        }
        #endregion

        #region  Parameters

        List<OdbcParameter> oParameterCollection = new List<OdbcParameter>();

        /// <summary>
        /// Removes all the System.Data.OdbcClient.OdbcParameter objects from the System.Data.OdbcClient.OdbcParameterCollection.
        /// </summary>
        public void ClearParameters()
        {
            oParameterCollection.Clear();
        }

        /// <summary>
        /// Removes the specified System.Data.OdbcClient.OdbcParameter from the collection.
        /// </summary>
        /// <param name="parameterName">The name of the parameter to retrieve. </param>
        public void RemoveParameters(string parameterName)
        {
            foreach (OdbcParameter param in oParameterCollection)
            {
                if (parameterName == param.ParameterName)
                {
                    oParameterCollection.Remove(param);
                    return;
                }
            }
        }

        public void AddParameter(string strName, OdbcType oOdbcType, int iSize, object oValue, ParameterDirection oParamDirection)
        {
            OdbcParameter oOdbcParameter = new OdbcParameter(strName, oOdbcType, iSize);

            try
            {
                oOdbcParameter.Value = oValue;
                oOdbcParameter.Direction = oParamDirection;
                oParameterCollection.Add(oOdbcParameter);
            }
            catch (OdbcException OdbcEx)
            {
                throw new Exception("OdbcException:- [" + strName + "]" + OdbcEx.ErrorCode + ":" + OdbcEx.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception:- [" + strName + "]" + ex.Message);
            }
            finally
            {
                oOdbcParameter = null;
            }
        }

        public object GetParameterValue(string parameterName)
        {
            foreach (OdbcParameter param in oParameterCollection)
            {
                if (parameterName == param.ParameterName)
                {
                    return param.Value;
                }
            }
            return null;
        }

        /// <summary>
        /// Add a new instance of the System.Data.OdbcClient.OdbcParameter class into active OdbcCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOdbcType"> One of the System.Data.OdbcType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.String that is the value of the System.Data.OdbcClient.OdbcParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OdbcType oOdbcType, int iSize, string oValue)
        {
            AddParameter(strName, oOdbcType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OdbcClient.OdbcParameter class into active OdbcCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOdbcType"> One of the System.Data.OdbcType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An Array of System.Byte that is the value of the System.Data.OdbcClient.OdbcParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OdbcType oOdbcType, int iSize, byte[] oValue)
        {
            AddParameter(strName, oOdbcType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OdbcClient.OdbcParameter class into active OdbcCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOdbcType"> One of the System.Data.OdbcType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Boolean that is the value of the System.Data.OdbcClient.OdbcParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OdbcType oOdbcType, int iSize, bool oValue)
        {
            AddParameter(strName, oOdbcType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OdbcClient.OdbcParameter class into active OdbcCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOdbcType"> One of the System.Data.OdbcType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.DBNull that is the value of the System.Data.OdbcClient.OdbcParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OdbcType oOdbcType, int iSize, DBNull oValue)
        {
            AddParameter(strName, oOdbcType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OdbcClient.OdbcParameter class into active OdbcCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOdbcType"> One of the System.Data.OdbcType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Image that is the value of the System.Data.OdbcClient.OdbcParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OdbcType oOdbcType, int iSize, Image oValue)
        {
            AddParameter(strName, oOdbcType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OdbcClient.OdbcParameter class into active OdbcCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOdbcType"> One of the System.Data.OdbcType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Guid that is the value of the System.Data.OdbcClient.OdbcParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OdbcType oOdbcType, int iSize, Guid oValue)
        {
            AddParameter(strName, oOdbcType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OdbcClient.OdbcParameter class into active OdbcCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOdbcType"> One of the System.Data.OdbcType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Integer that is the value of the System.Data.OdbcClient.OdbcParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OdbcType oOdbcType, int iSize, int oValue)
        {
            AddParameter(strName, oOdbcType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OdbcClient.OdbcParameter class into active OdbcCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOdbcType"> One of the System.Data.OdbcType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Double that is the value of the System.Data.OdbcClient.OdbcParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OdbcType oOdbcType, int iSize, double oValue)
        {
            AddParameter(strName, oOdbcType, iSize, oValue, ParameterDirection.Input);
        }
        #endregion
    }
}
