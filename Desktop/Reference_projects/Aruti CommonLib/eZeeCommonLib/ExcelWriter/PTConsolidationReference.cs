﻿namespace ExcelWriter
{
    using System;
    using System.Xml;

    public sealed class PTConsolidationReference : IWriter
    {
        private string _fileName;
        private string _reference;

        internal PTConsolidationReference()
        {
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "ConsolidationReference", "urn:schemas-microsoft-com:office:excel");
            if (this._fileName != null)
            {
                writer.WriteElementString("FileName", "urn:schemas-microsoft-com:office:excel", this._fileName);
            }
            if (this._reference != null)
            {
                writer.WriteElementString("Reference", "urn:schemas-microsoft-com:office:excel", this._reference);
            }
            writer.WriteEndElement();
        }

        public string FileName
        {
            get
            {
                return this._fileName;
            }
            set
            {
                this._fileName = value;
            }
        }

        public string Reference
        {
            get
            {
                return this._reference;
            }
            set
            {
                this._reference = value;
            }
        }
    }
}

