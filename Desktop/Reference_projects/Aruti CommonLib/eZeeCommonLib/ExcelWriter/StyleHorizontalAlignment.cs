﻿namespace ExcelWriter
{
    using System;

    public enum StyleHorizontalAlignment
    {
        Automatic,
        Left,
        Center,
        Right,
        Fill,
        Justify,
        CenterAcrossSelection,
        Distributed,
        JustifyDistributed
    }
}

