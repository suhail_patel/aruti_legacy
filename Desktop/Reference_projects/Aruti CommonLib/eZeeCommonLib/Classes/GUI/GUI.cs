using System.Drawing;
public class GUI
{
	private static Color mclrFormColor = Color.FromArgb(240, 248, 255);
	private static Color mclrComp = System.Drawing.Color.FromArgb(218, 225, 235);
	private static Color mclrOpt = System.Drawing.Color.FromArgb(231, 235, 231);
	private static Color mclrDisplay = System.Drawing.Color.FromArgb(249, 248, 231);

	private static Color mclrHeaderBackColor1 = SystemColors.Control;
	private static Color mclrHeaderBackColor2 = SystemColors.ControlLightLight;
	private static Color mclrHeaderTitleFontColor = SystemColors.WindowText;
	private static Color mclrHeaderMessageFontColor = SystemColors.ControlDarkDark;
	private static Color mclrHeaderBorderColor = SystemColors.ControlDarkDark;

	private static Color mclrButttonBackColor = SystemColors.ActiveBorder;
	private static Color mclrButttonFontColor = SystemColors.WindowText;
	private static Color mclrButttonBorderColor = SystemColors.ControlDarkDark;

	private static Color mclreZeeContainerHeaderBackColor = SystemColors.ButtonFace;
	private static Color mclreZeeContainerHeaderForeColor = SystemColors.ControlDarkDark;

	private static Color mclrFooterBackColor1 = SystemColors.Control;
	private static Color mclrFooterBackColor2 = SystemColors.Control;
	private static Color mclrFooterBorderColor = SystemColors.ControlDarkDark;

	private static Color mclrLineColor = SystemColors.Desktop;

	public static string DecimalSeparator
	{
		get
		{
			return modGlobal.gDecimalSeparator;
		}
	}

	public static string fmtCurrency
	{
		get
		{
            return modGlobal.gfmtCurrency;
		}
		set
		{
            modGlobal.gfmtCurrency = value;
		}
	}

	//Naimish (17 Feb 2009) -- Start
	public static string fmtPQ_DecimalPoint
	{
		get
		{
            return modGlobal.gfmtPQ_DecimalPoint;
		}
		set
		{
            modGlobal.gfmtPQ_DecimalPoint = value;
		}
	}

	public static string fmtSQ_DecimalPoint
	{
		get
		{
            return modGlobal.gfmtSQ_DecimalPoint;
		}
		set
		{
            modGlobal.gfmtSQ_DecimalPoint = value;
		}
	}
	//Naimish (17 Feb 2009) -- End

	public static string fmtDate
	{
		get
		{
            return modGlobal.gfmtDate;
		}
	}

    public static string fmtTime
    {
        get
        {
            return modGlobal.gfmtTime;
        }
    }

    public static string fmtTimeDisplay
    {
        get
        {
            return modGlobal.gfmtTimeDisplay;
        }
    }
    
	public static Image LanguageImage
	{
		get
		{
			return eZeeCommonLib.Properties.Resources.font_txt;
		}
	}

	public static Icon eZeeIcon
	{
		get
		{
			return eZeeCommonLib.Properties.Resources.eZeeIcon;
		}
	}

	public static Color FormColor
	{
		get
		{
            return mclrFormColor;
		}
		set
		{
            mclrFormColor = value;
		}
	}

	public static Color ColorComp
	{
		get
		{
            return mclrComp;
		}
		set
		{
            mclrComp = value;
		}
	}

	public static Color ColorDisplay
	{
		get
		{
			return mclrDisplay;
		}
		set
		{
			mclrDisplay = value;
		}
	}

	public static Color ColorOptional
	{
		get
		{
			return mclrOpt;
		}
		set
		{
			mclrOpt = value;
		}
	}

	public static Color _HeaderBackColor1
	{
		get
		{
			return mclrHeaderBackColor1;
		}
		set
		{
			mclrHeaderBackColor1 = value;
		}
	}

	public static Color _HeaderBackColor2
	{
		get
		{
			return mclrHeaderBackColor2;
		}
		set
		{
			mclrHeaderBackColor2 = value;
		}
	}

	public static Color _HeaderBorderColor
	{
		get
		{
			return mclrHeaderBorderColor;
		}
		set
		{
			mclrHeaderBorderColor = value;
		}
	}

	public static Color _HeaderTitleFontColor
	{
		get
		{
			return mclrHeaderTitleFontColor;
		}
		set
		{
			mclrHeaderTitleFontColor = value;
		}
	}

	public static Color _HeaderMessageFontColor
	{
		get
		{
			return mclrHeaderMessageFontColor;
		}
		set
		{
			mclrHeaderMessageFontColor = value;
		}
	}

	public static Color _ButttonBackColor
	{
		get
		{
			return mclrButttonBackColor;
		}
		set
		{
			mclrButttonBackColor = value;
		}
	}

	public static Color _ButttonFontColor
	{
		get
		{
			return mclrButttonFontColor;
		}
		set
		{
			mclrButttonFontColor = value;
		}
	}

	public static Color _ButttonBorderColor
	{
		get
		{
			return mclrButttonBorderColor;
		}
		set
		{
			mclrButttonBorderColor = value;
		}
	}

	public static Color _eZeeContainerHeaderBackColor
	{
		get
		{
			return mclreZeeContainerHeaderBackColor;
		}
		set
		{
			mclreZeeContainerHeaderBackColor = value;
		}
	}

	public static Color _eZeeContainerHeaderForeColor
	{
		get
		{
			return mclreZeeContainerHeaderForeColor;
		}
		set
		{
			mclreZeeContainerHeaderForeColor = value;
		}
	}

	public static Color _FooterBackColor1
	{
		get
		{
			return mclrFooterBackColor1;
		}
		set
		{
			mclrFooterBackColor1 = value;
		}
	}

	public static Color _FooterBackColor2
	{
		get
		{
			return mclrFooterBackColor2;
		}
		set
		{
			mclrFooterBackColor2 = value;
		}
	}

	public static Color _FooterBorderColor
	{
		get
		{
			return mclrFooterBorderColor;
		}
		set
		{
			mclrFooterBorderColor = value;
		}
	}

	public static Color _LineColor
	{
		get
		{
			return mclrLineColor;
		}
		set
		{
			mclrLineColor = value;
		}
	}
}
