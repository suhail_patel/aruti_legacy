using System.Windows.Forms;
internal static class modGlobal
{
	public static System.Data.SqlClient.SqlConnection gConn;
    public static bool gblnIsReConnect = true;
	public static System.Data.SqlClient.SqlTransaction gsqlTransaction;
	public static bool gblnBindTransaction = false;

	/// <summary>
	/// Regional Currency Format.
	/// </summary>
	internal static string gfmtCurrency;

	//Naimish (17 Feb 2009) -- Start
	/// <summary>
	/// Regional Currency Format.
	/// </summary>
	internal static string gfmtPQ_DecimalPoint;

	//Naimish (17 Feb 2009) -- Start
	/// <summary>
	/// Regional Currency Format.
	/// </summary>
	internal static string gfmtSQ_DecimalPoint;

	//Naimish (17 Feb 2009) -- End
	/// <summary>
	/// Regional Date Format.
	/// </summary>
	internal static string gfmtDate;


    /// <summary>
	/// Regional Time Format.
	/// </summary>
    internal static string gfmtTime;
    internal static string gfmtTimeDisplay;
    
	/// <summary>
	/// Regional Decimal Separator Format.
	/// </summary>
	public static string gDecimalSeparator;

	/// <summary>
	/// Set Regional and Language Setting.
	/// </summary>
	internal static void createFormatString()
	{
        gConn.StateChange += new System.Data.StateChangeEventHandler(gConn_StateChange);

		string strDecimalSeparator = null;
		System.Globalization.CultureInfo culture = null;

		try
		{
			culture = System.Globalization.CultureInfo.CurrentCulture;

			strDecimalSeparator = culture.NumberFormat.CurrencyDecimalSeparator;
			gDecimalSeparator = strDecimalSeparator;

			gfmtCurrency = "#########0.00";
			//gfmtCurrency = "#,##,##,##,##0.00"
			gfmtDate = culture.DateTimeFormat.ShortDatePattern;
            gfmtTime = "HH:mm:ss";
            gfmtTimeDisplay = culture.DateTimeFormat.LongTimePattern;
            
		}
		catch (System.Exception ex)
		{
            //System.Windows.Forms.MessageBox.Show(ex.Message + "[createFormatString-modGlobal]");
            string S_dispmsg = ex.Message + "; " + ex.StackTrace.ToString();
            if (ex.InnerException != null)
            {
                S_dispmsg += "; " + ex.InnerException.Message;
            }
            S_dispmsg = S_dispmsg.Replace("'", "");
            S_dispmsg = S_dispmsg.Replace(System.Environment.NewLine, "");
            throw new System.Exception(S_dispmsg + "[createFormatString-modGloba]");
		}
		finally
		{
			culture = null;
		}
	}

   static void gConn_StateChange(object sender, System.Data.StateChangeEventArgs e)
    {
        if (gblnIsReConnect) return;
        try
        {
            if (e.OriginalState == System.Data.ConnectionState.Open && e.CurrentState == System.Data.ConnectionState.Closed)
            {
                gConn.Open();
                eZeeCommonLib.eZeeDatabase.change_database("");
                return;
            }
            else if (e.OriginalState == System.Data.ConnectionState.Closed && 
                    e.CurrentState == System.Data.ConnectionState.Open &&
                    gConn.State == System.Data.ConnectionState.Closed)
            {
                gConn.Open();
                eZeeCommonLib.eZeeDatabase.change_database("");
                return;
            }
        }
        catch (System.Exception ex)
        {
            throw ex;
        }
    }


    public static void ctlRightToLeftlayOut(System.Windows.Forms.Control ctlParents)
    {
        int i = 0;

        if (ctlParents.Dock == DockStyle.Right)
        {
            ctlParents.Dock = DockStyle.Left;
        }
        else if (ctlParents.Dock == DockStyle.Left)
        {
            ctlParents.Dock = DockStyle.Right;
        }

        if (ctlParents is Form)
        {
            for (i = 0; i < ctlParents.Controls.Count; i++)
            {
                if (ctlParents.Controls[i] is Panel | ctlParents.Controls[i] is GroupBox | ctlParents.Controls[i] is TabControl | ctlParents.Controls[i] is UserControl)
                {
                    ctlRightToLeftlayOut(ctlParents.Controls[i]);
                }
            }
        }
        else if (ctlParents is TabControl)
        {
            TabControl tbc = null;
            tbc = (TabControl)ctlParents;
            tbc.RightToLeft = RightToLeft.Yes;
            tbc.RightToLeftLayout = true;
            foreach (TabPage ctl in tbc.TabPages)
            {
                ctlRightToLeftlayOut(ctl);
            }
        }
        else if (ctlParents is Panel | ctlParents is GroupBox | ctlParents is TabPage | ctlParents is UserControl)
        {
            for (i = 0; i < ctlParents.Controls.Count; i++)
            {
                if (ctlParents.Controls[i] is ListView)
                {
                    ListView lst = null;
                    lst = (ListView)(ctlParents.Controls[i]);
                    lst.RightToLeft = RightToLeft.Yes;
                    lst.RightToLeftLayout = true;
                }
                if (ctlParents.Controls[i] is TreeView)
                {
                    TreeView tv = null;
                    tv = (TreeView)(ctlParents.Controls[i]);
                    tv.RightToLeft = RightToLeft.Yes;
                    tv.RightToLeftLayout = true;
                }
                ctlParents.Controls[i].Left = ctlParents.Width - (ctlParents.Controls[i].Left + ctlParents.Controls[i].Width);
                if (ctlParents.Controls[i] is Panel | ctlParents.Controls[i] is GroupBox | ctlParents.Controls[i] is TabPage | ctlParents.Controls[i] is TabControl | ctlParents.Controls[i] is UserControl)
                {
                    ctlRightToLeftlayOut(ctlParents.Controls[i]);
                }
            }
        }
    }
}
