/// <summary>
/// Indicate a process action( Add or Edit)
/// </summary>
public enum enAction: int
{
    ADD_ONE = 0,
    EDIT_ONE = 1,
    ADD_CONTINUE = 2
}

/// <summary>
/// Indicate a Application type for DataOpration project.
/// </summary>
public enum enApplication: int
{
    ALL = -1,
    eZeeFDConfig = 0,
    eZeeFD = 1,
    eZeePOSConfigartion = 2,
    eZeePOS = 3,
    eZeePOSPDA = 4
}
