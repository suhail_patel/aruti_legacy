using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

internal enum TaskPage: int
{
	TASK = 0,
	SCHEDULE = 1,
	SETTINGS = 2
}

internal enum ErrorCode: uint
{
	TRIGGER_NOT_FOUND = 0X80041309,
	TASK_NOT_READY = 0X8004130A,
	TASK_NOT_RUNNING = 0X8004130B,
	SERVICE_NOT_INSTALLED = 0X8004130C,
	CANNOT_OPEN_TASK = 0X8004130D,
	INVALID_TASK = 0X8004130E,
	ACCOUNT_INFORMATION_NOT_SET = 0X8004130F,
	ACCOUNT_NAME_NOT_FOUND = 0X80041310,
	ACCOUNT_DBASE_CORRUPT = 0X80041311,
	NO_SECURITY_SERVICES = 0X80041312,
	UNKNOWN_OBJECT_VERSION = 0X80041313,
	UNSUPPORTED_ACCOUNT_OPTION = 0X80041314,
	SERVICE_NOT_RUNNING = 0X80041315
}
