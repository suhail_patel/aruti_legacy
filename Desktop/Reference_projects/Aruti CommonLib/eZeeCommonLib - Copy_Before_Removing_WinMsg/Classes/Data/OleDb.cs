using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Drawing;

namespace eZee.Data.Manager
{
    public class OleDb
    {

        internal Connection cnn;

        public OleDb(Connection Connection)
        {
            cnn = Connection;
        }

        #region Execute
        public void ExecNonQuery(string DbfQuery)
        {
            string strConnect = cnn.Oledb_CnnString;

            OleDbConnection con = new OleDbConnection(strConnect);

            OleDbCommand cmd = new OleDbCommand();

            foreach (OleDbParameter param in oParameterCollection)
            {
                cmd.Parameters.Add(param);
            }

            cmd.Connection = con;

            try
            {
                con.Open();
                cmd.CommandText = DbfQuery;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                con.Close();
            }
        }

        public DataSet ExecQuery(string strQuery, string strTableName)
        {
            string strConnect = cnn.Oledb_CnnString;

            OleDbConnection con = new OleDbConnection(strConnect);
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet dsreturn = new DataSet();

            foreach (OleDbParameter param in oParameterCollection)
            {
                cmd.Parameters.Add(param);
            }
            cmd.CommandText = strQuery;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;

            try
            {
                con.Open();
                myAdapter.SelectCommand = cmd;
                cmd.ExecuteNonQuery();

                myAdapter.Fill(dsreturn, strTableName);

                return dsreturn;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                con.Close();
            }
        }
        #endregion

        #region  Parameters

        List<OleDbParameter> oParameterCollection = new List<OleDbParameter>();

        /// <summary>
        /// Removes all the System.Data.OleDbClient.OleDbParameter objects from the System.Data.OleDbClient.OleDbParameterCollection.
        /// </summary>
        public void ClearParameters()
        {
            oParameterCollection.Clear();
        }

        /// <summary>
        /// Removes the specified System.Data.OleDbClient.OleDbParameter from the collection.
        /// </summary>
        /// <param name="parameterName">The name of the parameter to retrieve. </param>
        public void RemoveParameters(string parameterName)
        {
            foreach (OleDbParameter param in oParameterCollection)
            {
                if (parameterName == param.ParameterName)
                {
                    oParameterCollection.Remove(param);
                    return;
                }
            }
        }

        public void AddParameter(string strName, OleDbType oOleDbType, int iSize, object oValue, ParameterDirection oParamDirection)
        {
            OleDbParameter oOleDbParameter = new OleDbParameter(strName, oOleDbType, iSize);

            try
            {
                oOleDbParameter.Value = oValue;
                oOleDbParameter.Direction = oParamDirection;
                oParameterCollection.Add(oOleDbParameter);
            }
            catch (OleDbException OleDbEx)
            {
                throw new Exception("OleDbException:- [" + strName + "]" + OleDbEx.ErrorCode + ":" + OleDbEx.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception:- [" + strName + "]" + ex.Message);
            }
            finally
            {
                oOleDbParameter = null;
            }
        }

        public object GetParameterValue(string parameterName)
        {
            foreach (OleDbParameter param in oParameterCollection)
            {
                if (parameterName == param.ParameterName)
                {
                    return param.Value;
                }
            }
            return null;
        }

        /// <summary>
        /// Add a new instance of the System.Data.OleDbClient.OleDbParameter class into active OleDbCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOleDbType"> One of the System.Data.OleDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.String that is the value of the System.Data.OleDbClient.OleDbParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OleDbType oOleDbType, int iSize, string oValue)
        {
            AddParameter(strName, oOleDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OleDbClient.OleDbParameter class into active OleDbCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOleDbType"> One of the System.Data.OleDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An Array of System.Byte that is the value of the System.Data.OleDbClient.OleDbParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OleDbType oOleDbType, int iSize, byte[] oValue)
        {
            AddParameter(strName, oOleDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OleDbClient.OleDbParameter class into active OleDbCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOleDbType"> One of the System.Data.OleDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Boolean that is the value of the System.Data.OleDbClient.OleDbParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OleDbType oOleDbType, int iSize, bool oValue)
        {
            AddParameter(strName, oOleDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OleDbClient.OleDbParameter class into active OleDbCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOleDbType"> One of the System.Data.OleDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.DBNull that is the value of the System.Data.OleDbClient.OleDbParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OleDbType oOleDbType, int iSize, DBNull oValue)
        {
            AddParameter(strName, oOleDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OleDbClient.OleDbParameter class into active OleDbCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOleDbType"> One of the System.Data.OleDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Image that is the value of the System.Data.OleDbClient.OleDbParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OleDbType oOleDbType, int iSize, Image oValue)
        {
            AddParameter(strName, oOleDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OleDbClient.OleDbParameter class into active OleDbCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOleDbType"> One of the System.Data.OleDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Guid that is the value of the System.Data.OleDbClient.OleDbParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OleDbType oOleDbType, int iSize, Guid oValue)
        {
            AddParameter(strName, oOleDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OleDbClient.OleDbParameter class into active OleDbCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOleDbType"> One of the System.Data.OleDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Integer that is the value of the System.Data.OleDbClient.OleDbParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OleDbType oOleDbType, int iSize, int oValue)
        {
            AddParameter(strName, oOleDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.OleDbClient.OleDbParameter class into active OleDbCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oOleDbType"> One of the System.Data.OleDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Double that is the value of the System.Data.OleDbClient.OleDbParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, OleDbType oOleDbType, int iSize, double oValue)
        {
            AddParameter(strName, oOleDbType, iSize, oValue, ParameterDirection.Input);
        }
        #endregion
    }
}
