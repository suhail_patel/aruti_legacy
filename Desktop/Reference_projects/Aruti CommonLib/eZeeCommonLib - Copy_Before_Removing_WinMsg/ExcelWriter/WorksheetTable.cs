﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.ComponentModel;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetTable : IWriter, IReader, ICodeWriter
    {
        private WorksheetColumnCollection _columns;
        private float _defaultColumnWidth = 48f;
        private float _defaultRowHeight = 12.75f;
        private int _expandedColumnCount = -2147483648;
        private int _expandedRowCount = -2147483648;
        private int _fullColumns = -2147483648;
        private int _fullRows = -2147483648;
        private WorksheetRowCollection _rows;
        private string _styleID;

        internal WorksheetTable()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._defaultRowHeight != 12.75f)
            {
                Util.AddAssignment(method, targetObject, "DefaultRowHeight", this._defaultRowHeight);
            }
            if (this._defaultColumnWidth != 48f)
            {
                Util.AddAssignment(method, targetObject, "DefaultColumnWidth", this._defaultColumnWidth);
            }
            if (this._expandedColumnCount != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "ExpandedColumnCount", this._expandedColumnCount);
            }
            if (this._expandedRowCount != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "ExpandedRowCount", this._expandedRowCount);
            }
            if (this._fullColumns != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "FullColumns", this._fullColumns);
            }
            if (this._fullRows != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "FullRows", this._fullRows);
            }
            if (this._styleID != null)
            {
                Util.AddAssignment(method, targetObject, "StyleID", this._styleID);
            }
            if (this._columns != null)
            {
                Util.Traverse(type, this._columns, method, targetObject, "Columns");
            }
            if (this._rows != null)
            {
                Util.Traverse(type, this._rows, method, targetObject, "Rows");
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._defaultRowHeight = Util.GetAttribute(element, "DefaultRowHeight", "urn:schemas-microsoft-com:office:spreadsheet", (float) 12.75f);
            this._defaultColumnWidth = Util.GetAttribute(element, "DefaultColumnWidth", "urn:schemas-microsoft-com:office:spreadsheet", (float) 48f);
            this._expandedColumnCount = Util.GetAttribute(element, "ExpandedColumnCount", "urn:schemas-microsoft-com:office:spreadsheet", -2147483648);
            this._expandedRowCount = Util.GetAttribute(element, "ExpandedRowCount", "urn:schemas-microsoft-com:office:spreadsheet", -2147483648);
            this._fullColumns = Util.GetAttribute(element, "FullColumns", "urn:schemas-microsoft-com:office:excel", -2147483648);
            this._fullRows = Util.GetAttribute(element, "FullRows", "urn:schemas-microsoft-com:office:excel", -2147483648);
            this._fullRows = Util.GetAttribute(element, "FullRows", "urn:schemas-microsoft-com:office:excel", -2147483648);
            this._styleID = Util.GetAttribute(element, "StyleID", "urn:schemas-microsoft-com:office:spreadsheet");
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (WorksheetColumn.IsElement(element2))
                    {
                        WorksheetColumn column = new WorksheetColumn();
                        ((IReader) column).ReadXml(element2);
                        this.Columns.Add(column);
                    }
                    else if (WorksheetRow.IsElement(element2))
                    {
                        WorksheetRow row = new WorksheetRow();
                        ((IReader) row).ReadXml(element2);
                        this.Rows.Add(row);
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Table", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._defaultRowHeight != 12.75f)
            {
                writer.WriteAttributeString("s", "DefaultRowHeight", "urn:schemas-microsoft-com:office:spreadsheet", this._defaultRowHeight.ToString());
            }
            if (this._defaultColumnWidth != 48f)
            {
                writer.WriteAttributeString("s", "DefaultColumnWidth", "urn:schemas-microsoft-com:office:spreadsheet", this._defaultColumnWidth.ToString());
            }
            if (this._expandedColumnCount != -2147483648)
            {
                writer.WriteAttributeString("s", "ExpandedColumnCount", "urn:schemas-microsoft-com:office:spreadsheet", this._expandedColumnCount.ToString());
            }
            if (this._expandedRowCount != -2147483648)
            {
                writer.WriteAttributeString("s", "ExpandedRowCount", "urn:schemas-microsoft-com:office:spreadsheet", this._expandedRowCount.ToString());
            }
            if (this._fullColumns != -2147483648)
            {
                writer.WriteAttributeString("x", "FullColumns", "urn:schemas-microsoft-com:office:excel", this._fullColumns.ToString());
            }
            if (this._fullRows != -2147483648)
            {
                writer.WriteAttributeString("x", "FullRows", "urn:schemas-microsoft-com:office:excel", this._fullRows.ToString());
            }
            if (this._styleID != null)
            {
                writer.WriteAttributeString("s", "StyleID", "urn:schemas-microsoft-com:office:spreadsheet", this._styleID);
            }
            if (this._columns != null)
            {
                ((IWriter) this._columns).WriteXml(writer);
            }
            if (this._rows != null)
            {
                ((IWriter) this._rows).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Table", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public WorksheetColumnCollection Columns
        {
            get
            {
                if (this._columns == null)
                {
                    this._columns = new WorksheetColumnCollection(this);
                }
                return this._columns;
            }
        }

        public float DefaultColumnWidth
        {
            get
            {
                return this._defaultColumnWidth;
            }
            set
            {
                this._defaultColumnWidth = value;
            }
        }

        public float DefaultRowHeight
        {
            get
            {
                return this._defaultRowHeight;
            }
            set
            {
                this._defaultRowHeight = value;
            }
        }

        public int ExpandedColumnCount
        {
            get
            {
                return this._expandedColumnCount;
            }
            set
            {
                this._expandedColumnCount = value;
            }
        }

        public int ExpandedRowCount
        {
            get
            {
                return this._expandedRowCount;
            }
            set
            {
                this._expandedRowCount = value;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public int FullColumns
        {
            get
            {
                return this._fullColumns;
            }
            set
            {
                this._fullColumns = value;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public int FullRows
        {
            get
            {
                return this._fullRows;
            }
            set
            {
                this._fullRows = value;
            }
        }

        public WorksheetRowCollection Rows
        {
            get
            {
                if (this._rows == null)
                {
                    this._rows = new WorksheetRowCollection(this);
                }
                return this._rows;
            }
        }

        public string StyleID
        {
            get
            {
                return this._styleID;
            }
            set
            {
                this._styleID = value;
            }
        }
    }
}

