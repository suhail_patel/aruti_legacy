﻿namespace ExcelWriter
{
    using System;

    public enum PivotFieldOrientation
    {
        NotSet,
        Row,
        Data
    }
}

