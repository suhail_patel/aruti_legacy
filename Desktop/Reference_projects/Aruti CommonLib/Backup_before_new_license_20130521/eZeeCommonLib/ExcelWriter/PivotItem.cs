﻿namespace ExcelWriter
{
    using System;
    using System.Xml;

    public sealed class PivotItem : IWriter
    {
        private bool _hideDetail;
        private string _name;

        public PivotItem()
        {
        }

        public PivotItem(string name)
        {
            this._name = name;
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "PivotItem", "urn:schemas-microsoft-com:office:excel");
            if (this._name != null)
            {
                writer.WriteElementString("Name", "urn:schemas-microsoft-com:office:excel", this._name);
            }
            if (this._hideDetail)
            {
                writer.WriteElementString("HideDetail", "urn:schemas-microsoft-com:office:excel", "");
            }
            writer.WriteEndElement();
        }

        public bool HideDetail
        {
            get
            {
                return this._hideDetail;
            }
            set
            {
                this._hideDetail = value;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }
    }
}

