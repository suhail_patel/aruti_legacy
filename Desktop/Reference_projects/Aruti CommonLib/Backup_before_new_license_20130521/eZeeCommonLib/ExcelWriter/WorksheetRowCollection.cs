﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections;
    using System.Globalization;
    using System.Reflection;
    using System.Xml;

    public sealed class WorksheetRowCollection : CollectionBase, IWriter, ICodeWriter
    {
        private WorksheetTable _table;

        internal WorksheetRowCollection()
        {
        }

        internal WorksheetRowCollection(WorksheetTable table)
        {
            if (table == null)
            {
                throw new ArgumentNullException("table");
            }
            this._table = table;
        }

        public WorksheetRow Add()
        {
            WorksheetRow row = new WorksheetRow();
            this.Add(row);
            return row;
        }

        public int Add(WorksheetRow row)
        {
            if (row == null)
            {
                throw new ArgumentNullException("row");
            }
            row._table = this._table;
            return base.InnerList.Add(row);
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                Util.AddCommentSeparator(method);
                WorksheetRow row = this[i];
                string name = "Row" + i.ToString();
                CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(WorksheetRow), name, new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[0]));
                method.Statements.Add(statement);
                ((ICodeWriter) row).WriteTo(type, method, new CodeVariableReferenceExpression(name));
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(WorksheetRow item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(WorksheetRow[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(WorksheetRow item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, WorksheetRow row)
        {
            if (row == null)
            {
                throw new ArgumentNullException("row");
            }
            row._table = this._table;
            base.InnerList.Insert(index, row);
        }

        public void Remove(WorksheetRow row)
        {
            row._table = null;
            base.InnerList.Remove(row);
        }

        public WorksheetRow this[int index]
        {
            get
            {
                return (WorksheetRow) base.InnerList[index];
            }
        }
    }
}

