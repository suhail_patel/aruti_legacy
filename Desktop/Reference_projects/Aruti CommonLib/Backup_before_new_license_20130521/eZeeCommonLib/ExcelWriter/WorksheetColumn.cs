﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetColumn : IWriter, IReader, ICodeWriter
    {
        private bool _autoFitWidth;
        private bool _hidden;
        private int _index;
        private int _span;
        private string _styleID;
        internal WorksheetTable _table;
        private int _width;

        public WorksheetColumn()
        {
            this._width = -2147483648;
            this._span = -2147483648;
        }

        public WorksheetColumn(int width)
        {
            this._width = -2147483648;
            this._span = -2147483648;
            this._width = width;
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._index != 0)
            {
                Util.AddAssignment(method, targetObject, "Index", this._index);
            }
            if (this._width != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "Width", this._width);
            }
            if (this._hidden)
            {
                Util.AddAssignment(method, targetObject, "Hidden", this._hidden);
            }
            if (this._autoFitWidth)
            {
                Util.AddAssignment(method, targetObject, "AutoFitWidth", this._autoFitWidth);
            }
            if (this._styleID != null)
            {
                Util.AddAssignment(method, targetObject, "StyleID", this._styleID);
            }
            if (this._span != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "Span", this._span);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._index = Util.GetAttribute(element, "Index", "urn:schemas-microsoft-com:office:spreadsheet", 0);
            this._span = Util.GetAttribute(element, "Span", "urn:schemas-microsoft-com:office:spreadsheet", -2147483648);
            this._width = Util.GetAttribute(element, "Width", "urn:schemas-microsoft-com:office:spreadsheet", -2147483648);
            this._hidden = Util.GetAttribute(element, "Hidden", "urn:schemas-microsoft-com:office:spreadsheet", false);
            this._autoFitWidth = Util.GetAttribute(element, "AutoFitWidth", "urn:schemas-microsoft-com:office:spreadsheet", false);
            this._styleID = Util.GetAttribute(element, "StyleID", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Column", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._index != 0)
            {
                writer.WriteAttributeString("s", "Index", "urn:schemas-microsoft-com:office:spreadsheet", this._index.ToString());
            }
            if (this._width != -2147483648)
            {
                writer.WriteAttributeString("s", "Width", "urn:schemas-microsoft-com:office:spreadsheet", this._width.ToString());
            }
            if (this._hidden)
            {
                writer.WriteAttributeString("s", "Hidden", "urn:schemas-microsoft-com:office:spreadsheet", "1");
            }
            if (this._autoFitWidth)
            {
                writer.WriteAttributeString("s", "AutoFitWidth", "urn:schemas-microsoft-com:office:spreadsheet", "1");
            }
            if (this._styleID != null)
            {
                writer.WriteAttributeString("s", "StyleID", "urn:schemas-microsoft-com:office:spreadsheet", this._styleID);
            }
            if (this._span != -2147483648)
            {
                writer.WriteAttributeString("s", "Span", "urn:schemas-microsoft-com:office:spreadsheet", this._span.ToString());
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Column", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public bool AutoFitWidth
        {
            get
            {
                return this._autoFitWidth;
            }
            set
            {
                this._autoFitWidth = value;
            }
        }

        public bool Hidden
        {
            get
            {
                return this._hidden;
            }
            set
            {
                this._hidden = value;
            }
        }

        public int Index
        {
            get
            {
                return this._index;
            }
            set
            {
                this._index = value;
            }
        }

        internal bool IsSimple
        {
            get
            {
                return ((((this._width != -2147483648) && (this._index == 0)) && (!this._hidden && (this._styleID == null))) && (this._span == -2147483648));
            }
        }

        public int Span
        {
            get
            {
                if (this._span == -2147483648)
                {
                    return 0;
                }
                return this._span;
            }
            set
            {
                this._span = value;
            }
        }

        public string StyleID
        {
            get
            {
                return this._styleID;
            }
            set
            {
                this._styleID = value;
            }
        }

        public WorksheetTable Table
        {
            get
            {
                return this._table;
            }
        }

        public int Width
        {
            get
            {
                if ((this._width == -2147483648) && (this._table != null))
                {
                    return (int) this._table.DefaultColumnWidth;
                }
                return this._width;
            }
            set
            {
                this._width = value;
            }
        }
    }
}

