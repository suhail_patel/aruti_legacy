﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetCellData : IWriter, IReader, ICodeWriter
    {
        private IReader _parent;
        private string _text;
        private DataType _type = DataType.String;

        internal WorksheetCellData(IReader parent)
        {
            this._parent = parent;
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if ((this._type != DataType.NotSet) && !(this._parent is WorksheetComment))
            {
                Util.AddAssignment(method, targetObject, "Type", this._type);
            }
            if (this._text != null)
            {
                Util.AddAssignment(method, targetObject, "Text", this._text);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            string attribute = element.GetAttribute("Type", "urn:schemas-microsoft-com:office:spreadsheet");
            if ((attribute != null) && (attribute.Length > 0))
            {
                this._type = (DataType) Enum.Parse(typeof(DataType), attribute);
            }
            if (!element.IsEmpty)
            {
                this._text = element.InnerText;
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Data", "urn:schemas-microsoft-com:office:spreadsheet");
            if ((this._type != DataType.NotSet) && !(this._parent is WorksheetComment))
            {
                writer.WriteAttributeString("s", "Type", "urn:schemas-microsoft-com:office:spreadsheet", this._type.ToString());
            }
            writer.WriteString(this._text);
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Data", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        internal bool IsSimple
        {
            get
            {
                return ((this._type != DataType.NotSet) && (this._text != null));
            }
        }

        public string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
            }
        }

        public DataType Type
        {
            get
            {
                return this._type;
            }
            set
            {
                this._type = value;
            }
        }
    }
}

