﻿namespace ExcelWriter
{
    using System;
    using System.Xml;

    public sealed class WorksheetNamedRange : IWriter, IReader
    {
        private bool _hidden;
        private string _name;
        private string _refersTo;

        internal WorksheetNamedRange()
        {
        }

        public WorksheetNamedRange(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }
            this._name = name;
        }

        public WorksheetNamedRange(string name, string refersTo, bool hidden) : this(name)
        {
            this._refersTo = refersTo;
            this._hidden = hidden;
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._name = Util.GetAttribute(element, "Name", "urn:schemas-microsoft-com:office:spreadsheet");
            this._refersTo = Util.GetAttribute(element, "RefersTo", "urn:schemas-microsoft-com:office:spreadsheet");
            this._hidden = Util.GetAttribute(element, "Hidden", "urn:schemas-microsoft-com:office:spreadsheet", false);
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "NamedRange", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._name != null)
            {
                writer.WriteAttributeString("s", "Name", "urn:schemas-microsoft-com:office:spreadsheet", this._name);
            }
            if (this._refersTo != null)
            {
                writer.WriteAttributeString("s", "RefersTo", "urn:schemas-microsoft-com:office:spreadsheet", this._refersTo);
            }
            if (this._hidden)
            {
                writer.WriteAttributeString("s", "Hidden", "urn:schemas-microsoft-com:office:spreadsheet", "1");
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "NamedRange", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public bool Hidden
        {
            get
            {
                return this._hidden;
            }
            set
            {
                this._hidden = value;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
        }

        public string RefersTo
        {
            get
            {
                return this._refersTo;
            }
            set
            {
                this._refersTo = value;
            }
        }
    }
}

