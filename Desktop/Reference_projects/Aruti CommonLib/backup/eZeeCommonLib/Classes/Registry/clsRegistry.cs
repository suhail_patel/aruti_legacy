//************************************************************************************************************************************
//Class Name : clsRegistry.vb
//Purpose    : Read Write from Windows Registry.
//Date       : 
//Written By : Jitu
//Modified   : Naimish
//Note       : MergeJitu
//************************************************************************************************************************************

using Microsoft.Win32;
using System;
namespace eZeeCommonLib
{
    /// <summary>
    /// Read Write from Windows Registry.
    /// </summary>
    public class clsRegistry
    {
        #region  Properties
        public RegistryKey HKeyLocalMachine
        {
            get
            {
                return Registry.LocalMachine;
            }
        }

        public RegistryKey HkeyClassesRoot
        {
            get
            {
                return Registry.ClassesRoot;
            }
        }

        public RegistryKey HKeyCurrentUser
        {
            get
            {
                return Registry.CurrentUser;
            }
        }

        public RegistryKey HKeyUsers
        {
            get
            {
                return Registry.Users;
            }
        }

        public RegistryKey HKeyCurrentConfig
        {
            get
            {
                return Registry.CurrentConfig;
            }
        }

        public RegistryKey HKeyDynData
        {
            get
            {
                return Registry.DynData;
            }
        }
        #endregion

        /// <summary>
        /// Writes a value in the Registry.
        /// </summary>
        /// <param name="ParentKey">a RegistryKey that represents any of the six partent 
        /// keys where you want to write.</param> 
        /// <param name="SubKey">a String with the name of the subkey(or nested subkeys)
        /// where you want to write. This Subkey or subkeys may exist
        /// or not. If a subkey(or subkeys) doesn't exist
        /// this method will create it.
        /// </param> 
        /// <param name="ValueName">a String with the name of the value to be created.</param> 
        /// <param name="Value">an Object with the value to be stored.</param> 
        /// <returns>True if the method succeded, otherwise False.</returns> 
        public bool setValue(RegistryKey ParentKey, string SubKey, string ValueName, object Value)
        {

            RegistryKey Key = null;

            try
            {
                //Opens the given subkey
                Key = ParentKey.OpenSubKey(SubKey, true);
                if (Key == null) //if Key doesn't exist then create it
                {
                    Key = ParentKey.CreateSubKey(SubKey);
                }

                //sets the value
                Key.SetValue(ValueName, Value);

                return true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }

        /// <summary>
        /// Reads a value from the Registry.
        /// </summary>
        /// <param name="ParentKey">a RegistryKey that represents any of the six partent keys
        /// where you want to read.</param> 
        /// <param name="SubKey">a String with the name of the subkey(or nested subkeys)
        /// where you want to read.
        /// </param> 
        /// <param name="ValueName">a String with the name of the value to be read.</param> 
        /// <param name="Value">an Object with the value to be read.</param> 
        /// <returns>True if the method succeded, otherwise False.</returns> 
        public bool getValue(RegistryKey ParentKey, string SubKey, string ValueName, ref object Value)
        {
            RegistryKey Key = null;

            try
            {
                //opens the given subkey
                Key = ParentKey.OpenSubKey(SubKey, true);
                if (Key == null) //it Key doesn't exist then throw an exception
                {
                    throw new Exception("Given Subkey not exist.");
                }

                //Gets the value
                Value = Key.GetValue(ValueName);

                return true;
            }
            catch (Exception e)
            {
                e.ToString();
                return false;
            }
        }

        /// <summary>
        /// Reads a value from the Registry.
        /// </summary>
        /// <param name="ParentKey">a RegistryKey that represents any of the six partent keys
        /// where you want to read.</param> 
        /// <param name="SubKey">a String with the name of the subkey(or nested subkeys)
        /// where you want to read.
        /// </param> 
        /// <param name="Value">an Object with the value to be read.</param> 
        /// <returns>True if the method succeded, otherwise False.</returns> 
        public bool getValues(RegistryKey ParentKey, string SubKey, ref string[] Value)
        {
            RegistryKey Key = null;

            try
            {
                //opens the given subkey
                Key = ParentKey.OpenSubKey(SubKey, true);
                if (Key == null) //it Key doesn't exist then throw an exception
                {
                    throw new Exception("Given Subkey not exist.");
                }

                //Gets the value
                Value = Key.GetValueNames();

                return true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }

        /// <summary>
        /// Deletes a value from the Registry.
        /// </summary>
        /// <param name="ParentKey">a RegistryKey that represents any of the six partent keys
        /// where you want to delete a value from.</param> 
        /// <param name="SubKey">a String with the name of the subkey(or nested subkeys)
        /// where you want to delete a value from.
        /// </param> 
        /// <param name="ValueName">a String with the name of the value to be deleted.</param> 
        /// <returns>True if the method succeded, otherwise False.</returns> 
        public bool DeleteValue(RegistryKey ParentKey, string SubKey, string ValueName)
        {
            RegistryKey Key = null;

            try
            {
                //opens the given subkey
                Key = ParentKey.OpenSubKey(SubKey, true);

                //deletes the value
                if (Key != null)
                {
                    Key.DeleteValue(ValueName);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }

        }

        /// <summary>
        /// Deletes a key from the Registry.
        /// </summary>
        /// <param name="ParentKey">a RegistryKey that represents any of the six partent keys
        /// where you want to delete a subkey from.</param> 
        /// <param name="SubKey">a String with the name of the subkey to be deleted.</param> 
        /// <returns>True if the method succeded, otherwise False.</returns> 
        public bool DeleteSubKey(RegistryKey ParentKey, string SubKey)
        {

            try
            {
                //deletes the subkey and returns a False if Subkey doesn't exist
                ParentKey.DeleteSubKey(SubKey, false);
                return true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }

        /// <summary>
        /// Create a key from the Registry.
        /// </summary>
        /// <param name="ParentKey">a RegistryKey that represents any of the six partent keys
        /// where you want to create a subkey.</param> 
        /// <param name="SubKey">a String with the name of the subkey to be created.</param> 
        /// <returns>True if the method succeded, otherwise False.</returns> 
        public bool CreateSubKey(RegistryKey ParentKey, string SubKey)
        {

            try
            {
                //creates the given subkey
                ParentKey.CreateSubKey(SubKey);
                return true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }

        /// <summary>
        /// Get collection of subkeys of given parent subkeys.
        /// </summary>
        /// <param name="ParentKey">a RegistryKey that represents any of the six partent keys
        /// where you want to create a subkey.</param> 
        /// <param name="SubKey">a String with the name of the subkey(or nested subkeys)
        /// where you want to read child subkeys.</param> 
        /// <param name="strSubKeys">a String Collection with the name of the child subkey of parent subkeys.</param> 
        /// <returns>True if the method succeded, otherwise False.</returns> 
        public bool GetAllSubKeys(RegistryKey ParentKey, string SubKey, ref string[] strSubKeys)
        {

            RegistryKey Key = null;

            try
            {
                //opens the given subkey
                Key = ParentKey.OpenSubKey(SubKey);
                if (Key != null)
                {
                    //get all the subKeys (child subkeys)
                    strSubKeys = Key.GetSubKeyNames();

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }

    }
}