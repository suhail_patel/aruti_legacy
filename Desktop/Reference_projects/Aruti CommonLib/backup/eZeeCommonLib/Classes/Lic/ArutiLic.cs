﻿using SKCLNET;
using eZeeCommonLib;
using Microsoft.Win32;
using System;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualBasic.Devices;
using Microsoft.VisualBasic;

namespace eZeeCommonLib
{
    public class ArutiLic
    {
        #region "Declaration"

        public enum enApplicationType
        {
            FD,
            POS
        }
               
        private string strAppPath; 
        private LFile lf = new LFile();
        private string mstrSessionId = "0";
        private string mstrMachineId = "0";
        private string mstrHotelName = "";
        private string mstrNoOfCompany = "1";
        private string mstrNoOfEmp = "0";
        private string mstrNoOfPDA = "0";
        private string mstrLicenseKey = "0";
        private string mstrRoomKey = "0";
        private string mstrStatusMessage = "";
        private string mstrSoftDate = "";
        private string mstrHardDate = "";
        private bool mblnIsNewLicenseActivated = false;
        private enApplicationType menApplication;
        private string mstrEdition = "";
        //private bool mboolFailure = false;
        private int param2 = 0;

        
        #endregion

        //string strParameterModuleKey = "ModulesLicense";

        #region "Contrsuctor"
        public ArutiLic(bool IsOldLicenseActivated) 
        {
            clsRegistry objReg = new clsRegistry();
            object obj1;
            obj1 = null;
            objReg.getValue(objReg.HKeyLocalMachine, "SOFTWARE\\NPK\\Aruti", "apppath", ref obj1);
            strAppPath = obj1.ToString();
 
            bool blnInitialRun = false;

            lf.Trigger += new LFile.__Delegate_Trigger(lf_Trigger);
            lf.Error += new LFile.__Delegate_Error(lf_error);
            lf.StatusChanged +=new LFile.__Delegate_StatusChanged(lf_StatusChanged);

            lf.EZTrial = true;
            lf.UseEZTrigger = false;
            lf.LFOpenFlags = 0;

            lf.LFPassword = "{67d997ba-fe65-40e0-85d0-ee029ff9ac83}";
            lf.LFName = strAppPath + "hraruti.ini";
            lf.TCSeed = Convert.ToInt32("43100");
            lf.TCRegKey2Seed = Convert.ToInt32("147");
            
            lf.Enabled = false;

            /*
             * To prevent premature termination always check
             * Expiration Limit and Expiration Count if they
             * shows 0 turn them to -1
            */
            if (lf.GetVarNum(SKGETVARNUM.SK_VAR_EXP_LIMIT) <= 0)
            {
                lf.ExecutionCount = -1;
                lf.SetVarNum(SKGETVARNUM.SK_VAR_EXP_LIMIT, -1);
            }

            if (lf.GetUserString(10) == "1")
            {
                lf.SetUserString(10, "0");
                lf.SetUserDate(1, "0/0/0");
                blnInitialRun = true;
                lf.SetUserString(10, "0");
                lf.ExpireMode = "D";
                lf.ExecutionCount = -1;
                lf.SetVarNum(SKGETVARNUM.SK_VAR_EXP_LIMIT, -1);
                DateTime d = DateTime.Now;
                d=d.AddDays(30);
                lf.SetVarDate(SKGETVARDATE.SK_VAR_EXP_DATE_SOFT, d.Month, d.Day, d.Year);
                //d=d.AddMonths(5);
                //lf.SetVarDate(SKGETVARDATE.SK_VAR_EXP_DATE_HARD, d.Month, d.Day, d.Year);

                //SOHAIL - Start - To give 30 days demo when Aruti is installed very first time OR Clear Licence
                clsDataOperation objDataOprn = new clsDataOperation();
                DataSet ds = new DataSet();

                string strKey = "1321";
                string strParameterDemoKey = "PayrollProcess"; //7WJwOhm23sKevuWxPqT+KWnDtA3JD0a3nsx+VdUTlYz1cEq+dw+hAbphOf3xJIGN
                string strParameterActivateKey = "TnAActivate";//7WJwOhm23sKevuWxPqT+KRAcmRDYVV6gWmUO9S8mWgamUv0lfwMfwbckAf2ziVd07b8begW0Zl2Hv8QL7CzCoA==

                ds = objDataOprn.ExecQuery("SELECT * FROM hrmsconfiguration..cfgroup_master", "Data");
                if (ds.Tables["Data"].Rows.Count > 0)
                {
                    strKey = ds.Tables["Data"].Rows[0]["groupname"].ToString();
                    if (strKey.Length > 4)
                    {
                        strKey = strKey.Substring(0, 1) + strKey.Substring(1, 1) + strKey.Substring(strKey.Length - 2, 1) + strKey.Substring(strKey.Length - 1, 1);
                       
                    }                    
                }
              

                string strParameterDemoValue = clsSecurity.Encrypt(d.ToString("yyyyMMdd"), strKey);
                string strParameterActivateValue = clsSecurity.Encrypt("Aruti Lic Not Activated", strKey);

                /* PayrollProcess */
                 ds = objDataOprn.ExecQuery("SELECT * FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterDemoKey + "'", "Data");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return;
                }

                if (ds.Tables["Data"].Rows.Count <= 0)
                {

                    objDataOprn.ExecNonQuery("INSERT INTO hrmsconfiguration..cfconfiguration(key_name,key_value) SELECT '" + strParameterDemoKey + "','" + strParameterDemoValue + "' WHERE NOT EXISTS (SELECT configunkid FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterDemoKey + "')");
                    if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                    {
                        eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                        return;
                    }
                }
                //else
                //{                    
                //    objDataOprn.ExecNonQuery("UPDATE hrmsconfiguration..cfconfiguration SET key_value = '" + strParameterDemoValue + "' WHERE key_name = '" + strParameterDemoKey + "'");
                //    if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                //    {
                //        eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                //        return;
                //    }
                //}

                /* TnAActivate */
                ds = objDataOprn.ExecQuery("SELECT * FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterActivateKey + "'", "Data");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return;
                }

                if (ds.Tables["Data"].Rows.Count <= 0)
                {

                    objDataOprn.ExecNonQuery("INSERT INTO hrmsconfiguration..cfconfiguration(key_name,key_value) SELECT '" + strParameterActivateKey + "','" + strParameterActivateValue + "' WHERE NOT EXISTS (SELECT configunkid FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterActivateKey + "')");
                    if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                    {
                        eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                        return;
                    }
                }
                //else
                //{
                //    objDataOprn.ExecNonQuery("UPDATE hrmsconfiguration..cfconfiguration SET key_value = '" + strParameterActivateValue + "' WHERE key_name = '" + strParameterActivateKey + "'");
                //    if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                //    {
                //        eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                //        return;
                //    }
                //}
                //SOHAIL - End

                lf.ForceStatusChanged();
                lf.UpdateLastUsedInfo();
                mstrStatusMessage = "";
                objReg.setValue(objReg.HKeyLocalMachine, "SOFTWARE\\NPK\\Aruti", "arg0", "1");
            }

            mblnIsNewLicenseActivated = true;

            lf.Enabled = true;

            if (lf.GetUserNumber(5) == 0)
            {
                mstrSessionId = lf.TCSessionCode.ToString();
                lf.SetUserNumber(5, Int32.Parse(mstrSessionId));
            }
            else
            {
                mstrSessionId = lf.GetUserNumber(5).ToString();
            }

            mstrMachineId = lf.CPCompNo.ToString();

            mstrSoftDate = lf.ExpireDateSoft;
            mstrHardDate = lf.ExpireDateHard;

            if (!string.IsNullOrEmpty(lf.GetUserString(1)))
            {
                mstrHotelName = lf.GetUserString(1);
            }
            else
            {
                mstrHotelName = "";
            }

            if (!string.IsNullOrEmpty(lf.GetUserString(2)))
            {
                mstrNoOfEmp = lf.GetUserString(2);
            }
            else
            {
                mstrNoOfEmp = "0";
            }

            if (lf.GetUserNumber(1) != 0)
            {
                mstrNoOfCompany = lf.GetUserNumber(1).ToString();
            }
            else
            {
                mstrNoOfCompany = "";
            }

            if (lf.GetUserNumber(2) != 0)
            {
                mstrNoOfPDA = lf.GetUserNumber(2).ToString();
            }
            else
            {
                mstrNoOfPDA = "";
            }

            if (lf.GetUserNumber(4) != 0)
            {
                mstrEdition = lf.GetUserNumber(4).ToString();
            }
            else
            {
                mstrEdition = "0";
            }

            if (blnInitialRun)
            {
                LogOperation("Initial Demo", "This first time application run and 30 days evaluation granted.", -1, -1);
            }
        }

        #endregion

        #region "Properties"
        public string SessionId
        {
            get { return mstrSessionId; }
        }

        public string MachineId
        {
            get { return mstrMachineId; }
        }

        public string HotelName
        {
            get { return mstrHotelName; }
            set { mstrHotelName = value; }
        }

        public string NoOfCompany
        {
            get { return (mstrNoOfCompany.Trim().Equals("") ? "1" : mstrNoOfCompany); }
            set { mstrNoOfCompany = value; }
        }

        public string NoOfEmployees
        {
            get { return (mstrNoOfEmp.Trim().Equals("") ? "0" : Convert.ToInt32(mstrNoOfEmp).ToString()); }
            set { mstrNoOfEmp = value; }
        }

        public string NoOfPDA
        {
            get { return (mstrNoOfPDA.Trim().Equals("") ? "0" : mstrNoOfPDA); }
            set { mstrNoOfPDA = value; }
        }

        public string LicenseKey
        {
            set { mstrLicenseKey = value; }
        }

        public string RoomKey
        {
            set { mstrRoomKey = (value.Trim().Equals("") ? "0" : value); }
        }

        public string StatusMessage
        {
            get { return mstrStatusMessage; }
        }

        public string SoftDate
        {
            get { return mstrSoftDate; }
        }

        public string HardDate
        {
            get { return mstrHardDate; }
        }

        public bool IsExpire
        {
            get 
            {
                if (IsNewLicenseActivated)
                {
                if (lf.IsExpired || (!IsPropertyNameMatched(mstrHotelName)) || (!IsMachineMatched))
                    return true;
                else
                    return false;
             }
                else
                    return false;
             }
        }

        public bool IsDemo
        {
            get { return lf.IsDemo; }
        }

        public bool IsClockTurnBack
        {
            get { return lf.IsClockTurnedBack; }
        }

        public int DaysLeft
        {
            get { return lf.DaysLeft; }
        }

        public bool ModuleStatus(int intModuleId)
        {
            try
            {
                //intModuleId -= 51;
                //if (intModuleId < 0 & intModuleId > 50) return false;
                string strParameterModuleValue = new string('0', 100);

                string strKey = "1321";
                if (!mstrHotelName.Trim().Equals(""))
                {
                    if (mstrHotelName.Length > 4)
                    {
                        strKey = mstrHotelName.Substring(0, 1) + mstrHotelName.Substring(1, 1) + mstrHotelName.Substring(mstrHotelName.Length - 2, 1) + mstrHotelName.Substring(mstrHotelName.Length - 1, 1);
                    }
                    else
                    {
                        strKey = mstrHotelName;
                    }
                }
                //MessageBox.Show(intModuleId.ToString() );
                //strParameterModuleKey = clsSecurity.Encrypt(strParameterModuleKey, "1321");
                string strParameterModuleKey = "ArutiModulesLicense";
                strParameterModuleKey = clsSecurity.Encrypt(strParameterModuleKey, "1321");
                strParameterModuleValue = clsSecurity.Encrypt(strParameterModuleValue, strKey);
                clsDataOperation objDataOprn = new clsDataOperation();
                DataSet ds = new DataSet();

                ds = objDataOprn.ExecQuery("SELECT * FROM hrmsConfiguration..cfconfiguration WHERE key_name = '" + strParameterModuleKey + "'", "Data");

                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }

                if (ds.Tables["Data"].Rows.Count > 0)
                {
                    strParameterModuleValue = ds.Tables["Data"].Rows[0]["key_value"].ToString();
                    //MsgBox(strParameterModuleValue) 
                    strParameterModuleValue = clsSecurity.Decrypt(strParameterModuleValue, strKey);
                    //MessageBox.Show(strParameterModuleValue); 
                    //MessageBox.Show(strParameterModuleValue.ToCharArray()[intModuleId].ToString());
                    if (strParameterModuleValue.ToCharArray()[intModuleId] == '1')
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                //MsgBox(ex.Message) 
                return false;
            }
        }

        public bool IsMachineMatched
        {
            get
            {
                if (lf.ExpireMode == "N")
                {
                    //HRK - 5 Nov 2009
                    //OLD :: if (lf.GetUserNumber(3) == lf.CPCompNo)
                    if (lf.CPCheck(0)==1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }

        public bool IsPropertyNameMatched(string strPropertyName)
        {
            if (lf.ExpireMode == "N")
            {
                //OLD
                //if (lf.GetUserString(1) == strPropertyName)
                //NEW
                if ((lf.GetUserString(1) == Unicode2Ascii(strPropertyName)) || lf.GetUserString(1) == strPropertyName )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool IsNewLicenseActivated
        {
            get { return mblnIsNewLicenseActivated; }
        }

        public string Edition
        { 
            get { return mstrEdition; }
        }

        #endregion

        #region "Events"
        private void lf_error()
        {
            mstrStatusMessage = lf.LastErrorNumber + " " + lf.LastErrorString;
            if (lf.LastErrorNumber != 15 & lf.LastErrorNumber != 18)
            {
                LogOperation("Error Condition", mstrStatusMessage, -1, -1);
            }
        }

        private void lf_StatusChanged(bool startup)
        {
            if (lf.IsExpired)
            {
                mstrStatusMessage = "This demo has expired!";
                LogOperation("Status Change.", mstrStatusMessage, -1, -1);
            }
        }

        private void lf_Trigger(int p1, int p2)
        {
            //MsgBox(p1 & " " & p2) 
            if (p1 == 0)
            {
                mstrStatusMessage = "Invalid code entered!";
                LogOperation("Invalid Trigger Fire", mstrStatusMessage, p1, param2);
                return;
            }
            switch (p1)
            {
                case 1:
                    // Activate Module 
                    LogOperation("Trigger Fire", "Module Activation requested, Module Id : " + p2, p1, param2);
                    ModifySingleModule(param2, true);
                    break;
                case 2:
                    // Deactivate Module 
                    LogOperation("Trigger Fire", "Module Deactivation requested, Module Id : " + p2, p1, param2);
                    ModifySingleModule(param2, false);
                    break;
                case 3:
                    // FD POS Module 
                    LogOperation("Trigger Fire", "FD POS Module Activation requested, Module Id : " + p2, p1, param2);
                    ModifyBulkModule(param2);
                    break;
                case 4:
                    // Clear License 
                    LogOperation("Trigger Fire", "Clear License", p1, param2);
                    lf.SetUserString(10, "1");
                    DateTime d = DateTime.Now;
                    d = DateTime.Now;
                    d = d.AddDays(30);
                    InsertUpdateDemoProperty(d);
                    break;
                case 11:
                    // Authorised this machine 

                    lf.Enabled = false;
                    //OLD : for unicode this is creating problem
                    //lf.SetUserString(1, mstrHotelName);
                    //NEW : before stroing convert string to ASCII
                    lf.SetUserString(1, Unicode2Ascii(mstrHotelName));
                    string strEmp;
                    string strCompany;
                    strEmp = param2.ToString().Substring(param2.ToString().Length - 6, 6);
                    strCompany = param2.ToString().Substring(0, param2.ToString().Length - 6);
                    //lf.SetUserNumber(1, param2);
                    lf.SetUserNumber(1, Convert.ToInt32(strCompany));
                    lf.SetUserString(2, strEmp);
                    lf.SetUserNumber(2, 0);
                    lf.SetUserNumber(3, lf.CPCompNo);
                    lf.SetUserNumber(4, 0);
                    //Sohail (09 May 213) - Start
                    //DateTime d = DateTime.Now;
                    d = DateTime.Now;
                    //Sohail (09 May 213) - End
                    d = d.AddYears(50);
                    lf.SetVarDate(SKGETVARDATE.SK_VAR_EXP_DATE_HARD, d.Month, d.Day, d.Year);
                    lf.CPAdd(4, 0);
                    lf.ExpireMode = "N";
                    lf.Enabled = true;
                    lf.ForceStatusChanged();

                    //HRK - 23 Sep 2009 - Start
                    //Issue : To detect new licensing policy is already activated
                    clsRegistry objReg = new clsRegistry();
                    objReg.setValue(objReg.HKeyLocalMachine, "SOFTWARE\\NPK\\Aruti", "arg0", "2");
                    objReg = null;
                    //Try to rename old license file, so user can never go back
                    try
                    {
                        if (System.IO.File.Exists(System.Environment.GetEnvironmentVariable("SystemRoot") + "\\ezeenextgen.ini"))
                        {
                            System.IO.File.Move(System.Environment.GetEnvironmentVariable("SystemRoot") + "\\ezeenextgen.ini", System.Environment.GetEnvironmentVariable("SystemRoot") + "\\ezeenextgen.ini.bak");
                        }
                    }
                    catch
                    {

                    }
                    //HRK - 23 Sep 2009 - End

                    InsertEmployeeCompany(Convert.ToInt32(strEmp), Convert.ToInt32(strCompany));

                    LogOperation("Trigger Fire", "Authorised this machine", p1, p2);
                    break;
                case 8:
                    // Custom days extenstion 
                    lf.Enabled = false;
                    lf.ExpireMode = "D";
                    d = DateTime.Now;
                    d = d.AddDays(param2);
                    lf.SetVarDate(SKGETVARDATE.SK_VAR_EXP_DATE_SOFT, d.Month, d.Day, d.Year);
                    InsertUpdateDemoProperty(d);
                    lf.Enabled = true;
                    lf.ForceStatusChanged();
                    lf.UpdateLastUsedInfo();
                    LogOperation("Trigger Fire", "Custom days extenstion, Days to extend : " + p2, p1, p2);
                    break;
            }
        }

        private bool InsertUpdateDemoProperty(DateTime dtExpDate)
        {
            try
            {
                string strParameterEmployeeKey = "PayrollProcess"; //7WJwOhm23sKevuWxPqT+KWnDtA3JD0a3nsx+VdUTlYz1cEq+dw+hAbphOf3xJIGN
                string strParameterActivateKey = "TnAActivate";////SOHAIL///// //7WJwOhm23sKevuWxPqT+KRAcmRDYVV6gWmUO9S8mWgamUv0lfwMfwbckAf2ziVd07b8begW0Zl2Hv8QL7CzCoA==
                string strKey = "1321";
                if (!mstrHotelName.Trim().Equals(""))
                {
                    if (mstrHotelName.Length > 4)
                    {
                        strKey = mstrHotelName.Substring(0, 1) + mstrHotelName.Substring(1, 1) + mstrHotelName.Substring(mstrHotelName.Length - 2, 1) + mstrHotelName.Substring(mstrHotelName.Length - 1, 1);
                    }
                    else
                    {
                        strKey = mstrHotelName;
                    }
                }

                string strParameterEmployeeValue = clsSecurity.Encrypt(dtExpDate.ToString("yyyyMMdd"), strKey);
                string strParameterActivateValue = clsSecurity.Encrypt("Aruti Lic Not Activated", strKey);////SOHAIL/////

                clsDataOperation objDataOprn = new clsDataOperation();
                DataSet ds = new DataSet();

                ds = objDataOprn.ExecQuery("SELECT * FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterEmployeeKey + "'", "Data");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }

                if (ds.Tables["Data"].Rows.Count <= 0)
                {
                    objDataOprn.ExecNonQuery("INSERT INTO hrmsconfiguration..cfconfiguration(key_name,key_value) SELECT '" + strParameterEmployeeKey + "','" + strParameterEmployeeValue + "' WHERE NOT EXISTS (SELECT configunkid FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterEmployeeKey + "')");
                    if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                    {
                        eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                        return false;
                    }
                    
                }
                objDataOprn.ExecNonQuery("UPDATE hrmsconfiguration..cfconfiguration SET key_value = '" + strParameterEmployeeValue + "' WHERE key_name = '" + strParameterEmployeeKey + "'");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }

                ////SOHAIL/////
                objDataOprn.ExecNonQuery("UPDATE hrmsconfiguration..cfconfiguration SET key_value = '" + strParameterActivateValue + "' WHERE key_name = '" + strParameterActivateKey + "'");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }
                ////SOHAIL/////
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool InsertEmployeeCompany(int intEmp, int intCompany)
        {
            try
            {
                string strParameterEmployeeKey = "Emp";
                string strParameterCompanyKey = "Comp";
                string strParameterActivateKey = "TnAActivate"; //7WJwOhm23sKevuWxPqT+KRAcmRDYVV6gWmUO9S8mWgamUv0lfwMfwbckAf2ziVd07b8begW0Zl2Hv8QL7CzCoA==
                string strKey = "1321";
                if (!mstrHotelName.Trim().Equals(""))
                {
                    if (mstrHotelName.Length > 4)
                    {
                        strKey = mstrHotelName.Substring(0, 1) + mstrHotelName.Substring(1, 1) + mstrHotelName.Substring(mstrHotelName.Length - 2, 1) + mstrHotelName.Substring(mstrHotelName.Length - 1, 1);
                    }
                    else
                    {
                        strKey = mstrHotelName;
                    }
                }

                string strParameterEmployeeValue = clsSecurity.Encrypt(intEmp.ToString(), strKey);
                string strParameterCompanyValue = clsSecurity.Encrypt(intCompany.ToString(), strKey);
                string strParameterActivateValue = clsSecurity.Encrypt("Aruti Lic Activated", strKey);

                clsDataOperation objDataOprn = new clsDataOperation();
                DataSet ds = new DataSet();

                ds = objDataOprn.ExecQuery("SELECT * FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterEmployeeKey + "'", "Data");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }

                if (ds.Tables["Data"].Rows.Count <= 0)
                {
                    objDataOprn.ExecNonQuery("INSERT INTO hrmsconfiguration..cfconfiguration(key_name,key_value) SELECT '" + strParameterEmployeeKey + "','" + strParameterEmployeeValue + "' WHERE NOT EXISTS (SELECT configunkid FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterEmployeeKey + "')");
                    if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                    {
                        eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                        return false;
                    }
                    objDataOprn.ExecNonQuery("INSERT INTO hrmsconfiguration..cfconfiguration(key_name,key_value) SELECT '" + strParameterCompanyKey + "','" + strParameterCompanyValue + "' WHERE NOT EXISTS (SELECT configunkid FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterCompanyKey + "')");
                    if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                    {
                        eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                        return false;
                    }
                    objDataOprn.ExecNonQuery("INSERT INTO hrmsconfiguration..cfconfiguration(key_name,key_value) SELECT '" + strParameterActivateKey + "','" + strParameterActivateValue + "' WHERE NOT EXISTS (SELECT configunkid FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterActivateKey + "')");
                    if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                    {
                        eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                        return false;
                    }
                }
                objDataOprn.ExecNonQuery("UPDATE hrmsconfiguration..cfconfiguration SET key_value = '" + strParameterEmployeeValue + "' WHERE key_name = '" + strParameterEmployeeKey + "'");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }
                objDataOprn.ExecNonQuery("UPDATE hrmsconfiguration..cfconfiguration SET key_value = '" + strParameterCompanyValue + "' WHERE key_name = '" + strParameterCompanyKey + "'");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }
                objDataOprn.ExecNonQuery("UPDATE hrmsconfiguration..cfconfiguration SET key_value = '" + strParameterActivateValue + "' WHERE key_name = '" + strParameterActivateKey + "'");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }
                return true; 
            }
            catch
            {
                return false;
            }
        }

        public string Decrypt(string strData)
        {
            string strKey = "1321";
            if (!mstrHotelName.Trim().Equals(""))
            {
                if (mstrHotelName.Length > 4)
                {
                    strKey = mstrHotelName.Substring(0, 1) + mstrHotelName.Substring(1, 1) + mstrHotelName.Substring(mstrHotelName.Length - 2, 1) + mstrHotelName.Substring(mstrHotelName.Length - 1, 1);
                }
                else
                {
                    strKey = mstrHotelName;
                }
            }
            return clsSecurity.Decrypt(strData, strKey);
        }

        private bool ModifyBulkModule(int intModuleIds)
        {
            try
            {
              
                if (intModuleIds < 0 & intModuleIds > 16383) return false;
                string strParameterModuleKey = "ArutiModulesLicense";
                //string strParameterModuleValue = new string('0', 50);
                string strParameterModuleValue = new string('0', 19);
                string strKey = "1321";
                if (!mstrHotelName.Trim().Equals(""))
                {
                    if (mstrHotelName.Length > 4)
                    {
                        strKey = mstrHotelName.Substring(0, 1) + mstrHotelName.Substring(1, 1) + mstrHotelName.Substring(mstrHotelName.Length - 2, 1) + mstrHotelName.Substring(mstrHotelName.Length - 1, 1);
                    }
                    else
                    {
                        strKey = mstrHotelName;
                    }
                }
                strParameterModuleKey = clsSecurity.Encrypt(strParameterModuleKey, "1321");
                strParameterModuleValue = clsSecurity.Encrypt(strParameterModuleValue, strKey);

                //MsgBox("1 : " & strParameterModuleKey) 
                //MsgBox("2 : " & strParameterModuleValue) 
                clsDataOperation objDataOprn = new clsDataOperation();
                DataSet ds = new DataSet();
                //to delete old module license.
                objDataOprn.ExecNonQuery("DELETE FROM hrmsconfiguration..cfconfiguration WHERE key_name = 'B7Hk00tbNd5oRrD9DojgQIbOmc3xwZEwMnenWdlsTrg=' ");

                ds = objDataOprn.ExecQuery("SELECT * FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterModuleKey + "'", "Data");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }

                if (ds.Tables["Data"].Rows.Count > 0)
                {
                    //MsgBox("3.1 : " & strParameterModuleValue) 
                    strParameterModuleValue = ds.Tables["Data"].Rows[0]["key_value"].ToString();
                    strParameterModuleValue = clsSecurity.Decrypt(strParameterModuleValue, strKey);
                }
                else
                {
                    //MsgBox("3 : " & strParameterModuleValue) 
                    //MsgBox("4.1 : " & "INSERT INTO cfparameter(key_name,key_value) VALUES('" & strParameterModuleKey & "','" & strParameterModuleValue & "')") 
                    objDataOprn.ExecNonQuery("INSERT INTO hrmsconfiguration..cfconfiguration(key_name,key_value) SELECT '" + strParameterModuleKey + "','" + strParameterModuleValue + "' WHERE NOT EXISTS (SELECT configunkid FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterModuleKey + "')");
                    if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                    {
                        eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                        return false;
                    }
                    strParameterModuleValue = clsSecurity.Decrypt(strParameterModuleValue, strKey);
                }
                //MsgBox("4 : " & strParameterModuleValue) 

                //MsgBox("5.1 : " & strParameterModuleValue) 

                //if (menApplication == enApplicationType.FD)
                //{
                //    strParameterModuleValue = strParameterModuleValue.Remove(0, 14);
                //    strParameterModuleValue = Convert.ToString(intModuleIds, 2).PadLeft(14, '0') + strParameterModuleValue;
                //}
                //else if (menApplication == enApplicationType.POS)
                //{
                //    strParameterModuleValue = strParameterModuleValue.Remove(14, 14);
                //    strParameterModuleValue = strParameterModuleValue.Insert(14, Convert.ToString(intModuleIds, 2).PadLeft(14, '0'));
                //}
                //else
                //{
                    //strParameterModuleValue = strParameterModuleValue.Remove(0, 21);
                    //strParameterModuleValue = strParameterModuleValue.Insert(0, Convert.ToString(intModuleIds, 2).PadLeft(21, '0'));
                strParameterModuleValue = Convert.ToString(intModuleIds, 2).PadLeft(19, '0');
                //}
                strParameterModuleValue = clsSecurity.Encrypt(strParameterModuleValue, strKey);
                //MsgBox("5 : " & strParameterModuleValue) 

                objDataOprn.ExecNonQuery("UPDATE hrmsconfiguration..cfconfiguration SET key_value = '" + strParameterModuleValue + "' WHERE key_name = '" + strParameterModuleKey + "'");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }
                //MsgBox("6 : " & strParameterModuleValue) 
                return true;
            }
            catch
            {
                //MsgBox(ex.Message) 
                return false;
            }
        }

        private bool ModifySingleModule(int intModuleId, bool blnActivationFlag)
        {
            try
            {
                intModuleId -= 1;
                //if (intModuleId < 0 & intModuleId > 50) return false;
                string strParameterModuleKey = "ArutiModulesLicense";
                string strParameterModuleValue = new string('0', 100);
                string strKey = "1321";
                if (!mstrHotelName.Trim().Equals(""))
                {
                    if (mstrHotelName.Length > 4)
                    {
                        strKey = mstrHotelName.Substring(0, 1) + mstrHotelName.Substring(1, 1) + mstrHotelName.Substring(mstrHotelName.Length - 2, 1) + mstrHotelName.Substring(mstrHotelName.Length - 1, 1);
                    }
                    else
                    {
                        strKey = mstrHotelName;
                    }
                }
                strParameterModuleKey = clsSecurity.Encrypt(strParameterModuleKey, "1321");
                strParameterModuleValue = clsSecurity.Encrypt(strParameterModuleValue, strKey);

                //MsgBox("1 : " & strParameterModuleKey) 
                //MsgBox("2 : " & strParameterModuleValue) 
                clsDataOperation objDataOprn = new clsDataOperation();
                DataSet ds = new DataSet();

                ds = objDataOprn.ExecQuery("SELECT * FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterModuleKey + "'", "Data");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }

                if (ds.Tables["Data"].Rows.Count > 0)
                {
                    //MsgBox("3.1 : " & strParameterModuleValue) 
                    strParameterModuleValue = ds.Tables["Data"].Rows[0]["key_value"].ToString();
                    strParameterModuleValue = clsSecurity.Decrypt(strParameterModuleValue, strKey);
                }
                else
                {
                    //MsgBox("3 : " & strParameterModuleValue) 
                    //MsgBox("4.1 : " & "INSERT INTO cfparameter(key_name,key_value) VALUES('" & strParameterModuleKey & "','" & strParameterModuleValue & "')") 
                    objDataOprn.ExecNonQuery("INSERT INTO hrmsconfiguration..cfconfiguration(key_name,key_value) SELECT '" + strParameterModuleKey + "','" + strParameterModuleValue + "' WHERE NOT EXISTS (SELECT configunkid FROM hrmsconfiguration..cfconfiguration WHERE key_name = '" + strParameterModuleKey + "')");
                    if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                    {
                        eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                        return false;
                    }
                    strParameterModuleValue = clsSecurity.Decrypt(strParameterModuleValue, strKey);
                }
                //MsgBox("4 : " & strParameterModuleValue) 

                //MsgBox("5.1 : " & strParameterModuleValue) 
                char[] arrChr = strParameterModuleValue.ToCharArray();
                arrChr[intModuleId] = (blnActivationFlag ? '1' : '0');
                strParameterModuleValue = new string(arrChr);
                strParameterModuleValue = clsSecurity.Encrypt(strParameterModuleValue, strKey);

                //MsgBox("5 : " & strParameterModuleValue) 

                objDataOprn.ExecNonQuery("UPDATE hrmsconfiguration..cfconfiguration SET key_value = '" + strParameterModuleValue + "' WHERE key_name = '" + strParameterModuleKey + "'");
                if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                {
                    eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
                    return false;
                }
                //MsgBox("6 : " & strParameterModuleValue) 
                return true;
            }
            catch
            {
                //MsgBox(ex.Message) 
                return false;
            }
        }
        #endregion

        #region "Public Functions"
        public bool licenseIsValid()
        {
            try
            {
                int tcdata = 0;
                int rooms=0;
                if (mstrRoomKey != "0")
                {
                    rooms = ValidateRoomKey(mstrRoomKey);
                    if (rooms < 0)
                    {
                        LogOperation("licenseIsValid", "Error : Invalid RoomKey ", -1, -1);
                        //mboolFailure = true;
                        mstrSessionId = lf.TCSessionCode.ToString();
                        lf.SetUserNumber(5, Int32.Parse(mstrSessionId));
                        return false;
                    }
                }
                param2 = rooms;
                //LogOperation("licenseIsValid", "Attempt to fire trigger", -1, -1);
                tcdata = lf.TCode(Int32.Parse(mstrLicenseKey), 0, Int32.Parse(mstrSessionId), Int32.Parse(mstrMachineId), ref tcdata);
                mstrSessionId = lf.TCSessionCode.ToString();
                lf.SetUserNumber(5, Int32.Parse(mstrSessionId));
                
                if (tcdata == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                mstrSessionId = lf.TCSessionCode.ToString();
                lf.SetUserNumber(5, Int32.Parse(mstrSessionId));
                return false;
            }
        }

        public int GetEventCode()
        {
            try
            {
                int tcdata=0;
                tcdata = lf.TCode(Int32.Parse(mstrLicenseKey), 0, Int32.Parse(mstrSessionId), Int32.Parse(mstrMachineId), ref tcdata);
                return tcdata;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }

        #endregion

        #region "Private Functions"

        //private bool ModifyBulkModule(int intModuleIds)
        //{
        //    try
        //    {
        //        if (intModuleIds < 0 & intModuleIds > 16383) return false;
        //        string strParameterModuleKey = "ModulesLicense";
        //        string strParameterModuleValue = new string('0', 50);
        //        string strKey = "1321";
        //        if (!mstrHotelName.Trim().Equals(""))
        //        {
        //            if (mstrHotelName.Length > 4)
        //            {
        //                strKey = mstrHotelName.Substring(0, 1) + mstrHotelName.Substring(1, 1) + mstrHotelName.Substring(mstrHotelName.Length - 2,1) + mstrHotelName.Substring(mstrHotelName.Length - 1, 1);
        //            }
        //            else
        //            {
        //                strKey = mstrHotelName;
        //            }
        //        }
        //        strParameterModuleKey = clsSecurity.Encrypt(strParameterModuleKey, "1321");
        //        strParameterModuleValue = clsSecurity.Encrypt(strParameterModuleValue, strKey);

        //        //MsgBox("1 : " & strParameterModuleKey) 
        //        //MsgBox("2 : " & strParameterModuleValue) 
        //        clsDataOperation objDataOprn = new clsDataOperation();
        //        DataSet ds = new DataSet();

        //        ds = objDataOprn.ExecQuery("SELECT * FROM cfparameter WHERE key_name = '" + strParameterModuleKey + "'", "Data");
        //        if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
        //        {
        //            eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
        //            return false;
        //        }

        //        if (ds.Tables["Data"].Rows.Count > 0)
        //        {
        //            //MsgBox("3.1 : " & strParameterModuleValue) 
        //            strParameterModuleValue = ds.Tables["Data"].Rows[0]["key_value"].ToString();
        //            strParameterModuleValue = clsSecurity.Decrypt(strParameterModuleValue, strKey);
        //        }
        //        else
        //        {
        //            //MsgBox("3 : " & strParameterModuleValue) 
        //            //MsgBox("4.1 : " & "INSERT INTO cfparameter(key_name,key_value) VALUES('" & strParameterModuleKey & "','" & strParameterModuleValue & "')") 
        //            objDataOprn.ExecNonQuery("INSERT INTO cfparameter(key_name,key_value) SELECT '" + strParameterModuleKey + "','" + strParameterModuleValue + "' WHERE NOT EXISTS (SELECT parameterunkid FROM cfparameter WHERE key_name = '" + strParameterModuleKey + "')");
        //            if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
        //            {
        //                eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
        //                return false;
        //            }
        //            strParameterModuleValue = clsSecurity.Decrypt(strParameterModuleValue, strKey);
        //        }
        //        //MsgBox("4 : " & strParameterModuleValue) 

        //        //MsgBox("5.1 : " & strParameterModuleValue) 

        //        if (menApplication == enApplicationType.FD)
        //        {
        //            strParameterModuleValue = strParameterModuleValue.Remove(0, 14);
        //            strParameterModuleValue = Convert.ToString(intModuleIds, 2).PadLeft(14, '0') + strParameterModuleValue;
        //        }
        //        else if (menApplication == enApplicationType.POS)
        //        {
        //            strParameterModuleValue = strParameterModuleValue.Remove(14, 14);
        //            strParameterModuleValue = strParameterModuleValue.Insert(14, Convert.ToString(intModuleIds, 2).PadLeft(14, '0'));
        //        }
        //        strParameterModuleValue = clsSecurity.Encrypt(strParameterModuleValue, strKey);
        //        //MsgBox("5 : " & strParameterModuleValue) 

        //        objDataOprn.ExecNonQuery("UPDATE cfparameter SET key_value = '" + strParameterModuleValue + "' WHERE key_name = '" + strParameterModuleKey + "'");
        //        if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
        //        {
        //            eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
        //            return false;
        //        }
        //        //MsgBox("6 : " & strParameterModuleValue) 
        //        return true;
        //    }
        //    catch 
        //    {
        //        //MsgBox(ex.Message) 
        //        return false;
        //    }
        //}

        //private bool ModifySingleModule(int intModuleId, bool blnActivationFlag)
        //{
        //    try
        //    {
        //        intModuleId -= 1;
        //        if (intModuleId < 0 & intModuleId > 50) return false;
        //        string strParameterModuleKey = "ModulesLicense";
        //        string strParameterModuleValue = new string('0', 50);
        //        string strKey = "1321";
        //        if (!mstrHotelName.Trim().Equals(""))
        //        {
        //            if (mstrHotelName.Length > 4)
        //            {
        //                strKey = mstrHotelName.Substring(0, 1) + mstrHotelName.Substring(1, 1) + mstrHotelName.Substring(mstrHotelName.Length - 2, 1) + mstrHotelName.Substring(mstrHotelName.Length - 1,1);
        //            }
        //            else
        //            {
        //                strKey = mstrHotelName;
        //            }
        //        }
        //        strParameterModuleKey = clsSecurity.Encrypt(strParameterModuleKey, "1321");
        //        strParameterModuleValue = clsSecurity.Encrypt(strParameterModuleValue, strKey);

        //        //MsgBox("1 : " & strParameterModuleKey) 
        //        //MsgBox("2 : " & strParameterModuleValue) 
        //        clsDataOperation objDataOprn = new clsDataOperation();
        //        DataSet ds = new DataSet();

        //        ds = objDataOprn.ExecQuery("SELECT * FROM cfparameter WHERE key_name = '" + strParameterModuleKey + "'", "Data");
        //        if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
        //        {
        //            eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
        //            return false;
        //        }

        //        if (ds.Tables["Data"].Rows.Count > 0)
        //        {
        //            //MsgBox("3.1 : " & strParameterModuleValue) 
        //            strParameterModuleValue = ds.Tables["Data"].Rows[0]["key_value"].ToString();
        //            strParameterModuleValue = clsSecurity.Decrypt(strParameterModuleValue, strKey);
        //        }
        //        else
        //        {
        //            //MsgBox("3 : " & strParameterModuleValue) 
        //            //MsgBox("4.1 : " & "INSERT INTO cfparameter(key_name,key_value) VALUES('" & strParameterModuleKey & "','" & strParameterModuleValue & "')") 
        //            objDataOprn.ExecNonQuery("INSERT INTO cfparameter(key_name,key_value) SELECT '" + strParameterModuleKey + "','" + strParameterModuleValue + "' WHERE NOT EXISTS (SELECT parameterunkid FROM cfparameter WHERE key_name = '" + strParameterModuleKey + "')");
        //            if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
        //            {
        //                eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
        //                return false;
        //            }
        //            strParameterModuleValue = clsSecurity.Decrypt(strParameterModuleValue, strKey);
        //        }
        //        //MsgBox("4 : " & strParameterModuleValue) 

        //        //MsgBox("5.1 : " & strParameterModuleValue) 
        //        char[] arrChr = strParameterModuleValue.ToCharArray();
        //        arrChr[intModuleId] = (blnActivationFlag ? '1' : '0');
        //        strParameterModuleValue = new string(arrChr);
        //        strParameterModuleValue = clsSecurity.Encrypt(strParameterModuleValue, strKey);

        //        //MsgBox("5 : " & strParameterModuleValue) 

        //        objDataOprn.ExecNonQuery("UPDATE cfparameter SET key_value = '" + strParameterModuleValue + "' WHERE key_name = '" + strParameterModuleKey + "'");
        //        if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
        //        {
        //            eZeeMsgBox.Show(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
        //            return false;
        //        }
        //        //MsgBox("6 : " & strParameterModuleValue) 
        //        return true;
        //    }
        //    catch 
        //    {
        //        //MsgBox(ex.Message) 
        //        return false;
        //    }
        //}

        private void LogOperation(string strOperation, string strDescription, int intTriggerEvent, int intTriggerData)
        {
            try
            {
                if (lf.GetUserDate(1) != "0/0/0") return;

                string strQry = "INSERT INTO cfdailytax_detail " + "(operation,description,dateofoperation,sessionid,machineid,hotelname," + "noofrooms,noofpda,edition,licensekey,roomkey,softdate,harddate,licensetype,isdemo,isexpire,triggerevent,triggerdata) " + "VALUES(@operation,@description,@dateofoperation,@sessionid,@machineid,@hotelname," + "@noofrooms,@noofpda,@edition,@licensekey,@roomkey,@softdate,@harddate,@licensetype,@isdemo,@isexpire,@triggerevent,@triggerdata)";
                clsDataOperation objDataOprn = new clsDataOperation();

                objDataOprn.ClearParameters();
                objDataOprn.AddParameter("@operation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strOperation);
                objDataOprn.AddParameter("@description", SqlDbType.NText, eZeeDataType.DESC_SIZE, strDescription);
                objDataOprn.AddParameter("@dateofoperation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, DateTime.Now.ToString());
                objDataOprn.AddParameter("@sessionid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSessionId);
                objDataOprn.AddParameter("@machineid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachineId);
                objDataOprn.AddParameter("@hotelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, lf.GetUserString(1));
                objDataOprn.AddParameter("@noofrooms", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, lf.GetUserNumber(1));
                objDataOprn.AddParameter("@noofpda", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, lf.GetUserNumber(2));
                objDataOprn.AddParameter("@edition", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, lf.GetUserNumber(3));
                objDataOprn.AddParameter("@licensekey", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLicenseKey);
                objDataOprn.AddParameter("@roomkey", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRoomKey);
                objDataOprn.AddParameter("@softdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, lf.ExpireDateSoft + " " + DateTime.Now.ToString("hh:mm:ss tt"));
                objDataOprn.AddParameter("@harddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, lf.ExpireDateHard + " " + DateTime.Now.ToString("hh:mm:ss tt"));
                objDataOprn.AddParameter("@licensetype", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, lf.ExpireMode);
                objDataOprn.AddParameter("@isdemo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IsDemo);
                objDataOprn.AddParameter("@isexpire", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IsExpire);
                objDataOprn.AddParameter("@triggerevent", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intTriggerEvent);
                objDataOprn.AddParameter("@triggerdata", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intTriggerData);

                //objDataOprn.ExecNonQuery(strQry);
                //if (!string.IsNullOrEmpty(objDataOprn.ErrorMessage))
                //{
                   // throw new Exception(objDataOprn.ErrorNumber + " " + objDataOprn.ErrorMessage);
               // }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ValidateRoomKey(string intRoomKey)
        {
            if (mstrHotelName == "" || intRoomKey == "") return -1;
            string r = intRoomKey.Substring(intRoomKey.Length-Conversion.Val(intRoomKey[0]), Conversion.Val(intRoomKey[0]));
            string strR = "";
            string arr1 = "9183627540";
            foreach (char c1 in r)
            {
                strR += arr1[Conversion.Val(c1)];
            }
            strR = Strings.StrReverse(strR);
            string strHash = intRoomKey.Remove(0, 1).Remove(intRoomKey.Length - Conversion.Val(intRoomKey[0]) - 1, Conversion.Val(intRoomKey[0]));
            string series = "ZrE0CxAnB4pD3FiMsd9Ukq1tR JzXj7mIPyuVQeH6OaSv2gWlYc8NbTwfKhLoG5";
            long sum = 0;
            int idx = 0;
            int i = 0;
            String strProperty = mstrHotelName;
            //if (menApplication == enApplicationType.FD)
            //{
            //    strProperty += "FD";
            //}
            //else
            //{
            //    strProperty += "POS";
            //}
            foreach (char c in strProperty)
            {
                idx = series.IndexOf(c);
                if (idx > 0)
                {
                    sum += idx * i;
                }
                else
                {
                    sum += i;
                }
                i += 1;
            }
            long k = Convert.ToInt64(Strings.StrReverse(mstrMachineId)) + Convert.ToInt64(sum.ToString().PadRight(mstrMachineId.Length - 2, '0')) + Convert.ToInt64(strR);
            k = Convert.ToInt64(k.ToString().Substring(2));
            if (strHash == k.ToString())
            {
                return Convert.ToInt32(strR);
            }
            else
            {
                return -1;
            }
        }

        private string Unicode2Ascii(String param)
        {
            try
            {
                string ascii = "";
                foreach (char c in param.ToCharArray())
                {
                    ascii += (char)(c % 255);
                }
                return ascii;
            }
            catch
            {
                return "13061985";
            }
        }
        #endregion

    }
}
