﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections;
    using System.Globalization;
    using System.Reflection;
    using System.Xml;

    public sealed class WorksheetStyleCollection : CollectionBase, IWriter, IReader, ICodeWriter
    {
        internal Workbook _book;

        internal WorksheetStyleCollection(Workbook book)
        {
            if (book == null)
            {
                throw new ArgumentNullException("book");
            }
            this._book = book;
        }

        public int Add(WorksheetStyle style)
        {
            if (style == null)
            {
                throw new ArgumentNullException("style");
            }
            style._book = this._book;
            return base.InnerList.Add(style);
        }

        public WorksheetStyle Add(string id)
        {
            WorksheetStyle style = new WorksheetStyle(id);
            this.Add(style);
            return style;
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            CodeMemberMethod method2 = new CodeMemberMethod();
            method2.Name = "GenerateStyles";
            method2.Parameters.Add(new CodeParameterDeclarationExpression(typeof(WorksheetStyleCollection), "styles"));
            Util.AddComment(method, "Generate Styles");
            method.Statements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), method2.Name, new CodeExpression[] { targetObject }));
            type.Members.Add(method2);
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                WorksheetStyle style = this[i];
                string name = Util.CreateSafeName(style.ID, "style");
                CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(WorksheetStyle), name, new CodeMethodInvokeExpression(new CodeVariableReferenceExpression("styles"), "Add", new CodeExpression[] { new CodePrimitiveExpression(style.ID) }));
                Util.AddComment(method2, style.ID);
                method2.Statements.Add(statement);
                ((ICodeWriter) style).WriteTo(type, method2, new CodeVariableReferenceExpression(name));
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if ((element2 != null) && WorksheetStyle.IsElement(element2))
                {
                    WorksheetStyle style = new WorksheetStyle(null);
                    ((IReader) style).ReadXml(element2);
                    this.Add(style);
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Styles", "urn:schemas-microsoft-com:office:spreadsheet");
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public bool Contains(WorksheetStyle item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(WorksheetStyle[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(WorksheetStyle item)
        {
            return base.InnerList.IndexOf(item);
        }

        public int IndexOf(string id)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                if (string.Compare(((WorksheetStyle) base.InnerList[i]).ID, id, true, CultureInfo.InvariantCulture) == 0)
                {
                    return i;
                }
            }
            return -1;
        }

        public void Insert(int index, WorksheetStyle item)
        {
            base.InnerList.Insert(index, item);
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Styles", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public void Remove(WorksheetStyle item)
        {
            base.InnerList.Remove(item);
        }

        public WorksheetStyle this[string id]
        {
            get
            {
                int index = this.IndexOf(id);
                if (index == -1)
                {
                    throw new ArgumentException("The specified style " + id + " does not exists in the collection");
                }
                return (WorksheetStyle) base.InnerList[index];
            }
        }

        public WorksheetStyle this[int index]
        {
            get
            {
                return (WorksheetStyle) base.InnerList[index];
            }
            set
            {
                base.InnerList[index] = value;
            }
        }
    }
}

