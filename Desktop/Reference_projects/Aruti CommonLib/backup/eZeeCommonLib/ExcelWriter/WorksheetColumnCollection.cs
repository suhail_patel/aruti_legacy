﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections;
    using System.Globalization;
    using System.Reflection;
    using System.Xml;

    public sealed class WorksheetColumnCollection : CollectionBase, IWriter, ICodeWriter
    {
        private WorksheetTable _table;

        internal WorksheetColumnCollection(WorksheetTable table)
        {
            if (table == null)
            {
                throw new ArgumentNullException("table");
            }
            this._table = table;
        }

        public WorksheetColumn Add()
        {
            WorksheetColumn column = new WorksheetColumn();
            this.Add(column);
            return column;
        }

        public int Add(WorksheetColumn column)
        {
            if (column == null)
            {
                throw new ArgumentNullException("column");
            }
            column._table = this._table;
            return base.InnerList.Add(column);
        }

        public WorksheetColumn Add(int width)
        {
            WorksheetColumn column = new WorksheetColumn();
            column.Width = width;
            this.Add(column);
            return column;
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                WorksheetColumn column = this[i];
                if (column.IsSimple)
                {
                    method.Statements.Add(new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[] { new CodePrimitiveExpression(column.Width) }));
                }
                else
                {
                    string name = "column" + i.ToString();
                    CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(WorksheetColumn), name, new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[0]));
                    method.Statements.Add(statement);
                    ((ICodeWriter) column).WriteTo(type, method, new CodeVariableReferenceExpression(name));
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(WorksheetColumn item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(WorksheetColumn[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(WorksheetColumn item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, WorksheetColumn item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(WorksheetColumn item)
        {
            base.InnerList.Remove(item);
        }

        public WorksheetColumn this[int index]
        {
            get
            {
                return (WorksheetColumn) base.InnerList[index];
            }
        }
    }
}

