﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Xml;

    public sealed class WorksheetStyle : IWriter, IReader, ICodeWriter
    {
        private WorksheetStyleAlignment _alignment;
        internal ExcelWriter.Workbook _book;
        private WorksheetStyleBorderCollection _borders;
        private WorksheetStyleFont _font;
        private string _id;
        private WorksheetStyleInterior _interior;
        private string _name;
        private string _numberFormat = "General";
        private string _parent;

        public WorksheetStyle(string id)
        {
            this._id = id;
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._name != null)
            {
                Util.AddAssignment(method, targetObject, "Name", this._name);
            }
            if (this._parent != null)
            {
                Util.AddAssignment(method, targetObject, "Parent", this._parent);
            }
            if (this._font != null)
            {
                Util.Traverse(type, this._font, method, targetObject, "Font");
            }
            if (this._interior != null)
            {
                Util.Traverse(type, this._interior, method, targetObject, "Interior");
            }
            if (this._alignment != null)
            {
                Util.Traverse(type, this._alignment, method, targetObject, "Alignment");
            }
            if (this._borders != null)
            {
                Util.Traverse(type, this._borders, method, targetObject, "Borders");
            }
            if (this._numberFormat != "General")
            {
                Util.AddAssignment(method, targetObject, "NumberFormat", this._numberFormat);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._id = Util.GetAttribute(element, "ID", "urn:schemas-microsoft-com:office:spreadsheet");
            this._name = Util.GetAttribute(element, "Name", "urn:schemas-microsoft-com:office:spreadsheet");
            this._parent = Util.GetAttribute(element, "Parent", "urn:schemas-microsoft-com:office:spreadsheet");
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (WorksheetStyleFont.IsElement(element2))
                    {
                        ((IReader) this.Font).ReadXml(element2);
                    }
                    else
                    {
                        if (WorksheetStyleInterior.IsElement(element2))
                        {
                            ((IReader) this.Interior).ReadXml(element2);
                            continue;
                        }
                        if (WorksheetStyleAlignment.IsElement(element2))
                        {
                            ((IReader) this.Alignment).ReadXml(element2);
                            continue;
                        }
                        if (WorksheetStyleBorderCollection.IsElement(element2))
                        {
                            ((IReader) this.Borders).ReadXml(element2);
                            continue;
                        }
                        if (Util.IsElement(element2, "NumberFormat", "urn:schemas-microsoft-com:office:spreadsheet"))
                        {
                            string attribute = element2.GetAttribute("Format", "urn:schemas-microsoft-com:office:spreadsheet");
                            if ((attribute != null) && (attribute.Length > 0))
                            {
                                this._numberFormat = attribute;
                            }
                        }
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Style", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._id != null)
            {
                writer.WriteAttributeString("ID", "urn:schemas-microsoft-com:office:spreadsheet", this._id);
            }
            if (this._name != null)
            {
                writer.WriteAttributeString("Name", "urn:schemas-microsoft-com:office:spreadsheet", this._name);
            }
            if (this._parent != null)
            {
                writer.WriteAttributeString("Parent", "urn:schemas-microsoft-com:office:spreadsheet", this._parent);
            }
            if (this._alignment != null)
            {
                ((IWriter) this._alignment).WriteXml(writer);
            }
            if (this._borders != null)
            {
                ((IWriter) this._borders).WriteXml(writer);
            }
            if (this._font != null)
            {
                ((IWriter) this._font).WriteXml(writer);
            }
            if (this._interior != null)
            {
                ((IWriter) this._interior).WriteXml(writer);
            }
            if (this._numberFormat != "General")
            {
                writer.WriteStartElement("NumberFormat", "urn:schemas-microsoft-com:office:spreadsheet");
                writer.WriteAttributeString("s", "Format", "urn:schemas-microsoft-com:office:spreadsheet", this._numberFormat);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Style", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public WorksheetStyleAlignment Alignment
        {
            get
            {
                if (this._alignment == null)
                {
                    this._alignment = new WorksheetStyleAlignment();
                }
                return this._alignment;
            }
        }

        public WorksheetStyleBorderCollection Borders
        {
            get
            {
                if (this._borders == null)
                {
                    this._borders = new WorksheetStyleBorderCollection(this);
                }
                return this._borders;
            }
        }

        public WorksheetStyleFont Font
        {
            get
            {
                if (this._font == null)
                {
                    this._font = new WorksheetStyleFont();
                }
                return this._font;
            }
        }

        public string ID
        {
            get
            {
                return this._id;
            }
        }

        public WorksheetStyleInterior Interior
        {
            get
            {
                if (this._interior == null)
                {
                    this._interior = new WorksheetStyleInterior();
                }
                return this._interior;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public string NumberFormat
        {
            get
            {
                return this._numberFormat;
            }
            set
            {
                this._numberFormat = value;
            }
        }

        public string Parent
        {
            get
            {
                return this._parent;
            }
            set
            {
                this._parent = value;
            }
        }

        public ExcelWriter.Workbook Workbook
        {
            get
            {
                return this._book;
            }
        }
    }
}

