﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections;
    using System.Globalization;
    using System.Reflection;
    using System.Xml;

    public sealed class WorksheetStyleBorderCollection : CollectionBase, IWriter, IReader, ICodeWriter
    {
        private WorksheetStyle _style;

        internal WorksheetStyleBorderCollection(WorksheetStyle style)
        {
            this._style = style;
        }

        public WorksheetStyleBorder Add()
        {
            WorksheetStyleBorder border = new WorksheetStyleBorder();
            this.Add(border);
            return border;
        }

        public int Add(WorksheetStyleBorder border)
        {
            return base.InnerList.Add(border);
        }

        public WorksheetStyleBorder Add(StylePosition position, LineStyleOption lineStyle)
        {
            return this.Add(position, lineStyle, 0, null);
        }

        public WorksheetStyleBorder Add(StylePosition position, LineStyleOption lineStyle, int weight)
        {
            return this.Add(position, lineStyle, weight, null);
        }

        public WorksheetStyleBorder Add(StylePosition position, LineStyleOption lineStyle, int weight, string color)
        {
            WorksheetStyleBorder border = new WorksheetStyleBorder();
            border.Position = position;
            border.LineStyle = lineStyle;
            border.Weight = weight;
            border.Color = color;
            this.Add(border);
            return border;
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                WorksheetStyleBorder border = this[i];
                switch (border.IsSpecial())
                {
                    case 0:
                        method.Statements.Add(new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[] { Util.GetRightExpressionForValue(border.Position, typeof(StylePosition)), Util.GetRightExpressionForValue(border.LineStyle, typeof(LineStyleOption)), new CodePrimitiveExpression(border.Weight) }));
                        break;

                    case 1:
                        method.Statements.Add(new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[] { Util.GetRightExpressionForValue(border.Position, typeof(StylePosition)), Util.GetRightExpressionForValue(border.LineStyle, typeof(LineStyleOption)), new CodePrimitiveExpression(border.Weight), new CodePrimitiveExpression(border.Color) }));
                        break;

                    default:
                    {
                        string name = this._style.ID + "Border" + i.ToString();
                        CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(WorksheetStyleBorder), name, new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[0]));
                        method.Statements.Add(statement);
                        ((ICodeWriter) border).WriteTo(type, method, new CodeVariableReferenceExpression(name));
                        break;
                    }
                }
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if ((element2 != null) && WorksheetStyleBorder.IsElement(element2))
                {
                    WorksheetStyleBorder border = new WorksheetStyleBorder();
                    ((IReader) border).ReadXml(element2);
                    this.Add(border);
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Borders", "urn:schemas-microsoft-com:office:spreadsheet");
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public bool Contains(WorksheetStyleBorder item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(WorksheetStyleBorder[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(WorksheetStyleBorder item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, WorksheetStyleBorder border)
        {
            base.InnerList.Insert(index, border);
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Borders", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public void Remove(WorksheetStyleBorder border)
        {
            base.InnerList.Remove(border);
        }

        public object[] ToArray()
        {
            return base.InnerList.ToArray();
        }

        public WorksheetStyleBorder this[int index]
        {
            get
            {
                return (WorksheetStyleBorder) base.InnerList[index];
            }
            set
            {
                base.InnerList[index] = value;
            }
        }
    }
}

