﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetStyleFont : IWriter, IReader, ICodeWriter
    {
        private bool _bold;
        private string _color;
        private string _fontName;
        private bool _italic;
        private int _size;
        private bool _strikethrough;
        private UnderlineStyle _underline;

        internal WorksheetStyleFont()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._bold)
            {
                Util.AddAssignment(method, targetObject, "Bold", this._bold);
            }
            if (this._italic)
            {
                Util.AddAssignment(method, targetObject, "Italic", this._italic);
            }
            if (this._underline != UnderlineStyle.None)
            {
                Util.AddAssignment(method, targetObject, "Underline", this._underline);
            }
            if (this._strikethrough)
            {
                Util.AddAssignment(method, targetObject, "StrikeThrough", this._strikethrough);
            }
            if (this._fontName != null)
            {
                Util.AddAssignment(method, targetObject, "FontName", this._fontName);
            }
            if (this._size != 0)
            {
                Util.AddAssignment(method, targetObject, "Size", this._size);
            }
            if (this._color != null)
            {
                Util.AddAssignment(method, targetObject, "Color", this._color);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._bold = element.GetAttribute("Bold", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
            this._italic = element.GetAttribute("Italic", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
            string attribute = element.GetAttribute("Underline", "urn:schemas-microsoft-com:office:spreadsheet");
            if ((attribute != null) && (attribute.Length != 0))
            {
                this._underline = (UnderlineStyle) Enum.Parse(typeof(UnderlineStyle), attribute, true);
            }
            this._strikethrough = element.GetAttribute("StrikeThrough", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
            this._fontName = Util.GetAttribute(element, "FontName", "urn:schemas-microsoft-com:office:spreadsheet");
            this._size = Util.GetAttribute(element, "Size", "urn:schemas-microsoft-com:office:spreadsheet", 0);
            this._color = Util.GetAttribute(element, "Color", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Font", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._bold)
            {
                writer.WriteAttributeString("Bold", "urn:schemas-microsoft-com:office:spreadsheet", "1");
            }
            if (this._italic)
            {
                writer.WriteAttributeString("Italic", "urn:schemas-microsoft-com:office:spreadsheet", "1");
            }
            if (this._underline != UnderlineStyle.None)
            {
                writer.WriteAttributeString("Underline", "urn:schemas-microsoft-com:office:spreadsheet", this._underline.ToString());
            }
            if (this._strikethrough)
            {
                writer.WriteAttributeString("StrikeThrough", "urn:schemas-microsoft-com:office:spreadsheet", "1");
            }
            if (this._fontName != null)
            {
                writer.WriteAttributeString("FontName", "urn:schemas-microsoft-com:office:spreadsheet", this._fontName.ToString());
            }
            if (this._size != 0)
            {
                writer.WriteAttributeString("Size", "urn:schemas-microsoft-com:office:spreadsheet", this._size.ToString());
            }
            if (this._color != null)
            {
                writer.WriteAttributeString("Color", "urn:schemas-microsoft-com:office:spreadsheet", this._color);
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Font", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public bool Bold
        {
            get
            {
                return this._bold;
            }
            set
            {
                this._bold = value;
            }
        }

        public string Color
        {
            get
            {
                return this._color;
            }
            set
            {
                this._color = value;
            }
        }

        public string FontName
        {
            get
            {
                return this._fontName;
            }
            set
            {
                this._fontName = value;
            }
        }

        public bool Italic
        {
            get
            {
                return this._italic;
            }
            set
            {
                this._italic = value;
            }
        }

        public int Size
        {
            get
            {
                return this._size;
            }
            set
            {
                this._size = value;
            }
        }

        public bool Strikethrough
        {
            get
            {
                return this._strikethrough;
            }
            set
            {
                this._strikethrough = value;
            }
        }

        public UnderlineStyle Underline
        {
            get
            {
                return this._underline;
            }
            set
            {
                this._underline = value;
            }
        }
    }
}

