﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections;
    using System.Globalization;
    using System.Reflection;
    using System.Xml;

    public sealed class XctCollection : CollectionBase, IWriter, ICodeWriter
    {
        internal XctCollection()
        {
        }

        public Xct Add()
        {
            Xct item = new Xct();
            this.Add(item);
            return item;
        }

        public int Add(Xct item)
        {
            return base.InnerList.Add(item);
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                Xct xct = this[i];
                string name = "Xct" + i.ToString();
                CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(Xct), name, new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "References"), "Add", new CodeExpression[0]));
                method.Statements.Add(statement);
                ((ICodeWriter) xct).WriteTo(type, method, new CodeVariableReferenceExpression(name));
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(Xct link)
        {
            return base.InnerList.Contains(link);
        }

        public void CopyTo(Xct[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(Xct item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, Xct item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(string item)
        {
            base.InnerList.Remove(item);
        }

        public Xct this[int index]
        {
            get
            {
                return (Xct) base.InnerList[index];
            }
        }
    }
}

