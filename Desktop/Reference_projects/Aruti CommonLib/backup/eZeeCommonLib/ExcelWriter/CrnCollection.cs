﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections;
    using System.Globalization;
    using System.Reflection;
    using System.Xml;

    public sealed class CrnCollection : CollectionBase, IWriter, ICodeWriter
    {
        internal static int GlobalCounter;

        internal CrnCollection()
        {
        }

        public Crn Add()
        {
            Crn item = new Crn();
            this.Add(item);
            return item;
        }

        public int Add(Crn item)
        {
            return base.InnerList.Add(item);
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                Crn crn = this[i];
                string name = "Crn" + GlobalCounter++.ToString();
                CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(Crn), name, new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "Operands"), "Add", new CodeExpression[0]));
                method.Statements.Add(statement);
                ((ICodeWriter) crn).WriteTo(type, method, new CodeVariableReferenceExpression(name));
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(Crn link)
        {
            return base.InnerList.Contains(link);
        }

        public void CopyTo(Crn[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(Crn item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, Crn item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(string item)
        {
            base.InnerList.Remove(item);
        }

        public Crn this[int index]
        {
            get
            {
                return (Crn) base.InnerList[index];
            }
        }
    }
}

