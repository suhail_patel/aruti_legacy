﻿namespace ExcelWriter
{
    using System;
    using System.Globalization;
    using System.Xml;

    public sealed class PivotField : IWriter
    {
        private string _dataField;
        private ExcelWriter.DataType _dataType;
        private PTFunction _function;
        private string _name;
        private PivotFieldOrientation _orientation = PivotFieldOrientation.NotSet;
        private string _parentField;
        private PivotItemCollection _pivotItems;
        private int _position = -2147483648;

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "PivotField", "urn:schemas-microsoft-com:office:excel");
            if (this._dataField != null)
            {
                writer.WriteElementString("DataField", "urn:schemas-microsoft-com:office:excel", this._dataField);
            }
            if (this._name != null)
            {
                writer.WriteElementString("Name", "urn:schemas-microsoft-com:office:excel", this._name);
            }
            if (this._parentField != null)
            {
                writer.WriteElementString("ParentField", "urn:schemas-microsoft-com:office:excel", this._parentField);
            }
            if (this._dataType != ExcelWriter.DataType.NotSet)
            {
                writer.WriteElementString("DataType", "urn:schemas-microsoft-com:office:excel", this._dataType.ToString());
            }
            if (this._function != PTFunction.NotSet)
            {
                writer.WriteElementString("Function", "urn:schemas-microsoft-com:office:excel", this._function.ToString());
            }
            if (this._position != -2147483648)
            {
                writer.WriteElementString("Position", "urn:schemas-microsoft-com:office:excel", this._position.ToString());
            }
            if (this._orientation != PivotFieldOrientation.NotSet)
            {
                writer.WriteElementString("Orientation", "urn:schemas-microsoft-com:office:excel", this._orientation.ToString());
            }
            if (this._pivotItems != null)
            {
                ((IWriter) this._pivotItems).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public string DataField
        {
            get
            {
                return this._dataField;
            }
            set
            {
                this._dataField = value;
            }
        }

        public ExcelWriter.DataType DataType
        {
            get
            {
                return this._dataType;
            }
            set
            {
                this._dataType = value;
            }
        }

        public PTFunction Function
        {
            get
            {
                return this._function;
            }
            set
            {
                this._function = value;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public PivotFieldOrientation Orientation
        {
            get
            {
                return this._orientation;
            }
            set
            {
                this._orientation = value;
            }
        }

        public string ParentField
        {
            get
            {
                return this._parentField;
            }
            set
            {
                this._parentField = value;
            }
        }

        public PivotItemCollection PivotItems
        {
            get
            {
                if (this._pivotItems == null)
                {
                    this._pivotItems = new PivotItemCollection();
                }
                return this._pivotItems;
            }
        }

        public int Position
        {
            get
            {
                return this._position;
            }
            set
            {
                this._position = value;
            }
        }
    }
}

