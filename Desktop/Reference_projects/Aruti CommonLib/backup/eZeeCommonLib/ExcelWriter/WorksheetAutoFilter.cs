﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Xml;

    public sealed class WorksheetAutoFilter : IReader, IWriter, ICodeWriter
    {
        private string _range;

        internal WorksheetAutoFilter()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._range != null)
            {
                Util.AddAssignment(method, targetObject, "Range", this._range);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._range = Util.GetAttribute(element, "Range", "urn:schemas-microsoft-com:office:excel");
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "AutoFilter", "urn:schemas-microsoft-com:office:excel");
            if (this._range != null)
            {
                writer.WriteAttributeString("Range", "urn:schemas-microsoft-com:office:excel", this._range);
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "AutoFilter", "urn:schemas-microsoft-com:office:excel");
        }

        public string Range
        {
            get
            {
                return this._range;
            }
            set
            {
                this._range = value;
            }
        }
    }
}

