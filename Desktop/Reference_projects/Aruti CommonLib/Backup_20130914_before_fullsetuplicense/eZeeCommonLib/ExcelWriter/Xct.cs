﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class Xct : IWriter, IReader, ICodeWriter
    {
        private CrnCollection _operands;
        private int _sheetIndex = -2147483648;

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._sheetIndex != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "SheetIndex", this._sheetIndex);
            }
            if (this._operands != null)
            {
                ((ICodeWriter) this._operands).WriteTo(type, method, targetObject);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (Util.IsElement(element2, "SheetIndex", "urn:schemas-microsoft-com:office:excel"))
                    {
                        this._sheetIndex = int.Parse(element2.InnerText);
                    }
                    else if (Crn.IsElement(element2))
                    {
                        Crn item = new Crn();
                        ((IReader) item).ReadXml(element2);
                        this.Operands.Add(item);
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "Xct", "urn:schemas-microsoft-com:office:excel");
            if (this._operands != null)
            {
                writer.WriteElementString("Count", "urn:schemas-microsoft-com:office:excel", this._operands.Count.ToString());
            }
            else
            {
                writer.WriteElementString("Count", "urn:schemas-microsoft-com:office:excel", "0");
            }
            if (this._sheetIndex != -2147483648)
            {
                writer.WriteElementString("SheetIndex", "urn:schemas-microsoft-com:office:excel", this._sheetIndex.ToString());
            }
            if (this._operands != null)
            {
                ((IWriter) this._operands).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Xct", "urn:schemas-microsoft-com:office:excel");
        }

        public CrnCollection Operands
        {
            get
            {
                if (this._operands == null)
                {
                    this._operands = new CrnCollection();
                }
                return this._operands;
            }
        }

        public int SheetIndex
        {
            get
            {
                if (this._sheetIndex == -2147483648)
                {
                    return 0;
                }
                return this._sheetIndex;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Invalid range, > 0");
                }
                this._sheetIndex = value;
            }
        }
    }
}

