﻿namespace ExcelWriter
{
    using System;
    using System.Xml;

    public sealed class PivotTable : IWriter
    {
        private string _defaultVersion;
        private string _location;
        private string _name;
        private PivotFieldCollection _pivotFields;
        private PTLineItemCollection _pTLinesItems;
        private PTSource _pTSource;
        private string _versionLastUpdate;

        internal PivotTable()
        {
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "PivotTable", "urn:schemas-microsoft-com:office:excel");
            if (this._name != null)
            {
                writer.WriteElementString("Name", "urn:schemas-microsoft-com:office:excel", this._name);
            }
            writer.WriteStartElement("x", "ImmediateItemsOnDrop", "urn:schemas-microsoft-com:office:excel");
            writer.WriteEndElement();
            writer.WriteStartElement("x", "ShowPageMultipleItemLabel", "urn:schemas-microsoft-com:office:excel");
            writer.WriteEndElement();
            if (this._location != null)
            {
                writer.WriteElementString("Location", "urn:schemas-microsoft-com:office:excel", this._location);
            }
            if (this._defaultVersion != null)
            {
                writer.WriteElementString("DefaultVersion", "urn:schemas-microsoft-com:office:excel", this._defaultVersion);
            }
            if (this._versionLastUpdate != null)
            {
                writer.WriteElementString("VersionLastUpdate", "urn:schemas-microsoft-com:office:excel", this._versionLastUpdate);
            }
            if (this._pivotFields != null)
            {
                ((IWriter) this._pivotFields).WriteXml(writer);
            }
            if (this._pTLinesItems != null)
            {
                ((IWriter) this._pTLinesItems).WriteXml(writer);
            }
            if (this._pTSource != null)
            {
                ((IWriter) this._pTSource).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public string DefaultVersion
        {
            get
            {
                return this._defaultVersion;
            }
            set
            {
                this._defaultVersion = value;
            }
        }

        public PTLineItemCollection LineItems
        {
            get
            {
                if (this._pTLinesItems == null)
                {
                    this._pTLinesItems = new PTLineItemCollection();
                }
                return this._pTLinesItems;
            }
        }

        public string Location
        {
            get
            {
                return this._location;
            }
            set
            {
                this._location = value;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public PivotFieldCollection PivotFields
        {
            get
            {
                if (this._pivotFields == null)
                {
                    this._pivotFields = new PivotFieldCollection();
                }
                return this._pivotFields;
            }
        }

        public PTSource Source
        {
            get
            {
                if (this._pTSource == null)
                {
                    this._pTSource = new PTSource();
                }
                return this._pTSource;
            }
        }

        public string VersionLastUpdate
        {
            get
            {
                return this._versionLastUpdate;
            }
            set
            {
                this._versionLastUpdate = value;
            }
        }
    }
}

