﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetPageMargins : IWriter, IReader, ICodeWriter
    {
        private float _bottom = 1f;
        private float _left = 0.75f;
        private float _right = 0.75f;
        private float _top = 1f;

        internal WorksheetPageMargins()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            Util.AddAssignment(method, targetObject, "Bottom", this._bottom);
            Util.AddAssignment(method, targetObject, "Left", this._left);
            Util.AddAssignment(method, targetObject, "Right", this._right);
            Util.AddAssignment(method, targetObject, "Top", this._top);
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._bottom = Util.GetAttribute(element, "Bottom", "urn:schemas-microsoft-com:office:excel", (float) 1f);
            this._left = Util.GetAttribute(element, "Left", "urn:schemas-microsoft-com:office:excel", (float) 0.75f);
            this._right = Util.GetAttribute(element, "Right", "urn:schemas-microsoft-com:office:excel", (float) 0.75f);
            this._top = Util.GetAttribute(element, "Top", "urn:schemas-microsoft-com:office:excel", (float) 1f);
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "PageMargins", "urn:schemas-microsoft-com:office:excel");
            writer.WriteAttributeString("Bottom", "urn:schemas-microsoft-com:office:excel", this._bottom.ToString());
            writer.WriteAttributeString("Left", "urn:schemas-microsoft-com:office:excel", this._left.ToString());
            writer.WriteAttributeString("Right", "urn:schemas-microsoft-com:office:excel", this._right.ToString());
            writer.WriteAttributeString("Top", "urn:schemas-microsoft-com:office:excel", this._top.ToString());
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "PageMargins", "urn:schemas-microsoft-com:office:excel");
        }

        public float Bottom
        {
            get
            {
                return this._bottom;
            }
            set
            {
                this._bottom = value;
            }
        }

        public float Left
        {
            get
            {
                return this._left;
            }
            set
            {
                this._left = value;
            }
        }

        public float Right
        {
            get
            {
                return this._right;
            }
            set
            {
                this._right = value;
            }
        }

        public float Top
        {
            get
            {
                return this._top;
            }
            set
            {
                this._top = value;
            }
        }
    }
}

