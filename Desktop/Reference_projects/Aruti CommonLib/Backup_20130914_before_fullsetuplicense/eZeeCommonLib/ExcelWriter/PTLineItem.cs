﻿namespace ExcelWriter
{
    using System;
    using System.Globalization;
    using System.Xml;

    public sealed class PTLineItem : IWriter
    {
        private string _item;
        private ExcelWriter.ItemType _itemType;

        public PTLineItem()
        {
        }

        public PTLineItem(string item)
        {
            this._item = item;
        }

        public PTLineItem(string item, ExcelWriter.ItemType itemType)
        {
            this._item = item;
            this._itemType = itemType;
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "PTLineItem", "urn:schemas-microsoft-com:office:excel");
            writer.WriteElementString("Item", "urn:schemas-microsoft-com:office:excel", this._item);
            if (this._itemType != ExcelWriter.ItemType.NotSet)
            {
                writer.WriteElementString("ItemType", "urn:schemas-microsoft-com:office:excel", this._itemType.ToString());
            }
            writer.WriteEndElement();
        }

        public string Item
        {
            get
            {
                return this._item;
            }
            set
            {
                this._item = value;
            }
        }

        public ExcelWriter.ItemType ItemType
        {
            get
            {
                return this._itemType;
            }
            set
            {
                this._itemType = value;
            }
        }
    }
}

