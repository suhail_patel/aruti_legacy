﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetComment : IWriter, IReader, ICodeWriter
    {
        private string _author;
        private WorksheetCellData _data;
        private bool _showAlways;

        internal WorksheetComment()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._showAlways)
            {
                Util.AddAssignment(method, targetObject, "ShowAlways", this._showAlways);
            }
            if (this._author != null)
            {
                Util.AddAssignment(method, targetObject, "Author", this._author);
            }
            if (this._data != null)
            {
                Util.Traverse(type, this._data, method, targetObject, "Data");
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._showAlways = Util.GetAttribute(element, "ShowAlways", "urn:schemas-microsoft-com:office:spreadsheet", false);
            this._author = Util.GetAttribute(element, "Author", "urn:schemas-microsoft-com:office:spreadsheet");
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if ((element2 != null) && WorksheetCellData.IsElement(element2))
                {
                    ((IReader) this.Data).ReadXml(element2);
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Comment", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._showAlways)
            {
                writer.WriteAttributeString("s", "ShowAlways", "urn:schemas-microsoft-com:office:spreadsheet", this._showAlways.ToString());
            }
            if (this._author != null)
            {
                writer.WriteAttributeString("s", "Author", "urn:schemas-microsoft-com:office:spreadsheet", this._author);
            }
            if (this._data != null)
            {
                ((IWriter) this._data).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Comment", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public string Author
        {
            get
            {
                return this._author;
            }
            set
            {
                this._author = value;
            }
        }

        public WorksheetCellData Data
        {
            get
            {
                if (this._data == null)
                {
                    this._data = new WorksheetCellData(this);
                }
                return this._data;
            }
        }

        public bool ShowAlways
        {
            get
            {
                return this._showAlways;
            }
            set
            {
                this._showAlways = value;
            }
        }
    }
}

