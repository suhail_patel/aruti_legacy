﻿namespace ExcelWriter.Schemas
{
    using ExcelWriter;
    using System;
    using System.Xml;

    public sealed class Schema : IWriter
    {
        private SchemaTypeCollection _types;

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Schema", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
            if (this._types != null)
            {
                ((IWriter) this._types).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public SchemaTypeCollection Types
        {
            get
            {
                if (this._types == null)
                {
                    this._types = new SchemaTypeCollection();
                }
                return this._types;
            }
        }
    }
}

