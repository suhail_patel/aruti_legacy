using System.Globalization;
using System;
namespace eZeeCommonLib
{
    public class eZeeDate
    {
        /// <summary>
        /// Convert system date in Database Date format(YYYYMMDD).
        /// </summary>
        /// <param name="Date">Date in System Format.</param> 
        /// <returns>Date as string in database date Format(YYYYMMDD).</returns>
        public static string convertDate(System.DateTime Date)
        {
            return string.Format("{0:0000}{1:00}{2:00}", Date.Year, Date.Month, Date.Day);
        }

        /// <summary>
        /// Convert Database Formated Date string in system date format.
        /// </summary>
        /// <param name="DateSting">Date as string in database date Format(YYYYMMDD).</param> 
        /// <returns>Date in System Format.</returns>
        public static System.DateTime convertDate(string DateSting)
        {
            try
            {

                return new DateTime(int.Parse(DateSting.Substring(0, 4))
                    , int.Parse(DateSting.Substring(4, 2))
                    , int.Parse(DateSting.Substring(6, 2))
                    );

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[convertDate]");
            }
        }

        /// <summary>
        /// Convert system date in Database Date Time format(YYYYMMDD HH:mm:ss).
        /// </summary>
        /// <param name="Date">Date in System Format.</param> 
        /// <param name="WithTime">Want date string with time.</param> 
        /// <returns>Date as string in database date Format(YYYYMMDD HH:mm:ss).</returns>
        public static string convertDateTime(System.DateTime Date)
        {
            return string.Format("{0:0000}{1:00}{2:00} {3:00}:{4:00}:{5:00}", Date.Year, Date.Month, Date.Day, Date.Hour, Date.Minute, Date.Second);
        }

        /// <summary>
        /// Convert given different date and time in Database Date Time format(YYYYMMDD HH:mm:ss).
        /// </summary>
        /// <param name="Date">Date in System Format.</param> 
        /// <param name="Time">Time in System Format.</param> 
        /// <returns>Date as string in database date Format(YYYYMMDD HH:mm:ss).</returns>
        public static string convertDateTime(System.DateTime Date, System.DateTime Time)
        {
            return string.Format("{0:0000}{1:00}{2:00} {3:00}:{4:00}:{5:00}", Date.Year, Date.Month, Date.Day, Time.Hour, Time.Minute, Time.Second);
        }

        /// <summary>
        /// Convert Database Formated Date/Time string in system date format.
        /// </summary>
        /// <param name="DateTimeSting">Date as string in database date Format(YYYYMMDD HH:mm:ss [112] + ' ' + [108]).</param> 
        /// <returns>Date/Time in System Format.</returns>
        public static System.DateTime convertDateTime(string DateTimeSting)
        {
            try
            {
                return new DateTime(int.Parse(DateTimeSting.Substring(0, 4))
                    , int.Parse(DateTimeSting.Substring(4, 2))
                    , int.Parse(DateTimeSting.Substring(6, 2))
                    , int.Parse(DateTimeSting.Substring(9, 2))
                    , int.Parse(DateTimeSting.Substring(12, 2))
                    , int.Parse(DateTimeSting.Substring(15, 2))
                    );
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[convertDateTime]");
            }
        }

        /// <summary>
        /// Convert separately Database Format Date Time string in system date format.
        /// </summary>
        /// <param name="DateSting">Date as string in database Format(YYYYMMDD [112]).</param> 
        /// <param name="TimeSting">Time as string in database Format(HH:mm:ss [108]).</param> 
        /// <returns>Date/Time in System Format.</returns>
        public static System.DateTime convertDateTime(string DateSting, string TimeSting)
        {
            try
            {
                return new DateTime(int.Parse(DateSting.Substring(0, 4))
                    , int.Parse(DateSting.Substring(4, 2))
                    , int.Parse(DateSting.Substring(6, 2))
                    , int.Parse(TimeSting.Substring(0, 2))
                    , int.Parse(TimeSting.Substring(3, 2))
                    , int.Parse(TimeSting.Substring(6, 2))
                    );
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[convertDateTime]");
            }
        }

        /// <summary>
        /// Replace given Date's time value with given Time.
        /// </summary>
        /// <param name="Date">Date in System Format.</param>
        /// <param name="Time">Time in System Format.</param>
        /// <returns>Date Time in System Format.</returns>
        public static System.DateTime ReplaceTime(System.DateTime Date, System.DateTime Time)
        {
            try
            {
                return new DateTime(Date.Year
                    , Date.Month
                    , Date.Day
                    , Time.Hour
                    , Time.Minute
                    , Time.Second
                    , Time.Millisecond 
                    );
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[ReplaceTime]");
            }
        }

        /// <summary>
        /// Replace given Date's time value with given Time.
        /// </summary>
        /// <param name="Date">Date in System Format.</param>
        /// <param name="TimeSting">Time in Database Format.</param>
        /// <returns>Date Time in System Format.</returns>
        public static System.DateTime ReplaceTime(System.DateTime Date, string TimeSting)
        {
            try
            {
                return new DateTime(Date.Year
                    , Date.Month
                    , Date.Day
                    , int.Parse(TimeSting.Substring(0, 2))
                    , int.Parse(TimeSting.Substring(3, 2))
                    , int.Parse(TimeSting.Substring(6, 2))
                    );
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[ReplaceTime]");
            }
        }

        /// <summary>
        /// Convert system time in Database Time format(HH:mm:ss).
        /// </summary>
        /// <param name="Date">Time in System Format.</param> 
        /// <returns>Time as string in database date Format(HH:mm:ss).</returns
        public static string convertTime(System.DateTime Time)
        {
            try
            {
                return string.Format("{0:00}:{1:00}:{2:00}", Time.Hour, Time.Minute, Time.Second);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[convertTime]");
            }
        }
    }
}


///// <summary>
///// Convert Database Date in system date format.
///// </summary>
///// <param name="strDate">Date as string in database date Format(YYYYMMDD).</param> 
///// <returns>Date in System Format.</returns>
//public static System.DateTime convertDate(string strDate)
//{
//    System.DateTime dtTemp = DateTime.MinValue;
//    CultureInfo culture = null;

//    try
//    {
//        culture = CultureInfo.CurrentCulture;

//        string fmtDate = culture.DateTimeFormat.ShortDatePattern;
//        string strYear = strDate.Substring(0, 4);
//        string strMonth = strDate.Substring(4, 2);
//        string strDay = strDate.Substring(strDate.Length - 2);
//        string[] part = null;
//        string strDateSeparator = culture.DateTimeFormat.DateSeparator;

//        part = fmtDate.Split(strDateSeparator.ToCharArray());

//        string strDateFormat = null;
//        strDateFormat = part[0].Substring(0, 1).ToUpper() + part[1].Substring(0, 1).ToUpper() + part[2].Substring(0, 1).ToUpper();

//        switch (strDateFormat)
//        {
//            case "DMY":
//                dtTemp = System.Convert.ToDateTime(strDay + strDateSeparator + strMonth + strDateSeparator + strYear);
//                break;
//            case "DYM":
//                dtTemp = System.Convert.ToDateTime(strDay + strDateSeparator + strYear + strDateSeparator + strMonth);
//                break;
//            case "MDY":
//                dtTemp = System.Convert.ToDateTime(strMonth + strDateSeparator + strDay + strDateSeparator + strYear);
//                break;
//            case "MYD":
//                dtTemp = System.Convert.ToDateTime(strMonth + strDateSeparator + strYear + strDateSeparator + strDay);
//                break;
//            case "YDM":
//                dtTemp = System.Convert.ToDateTime(strYear + strDateSeparator + strDay + strDateSeparator + strMonth);
//                break;
//            case "YMD":
//                dtTemp = System.Convert.ToDateTime(strYear + strDateSeparator + strMonth + strDateSeparator + strDay);
//                break;
//        }

//        return dtTemp;

//    }
//    catch (System.Exception ex)
//    {
//        throw new Exception( ex.Message + "[convertDate]");
//    }
//    finally
//    {
//        culture = null;
//    }
//}

///// <summary>
///// Convert system date in Database Date format(YYYYMMDD).
///// </summary>
///// <param name="dtDate">Date in System Format.</param> 
///// <returns>Date as string in database date Format(YYYYMMDD).</returns>
//public static string convertDate(System.DateTime dtDate)
//{
//    string strDate = "";
//    try
//    {
//        strDate = dtDate.Year.ToString("####") + dtDate.Month.ToString("0#") + dtDate.Day.ToString("0#");

//        return strDate;

//    }
//    catch (Exception ex)
//    {
//        throw new Exception(ex.Message + "[convertDate]");
//        //return "";
//    }
//}