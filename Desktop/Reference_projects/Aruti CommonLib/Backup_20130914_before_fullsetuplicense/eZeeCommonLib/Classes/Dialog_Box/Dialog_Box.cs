using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System;
namespace eZeeCommonLib
{
    public class Dialog_Box
    {


        public static string conImageFiles = "All Image Files|*.jpg;*.bmp;*.gif;*.ico;*.png|All Files|*.*|Image Files (*.gif)|*.gif|Bitmap Image Files (*.bmp)|*.bmp|JPG Files (*.jpg)|*.jpg|Icon Files (*.ico)|*.ico|Png Files (*.png)|*.png";


        public static List<int> iColorCnt;

        /// <summary>
        /// Open a color dialog box for select a color for object.
        /// </summary>
        /// <param name="defaultColor">A default color which is selected in color dialog box. </param> 
        /// <returns>A selected color from color dialog box.</returns>
        public static System.Drawing.Color dialogSelectColor(System.Drawing.Color defaultColor)
        {
            ColorDialog cdlg = new ColorDialog();
            int intColor = 0;
            int[] aryColors = null;

            if (iColorCnt == null)
            {
                iColorCnt = new List<int>();
                iColorCnt.Add(9850885);
            }

            intColor = ColorTranslator.ToWin32(defaultColor);
            iColorCnt.Add(intColor);

            //aryColors = New Integer() {9850885, intColor}
            aryColors = iColorCnt.ToArray();

            try
            {
                cdlg.FullOpen = true;
                cdlg.AnyColor = true;
                cdlg.CustomColors = aryColors;
                cdlg.SolidColorOnly = false;
                cdlg.Color = defaultColor;

                if (cdlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {

                    iColorCnt.Clear();
                    foreach (int ix in cdlg.CustomColors)
                    {
                        iColorCnt.Add(ix);
                    }
                    return cdlg.Color;
                }
                else
                {
                    return defaultColor;
                }


            }
            catch (Exception ex)
            {
                throw new Exception( ex.Message + "[dialogSelectColor-DialogBox]");
            }
            finally
            {
                cdlg = null;
                aryColors = null;
            }
        }

        /// <summary>
        /// Open a file dialog box for select an image files.
        /// </summary>
        /// <param name="strInitialDirectory">Optional Para, A directory path where to find image file. </param> 
        /// <returns>A file name with full path which is select from file dialog box.</returns>

        public static string dialogOpenFile(string strInitialDirectory)
        {
            return dialogOpenFile(strInitialDirectory, "All Files|*.*");
        }

        public static string dialogOpenFile()
        {
            return dialogOpenFile("", "All Files|*.*");
        }

        //ORIGINAL LINE: Public Shared Function dialogOpenFile(Optional ByVal strInitialDirectory As String = "", Optional ByVal FileExtanstion As String = "All Files|*.*") As String
        public static string dialogOpenFile(string strInitialDirectory, string FileExtanstion)
        {
            OpenFileDialog cdlg = new OpenFileDialog();
            string strFilePath = "";
            try
            {
                if (strInitialDirectory == "")
                {
                    strInitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }
                cdlg.InitialDirectory = strInitialDirectory;
                cdlg.Filter = FileExtanstion;
                if (cdlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    strFilePath = cdlg.FileName;
                }

                return strFilePath;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[dialogOpenFile-DialogBox]");
            }
            finally
            {
                cdlg = null;
            }
        }

        /// <summary>
        /// Open a file dialog box for select an image files(*.jpg;*.bmp;*.gif).
        /// </summary>
        /// <param name="strInitialDirectory">Optional Para, A directory path where to find image file. </param> 
        /// <returns>A file name with full path which is select from file dialog box.</returns>

        public static string dialogOpenImageFile()
        {
            return dialogOpenImageFile("");
        }

        //ORIGINAL LINE: Public Shared Function dialogOpenImageFile(Optional ByVal strInitialDirectory As String = "") As String
        public static string dialogOpenImageFile(string strInitialDirectory)
        {
            OpenFileDialog cdlg = new OpenFileDialog();
            string strFilePath = "";
            try
            {
                if (strInitialDirectory == "")
                {
                    strInitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }
                cdlg.InitialDirectory = strInitialDirectory;
                cdlg.Filter = conImageFiles;
                if (cdlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    strFilePath = cdlg.FileName;
                }

                return strFilePath;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[dialogOpenImageFile-DialogBox]"); 
            }
            finally
            {
                cdlg = null;
            }
        }

        /// <summary>
        /// Open a file dialog box for select one or more image file(s) (*.jpg;*.bmp;*.gif;*.ico;*.png).
        /// </summary>
        /// <param name="strInitialDirectory">Optional Para, A directory path where to find image file. </param> 
        /// <returns>The file(s) name with full path which is/are selected from file dialog box.</returns>

        public static string[] dialogOpenImageFiles()
        {
            return dialogOpenImageFiles("");
        }

        //ORIGINAL LINE: Public Shared Function dialogOpenImageFiles(Optional ByVal strInitialDirectory As String = "") As String()
        public static string[] dialogOpenImageFiles(string strInitialDirectory)
        {
            OpenFileDialog cdlg = new OpenFileDialog();
            string[] strFilePath = null;
            try
            {
                if (strInitialDirectory == "")
                {

                    strInitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }
                cdlg.Multiselect = true;
                cdlg.InitialDirectory = strInitialDirectory;
                cdlg.Filter = conImageFiles;
                if (cdlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    strFilePath = cdlg.FileNames;
                }

                return strFilePath;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[dialogOpenImageFiles-DialogBox]"); 
            }
            finally
            {
                cdlg = null;
                strFilePath = null;
            }
        }

    }
}