using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using System.Runtime.InteropServices;

namespace TaskScheduler.Definition
{

#region  Structures 

	internal struct DAILY
	{
		public short DaysInterval;
	}

	internal struct WEEKLY
	{
		public short WeeksInterval;
		public Trigger.Days rgfDaysOfTheWeek;
	}

	// 09/18/2004 - Changed rgfDays to Integer
	[StructLayout(LayoutKind.Sequential, Size=6)]
	internal struct MONTHLYDATE
	{
		public int rgfDays;
		public Trigger.Months rgfMonths;
	}

	internal struct MONTHLYDOW
	{
		public Trigger.Weeks wWhichWeek;
		public Trigger.Days rgfDaysOfTheWeek;
		public Trigger.Months rgfMonths;
	}

	[StructLayout(LayoutKind.Explicit, Size=6)]
	internal struct TRIGGER_TYPE_UNION
	{
		[FieldOffset(0)]
		public DAILY Daily;
		[FieldOffset(0)]
		public WEEKLY Weekly;
		[FieldOffset(0)]
		public MONTHLYDATE MonthlyDate;
		[FieldOffset(0)]
		public MONTHLYDOW MonthlyDOW;
	}

	[StructLayout(LayoutKind.Sequential, Pack=2)]
	internal struct TASK_TRIGGER
	{
		public short cbTriggerSize;
		public short Reserved1;
		public short wBeginYear;
		public short wBeginMonth;
		public short wBeginDay;
		public short wEndYear;
		public short wEndMonth;
		public short wEndDay;
		public short wStartHour;
		public short wStartMinute;
		public int MinutesDuration;
		public int MinutesInterval;
		public Trigger.TriggerFlags rgFlags;
		public Trigger.Types TriggerType;
		public TRIGGER_TYPE_UNION Type;
		public short Reserved2;
		public short wRandomMinutesInterval;       
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct SYSTEMTIME
	{
		public short wYear;
		public short wMonth;
		public short wDayOfWeek;
		public short wDay;
		public short wHour;
		public short wMinute;
		public short wSecond;
		public short wMilliseconds;
	}

#endregion

#region  Interfaces 

	[ComImport(), Guid("148BD52B-A2AB-11CE-B11F-00AA00530503"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface ITaskTrigger
	{

		void SetTrigger([In()] ref TASK_TRIGGER pTrigger);

		void GetTrigger([In(), Out()] ref TASK_TRIGGER pTrigger);

		[return: MarshalAs(UnmanagedType.LPWStr)]
		string GetTriggerString();

	}

	[ComImport(), Guid("148BD524-A2AB-11CE-B11F-00AA00530503"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface ITask
	{

        [return: MarshalAs(UnmanagedType.Interface)]
		ITaskTrigger CreateTrigger(out short piNewTrigger);

		void DeleteTrigger([In()] short iTrigger);

		short GetTriggerCount();

        [return: MarshalAs(UnmanagedType.Interface)]
		ITaskTrigger GetTrigger([In()] short iTrigger);

        [return: MarshalAs(UnmanagedType.LPWStr)]
		string GetTriggerString([In()] short iTrigger);

        void GetRunTimes([In(), MarshalAs(UnmanagedType.LPStruct)] ref SYSTEMTIME pstBegin, [In(), MarshalAs(UnmanagedType.LPStruct)] ref SYSTEMTIME pstEnd, [In(), Out()] ref short pCount, [In(), Out(), MarshalAs(UnmanagedType.LPStruct)] ref SYSTEMTIME[] rgstTaskTimes);

        [return: MarshalAs(UnmanagedType.Struct)]
		SYSTEMTIME GetNextRunTime();

		void SetIdleWait([In()] short wMinutes);

		short GetIdleWait();

		void Run();

		void Terminate();

		void EditWorkItem([In()] IntPtr hWndParent, [In()] int dwReserved);

        [return: MarshalAs(UnmanagedType.Struct)]
		SYSTEMTIME GetMostRecentRunTime();

		Task.Status GetStatus();

		int GetExitCode();

        void SetComment([In(), MarshalAs(UnmanagedType.LPWStr)] string Text);

        [return: MarshalAs(UnmanagedType.LPWStr)]
		string GetComment();

        void SetCreator([In(), MarshalAs(UnmanagedType.LPWStr)] string Text);

        [return: MarshalAs(UnmanagedType.LPWStr)]
		string GetCreator();

        void SetWorkItemData([In()] short cbData, [In(), MarshalAs(UnmanagedType.LPArray)] byte[] rgbData);

        void GetWorkItemData([In(), Out()] ref short pcbData, [In(), MarshalAs(UnmanagedType.LPArray)] byte[] rgbData);

		void SetErrorRetryCount([In()] short wRetryCount);

		short GetErrorRetryCount();

		void SetErrorRetryInterval([In()] short wRetryInterval);

		short GetErrorRetryInterval();

		void SetFlags([In()] int dwFlags);

		Task.TaskFlags GetFlags();

        void SetAccountInformation([In(), MarshalAs(UnmanagedType.LPWStr)] string pwszAccountName, [In(), MarshalAs(UnmanagedType.LPWStr)] string pwszPassword);

        [return: MarshalAs(UnmanagedType.LPWStr)]
		string GetAccountInformation();

        void SetApplicationName([In(), MarshalAs(UnmanagedType.LPWStr)] string pwszApplicationName);

        [return: MarshalAs(UnmanagedType.LPWStr)]
		string GetApplicationName();

        void SetParameters([In(), MarshalAs(UnmanagedType.LPWStr)] string pwszParameters);

        [return: MarshalAs(UnmanagedType.LPWStr)]
		string GetParameters();

        void SetWorkingDirectory([In(), MarshalAs(UnmanagedType.LPWStr)] string pwszWorkingDirectory);

        [return: MarshalAs(UnmanagedType.LPWStr)]
		string GetWorkingDirectory();

		void SetPriority([In()] int dwPriority);

		int GetPriority();

		void SetTaskFlags([In()] int dwFlags);

		int GetTaskFlags();

		void SetMaxRunTime([In()] int dwMaxRunTimeMS);

		int GetMaxRunTime();

	}

	[ComImport(), Guid("148BD528-A2AB-11CE-B11F-00AA00530503"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IEnumWorkItems
	{
		[PreserveSig()]
		int Next([In()] int celt, out IntPtr rgpwszNames, out int Fetched);

		void Skip([In()] int celt);

		void Reset();

        [return: MarshalAs(UnmanagedType.Interface)]
		IEnumWorkItems Clone();

	}

    [ComImport(), Guid("148BD527-A2AB-11CE-B11F-00AA00530503"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    internal interface ISchedulingAgent
    {

        void SetTargetComputer([MarshalAs(UnmanagedType.LPWStr)] string pwszComputer);

        [return: MarshalAs(UnmanagedType.LPWStr)]
        string GetTargetComputer();

        [return: MarshalAs(UnmanagedType.Interface)]
        IEnumWorkItems Enums();

        [return: MarshalAs(UnmanagedType.Interface)]
        ITask Activate([MarshalAs(UnmanagedType.LPWStr)] string pwszName, ref System.Guid riid);

        void Delete([MarshalAs(UnmanagedType.LPWStr)] string Name);

        [return: MarshalAs(UnmanagedType.Interface)]
        ITask NewWorkItem([MarshalAs(UnmanagedType.LPWStr)] string Name, ref System.Guid clsid, ref System.Guid riid);

        void AddWorkItem([MarshalAs(UnmanagedType.LPWStr)] string Name, [MarshalAs(UnmanagedType.Interface)] ITask WorkItem);

        int IsOfType([MarshalAs(UnmanagedType.LPWStr)] string Name, ref System.Guid clsid);

    }

	[ComImport(), Guid("4086658a-cbbb-11cf-b604-00c04fd8d565"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IProvideTaskPage
	{

		int GetPage([In()] TaskPage tpType, [In()] int fPersistChanges);
	}

#endregion
}
