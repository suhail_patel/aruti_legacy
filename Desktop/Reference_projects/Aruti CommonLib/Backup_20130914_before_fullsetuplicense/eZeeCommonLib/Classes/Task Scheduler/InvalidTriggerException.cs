using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace TaskScheduler
{
	[System.Serializable()]
	public sealed class InvalidTriggerException : System.InvalidOperationException
	{

		public InvalidTriggerException() : base("Invalid operation for the current trigger type.")
		{
		}

		public InvalidTriggerException(string message) : base(message)
		{
		}

		public InvalidTriggerException(string message, System.Exception innerException) : base(message, innerException)
		{
		}

		public InvalidTriggerException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
		{
		}
	}
}
