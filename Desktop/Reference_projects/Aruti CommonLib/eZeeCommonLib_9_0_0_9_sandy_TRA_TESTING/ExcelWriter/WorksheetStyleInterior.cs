﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetStyleInterior : IWriter, IReader, ICodeWriter
    {
        private string _color;
        private StyleInteriorPattern _pattern;

        internal WorksheetStyleInterior()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._color != null)
            {
                Util.AddAssignment(method, targetObject, "Color", this._color);
            }
            if (this._pattern != StyleInteriorPattern.NotSet)
            {
                Util.AddAssignment(method, targetObject, "Pattern", this._pattern);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._color = Util.GetAttribute(element, "Color", "urn:schemas-microsoft-com:office:spreadsheet");
            string attribute = element.GetAttribute("Pattern", "urn:schemas-microsoft-com:office:spreadsheet");
            if ((attribute != null) && (attribute.Length != 0))
            {
                this._pattern = (StyleInteriorPattern) Enum.Parse(typeof(StyleInteriorPattern), attribute, true);
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Interior", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._color != null)
            {
                writer.WriteAttributeString("Color", "urn:schemas-microsoft-com:office:spreadsheet", this._color);
            }
            if (this._pattern != StyleInteriorPattern.NotSet)
            {
                writer.WriteAttributeString("Pattern", "urn:schemas-microsoft-com:office:spreadsheet", this._pattern.ToString());
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Interior", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public string Color
        {
            get
            {
                return this._color;
            }
            set
            {
                this._color = value;
            }
        }

        public StyleInteriorPattern Pattern
        {
            get
            {
                return this._pattern;
            }
            set
            {
                this._pattern = value;
            }
        }
    }
}

