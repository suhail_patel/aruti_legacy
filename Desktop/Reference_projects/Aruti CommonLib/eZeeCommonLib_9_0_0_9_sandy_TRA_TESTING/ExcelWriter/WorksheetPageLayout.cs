﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetPageLayout : IWriter, IReader, ICodeWriter
    {
        private bool _centerHorizontal;
        private bool _centerVertical;
        private ExcelWriter.Orientation _orientation;

        internal WorksheetPageLayout()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._orientation != ExcelWriter.Orientation.NotSet)
            {
                Util.AddAssignment(method, targetObject, "Orientation", this._orientation);
            }
            if (this._centerHorizontal)
            {
                Util.AddAssignment(method, targetObject, "CenterHorizontal", true);
            }
            if (this._centerVertical)
            {
                Util.AddAssignment(method, targetObject, "CenterVertical", true);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            string str = Util.GetAttribute(element, "Orientation", "urn:schemas-microsoft-com:office:excel");
            if ((str != null) && (str.Length > 0))
            {
                this._orientation = (ExcelWriter.Orientation) Enum.Parse(typeof(ExcelWriter.Orientation), str);
            }
            this._centerHorizontal = Util.GetAttribute(element, "CenterHorizontal", "urn:schemas-microsoft-com:office:excel", false);
            this._centerVertical = Util.GetAttribute(element, "CenterVertical", "urn:schemas-microsoft-com:office:excel", false);
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "Layout", "urn:schemas-microsoft-com:office:excel");
            if (this._orientation != ExcelWriter.Orientation.NotSet)
            {
                writer.WriteAttributeString("Orientation", "urn:schemas-microsoft-com:office:excel", this._orientation.ToString());
            }
            if (this._centerHorizontal)
            {
                writer.WriteAttributeString("CenterHorizontal", "urn:schemas-microsoft-com:office:excel", "1");
            }
            if (this._centerVertical)
            {
                writer.WriteAttributeString("CenterVertical", "urn:schemas-microsoft-com:office:excel", "1");
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Layout", "urn:schemas-microsoft-com:office:excel");
        }

        public bool CenterHorizontal
        {
            get
            {
                return this._centerHorizontal;
            }
            set
            {
                this._centerHorizontal = value;
            }
        }

        public bool CenterVertical
        {
            get
            {
                return this._centerVertical;
            }
            set
            {
                this._centerVertical = value;
            }
        }

        public ExcelWriter.Orientation Orientation
        {
            get
            {
                return this._orientation;
            }
            set
            {
                this._orientation = value;
            }
        }
    }
}

