﻿namespace ExcelWriter
{
    using System;
    using System.Globalization;
    using System.Xml;

    public sealed class PTSource : IWriter
    {
        private int _cacheIndex = -2147483648;
        private PTConsolidationReference _consolidationReference;
        private DateTime _refreshDate = DateTime.MinValue;
        private DateTime _refreshDateCopy = DateTime.MinValue;
        private string _refreshName;
        private int _versionLastRefresh = -2147483648;

        internal PTSource()
        {
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "PTSource", "urn:schemas-microsoft-com:office:excel");
            if (this._cacheIndex != -2147483648)
            {
                writer.WriteElementString("CacheIndex", "urn:schemas-microsoft-com:office:excel", this._cacheIndex.ToString());
            }
            if (this._versionLastRefresh != -2147483648)
            {
                writer.WriteElementString("VersionLastRefresh", "urn:schemas-microsoft-com:office:excel", this._versionLastRefresh.ToString());
            }
            if (this._refreshName != null)
            {
                writer.WriteElementString("RefreshName", "urn:schemas-microsoft-com:office:excel", this._refreshName);
            }
            if (this._refreshDate != DateTime.MinValue)
            {
                writer.WriteElementString("RefreshDate", "urn:schemas-microsoft-com:office:excel", this._refreshDate.ToString("s", CultureInfo.InvariantCulture));
            }
            if (this._refreshDateCopy != DateTime.MinValue)
            {
                writer.WriteElementString("RefreshDateCopy", "urn:schemas-microsoft-com:office:excel", this._refreshDateCopy.ToString("s", CultureInfo.InvariantCulture));
            }
            if (this._consolidationReference != null)
            {
                ((IWriter) this._consolidationReference).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public int CacheIndex
        {
            get
            {
                return this._cacheIndex;
            }
            set
            {
                this._cacheIndex = value;
            }
        }

        public PTConsolidationReference ConsolidationReference
        {
            get
            {
                if (this._consolidationReference == null)
                {
                    this._consolidationReference = new PTConsolidationReference();
                }
                return this._consolidationReference;
            }
            set
            {
                this._consolidationReference = value;
            }
        }

        public DateTime RefreshDate
        {
            get
            {
                return this._refreshDate;
            }
            set
            {
                this._refreshDate = value;
            }
        }

        public DateTime RefreshDateCopy
        {
            get
            {
                return this._refreshDateCopy;
            }
            set
            {
                this._refreshDateCopy = value;
            }
        }

        public string RefreshName
        {
            get
            {
                return this._refreshName;
            }
            set
            {
                this._refreshName = value;
            }
        }

        public int VersionLastRefresh
        {
            get
            {
                return this._versionLastRefresh;
            }
            set
            {
                this._versionLastRefresh = value;
            }
        }
    }
}

