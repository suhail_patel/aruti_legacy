﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetPageFooter : IWriter, IReader, ICodeWriter
    {
        private string _data;
        private float _margin = 0.5f;

        internal WorksheetPageFooter()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._data != null)
            {
                Util.AddAssignment(method, targetObject, "Data", this._data);
            }
            if (this._margin != 0.5)
            {
                Util.AddAssignment(method, targetObject, "Margin", this._margin);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._margin = Util.GetAttribute(element, "Margin", "urn:schemas-microsoft-com:office:excel", (float) 0.5f);
            this._data = Util.GetAttribute(element, "Data", "urn:schemas-microsoft-com:office:excel");
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "Footer", "urn:schemas-microsoft-com:office:excel");
            if (this._data != null)
            {
                writer.WriteAttributeString("Data", "urn:schemas-microsoft-com:office:excel", this._data);
            }
            if (this._margin != 0.5)
            {
                writer.WriteAttributeString("Margin", "urn:schemas-microsoft-com:office:excel", this._margin.ToString());
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Footer", "urn:schemas-microsoft-com:office:excel");
        }

        public string Data
        {
            get
            {
                return this._data;
            }
            set
            {
                this._data = value;
            }
        }

        public float Margin
        {
            get
            {
                return this._margin;
            }
            set
            {
                this._margin = value;
            }
        }
    }
}

