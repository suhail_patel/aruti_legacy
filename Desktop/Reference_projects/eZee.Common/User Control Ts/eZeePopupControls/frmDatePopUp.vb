'************************************************************************************************************************************
'Class Name : frmDatePopUp
'Purpose    : 
'Written By : 
'Modified   : Naimish
'************************************************************************************************************************************



Public Class frmDatePopUp
    Private Readonly mstrModuleName as  String  = "frmDatePopUp"
    Dim m_blnCancel As Boolean = True


    Dim m_dtDate As Date
    Dim m_dtMaxDate As Date = Nothing
    Dim m_dtMinDate As Date = Nothing

    Dim m_strTitle As String = "Select date"
    Dim m_strOkCaption As String = "Finish"
    Dim m_strCancelCaption As String = "Cancel"

#Region " Properties "
    Public Property _Caption() As String
        Get
            Return m_strTitle
        End Get
        Set(ByVal value As String)
            m_strTitle = value
        End Set
    End Property

    Public Property _Ok_Caption() As String
        Get
            Return m_strOkCaption
        End Get
        Set(ByVal value As String)
            m_strOkCaption = value
        End Set
    End Property

    Public Property _Cancel_Caption() As String
        Get
            Return m_strCancelCaption
        End Get
        Set(ByVal value As String)
            m_strCancelCaption = value
        End Set
    End Property

    Public Property SelectedDate() As Date
        Get
            Return m_dtDate
        End Get
        Set(ByVal value As Date)
            m_dtDate = value
        End Set
    End Property

    Public Property MaxDate() As Date
        Get
            Return m_dtMaxDate
        End Get
        Set(ByVal value As Date)
            m_dtMaxDate = value
        End Set
    End Property

    Public Property MinDate() As Date
        Get
            Return m_dtMinDate
        End Get
        Set(ByVal value As Date)
            m_dtMinDate = value
        End Set
    End Property

#End Region

#Region " Public Method "
    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
        Catch ex As Exception
            Throw ex
        End Try
        Return Not m_blnCancel
    End Function
#End Region

#Region " Form "
    Private Sub frmDateTime_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Text = m_strTitle
            btnCancel.Text = m_strCancelCaption
            btnFinish.Text = m_strOkCaption

            dtpMain.ActiveMonth.Year = m_dtDate.Year
            dtpMain.ActiveMonth.Month = m_dtDate.Month
            dtpMain.SelectionMode = Calendar.mcSelectionMode.One

            dtpMain.SelectDate(m_dtDate)

            If m_dtMinDate = Nothing Then
                m_dtMinDate = dtpMain.MinDate
            Else
                dtpMain.MinDate = m_dtMinDate
            End If

            If m_dtMinDate = Nothing Then
                m_dtMaxDate = dtpMain.MaxDate
            Else
                dtpMain.MaxDate = m_dtMaxDate
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Buttons "
    Private Sub dtpMain_DaySelected(ByVal sender As Object, ByVal e As Calendar.DaySelectedEventArgs) Handles dtpMain.DaySelected
        m_dtDate = dtpMain.SelectedDates(0)
    End Sub

    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        Try
            m_blnCancel = False
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
    
End Class
