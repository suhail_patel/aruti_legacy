<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListBox
    Inherits frmBasePopup

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.ezListBox = New eZeeListBox
        Me.btnCancel = New eZeeButton
        Me.btnFinish = New eZeeButton
        Me.pnlMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.Transparent
        Me.pnlMain.Controls.Add(Me.ezListBox)
        Me.pnlMain.Controls.Add(Me.btnCancel)
        Me.pnlMain.Controls.Add(Me.btnFinish)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(1, 35)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(310, 363)
        Me.pnlMain.TabIndex = 0
        '
        'ezListBox
        '
        Me.ezListBox._BackcolorMember = ""
        Me.ezListBox._BorderColor = System.Drawing.Color.CadetBlue
        Me.ezListBox._BorderWidth = 0
        Me.ezListBox._Columns = 1
        Me.ezListBox._ControlColor = System.Drawing.Color.GreenYellow
        Me.ezListBox._CotrolButtonSize = 40
        Me.ezListBox._DataSource = Nothing
        Me.ezListBox._DisplayMember = ""
        Me.ezListBox._ForecolorMember = ""
        Me.ezListBox._ImageMember = ""
        Me.ezListBox._ItemColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.ezListBox._Orientation = eZeeListBox.enOrientation.None
        Me.ezListBox._Rows = 6
        Me.ezListBox._SelectedId = -1
        Me.ezListBox._SelectedIndex = 0
        Me.ezListBox._SelectedText = ""
        Me.ezListBox._ShowCaptionMember = ""
        Me.ezListBox._ShowOutline = True
        Me.ezListBox._TextAlignMember = ""
        Me.ezListBox._TextSizeMember = ""
        Me.ezListBox._ValueMember = ""
        Me.ezListBox.Font = New System.Drawing.Font("Verdana", 14.25!)
        Me.ezListBox.Location = New System.Drawing.Point(6, 0)
        Me.ezListBox.Margin = New System.Windows.Forms.Padding(6, 5, 6, 5)
        Me.ezListBox.Name = "ezListBox"
        Me.ezListBox.Size = New System.Drawing.Size(298, 308)
        Me.ezListBox.TabIndex = 13
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.Red
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.btnCancel.GlowColor = System.Drawing.Color.MistyRose
        Me.btnCancel.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnCancel.Location = New System.Drawing.Point(191, 323)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.ShineColor = System.Drawing.Color.LightCoral
        Me.btnCancel.Size = New System.Drawing.Size(113, 34)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'btnFinish
        '
        Me.btnFinish.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinish.BackColor = System.Drawing.Color.Green
        Me.btnFinish.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.btnFinish.GlowColor = System.Drawing.Color.PaleGreen
        Me.btnFinish.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnFinish.Location = New System.Drawing.Point(72, 323)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.ShineColor = System.Drawing.Color.PaleGreen
        Me.btnFinish.Size = New System.Drawing.Size(113, 34)
        Me.btnFinish.TabIndex = 1
        Me.btnFinish.Text = "Finish"
        '
        'frmListBox
        '
        Me.AcceptButton = Me.btnFinish
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.CadetBlue
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(312, 399)
        Me.Controls.Add(Me.pnlMain)
        Me.Name = "frmListBox"
        Me.pnlMain.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As eZeeButton
    Friend WithEvents btnFinish As eZeeButton
    Friend WithEvents ezListBox As eZeeListBox
End Class
