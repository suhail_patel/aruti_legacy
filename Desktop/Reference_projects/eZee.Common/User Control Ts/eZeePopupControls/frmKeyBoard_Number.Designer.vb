<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmKeyBoard_Number
    Inherits frmBasePopup

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ezKbMain = New eZeeKeyboard
        Me.txtFigure = New eZee.TextBox.NumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnCancel = New eZeeButton
        Me.btnFinish = New eZeeButton
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ezKbMain
        '
        Me.ezKbMain.BackColor = System.Drawing.Color.DarkSlateGray
        Me.ezKbMain.KeyboardType = eZeeKeyBoardLayOut.NumPad_Number
        Me.ezKbMain.Location = New System.Drawing.Point(5, 41)
        Me.ezKbMain.Name = "ezKbMain"
        Me.ezKbMain.Size = New System.Drawing.Size(233, 233)
        Me.ezKbMain.TabIndex = 3
        '
        'txtFigure
        '
        Me.txtFigure.AllowNegative = True
        Me.txtFigure.DigitsInGroup = 0
        Me.txtFigure.Flags = 0
        Me.txtFigure.Font = New System.Drawing.Font("Verdana", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFigure.ForeColor = System.Drawing.SystemColors.Desktop
        Me.txtFigure.Location = New System.Drawing.Point(5, 3)
        Me.txtFigure.MaxDecimalPlaces = 4
        Me.txtFigure.MaxWholeDigits = 8
        Me.txtFigure.Name = "txtFigure"
        Me.txtFigure.Prefix = ""
        Me.txtFigure.RangeMax = 1.7976931348623157E+308
        Me.txtFigure.RangeMin = -1.7976931348623157E+308
        Me.txtFigure.Size = New System.Drawing.Size(233, 33)
        Me.txtFigure.TabIndex = 2
        Me.txtFigure.Text = "0"
        Me.txtFigure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.btnFinish)
        Me.Panel1.Controls.Add(Me.ezKbMain)
        Me.Panel1.Controls.Add(Me.txtFigure)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(1, 35)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(244, 324)
        Me.Panel1.TabIndex = 23
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.Red
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.btnCancel.GlowColor = System.Drawing.Color.MistyRose
        Me.btnCancel.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnCancel.Location = New System.Drawing.Point(125, 283)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.ShineColor = System.Drawing.Color.LightCoral
        Me.btnCancel.Size = New System.Drawing.Size(113, 34)
        Me.btnCancel.TabIndex = 19
        Me.btnCancel.Text = "Cancel"
        '
        'btnFinish
        '
        Me.btnFinish.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinish.BackColor = System.Drawing.Color.Green
        Me.btnFinish.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.btnFinish.GlowColor = System.Drawing.Color.PaleGreen
        Me.btnFinish.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnFinish.Location = New System.Drawing.Point(6, 283)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.ShineColor = System.Drawing.Color.PaleGreen
        Me.btnFinish.Size = New System.Drawing.Size(113, 34)
        Me.btnFinish.TabIndex = 18
        Me.btnFinish.Text = "Finish"
        '
        'frmKeyBoard_Number
        '
        Me.AcceptButton = Me.btnFinish
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.CadetBlue
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(246, 360)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmKeyBoard_Number"
        Me.ShowIcon = False
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ezKbMain As eZeeKeyboard
    Friend WithEvents txtFigure As eZee.TextBox.NumericTextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As eZeeButton
    Friend WithEvents btnFinish As eZeeButton
End Class
