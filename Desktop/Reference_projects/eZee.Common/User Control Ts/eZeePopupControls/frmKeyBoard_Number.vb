'************************************************************************************************************************************
'Class Name : frmKeyBoard_Number.vb
'Purpose    : 
'Written By : 
'Modified   : Naimish
'************************************************************************************************************************************
Imports System.Windows.Forms


Public Class frmKeyBoard_Number
    Private Readonly mstrModuleName as  String  = "frmKeyBoard_Number"

    Dim mblnCancel As Boolean = True
    Dim _dblText As String = 0
    Dim _blnAllowNegative As Boolean = True
    Dim _intMaxDecimalPlaces As Integer = 4
    Dim _intMaxWholeDigits As Integer = 10

    Dim _intMaxLength As Integer = 32767

    Dim m_strTitle As String = "Enter Value"
    Dim m_strOkCaption As String = "Finish"
    Dim m_strCancelCaption As String = "Cancel"

#Region " Properties "
    Public Property _Text() As String
        Get
            Return _dblText
        End Get
        Set(ByVal value As String)
            _dblText = value
        End Set
    End Property

    Public Property _Caption() As String
        Get
            Return m_strTitle
        End Get
        Set(ByVal value As String)
            m_strTitle = value
        End Set
    End Property

    Public Property _Ok_Caption() As String
        Get
            Return m_strOkCaption
        End Get
        Set(ByVal value As String)
            m_strOkCaption = value
        End Set
    End Property

    Public Property _Cancel_Caption() As String
        Get
            Return m_strCancelCaption
        End Get
        Set(ByVal value As String)
            m_strCancelCaption = value
        End Set
    End Property

    Public Property AllowNegative() As Boolean
        Get
            Return _blnAllowNegative
        End Get
        Set(ByVal value As Boolean)
            _blnAllowNegative = value
        End Set
    End Property

    Public Property MaxDecimalPlaces() As Integer
        Get
            Return _intMaxDecimalPlaces
        End Get
        Set(ByVal value As Integer)
            _intMaxDecimalPlaces = value
        End Set
    End Property

    Public Property MaxWholeDigits() As Integer
        Get
            Return _intMaxWholeDigits
        End Get
        Set(ByVal value As Integer)
            _intMaxWholeDigits = value

        End Set
    End Property

    Public Property MaxLength() As Integer
        Get
            Return _intMaxLength
        End Get
        Set(ByVal value As Integer)
            _intMaxLength = value
        End Set
    End Property

#End Region

#Region " Public Method "
    Public Overloads Function DisplayDialog() As Boolean

        Try
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Exception
            Throw ex
            Return Text
        End Try
    End Function

#End Region

#Region " Form "
    Private Sub frmKeyBoard_Number_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Text = m_strTitle
            btnCancel.Text = m_strCancelCaption
            btnFinish.Text = m_strOkCaption

            txtFigure.Text = _dblText
            txtFigure.AllowNegative = _blnAllowNegative
            txtFigure.MaxWholeDigits = _intMaxWholeDigits
            txtFigure.MaxDecimalPlaces = _intMaxDecimalPlaces
            txtFigure.MaxLength = _intMaxLength

            txtFigure.Focus()

            txtFigure.SelectionStart = 0
            txtFigure.SelectionLength = Len(txtFigure.Text)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Buttons "
    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        Try
            _dblText = txtFigure.NumericText
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " Controls "
    Private Sub ezKbMain_UserKeyPressed(ByVal sender As Object, ByVal e As eZeeTsKeyboardEventArgs) Handles ezKbMain.UserKeyPressed

        Dim txtKeyPress As String = e.KeyboardKeyPressed
        txtFigure.Focus()
        Select Case txtKeyPress
            Case "C"
                txtFigure.Text = ""
            Case "{ENTER}"
                _dblText = Val(txtFigure.Text)
                Me.Close()
            Case Else
                SendKeys.Send(e.KeyboardKeyPressed)
        End Select
    End Sub
#End Region



End Class