'************************************************************************************************************************************
'Class Name : frmListBox
'Purpose    : 
'Written By : 
'Modified   : Naimish
'************************************************************************************************************************************



Public Class frmListBox
    Private Readonly mstrModuleName as  String  = "frmListBox"

    Private mdtListBox As DataTable
    Private mstrDisplayName As String
    Private mstrValueMember As String

    Private mstrSelectedName As String = ""
    Private mintSelectedValue As Integer = -1

    Private mblnCancel As Boolean = True

    Dim m_strTitle As String = "Select Value"
    Dim m_strOkCaption As String = "Finish"
    Dim m_strCancelCaption As String = "Cancel"

#Region " Properties "
    Public Property _Caption() As String
        Get
            Return m_strTitle
        End Get
        Set(ByVal value As String)
            m_strTitle = value
        End Set
    End Property

    Public Property _Ok_Caption() As String
        Get
            Return m_strOkCaption
        End Get
        Set(ByVal value As String)
            m_strOkCaption = value
        End Set
    End Property

    Public Property _Cancel_Caption() As String
        Get
            Return m_strCancelCaption
        End Get
        Set(ByVal value As String)
            m_strCancelCaption = value
        End Set
    End Property

    Public WriteOnly Property _DataSource() As DataTable
        Set(ByVal value As DataTable)
            mdtListBox = value
        End Set
    End Property

    Public WriteOnly Property _DisplayName() As String
        Set(ByVal value As String)
            mstrDisplayName = value
        End Set
    End Property

    Public WriteOnly Property _ValueMember() As String
        Set(ByVal value As String)
            mstrValueMember = value
        End Set
    End Property

    Public Property _SelectedName() As String
        Get
            Return mstrSelectedName
        End Get
        Set(ByVal value As String)
            mstrSelectedName = value
        End Set
    End Property

    Public Property _SelectedValue() As Integer
        Get
            Return mintSelectedValue
        End Get
        Set(ByVal value As Integer)
            mintSelectedValue = value
        End Set
    End Property

#End Region

#Region " Public Method "
    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()

        Catch ex As Exception
            Throw ex
        End Try
        Return Not mblnCancel
    End Function
#End Region

#Region " Form "
    Private Sub frmListBox_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Text = m_strTitle
            btnCancel.Text = m_strCancelCaption
            btnFinish.Text = m_strOkCaption

            ezListBox._DisplayMember = mstrDisplayName
            ezListBox._ValueMember = mstrValueMember
            ezListBox._DataSource = mdtListBox

            If mintSelectedValue > 0 Then
                ezListBox._SelectedId = mintSelectedValue
            ElseIf mstrSelectedName <> "" Then
                ezListBox._SelectedText = mstrSelectedName
            Else
                If ezListBox._Items.Count > 0 Then ezListBox._SelectedIndex = 0
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Buttons "
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        Try
            mstrSelectedName = ezListBox._SelectedText
            mintSelectedValue = ezListBox._SelectedId

            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class