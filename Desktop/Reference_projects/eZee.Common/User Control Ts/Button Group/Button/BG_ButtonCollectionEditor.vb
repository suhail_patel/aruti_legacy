Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.ComponentModel
Imports System.ComponentModel.Design

Public Class BG_ButtonCollectionEditor
    Inherits CollectionEditor

    Public Sub New(ByVal type As Type)
        MyBase.New(type)
    End Sub

    Protected Overrides Function CreateInstance(ByVal itemType As Type) As Object
        Dim obj2 As Object = MyBase.CreateInstance(itemType)
        Return obj2
    End Function

    Protected Overrides Sub DestroyInstance(ByVal instance As Object)
        MyBase.DestroyInstance(instance)
    End Sub

    Public Overloads Overrides Function EditValue(ByVal context As ITypeDescriptorContext, ByVal isp As IServiceProvider, ByVal value As Object) As Object
        Dim instance As ButtonGroup = CType(context.Instance, ButtonGroup)
        Dim obj2 As Object = MyBase.EditValue(context, isp, value)
        instance.Invalidate()
        Return obj2
    End Function
End Class
