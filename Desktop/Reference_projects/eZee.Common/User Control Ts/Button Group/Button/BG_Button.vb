Imports System
Imports System.ComponentModel
Imports System.Drawing
Imports System.Runtime.CompilerServices
Imports System.Windows.Forms

Public Delegate Sub RefreshKeyRequiredEventHandler(ByVal sender As Object, ByVal e As BG_ButtonEventArgs)

<DesignTimeVisible(False), ToolboxItem(False)> _
<DefaultProperty("Normal_Display")> _
Public Class BG_Button
    Inherits Component

    Friend Control As ButtonGroup = Nothing

    Public Sub New()
        Me.m_TextFormat.Alignment = StringAlignment.Center
        Me.m_TextFormat.LineAlignment = StringAlignment.Center
        Me.m_TextAlignment = Alignment.MiddleCenter
        Me.m_BackColor = Color.White
        Me.m_ForeColor = Color.Black
        Me.m_SelectedBackColor = Color.LightGray
        Me.m_SelectedForeColor = Color.Blue
        Me.m_BorderColor = Color.Black
        Me.m_GradientBackColor = New GradientColor(Color.LightGray, Color.DarkGray, Drawing2D.LinearGradientMode.Vertical)
    End Sub

#Region " Button Value "
    Private mstrNormal_Display As String = String.Empty
    Private mstrShiftPressed_Display As String = String.Empty
    Private mstrValue As String = String.Empty

    Friend Event RefreshKeyRequired As RefreshKeyRequiredEventHandler

    <Description("Normal Display Name of button."), Browsable(True), Category("Value")> _
    <DefaultValue("")> _
    Public Property Normal_Display() As String
        Get
            Return mstrNormal_Display
        End Get
        Set(ByVal value As String)
            mstrNormal_Display = value
        End Set
    End Property

    <Description("Display Name of button when shift key pressed."), Browsable(True), Category("Value")> _
    <DefaultValue("")> _
    Public Property ShiftPressed_Display() As String
        Get
            Return mstrShiftPressed_Display
        End Get
        Set(ByVal value As String)
            mstrShiftPressed_Display = value
        End Set
    End Property

    <Description("Send Key Value of button."), Browsable(True), Category("Value")> _
    <DefaultValue("")> _
    Public Property Value() As String
        Get
            Return mstrValue
        End Get
        Set(ByVal value As String)
            mstrValue = value
        End Set
    End Property
#End Region

#Region " Appearance "
    Private m_BackColor As Color
    <Browsable(True), Category("Appearance"), Description("Button's Back color.")> _
    <DefaultValue(GetType(Color), "White")> _
    Public Property BackColor() As Color
        Get
            Return Me.m_BackColor
        End Get
        Set(ByVal value As Color)
            Me.m_BackColor = value
            Me.Invalidate()
        End Set
    End Property

    Private m_ForeColor As Color
    <Category("Appearance"), Description("Button's Fore color."), Browsable(True)> _
    <DefaultValue(GetType(Color), "Black")> _
    Public Property ForeColor() As Color
        Get
            Return Me.m_ForeColor
        End Get
        Set(ByVal value As Color)
            Me.m_ForeColor = value
            Me.Invalidate()
        End Set
    End Property

    Private m_SelectedBackColor As Color
    <Browsable(True), Category("Appearance"), Description("Button's Selected Back color.")> _
    <DefaultValue(GetType(Color), "LightGray")> _
    Public Property SelectedBackColor() As Color
        Get
            Return Me.m_SelectedBackColor
        End Get
        Set(ByVal value As Color)
            Me.m_SelectedBackColor = value
            Me.Invalidate()
        End Set
    End Property

    Private m_SelectedForeColor As Color
    <Browsable(True), Category("Appearance"), Description("Button's Selected Fore color.")> _
    <DefaultValue(GetType(Color), "Blue")> _
    Public Property SelectedForeColor() As Color
        Get
            Return Me.m_SelectedForeColor
        End Get
        Set(ByVal value As Color)
            Me.m_SelectedForeColor = value
            Me.Invalidate()
        End Set
    End Property

    Private m_GradientBackColor As GradientColor
    <Description("Specify a gradient back colors."), Browsable(True), Category("Gradient")> _
    <DefaultValue(GetType(GradientColor), "LightGray,DarkGray,Vertical")> _
    Public Property GradientBackColor() As GradientColor
        Get
            Return Me.m_GradientBackColor
        End Get
        Set(ByVal value As GradientColor)
            If value Is Nothing Then
                value = New GradientColor(Color.LightGray, Color.DarkGray, Drawing2D.LinearGradientMode.Vertical)
            End If
            Me.m_GradientBackColor = value
            Me.Invalidate()
        End Set
    End Property


    Private m_BorderColor As Color
    <Browsable(True), Category("Appearance"), Description("Border's color.")> _
    <DefaultValue(GetType(Color), "Black")> _
    Public Property BorderColor() As Color
        Get
            Return Me.m_BorderColor
        End Get
        Set(ByVal value As Color)
            Me.m_BorderColor = value
            Me.Invalidate()
        End Set
    End Property

    Private m_BorderWidth As Single = 1.0F

    <Browsable(True), Category("Appearance"), Description("Border's width.")> _
    <DefaultValue(GetType(Single), "1.0")> _
    Public Property BorderWidth() As Single
        Get
            Return Me.m_BorderWidth
        End Get
        Set(ByVal value As Single)
            Me.m_BorderWidth = value
            Me.Invalidate()
        End Set
    End Property

    Private mrctBounds As Rectangle = New Rectangle(0, 0, 50, 50)
    <Category("Appearance"), Description("Button's Position and Size."), Browsable(True)> _
    <DefaultValue(GetType(Rectangle), "0,0,50,50")> _
    Public Property Bounds() As Rectangle
        Get
            Return mrctBounds
        End Get
        Set(ByVal value As Rectangle)
            mrctBounds = value
        End Set
    End Property

    Private m_Font As New System.Drawing.Font("Tahoma", 10.0F, FontStyle.Regular)
    <Category("Appearance"), Description("Button's font style."), Browsable(True)> _
    <DefaultValue(GetType(System.Drawing.Font), "Tahoma, 10pt")> _
    Public Property Font() As System.Drawing.Font
        Get
            Return Me.m_Font
        End Get
        Set(ByVal value As System.Drawing.Font)
            Me.m_Font = value
            Me.Invalidate()
        End Set
    End Property

    Private m_TextAlignment As Alignment
    <Description("The text's alignment."), Browsable(True), Category("Appearance")> _
    <DefaultValue(GetType(Alignment), "MiddleCenter")> _
    Public Property TextAlignment() As Alignment
        Get
            Return Me.m_TextAlignment
        End Get
        Set(ByVal value As Alignment)
            Me.m_TextAlignment = value
            Me.OnTextAlignmentChanged()
            Me.Invalidate()
        End Set
    End Property

    Private m_TextFormat As New StringFormat()
    <Browsable(False)> _
    Public ReadOnly Property TextFormat() As StringFormat
        Get
            Return Me.m_TextFormat
        End Get
    End Property

#End Region


    Private mblnPressed As Boolean = False
    <Browsable(False)> _
    Friend Property Pressed() As Boolean
        Get
            Return mblnPressed
        End Get
        Set(ByVal value As Boolean)
            mblnPressed = value
        End Set
    End Property

    Friend Sub Invalidate()
        Me.OnRefreshKeyRequired()
    End Sub

    Protected Overridable Sub OnRefreshKeyRequired()
        If Not Me.RefreshKeyRequiredEvent Is Nothing Then
            RaiseEvent RefreshKeyRequired(Me, New BG_ButtonEventArgs(Me))
        End If
    End Sub

    Private Sub OnTextAlignmentChanged()
        Select Case Me.m_TextAlignment
            Case Alignment.TopLeft
                Me.m_TextFormat.LineAlignment = StringAlignment.Near
                Me.m_TextFormat.Alignment = StringAlignment.Near
                Return

            Case Alignment.TopCenter
                Me.m_TextFormat.LineAlignment = StringAlignment.Near
                Me.m_TextFormat.Alignment = StringAlignment.Center
                Return

            Case Alignment.TopRight
                Me.m_TextFormat.LineAlignment = StringAlignment.Near
                Me.m_TextFormat.Alignment = StringAlignment.Far
                Return

            Case Alignment.MiddleLeft
                Me.m_TextFormat.LineAlignment = StringAlignment.Center
                Me.m_TextFormat.Alignment = StringAlignment.Near
                Return

            Case Alignment.MiddleCenter
                Me.m_TextFormat.LineAlignment = StringAlignment.Center
                Me.m_TextFormat.Alignment = StringAlignment.Center
                Return

            Case Alignment.MiddleRight
                Me.m_TextFormat.LineAlignment = StringAlignment.Center
                Me.m_TextFormat.Alignment = StringAlignment.Far
                Return

            Case Alignment.BottomLeft
                Me.m_TextFormat.LineAlignment = StringAlignment.Far
                Me.m_TextFormat.Alignment = StringAlignment.Near
                Return

            Case Alignment.BottomCenter
                Me.m_TextFormat.LineAlignment = StringAlignment.Far
                Me.m_TextFormat.Alignment = StringAlignment.Center
                Return

            Case Alignment.BottomRight
                Me.m_TextFormat.LineAlignment = StringAlignment.Far
                Me.m_TextFormat.Alignment = StringAlignment.Far
                Return
        End Select
    End Sub

End Class
