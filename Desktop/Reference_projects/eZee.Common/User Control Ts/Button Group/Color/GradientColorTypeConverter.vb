Imports System
Imports System.ComponentModel
Imports System.ComponentModel.Design.Serialization
Imports System.Drawing
Imports System.Globalization
Imports System.Reflection

Friend Class GradientColorTypeConverter
    Inherits ExpandableObjectConverter

    Public Overloads Overrides Function CanConvertFrom(ByVal context As ITypeDescriptorContext, ByVal sourceType As Type) As Boolean
        Return ((sourceType Is GetType(InstanceDescriptor)) OrElse MyBase.CanConvertFrom(context, sourceType))
    End Function

    Public Overloads Overrides Function CanConvertTo(ByVal context As ITypeDescriptorContext, ByVal destinationType As Type) As Boolean
        Return ((destinationType Is GetType(String)) OrElse ((destinationType Is GetType(InstanceDescriptor)) OrElse MyBase.CanConvertTo(context, destinationType)))
    End Function

    Public Overloads Overrides Function ConvertTo(ByVal context As ITypeDescriptorContext, ByVal culture As CultureInfo, ByVal value As Object, ByVal destinationType As Type) As Object
        If (destinationType Is GetType(InstanceDescriptor)) AndAlso (TypeOf value Is GradientColor) Then
            Dim component As GradientColor = CType(value, GradientColor)
            Dim constructor As ConstructorInfo = GetType(GradientColor).GetConstructor(New Type() {GetType(Color), GetType(Color), GetType(Drawing2D.LinearGradientMode)})
            If Not constructor Is Nothing Then
                Try
                    Dim properties As PropertyDescriptorCollection = TypeDescriptor.GetProperties(component)
                    Dim color2 As Color = CType(properties("StartColor").GetValue(component), Color)
                    Dim color3 As Color = CType(properties("EndColor").GetValue(component), Color)
                    Return New InstanceDescriptor(constructor, New Object() {color2, color3, component.FillDirection}, True)
                Catch
                    Return New InstanceDescriptor(constructor, New Object() {component.StartColor, component.EndColor, component.FillDirection}, True)
                End Try
            End If
        End If

        Return MyBase.ConvertTo(context, culture, value, destinationType)
    End Function

    Public Overloads Overrides Function GetProperties(ByVal context As ITypeDescriptorContext, ByVal value As Object, ByVal attributes() As Attribute) As PropertyDescriptorCollection
        Return TypeDescriptor.GetProperties(value, attributes).Sort(New String() {"StartColor", "EndColor", "FillDirection"})
    End Function
End Class

