Imports System.Drawing
Imports System.Windows.Forms

Public Class eZeeKDSItemList

    Public Event Selecting As SelectedEventHandler
    Public Delegate Sub SelectedEventHandler(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Dim mblnIsKOT As Boolean = True
    Public Property _IsKOT() As Boolean
        Get
            Return mblnIsKOT
        End Get
        Set(ByVal value As Boolean)
            mblnIsKOT = value
        End Set
    End Property

    Dim mintMasterUnkId As Integer = -1
    Public Property _MasterUnkId() As Integer
        Get
            Return mintMasterUnkId
        End Get
        Set(ByVal value As Integer)
            mintMasterUnkId = value
        End Set
    End Property

    Dim mintDisplayNumber As Integer = 0
    Public Property _DisplayNumber() As Integer
        Get
            Return mintDisplayNumber
        End Get
        Set(ByVal value As Integer)
            mintDisplayNumber = value
        End Set
    End Property

    Dim mstrMsgLine1 As String = ""
    Public Property _MsgLine1() As String
        Get
            Return mstrMsgLine1
        End Get
        Set(ByVal value As String)
            mstrMsgLine1 = value
        End Set
    End Property

    Dim mstrMsgLine2 As String = ""
    Public Property _MsgLine2() As String
        Get
            Return mstrMsgLine2
        End Get
        Set(ByVal value As String)
            mstrMsgLine2 = value
        End Set
    End Property

    Dim mdtItemData As DataTable = Nothing
    Public Property _ItemData() As DataTable
        Get
            If mdtItemData Is Nothing Then
                Call CreateTable()
            End If
            Return mdtItemData
        End Get
        Set(ByVal value As DataTable)
            mdtItemData = value
        End Set
    End Property

    Dim mblnSelected As Boolean = False
    Public Property Selected() As Boolean
        Get
            Return mblnSelected
        End Get
        Set(ByVal value As Boolean)
            mblnSelected = value

            If mblnSelected Then
                lblDisplayNumber.BackColor = Color.LightGreen
            Else
                lblDisplayNumber.BackColor = Color.LightGray
            End If

            RaiseEvent Selecting(Me, New System.EventArgs)
        End Set
    End Property


    Dim mblnLock As Boolean = True
    Public Property _Lock() As Boolean
        Get
            Return mblnLock
        End Get
        Set(ByVal value As Boolean)
            mblnLock = value
        End Set
    End Property
    'Rashmi (06 Jun 2009) -- Start

    Dim mblnKithcenView As Boolean = True
    Public Property _IsKitchenView() As Boolean
        Get
            Return mblnKithcenView
        End Get
        Set(ByVal value As Boolean)
            mblnKithcenView = value
        End Set
    End Property
    'Rashmi (06 Jun 2009) -- End

    'Rashmi (23 Jul 2009) -- Start
    Dim mdblSize As Double = 8.5
    Public WriteOnly Property _FontSize() As Double
        Set(ByVal value As Double)
            mdblSize = value
        End Set
    End Property

    Dim mstrFont As String = "Tahoma"
    Public WriteOnly Property _Font() As String
        Set(ByVal value As String)
            mstrFont = value
        End Set
    End Property

    Dim mintFontStyle As Integer
    Public WriteOnly Property _FontStyle() As Integer
        Set(ByVal value As Integer)
            mintFontStyle = value
        End Set
    End Property

    Dim mintVoidColor As Integer
    Public WriteOnly Property _VoidColor() As Integer
        Set(ByVal value As Integer)
            mintVoidColor = value
        End Set
    End Property

    Dim mintModifiedColor As Integer
    Public WriteOnly Property _ModifiedColor() As Integer
        Set(ByVal value As Integer)
            mintModifiedColor = value
        End Set
    End Property

    Dim mintBumpedColor As Integer
    Public WriteOnly Property _BumpedColor() As Integer
        Set(ByVal value As Integer)
            mintBumpedColor = value
        End Set
    End Property
    'Rashmi (23 Jul 2009) -- End

    Public Sub RefreshItem()
        Try
            lblDisplayNumber.Text = mintDisplayNumber
            lblMsgLine1.Text = mstrMsgLine1
            lblMsgLine2.Text = mstrMsgLine2
            pnlLock.Visible = mblnLock
            If mblnSelected Then
                lblDisplayNumber.BackColor = Color.LightGreen
            Else
                lblDisplayNumber.BackColor = Color.LightGray
            End If

            If mdtItemData Is Nothing Then
                Call CreateTable()
            End If

            dgvList.AutoGenerateColumns = False

            dgcolQty.DataPropertyName = "Qty"
            dgcolUnit.DataPropertyName = "UnitName"
            dgcolDescription.DataPropertyName = "ItemName"
            dgcolStatus.DataPropertyName = "Status"
            dgcolModifier.DataPropertyName = "isModifier"
            dgcolTime.DataPropertyName = "Time"
            dgvList.DataSource = mdtItemData

            dgvList.Refresh()

        Catch ex As Exception

        End Try
    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub CreateTable()
        mdtItemData = New DataTable

        mdtItemData.Columns.Add(New DataColumn("Qty", System.Type.GetType("System.String")))
        mdtItemData.Columns.Add(New DataColumn("UnitName", System.Type.GetType("System.String")))
        mdtItemData.Columns.Add(New DataColumn("ItemName", System.Type.GetType("System.String")))
        mdtItemData.Columns.Add(New DataColumn("Status", System.Type.GetType("System.Int32")))
        mdtItemData.Columns.Add(New DataColumn("masterunkid", System.Type.GetType("System.Int32")))
        mdtItemData.Columns.Add(New DataColumn("tranunkid", System.Type.GetType("System.Int32")))
        mdtItemData.Columns.Add(New DataColumn("iskot", System.Type.GetType("System.Boolean")))
        mdtItemData.Columns.Add(New DataColumn("isModifier", System.Type.GetType("System.Boolean")))
        mdtItemData.Columns.Add(New DataColumn("Time", System.Type.GetType("System.String")))

    End Sub


    Private Sub Me_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
        dgvList.Click, lblDisplayNumber.Click, lblMsgLine1.Click, lblMsgLine2.Click, pnlBottom.Click, pnlMain.Click, pnlTop.Click, pnlLock.Click
        Me.OnClick(e)
    End Sub
    'Rashmi (06 Jun 2009) -- Start
    ' Private Sub dgvList_RowPostPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs) Handles dgvList.RowPostPaint

    'Select Case dgvList.Rows(e.RowIndex).Cells(dgcolModifier.Index).Value
    '    Case 0
    '        Select Case dgvList.Rows(e.RowIndex).Cells(dgcolStatus.Index).Value
    '            Case 0 ' New
    '                dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Black
    '            Case 1 ' Edit
    '                dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Blue
    '            Case 2 ' Void
    '                dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Red
    '            Case 3 ' Bump
    '                dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Green
    '            Case 4
    '                If mblnKithcenView Then
    '                    Dim fnstyle As New DataGridViewCellStyle
    '                    fnstyle.BackColor = Color.Brown 'New Font(dgvList.Font, FontStyle.Strikeout)
    '                    dgvList.Rows(e.RowIndex).DefaultCellStyle.ApplyStyle(fnstyle) ' = New System.Drawing.Font(dgist.Font, FontStyle.Strikeout)
    '                End If
    '        End Select
    '    Case 1
    '        dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Gray
    'End Select

    'dgvList.Rows(e.RowIndex).DefaultCellStyle.SelectionForeColor = dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor

    'End Sub

    Private Sub dgvList_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvList.CellFormatting

        Select Case dgvList.Rows(e.RowIndex).Cells(dgcolModifier.Index).Value
            Case 0
                Select Case dgvList.Rows(e.RowIndex).Cells(dgcolStatus.Index).Value
                    Case 0 ' New
                        dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Black
                    Case 1 ' Edit
                        'Rashmi (23 Jul 2009) -- Start
                        dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = System.Drawing.ColorTranslator.FromWin32(mintModifiedColor) 'Color.Blue
                    Case 2 ' Void
                        dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = System.Drawing.ColorTranslator.FromWin32(mintVoidColor) 'Color.Red
                    Case 3 ' Bump
                        dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Green
                    Case 4 ' Bump
                        If mblnKithcenView Then
                            dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = System.Drawing.ColorTranslator.FromWin32(mintBumpedColor)
                            dgvList.Rows(e.RowIndex).DefaultCellStyle.Font = New System.Drawing.Font(dgvList.Font, FontStyle.Strikeout)
                        End If
                        'Rashmi (23 Jul 2009) -- End
                End Select
            Case 1
                dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Gray
        End Select

        dgvList.Rows(e.RowIndex).DefaultCellStyle.SelectionForeColor = dgvList.Rows(e.RowIndex).DefaultCellStyle.ForeColor
    End Sub
    'Rashmi (06 Jun 2009) -- End
End Class
