<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeKDSItemList
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.pnlBottom = New System.Windows.Forms.Panel
        Me.dgvList = New System.Windows.Forms.DataGridView
        Me.pnlLock = New System.Windows.Forms.Panel
        Me.pnlTop = New System.Windows.Forms.Panel
        Me.lblMsgLine2 = New System.Windows.Forms.Label
        Me.lblMsgLine1 = New System.Windows.Forms.Label
        Me.lblDisplayNumber = New System.Windows.Forms.Label
        Me.dgcolQty = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolUnit = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolTime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolModifier = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.pnlBottom.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMain.Controls.Add(Me.pnlBottom)
        Me.pnlMain.Controls.Add(Me.pnlTop)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(323, 386)
        Me.pnlMain.TabIndex = 0
        '
        'pnlBottom
        '
        Me.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBottom.Controls.Add(Me.pnlLock)
        Me.pnlBottom.Controls.Add(Me.dgvList)
        Me.pnlBottom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBottom.Location = New System.Drawing.Point(0, 53)
        Me.pnlBottom.Name = "pnlBottom"
        Me.pnlBottom.Size = New System.Drawing.Size(321, 331)
        Me.pnlBottom.TabIndex = 2
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.AllowUserToOrderColumns = True
        Me.dgvList.AllowUserToResizeColumns = False
        Me.dgvList.AllowUserToResizeRows = False
        Me.dgvList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvList.BackgroundColor = System.Drawing.Color.White
        Me.dgvList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.ColumnHeadersVisible = False
        Me.dgvList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolQty, Me.dgcolUnit, Me.dgcolDescription, Me.dgcolTime, Me.dgcolStatus, Me.dgcolModifier})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvList.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvList.Location = New System.Drawing.Point(0, 0)
        Me.dgvList.MultiSelect = False
        Me.dgvList.Name = "dgvList"
        Me.dgvList.ReadOnly = True
        Me.dgvList.RowHeadersVisible = False
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvList.RowsDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvList.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvList.Size = New System.Drawing.Size(319, 329)
        Me.dgvList.TabIndex = 0
        '
        'pnlLock
        '
        Me.pnlLock.BackColor = System.Drawing.Color.FromArgb(CType(CType(150, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.pnlLock.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlLock.Location = New System.Drawing.Point(0, 0)
        Me.pnlLock.Name = "pnlLock"
        Me.pnlLock.Size = New System.Drawing.Size(319, 329)
        Me.pnlLock.TabIndex = 1
        '
        'pnlTop
        '
        Me.pnlTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlTop.Controls.Add(Me.lblMsgLine2)
        Me.pnlTop.Controls.Add(Me.lblMsgLine1)
        Me.pnlTop.Controls.Add(Me.lblDisplayNumber)
        Me.pnlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTop.Location = New System.Drawing.Point(0, 0)
        Me.pnlTop.Name = "pnlTop"
        Me.pnlTop.Size = New System.Drawing.Size(321, 53)
        Me.pnlTop.TabIndex = 1
        '
        'lblMsgLine2
        '
        Me.lblMsgLine2.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblMsgLine2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblMsgLine2.Location = New System.Drawing.Point(60, 23)
        Me.lblMsgLine2.Name = "lblMsgLine2"
        Me.lblMsgLine2.Size = New System.Drawing.Size(259, 29)
        Me.lblMsgLine2.TabIndex = 2
        Me.lblMsgLine2.Text = "xxxx"
        '
        'lblMsgLine1
        '
        Me.lblMsgLine1.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblMsgLine1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsgLine1.Location = New System.Drawing.Point(60, 0)
        Me.lblMsgLine1.Name = "lblMsgLine1"
        Me.lblMsgLine1.Size = New System.Drawing.Size(259, 23)
        Me.lblMsgLine1.TabIndex = 1
        Me.lblMsgLine1.Text = "xxxx"
        '
        'lblDisplayNumber
        '
        Me.lblDisplayNumber.BackColor = System.Drawing.Color.LightBlue
        Me.lblDisplayNumber.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblDisplayNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisplayNumber.Location = New System.Drawing.Point(0, 0)
        Me.lblDisplayNumber.Name = "lblDisplayNumber"
        Me.lblDisplayNumber.Size = New System.Drawing.Size(60, 51)
        Me.lblDisplayNumber.TabIndex = 0
        Me.lblDisplayNumber.Text = "0"
        Me.lblDisplayNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dgcolQty
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolQty.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolQty.FillWeight = 50.0!
        Me.dgcolQty.HeaderText = "Qty"
        Me.dgcolQty.Name = "dgcolQty"
        Me.dgcolQty.ReadOnly = True
        Me.dgcolQty.Width = 50
        '
        'dgcolUnit
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolUnit.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolUnit.FillWeight = 50.0!
        Me.dgcolUnit.HeaderText = "Unit"
        Me.dgcolUnit.Name = "dgcolUnit"
        Me.dgcolUnit.ReadOnly = True
        Me.dgcolUnit.Width = 50
        '
        'dgcolDescription
        '
        Me.dgcolDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolDescription.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolDescription.FillWeight = 50.0!
        Me.dgcolDescription.HeaderText = "Description"
        Me.dgcolDescription.Name = "dgcolDescription"
        Me.dgcolDescription.ReadOnly = True
        '
        'dgcolTime
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolTime.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolTime.FillWeight = 50.0!
        Me.dgcolTime.HeaderText = "Time"
        Me.dgcolTime.Name = "dgcolTime"
        Me.dgcolTime.ReadOnly = True
        Me.dgcolTime.Width = 50
        '
        'dgcolStatus
        '
        Me.dgcolStatus.HeaderText = "Status"
        Me.dgcolStatus.Name = "dgcolStatus"
        Me.dgcolStatus.ReadOnly = True
        Me.dgcolStatus.Visible = False
        '
        'dgcolModifier
        '
        Me.dgcolModifier.HeaderText = "dgcolModifier"
        Me.dgcolModifier.Name = "dgcolModifier"
        Me.dgcolModifier.ReadOnly = True
        Me.dgcolModifier.Visible = False
        '
        'eZeeKDSItemList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.pnlMain)
        Me.Name = "eZeeKDSItemList"
        Me.Size = New System.Drawing.Size(323, 386)
        Me.pnlMain.ResumeLayout(False)
        Me.pnlBottom.ResumeLayout(False)
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTop.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents pnlTop As System.Windows.Forms.Panel
    Friend WithEvents lblDisplayNumber As System.Windows.Forms.Label
    Friend WithEvents pnlBottom As System.Windows.Forms.Panel
    Friend WithEvents lblMsgLine1 As System.Windows.Forms.Label
    Friend WithEvents lblMsgLine2 As System.Windows.Forms.Label
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents pnlLock As System.Windows.Forms.Panel
    Friend WithEvents dgcolQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolUnit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolModifier As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
