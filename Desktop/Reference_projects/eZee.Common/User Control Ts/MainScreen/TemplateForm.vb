Imports System.Drawing
Imports System.Windows.Forms

Public Class TemplateForm
    Private mstrHelpTag As String = ""
    Public Property _HelpTag() As String
        Get
            Return mstrHelpTag
        End Get
        Set(ByVal value As String)
            mstrHelpTag = value
        End Set
    End Property

    Private Sub TemplateForm_BackColorChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.BackColorChanged
        Me.BackColor = Color.FromArgb(5, 80, 150)
    End Sub

    Private Sub TemplateForm_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Me.Size = New Size(800, 570)
    End Sub

#Region " Help "
    Private Sub ShowHelp()
        If eZeeFrom_Configuration._ShowHelpButton Then
            If mstrHelpTag <> "" Then
                Help.ShowHelp(Me, eZeeFrom_Configuration._HelpFileName, mstrHelpTag & ".htm")
            Else
                Help.ShowHelp(Me, eZeeFrom_Configuration._HelpFileName, "Help_Not_Found.htm")
            End If
        End If
    End Sub

    Private Sub TemplateForm_HelpButtonClicked(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.HelpButtonClicked
        e.Cancel = True
        Call Me.ShowHelp()
    End Sub

    Private Sub TemplateForm_HelpRequested(ByVal sender As System.Object, ByVal hlpevent As System.Windows.Forms.HelpEventArgs) Handles MyBase.HelpRequested
        Call Me.ShowHelp()
    End Sub
#End Region
End Class