Imports System.Windows.Forms

Public Class eZeeCollapsibleDataGrid
    Inherits DataGridView

#Region " Private Variables "

    Private mstrGroupByField As String = ""
    Private mdtCustomDataSource As DataTable = Nothing

#End Region

    Public Enum enFamily
        Child = 0
        Parent = 1
    End Enum

#Region " Properties "

    <System.ComponentModel.Browsable(False)> _
    Public Property GroupBy() As String
        Get
            Return mstrGroupByField
        End Get
        Set(ByVal value As String)
            mstrGroupByField = value
            If mstrGroupByField <> "" Then
                Call CreateGroupByColumn(mstrGroupByField)
                'Else
                '    Call DeleteGroupByColumn()
            End If
        End Set
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public Property CustomDataSource() As DataTable
        Get
            Return mdtCustomDataSource
        End Get
        Set(ByVal value As DataTable)
            mdtCustomDataSource = value
            If Not mdtCustomDataSource Is Nothing Then
                MyBase.DataSource = New DataView(mdtCustomDataSource, "", mstrGroupByField, DataViewRowState.CurrentRows).ToTable
            End If
        End Set
    End Property

#End Region

#Region " Private & Public Methods "

    Private Sub DeleteGroupByColumn()
        'Finding and deleting the collapsible column
        Try
            Dim dtCol As DataGridViewColumn
            For Each dtCol In MyBase.Columns
                If dtCol.Name = "colCollapse" Then
                    MyBase.Columns.Remove(dtCol)
                    Exit For
                End If
            Next
        Catch ex As Exception
            'MsgBox(ex.Message)
            Throw ex
        End Try
    End Sub

    Public Function GetGroupColumnName() As String
        Dim strGroupByColumn As String = ""
        Try
            For Each col As DataGridViewTextBoxColumn In MyBase.Columns
                If col.DataPropertyName = mstrGroupByField Then
                    strGroupByColumn = col.Name
                    Exit For
                End If
            Next
            Return strGroupByColumn
        Catch ex As Exception
            'MsgBox(ex.Message)
            Return ""
        End Try
    End Function

    Public Sub SetGrouping()
        Try

            Dim intFirstRow As Integer = -1
            Dim strGroupByColumn As String = ""
            'HRK - 01 Jul 2010 - Start
            'Issue : Sinlge Group Head (+ Sign ) Issue
            Dim isLastParent As Boolean = False
            'HRK - 01 Jul 2010 - End
            strGroupByColumn = GetGroupColumnName()

            For i As Integer = 0 To MyBase.RowCount - 1
                If i = MyBase.RowCount - 1 Then
                    If intFirstRow = 0 Then
                        'HRK - 01 Jul 2010 - Start
                        'Issue : Sinlge Group Head (+ Sign ) Issue
                        'MyBase.Rows(i).Cells("colCollapse").Value = "+"
                        MyBase.Rows(i).Cells("colCollapse").Value = ""
                        'HRK - 01 Jul 2010 - End
                        'MyBase.Rows(i).DefaultCellStyle.Font = New Drawing.Font("Microsoft Sans Serif", 8, Drawing.FontStyle.Bold)
                        MyBase.Rows(i).DefaultCellStyle.Font = New Drawing.Font("Tahoma", 9, Drawing.FontStyle.Bold, Drawing.GraphicsUnit.Pixel, Nothing)
                        MyBase.Rows(i).Cells("colCollapse").Style.Font = New Drawing.Font("Verdana", 9, Drawing.FontStyle.Bold)
                        'MyBase.Rows(i).Cells("colCollapse").Tag = MyBase.Rows(i).Cells(strGroupByColumn).Value
                    ElseIf intFirstRow > 0 Then
                        MyBase.Rows(i).Cells("colCollapse").Value = ""
                        MyBase.Rows(i).Visible = False
                        'MyBase.Rows(i).Cells("colCollapse").Tag = MyBase.Rows(i).Cells(strGroupByColumn).Value
                    Else
                        'HRK - 01 Jul 2010 - Start
                        'Issue : Sinlge Group Head (+ Sign ) Issue
                        'MyBase.Rows(i).Cells("colCollapse").Value = "+"
                        MyBase.Rows(i).Cells("colCollapse").Value = ""
                        'HRK - 01 Jul 2010 - End
                        'MyBase.Rows(i).DefaultCellStyle.Font = New Drawing.Font("Tahoma", 8, Drawing.FontStyle.Bold)
                        MyBase.Rows(i).DefaultCellStyle.Font = New Drawing.Font("Tahoma", 9, Drawing.FontStyle.Bold, Drawing.GraphicsUnit.Pixel, Nothing)
                        MyBase.Rows(i).Cells("colCollapse").Style.Font = New Drawing.Font("Verdana", 9, Drawing.FontStyle.Bold)
                    End If
                    MyBase.Rows(i).Cells("colCollapse").Tag = MyBase.Rows(i).Cells(strGroupByColumn).Value
                    Exit For
                End If
                If MyBase.Rows(i).Cells(strGroupByColumn).Value = MyBase.Rows(i + 1).Cells(strGroupByColumn).Value Then
                    intFirstRow += 1
                    If intFirstRow = 0 Then
                        'HRK - 01 Jul 2010 - Start
                        'Issue : Sinlge Group Head (+ Sign ) Issue
                        If isLastParent Then
                            MyBase.Rows(i - 1).Cells("colCollapse").Value = ""
                        End If
                        'HRK - 01 Jul 2010 - End
                        MyBase.Rows(i).Cells("colCollapse").Value = "+"
                        'MyBase.Rows(i).DefaultCellStyle.Font = New Drawing.Font("Tahoma", 8, Drawing.FontStyle.Bold)
                        MyBase.Rows(i).DefaultCellStyle.Font = New Drawing.Font("Tahoma", 9, Drawing.FontStyle.Bold, Drawing.GraphicsUnit.Pixel, Nothing)
                        MyBase.Rows(i).Cells("colCollapse").Style.Font = New Drawing.Font("Verdana", 9, Drawing.FontStyle.Bold)
                        'MyBase.Rows(i).Cells("colCollapse").Tag = MyBase.Rows(i).Cells(strGroupByColumn).Value
                        'HRK - 01 Jul 2010 - Start
                        'Issue : Last Child Row Treated As Group Head Issue
                        intFirstRow += 1
                        'HRK - 01 Jul 2010 - End
                        'HRK - 01 Jul 2010 - Start
                        'Issue : Sinlge Group Head (+ Sign ) Issue
                        isLastParent = True
                        'HRK - 01 Jul 2010 - End
                    Else
                        MyBase.Rows(i).Cells("colCollapse").Value = ""
                        MyBase.Rows(i).Visible = False
                        'MyBase.Rows(i).Cells("colCollapse").Tag = MyBase.Rows(i).Cells(strGroupByColumn).Value
                        'HRK - 01 Jul 2010 - Start
                        'Issue : Sinlge Group Head (+ Sign ) Issue
                        isLastParent = False
                        'HRK - 01 Jul 2010 - End
                    End If
                    MyBase.Rows(i).Cells("colCollapse").Tag = MyBase.Rows(i).Cells(strGroupByColumn).Value
                ElseIf intFirstRow > -1 Then
                    'MyBase.Rows(i).Cells(0).Value = "-"
                    MyBase.Rows(i).Cells("colCollapse").Value = ""
                    MyBase.Rows(i).Visible = False
                    MyBase.Rows(i).Cells("colCollapse").Tag = MyBase.Rows(i).Cells(strGroupByColumn).Value
                    intFirstRow = -1
                    'HRK - 01 Jul 2010 - Start
                    'Issue : Sinlge Group Head (+ Sign ) Issue
                    isLastParent = False
                    'HRK - 01 Jul 2010 - End
                Else
                    'HRK - 01 Jul 2010 - Start
                    'Issue : Sinlge Group Head (+ Sign ) Issue
                    If isLastParent Then
                        MyBase.Rows(i - 1).Cells("colCollapse").Value = ""
                    End If
                    'HRK - 01 Jul 2010 - End
                    MyBase.Rows(i).Cells("colCollapse").Value = "+"
                    MyBase.Rows(i).DefaultCellStyle.Font = New Drawing.Font("Tahoma", 9, Drawing.FontStyle.Bold, Drawing.GraphicsUnit.Pixel, Nothing)
                    MyBase.Rows(i).Cells("colCollapse").Style.Font = New Drawing.Font("Verdana", 9, Drawing.FontStyle.Bold)
                    MyBase.Rows(i).Cells("colCollapse").Tag = MyBase.Rows(i).Cells(strGroupByColumn).Value
                    'HRK - 01 Jul 2010 - Start
                    'Issue : Sinlge Group Head (+ Sign ) Issue
                    isLastParent = True
                    'HRK - 01 Jul 2010 - End
                End If
            Next
        Catch ex As Exception
            Throw ex
            'MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CreateGroupByColumn(ByVal strGroupByField As String)
        Try
            'Creating and adding collapsible columne to datagridview
            Dim dtCol As DataGridViewColumn
            Dim blnColumnExist As Boolean = False
            For Each dtCol In MyBase.Columns
                If dtCol.Name = "colCollapse" Then
                    blnColumnExist = True
                    Exit For
                End If
            Next

            If Not blnColumnExist Then
                MyBase.SelectionMode = DataGridViewSelectionMode.FullRowSelect
                MyBase.MultiSelect = False

                dtCol = New DataGridViewTextBoxColumn
                dtCol.ValueType = System.Type.GetType("System.Type.String")
                dtCol.ReadOnly = True
                dtCol.DisplayIndex = 0
                'dtCol.DefaultCellStyle.Font = New Drawing.Font("Verdana", 10, Drawing.FontStyle.Bold)
                dtCol.Name = "colCollapse"
                dtCol.Width = 25
                dtCol.HeaderText = ""
                dtCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                MyBase.Columns.Add(dtCol)
                For Each col As DataGridViewColumn In MyBase.Columns
                    col.SortMode = DataGridViewColumnSortMode.NotSortable
                    'col.DefaultCellStyle.WrapMode = DataGridViewTriState.True
                Next
            End If
        Catch ex As Exception
            Throw ex
            'MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CollapseRow()
        Dim strGroupColName As String = ""
        Try
            Dim i As Integer = 0
            strGroupColName = GetGroupColumnName()
            For Each dgvRow As DataGridViewRow In MyBase.Rows
                If MyBase.SelectedRows(0).Cells("colCollapse").Tag = dgvRow.Cells("colCollapse").Tag Then
                    If i <> 0 Then
                        dgvRow.Visible = False
                    End If
                    i += 1
                End If
            Next
        Catch ex As Exception
            Throw ex
            'MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ExpandRow()
        Dim strGroupColName As String = ""
        Try
            Dim i As Integer = 0
            strGroupColName = GetGroupColumnName()
            For Each dgvRow As DataGridViewRow In MyBase.Rows
                If MyBase.SelectedRows(0).Cells("colCollapse").Tag = dgvRow.Cells("colCollapse").Tag Then
                    If i <> 0 Then
                        dgvRow.Visible = True
                    End If
                    i += 1
                End If
            Next
        Catch ex As Exception
            Throw ex
            'MsgBox(ex.Message)
        End Try
    End Sub

#End Region

#Region " Overrides Events "

    Protected Overrides Sub OnCellClick(ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        MyBase.OnCellClick(e)

        If MyBase.RowCount < 1 Then Exit Sub
        If e.ColumnIndex <> MyBase.Columns("colCollapse").Index Then Exit Sub

        Try
            If MyBase.SelectedRows(0).Cells("colCollapse").Value = "+" Then
                MyBase.SelectedRows(0).Cells("colCollapse").Value = "-"
                MyBase.SelectedRows(0).Cells("colCollapse").Style.Font = New Drawing.Font("Verdana", 12, Drawing.FontStyle.Bold)
                Call ExpandRow()
            ElseIf MyBase.SelectedRows(0).Cells("colCollapse").Value = "-" Then
                MyBase.SelectedRows(0).Cells("colCollapse").Value = "+"
                MyBase.SelectedRows(0).Cells("colCollapse").Style.Font = New Drawing.Font("Verdana", 9, Drawing.FontStyle.Bold)
                Call CollapseRow()
            End If
        Catch ex As Exception
            'MsgBox(ex.Message)
            Throw ex
        End Try
    End Sub

    Protected Overrides Sub OnCellDoubleClick(ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        MyBase.OnCellDoubleClick(e)

        If MyBase.RowCount < 1 Then Exit Sub
        'If dgvItemTran.DefaultCellStyle.ForeColor = Color.RoyalBlue Then Exit Sub
        If e.ColumnIndex = MyBase.Columns("colCollapse").Index Then Exit Sub

        Try
            If MyBase.SelectedRows(0).Cells("colCollapse").Value = "+" Then
                MyBase.SelectedRows(0).Cells("colCollapse").Value = "-"
                MyBase.SelectedRows(0).Cells("colCollapse").Style.Font = New Drawing.Font("Verdana", 12, Drawing.FontStyle.Bold)
                Call ExpandRow()
            ElseIf MyBase.SelectedRows(0).Cells("colCollapse").Value = "-" Then
                MyBase.SelectedRows(0).Cells("colCollapse").Value = "+"
                MyBase.SelectedRows(0).Cells("colCollapse").Style.Font = New Drawing.Font("Verdana", 9, Drawing.FontStyle.Bold)
                Call CollapseRow()
            End If
        Catch ex As Exception
            Throw ex
            'MsgBox(ex.Message)
        End Try
    End Sub

    Protected Overrides Sub OnKeyDown(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.OnKeyDown(e)

        If MyBase.Rows.Count < 1 Then Exit Sub
        'If MyBase.CurrentCell.ColumnIndex >= colPrice.Index Then Exit Sub
        Try
            Select Case e.KeyCode
                Case Keys.Left
                    If MyBase.SelectedRows(0).Cells("colCollapse").Value <> "" Then
                        If MyBase.SelectedRows(0).Cells("colCollapse").Value.ToString = "+" _
                                Or MyBase.SelectedRows(0).Cells("colCollapse").Value.ToString = "" Then
                            Exit Try
                        End If
                        MyBase.SelectedRows(0).Cells("colCollapse").Value = "+"
                        MyBase.SelectedRows(0).Cells("colCollapse").Style.Font = New Drawing.Font("Verdana", 9, Drawing.FontStyle.Bold)
                        MyBase.SelectedRows(0).Cells("colCollapse").Selected = True
                        Call CollapseRow()
                    Else
                        For Each dgvRow As DataGridViewRow In MyBase.Rows
                            If (dgvRow.Cells("colCollapse").Tag = MyBase.SelectedRows(0).Cells("colCollapse").Tag) _
                                    And dgvRow.Cells("colCollapse").Value = "-" Then
                                dgvRow.Selected = True
                                Exit Try
                            End If
                        Next
                    End If
                Case Keys.Right
                    If MyBase.SelectedRows(0).Cells("colCollapse").Tag.ToString <> "" Then
                        If MyBase.SelectedRows(0).Cells("colCollapse").Value.ToString = "-" _
                                Or MyBase.SelectedRows(0).Cells("colCollapse").Value.ToString = "" Then
                            Exit Try
                        End If
                        MyBase.SelectedRows(0).Cells("colCollapse").Value = "-"
                        MyBase.SelectedRows(0).Cells("colCollapse").Style.Font = New Drawing.Font("Verdana", 12, Drawing.FontStyle.Bold)
                        MyBase.SelectedRows(0).Cells("colCollapse").Selected = True
                        Call ExpandRow()
                    End If
            End Select
        Catch ex As Exception
            'MsgBox(ex.Message)
            Throw ex
        End Try
    End Sub

#End Region

End Class
