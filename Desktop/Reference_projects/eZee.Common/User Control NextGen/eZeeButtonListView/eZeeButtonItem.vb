Public Class eZeeButtonItem
    Inherits System.Windows.Forms.RadioButton
    Implements ICloneable, Runtime.Serialization.ISerializable

#Region " Constructor "

    Public Sub New()
        Me.SetDefaultValues()
    End Sub

    Public Sub New(ByVal text As String)
        Me.SetDefaultValues()
        Me.Text = text
    End Sub

    Private Sub SetDefaultValues()
        Me.Appearance = System.Windows.Forms.Appearance.Button
        Me.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        'Orange
        'Me.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(192, 64, 0)
        'Me.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(255, 192, 128)
        'Me.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(192, 64, 0)
        'Me.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(255, 224, 192)
        'Blue
        'Me.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue
        'Me.FlatAppearance.CheckedBackColor = System.Drawing.Color.LightSteelBlue
        'Me.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSlateGray
        'Me.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        'Black
        Me.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gainsboro
        Me.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke

        Me.Size = New System.Drawing.Size(85, 85)
        Me.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    End Sub

    'Public Sub New(ByVal group As eZeeButtonGroup)
    '    Me.SetDefaultValues()
    '    Me.Group = group
    'End Sub

    'Public Sub New(ByVal text As String, ByVal group As eZeeButtonGroup)
    '    Me.SetDefaultValues()
    '    Me.Text = text
    '    Me.Group = group
    'End Sub

#End Region

#Region " Properties "

    Private m_intIndex As Integer = -1
    Private m_intItemId As Integer = -1
    Private m_intGroupId As Integer = -1
    Private m_Group As eZeeButtonGroup = Nothing
    Private m_Owner As eZeeButtonListView = Nothing

    Public Property Index() As Integer
        Get
            Return m_intIndex
        End Get
        Set(ByVal value As Integer)
            m_intIndex = value
        End Set
    End Property

    Public Property ItemId() As Integer
        Get
            Return m_intItemId
        End Get
        Set(ByVal value As Integer)
            m_intItemId = value
        End Set
    End Property

    Public Property GroupId() As Integer
        Get
            Return m_intGroupId
        End Get
        Set(ByVal value As Integer)
            m_intGroupId = value
        End Set
    End Property

    Public Property Group() As eZeeButtonGroup
        Get
            Return m_Group
        End Get
        Set(ByVal value As eZeeButtonGroup)
            m_Group = value
        End Set
    End Property

    <System.ComponentModel.DescriptionAttribute("Gets/sets the owning ButtonListView for this item")> _
    Friend Property Owner() As eZeeButtonListView
        Get
            Owner = m_Owner
        End Get
        Set(ByVal Value As eZeeButtonListView)
            m_Owner = Value
        End Set
    End Property

#End Region

#Region " Methods "

    Public Function Clone() As Object Implements System.ICloneable.Clone
        Return Me.Clone()
    End Function

    Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData

    End Sub

#End Region

#Region " Override Events "

    Protected Overrides Sub OnMouseClick(ByVal e As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseClick(e)

        Me.m_Owner.RaiseItemClickEvent(Me)
    End Sub

    Protected Overrides Sub OnKeyPress(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        MyBase.OnKeyPress(e)

        If Asc(e.KeyChar) = 13 Then
            Me.m_Owner.RaiseItemClickEvent(Me)
        End If
    End Sub

#End Region

End Class