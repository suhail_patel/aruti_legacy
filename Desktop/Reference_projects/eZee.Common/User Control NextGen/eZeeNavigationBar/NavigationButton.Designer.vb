Partial Class NavigationButton
    Inherits System.Windows.Forms.UserControl

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    'Component overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picImage = New System.Windows.Forms.PictureBox
        Me.lblText = New System.Windows.Forms.Label
        Me.lblSpace = New System.Windows.Forms.Label
        CType(Me.picImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picImage
        '
        Me.picImage.BackColor = System.Drawing.Color.Transparent
        Me.picImage.Dock = System.Windows.Forms.DockStyle.Left
        Me.picImage.InitialImage = Nothing
        Me.picImage.Location = New System.Drawing.Point(8, 3)
        Me.picImage.Name = "picImage"
        Me.picImage.Size = New System.Drawing.Size(29, 29)
        Me.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picImage.TabIndex = 0
        Me.picImage.TabStop = False
        '
        'lblText
        '
        Me.lblText.BackColor = System.Drawing.Color.Transparent
        Me.lblText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblText.Location = New System.Drawing.Point(42, 3)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(105, 29)
        Me.lblText.TabIndex = 1
        Me.lblText.Text = ""
        Me.lblText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSpace
        '
        Me.lblSpace.BackColor = System.Drawing.Color.Transparent
        Me.lblSpace.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblSpace.Location = New System.Drawing.Point(37, 3)
        Me.lblSpace.Name = "lblSpace"
        Me.lblSpace.Size = New System.Drawing.Size(5, 29)
        Me.lblSpace.TabIndex = 2
        '
        'NavigationButton
        '
        Me.Controls.Add(Me.lblText)
        Me.Controls.Add(Me.lblSpace)
        Me.Controls.Add(Me.picImage)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "NavigationButton"
        Me.Padding = New System.Windows.Forms.Padding(8, 3, 3, 3)
        Me.Size = New System.Drawing.Size(150, 35)
        CType(Me.picImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picImage As System.Windows.Forms.PictureBox
    Friend WithEvents lblText As System.Windows.Forms.Label
    Friend WithEvents lblSpace As System.Windows.Forms.Label

End Class
