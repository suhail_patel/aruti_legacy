Imports System
Imports System.Diagnostics
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
Imports System.Drawing
Imports System.ComponentModel
Imports System.ComponentModel.Component


''' <summary> 
''' Summary description for PieChartControl. 
''' </summary> 

Public Class eZeePieChart
    Inherits System.Windows.Forms.Panel

    ''' <summary> 
    ''' Sets the left margin for the chart. 
    ''' </summary> 
    Public WriteOnly Property LeftMargin() As Single
        Set(ByVal value As Single)
            Debug.Assert(value >= 0)
            m_leftMargin = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Sets the right margin for the chart. 
    ''' </summary> 
    Public WriteOnly Property RightMargin() As Single
        Set(ByVal value As Single)
            Debug.Assert(value >= 0)
            m_rightMargin = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Sets the top margin for the chart. 
    ''' </summary> 
    Public WriteOnly Property TopMargin() As Single
        Set(ByVal value As Single)
            Debug.Assert(value >= 0)
            m_topMargin = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Sets the bottom margin for the chart. 
    ''' </summary> 
    Public WriteOnly Property BottomMargin() As Single
        Set(ByVal value As Single)
            Debug.Assert(value >= 0)
            m_bottomMargin = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Sets the indicator if chart should fit the bounding rectangle 
    ''' exactly. 
    ''' </summary> 
    Public WriteOnly Property FitChart() As Boolean
        Set(ByVal value As Boolean)
            m_fitChart = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Sets values to be represented by the chart. 
    ''' </summary> 
    Public WriteOnly Property Values() As Decimal()
        Set(ByVal value As Decimal())
            m_values = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Sets colors to be used for rendering pie slices. 
    ''' </summary> 
    Public WriteOnly Property Colors() As Color()
        Set(ByVal value As Color())
            m_colors = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Sets values for slice displacements. 
    ''' </summary> 
    Public WriteOnly Property SliceRelativeDisplacements() As Single()
        Set(ByVal value As Single())
            m_relativeSliceDisplacements = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Gets or sets tooltip texts. 
    ''' </summary> 
    Public Property ToolTips() As String()
        Get
            Return m_toolTipTexts
        End Get
        Set(ByVal value As String())
            m_toolTipTexts = value
        End Set
    End Property

    ''' <summary> 
    ''' Sets texts appearing by each pie slice. 
    ''' </summary> 
    Public WriteOnly Property Texts() As String()
        Set(ByVal value As String())
            m_texts = value
        End Set
    End Property

    ''' <summary> 
    ''' Sets pie slice reative height. 
    ''' </summary> 
    Public WriteOnly Property SliceRelativeHeight() As Single
        Set(ByVal value As Single)
            m_sliceRelativeHeight = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Sets the shadow style. 
    ''' </summary> 
    Public WriteOnly Property ShadowStyle() As ShadowStyle
        Set(ByVal value As ShadowStyle)
            m_shadowStyle = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Sets the edge color type. 
    ''' </summary> 
    Public WriteOnly Property EdgeColorType() As EdgeColorType
        Set(ByVal value As EdgeColorType)
            m_edgeColorType = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Sets the edge lines width. 
    ''' </summary> 
    Public WriteOnly Property EdgeLineWidth() As Single
        Set(ByVal value As Single)
            m_edgeLineWidth = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Sets the initial angle from which pies are drawn. 
    ''' </summary> 
    Public WriteOnly Property InitialAngle() As Single
        Set(ByVal value As Single)
            m_initialAngle = value
            Invalidate()
        End Set
    End Property

    Private ValueList As New ArrayList()
    Private TextList As New ArrayList()
    Private DisplacementList As New ArrayList()
    Private ColorList As New ArrayList()
    Private ToolTipList As New ArrayList()

    Public Sub AddSlice(ByVal SliceText As String, ByVal SliceValue As Single, ByVal SliceDisplacement As Decimal, ByVal SliceColor As Color, Optional ByVal SliceTips As String = "")
        ValueList.Add(CDec(SliceValue))
        TextList.Add(SliceText)
        DisplacementList.Add(CSng(SliceDisplacement))
        ColorList.Add(SliceColor)
        ToolTipList.Add(SliceTips)

        m_values = DirectCast(ValueList.ToArray(GetType(Decimal)), Decimal())
        m_texts = DirectCast(TextList.ToArray(GetType(String)), String())
        m_relativeSliceDisplacements = DirectCast(DisplacementList.ToArray(GetType(Single)), Single())
        m_colors = DirectCast(ColorList.ToArray(GetType(Color)), Color())
        m_toolTipTexts = DirectCast(ToolTipList.ToArray(GetType(String)), String())
    End Sub

    ''' <summary> 
    ''' Handles <c>OnPaint</c> event. 
    ''' </summary> 
    ''' <param name="args"> 
    ''' <c>PaintEventArgs</c> object. 
    ''' </param> 
    Protected Overloads Overrides Sub OnPaint(ByVal args As PaintEventArgs)
        MyBase.OnPaint(args)
        If HasAnyValue Then
            DoDraw(args.Graphics)
        End If
    End Sub

    ''' <summary> 
    ''' Sets values for the chart and draws them. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' Graphics object used for drawing. 
    ''' </param> 
    Protected Sub DoDraw(ByVal graphics As Graphics)
        If m_values IsNot Nothing AndAlso m_values.Length > 0 Then
            graphics.SmoothingMode = SmoothingMode.AntiAlias
            Dim width As Single = ClientSize.Width - m_leftMargin - m_rightMargin
            Dim height As Single = ClientSize.Height - m_topMargin - m_bottomMargin
            ' if the width or height if <=0 an exception would be thrown -> exit method.. 
            If width <= 0 OrElse height <= 0 Then
                Return
            End If
            If m_pieChart IsNot Nothing Then
                m_pieChart.Dispose()
            End If
            If m_colors IsNot Nothing AndAlso m_colors.Length > 0 Then
                m_pieChart = New PieChart(m_leftMargin, m_topMargin, width, height, m_values, m_colors, _
                m_sliceRelativeHeight, m_texts)
            Else
                m_pieChart = New PieChart(m_leftMargin, m_topMargin, width, height, m_values, m_sliceRelativeHeight, _
                m_texts)
            End If
            m_pieChart.FitToBoundingRectangle = m_fitChart
            m_pieChart.InitialAngle = m_initialAngle
            m_pieChart.SliceRelativeDisplacements = m_relativeSliceDisplacements
            m_pieChart.EdgeColorType = m_edgeColorType
            m_pieChart.EdgeLineWidth = m_edgeLineWidth
            m_pieChart.ShadowStyle = m_shadowStyle
            m_pieChart.HighlightedIndex = m_highlightedIndex

            m_pieChart.Values = m_values
            m_pieChart.Colors = m_colors
            m_pieChart.SliceRelativeDisplacements = m_relativeSliceDisplacements
            m_pieChart.Texts = m_texts
            'm_pieChart.toolTipTexts = m_toolTipTexts

            m_pieChart.Draw(graphics)
            m_pieChart.Font = Me.Font
            m_pieChart.ForeColor = Me.ForeColor
            m_pieChart.PlaceTexts(graphics)
        End If
    End Sub

    ''' <summary> 
    ''' Handles <c>MouseEnter</c> event to activate the tooltip. 
    ''' </summary> 
    ''' <param name="e"></param> 
    Protected Overloads Overrides Sub OnMouseEnter(ByVal e As System.EventArgs)
        MyBase.OnMouseEnter(e)
        m_defaultToolTipAutoPopDelay = m_toolTip.AutoPopDelay
        m_toolTip.AutoPopDelay = Int16.MaxValue
    End Sub

    ''' <summary> 
    ''' Handles <c>MouseLeave</c> event to disable tooltip. 
    ''' </summary> 
    ''' <param name="e"></param> 
    Protected Overloads Overrides Sub OnMouseLeave(ByVal e As System.EventArgs)
        MyBase.OnMouseLeave(e)
        m_toolTip.RemoveAll()
        m_toolTip.AutoPopDelay = m_defaultToolTipAutoPopDelay
        m_highlightedIndex = -1
        Refresh()
    End Sub

    ''' <summary> 
    ''' Handles <c>MouseMove</c> event to display tooltip for the pie 
    ''' slice under pointer and to display slice in highlighted color. 
    ''' </summary> 
    ''' <param name="e"></param> 
    Protected Overloads Overrides Sub OnMouseMove(ByVal e As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseMove(e)
        If m_pieChart IsNot Nothing AndAlso m_values IsNot Nothing AndAlso m_values.Length > 0 Then
            Dim index As Integer = m_pieChart.FindPieSliceUnderPoint(New PointF(e.X, e.Y))
            If index <> m_highlightedIndex Then
                m_highlightedIndex = index
                Refresh()
            End If
            If m_highlightedIndex <> -1 Then
                If m_toolTipTexts Is Nothing OrElse m_toolTipTexts.Length <= m_highlightedIndex OrElse m_toolTipTexts(m_highlightedIndex).Length = 0 Then
                    m_toolTip.SetToolTip(Me, m_values(m_highlightedIndex).ToString())
                Else
                    m_toolTip.SetToolTip(Me, m_toolTipTexts(m_highlightedIndex))
                End If
            Else
                m_toolTip.RemoveAll()
            End If
        End If
    End Sub


    ''' <summary> 
    ''' Gets a flag indicating if at least one value is nonzero. 
    ''' </summary> 
    Private ReadOnly Property HasAnyValue() As Boolean
        Get
            If m_values Is Nothing Then
                Return False
            End If
            For Each angle As Decimal In m_values
                If angle <> 0 Then
                    Return True
                End If
            Next
            Return False
        End Get
    End Property

    Private m_pieChart As PieChart = Nothing
    Private m_leftMargin As Single
    Private m_topMargin As Single
    Private m_rightMargin As Single
    Private m_bottomMargin As Single
    Private m_fitChart As Boolean = False

    Private m_values As Decimal() = Nothing
    Private m_colors As Color() = Nothing
    Private m_sliceRelativeHeight As Single
    Private m_relativeSliceDisplacements As Single() = New Single() {0.0F}
    Private m_texts As String() = Nothing
    Private m_toolTipTexts As String() = Nothing
    Private m_shadowStyle As ShadowStyle = eZee.Common.ShadowStyle.GradualShadow
    Private m_edgeColorType As EdgeColorType = eZee.Common.EdgeColorType.SurfaceColor
    Private m_edgeLineWidth As Single = 1.0F
    Private m_initialAngle As Single
    Private m_highlightedIndex As Integer = -1
    Private m_toolTip As ToolTip = Nothing
    ''' <summary> 
    ''' Default AutoPopDelay of the ToolTip control. 
    ''' </summary> 
    Private m_defaultToolTipAutoPopDelay As Integer
    ''' <summary> 
    ''' Flag indicating that object has been disposed. 
    ''' </summary> 
    Private m_disposed As Boolean = False
End Class
