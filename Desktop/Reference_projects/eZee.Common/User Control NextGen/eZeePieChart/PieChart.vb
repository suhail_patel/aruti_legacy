Imports System
Imports System.Collections
Imports System.Diagnostics
Imports System.Drawing

''' <summary> 
''' Object representing a pie chart. 
''' </summary> 
Public Class PieChart

    ''' <summary> 
    ''' Initializes an empty instance of <c>PieChart3D</c>. 
    ''' </summary> 
    Protected Sub New()
    End Sub

    ''' <summary> 
    ''' Initializes an instance of a flat <c>PieChart3D</c> with 
    ''' specified bounds, values to chart and relative thickness. 
    ''' </summary> 
    ''' <param name="xBoundingRect"> 
    ''' x-coordinate of the upper-left corner of the rectangle that 
    ''' bounds the chart. 
    ''' </param> 
    ''' <param name="yBoundingRect"> 
    ''' y-coordinate of the upper-left corner of the rectangle that 
    ''' bounds the chart. 
    ''' </param> 
    ''' <param name="widthBoundingRect"> 
    ''' Width of the rectangle that bounds the chart. 
    ''' </param> 
    ''' <param name="heightBoundingRect"> 
    ''' Height of the rectangle that bounds the chart. 
    ''' </param> 
    ''' <param name="values"> 
    ''' An array of <c>decimal</c> values to chart. 
    ''' </param> 
    Public Sub New(ByVal xBoundingRect As Single, ByVal yBoundingRect As Single, ByVal widthBoundingRect As Single, ByVal heightBoundingRect As Single, ByVal values As Decimal())
        Me.New()
        m_xBoundingRect = xBoundingRect
        m_yBoundingRect = yBoundingRect
        m_widthBoundingRect = widthBoundingRect
        m_heightBoundingRect = heightBoundingRect
        values = values
    End Sub

    ''' <summary> 
    ''' Initializes an instance of <c>PieChart3D</c> with specified 
    ''' bounds, values to chart and relative thickness. 
    ''' </summary> 
    ''' <param name="xBoundingRect"> 
    ''' x-coordinate of the upper-left corner of the rectangle bounding 
    ''' the chart. 
    ''' </param> 
    ''' <param name="yBoundingRect"> 
    ''' y-coordinate of the upper-left corner of the rectangle bounding 
    ''' the chart. 
    ''' </param> 
    ''' <param name="widthBoundingRect"> 
    ''' Width of the rectangle bounding the chart. 
    ''' </param> 
    ''' <param name="heightBoundingRect"> 
    ''' Height of the rectangle bounding the chart. 
    ''' </param> 
    ''' <param name="values"> 
    ''' An array of <c>decimal</c> values to chart. 
    ''' </param> 
    ''' <param name="sliceRelativeHeight"> 
    ''' Thickness of the pie slice to chart relative to the height of the 
    ''' bounding rectangle. 
    ''' </param> 
    Public Sub New(ByVal xBoundingRect As Single, ByVal yBoundingRect As Single, ByVal widthBoundingRect As Single, ByVal heightBoundingRect As Single, ByVal values As Decimal(), ByVal sliceRelativeHeight As Single)
        Me.New(xBoundingRect, yBoundingRect, widthBoundingRect, heightBoundingRect, values)
        m_sliceRelativeHeight = sliceRelativeHeight
    End Sub

    ''' <summary> 
    ''' Initializes a new instance of <c>PieChart3D</c> with given bounds, 
    ''' array of values and pie slice thickness. 
    ''' </summary> 
    ''' <param name="boundingRectangle"> 
    ''' Bounding rectangle. 
    ''' </param> 
    ''' <param name="values"> 
    ''' Array of values to initialize with. 
    ''' </param> 
    ''' <param name="sliceRelativeHeight"></param> 
    Public Sub New(ByVal boundingRectangle As RectangleF, ByVal values As Decimal(), ByVal sliceRelativeHeight As Single)
        Me.New(boundingRectangle.X, boundingRectangle.Y, boundingRectangle.Width, boundingRectangle.Height, values, sliceRelativeHeight)
    End Sub

    ''' <summary> 
    ''' Initializes a new instance of <c>PieChart3D</c> with given bounds, 
    ''' array of values and relative pie slice height. 
    ''' </summary> 
    ''' <param name="xBoundingRect"> 
    ''' x-coordinate of the upper-left corner of the rectangle bounding 
    ''' the chart. 
    ''' </param> 
    ''' <param name="yBoundingRect"> 
    ''' y-coordinate of the upper-left corner of the rectangle bounding 
    ''' the chart. 
    ''' </param> 
    ''' <param name="widthBoundingRect"> 
    ''' Width of the rectangle bounding the chart. 
    ''' </param> 
    ''' <param name="heightBoundingRect"> 
    ''' Height of the rectangle bounding the chart. 
    ''' </param> 
    ''' <param name="values"> 
    ''' An array of <c>decimal</c> values to chart. 
    ''' </param> 
    ''' <param name="sliceColors"> 
    ''' An array of colors used to render slices. 
    ''' </param> 
    ''' <param name="sliceRelativeHeight"> 
    ''' Thickness of the slice to chart relative to the height of the 
    ''' bounding rectangle. 
    ''' </param> 
    Public Sub New(ByVal xBoundingRect As Single, ByVal yBoundingRect As Single, ByVal widthBoundingRect As Single, ByVal heightBoundingRect As Single, ByVal values As Decimal(), ByVal sliceColors As Color(), _
    ByVal sliceRelativeHeight As Single)
        Me.New(xBoundingRect, yBoundingRect, widthBoundingRect, heightBoundingRect, values, sliceRelativeHeight)
        m_sliceColors = sliceColors
    End Sub

    ''' <summary> 
    ''' Initializes a new instance of <c>PieChart3D</c> with given bounds, 
    ''' array of values and corresponding colors. 
    ''' </summary> 
    ''' <param name="boundingRectangle"> 
    ''' Bounding rectangle. 
    ''' </param> 
    ''' <param name="values"> 
    ''' Array of values to chart. 
    ''' </param> 
    ''' <param name="sliceColors"> 
    ''' Colors used for rendering individual slices. 
    ''' </param> 
    ''' <param name="sliceRelativeHeight"> 
    ''' Pie slice relative height. 
    ''' </param> 
    Public Sub New(ByVal boundingRectangle As RectangleF, ByVal values As Decimal(), ByVal sliceColors As Color(), ByVal sliceRelativeHeight As Single)
        Me.New(boundingRectangle, values, sliceRelativeHeight)
        m_sliceColors = sliceColors
    End Sub

    ''' <summary> 
    ''' Initializes a new instance of <c>PieChart3D</c> with given bounds, 
    ''' array of values and relative pie slice height. 
    ''' </summary> 
    ''' <param name="xBoundingRect"> 
    ''' x-coordinate of the upper-left corner of the rectangle bounding 
    ''' the chart. 
    ''' </param> 
    ''' <param name="yBoundingRect"> 
    ''' y-coordinate of the upper-left corner of the rectangle bounding 
    ''' the chart. 
    ''' </param> 
    ''' <param name="widthBoundingRect"> 
    ''' Width of the rectangle bounding the chart. 
    ''' </param> 
    ''' <param name="heightBoundingRect"> 
    ''' Height of the rectangle bounding the chart. 
    ''' </param> 
    ''' <param name="values"> 
    ''' An array of <c>decimal</c> values to chart. 
    ''' </param> 
    ''' <param name="sliceColors"> 
    ''' An array of colors used to render slices. 
    ''' </param> 
    ''' <param name="sliceRelativeHeight"> 
    ''' Thickness of the slice to chart relative to the height of the 
    ''' bounding rectangle. 
    ''' </param> 
    ''' <param name="texts"> 
    ''' An array of strings that are displayed on corresponding slice. 
    ''' </param> 
    Public Sub New(ByVal xBoundingRect As Single, ByVal yBoundingRect As Single, ByVal widthBoundingRect As Single, ByVal heightBoundingRect As Single, ByVal values As Decimal(), ByVal sliceColors As Color(), _
    ByVal sliceRelativeHeight As Single, ByVal texts As String())
        Me.New(xBoundingRect, yBoundingRect, widthBoundingRect, heightBoundingRect, values, sliceColors, _
        sliceRelativeHeight)
        m_texts = texts
    End Sub

    ''' <summary> 
    ''' Initializes a new instance of <c>PieChart3D</c> with given bounds, 
    ''' array of values and relative pie slice height. 
    ''' </summary> 
    ''' <param name="xBoundingRect"> 
    ''' x-coordinate of the upper-left corner of the rectangle bounding 
    ''' the chart. 
    ''' </param> 
    ''' <param name="yBoundingRect"> 
    ''' y-coordinate of the upper-left corner of the rectangle bounding 
    ''' the chart. 
    ''' </param> 
    ''' <param name="widthBoundingRect"> 
    ''' Width of the rectangle bounding the chart. 
    ''' </param> 
    ''' <param name="heightBoundingRect"> 
    ''' Height of the rectangle bounding the chart. 
    ''' </param> 
    ''' <param name="values"> 
    ''' An array of <c>decimal</c> values to chart. 
    ''' </param> 
    ''' <param name="sliceRelativeHeight"> 
    ''' Thickness of the slice to chart relative to the height of the 
    ''' bounding rectangle. 
    ''' </param> 
    ''' <param name="texts"> 
    ''' An array of strings that are displayed on corresponding slice. 
    ''' </param> 
    Public Sub New(ByVal xBoundingRect As Single, ByVal yBoundingRect As Single, ByVal widthBoundingRect As Single, ByVal heightBoundingRect As Single, ByVal values As Decimal(), ByVal sliceRelativeHeight As Single, _
    ByVal texts As String())
        Me.New(xBoundingRect, yBoundingRect, widthBoundingRect, heightBoundingRect, values, sliceRelativeHeight)
        m_texts = texts
    End Sub
    Protected Overrides Sub Finalize()
        Try
            Dispose(False)
        Finally
            MyBase.Finalize()
        End Try
    End Sub

    ''' <summary> 
    ''' Implementation of <c>IDisposable</c> interface. 
    ''' </summary> 
    Public Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> 
    ''' Disposes of all pie slices. 
    ''' </summary> 
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not m_disposed Then
            If Not m_pieSlices Is Nothing Then
                If disposing Then
                    For Each slice As PieSlice In m_pieSlices
                        slice = Nothing
                    Next
                End If
            End If
            m_disposed = True
        End If
    End Sub

    ''' <summary> 
    ''' Sets values to be displayed on the chart. 
    ''' </summary> 
    Public WriteOnly Property Values() As Decimal()
        Set(ByVal value As Decimal())
            Debug.Assert(value IsNot Nothing AndAlso value.Length > 0)
            m_values = value
        End Set
    End Property

    ''' <summary> 
    ''' Sets colors used for individual pie slices. 
    ''' </summary> 
    Public WriteOnly Property Colors() As Color()
        Set(ByVal value As Color())
            m_sliceColors = value
        End Set
    End Property

    ''' <summary> 
    ''' Sets text displayed by slices. 
    ''' </summary> 
    Public WriteOnly Property Texts() As String()
        Set(ByVal value As String())
            m_texts = value
        End Set
    End Property

    ''' <summary> 
    ''' Gets or sets the font of the text displayed by the control. 
    ''' </summary> 
    Public Property Font() As Font
        Get
            Return m_font
        End Get
        Set(ByVal value As Font)
            m_font = value
        End Set
    End Property

    ''' <summary> 
    ''' Gets or sets the foreground color of the control used to draw text. 
    ''' </summary> 
    Public Property ForeColor() As Color
        Get
            Return m_foreColor
        End Get
        Set(ByVal value As Color)
            m_foreColor = value
        End Set
    End Property

    ''' <summary> 
    ''' Sets slice edge color mode. If set to <c>PenColor</c> (default), 
    ''' then value set by <c>EdgeColor</c> property is used. 
    ''' </summary> 
    Public WriteOnly Property EdgeColorType() As EdgeColorType
        Set(ByVal value As EdgeColorType)
            m_edgeColorType = value
        End Set
    End Property

    ''' <summary> 
    ''' Sets slice edge line width. If not set, default value is 1. 
    ''' </summary> 
    Public WriteOnly Property EdgeLineWidth() As Single
        Set(ByVal value As Single)
            m_edgeLineWidth = value
        End Set
    End Property

    ''' <summary> 
    ''' Sets slice height, relative to the top ellipse semi-axis. Must be 
    ''' less than or equal to 0.5. 
    ''' </summary> 
    Public WriteOnly Property SliceRelativeHeight() As Single
        Set(ByVal value As Single)
            Debug.Assert(value <= 0.5F)
            m_sliceRelativeHeight = value
        End Set
    End Property

    ''' <summary> 
    ''' Sets the slice displacement relative to the ellipse semi-axis. 
    ''' Must be less than 1. 
    ''' </summary> 
    Public WriteOnly Property SliceRelativeDisplacement() As Single
        Set(ByVal value As Single)
            Debug.Assert(IsDisplacementValid(value))
            m_sliceRelativeDisplacements = New Single() {value}
        End Set
    End Property

    ''' <summary> 
    ''' Sets the slice displacement relative to the ellipse semi-axis. 
    ''' Must be less than 1. 
    ''' </summary> 
    Public WriteOnly Property SliceRelativeDisplacements() As Single()
        Set(ByVal value As Single())
            m_sliceRelativeDisplacements = value
            Debug.Assert(AreDisplacementsValid(value))
        End Set
    End Property

    ''' <summary> 
    ''' Gets or sets the size of the entire pie chart. 
    ''' </summary> 
    Public WriteOnly Property ChartSize() As SizeF
        Set(ByVal value As SizeF)
            m_widthBoundingRect = value.Width
            m_heightBoundingRect = value.Height
        End Set
    End Property

    ''' <summary> 
    ''' Gets or sets the width of the bounding rectangle. 
    ''' </summary> 
    Public Property Width() As Single
        Get
            Return m_widthBoundingRect
        End Get
        Set(ByVal value As Single)
            m_widthBoundingRect = value
        End Set
    End Property

    ''' <summary> 
    ''' Gets or sets the height of the bounding rectangle. 
    ''' </summary> 
    Public Property Height() As Single
        Get
            Return m_heightBoundingRect
        End Get
        Set(ByVal value As Single)
            m_heightBoundingRect = value
        End Set
    End Property

    ''' <summary> 
    ''' Gets the y-coordinate of the bounding rectangle top edge. 
    ''' </summary> 
    Public ReadOnly Property Top() As Single
        Get
            Return m_yBoundingRect
        End Get
    End Property

    ''' <summary> 
    ''' Gets the y-coordinate of the bounding rectangle bottom edge. 
    ''' </summary> 
    Public ReadOnly Property Bottom() As Single
        Get
            Return m_yBoundingRect + m_heightBoundingRect
        End Get
    End Property

    ''' <summary> 
    ''' Gets the x-coordinate of the bounding rectangle left edge. 
    ''' </summary> 
    Public ReadOnly Property Left() As Single
        Get
            Return m_xBoundingRect
        End Get
    End Property

    ''' <summary> 
    ''' Gets the x-coordinate of the bounding rectangle right edge. 
    ''' </summary> 
    Public ReadOnly Property Right() As Single
        Get
            Return m_xBoundingRect + m_widthBoundingRect
        End Get
    End Property

    ''' <summary> 
    ''' Gets or sets the x-coordinate of the upper-left corner of the 
    ''' bounding rectangle. 
    ''' </summary> 
    Public Property X() As Single
        Get
            Return m_xBoundingRect
        End Get
        Set(ByVal value As Single)
            m_xBoundingRect = value
        End Set
    End Property

    ''' <summary> 
    ''' Gets or sets the y-coordinate of the upper-left corner of the 
    ''' bounding rectangle. 
    ''' </summary> 
    Public Property Y() As Single
        Get
            Return m_yBoundingRect
        End Get
        Set(ByVal value As Single)
            m_yBoundingRect = value
        End Set
    End Property

    ''' <summary> 
    ''' Sets the shadowing style used. 
    ''' </summary> 
    Public WriteOnly Property ShadowStyle() As ShadowStyle
        Set(ByVal value As ShadowStyle)
            m_shadowStyle = value
        End Set
    End Property

    ''' <summary> 
    ''' Sets the flag that controls if chart is fit to bounding rectangle 
    ''' exactly. 
    ''' </summary> 
    Public WriteOnly Property FitToBoundingRectangle() As Boolean
        Set(ByVal value As Boolean)
            m_fitToBoundingRectangle = value
        End Set
    End Property

    ''' <summary> 
    ''' Sets the initial angle from which pies are placed. 
    ''' </summary> 
    Public WriteOnly Property InitialAngle() As Single
        Set(ByVal value As Single)
            m_initialAngle = value Mod 360
            If m_initialAngle < 0 Then
                m_initialAngle += 360
            End If
        End Set
    End Property

    ''' <summary> 
    ''' Sets the index of the highlighted pie. 
    ''' </summary> 
    Public WriteOnly Property HighlightedIndex() As Integer
        Set(ByVal value As Integer)
            m_highlightedIndex = value
        End Set
    End Property

    ''' <summary> 
    ''' Draws the chart. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> object used for drawing. 
    ''' </param> 
    Public Sub Draw(ByVal graphics As Graphics)
        If m_values Is Nothing Then
            Exit Sub
        End If
        If m_values.Length < 1 Then
            Exit Sub
        End If

        InitializePieSlices()
        If m_fitToBoundingRectangle Then
            Dim newBoundingRectangle As RectangleF = GetFittingRectangle()
            ReadjustSlices(newBoundingRectangle)
        End If
        DrawBottoms(graphics)
        If m_sliceRelativeHeight > 0.0F Then
            DrawSliceSides(graphics)
        End If
        DrawTops(graphics)
    End Sub

    ''' <summary> 
    ''' Draws strings by individual slices. Position of the text is 
    ''' calculated by overridable <c>GetTextPosition</c> method of the 
    ''' <c>PieSlice</c> type. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> object. 
    ''' </param> 
    Public Overridable Sub PlaceTexts(ByVal graphics As Graphics)
        Debug.Assert(graphics IsNot Nothing)
        Debug.Assert(m_font IsNot Nothing)
        Debug.Assert(m_foreColor <> Color.Empty)
        Dim drawFormat As New StringFormat()
        drawFormat.Alignment = StringAlignment.Center
        drawFormat.LineAlignment = StringAlignment.Center
        Using fontBrush As Brush = New SolidBrush(m_foreColor)
            For Each slice As PieSlice In m_pieSlices
                If slice.Text IsNot Nothing AndAlso slice.Text.Length > 0 Then
                    Dim point As PointF = slice.GetTextPosition()
                    graphics.DrawString(slice.Text, m_font, fontBrush, point, drawFormat)
                End If
            Next
        End Using
    End Sub

    ''' <summary> 
    ''' Searches the chart to find the index of the pie slice which 
    ''' contains point given. Search order goes in the direction opposite 
    ''' to drawing order. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> point for which pie slice is searched for. 
    ''' </param> 
    ''' <returns> 
    ''' Index of the corresponding pie slice, or -1 if none is found. 
    ''' </returns> 
    Public Function FindPieSliceUnderPoint(ByVal point As PointF) As Integer
        If m_pieSlices Is Nothing Then Return 0

        For i As Integer = 0 To m_pieSlices.Length - 1
            ' first check tops 
            Dim slice As PieSlice = DirectCast(m_pieSlices(i), PieSlice)
            If slice.PieSliceContainsPoint(point) Then
                Return CInt(m_pieSlicesMapping(i))
            End If
        Next
        ' split the backmost (at 270 degrees) pie slice 
        Dim pieSlicesList As New ArrayList(m_pieSlices)
        Dim splitSlices As PieSlice() = m_pieSlices(0).Split(270.0F)
        If splitSlices.Length > 1 Then
            pieSlicesList(0) = splitSlices(1)
            If splitSlices(0).SweepAngle > 0.0F Then
                pieSlicesList.Add(splitSlices(0))
            End If
        End If
        Dim pieSlices As PieSlice() = DirectCast(pieSlicesList.ToArray(GetType(PieSlice)), PieSlice())
        Dim indexFound As Integer = -1
        ' if not found yet, then check for periferies 
        Dim incrementIndex As Integer = 0
        Dim decrementIndex As Integer = pieSlices.Length - 1
        While incrementIndex <= decrementIndex
            Dim sliceLeft As PieSlice = pieSlices(decrementIndex)
            Dim angle1 As Single = 270 - sliceLeft.StartAngle
            Dim sliceRight As PieSlice = pieSlices(incrementIndex)
            Dim angle2 As Single = (sliceRight.EndAngle + 90) Mod 360
            Debug.Assert(angle2 >= 0)
            If angle2 < angle1 Then
                If sliceRight.PeripheryContainsPoint(point) Then
                    indexFound = incrementIndex
                End If
                incrementIndex += 1
            Else
                If sliceLeft.PeripheryContainsPoint(point) Then
                    indexFound = decrementIndex
                End If
                decrementIndex -= 1
            End If
        End While
        ' check for start/stop sides, starting from the foremost 
        If indexFound < 0 Then
            Dim foremostPieIndex As Integer = GetForemostPieSlice(pieSlices)
            ' check for start sides from the foremost slice to the left 
            ' side 
            Dim i As Integer = foremostPieIndex
            While i < pieSlices.Length
                Dim sliceLeft As PieSlice = DirectCast(pieSlices(i), PieSlice)
                If sliceLeft.StartSideContainsPoint(point) Then
                    indexFound = i
                    Exit While
                End If
                i += 1
            End While
            ' if not found yet, check end sides from the foremost to the right 
            ' side 
            If indexFound < 0 Then
                i = foremostPieIndex
                While i >= 0
                    Dim sliceLeft As PieSlice = DirectCast(pieSlices(i), PieSlice)
                    If sliceLeft.EndSideContainsPoint(point) Then
                        indexFound = i
                        Exit While
                    End If
                    i -= 1
                End While
            End If
        End If
        ' finally search for bottom sides 
        If indexFound < 0 Then
            For i As Integer = 0 To m_pieSlices.Length - 1
                Dim slice As PieSlice = DirectCast(m_pieSlices(i), PieSlice)
                If slice.BottomSurfaceSectionContainsPoint(point) Then
                    Return CInt(m_pieSlicesMapping(i))
                End If
            Next
        End If
        If indexFound > -1 Then
            indexFound = indexFound Mod (m_pieSlicesMapping.Count)
            Return CInt(m_pieSlicesMapping(indexFound))
        End If
        Return -1
    End Function

    ''' <summary> 
    ''' Return the index of the foremost pie slice i.e. the one crossing 
    ''' 90 degrees boundary. 
    ''' </summary> 
    ''' <param name="pieSlices"> 
    ''' Array of <c>PieSlice</c> objects to examine. 
    ''' </param> 
    ''' <returns> 
    ''' Index of the foremost pie slice. 
    ''' </returns> 
    Private Function GetForemostPieSlice(ByVal pieSlices As PieSlice()) As Integer
        Debug.Assert(pieSlices IsNot Nothing AndAlso pieSlices.Length > 0)
        For i As Integer = 0 To pieSlices.Length - 1
            Dim pieSlice As PieSlice = pieSlices(i)
            If ((pieSlice.StartAngle <= 90) AndAlso ((pieSlice.StartAngle + pieSlice.SweepAngle) >= 90)) OrElse ((pieSlice.StartAngle + pieSlice.SweepAngle > 360) AndAlso ((pieSlice.StartAngle) <= 450) AndAlso (pieSlice.StartAngle + pieSlice.SweepAngle) >= 450) Then
                Return i
            End If
        Next
        Debug.Assert(False, "Foremost pie slice not found")
        Return -1
    End Function

    ''' <summary> 
    ''' Finds the smallest rectangle int which chart fits entirely. 
    ''' </summary> 
    ''' <returns> 
    ''' <c>RectangleF</c> into which all member slices fit. 
    ''' </returns> 
    Protected Function GetFittingRectangle() As RectangleF
        Dim boundingRectangle As RectangleF = m_pieSlices(0).GetFittingRectangle()
        For i As Integer = 1 To m_pieSlices.Length - 1
            boundingRectangle = RectangleF.Union(boundingRectangle, m_pieSlices(i).GetFittingRectangle())
        Next
        Return boundingRectangle
    End Function

    ''' <summary> 
    ''' Readjusts each slice for new bounding rectangle. 
    ''' </summary> 
    ''' <param name="newBoundingRectangle"> 
    ''' <c>RectangleF</c> representing new boundary. 
    ''' </param> 
    Protected Sub ReadjustSlices(ByVal newBoundingRectangle As RectangleF)
        Dim xResizeFactor As Single = m_widthBoundingRect / newBoundingRectangle.Width
        Dim yResizeFactor As Single = m_heightBoundingRect / newBoundingRectangle.Height
        Dim xOffset As Single = newBoundingRectangle.X - m_xBoundingRect
        Dim yOffset As Single = newBoundingRectangle.Y - m_yBoundingRect
        For Each slice As PieSlice In m_pieSlices
            Dim x As Single = slice.BoundingRectangle.X - xOffset
            Dim y As Single = slice.BoundingRectangle.Y - yOffset
            Dim width As Single = slice.BoundingRectangle.Width * xResizeFactor
            Dim height As Single = slice.BoundingRectangle.Height * yResizeFactor
            Dim sliceHeight As Single = slice.SliceHeight * yResizeFactor
            slice.Readjust(x, y, width, height, sliceHeight)
        Next
    End Sub

    ''' <summary> 
    ''' Finds the largest displacement. 
    ''' </summary> 
    Protected ReadOnly Property LargestDisplacement() As Single
        Get
            Dim value As Single = 0.0F
            Dim i As Integer = 0
            While i < m_sliceRelativeDisplacements.Length AndAlso i < m_values.Length
                If m_sliceRelativeDisplacements(i) > value Then
                    value = m_sliceRelativeDisplacements(i)
                End If
                i += 1
            End While
            Return value
        End Get
    End Property

    ''' <summary> 
    ''' Gets the top ellipse size. 
    ''' </summary> 
    Protected ReadOnly Property TopEllipseSize() As SizeF
        Get
            Dim factor As Single = 1 + LargestDisplacement
            Dim widthTopEllipse As Single = m_widthBoundingRect / factor
            Dim heightTopEllipse As Single = m_heightBoundingRect / factor * (1 - m_sliceRelativeHeight)
            Return New SizeF(widthTopEllipse, heightTopEllipse)
        End Get
    End Property

    ''' <summary> 
    ''' Gets the ellipse defined by largest displacement. 
    ''' </summary> 
    Protected ReadOnly Property LargestDisplacementEllipseSize() As SizeF
        Get
            Dim factor As Single = LargestDisplacement
            Dim widthDisplacementEllipse As Single = TopEllipseSize.Width * factor
            Dim heightDisplacementEllipse As Single = TopEllipseSize.Height * factor
            Return New SizeF(widthDisplacementEllipse, heightDisplacementEllipse)
        End Get
    End Property

    ''' <summary> 
    ''' Calculates the pie height. 
    ''' </summary> 
    Protected ReadOnly Property PieHeight() As Single
        Get
            Return m_heightBoundingRect / (1 + LargestDisplacement) * m_sliceRelativeHeight
        End Get
    End Property

    ''' <summary> 
    ''' Initializes pies. 
    ''' </summary> 
    ''' Creates a list of pies, starting with the pie that is crossing the 
    ''' 270 degrees boundary, i.e. "backmost" pie that always has to be 
    ''' drawn first to ensure correct surface overlapping. 
    Protected Overridable Sub InitializePieSlices()
        ' calculates the sum of values required to evaluate sweep angles 
        ' for individual pies 
        Dim sum As Double = 0
        For Each itemValue As Decimal In m_values
            sum += CDbl(itemValue)
        Next
        ' some values and indices that will be used in the loop 
        Dim topEllipeSize As SizeF = TopEllipseSize
        Dim largestDisplacementEllipseSize As SizeF = largestDisplacementEllipseSize
        Dim maxDisplacementIndex As Integer = m_sliceRelativeDisplacements.Length - 1
        Dim largestDisplacement As Single = largestDisplacement
        Dim listPieSlices As New ArrayList()
        m_pieSlicesMapping.Clear()
        Dim colorIndex As Integer = 0
        Dim backPieIndex As Integer = -1
        Dim displacementIndex As Integer = 0
        Dim startAngle As Double = CDbl(m_initialAngle)

        For i As Integer = 0 To m_values.Length - 1
            Dim itemValue As Decimal = m_values(i)
            Dim sweepAngle As Double = CDbl(itemValue) / sum * 360
            ' displacement from the center of the ellipse 
            Dim xDisplacement As Single = m_sliceRelativeDisplacements(displacementIndex)
            Dim yDisplacement As Single = m_sliceRelativeDisplacements(displacementIndex)
            If xDisplacement > 0.0F Then
                'Change
                If largestDisplacement = 0 Then
                    largestDisplacement = 0.01
                End If
                Debug.Assert(largestDisplacement > 0.0F)
                Dim pieDisplacement As SizeF = GetSliceDisplacement(CSng((startAngle + sweepAngle / 2)), m_sliceRelativeDisplacements(displacementIndex))
                xDisplacement = pieDisplacement.Width
                yDisplacement = pieDisplacement.Height
            End If
            Dim slice As PieSlice = Nothing
            If i = m_highlightedIndex Then
                slice = CreatePieSliceHighlighted(m_xBoundingRect + largestDisplacementEllipseSize.Width / 2 + xDisplacement, m_yBoundingRect + largestDisplacementEllipseSize.Height / 2 + yDisplacement, topEllipeSize.Width, topEllipeSize.Height, PieHeight, CSng((startAngle Mod 360)), _
                CSng((sweepAngle)), m_sliceColors(colorIndex), m_shadowStyle, m_edgeColorType, m_edgeLineWidth)
            Else
                slice = CreatePieSlice(m_xBoundingRect + largestDisplacementEllipseSize.Width / 2 + xDisplacement, m_yBoundingRect + largestDisplacementEllipseSize.Height / 2 + yDisplacement, topEllipeSize.Width, topEllipeSize.Height, PieHeight, CSng((startAngle Mod 360)), _
                CSng((sweepAngle)), m_sliceColors(colorIndex), m_shadowStyle, m_edgeColorType, m_edgeLineWidth)
            End If
            slice.Text = m_texts(i)
            ' the backmost pie is inserted to the front of the list for correct drawing 
            If backPieIndex > -1 OrElse ((startAngle <= 270) AndAlso (startAngle + sweepAngle > 270)) OrElse ((startAngle >= 270) AndAlso (startAngle + sweepAngle > 630)) Then
                backPieIndex += 1
                listPieSlices.Insert(backPieIndex, slice)
                m_pieSlicesMapping.Insert(backPieIndex, i)
            Else
                listPieSlices.Add(slice)
                m_pieSlicesMapping.Add(i)
            End If
            ' increment displacementIndex only if there are more displacements available 
            If displacementIndex < maxDisplacementIndex Then
                displacementIndex += 1
            End If
            colorIndex += 1
            ' if all colors have been exhausted, reset color index 
            If colorIndex >= m_sliceColors.Length Then
                colorIndex = 0
            End If
            ' prepare for the next pie slice 
            startAngle += sweepAngle
            If startAngle > 360 Then
                startAngle -= 360
            End If
        Next
        m_pieSlices = DirectCast(listPieSlices.ToArray(GetType(PieSlice)), PieSlice())
    End Sub

    ''' <summary> 
    ''' Creates a <c>PieSlice</c> object. 
    ''' </summary> 
    ''' <param name="boundingRectLeft"> 
    ''' x-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the slice. 
    ''' </param> 
    ''' <param name="boundingRectTop"> 
    ''' y-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the slice. 
    ''' </param> 
    ''' <param name="boundingRectWidth"> 
    ''' Width of the rectangle that is used to draw the top surface of 
    ''' the slice. 
    ''' </param> 
    ''' <param name="boundingRectHeight"> 
    ''' Height of the rectangle that is used to draw the top surface of 
    ''' the slice. 
    ''' </param> 
    ''' <param name="sliceHeight"> 
    ''' Slice height. 
    ''' </param> 
    ''' <param name="startAngle"> 
    ''' Starting angle. 
    ''' </param> 
    ''' <param name="sweepAngle"> 
    ''' Sweep angle. 
    ''' </param> 
    ''' <param name="color"> 
    ''' Color used for slice rendering. 
    ''' </param> 
    ''' <param name="shadowStyle"> 
    ''' Shadow style used for slice rendering. 
    ''' </param> 
    ''' <param name="edgeColorType"> 
    ''' Edge lines color type. 
    ''' </param> 
    ''' <param name="edgeLineWidth"> 
    ''' Edge lines width. 
    ''' </param> 
    ''' <returns> 
    ''' <c>PieSlice</c> object with given values. 
    ''' </returns> 
    Protected Overridable Function CreatePieSlice(ByVal boundingRectLeft As Single, ByVal boundingRectTop As Single, ByVal boundingRectWidth As Single, ByVal boundingRectHeight As Single, ByVal sliceHeight As Single, ByVal startAngle As Single, _
    ByVal sweepAngle As Single, ByVal color As Color, ByVal shadowStyle As ShadowStyle, ByVal edgeColorType As EdgeColorType, ByVal edgeLineWidth As Single) As PieSlice
        Return New PieSlice(boundingRectLeft, boundingRectTop, boundingRectWidth, boundingRectHeight, sliceHeight, CSng((startAngle Mod 360)), _
        sweepAngle, color, shadowStyle, edgeColorType, edgeLineWidth)
    End Function

    ''' <summary> 
    ''' Creates highlighted <c>PieSlice</c> object. 
    ''' </summary> 
    ''' <param name="boundingRectLeft"> 
    ''' x-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the slice. 
    ''' </param> 
    ''' <param name="boundingRectTop"> 
    ''' y-coordinate of the upper-left corner of the rectangle that is 
    ''' used to draw the top surface of the slice. 
    ''' </param> 
    ''' <param name="boundingRectWidth"> 
    ''' Width of the rectangle that is used to draw the top surface of 
    ''' the slice. 
    ''' </param> 
    ''' <param name="boundingRectHeight"> 
    ''' Height of the rectangle that is used to draw the top surface of 
    ''' the slice. 
    ''' </param> 
    ''' <param name="sliceHeight"> 
    ''' Slice height. 
    ''' </param> 
    ''' <param name="startAngle"> 
    ''' Starting angle. 
    ''' </param> 
    ''' <param name="sweepAngle"> 
    ''' Sweep angle. 
    ''' </param> 
    ''' <param name="color"> 
    ''' Color used for slice rendering. 
    ''' </param> 
    ''' <param name="shadowStyle"> 
    ''' Shadow style used for slice rendering. 
    ''' </param> 
    ''' <param name="edgeColorType"> 
    ''' Edge lines color type. 
    ''' </param> 
    ''' <param name="edgeLineWidth"> 
    ''' Edge lines width. 
    ''' </param> 
    ''' <returns> 
    ''' <c>PieSlice</c> object with given values. 
    ''' </returns> 
    Protected Overridable Function CreatePieSliceHighlighted(ByVal boundingRectLeft As Single, ByVal boundingRectTop As Single, ByVal boundingRectWidth As Single, ByVal boundingRectHeight As Single, ByVal sliceHeight As Single, ByVal startAngle As Single, _
    ByVal sweepAngle As Single, ByVal color As Color, ByVal shadowStyle As ShadowStyle, ByVal edgeColorType As EdgeColorType, ByVal edgeLineWidth As Single) As PieSlice
        Dim highLightedColor As Color = ColorUtil.CreateColorWithCorrectedLightness(color, ColorUtil.BrightnessEnhancementFactor1)
        Return New PieSlice(boundingRectLeft, boundingRectTop, boundingRectWidth, boundingRectHeight, sliceHeight, CSng((startAngle Mod 360)), _
        sweepAngle, highLightedColor, shadowStyle, edgeColorType, edgeLineWidth)
    End Function

    ''' <summary> 
    ''' Calculates the displacement for given angle. 
    ''' </summary> 
    ''' <param name="angle"> 
    ''' Angle (in degrees). 
    ''' </param> 
    ''' <param name="displacementFactor"> 
    ''' Displacement factor. 
    ''' </param> 
    ''' <returns> 
    ''' <c>SizeF</c> representing displacement. 
    ''' </returns> 
    Protected Function GetSliceDisplacement(ByVal angle As Single, ByVal displacementFactor As Single) As SizeF
        Debug.Assert(displacementFactor > 0.0F AndAlso displacementFactor <= 1.0F)
        If displacementFactor = 0.0F Then
            Return SizeF.Empty
        End If
        Dim xDisplacement As Single = CSng((TopEllipseSize.Width * displacementFactor / 2 * Math.Cos(angle * Math.PI / 180)))
        Dim yDisplacement As Single = CSng((TopEllipseSize.Height * displacementFactor / 2 * Math.Sin(angle * Math.PI / 180)))
        Return New SizeF(xDisplacement, yDisplacement)
    End Function

    ''' <summary> 
    ''' Draws outer peripheries of all slices. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used for drawing. 
    ''' </param> 
    Protected Sub DrawSliceSides(ByVal graphics As Graphics)
        Dim pieSlicesList As New ArrayList(m_pieSlices)
        Dim ps As PieSlice = Nothing
        ' if the first pie slice (crossing 270 i.e. back) is crossing 90 
        ' (front) axis too, we have to split it 
        If (m_pieSlices(0).StartAngle > 90) AndAlso (m_pieSlices(0).StartAngle <= 270) AndAlso (m_pieSlices(0).StartAngle + m_pieSlices(0).SweepAngle > 450) Then
            ps = DirectCast(pieSlicesList(0), PieSlice)
            ' this one is split at 0 deg to avoid line of split to be 
            ' visible on the periphery 
            Dim splitSlices As PieSlice() = ps.Split(0.0F)
            pieSlicesList(0) = splitSlices(0)
            If splitSlices(1).SweepAngle > 0.0F Then
                pieSlicesList.Insert(1, splitSlices(1))
            End If
        ElseIf ((m_pieSlices(0).StartAngle > 270) AndAlso (m_pieSlices(0).StartAngle + m_pieSlices(0).SweepAngle > 450)) OrElse ((m_pieSlices(0).StartAngle < 90) AndAlso (m_pieSlices(0).StartAngle + m_pieSlices(0).SweepAngle > 270)) Then
            ps = DirectCast(pieSlicesList(0), PieSlice)
            ' this one is split at 180 deg to avoid line of split to be 
            ' visible on the periphery 
            Dim splitSlices As PieSlice() = ps.Split(180.0F)
            pieSlicesList(0) = splitSlices(1)
            If splitSlices(1).SweepAngle > 0.0F Then
                pieSlicesList.Add(splitSlices(0))
            End If
        End If
        ' first draw the backmost pie slice 
        ps = DirectCast(pieSlicesList(0), PieSlice)
        ps.DrawSides(graphics)
        ' draw pie slices from the backmost to forward 
        Dim incrementIndex As Integer = 1
        Dim decrementIndex As Integer = pieSlicesList.Count - 1
        While incrementIndex < decrementIndex
            Dim sliceLeft As PieSlice = DirectCast(pieSlicesList(decrementIndex), PieSlice)
            Dim angle1 As Single = sliceLeft.StartAngle - 90
            If angle1 > 180 OrElse angle1 < 0 Then
                angle1 = 0
            End If
            Dim sliceRight As PieSlice = DirectCast(pieSlicesList(incrementIndex), PieSlice)
            Dim angle2 As Single = (450 - sliceRight.EndAngle) Mod 360
            If angle2 > 180 OrElse angle2 < 0 Then
                angle2 = 0
            End If
            Debug.Assert(angle1 >= 0)
            Debug.Assert(angle2 >= 0)
            If angle2 >= angle1 Then
                sliceRight.DrawSides(graphics)
                incrementIndex += 1
            ElseIf angle2 < angle1 Then
                sliceLeft.DrawSides(graphics)
                decrementIndex -= 1
            End If
        End While
        ps = DirectCast(pieSlicesList(decrementIndex), PieSlice)
        ps.DrawSides(graphics)
    End Sub

    ''' <summary> 
    ''' Draws bottom sides of all pie slices. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used for drawing. 
    ''' </param> 
    Protected Sub DrawBottoms(ByVal graphics As Graphics)
        For Each slice As PieSlice In m_pieSlices
            slice.DrawBottom(graphics)
        Next
    End Sub

    ''' <summary> 
    ''' Draws top sides of all pie slices. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used for drawing. 
    ''' </param> 
    Protected Sub DrawTops(ByVal graphics As Graphics)
        For Each slice As PieSlice In m_pieSlices
            slice.DrawTop(graphics)
        Next
    End Sub

    ''' <summary> 
    ''' Helper function used in assertions. Checks the validity of 
    ''' slice displacements. 
    ''' </summary> 
    ''' <param name="displacements"> 
    ''' Array of displacements to check. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if all displacements have a valid value; otherwise 
    ''' <c>false</c>. 
    ''' </returns> 
    Private Function AreDisplacementsValid(ByVal displacements As Single()) As Boolean
        For Each value As Single In displacements
            If Not IsDisplacementValid(value) Then
                Return False
            End If
        Next
        Return True
    End Function

    ''' <summary> 
    ''' Helper function used in assertions. Checks the validity of 
    ''' a slice displacement. 
    ''' </summary> 
    ''' <param name="value"> 
    ''' Displacement value to check. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if displacement has a valid value; otherwise 
    ''' <c>false</c>. 
    ''' </returns> 
    Private Function IsDisplacementValid(ByVal value As Single) As Boolean
        Return (value >= 0.0F AndAlso value <= 1.0F)
    End Function

    ''' <summary> 
    ''' x-coordinate of the top left corner of the bounding rectangle. 
    ''' </summary> 
    Protected m_xBoundingRect As Single
    ''' <summary> 
    ''' y-coordinate of the top left corner of the bounding rectangle. 
    ''' </summary> 
    Protected m_yBoundingRect As Single
    ''' <summary> 
    ''' Width of the bounding rectangle. 
    ''' </summary> 
    Protected m_widthBoundingRect As Single
    ''' <summary> 
    ''' Height of the bounding rectangle. 
    ''' </summary> 
    Protected m_heightBoundingRect As Single
    ''' <summary> 
    ''' Slice relative height. 
    ''' </summary> 
    Protected m_sliceRelativeHeight As Single = 0.0F
    ''' <summary> 
    ''' Initial angle from which chart is drawn. 
    ''' </summary> 
    Protected m_initialAngle As Single = 0.0F
    ''' <summary> 
    ''' Array of ordered pie slices constituting the chart, starting from 
    ''' 270 degrees axis. 
    ''' </summary> 
    Protected m_pieSlices As PieSlice()
    ''' <summary> 
    ''' Collection of reordered pie slices mapped to original order. 
    ''' </summary> 
    Protected m_pieSlicesMapping As New ArrayList()
    ''' <summary> 
    ''' Array of values to be presented by the chart. 
    ''' </summary> 
    Protected m_values As Decimal() = New Decimal() {}
    ''' <summary> 
    ''' Array of colors used for rendering. 
    ''' </summary> 
    Protected m_sliceColors As Color() = New Color() {Color.Red, Color.Green, Color.Blue, Color.Yellow, Color.Purple, Color.Olive, _
    Color.Navy, Color.Aqua, Color.Lime, Color.Maroon, Color.Teal, Color.Fuchsia}
    ''' <summary> 
    ''' Array of description texts. 
    ''' </summary> 
    Protected m_texts As String()
    ''' <summary> 
    ''' Font used to display texts. 
    ''' </summary> 
    Protected m_font As Font = System.Windows.Forms.Control.DefaultFont
    ''' <summary> 
    ''' Fore color used to display texts. 
    ''' </summary> 
    Protected m_foreColor As Color = SystemColors.WindowText
    ''' <summary> 
    ''' Array of relative displacements from the common center. 
    ''' </summary> 
    Protected m_sliceRelativeDisplacements As Single() = New Single() {0.0F}
    ''' <summary> 
    ''' Edge color type used for rendering. 
    ''' </summary> 
    Protected m_edgeColorType As EdgeColorType = eZee.Common.EdgeColorType.SurfaceColor
    ''' <summary> 
    ''' Edge line width. 
    ''' </summary> 
    Protected m_edgeLineWidth As Single = 1.0F
    ''' <summary> 
    ''' Shadow style. 
    ''' </summary> 
    Protected m_shadowStyle As ShadowStyle = eZee.Common.ShadowStyle.NoShadow
    ''' <summary> 
    ''' Should the chart fit the bounding rectangle exactly. 
    ''' </summary> 
    Protected m_fitToBoundingRectangle As Boolean = True
    ''' <summary> 
    ''' Index of the currently highlighted pie slice. 
    ''' </summary> 
    Protected m_highlightedIndex As Integer = -1
    ''' <summary> 
    ''' Flag indicating if object has been disposed. 
    ''' </summary> 
    Private m_disposed As Boolean = False
End Class
