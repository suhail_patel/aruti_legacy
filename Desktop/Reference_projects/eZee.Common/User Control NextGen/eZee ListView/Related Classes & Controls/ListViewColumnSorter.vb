Imports System.ComponentModel
Imports System.Windows.Forms

Public Class ListViewColumnSorter
    Implements IComparer

    Public Enum ComparerType
        StringComparer
        DateComparer
        MonthComparer
        NumericComparer
        CaseInsensitiveComparer
    End Enum

    Private _ColumnIndex As Integer
    Private _Order As SortOrder
    Private _ComparerType As ComparerType = ComparerType.StringComparer

    Public Property ColumnIndex() As Integer
        Get
            Return _ColumnIndex
        End Get
        Set(ByVal value As Integer)
            If _ColumnIndex <> value Then
                'Set the sort order to ascending by default.
                _Order = SortOrder.Ascending
            Else
                'Determine what the last sort order was and change it.
                If _Order = SortOrder.Ascending Then
                    _Order = SortOrder.Descending
                Else
                    _Order = SortOrder.Ascending
                End If
            End If
            _ColumnIndex = value
        End Set
    End Property

    Public Property Order() As SortOrder
        Get
            Return _Order
        End Get
        Set(ByVal value As SortOrder)
            _Order = value
        End Set
    End Property

    Public Property DataType() As ComparerType
        Get
            Return _ComparerType
        End Get
        Set(ByVal value As ComparerType)
            _ComparerType = value
        End Set
    End Property

    Public Sub New()
        _ColumnIndex = 0
        _Order = SortOrder.Ascending
        _ComparerType = ComparerType.StringComparer
    End Sub

    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
        Dim returnVal As Integer = -1

        Dim x1 As String = (CType(x, ListViewItem)).SubItems(_ColumnIndex).Text
        Dim y1 As String = (CType(y, ListViewItem)).SubItems(_ColumnIndex).Text

        If HasEmptyValue(x, y) Then
            returnVal = CompareEmpty(x, y)
        Else
            Try
                Select Case Me._ComparerType
                    Case ComparerType.StringComparer
                        returnVal = [String].Compare(x1, y1)
                    Case ComparerType.DateComparer
                        returnVal = DateTime.Compare(x1, y1)
                    Case ComparerType.MonthComparer
                        Dim v1 As Integer = 0
                        Dim v2 As Integer = 0
                        For iCnt As Integer = 1 To 12
                            If MonthName(iCnt) = x1 Then v1 = iCnt
                            If MonthName(iCnt) = y1 Then v2 = iCnt
                        Next
                        returnVal = Decimal.Compare(v1, v2)
                    Case ComparerType.NumericComparer
                        returnVal = Decimal.Compare(x1, y1)
                    Case ComparerType.CaseInsensitiveComparer
                        returnVal = (New CaseInsensitiveComparer).Compare(x1, y1)
                    Case Else
                        returnVal = [String].Compare(x1, y1)
                End Select
            Catch
                ' Compare the two items as a string.
                returnVal = [String].Compare(x1, y1)
            End Try
        End If

        ' Determine whether the sort order is descending.
        If Me._Order = SortOrder.Descending Then
            ' Invert the value returned by String.Compare.
            returnVal *= -1
        End If

        Return returnVal
    End Function

    Protected Function HasEmptyValue(ByVal x As Object, ByVal y As Object) As Boolean
        If x Is Nothing OrElse x.ToString().Equals("") OrElse y Is Nothing OrElse y.ToString().Equals("") Then
            Return True
        End If

        Return False
    End Function

    Protected Function CompareEmpty(ByVal x As Object, ByVal y As Object) As Integer
        If (x Is Nothing OrElse x.ToString().Equals("")) AndAlso (y Is Nothing OrElse y.ToString().Equals("")) Then
            ' Both are null or empty
            Return 0
        ElseIf x Is Nothing OrElse x.ToString().Equals("") Then
            Return String.Compare("", CStr(y))
        ElseIf y Is Nothing OrElse y.ToString().Equals("") Then
            Return String.Compare(CStr(x), "")
        End If
        Return 0
    End Function
End Class