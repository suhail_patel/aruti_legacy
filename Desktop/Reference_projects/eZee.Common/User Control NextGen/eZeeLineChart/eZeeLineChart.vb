Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Collections

''' <summary> 
''' Summary description for Line2D. 
''' </summary> 
Public Class eZeeLineChart
    Inherits System.Windows.Forms.Panel

    Private m_Width As Integer = 700
    'Width of the rectangle container for graph. 
    Private m_Height As Integer = 400
    'Height of the rectangle container for graph. 
    Private m_XAxis1 As ArrayList
    'X-axis for the graph. 
    Private m_YAxis1 As ArrayList
    'Y-Axis for the graph. 
    Private m_XAxis2 As ArrayList
    'X-axis for the graph. 
    Private m_YAxis2 As ArrayList
    'Y-Axis for the graph. 
    Private m_ColorSeries1 As Color = Color.Red
    Private m_ColorSeries2 As Color = Color.Blue
    'Color of the line graph. 
    Private m_XSlice As Single = 1
    'Slice for X Axis. 
    Private m_YSlice As Single = 1
    'Slice for Y Axis. 
    Private objGraphics As Graphics
    Private objBitmap As Bitmap
    Private m_XAxisText As String = "X-Axis"
    Private m_YAxisText As String = "Y-Axis"
    Private m_Title As String = "Line Graph"
    Private m_TitleBackColor As Color = Color.Cyan
    Private m_TitleForeColor As Color = Color.Green



    ''Sets or Gets the Width for the rectangle container of graph. 
    'Public Property Width() As Integer
    '    Get
    '        Return m_Width
    '    End Get
    '    Set(ByVal value As Integer)
    '        If value > 200 Then
    '            m_Width = value
    '            'else 
    '            'Width should be greater than 200. 
    '        End If
    '    End Set
    'End Property

    ''Sets or Gets the Height for the rectangle container of graph. 
    'Public Property Height() As Integer
    '    Get
    '        Return m_Height
    '    End Get
    '    Set(ByVal value As Integer)
    '        If value > 200 Then
    '            m_Height = value
    '            'else 
    '            'Height should be greater than 200. 
    '        End If
    '    End Set
    'End Property

    'Sets or Gets the X-Axis pixels for the graph. 
    Public Property XAxis_Series1() As ArrayList
        Get
            Return m_XAxis1
        End Get
        Set(ByVal value As ArrayList)
            m_XAxis1 = value
        End Set
    End Property

    'Sets or Gets the Y-Axis pixels for the graph. 
    Public Property YAxis_Series1() As ArrayList
        Get
            Return m_YAxis1
        End Get
        Set(ByVal value As ArrayList)
            m_YAxis1 = value
        End Set
    End Property

    'Sets or Gets the X-Axis pixels for the graph. 
    Public Property XAxis_Series2() As ArrayList
        Get
            Return m_XAxis2
        End Get
        Set(ByVal value As ArrayList)
            m_XAxis2 = value
        End Set
    End Property

    'Sets or Gets the Y-Axis pixels for the graph. 
    Public Property YAxis_Series2() As ArrayList
        Get
            Return m_YAxis2
        End Get
        Set(ByVal value As ArrayList)
            m_YAxis2 = value
        End Set
    End Property

    'Sets or Gets the Color of the line Graph. 
    Public Property GraphColor() As Color
        Get
            Return m_ColorSeries1
        End Get
        Set(ByVal value As Color)
            m_ColorSeries1 = value
        End Set
    End Property


    'Sets or Gets the X Axis Slice. 
    Public Property XSlice() As Single
        Get
            Return m_XSlice
        End Get
        Set(ByVal value As Single)
            m_XSlice = value
        End Set
    End Property

    'Sets or Gets the Y Axis Slice. 
    Public Property YSlice() As Single
        Get
            Return m_YSlice
        End Get
        Set(ByVal value As Single)
            m_YSlice = value
        End Set
    End Property

    'Sets or Gets the X-Axis Test. 
    Public Property XAxisText() As String
        Get
            Return m_XAxisText
        End Get
        Set(ByVal value As String)
            m_XAxisText = value
        End Set
    End Property

    'Sets or Gets the Y-Axis Test. 
    Public Property YAxisText() As String
        Get
            Return m_YAxisText
        End Get
        Set(ByVal value As String)
            m_YAxisText = value
        End Set
    End Property

    'Sets or Gets the title for the Graph. 
    Public Property Title() As String
        Get
            Return m_Title
        End Get
        Set(ByVal value As String)
            m_Title = value
        End Set
    End Property

    'Sets or Gets the title Backcolor. 
    Public Property TitleBackColor() As Color
        Get
            Return m_TitleBackColor
        End Get
        Set(ByVal value As Color)
            m_TitleBackColor = value
        End Set
    End Property

    'Sets or Gets the Title ForeColor. 
    Public Property TitleForeColor() As Color
        Get
            Return m_TitleForeColor
        End Get
        Set(ByVal value As Color)
            m_TitleForeColor = value
        End Set
    End Property

    'Sets or Gets the Title Series Color. 
    Public Property Series1Color() As Color
        Get
            Return m_ColorSeries1
        End Get
        Set(ByVal value As Color)
            m_ColorSeries1 = value
        End Set
    End Property

    'Sets or Gets the Title Series Color. 
    Public Property Series2Color() As Color
        Get
            Return m_ColorSeries2
        End Get
        Set(ByVal value As Color)
            m_ColorSeries2 = value
        End Set
    End Property

    'Default constructor. 

    Public Sub New()
    End Sub


    '***************************************************************************************** 
    ' * Method : Public InitializeGraph 
    ' * Purpose : Initialises the Graph.Draws rectangle region and fills the region. 
    ' * Draws X-Axis line and Y-Axis line. 
    ' * Draws Origin (0,0) point.Sets Axis Text and creates Title. 
    ' * **************************************************************************************** 

    Public Sub InitializeGraph()

        'Creating a bitmap image with given height and width. 
        'objBitmap = New Bitmap(Width, Height)

        'Getting the bitmap image into the graphics portion of the screen. 
        'objGraphics = Graphics.FromImage(objBitmap)

        'Filling the rectangle portion of the graphics with custom color. 
        objGraphics.FillRectangle(New SolidBrush(Color.LightGray), 0, 0, Width, Height)

        'Drawing X-Axis line. 
        objGraphics.DrawLine(New Pen(New SolidBrush(Color.Black), 2), 100, Height - 100, Width - 100, Height - 100)

        'Drawing Y-Axis line. 
        objGraphics.DrawLine(New Pen(New SolidBrush(Color.Black), 2), 100, Height - 100, 100, 10)

        'Plotting Origin(0,0). 
        objGraphics.DrawString("0", New Font("Courier New", 10), New SolidBrush(Color.White), 100, Height - 90)

        'Sets Axis text. 
        SetAxisText(objGraphics)

        'Sets the title for the Graph. 
        CreateTitle(objGraphics)

        CreateGraph()
    End Sub


    '***************************************************************************************** 
    ' * Method : Public CreateGraph 
    ' * Purpose : Calls SetPixel function to draw line in the rectangle region. 
    ' * Input : Color of the line Graph. 
    ' * **************************************************************************************** 

    Public Sub CreateGraph()
        'Plotting the pixels. 
        SetPixels(objGraphics)
    End Sub

    '***************************************************************************************** 
    ' * Method : Public Draw2D. 
    ' * Purpose : Creates 2D graph for the given X and Y Axis. 
    ' * Returns : (Bitmap) reference of the graphics portions. 
    ' * **************************************************************************************** 

    Public Function GetGraph() As Bitmap

        'Creating X-Axis slices. 
        SetXAxis(objGraphics, XSlice)

        'Creating Y-Axis slices. 
        SetYAxis(objGraphics, YSlice)


        Return objBitmap
    End Function




    '***************************************************************************************** 
    ' * Method : Public PlotGraph. 
    ' * Purpose : Draws Axis Line. 
    ' * Input : Graphics object,X Axis, Y Axis for both points. 
    ' ***************************************************************************************** 

    Private Sub PlotGraph(ByRef objGraphics As Graphics, ByVal x1 As Single, ByVal y1 As Single, ByVal x2 As Single, ByVal y2 As Single, ByVal GraphColor As Color)
        objGraphics.DrawLine(New Pen(New SolidBrush(GraphColor), 2), x1 + 100, (Height - 100) - y1, x2 + 100, (Height - 100) - y2)
    End Sub


    '***************************************************************************************** 
    ' * Method : Public SetXAxis. 
    ' * Purpose : Draws X-Axis Slices. 
    ' * Input : Graphics object, Slices for the XAxis. 
    ' ***************************************************************************************** 

    Private Sub SetXAxis(ByRef objGraphics As Graphics, ByVal iSlices As Single)
        Dim x1 As Single = 100, y1 As Single = Height - 110, x2 As Single = 100, y2 As Single = Height - 90

        Dim iCount As Integer = 0
        Dim iSliceCount As Integer = 1
        For iIndex As Integer = 0 To Width - 200 Step 10


            If iCount = 5 Then
                objGraphics.DrawLine(New Pen(New SolidBrush(Color.Black)), x1 + iIndex, y1, x2 + iIndex, y2)
                objGraphics.DrawString(Convert.ToString(iSlices * iSliceCount), New Font("verdana", 8), New SolidBrush(Color.White), x1 + iIndex - 10, y2)
                iCount = 0

                iSliceCount += 1
            Else
                objGraphics.DrawLine(New Pen(New SolidBrush(Color.Goldenrod)), x1 + iIndex, y1 + 5, x2 + iIndex, y2 - 5)
            End If

            iCount += 1
        Next

    End Sub


    '***************************************************************************************** 
    ' * Method : Public SetYAxis. 
    ' * Purpose : Draws Y-Axis Slices. 
    ' * Input : Graphics object, Slices for the axis. 
    ' ***************************************************************************************** 

    Private Sub SetYAxis(ByRef objGraphics As Graphics, ByVal iSlices As Single)
        Dim x1 As Integer = 95
        Dim y1 As Integer = Height - 110
        Dim x2 As Integer = 105
        Dim y2 As Integer = Height - 110
        Dim iCount As Integer = 1
        Dim iSliceCount As Integer = 1
        For iIndex As Integer = 0 To Height - 201 Step 10

            If iCount = 5 Then
                objGraphics.DrawLine(New Pen(New SolidBrush(Color.Black)), x1 - 5, y1 - iIndex, x2 + 5, y2 - iIndex)
                objGraphics.DrawString(Convert.ToString(iSlices * iSliceCount), New Font("verdana", 8), New SolidBrush(Color.White), 60, y1 - iIndex)
                iCount = 0

                iSliceCount += 1
            Else
                objGraphics.DrawLine(New Pen(New SolidBrush(Color.Goldenrod)), x1, (y1 - iIndex), x2, (y2 - iIndex))
            End If
            iCount += 1
        Next

    End Sub



    '***************************************************************************************** 
    ' * Method : Public SetPixels. 
    ' * Purpose : Plots pixels. 
    ' * Input : Graphics object. 
    ' ***************************************************************************************** 

    Private Sub SetPixels(ByRef objGraphics As Graphics)

        If XAxis_Series1 Is Nothing Then
            Exit Sub
        End If

        Dim X1 As Single = CDbl(XAxis_Series1(0).ToString())
        Dim Y1 As Single = CDbl(YAxis_Series1(0).ToString())

        Dim iXaxis As Integer = 0, iYaxis As Integer = 0

       

        If XAxis_Series1.Count = YAxis_Series1.Count Then

            While (iXaxis < XAxis_Series1.Count - 1 AndAlso iYaxis < YAxis_Series1.Count - 1)
                PlotGraph(objGraphics, X1, Y1, CDbl(XAxis_Series1(iXaxis + 1).ToString()), CDbl(YAxis_Series1(iYaxis + 1).ToString()), m_ColorSeries1)
                X1 = CDbl(XAxis_Series1(iXaxis + 1).ToString())

                Y1 = CDbl(YAxis_Series1(iYaxis + 1).ToString())
                iXaxis += 1
                iYaxis += 1

            End While
            'X and Y axis length should be same. 
        Else
        End If

        If Not XAxis_Series2 Is Nothing Then
            X1 = CDbl(XAxis_Series2(0).ToString())
            Y1 = CDbl(YAxis_Series2(0).ToString())

            If XAxis_Series2.Count = YAxis_Series2.Count Then
                iXaxis = 0
                iYaxis = 0
                While (iXaxis < XAxis_Series2.Count - 1 AndAlso iYaxis < YAxis_Series2.Count - 1)
                    PlotGraph(objGraphics, X1, Y1, CDbl(XAxis_Series2(iXaxis + 1).ToString()), CDbl(YAxis_Series2(iYaxis + 1).ToString()), m_ColorSeries2)
                    X1 = CDbl(XAxis_Series2(iXaxis + 1).ToString())

                    Y1 = CDbl(YAxis_Series2(iYaxis + 1).ToString())
                    iXaxis += 1
                    iYaxis += 1
                End While
                'X and Y axis length should be same. 
            Else
            End If
        End If
    End Sub


    '***************************************************************************************** 
    ' * Method : Private SetAxisText. 
    ' * Purpose : Sets the Axis text. 
    ' * Input : Graphics object. 
    ' ***************************************************************************************** 

    Private Sub SetAxisText(ByRef objGraphics As Graphics)
        objGraphics.DrawString(XAxisText, New Font("Courier New", 10), New SolidBrush(Color.LimeGreen), Width / 2 - 50, Height - 50)

        Dim X As Integer = 30
        Dim Y As Integer = (Height / 2) - 50
        For iIndex As Integer = 0 To YAxisText.Length - 1
            objGraphics.DrawString(YAxisText(iIndex).ToString(), New Font("Courier New", 10), New SolidBrush(Color.LimeGreen), X, Y)
            Y += 10
        Next

    End Sub


    '***************************************************************************************** 
    ' * Method : Public CreateTitle. 
    ' * Purpose : Creates title for the graph. 
    ' * Input : Graphics object. 
    ' ***************************************************************************************** 

    Private Sub CreateTitle(ByRef objGraphics As Graphics)
        objGraphics.FillRectangle(New SolidBrush(TitleBackColor), Height - 100, 20, Height - 200, 20)
        Dim rect As New Rectangle(Height - 100, 20, Height - 200, 20)
        objGraphics.DrawString(Title, New Font("Verdana", 10), New SolidBrush(TitleForeColor), rect)
    End Sub


    Private Sub eZeeLineChart_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Me.Paint
        objGraphics = e.Graphics
        Call InitializeGraph()
    End Sub
End Class
