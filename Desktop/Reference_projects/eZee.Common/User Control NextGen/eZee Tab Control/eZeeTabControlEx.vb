Imports System.Windows.Forms
Imports System.Drawing

Public Class eZeeTabControlEx
    Inherits TabControl

    Private _HeaderWidth As Integer
    Private _HeaderHeight As Integer

    Private _HeaderAlignment As System.Drawing.ContentAlignment
    Private _HeaderPadding As System.Windows.Forms.Padding
    Private _HeaderFont As Font
    Private _HeaderBackColor As Color
    Private _HeaderBackBrush As SolidBrush
    Private _HeaderBorderColor As Color
    Private _HeaderBackPen As Pen
    Private _HeaderForeColor As Color
    Private _HeaderForeBrush As SolidBrush
    Private _HeaderSelectedBackColor As Color
    Private _HeaderSelectedBackBrush As SolidBrush
    Private _HeaderSelectedForeColor As Color
    Private _HeaderSelectedForeBrush As Brush

    Private _BackColor As Color
    Private _BackBrush As System.Drawing.SolidBrush

#Region " Header Properties "
    Public Overloads Property Alignment() As TabAlignment
        Get
            Return MyBase.Alignment
        End Get
        Set(ByVal value As TabAlignment)
            Dim old As TabAlignment = MyBase.Alignment
            MyBase.Alignment = value

            Select Case old
                Case TabAlignment.Top, TabAlignment.Bottom
                    If value = TabAlignment.Right Or value = TabAlignment.Left Then
                        Me.ItemSize = New Size(Me.ItemSize.Height, Me.ItemSize.Width)
                    End If
                Case TabAlignment.Left, TabAlignment.Right
                    If value = TabAlignment.Top Or value = TabAlignment.Bottom Then
                        Me.ItemSize = New Size(Me.ItemSize.Height, Me.ItemSize.Width)
                    End If
            End Select
        End Set
    End Property

    <System.ComponentModel.DefaultValue(100)> _
    Public Property HeaderWidth() As Integer
        Get
            Return Me._HeaderWidth
        End Get
        Set(ByVal value As Integer)
            Me._HeaderWidth = value
            Me.ItemSize = New Size(Me.ItemSize.Width, value)
        End Set
    End Property

    <System.ComponentModel.DefaultValue(32)> _
    Public Property HeaderHeight() As Integer
        Get
            Return Me._HeaderHeight
        End Get
        Set(ByVal value As Integer)
            Me._HeaderHeight = value
            Me.ItemSize = New Size(value, Me.ItemSize.Height)
        End Set
    End Property

    <System.ComponentModel.DefaultValue(GetType(System.Drawing.ContentAlignment), "ContentAlignment.MiddleLeft")> _
    Public Property HeaderAlignment() As System.Drawing.ContentAlignment
        Get
            Return Me._HeaderAlignment
        End Get
        Set(ByVal value As System.Drawing.ContentAlignment)
            Me._HeaderAlignment = value
            Me.Invalidate()
        End Set
    End Property

    <System.ComponentModel.DefaultValue(GetType(System.Windows.Forms.Padding), "3,3,3,3")> _
    Public Property HeaderPadding() As System.Windows.Forms.Padding
        Get
            Return Me._HeaderPadding
        End Get
        Set(ByVal value As System.Windows.Forms.Padding)
            Me._HeaderPadding = value
            Me.Invalidate()
        End Set
    End Property

    <System.ComponentModel.DefaultValue(GetType(Color), "White")> _
    Public Property HeaderBorderColor() As Color
        Get
            Return Me._HeaderBorderColor
        End Get
        Set(ByVal value As Color)
            If Not value = Me._HeaderBorderColor Then
                Me._HeaderBorderColor = value
                If Me._HeaderBackPen IsNot Nothing Then
                    Me._HeaderBackPen.Dispose()
                    Me._HeaderBackPen = Nothing
                End If
                Me.Invalidate()
            End If
        End Set
    End Property

    <System.ComponentModel.DefaultValue(GetType(Color), "LightGray")> _
    Public Property HeaderBackColor() As Color
        Get
            Return Me._HeaderBackColor
        End Get
        Set(ByVal value As Color)
            If Not value = Me._HeaderBackColor Then
                Me._HeaderBackColor = value
                If Me._HeaderBackBrush IsNot Nothing Then
                    Me._HeaderBackBrush.Dispose()
                    Me._HeaderBackBrush = Nothing
                End If
                Me.Invalidate()
            End If
        End Set
    End Property

    Private ReadOnly Property HeaderBackBrush() As SolidBrush
        Get
            If Me._HeaderBackBrush Is Nothing Then
                Me._HeaderBackBrush = New SolidBrush(Me.HeaderBackColor)
            End If
            Return Me._HeaderBackBrush
        End Get
    End Property

    Private ReadOnly Property HeaderPen() As Pen
        Get
            If Me._HeaderBackPen Is Nothing Then
                Me._HeaderBackPen = New Pen(Me.HeaderBorderColor)
            End If
            Return Me._HeaderBackPen
        End Get
    End Property

    <System.ComponentModel.DefaultValue(GetType(Color), "Black")> _
    Public Property HeaderForeColor() As Color
        Get
            Return Me._HeaderForeColor
        End Get
        Set(ByVal value As Color)
            If Not value = Me._HeaderForeColor Then
                Me._HeaderForeColor = value
                If Me._HeaderForeBrush IsNot Nothing Then
                    Me._HeaderForeBrush.Dispose()
                    Me._HeaderForeBrush = Nothing
                End If
                Me.Invalidate()
            End If
        End Set
    End Property

    Private ReadOnly Property HeaderForeBrush() As SolidBrush
        Get
            If Me._HeaderForeBrush Is Nothing Then
                Me._HeaderForeBrush = New SolidBrush(Me.HeaderForeColor)
            End If
            Return Me._HeaderForeBrush
        End Get
    End Property

    <System.ComponentModel.DefaultValue(GetType(Color), "DarkGray")> _
    Public Property HeaderSelectedBackColor() As Color
        Get
            Return Me._HeaderSelectedBackColor
        End Get
        Set(ByVal value As Color)
            If Not value = Me._HeaderSelectedBackColor Then
                Me._HeaderSelectedBackColor = value
                If Me._HeaderSelectedBackBrush IsNot Nothing Then
                    Me._HeaderSelectedBackBrush.Dispose()
                    Me._HeaderSelectedBackBrush = Nothing
                End If
                Me.Invalidate()
            End If
        End Set
    End Property

    Private ReadOnly Property HeaderSelectedBackBrush() As SolidBrush
        Get
            If Me._HeaderSelectedBackBrush Is Nothing Then
                Me._HeaderSelectedBackBrush = New SolidBrush(Me.HeaderSelectedBackColor)
            End If
            Return Me._HeaderSelectedBackBrush
        End Get
    End Property

    <System.ComponentModel.DefaultValue(GetType(Color), "Black")> _
    Public Property HeaderSelectedForeColor() As Color
        Get
            Return Me._HeaderSelectedForeColor
        End Get
        Set(ByVal value As Color)
            If Not value = Me._HeaderSelectedForeColor Then
                Me._HeaderSelectedForeColor = value
                Me._HeaderSelectedForeBrush.Dispose()
                Me._HeaderSelectedForeBrush = Nothing
                Me.Invalidate()
            End If
        End Set
    End Property

    Private ReadOnly Property HeaderSelectedForeBrush() As SolidBrush
        Get
            If Me._HeaderSelectedForeBrush Is Nothing Then
                Me._HeaderSelectedForeBrush = New SolidBrush(Me.HeaderSelectedForeColor)
            End If
            Return Me._HeaderSelectedForeBrush
        End Get
    End Property

    Public Property HeaderFont() As Font
        Get
            Return Me._HeaderFont
        End Get
        Set(ByVal value As Font)
            Me._HeaderFont = value
            Me.Invalidate()
        End Set
    End Property
#End Region

    Protected Overrides Sub OnParentBackColorChanged(ByVal e As System.EventArgs)
        MyBase.OnParentBackColorChanged(e)
        If Me._BackBrush IsNot Nothing Then
            Me._BackBrush.Dispose()
            Me._BackBrush = Nothing
        End If
        Me.Invalidate()
    End Sub

    <System.ComponentModel.DefaultValue(GetType(Color), "White")> _
    <System.ComponentModel.Browsable(True)> _
    Public Overrides Property BackColor() As Color
        Get
            Return Me._BackColor
        End Get
        Set(ByVal value As Color)
            If Not Me._BackColor = value Then
                Me._BackColor = value
                If Me._BackBrush IsNot Nothing Then
                    Me._BackBrush.Dispose()
                    Me._BackBrush = Nothing
                End If
                Me.Invalidate()
            End If
        End Set
    End Property

    Private ReadOnly Property BackBrush() As SolidBrush
        Get
            If Me._BackBrush Is Nothing Then
                If Me._BackColor = Color.Transparent AndAlso Me.Parent IsNot Nothing Then
                    Me._BackBrush = New SolidBrush(MyBase.Parent.BackColor)
                Else
                    Me._BackBrush = New SolidBrush(Me.BackColor)
                End If
            End If
            Return Me._BackBrush
        End Get
    End Property

    Public Sub New()
        Me._HeaderWidth = 100
        Me._HeaderHeight = 32
        Me._HeaderAlignment = ContentAlignment.MiddleLeft
        Me._HeaderPadding = New Padding(3)
        Me._BackColor = Color.White
        Me._HeaderBorderColor = Color.White
        Me._HeaderFont = Me.Font
        Me._HeaderForeColor = Color.Black
        Me._HeaderBackColor = Color.DarkGray
        Me._HeaderSelectedBackColor = Color.LightGray
        Me._HeaderSelectedForeColor = Color.Black
        Me.DrawMode = TabDrawMode.OwnerDrawFixed
        Me.SizeMode = TabSizeMode.Fixed

        Me.ItemSize = New Size(Me.HeaderHeight, Me.HeaderWidth)

        Me.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        Me.SetStyle(ControlStyles.UserPaint, True)
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        Dim g As Graphics

        g = e.Graphics

        g.FillRectangle(Me.BackBrush, e.ClipRectangle)      ' background

        For i As Integer = 0 To Me.TabPages.Count - 1
            Call Me.DrawTabButton(g, i)
        Next
    End Sub

    Private Sub DrawTabButton(ByVal g As Graphics, ByVal TabPageIndex As Integer)
        Dim rectTab As Rectangle
        ' get the tab rectangle
        rectTab = Me.GetTabRect(TabPageIndex)

        Dim path As New System.Drawing.Drawing2D.GraphicsPath

        ' if first tab page
        Select Case MyBase.Alignment
            Case TabAlignment.Top
                rectTab.Height = rectTab.Height + 2
                ' reduce its height and move it a little bit lower
                ' since in tab control first tab button is displayed a little
                ' bit heigher
                If TabPageIndex = 0 Then
                    rectTab.Width = rectTab.Width - 2
                    rectTab.X = rectTab.X + 2
                    '***** if first tab button, draw a line on left
                    path.AddLine(rectTab.Left, rectTab.Top, rectTab.Left, rectTab.Bottom)
                End If
                '**** line at Top
                path.AddLine(rectTab.Left, rectTab.Top, rectTab.Right - 1, rectTab.Top)
                '**** line at Right
                path.AddLine(rectTab.Right - 1, rectTab.Top, rectTab.Right - 1, rectTab.Bottom)

            Case TabAlignment.Bottom
                rectTab.Y = rectTab.Y - 2
                rectTab.Height = rectTab.Height + 2
                ' reduce its height and move it a little bit lower
                ' since in tab control first tab button is displayed a little
                ' bit heigher
                If TabPageIndex = 0 Then
                    rectTab.Width = rectTab.Width - 2
                    rectTab.X = rectTab.X + 2
                    '***** if first tab button, draw a line on left
                    path.AddLine(rectTab.Left, rectTab.Top, rectTab.Left, rectTab.Bottom)
                End If

                '**** line at bottom
                path.AddLine(rectTab.Left, rectTab.Bottom, rectTab.Right - 1, rectTab.Bottom)
                '**** line at Right
                path.AddLine(rectTab.Right - 1, rectTab.Top, rectTab.Right - 1, rectTab.Bottom)
                '**** line at Top
                'path.AddLine(r.Left, r.Top, r.Right, r.Top)

            Case TabAlignment.Left
                ' increase its width we dont want the background in between
                rectTab.Width = rectTab.Width + 2

                ' reduce its height and move it a little bit lower
                ' since in tab control first tab button is displayed a little
                ' bit heigher
                If TabPageIndex = 0 Then
                    rectTab.Height = rectTab.Height - 2
                    rectTab.Y = rectTab.Y + 2
                    '***** if first tab button, draw a line on top
                    path.AddLine(rectTab.Left, rectTab.Top, rectTab.Right, rectTab.Top)
                End If
                '**** line at Left
                path.AddLine(rectTab.Left, rectTab.Top, rectTab.Left, rectTab.Bottom - 1)
                '**** line at bottom
                path.AddLine(rectTab.Left, rectTab.Bottom - 1, rectTab.Right, rectTab.Bottom - 1)
            Case TabAlignment.Right
                ' increase its width we dont want the background in between
                rectTab.X = rectTab.X - 2
                rectTab.Width = rectTab.Width + 2

                ' reduce its height and move it a little bit lower
                ' since in tab control first tab button is displayed a little
                ' bit heigher
                If TabPageIndex = 0 Then
                    rectTab.Height = rectTab.Height - 2
                    rectTab.Y = rectTab.Y + 2
                    '***** if first tab button, draw a line on top
                    path.AddLine(rectTab.Left, rectTab.Top, rectTab.Right, rectTab.Top)
                End If
                '**** line at Right
                path.AddLine(rectTab.Right, rectTab.Top, rectTab.Right, rectTab.Bottom - 1)
                '**** line at bottom
                path.AddLine(rectTab.Left, rectTab.Bottom - 1, rectTab.Right, rectTab.Bottom - 1)
        End Select

        ' if given tab button is selected
        If Me.SelectedIndex = TabPageIndex Then
            ' use selected properties
            g.FillRectangle(Me.HeaderSelectedBackBrush, rectTab)
            ' if currently focused then draw focus rectangle
            If Me.Focused Then
                System.Windows.Forms.ControlPaint.DrawFocusRectangle(g, New Rectangle(rectTab.Left + 2, rectTab.Top + 2, rectTab.Width - 4, rectTab.Height - 5))
            End If
        Else ' else (not the selected tab page)
            g.FillRectangle(Me.HeaderBackBrush, rectTab)
        End If


        g.DrawPath(Me.HeaderPen, path)

        '' if first tab button
        'If TabPageIndex = 0 Then
        '    ' draw a line on top
        '    g.DrawLine(Me.HeaderPen, r.Left, r.Top, r.Right, r.Top)
        'End If
        '' line at left
        'g.DrawLine(Me.HeaderPen, r.Left, r.Top, r.Left, r.Bottom - 1)
        '' line at bottom
        'g.DrawLine(Me.HeaderPen, r.Left, r.Bottom - 1, r.Right, r.Bottom - 1)
        '' no line at right since we want to give an effect of
        '' pages

        rectTab.X += Me.HeaderPadding.Left
        rectTab.Width -= Me.HeaderPadding.Right
        rectTab.Y += Me.HeaderPadding.Top
        rectTab.Height -= Me.HeaderPadding.Bottom

        Using SF As New Drawing.StringFormat
            ' check text alignment
            Select Case Me.HeaderAlignment
                Case ContentAlignment.MiddleLeft, ContentAlignment.BottomLeft, ContentAlignment.TopLeft
                    SF.Alignment = StringAlignment.Near
                Case ContentAlignment.MiddleRight, ContentAlignment.BottomRight, ContentAlignment.TopRight
                    SF.Alignment = StringAlignment.Far
                Case ContentAlignment.MiddleCenter, ContentAlignment.BottomCenter, ContentAlignment.TopCenter
                    SF.Alignment = StringAlignment.Center
            End Select

            Select Case Me.HeaderAlignment
                Case ContentAlignment.TopLeft, ContentAlignment.TopCenter, ContentAlignment.TopRight
                    SF.LineAlignment = StringAlignment.Near
                Case ContentAlignment.BottomLeft, ContentAlignment.BottomCenter, ContentAlignment.BottomRight
                    SF.LineAlignment = StringAlignment.Far
                Case ContentAlignment.MiddleCenter, ContentAlignment.MiddleLeft, ContentAlignment.MiddleRight
                    SF.LineAlignment = StringAlignment.Center
            End Select

            If Me.SelectedIndex = TabPageIndex Then
                g.DrawString(Me.TabPages(TabPageIndex).Text, Me.HeaderFont, Me.HeaderSelectedForeBrush, rectTab, SF)
            Else
                g.DrawString(Me.TabPages(TabPageIndex).Text, Me.HeaderFont, Me.HeaderForeBrush, rectTab, SF)
            End If

        End Using
    End Sub

#Region " Hide some properties which can disturb our customization "
    <System.ComponentModel.Browsable(False)> _
    Public Overloads Property DrawMode() As System.Windows.Forms.TabDrawMode
        Get
            Return MyBase.DrawMode
        End Get
        Set(ByVal value As System.Windows.Forms.TabDrawMode)
            MyBase.DrawMode = TabDrawMode.OwnerDrawFixed
        End Set
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public Overloads Property Appearance() As System.Windows.Forms.TabAppearance
        Get
            Return MyBase.Appearance
        End Get
        Set(ByVal value As System.Windows.Forms.TabAppearance)
            MyBase.Appearance = TabAppearance.Normal
        End Set
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public Overloads Property ItemSize() As Size
        Get
            Return MyBase.ItemSize
        End Get
        Set(ByVal value As Size)
            MyBase.ItemSize = value
        End Set
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public Overloads Property SizeMode() As System.Windows.Forms.TabSizeMode
        Get
            Return MyBase.SizeMode
        End Get
        Set(ByVal value As System.Windows.Forms.TabSizeMode)
            MyBase.SizeMode = TabSizeMode.Fixed
        End Set
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public Overloads Property Multiline() As Boolean
        Get
            Return MyBase.Multiline
        End Get
        Set(ByVal value As Boolean)
            MyBase.Multiline = True
        End Set
    End Property

    Public Overloads Sub OnRightToLeftLayoutChanged()

    End Sub
#End Region

End Class
