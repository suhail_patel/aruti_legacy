Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.ComponentModel
Imports System.Windows.Forms

'<System.Diagnostics.DebuggerStepThrough()> _
Public Class eZeeLightButton
    Inherits Button

#Region " Private Variables "

    Private StructColors As ButtonColors
    Private mblnShowDefaultBorderColor As Boolean = True
    Private mlocCurrentLocation As New Point

    Private mblnSelected As Boolean = False
    Private memButtonType As enButtonType = enButtonType.NormalButton

    Public Event CheckedChanged As EventHandler

    Structure ButtonColors
        Dim mclrHoverForeColor As Color
        Dim mclrPressedForeColor As Color
        Dim mclrGradientForeColor As Color

        Dim mclrHoverBackColor As Color
        Dim mclrPressedBackColor As Color
        Dim mclrGradientBackColor As Color

        Dim mclrBorderColor As Color
    End Structure

    Public Enum enButtonType
        NormalButton = 0
        CheckButton = 1
        RadioButton = 2
    End Enum

#End Region

#Region " Contructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.Height = 30
        Me.Width = 90
        Me.BackgroundImageLayout = ImageLayout.Center
        Me.BackColor = Color.White
        'Me.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)

        Me.FlatStyle = Windows.Forms.FlatStyle.Flat
        Me.FlatAppearance.BorderSize = 0

        StructColors = New ButtonColors

        Me.StructColors.mclrHoverForeColor = Color.Black
        Me.StructColors.mclrPressedForeColor = Color.Black
        Me.StructColors.mclrGradientForeColor = Color.Black
        Me.StructColors.mclrHoverBackColor = Color.Transparent
        Me.StructColors.mclrPressedBackColor = Color.Transparent
        Me.StructColors.mclrGradientBackColor = SystemColors.ActiveBorder

        Me.Invalidate()
    End Sub

#End Region

#Region " Properties "

    <DefaultValue(GetType(Color), "system.Drawing.Color.Black")> _
    Public Property GradientForeColor() As Color
        Get
            Return StructColors.mclrGradientForeColor
        End Get
        Set(ByVal value As Color)
            StructColors.mclrGradientForeColor = value
            Call SetDefaultGradientImage()
        End Set
    End Property

    <DefaultValue(GetType(Color), "system.Drawing.Color.Black"), Browsable(False)> _
    Public Property HoverGradientForeColor() As Color
        Get
            Return StructColors.mclrHoverForeColor
        End Get
        Set(ByVal value As Color)
            StructColors.mclrHoverForeColor = value
        End Set
    End Property

    <DefaultValue(GetType(Color), "system.Drawing.Color.Black"), Browsable(False)> _
    Public Property PressedGradientForeColor() As Color
        Get
            Return StructColors.mclrPressedForeColor
        End Get
        Set(ByVal value As Color)
            StructColors.mclrPressedForeColor = value
        End Set
    End Property

    <DefaultValue(GetType(Color), "System.Drawing.SystemColors.ActiveBorder")> _
    Public Property GradientBackColor() As Color
        Get
            Return StructColors.mclrGradientBackColor
        End Get
        Set(ByVal value As Color)
            StructColors.mclrGradientBackColor = value
            Call SetDefaultGradientImage()
        End Set
    End Property

    <DefaultValue(GetType(Color), "System.Drawing.Color.Transparent"), Browsable(False)> _
    Public Property HoverGradientBackColor() As Color
        Get
            Return StructColors.mclrHoverBackColor
        End Get
        Set(ByVal value As Color)
            StructColors.mclrHoverBackColor = value
        End Set
    End Property

    <DefaultValue(GetType(Color), "System.Drawing.Color.Transparent"), Browsable(False)> _
    Public Property PressedGradientBackColor() As Color
        Get
            Return StructColors.mclrPressedBackColor
        End Get
        Set(ByVal value As Color)
            StructColors.mclrPressedBackColor = value
        End Set
    End Property

    Public Property BorderColor() As Color
        Get
            Return StructColors.mclrBorderColor
        End Get
        Set(ByVal value As Color)
            StructColors.mclrBorderColor = value
            Call SetDefaultGradientImage()
        End Set
    End Property

    <DefaultValue(True)> _
    Public Property ShowDefaultBorderColor() As Boolean
        Get
            Return mblnShowDefaultBorderColor
        End Get
        Set(ByVal value As Boolean)
            mblnShowDefaultBorderColor = value
            Call SetDefaultGradientImage()
        End Set
    End Property

    <Browsable(False)> _
    Public Shadows Property ForeColor() As Color
        Get
            Return MyBase.ForeColor
        End Get
        Set(ByVal value As Color)
            MyBase.ForeColor = value
        End Set
    End Property

    <DefaultValue(False)> _
    Public Property Selected() As Boolean
        Get
            Return mblnSelected
        End Get
        Set(ByVal value As Boolean)
            mblnSelected = value
            Call DoToggleButtonOperation()
            RaiseEvent CheckedChanged(Me, New EventArgs)
        End Set
    End Property

    <DefaultValue(DirectCast(enButtonType.NormalButton, Integer))> _
    Public Property ButtonType() As enButtonType
        Get
            Return memButtonType
        End Get
        Set(ByVal value As enButtonType)
            memButtonType = value
            Call DoToggleButtonOperation()
        End Set
    End Property

#End Region

#Region " Private Methods "

    Private Function GetHoverBackImage(ByVal cntrl As Control) As Image
        Dim btmap As New Bitmap(cntrl.Width, cntrl.Height)

        Dim g As Graphics = Graphics.FromImage(btmap)
        Dim start As New Point
        start.X = CInt(cntrl.Width / 0.4)
        start.Y = 0
        Dim gb As New LinearGradientBrush(start, New Point(start.X, CInt((cntrl.Height * 0.4))), Color.FromArgb(255, 251, 212), Color.FromArgb(255, 231, 150))
        g.FillRectangle(gb, New Rectangle(0, 0, cntrl.Width, CInt(cntrl.Height * 0.4)))
        gb = New LinearGradientBrush(New Point(start.X, CInt(cntrl.Height * 0.4)), New Point(start.X, cntrl.Height), Color.FromArgb(255, 213, 82), Color.FromArgb(255, 230, 151))
        g.FillRectangle(gb, New Rectangle(0, CInt(cntrl.Height * 0.4), cntrl.Width, cntrl.Height))

        g.DrawLine(New Pen(Color.FromArgb(255, 247, 185)), 1, 1, cntrl.Width - 2, 1) 'Up Inner Border
        g.DrawLine(New Pen(Color.FromArgb(219, 197, 123)), 1, 0, cntrl.Width - 2, 0) 'Up Outer Border
        g.DrawLine(New Pen(Color.FromArgb(255, 216, 146)), 1, cntrl.Height - 2, cntrl.Width - 2, cntrl.Height - 2) 'Down Inner Border
        g.DrawLine(New Pen(Color.FromArgb(208, 186, 115)), 1, cntrl.Height - 1, cntrl.Width - 2, cntrl.Height - 1) 'Down Outer Border
        g.DrawLine(New Pen(Color.FromArgb(215, 183, 102)), 0, 0, 0, cntrl.Height) 'Left Vertical Border
        g.DrawLine(New Pen(Color.FromArgb(216, 182, 101)), cntrl.Width - 1, 0, cntrl.Width - 1, cntrl.Height) 'Right Vertical Border

        Return btmap

    End Function

    Private Function GetSelectedImage(ByVal cntrl As Control) As Image
        Dim btmap As New Bitmap(cntrl.Width, cntrl.Height)

        Dim g As Graphics = Graphics.FromImage(btmap)
        Dim start As New Point
        Dim gb As LinearGradientBrush
        start.X = CInt(cntrl.Width * 0.45)
        start.Y = 0
        gb = New LinearGradientBrush(start, New Point(start.X, CInt(cntrl.Height * 0.45)), Color.FromArgb(244, 188, 106), Color.FromArgb(255, 176, 92))
        g.FillRectangle(gb, New Rectangle(0, 0, cntrl.Width, CInt(cntrl.Height * 0.45)))
        gb = New LinearGradientBrush(New Point(start.X, CInt(cntrl.Height * 0.45 - 1)), New Point(start.X, cntrl.Height), Color.FromArgb(254, 160, 63), Color.FromArgb(255, 205, 130))
        g.FillRectangle(gb, New Rectangle(0, CInt(cntrl.Height * 0.45), cntrl.Width, cntrl.Height))

        'Left Vertical Border
        gb = New LinearGradientBrush(start, New Point(start.X, cntrl.Height), Color.FromArgb(142, 120, 87), Color.FromArgb(200, 186, 164))
        g.FillRectangle(gb, New Rectangle(0, 1, 1, cntrl.Height))

        'Right Vertical Border
        gb = New LinearGradientBrush(start, New Point(start.X, cntrl.Height), Color.FromArgb(142, 120, 87), Color.FromArgb(200, 186, 164))
        g.FillRectangle(gb, New Rectangle(cntrl.Width - 1, 1, 1, cntrl.Height))

        g.DrawLine(New Pen(Color.FromArgb(139, 118, 84)), 1, 0, cntrl.Width - 2, 0) 'Up Outer Border
        g.DrawLine(New Pen(Color.FromArgb(201, 159, 97)), 1, 1, cntrl.Width - 2, 1) 'Up Inner Border 1
        g.DrawLine(New Pen(Color.FromArgb(226, 177, 103)), 1, 2, cntrl.Width - 2, 2) 'Up Inner Border 2
        g.DrawLine(New Pen(Color.FromArgb(253, 173, 17)), 1, cntrl.Height - 1, cntrl.Width - 2, cntrl.Height - 1) 'Down Outer Border

        Return btmap
    End Function

    Private Function GetHoverSelectedImage(ByVal cntrl As Control) As Image
        Dim btmap As New Bitmap(cntrl.Width, cntrl.Height)

        Dim g As Graphics = Graphics.FromImage(btmap)
        Dim start As New Point
        Dim gb As LinearGradientBrush
        start.X = CInt(Me.Width * 0.45)
        start.Y = 0
        gb = New LinearGradientBrush(start, New Point(start.X, CInt(Me.Height * 0.45)), Color.FromArgb(252, 194, 124), Color.FromArgb(251, 179, 97))
        g.FillRectangle(gb, New Rectangle(0, 0, cntrl.Width, CInt(Me.Height * 0.45)))
        gb = New LinearGradientBrush(New Point(start.X, CInt(Me.Height * 0.45 - 1)), New Point(start.X, cntrl.Height), Color.FromArgb(251, 166, 62), Color.FromArgb(252, 211, 111))
        g.FillRectangle(gb, New Rectangle(0, CInt(Me.Height * 0.45), cntrl.Width, cntrl.Height))

        'Left Vertical Border
        gb = New LinearGradientBrush(start, New Point(start.X, cntrl.Height), Color.FromArgb(142, 129, 101), Color.FromArgb(200, 186, 164))
        g.FillRectangle(gb, New Rectangle(0, 1, 1, cntrl.Height))

        'Right Vertical Border
        gb = New LinearGradientBrush(start, New Point(start.X, cntrl.Height), Color.FromArgb(142, 129, 101), Color.FromArgb(204, 190, 165))
        g.FillRectangle(gb, New Rectangle(cntrl.Width - 1, 1, 1, cntrl.Height))

        g.DrawLine(New Pen(Color.FromArgb(142, 129, 101)), 1, 0, cntrl.Width - 2, 0) 'Up Outer Border
        g.DrawLine(New Pen(Color.FromArgb(168, 136, 93)), 1, 1, cntrl.Width - 2, 1) 'Up Inner Border 1
        g.DrawLine(New Pen(Color.FromArgb(211, 160, 103)), 1, 2, cntrl.Width - 2, 2) 'Up Inner Border 2
        g.DrawLine(New Pen(Color.FromArgb(212, 197, 173)), 1, cntrl.Height - 1, cntrl.Width - 2, cntrl.Height - 1) 'Down Outer Border
        g.DrawLine(New Pen(Color.FromArgb(255, 229, 141)), 1, cntrl.Height - 2, cntrl.Width - 2, cntrl.Height - 2) 'Down Inner Border

        Return btmap
    End Function

    Private Function GetEnabledImage() As Image
        Dim btmap As New Bitmap(Me.Width, Me.Height)

        Dim g As Graphics = Graphics.FromImage(btmap)

        g.DrawLine(New Pen(SystemColors.ControlDark), 1, 0, Me.Width - 2, 0) 'Up Outer Border
        g.DrawLine(New Pen(SystemColors.ControlDark), 1, Me.Height - 1, Me.Width - 2, Me.Height - 1) 'Down Outer Border
        g.DrawLine(New Pen(SystemColors.ControlDark), 0, 0, 0, Me.Height) 'Left Vertical Border
        g.DrawLine(New Pen(SystemColors.ControlDark), Me.Width - 1, 0, Me.Width - 1, Me.Height) 'Right Vertical Border

        Return btmap
    End Function

    Private Sub DoToggleButtonOperation()
        If Not Me.ButtonType = enButtonType.NormalButton Then
            If Me.Selected = False Then
                SetDefaultGradientImage()
            Else
                SetPressedGradientImage()
            End If
        End If
    End Sub

#End Region

#Region " Public Methods "

    Public Sub SetDefaultGradientImage()
        Select Case Me.ButtonType
            Case enButtonType.RadioButton, enButtonType.CheckButton
                If Me.Selected Then
                    Me.BackgroundImage = Me.GetSelectedImage(Me)
                Else
                    Me.BackgroundImage = Me.GetDefaultBackImage(Me.GradientBackColor)
                End If
            Case Else
                Me.BackgroundImage = Me.GetDefaultBackImage(Me.GradientBackColor)
        End Select

        Me.ForeColor = Me.GradientForeColor
        Me.Invalidate()
    End Sub

    Public Sub SetHoverGradientImage()
        If Me.Selected Then
            Me.BackgroundImage = Me.GetHoverSelectedImage(Me)
        Else
            Me.BackgroundImage = Me.GetHoverBackImage(Me)
        End If
        Me.ForeColor = Color.Black
        Me.Invalidate()
    End Sub

    Public Sub SetPressedGradientImage()
        Me.BackgroundImage = Me.GetSelectedImage(Me)
        Me.ForeColor = Color.Black
        Me.Invalidate()
    End Sub

    Public Sub SetEnabledImage()
        If Me.Enabled = False Then
            Me.BackgroundImage = Me.GetEnabledImage()
        Else
            Call SetDefaultGradientImage()
        End If
    End Sub

    Public Function GetDefaultBackImage(ByVal GradientColor As Color) As Image
        Dim btmap As New Bitmap(Me.Width, Me.Height)

        Dim g As Graphics = Graphics.FromImage(btmap)

        Dim start As New Point
        start.X = CInt(Me.Width / 0.5)
        start.Y = 0

        Dim rect As Rectangle

        rect = New Rectangle(start, CType(New Point(start.X, CInt((Me.Height * 0.6))), Drawing.Size))
        Dim gb As New LinearGradientBrush(rect, ControlPaint.Light(GradientColor, 1.5F), GradientColor, LinearGradientMode.Vertical) 'Color.FromArgb(206, 223, 251)
        g.FillRectangle(gb, New Rectangle(0, 0, Me.Width, CInt(Me.Height * 0.6)))

        rect = New Rectangle(New Point(start.X, CInt(Me.Height * 0.4)), CType(New Point(start.X, Me.Height), Drawing.Size))
        gb = New LinearGradientBrush(rect, GradientColor, ControlPaint.Light(GradientColor, 1.6), LinearGradientMode.Vertical) 'Color.FromArgb(241, 247, 254))
        g.FillRectangle(gb, New Rectangle(0, CInt(Me.Height * 0.5), Me.Width, CInt(Me.Height * 0.5)))

        rect = New Rectangle(start, New Size(Me.Width, Me.Height))

        If Me.ShowDefaultBorderColor Then
            g.DrawLine(New Pen(ControlPaint.Dark(GradientColor, -0.1F)), 1, 0, Me.Width - 2, 0) 'Up Outer Border
            g.DrawLine(New Pen(ControlPaint.Dark(GradientColor, -0.1F)), 1, Me.Height - 1, Me.Width - 2, Me.Height - 1) 'Down Outer Border
            g.DrawLine(New Pen(ControlPaint.Dark(GradientColor, -0.1F)), 0, 0, 0, Me.Height) 'Left Vertical Border
            g.DrawLine(New Pen(ControlPaint.Dark(GradientColor, -0.1F)), Me.Width - 1, 0, Me.Width - 1, Me.Height) 'Right Vertical Border
        Else
            g.DrawLine(New Pen(Me.BorderColor), 1, 0, Me.Width - 2, 0) 'Up Outer Border
            g.DrawLine(New Pen(Me.BorderColor), 1, Me.Height - 1, Me.Width - 2, Me.Height - 1) 'Down Outer Border
            g.DrawLine(New Pen(Me.BorderColor), 0, 0, 0, Me.Height) 'Left Vertical Border
            g.DrawLine(New Pen(Me.BorderColor), Me.Width - 1, 0, Me.Width - 1, Me.Height) 'Right Vertical Border
        End If

        If Me.ShowDefaultBorderColor Then
            ControlPaint.DrawBorder(Me.CreateGraphics, Me.ClientRectangle, ControlPaint.Dark(GradientColor, -0.1F), ButtonBorderStyle.Solid)
        Else
            ControlPaint.DrawBorder(Me.CreateGraphics, Me.ClientRectangle, Me.BorderColor, ButtonBorderStyle.Solid)
        End If

        If Me.Enabled = True Then
            Dim DisableColor As Color
            DisableColor = ControlPaint.Light(GradientColor, 1.6)
            g.FillRectangle(New SolidBrush(Color.FromArgb(10, DisableColor.R, DisableColor.G, DisableColor.B)), rect)
            Me.ForeColor = Me.GradientForeColor
        Else
            'Me.BackColor = Color.Transparent
            Me.ForeColor = SystemColors.Control
        End If

        Return btmap
    End Function

#End Region

#Region " Overrides Events "

    'Public Shadows Property Enabled() As Boolean
    '    Get
    '        Return MyBase.Enabled
    '    End Get
    '    Set(ByVal value As Boolean)
    '        If value = True Then
    '            Call SetDefaultGradientImage()
    '            Me.ForeColor = Me.GradientForeColor
    '        Else
    '            Me.BackColor = Color.Transparent
    '            Me.ForeColor = SystemColors.Control ' ControlPaint.LightLight(Me.GradientForeColor)
    '        End If
    '        MyBase.Enabled = value
    '    End Set
    'End Property

    Protected Overrides Sub OnEnabledChanged(ByVal e As System.EventArgs)
        MyBase.OnEnabledChanged(e)
        Call SetDefaultGradientImage()
    End Sub

    Protected Overrides Sub OnParentEnabledChanged(ByVal e As System.EventArgs)
        MyBase.OnParentEnabledChanged(e)
        Call SetDefaultGradientImage()
    End Sub

    Protected Overrides Sub OnParentVisibleChanged(ByVal e As System.EventArgs)
        MyBase.OnParentVisibleChanged(e)
        Call SetDefaultGradientImage()
    End Sub

    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        MyBase.OnResize(e)

        Call SetDefaultGradientImage()
    End Sub

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()

        Call SetDefaultGradientImage()
    End Sub

    Protected Overrides Sub OnMouseEnter(ByVal e As System.EventArgs)
        MyBase.OnMouseEnter(e)

        Call SetHoverGradientImage()
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal mevent As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseDown(mevent)

        Call SetPressedGradientImage()
    End Sub

    'Krushna (10 Sep 2009) (Loyalty Program) -- Start
    Protected Overrides Sub OnMouseUp(ByVal mevent As System.Windows.Forms.MouseEventArgs)
        If Not Me.IsDisposed Then Call SetDefaultGradientImage()
        MyBase.OnMouseUp(mevent)
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As System.EventArgs)
        If Not Me.IsDisposed Then Call SetDefaultGradientImage()
        MyBase.OnMouseLeave(e)
    End Sub
    'Krushna (10 Sep 2009) (Loyalty Program) -- End

    Protected Overrides Sub OnLostFocus(ByVal e As System.EventArgs)
        MyBase.OnLostFocus(e)

        Call SetDefaultGradientImage()
    End Sub

    Protected Overrides Sub OnGotFocus(ByVal e As System.EventArgs)
        MyBase.OnGotFocus(e)

        Call SetDefaultGradientImage()
    End Sub

    Protected Overrides Sub OnKeyDown(ByVal kevent As System.Windows.Forms.KeyEventArgs)
        MyBase.OnKeyDown(kevent)

        If kevent.KeyCode = Keys.Enter Or kevent.KeyCode = Keys.Space Then
            Call SetPressedGradientImage()
        End If
    End Sub

    Protected Overrides Sub OnKeyUp(ByVal kevent As System.Windows.Forms.KeyEventArgs)
        MyBase.OnKeyUp(kevent)

        If Me.IsDisposed Then Exit Sub
        If kevent.KeyCode = Keys.Enter Or kevent.KeyCode = Keys.Space Then
            Call SetDefaultGradientImage()
        End If
    End Sub

    Protected Overrides Sub OnPaint(ByVal pevent As System.Windows.Forms.PaintEventArgs)
        MyBase.OnPaint(pevent)

        If Me.Enabled = False Then
            Me.BackgroundImage = Nothing
            Dim g As Graphics = pevent.Graphics

            g.DrawLine(New Pen(SystemColors.ControlDark), 1, 0, Me.Width - 2, 0) 'Up Outer Border
            g.DrawLine(New Pen(SystemColors.ControlDark), 1, Me.Height - 1, Me.Width - 2, Me.Height - 1) 'Down Outer Border
            g.DrawLine(New Pen(SystemColors.ControlDark), 0, 0, 0, Me.Height) 'Left Vertical Border
            g.DrawLine(New Pen(SystemColors.ControlDark), Me.Width - 1, 0, Me.Width - 1, Me.Height) 'Right Vertical Border
        End If
    End Sub

    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        Select Case Me.ButtonType
            Case enButtonType.NormalButton

            Case enButtonType.CheckButton
                Me.Selected = Not Me.Selected
            Case enButtonType.RadioButton
                If Me.Parent IsNot Nothing Then
                    For Each ctr As Control In Me.Parent.Controls
                        If TypeOf ctr Is eZeeLightButton _
                            AndAlso TryCast(ctr, eZeeLightButton).ButtonType = enButtonType.RadioButton _
                            AndAlso TryCast(ctr, eZeeLightButton).Selected = True _
                            AndAlso ctr.Name <> Me.Name Then

                            TryCast(ctr, eZeeLightButton).Selected = False
                        End If
                    Next
                End If
                Me.Selected = True
        End Select

        'Krishna (24 Aug 08) -- Start
        MyBase.OnClick(e)
        'Krishna (24 Aug 08) -- End
        'Call DoToggleButtonOperation()
    End Sub

    'Public Overloads Sub PerformClick()
    '    If Me.CanSelect Then
    '        Me.OnClick(EventArgs.Empty)
    '    End If
    'End Sub
#End Region

End Class
