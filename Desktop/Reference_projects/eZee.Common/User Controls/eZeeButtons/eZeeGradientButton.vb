Imports System.ComponentModel
Imports System.ComponentModel.Design
Imports System.Windows.Forms
Imports System.Drawing

<Designer(GetType(System.Windows.Forms.Design.ControlDesigner), GetType(System.ComponentModel.Design.IDesigner)), _
    DesignTimeVisibleAttribute(True), DefaultProperty("Text"), DefaultEvent("Click")> _
Public Class eZeeGradientButton
    Inherits System.Windows.Forms.Control
    Implements System.Windows.Forms.IButtonControl

#Region "Private fields"
    Private mTextAlign As ContentAlignment = ContentAlignment.MiddleCenter
    Private mTextQuality As eZeeGradientButtonEnums.TextRendering = eZeeGradientButtonEnums.TextRendering.SystemDefault
    Private mBorderNormal As Boolean = False
    Private mBorderHover As Boolean = False
    Private mBorderSelected As Boolean = True
    Private mBorderNormalColor As Color = Color.FromArgb(179, 176, 208)
    Private mBorderHoverColor As Color = Color.FromArgb(179, 176, 208)
    Private mBorderSelectedColor As Color = Color.Blue
    Private mBackColor1 As Color = Color.FromArgb(221, 236, 254)
    Private mBackColor2 As Color = Color.FromArgb(129, 169, 226)
    Private mHoverColor1 As Color = Color.FromArgb(255, 244, 204)
    Private mHoverColor2 As Color = Color.FromArgb(255, 208, 145)
    Private mSelectedColor1 As Color = Color.FromArgb(255, 213, 140)
    Private mSelectedColor2 As Color = Color.FromArgb(255, 173, 85)
    Private mSelectedHoverColor1 As Color = Color.FromArgb(254, 145, 78)
    Private mSelectedHoverColor2 As Color = Color.FromArgb(255, 211, 142)
    Private mClickColor1 As Color = Color.FromArgb(254, 145, 78)
    Private mClickColor2 As Color = Color.FromArgb(255, 211, 142)
    Private mGradientMode As Drawing2D.LinearGradientMode = Drawing2D.LinearGradientMode.Vertical
    Private mImage As Image = Nothing
    Private mImageAlign As ContentAlignment = ContentAlignment.MiddleLeft
    Private mImageSelected As Image = Nothing
    Private mHoverForeColor As Color = Color.Blue
    'IButtonControl interface
    Private mIsDefaultButton As Boolean = False
    Private mDialogResult As DialogResult = DialogResult.None

    Public Enum HoverButtonType
        HoverNormal
        HoverCheck
        HoverOption
    End Enum
    Private mButtonType As HoverButtonType = HoverButtonType.HoverNormal
    Private mSelected As Boolean = False
    Private mOptionGroup As Integer = 0

    Private mColorDefault As Color = Color.Blue
    Private mDoClick As Boolean = False
    Private mMouseHover As Boolean = False

    Private mToolTip As ToolTip = New ToolTip
    Private mToolTipActive As Boolean = False
    Private mToolTipText As String = ""

#End Region

#Region "Component Designer generated code"

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

        ' This draws the control whenever it is resized
        SetStyle(ControlStyles.ResizeRedraw, True)

        ' This supports mouse movement such as the mouse wheel
        SetStyle(ControlStyles.UserMouse, True)

        ' This allows the control to be transparent
        SetStyle(ControlStyles.SupportsTransparentBackColor, True)

        ' This helps with drawing the control so that it doesn't flicker
        Me.SetStyle(ControlStyles.DoubleBuffer _
          Or ControlStyles.UserPaint _
          Or ControlStyles.AllPaintingInWmPaint, _
          True)

        ' This control is a container
        SetStyle(ControlStyles.ContainerControl, False)

        ' This updates the styles
        Me.UpdateStyles()

    End Sub

    'Control overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Control Designer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  Do not modify it
    ' using the code editor.
    Private components As System.ComponentModel.IContainer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'HoverGradientButton
        '
        Me.Size = New System.Drawing.Size(220, 112)
        Me.TabStop = False
        If Not mToolTip Is Nothing Then
            mToolTip.Active = Me.ToolTipActive
        End If

    End Sub

#End Region

#Region "Public Properties"

#Region "TextAlign"
    <DefaultValue(GetType(ContentAlignment), "MiddleCenter")> _
  Public Property TextAlign() As ContentAlignment
        Get
            Return mTextAlign
        End Get
        Set(ByVal Value As ContentAlignment)
            mTextAlign = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "Property TextQuality"
    <DefaultValue(GetType(eZeeGradientButtonEnums.TextRendering), "SystemDefault")> _
    Public Property TextQuality() As eZeeGradientButtonEnums.TextRendering
        Get
            Return mTextQuality
        End Get
        Set(ByVal Value As eZeeGradientButtonEnums.TextRendering)
            mTextQuality = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "Border Normal"
    <DefaultValue(GetType(Boolean), "False")> _
    Public Property BorderNormal() As Boolean
        Get
            Return mBorderNormal
        End Get
        Set(ByVal Value As Boolean)
            mBorderNormal = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "Border Hover"
    <DefaultValue(GetType(Boolean), "False")> _
    Public Property BorderHover() As Boolean
        Get
            Return mBorderHover
        End Get
        Set(ByVal Value As Boolean)
            mBorderHover = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "Border Selected"
    <DefaultValue(GetType(Boolean), "True")> _
    Public Property BorderSelected() As Boolean
        Get
            Return mBorderSelected
        End Get
        Set(ByVal Value As Boolean)
            mBorderSelected = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "BorderNormalColor"
    <DefaultValue(GetType(Color), "179, 176, 208")> _
    Public Property BorderNormalColor() As Color
        Get
            Return mBorderNormalColor
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = Color.Blue
            mBorderNormalColor = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "BorderHoverColor"
    <DefaultValue(GetType(Color), "179, 176, 208")> _
    Public Property BorderHoverColor() As Color
        Get
            Return mBorderHoverColor
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = Color.Blue
            mBorderHoverColor = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "BorderSelectedColor"
    <DefaultValue(GetType(Color), "Blue")> _
    Public Property BorderSelectedColor() As Color
        Get
            Return mBorderSelectedColor
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = Color.Blue
            mBorderSelectedColor = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "BackColor1"
    <DefaultValue(GetType(Color), "221, 236, 254")> _
    Public Property BackColor1() As Color
        Get
            Return mBackColor1
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = mColorDefault
            mBackColor1 = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "BackColor2"
    <DefaultValue(GetType(Color), "Color.FromArgb(129, 169, 226)")> _
    Public Property BackColor2() As Color
        Get
            Return mBackColor2
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = mColorDefault
            mBackColor2 = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "HoverColor1"
    <DefaultValue(GetType(Color), "255, 244, 204")> _
    Public Property HoverColor1() As Color
        Get
            Return mHoverColor1
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = mColorDefault
            mHoverColor1 = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "HoverColor2"
    <DefaultValue(GetType(Color), "255, 208, 145")> _
    Public Property HoverColor2() As Color
        Get
            Return mHoverColor2
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = mColorDefault
            mHoverColor2 = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "SelectedColor1"
    <DefaultValue(GetType(Color), "255, 213, 140")> _
    Public Property SelectedColor1() As Color
        Get
            Return mSelectedColor1
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = mColorDefault
            mSelectedColor1 = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "SelectedColor2"
    <DefaultValue(GetType(Color), "255, 173, 85")> _
    Public Property SelectedColor2() As Color
        Get
            Return mSelectedColor2
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = mColorDefault
            mSelectedColor2 = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "SelectedHoverColor1"
    <DefaultValue(GetType(Color), "254, 145, 78")> _
    Public Property SelectedHoverColor1() As Color
        Get
            Return mSelectedHoverColor1
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = mColorDefault
            mSelectedHoverColor1 = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "SelectedHoverColor2"
    <DefaultValue(GetType(Color), "255, 211, 142")> _
    Public Property SelectedHoverColor2() As Color
        Get
            Return mSelectedHoverColor2
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = mColorDefault
            mSelectedHoverColor2 = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "ClickColor1"
    <DefaultValue(GetType(Color), "254, 145, 78")> _
    Public Property ClickColor1() As Color
        Get
            Return mClickColor1
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = mColorDefault
            mClickColor1 = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "ClickColor2"
    <DefaultValue(GetType(Color), "255, 211, 142")> _
    Public Property ClickColor2() As Color
        Get
            Return mClickColor2
        End Get
        Set(ByVal Value As Color)
            If Value.Equals(Color.Empty) Then Value = mColorDefault
            mClickColor2 = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "Property GradientMode"
    <DefaultValue(GetType(Drawing2D.LinearGradientMode), "Vertical")> _
    Public Property GradientMode() As Drawing2D.LinearGradientMode
        Get
            Return mGradientMode
        End Get
        Set(ByVal value As Drawing2D.LinearGradientMode)
            mGradientMode = value
            Invalidate()
        End Set
    End Property
#End Region

#Region "Image"
    <DefaultValue(GetType(Image), "Nothing")> _
    Public Property Image() As Image
        Get
            Return mImage
        End Get
        Set(ByVal Value As Image)
            mImage = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "ImageAlign"
    <DefaultValue(GetType(ContentAlignment), "MiddleLeft")> _
    Public Property ImageAlign() As ContentAlignment
        Get
            Return mImageAlign
        End Get
        Set(ByVal Value As ContentAlignment)
            mImageAlign = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "ImageSelected"
    <DefaultValue(GetType(Image), "Nothing")> _
    Public Property ImageSelected() As Image
        Get
            Return mImageSelected
        End Get
        Set(ByVal Value As Image)
            mImageSelected = Value
            Invalidate()
        End Set
    End Property
#End Region

#Region "HoverForeColor"
    <DefaultValue(GetType(Color), "Blue")> _
    Public Property HoverForeColor() As Color
        Get
            Return mHoverForeColor
        End Get
        Set(ByVal Value As Color)
            mHoverForeColor = Value
        End Set
    End Property
#End Region

#Region "ButtonType"
    <DefaultValue(GetType(HoverButtonType), "HoverNormal")> _
    Public Property ButtonType() As HoverButtonType
        Get
            Return mButtonType
        End Get
        Set(ByVal Value As HoverButtonType)
            mButtonType = Value
            If mButtonType = HoverButtonType.HoverOption Then ClearOptionButtons.Clear(Me)
            Invalidate()
        End Set
    End Property
#End Region

#Region "Selected"
    <DefaultValue(GetType(Boolean), "False")> _
    Public Property Selected() As Boolean
        Get
            Return mSelected
        End Get
        Set(ByVal Value As Boolean)
            mSelected = Value
            If mButtonType = HoverButtonType.HoverNormal Then Exit Property
            If mSelected AndAlso mButtonType = HoverButtonType.HoverOption Then ClearOptionButtons.Clear(Me)
            Invalidate()
        End Set
    End Property
#End Region

#Region "OptionGroup"
    <DefaultValue(GetType(Integer), "0")> _
    Public Property OptionGroup() As Integer
        Get
            Return mOptionGroup
        End Get
        Set(ByVal Value As Integer)
            mOptionGroup = Value
        End Set
    End Property
#End Region

#Region "ToolTipActive"
    <DefaultValue(GetType(Boolean), "False")> _
    Public Property ToolTipActive() As Boolean
        Get
            Return mToolTipActive
        End Get
        Set(ByVal Value As Boolean)
            mToolTipActive = Value
            mToolTip.Active = Value
        End Set
    End Property
#End Region

#Region "TooltipText"
    <DefaultValue(GetType(String), "")> _
    Public Property ToolTipText() As String
        Get
            Return mToolTipText
        End Get
        Set(ByVal Value As String)
            mToolTipText = Value
            mToolTip.SetToolTip(Me, Value)
        End Set
    End Property
#End Region

#Region "TooltipText"
    <DefaultValue(False)> _
    Public Overloads Property TabStop() As Boolean
        Get
            Return MyBase.TabStop
        End Get
        Set(ByVal Value As Boolean)
            MyBase.TabStop = Value
        End Set
    End Property
#End Region
#End Region

#Region "Public Overrided Properties"

    <Browsable(True), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)> _
  Public Overrides Property Text() As String
        Get
            Return MyBase.Text
        End Get
        Set(ByVal Value As String)
            MyBase.Text = Value
            Invalidate()
        End Set
    End Property

#End Region

#Region " Interface Properties "

    <Browsable(True)> _
     Public Property DialogResult() As DialogResult Implements IButtonControl.DialogResult
        Get
            Return mDialogResult
        End Get
        Set(ByVal Value As DialogResult)
            If mDialogResult <> Value Then
                mDialogResult = Value
            End If
        End Set
    End Property

#End Region

#Region "Private Methods"
    Private Sub HoverGradientButton_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.MouseEnter
        mMouseHover = True
        Invalidate()
    End Sub
    Private Sub HoverGradientButton_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.MouseLeave
        mMouseHover = False
        Invalidate()
    End Sub
    Private Sub HoverGradientButton_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseDown
        mDoClick = True
        Invalidate()
    End Sub
    Private Sub HoverGradientButton_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseUp
        Invalidate()
    End Sub

    Private Sub HoverGradientButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Click
        If mButtonType = HoverButtonType.HoverNormal Then Exit Sub
        If mButtonType = HoverButtonType.HoverOption Then
            If Not mSelected Then
                mSelected = True
                ClearOptionButtons.Clear(Me)
            End If
        Else
            mSelected = Not mSelected
        End If
    End Sub
#End Region

#Region "Protected Overrided Methods"

#Region "-------> OnPaint Overrided Method"

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        'MyBase.OnPaint(pe)

        Dim c1 As Color
        Dim c2 As Color

        If mDoClick Then
            c1 = mClickColor1
            c2 = mClickColor2
        Else
            If mMouseHover Then
                If mSelected Then
                    c1 = mSelectedHoverColor1
                    c2 = mSelectedHoverColor2
                Else
                    c1 = mHoverColor1
                    c2 = mHoverColor2
                End If
            Else
                If mSelected Then
                    c1 = mSelectedColor1
                    c2 = mSelectedColor2
                Else
                    c1 = mBackColor1
                    c2 = mBackColor2
                End If
            End If
        End If

        GS.PaintGradientRectangle(e.Graphics, _
                                              0, 0, Me.Width, Me.Height, _
                                              c1, c2, mGradientMode)

        If mDoClick Then mDoClick = False

        Dim ThisImage As Image
        If Not mImage Is Nothing Then
            ThisImage = mImage
            If mButtonType = HoverButtonType.HoverNormal Then
                ThisImage = mImage
            Else
                If mSelected Then
                    If Not (mImageSelected Is Nothing) Then
                        ThisImage = mImageSelected
                    End If
                Else
                    ThisImage = mImage
                End If
            End If


            Dim p As New Point
            p = GS.GetPointAligned(ThisImage.Size, New Point(0, 0), _
                                                   New Size(Me.Size.Width, Me.Size.Height), _
                                   5, 5, 5, 3, ImageAlign)


            Dim tempImage As Image = CType(IIf(Me.Enabled, Me.Image, GS.GetDisabledImage(Me.Image)), Image)
            e.Graphics.DrawImage(tempImage, p.X, p.Y, ThisImage.Width, ThisImage.Height)

        End If

        '
        'Text
        '
        GS.WriteText(e.Graphics, _
                              Me.Text, Me.Font, _
                              Me.Enabled, _
                              Me.TextAlign, _
                              mTextQuality, _
                              Me.ForeColor, _
                              0, 0, Me.Width, Me.Height)

        ' Draw the border
        Dim mDrawBorder As Boolean = False
        Dim cb As Color
        If mMouseHover Then
            If mBorderHover Then
                mDrawBorder = True
                cb = mBorderHoverColor
            End If
        Else
            If mSelected Then
                If mBorderSelected Then
                    mDrawBorder = True
                    cb = mBorderSelectedColor
                End If
            Else
                If mBorderNormal Then
                    mDrawBorder = True
                    cb = mBorderNormalColor
                End If
            End If
        End If

        If mDrawBorder Then GS.PaintBorder(e.Graphics, 0, 0, Me.Width, Me.Height, cb)

    End Sub

#End Region

#End Region

#Region " Interface Methods "

    Public Sub NotifyDefault(ByVal value As Boolean) Implements IButtonControl.NotifyDefault
        mIsDefaultButton = value
        Invalidate()
    End Sub

    Public Sub PerformClick() Implements IButtonControl.PerformClick
        If MyBase.CanSelect Then
            OnClick(EventArgs.Empty)
        End If
    End Sub

#End Region

#Region "Class ClearOptionButtons"

    Friend Class ClearOptionButtons

        Public Sub New()
        End Sub

        Public Shared Sub Clear(ByRef cBtn As eZeeGradientButton)

            Try

                Dim cContainer As Control = CType(cBtn, Control).Parent
                Dim cOggetti As System.Windows.Forms.Control.ControlCollection = cContainer.Controls
                Dim o As Control
                For Each o In cOggetti
                    'E' un pulsante
                    If TypeName(o) = TypeName(cBtn) Then
                        'Non � lui stesso
                        If Not o Is cBtn Then
                            'C'� qualcosa in OptionGroup
                            If Not cBtn.OptionGroup = 0 Then
                                If DirectCast(o, eZeeGradientButton).OptionGroup = cBtn.OptionGroup Then
                                    DirectCast(o, eZeeGradientButton).Selected = False
                                End If
                            End If
                        End If
                    End If
                Next

            Catch ex As Exception

            End Try


        End Sub

    End Class

#End Region

End Class


