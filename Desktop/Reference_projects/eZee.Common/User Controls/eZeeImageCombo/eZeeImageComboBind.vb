Imports System
Imports System.Drawing
Imports System.ComponentModel

Public Class eZeeImageComboBind
    Inherits Windows.Forms.ComboBox
    Private imgs As Windows.Forms.ImageList = New Windows.Forms.ImageList()

    ' constructor
    Public Sub New()
        ' set draw mode to owner draw
        Me.DrawMode = Windows.Forms.DrawMode.OwnerDrawFixed
    End Sub

    ' ImageList property
    Public Property ImageList() As Windows.Forms.ImageList
        Get
            Return imgs
        End Get
        Set(ByVal value As Windows.Forms.ImageList)
            imgs = value
        End Set
    End Property

    ' customized drawing process
    Protected Overrides Sub OnDrawItem(ByVal e As Windows.Forms.DrawItemEventArgs)
        ' draw background & focus rect
        e.DrawBackground()
        e.DrawFocusRectangle()

        ' check if it is an item from the Items collection
        If e.Index < 0 Then
            ' not an item, draw the text (indented)
            'e.Graphics.DrawString(Me.Text, e.Font, New SolidBrush(e.ForeColor), e.Bounds.Left + imgs.ImageSize.Width, e.Bounds.Top)
            e.Graphics.DrawString(Me.Text, e.Font, New SolidBrush(e.ForeColor), e.Bounds.Left + imgs.ImageSize.Width, e.Bounds.Top)
        Else

            ' check if item is an ImageComboItem
            If Me.Items(e.Index).GetType() Is GetType(ImageComboItem) Then

                ' get item to draw
                Dim item As ImageComboItem = CType(Me.Items(e.Index), ImageComboItem)

                ' get forecolor & font
                Dim forecolor As Color
                If (item.ForeColor <> Color.FromKnownColor(KnownColor.Transparent)) Then
                    forecolor = item.ForeColor
                Else
                    forecolor = e.ForeColor
                End If
                Dim font As Font
                If item.Mark Then
                    font = New Font(e.Font, FontStyle.Bold)
                Else
                    font = e.Font
                End If

                ' -1: no image
                If item.ImageIndex <> -1 Then
                    ' draw image, then draw text next to it
                    'Me.ImageList.Draw(e.Graphics, e.Bounds.Left, e.Bounds.Top, item.ImageIndex)
                    'e.Graphics.DrawString(item.Text, font, New SolidBrush(forecolor), e.Bounds.Left + imgs.ImageSize.Width, e.Bounds.Top)
                    Me.ImageList.Draw(e.Graphics, e.Bounds.Left, e.Bounds.Top, 16, 16, item.ImageIndex)
                    e.Graphics.DrawString(item.Text, font, New SolidBrush(forecolor), e.Bounds.Left + 16, e.Bounds.Top)
                Else
                    ' draw text (indented)
                    e.Graphics.DrawString(item.Text, font, New SolidBrush(forecolor), e.Bounds.Left + imgs.ImageSize.Width, e.Bounds.Top)
                End If
            Else
                ' it is not an ImageComboItem, draw it
                e.Graphics.DrawString(Me.Items(e.Index).ToString(), e.Font, New SolidBrush(e.ForeColor), e.Bounds.Left + imgs.ImageSize.Width, e.Bounds.Top)
            End If

        End If

        MyBase.OnDrawItem(e)
    End Sub
End Class

Friend Class ImageComboItem
    Inherits Object

    ' forecolor: transparent = inherit
    Private forecolor_Renamed As Color = Color.FromKnownColor(KnownColor.Transparent)
    Private mark_Renamed As Boolean = False
    Private imageindex_Renamed As Integer = -1
    Private tag_Renamed As Object = Nothing
    Private text_Renamed As String = Nothing

    ' constructors
    Public Sub New()
    End Sub

    Public Sub New(ByVal Text As String)
        Me.text_Renamed = Text
    End Sub

    Public Sub New(ByVal Text As String, ByVal ImageIndex As Integer)
        Me.text_Renamed = Text
        Me.imageindex_Renamed = ImageIndex
    End Sub

    Public Sub New(ByVal Text As String, ByVal ImageIndex As Integer, ByVal Mark As Boolean)
        Me.text_Renamed = Text
        Me.imageindex_Renamed = ImageIndex
        Me.mark_Renamed = Mark
    End Sub

    Public Sub New(ByVal Text As String, ByVal ImageIndex As Integer, ByVal Mark As Boolean, ByVal ForeColor As Color)
        Me.text_Renamed = Text
        Me.imageindex_Renamed = ImageIndex
        Me.mark_Renamed = Mark
        Me.forecolor_Renamed = ForeColor
    End Sub

    Public Sub New(ByVal Text As String, ByVal ImageIndex As Integer, ByVal Mark As Boolean, ByVal ForeColor As Color, ByVal Tag As Object)
        Me.text_Renamed = Text
        Me.imageindex_Renamed = ImageIndex
        Me.mark_Renamed = Mark
        Me.forecolor_Renamed = ForeColor
        Me.tag_Renamed = Tag
    End Sub

    ' forecolor
    Public Property ForeColor() As Color
        Get
            Return forecolor_Renamed
        End Get
        Set(ByVal value As Color)
            forecolor_Renamed = value
        End Set
    End Property

    ' image index
    Public Property ImageIndex() As Integer
        Get
            Return imageindex_Renamed
        End Get
        Set(ByVal value As Integer)
            imageindex_Renamed = value
        End Set
    End Property

    ' mark (bold)
    Public Property Mark() As Boolean
        Get
            Return mark_Renamed
        End Get
        Set(ByVal value As Boolean)
            mark_Renamed = value
        End Set
    End Property

    ' tag
    Public Property Tag() As Object
        Get
            Return tag_Renamed
        End Get
        Set(ByVal value As Object)
            tag_Renamed = value
        End Set
    End Property

    ' item text
    Public Property Text() As String
        Get
            Return text_Renamed
        End Get
        Set(ByVal value As String)
            text_Renamed = value
        End Set
    End Property

    ' ToString() should return item text
    Public Overrides Function ToString() As String
        Return text_Renamed
    End Function
End Class
