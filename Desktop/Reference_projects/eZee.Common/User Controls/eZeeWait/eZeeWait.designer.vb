
Partial Public Class eZeeWait
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If (Not m_Timer Is Nothing) Then
                m_Timer.Stop()
                RemoveHandler m_Timer.Tick, AddressOf aTimer_Tick
            End If

            If (Not components Is Nothing) Then
            components.Dispose()
        End If
        End If
        MyBase.Dispose(disposing)
    End Sub
End Class
