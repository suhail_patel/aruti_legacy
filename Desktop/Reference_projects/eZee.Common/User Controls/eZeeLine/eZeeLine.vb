Imports System
Imports System.ComponentModel
Imports System.ComponentModel.Design
Imports System.ComponentModel.Design.Serialization
Imports System.Drawing


<ToolboxItem(True)> _
<ToolboxBitmap(GetType(eZeeLine))> _
Public Class eZeeLine
    Inherits System.Windows.Forms.Label

    Private m_Orientation As Orientation = Orientation.Horizontal
    Private mCaption As String = ""
    ''' <summary>
    ''' Specifies how an object or text in a control Orientation
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum Orientation
        Horizontal = 1
        Verticle = 0
    End Enum

    ''' <summary>
    ''' Summary:
    ''' Specifies how an object or text in a control is horizontally aligned relative
    ''' to an element of the control.
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum HorizontalAlignment
        '' Summary:
        ''     The object or text is aligned on the left of the control element.
        Left = 0
        ''
        '' Summary:
        ''     The object or text is aligned on the right of the control element.
        Right = 1
        ''
        '' Summary:
        ''     The object or text is aligned in the center of the control element.
        Center = 2
    End Enum

    ''' <summary>
    ''' Summary:
    ''' Specifies how an object or text in a control is Verticlly aligned relative
    ''' to an element of the control.
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum VerticleAlignment
        Top = 0
        Middle = 1
        Bottom = 2
    End Enum

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property LineOrientation() As Orientation
        Get
            Return m_Orientation
        End Get
        Set(ByVal value As Orientation)
            m_Orientation = value
        End Set
    End Property

    ''' <summary> 
    ''' Gets or sets the gap (in pixels) between label and line. 
    ''' </summary> 
    <Category("Appearance")> _
    <Description("Gap between text and divider line.")> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(0)> _
    Public Property Gap() As Integer
        Get
            Return m_gap
        End Get
        Set(ByVal value As Integer)
            m_gap = value
            Invalidate()
        End Set
    End Property

    ''' <summary> 
    ''' Overrides <c>Label.OnPaint</c> method. 
    ''' </summary> 
    ''' <param name="e"></param> 
    Protected Overloads Overrides Sub OnPaint(ByVal e As Windows.Forms.PaintEventArgs)
        If m_Orientation = Orientation.Verticle Then
            If mCaption = "" Then mCaption = Text
            Text = ""
            Dim g As Graphics
            g = e.Graphics

            Dim br As Drawing.SolidBrush
            br = New Drawing.SolidBrush(Me.ForeColor)

            Dim fmt As New StringFormat
            If Me.TextAlign = ContentAlignment.TopCenter OrElse Me.TextAlign = ContentAlignment.MiddleLeft OrElse Me.TextAlign = ContentAlignment.MiddleRight Then
                fmt.Alignment = StringAlignment.Near
            ElseIf Me.TextAlign = ContentAlignment.BottomCenter OrElse Me.TextAlign = ContentAlignment.BottomLeft OrElse Me.TextAlign = ContentAlignment.BottomRight Then
                fmt.Alignment = StringAlignment.Far
            Else
                fmt.Alignment = StringAlignment.Center
            End If
            fmt.FormatFlags = StringFormatFlags.DirectionVertical

            g.DrawString(mCaption, Me.Font, New Drawing.SolidBrush(Me.ForeColor), New Rectangle(0, 0, Me.Height, Me.Width), fmt)
        End If
        PlaceLine(e.Graphics)
        MyBase.OnPaint(e)
    End Sub

    ''' <summary> 
    ''' Calculates points for 3D horizontal divider and places it. 
    ''' </summary> 
    ''' <param name="g"> 
    ''' <c>Graphics</c> object. 
    ''' </param> 
    Protected Sub PlaceLine(ByVal g As Graphics)
        ' evaluates text size 

        Dim x0 As Integer = 0
        ' first point x-coordinate 
        Dim x1 As Integer = Me.Width
        ' second point x-coordinate 

        Dim y0 As Integer = 0
        ' first point y-coordinate
        Dim y1 As Integer = Me.Height
        ' second point y-coordinate

        ' for different horizontal alignments recalculates x-coordinates 
        If m_Orientation = Orientation.Horizontal Then
            Dim textSize As SizeF = g.MeasureString(Me.Text, Me.Font)
            Select Case GetHorizontalAlignment()
                Case HorizontalAlignment.Left
                    x0 = CInt(textSize.Width) + m_gap
                    Exit Select
                Case HorizontalAlignment.Right
                    x1 = Me.Width - CInt(textSize.Width) - m_gap
                    Exit Select
                Case HorizontalAlignment.Center
                    x1 = (Me.Width - CInt(textSize.Width)) / 2 - m_gap
                    Exit Select
            End Select
            Dim y As Integer = CInt(textSize.Height) / 2
            ' for different vertical alignments recalculates y-coordinate 
            If TextAlign = ContentAlignment.MiddleLeft OrElse TextAlign = ContentAlignment.MiddleCenter OrElse TextAlign = ContentAlignment.MiddleRight Then
                y = Me.Height / 2
            ElseIf TextAlign = ContentAlignment.BottomLeft OrElse TextAlign = ContentAlignment.BottomCenter OrElse TextAlign = ContentAlignment.BottomRight Then
                y = Me.Height - CInt((textSize.Height / 2)) - 2
            End If

            Draw3DLine(g, x0, y, x1, y)
            ' for centered text, two line sections have to be drawn 
            If TextAlign = ContentAlignment.TopCenter OrElse TextAlign = ContentAlignment.MiddleCenter OrElse TextAlign = ContentAlignment.BottomCenter Then
                x0 = (Me.Width + CInt(textSize.Width)) / 2 + m_gap
                x1 = Me.Width
                Draw3DLine(g, x0, y, x1, y)
            End If
        Else
            Dim textSize As SizeF = g.MeasureString(mCaption, Me.Font)
            Select Case GetVerticleAlignment()
                Case VerticleAlignment.Top
                    y0 = CInt(textSize.Width) + m_gap
                    Exit Select
                Case VerticleAlignment.Bottom
                    y1 = Me.Height - CInt(textSize.Width) - m_gap
                    Exit Select
                Case VerticleAlignment.Middle
                    y1 = (Me.Height - CInt(textSize.Width)) / 2 - m_gap
                    Exit Select
            End Select

            Dim x As Integer = CInt(textSize.Height) / 2

            Draw3DLine(g, x, y0, x, y1)
            ' for centered text, two line sections have to be drawn 
            If TextAlign = ContentAlignment.MiddleCenter OrElse TextAlign = ContentAlignment.MiddleLeft OrElse TextAlign = ContentAlignment.MiddleRight Then
                y0 = (Me.Height + CInt(textSize.Width)) / 2 + m_gap
                y1 = Me.Height
                Draw3DLine(g, x, y0, x, y1)
            End If
        End If
    End Sub

    ''' <summary> 
    ''' Evaluates horizontal alignment depending on <c>TextAlign</c> and 
    ''' <c>RightToLeft</c> settings. 
    ''' </summary> 
    ''' <returns> 
    ''' One of the <c>HorizontalAlignment</c> values. 
    ''' </returns> 
    Protected Function GetHorizontalAlignment() As HorizontalAlignment
        If TextAlign = ContentAlignment.TopLeft OrElse TextAlign = ContentAlignment.MiddleLeft OrElse TextAlign = ContentAlignment.BottomLeft Then
            If RightToLeft = Windows.Forms.RightToLeft.Yes Then
                Return HorizontalAlignment.Right
            Else
                Return HorizontalAlignment.Left
            End If
        End If
        If TextAlign = ContentAlignment.TopRight OrElse TextAlign = ContentAlignment.MiddleRight OrElse TextAlign = ContentAlignment.BottomRight Then
            If RightToLeft = Windows.Forms.RightToLeft.Yes Then
                Return HorizontalAlignment.Left
            Else
                Return HorizontalAlignment.Right
            End If
        End If
        Return HorizontalAlignment.Center
    End Function

    ''' <summary>
    ''' Evaluates Verticle alignment depending on <c>TextAlign</c> and
    ''' </summary>
    ''' <returns></returns>
    ''' One of the <c>VerticleAlignment</c> values.
    ''' <remarks></remarks>
    Protected Function GetVerticleAlignment() As VerticleAlignment
        If TextAlign = ContentAlignment.TopCenter OrElse TextAlign = ContentAlignment.TopLeft OrElse TextAlign = ContentAlignment.TopRight Then
            Return VerticleAlignment.Top
        End If
        If TextAlign = ContentAlignment.BottomCenter OrElse TextAlign = ContentAlignment.BottomLeft OrElse TextAlign = ContentAlignment.BottomRight Then
            Return VerticleAlignment.Bottom
        End If
        Return VerticleAlignment.Middle
    End Function

    ''' <summary> 
    ''' Draws 3D horizontal divider line 
    ''' </summary> 
    ''' <param name="g"> 
    ''' <c>Graphics</c> object. 
    ''' </param> 
    ''' <param name="x1"> 
    ''' x-coordinate of the first point. 
    ''' </param> 
    ''' <param name="y1"> 
    ''' y-coordinate of the first point. 
    ''' </param> 
    ''' <param name="x2"> 
    ''' x-coordinate of the second point. 
    ''' </param> 
    ''' <param name="y2"> 
    ''' y-coordinate of the second point. 
    ''' </param> 
    Protected Sub Draw3DLine(ByVal g As Graphics, ByVal x1 As Integer, ByVal y1 As Integer, ByVal x2 As Integer, ByVal y2 As Integer)
        g.DrawLine(SystemPens.ControlDark, x1, y1, x2, y2)
        g.DrawLine(SystemPens.ControlLightLight, x1, y1 + 1, x2, y2 + 1)
    End Sub

    Private m_gap As Integer
End Class
