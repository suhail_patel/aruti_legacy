Imports System
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Windows.Forms


Public Class eZeeMainHeader
    Inherits System.Windows.Forms.UserControl

    '************************************************************** 
    ' static properties 
    ' ************************************************************** 

    Private _ForeColor As Color = Color.White

    Public Shared ReadOnly DefaultMessageFontStyle As System.Drawing.FontStyle = System.Drawing.FontStyle.Regular
    Public Shared ReadOnly DefaultTitleFontStyle As System.Drawing.FontStyle = System.Drawing.FontStyle.Bold
    Public Shared ReadOnly DefaultBoundrySize As Integer = 15

    Private _strTitle As String = [String].Empty
    Private _strMessage As String = [String].Empty
    Private _messageFont As Font
    Private _titleFont As Font
    Private _icon As Icon = Nothing
    Private _image As Image = Nothing
    Private _iBoundrySize As Integer = DefaultBoundrySize
    Private _titleFontStyle As System.Drawing.FontStyle = DefaultTitleFontStyle
    Private _messageFontStyle As System.Drawing.FontStyle = DefaultMessageFontStyle
    Private _drawTextWorkaroundAppendString As String = New String(" "c, 10000) + "."
    Private _textStartPoint As New Point(DefaultBoundrySize, DefaultBoundrySize)




    '************************************************************** 
    ' public properties 
    ' ************************************************************** 

    Public Property Message() As String
        Get
            Return _strMessage
        End Get
        Set(ByVal value As String)
            _strMessage = value
            Invalidate()
        End Set
    End Property
    Public Property Title() As String
        Get
            Return _strTitle
        End Get
        Set(ByVal value As String)
            _strTitle = value
            Invalidate()
        End Set
    End Property
    Public Property Icon() As Icon
        Get
            Return Me._icon
        End Get
        Set(ByVal value As Icon)
            Me._icon = value
            Invalidate()
        End Set
    End Property
    Public Property Image() As Image
        Get
            Return Me._image
        End Get
        Set(ByVal value As Image)
            Me._image = value
            Invalidate()
        End Set
    End Property
    Public Property TitleFontStyle() As System.Drawing.FontStyle
        Get
            Return Me._titleFontStyle
        End Get
        Set(ByVal value As System.Drawing.FontStyle)
            Me._titleFontStyle = value
            Me.CreateTitleFont()
            Invalidate()
        End Set
    End Property

    Public Property MessageFontStyle() As System.Drawing.FontStyle
        Get
            Return Me._messageFontStyle
        End Get
        Set(ByVal value As System.Drawing.FontStyle)
            Me._messageFontStyle = value
            Me.CreateMessageFont()
            Invalidate()
        End Set
    End Property
    Public Property BoundrySize() As Integer
        Get
            Return _iBoundrySize
        End Get
        Set(ByVal value As Integer)
            _iBoundrySize = value
            Invalidate()
        End Set
    End Property

    Public Property TextStartPosition() As Point
        Get
            Return _textStartPoint
        End Get
        Set(ByVal value As Point)
            _textStartPoint = value
            Invalidate()
        End Set
    End Property




    '************************************************************** 
    ' newly implemented/overridden public properties 
    ' ************************************************************** 

    'Public Shadows ReadOnly Property BackgroundImage() As Image
    '    Get
    '        Return Nothing
    '    End Get
    'End Property

    Public Shadows Property Anchor() As AnchorStyles
        Get
            Return System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right Or System.Windows.Forms.AnchorStyles.Top
        End Get
        Set(ByVal value As AnchorStyles)

        End Set
    End Property

    'only allow black foregound and white background 
    Public Shadows Property ForeColor() As Color
        Get
            Return _ForeColor
        End Get
        Set(ByVal value As Color)
            _ForeColor = value
            Me.Invalidate()
        End Set
    End Property

    Public Shadows ReadOnly Property BackColor() As Color
        Get
            Return Color.White
        End Get
    End Property

    '************************************************************** 
    ' drawing stuff 
    ' ************************************************************** 

    Protected Sub CreateTitleFont()
        Me._titleFont = New Font(Me.Font.FontFamily, Me.Font.Size, Me._titleFontStyle)
    End Sub
    Protected Sub CreateMessageFont()
        Me._messageFont = New Font(Me.Font.FontFamily, Me.Font.Size, Me._messageFontStyle)
    End Sub

    Protected Sub Draw3dLine(ByVal g As System.Drawing.Graphics)
        ControlPaint.DrawBorder3D(g, 0, Me.Height, Me.Width, 0, System.Windows.Forms.Border3DStyle.RaisedInner)
    End Sub

    Protected Sub DrawTitle(ByVal g As System.Drawing.Graphics)
        ' Normally the next line should work fine 
        ' but the spacing of the characters at the end of the string is smaller than at the beginning 
        ' therefore we add _drawTextWorkaroundAppendString to the string to be drawn 
        ' this works fine 
        ' 
        ' i reported this behaviour to microsoft. they confirmed this is a bug in GDI+. 
        ' 
        ' g.DrawString( this._strTitle, this._titleFont, new SolidBrush(Color.Black), BoundrySize, BoundrySize); //BoundrySize is used as the x & y coords 
        'g.DrawString(Me._strTitle + _drawTextWorkaroundAppendString, Me._titleFont, New SolidBrush(Color.Black), Me.TextStartPosition)
        g.DrawString(Me._strTitle + _drawTextWorkaroundAppendString, Me._titleFont, New SolidBrush(Me._ForeColor), Me.TextStartPosition.X, Me.TextStartPosition.Y - 10)
    End Sub

    Protected Sub DrawMessage(ByVal g As System.Drawing.Graphics)
        'calculate the new startpoint 
        Dim iNewPosY As Integer = Me.TextStartPosition.Y + Me.Font.Height * 3 \ 2 - 12
        Dim iNewPosX As Integer = Me.TextStartPosition.X + Me.Font.Height * 3 \ 2
        Dim iTextBoxWidth As Integer = Me.Width - iNewPosX
        Dim iTextBoxHeight As Integer = Me.Height - iNewPosY

        If Me._icon IsNot Nothing Then
            iTextBoxWidth -= (BoundrySize + _icon.Width)
        ElseIf Me._image IsNot Nothing Then
            iTextBoxWidth -= (BoundrySize + _image.Width)
            ' subtract the width of the icon and the boundry size again 
        End If
        ' subtract the width of the icon and the boundry size again 
        Dim rect As New Rectangle(iNewPosX, iNewPosY, iTextBoxWidth, iTextBoxHeight)
        'g.DrawString(Me._strMessage, Me._messageFont, New SolidBrush(Color.Black), rect)
        g.DrawString(Me._strMessage, Me._messageFont, New SolidBrush(Me._ForeColor), rect)
    End Sub

    Protected Sub DrawImage(ByVal g As System.Drawing.Graphics)
        If Me._image Is Nothing Then
            Return
        End If
        g.DrawImage(Me._image, Me.Width - Me._image.Width - BoundrySize, (Me.Height - Me._image.Height) \ 2)
    End Sub
    Protected Sub DrawIcon(ByVal g As System.Drawing.Graphics)
        If Me._icon Is Nothing Then
            Return
        End If
        g.DrawIcon(Me._icon, Me.Width - Me._icon.Width - BoundrySize, (Me.Height - Me._icon.Height) \ 2)
    End Sub
    Protected Overridable Sub DrawBackground(ByVal g As System.Drawing.Graphics)
        g.FillRectangle(New SolidBrush(Me.BackColor), 0, 0, Me.Width, Me.Height)
    End Sub


    '************************************************************** 
    ' overridden methods 
    ' ************************************************************** 

    Protected Overloads Overrides Sub OnFontChanged(ByVal e As EventArgs)
        Me.CreateTitleFont()
        MyBase.OnFontChanged(e)
    End Sub


    Protected Overloads Overrides Sub OnPaintBackground(ByVal pevent As PaintEventArgs)
    End Sub


    Protected Overloads Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        Me.DrawBackground(e.Graphics)
        Me.Draw3dLine(e.Graphics)
        Me.DrawTitle(e.Graphics)
        Me.DrawMessage(e.Graphics)
        If Me._icon IsNot Nothing Then
            Me.DrawIcon(e.Graphics)
        ElseIf Me._image IsNot Nothing Then
            Me.DrawImage(e.Graphics)
        End If
    End Sub


    Protected Overloads Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        Invalidate()
        MyBase.OnSizeChanged(e)
    End Sub

    Private Sub eZeeMainHeader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

        Me.Size = New Size(10, 70)
        'header height of 70 does not look bad 
        Me.Dock = Windows.Forms.DockStyle.Top
        Me.CreateTitleFont()
        Me.CreateMessageFont()
    End Sub

End Class


'****************************************************************************************************************************** 
' ColorSlideFormHeader is an extended version of the FormHeader class 
' It also provides the functionality of a color slide of the background image 
' ****************************************************************************************************************************** 

Public Class eZeeGradientHeader
    Inherits eZeeMainHeader
    Public Shared ReadOnly DefaultColor1 As Color = Color.White
    Public Shared ReadOnly DefaultColor2 As Color = Color.White

    Private _color1 As Color = DefaultColor1
    Private _color2 As Color = DefaultColor2
    Private _image As Image = Nothing

    Public Sub New()
        CreateBackgroundPicture()
    End Sub


    Protected Overridable Sub CreateBackgroundPicture()
        Try
            _image = New Bitmap(Me.Width, Me.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb)
        Catch
            Return
        End Try

        Dim gfx As Graphics = Graphics.FromImage(_image)

        If Me._color1.Equals(Me._color2) Then
            'check if we need to calc the color slide 
            gfx.FillRectangle(New SolidBrush(Me._color1), 0, 0, Me.Width, Me.Height)
        Else
            For i As Integer = 0 To _image.Width - 1
                ' 
                ' calculate the new color to use (linear color mix) 
                ' 
                Dim colorR As Integer = CInt((Me.Color2.R - Me.Color1.R)) * i / _image.Width
                Dim colorG As Integer = CInt((Me.Color2.G - Me.Color1.G)) * i / _image.Width
                Dim colorB As Integer = CInt((Me.Color2.B - Me.Color1.B)) * i / _image.Width
                Dim color As Color = Drawing.Color.FromArgb(Me.Color1.R + colorR, Me.Color1.G + colorG, Me.Color1.B + colorB)

                gfx.DrawLine(New Pen(New SolidBrush(color)), i, 0, i, Me.Height)
            Next
        End If
    End Sub


    Public Property Color1() As Color
        Get
            Return Me._color1
        End Get
        Set(ByVal value As Color)
            Me._color1 = value
            CreateBackgroundPicture()
            Invalidate()
        End Set
    End Property


    Public Property Color2() As Color
        Get
            Return Me._color2
        End Get
        Set(ByVal value As Color)
            Me._color2 = value
            CreateBackgroundPicture()
            Invalidate()
        End Set
    End Property


    Protected Overloads Overrides Sub DrawBackground(ByVal g As Graphics)
        'g.DrawImage(Me._image, 0, 0)
        g.DrawImageUnscaledAndClipped(Me._image, New Rectangle(0, 0, Me.Width, Me.Height))
    End Sub


    Protected Overloads Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        CreateBackgroundPicture()
        MyBase.OnSizeChanged(e)
        Invalidate()
    End Sub
End Class

Public Class eZeeImageHeader
    Inherits eZeeMainHeader

    Private _backgroundImage As Image


    Public Shadows Property BackgroundImage() As Image
        Get
            Return Me._backgroundImage
        End Get
        Set(ByVal value As Image)
            Me._backgroundImage = value
            MyBase.BackgroundImage = Me._backgroundImage
            MyBase.BackgroundImageLayout = ImageLayout.Tile
            Invalidate()
        End Set
    End Property

    Protected Sub DrawBackgroundImage(ByVal g As System.Drawing.Graphics)
        If Me._backgroundImage Is Nothing Then
            Return
        End If
        Select Case Me.BackgroundImageLayout
            Case ImageLayout.Tile
                Dim i As Integer = 0
                Dim j As Integer = 0
                For i = 0 To Me.Width Step Me._backgroundImage.Width
                    For j = 0 To Me.Height Step Me._backgroundImage.Height
                        g.DrawImage(Me._backgroundImage, i, j)
                    Next
                Next
            Case ImageLayout.None
                g.DrawImage(Me._backgroundImage, 0, 0)
            Case ImageLayout.Stretch
                g.DrawImageUnscaledAndClipped(Me._backgroundImage, New Rectangle(0, 0, Me.Width, Me.Height))
            Case ImageLayout.Center
                g.DrawImage(Me._backgroundImage, CInt(Me.Width / 2 - Me._backgroundImage.Width / 2), CInt(Me.Height / 2 - Me._backgroundImage.Height / 2))

        End Select

    End Sub

    Protected Overloads Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        Me.DrawBackground(e.Graphics)

        Me.DrawBackgroundImage(e.Graphics)
        Me.Draw3dLine(e.Graphics)
        Me.DrawTitle(e.Graphics)
        Me.DrawMessage(e.Graphics)
        If Me.Icon IsNot Nothing Then
            Me.DrawIcon(e.Graphics)
        ElseIf Me.Image IsNot Nothing Then
            Me.DrawImage(e.Graphics)
        End If
    End Sub
End Class
