Imports System
Imports System.Collections.Generic
Imports System.Text



Public Class TreeGridNodeEventBase
    Private _node As ExpandableGridViewNode

    Public Sub New(ByVal node As ExpandableGridViewNode)
        Me._node = node
    End Sub

    Public ReadOnly Property Node() As ExpandableGridViewNode
        Get
            Return _node
        End Get
    End Property
End Class

Public Class CollapsingEventArgs
    Inherits System.ComponentModel.CancelEventArgs
    Private _node As ExpandableGridViewNode

    Private Sub New()
    End Sub
    Public Sub New(ByVal node As ExpandableGridViewNode)
        MyBase.New()
        Me._node = node
    End Sub
    Public ReadOnly Property Node() As ExpandableGridViewNode
        Get
            Return _node
        End Get
    End Property

End Class

Public Class CollapsedEventArgs
    Inherits TreeGridNodeEventBase
    Public Sub New(ByVal node As ExpandableGridViewNode)
        MyBase.New(node)
    End Sub
End Class

Public Class ExpandingEventArgs
    Inherits System.ComponentModel.CancelEventArgs
    Private _node As ExpandableGridViewNode

    Private Sub New()
    End Sub
    Public Sub New(ByVal node As ExpandableGridViewNode)
        MyBase.New()
        Me._node = node
    End Sub
    Public ReadOnly Property Node() As ExpandableGridViewNode
        Get
            Return _node
        End Get
    End Property

End Class

Public Class ExpandedEventArgs
    Inherits TreeGridNodeEventBase
    Public Sub New(ByVal node As ExpandableGridViewNode)
        MyBase.New(node)
    End Sub
End Class

Public Delegate Sub ExpandingEventHandler(ByVal sender As Object, ByVal e As ExpandingEventArgs)
Public Delegate Sub ExpandedEventHandler(ByVal sender As Object, ByVal e As ExpandedEventArgs)

Public Delegate Sub CollapsingEventHandler(ByVal sender As Object, ByVal e As CollapsingEventArgs)
Public Delegate Sub CollapsedEventHandler(ByVal sender As Object, ByVal e As CollapsedEventArgs)

