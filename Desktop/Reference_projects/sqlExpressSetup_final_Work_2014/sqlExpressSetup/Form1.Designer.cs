namespace sqlExpressSetup
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblInstanceName = new System.Windows.Forms.Label();
            this.gbConfigurationSetup = new System.Windows.Forms.GroupBox();
            this.lblCaption = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblServerName = new System.Windows.Forms.Label();
            this.txtServerAddress = new System.Windows.Forms.TextBox();
            this.radNotInstallSQL = new System.Windows.Forms.RadioButton();
            this.txtSAPassword = new System.Windows.Forms.TextBox();
            this.lblSAPassword = new System.Windows.Forms.Label();
            this.txtInstanceName = new System.Windows.Forms.TextBox();
            this.radSQLwithCustomPwd = new System.Windows.Forms.RadioButton();
            this.radDefaultInstallation = new System.Windows.Forms.RadioButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.pgbInstall = new System.Windows.Forms.ProgressBar();
            this.lblTest = new System.Windows.Forms.Label();
            this.tmrtimer1 = new System.Windows.Forms.Timer(this.components);
            this.gbConfigurationSetup.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblInstanceName
            // 
            this.lblInstanceName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInstanceName.Location = new System.Drawing.Point(284, 29);
            this.lblInstanceName.Name = "lblInstanceName";
            this.lblInstanceName.Size = new System.Drawing.Size(99, 27);
            this.lblInstanceName.TabIndex = 5;
            this.lblInstanceName.Text = "Instance Name";
            this.lblInstanceName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbConfigurationSetup
            // 
            this.gbConfigurationSetup.Controls.Add(this.lblCaption);
            this.gbConfigurationSetup.Controls.Add(this.tableLayoutPanel1);
            this.gbConfigurationSetup.Controls.Add(this.btnOk);
            this.gbConfigurationSetup.Location = new System.Drawing.Point(12, 24);
            this.gbConfigurationSetup.Name = "gbConfigurationSetup";
            this.gbConfigurationSetup.Size = new System.Drawing.Size(550, 155);
            this.gbConfigurationSetup.TabIndex = 1;
            this.gbConfigurationSetup.TabStop = false;
            this.gbConfigurationSetup.Text = "SQL Configuration";
            // 
            // lblCaption
            // 
            this.lblCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblCaption.Location = new System.Drawing.Point(10, 125);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(383, 15);
            this.lblCaption.TabIndex = 0;
            this.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 279F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.tableLayoutPanel1.Controls.Add(this.lblServerName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtServerAddress, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.radNotInstallSQL, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtSAPassword, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblInstanceName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblSAPassword, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtInstanceName, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.radSQLwithCustomPwd, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radDefaultInstallation, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 28);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(529, 85);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // lblServerName
            // 
            this.lblServerName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblServerName.Location = new System.Drawing.Point(284, 1);
            this.lblServerName.Name = "lblServerName";
            this.lblServerName.Size = new System.Drawing.Size(99, 27);
            this.lblServerName.TabIndex = 3;
            this.lblServerName.Text = "Server Address";
            this.lblServerName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtServerAddress
            // 
            this.txtServerAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtServerAddress.Location = new System.Drawing.Point(390, 4);
            this.txtServerAddress.Name = "txtServerAddress";
            this.txtServerAddress.Size = new System.Drawing.Size(139, 21);
            this.txtServerAddress.TabIndex = 4;
            // 
            // radNotInstallSQL
            // 
            this.radNotInstallSQL.AutoSize = true;
            this.radNotInstallSQL.Checked = true;
            this.radNotInstallSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radNotInstallSQL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radNotInstallSQL.Location = new System.Drawing.Point(4, 4);
            this.radNotInstallSQL.Name = "radNotInstallSQL";
            this.radNotInstallSQL.Size = new System.Drawing.Size(273, 21);
            this.radNotInstallSQL.TabIndex = 1;
            this.radNotInstallSQL.TabStop = true;
            this.radNotInstallSQL.Text = "Install Aruti for Existing MS SQL Server";
            this.radNotInstallSQL.UseVisualStyleBackColor = true;
            this.radNotInstallSQL.CheckedChanged += new System.EventHandler(this.radNotInstallSQL_CheckedChanged);
            // 
            // txtSAPassword
            // 
            this.txtSAPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSAPassword.Location = new System.Drawing.Point(390, 60);
            this.txtSAPassword.Name = "txtSAPassword";
            this.txtSAPassword.PasswordChar = '*';
            this.txtSAPassword.Size = new System.Drawing.Size(139, 21);
            this.txtSAPassword.TabIndex = 8;
            this.txtSAPassword.Tag = "pRofessionalaRuti999";
            // 
            // lblSAPassword
            // 
            this.lblSAPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSAPassword.Location = new System.Drawing.Point(284, 57);
            this.lblSAPassword.Name = "lblSAPassword";
            this.lblSAPassword.Size = new System.Drawing.Size(99, 27);
            this.lblSAPassword.TabIndex = 7;
            this.lblSAPassword.Text = "SA Password";
            this.lblSAPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtInstanceName
            // 
            this.txtInstanceName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtInstanceName.Enabled = false;
            this.txtInstanceName.Location = new System.Drawing.Point(390, 32);
            this.txtInstanceName.Name = "txtInstanceName";
            this.txtInstanceName.Size = new System.Drawing.Size(139, 21);
            this.txtInstanceName.TabIndex = 6;
            this.txtInstanceName.Text = "aPayroll";
            // 
            // radSQLwithCustomPwd
            // 
            this.radSQLwithCustomPwd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSQLwithCustomPwd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radSQLwithCustomPwd.Location = new System.Drawing.Point(4, 60);
            this.radSQLwithCustomPwd.Name = "radSQLwithCustomPwd";
            this.radSQLwithCustomPwd.Size = new System.Drawing.Size(273, 21);
            this.radSQLwithCustomPwd.TabIndex = 2;
            this.radSQLwithCustomPwd.Text = "Default Installation with Custom MS SQL Password";
            this.radSQLwithCustomPwd.UseVisualStyleBackColor = true;
            this.radSQLwithCustomPwd.Visible = false;
            this.radSQLwithCustomPwd.CheckedChanged += new System.EventHandler(this.radSQLwithCustomPwd_CheckedChanged);
            // 
            // radDefaultInstallation
            // 
            this.radDefaultInstallation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radDefaultInstallation.Location = new System.Drawing.Point(4, 32);
            this.radDefaultInstallation.Name = "radDefaultInstallation";
            this.radDefaultInstallation.Size = new System.Drawing.Size(273, 21);
            this.radDefaultInstallation.TabIndex = 0;
            this.radDefaultInstallation.Text = "Default Installation";
            this.radDefaultInstallation.UseVisualStyleBackColor = true;
            this.radDefaultInstallation.CheckedChanged += new System.EventHandler(this.radDefaultInstallation_CheckedChanged);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(399, 119);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(140, 27);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "Continue";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // pgbInstall
            // 
            this.pgbInstall.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pgbInstall.Location = new System.Drawing.Point(0, 190);
            this.pgbInstall.MarqueeAnimationSpeed = 50;
            this.pgbInstall.Name = "pgbInstall";
            this.pgbInstall.Size = new System.Drawing.Size(574, 16);
            this.pgbInstall.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbInstall.TabIndex = 2;
            // 
            // lblTest
            // 
            this.lblTest.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTest.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblTest.Location = new System.Drawing.Point(0, 0);
            this.lblTest.Name = "lblTest";
            this.lblTest.Size = new System.Drawing.Size(574, 21);
            this.lblTest.TabIndex = 0;
            this.lblTest.Text = "Database Server Configuration Setup";
            this.lblTest.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrtimer1
            // 
            this.tmrtimer1.Enabled = true;
            this.tmrtimer1.Tick += new System.EventHandler(this.tmrtimer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 206);
            this.Controls.Add(this.pgbInstall);
            this.Controls.Add(this.gbConfigurationSetup);
            this.Controls.Add(this.lblTest);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SQL Installation";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbConfigurationSetup.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblInstanceName;
        private System.Windows.Forms.GroupBox gbConfigurationSetup;
        private System.Windows.Forms.TextBox txtSAPassword;
        private System.Windows.Forms.Label lblSAPassword;
        private System.Windows.Forms.TextBox txtInstanceName;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ProgressBar pgbInstall;
        private System.Windows.Forms.Label lblTest;
        private System.Windows.Forms.Timer tmrtimer1;
        private System.Windows.Forms.RadioButton radDefaultInstallation;
        private System.Windows.Forms.RadioButton radSQLwithCustomPwd;
        private System.Windows.Forms.RadioButton radNotInstallSQL;
        private System.Windows.Forms.TextBox txtServerAddress;
        private System.Windows.Forms.Label lblServerName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblCaption;
    }
}

