Imports System.Runtime.InteropServices
Imports Microsoft.Win32
Imports System
Imports System.IO

Module modGlobal

    Private Declare Function GetShortPathName Lib "kernel32" Alias "GetShortPathNameA" (ByVal longPath As String, ByVal shortPath As String, ByVal shortBufferSize As Int32) As Int32
    Private Const conREG_NODE As String = "Software\NPK\Aruti"
    Private mstrAppPath As String = ""
    Private Const MF_BYCOMMAND As Integer = &H0
    Public Const SC_CLOSE As Integer = &HF060
    Private mstrCommandsExec As String = String.Empty
    Dim strFile As String = ""
    Dim strDestPath As String = ""
    'Sandeep [15 Feb 2014] -- Start
    'ENHANCEMENT : Requested by Rutta
    Dim mstrOpenExeName As String = String.Empty
    'Sandeep [15 Feb 2014 ] -- End


    <DllImport("user32.dll")> _
    Public Function DeleteMenu(ByVal hMenu As IntPtr, ByVal nPosition As Integer, ByVal wFlags As Integer) As Integer
    End Function

    <DllImport("user32.dll")> _
    Private Function GetSystemMenu(ByVal hWnd As IntPtr, ByVal bRevert As Boolean) As IntPtr
    End Function

    <DllImport("kernel32.dll", ExactSpelling:=True)> _
    Private Function GetConsoleWindow() As IntPtr
    End Function

    Sub Main(ByVal Args As String())
        Try
        If Args.Length > 0 Then
                DeleteMenu(GetSystemMenu(GetConsoleWindow(), False), SC_CLOSE, MF_BYCOMMAND)
                'Sandeep [15 Feb 2014] -- Start
                'ENHANCEMENT : Requested by Rutta
                Dim iRunFile As String = String.Empty
                'Sandeep [15 Feb 2014 ] -- End

                mstrAppPath = getValue()

                mstrCommandsExec = ""

                'Sandeep [15 Feb 2014] -- Start
                'ENHANCEMENT : Requested by Rutta
                Try
                    mstrOpenExeName = Args(1)
                    mstrOpenExeName = GetShortPath(mstrOpenExeName)
                    iRunFile = "START " & mstrOpenExeName
                Catch ex As Exception
                    mstrOpenExeName = ""
                End Try
                'Sandeep [15 Feb 2014 ] -- End

                strFile = GetShortPath(Args(0))
                strDestPath = GetShortPath(mstrAppPath)
                strFile = strFile.ToUpper
                strFile = strFile.Substring(0, strFile.LastIndexOf(".CAB") + 4)
                strDestPath = strDestPath.Substring(0, strDestPath.LastIndexOf("\") + 1)

                'Sandeep [15 Feb 2014] -- Start
                'ENHANCEMENT : Requested by Rutta
                'mstrCommandsExec = "TaskKill /F /IM ArutiUpdate.exe " & vbCrLf & _
                '                   "IF exist " & strDestPath & "tempold ( echo ) ELSE ( mkdir " & strDestPath & "tempold ) " & vbCrLf & _
                '                   "COPY /V " & strDestPath & "" & Space(2) & " " & strDestPath & "tempold /Y" & vbCrLf & _
                '                   "expand " & strFile & " -F:* " & strDestPath.Substring(0, strDestPath.Length - 1) & vbCrLf & _
                '                   "EXIT "
                mstrCommandsExec = "TaskKill /F /IM ArutiUpdate.exe " & vbCrLf & _
                                   "IF exist " & strDestPath & "tempold ( echo ) ELSE ( mkdir " & strDestPath & "tempold ) " & vbCrLf & _
                                   "COPY /V " & strDestPath & "" & Space(2) & " " & strDestPath & "tempold /Y" & vbCrLf & _
                                   "expand " & strFile & " -F:* " & strDestPath.Substring(0, strDestPath.Length - 1) & vbCrLf & _
                                   iRunFile & " " & vbCrLf & _
                                   " PAUSE "
                'Sandeep [15 Feb 2014 ] -- End

                IO.File.WriteAllText(My.Computer.FileSystem.SpecialDirectories.Temp & "\ArutiTemp.bat", mstrCommandsExec)
                mstrCommandsExec = My.Computer.FileSystem.SpecialDirectories.Temp & "\ArutiTemp.bat"

                Shell(mstrCommandsExec, AppWinStyle.NormalFocus, True)

                If IO.File.Exists(My.Computer.FileSystem.SpecialDirectories.Temp & "\ArutiTemp.bat") = True Then
                    IO.File.Delete(My.Computer.FileSystem.SpecialDirectories.Temp & "\ArutiTemp.bat")
                End If

            End If
        Catch ex As Exception
            If System.IO.File.Exists(mstrAppPath & "UpdateError.txt") Then
                System.IO.File.AppendAllText(mstrAppPath & "UpdateError.txt", Now & " -> " & ex.Message)
            Else
                System.IO.File.Create(mstrAppPath & "UpdateError.txt")
                System.IO.File.AppendAllText(mstrAppPath & "UpdateError.txt", Now & " -> " & ex.Message)
            End If
            mstrCommandsExec = "COPY /V " & strDestPath & "tempold" & Space(2) & strDestPath & "  /Y"
            IO.File.WriteAllText(My.Computer.FileSystem.SpecialDirectories.Temp & "\ArutiTemp.bat", mstrCommandsExec)
            mstrCommandsExec = My.Computer.FileSystem.SpecialDirectories.Temp & "\ArutiTemp.bat"
            Shell(mstrCommandsExec, AppWinStyle.Hide, True)
        Finally
            If IO.File.Exists(My.Computer.FileSystem.SpecialDirectories.Temp & "\ArutiTemp.bat") = True Then
                IO.File.Delete(My.Computer.FileSystem.SpecialDirectories.Temp & "\ArutiTemp.bat")
        End If
        End Try
    End Sub

    Public Function getValue() As String
        Dim Key As RegistryKey = Nothing
        Dim Value As String = String.Empty
        Try
            'opens the given subkey
            Key = Registry.LocalMachine.OpenSubKey(conREG_NODE, True)
            If Key Is Nothing Then
                'if Key doesn't exist then throw an exception
                Throw New Exception("Given Subkey not exist.")
            End If

            'Gets the value
            Value = Key.GetValue("AppPath").ToString

            Return Value
        Catch e As Exception
            e.ToString()
            Return ""
        End Try
    End Function

    Private Function GetShortPath(ByVal strLongPath As String) As String
        Dim intLongPathLength As Int32
       
        'If strLongPath.StartsWith("\\") = True Then
        '    intLongPathLength = strLongPath.Length + 1
        'Else
        '    intLongPathLength = strLongPath.Length
        'End If
        intLongPathLength = strLongPath.Length + 1

        '
        'A string with a buffer to receive the short path from the api call�
        Dim strShortPath As String = Space(intLongPathLength)
        '
        'Will hold the return value of the api call which should be the length.
        Dim returnValue As Long
        '
        'Now call the function to do the conversion�
        returnValue = GetShortPathName(strLongPath, strShortPath, strShortPath.Length)

        'strShortPath = strShortPath.ToString

        Return strShortPath.ToString

    End Function

    'TaskKill /F /IM Aruti.exe
    'TaskKill /F /IM ArutiConfiguration.exe
    'TaskKill /F /IM ArutiTimeSheet.exe
    'TaskKill /F /IM ArutiAutoRecruit.exe

End Module
