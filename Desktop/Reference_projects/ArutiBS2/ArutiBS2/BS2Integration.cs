﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Suprema;
using System.Runtime.InteropServices;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;

namespace ArutiBS2
{
    public class BS2Integration
    {
        public bool IsConnected(string xDeviceIP, UInt16 xPortNo, ref uint xDeviceID, ref string strErrorMsg)
        {
            BS2ErrorCode result;
            IntPtr context = IntPtr.Zero;
            try
            {
                IntPtr sVersion = API.BS2_Version();

                context = API.BS2_AllocateContext();
                if (context == IntPtr.Zero)
                {
                    strErrorMsg = "Context Failed";
                    return false;
                }

                result = (BS2ErrorCode)API.BS2_Initialize(context);
                if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                {
                    strErrorMsg = "BS2_Initialization Failed. Error : " + result;
                    API.BS2_ReleaseContext(context);
                    return false;
                }

                if (xPortNo <= 0)
                {
                    xPortNo = BS2Envirionment.BS2_TCP_DEVICE_PORT_DEFAULT;
                }


                result = (BS2ErrorCode)API.BS2_ConnectDeviceViaIP(context, xDeviceIP, xPortNo, out xDeviceID);
                if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                {
                    strErrorMsg = "BS2_ConnectDeviceViaIP Failed. Error : " + result;
                    return false;
                }

                result = (BS2ErrorCode)API.BS2_DisconnectDevice(context, xDeviceID);
                if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                {
                    strErrorMsg = "BS2_DisconnectDevice Failed : " + result;
                    return false;
                }
                System.Threading.Thread.Sleep(500);

            }
            catch (Exception ex)
            {
                result = (BS2ErrorCode)API.BS2_DisconnectDevice(context, xDeviceID);
                if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                {
                    strErrorMsg = "BS2_DisconnectDevice Failed : " + result;
                    return false;
                }
                System.Threading.Thread.Sleep(500);
                throw new Exception("IsConnected :- " + ex.Message.ToString());
            }
            return true;
        }

        public DataTable GetUserLog(string xDeviceIP, ushort xPortNo, DateTime StartDate, DateTime EndDate, ref uint xDeviceID, ref string strErrorMsg)
        {
            DataTable dtUserLog = new DataTable();
            BS2ErrorCode result;
            IntPtr context = IntPtr.Zero;
            try
            {

                context = API.BS2_AllocateContext();
                if (context == IntPtr.Zero)
                {
                    strErrorMsg = "Context Failed";
                    return dtUserLog;
                }


                result = (BS2ErrorCode)API.BS2_Initialize(context);
                if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                {
                    strErrorMsg = "BS2_Initialization Failed. Error : " + result;
                    API.BS2_ReleaseContext(context);
                    return dtUserLog;
                }


                if (xPortNo <= 0)
                {
                    xPortNo = BS2Envirionment.BS2_TCP_DEVICE_PORT_DEFAULT;
                }

                result = (BS2ErrorCode)API.BS2_ConnectDeviceViaIP(context, xDeviceIP, xPortNo, out xDeviceID);
                if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                {
                    strErrorMsg = "BS2_ConnectDeviceViaIP Failed. Error : " + result;
                    return dtUserLog;
                }
                else
                {
                    Type structureType = typeof(BS2Event);
                    int structSize = Marshal.SizeOf(structureType);
                    IntPtr uid = IntPtr.Zero;
                    UInt16 eventCode = 0;
                    byte tnaKey = 0;
                    IntPtr outEventLogObjs = IntPtr.Zero;
                    UInt32 outNumEventLogs = 0;

                    //eventCode = (UInt16)BS2EventCodeEnum.IDENTIFY_SUCCESS | (UInt16)BS2EventCodeEnum.IDENTIFY_SUCCESS_FACE | (UInt16)BS2EventCodeEnum.IDENTIFY_SUCCESS_FACE_PIN |
                    //                   (UInt16)BS2EventCodeEnum.IDENTIFY_SUCCESS_FINGER | (UInt16)BS2EventCodeEnum.IDENTIFY_SUCCESS_FINGER_PIN | (UInt16)BS2EventCodeEnum.IDENTIFY_DURESS |
                    //                   (UInt16)BS2EventCodeEnum.IDENTIFY_DURESS_FACE | (UInt16)BS2EventCodeEnum.IDENTIFY_DURESS_FACE_PIN | (UInt16)BS2EventCodeEnum.IDENTIFY_DURESS_FINGER |
                    //                   (UInt16)BS2EventCodeEnum.IDENTIFY_DURESS_FINGER_PIN;

                    eventCode = (UInt16)BS2EventCodeEnum.IDENTIFY_SUCCESS;

                    UInt32 start = Convert.ToUInt32(Util.ConvertToUnixTimestamp(StartDate.Date));
                    UInt32 end = Convert.ToUInt32(Util.ConvertToUnixTimestamp(EndDate.Date));

                    //tnaKey = Util.GetInput(0);

                    //if (tnaKey > BS2Envirionment.BS2_MAX_TNA_KEY)
                    //{
                    //    strErrorMsg = "Invalid tnaKey :" + tnaKey;
                    //    return dtUserLog;
                    //}

                    //byte[] uidArray = Encoding.ASCII.GetBytes("1082");
                    //byte[] outUidArray = new byte[BS2Envirionment.BS2_USER_ID_SIZE];

                    //uid = Marshal.AllocHGlobal(BS2Envirionment.BS2_USER_ID_SIZE);
                    //for (int idx = 0; idx < BS2Envirionment.BS2_USER_ID_SIZE; idx++)
                    //{
                    //    outUidArray[idx] = 0;
                    //}

                    //Array.Copy(uidArray, outUidArray, uidArray.Length);
                    //Marshal.Copy(outUidArray, 0, uid, BS2Envirionment.BS2_USER_ID_SIZE);

                    result = (BS2ErrorCode)API.BS2_GetFilteredLog(context, xDeviceID, uid, eventCode, start, end, tnaKey, out outEventLogObjs, out outNumEventLogs);
                    if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                    {
                        strErrorMsg = "BS2_GetFilteredLog : " + result;
                        return dtUserLog;
                    }
                    else if (outNumEventLogs > 0)
                    {
                        DataSet dsList = new System.Data.DataSet();
                        dsList.Tables.Add("List");
                        dsList.Tables[0].Columns.Add("Id", Type.GetType("System.UInt32"));
                        dsList.Tables[0].Columns.Add("Code", Type.GetType("System.UInt16"));
                        dsList.Tables[0].Columns.Add("DeviceID", Type.GetType("System.UInt32"));
                        dsList.Tables[0].Columns.Add("UserId", Type.GetType("System.String"));
                        dsList.Tables[0].Columns.Add("LogTime", Type.GetType("System.DateTime"));

                        IntPtr curEventLogObjs = outEventLogObjs;
                        for (int idx = 0; idx < outNumEventLogs; idx++)
                        {
                            BS2Event eventLog = (BS2Event)Marshal.PtrToStructure(curEventLogObjs, structureType);
                            curEventLogObjs = (IntPtr)((long)curEventLogObjs + structSize);
                            DataRow drRow = dsList.Tables[0].NewRow();
                            drRow["Id"] = (uint)eventLog.id;
                            drRow["Code"] = (ushort)eventLog.code;
                            drRow["DeviceID"] = (uint)eventLog.deviceID;
                            drRow["UserId"] = Encoding.UTF8.GetString(eventLog.userID, 0, eventLog.userID.Length);
                            drRow["LogTime"] = Util.ConvertFromUnixTimestamp((uint)eventLog.dateTime).ToLocalTime();
                            dsList.Tables[0].Rows.Add(drRow);

                        }
                        dtUserLog = new DataView(dsList.Tables[0], "", "", DataViewRowState.CurrentRows).ToTable();

                        API.BS2_ReleaseObject(outEventLogObjs);
                    }
                    else
                    {
                        strErrorMsg = "There are no matching logs.";
                    }

                    if (uid != IntPtr.Zero)
                    {
                        Marshal.FreeHGlobal(uid);
                    }

                    result = (BS2ErrorCode)API.BS2_DisconnectDevice(context, xDeviceID);
                    if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                    {
                        strErrorMsg = "BS2_DisconnectDevice Failed : " + result;
                        return dtUserLog;
                    }
                    System.Threading.Thread.Sleep(500);

                }
            }
            catch (Exception ex)
            {
                result = (BS2ErrorCode)API.BS2_DisconnectDevice(context, xDeviceID);
                if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                {
                    strErrorMsg = "BS2_DisconnectDevice Failed : " + result;
                    return dtUserLog;
                }
                System.Threading.Thread.Sleep(500);

                throw new Exception("GetUserLog :- " + ex.Message.ToString());
            }
            return dtUserLog;
        }

       
    }
}
