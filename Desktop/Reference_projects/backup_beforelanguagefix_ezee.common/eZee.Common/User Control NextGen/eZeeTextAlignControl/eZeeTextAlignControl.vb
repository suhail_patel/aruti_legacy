Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports System.ComponentModel

Public Delegate Sub AlignChangeEventHandler(ByVal sender As Object, ByVal eventArgs As System.EventArgs)

<System.ComponentModel.DefaultEvent("AlignChanged")> _
Public Class eZeeTextAlignControl

#Region " Property Variables "

    Private menAlignment As ContentAlignment = ContentAlignment.MiddleCenter
    Private mintAlignValue As Integer = 2
    Private mblnDropDown As Boolean = False
    Private mclrGradientBackColor As Color = Color.LightSteelBlue
    Private mclrGradientForeColor As Color = Color.Black
    Private mclrGradientBorderColor As Color = Color.FromArgb(127, 157, 185)

    Public Event AlignChanged As AlignChangeEventHandler

#End Region

#Region " Properties "

    Public Property SelectedAlign() As ContentAlignment
        Get
            Return menAlignment
        End Get
        Set(ByVal value As ContentAlignment)
            menAlignment = value
            Call SetText()
        End Set
    End Property

    '<Browsable(False)> _
    'Public Property SelectedAlignValue() As Integer
    '    Get
    '        Return mintAlignValue
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintAlignValue = value
    '        Me.SelectedAlign = mintAlignValue
    '    End Set
    'End Property

    Public WriteOnly Property ButtonGradientBackColor() As Color
        Set(ByVal value As Color)
            mclrGradientBackColor = value
            Call SetTheme()
        End Set
    End Property

    Public WriteOnly Property ButtonGradientForeColor() As Color
        Set(ByVal value As Color)
            mclrGradientForeColor = value
            Call SetTheme()
        End Set
    End Property

    Public WriteOnly Property ButtonGradientBorderColor() As Color
        Set(ByVal value As Color)
            mclrGradientBorderColor = value
            Call SetTheme()
        End Set
    End Property

    Public WriteOnly Property TextBoxBackColor() As Color
        Set(ByVal value As Color)
            txtTextAlign.BackColor = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Private Sub SetTheme()
        For Each btn As eZeeLightButton In pnlTextAlign.Controls
            btn.GradientBackColor = Me.mclrGradientBackColor
            btn.GradientForeColor = Me.mclrGradientForeColor
            If btn.ShowDefaultBorderColor = False Then
                btn.BorderColor = Me.mclrGradientBorderColor
            End If
        Next
        objbtnDropDown.GradientBackColor = SystemColors.GradientInactiveCaption 'Me.mclrGradientBackColor
        objbtnDropDown.GradientForeColor = SystemColors.WindowText 'Me.mclrGradientForeColor
        If objbtnDropDown.ShowDefaultBorderColor = False Then
            objbtnDropDown.BorderColor = Me.mclrGradientBorderColor
        End If
    End Sub

    Private Sub OpenDropDown()
        Me.Height = 114
    End Sub

    Private Sub CloseDropDown()
        Me.Height = txtTextAlign.Height
    End Sub

  
    Public Property BottomCenter_Text() As String
        Get
            Return mstrBottomCenter
        End Get
        Set(ByVal value As String)
            mstrBottomCenter = value
        End Set
    End Property
    Public Property BottomLeft_Text() As String
        Get
            Return mstrBottomLeft
        End Get
        Set(ByVal value As String)
            mstrBottomLeft = value
        End Set
    End Property
    Public Property BottomRight_Text() As String
        Get
            Return mstrBottomRight
        End Get
        Set(ByVal value As String)
            mstrBottomRight = value
        End Set
    End Property
    Public Property MiddleCenter_Text() As String
        Get
            Return mstrMiddleCenter
        End Get
        Set(ByVal value As String)
            mstrMiddleCenter = value
        End Set
    End Property
    Public Property MiddleLeft_Text() As String
        Get
            Return mstrMiddleLeft
        End Get
        Set(ByVal value As String)
            mstrMiddleLeft = value
        End Set
    End Property
    Public Property MiddleRight_Text() As String
        Get
            Return mstrMiddleRight
        End Get
        Set(ByVal value As String)
            mstrMiddleRight = value
        End Set
    End Property
    Public Property TopCenter_Text() As String
        Get
            Return mstrTopCenter
        End Get
        Set(ByVal value As String)
            mstrTopCenter = value
        End Set
    End Property
    Public Property TopLeft_Text() As String
        Get
            Return mstrTopLeft
        End Get
        Set(ByVal value As String)
            mstrTopLeft = value
        End Set
    End Property
    Public Property TopRight_Text() As String
        Get
            Return mstrTopRight
        End Get
        Set(ByVal value As String)
            mstrTopRight = value
        End Set
    End Property

    Private mstrBottomCenter As String = "Bottom Center"
    Private mstrBottomLeft As String = "Bottom Left"
    Private mstrBottomRight As String = "Bottom Right"
    Private mstrMiddleCenter As String = "Middle Center"
    Private mstrMiddleLeft As String = "Middle Left"
    Private mstrMiddleRight As String = "Middle Right"
    Private mstrTopCenter As String = "Top Center"
    Private mstrTopLeft As String = "Top Left"
    Private mstrTopRight As String = "Top Right"

    Private Sub SetText()
        Dim btnButton As eZeeLightButton = Nothing
        If DesignMode = True Then Exit Sub
        Select Case Me.SelectedAlign
            Case ContentAlignment.BottomCenter
                txtTextAlign.Text = mstrBottomCenter
                objbtnBottomCenter.Selected = True
                btnButton = objbtnBottomCenter
            Case ContentAlignment.BottomLeft
                txtTextAlign.Text = mstrBottomLeft
                objbtnBottomLeft.Selected = True
                btnButton = objbtnBottomLeft
            Case ContentAlignment.BottomRight
                txtTextAlign.Text = mstrBottomRight
                objbtnBottomRight.Selected = True
                btnButton = objbtnBottomRight
            Case ContentAlignment.MiddleCenter
                txtTextAlign.Text = mstrMiddleCenter
                objbtnMiddleCenter.Selected = True
                btnButton = objbtnMiddleCenter
            Case ContentAlignment.MiddleLeft
                txtTextAlign.Text = mstrMiddleLeft
                objbtnMiddleLeft.Selected = True
                btnButton = objbtnMiddleLeft
            Case ContentAlignment.MiddleRight
                txtTextAlign.Text = mstrMiddleRight
                objbtnMiddleRight.Selected = True
                btnButton = objbtnMiddleRight
            Case ContentAlignment.TopCenter
                txtTextAlign.Text = mstrTopCenter
                objbtnTopCenter.Selected = True
                btnButton = objbtnTopCenter
            Case ContentAlignment.TopLeft
                txtTextAlign.Text = mstrTopLeft
                objbtnTopLeft.Selected = True
                btnButton = objbtnTopLeft
            Case ContentAlignment.TopRight
                txtTextAlign.Text = mstrTopRight
                objbtnTopRight.Selected = True
                btnButton = objbtnTopRight
        End Select

        For Each btn As eZeeLightButton In pnlTextAlign.Controls
            If btn IsNot btnButton Then
                btn.Selected = False
            End If
        Next
    End Sub

#End Region

#Region " Overrides Events "

    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        MyBase.OnResize(e)

        If Me.Width < 140 Then
            Me.Width = 140
        End If

        If mblnDropDown = True Then Exit Sub

        If Me.Height <> txtTextAlign.Height Then
            Call CloseDropDown()
        End If
    End Sub

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()

        Call CloseDropDown()

        'Call SetTheme()
    End Sub

    Protected Overrides Sub OnValidated(ByVal e As System.EventArgs)
        MyBase.OnValidated(e)
        Call CloseDropDown()
    End Sub

#End Region

#Region " Other Control's Events "

    Private Sub objbtnDropDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDropDown.Click
        mblnDropDown = True
        If Me.Height = txtTextAlign.Height Then
            Call OpenDropDown()
        Else
            Call CloseDropDown()
        End If
    End Sub

    Private Sub AlignButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnTopLeft.Click, objbtnTopCenter.Click, objbtnTopRight.Click, _
                                                objbtnMiddleLeft.Click, objbtnMiddleCenter.Click, objbtnMiddleRight.Click, _
                                                objbtnBottomLeft.Click, objbtnBottomCenter.Click, objbtnBottomRight.Click

        Dim btnButton As eZeeLightButton = CType(sender, eZeeLightButton)

        'btnButton.Selected = True


        Me.SelectedAlign = CType(btnButton.Tag, ContentAlignment)
        'Me.SelectedAlignValue = CInt(btnButton.Tag)

        Call SetText()
        'txtTextAlign.Text = Me.SelectedAlign.ToString

        Call CloseDropDown()
    End Sub

    Private Sub txtTextAlign_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTextAlign.GotFocus
        Call CloseDropDown()
    End Sub

    Private Sub txtTextAlign_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTextAlign.TextChanged
        RaiseEvent AlignChanged(Me, e)
    End Sub

    Private Sub pnlTextAlign_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnlTextAlign.LostFocus
        Call CloseDropDown()
    End Sub

    Private Sub pnlTextAlign_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlTextAlign.Paint
        'ControlPaint.DrawBorder(e.Graphics, New Rectangle(0, 0, pnlTextAlign.Width, pnlTextAlign.Height), Color.FromArgb(127, 157, 185), ButtonBorderStyle.Solid)
        ControlPaint.DrawVisualStyleBorder(e.Graphics, New Rectangle(0, 0, pnlTextAlign.Width - 1, pnlTextAlign.Height - 1))
    End Sub



#End Region

End Class
