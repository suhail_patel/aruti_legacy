Imports System
Imports System.Windows.Forms
Imports System.Drawing
Imports System.ComponentModel

Public Delegate Sub SplitButtonClickEventHandler(ByVal sender As Object, ByVal eventArgs As System.EventArgs)
'Public Delegate Sub MainButtonClickEventHandler(ByVal sender As Object, ByVal eventArgs As System.EventArgs)

'Krushna (10 Sep 2009) (Loyalty Program) -- Start
<System.Serializable()> <System.ComponentModel.DefaultEvent("Click")> _
Public Class eZeeSplitButton
    Inherits Control
    'Krushna (10 Sep 2009) (Loyalty Program) -- End
#Region " Private Variables "

    Private mclrGradientForeColor As Color = SystemColors.WindowText
    Private mclrGradientBackColor As Color = SystemColors.ActiveBorder
    Private mclrBorderColor As Color = Color.Black
    Private mblnShowDefaultBorderColor As Boolean = True
    Private mstrText As String = ""
    Private WithEvents mcntxtPopUpMenu As ContextMenuStrip = Nothing
    Private WithEvents btnMainButton As eZeeLightButton
    Private WithEvents objbtnSplitButton As eZeeLightButton

    'Krushna (10 Sep 2009) (Loyalty Program) -- Start
    Private mblnIsMenuOpened As Boolean = False
    'Krushna (10 Sep 2009) (Loyalty Program) -- End

#End Region

#Region " Event Declarations "

    Public Event SplitButtonClick As SplitButtonClickEventHandler
    Public Shadows Event Click As EventHandler

#End Region

#Region " Constructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        btnMainButton = New eZeeLightButton
        objbtnSplitButton = New eZeeLightButton

        ' Add any initialization after the InitializeComponent() call.
        SetStyle(ControlStyles.UserPaint, True)
        SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        SetStyle(ControlStyles.CacheText, True)

    End Sub

#End Region

#Region " Properties "

    <Browsable(True)> _
        Public Property GradientBackColor() As Color
        Get
            Return mclrGradientBackColor
        End Get
        Set(ByVal value As Color)
            mclrGradientBackColor = value
            Call SetThemeValues()
        End Set
    End Property

    <Browsable(True)> _
    Public Property GradientForeColor() As Color
        Get
            Return mclrGradientForeColor
        End Get
        Set(ByVal value As Color)
            mclrGradientForeColor = value
            Call SetThemeValues()
        End Set
    End Property

    <Browsable(True)> _
    Public Property ShowDefaultBorderColor() As Boolean
        Get
            Return mblnShowDefaultBorderColor
        End Get
        Set(ByVal value As Boolean)
            mblnShowDefaultBorderColor = value
            Call SetThemeValues()
        End Set
    End Property

    <Browsable(True)> _
    Public Property BorderColor() As Color
        Get
            Return mclrBorderColor
        End Get
        Set(ByVal value As Color)
            mclrBorderColor = value
            Call SetThemeValues()
        End Set
    End Property

    Public Property SplitButtonMenu() As ContextMenuStrip
        Get
            Return mcntxtPopUpMenu
        End Get
        Set(ByVal value As ContextMenuStrip)
            mcntxtPopUpMenu = value
        End Set
    End Property

    <Browsable(True)> _
    Public Overrides Property Text() As String
        Get
            Return mstrText
        End Get
        Set(ByVal value As String)
            mstrText = value
            btnMainButton.Text = mstrText
            Me.Invalidate()
        End Set
    End Property

    <Browsable(False)> _
    Public Shadows Property ForeColor() As Color
        Get
            Return MyBase.ForeColor
        End Get
        Set(ByVal value As Color)
            MyBase.ForeColor = value
        End Set
    End Property

    'Public Shadows Property Enabled() As Boolean
    '    Get
    '        Return MyBase.Enabled
    '    End Get
    '    Set(ByVal value As Boolean)
    '        If value Then
    '            Me.btnMainButton.SetDefaultGradientImage()
    '            Me.objbtnSplitButton.SetDefaultGradientImage()
    '        End If
    '        MyBase.Enabled = value

    '        'Krushna (09 Oct 2009) -- Start
    '        Call SetThemeValues()
    '        'Krushna (09 Oct 2009) -- End
    '    End Set
    'End Property


#End Region

#Region " Private Methods "
    Protected Overrides Sub OnEnabledChanged(ByVal e As System.EventArgs)
        MyBase.OnEnabledChanged(e)
        Call SetThemeValues()
    End Sub

    Protected Overrides Sub OnParentEnabledChanged(ByVal e As System.EventArgs)
        MyBase.OnParentEnabledChanged(e)
        Call SetThemeValues()
    End Sub

    Protected Overrides Sub OnParentVisibleChanged(ByVal e As System.EventArgs)
        MyBase.OnParentVisibleChanged(e)
        Call SetThemeValues()
    End Sub

    Private Sub SetThemeValues()
        If Me.Enabled = True Then
            btnMainButton.GradientBackColor = Me.GradientBackColor
            btnMainButton.GradientForeColor = Me.GradientForeColor
            btnMainButton.ShowDefaultBorderColor = Me.ShowDefaultBorderColor
            btnMainButton.BorderColor = Me.BorderColor

            objbtnSplitButton.GradientBackColor = Me.GradientBackColor
            objbtnSplitButton.GradientForeColor = Me.GradientForeColor
            objbtnSplitButton.ShowDefaultBorderColor = Me.ShowDefaultBorderColor
            objbtnSplitButton.BorderColor = Me.BorderColor
        End If
        Me.Invalidate()
    End Sub

#End Region

#Region " Other Control's Events "

    Private Sub objbtnSplitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSplitButton.Click
        RaiseEvent SplitButtonClick(Me, e)
        If Me.SplitButtonMenu IsNot Nothing Then
            'Krushna (10 Sep 2009) (Loyalty Program) -- Start
            objbtnSplitButton.SetPressedGradientImage()
            btnMainButton.SetHoverGradientImage()
            'Krushna (10 Sep 2009) (Loyalty Program) -- End
            mblnIsMenuOpened = True
            Me.SplitButtonMenu.Show(Me, 0, Me.Height)
        End If
    End Sub

    Private Sub btnMainButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMainButton.Click
        RaiseEvent Click(Me, e)
        'objbtnSplitButton.SetDefaultGradientImage()
    End Sub

    Private Sub btnMainButton_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMainButton.MouseEnter
        If mblnIsMenuOpened = False Then objbtnSplitButton.SetHoverGradientImage()
    End Sub

    Private Sub btnMainButton_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMainButton.MouseLeave
        If mblnIsMenuOpened = False Then objbtnSplitButton.SetDefaultGradientImage()
    End Sub

    Private Sub objbtnSplitButton_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSplitButton.MouseEnter
        If mblnIsMenuOpened = False Then
        btnMainButton.SetHoverGradientImage()
        Else
            objbtnSplitButton.SetPressedGradientImage()
        End If
    End Sub

    Private Sub objbtnSplitButton_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSplitButton.MouseLeave
        If mblnIsMenuOpened = False Then
        btnMainButton.SetDefaultGradientImage()
        Else
            objbtnSplitButton.SetPressedGradientImage()
        End If
    End Sub

    Private Sub mcntxtPopUpMenu_Closed(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripDropDownClosedEventArgs) Handles mcntxtPopUpMenu.Closed
        objbtnSplitButton.SetDefaultGradientImage()
        btnMainButton.SetDefaultGradientImage()
        mblnIsMenuOpened = False
    End Sub

#End Region

#Region " Overrides Events "

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()

        objbtnSplitButton.Image = My.Resources.SplitButtonImage1_16
        objbtnSplitButton.ImageAlign = ContentAlignment.MiddleCenter
        objbtnSplitButton.Padding = New Padding(0, 0, 2, 0)
        objbtnSplitButton.Dock = DockStyle.Right
        objbtnSplitButton.GradientBackColor = SystemColors.ActiveBorder
        objbtnSplitButton.Width = 20
        objbtnSplitButton.Height = Me.Height
        objbtnSplitButton.Text = ""

        btnMainButton.Text = Me.Text
        btnMainButton.Name = "btnMainButton"
        Me.btnMainButton.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        btnMainButton.GradientBackColor = SystemColors.ActiveBorder
        btnMainButton.Width = Me.Width - objbtnSplitButton.Width + 1
        btnMainButton.Height = Me.Height

        'Call SetControls()

        Me.Controls.Add(btnMainButton)
        Me.Controls.Add(objbtnSplitButton)

        objbtnSplitButton.BringToFront()
        btnMainButton.BringToFront()

        'MsgBox(Me.Text)
    End Sub

#End Region

    
End Class
