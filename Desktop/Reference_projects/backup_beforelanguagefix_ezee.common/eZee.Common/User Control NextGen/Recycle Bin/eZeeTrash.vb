Imports System

Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

Public Delegate Sub CleanTrashEventHandler(ByVal sender As Object, ByVal eventArgs As System.EventArgs)
Public Delegate Sub ShowVoidRecordsEventHandler(ByVal sender As Object, ByVal eventArgs As System.EventArgs)
Public Delegate Sub HideVoidRecordsEventHandler(ByVal sender As Object, ByVal eventArgs As System.EventArgs)

<System.Serializable()> <System.ComponentModel.DefaultEvent("ShowVoidRecordsClick")> _
Public Class eZeeTrash
    Inherits PictureBox

#Region " Variables & Enumeration "

    Private menTrashState As EnumTrashState = EnumTrashState.Empty
    Private mstrEmptyTrashMessage As String = ""
    Private mblnAskEmptyMessage As Boolean = True
    'Private mstrCurrentImageName As String = ""

    'Private mblnEnableDragDropEvent As Boolean = True
    Private mblnEnableDragEnterEvent As Boolean = True
    Private mblnEnableDragOverEvent As Boolean = True
    Private mblnPressed As Boolean = False

    Private mimgTrash_Full As Image = Nothing
    Private mimgTrash_Full_Hover As Image = Nothing
    Private mimgTrash_Empty As Image = Nothing
    Private mimgTrash_Empty_Hover As Image = Nothing

    Private menCurrentTrashState As EnumTrashState = EnumTrashState.Empty


    Public Enum EnumTrashState
        None = 0
        Full = 1
        Empty = 2
    End Enum

    Public Enum TrashImages
        Trash_Full = 0
        Trash_Empty = 1
        Trash_Blue_Full = 2
        Trash_Blue_Empty = 3
        Trash_Gray_Full = 4
        Trash_Gray_Empty = 5
    End Enum

#End Region

#Region " Control's Events "

    Public Event CleanTrashClick As CleanTrashEventHandler
    Public Event ShowVoidRecordsClick As ShowVoidRecordsEventHandler
    Public Event HideVoidRecordsClick As ShowVoidRecordsEventHandler

#End Region

#Region " Contructor "

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        Me.AllowDrop = True
        Me.BackColor = Color.Transparent
        Me.Size = New Size(48, 48)
        Me.Cursor = Cursors.Hand

        'Me.Invalidate()
    End Sub

#End Region

#Region " Properties "

    ''' <summary>
    ''' Gets or Sets the state of Trash
    ''' </summary>
    <System.ComponentModel.Browsable(True), _
        System.ComponentModel.Description("Gets or Sets the state of Trash")> _
    Public Property State() As EnumTrashState
        Get
            Return menTrashState
        End Get
        Set(ByVal value As EnumTrashState)
            menTrashState = value

            If Not (Me.TrashEmpty Is Nothing Or Me.TrashFull Is Nothing) Then
                If menTrashState = EnumTrashState.Empty Then
                    Me.Image = Me.TrashEmpty 'imgList.Images("RecycleBin_Empty.png")
                    'mstrCurrentImageName = Me.TrashEmpty.ToString '"RecycleBin_Empty.png"
                    menCurrentTrashState = EnumTrashState.Empty
                ElseIf menTrashState = EnumTrashState.Full Then
                    Me.Image = Me.TrashFull 'imgList.Images("RecycleBin_Full.png")
                    'mstrCurrentImageName = Me.TrashFull.ToString '"RecycleBin_Full.png"
                    menCurrentTrashState = EnumTrashState.Full
                Else
                    Me.Image = Nothing
                    'mstrCurrentImageName = ""
                    menCurrentTrashState = EnumTrashState.None
                End If
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets the message that is to be appeared while cleaning Trash.
    ''' </summary>
    <System.ComponentModel.Browsable(True), _
        System.ComponentModel.Description("Gets or Sets the message that is to be appeared while cleaning Trash.")> _
    Public Property MessageOnEmptyTrash() As String
        Get
            Return mstrEmptyTrashMessage
        End Get
        Set(ByVal value As String)
            mstrEmptyTrashMessage = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets a value indicating whether the control ask message for cleaning Trash
    ''' </summary>
    <System.ComponentModel.Browsable(True), _
        System.ComponentModel.Description("Gets or Sets a value indicating whether user wants handle DragDrop Event automatically or himself")> _
    Public Property AskEmptyMessage() As Boolean
        Get
            Return mblnAskEmptyMessage
        End Get
        Set(ByVal value As Boolean)
            mblnAskEmptyMessage = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets a value indicating whether control handles DragEnter Event automatically or not.
    ''' </summary>
    <System.ComponentModel.Browsable(True), _
        System.ComponentModel.Description("Gets or Sets a value indicating whether user wants handle DragEnter Event automatically or himself")> _
    Public Property EnableDragEnterEvent() As Boolean
        Get
            Return mblnEnableDragEnterEvent
        End Get
        Set(ByVal value As Boolean)
            mblnEnableDragEnterEvent = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets a value indicating whether control handles DragOver Event automatically or not.
    ''' </summary>
    <System.ComponentModel.Browsable(True), _
        System.ComponentModel.Description("Gets or Sets a value indicating whether control handles DragOver Event automatically or not.")> _
    Public Property EnableDragOverEvent() As Boolean
        Get
            Return mblnEnableDragOverEvent
        End Get
        Set(ByVal value As Boolean)
            mblnEnableDragOverEvent = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets a value indicating whether control can respond to user interaction.
    ''' </summary>
    <System.ComponentModel.Browsable(True), _
        System.ComponentModel.Description("Gets or Sets a value indicating whether control can respond to user interaction.")> _
    Public Property EnableShowVoidRecords() As Boolean
        Get
            Return tsmiShowVoidRecords.Enabled
        End Get
        Set(ByVal value As Boolean)
            tsmiShowVoidRecords.Enabled = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets a value indicating whether control can respond to user interaction.
    ''' </summary>
    <System.ComponentModel.Browsable(True), _
        System.ComponentModel.Description("Gets or Sets a value indicating whether control can respond to user interaction.")> _
    Public Property EnableCleanTrash() As Boolean
        Get
            Return tsmiCleanTrash.Enabled
        End Get
        Set(ByVal value As Boolean)
            tsmiCleanTrash.Enabled = value
        End Set
    End Property

    Public Property Pressed() As Boolean
        Get
            Return mblnPressed
        End Get
        Set(ByVal value As Boolean)
            mblnPressed = value
        End Set
    End Property

    <Browsable(False)> _
    Public Shadows Property Image() As Image
        Get
            Return MyBase.Image
        End Get
        Set(ByVal value As Image)
            MyBase.Image = value
        End Set
    End Property

    Public Property TrashFull() As Image
        Get
            Return mimgTrash_Full
        End Get
        Set(ByVal value As Image)
            mimgTrash_Full = value
        End Set
    End Property

    Public Property TrashFullHover() As Image
        Get
            Return mimgTrash_Full_Hover
        End Get
        Set(ByVal value As Image)
            mimgTrash_Full_Hover = value
        End Set
    End Property

    Public Property TrashEmpty() As Image
        Get
            Return mimgTrash_Empty
        End Get
        Set(ByVal value As Image)
            mimgTrash_Empty = value
        End Set
    End Property

    Public Property TrashEmptyHover() As Image
        Get
            Return mimgTrash_Empty_Hover
        End Get
        Set(ByVal value As Image)
            mimgTrash_Empty_Hover = value
        End Set
    End Property

    '''' <summary>
    '''' Gets or Sets a value indicating whether control handles DragDrop Event automatically or not.
    '''' </summary>
    '<System.ComponentModel.Browsable(True), _
    '    System.ComponentModel.Description("Gets or Sets a value indicating whether control handles DragDrop Event automatically or not.")> _
    'Public Property EnableDragDropEvent() As Boolean
    '    Get
    '        Return mblnEnableDragDropEvent
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnEnableDragDropEvent = value
    '    End Set
    'End Property

#End Region

#Region " Private Methods "

    Private Sub SetTrashImage()
        If menCurrentTrashState = EnumTrashState.Empty Then 'Me.Image Is Me.TrashEmptyHover 
            Me.Image = Me.TrashEmpty 'imgList.Images("RecycleBin_Empty.png")
            'mstrCurrentImageName = Me.TrashEmpty.ToString '"RecycleBin_Empty.png"
            menCurrentTrashState = EnumTrashState.Empty
        Else
            Me.Image = Me.TrashFull 'imgList.Images("RecycleBin_Full.png")
            'mstrCurrentImageName = Me.TrashFull.ToString  '"RecycleBin_Full.png"
            menCurrentTrashState = EnumTrashState.Full
        End If
    End Sub

    Private Sub SetTrashHoverImage()
        If menCurrentTrashState = EnumTrashState.Empty Then 'Me.Image Is Me.TrashEmpty 
            Me.Image = Me.TrashEmptyHover 'imgList.Images("RecycleBin_Empty_Light.png")
            'mstrCurrentImageName = Me.TrashEmptyHover.ToString '"RecycleBin_Empty_Light.png"
            menCurrentTrashState = EnumTrashState.Empty
        Else
            Me.Image = Me.TrashFullHover 'imgList.Images("RecycleBin_Full_Light.png")
            'mstrCurrentImageName = Me.TrashFullHover.ToString '"RecycleBin_Full_Light.png"
            menCurrentTrashState = EnumTrashState.Full
        End If
    End Sub

#End Region

#Region " Overrides Events "

    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        If Me.Width <> 48 Or Me.Height <> 48 Then
            Me.Size = New Size(48, 48)
        End If

        MyBase.OnResize(e)
    End Sub

    Protected Overrides Sub OnDragOver(ByVal drgevent As System.Windows.Forms.DragEventArgs)
        MyBase.OnDragOver(drgevent)

        If Not mblnEnableDragOverEvent Then Exit Sub

        If drgevent.Data.GetDataPresent("System.Windows.Forms.ListViewItem", False) Then
            drgevent.Effect = DragDropEffects.Move
        End If
    End Sub

    Protected Overrides Sub OnDragEnter(ByVal drgevent As System.Windows.Forms.DragEventArgs)
        MyBase.OnDragEnter(drgevent)

        If Not mblnEnableDragEnterEvent Then Exit Sub

        Call SetTrashHoverImage()
    End Sub

    Protected Overrides Sub OnDragLeave(ByVal e As System.EventArgs)
        MyBase.OnDragLeave(e)

        Call SetTrashImage()
    End Sub

    Protected Overrides Sub OnMouseEnter(ByVal e As System.EventArgs)
        MyBase.OnMouseEnter(e)

        Call SetTrashHoverImage()
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As System.EventArgs)
        MyBase.OnMouseLeave(e)

       Call SetTrashImage()
    End Sub

    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        MyBase.OnClick(e)
        If Me.Pressed Then
            Me.Pressed = False
            tsmiHideVoidRecords.Visible = False
            tsmiShowVoidRecords.Visible = True
        Else
            Me.Pressed = True
            tsmiHideVoidRecords.Visible = True
            tsmiShowVoidRecords.Visible = False
        End If
    End Sub

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()

        Me.TrashEmpty = My.Resources.TrashEmpty
        Me.TrashEmptyHover = My.Resources.TrashEmptyHover
        Me.TrashFull = My.Resources.TrashFull
        Me.TrashFullHover = My.Resources.TrashFullHover

    End Sub

#End Region

#Region " Raise ContextMenuItem's Events "

    Private Sub tsmiCleanTrash_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsmiCleanTrash.Click
        RaiseEvent CleanTrashClick(Me, e)
    End Sub

    Private Sub tsmiShowVoidRecords_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsmiShowVoidRecords.Click
        RaiseEvent ShowVoidRecordsClick(Me, e)
        tsmiHideVoidRecords.Visible = True
        tsmiShowVoidRecords.Visible = False
    End Sub

    Private Sub tsmiHideVoidRecords_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsmiHideVoidRecords.Click
        RaiseEvent HideVoidRecordsClick(Me, e)
        tsmiHideVoidRecords.Visible = False
        tsmiShowVoidRecords.Visible = True
    End Sub

#End Region

End Class


