Partial Class eZeeTrash
    Inherits System.Windows.Forms.PictureBox

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    'Component overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.cmsTrashOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmiShowVoidRecords = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmiHideVoidRecords = New System.Windows.Forms.ToolStripMenuItem
        Me.objtssSeparator = New System.Windows.Forms.ToolStripSeparator
        Me.tsmiCleanTrash = New System.Windows.Forms.ToolStripMenuItem
        Me.cmsTrashOperations.SuspendLayout()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmsTrashOperations
        '
        Me.cmsTrashOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiShowVoidRecords, Me.tsmiHideVoidRecords, Me.objtssSeparator, Me.tsmiCleanTrash})
        Me.cmsTrashOperations.Name = "cmsTrashOperations"
        Me.cmsTrashOperations.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.cmsTrashOperations.ShowImageMargin = False
        Me.cmsTrashOperations.Size = New System.Drawing.Size(156, 76)
        Me.cmsTrashOperations.Text = "Trash Operations"
        '
        'tsmiShowVoidRecords
        '
        Me.tsmiShowVoidRecords.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsmiShowVoidRecords.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsmiShowVoidRecords.Name = "tsmiShowVoidRecords"
        Me.tsmiShowVoidRecords.Size = New System.Drawing.Size(155, 22)
        Me.tsmiShowVoidRecords.Text = "&Show Void Records"
        Me.tsmiShowVoidRecords.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tsmiHideVoidRecords
        '
        Me.tsmiHideVoidRecords.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsmiHideVoidRecords.Name = "tsmiHideVoidRecords"
        Me.tsmiHideVoidRecords.Size = New System.Drawing.Size(155, 22)
        Me.tsmiHideVoidRecords.Text = "Hide Void Records"
        Me.tsmiHideVoidRecords.Visible = False
        '
        'objtssSeparator
        '
        Me.objtssSeparator.Name = "objtssSeparator"
        Me.objtssSeparator.Size = New System.Drawing.Size(152, 6)
        '
        'tsmiCleanTrash
        '
        Me.tsmiCleanTrash.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsmiCleanTrash.Name = "tsmiCleanTrash"
        Me.tsmiCleanTrash.Size = New System.Drawing.Size(155, 22)
        Me.tsmiCleanTrash.Text = "&Clean Trash"
        Me.tsmiCleanTrash.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeTrash
        '
        Me.ContextMenuStrip = Me.cmsTrashOperations
        Me.ErrorImage = Nothing
        Me.InitialImage = Nothing
        Me.cmsTrashOperations.ResumeLayout(False)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmsTrashOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmiShowVoidRecords As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiCleanTrash As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objtssSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiHideVoidRecords As System.Windows.Forms.ToolStripMenuItem

End Class
