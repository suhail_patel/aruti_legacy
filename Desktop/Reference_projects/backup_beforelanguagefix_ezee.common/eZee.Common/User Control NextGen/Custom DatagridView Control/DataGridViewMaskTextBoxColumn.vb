Imports System.Windows.Forms
Imports System.ComponentModel

<ToolboxItem(False)> _
<Drawing.ToolboxBitmap(GetType(MaskedTextBox))> _
Class DataGridViewMaskTextBoxEditingControl
    Inherits MaskedTextBox
    Implements IDataGridViewEditingControl

    Private dataGridViewControl As DataGridView
    Private valueIsChanged As Boolean = False
    Private rowIndexNum As Integer

    Public Sub New()
        Me.TabStop = False
    End Sub

    Public Property EditingControlFormattedValue() As Object _
        Implements IDataGridViewEditingControl.EditingControlFormattedValue
        Get
            Return Me.Text
        End Get
        Set(ByVal value As Object)
            If TypeOf value Is [String] Then
                Me.Text = value
            End If
        End Set
    End Property

    Public Function GetEditingControlFormattedValue(ByVal context As DataGridViewDataErrorContexts) As Object _
        Implements IDataGridViewEditingControl.GetEditingControlFormattedValue
        Return Me.Text
    End Function

    Public Overridable Sub ApplyCellStyleToEditingControl(ByVal dataGridViewCellStyle As DataGridViewCellStyle) Implements System.Windows.Forms.IDataGridViewEditingControl.ApplyCellStyleToEditingControl
        Me.Font = dataGridViewCellStyle.Font
        If dataGridViewCellStyle.BackColor.A < 255 Then
            ' The NumericUpDown control does not support transparent back colors
            Dim opaqueBackColor As Drawing.Color = Drawing.Color.FromArgb(255, dataGridViewCellStyle.BackColor)
            Me.BackColor = opaqueBackColor
            Me.dataGridViewControl.EditingPanel.BackColor = opaqueBackColor
        Else
            Me.BackColor = dataGridViewCellStyle.BackColor
        End If
        Select Case dataGridViewCellStyle.Alignment
            Case DataGridViewContentAlignment.BottomCenter, DataGridViewContentAlignment.MiddleCenter, DataGridViewContentAlignment.TopCenter
                Me.TextAlign = HorizontalAlignment.Center
            Case DataGridViewContentAlignment.BottomLeft, DataGridViewContentAlignment.MiddleLeft, DataGridViewContentAlignment.TopLeft
                Me.TextAlign = HorizontalAlignment.Left
            Case DataGridViewContentAlignment.BottomRight, DataGridViewContentAlignment.MiddleRight, DataGridViewContentAlignment.TopRight
                Me.TextAlign = HorizontalAlignment.Right
        End Select

        Me.ForeColor = dataGridViewCellStyle.ForeColor
    End Sub

    Public Property EditingControlRowIndex() As Integer Implements IDataGridViewEditingControl.EditingControlRowIndex

        Get
            Return rowIndexNum
        End Get
        Set(ByVal value As Integer)
            rowIndexNum = value
        End Set

    End Property

    Public Function EditingControlWantsInputKey(ByVal key As Keys, ByVal dataGridViewWantsInputKey As Boolean) As Boolean _
        Implements IDataGridViewEditingControl.EditingControlWantsInputKey

        ' Let the DateTimePicker handle the keys listed.
        Select Case key And Keys.KeyCode
            Case Keys.Left, Keys.Up, Keys.Down, Keys.Right, _
                Keys.Home, Keys.End, Keys.PageDown, Keys.PageUp

                Return True

            Case Else
                Return False
        End Select

    End Function

    Public Sub PrepareEditingControlForEdit(ByVal selectAll As Boolean) _
        Implements IDataGridViewEditingControl.PrepareEditingControlForEdit

        ' No preparation needs to be done.

    End Sub

    Public ReadOnly Property RepositionEditingControlOnValueChange() As Boolean _
        Implements IDataGridViewEditingControl.RepositionEditingControlOnValueChange

        Get
            Return False
        End Get

    End Property

    Public Property EditingControlDataGridView() As DataGridView _
        Implements IDataGridViewEditingControl.EditingControlDataGridView

        Get
            Return dataGridViewControl
        End Get

        Set(ByVal value As DataGridView)
            dataGridViewControl = value
        End Set

    End Property

    Public Property EditingControlValueChanged() As Boolean _
        Implements IDataGridViewEditingControl.EditingControlValueChanged
        Get
            Return valueIsChanged
        End Get
        Set(ByVal value As Boolean)
            valueIsChanged = value
        End Set
    End Property

    Public ReadOnly Property EditingControlCursor() As Cursor _
        Implements IDataGridViewEditingControl.EditingPanelCursor

        Get
            Return MyBase.Cursor
        End Get

    End Property

    Protected Overrides Sub OnTextChanged(ByVal eventargs As EventArgs)
        Try
            valueIsChanged = True

            If Not Me.EditingControlDataGridView Is Nothing Then
                Me.EditingControlDataGridView.NotifyCurrentCellDirty(True)
            End If

            MyBase.OnTextChanged(eventargs)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class

<Drawing.ToolboxBitmap(GetType(MaskedTextBox))> _
Public Class DataGridViewMaskTextBoxColumn
    Inherits DataGridViewColumn

    Public Sub New()
        MyBase.New(New DataGridViewMaskTextBoxCell())
        Dim cell As New DataGridViewMaskTextBoxCell()
        MyBase.CellTemplate = cell

    End Sub

    Public Overrides Property CellTemplate() As DataGridViewCell
        Get
            Return MyBase.CellTemplate
        End Get
        Set(ByVal value As DataGridViewCell)
            ' Ensure that the cell used for the template is a CalendarCell.
            If Not (value Is Nothing) AndAlso _
                Not value.GetType().IsAssignableFrom(GetType(DataGridViewMaskTextBoxCell)) Then
                Throw New InvalidCastException("Must be a MaskCell")
            End If
            MyBase.CellTemplate = value
        End Set

    End Property

    Private ReadOnly Property MaskTextBoxEditCellTemplate() As DataGridViewMaskTextBoxCell
        Get
            Dim cell As DataGridViewMaskTextBoxCell = TryCast(Me.CellTemplate, DataGridViewMaskTextBoxCell)
            If cell Is Nothing Then
                Throw New InvalidOperationException("DataGridViewMaskTextBoxColumn does not have a CellTemplate.")
            End If
            Return cell
        End Get
    End Property

    <Category("Appearance"), DefaultValue(0), Description("The decimal length of cell value.")> _
    Public Property Mask() As String
        Get
            Return Me.MaskTextBoxEditCellTemplate.Mask
        End Get
        Set(ByVal value As String)

            MyBase.DefaultCellStyle.Format = value

            Me.MaskTextBoxEditCellTemplate.Mask = value
            If Me.DataGridView IsNot Nothing Then
                ' Update all the existing DataGridViewNumericUpDownCell cells in the column accordingly.
                Dim dataGridViewRows As DataGridViewRowCollection = Me.DataGridView.Rows
                Dim rowCount As Integer = dataGridViewRows.Count
                For rowIndex As Integer = 0 To rowCount - 1
                    ' Be careful not to unshare rows unnecessarily. 
                    ' This could have severe performance repercussions.
                    Dim dataGridViewRow As DataGridViewRow = dataGridViewRows.SharedRow(rowIndex)
                    Dim dataGridViewCell As DataGridViewMaskTextBoxCell = TryCast(dataGridViewRow.Cells(Me.Index), DataGridViewMaskTextBoxCell)
                    If dataGridViewCell IsNot Nothing Then
                        ' Call the internal SetDecimalPlaces method instead of the property to avoid invalidation 
                        ' of each cell. The whole column is invalidated later in a single operation for better performance.
                        dataGridViewCell.SetMask(rowIndex, value)
                    End If
                Next
                Me.DataGridView.InvalidateColumn(Me.Index)
                ' TODO: Call the grid's autosizing methods to autosize the column, rows, column headers / row headers as needed.
            End If
        End Set
    End Property
End Class

Public Class DataGridViewMaskTextBoxCell
    Inherits DataGridViewTextBoxCell

    Public str As String = Nothing

    Public Sub New()
        'Me.Style.Format = "hh.mm"
    End Sub

    Private m_Mask As String = ""

    <DefaultValue(0)> _
    Public Property Mask() As String
        Get
            Return m_Mask
        End Get
        Set(ByVal value As String)
            If m_Mask <> value Then
                SetMask(Me.RowIndex, value)
                OnCommonChange()
                ' Assure that the cell/column gets repainted and autosized if needed
            End If
        End Set
    End Property

    Private ReadOnly Property MaskTextBoxEditingControl() As DataGridViewMaskTextBoxEditingControl
        Get
            Return TryCast(Me.DataGridView.EditingControl, DataGridViewMaskTextBoxEditingControl)
        End Get
    End Property

    Friend Sub SetMask(ByVal rowIndex As Integer, ByVal value As String)
        m_Mask = value
        If OwnsEditingControl(rowIndex) Then
            Me.MaskTextBoxEditingControl.Mask = value
        End If
    End Sub

    Private Sub OnCommonChange()
        If Me.DataGridView IsNot Nothing AndAlso Not Me.DataGridView.IsDisposed AndAlso Not Me.DataGridView.Disposing Then
            If Me.RowIndex = -1 Then
                Me.DataGridView.InvalidateColumn(Me.ColumnIndex)
            Else
                Me.DataGridView.UpdateCellValue(Me.ColumnIndex, Me.RowIndex)
            End If
        End If
    End Sub

    Private Function OwnsEditingControl(ByVal rowIndex As Integer) As Boolean
        If rowIndex = -1 OrElse Me.DataGridView Is Nothing Then
            Return False
        End If
        Dim editingControl As DataGridViewMaskTextBoxEditingControl = TryCast(Me.DataGridView.EditingControl, DataGridViewMaskTextBoxEditingControl)
        Return editingControl IsNot Nothing AndAlso rowIndex = DirectCast(editingControl, IDataGridViewEditingControl).EditingControlRowIndex
    End Function

    Public Overloads Overrides Function Clone() As Object
        Dim dataGridViewCell As DataGridViewMaskTextBoxCell = TryCast(MyBase.Clone(), DataGridViewMaskTextBoxCell)
        If dataGridViewCell IsNot Nothing Then
            dataGridViewCell.Mask = Me.Mask
        End If
        Return dataGridViewCell
    End Function

    Public Overrides Sub InitializeEditingControl(ByVal rowIndex As Integer, ByVal initialFormattedValue As Object, ByVal dataGridViewCellStyle As DataGridViewCellStyle)

        ' Set the value of the editing control to the current cell value.
        MyBase.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle)
        Dim ctl As DataGridViewMaskTextBoxEditingControl = CType(DataGridView.EditingControl, DataGridViewMaskTextBoxEditingControl)

        If ctl IsNot Nothing Then
            ctl.BorderStyle = BorderStyle.None
            ctl.Mask = Me.Mask

            Dim initialFormattedValueStr As String = TryCast(initialFormattedValue, String)

            If String.IsNullOrEmpty(initialFormattedValueStr) Then
                ctl.Text = ""
            Else
                ctl.Text = initialFormattedValueStr
            End If
        End If
    End Sub

    Public Overrides ReadOnly Property EditType() As Type
        Get
            ' Return the type of the editing contol that CalendarCell uses.
            Return GetType(DataGridViewMaskTextBoxEditingControl)
        End Get
    End Property

    Public Overrides ReadOnly Property ValueType() As Type
        Get
            ' Return the type of the value that CalendarCell contains.
            Return GetType(String)
        End Get
    End Property

End Class

