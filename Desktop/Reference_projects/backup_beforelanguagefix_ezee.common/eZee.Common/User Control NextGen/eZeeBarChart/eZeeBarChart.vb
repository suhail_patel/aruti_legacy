Imports System.Drawing

Public Class eZeeBarChart
    Private mdsSource As DataSet = Nothing
    Private mintBarWidth As Integer = 20
    Private mintGroupWidth As Integer = 20

    Private mstrTitle As String = ""
    Private mfnTitle As Font = Me.Font
    Private mclrTitle As Color = Color.Black

    Private mstrXAxisName As String = ""
    Private mstrYAxisName As String = ""
    Private mfnAxis As Font = Me.Font
    Private mclrAxis As Color = Color.Black

    Private mcolTextField As Collection
    Private mcolValueField As Collection
    Private mcolColor As Collection
    Private mcolGroupField As Collection


    Private mintGap As Integer = 2
    Private mintElipsWidth As Integer = 20
    Private mfont As Font = New Font(Me.Font, FontStyle.Regular)

    Private mintX1 As Integer = 0
    Private mintIndex As Integer = 0

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        mcolColor = New Collection
        mcolTextField = New Collection
        mcolValueField = New Collection
        mcolGroupField = New Collection
    End Sub

    Public Property Title() As String
        Get
            Return mstrTitle
        End Get
        Set(ByVal value As String)
            mstrTitle = value
        End Set
    End Property

    Public Property TitleFont() As Font
        Get
            Return mfnTitle
        End Get
        Set(ByVal value As Font)
            mfnTitle = value
        End Set
    End Property

    Public Property TitleForeColor() As Color
        Get
            Return mclrTitle
        End Get
        Set(ByVal value As Color)
            mclrTitle = value
        End Set
    End Property

    Public Property XxisCaption() As String
        Get
            Return mstrXAxisName
        End Get
        Set(ByVal value As String)
            mstrXAxisName = value
        End Set
    End Property

    Public Property YaxisCaption() As String
        Get
            Return mstrYAxisName
        End Get
        Set(ByVal value As String)
            mstrYAxisName = value
        End Set
    End Property

    Public Property AxisFont() As Font
        Get
            Return mfnAxis
        End Get
        Set(ByVal value As Font)
            mfnAxis = value
        End Set
    End Property

    Public Property AxisForeColor() As Color
        Get
            Return mclrAxis
        End Get
        Set(ByVal value As Color)
            mclrAxis = value
        End Set
    End Property

    Public Property DataSource() As DataSet
        Get
            Return mdsSource
        End Get
        Set(ByVal value As DataSet)
            mdsSource = value
        End Set
    End Property

    Public Property BarWidth() As Integer
        Get
            Return mintGroupWidth
        End Get
        Set(ByVal value As Integer)
            mintGroupWidth = value
        End Set
    End Property

    Public Sub AddBar(ByVal TextField As String, ByVal ValueField As String, ByVal BarColor As Color, Optional ByVal GroupField As String = "")
        mintIndex += 1
        mcolTextField.Add(TextField, mintIndex)
        mcolValueField.Add(ValueField, mintIndex)
        mcolColor.Add(BarColor, mintIndex)
        mcolGroupField.Add(GroupField, mintIndex)
    End Sub

    Private Sub picBarChart_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles picBarChart.Paint
        e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
        Call fillChart(e.Graphics)
    End Sub

    Private Sub fillChart(ByVal g As Graphics)
        Try
            If Not mdsSource Is Nothing Then
                If mdsSource.Tables.Count > 0 Then
                    Dim i As Integer
                    Dim intIndex As Integer = 0
                    mintBarWidth = mintGroupWidth / mintIndex
                    mintElipsWidth = 20 / mintIndex
                    mintX1 = 0
                    Dim mdblmax As Double = GetMaxValue()
                    If mdblmax = 0 Then mdblmax = 1
                    For Each dr As DataRow In mdsSource.Tables(0).Rows
                        For i = 1 To mcolTextField.Count
                            intIndex += 1
                            If i = 1 Then
                                drawBar(g, intIndex, (dr.Item(mcolValueField.Item(i).ToString) * (Me.Height - 120)) / mdblmax, dr.Item(mcolValueField.Item(i).ToString).ToString, dr.Item(mcolTextField.Item(i).ToString), mcolColor.Item(i), False, IIf(mcolTextField.Item(i).ToString <> "", dr.Item(mcolTextField.Item(i).ToString), ""))
                            Else
                                drawBar(g, intIndex, (dr.Item(mcolValueField.Item(i).ToString) * (Me.Height - 120)) / mdblmax, dr.Item(mcolValueField.Item(i).ToString).ToString, dr.Item(mcolTextField.Item(i).ToString), mcolColor.Item(i), True)
                            End If
                        Next
                    Next

                    Dim fmt As New StringFormat
                    fmt.Alignment = StringAlignment.Center

                    Dim fmtVer As New StringFormat(StringFormatFlags.DirectionVertical)
                    fmtVer.Alignment = StringAlignment.Center
                    'fmtVer.LineAlignment = StringAlignment.Center

                    Dim sizef As SizeF = g.MeasureString(mstrYAxisName, mfnAxis, Int32.MaxValue, fmtVer)

                    g.DrawString(mstrTitle, mfnTitle, New Drawing.SolidBrush(mclrTitle), New Rectangle(0, 5, Me.Width, 20), fmt)
                    g.DrawString(mstrXAxisName, mfnAxis, New Drawing.SolidBrush(mclrAxis), New Rectangle(0, Me.Height - 20, Me.Width, 20), fmt)
                    g.DrawString(mstrYAxisName, mfnAxis, New Drawing.SolidBrush(mclrAxis), New Rectangle(3, 0, sizef.Width, Me.Height), fmtVer)

                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub drawBar(ByVal g As Graphics, ByVal intIndex As Integer, ByVal intValue As Integer, ByVal strvalue As String, ByVal strText As String, ByVal colBar As Color, Optional ByVal blnGroupCol As Boolean = False, Optional ByVal strGroupText As String = "")
        Dim rect As New Rectangle

        Dim x1 As Integer = 0, x2 As Integer = 0
        Dim y1 As Integer = Me.Height - 65, y2 As Integer = 0


        'If blnGroupCol = False Then
        '    x1 = ((intIndex - 1) * (mintBarWidth + mintGap)) + (mintGap / 2)
        'Else
        '    x1 = ((intIndex - 1) * (mintBarWidth))
        'End If

        If blnGroupCol = False Then
            If mintX1 = 0 Then
                If mdsSource.Tables(0).Rows.Count = 31 Then
                    x1 = mintX1 + ((intIndex - 1) * mintGap) + (mintGap / 2) + 3
                ElseIf mdsSource.Tables.Count = 30 Then
                    x1 = mintX1 + ((intIndex - 1) * mintGap) + (mintGap / 2) + 12
                ElseIf mdsSource.Tables.Count = 29 Then
                    x1 = mintX1 + ((intIndex - 1) * mintGap) + (mintGap / 2) + 22
                Else
                    x1 = mintX1 + ((intIndex - 1) * mintGap) + (mintGap / 2) + 32
                End If
            Else
                x1 = mintX1 + (mintGap) + (mintGap / 2)
            End If
            Else
                x1 = mintX1
            End If
        mintX1 = x1 + mintBarWidth

        x2 = x1 + mintBarWidth
        y2 = intValue

        Dim pn As New Pen(colBar, 1)

        Dim col1 As New Color
        col1 = Color.FromArgb(255, Color.White)

        Dim col2 As New Color
        col2 = Color.FromArgb(255, getLightColor(colBar, 50))

        Dim col3 As New Color
        col3 = getLightColor(colBar, 100)

        If intValue = 0 Then intValue = 1

        Dim br As Drawing2D.LinearGradientBrush
        br = New Drawing2D.LinearGradientBrush(New Rectangle(x1, y1 - intValue, mintBarWidth / 2, intValue), col2, col1, Drawing2D.LinearGradientMode.Horizontal)

        Dim br2 As Drawing2D.LinearGradientBrush
        br2 = New Drawing2D.LinearGradientBrush(New Rectangle(x1, y1 - intValue, mintBarWidth / 2, intValue), col1, col2, Drawing2D.LinearGradientMode.Horizontal)


        Dim br1 As Drawing2D.LinearGradientBrush
        br1 = New Drawing2D.LinearGradientBrush(New Rectangle(x1, y1 - intValue, mintBarWidth, intValue), col3, col3, Drawing2D.LinearGradientMode.Horizontal)

        g.FillRectangle(br, New Rectangle(x1, y1 - intValue, mintBarWidth / 2, intValue))
        g.FillRectangle(br2, New Rectangle(x1 + mintBarWidth / 2 + 1, y1 - intValue, mintBarWidth / 2, intValue))
        'g.DrawRectangle(pn, New Rectangle(x1, y1 - intValue, mintBarWidth, intValue))
        g.DrawLine(pn, x1, y1 - intValue, x1, y1)
        g.DrawLine(pn, x2, y1 - intValue, x2, y1)
        g.FillEllipse(br, New Rectangle(x1, y1 - (mintElipsWidth / 2), mintBarWidth / 2, mintElipsWidth))
        g.FillEllipse(br2, New Rectangle(x1 + mintBarWidth / 2 + 1, y1 - (mintElipsWidth / 2), mintBarWidth / 2, mintElipsWidth))
        g.DrawArc(pn, New Rectangle(x1, y1 - (mintElipsWidth / 2), mintBarWidth, mintElipsWidth), 0, 180)
        g.FillEllipse(br1, New Rectangle(x1, y1 - intValue - (mintElipsWidth / 2), mintBarWidth, mintElipsWidth))
        g.DrawEllipse(pn, New Rectangle(x1, y1 - intValue - (mintElipsWidth / 2), mintBarWidth, mintElipsWidth))

        Dim fmt As New StringFormat
        fmt.Alignment = StringAlignment.Center

        If Val(strvalue) > 0 Then g.DrawString(strvalue, mfont, Brushes.Black, New Rectangle(x1, y1 - intValue - (mintElipsWidth / 2) - 20, mintBarWidth, 50), fmt)

        g.DrawString(strText, mfont, Brushes.Black, New Rectangle(x1, y1 + (mintElipsWidth / 2) + 10, mintBarWidth, 20), fmt)

        If blnGroupCol = False Then
            'g.DrawString(strGroupText, mfont, Brushes.Black, New Rectangle(x1, y1 + (mintElipsWidth / 2) + 25, mintGroupWidth, 20), fmt)
        End If
    End Sub

    Private Function getLightColor(ByVal c As Color, ByVal d As Byte) As Color
        Dim r As Byte = 255
        Dim g As Byte = 255
        Dim b As Byte = 255

        If (CInt(c.R) + CInt(d) <= 255) Then r = (c.R + d)
        If (CInt(c.G) + CInt(d) <= 255) Then g = (c.G + d)
        If (CInt(c.B) + CInt(d) <= 255) Then b = (c.B + d)

        Dim c2 As Color = Color.FromArgb(r, g, b)
        Return c2
    End Function

    Private Function GetMaxValue() As Double
        Dim mdblMax As Double = 0
        Try

            If Not mdsSource Is Nothing Then
                If mdsSource.Tables.Count > 0 Then
                    Dim i As Integer
                    For Each dr As DataRow In mdsSource.Tables(0).Rows
                        For i = 1 To mcolTextField.Count
                            If dr.Item(mcolValueField.Item(i).ToString) > mdblMax Then
                                mdblMax = dr.Item(mcolValueField.Item(i).ToString)
                            End If
                        Next
                    Next
                End If
            End If
            Return mdblMax
        Catch ex As Exception
            MsgBox(ex.Message)
            Return mdblMax
        End Try
    End Function

    Private Sub picBarChart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picBarChart.Click

    End Sub
End Class
