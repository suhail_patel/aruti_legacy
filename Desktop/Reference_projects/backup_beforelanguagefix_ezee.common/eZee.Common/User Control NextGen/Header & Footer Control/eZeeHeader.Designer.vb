<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeHeader
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.objlblHeaderText = New System.Windows.Forms.Label
        Me.objlblDescription = New System.Windows.Forms.Label
        Me.picIcon = New System.Windows.Forms.PictureBox
        CType(Me.picIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objlblHeaderText
        '
        Me.objlblHeaderText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objlblHeaderText.BackColor = System.Drawing.Color.Transparent
        Me.objlblHeaderText.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblHeaderText.Location = New System.Drawing.Point(10, 9)
        Me.objlblHeaderText.Name = "objlblHeaderText"
        Me.objlblHeaderText.Size = New System.Drawing.Size(518, 13)
        Me.objlblHeaderText.TabIndex = 0
        Me.objlblHeaderText.Text = "HeaderText"
        '
        'objlblDescription
        '
        Me.objlblDescription.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objlblDescription.BackColor = System.Drawing.Color.Transparent
        Me.objlblDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.objlblDescription.Location = New System.Drawing.Point(23, 27)
        Me.objlblDescription.Name = "objlblDescription"
        Me.objlblDescription.Size = New System.Drawing.Size(505, 27)
        Me.objlblDescription.TabIndex = 1
        Me.objlblDescription.Text = "Description"
        '
        'picIcon
        '
        Me.picIcon.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picIcon.BackColor = System.Drawing.Color.Transparent
        Me.picIcon.ErrorImage = Nothing
        Me.picIcon.InitialImage = Nothing
        Me.picIcon.Location = New System.Drawing.Point(540, 0)
        Me.picIcon.Name = "picIcon"
        Me.picIcon.Size = New System.Drawing.Size(60, 60)
        Me.picIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picIcon.TabIndex = 2
        Me.picIcon.TabStop = False
        '
        'eZeeHeader
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.picIcon)
        Me.Controls.Add(Me.objlblDescription)
        Me.Controls.Add(Me.objlblHeaderText)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "eZeeHeader"
        Me.Size = New System.Drawing.Size(600, 60)
        CType(Me.picIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objlblHeaderText As System.Windows.Forms.Label
    Friend WithEvents objlblDescription As System.Windows.Forms.Label
    Friend WithEvents picIcon As System.Windows.Forms.PictureBox

End Class
