Imports System
Imports System.Diagnostics
Imports System.Drawing

''' <summary> 
''' Enumeration for different shadow styles 
''' </summary> 
Public Enum ShadowStyle
    ''' <summary> 
    ''' No shadow. Sides are drawn in the same color as the top od the 
    ''' pie. 
    ''' </summary> 
    NoShadow
    ''' <summary> 
    ''' Uniform shadow. Sides are drawn somewhat darker. 
    ''' </summary> 
    UniformShadow
    ''' <summary> 
    ''' Gradual shadow is used to fully simulate 3-D shadow. 
    ''' </summary> 
    GradualShadow
End Enum

''' <summary> 
''' Enumeration for edge color types. 
''' </summary> 
Public Enum EdgeColorType
    ''' <summary> 
    ''' Edges are not drawn at all. 
    ''' </summary> 
    NoEdge
    ''' <summary> 
    ''' System (window text) color is used to draw edges. 
    ''' </summary> 
    SystemColor
    ''' <summary> 
    ''' Surface color is used for edges. 
    ''' </summary> 
    SurfaceColor
    ''' <summary> 
    ''' A color that is little darker than surface color is used for 
    ''' edges. 
    ''' </summary> 
    DarkerThanSurface
    ''' <summary> 
    ''' A color that is significantly darker than surface color is used 
    ''' for edges. 
    ''' </summary> 
    DarkerDarkerThanSurface
    ''' <summary> 
    ''' A color that is little lighter than surface color is used for 
    ''' edges. 
    ''' </summary> 
    LighterThanSurface
    ''' <summary> 
    ''' A color that is significantly lighter than surface color is used 
    ''' for edges. 
    ''' </summary> 
    LighterLighterThanSurface
    ''' <summary> 
    ''' Contrast color is used for edges. 
    ''' </summary> 
    Contrast
    ''' <summary> 
    ''' Enhanced contrast color is used for edges. 
    ''' </summary> 
    EnhancedContrast
    ''' <summary> 
    ''' Black color is used for light surfaces and white for dark 
    ''' surfaces. 
    ''' </summary> 
    FullContrast
End Enum

''' <summary> 
''' Structure representing edge color used for rendering. 
''' </summary> 
Public Class EdgeColor

    ''' <summary> 
    ''' Gets the actual color used for rendering. 
    ''' </summary> 
    Public Shared Function GetRenderingColor(ByVal edgeColorType As EdgeColorType, ByVal color As Color) As Color
        Debug.Assert(color <> color.Empty)
        If edgeColorType = edgeColorType.Contrast OrElse edgeColorType = edgeColorType.EnhancedContrast Then
            edgeColorType = GetContrastColorType(color, edgeColorType)
        End If
        Dim correctionFactor As Single = 0
        Select Case edgeColorType
            Case edgeColorType.SystemColor
                Return SystemColors.WindowText
            Case edgeColorType.SurfaceColor
                Return color
            Case edgeColorType.FullContrast
                Return GetFullContrastColor(color)
            Case edgeColorType.DarkerThanSurface
                correctionFactor = -ColorUtil.BrightnessEnhancementFactor1
                Exit Select
            Case edgeColorType.DarkerDarkerThanSurface
                correctionFactor = -ColorUtil.BrightnessEnhancementFactor2
                Exit Select
            Case edgeColorType.LighterThanSurface
                correctionFactor = +ColorUtil.BrightnessEnhancementFactor1
                Exit Select
            Case edgeColorType.LighterLighterThanSurface
                correctionFactor = +ColorUtil.BrightnessEnhancementFactor2
                Exit Select
            Case edgeColorType.NoEdge
                Return System.Drawing.Color.Transparent
        End Select
        Return ColorUtil.CreateColorWithCorrectedLightness(color, correctionFactor)
    End Function

    Private Shared Function GetContrastColorType(ByVal color As Color, ByVal colorType As EdgeColorType) As EdgeColorType
        Debug.Assert(colorType = EdgeColorType.Contrast OrElse colorType = EdgeColorType.EnhancedContrast)
        If color.GetBrightness() > s_brightnessThreshold Then
            If colorType = EdgeColorType.Contrast Then
                Return EdgeColorType.DarkerThanSurface
            Else
                Return EdgeColorType.DarkerDarkerThanSurface
            End If
        End If
        If colorType = EdgeColorType.Contrast Then
            Return EdgeColorType.LighterThanSurface
        Else
            Return EdgeColorType.LighterLighterThanSurface
        End If
    End Function

    Private Shared Function GetFullContrastColor(ByVal color As Color) As Color
        If color.GetBrightness() > s_brightnessThreshold Then
            Return Drawing.Color.Black
        End If
        Return Drawing.Color.White
    End Function

    Private Shared ReadOnly s_brightnessThreshold As Single = 0.4F
End Class

''' <summary> 
''' Color utility structure. 
''' </summary> 
Public Class ColorUtil
    ''' <summary> 
    ''' Creates color with corrected lightness. 
    ''' </summary> 
    ''' <param name="color"> 
    ''' Color to correct. 
    ''' </param> 
    ''' <param name="correctionFactor"> 
    ''' Correction factor, with a value between -1 and 1. Negative values 
    ''' create darker color, positive values lighter color. Zero value 
    ''' returns the current color. 
    ''' </param> 
    ''' <returns> 
    ''' Corrected <c>Color</c> structure. 
    ''' </returns> 
    Public Shared Function CreateColorWithCorrectedLightness(ByVal color As Color, ByVal correctionFactor As Single) As Color
        Debug.Assert(correctionFactor <= 1 AndAlso correctionFactor >= -1)
        If correctionFactor = 0 Then
            Return color
        End If
        Dim red As Single = CSng(color.R)
        Dim green As Single = CSng(color.G)
        Dim blue As Single = CSng(color.B)
        If correctionFactor < 0 Then
            correctionFactor = 1 + correctionFactor
            red *= correctionFactor
            green *= correctionFactor
            blue *= correctionFactor
        Else
            red = (255 - red) * correctionFactor + red
            green = (255 - green) * correctionFactor + green
            blue = (255 - blue) * correctionFactor + blue
        End If
        Return System.Drawing.Color.FromArgb(color.A, CInt(red), CInt(green), CInt(blue))
    End Function

    ''' <summary> 
    ''' Small brightness change factor. 
    ''' </summary> 
    Public Shared ReadOnly BrightnessEnhancementFactor1 As Single = 0.3F
    ''' <summary> 
    ''' Large brightness change factor. 
    ''' </summary> 
    Public Shared ReadOnly BrightnessEnhancementFactor2 As Single = 0.5F
End Class
