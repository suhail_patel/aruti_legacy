Imports System
Imports System.Diagnostics
Imports System.Drawing
Imports System.Drawing.Drawing2D

''' <summary> 
''' Quadrilateral object. 
''' </summary> 

Public Class Quadrilateral
    ''' <summary> 
    ''' Creates empty <c>Quadrilateral</c> object 
    ''' </summary> 
    Protected Sub New()
    End Sub

    ''' <summary> 
    ''' Initilizes <c>Quadrilateral</c> object with given corner points. 
    ''' </summary> 
    ''' <param name="point1"> 
    ''' First <c>PointF</c>. 
    ''' </param> 
    ''' <param name="point2"> 
    ''' Second <c>PointF</c>. 
    ''' </param> 
    ''' <param name="point3"> 
    ''' Third <c>PointF</c>. 
    ''' </param> 
    ''' <param name="point4"> 
    ''' Fourth <c>PointF</c>. 
    ''' </param> 
    ''' <param name="toClose"> 
    ''' Indicator should the quadrilateral be closed by the line. 
    ''' </param> 
    Public Sub New(ByVal point1 As PointF, ByVal point2 As PointF, ByVal point3 As PointF, ByVal point4 As PointF, ByVal toClose As Boolean)
        Dim pointTypes As Byte() = s_quadrilateralPointTypes.Clone()
        If toClose Then
            pointTypes(3) = pointTypes(3) Or CByte(PathPointType.CloseSubpath)
        End If
        m_path = New GraphicsPath(New PointF() {point1, point2, point3, point4}, pointTypes)
    End Sub

    ''' <summary> 
    ''' <c>Finalize</c> method. 
    ''' </summary> 
    Protected Overrides Sub Finalize()
        Try
            Dispose(False)
        Finally
            MyBase.Finalize()
        End Try
    End Sub

    ''' <summary> 
    ''' Implementation of <c>IDisposable</c> interface. 
    ''' </summary> 
    Public Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> 
    ''' Disposes of all pie slices. 
    ''' </summary> 
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not m_disposed Then
            If disposing Then
                m_path.Dispose()
            End If
            m_disposed = True
        End If
    End Sub

    ''' <summary> 
    ''' Draws the <c>Quadrilateral</c> with <c>Graphics</c> provided. 
    ''' </summary> 
    ''' <param name="graphics"> 
    ''' <c>Graphics</c> used to draw. 
    ''' </param> 
    ''' <param name="pen"> 
    ''' <c>Pen</c> used to draw outline. 
    ''' </param> 
    ''' <param name="brush"> 
    ''' <c>Brush</c> used to fill the inside. 
    ''' </param> 
    Public Sub Draw(ByVal graphics As Graphics, ByVal pen As Pen, ByVal brush As Brush)
        graphics.FillPath(brush, m_path)
        graphics.DrawPath(pen, m_path)
    End Sub

    ''' <summary> 
    ''' Checks if the given <c>PointF</c> is contained within the 
    ''' quadrilateral. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> structure to check for. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if the point is contained within the quadrilateral. 
    ''' </returns> 
    Public Function Contains(ByVal point As PointF) As Boolean
        If m_path.PointCount = 0 OrElse m_path.PathPoints.Length = 0 Then
            Return False
        End If
        Return Contains(point, m_path.PathPoints)
    End Function

    ''' <summary> 
    ''' Checks if given <c>PointF</c> is contained within quadrilateral 
    ''' defined by <c>cornerPoints</c> provided. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> to check. 
    ''' </param> 
    ''' <param name="cornerPoints"> 
    ''' Array of <c>PointF</c> structures defining corners of the 
    ''' quadrilateral. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if the point is contained within the quadrilateral. 
    ''' </returns> 
    Public Shared Function Contains(ByVal point As PointF, ByVal cornerPoints As PointF()) As Boolean
        Dim intersections As Integer = 0
        For i As Integer = 1 To cornerPoints.Length - 1
            If DoesIntersect(point, cornerPoints(i), cornerPoints(i - 1)) Then
                intersections += 1
            End If
        Next
        If DoesIntersect(point, cornerPoints(cornerPoints.Length - 1), cornerPoints(0)) Then
            intersections += 1
        End If
        Return (intersections Mod 2 <> 0)
    End Function

    ''' <summary> 
    ''' Checks if the line coming out of the <c>point</c> downwards 
    ''' intersects with a line through <c>point1</c> and <c>point2</c>. 
    ''' </summary> 
    ''' <param name="point"> 
    ''' <c>PointF</c> from which vertical line is drawn downwards. 
    ''' </param> 
    ''' <param name="point1"> 
    ''' First <c>PointF</c> through which line is drawn. 
    ''' </param> 
    ''' <param name="point2"> 
    ''' Second <c>PointF</c> through which line is drawn. 
    ''' </param> 
    ''' <returns> 
    ''' <c>true</c> if lines intersect. 
    ''' </returns> 
    Private Shared Function DoesIntersect(ByVal point As PointF, ByVal point1 As PointF, ByVal point2 As PointF) As Boolean
        Dim x2 As Single = point2.X
        Dim y2 As Single = point2.Y
        Dim x1 As Single = point1.X
        Dim y1 As Single = point1.Y
        If (x2 < point.X AndAlso x1 >= point.X) OrElse (x2 >= point.X AndAlso x1 < point.X) Then
            Dim y As Single = (y2 - y1) / (x2 - x1) * (point.X - x1) + y1
            Return y > point.Y
        End If
        Return False
    End Function

    ''' <summary> 
    ''' <c>GraphicsPath</c> representing the quadrilateral. 
    ''' </summary> 
    Private m_path As New GraphicsPath()

    Private m_disposed As Boolean = False

    ''' <summary> 
    ''' <c>PathPointType</c>s decribing the <c>GraphicsPath</c> points. 
    ''' </summary> 
    Private Shared s_quadrilateralPointTypes As Byte() = New Byte() {CByte(PathPointType.Start), CByte(PathPointType.Line), CByte(PathPointType.Line), CByte(PathPointType.Line) Or CByte(PathPointType.CloseSubpath)}

    Public Shared ReadOnly Empty As New Quadrilateral()
End Class
