Imports System
Imports System.ComponentModel
Imports System.Runtime.InteropServices
Imports System.Drawing
Imports System.Drawing.Design
Imports System.Windows.Forms.Design
Imports System.Windows.Forms


Public Class eZeeBaseListView
    Inherits ListView

    Private cpHeader As New ContextMenuStrip
    Private chkColumnList As ColumnShowHideEditor
    Private mintLastGroupColumnIndex As Integer = -1
    Private mstrNecessaryColumns As String = ""
    Private mstrVisibleColumnList As String = ""
    Private mstrCheckedItems As String = ""
    Private mintMinColumnWidth As Integer = 50
    Private mblnShowSelectAll As Boolean = True


    Private mblnShow_SaveItem As Boolean = False
    Private mblnShow_MoreItem As Boolean = False
    Private mblnShow_SizeAllColumnsToFit As Boolean = True

    Private _headerRect As Rectangle

    Private mdicColumns As Dictionary(Of Integer, eZeeToolStripMenuItem)

    Public Event HeaderItemClick As EventHandler
    Public Event MoreItemClick As EventHandler
    Public Event ShowAllItemClick As EventHandler
    Public Event SizeAllColumnToFitItemClick As EventHandler
    Public Event SaveItemClick As EventHandler

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure RECT
        Public Left As Integer
        Public Top As Integer
        Public Right As Integer
        Public Bottom As Integer
    End Structure

#Region " Constructor "

    Public Sub New()
        Me.View = Windows.Forms.View.Details
        Me.MultiSelect = False

        Me.HideSelection = False
        Me.FullRowSelect = True
        Me.GridLines = True

        SetStyle(ControlStyles.SupportsTransparentBackColor, True)
        SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)
    End Sub

#End Region

#Region " Control Declaration "

    Friend WithEvents cpOperation As System.Windows.Forms.ContextMenuStrip

#End Region

#Region " API Declaration "

    ''' <summary>
    ''' Delegate that is called for each child window of the ListView.
    ''' </summary>
    Private Delegate Function EnumWinCallBack(ByVal hwnd As IntPtr, ByVal lParam As IntPtr) As Boolean

    ''' <summary>
    ''' Calls EnumWinCallBack for each child window of hWndParent (i.e. the ListView).
    ''' </summary>
    <DllImport("user32.Dll")> _
    Private Shared Function EnumChildWindows(ByVal hWndParent As IntPtr, ByVal callBackFunc As EnumWinCallBack, ByVal lParam As IntPtr) As Integer
    End Function

    ''' <summary>
    ''' Gets the bounding rectangle of the specified window (ListView header bar).
    ''' </summary>
    <DllImport("user32.dll")> _
    Private Shared Function GetWindowRect(ByVal hWnd As IntPtr, ByRef lpRect As RECT) As Boolean
    End Function

#End Region

#Region " Properties "

    Public Overrides Property ContextMenuStrip() As System.Windows.Forms.ContextMenuStrip
        Get
            cpOperation = MyBase.ContextMenuStrip
            Return cpOperation
        End Get
        Set(ByVal value As System.Windows.Forms.ContextMenuStrip)
            MyBase.ContextMenuStrip = value
            cpOperation = value
        End Set
    End Property

    Public Property ContextMenuStripHeader() As ContextMenuStrip
        Get
            Return cpHeader
        End Get
        Set(ByVal value As ContextMenuStrip)
            cpHeader = value
            Me.Invalidate()
        End Set
    End Property

    Public Property MinColumnWidth() As Integer
        Get
            Return mintMinColumnWidth
        End Get
        Set(ByVal value As Integer)
            If value < 30 Then
                MsgBox("You can't set minimum column width less than 30.", MsgBoxStyle.Critical, "eZee ListView")
                Exit Property
            End If
            mintMinColumnWidth = value
        End Set
    End Property

    <Editor(GetType(NecessaryColumnListEditor), GetType(UITypeEditor)), Description("Necessary Column List")> _
    Public Property CompulsoryColumns() As String
        Get
            Return mstrNecessaryColumns
        End Get
        Set(ByVal value As String)
            mstrNecessaryColumns = value
            'For Each str As String In mstrNecessaryColumns.Split(CChar(","))
            '    If Not Me.VisibleColumnList.Contains(str) Then
            '        If Me.VisibleColumnList.Length < 1 Then
            '            Me.VisibleColumnList &= str
            '        Else
            '            Me.VisibleColumnList &= "," + str
            '        End If
            '    End If
            'Next
            Me.Invalidate()
        End Set
    End Property

    <Editor(GetType(ColumnListEditor), GetType(UITypeEditor)), Description("Visible Column List")> _
    Public Property OptionalColumns() As String
        Get
            Return mstrVisibleColumnList
        End Get
        Set(ByVal value As String)
            mstrVisibleColumnList = value
            Me.Invalidate()
        End Set
    End Property

    Public Property ColumnHeaders() As Dictionary(Of Integer, eZeeToolStripMenuItem)
        Get
            Return mdicColumns
        End Get
        Set(ByVal value As Dictionary(Of Integer, eZeeToolStripMenuItem))
            mdicColumns = value
        End Set
    End Property

    Public Property ShowMoreItem() As Boolean
        Get
            Return mblnShow_MoreItem
        End Get
        Set(ByVal value As Boolean)
            mblnShow_MoreItem = value
        End Set
    End Property

    Public Property ShowSaveItem() As Boolean
        Get
            Return mblnShow_SaveItem
        End Get
        Set(ByVal value As Boolean)
            mblnShow_SaveItem = value
        End Set
    End Property

    Public Property ShowSizeAllColumnsToFit() As Boolean
        Get
            Return mblnShow_SizeAllColumnsToFit
        End Get
        Set(ByVal value As Boolean)
            mblnShow_SizeAllColumnsToFit = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Private Sub ToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim tsMenuItem As eZeeToolStripMenuItem = CType(sender, eZeeToolStripMenuItem)
        Me.SuspendLayout()
        If tsMenuItem.Image Is Nothing Then
            ShowColumn(tsMenuItem.BindedColumn, tsMenuItem)
        Else
            HideColumn(tsMenuItem.BindedColumn, tsMenuItem)
        End If
        RaiseEvent HeaderItemClick(sender, e)
        Me.ResumeLayout()
    End Sub

    ''' <summary>
    ''' This returns an array of ColumnHeaders in the order they are displayed by the ListView.  
    ''' </summary>
    Private Shared Function GetOrderedHeaders(ByVal lv As ListView) As ColumnHeader()
        Dim arr As ColumnHeader() = New ColumnHeader(lv.Columns.Count - 1) {}

        For Each header As ColumnHeader In lv.Columns
            arr(header.DisplayIndex) = header
        Next

        Return arr
    End Function

    ''' <summary>
    ''' Called when the specified column header is right-clicked.
    ''' </summary>
    Private Sub HandleRightClickOnHeader(ByVal header As ColumnHeader)
        'We can do anything here, but most likely we want to 
        'display a context menu for the header.  This code
        'displays the same context menu for every header, but
        'changes the menu item text based on the header.
        'It sets the context menu tag to the specified header so the 
        'handler for whatever command the user clicks can know the column.
        cpHeader.Tag = header
        'cpHeader.Items(0).Text = "Command for Header " & header.Text
        If Not cpHeader.Items.Count > 0 Then
            'Call CreateHeaderItems()
        End If
        cpHeader.Show(Control.MousePosition)
    End Sub

    ''' <summary>
    ''' This should get called with the only child window of the ListView, which should be the header bar.
    ''' </summary>
    Private Function EnumWindowCallBack(ByVal hwnd As IntPtr, ByVal lParam As IntPtr) As Boolean
        ' Determine the rectangle of the ListView header bar and save it in _headerRect.
        Dim rct As RECT

        If Not GetWindowRect(hwnd, rct) Then
            _headerRect = Rectangle.Empty
        Else
            _headerRect = New Rectangle(rct.Left, rct.Top, rct.Right - rct.Left, rct.Bottom - rct.Top)
        End If

        ' Stop the enum
        Return False
    End Function

    Private Sub ShowAllMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call Me.ShowAllColumns()
        RaiseEvent HeaderItemClick(sender, e)
    End Sub

    Private Sub objtsmiMore_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        RaiseEvent MoreItemClick(sender, e)
    End Sub

    Private Sub objtsmiSizeAllColumnToFit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call Me.SizeAllColumnsToFit()
        RaiseEvent SizeAllColumnToFitItemClick(sender, e)
    End Sub

    Private Sub objtsmiSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        RaiseEvent SaveItemClick(sender, e)
    End Sub

#End Region

#Region " Public Methods "

    Public Sub CreateHeaderItems()

        If cpHeader Is Nothing Then Exit Sub
        If mdicColumns Is Nothing Then
            mdicColumns = New Dictionary(Of Integer, eZeeToolStripMenuItem)
        End If

        If Me.Columns.Count > 0 Then
            cpHeader.Items.Clear()
            mdicColumns.Clear()
            'Dim arrDeletedColumns As New ArrayList
            Dim cpMenuItem As eZeeToolStripMenuItem

            For Each col As ColumnHeader In Me.Columns
                If Me.CheckBoxes = True And col.Index = 0 Then Continue For
                If col.Width = 0 Then Continue For

                cpMenuItem = New eZeeToolStripMenuItem

                mdicColumns.Add(col.DisplayIndex, cpMenuItem)

                cpMenuItem.Name = "objtsmi" + col.Tag.ToString()
                cpMenuItem.Text = col.Text
                cpMenuItem.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText
                cpMenuItem.BindedColumn = col
                cpMenuItem.BindedColumnIndex = col.DisplayIndex
                cpMenuItem.BindedColumnWidth = col.Width

                AddHandler cpMenuItem.Click, AddressOf ToolStripMenuItem_Click

                If Me.CompulsoryColumns.Contains(col.Tag.ToString) Then
                    'Continue For
                    cpMenuItem.Enabled = False
                    cpMenuItem.Image = My.Resources.Check_16

                    cpHeader.Items.Add(cpMenuItem)
                    'Krushna (FD GUI) (10 Aug 2009) -- Start
                    Continue For
                    'Krushna (FD GUI) (10 Aug 2009) -- End
                End If

                If Not Me.OptionalColumns.Contains(col.Tag.ToString) Then
                    col.Width = 0
                    'arrDeletedColumns.Add(col)
                    cpMenuItem.Image = Nothing
                    'Continue For
                Else
                    cpMenuItem.Image = My.Resources.Check_16
                End If

                cpHeader.Items.Add(cpMenuItem)
                cpMenuItem = Nothing
            Next

            Dim cpSep As New ToolStripSeparator
            cpHeader.Items.Add(cpSep)

            'Show All
            cpMenuItem = New eZeeToolStripMenuItem
            cpMenuItem.Text = eZee.Common.Ui.Language.ListView.Caption_ShowAll
            cpMenuItem.Name = "objShowAll"
            AddHandler cpMenuItem.Click, AddressOf ShowAllMenu_Click
            cpHeader.Items.Add(cpMenuItem)

            'More...
            If mblnShow_MoreItem Then
                cpMenuItem = New eZeeToolStripMenuItem
                cpMenuItem.Text = eZee.Common.Ui.Language.ListView.Caption_More
                cpMenuItem.Name = "objtsmiMore"
                AddHandler cpMenuItem.Click, AddressOf objtsmiMore_Click
                cpHeader.Items.Add(cpMenuItem)
            End If

            'Size All Columns to Fit]
            cpMenuItem = New eZeeToolStripMenuItem
            cpMenuItem.Text = eZee.Common.Ui.Language.ListView.Caption_SizeAllColumnsToFit
            cpMenuItem.Name = "objtsmiSizeAllColumnsToFit"
            AddHandler cpMenuItem.Click, AddressOf objtsmiSizeAllColumnToFit_Click
            cpHeader.Items.Add(cpMenuItem)

            'Save
            If mblnShow_SaveItem Then
                cpMenuItem = New eZeeToolStripMenuItem
                cpMenuItem.Text = eZee.Common.Ui.Language.ListView.Caption_Save
                cpMenuItem.Name = "objtsmiSave"
                AddHandler cpMenuItem.Click, AddressOf objtsmiSave_Click
                cpHeader.Items.Add(cpMenuItem)
            End If
        End If
    End Sub

    Public Sub ShowAllColumns()
        With Me.ContextMenuStripHeader
            For i As Integer = 0 To .Items.Count - 1
                If TypeOf .Items(i) Is ToolStripSeparator Then Exit For

                Dim cpItem As eZeeToolStripMenuItem = CType(.Items(i), eZeeToolStripMenuItem)
                If cpItem.Enabled = False Then Continue For
                If cpItem.Image Is Nothing Then
                    ShowColumn(cpItem.BindedColumn, cpItem)
                End If
            Next
        End With
    End Sub

    Public Sub SizeAllColumnsToFit()
        Dim intMaxColHeaderWidth As Integer
        Dim g As Graphics = Me.CreateGraphics
        Me.BeginUpdate()
        For Each col As ColumnHeader In Me.Columns
            If col.Tag Is Nothing Then Continue For
            If col.Tag.ToString.StartsWith("obj") Then Continue For
            If col.Width = 0 Then Continue For
            If Me.CheckBoxes And col.Index = 0 Then Continue For

            col.AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)

            intMaxColHeaderWidth = CInt(g.MeasureString(col.Text, Me.Font).Width) + 10
            If intMaxColHeaderWidth > col.Width Then
                col.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
            End If
        Next
        Me.EndUpdate()
        g = Nothing
    End Sub

    Private Sub HideColumn(ByVal header As ColumnHeader, ByVal menuItem As eZeeToolStripMenuItem)
        If Me.CompulsoryColumns.Contains(header.Tag.ToString) Then Exit Sub
        header.Width = 0
        Me.OptionalColumns.Replace(header.Tag.ToString + ",", "")
        menuItem.Image = Nothing
    End Sub

    Public Sub HideColumn(ByVal index As Integer)
        If index = mdicColumns.Count Then
            Throw New IndexOutOfRangeException()
        End If
        HideColumn(mdicColumns(index).BindedColumn, mdicColumns(index))
    End Sub

    Public Sub HideColumn(ByVal col As ColumnHeader)
        HideColumn(col.DisplayIndex)
    End Sub

    Private Sub ShowColumn(ByVal header As ColumnHeader, ByVal menuItem As eZeeToolStripMenuItem)
        header.Width = menuItem.BindedColumnWidth
        Me.OptionalColumns &= "," + header.Tag.ToString
        menuItem.Image = My.Resources.Check_16
    End Sub

    Public Sub ShowColumn(ByVal index As Integer)
        If index = mdicColumns.Count Then
            Throw New IndexOutOfRangeException()
        End If
        ShowColumn(mdicColumns(index).BindedColumn, mdicColumns(index))
    End Sub

    Public Sub ShowColumn(ByVal col As ColumnHeader)
        ShowColumn(col.DisplayIndex)
    End Sub

    Public Sub UpdateColumnHeaders()
        If cpHeader Is Nothing Then Exit Sub
        If mdicColumns Is Nothing Then Exit Sub

        mdicColumns.Clear()
        Dim item As eZeeToolStripMenuItem = Nothing
        For Each mnuItem As ToolStripItem In Me.cpHeader.Items
            If TypeOf mnuItem Is ToolStripSeparator Then Exit For
            item = CType(mnuItem, eZeeToolStripMenuItem)

            mdicColumns.Add(item.BindedColumn.DisplayIndex, item)
        Next
    End Sub


#End Region


#Region " Override Events "

    Protected Overrides Sub OnColumnReordered(ByVal e As System.Windows.Forms.ColumnReorderedEventArgs)
        MyBase.OnColumnReordered(e)

        If Me.CheckBoxes Then
            If e.OldDisplayIndex = 0 Then
                e.Cancel = True
            End If
        End If

        If cpHeader Is Nothing Then Exit Sub
        'Dim item As eZeeToolStripMenuItem





        'item = mdicColumns(e.OldDisplayIndex)
        'mdicColumns(e.OldDisplayIndex) = mdicColumns(e.NewDisplayIndex)
        'mdicColumns(e.NewDisplayIndex) = item

        'item = Nothing

        Try
            Me.UpdateColumnHeaders()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        'item = Nothing
    End Sub

    Protected Overrides Sub OnColumnWidthChanging(ByVal e As System.Windows.Forms.ColumnWidthChangingEventArgs)
        MyBase.OnColumnWidthChanging(e)
        Dim blnAllowColumnChanging As Boolean = True

        'If Me.ContextMenuStripHeader IsNot Nothing Then
        '    For Each mnuItem As ToolStripItem In Me.ContextMenuStripHeader.Items
        '        If TypeOf mnuItem Is ToolStripSeparator Then Exit For
        '        If CType(mnuItem, eZeeToolStripMenuItem).BindedColumn Is Me.Columns(e.ColumnIndex) Then
        '            If mnuItem.Image Is Nothing Then
        '                blnAllowColumnChanging = False
        '            End If
        '        End If
        '    Next
        'End If

        If Not blnAllowColumnChanging Then
            e.Cancel = True
            e.NewWidth = 0
            Exit Sub
        ElseIf e.ColumnIndex = 0 Then
            If Me.CheckBoxes = True Then
                e.Cancel = True
                e.NewWidth = 25
            End If
        End If

        If e.NewWidth < Me.MinColumnWidth Then
            e.Cancel = True
            e.NewWidth = Me.MinColumnWidth
        End If
    End Sub

#End Region

#Region " Control's Events "

    ''' <summary>
    ''' Called when the user right-clicks anywhere in the ListView, including the header bar.  It displays the appropriate context menu for the data row or header that was right-clicked. 
    ''' </summary>
    Private Sub cpOperation_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cpOperation.Opening
        If Me.ContextMenuStripHeader Is Nothing Then Exit Sub

        ' This call indirectly calls EnumWindowCallBack which sets _headerRect
        ' to the area occupied by the ListView's header bar.
        EnumChildWindows(Me.Handle, New EnumWinCallBack(AddressOf EnumWindowCallBack), IntPtr.Zero)

        ' If the mouse position is in the header bar, cancel the display
        ' of the regular context menu and display the column header context menu instead.
        If _headerRect.Contains(Control.MousePosition) Then
            e.Cancel = True

            ' The xoffset is how far the mouse is from the left edge of the header.
            Dim xoffset As Integer = Control.MousePosition.X - _headerRect.Left

            ' Iterate through the column headers in the order they are displayed, adding up
            ' their widths as we go.  When the sum exceeds the xoffset, we know the mouse
            ' is on the current header. 
            Dim sum As Integer = 0
            For Each header As ColumnHeader In GetOrderedHeaders(Me)
                sum += header.Width
                If sum > xoffset Then
                    HandleRightClickOnHeader(header)
                    Exit For
                End If
            Next
        Else
            'Allow the regular context menu to be displayed.
            'We may want to update the menu here.
        End If
    End Sub

#End Region

#Region " Friend Class "

    Friend Class ColumnListEditor
        Inherits UITypeEditor

        Public Overloads Overrides Function GetEditStyle(ByVal context As ITypeDescriptorContext) As UITypeEditorEditStyle
            If Not context Is Nothing Then
                Return UITypeEditorEditStyle.DropDown
            End If
            Return (MyBase.GetEditStyle(context))
        End Function

        Public Overloads Overrides Function EditValue(ByVal context As ITypeDescriptorContext, ByVal provider As IServiceProvider, ByVal value As Object) As Object
            If (Not context Is Nothing) And (Not provider Is Nothing) Then
                ' Access the property browser's UI display service, IWindowsFormsEditorService
                Dim editorService As IWindowsFormsEditorService = CType(provider.GetService(GetType(IWindowsFormsEditorService)), IWindowsFormsEditorService)
                If Not editorService Is Nothing Then
                    ' Create an instance of the UI editor, passing a reference to the editor service
                    Dim dropDownEditor As ColumnShowHideEditor = New ColumnShowHideEditor(editorService)

                    '' Pass the UI editor the current property value
                    Dim Instance As eZeeBaseListView = Nothing
                    If context.Instance.GetType Is GetType(eZeeBaseListView) Then
                        Instance = CType(context.Instance, eZeeBaseListView)
                        'Else
                        '    'This is needed if using in a SmartTag
                        '    Instance = CType(context.Instance.CurrShape, eZeeListView)
                    End If
                    dropDownEditor.eZeeLView = Instance
                    dropDownEditor.IsVisibleColumnListProperty = True

                    ' Display the UI editor
                    editorService.DropDownControl(dropDownEditor)

                    ' Return the new property value from the editor
                    Return dropDownEditor.eZeeLView.OptionalColumns
                End If
            End If
            Return MyBase.EditValue(context, provider, value)
        End Function

    End Class

#End Region 'Visible Column List Editor

#Region " Friend Class "

    Friend Class NecessaryColumnListEditor
        Inherits UITypeEditor

        Public Overloads Overrides Function GetEditStyle(ByVal context As ITypeDescriptorContext) As UITypeEditorEditStyle
            If Not context Is Nothing Then
                Return UITypeEditorEditStyle.DropDown
            End If
            Return (MyBase.GetEditStyle(context))
        End Function

        Public Overloads Overrides Function EditValue(ByVal context As ITypeDescriptorContext, ByVal provider As IServiceProvider, ByVal value As Object) As Object
            If (Not context Is Nothing) And (Not provider Is Nothing) Then
                ' Access the property browser's UI display service, IWindowsFormsEditorService
                Dim editorService As IWindowsFormsEditorService = CType(provider.GetService(GetType(IWindowsFormsEditorService)), IWindowsFormsEditorService)
                If Not editorService Is Nothing Then
                    ' Create an instance of the UI editor, passing a reference to the editor service
                    Dim dropDownEditor As ColumnShowHideEditor = New ColumnShowHideEditor(editorService)

                    '' Pass the UI editor the current property value
                    Dim Instance As eZeeListView = Nothing
                    If context.Instance.GetType Is GetType(eZeeListView) Then
                        Instance = CType(context.Instance, eZeeListView)
                        'Else
                        '    'This is needed if using in a SmartTag
                        '    Instance = CType(context.Instance.CurrShape, eZeeListView)
                    End If
                    dropDownEditor.eZeeLView = Instance
                    dropDownEditor.IsVisibleColumnListProperty = False

                    ' Display the UI editor
                    editorService.DropDownControl(dropDownEditor)

                    ' Return the new property value from the editor
                    Return dropDownEditor.eZeeLView.OptionalColumns
                End If
            End If
            Return MyBase.EditValue(context, provider, value)
        End Function

    End Class

#End Region 'Necessary Column List Editor

#Region " Disable VSplit Cursor "

    Private mHeader As HeaderWindow

    Private Const WM_CREATE As Integer = &H1
    Private Const WM_DESTROY As Integer = &H2
    Private Const WM_NOTIFY As Integer = &H4E
    Private Const LVM_GETHEADER As Integer = &H101F
    Private Const HDN_FIRST As Integer = -300
    Private Const HDN_BEGINTRACK As Integer = HDN_FIRST - 26
    Public Shared mcolHoveredColumn As ColumnHeader = Nothing

    Public mblnResizeColumn As Boolean = True

    <System.Diagnostics.DebuggerStepThrough()> _
    Protected Overloads Overrides Sub WndProc(ByRef m As Message)
        'Console.WriteLine(m.ToString());
        If m.Msg = WM_DESTROY Then
            ' Un-subclass header control
            If mHeader IsNot Nothing Then
                mHeader.ReleaseHandle()
            End If
            mHeader = Nothing
        End If

        MyBase.WndProc(m)
        If m.Msg = WM_CREATE Then
            ' Subclass the header control
            Dim hWnd As IntPtr = SendMessage(Me.Handle, LVM_GETHEADER, IntPtr.Zero, IntPtr.Zero)
            If mHeader Is Nothing OrElse mHeader.Handle <> hWnd Then
                If mHeader IsNot Nothing Then
                    mHeader.ReleaseHandle()
                Else
                    mHeader = New HeaderWindow(Me)
                End If
                mHeader.AssignHandle(hWnd)
            End If
        End If
    End Sub

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure NMHDR
        Public hWndFrom As Integer
        Public idFrom As Integer
        Public code As Integer
    End Structure

    <DllImport("user32.dll")> _
    Private Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal wp As IntPtr, ByVal lp As IntPtr) As IntPtr
    End Function

#End Region

End Class

<System.Diagnostics.DebuggerStepThrough()> _
Public Class HeaderWindow
    Inherits NativeWindow

    'Krushna (FD GUI) -- End
    Private Const WM_SETCURSOR As Integer = &H20
    Private Const WM_LBUTTONDBLCLICK As Integer = &H203
    Private Const WM_LBUTTONDOWN As Integer = &H201

    Dim lvList As ListView

    Private Enum EnumLockResult
        Lock = 0
        Unlock = 1
        Other = -1
    End Enum

    Public Sub New(ByVal lvView As ListView)
        lvList = lvView
    End Sub

    Protected Overloads Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = WM_SETCURSOR Then
            If IsVSplitCursorOverHiddenColumn Then
                m.Msg = 0
            End If
        End If
        If m.Msg = WM_LBUTTONDOWN Then
            If IsVSplitCursorOverHiddenColumn Then
                m.Msg = 0
                Exit Sub
            End If
        End If
        If m.Msg = WM_LBUTTONDBLCLICK Then
            ' Prevent double-click from changing column
            If IsVSplitCursorOverHiddenColumn Then
                Exit Sub
            End If
        End If
        MyBase.WndProc(m)
    End Sub

    Public ReadOnly Property IsVSplitCursorOverHiddenColumn() As Boolean
        Get
            Dim pt As Point = Me.lvList.PointToClient(Cursor.Position)
            pt.X += ListViewMethods.GetScrollPosition(Me.lvList.Handle, True)
            Dim dividerIndex As Integer = ListViewMethods.GetDividerUnderPoint(Me.Handle, pt)
            Dim columnIndex As Integer = ListViewMethods.GetColumnUnderPoint(Me.Handle, pt)

            'Me.lvList.Columns(1).Text = pt.X.ToString
            'Me.lvList.Columns(2).Text = Cursor.Position.X.ToString
            'Me.lvList.Columns(3).Text = lvList.GetHeaderRectangle.Left.ToString

            If dividerIndex = 0 And columnIndex = -1 Then
                If lvList.CheckBoxes = True Then
                    Return True
                Else
                    Return False
                End If
            ElseIf dividerIndex = -1 And columnIndex = -1 Then
                'Me.lvList.Columns(1).Text = pt.X.ToString
                'Me.lvList.Columns(2).Text = Cursor.Position.X.ToString 'columnIndex.ToString

                'If lvList.GetHeaderRectangle.Contains(Cursor.Position) Then
                '    Return True
                'End If
                'Return False
                Return True
            ElseIf dividerIndex > 0 And columnIndex = -1 Then
                If Me.lvList.Columns(dividerIndex).Width = 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        End Get
    End Property

End Class
