<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VirtualColumnShowHideEditor
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvColumnsView = New System.Windows.Forms.ListView
        Me.colhColumns = New System.Windows.Forms.ColumnHeader
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lvColumnsView
        '
        Me.lvColumnsView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvColumnsView.CheckBoxes = True
        Me.lvColumnsView.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhColumns})
        Me.lvColumnsView.FullRowSelect = True
        Me.lvColumnsView.GridLines = True
        Me.lvColumnsView.HideSelection = False
        Me.lvColumnsView.Location = New System.Drawing.Point(0, 0)
        Me.lvColumnsView.MultiSelect = False
        Me.lvColumnsView.Name = "lvColumnsView"
        Me.lvColumnsView.Size = New System.Drawing.Size(256, 142)
        Me.lvColumnsView.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lvColumnsView.TabIndex = 6
        Me.lvColumnsView.UseCompatibleStateImageBehavior = False
        Me.lvColumnsView.View = System.Windows.Forms.View.Details
        '
        'colhColumns
        '
        Me.colhColumns.Text = "Columns"
        Me.colhColumns.Width = 252
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(190, 147)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(63, 23)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(121, 147)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(63, 23)
        Me.btnOK.TabIndex = 4
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'VirtualColumnShowHideEditor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lvColumnsView)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Name = "VirtualColumnShowHideEditor"
        Me.Size = New System.Drawing.Size(256, 175)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lvColumnsView As System.Windows.Forms.ListView
    Friend WithEvents colhColumns As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button

End Class
