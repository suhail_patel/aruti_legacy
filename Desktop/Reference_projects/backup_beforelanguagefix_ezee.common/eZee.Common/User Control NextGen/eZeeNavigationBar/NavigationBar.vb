Imports System
Imports System.ComponentModel
Imports System.ComponentModel.Design
Imports system.Windows.Forms.Design
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
Imports System.Drawing

<Designer(GetType(NavigationBarDesigner)), TypeConverter(GetType(NavigationBar.NavButtonContainerConverter)), DesignTimeVisible(True), ToolboxItem(True)> _
Public Class NavigationBar
    Inherits System.Windows.Forms.Panel

    Private Const intControlSpace As Integer = 3

    Private NavButtons As NavigationButtonCollection
    Private NavSelectedButton As NavigationButton
    Private blnSelected As Boolean = False


#Region " Contructors "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        MyBase.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        MyBase.SetStyle(ControlStyles.DoubleBuffer, True)
        MyBase.SetStyle(ControlStyles.ResizeRedraw, True)
        MyBase.SetStyle(ControlStyles.UserPaint, True)

        Me.BackColor = Color.White
        Me.AutoScroll = True

        NavButtons = New NavigationButtonCollection(Me)
    End Sub

#End Region

#Region " Properties "

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)> _
    Public ReadOnly Property Buttons() As NavigationButtonCollection
        Get
            Return NavButtons
        End Get
    End Property

    <Browsable(False)> _
    Public Property SelectedIndex() As Integer
        Get
            Return Me.NavButtons.IndexOf(Me.NavSelectedButton)
        End Get
        Set(ByVal value As Integer)
            ' check if there are any pages 
            If Me.NavButtons.Count = 0 Then
                ' reset invalid index 
                'Me.ActivateButton(-1)
                Return
            End If

            ' validate page index 
            If value < -1 OrElse value >= Me.NavButtons.Count Then
                Throw New ArgumentOutOfRangeException("SelectedIndex", value, "The Navigation Button index must be between 0 and " + Convert.ToString(Me.NavButtons.Count - 1))
            End If

            ' select new page 
            'Me.ActivateButton(value)
        End Set
    End Property

    Public Property Selected() As Boolean
        Get
            Return blnSelected
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

#End Region

#Region " Friend Methods "

    Friend Function GetNewLocation() As Point
        Dim NewLocation As Point
        Dim intControlsHeight As Integer = 0

        intControlsHeight = intControlSpace
        With Me
            'Calculate the height of the all the controls
            For Each cntrl As Control In .Controls
                intControlsHeight += cntrl.Height + intControlSpace
            Next
            NewLocation = New Point(intControlSpace, intControlsHeight)
        End With

        Return NewLocation
    End Function

    Friend Function GetButtonWidth() As Integer
        Dim intWidth As Integer
        If Me.GetScrollState(ScrollableControl.ScrollStateVScrollVisible) Then
            intWidth = Me.Width - (intControlSpace + intControlSpace) - 17
        Else
            intWidth = Me.Width - (intControlSpace + intControlSpace)
        End If

        Return intWidth
    End Function

#End Region

#Region " Protected Events "

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()

        ' activate first page, if exists 
        If Me.NavButtons.Count > 0 Then
            'Me.ActivateButton(0)
        End If
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        MyBase.OnPaint(e)

        'Dim g As Graphics = e.Graphics
        'g.DrawRectangle(New Pen(SystemColors.ActiveCaption, 1), New Rectangle(0, 0, Me.Width - 1, Me.Height - 1))
        'ControlPaint.DrawBorder(e.Graphics, New Rectangle(0, 0, Me.Width, Me.Height), Color.Silver, ButtonBorderStyle.Solid)
        ControlPaint.DrawVisualStyleBorder(e.Graphics, New Rectangle(0, 0, Me.Width - 1, Me.Height - 1))
    End Sub

    '<System.ComponentModel.Description("Raises the ItemClick event.")> _
    'Protected Overridable Sub OnItemClick(ByVal biItem As NavigationButton, ByVal e As ItemClickEventArgs)
    '    RaiseEvent ItemClick(biItem, e)
    'End Sub

    '<System.ComponentModel.Description("Raises the SelectionChanged event.")> _
    'Protected Overridable Sub OnSelectionChanged(ByVal e As SelectionChangedEventArgs)
    '    RaiseEvent SelectionChanged(Me, e)
    'End Sub

    'Friend Function OnClickItem(ByVal biItem As NavigationButton) As Boolean
    '    biItem.Image = GetSelectedImage(biItem)

    '    Dim itemEventArgs As ItemClickEventArgs
    '    itemEventArgs = New ItemClickEventArgs(biItem, biItem.Text)
    '    OnItemClick(biItem, itemEventArgs)
    'End Function

    'Friend Function OnSelectItem(ByVal item As NavigationButton, ByVal Value As Boolean) As Boolean
    '    If (Value) Then
    '        For Each cntrl As Control In MyBase.Controls
    '            If TypeOf cntrl Is NavigationButton Then
    '                If Not cntrl Is item Then
    '                    If CType(cntrl, NavigationButton).Selected Then
    '                        CType(cntrl, NavigationButton).Selected = False
    '                    End If
    '                End If
    '            End If
    '        Next
    '        Dim e As New SelectionChangedEventArgs(item, item.Text)
    '        OnSelectionChanged(e)
    '        OnSelectItem = Value
    '    Else
    '        OnSelectItem = Value
    '    End If
    'End Function

#End Region

#Region " Friend Class "

    Friend Class NavButtonContainerConverter
        Inherits TypeConverter

        Public Overloads Overrides Function CanConvertTo(ByVal context As ITypeDescriptorContext, ByVal destType As Type) As Boolean
            If destType Is GetType(Serialization.InstanceDescriptor) Then
                Return True
            End If

            Return MyBase.CanConvertTo(context, destType)
        End Function

        Public Overloads Overrides Function ConvertTo(ByVal context As ITypeDescriptorContext, ByVal culture As System.Globalization.CultureInfo, ByVal value As Object, ByVal destType As Type) As Object
            If destType Is GetType(Serialization.InstanceDescriptor) Then
                Dim ci As System.Reflection.ConstructorInfo = GetType(NavigationBar).GetConstructor(System.Type.EmptyTypes)

                Return New Serialization.InstanceDescriptor(ci, Nothing, False)
            End If

            Return MyBase.ConvertTo(context, culture, value, destType)
        End Function

    End Class

#End Region

End Class

#Region " Navigation Bar Designer Classes "

Public Class NavigationBarDesigner
    Inherits ControlDesigner

    Private NavBar As NavigationBar

    Private lists As DesignerActionListCollection

    Public Overrides ReadOnly Property ActionLists() As DesignerActionListCollection
        Get
            If lists Is Nothing Then
                lists = New DesignerActionListCollection()
                lists.Add(New NavigationBarActionList(Me.Component))
            End If
            Return lists
        End Get
    End Property

    Public Overloads Overrides Sub Initialize(ByVal component As IComponent)
        MyBase.Initialize(component)

        'Record instance of control we're designing
        NavBar = DirectCast(component, NavigationBar)

        'Hook up events
        'Dim s As ISelectionService = DirectCast(GetService(GetType(ISelectionService)), ISelectionService)
        Dim c As IComponentChangeService = DirectCast(GetService(GetType(IComponentChangeService)), IComponentChangeService)
        'AddHandler s.SelectionChanged, AddressOf OnSelectionChanged
        AddHandler c.ComponentRemoving, AddressOf OnComponentRemoving
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Dim s As ISelectionService = DirectCast(GetService(GetType(ISelectionService)), ISelectionService)
        Dim c As IComponentChangeService = DirectCast(GetService(GetType(IComponentChangeService)), IComponentChangeService)

        ' Unhook events
        'RemoveHandler s.SelectionChanged, AddressOf OnSelectionChanged
        RemoveHandler c.ComponentRemoving, AddressOf OnComponentRemoving

        MyBase.Dispose(disposing)
    End Sub

    Private Sub OnComponentRemoving(ByVal sender As Object, ByVal e As ComponentEventArgs)
        Dim c As IComponentChangeService = DirectCast(GetService(GetType(IComponentChangeService)), IComponentChangeService)
        Dim button As NavigationButton
        Dim h As IDesignerHost = DirectCast(GetService(GetType(IDesignerHost)), IDesignerHost)
        Dim i As Integer

        ' If the user is removing a button
        If TypeOf e.Component Is NavigationButton Then
            button = DirectCast(e.Component, NavigationButton)
            If NavBar.Buttons.Contains(button) Then
                c.OnComponentChanging(NavBar, Nothing)
                NavBar.Buttons.Remove(button)
                c.OnComponentChanged(NavBar, Nothing, Nothing, Nothing)
                Exit Sub
            End If
        End If

        ' If the user is removing the control itself
        If e.Component Is NavBar Then
            For i = NavBar.Buttons.Count - 1 To 0 Step -1
                button = NavBar.Buttons(i)
                c.OnComponentChanging(NavBar, Nothing)
                NavBar.Buttons.Remove(button)
                h.DestroyComponent(button)
                c.OnComponentChanged(NavBar, Nothing, Nothing, Nothing)
            Next
        End If
    End Sub

End Class

Public Class NavigationBarActionList
    Inherits DesignerActionList

    Private Navbar As NavigationBar
    Private DesignerActionSvc As DesignerActionUIService = Nothing

    Public Sub New(ByVal component As IComponent)
        MyBase.New(component)
        Me.Navbar = component

        Me.DesignerActionSvc = CType(GetService(GetType(DesignerActionUIService)), DesignerActionUIService)

    End Sub

    Public Overrides Function GetSortedActionItems() As DesignerActionItemCollection

        Dim items As New DesignerActionItemCollection()

        items.Add(New DesignerActionMethodItem(Me, "AddButton", "Add Navigation"))

        Return items

    End Function

    Private Sub AddButton()
        Dim button As NavigationButton
        Dim h As IDesignerHost = DirectCast(GetService(GetType(IDesignerHost)), IDesignerHost)
        Dim dt As DesignerTransaction
        Dim c As IComponentChangeService = DirectCast(GetService(GetType(IComponentChangeService)), IComponentChangeService)

        ' Add a new button to the collection
        dt = h.CreateTransaction("Add Button")

        button = DirectCast(h.CreateComponent(GetType(NavigationButton)), NavigationButton)
        c.OnComponentChanging(Navbar, Nothing)
        Dim loc As Point = Navbar.GetNewLocation()

        button.Location = loc
        button.Width = Navbar.GetButtonWidth()
        button.Parent = Navbar
        button.Visible = True
        button.Owner = Navbar
        button.Select()

        Navbar.Buttons.Add(button)

        c.OnComponentChanged(Navbar, Nothing, Nothing, Nothing)

        dt.Commit()

        button.Invalidate()
    End Sub

End Class

#End Region

