Imports System.Windows.Forms
Imports System.Drawing

Public Class eZeeTicker
    Inherits System.Windows.Forms.UserControl

    Public Sub New()

        InitializeComponent()
        Me.SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        Me.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        Me.SetStyle(ControlStyles.ResizeRedraw, True)
        Me.SetStyle(ControlStyles.UserPaint, True)
    End Sub

#Region "Properties"

    'Timer Interval Property
    Private _timeinterval As Integer = 21
    '<Browsable(True), Category("Ticker")> _
    '<DefaultValue(200)> _
    Public Property TimeInterval() As Integer
        Get
            Return _timeinterval
        End Get
        Set(ByVal value As Integer)
            _timeinterval = value
            Timer1.Interval = _timeinterval
            Invalidate()
        End Set
    End Property

    ' Html Text Property
    Private _htmltext As String = String.Empty
    '<Browsable(True), Category("Ticker")> _
    Public Property HtmlText() As String
        Get
            Return _htmltext
        End Get
        Set(ByVal value As String)
            _htmltext = value
            htmlLabel1.DocumentText = _htmltext
            Invalidate()
        End Set
    End Property

    'Mouse Over Property
    Private _mouseoverstop As [Boolean] = False
    '<Browsable(True), Category("Ticker")> _
    Public Property MouseOverStop() As [Boolean]
        Get
            Return _mouseoverstop
        End Get
        Set(ByVal value As [Boolean])
            _mouseoverstop = value
            Invalidate()
        End Set
    End Property

    'Html Lavel Size Properties
    Private _width As Integer = 542
    '<Browsable(True), Category("Ticker")> _
    '<DefaultValue(542)> _
    Public Property HtmlLabelWidth() As Integer
        Get
            Return _width
        End Get
        Set(ByVal value As Integer)
            _width = value
            Me.htmlLabel1.Width = _width
            Invalidate()
        End Set
    End Property

    Private _height As Integer = 22
    '<Browsable(True), Category("Ticker")> _
    '<DefaultValue(35)> _
    Public Property HtmlLabelHeight() As Integer
        Get
            Return _height
        End Get
        Set(ByVal value As Integer)
            _height = value
            Me.htmlLabel1.Height = _height
            Invalidate()
        End Set
    End Property


    ' Alignment Property 
    Public Enum Alignment_Main
        UpToDown = 1
        DownToUp = 2
        LeftToRight = 3
        RightToLeft = 4
    End Enum

    Private _alignment As Alignment_Main = Alignment_Main.RightToLeft
    '<Browsable(True), Category("Ticker")> _
    Public Property Alignment() As Alignment_Main
        Get
            Return _alignment
        End Get
        Set(ByVal value As Alignment_Main)
            _alignment = value
            If _alignment = Alignment_Main.UpToDown Then
                htmlLabel1.Location = New Point(0, 0 - htmlLabel1.Height)
            End If
            If _alignment = Alignment_Main.DownToUp Then
                htmlLabel1.Location = New Point(0, panel1.Height)
            End If
            If _alignment = Alignment_Main.LeftToRight Then
                htmlLabel1.Location = New Point(0 - htmlLabel1.Width, 0)
            End If
            If _alignment = Alignment_Main.RightToLeft Then
                htmlLabel1.Location = New Point(panel1.Width, 0)
            End If
            Timer1.Interval = _timeinterval
            Timer1.Enabled = True
            Timer1.Start()
            Invalidate()
        End Set
    End Property


#End Region


#Region "Timer's Events"
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If _alignment = Alignment_Main.UpToDown Then
            If htmlLabel1.Location.Y = panel1.Height + htmlLabel1.Height Then
                htmlLabel1.Location = New Point(0, 0 - htmlLabel1.Height)
            Else
                htmlLabel1.Location = New Point(htmlLabel1.Location.X, htmlLabel1.Location.Y + 1)
            End If
        End If
        If _alignment = Alignment_Main.DownToUp Then
            If htmlLabel1.Location.Y = (0 - htmlLabel1.Height) Then
                htmlLabel1.Location = New Point(0, panel1.Height)
            Else
                htmlLabel1.Location = New Point(htmlLabel1.Location.X, htmlLabel1.Location.Y - 1)
            End If
        End If
        If _alignment = Alignment_Main.LeftToRight Then
            If htmlLabel1.Location.X = panel1.Width Then
                htmlLabel1.Location = New Point(0 - htmlLabel1.Width, 0)
            Else
                htmlLabel1.Location = New Point(htmlLabel1.Location.X + 1, 0)
            End If
        End If
        If _alignment = Alignment_Main.RightToLeft Then
            If htmlLabel1.Location.X = (0 - htmlLabel1.Width) Then
                htmlLabel1.Location = New Point(panel1.Width, 0)
            Else
                htmlLabel1.Location = New Point(htmlLabel1.Location.X - 1, 0)
            End If
        End If
    End Sub
#End Region

    Private Sub Ticker_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.MouseHover, panel1.MouseHover
        If _mouseoverstop = True Then
            Timer1.[Stop]()
        Else
            Timer1.Start()
        End If
    End Sub

    Private Sub Ticker_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.MouseLeave, panel1.MouseLeave
        Timer1.Start()
    End Sub
End Class
