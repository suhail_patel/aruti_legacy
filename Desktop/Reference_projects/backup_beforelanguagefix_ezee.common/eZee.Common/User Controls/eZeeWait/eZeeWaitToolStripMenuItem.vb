
Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Windows.Forms.ToolStripControlHost
Imports System.Windows.Forms.Design
Imports System.ComponentModel

<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.All)> _
 Public Class eZeeWaitToolStripMenuItem
    Inherits ToolStripControlHost

#Region " Properties "
    ''' <summary>
    ''' Gets the eZeeWait control.
    ''' </summary>
    ''' <value>The eZeeWait control.</value>
    <RefreshProperties(RefreshProperties.All), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)> _
    Public ReadOnly Property eZeeWaitControl() As eZeeWait
        Get
            Return TryCast(Control, eZeeWait)
        End Get
    End Property
#End Region
   
#Region " Constructor "
    ''' <summary>
    ''' Initializes a new instance of the class.
    ''' </summary>
    Public Sub New()
        MyBase.New(New eZeeWait())
    End Sub
#End Region

    ''' <summary>
    ''' Retrieves the size of a rectangular area into which a control can be fitted.
    ''' </summary>
    ''' <param name="constrainingSize">The custom-sized area for a control.</param>
    ''' <returns>
    ''' An ordered pair of type <see cref="T:System.Drawing.Size"></see> representing the width and height of a rectangle.
    ''' </returns>
    ''' <PermissionSet><IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/><IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/></PermissionSet>
    Public Overrides Function GetPreferredSize(ByVal constrainingSize As Size) As Size
        'return base.GetPreferredSize(constrainingSize);
        Return Me.eZeeWaitControl.GetPreferredSize(constrainingSize)
    End Function

    ''' <summary>
    ''' Subscribes events from the hosted control.
    ''' </summary>
    ''' <param name="control">The control from which to subscribe events.</param>
    Protected Overrides Sub OnSubscribeControlEvents(ByVal control As Control)
        MyBase.OnSubscribeControlEvents(control)

        'Add your code here to subsribe to Control Events
    End Sub

    ''' <summary>
    ''' Unsubscribes events from the hosted control.
    ''' </summary>
    ''' <param name="control">The control from which to unsubscribe events.</param>
    Protected Overrides Sub OnUnsubscribeControlEvents(ByVal control As Control)
        MyBase.OnUnsubscribeControlEvents(control)

        'Add your code here to unsubscribe from control events.
    End Sub
End Class

