Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports System
Imports System.ComponentModel.Design
Imports Microsoft.Win32
Imports System.Drawing
Imports System.Drawing.Design
Imports System.Windows.Forms
Imports System.Windows.Forms.ComponentModel
Imports System.Windows.Forms.Design



' Used by the eZeeStraightLine to specify the type of line to draw 
<Editor("StraightLineTypeEditor", GetType(UITypeEditor))> _
Public Enum StraightLineTypes
    Horizontal
    Vertical
    DiagonalDescending
    DiagonalAscending
End Enum

' The UITypeEditor for the StraightLineTypes enum 
#Region "UITypeEditor"
Public Class StraightLineTypeEditor
    Inherits UITypeEditor
    Private lineTypeUI As StraightLineTypeUI

    Public Overloads Overrides Function EditValue(ByVal context As ITypeDescriptorContext, ByVal provider As IServiceProvider, ByVal value As Object) As Object
        Dim returnValue As Object = value
        If provider IsNot Nothing Then
            Dim edSvc As IWindowsFormsEditorService = DirectCast(provider.GetService(GetType(IWindowsFormsEditorService)), IWindowsFormsEditorService)
            If edSvc IsNot Nothing Then
                If lineTypeUI Is Nothing Then
                    lineTypeUI = New StraightLineTypeUI(Me)
                End If
                lineTypeUI.Start(edSvc, value)
                edSvc.DropDownControl(lineTypeUI)
                value = lineTypeUI.Value
                lineTypeUI.[End]()
            End If
        End If
        Return value
    End Function

    Public Overloads Overrides Function GetEditStyle(ByVal context As ITypeDescriptorContext) As UITypeEditorEditStyle
        Return UITypeEditorEditStyle.DropDown
    End Function

#Region "Control used in the UITypeEditor"
    Private Class StraightLineTypeUI
        Inherits Control
        Private edSvc As IWindowsFormsEditorService
        Private editor As StraightLineTypeEditor = Nothing
        Private oldLineType As StraightLineTypes
        Private m_value As Object

        Public Sub New(ByVal editor As StraightLineTypeEditor)
            Me.editor = editor
            InitializeComponent()
        End Sub

        Public ReadOnly Property Value() As Object
            Get
                Return m_value
            End Get
        End Property

        Public Sub [End]()
            m_value = Nothing
            edSvc = Nothing
        End Sub

        Public Sub Start(ByVal edSvc As IWindowsFormsEditorService, ByVal value As Object)
            Me.edSvc = edSvc
            Me.m_value = value
            Me.oldLineType = DirectCast(value, StraightLineTypes)

            Dim panel As Panel
            For Each c As Control In Controls
                panel = TryCast(c, Panel)
                If c IsNot Nothing Then
                    c.BackColor = SystemColors.Control
                    c.ForeColor = SystemColors.ControlText
                End If
                panel = Nothing
            Next

            Select Case DirectCast(value, StraightLineTypes)
                Case StraightLineTypes.Horizontal
                    Me.horizontalPanel.BackColor = SystemColors.ControlText
                    Me.horizontalPanel.ForeColor = SystemColors.Control
                    Exit Select
                Case StraightLineTypes.Vertical
                    Me.verticalPanel.BackColor = SystemColors.ControlText
                    Me.verticalPanel.ForeColor = SystemColors.Control
                    Exit Select
                Case StraightLineTypes.DiagonalAscending
                    Me.diagonalAscendingPanel.BackColor = SystemColors.ControlText
                    Me.diagonalAscendingPanel.ForeColor = SystemColors.Control
                    Exit Select
                Case StraightLineTypes.DiagonalDescending
                    Me.diagonalDescendingPanel.BackColor = SystemColors.ControlText
                    Me.diagonalDescendingPanel.ForeColor = SystemColors.Control
                    Exit Select
                Case Else
                    Exit Select
            End Select
        End Sub

        Private Sub Teardown(ByVal save As Boolean)
            If Not save Then
                m_value = oldLineType
            End If
            edSvc.CloseDropDown()
        End Sub

        Private Sub button_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim line As eZeeStraightLine = TryCast(sender, eZeeStraightLine)
            If line IsNot Nothing Then
                Me.m_value = line.LineType
                Teardown(True)
            End If
        End Sub

#Region "Component Designer generated code"

        Private horizontalPanel As System.Windows.Forms.Panel
        Private verticalPanel As System.Windows.Forms.Panel
        Private diagonalDescendingPanel As System.Windows.Forms.Panel
        Private diagonalAscendingPanel As System.Windows.Forms.Panel
        Private horizontalLine As eZeeStraightLine
        Private verticalLine As eZeeStraightLine
        Private diagonalDescendingLine As eZeeStraightLine
        Private diagonalAscendingLine As eZeeStraightLine

        Friend Overridable Sub InitializeComponent()
            Me.horizontalPanel = New System.Windows.Forms.Panel()
            Me.verticalPanel = New System.Windows.Forms.Panel()
            Me.diagonalDescendingPanel = New System.Windows.Forms.Panel()
            Me.diagonalAscendingPanel = New System.Windows.Forms.Panel()
            Me.horizontalLine = New eZeeStraightLine()
            Me.verticalLine = New eZeeStraightLine()
            Me.diagonalDescendingLine = New eZeeStraightLine()
            Me.diagonalAscendingLine = New eZeeStraightLine()
            Me.horizontalPanel.SuspendLayout()
            Me.verticalPanel.SuspendLayout()
            Me.diagonalDescendingPanel.SuspendLayout()
            Me.diagonalAscendingPanel.SuspendLayout()
            Me.SuspendLayout()
            ' 
            ' horizontalPanel 
            ' 
            Me.horizontalPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.horizontalPanel.Controls.AddRange(New System.Windows.Forms.Control() {Me.horizontalLine})
            Me.horizontalPanel.Location = New System.Drawing.Point(2, 2)
            Me.horizontalPanel.Name = "horizontalPanel"
            Me.horizontalPanel.Size = New System.Drawing.Size(40, 40)
            Me.horizontalPanel.TabIndex = 0
            ' 
            ' verticalPanel 
            ' 
            Me.verticalPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.verticalPanel.Controls.AddRange(New System.Windows.Forms.Control() {Me.verticalLine})
            Me.verticalPanel.Location = New System.Drawing.Point(44, 2)
            Me.verticalPanel.Name = "verticalPanel"
            Me.verticalPanel.Size = New System.Drawing.Size(40, 40)
            Me.verticalPanel.TabIndex = 1
            ' 
            ' diagonalDescendingPanel 
            ' 
            Me.diagonalDescendingPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.diagonalDescendingPanel.Controls.AddRange(New System.Windows.Forms.Control() {Me.diagonalDescendingLine})
            Me.diagonalDescendingPanel.Location = New System.Drawing.Point(86, 2)
            Me.diagonalDescendingPanel.Name = "diagonalDescendingPanel"
            Me.diagonalDescendingPanel.Size = New System.Drawing.Size(40, 40)
            Me.diagonalDescendingPanel.TabIndex = 2
            ' 
            ' diagonalAscendingPanel 
            ' 
            Me.diagonalAscendingPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.diagonalAscendingPanel.Controls.AddRange(New System.Windows.Forms.Control() {Me.diagonalAscendingLine})
            Me.diagonalAscendingPanel.Location = New System.Drawing.Point(128, 2)
            Me.diagonalAscendingPanel.Name = "diagonalAscendingPanel"
            Me.diagonalAscendingPanel.Size = New System.Drawing.Size(40, 40)
            Me.diagonalAscendingPanel.TabIndex = 3
            ' 
            ' horizontalLine 
            ' 
            Me.horizontalLine.Dock = System.Windows.Forms.DockStyle.Fill
            Me.horizontalLine.LineType = StraightLineTypes.Horizontal
            Me.horizontalLine.Name = "horizontalLine"
            Me.horizontalLine.Size = New System.Drawing.Size(38, 38)
            Me.horizontalLine.TabIndex = 0
            Me.horizontalLine.Text = "straightLine1"
            AddHandler Me.horizontalLine.Click, AddressOf button_Click
            ' 
            ' verticalLine 
            ' 
            Me.verticalLine.Dock = System.Windows.Forms.DockStyle.Fill
            Me.verticalLine.LineType = StraightLineTypes.Vertical
            Me.verticalLine.Name = "verticalLine"
            Me.verticalLine.Size = New System.Drawing.Size(38, 38)
            Me.verticalLine.TabIndex = 0
            Me.verticalLine.Text = "straightLine2"
            AddHandler Me.verticalLine.Click, AddressOf button_Click
            ' 
            ' diagonalDescendingLine 
            ' 
            Me.diagonalDescendingLine.Dock = System.Windows.Forms.DockStyle.Fill
            Me.diagonalDescendingLine.LineType = StraightLineTypes.DiagonalDescending
            Me.diagonalDescendingLine.Name = "diagonalDescendingLine"
            Me.diagonalDescendingLine.Size = New System.Drawing.Size(38, 38)
            Me.diagonalDescendingLine.TabIndex = 0
            Me.diagonalDescendingLine.Text = "straightLine3"
            AddHandler Me.diagonalDescendingLine.Click, AddressOf button_Click
            ' 
            ' diagonalAscendingLine 
            ' 
            Me.diagonalAscendingLine.Dock = System.Windows.Forms.DockStyle.Fill
            Me.diagonalAscendingLine.LineType = StraightLineTypes.DiagonalAscending
            Me.diagonalAscendingLine.Name = "diagonalAscendingLine"
            Me.diagonalAscendingLine.Size = New System.Drawing.Size(38, 38)
            Me.diagonalAscendingLine.TabIndex = 0
            Me.diagonalAscendingLine.Text = "straightLine4"
            AddHandler Me.diagonalAscendingLine.Click, AddressOf button_Click
            ' 
            ' UserControl1 
            ' 
            Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.diagonalAscendingPanel, Me.diagonalDescendingPanel, Me.verticalPanel, Me.horizontalPanel})
            Me.Name = "UserControl1"
            Me.Size = New System.Drawing.Size(164, 44)
            Me.BackColor = System.Drawing.SystemColors.InactiveBorder
            Me.horizontalPanel.ResumeLayout(False)
            Me.verticalPanel.ResumeLayout(False)
            Me.diagonalDescendingPanel.ResumeLayout(False)
            Me.diagonalAscendingPanel.ResumeLayout(False)
            Me.ResumeLayout(False)
        End Sub
#End Region
    End Class
#End Region
End Class
#End Region