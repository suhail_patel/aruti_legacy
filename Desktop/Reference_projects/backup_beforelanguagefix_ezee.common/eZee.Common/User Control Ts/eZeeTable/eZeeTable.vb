'************************************************************************************************************************************
'Class Name : eZeeTable.vb
'Purpose    : 
'Date       : 18 July 2007
'Written By : Naimish
'Modified   :	
'************************************************************************************************************************************

Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

#Region " ENum "
Public Enum enTableStatus
    Clean = 0
    OpenKOT = 1
    OpenReceipt = 2
    Reserved = 3
    HoldKOT = 4
    HoldReceipt = 5
End Enum

#End Region

Public Class eZeeTable
    Inherits eZee_ImageButton

    Private Const conTableProperty As String = "Table Property"

#Region " Properties Variable "
    Private mintLayoutUnkid As Integer = 0
    Private mintOutletUnkid As Integer = 0
    Private mstrTableName As String = ""
    Private mintTableUnkID As Integer = 0
    Private mintImageUnkID As Integer = 0

    Private mblnShowCaption As Boolean

    Private mintKotUnkID As Integer
    Private mintReceiptUnkID As Integer
    Private mintStatusUnkID As Integer

    'Krushna (15 Sep 2008) -- Start
    Private mintReservationUnkId As Integer
    'Krushna (15 Sep 2008) -- End
    Private mstrHoldFileName As String = ""
#End Region

#Region " Constructor "

    Public Sub New()
        SetStyle(ControlStyles.AllPaintingInWmPaint Or ControlStyles.OptimizedDoubleBuffer Or ControlStyles.ResizeRedraw Or ControlStyles.SupportsTransparentBackColor Or ControlStyles.UserPaint, True)
        SetStyle(ControlStyles.Opaque, False)
    End Sub

#End Region

#Region " Properties "

    <Category(conTableProperty), _
    DisplayName(" Layout ID"), _
    Description("Layout unique key")> _
    Public Property _LayoutUnkid() As Integer
        Get
            Return mintLayoutUnkid
        End Get
        Set(ByVal Value As Integer)
            mintLayoutUnkid = Value
        End Set
    End Property

    <Category(conTableProperty), _
    DisplayName(" Outlet ID"), _
    Description("Outlet unique key")> _
    Public Property _OutletUnkID() As Boolean
        Get
            Return mblnShowCaption
        End Get
        Set(ByVal Value As Boolean)
            mblnShowCaption = Value
        End Set
    End Property


    <Category(conTableProperty), _
    DisplayName(" Table Name"), _
    Description("Table Name")> _
    Public Property _TableName() As String
        Get
            Return mstrTableName
        End Get
        Set(ByVal Value As String)
            mstrTableName = Value
        End Set
    End Property


    <Category(conTableProperty), _
    DisplayName(" Table ID"), _
    Description("Table unique key")> _
    Public Property _TableUnkid() As Integer
        Get
            Return mintTableUnkID
        End Get
        Set(ByVal Value As Integer)
            mintTableUnkID = Value
        End Set
    End Property

    <Category(conTableProperty), _
    DisplayName(" Image ID"), _
    Description("Image unique key")> _
    Public Property _ImageUnkid() As Integer
        Get
            Return mintImageUnkID
        End Get
        Set(ByVal Value As Integer)
            mintImageUnkID = Value
        End Set
    End Property

    <Category(conTableProperty)> _
    Public Property _ShowCaption() As Boolean
        Get
            Return mblnShowCaption
        End Get
        Set(ByVal Value As Boolean)
            mblnShowCaption = Value
        End Set
    End Property

    <Browsable(False)> _
    Public Property _KotUnkid() As Integer
        Get
            Return mintKotUnkID
        End Get
        Set(ByVal Value As Integer)
            mintKotUnkID = Value
        End Set
    End Property

    <Browsable(False)> _
    Public Property _ReceiptUnkid() As Integer
        Get
            Return mintReceiptUnkID
        End Get
        Set(ByVal Value As Integer)
            mintReceiptUnkID = Value
        End Set
    End Property

    'Krushna (15 Sep 2008) -- Start
    <Browsable(False)> _
    Public Property _ReservationUnkid() As Integer
        Get
            Return mintReservationUnkId
        End Get
        Set(ByVal Value As Integer)
            mintReservationUnkId = Value
        End Set
    End Property
    'Krushna (15 Sep 2008) -- End

    <Browsable(False)> _
    Public Property _TableStatus() As enTableStatus
        Get
            Return mintStatusUnkID
        End Get
        Set(ByVal Value As enTableStatus)
            If mintStatusUnkID <> Value Then
                mintStatusUnkID = Value
                ChangeStatus()
            End If
        End Set
    End Property

    <Browsable(False)> _
    Public Property _Netlock() As Boolean
        Get
            Return MyBase.Netlock
        End Get
        Set(ByVal Value As Boolean)
            MyBase.Netlock = Value
        End Set
    End Property

    <Browsable(False)> _
   Public Property _HoldFileName() As String
        Get
            Return mstrHoldFileName
        End Get
        Set(ByVal Value As String)
            mstrHoldFileName = Value
        End Set
    End Property
#End Region

#Region " Private Methods "
    Private Sub ChangeStatus()
        Select Case DirectCast(mintStatusUnkID, enTableStatus)
            Case enTableStatus.Clean
                StatusImage = Nothing
            Case enTableStatus.OpenKOT
                StatusImage = My.Resources.InUse
            Case enTableStatus.OpenReceipt
                StatusImage = My.Resources.Receipt_Recall_32
            Case enTableStatus.Reserved
                StatusImage = My.Resources.Reservation
            Case enTableStatus.HoldKOT, enTableStatus.HoldReceipt
                StatusImage = My.Resources.Hold_16
        End Select
    End Sub
#End Region

End Class
