Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Text
Imports System.Windows.Forms

Public Class eZeeKeyboard
    Private shiftindicator As Boolean = False
    Private capslockindicator As Boolean = False
    Private pvtKeyboardKeyPressed As String = ""

    Private pvtKeyboardType As eZeeKeyBoardLayOut = eZeeKeyBoardLayOut.None

    Private ReadOnly BaseSize As Size = New Size(871, 282)

#Region " Property "

    Public Property KeyboardType() As eZeeKeyBoardLayOut
        Get
            Return pvtKeyboardType
        End Get
        Set(ByVal value As eZeeKeyBoardLayOut)
            If pvtKeyboardType = value Then Exit Property
            pvtKeyboardType = value
            If shiftindicator Then
                HandleShiftClick()
            End If
            If capslockindicator Then
                HandleCapsLock()
            End If
            Call ChangeLayout()
        End Set
    End Property

#End Region

#Region " Events "

    Private Sub ChangeLayout()

            Select Case pvtKeyboardType
                Case eZeeKeyBoardLayOut.KeyBoard_Standard
                    picKeyboard.Image = My.Resources.Keyboard_Alphanumeric
                    picCapsLockDown.Image = My.Resources.CapsLock_Down
                    picLeftShiftDown.Image = My.Resources.Shift_Left_Down
                    picRightShiftDown.Image = My.Resources.Shift_Right_Down
                Case eZeeKeyBoardLayOut.KeyBoard_Alphabetical
                    picKeyboard.Image = My.Resources.Keyboard_Alphanumeric_AZ
                    picCapsLockDown.Image = My.Resources.CapsLock_Down
                    picLeftShiftDown.Image = My.Resources.Shift_Left_Down
                    picRightShiftDown.Image = My.Resources.Shift_Right_Down
                Case eZeeKeyBoardLayOut.NumPad_Number
                    picKeyboard.Image = My.Resources.Keyboard_Number
                Case eZeeKeyBoardLayOut.NumPad_Decimal
                    picKeyboard.Image = My.Resources.Keyboard_Decimal
                Case eZeeKeyBoardLayOut.NumPad_Amount
                    picKeyboard.Image = My.Resources.Keyboard_Amount
            End Select
    End Sub

    <Category("Mouse"), Description("Return value of mouseclicked key")> _
    Public Event UserKeyPressed As KeyboardDelegate

    Protected Overridable Sub OnUserKeyPressed(ByVal e As eZeeTsKeyboardEventArgs)
        If Not UserKeyPressedEvent Is Nothing Then
            RaiseEvent UserKeyPressed(Me, e)
        End If
    End Sub

#End Region

#Region " Controls Events "
    Private Sub pictureBoxKeyboard_SizeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles picKeyboard.SizeChanged
        ' position the capslock and shift down overlays
        picCapsLockDown.Left = Convert.ToInt16(picKeyboard.Width * 3 \ BaseSize.Width)
        picCapsLockDown.Top = Convert.ToInt16(picKeyboard.Height * 115 \ BaseSize.Height)

        picLeftShiftDown.Left = Convert.ToInt16(picKeyboard.Width * 3 \ BaseSize.Width)
        picLeftShiftDown.Top = Convert.ToInt16(picKeyboard.Height * 171 \ BaseSize.Height)

        picRightShiftDown.Left = Convert.ToInt16(picKeyboard.Width * 684 \ BaseSize.Width)
        picRightShiftDown.Top = picLeftShiftDown.Top


        ' size the capslock and shift down overlays

        picCapsLockDown.Width = Convert.ToInt16(picKeyboard.Width * 104 \ BaseSize.Width)
        picCapsLockDown.Height = Convert.ToInt16(picKeyboard.Height * 52 \ BaseSize.Height)

        picLeftShiftDown.Width = Convert.ToInt16(picKeyboard.Width * 128 \ BaseSize.Width)
        picLeftShiftDown.Height = Convert.ToInt16(picKeyboard.Height * 52 \ BaseSize.Height)

        picRightShiftDown.Width = Convert.ToInt16(picKeyboard.Width * 74 \ BaseSize.Width)
        picRightShiftDown.Height = picLeftShiftDown.Height
    End Sub

    Private Sub pictureBoxKeyboard_MouseClick(ByVal sender As Object, ByVal e As MouseEventArgs) Handles picKeyboard.MouseClick
        Dim xpos As Single = e.X
        Dim ypos As Single = e.Y

        xpos = BaseSize.Width * (xpos / picKeyboard.Width)
        ypos = BaseSize.Height * (ypos / picKeyboard.Height)

        'If pvtKeyboardType = BoW.Kids Then
        'pvtKeyboardKeyPressed = HandleKidsMouseClick(xpos, ypos)
        'Else

        Select Case pvtKeyboardType
            Case eZeeKeyBoardLayOut.NumPad_Number
                pvtKeyboardKeyPressed = HandleMouseClick_Number(xpos, ypos)
            Case eZeeKeyBoardLayOut.NumPad_Decimal
                pvtKeyboardKeyPressed = HandleMouseClick_Decimal(xpos, ypos)
            Case eZeeKeyBoardLayOut.NumPad_Amount
                pvtKeyboardKeyPressed = HandleMouseClick_Amount(xpos, ypos)
            Case Else
                pvtKeyboardKeyPressed = HandleMouseClick_KeyBoard(xpos, ypos)
        End Select

        Dim dea As eZeeTsKeyboardEventArgs = New eZeeTsKeyboardEventArgs(pvtKeyboardKeyPressed)
        OnUserKeyPressed(dea)

    End Sub

    Private Sub pictureBoxLeftShiftState_MouseClick(ByVal sender As Object, ByVal e As MouseEventArgs) Handles picLeftShiftDown.MouseClick
        HandleShiftClick()
    End Sub

    Private Sub pictureBoxRightShiftState_MouseClick(ByVal sender As Object, ByVal e As MouseEventArgs) Handles picRightShiftDown.MouseClick
        HandleShiftClick()
    End Sub

    Private Sub pictureBoxCapsLockState_MouseClick(ByVal sender As Object, ByVal e As MouseEventArgs) Handles picCapsLockDown.MouseClick
        HandleCapsLock()
    End Sub

#End Region

#Region " KeyBoard Key Handler"
    Private Function HandleMouseClick_KeyBoard(ByVal x As Single, ByVal y As Single) As String
        Dim Keypressed As String = Nothing

        Select Case True
            '1St Line
            Case y >= 3 AndAlso y <= 55
                If x >= 3 AndAlso x <= 54 Then ''
                    Keypressed = HandleShiftableKey("`")
                ElseIf x >= 57 AndAlso x <= 109 Then '1
                    Keypressed = HandleShiftableKey("1")
                ElseIf x >= 112 AndAlso x <= 164 Then '2
                    Keypressed = HandleShiftableKey("2")
                ElseIf x >= 167 AndAlso x <= 219 Then '3
                    Keypressed = HandleShiftableKey("3")
                ElseIf x >= 222 AndAlso x <= 274 Then '4
                    Keypressed = HandleShiftableKey("4")
                ElseIf x >= 277 AndAlso x <= 329 Then '5
                    Keypressed = HandleShiftableKey("5")
                ElseIf x >= 332 AndAlso x <= 384 Then '6
                    Keypressed = HandleShiftableKey("6")
                ElseIf x >= 387 AndAlso x <= 439 Then '7
                    Keypressed = HandleShiftableKey("7")
                ElseIf x >= 442 AndAlso x <= 494 Then '8
                    Keypressed = HandleShiftableKey("8")
                ElseIf x >= 497 AndAlso x <= 549 Then '9
                    Keypressed = HandleShiftableKey("9")
                ElseIf x >= 552 AndAlso x <= 604 Then '0
                    Keypressed = HandleShiftableKey("0")
                ElseIf x >= 607 AndAlso x <= 659 Then '-
                    Keypressed = HandleShiftableKey("-")
                ElseIf x >= 662 AndAlso x <= 714 Then '=
                    Keypressed = HandleShiftableKey("=")
                ElseIf x >= 717 AndAlso x <= 812 Then 'Bk
                    Keypressed = "{BACKSPACE}"
                ElseIf x >= 816 AndAlso x <= 868 Then 'Home
                    Keypressed = "{HOME}"
                End If

                '2nd Line
            Case y >= 59 AndAlso y <= 111
                If x >= 3 AndAlso x <= 81 Then 'Tab
                    Keypressed = HandleShiftableKey("{TAB}")
                ElseIf x >= 84 AndAlso x <= 136 Then 'q
                    Keypressed = HandleShiftableCaplockableKey("q")
                ElseIf x >= 139 AndAlso x <= 191 Then 'w
                    Keypressed = HandleShiftableCaplockableKey("w")
                ElseIf x >= 194 AndAlso x <= 246 Then 'e
                    Keypressed = HandleShiftableCaplockableKey("e")
                ElseIf x >= 249 AndAlso x <= 301 Then 'r
                    Keypressed = HandleShiftableCaplockableKey("r")
                ElseIf x >= 304 AndAlso x <= 356 Then 't
                    Keypressed = HandleShiftableCaplockableKey("t")
                ElseIf x >= 359 AndAlso x <= 411 Then 'y
                    Keypressed = HandleShiftableCaplockableKey("y")
                ElseIf x >= 414 AndAlso x <= 466 Then 'u
                    Keypressed = HandleShiftableCaplockableKey("u")
                ElseIf x >= 469 AndAlso x <= 521 Then 'i
                    Keypressed = HandleShiftableCaplockableKey("i")
                ElseIf x >= 524 AndAlso x <= 576 Then 'o
                    Keypressed = HandleShiftableCaplockableKey("o")
                ElseIf x >= 579 AndAlso x <= 631 Then 'p
                    Keypressed = HandleShiftableCaplockableKey("p")
                ElseIf x >= 634 AndAlso x <= 686 Then '[
                    Keypressed = HandleShiftableKey("[")
                ElseIf x >= 689 AndAlso x <= 741 Then ']
                    Keypressed = HandleShiftableKey("]")
                ElseIf x >= 743 AndAlso x <= 812 Then 'Blank
                    Keypressed = ""
                ElseIf x >= 816 AndAlso x <= 868 Then 'Del
                    Keypressed = "{DELETE}"
                End If

                '3ed Line
            Case y >= 115 AndAlso y <= 167
                If x >= 3 AndAlso x <= 104 Then 'caps
                    HandleCapsLock()
                ElseIf x >= 107 AndAlso x <= 159 Then 'a
                    Keypressed = HandleShiftableCaplockableKey("a")
                ElseIf x >= 162 AndAlso x <= 214 Then 's
                    Keypressed = HandleShiftableCaplockableKey("s")
                ElseIf x >= 217 AndAlso x <= 269 Then 'd
                    Keypressed = HandleShiftableCaplockableKey("d")
                ElseIf x >= 272 AndAlso x <= 324 Then 'f
                    Keypressed = HandleShiftableCaplockableKey("f")
                ElseIf x >= 327 AndAlso x <= 379 Then 'g
                    Keypressed = HandleShiftableCaplockableKey("g")
                ElseIf x >= 382 AndAlso x <= 434 Then 'h
                    Keypressed = HandleShiftableCaplockableKey("h")
                ElseIf x >= 437 AndAlso x <= 489 Then 'j
                    Keypressed = HandleShiftableCaplockableKey("j")
                ElseIf x >= 492 AndAlso x <= 544 Then 'k
                    Keypressed = HandleShiftableCaplockableKey("k")
                ElseIf x >= 547 AndAlso x <= 599 Then 'l
                    Keypressed = HandleShiftableCaplockableKey("l")
                ElseIf x >= 602 AndAlso x <= 654 Then ';
                    Keypressed = HandleShiftableKey(";")
                ElseIf x >= 657 AndAlso x <= 709 Then ''
                    Keypressed = HandleShiftableKey("'")
                ElseIf x >= 712 AndAlso x <= 813 Then 'Enter
                    Keypressed = "{ENTER}"
                ElseIf x >= 816 AndAlso x <= 868 Then 'Insert
                    Keypressed = "{INSERT}"
                End If

                '4th Line
            Case y >= 171 AndAlso y <= 223
                If x >= 3 AndAlso x <= 131 Then 'Shift
                    HandleShiftClick()
                ElseIf x >= 134 AndAlso x <= 186 Then 'z
                    Keypressed = HandleShiftableCaplockableKey("z")
                ElseIf x >= 189 AndAlso x <= 241 Then 'x
                    Keypressed = HandleShiftableCaplockableKey("x")
                ElseIf x >= 244 AndAlso x <= 296 Then 'c
                    Keypressed = HandleShiftableCaplockableKey("c")
                ElseIf x >= 299 AndAlso x <= 351 Then 'v
                    Keypressed = HandleShiftableCaplockableKey("v")
                ElseIf x >= 354 AndAlso x <= 406 Then 'b
                    Keypressed = HandleShiftableCaplockableKey("b")
                ElseIf x >= 409 AndAlso x <= 461 Then 'n
                    Keypressed = HandleShiftableCaplockableKey("n")
                ElseIf x >= 464 AndAlso x <= 516 Then 'm
                    Keypressed = HandleShiftableCaplockableKey("m")
                ElseIf x >= 519 AndAlso x <= 571 Then ',
                    Keypressed = HandleShiftableKey(",")
                ElseIf x >= 574 AndAlso x <= 626 Then '.
                    Keypressed = HandleShiftableKey(".")
                ElseIf x >= 629 AndAlso x <= 681 Then '/
                    Keypressed = HandleShiftableKey("/")
                ElseIf x >= 684 AndAlso x <= 758 Then 'Shift
                    HandleShiftClick()
                ElseIf x >= 761 AndAlso x <= 813 Then 'UP
                    Keypressed = "{UP}"
                ElseIf x >= 816 AndAlso x <= 868 Then 'END
                    Keypressed = "{END}"
                End If

                '5th Line
            Case y >= 227 AndAlso y <= 279
                If x >= 200 AndAlso x <= 615 Then 'Space
                    Keypressed = " "
                ElseIf x >= 706 AndAlso x <= 758 Then 'Left
                    Keypressed = "{LEFT}"
                ElseIf x >= 761 AndAlso x <= 813 Then 'DOWN
                    Keypressed = "{DOWN}"
                ElseIf x >= 816 AndAlso x <= 868 Then 'RIGHT
                    Keypressed = "{RIGHT}"
                End If
        End Select

        If Not Keypressed Is Nothing Then
            If shiftindicator Then
                HandleShiftClick()
            End If
            Return Keypressed
        Else
            Return Nothing
        End If

        Return Keypressed
    End Function

    Private Function HandleMouseClick_Number(ByVal x As Single, ByVal y As Single) As String
        Dim Keypressed As String = Nothing

        Select Case True
            '1St Line
            Case y >= 6 AndAlso y <= 69
                If x >= 13 AndAlso x <= 285 Then '1
                    Keypressed = "1"
                ElseIf x >= 298 AndAlso x <= 570 Then '2
                    Keypressed = "2"
                ElseIf x >= 584 AndAlso x <= 856 Then '3
                    Keypressed = "3"
                End If

                '2nd Line
            Case y >= 75 AndAlso y <= 138
                If x >= 13 AndAlso x <= 285 Then '4
                    Keypressed = "4"
                ElseIf x >= 298 AndAlso x <= 570 Then '5
                    Keypressed = "5"
                ElseIf x >= 584 AndAlso x <= 856 Then '6
                    Keypressed = "6"
                End If


                '3ed Line
            Case y >= 144 AndAlso y <= 207
                If x >= 13 AndAlso x <= 285 Then '7
                    Keypressed = "7"
                ElseIf x >= 298 AndAlso x <= 570 Then '8
                    Keypressed = "8"
                ElseIf x >= 584 AndAlso x <= 856 Then '9
                    Keypressed = "9"
                End If

                '4th Line
            Case y >= 213 AndAlso y <= 276
                If x >= 13 AndAlso x <= 285 Then 'BK
                    Keypressed = "{BACKSPACE}"
                ElseIf x >= 298 AndAlso x <= 570 Then '0
                    Keypressed = "0"
                ElseIf x >= 584 AndAlso x <= 856 Then 'Clear
                    Keypressed = "C"
                End If
        End Select

        Return Keypressed
    End Function

    Private Function HandleMouseClick_Decimal(ByVal x As Single, ByVal y As Single) As String
        Dim Keypressed As String = Nothing

        Select Case True
            '1St Line
            Case y >= 5 AndAlso y <= 69
                If x >= 12 AndAlso x <= 212 Then '1
                    Keypressed = "1"
                ElseIf x >= 227 AndAlso x <= 427 Then '2
                    Keypressed = "2"
                ElseIf x >= 442 AndAlso x <= 642 Then '3
                    Keypressed = "3"
                ElseIf x >= 657 AndAlso x <= 857 Then '3
                    Keypressed = "{BACKSPACE}"
                End If

                '2nd Line
            Case y >= 74 AndAlso y <= 138
                If x >= 12 AndAlso x <= 212 Then '4
                    Keypressed = "4"
                ElseIf x >= 227 AndAlso x <= 427 Then '5
                    Keypressed = "5"
                ElseIf x >= 442 AndAlso x <= 642 Then '6
                    Keypressed = "6"
                ElseIf x >= 657 AndAlso x <= 857 Then 'Clear
                    Keypressed = "C"
                End If

                '3ed Line
            Case y >= 144 AndAlso y <= 208
                If x >= 12 AndAlso x <= 212 Then '7
                    Keypressed = "7"
                ElseIf x >= 227 AndAlso x <= 427 Then '8
                    Keypressed = "8"
                ElseIf x >= 442 AndAlso x <= 642 Then '9
                    Keypressed = "9"
                ElseIf x >= 657 AndAlso x <= 857 Then 'Enetr
                    Keypressed = "{ENTER}"
                End If

                '4th Line
            Case y >= 213 AndAlso y <= 276
                If x >= 12 AndAlso x <= 212 Then '-
                    Keypressed = "-"
                ElseIf x >= 227 AndAlso x <= 427 Then '0
                    Keypressed = "0"
                ElseIf x >= 442 AndAlso x <= 642 Then '.
                    Keypressed = "."
                ElseIf x >= 657 AndAlso x <= 857 Then 'Enetr
                    Keypressed = "{ENTER}"
                End If
        End Select

        Return Keypressed
    End Function

    Private Function HandleMouseClick_Amount(ByVal x As Single, ByVal y As Single) As String
        Dim Keypressed As String = Nothing

        Select Case True
            '1St Line
            Case y >= 5 AndAlso y <= 69
                If x >= 7 AndAlso x <= 210 Then '1.00
                    Keypressed = "1.00"
                ElseIf x >= 219 AndAlso x <= 423 Then '5.00
                    Keypressed = "5.00"
                ElseIf x >= 432 AndAlso x <= 533 Then '1
                    Keypressed = "1"
                ElseIf x >= 542 AndAlso x <= 643 Then '2
                    Keypressed = "2"
                ElseIf x >= 652 AndAlso x <= 753 Then '3
                    Keypressed = "3"
                ElseIf x >= 762 AndAlso x <= 865 Then '3
                    Keypressed = "{BACKSPACE}"
                End If

                '2nd Line
            Case y >= 74 AndAlso y <= 138
                If x >= 7 AndAlso x <= 210 Then '10.00
                    Keypressed = "10.00"
                ElseIf x >= 219 AndAlso x <= 423 Then '20.00
                    Keypressed = "20.00"
                ElseIf x >= 432 AndAlso x <= 533 Then '4
                    Keypressed = "4"
                ElseIf x >= 542 AndAlso x <= 643 Then '5
                    Keypressed = "5"
                ElseIf x >= 652 AndAlso x <= 753 Then '6
                    Keypressed = "6"
                ElseIf x >= 762 AndAlso x <= 865 Then 'Clear
                    Keypressed = "C"
                End If

                '3ed Line
            Case y >= 144 AndAlso y <= 208
                If x >= 7 AndAlso x <= 210 Then '50
                    Keypressed = "50.00"
                ElseIf x >= 219 AndAlso x <= 423 Then '100
                    Keypressed = "100.00"
                ElseIf x >= 432 AndAlso x <= 533 Then '7
                    Keypressed = "7"
                ElseIf x >= 542 AndAlso x <= 643 Then '8
                    Keypressed = "8"
                ElseIf x >= 652 AndAlso x <= 753 Then '9
                    Keypressed = "9"
                ElseIf x >= 762 AndAlso x <= 865 Then 'Enter
                    Keypressed = "{ENTER}"
                End If

                '4th Line
            Case y >= 213 AndAlso y <= 276
                If x >= 7 AndAlso x <= 210 Then '
                    Keypressed = "#"
                ElseIf x >= 219 AndAlso x <= 423 Then '
                    Keypressed = "@"
                ElseIf x >= 432 AndAlso x <= 533 Then '-
                    Keypressed = "-"
                ElseIf x >= 542 AndAlso x <= 643 Then '0
                    Keypressed = "0"
                ElseIf x >= 652 AndAlso x <= 753 Then '.
                    Keypressed = "."
                ElseIf x >= 762 AndAlso x <= 865 Then 'Enter
                    Keypressed = "{ENTER}"
                End If
        End Select

        Return Keypressed
    End Function


    Private Function HandleShiftableKey(ByVal theKey As String) As String
        If shiftindicator Then
            Return "+" & theKey
        Else
            Return theKey
        End If
    End Function

    Private Function HandleShiftableCaplockableKey(ByVal theKey As String) As String
        If pvtKeyboardType <> eZeeKeyBoardLayOut.KeyBoard_Standard Then
            Select Case theKey
                Case ("q")
                    theKey = "a"
                Case ("w")
                    theKey = "b"
                Case ("e")
                    theKey = "c"
                Case ("r")
                    theKey = "d"
                Case ("t")
                    theKey = "e"
                Case ("y")
                    theKey = "f"
                Case ("u")
                    theKey = "g"
                Case ("i")
                    theKey = "h"
                Case ("o")
                    theKey = "i"
                Case ("p")
                    theKey = "j"
                Case ("a")
                    theKey = "k"
                Case ("s")
                    theKey = "l"
                Case ("d")
                    theKey = "m"
                Case ("f")
                    theKey = "n"
                Case ("g")
                    theKey = "o"
                Case ("h")
                    theKey = "p"
                Case ("j")
                    theKey = "q"
                Case ("k")
                    theKey = "r"
                Case ("l")
                    theKey = "s"
                Case ("z")
                    theKey = "t"
                Case ("x")
                    theKey = "u"
                Case ("c")
                    theKey = "v"
                Case ("v")
                    theKey = "w"
                Case ("b")
                    theKey = "x"
                Case ("n")
                    theKey = "y"
                Case ("m")
                    theKey = "z"
            End Select
        End If
        If capslockindicator Then
            Return "+" & theKey
        ElseIf shiftindicator Then
            Return "+" & theKey
        Else
            Return theKey
        End If
    End Function

    Private Sub HandleShiftClick()
        If shiftindicator Then
            shiftindicator = False
            picLeftShiftDown.Visible = False
            picRightShiftDown.Visible = False
        Else
            shiftindicator = True
            picLeftShiftDown.Visible = True
            picRightShiftDown.Visible = True
        End If
    End Sub

    Private Sub HandleCapsLock()
        If capslockindicator Then
            capslockindicator = False
            picCapsLockDown.Visible = False
        Else
            capslockindicator = True
            picCapsLockDown.Visible = True
        End If
    End Sub

#End Region

End Class

