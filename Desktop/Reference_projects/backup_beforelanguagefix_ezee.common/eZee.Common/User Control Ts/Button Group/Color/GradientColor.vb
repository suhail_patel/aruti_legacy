Imports System
Imports System.ComponentModel
Imports System.Drawing
Imports System.Drawing.Design
Imports System.Runtime.CompilerServices

<TypeConverter(GetType(GradientColorTypeConverter))> _
Public Class GradientColor
    Private m_endColor As Color
    Private m_fillDirection As Drawing2D.LinearGradientMode
    Private m_startColor As Color

    '<Editor(GetType(GradientColorTypeEditor), GetType(UITypeEditor)), TypeConverter(GetType(GradientColorTypeConverter))> _

    Public Event PropertyChanged As EventHandler

    Public Sub New()
        Me.m_endColor = Color.White
        Me.m_startColor = Me.m_endColor
        Me.m_fillDirection = Drawing2D.LinearGradientMode.Vertical
    End Sub

    Public Sub New(ByVal startColor As Color, ByVal endColor As Color)
        Me.m_startColor = startColor
        Me.m_endColor = endColor
        Me.m_fillDirection = Drawing2D.LinearGradientMode.Vertical
    End Sub

    Public Sub New(ByVal startColor As Color, ByVal endColor As Color, ByVal fillDirection As Drawing2D.LinearGradientMode)
        Me.m_startColor = startColor
        Me.m_endColor = endColor
        Me.m_fillDirection = fillDirection
    End Sub

    Protected Overridable Sub OnPropertyChanged(ByVal e As EventArgs)
        If Not Me.PropertyChangedEvent Is Nothing Then
            RaiseEvent PropertyChanged(Me, e)
        End If
    End Sub

    Protected Overridable Function ShouldSerializeEndColor() As Boolean
        Return (Me.m_endColor.ToArgb() <> Color.White.ToArgb())
    End Function

    Protected Overridable Function ShouldSerializeFillDirection() As Boolean
        Return (Me.m_fillDirection <> Drawing2D.LinearGradientMode.Vertical)
    End Function

    Protected Overridable Function ShouldSerializeStartColor() As Boolean
        Return (Me.m_startColor.ToArgb() <> Color.White.ToArgb())
    End Function

    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
   <DefaultValue(GetType(Color), "LightGray")> _
   Public Property StartColor() As Color
        Get
            Return Me.m_startColor
        End Get
        Set(ByVal value As Color)
            If value.IsEmpty Then
                value = Color.White
            End If
            Me.m_startColor = value
            Me.OnPropertyChanged(EventArgs.Empty)
        End Set
    End Property

    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    <DefaultValue(GetType(Color), "DarkGray")> _
    Public Property EndColor() As Color
        Get
            Return Me.m_endColor
        End Get
        Set(ByVal value As Color)
            If value.IsEmpty Then
                value = Color.White
            End If
            Me.m_endColor = value
            Me.OnPropertyChanged(EventArgs.Empty)
        End Set
    End Property

    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    <DefaultValue(GetType(Drawing2D.LinearGradientMode), "Vertical")> _
    Public Property FillDirection() As Drawing2D.LinearGradientMode
        Get
            Return Me.m_fillDirection
        End Get
        Set(ByVal value As Drawing2D.LinearGradientMode)
            Me.m_fillDirection = value
            Me.OnPropertyChanged(EventArgs.Empty)
        End Set
    End Property

End Class
