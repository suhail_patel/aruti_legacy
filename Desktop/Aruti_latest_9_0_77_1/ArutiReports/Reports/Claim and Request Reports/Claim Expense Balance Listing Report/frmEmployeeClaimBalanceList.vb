#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
#End Region

Public Class frmEmployeeClaimBalanceList

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmEmployeeClaimBalanceList"
    Private objEmpClaimBalanceReport As clsEmployeeClaimBalanceReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region "Constructor"
    Public Sub New()
        objEmpClaimBalanceReport = New clsEmployeeClaimBalanceReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpClaimBalanceReport.SetDefaultValue()
        InitializeComponent()
        _Show_AdvanceFilter = True
    End Sub
#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsCombo As New DataSet
        Try
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True)
            cboExpenseCategory.ValueMember = "Id"
            cboExpenseCategory.DisplayMember = "Name"
            cboExpenseCategory.DataSource = dsCombo.Tables(0)

            cboEmployee.DataSource = (New clsEmployee_Master).GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True).Tables(0)

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"

            Dim objMaster As New clsMasterData
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsCombo = objMaster.GetCondition(True, True, False, False)
            dsCombo = objMaster.GetCondition(True, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            cboFromCondition.DisplayMember = "Name"
            cboFromCondition.ValueMember = "id"
            cboFromCondition.DataSource = dsCombo.Tables(0)

            cboToCondition.DisplayMember = "Name"
            cboToCondition.ValueMember = "id"
            cboToCondition.DataSource = dsCombo.Tables(0).Copy

            objMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub FillList()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpFromDate.Value = DateTime.Now
            cboEmployee.SelectedValue = 0
            cboExpenseCategory.SelectedIndex = 0
            cboExpense.SelectedIndex = 0
            cboFromCondition.SelectedIndex = 0
            txtFromExpenseAmt.Decimal = 0
            cboToCondition.SelectedIndex = 0
            txtToExpenseAmt.Decimal = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAdvanceFilter = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            objEmpClaimBalanceReport.setDefaultOrderBy(cboExpenseCategory.SelectedIndex)
            txtOrderBy.Text = objEmpClaimBalanceReport.OrderByDisplay

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboExpenseCategory.Select()
                Return False
            End If

            objEmpClaimBalanceReport.SetDefaultValue()

            objEmpClaimBalanceReport._EmployeeId = cboEmployee.SelectedValue
            objEmpClaimBalanceReport._EmployeeName = cboEmployee.Text
            objEmpClaimBalanceReport._ExpenseCategoryId = CInt(cboExpenseCategory.SelectedValue)
            objEmpClaimBalanceReport._ExpenseCategory = cboExpenseCategory.Text
            objEmpClaimBalanceReport._ExpenseId = CInt(cboExpense.SelectedValue)
            objEmpClaimBalanceReport._Expense = cboExpense.Text
            objEmpClaimBalanceReport._FromConditionID = CInt(cboFromCondition.SelectedValue)
            objEmpClaimBalanceReport._FromCondition = cboFromCondition.Text
            objEmpClaimBalanceReport._FromAmount = txtFromExpenseAmt.Decimal
            objEmpClaimBalanceReport._ToConditionID = CInt(cboToCondition.SelectedValue)
            objEmpClaimBalanceReport._ToCondition = cboToCondition.Text
            objEmpClaimBalanceReport._ToAmount = txtToExpenseAmt.Decimal
            objEmpClaimBalanceReport._IsPaymentApprovalwithLeaveApproval = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
            objEmpClaimBalanceReport._ReportId = cboExpenseCategory.SelectedIndex
            objEmpClaimBalanceReport._ReportTypeName = cboExpenseCategory.Text
            objEmpClaimBalanceReport._FromDate = dtpFromDate.Value.Date
            objEmpClaimBalanceReport._ToDate = dtpToDate.Value.Date
            objEmpClaimBalanceReport._ViewByIds = mstrStringIds
            objEmpClaimBalanceReport._ViewIndex = mintViewIdx
            objEmpClaimBalanceReport._ViewByName = mstrStringName
            objEmpClaimBalanceReport._Analysis_Fields = mstrAnalysis_Fields
            objEmpClaimBalanceReport._Analysis_Join = mstrAnalysis_Join
            objEmpClaimBalanceReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpClaimBalanceReport._Report_GroupName = mstrReport_GroupName
            objEmpClaimBalanceReport._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmEmployeeClaimBalanceList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpClaimBalanceReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimBalanceList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimBalanceList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Me._Title = objEmpClaimBalanceReport._ReportName
            Me._Message = objEmpClaimBalanceReport._ReportDesc

            Call FillCombo()
            Call FillList()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimBalanceList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimBalanceList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmployeeClaimBalanceList_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimBalanceList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimBalanceList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimBalanceList_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons"

    Private Sub frmEmployeeClaimBalanceList_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Dim strLeaveId As String = ""

        Try

            If SetFilter() = False Then Exit Sub



            'Pinkal (30-Mar-2017) -- Start
            'Enhancement - Changing in Claim Balance Listing Report for CCK as per Matthew AND CCK Requirement.

            'objEmpClaimBalanceReport.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                                User._Object._Userunkid, _
            '                                                FinancialYear._Object._YearUnkid, _
            '                                                Company._Object._Companyunkid, _
            '                                                dtpFromDate.Value.Date, _
            '                                                dtpFromDate.Value.Date, _
            '                                                ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                                ConfigParameter._Object._ExportReportPath, _
            '                                                ConfigParameter._Object._OpenAfterExport, _
            '                                                cboExpenseCategory.SelectedIndex, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)

            objEmpClaimBalanceReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                            User._Object._Userunkid, _
                                                            FinancialYear._Object._YearUnkid, _
                                                            Company._Object._Companyunkid, _
                                                            dtpFromDate.Value.Date, _
                                                           dtpToDate.Value.Date, _
                                                            ConfigParameter._Object._UserAccessModeSetting, True, _
                                                            ConfigParameter._Object._ExportReportPath, _
                                                            ConfigParameter._Object._OpenAfterExport, _
                                                            cboExpenseCategory.SelectedIndex, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)

            'Pinkal (30-Mar-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimBalanceList_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimBalanceList_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Dim strLeaveId As String = ""
        Try
            If SetFilter() = False Then Exit Sub

            objEmpClaimBalanceReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                           User._Object._Userunkid, _
                                                           FinancialYear._Object._YearUnkid, _
                                                           Company._Object._Companyunkid, _
                                                           dtpFromDate.Value.Date, _
                                                           dtpFromDate.Value.Date, _
                                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                                           ConfigParameter._Object._ExportReportPath, _
                                                           ConfigParameter._Object._OpenAfterExport, _
                                                           cboExpenseCategory.SelectedIndex, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimBalanceList_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimBalanceList_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimBalanceList_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimBalanceList_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeClaimBalanceList_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeClaimBalanceList_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeClaimBalanceReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeClaimBalanceReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEmployeeClaimBalanceList_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchExpense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpense.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExpense.DataSource
            frm.ValueMember = cboExpense.ValueMember
            frm.DisplayMember = cboExpense.DisplayMember
            If frm.DisplayDialog Then
                cboExpense.SelectedValue = frm.SelectedValue
                cboExpense.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExpense_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Controls"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmpClaimBalanceReport.setOrderBy(cboExpenseCategory.SelectedIndex)
            txtOrderBy.Text = objEmpClaimBalanceReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim objExpMst As New clsExpense_Master
            Dim dsCombo As DataSet = objExpMst.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List", CInt(cboEmployee.SelectedValue), False)
            Dim dtTable As DataTable = New DataView(dsCombo.Tables(0), "Id <=0 OR isaccrue = 1", "", DataViewRowState.CurrentRows).ToTable
            cboExpense.DisplayMember = "Name"
            cboExpense.ValueMember = "Id"
            cboExpense.DataSource = dtTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpenseCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region



  
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.LblExpenseCategory.Text = Language._Object.getCaption(Me.LblExpenseCategory.Name, Me.LblExpenseCategory.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.LblExpense.Text = Language._Object.getCaption(Me.LblExpense.Name, Me.LblExpense.Text)
			Me.LblPayableAmount.Text = Language._Object.getCaption(Me.LblPayableAmount.Name, Me.LblPayableAmount.Text)
			Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
