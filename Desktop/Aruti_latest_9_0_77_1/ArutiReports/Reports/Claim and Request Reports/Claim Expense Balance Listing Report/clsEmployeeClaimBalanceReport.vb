Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Math

Public Class clsEmployeeClaimBalanceReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeClaimBalanceReport"
    Private mstrReportId As String = enArutiReport.Employee_Claim_Balance_Report '180
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintExpenseCategoryId As Integer = 0
    Private mstrExpenseCategory As String = ""
    Private mintExpenseId As Integer = 0
    Private mstrExpense As String = ""
    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintFromCondition As Integer = 0
    Private mstrFromCondition As String = ""
    Private mdecFromAmount As Decimal = 0
    Private mintToCondition As Integer = 0
    Private mstrToCondition As String = ""
    Private mdecToAmount As Decimal = 0
    Private mstrOrderByQuery As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Dim mblnIncludeInactiveEmp As Boolean = False
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mblnIncludeAccessFilterQry As Boolean = True
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseCategoryId() As Integer
        Set(ByVal value As Integer)
            mintExpenseCategoryId = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseCategory() As String
        Set(ByVal value As String)
            mstrExpenseCategory = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseId() As Integer
        Set(ByVal value As Integer)
            mintExpenseId = value
        End Set
    End Property

    Public WriteOnly Property _Expense() As String
        Set(ByVal value As String)
            mstrExpense = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _FromConditionID() As Integer
        Set(ByVal value As Integer)
            mintFromCondition = value
        End Set
    End Property

    Public WriteOnly Property _FromCondition() As String
        Set(ByVal value As String)
            mstrFromCondition = value
        End Set
    End Property

    Public WriteOnly Property _FromAmount() As Decimal
        Set(ByVal value As Decimal)
            mdecFromAmount = value
        End Set
    End Property

    Public WriteOnly Property _ToConditionID() As Integer
        Set(ByVal value As Integer)
            mintToCondition = value
        End Set
    End Property

    Public WriteOnly Property _ToCondition() As String
        Set(ByVal value As String)
            mstrToCondition = value
        End Set
    End Property

    Public WriteOnly Property _ToAmount() As Decimal
        Set(ByVal value As Decimal)
            mdecToAmount = value
        End Set
    End Property

    Public Property _IsPaymentApprovalwithLeaveApproval() As Boolean
        Get
            Return mblnPaymentApprovalwithLeaveApproval
        End Get
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintExpenseCategoryId = 0
            mstrExpenseCategory = ""
            mintExpenseId = 0
            mstrExpense = ""
            mintFromCondition = 0
            mstrFromCondition = ""
            mdecFromAmount = 0
            mintToCondition = 0
            mstrToCondition = ""
            mdecToAmount = 0
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mstrOrderByQuery = ""
            mblnIncludeInactiveEmp = False
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnIncludeAccessFilterQry = True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, " From Date : ") & " " & mdtFromDate.ToShortDateString & " "
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, " To Date : ") & " " & mdtToDate.ToShortDateString & " "

            'Pinkal (30-Mar-2017) -- Start
            'Enhancement - Changing in Claim Balance Listing Report for CCK as per Matthew AND CCK Requirement.
            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromDate).ToString())
            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToDate).ToString())
            'Pinkal (30-Mar-2017) -- End


            If mintExpenseCategoryId > 0 Then
                objDataOperation.AddParameter("@ExpenseCatID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryId)
                Me._FilterQuery &= " AND cmexpense_master.expensetypeid = @ExpenseCatID "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, " Expense Category : ") & " " & mstrExpenseCategory & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, " Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintExpenseId > 0 Then
                objDataOperation.AddParameter("@ExpenseID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseId)
                Me._FilterQuery &= " AND cmexpbalance_tran.expenseunkid = @ExpenseID "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, " Expense : ") & " " & mstrExpense & " "
            End If

            If mintFromCondition > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, " Payable Amount From : ") & " " & mstrFromCondition & " " & mdecFromAmount & " "
            End If

            If mintToCondition > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, " To ") & mstrToCondition & " " & mdecToAmount & " "
            End If


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Order By :") & " " & Me.OrderByDisplay & " "
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= "ORDER BY GName," & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                , ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                , ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer _
                                                                , Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                                , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                                , Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("Employeecode", Language.getMessage(mstrModuleName, 1, "Code")))
            iColumn_DetailReport.Add(New IColumn("Employee", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumn_DetailReport.Add(New IColumn("Department", Language.getMessage(mstrModuleName, 3, "Department")))
            iColumn_DetailReport.Add(New IColumn("Grade", Language.getMessage(mstrModuleName, 4, "Grade")))
            iColumn_DetailReport.Add(New IColumn("Expense", Language.getMessage(mstrModuleName, 5, "Expense")))


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                , ByVal xEmployeeAsonDate As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xEmployeeAsonDate, xEmployeeAsonDate, , , xDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xEmployeeAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, "")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xEmployeeAsonDate, xDatabaseName)


            StrQ = " SELECT " & _
                       " hremployee_master.employeeunkid " & _
                       ", ISNULL(hremployee_master.employeecode, '') AS Employeecode "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS Employee "
            End If


            StrQ &= ", ISNULL(dm.name, '') AS Department " & _
                         ", ISNULL(GM.name, '') AS Grade " & _
                         ", ISNULL(cmexpense_master.expenseunkid, '') AS expenseunkid " & _
                         ", ISNULL(cmexpense_master.name, '') AS Expense " & _
                         ", cmexpense_master.isaccrue " & _
                         ", cmexpense_master.uomunkid " & _
                         ", ISNULL(cmexpbalance_tran.accrue_amount, 0.00) AS Accrue_amount " & _
                         ", cmexpbalance_tran.startdate " & _
                         ", cmexpbalance_tran.enddate " & _
                         ", cmexpbalance_tran.daily_amount " & _
                         ", ISNULL(Bal.amount,0) AS [Payable Amount] " & _
                         ", ISNULL(cmexpbalance_tran.accrue_amount,0.00) -  ISNULL(cmexpbalance_tran.issue_amount,0.00) AS Remaining_Bal"

            'Pinkal (30-Mar-2017) -- Start
            'Enhancement - Changing in Claim Balance Listing Report for CCK as per Matthew AND CCK Requirement.
            '", 0 AS [Payable Amount] " & _
            'Pinkal (30-Mar-2017) -- End


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "  FROM cmexpbalance_tran " & _
                         "  LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpbalance_tran.employeeunkid " & _
                         "  LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmexpbalance_tran.expenseunkid " & _
                         "  LEFT JOIN  " & _
                         "  ( " & _
                         "      SELECT " & _
                         "      departmentunkid " & _
                         "    , employeeunkid " & _
                         "    , ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "       FROM hremployee_transfer_tran " & _
                         "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtToDate) & "' ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid  AND Alloc.rno = 1 " & _
                         " JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                         " LEFT JOIN  " & _
                         " (  " & _
                         "       SELECT  " & _
                         "       gradeunkid " & _
                         "     , employeeunkid " & _
                         "     , ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno  " & _
                         " FROM prsalaryincrement_tran " & _
                         " WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtToDate) & "' ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                         " JOIN hrgrade_master AS GM ON Grds.gradeunkid = GM.gradeunkid "



            'Pinkal (30-Mar-2017) -- Start
            'Enhancement - Changing in Claim Balance Listing Report for CCK as per Matthew AND CCK Requirement.

            StrQ &= " JOIN " & _
                         " ( " & _
                         "          SELECT " & _
                         "           crmasterunkid " & _
                         "          ,expenseunkid " & _
                         "          ,employeeunkid " & _
                         "          ,approvaldate " & _
                         "          , rno " & _
                         "          , Amount " & _
                         "           FROM " & _
                         "           ( " & _
                         "              SELECT " & _
                         "                   cmclaim_request_master.crmasterunkid " & _
                         "                  ,cmclaim_approval_tran.expenseunkid " & _
                         "                  ,cmclaim_request_master.employeeunkid " & _
                         "                  ,cmclaim_approval_tran.approvaldate " & _
                         "                  ,ISNULL(cmclaim_approval_tran.amount,0) AS Amount "

            If mblnPaymentApprovalwithLeaveApproval Then
                StrQ &= "                  ,ROW_NUMBER()OVER(PARTITION BY cmclaim_request_master.crmasterunkid ORDER BY priority DESC) AS rno "
            Else
                StrQ &= "                  ,ROW_NUMBER()OVER(PARTITION BY cmclaim_request_master.crmasterunkid ORDER BY crpriority DESC) AS rno "
            End If


            StrQ &= "                  FROM cmclaim_request_master " & _
                        "                  JOIN cmclaim_approval_tran ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid "

            If mblnPaymentApprovalwithLeaveApproval Then

                StrQ &= "                  JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                             "                  JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 "
            Else
                StrQ &= "                  JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                             "                  JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmapproverlevel_master.isactive = 1 "
            End If

            StrQ &= "                  WHERE cmclaim_approval_tran.isvoid = 0 AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 " & _
                    "                  AND cmclaim_approval_tran.approvaldate IS NOT NULL" & _
                    "           ) AS B  WHERE B.rno = 1  " & _
                    " ) AS bal ON cmexpbalance_tran.employeeunkid = bal.employeeunkid AND cmexpbalance_tran.expenseunkid = bal.expenseunkid " & _
                    " AND CONVERT(CHAR(8),bal.approvaldate,112) >= @FromDate AND CONVERT(CHAR(8),bal.approvaldate,112) <= @ToDate "

            'Pinkal (30-Mar-2017) -- End


            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE cmexpbalance_tran.isvoid = 0 AND cmexpbalance_tran.accrue_amount > 0 AND cmexpense_master.isaccrue = 1 "


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim rpt_Rows As DataRow = Nothing

            Dim objclaimMst As New clsclaim_request_master

            For Each dFRow As DataRow In dsList.Tables(0).Rows
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dFRow.Item("GName")
                rpt_Rows.Item("Column2") = dFRow.Item("Employeecode")
                rpt_Rows.Item("Column3") = dFRow.Item("Employee")
                rpt_Rows.Item("Column4") = dFRow.Item("Department")
                rpt_Rows.Item("Column5") = dFRow.Item("Grade")
                rpt_Rows.Item("Column6") = dFRow.Item("Expense")

                If CInt(dFRow.Item("uomunkid")) = 1 Then
                    rpt_Rows.Item("Column7") = Math.Round(CDec(dFRow.Item("Accrue_amount")), 2)

                    rpt_Rows.Item("Column8") = Math.Round(CDec(dFRow.Item("Remaining_Bal")), 2)

                    'Pinkal (30-Mar-2017) -- Start
                    'Enhancement - Changing in Claim Balance Listing Report for CCK as per Matthew AND CCK Requirement.

                    'rpt_Rows.Item("Column9") = Math.Round(objclaimMst.GetApprovedClaimApprovedAmount(mblnPaymentApprovalwithLeaveApproval, CInt(dFRow.Item("employeeunkid")) _
                    '                                                                                                               , mintExpenseCategoryId, CInt(dFRow.Item("expenseunkid")), CBool(dFRow.Item("isaccrue"))), 2)
                    rpt_Rows.Item("Column9") = Math.Round(CDec(dFRow.Item("Payable Amount")), 2)

                    'Pinkal (30-Mar-2017) -- End

                ElseIf CInt(dFRow.Item("uomunkid")) = 2 Then
                    rpt_Rows.Item("Column7") = Format(CDec(dFRow.Item("Accrue_amount")), GUI.fmtCurrency)

                    rpt_Rows.Item("Column8") = Format(CDec(dFRow.Item("Remaining_Bal")), GUI.fmtCurrency)

                    'Pinkal (30-Mar-2017) -- Start
                    'Enhancement - Changing in Claim Balance Listing Report for CCK as per Matthew AND CCK Requirement.

                    'rpt_Rows.Item("Column9") = Format(objclaimMst.GetApprovedClaimApprovedAmount(mblnPaymentApprovalwithLeaveApproval, CInt(dFRow.Item("employeeunkid")) _
                    '                                                                                                               , mintExpenseCategoryId, CInt(dFRow.Item("expenseunkid")), CBool(dFRow.Item("isaccrue"))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column9") = Math.Round(CDec(dFRow.Item("Payable Amount")), 2)

                    'Pinkal (30-Mar-2017) -- End

                End If

                rpt_Rows.Item("Column10") = CInt(dFRow.Item("Id"))
                rpt_Rows.Item("Column11") = CInt(dFRow.Item("employeeunkid"))
                rpt_Rows.Item("Column12") = CInt(dFRow.Item("uomunkid"))
                rpt_Rows.Item("Column81") = CDec(rpt_Rows.Item("Column9"))
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            Dim strFilter As String = ""
            If mintFromCondition > 0 Then
                strFilter = "Column81 " & mstrFromCondition & " " & mdecFromAmount
            End If
            If mintToCondition > 0 Then
                If mintFromCondition > 0 Then
                    strFilter &= "AND Column81 " & mstrToCondition & " " & mdecToAmount
                Else
                    strFilter &= "Column81 " & mstrToCondition & " " & mdecToAmount
                End If
            End If


            Dim dtTable As DataTable = New DataView(rpt_Data.Tables("ArutiTable").Copy, strFilter, "", DataViewRowState.CurrentRows).ToTable
            rpt_Data.Tables.Remove("ArutiTable")
            rpt_Data.Tables.Add(dtTable)

            objclaimMst = Nothing

            objRpt = New ArutiReport.Designer.rptEmpClaim_ExpenseBalanceList

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 18, "Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 19, "Employee :"))


            If mintViewIndex <= 0 Then
                ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            Else
                Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
                Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", mstrReport_GroupName.Trim.Replace(":", "") & Language.getMessage(mstrModuleName, 20, " Total :"))
            End If



            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 6, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 7, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 8, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 9, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 21, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtGrade", Language.getMessage(mstrModuleName, 22, "Grade"))
            Call ReportFunction.TextChange(objRpt, "txtExpense", Language.getMessage(mstrModuleName, 23, "Expense"))
            Call ReportFunction.TextChange(objRpt, "txtClaimAssigned", Language.getMessage(mstrModuleName, 24, "Claim Limit Assigned"))
            Call ReportFunction.TextChange(objRpt, "txtBalanceAsondate", Language.getMessage(mstrModuleName, 25, "Balance As On Date"))
            Call ReportFunction.TextChange(objRpt, "txtPayableAmount", Language.getMessage(mstrModuleName, 26, "Payable Amount"))

            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 27, "Group Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 28, "Grand Total :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 29, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 30, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Code")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "Department")
            Language.setMessage(mstrModuleName, 4, "Grade")
            Language.setMessage(mstrModuleName, 5, "Expense")
            Language.setMessage(mstrModuleName, 6, "Prepared By :")
            Language.setMessage(mstrModuleName, 7, "Checked By :")
            Language.setMessage(mstrModuleName, 8, "Approved By :")
            Language.setMessage(mstrModuleName, 9, "Received By :")
            Language.setMessage(mstrModuleName, 10, " From Date :")
            Language.setMessage(mstrModuleName, 11, " To Date :")
            Language.setMessage(mstrModuleName, 12, " Expense Category :")
            Language.setMessage(mstrModuleName, 13, " Employee :")
            Language.setMessage(mstrModuleName, 14, " Expense :")
            Language.setMessage(mstrModuleName, 15, " Payable Amount From :")
            Language.setMessage(mstrModuleName, 16, " To")
            Language.setMessage(mstrModuleName, 17, "Order By :")
            Language.setMessage(mstrModuleName, 18, "Code :")
            Language.setMessage(mstrModuleName, 19, "Employee :")
            Language.setMessage(mstrModuleName, 20, " Total :")
            Language.setMessage(mstrModuleName, 21, "Department")
            Language.setMessage(mstrModuleName, 22, "Grade")
            Language.setMessage(mstrModuleName, 23, "Expense")
            Language.setMessage(mstrModuleName, 24, "Claim Limit Assigned")
            Language.setMessage(mstrModuleName, 25, "Balance As On Date")
            Language.setMessage(mstrModuleName, 26, "Payable Amount")
            Language.setMessage(mstrModuleName, 27, "Group Total :")
            Language.setMessage(mstrModuleName, 28, "Grand Total :")
            Language.setMessage(mstrModuleName, 29, "Printed By :")
            Language.setMessage(mstrModuleName, 30, "Printed Date :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
