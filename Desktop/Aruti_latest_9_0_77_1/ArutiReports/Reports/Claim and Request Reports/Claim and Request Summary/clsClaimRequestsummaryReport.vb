'************************************************************************************************************************************
'Class Name :clsAirPassageReport.vb
'Purpose    :
'Date       : 08-Aug-2014
'Written By : Shani Sheladiya.
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsClaimRequestsummaryReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsClaimRequestsummaryReport"
    Private mstrReportId As String = enArutiReport.CR_Summary_Report
    Dim objDataOperation As clsDataOperation
#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "
    Private mdtFromdate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintExpCateId As Integer = 0
    Private mstrExpCateName As String = ""
    Private mintEmpUnkId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = ""
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Dim mblnShowClaimFormStatus As Boolean = True
    'Pinkal (18-Feb-2016) -- End


    'Pinkal (22-Mar-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
    Private mintUOMunkid As Integer
    Private mstrUOM As String
    Private mintFromPeriodID As Integer = 0
    Private mstrFromPeriod As String = ""
    Private mintToPeriodID As Integer = 0
    Private mstrToPeriod As String = ""
    Private mdtPeriodStartDate As DateTime = Nothing
    Private mdtPeriodEndDate As DateTime = Nothing
    'Pinkal (22-Mar-2016) -- End

    'Pinkal (18-Apr-2016) -- Start
    'Enhancement - Enhancement for KBC As Per Allan's Comment.
    Private mintExpenseID As Integer = -1
    Private mstrExpense As String = ""
    'Pinkal (18-Apr-2016) -- End


    'Pinkal (04-May-2020) -- Start
    'Enhancement NMB Claim Request Summary Report -   Working on Claim Request Summary Report as NMB Wants Analysis by.
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Pinkal (04-May-2020) -- End



    'Pinkal (30-Mar-2021)-- Start
    'NMB Enhancement  -  Working on Employee Recategorization history Report.
    Private mintCurrencyId As Integer = 0
    Private mstrCurrency As String = ""
    'Pinkal (30-Mar-2021) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromdate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _ExpCateId() As Integer
        Set(ByVal value As Integer)
            mintExpCateId = value
        End Set
    End Property

    Public WriteOnly Property _ExpCateName() As String
        Set(ByVal value As String)
            mstrExpCateName = value
        End Set
    End Property

    Public WriteOnly Property _EmpUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmpUnkId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkid() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    'Pinkal (18-Feb-2016) -- Start
    'Enhancement - CR Changes for ASP as per Rutta's Request.

    Public WriteOnly Property _ShowClaimFormStatus() As Boolean
        Set(ByVal value As Boolean)
            mblnShowClaimFormStatus = value
        End Set
    End Property

    'Pinkal (18-Feb-2016) -- End


    'Pinkal (22-Mar-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.

    Public WriteOnly Property _UOMId() As Integer
        Set(ByVal value As Integer)
            mintUOMunkid = value
        End Set
    End Property

    Public WriteOnly Property _UOM() As String
        Set(ByVal value As String)
            mstrUOM = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodId() As Integer
        Set(ByVal value As Integer)
            mintFromPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriod() As String
        Set(ByVal value As String)
            mstrFromPeriod = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriod() As String
        Set(ByVal value As String)
            mstrToPeriod = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriodEndDate = value
        End Set
    End Property

    'Pinkal (22-Mar-2016) -- End


    'Pinkal (18-Apr-2016) -- Start
    'Enhancement - Enhancement for KBC As Per Allan's Comment.

    Public WriteOnly Property _ExpenseID() As Integer
        Set(ByVal value As Integer)
            mintExpenseID = value
        End Set
    End Property

    Public WriteOnly Property _Expense() As String
        Set(ByVal value As String)
            mstrExpense = value
        End Set
    End Property

    'Pinkal (18-Apr-2016) -- End


    'Pinkal (04-May-2020) -- Start
    'Enhancement NMB Claim Request Summary Report -   Working on Claim Request Summary Report as NMB Wants Analysis by.

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Pinkal (04-May-2020) -- End


    'Pinkal (30-Mar-2021)-- Start
    'NMB Enhancement  -  Working on Employee Recategorization history Report.
    Public Property _CurrencyId() As Integer
        Get
            Return mintCurrencyId
        End Get
        Set(ByVal value As Integer)
            mintCurrencyId = value
        End Set
    End Property

    Public Property _Currency() As String
        Get
            Return mstrCurrency
        End Get
        Set(ByVal value As String)
            mstrCurrency = value
        End Set
    End Property
    'Pinkal (30-Mar-2021) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtFromdate = Nothing
            mdtToDate = Nothing
            mintExpCateId = 0
            mstrExpCateName = ""
            mintEmpUnkId = 0
            mstrEmpName = ""
            mintStatusId = 0
            mstrStatusName = ""
            mintUserUnkid = 0
            mintCompanyUnkid = 0

            'Pinkal (18-Feb-2016) -- Start
            'Enhancement - CR Changes for ASP as per Rutta's Request.
            mblnShowClaimFormStatus = True
            'Pinkal (18-Feb-2016) -- End


            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            mintUOMunkid = 0
            mstrUOM = ""
            mintFromPeriodID = 0
            mstrFromPeriod = ""
            mintToPeriodID = 0
            mstrToPeriod = ""
            mdtPeriodStartDate = Nothing
            mdtPeriodEndDate = Nothing
            'Pinkal (22-Mar-2016) -- End

            'Pinkal (18-Apr-2016) -- Start
            'Enhancement - Enhancement for KBC As Per Allan's Comment.
            mintExpenseID = 0
            mstrExpense = ""
            'Pinkal (18-Apr-2016) -- End


            'Pinkal (04-May-2020) -- Start
            'Enhancement NMB Claim Request Summary Report -   Working on Claim Request Summary Report as NMB Wants Analysis by.
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnIncludeAccessFilterQry = True
            'Pinkal (04-May-2020) -- End


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            mintCurrencyId = 0
            mstrCurrency = ""
            'Pinkal (30-Mar-2021) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mdtFromdate <> Nothing AndAlso mdtToDate <> Nothing Then
                objDataOperation.AddParameter("@fromdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromdate))
                objDataOperation.AddParameter("@todate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) BETWEEN @fromdate AND @todate "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "From Date :") & " " & mdtFromdate.ToShortDateString & " To " & mdtToDate.ToShortDateString & " "
            End If

            If mintExpCateId > 0 Then
                objDataOperation.AddParameter("@expcateid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpCateId)
                Me._FilterQuery &= " AND cmclaim_request_master.expensetypeid = @expcateid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Expense Category :") & " " & mstrExpCateName & " "
            End If

            If mintEmpUnkId > 0 Then
                objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpUnkId)
                Me._FilterQuery &= " AND cmclaim_request_master.employeeunkid = @empunkid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Employee :") & " " & mstrEmpName & " "
            End If

            If mintStatusId > 0 AndAlso (mdtPeriodStartDate = Nothing AndAlso mdtPeriodEndDate = Nothing) Then
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND cmclaim_request_master.statusunkid = @statusid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Status :") & " " & mstrStatusName & " "
            End If


            'Pinkal (18-Apr-2016) -- Start
            'Enhancement - Enhancement for KBC As Per Allan's Comment.

            If mintFromPeriodID > 0 AndAlso mintToPeriodID > 0 Then
                'Pinkal (18-Dec-2020) -- Start 
                'Bug Resolved for KBC in Claim Summary Report.
                'Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "Posted Period From :") & " " & mstrFromPeriod & Language.getMessage(mstrModuleName, 34, "Posted Period To :") & mstrToPeriod & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "Posted Period From :") & " " & mstrFromPeriod & " " & Language.getMessage(mstrModuleName, 34, "Posted Period To :") & mstrToPeriod & " "
                'Pinkal (18-Dec-2020) -- End
            End If

            If mintExpenseID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, "Expense :") & " " & mstrExpense & " "
            End If

            'Pinkal (18-Apr-2016) -- End


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 37, "Currency :") & " " & mstrCurrency & " "
            End If
            'Pinkal (30-Mar-2021) -- End


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, " Order By : ") & " " & Me.OrderByDisplay
                'Pinkal (04-May-2020) -- Start
                'Enhancement NMB Claim Request Summary Report -   Working on Claim Request Summary Report as NMB Wants Analysis by.
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY GName," & Me.OrderByQuery
                Else
                    Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                End If
                'Pinkal (04-May-2020) -- End
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()

        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then

                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.
                'Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
                'Pinkal (06-Jan-2016) -- End


            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("cmclaim_request_master.claimrequestno", Language.getMessage(mstrModuleName, 6, "Claim No")))

            'Pinkal (18-Feb-2016) -- Start
            'Enhancement - CR Changes for ASP as per Rutta's Request.
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 27, "Employee Code")))
            'Pinkal (18-Feb-2016) -- End

            If mblnFirstNamethenSurname = True Then
                iColumn_DetailReport.Add(New IColumn(" ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", Language.getMessage(mstrModuleName, 7, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn(" ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') ", Language.getMessage(mstrModuleName, 7, "Employee Name")))
            End If
            iColumn_DetailReport.Add(New IColumn("cmclaim_request_master.transactiondate", Language.getMessage(mstrModuleName, 8, "Transcation Date")))

            'Pinkal (18-Feb-2016) -- Start
            'Enhancement - CR Changes for ASP as per Rutta's Request.
            'iColumn_DetailReport.Add(New IColumn("cmclaim_request_master.statusunkid", Language.getMessage(mstrModuleName, 9, "Status")))
            'Pinkal (18-Feb-2016) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim StrQuery As String = String.Empty
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty


            StrQ = "SELECT " & _
                   "ISNULL(hremployee_master.employeecode,'') AS Code "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS employee "
            End If

            StrQ &= ",ISNULL(cmclaim_request_master.claimrequestno,'') AS ClaimNO " & _
                        ", cmclaim_request_master.transactiondate AS ClaimDate " & _
                        ", cmclaim_request_master.expensetypeid " & _
                        ",ISNULL(cmclaim_request_master.claim_remark,'') AS Description " & _
                        ",CASE WHEN cmclaim_request_master.expensetypeid = 1 THEN @Leave " & _
                                "WHEN cmclaim_request_master.expensetypeid = 2 THEN @Medical " & _
                                "WHEN cmclaim_request_master.expensetypeid = 3 THEN @Training " & _
                                "WHEN cmclaim_request_master.expensetypeid = 4 THEN @Miscellaneous " & _
                                "WHEN cmclaim_request_master.expensetypeid = 5 THEN @Imprest END AS expensetype " & _
                        ",ISNULL(ReqTable.ReqAmt,0) AS ReqAmt " & _
                        ",ISNULL(ReqTable.ReqQty,0) AS ReqQty " & _
                        ",CASE WHEN cmclaim_request_master.statusunkid = 1 THEN ISNULL(ApprTable.ApprAmt,0) " & _
                                "ELSE 0.00 " & _
                        "END AS ApprAmount " & _
                         ",CASE WHEN cmclaim_request_master.statusunkid = 1 THEN ISNULL(ApprTable.ApprQty,0) " & _
                                "ELSE 0.00 " & _
                        "END AS ApprQty " & _
                        ",ISNULL(cmclaim_request_master.statusunkid,0) AS statusunkid " & _
                        ",CASE WHEN cmclaim_request_master.statusunkid = 1 then @Approve " & _
                                "WHEN cmclaim_request_master.statusunkid = 2 then @Pending " & _
                                "WHEN cmclaim_request_master.statusunkid = 3 then @Reject " & _
                                "WHEN cmclaim_request_master.statusunkid = 6 then @Cancel END as status " & _
                        ",ApprTable.ApprName "

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQ &= ",ApprTable.isposted "
            End If

            StrQ &= ",CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) "


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQ &= " ,ApprTable.countryunkid,ApprTable.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End


            'Pinkal (04-May-2020) -- Start
            'Enhancement NMB Claim Request Summary Report -   Working on Claim Request Summary Report as NMB Wants Analysis by.
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            'Pinkal (04-May-2020) -- End


            StrQ &= "FROM cmclaim_request_master " & _
                    "JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid " & _
                    "JOIN ( " & _
                        "SELECT " & _
                                "crmasterunkid " & _
                        "           ,SUM(cmclaim_request_tran.amount) AS ReqAmt " & _
                        "           ,SUM(cmclaim_request_tran.quantity) AS ReqQty "


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.

            If mintCurrencyId > 0 Then
                StrQ &= " , cmclaim_request_tran.countryunkid,cfexchange_rate.currency_name "
            End If

            StrQ &= "FROM cmclaim_request_tran " & _
                        "           JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid "

            If mintCurrencyId > 0 Then
                StrQ &= "  LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_request_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_request_tran.countryunkid "
            End If

            StrQ &= "           WHERE cmexpense_master.uomunkid = " & mintUOMunkid

            'Pinkal (30-Mar-2021) -- End


            'Pinkal (18-Apr-2016) -- Start
            'Enhancement - Enhancement for KBC As Per Allan's Comment.
            If mintExpenseID > 0 Then
                StrQ &= " AND cmclaim_request_tran.expenseunkid = " & mintExpenseID
            End If
            'Pinkal (18-Apr-2016) -- End


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQ &= " AND cmclaim_request_tran.countryunkid = @countryunkid "
            End If

            StrQ &= "GROUP BY crmasterunkid "

            If mintCurrencyId > 0 Then
                StrQ &= ", cmclaim_request_tran.countryunkid,cfexchange_rate.currency_name "
            End If

            StrQ &= " ) AS ReqTable ON ReqTable.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                         " JOIN( " & _
                         "                      SELECT " & _
                         "                          crmasterunkid " & _
                         "                          ,ApprAmt " & _
                         "                           ,ApprQty " & _
                         "                          ,statusunkid " & _
                         "                          ,A.ApprName " & _
                         "                          ,ROW_NUMBER() OVER (PARTITION BY crmasterunkid ORDER BY visibleid DESC, statusunkid, crpriority DESC) AS Rno "


            If mintCurrencyId > 0 Then
                StrQ &= "              ,countryunkid,currency_name "
            End If

            'Pinkal (30-Mar-2021) -- End 


            'Pinkal (18-Mar-2021) -- NMB Enhancmenet : AD Enhancement for NMB.[visibleid DESC]

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQ &= "           ,isposted "
            End If


            StrQ &= "         FROM " & _
                        "          ( "


            StrQuery = "    SELECT " & _
                                    "cmclaim_approval_tran.crapproverunkid " & _
                                    ",cmclaim_approval_tran.crmasterunkid " & _
                                    ",SUM(cmclaim_approval_tran.amount) AS ApprAmt " & _
                        "               ,SUM(cmclaim_approval_tran.quantity) AS ApprQty " & _
                        "               ,cmclaim_approval_tran.statusunkid " & _
                        "               ,cmclaim_approval_tran.visibleid " & _
                        "               , #APPR_NAME# AS ApprName " & _
                        "               ,cmapproverlevel_master.crpriority "


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= "               ,cmclaim_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End

            'Pinkal (18-Mar-2021) -- NMB Enhancmenet : AD Enhancement for NMB.[  "               ,cmclaim_approval_tran.visibleid " & _]

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= "               , cmprocess.isposted "
            End If

            StrQuery &= "               FROM cmclaim_approval_tran " & _
                               "               LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                               "               LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                               "               LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                        "               JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                        "               #EMPL_JOIN#  "


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_approval_tran.exchangerateunkid	AND cfexchange_rate.countryunkid = cmclaim_approval_tran.countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End


            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= " JOIN (SELECT cmclaim_process_tran.employeeunkid,cmclaim_process_tran.expenseunkid,cmclaim_process_tran.isposted, cmclaim_process_tran.crmasterunkid " & _
                                   "         ,cmclaim_process_tran.crapprovaltranunkid  ,cmclaim_process_tran.crtranunkid " & _
                                   "           FROM cmclaim_process_tran " & _
                                   "           LEFT JOIN cfcommon_period_tran  ON cmclaim_process_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                   "           WHERE cmclaim_process_tran.isvoid = 0 AND "


                'Pinkal (18-Dec-2020) -- Bug Resolved for KBC in Claim Summary Report.


                If mintStatusId <= 0 Then
                    StrQuery &= "  (ISNULL(cmclaim_process_tran.periodunkid,0) <=0 OR Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "') "
                ElseIf mintStatusId = 1 Then
                    StrQuery &= " Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "'"
                ElseIf mintStatusId = 2 Then
                    StrQuery &= " ISNULL(cmclaim_process_tran.periodunkid,0) <=0"
                End If

                StrQuery &= "        ) AS cmprocess ON cmprocess.employeeunkid= cmclaim_request_master.employeeunkid AND cmprocess.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                                   " AND cmprocess.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmprocess.crapprovaltranunkid = cmclaim_approval_tran.crapprovaltranunkid " & _
                                   " AND cmprocess.crtranunkid = cmclaim_approval_tran.crtranunkid "

                'Pinkal (18-Dec-2020) -- Bug Resolved for KBC in Claim Summary Report.

            End If


            StrQCondition = " WHERE cmclaim_approval_tran.isvoid = 0 AND cmclaim_request_master.iscancel = 0 AND cmclaim_approval_tran.iscancel = 0 " & _
                                     "  AND cmexpense_master.uomunkid =   " & mintUOMunkid & _
                                      " AND cmexpapprover_master.isexternalapprover = #isExternal#  "


            'Pinkal (18-Apr-2016) -- Start
            'Enhancement - Enhancement for KBC As Per Allan's Comment.
            If mintExpenseID > 0 Then
                StrQCondition &= " AND cmclaim_approval_tran.expenseunkid = " & mintExpenseID
            End If
            'Pinkal (18-Apr-2016) -- End

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQCondition &= " AND cmclaim_approval_tran.countryunkid = @countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End


            StrQCondition &= "GROUP BY " & _
                                    "cmclaim_approval_tran.crapproverunkid " & _
                                    ",cmclaim_approval_tran.crmasterunkid " & _
                                    ",cmclaim_approval_tran.statusunkid " & _
                                    " ,cmclaim_approval_tran.visibleid " & _
                                      " ,cmapproverlevel_master.crpriority " & _
                                      " ,#APPR_NAME# "

            'Pinkal (18-Mar-2021) -- Start 
            'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug. 
            If mintCurrencyId > 0 Then
                StrQCondition &= " ,cmclaim_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (18-Mar-2021) -- End 

            'Pinkal (18-Mar-2021) -- NMB Enhancmenet : AD Enhancement for NMB.[  "               ,cmclaim_approval_tran.visibleid " & _]

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQCondition &= ", cmprocess.isposted "
            End If


            StrQuery &= StrQCondition

            StrFinalQurey = StrQuery
            StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid")
            StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", IIf(mblnFirstNamethenSurname, "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", " ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') "))
            StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
            StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "0")

            Dim objExpApproverMaster As New clsExpenseApprover_Master
            Dim dsExternalCompany As DataSet = objExpApproverMaster.GetClaimExternalApproverList(Nothing, "List")
            objExpApproverMaster = Nothing
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dr In dsExternalCompany.Tables(0).Rows
                If StrFinalQurey.Trim.Length > 0 Then StrFinalQurey &= " UNION  "
                StrFinalQurey &= StrQuery
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")
                    StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", "")
                Else

                    If mblnFirstNamethenSurname Then
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    Else
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                     "ELSE ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') END ")
                    End If

                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", dr("DName") & "..")

                End If

                If dr("DName").ToString.Trim.Length > 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", dr("DName") & "..")
                Else
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
                End If
                StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")

            Next

            StrQ &= StrFinalQurey

            StrQuery = ""
            StrFinalQurey = ""
            StrQCondition = ""

            StrQ &= " UNION "


            StrQuery = " SELECT " & _
                             "      cmclaim_approval_tran.crapproverunkid " & _
                             "      ,cmclaim_approval_tran.crmasterunkid " & _
                             "      ,SUM(cmclaim_approval_tran.amount) AS ApprAmt " & _
                             "      ,SUM(cmclaim_approval_tran.quantity) AS ApprQty " & _
                             "      ,cmclaim_approval_tran.statusunkid " & _
                             "      ,cmclaim_approval_tran.visibleid " & _
                             "      , #APPR_NAME# AS ApprName " & _
                             "       ,cmapproverlevel_master.crpriority "


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= ",cmclaim_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End


            'Pinkal (18-Mar-2021) -- NMB Enhancmenet : AD Enhancement for NMB.[  "               ,cmclaim_approval_tran.visibleid " & _]

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= ",cmprocess.isposted "
            End If

            StrQuery &= " FROM cmclaim_approval_tran " & _
                               " LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                               " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                               "  LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                            " JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                            " #EMPL_JOIN#  "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_approval_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_approval_tran.countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End


            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= " JOIN (SELECT cmclaim_process_tran.employeeunkid,cmclaim_process_tran.expenseunkid,cmclaim_process_tran.isposted " & _
                                   "                    , cmclaim_process_tran.crmasterunkid ,cmclaim_process_tran.crapprovaltranunkid  ,cmclaim_process_tran.crtranunkid " & _
                                   "           FROM cmclaim_process_tran " & _
                                   "           LEFT JOIN cfcommon_period_tran  ON cmclaim_process_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                   "           WHERE cmclaim_process_tran.isvoid = 0 AND "

                'Pinkal (18-Dec-2020) -- Bug Resolved for KBC in Claim Summary Report.

                If mintStatusId <= 0 Then
                    StrQuery &= " (ISNULL(cmclaim_process_tran.periodunkid,0) <=0 OR Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND 	 Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "') "
                ElseIf mintStatusId = 1 Then
                    StrQuery &= " Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "'"
                ElseIf mintStatusId = 2 Then
                    StrQuery &= " ISNULL(cmclaim_process_tran.periodunkid,0) <=0  "
                End If
                StrQuery &= "        ) AS cmprocess ON cmprocess.employeeunkid= cmclaim_request_master.employeeunkid AND cmprocess.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                                   " AND cmprocess.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmprocess.crapprovaltranunkid = cmclaim_approval_tran.crapprovaltranunkid " & _
                                   " AND cmprocess.crtranunkid = cmclaim_approval_tran.crtranunkid "

                'Pinkal (18-Dec-2020) -- Bug Resolved for KBC in Claim Summary Report.

            End If

            StrQCondition = " WHERE cmclaim_approval_tran.isvoid = 0 AND cmclaim_request_master.iscancel  =1 " & _
                                    "  AND cmexpense_master.uomunkid =   " & mintUOMunkid & _
                                    " AND cmexpapprover_master.isexternalapprover = #isExternal#  "


            'Pinkal (18-Apr-2016) -- Start
            'Enhancement - Enhancement for KBC As Per Allan's Comment.
            If mintExpenseID > 0 Then
                StrQCondition &= " AND cmclaim_approval_tran.expenseunkid = " & mintExpenseID
            End If
            'Pinkal (18-Apr-2016) -- End


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQCondition &= " AND cmclaim_approval_tran.countryunkid =  @countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End



            StrQCondition &= "GROUP BY " & _
                                    "cmclaim_approval_tran.crapproverunkid " & _
                                    ",cmclaim_approval_tran.crmasterunkid " & _
                                    ",cmclaim_approval_tran.statusunkid " & _
                                    ",cmclaim_approval_tran.visibleid " & _
                                    ",cmapproverlevel_master.crpriority " & _
                                    ",#APPR_NAME# "


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQCondition &= ", cmclaim_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End


            'Pinkal (18-Mar-2021) -- NMB Enhancmenet : AD Enhancement for NMB.[  "               ,cmclaim_approval_tran.visibleid " & _]

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQCondition &= ", cmprocess.isposted "
            End If



            StrQuery &= StrQCondition

            StrFinalQurey = StrQuery
            StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid")
            StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", IIf(mblnFirstNamethenSurname, "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", " ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') "))
            StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
            StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "0")


            For Each dr In dsExternalCompany.Tables(0).Rows
                If StrFinalQurey.Trim.Length > 0 Then StrFinalQurey &= " UNION  "
                StrFinalQurey &= StrQuery
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")
                    StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", "")
                Else

                    If mblnFirstNamethenSurname Then
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    Else
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                     "ELSE ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') END ")
                    End If

                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", dr("DName") & "..")

                End If

                If dr("DName").ToString.Trim.Length > 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", dr("DName") & "..")
                Else
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
                End If
                StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")

            Next

            StrQ &= StrFinalQurey



            StrQ &= ")AS A  " & _
                        ") AS ApprTable ON ApprTable.crmasterunkid = cmclaim_request_master.crmasterunkid AND ApprTable.Rno <=1 "

            'Pinkal (01-Mar-2016) -- End


            'Pinkal (04-May-2020) -- Start
            'Enhancement NMB Claim Request Summary Report -   Working on Claim Request Summary Report as NMB Wants Analysis by.
            StrQ &= mstrAnalysis_Join
            'Pinkal (04-May-2020) -- End


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE     cmclaim_request_master.isvoid = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery


            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
            'Pinkal (10-Feb-2021) -- End


            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("Code")
                rpt_Rows.Item("Column2") = dtRow.Item("employee")
                rpt_Rows.Item("Column3") = dtRow.Item("ClaimNO")
                rpt_Rows.Item("Column4") = dtRow.Item("Description")

                If mintUOMunkid = enExpUoM.UOM_AMOUNT Then
                    rpt_Rows.Item("Column5") = Format(dtRow.Item("ReqAmt"), GUI.fmtCurrency)
                    rpt_Rows.Item("Column6") = Format(dtRow.Item("ApprAmount"), GUI.fmtCurrency)


                    'Pinkal (04-May-2020) -- Start
                    'Enhancement NMB Claim Request Summary Report -   Working on Claim Request Summary Report as NMB Wants Analysis by.
                    If mintViewIndex > 0 Then
                        rpt_Rows.Item("Column10") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqAmt)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(ReqAmt)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                        rpt_Rows.Item("Column11") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ApprAmount)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(ApprAmount)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                        rpt_Rows.Item("Column15") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqAmt)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(ReqAmt)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                        rpt_Rows.Item("Column16") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ApprAmount)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(ApprAmount)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    Else
                        rpt_Rows.Item("Column10") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqAmt)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), 0, dsList.Tables(0).Compute("SUM(ReqAmt)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), GUI.fmtCurrency)
                        rpt_Rows.Item("Column11") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ApprAmount)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), 0, dsList.Tables(0).Compute("SUM(ApprAmount)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), GUI.fmtCurrency)
                    End If
                    rpt_Rows.Item("Column12") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqAmt)", "1=1")), 0, dsList.Tables(0).Compute("SUM(ReqAmt)", "1=1")), GUI.fmtCurrency)
                    rpt_Rows.Item("Column13") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ApprAmount)", "1=1")), 0, dsList.Tables(0).Compute("SUM(ApprAmount)", "1=1")), GUI.fmtCurrency)
                    'Pinkal (04-May-2020) -- End


                ElseIf mintUOMunkid = enExpUoM.UOM_QTY Then
                    rpt_Rows.Item("Column5") = CDec(dtRow.Item("ReqQty")).ToString("#0.00")
                    rpt_Rows.Item("Column6") = CDec(dtRow.Item("ApprQty")).ToString("#0.00")
                    rpt_Rows.Item("Column10") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqQty)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), 0, dsList.Tables(0).Compute("SUM(ReqQty)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column11") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ApprQty)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), 0, dsList.Tables(0).Compute("SUM(ApprQty)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column12") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqQty)", "1=1")), 0, dsList.Tables(0).Compute("SUM(ReqQty)", "1=1")), GUI.fmtCurrency)
                    rpt_Rows.Item("Column13") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ApprQty)", "1=1")), 0, dsList.Tables(0).Compute("SUM(ApprQty)", "1=1")), GUI.fmtCurrency)
                End If

                If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                    rpt_Rows.Item("Column7") = IIf(CBool(dtRow.Item("isposted")), Language.getMessage(mstrModuleName, 31, "Posted"), Language.getMessage(mstrModuleName, 32, "Not Posted"))
                Else
                    rpt_Rows.Item("Column7") = dtRow.Item("status") & " BY :- " & dtRow.Item("ApprName")
                End If

                rpt_Rows.Item("Column8") = CDate(dtRow.Item("ClaimDate")).ToShortDateString()
                rpt_Rows.Item("Column9") = dtRow.Item("expensetype").ToString()


                'Pinkal (04-May-2020) -- Start
                'Enhancement NMB Claim Request Summary Report -   Working on Claim Request Summary Report as NMB Wants Analysis by.
                If mintViewIndex > 0 Then
                    rpt_Rows.Item("Column14") = dtRow.Item("GName").ToString()
                End If
                'Pinkal (04-May-2020) -- End


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptClaimRequestSummary

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            'Pinkal (04-May-2020) -- Start
            'Enhancement NMB Claim Request Summary Report -   Working on Claim Request Summary Report as NMB Wants Analysis by.
            If mintViewIndex <= 0 Then
                ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            Else
                Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
                Call ReportFunction.TextChange(objRpt, "txtAllocationGroupTotal", mstrReport_GroupName.Trim.Replace(":", "") & Language.getMessage(mstrModuleName, 36, " Total :"))
            End If
            'Pinkal (04-May-2020) -- End


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 10, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 11, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 12, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 13, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 14, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 15, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 16, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 17, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtClaimNo", Language.getMessage(mstrModuleName, 18, "Claim No."))

            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.

            'Call ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 19, "Description"))
            Call ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 28, "Claim Remark"))

            If mintUOMunkid = enExpUoM.UOM_QTY Then
                Call ReportFunction.TextChange(objRpt, "txtReqAmt", Language.getMessage(mstrModuleName, 29, "Req. Qty"))
                Call ReportFunction.TextChange(objRpt, "txtApprAmt", Language.getMessage(mstrModuleName, 30, "Appr. Qty"))
            ElseIf mintUOMunkid = enExpUoM.UOM_AMOUNT Then
                Call ReportFunction.TextChange(objRpt, "txtReqAmt", Language.getMessage(mstrModuleName, 20, "Req. Amount"))
                Call ReportFunction.TextChange(objRpt, "txtApprAmt", Language.getMessage(mstrModuleName, 21, "Appr. Amount"))
            End If

            'Pinkal (22-Mar-2016) -- End



            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 22, "Status"))

            'Pinkal (18-Feb-2016) -- Start
            'Enhancement - CR Changes for ASP as per Rutta's Request.
            Call ReportFunction.TextChange(objRpt, "txtCategory", Language.getMessage(mstrModuleName, 23, "Expense Category : "))
            Call ReportFunction.TextChange(objRpt, "txtClaimDate", Language.getMessage(mstrModuleName, 24, "Claim Date"))
            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 25, "Group Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 26, "Grand Total :"))
            Call ReportFunction.EnableSuppress(objRpt, "txtStatus", Not mblnShowClaimFormStatus)
            Call ReportFunction.EnableSuppress(objRpt, "Column71", Not mblnShowClaimFormStatus)
            'Pinkal (18-Feb-2016) -- End

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From Date :")
            Language.setMessage(mstrModuleName, 2, "Expense Category :")
            Language.setMessage(mstrModuleName, 3, "Employee :")
            Language.setMessage(mstrModuleName, 4, "Status :")
            Language.setMessage(mstrModuleName, 5, " Order By :")
            Language.setMessage(mstrModuleName, 6, "Claim No")
            Language.setMessage(mstrModuleName, 7, "Employee Name")
            Language.setMessage(mstrModuleName, 8, "Transcation Date")
            Language.setMessage("clsExpCommonMethods", 9, "Imprest")
            Language.setMessage(mstrModuleName, 10, "Prepared By :")
            Language.setMessage(mstrModuleName, 11, "Checked By :")
            Language.setMessage(mstrModuleName, 12, "Approved By :")
            Language.setMessage(mstrModuleName, 13, "Received By :")
            Language.setMessage(mstrModuleName, 14, "Printed By :")
            Language.setMessage(mstrModuleName, 15, "Printed Date :")
            Language.setMessage(mstrModuleName, 16, "Code")
            Language.setMessage(mstrModuleName, 17, "Employee")
            Language.setMessage(mstrModuleName, 18, "Claim No.")
            Language.setMessage(mstrModuleName, 20, "Req. Amount")
            Language.setMessage(mstrModuleName, 21, "Appr. Amount")
            Language.setMessage(mstrModuleName, 22, "Status")
            Language.setMessage(mstrModuleName, 23, "Expense Category :")
            Language.setMessage(mstrModuleName, 24, "Claim Date")
            Language.setMessage(mstrModuleName, 25, "Group Total :")
            Language.setMessage(mstrModuleName, 26, "Grand Total :")
            Language.setMessage(mstrModuleName, 27, "Employee Code")
            Language.setMessage(mstrModuleName, 28, "Claim Remark")
            Language.setMessage(mstrModuleName, 29, "Req. Qty")
            Language.setMessage(mstrModuleName, 30, "Appr. Qty")
            Language.setMessage(mstrModuleName, 31, "Posted")
            Language.setMessage(mstrModuleName, 32, "Not Posted")
            Language.setMessage(mstrModuleName, 33, "Posted Period From :")
            Language.setMessage(mstrModuleName, 34, "Posted Period To :")
            Language.setMessage(mstrModuleName, 35, "Expense :")
            Language.setMessage(mstrModuleName, 36, " Total :")
            Language.setMessage(mstrModuleName, 37, "Currency :")

            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 115, "Cancelled")
            Language.setMessage("clsExpCommonMethods", 2, "Leave")
            Language.setMessage("clsExpCommonMethods", 3, "Medical")
            Language.setMessage("clsExpCommonMethods", 4, "Training")
            Language.setMessage("clsExpCommonMethods", 8, "Miscellaneous")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
