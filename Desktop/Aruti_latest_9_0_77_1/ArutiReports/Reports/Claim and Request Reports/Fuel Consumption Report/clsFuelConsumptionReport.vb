'************************************************************************************************************************************
'Class Name :clsFuelConsumptionReport.vb
'Purpose    :
'Date       : 20-Nov-2020
'Written By : Pinkal Jariwala.
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsFuelConsumptionReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsFuelConsumptionReport"
    Private mstrReportId As String = enArutiReport.FuelConsumption_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "
    Private mdtFromdate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintExpCateId As Integer = 0
    Private mstrExpCateName As String = ""
    Private mintEmpUnkId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = ""
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUOMunkid As Integer
    Private mstrUOM As String
    Private mintExpenseID As Integer = -1
    Private mstrExpense As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True
    Private mblnShowRequiredQuantity As Boolean = True
#End Region

#Region " Properties "

    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromdate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _ExpCateId() As Integer
        Set(ByVal value As Integer)
            mintExpCateId = value
        End Set
    End Property

    Public WriteOnly Property _ExpCateName() As String
        Set(ByVal value As String)
            mstrExpCateName = value
        End Set
    End Property

    Public WriteOnly Property _EmpUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmpUnkId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkid() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UOMId() As Integer
        Set(ByVal value As Integer)
            mintUOMunkid = value
        End Set
    End Property

    Public WriteOnly Property _UOM() As String
        Set(ByVal value As String)
            mstrUOM = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseID() As Integer
        Set(ByVal value As Integer)
            mintExpenseID = value
        End Set
    End Property

    Public WriteOnly Property _Expense() As String
        Set(ByVal value As String)
            mstrExpense = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    Public Property _ShowRequiredQuanitty() As Boolean
        Get
            Return mblnShowRequiredQuantity
        End Get
        Set(ByVal value As Boolean)
            mblnShowRequiredQuantity = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtFromdate = Nothing
            mdtToDate = Nothing
            mintExpCateId = 0
            mstrExpCateName = ""
            mintEmpUnkId = 0
            mstrEmpName = ""
            mintStatusId = 0
            mstrStatusName = ""
            mintUserUnkid = 0
            mintCompanyUnkid = 0
            mintUOMunkid = 0
            mstrUOM = ""
            mintExpenseID = 0
            mstrExpense = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnIncludeAccessFilterQry = True
            mblnShowRequiredQuantity = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mdtFromdate <> Nothing AndAlso mdtToDate <> Nothing Then
                objDataOperation.AddParameter("@fromdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromdate))
                objDataOperation.AddParameter("@todate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) BETWEEN @fromdate AND @todate "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "From Date :") & " " & mdtFromdate.ToShortDateString & " To " & mdtToDate.ToShortDateString & " "
            End If

            If mintExpCateId > 0 Then
                objDataOperation.AddParameter("@expcateid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpCateId)
                Me._FilterQuery &= " AND cmclaim_request_master.expensetypeid = @expcateid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Expense Category :") & " " & mstrExpCateName & " "
            End If

            If mintEmpUnkId > 0 Then
                objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpUnkId)
                Me._FilterQuery &= " AND cmclaim_request_master.employeeunkid = @empunkid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Employee :") & " " & mstrEmpName & " "
            End If

            If mintExpenseID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Expense :") & " " & mstrExpense
                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseID)
            End If

            If mintUOMunkid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "UOM :") & " " & mstrUOM
                objDataOperation.AddParameter("@uomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUOMunkid)
            End If

            If mintStatusId > 0 Then
                Me._FilterQuery &= " AND cmclaim_request_master.statusunkid = @statusunkid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, "Status :") & " " & mstrStatusName
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
            End If


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, " Order By : ") & " " & Me.OrderByDisplay
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY GName," & Me.OrderByQuery
                Else
                    Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
       
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 7, "Employee Code")))
            If mblnFirstNamethenSurname = True Then
                iColumn_DetailReport.Add(New IColumn(" ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", Language.getMessage(mstrModuleName, 8, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn(" ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') ", Language.getMessage(mstrModuleName, 8, "Employee Name")))
            End If
            iColumn_DetailReport.Add(New IColumn("cmclaim_request_master.claimrequestno", Language.getMessage(mstrModuleName, 9, "Form No")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim StrQuery As String = String.Empty
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty


            StrQ = "SELECT " & _
                      "  cmclaim_request_master.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode,'') AS Code "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS Employee "
            End If

            StrQ &= ",ISNULL(cmclaim_request_master.claimrequestno,'') AS ClaimNo " & _
                        ", cmclaim_request_master.transactiondate AS ClaimDate " & _
                        ", cmclaim_request_master.expensetypeid " & _
                        ", ISNULL(Req.carRegNo, '') AS carRegNo" & _
                        ", ISNULL(cmclaim_request_master.claim_remark, '') AS Purpose " & _
                        ", ISNULL(Req.ReqAmt,0.00) AS ReqQty " & _
                        ", CASE WHEN cmclaim_request_master.statusunkid = 1 THEN ISNULL(App.ApprovedAmt,0.00) ELSE 0.00 END AS AppQty " & _
                        ", ISNULL(cmclaim_request_master.statusunkid,0) AS statusunkid " & _
                        ", CASE WHEN cmclaim_request_master.statusunkid = 1 then @Approve " & _
                        "           WHEN cmclaim_request_master.statusunkid = 2 then @Pending " & _
                        "           WHEN cmclaim_request_master.statusunkid = 3 then @Reject " & _
                        "           WHEN cmclaim_request_master.statusunkid = 6 then @Cancel " & _
                        " END as status "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM cmclaim_request_master " & _
                        " JOIN ( " & _
                        "           SELECT " & _
                        "                    crmasterunkid " & _
                        "                   ,SUM(cmclaim_request_tran.amount) AS ReqAmt " & _
                        "                   ,expense_remark AS carRegNo " & _
                        "           FROM cmclaim_request_tran " & _
                        "           JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid " & _
                        "           WHERE 1 = 1 "

            If mintUOMunkid > 0 Then
                StrQ &= " AND cmexpense_master.uomunkid = @uomunkid "
            End If

            If mintExpenseID > 0 Then
                StrQ &= " AND cmclaim_request_tran.expenseunkid = @expenseunkid "
            End If

            StrQ &= " GROUP BY crmasterunkid,expense_remark " & _
                         " ) AS Req ON Req.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                         " LEFT JOIN  " & _
                         "              (   SELECT  " & _
                         "                       crmasterunkid " & _
                         "                      ,ISNULL(SUM(amount), 0.00) AS ApprovedAmt " & _
                         "                  FROM cmclaim_approval_tran " & _
                         "                  LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmclaim_approval_tran.expenseunkid  " & _
                         "                  WHERE isvoid = 0	AND statusunkid = 1 "

            If mintUOMunkid > 0 Then
                StrQ &= " AND cmexpense_master.uomunkid = @uomunkid "
            End If

            If mintExpenseID > 0 Then
                StrQ &= " AND cmclaim_approval_tran.expenseunkid = @expenseunkid "
            End If

            StrQ &= "  AND crapproverunkid IN " & _
                         "                   (      SELECT " & _
                         "                                  crapproverunkid " & _
                         "                          FROM  " & _
                         "                                  (   SELECT " & _
                         "                              				ISNULL(cm.crapproverunkid, 0) AS crapproverunkid " & _
                         "                                             ,cm.crmasterunkid " & _
                         "                                             ,DENSE_RANK() OVER (PARTITION BY cm.crmasterunkid ORDER BY crpriority DESC) AS rno " & _
                         "                                      FROM cmclaim_approval_tran cm " & _
                         "                                      JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cm.crapproverunkid " & _
                         "                                      JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                         "                              		WHERE cm.isvoid = 0 " & _
                         "                                  ) AS cnt " & _
                         "                      	WHERE cnt.rno = 1 AND cnt.crmasterunkid = cmclaim_approval_tran.crmasterunkid " & _
                         "                  )	GROUP BY crmasterunkid " & _
                         "          ) AS App ON App.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                         " LEFT JOIN hremployee_master	ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid  "
                 

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE  cmclaim_request_master.isvoid = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery


            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("Code").ToString()
                rpt_Rows.Item("Column2") = dtRow.Item("Employee").ToString()
                rpt_Rows.Item("Column3") = dtRow.Item("ClaimNo").ToString()
                rpt_Rows.Item("Column4") = CDate(dtRow.Item("ClaimDate")).ToShortDateString()
                rpt_Rows.Item("Column5") = dtRow.Item("carRegNo").ToString()
                rpt_Rows.Item("Column6") = dtRow.Item("Purpose").ToString()
                rpt_Rows.Item("Column7") = CDec(dtRow.Item("ReqQty")).ToString("#0.00")
                rpt_Rows.Item("Column8") = CDec(dtRow.Item("AppQty")).ToString("#0.00")
                rpt_Rows.Item("Column9") = dtRow.Item("status").ToString()
                If mintViewIndex > 0 Then
                    rpt_Rows.Item("Column10") = dtRow.Item("GName").ToString()
                End If

                If mintViewIndex > 0 Then
                    rpt_Rows.Item("Column11") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqQty)", "employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(ReqQty)", "employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column12") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(AppQty)", "employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(AppQty)", "employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column13") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqQty)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(ReqQty)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column14") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(AppQty)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(AppQty)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                Else
                    rpt_Rows.Item("Column11") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqQty)", "employeeunkid = " & CInt(dtRow.Item("employeeunkid")))), 0, dsList.Tables(0).Compute("SUM(ReqQty)", "employeeunkid = " & CInt(dtRow.Item("employeeunkid")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column12") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(AppQty)", "employeeunkid = " & CInt(dtRow.Item("employeeunkid")))), 0, dsList.Tables(0).Compute("SUM(AppQty)", "employeeunkid = " & CInt(dtRow.Item("employeeunkid")))), GUI.fmtCurrency)
                End If

                rpt_Rows.Item("Column15") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqQty)", "1=1")), 0, dsList.Tables(0).Compute("SUM(ReqQty)", "1=1")), GUI.fmtCurrency)
                rpt_Rows.Item("Column16") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(AppQty)", "1=1")), 0, dsList.Tables(0).Compute("SUM(AppQty)", "1=1")), GUI.fmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptFuelConsumption

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex <= 0 Then
                ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            Else
                Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
                Call ReportFunction.TextChange(objRpt, "txtAllocationGroupTotal", mstrReport_GroupName.Trim.Replace(":", "") & " " & Language.getMessage(mstrModuleName, 10, "Total :"))
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 11, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 12, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 13, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 14, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If


            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 7, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 8, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtClaimNo", Language.getMessage(mstrModuleName, 9, "Form No"))
            Call ReportFunction.TextChange(objRpt, "txtClaimDate", Language.getMessage(mstrModuleName, 15, "Claim Date"))
            Call ReportFunction.TextChange(objRpt, "txtCarRegNo", Language.getMessage(mstrModuleName, 16, "Car Reg No."))
            Call ReportFunction.TextChange(objRpt, "txtPurpose", Language.getMessage(mstrModuleName, 17, "Purpose"))
            Call ReportFunction.TextChange(objRpt, "txtReqAmt", Language.getMessage(mstrModuleName, 18, "Req. Qty"))
            Call ReportFunction.TextChange(objRpt, "txtApprAmt", Language.getMessage(mstrModuleName, 19, "Appr.Qty"))
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 20, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 21, "Group Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 22, "Grand Total :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 23, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 24, "Printed Date :"))

            Call ReportFunction.EnableSuppress(objRpt, "txtReqAmt", Not mblnShowRequiredQuantity)
            Call ReportFunction.EnableSuppress(objRpt, "Column71", Not mblnShowRequiredQuantity)
            Call ReportFunction.EnableSuppress(objRpt, "Column111", Not mblnShowRequiredQuantity)
            Call ReportFunction.EnableSuppress(objRpt, "Column131", Not mblnShowRequiredQuantity)
            Call ReportFunction.EnableSuppress(objRpt, "Column151", Not mblnShowRequiredQuantity)


            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "From Date :")
			Language.setMessage(mstrModuleName, 2, "Expense Category :")
			Language.setMessage(mstrModuleName, 3, "Employee :")
			Language.setMessage(mstrModuleName, 4, "Expense :")
			Language.setMessage(mstrModuleName, 5, "UOM :")
			Language.setMessage(mstrModuleName, 6, " Order By :")
			Language.setMessage(mstrModuleName, 7, "Employee Code")
			Language.setMessage(mstrModuleName, 8, "Employee Name")
			Language.setMessage(mstrModuleName, 9, "Form No")
			Language.setMessage(mstrModuleName, 10, "Total :")
			Language.setMessage(mstrModuleName, 11, "Prepared By :")
			Language.setMessage(mstrModuleName, 12, "Checked By :")
			Language.setMessage(mstrModuleName, 13, "Approved By :")
			Language.setMessage(mstrModuleName, 14, "Received By :")
			Language.setMessage(mstrModuleName, 15, "Claim Date")
			Language.setMessage(mstrModuleName, 16, "Car Reg No.")
			Language.setMessage(mstrModuleName, 17, "Purpose")
			Language.setMessage(mstrModuleName, 18, "Req. Qty")
			Language.setMessage(mstrModuleName, 19, "Appr.Qty")
			Language.setMessage(mstrModuleName, 20, "Status")
			Language.setMessage(mstrModuleName, 21, "Group Total :")
			Language.setMessage(mstrModuleName, 22, "Grand Total :")
			Language.setMessage(mstrModuleName, 23, "Printed By :")
			Language.setMessage(mstrModuleName, 24, "Printed Date :")
			Language.setMessage("clsMasterData", 110, "Approved")
			Language.setMessage("clsMasterData", 111, "Pending")
			Language.setMessage("clsMasterData", 112, "Rejected")
			Language.setMessage("clsMasterData", 115, "Cancelled")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
