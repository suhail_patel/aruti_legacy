﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApprovedCRSummaryReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.LblCurrency = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchExpense = New eZee.Common.eZeeGradientButton
        Me.LblExpense = New System.Windows.Forms.Label
        Me.cboExpense = New System.Windows.Forms.ComboBox
        Me.cboUOM = New System.Windows.Forms.ComboBox
        Me.LblUOM = New System.Windows.Forms.Label
        Me.dtpApprovedToDate = New System.Windows.Forms.DateTimePicker
        Me.lblApprovedToDate = New System.Windows.Forms.Label
        Me.objbtnSearchCategory = New eZee.Common.eZeeGradientButton
        Me.LblExpenseCategory = New System.Windows.Forms.Label
        Me.cboExpenseCategory = New System.Windows.Forms.ComboBox
        Me.dtpApprovedFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblApprovedFromDate = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 437)
        Me.NavPanel.Size = New System.Drawing.Size(573, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.LblCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.cboCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchExpense)
        Me.gbFilterCriteria.Controls.Add(Me.LblExpense)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpense)
        Me.gbFilterCriteria.Controls.Add(Me.cboUOM)
        Me.gbFilterCriteria.Controls.Add(Me.LblUOM)
        Me.gbFilterCriteria.Controls.Add(Me.dtpApprovedToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprovedToDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchCategory)
        Me.gbFilterCriteria.Controls.Add(Me.LblExpenseCategory)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpenseCategory)
        Me.gbFilterCriteria.Controls.Add(Me.dtpApprovedFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprovedFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(418, 197)
        Me.gbFilterCriteria.TabIndex = 18
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCurrency
        '
        Me.LblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrency.Location = New System.Drawing.Point(9, 170)
        Me.LblCurrency.Name = "LblCurrency"
        Me.LblCurrency.Size = New System.Drawing.Size(110, 15)
        Me.LblCurrency.TabIndex = 104
        Me.LblCurrency.Text = "Currency"
        Me.LblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(132, 167)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(254, 21)
        Me.cboCurrency.TabIndex = 105
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(320, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 22
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchExpense
        '
        Me.objbtnSearchExpense.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchExpense.BorderSelected = False
        Me.objbtnSearchExpense.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchExpense.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchExpense.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchExpense.Location = New System.Drawing.Point(394, 89)
        Me.objbtnSearchExpense.Name = "objbtnSearchExpense"
        Me.objbtnSearchExpense.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchExpense.TabIndex = 100
        '
        'LblExpense
        '
        Me.LblExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExpense.Location = New System.Drawing.Point(9, 92)
        Me.LblExpense.Name = "LblExpense"
        Me.LblExpense.Size = New System.Drawing.Size(110, 15)
        Me.LblExpense.TabIndex = 98
        Me.LblExpense.Text = "Expense"
        Me.LblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpense
        '
        Me.cboExpense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpense.FormattingEnabled = True
        Me.cboExpense.Location = New System.Drawing.Point(132, 89)
        Me.cboExpense.Name = "cboExpense"
        Me.cboExpense.Size = New System.Drawing.Size(254, 21)
        Me.cboExpense.TabIndex = 99
        '
        'cboUOM
        '
        Me.cboUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUOM.FormattingEnabled = True
        Me.cboUOM.Location = New System.Drawing.Point(132, 141)
        Me.cboUOM.Name = "cboUOM"
        Me.cboUOM.Size = New System.Drawing.Size(254, 21)
        Me.cboUOM.TabIndex = 97
        '
        'LblUOM
        '
        Me.LblUOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUOM.Location = New System.Drawing.Point(9, 144)
        Me.LblUOM.Name = "LblUOM"
        Me.LblUOM.Size = New System.Drawing.Size(110, 15)
        Me.LblUOM.TabIndex = 95
        Me.LblUOM.Text = "UOM"
        Me.LblUOM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpApprovedToDate
        '
        Me.dtpApprovedToDate.Checked = False
        Me.dtpApprovedToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApprovedToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApprovedToDate.Location = New System.Drawing.Point(283, 63)
        Me.dtpApprovedToDate.Name = "dtpApprovedToDate"
        Me.dtpApprovedToDate.ShowCheckBox = True
        Me.dtpApprovedToDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpApprovedToDate.TabIndex = 91
        '
        'lblApprovedToDate
        '
        Me.lblApprovedToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovedToDate.Location = New System.Drawing.Point(244, 66)
        Me.lblApprovedToDate.Name = "lblApprovedToDate"
        Me.lblApprovedToDate.Size = New System.Drawing.Size(34, 15)
        Me.lblApprovedToDate.TabIndex = 90
        Me.lblApprovedToDate.Text = "To"
        Me.lblApprovedToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCategory
        '
        Me.objbtnSearchCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCategory.BorderSelected = False
        Me.objbtnSearchCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCategory.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCategory.Location = New System.Drawing.Point(393, 35)
        Me.objbtnSearchCategory.Name = "objbtnSearchCategory"
        Me.objbtnSearchCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCategory.TabIndex = 88
        '
        'LblExpenseCategory
        '
        Me.LblExpenseCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExpenseCategory.Location = New System.Drawing.Point(9, 38)
        Me.LblExpenseCategory.Name = "LblExpenseCategory"
        Me.LblExpenseCategory.Size = New System.Drawing.Size(110, 15)
        Me.LblExpenseCategory.TabIndex = 86
        Me.LblExpenseCategory.Text = "Exp. Category"
        Me.LblExpenseCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpenseCategory
        '
        Me.cboExpenseCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpenseCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpenseCategory.FormattingEnabled = True
        Me.cboExpenseCategory.Location = New System.Drawing.Point(132, 35)
        Me.cboExpenseCategory.Name = "cboExpenseCategory"
        Me.cboExpenseCategory.Size = New System.Drawing.Size(254, 21)
        Me.cboExpenseCategory.TabIndex = 87
        '
        'dtpApprovedFromDate
        '
        Me.dtpApprovedFromDate.Checked = False
        Me.dtpApprovedFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApprovedFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApprovedFromDate.Location = New System.Drawing.Point(132, 63)
        Me.dtpApprovedFromDate.Name = "dtpApprovedFromDate"
        Me.dtpApprovedFromDate.ShowCheckBox = True
        Me.dtpApprovedFromDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpApprovedFromDate.TabIndex = 81
        '
        'lblApprovedFromDate
        '
        Me.lblApprovedFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovedFromDate.Location = New System.Drawing.Point(9, 66)
        Me.lblApprovedFromDate.Name = "lblApprovedFromDate"
        Me.lblApprovedFromDate.Size = New System.Drawing.Size(110, 15)
        Me.lblApprovedFromDate.TabIndex = 79
        Me.lblApprovedFromDate.Text = "Approved From Date"
        Me.lblApprovedFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(394, 115)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 59
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(9, 118)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(110, 15)
        Me.lblEmployee.TabIndex = 57
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(132, 115)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(254, 21)
        Me.cboEmployee.TabIndex = 58
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 268)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(419, 63)
        Me.gbSortBy.TabIndex = 19
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(355, 31)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 35)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(88, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(105, 31)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(244, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'frmApprovedCRSummaryReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(573, 492)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApprovedCRSummaryReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Claim Request Summary Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblApprovedFromDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents dtpApprovedFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblExpenseCategory As System.Windows.Forms.Label
    Friend WithEvents cboExpenseCategory As System.Windows.Forms.ComboBox
    Friend WithEvents dtpApprovedToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblApprovedToDate As System.Windows.Forms.Label
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCategory As eZee.Common.eZeeGradientButton
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents LblUOM As System.Windows.Forms.Label
    Friend WithEvents cboUOM As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchExpense As eZee.Common.eZeeGradientButton
    Friend WithEvents LblExpense As System.Windows.Forms.Label
    Friend WithEvents cboExpense As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents LblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
End Class
