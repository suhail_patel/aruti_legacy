'************************************************************************************************************************************
'Class Name :clsAirPassageReport.vb
'Purpose    :
'Date       : 08-Aug-2014
'Written By : Shani Sheladiya.
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsApprovedCRSummaryReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsApprovedCRSummaryReport"
    Private mstrReportId As String = enArutiReport.Approved_CR_Summary_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "
    Private mdtApprovedFromdate As DateTime = Nothing
    Private mdtApprovedToDate As DateTime = Nothing
    Private mintExpCateId As Integer = 0
    Private mstrExpCateName As String = ""
    Private mintEmpUnkId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUOMunkid As Integer
    Private mstrUOM As String
    Private mintExpenseID As Integer = -1
    Private mstrExpense As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True
    Private mintCurrencyId As Integer = 0
    Private mstrCurrency As String = ""

#End Region

#Region " Properties "

    Public WriteOnly Property _ApprovedFromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtApprovedFromdate = value
        End Set
    End Property

    Public WriteOnly Property _ApprovedToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtApprovedToDate = value
        End Set
    End Property

    Public WriteOnly Property _ExpCateId() As Integer
        Set(ByVal value As Integer)
            mintExpCateId = value
        End Set
    End Property

    Public WriteOnly Property _ExpCateName() As String
        Set(ByVal value As String)
            mstrExpCateName = value
        End Set
    End Property

    Public WriteOnly Property _EmpUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmpUnkId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkid() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UOMId() As Integer
        Set(ByVal value As Integer)
            mintUOMunkid = value
        End Set
    End Property

    Public WriteOnly Property _UOM() As String
        Set(ByVal value As String)
            mstrUOM = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseID() As Integer
        Set(ByVal value As Integer)
            mintExpenseID = value
        End Set
    End Property

    Public WriteOnly Property _Expense() As String
        Set(ByVal value As String)
            mstrExpense = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    
    Public Property _CurrencyId() As Integer
        Get
            Return mintCurrencyId
        End Get
        Set(ByVal value As Integer)
            mintCurrencyId = value
        End Set
    End Property

    Public Property _Currency() As String
        Get
            Return mstrCurrency
        End Get
        Set(ByVal value As String)
            mstrCurrency = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtApprovedFromdate = Nothing
            mdtApprovedToDate = Nothing
            mintExpCateId = 0
            mstrExpCateName = ""
            mintEmpUnkId = 0
            mstrEmpName = ""
            mintUserUnkid = 0
            mintCompanyUnkid = 0
            mintUOMunkid = 0
            mstrUOM = ""
            mintExpenseID = 0
            mstrExpense = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnIncludeAccessFilterQry = True
            mintCurrencyId = 0
            mstrCurrency = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mdtApprovedFromdate <> Nothing AndAlso mdtApprovedToDate <> Nothing Then
                objDataOperation.AddParameter("@fromdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtApprovedFromdate))
                objDataOperation.AddParameter("@todate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtApprovedToDate))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),ApprTable.approvaldate,112) BETWEEN @fromdate AND @todate "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Approved From Date :") & " " & mdtApprovedFromdate.ToShortDateString & " To " & mdtApprovedToDate.ToShortDateString & " "
            End If

            If mintExpCateId > 0 Then
                objDataOperation.AddParameter("@expcateid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpCateId)
                Me._FilterQuery &= " AND cmclaim_request_master.expensetypeid = @expcateid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Expense Category :") & " " & mstrExpCateName & " "
            End If

            If mintEmpUnkId > 0 Then
                objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpUnkId)
                Me._FilterQuery &= " AND cmclaim_request_master.employeeunkid = @empunkid"
                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Employee :") & " " & info1.ToTitleCase(mstrEmpName.ToLower) & " "
                info1 = Nothing
            End If

            If mintExpenseID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Expense :") & " " & mstrExpense & " "
            End If

            If mintCurrencyId > 0 Then
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Currency :") & " " & mstrCurrency & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, " Order By : ") & " " & Me.OrderByDisplay
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY GName," & Me.OrderByQuery
                Else
                    Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
     
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("cmclaim_request_master.claimrequestno", Language.getMessage(mstrModuleName, 7, "Claim No.")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 8, "Code")))
            If mblnFirstNamethenSurname = True Then
                iColumn_DetailReport.Add(New IColumn(" ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", Language.getMessage(mstrModuleName, 9, "Employee")))
            Else
                iColumn_DetailReport.Add(New IColumn(" ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') ", Language.getMessage(mstrModuleName, 9, "Employee")))
            End If
            iColumn_DetailReport.Add(New IColumn("ApprTable.approvaldate", Language.getMessage(mstrModuleName, 10, "Approved From Date")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim StrQuery As String = String.Empty
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty


            StrQ = "SELECT " & _
                       " ISNULL(hremployee_master.employeecode,'') AS Code "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS employee "
            End If

            StrQ &= ",ISNULL(cmclaim_request_master.claimrequestno,'') AS ClaimNo " & _
                        ",CASE WHEN cmclaim_request_master.expensetypeid = 1 THEN @Leave " & _
                        "          WHEN cmclaim_request_master.expensetypeid = 2 THEN @Medical " & _
                        "          WHEN cmclaim_request_master.expensetypeid = 3 THEN @Training " & _
                        "          WHEN cmclaim_request_master.expensetypeid = 4 THEN @Miscellaneous " & _
                        "          WHEN cmclaim_request_master.expensetypeid = 5 THEN @Imprest END AS expensetype " & _
                        ",ISNULL(ReqTable.ReqAmt,0.00) AS ReqAmt " & _
                        ",ISNULL(ApprTable.ApprAmt,0.00) AS ApprAmount " & _
                        ",ApprTable.approvaldate " & _
                        ",ApprTable.ApprName " & _
                        ",ISNULL(cmclaim_request_master.claim_remark,'') AS Description " & _
                        ",cmclaim_request_master.transactiondate AS ClaimDate " & _
                        ",cmclaim_request_master.expensetypeid " & _
                        ",CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) AS transactiondate "

            If mintCurrencyId > 0 Then
                StrQ &= " ,ApprTable.countryunkid,ApprTable.currency_name "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM cmclaim_request_master " & _
                        " JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid " & _
                        " JOIN ( " & _
                        "       SELECT " & _
                        "            crmasterunkid " & _
                        "	        ,CASE WHEN cmexpense_master.uomunkid = 1 THEN SUM(cmclaim_request_tran.quantity) " & _
                        "                     WHEN cmexpense_master.uomunkid = 2 THEN SUM(cmclaim_request_tran.amount) " & _
                        "           END AS ReqAmt "


            If mintCurrencyId > 0 Then
                StrQ &= " , cmclaim_request_tran.countryunkid,cfexchange_rate.currency_name "
            End If

            StrQ &= "          FROM cmclaim_request_tran " & _
                        "           JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid "

            If mintCurrencyId > 0 Then
                StrQ &= "  LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_request_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_request_tran.countryunkid "
            End If

            StrQ &= "       WHERE cmexpense_master.uomunkid = " & mintUOMunkid


            If mintExpenseID > 0 Then
                StrQ &= " AND cmclaim_request_tran.expenseunkid = " & mintExpenseID
            End If

            If mintCurrencyId > 0 Then
                StrQ &= " AND cmclaim_request_tran.countryunkid = @countryunkid "
            End If

            StrQ &= "GROUP BY crmasterunkid,cmexpense_master.uomunkid "

            If mintCurrencyId > 0 Then
                StrQ &= ", cmclaim_request_tran.countryunkid,cfexchange_rate.currency_name "
            End If

            StrQ &= " ) AS ReqTable ON ReqTable.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                         " JOIN( " & _
                         "                      SELECT " & _
                         "                          crmasterunkid " & _
                         "                          ,ApprAmt " & _
                         "                          ,approvaldate " & _
                         "                          ,statusunkid " & _
                         "                          ,A.ApprName " & _
                         "                          ,ROW_NUMBER() OVER (PARTITION BY crmasterunkid ORDER BY  statusunkid, crpriority DESC) AS Rno "


            If mintCurrencyId > 0 Then
                StrQ &= "              ,countryunkid,currency_name "
            End If


            StrQ &= "         FROM " & _
                        "          ( "


            StrQuery = "    SELECT " & _
                             "       cmclaim_approval_tran.crapproverunkid " & _
                             "       ,cmclaim_approval_tran.crmasterunkid " & _
                             "       ,cmclaim_approval_tran.approvaldate " & _
                             "       ,CASE WHEN cmexpense_master.uomunkid = 1 THEN SUM(cmclaim_approval_tran.quantity) " & _
                             "                 WHEN cmexpense_master.uomunkid = 2 THEN SUM(cmclaim_approval_tran.amount)  " & _
                             "        END AS ApprAmt " & _
                             "       ,cmclaim_approval_tran.statusunkid " & _
                             "       ,cmclaim_approval_tran.visibleid " & _
                             "       , #APPR_NAME# AS ApprName " & _
                             "       ,cmapproverlevel_master.crpriority "


            If mintCurrencyId > 0 Then
                StrQuery &= "               ,cmclaim_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            

            StrQuery &= "               FROM cmclaim_approval_tran " & _
                               "               LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                               "               LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                               "               LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                               "               JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                               "               #EMPL_JOIN#  "


            If mintCurrencyId > 0 Then
                StrQuery &= " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_approval_tran.exchangerateunkid	AND cfexchange_rate.countryunkid = cmclaim_approval_tran.countryunkid "
            End If


            StrQCondition = " WHERE cmclaim_approval_tran.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 AND cmclaim_request_master.iscancel = 0 AND cmclaim_approval_tran.iscancel = 0 " & _
                                     "  AND cmexpense_master.uomunkid =   " & mintUOMunkid & _
                                      " AND cmexpapprover_master.isexternalapprover = #isExternal#  "


            If mintExpenseID > 0 Then
                StrQCondition &= " AND cmclaim_approval_tran.expenseunkid = " & mintExpenseID
            End If

            If mintCurrencyId > 0 Then
                StrQCondition &= " AND cmclaim_approval_tran.countryunkid = @countryunkid "
            End If


            StrQCondition &= "GROUP BY " & _
                                    "cmclaim_approval_tran.crapproverunkid " & _
                                    ",cmclaim_approval_tran.crmasterunkid " & _
                                    ",cmclaim_approval_tran.approvaldate " & _
                                    ",cmclaim_approval_tran.statusunkid " & _
                                    ",cmclaim_approval_tran.visibleid " & _
                                    ",cmexpense_master.uomunkid " & _
                                    ",cmapproverlevel_master.crpriority " & _
                                    ",#APPR_NAME# "

            If mintCurrencyId > 0 Then
                StrQCondition &= " ,cmclaim_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If

            StrQuery &= StrQCondition

            StrFinalQurey = StrQuery
            StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid")
            StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", IIf(mblnFirstNamethenSurname, "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", " ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') "))
            StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
            StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "0")

            Dim objExpApproverMaster As New clsExpenseApprover_Master
            Dim dsExternalCompany As DataSet = objExpApproverMaster.GetClaimExternalApproverList(Nothing, "List")
            objExpApproverMaster = Nothing
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dr In dsExternalCompany.Tables(0).Rows
                'Pinkal (22-Oct-2021)-- Start
                'KBC #AH-4475 - Error Generating Approved Claims Report
                StrFinalQurey &= " UNION " & StrQuery
                'Pinkal (22-Oct-2021)-- End
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")
                    StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", "")
                Else

                    If mblnFirstNamethenSurname Then
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    Else
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                     "ELSE ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') END ")
                    End If

                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", dr("DName") & "..")

                End If

                If dr("DName").ToString.Trim.Length > 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", dr("DName") & "..")
                Else
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
                End If
                StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")

            Next

            StrQ &= StrFinalQurey



            StrQ &= ")AS A  " & _
                        ") AS ApprTable ON ApprTable.crmasterunkid = cmclaim_request_master.crmasterunkid AND ApprTable.Rno <=1 "

            
            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 AND cmclaim_request_master.iscancel = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery


            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("Code")
                rpt_Rows.Item("Column2") = info1.ToTitleCase(dtRow.Item("employee").ToString().ToLower())
                rpt_Rows.Item("Column3") = dtRow.Item("ClaimNo")
                rpt_Rows.Item("Column4") = Format(dtRow.Item("ReqAmt"), GUI.fmtCurrency)
                rpt_Rows.Item("Column5") = Format(dtRow.Item("ApprAmount"), GUI.fmtCurrency)
                rpt_Rows.Item("Column6") = CDate(dtRow.Item("approvaldate")).ToShortDateString()
                rpt_Rows.Item("Column7") = info1.ToTitleCase(dtRow.Item("ApprName").ToString().ToLower())
                rpt_Rows.Item("Column9") = dtRow.Item("expensetype").ToString()

                If mintViewIndex > 0 Then
                    rpt_Rows.Item("Column10") = mstrCurrency & " " & Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqAmt)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(ReqAmt)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column11") = mstrCurrency & " " & Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ApprAmount)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(ApprAmount)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column12") = mstrCurrency & " " & Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqAmt)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(ReqAmt)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column13") = mstrCurrency & " " & Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ApprAmount)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(ApprAmount)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                Else
                    rpt_Rows.Item("Column10") = mstrCurrency & " " & Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqAmt)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), 0, dsList.Tables(0).Compute("SUM(ReqAmt)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column11") = mstrCurrency & " " & Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ApprAmount)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), 0, dsList.Tables(0).Compute("SUM(ApprAmount)", "expensetypeid = " & CInt(dtRow.Item("expensetypeid")))), GUI.fmtCurrency)
                End If

                rpt_Rows.Item("Column15") = mstrCurrency & " " & Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ReqAmt)", "1=1")), 0, dsList.Tables(0).Compute("SUM(ReqAmt)", "1=1")), GUI.fmtCurrency)
                rpt_Rows.Item("Column16") = mstrCurrency & " " & Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(ApprAmount)", "1=1")), 0, dsList.Tables(0).Compute("SUM(ApprAmount)", "1=1")), GUI.fmtCurrency)


                If mintViewIndex > 0 Then
                    rpt_Rows.Item("Column14") = dtRow.Item("GName").ToString()
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            info1 = Nothing

            objRpt = New ArutiReport.Designer.rptApprovedCRSummary

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If mintViewIndex <= 0 Then
                ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            Else
                Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
                Call ReportFunction.TextChange(objRpt, "txtAllocationGroupTotal", mstrReport_GroupName.Trim.Replace(":", "") & Language.getMessage(mstrModuleName, 11, " Total :"))
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 13, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 14, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 15, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 16, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 17, "Printed Date :"))


            Call ReportFunction.TextChange(objRpt, "txtClaimNo", Language.getMessage(mstrModuleName, 7, "Claim No."))
            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 8, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 9, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtReqAmt", Language.getMessage(mstrModuleName, 18, "Req. Amount"))
            Call ReportFunction.TextChange(objRpt, "txtApprAmt", Language.getMessage(mstrModuleName, 19, "Approved Amount"))
            Call ReportFunction.TextChange(objRpt, "txtApprovalDate", Language.getMessage(mstrModuleName, 20, "Approval Date"))
            Call ReportFunction.TextChange(objRpt, "txtApprovedBy", Language.getMessage(mstrModuleName, 21, "Approved By"))
            Call ReportFunction.TextChange(objRpt, "txtCategory", Language.getMessage(mstrModuleName, 2, "Expense Category :"))

            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 22, "Group Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 23, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Approved From Date :")
			Language.setMessage(mstrModuleName, 2, "Expense Category :")
			Language.setMessage(mstrModuleName, 3, "Employee :")
			Language.setMessage(mstrModuleName, 4, "Expense :")
			Language.setMessage(mstrModuleName, 5, "Currency :")
			Language.setMessage(mstrModuleName, 6, " Order By :")
			Language.setMessage(mstrModuleName, 7, "Claim No.")
			Language.setMessage(mstrModuleName, 8, "Code")
			Language.setMessage(mstrModuleName, 9, "Employee")
			Language.setMessage(mstrModuleName, 10, "Approved From Date")
			Language.setMessage(mstrModuleName, 11, " Total :")
			Language.setMessage(mstrModuleName, 12, "Prepared By :")
			Language.setMessage(mstrModuleName, 13, "Checked By :")
			Language.setMessage(mstrModuleName, 14, "Approved By :")
			Language.setMessage(mstrModuleName, 15, "Received By :")
			Language.setMessage(mstrModuleName, 16, "Printed By :")
			Language.setMessage(mstrModuleName, 17, "Printed Date :")
			Language.setMessage(mstrModuleName, 18, "Req. Amount")
			Language.setMessage(mstrModuleName, 19, "Approved Amount")
			Language.setMessage(mstrModuleName, 20, "Approval Date")
			Language.setMessage(mstrModuleName, 21, "Approved By")
			Language.setMessage(mstrModuleName, 22, "Group Total :")
			Language.setMessage(mstrModuleName, 23, "Grand Total :")
			Language.setMessage("clsExpCommonMethods", 2, "Leave")
			Language.setMessage("clsExpCommonMethods", 3, "Medical")
			Language.setMessage("clsExpCommonMethods", 4, "Training")
			Language.setMessage("clsExpCommonMethods", 8, "Miscellaneous")
			Language.setMessage("clsExpCommonMethods", 9, "Imprest")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
