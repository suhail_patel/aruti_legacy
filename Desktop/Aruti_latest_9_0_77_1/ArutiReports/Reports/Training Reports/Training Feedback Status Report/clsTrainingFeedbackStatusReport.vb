﻿'Class Name : clsTrainingFeedbackStatusReport.vb
'Purpose    :
'Date       : 04-Sep-2021
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsTrainingFeedbackStatusReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsTrainingFeedbackStatusReport"
    Private mstrReportId As String = enArutiReport.Training_Feedback_Status_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintFeedbackModeId As Integer = 0
    Private mstrFeedbackModeName As String = ""
    Private mintTrainingUnkid As Integer = 0
    Private mstrTrainingName As String = ""
    Private mintFeedbackStatusid As Integer = 0
    Private mstrFeedbackStatusName As String = ""
    Private mintCalendarUnkid As Integer = 0
    Private mstrCalendarName As String = ""
    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = ""
    'Hemant (25 Oct 2021) -- Start
    'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
    Private mblnIsActive As Boolean = True
    'Hemant (25 Oct 2021) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _FeedbackModeid() As Integer
        Set(ByVal value As Integer)
            mintFeedbackModeId = value
        End Set
    End Property

    Public WriteOnly Property _FeedbackModeName() As String
        Set(ByVal value As String)
            mstrFeedbackModeName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingUnkid() As Integer
        Set(ByVal value As Integer)
            mintTrainingUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property

    Public WriteOnly Property _FeedbackStatusid() As Integer
        Set(ByVal value As Integer)
            mintFeedbackStatusid = value
        End Set
    End Property

    Public WriteOnly Property _FeedbackStatusName() As String
        Set(ByVal value As String)
            mstrFeedbackStatusName = value
        End Set
    End Property


    Public WriteOnly Property _CalendarUnkid() As Integer
        Set(ByVal value As Integer)
            mintCalendarUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CalendarName() As String
        Set(ByVal value As String)
            mstrCalendarName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    'Hemant (25 Oct 2021) -- Start
    'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Hemant (25 Oct 2021) -- End

    
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintFeedbackModeId = 0
            mstrFeedbackModeName = ""
            mintTrainingUnkid = 0
            mstrTrainingName = ""
            mintFeedbackStatusid = 0
            mstrFeedbackStatusName = ""
            mintCalendarUnkid = 0
            mstrCalendarName = ""
            mintEmployeeUnkid = 0
            mstrEmployeeName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintFeedbackModeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Evaluation Category :") & " " & mstrFeedbackModeName & " "
                If mintFeedbackStatusid > 0 Then
                    Select Case mintFeedbackModeId
                        Case clseval_group_master.enFeedBack.PRETRAINING
                            If mintFeedbackStatusid = 1 Then
                                Me._FilterQuery &= " AND trtraining_request_master.ispretrainingfeedback_submitted = 1 "
                            ElseIf mintFeedbackStatusid = 2 Then
                                Me._FilterQuery &= " AND trtraining_request_master.ispretrainingfeedback_submitted = 0 "
                            End If
                        Case clseval_group_master.enFeedBack.POSTTRAINING
                            If mintFeedbackStatusid = 1 Then
                                Me._FilterQuery &= " AND trtraining_request_master.isposttrainingfeedback_submitted = 1 "
                            ElseIf mintFeedbackStatusid = 2 Then
                                Me._FilterQuery &= " AND trtraining_request_master.isposttrainingfeedback_submitted = 0 "
                            End If
                        Case clseval_group_master.enFeedBack.PRETRAINING
                            If mintFeedbackStatusid = 1 Then
                                Me._FilterQuery &= " AND trtraining_request_master.isdaysafterfeedback_submitted = 1 "
                            ElseIf mintFeedbackStatusid = 2 Then
                                Me._FilterQuery &= " AND trtraining_request_master.isdaysafterfeedback_submitted = 0 "
                            End If
                    End Select
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Feedback Status :") & " " & mstrFeedbackStatusName & " "
                Else
                    Select Case mintFeedbackModeId
                        Case clseval_group_master.enFeedBack.PRETRAINING
                            Me._FilterQuery &= " AND ( trtraining_request_master.ispretrainingfeedback_submitted = 1 OR trtraining_request_master.ispretrainingfeedback_submitted = 0 ) "
                        Case clseval_group_master.enFeedBack.POSTTRAINING
                            Me._FilterQuery &= " AND ( trtraining_request_master.isposttrainingfeedback_submitted = 1 OR trtraining_request_master.isposttrainingfeedback_submitted = 0 ) "
                        Case clseval_group_master.enFeedBack.PRETRAINING
                            Me._FilterQuery &= " AND ( trtraining_request_master.isdaysafterfeedback_submitted = 1 OR trtraining_request_master.isdaysafterfeedback_submitted = 0 ) "
                    End Select
                End If


            End If

            If mintTrainingUnkid > 0 Then
                objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingUnkid)
                Me._FilterQuery &= " AND trtraining_request_master.coursemasterunkid = @coursemasterunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Training :") & " " & mstrTrainingName & " "
            End If

            If mintCalendarUnkid > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarUnkid)
                Me._FilterQuery &= " AND trtraining_request_master.periodunkid = @periodunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Period :") & " " & mstrCalendarName & " "
            End If

            If mintEmployeeUnkid > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid)
                Me._FilterQuery &= " AND trtraining_request_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Employee :") & " " & mstrEmployeeName & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = ""
            OrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As Date, _
                                     ByVal xPeriodEnd As Date, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal xExportReportPath As String, _
                                     ByVal xOpenReportAfterExport As Boolean)
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Training")

            dtCol = New DataColumn("SrNo", System.Type.GetType("System.Int32"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Sr No")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Period", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Period")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Employee Code")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Employee Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("JobTitle", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Job Title")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Department", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "Department")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("ProgramName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "Program Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("FeedbackStatus", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Feedback Status")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            StrQ &= "SELECT " & _
                         "  trtraining_request_master.employeeunkid " & _
                         ", ISNULL(trtraining_calendar_master.calendar_name, '') AS Period " & _
                         ", ISNULL(hremployee_master.employeecode, '') AS EmployeeCode " & _
                         ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                         ", ISNULL(hjb.job_name, '') AS Job " & _
                         ", ISNULL(hd.name, '') AS Department " & _
                         ", ISNULL(tcourse.name, '')  AS Training "

            Select Case mintFeedbackModeId
                Case clseval_group_master.enFeedBack.PRETRAINING
                    StrQ &= ", CASE WHEN ispretrainingfeedback_submitted = 1 THEN @Done " & _
                        "       ELSE @NotDone " & _
                        "  END as Status "
                Case clseval_group_master.enFeedBack.POSTTRAINING
                    StrQ &= ", CASE WHEN isposttrainingfeedback_submitted = 1 THEN @Done " & _
                        "       ELSE @NotDone " & _
                        "  END as Status "
                Case clseval_group_master.enFeedBack.DAYSAFTERTRAINING
                    StrQ &= ", CASE WHEN isdaysafterfeedback_submitted = 1 THEN @Done " & _
                        "       ELSE @NotDone " & _
                        "  END as Status "
            End Select
           
            StrQ &= "FROM trtraining_request_master " & _
                    "LEFT JOIN trtraining_calendar_master ON trtraining_calendar_master.calendarunkid = trtraining_request_master.periodunkid and trtraining_calendar_master.isactive = 1 " & _
                    "LEFT JOIN cfcommon_master AS tcourse ON tcourse.masterunkid = trtraining_request_master.coursemasterunkid " & _
                                               "AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                                               "AND tcourse.isactive = 1 " & _
                     "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = trtraining_request_master.employeeunkid " & _
                                              "LEFT JOIN " & _
                                               "( " & _
                                                       "SELECT " & _
                                                           "stationunkid " & _
                                                          ",deptgroupunkid " & _
                                                          ",departmentunkid " & _
                                                          ",sectiongroupunkid " & _
                                                          ",sectionunkid " & _
                                                          ",unitgroupunkid " & _
                                                          ",unitunkid " & _
                                                          ",teamunkid " & _
                                                          ",classgroupunkid " & _
                                                           ",classunkid " & _
                                                          ",employeeunkid " & _
                                                          ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                                                     "FROM hremployee_transfer_tran " & _
                                                     "WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate " & _
                                               ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                                               "LEFT JOIN hrdepartment_master hd ON t.departmentunkid = hd.departmentunkid " & _
                                                     "LEFT JOIN " & _
                                               "( " & _
                                                           "SELECT " & _
                                                               "jobgroupunkid " & _
                                                              ",jobunkid " & _
                                                              ",employeeunkid " & _
                                                              ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                                                           "FROM hremployee_categorization_tran " & _
                                                           "WHERE isvoid = 0     AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate " & _
                                               ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                                               "LEFT JOIN hrjob_master hjb ON hjb.jobunkid = j.jobunkid "

            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'Hemant (25 Oct 2021) -- End

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE isvoid = 0 " & _
                    " AND trtraining_request_master.statusunkid = " & clstraining_requisition_approval_master.enApprovalStatus.Approved & " "

            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Hemant (25 Oct 2021) -- End

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart.Date))
            objDataOperation.AddParameter("@Done", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Done"))
            objDataOperation.AddParameter("@NotDone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Not Done"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intCount As Integer = 1
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("SrNo") = intCount
                rpt_Row.Item("Period") = drRow.Item("Period")
                rpt_Row.Item("EmployeeCode") = drRow.Item("EmployeeCode")
                rpt_Row.Item("EmployeeName") = drRow.Item("Employee")
                rpt_Row.Item("JobTitle") = drRow.Item("Job")
                rpt_Row.Item("Department") = drRow.Item("Department")
                rpt_Row.Item("ProgramName") = drRow.Item("Training")
                rpt_Row.Item("FeedbackStatus") = drRow.Item("Status")

                dtFinalTable.Rows.Add(rpt_Row)

                intCount = intCount + 1
            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Dim strarrGroupColumns As String() = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            'row = New WorksheetRow()

            'If Me._FilterTitle.ToString.Length > 0 Then
            '    wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
            '    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            '    row.Cells.Add(wcell)
            'End If
            'rowsArrayHeader.Add(row)

            If mintFeedbackModeId > 0 Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Evaluation Category :") & " " & mstrFeedbackModeName, "s10bw")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayHeader.Add(row)
            End If

            If mintTrainingUnkid > 0 Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Training :") & " " & mstrTrainingName, "s10bw")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayHeader.Add(row)
            End If

            If mintFeedbackStatusid > 0 Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "Feedback Status :") & " " & mstrFeedbackStatusName, "s10bw")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayHeader.Add(row)
            End If

            If mintCalendarUnkid > 0 Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Period :") & " " & mstrCalendarName, "s10bw")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayHeader.Add(row)
            End If

            If mintEmployeeUnkid > 0 Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Employee :") & " " & mstrEmployeeName, "s10bw")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayHeader.Add(row)
            End If

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = {100, 100, 100, 150, 100, 150, 150, 100}
            'ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            'For i As Integer = 0 To intArrayColumnWidth.Length - 1
            '    intArrayColumnWidth(i) = 125
            'Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", True, rowsArrayHeader)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sr No")
			Language.setMessage(mstrModuleName, 2, "Period")
			Language.setMessage(mstrModuleName, 3, "Employee Code")
			Language.setMessage(mstrModuleName, 4, "Employee Name")
			Language.setMessage(mstrModuleName, 5, "Job Title")
			Language.setMessage(mstrModuleName, 6, "Department")
			Language.setMessage(mstrModuleName, 7, "Program Name")
			Language.setMessage(mstrModuleName, 8, "Feedback Status")
			Language.setMessage(mstrModuleName, 9, "Done")
			Language.setMessage(mstrModuleName, 10, "Not Done")
			Language.setMessage(mstrModuleName, 11, "Period :")
			Language.setMessage(mstrModuleName, 12, "Employee :")
			Language.setMessage(mstrModuleName, 13, "Evaluation Category :")
			Language.setMessage(mstrModuleName, 14, "Training :")
			Language.setMessage(mstrModuleName, 15, "Prepared By :")
			Language.setMessage(mstrModuleName, 16, "Checked By :")
			Language.setMessage(mstrModuleName, 17, "Approved By")
			Language.setMessage(mstrModuleName, 18, "Received By :")
			Language.setMessage(mstrModuleName, 19, "Feedback Status :")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
