﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTrainingBudgetReportNew
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTrainingBudgetReportNew))
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkDisplayChart = New System.Windows.Forms.LinkLabel
        Me.chkIncludeZeroCost = New System.Windows.Forms.CheckBox
        Me.objbtnSearchTraining = New eZee.Common.eZeeGradientButton
        Me.cboTrainingName = New System.Windows.Forms.ComboBox
        Me.lblTrainingName = New System.Windows.Forms.Label
        Me.cboTrainingCalendar = New System.Windows.Forms.ComboBox
        Me.lblTrainingCalendar = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.chAnalysis_Chart = New System.Windows.Forms.DataVisualization.Charting.Chart
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSave = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuPrintPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.gbFilterCriteria.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        CType(Me.chAnalysis_Chart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.SuspendLayout()
        Me.cmnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkDisplayChart)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeZeroCost)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTraining)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrainingName)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrainingName)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrainingCalendar)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrainingCalendar)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(10, 70)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(459, 134)
        Me.gbFilterCriteria.TabIndex = 63
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkDisplayChart
        '
        Me.lnkDisplayChart.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkDisplayChart.BackColor = System.Drawing.Color.Transparent
        Me.lnkDisplayChart.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkDisplayChart.Location = New System.Drawing.Point(124, 105)
        Me.lnkDisplayChart.Name = "lnkDisplayChart"
        Me.lnkDisplayChart.Size = New System.Drawing.Size(94, 17)
        Me.lnkDisplayChart.TabIndex = 296
        Me.lnkDisplayChart.TabStop = True
        Me.lnkDisplayChart.Text = "Display Chart"
        Me.lnkDisplayChart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeZeroCost
        '
        Me.chkIncludeZeroCost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeZeroCost.Location = New System.Drawing.Point(127, 84)
        Me.chkIncludeZeroCost.Name = "chkIncludeZeroCost"
        Me.chkIncludeZeroCost.Size = New System.Drawing.Size(287, 16)
        Me.chkIncludeZeroCost.TabIndex = 293
        Me.chkIncludeZeroCost.Text = "Include Zero Cost"
        Me.chkIncludeZeroCost.UseVisualStyleBackColor = True
        '
        'objbtnSearchTraining
        '
        Me.objbtnSearchTraining.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTraining.BorderSelected = False
        Me.objbtnSearchTraining.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTraining.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTraining.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTraining.Location = New System.Drawing.Point(425, 59)
        Me.objbtnSearchTraining.Name = "objbtnSearchTraining"
        Me.objbtnSearchTraining.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTraining.TabIndex = 98
        '
        'cboTrainingName
        '
        Me.cboTrainingName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainingName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainingName.FormattingEnabled = True
        Me.cboTrainingName.Location = New System.Drawing.Point(127, 57)
        Me.cboTrainingName.Name = "cboTrainingName"
        Me.cboTrainingName.Size = New System.Drawing.Size(287, 21)
        Me.cboTrainingName.TabIndex = 24
        '
        'lblTrainingName
        '
        Me.lblTrainingName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingName.Location = New System.Drawing.Point(14, 60)
        Me.lblTrainingName.Name = "lblTrainingName"
        Me.lblTrainingName.Size = New System.Drawing.Size(84, 15)
        Me.lblTrainingName.TabIndex = 23
        Me.lblTrainingName.Text = "Training Name"
        Me.lblTrainingName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTrainingCalendar
        '
        Me.cboTrainingCalendar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainingCalendar.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainingCalendar.FormattingEnabled = True
        Me.cboTrainingCalendar.Location = New System.Drawing.Point(127, 30)
        Me.cboTrainingCalendar.Name = "cboTrainingCalendar"
        Me.cboTrainingCalendar.Size = New System.Drawing.Size(287, 21)
        Me.cboTrainingCalendar.TabIndex = 4
        '
        'lblTrainingCalendar
        '
        Me.lblTrainingCalendar.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingCalendar.Location = New System.Drawing.Point(14, 33)
        Me.lblTrainingCalendar.Name = "lblTrainingCalendar"
        Me.lblTrainingCalendar.Size = New System.Drawing.Size(108, 15)
        Me.lblTrainingCalendar.TabIndex = 3
        Me.lblTrainingCalendar.Text = "Training Calendar"
        Me.lblTrainingCalendar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(1125, 60)
        Me.eZeeHeader.TabIndex = 64
        Me.eZeeHeader.Title = "Training Budget Spreadsheet"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 645)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(1125, 55)
        Me.EZeeFooter1.TabIndex = 65
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(832, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 34
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(928, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 33
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(1024, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 32
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'chAnalysis_Chart
        '
        Me.chAnalysis_Chart.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chAnalysis_Chart.BackColor = System.Drawing.SystemColors.Control
        Me.chAnalysis_Chart.BorderlineColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.chAnalysis_Chart.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.chAnalysis_Chart.BorderlineWidth = 4
        ChartArea1.AxisX.MajorGrid.Enabled = False
        ChartArea1.AxisY.MajorGrid.Enabled = False
        ChartArea1.BackColor = System.Drawing.SystemColors.Control
        ChartArea1.Name = "ChartArea1"
        Me.chAnalysis_Chart.ChartAreas.Add(ChartArea1)
        Legend1.Alignment = System.Drawing.StringAlignment.Far
        Legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom
        Legend1.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row
        Legend1.Name = "Legend1"
        Legend1.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Tall
        Me.chAnalysis_Chart.Legends.Add(Legend1)
        Me.chAnalysis_Chart.Location = New System.Drawing.Point(0, 0)
        Me.chAnalysis_Chart.Name = "chAnalysis_Chart"
        Series1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.MarkerSize = 1
        Series1.Name = "Series1"
        Series1.ShadowOffset = 5
        Me.chAnalysis_Chart.Series.Add(Series1)
        Me.chAnalysis_Chart.Size = New System.Drawing.Size(1100, 340)
        Me.chAnalysis_Chart.TabIndex = 0
        Title1.Name = "Title1"
        Me.chAnalysis_Chart.Titles.Add(Title1)
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.chAnalysis_Chart)
        Me.pnlMain.Location = New System.Drawing.Point(10, 210)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1100, 340)
        Me.pnlMain.TabIndex = 66
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(10, 556)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(97, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperations
        Me.btnOperation.TabIndex = 67
        Me.btnOperation.Text = "Operation"
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSave, Me.objSep1, Me.mnuPrintPreview, Me.mnuPrint})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(144, 76)
        '
        'mnuSave
        '
        Me.mnuSave.Name = "mnuSave"
        Me.mnuSave.Size = New System.Drawing.Size(143, 22)
        Me.mnuSave.Text = "&Save"
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(140, 6)
        '
        'mnuPrintPreview
        '
        Me.mnuPrintPreview.Name = "mnuPrintPreview"
        Me.mnuPrintPreview.Size = New System.Drawing.Size(143, 22)
        Me.mnuPrintPreview.Text = "Print P&review"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(143, 22)
        Me.mnuPrint.Text = "&Print"
        '
        'frmTrainingBudgetReportNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1125, 700)
        Me.Controls.Add(Me.btnOperation)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmTrainingBudgetReportNew"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Training Budget Spreadsheet"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        CType(Me.chAnalysis_Chart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.ResumeLayout(False)
        Me.cmnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkIncludeZeroCost As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchTraining As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTrainingName As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrainingName As System.Windows.Forms.Label
    Friend WithEvents cboTrainingCalendar As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrainingCalendar As System.Windows.Forms.Label
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents lnkDisplayChart As System.Windows.Forms.LinkLabel
    Private WithEvents chAnalysis_Chart As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuPrintPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
End Class
