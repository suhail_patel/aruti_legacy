﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region
Public Class clsTrainingStatusSummaryReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsTrainingStatusSummaryReport"
    Private mstrReportId As String = enArutiReport.Training_Status_Summary_Report  '275
    Dim objDataOperation As clsDataOperation
   
#Region "Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)

    End Sub

#End Region

#Region " Private Variables "

    Private mintCalendarUnkId As Integer = 0
    Private mstrCalendarName As String = String.Empty
    Private mintTrainingStatusId As Integer = 0
    Private mstrTrainingStatusName As String = String.Empty
    Private mintTrainingUnkId As Integer = 0
    Private mstrTrainingName As String = String.Empty
    Private mblnIsActive As Boolean = True
    Private mblnIsShowTrainingStatusChart As Boolean = False

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mstrAdvance_Filter As String = String.Empty

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#End Region

#Region " Properties "

    Public WriteOnly Property _CalendarUnkid() As Integer
        Set(ByVal value As Integer)
            mintCalendarUnkId = value
        End Set
    End Property

    Public WriteOnly Property _CalendarName() As String
        Set(ByVal value As String)
            mstrCalendarName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingStatusid() As Integer
        Set(ByVal value As Integer)
            mintTrainingStatusId = value
        End Set
    End Property

    Public WriteOnly Property _TrainingStatusName() As String
        Set(ByVal value As String)
            mstrTrainingStatusName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingUnkid() As Integer
        Set(ByVal value As Integer)
            mintTrainingUnkId = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _IsShowTrainingStatusChart() As Boolean
        Set(ByVal value As Boolean)
            mblnIsShowTrainingStatusChart = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintCalendarUnkId = 0
            mstrCalendarName = String.Empty
            mintTrainingStatusId = 0
            mstrTrainingStatusName = String.Empty
            mintTrainingUnkId = 0
            mstrTrainingName = String.Empty
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultValue", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid
            objConfig._Companyunkid = xCompanyUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "generateReport", mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = ""
            OrderByQuery = ""

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "setDefaultOrderBy", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Sub FilterTitleAndFilterQuery()

        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintCalendarUnkId > 0 Then
                objDataOperation.AddParameter("@calendarUnkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarUnkId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Calendar : ") & " " & mstrCalendarName & " "
            End If

            If mintTrainingUnkId > 0 Then
                objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingUnkId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Training : ") & " " & mstrTrainingName & " "
            End If

            If mintTrainingStatusId > 0 Then
                objDataOperation.AddParameter("@TrainingStatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingStatusId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Training Status : ") & " " & mstrTrainingStatusName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FilterTitleAndFilterQuery", mstrModuleName)
        End Try
    End Sub

#End Region


#Region " Report Generation "

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As DateTime, _
                                           ByVal xPeriodEnd As DateTime, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim StrQ As String = ""
        Dim StrInnerQry As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As New ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, True, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            StrQ &= "IF OBJECT_ID('tempdb..#TableEmp') IS NOT NULL DROP TABLE #TableEmp " & _
                     "IF OBJECT_ID('tempdb..#Status') IS NOT NULL DROP TABLE #Status "

            StrQ &= "SELECT   hremployee_master.employeeunkid " & _
                           ", hremployee_master.employeecode AS ECode " & _
                           ", ISNULL(hrdepartment_master.name, '') AS Dept " & _
                           ", CASE WHEN CONVERT(CHAR(8),hremployee_master.appointeddate, 112) <= @enddate  " & _
                               "AND ISNULL(CONVERT(CHAR(8),ETERM.termination_from_date, 112),@startdate) >= @startdate " & _
                               "AND ISNULL(CONVERT(CHAR(8),ERET.termination_to_date, 112),@startdate) >= @startdate " & _
                               "AND ISNULL(CONVERT(CHAR(8),ETERM.empl_enddate, 112),@startdate) >= @startdate " & _
                               "THEN @Active ELSE @Inactive END AS EmployeeStatus "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS EName "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS EName "
            End If

            StrQ &= "INTO    #TableEmp " & _
                    "FROM  hremployee_master "

            StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                        "( " & _
                        "	SELECT " & _
                        "		 gradegroupunkid " & _
                        "		,gradeunkid " & _
                        "		,gradelevelunkid " & _
                        "		,employeeunkid " & _
                        "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                        "	FROM prsalaryincrement_tran " & _
                        "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    		 TERM.TEEmpId " & _
                    "    		,TERM.empl_enddate " & _
                    "    		,TERM.termination_from_date " & _
                    "    		,TERM.TEfDt " & _
                    "    		,TERM.isexclude_payroll " & _
                    "    		,TERM.TR_REASON " & _
                    "    		,TERM.changereasonunkid " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             TRM.employeeunkid AS TEEmpId " & _
                    "            ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                    "            ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                    "            ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                    "            ,TRM.isexclude_payroll " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                    "            ,TRM.changereasonunkid " & _
                    "        FROM hremployee_dates_tran AS TRM " & _
                    "            LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                    "        WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS TERM WHERE TERM.Rno = 1 " & _
                    ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    	 RET.REmpId " & _
                    "    	,RET.termination_to_date " & _
                    "    	,RET.REfDt " & _
                    "    	,RET.RET_REASON " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             RTD.employeeunkid AS REmpId " & _
                    "            ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                    "            ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                    "        FROM hremployee_dates_tran AS RTD " & _
                    "            LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                    "        WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS RET WHERE RET.Rno = 1 " & _
                    ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = T.departmentunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE 1 = 1 "

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= "SELECT " & _
                          "     * INTO #Status " & _
                    "FROM ( "

            If mintTrainingStatusId = 0 OrElse mintTrainingStatusId = 1 Then
                StrInnerQry &= "SELECT " & _
                          "         employeeunkid " & _
                          ",        coursemasterunkid" & _
                          ",        @Enrolled As TrainingStatus " & _
                          "     FROM trtraining_request_master " & _
                          "     WHERE isvoid = 0 " & _
                          "           AND periodunkid = @calendarUnkId " & _
                          "           AND coursemasterunkid = @coursemasterunkid " & _
                          "           AND isenroll_confirm = 1 " & _
                          "           AND completed_statusunkid = " & enTrainingRequestStatus.PENDING & " "
            End If

            If mintTrainingStatusId = 0 OrElse mintTrainingStatusId = 2 Then
                If StrInnerQry.Trim.Length > 0 Then StrInnerQry &= "UNION ALL "

                StrInnerQry &= "SELECT " & _
                          "         employeeunkid " & _
                          ",        coursemasterunkid" & _
                          ",        @Completed As TrainingStatus " & _
                          "     FROM trtraining_request_master " & _
                          "     WHERE isvoid = 0 " & _
                          "           AND periodunkid = @calendarUnkId " & _
                          "           AND coursemasterunkid = @coursemasterunkid " & _
                          "           AND isenroll_confirm = 1 " & _
                          "           AND completed_statusunkid = " & enTrainingRequestStatus.APPROVED & " "
            End If

            If mintTrainingStatusId = 0 OrElse mintTrainingStatusId = 3 Then
                If StrInnerQry.Trim.Length > 0 Then StrInnerQry &= "UNION ALL "

                StrInnerQry &= "SELECT " & _
                          "         employeeunkid " & _
                          ",        coursemasterunkid" & _
                          ",        @Cancelled As TrainingStatus " & _
                          "     FROM trtraining_request_master " & _
                          "     WHERE isvoid = 0 " & _
                          "           AND periodunkid = @calendarUnkId " & _
                          "           AND coursemasterunkid = @coursemasterunkid " & _
                          "           AND isenroll_confirm = 0 " & _
                          "           AND isenroll_reject = 1 "
            End If

            If mintTrainingStatusId = 0 OrElse mintTrainingStatusId = 4 Then
                If StrInnerQry.Trim.Length > 0 Then StrInnerQry &= "UNION ALL "

                StrInnerQry &= "SELECT " & _
                          "         employeeunkid " & _
                          ",        coursemasterunkid" & _
                          ",        @PendingToEnroll As TrainingStatus " & _
                          "     FROM trtraining_request_master " & _
                          "     WHERE isvoid = 0 " & _
                          "           AND periodunkid = @calendarUnkId " & _
                          "           AND coursemasterunkid = @coursemasterunkid " & _
                          "           AND statusunkid = " & enTrainingRequestStatus.APPROVED & " " & _
                          "           AND isenroll_confirm = 0 " & _
                          "           AND isenroll_reject = 0 "
            End If

            StrQ &= StrInnerQry & "  ) As A " & _
                      " SELECT " & _
                      "     #TableEmp.employeeunkid " & _
                      ",    #TableEmp.ECode " & _
                      ",    #TableEmp.Ename " & _
                      ",    #TableEmp.Dept" & _
                      ",    ISNULL(cfcommon_master.name, '') AS Training " & _
                      ",    #Status.TrainingStatus " & _
                      ",    #TableEmp.EmployeeStatus " & _
                      " FROM #Status " & _
                      " JOIN #TableEmp	ON #TableEmp.employeeunkid = #Status.employeeunkid " & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = #Status.coursemasterunkid " & _
                      " AND cfcommon_master.isactive = 1 AND mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "  "


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart.Date))
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart).ToString)
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd).ToString)
            objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Active"))
            objDataOperation.AddParameter("@Inactive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "Inactive"))
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@ENROLLED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Enrolled"))
            objDataOperation.AddParameter("@COMPLETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Completed"))
            objDataOperation.AddParameter("@CANCELLED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Cancelled"))
            objDataOperation.AddParameter("@PENDINGTOENROLL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Pending To Enroll"))


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Dim rpt_Rows As DataRow = Nothing
            For Each dRow As DataRow In dsList.Tables("List").Rows
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dRow.Item("ECode").ToString()
                rpt_Rows.Item("Column2") = dRow.Item("EName").ToString()
                rpt_Rows.Item("Column3") = dRow.Item("Dept").ToString()
                rpt_Rows.Item("Column4") = dRow.Item("Training").ToString()
                rpt_Rows.Item("Column5") = dRow.Item("TrainingStatus").ToString()
                rpt_Rows.Item("Column6") = dRow.Item("EmployeeStatus").ToString()
                rpt_Rows.Item("Column81") = 1
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

            Next

            objRpt = New ArutiReport.Designer.rptTrainingStatusSummaryReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription" _
                                        )

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            'If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
            '    rpt_Data.Tables("ArutiTable").Rows.Add("")
            'End If

            objRpt.SetDataSource(rpt_Data)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 24, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 25, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 22, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 23, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 OrElse mblnIsShowTrainingStatusChart = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection6", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 10, "Sr No."))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 11, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 12, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtDept", Language.getMessage(mstrModuleName, 13, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtTrainingName", Language.getMessage(mstrModuleName, 14, "Training Name"))
            Call ReportFunction.TextChange(objRpt, "txtTrainingStatus", Language.getMessage(mstrModuleName, 15, "Training Status"))
            Call ReportFunction.TextChange(objRpt, "txtEmpStatus", Language.getMessage(mstrModuleName, 16, "Employee Status"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 17, "Grand Total"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 18, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 19, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtTrainingStatusChart", Language.getMessage(mstrModuleName, 26, "Training Status Chart"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_DetailReport", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Enrolled")
			Language.setMessage(mstrModuleName, 3, "Completed")
			Language.setMessage(mstrModuleName, 4, "Cancelled")
			Language.setMessage(mstrModuleName, 5, "Pending To Enroll")
			Language.setMessage(mstrModuleName, 6, "Calendar :")
			Language.setMessage(mstrModuleName, 7, "Training Status :")
			Language.setMessage(mstrModuleName, 8, "Training :")
			Language.setMessage(mstrModuleName, 9, "Order By :")
			Language.setMessage(mstrModuleName, 10, "Sr No.")
			Language.setMessage(mstrModuleName, 11, "Employee Code")
			Language.setMessage(mstrModuleName, 12, "Employee Name")
			Language.setMessage(mstrModuleName, 13, "Department")
			Language.setMessage(mstrModuleName, 14, "Training Name")
			Language.setMessage(mstrModuleName, 15, "Training Status")
			Language.setMessage(mstrModuleName, 16, "Employee Status")
			Language.setMessage(mstrModuleName, 17, "Grand Total")
			Language.setMessage(mstrModuleName, 18, "Printed By :")
			Language.setMessage(mstrModuleName, 19, "Printed Date :")
			Language.setMessage(mstrModuleName, 20, "Active")
			Language.setMessage(mstrModuleName, 21, "Inactive")
			Language.setMessage(mstrModuleName, 22, "Approved By :")
			Language.setMessage(mstrModuleName, 23, "Received By :")
			Language.setMessage(mstrModuleName, 24, "Prepared By :")
			Language.setMessage(mstrModuleName, 25, "Checked By :")
            Language.setMessage(mstrModuleName, 26, "Training Status Chart")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
