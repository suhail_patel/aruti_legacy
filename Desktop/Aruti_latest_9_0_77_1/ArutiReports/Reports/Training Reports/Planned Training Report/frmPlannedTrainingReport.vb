﻿'************************************************************************************************************************************
'Class Name : frmTrainingPriorityReport.vb
'Purpose    : 
'Written By : sOHAIL
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPlannedTrainingReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTrainingPriorityReport"
    Private objPlannedTReport As clsPlannedTrainingReport
    Private mintFirstOpenPeriod As Integer = 0

#End Region

#Region " Constructor "

    Public Sub New()
        objPlannedTReport = New clsPlannedTrainingReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objPlannedTReport.SetDefaultValue()
        InitializeComponent()

        '_Show_ExcelExtra_Menu = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objTPeriod As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master
        Dim dsCombo As New DataSet

        Try

            mintFirstOpenPeriod = 0
            dsCombo = objTPeriod.getListForCombo("List", False, 1)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                mintFirstOpenPeriod = CInt(dsCombo.Tables(0).Rows(0).Item("calendarunkid"))
            End If
            dsCombo = objTPeriod.getListForCombo("List", True, 0)
            With cboPeriod
                .DisplayMember = "name"
                .ValueMember = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = mintFirstOpenPeriod.ToString
            End With

            dsCombo = Nothing
            Dim objDeptTNeetMaster As New clsDepartmentaltrainingneed_master
            Dim strIds As String = "-1," & clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved & "," & clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog
            dsCombo = objDeptTNeetMaster.getStatusComboList("List", True, strIds, "")
            With cboStatus
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = "-1"
                .SelectedIndex = 0
            End With
            objDeptTNeetMaster = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objTPeriod = Nothing

            objCommon = Nothing
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Dim dsList As DataSet = Nothing
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Training Calendar."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False
                'ElseIf CInt(cboStatus.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Status."), enMsgBoxStyle.Information)
                '    cboStatus.Select()
                '    Return False
            End If

            

            If dtpStartDate.Checked OrElse dtpEndDate.Checked Then
                Dim objPeriod As New clsTraining_Calendar_Master
                objPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)

                If dtpStartDate.Checked = True Then

                    If dtpStartDate.Value.Date < objPeriod._StartDate OrElse dtpStartDate.Value.Date > objPeriod._EndDate Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Start Date must be between start date and end date of selected training calendar."), enMsgBoxStyle.Information)
                        dtpStartDate.Select()
                        Return False
                    End If

                End If

                If dtpEndDate.Checked = True Then
                    If dtpEndDate.Value.Date < objPeriod._StartDate OrElse dtpEndDate.Value.Date > objPeriod._EndDate Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "End Date must be between start date and end date of selected training calendar."), enMsgBoxStyle.Information)
                        dtpEndDate.Select()
                        Return False
                    End If
                End If

                objPeriod = Nothing
            End If


            objPlannedTReport.SetDefaultValue()

            objPlannedTReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objPlannedTReport._PeriodName = cboPeriod.Text

            If dtpStartDate.Checked Then
                objPlannedTReport._StartDate = dtpStartDate.Value.Date
            End If

            If dtpEndDate.Checked Then
                objPlannedTReport._EndDate = dtpEndDate.Value.Date
            End If

            objPlannedTReport._StatusId = CInt(cboStatus.SelectedValue)
            objPlannedTReport._StatusName = cboStatus.Text

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objPlannedTReport.setDefaultOrderBy(0)
            txtOrderBy.Text = objPlannedTReport.OrderByDisplay
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboStatus.SelectedIndex = 0
            dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpEndDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpStartDate.Checked = False
            dtpEndDate.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPlannedTrainingReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPlannedTReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPlannedTrainingReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPlannedTrainingReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Me._Title = objPlannedTReport._ReportName
            Me._Message = objPlannedTReport._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPlannedTrainingReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPlannedTrainingReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPlannedTrainingReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons"

    Private Sub frmPlannedTrainingReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If SetFilter() = False Then Exit Sub

            Dim dtPeriodStart As Date
            Dim dtPeriodEnd As Date
            Dim objPeriod As New clsTraining_Calendar_Master
            objPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)
            dtPeriodStart = objPeriod._StartDate
            dtPeriodEnd = objPeriod._EndDate

            objPlannedTReport.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                , dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True _
                                                , ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPlannedTrainingReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPlannedTrainingReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If SetFilter() = False Then Exit Sub

            Dim dtPeriodStart As Date
            Dim dtPeriodEnd As Date
            Dim objPeriod As New clsTraining_Calendar_Master
            objPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)
            dtPeriodStart = objPeriod._StartDate
            dtPeriodEnd = objPeriod._EndDate

            objPlannedTReport.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                          dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPlannedTrainingReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPlannedTrainingReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPlannedTrainingReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPlannedTrainingReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPlannedTrainingReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmPlannedTrainingReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPlannedTrainingReport.SetMessages()
            objfrm._Other_ModuleNames = "clsPlannedTrainingReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmPlannedTrainingReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Control"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objPlannedTReport.setOrderBy(0)
            txtOrderBy.Text = objPlannedTReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ComboBox Event"

    Private Sub cboPeriod_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPeriod.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboPeriod.Name Then
                        .CodeMember = "name"
                    ElseIf cbo.Name = cboStatus.Name Then
                        .CodeMember = "priority"
                    Else
                        .CodeMember = "name"
                    End If
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region





    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LblStatus.Text = Language._Object.getCaption(Me.LblStatus.Name, Me.LblStatus.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.LblStartDate.Text = Language._Object.getCaption(Me.LblStartDate.Name, Me.LblStartDate.Text)
			Me.LblEndDate.Text = Language._Object.getCaption(Me.LblEndDate.Name, Me.LblEndDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Training Calendar.")
			Language.setMessage(mstrModuleName, 2, "Please Select Status.")
			Language.setMessage(mstrModuleName, 3, "Please Select Dates to do further operation on it .")
			Language.setMessage(mstrModuleName, 4, "Start Date must be between start date and end date of selected training calendar.")
			Language.setMessage(mstrModuleName, 5, "End Date must be between start date and end date of selected training calendar.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class