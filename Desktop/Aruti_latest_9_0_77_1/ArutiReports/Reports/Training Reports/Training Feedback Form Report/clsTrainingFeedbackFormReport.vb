﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class clsTrainingFeedbackFormReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsTrainingFeedbackFormReport"
    Private mstrReportId As String = enArutiReport.Training_Feedback_Form_Report  '274
    Dim objDataOperation As clsDataOperation
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1

#Region "Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)

    End Sub

#End Region

#Region " Private Variables "
    Private mintCalendarUnkId As Integer = 0
    Private mstrCalendarName As String = String.Empty
    Private mintFeedbackModeId As Integer = 0
    Private mstrFeedbackModeName As String = String.Empty
    Private mintTrainingUnkId As Integer = 0
    Private mstrTrainingName As String = String.Empty
    Private mlstEmployeeIds As List(Of String)
    Private mstrEmployeeName As String = String.Empty
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
#End Region

#Region " Properties "

    Public WriteOnly Property _FeedbackModeid() As Integer
        Set(ByVal value As Integer)
            mintFeedbackModeId = value
        End Set
    End Property

    Public WriteOnly Property _FeedbackModeName() As String
        Set(ByVal value As String)
            mstrFeedbackModeName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingUnkid() As Integer
        Set(ByVal value As Integer)
            mintTrainingUnkId = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property

    Public WriteOnly Property _CalendarUnkid() As Integer
        Set(ByVal value As Integer)
            mintCalendarUnkId = value
        End Set
    End Property

    Public WriteOnly Property _CalendarName() As String
        Set(ByVal value As String)
            mstrCalendarName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeIds() As List(Of String)
        Set(ByVal value As List(Of String))
            mlstEmployeeIds = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintCalendarUnkId = 0
            mstrCalendarName = String.Empty
            mintFeedbackModeId = 0
            mstrFeedbackModeName = String.Empty
            mintTrainingUnkId = 0
            mstrTrainingName = String.Empty
            mlstEmployeeIds = Nothing
            mstrEmployeeName = String.Empty

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultValue", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid
            objConfig._Companyunkid = xCompanyUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "generateReport", mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = "FeedbackMode"
            OrderByQuery = "trtraining_evaluation_tran.feedbackmodeid,  trtraining_evaluation_tran.employeeunkid, trtraining_evaluation_tran.questionnaireid, trtraining_evaluation_tran.subquestionid"

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "setDefaultOrderBy", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Sub FilterTitleAndFilterQuery()

        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintCalendarUnkId > 0 Then
                objDataOperation.AddParameter("@CalendarUnkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarUnkId)
                Me._FilterQuery &= " AND trtraining_request_master.periodunkid  = @CalendarUnkId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Calendar : ") & " " & mstrCalendarName & " "
            End If

            If mintFeedbackModeId > 0 Then
                objDataOperation.AddParameter("@FeedbackModeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFeedbackModeId)
                Me._FilterQuery &= " AND trtraining_evaluation_tran.feedbackmodeid = @FeedbackModeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "FeedbackMode : ") & " " & mstrFeedbackModeName & " "
            End If

            If mintTrainingUnkId > 0 Then
                objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingUnkId)
                Me._FilterQuery &= " AND trtraining_evaluation_tran.coursemasterunkid = @coursemasterunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Training : ") & " " & mstrTrainingName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FilterTitleAndFilterQuery", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As DateTime, _
                                           ByVal xPeriodEnd As DateTime, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objEvalAnswer As New clseval_answer_master
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As New ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Dim dsEvalAnswer As New DataSet

        Dim rpt_DataEmployee As New ArutiReport.Designer.dsArutiReport
        Dim rpt_DataLineManager As New ArutiReport.Designer.dsArutiReport

        Dim dtEmployee As New DataTable
        Dim dtLineManager As New DataTable
        Try
            dsEvalAnswer = objEvalAnswer.GetList("List")
            objDataOperation = New clsDataOperation

            If mlstEmployeeIds IsNot Nothing AndAlso mlstEmployeeIds.Count > 0 Then
                StrQ &= "IF OBJECT_ID('tempdb..#EmpList') IS NOT NULL " & _
                           "DROP TABLE #EmpList "
                StrQ &= "DECLARE @words NVARCHAR (MAX) " & _
                        "SET @words = '" & String.Join(",", mlstEmployeeIds.ToArray) & "' " & _
                        "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                        "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                        "SELECT " & _
                            "@words = @words + ',' " & _
                           ",@start = 1 " & _
                           ",@stop = LEN(@words) + 1 " & _
                                           "WHILE @start < @stop begin " & _
                        "SELECT " & _
                            "@end = CHARINDEX(',', @words, @start) " & _
                           ",@word = RTRIM(LTRIM(SUBSTRING(@words, @start, @end - @start))) " & _
                           ",@start = @end + 1 " & _
                        "INSERT @split " & _
                            "VALUES (@word) " & _
                        "END " & _
                          "SELECT " & _
                            "* INTO #EmpList from ( " & _
                        "SELECT " & _
                            "CAST(word AS INT) " & _
                             "AS empid " & _
                             ",1 AS ROWNO " & _
                        "FROM @split " & _
                         ") AS A " & _
                        "WHERE A.ROWNO = 1 "
            End If

            StrQ &= "SELECT " & _
                       "    trtraining_evaluation_tran.trainingevaluationtranunkid " & _
                       ",   CASE " & _
                       "        WHEN trtraining_evaluation_tran.feedbackmodeid = 1 THEN @PRETRAINING " & _
                       "        WHEN trtraining_evaluation_tran.feedbackmodeid = 2 THEN @POSTTRAINING " & _
                       "        WHEN trtraining_evaluation_tran.feedbackmodeid = 3 THEN @DAYSAFTERTRAINING " & _
                       "    END AS FeedbackMode " & _
                       ",   trtraining_evaluation_tran.employeeunkid " & _
                       ",   ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EName " & _
                       ",   ISNULL(cfcommon_master.name, '') AS Training " & _
                       ",   ISNULL(RepTable.Reporting_Name, '') AS  ReportingToName " & _
                       ",   trtraining_request_master.start_date AS CourseDate " & _
                       ",   trtraining_request_master.end_date AS CompleteDate " & _
                       ",   trtraining_evaluation_tran.questionnaireid " & _
                       ",   ISNULL(treval_question_master.question,'') AS question " & _
                       ",   trtraining_evaluation_tran.subquestionid " & _
                       ",   ISNULL(treval_subquestion_master.subquestion,'') AS subquestion " & _
                       ",   trtraining_evaluation_tran.answertypeid " & _
                       ",   trtraining_evaluation_tran.answer " & _
                       ",   trtraining_evaluation_tran.isaskforjustification " & _
                       ",   trtraining_evaluation_tran.justification_answer " & _
                       ",   trtraining_evaluation_tran.isforlinemanager " & _
                    "   FROM trtraining_evaluation_tran " & _
                    "   JOIN treval_question_master ON treval_question_master.questionnaireId = trtraining_evaluation_tran.questionnaireid AND treval_question_master.isvoid = 0 " & _
                    "   LEFT JOIN treval_subquestion_master ON treval_subquestion_master.subquestionid = trtraining_evaluation_tran.subquestionid AND treval_subquestion_master.isvoid = 0 " & _
                    "   LEFT JOIN hremployee_master    ON hremployee_master.employeeunkid = trtraining_evaluation_tran.employeeunkid "

            If mlstEmployeeIds IsNot Nothing AndAlso mlstEmployeeIds.Count > 0 Then
                StrQ &= " JOIN #EmpList on #EmpList.empid = hremployee_master.employeeunkid "
            End If

            StrQ &= "   LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = trtraining_evaluation_tran.coursemasterunkid    AND cfcommon_master.isactive = 1 AND mastertype = 30 " & _
                    "   LEFT JOIN trtraining_request_master    ON trtraining_request_master.trainingrequestunkid = trtraining_evaluation_tran.trainingrequestunkid    AND trtraining_request_master.isvoid = 0 " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             E_Emp.employeeunkid AS employeeunkid " & _
                    "            ,ISNULL(R_Emp.employeecode, '') AS Reporting_Code " & _
                    "            ,ISNULL(R_Emp.firstname, '') + ' ' + ISNULL(R_Emp.othername, '') + ' ' + ISNULL(R_Emp.surname, '') AS Reporting_Name " & _
                    "            ,ISNULL(R_Emp.firstname, '') AS rfname " & _
                    "            ,ISNULL(R_Emp.othername, '') AS roname " & _
                    "            ,ISNULL(R_Emp.surname, '') AS rsname " & _
                    "            ,CASE WHEN hremployee_reportto.ishierarchy = 1 THEN 1 ELSE 0 END AS Default_Reporting " & _
                    "        FROM hremployee_reportto " & _
                    "            LEFT JOIN hremployee_master AS E_Emp ON hremployee_reportto.employeeunkid = E_Emp.employeeunkid " & _
                    "            LEFT JOIN hremployee_master AS R_Emp ON hremployee_reportto.reporttoemployeeunkid = R_Emp.employeeunkid " & _
                    "        WHERE ishierarchy = 1 AND hremployee_reportto.isvoid = 0 " & _
                    "    ) AS RepTable ON RepTable.employeeunkid = hremployee_master.employeeunkid " & _
                    "   WHERE trtraining_evaluation_tran.isvoid = 0 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            objDataOperation.AddParameter("@PRETRAINING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clseval_group_master", 2, "Pre-Training"))
            objDataOperation.AddParameter("@POSTTRAINING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clseval_group_master", 3, "Post-Training"))
            objDataOperation.AddParameter("@DAYSAFTERTRAINING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clseval_group_master", 4, "Days after training attended."))


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Dim rpt_Rows As DataRow = Nothing

            For Each dRow As DataRow In dsList.Tables("List").Rows
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                
                rpt_Rows.Item("Column1") = dRow.Item("employeeunkid").ToString()
                rpt_Rows.Item("Column10") = dRow.Item("EName").ToString()
                rpt_Rows.Item("Column2") = dRow.Item("FeedbackMode").ToString()
                rpt_Rows.Item("Column3") = dRow.Item("Training").ToString()
                rpt_Rows.Item("Column4") = dRow.Item("ReportingToName").ToString()
                rpt_Rows.Item("Column5") = CDate(dRow.Item("CourseDate")).ToShortDateString
                rpt_Rows.Item("Column6") = CDate(dRow.Item("CompleteDate")).ToShortDateString

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                
            Next

            'Employee Feedback
            Dim intCnt As Integer = 1
            Dim intEmployeeId As Integer = 0
            Dim intQuestionId As Integer = 0
            Dim drEmployeeRow() As DataRow = Nothing

            drEmployeeRow = dsList.Tables(0).Select("isforlinemanager = 0")
            If drEmployeeRow.Length > 0 Then
                dtEmployee = drEmployeeRow.CopyToDataTable()
            End If

            'rpt_DataEmployee = rpt_Data.Tables("ArutiTable").Clone
            For Each dRow As DataRow In dtEmployee.Rows
                rpt_Rows = rpt_DataEmployee.Tables("ArutiTable").NewRow

                If intEmployeeId <> CInt(dRow.Item("employeeunkid")) Then intCnt = 1

                rpt_Rows.Item("Column1") = dRow.Item("employeeunkid").ToString()
                rpt_Rows.Item("Column2") = dRow.Item("FeedbackMode").ToString()

                If intQuestionId = CInt(dRow.Item("questionnaireid")) Then
                    rpt_Rows.Item("Column7") = ""
                Else
                    rpt_Rows.Item("Column7") = dRow.Item("question").ToString()
                    rpt_Rows.Item("Column12") = intCnt.ToString()
                    intCnt = intCnt + 1
                End If
                rpt_Rows.Item("Column8") = dRow.Item("subquestion").ToString()
                If CInt(dRow.Item("answertypeid")) = clseval_question_master.enAnswerType.SELECTION OrElse _
                  CInt(dRow.Item("answertypeid")) = clseval_question_master.enAnswerType.RATING Then
                    Dim strFilter As String = String.Empty
                    If CInt(dRow.Item("subquestionid")) > 0 Then
                        strFilter = "questionnaireid = " & CInt(dRow.Item("questionnaireid")) & " AND subquestionid = " & CInt(dRow.Item("subquestionid")) & " AND optionid = " & CInt(dRow.Item("answer"))
                    Else
                        strFilter = "questionnaireid = " & CInt(dRow.Item("questionnaireid")) & " AND subquestionid <= 0  AND optionid = " & CInt(dRow.Item("answer"))
                    End If

                    Dim drAns() As DataRow = dsEvalAnswer.Tables(0).Select(strFilter)
                    If drAns.Length > 0 Then
                        rpt_Rows.Item("Column9") = drAns(0).Item("optionvalue").ToString
                    End If

                ElseIf CInt(dRow.Item("answertypeid")) = clseval_question_master.enAnswerType.PICKDATE Then
                    rpt_Rows.Item("Column9") = Date.ParseExact(dRow.Item("answer").ToString(), "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo).ToShortDateString()
                Else
                    rpt_Rows.Item("Column9") = dRow.Item("answer").ToString()
                End If

                If CBool(dRow.Item("isAskforJustification")) = True Then
                    rpt_Rows.Item("Column11") = dRow.Item("justification_answer").ToString()
                End If

                rpt_DataEmployee.Tables("ArutiTable").Rows.Add(rpt_Rows)

                intEmployeeId = CInt(dRow.Item("employeeunkid"))
                intQuestionId = CInt(dRow.Item("questionnaireid"))
            Next

            'Line Manager Feedback
            intCnt = 1
            intEmployeeId = 0
            intQuestionId = 0
            Dim drLineManagerRow() As DataRow = Nothing

            drLineManagerRow = dsList.Tables(0).Select("isforlinemanager = 1")
            If drLineManagerRow.Length > 0 Then
                dtLineManager = drLineManagerRow.CopyToDataTable()
            End If

            'rpt_DataLineManager = rpt_Data.Tables("ArutiTable").Clone
            For Each dRow As DataRow In dtLineManager.Rows
                rpt_Rows = rpt_DataLineManager.Tables("ArutiTable").NewRow

                If intEmployeeId <> CInt(dRow.Item("employeeunkid")) Then intCnt = 1

                rpt_Rows.Item("Column1") = dRow.Item("employeeunkid").ToString()
                rpt_Rows.Item("Column2") = dRow.Item("FeedbackMode").ToString()

                If intQuestionId = CInt(dRow.Item("questionnaireid")) Then
                    rpt_Rows.Item("Column7") = ""
                Else
                    rpt_Rows.Item("Column7") = dRow.Item("question").ToString()
                    rpt_Rows.Item("Column12") = intCnt.ToString()
                    intCnt = intCnt + 1
                End If
                rpt_Rows.Item("Column8") = dRow.Item("subquestion").ToString()
                If CInt(dRow.Item("answertypeid")) = clseval_question_master.enAnswerType.SELECTION OrElse _
                  CInt(dRow.Item("answertypeid")) = clseval_question_master.enAnswerType.RATING Then
                    Dim strFilter As String = String.Empty
                    If CInt(dRow.Item("subquestionid")) > 0 Then
                        strFilter = "questionnaireid = " & CInt(dRow.Item("questionnaireid")) & " AND subquestionid = " & CInt(dRow.Item("subquestionid")) & " AND optionid = " & CInt(dRow.Item("answer"))
                    Else
                        strFilter = "questionnaireid = " & CInt(dRow.Item("questionnaireid")) & " AND subquestionid <= 0  AND optionid = " & CInt(dRow.Item("answer"))
                    End If

                    Dim drAns() As DataRow = dsEvalAnswer.Tables(0).Select(strFilter)
                    If drAns.Length > 0 Then
                        rpt_Rows.Item("Column9") = drAns(0).Item("optionvalue").ToString
                    End If

                ElseIf CInt(dRow.Item("answertypeid")) = clseval_question_master.enAnswerType.PICKDATE Then
                    rpt_Rows.Item("Column9") = Date.ParseExact(dRow.Item("answer").ToString(), "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo).ToShortDateString()
                Else
                    rpt_Rows.Item("Column9") = dRow.Item("answer").ToString()
                End If

                If CBool(dRow.Item("isAskforJustification")) = True Then
                    rpt_Rows.Item("Column11") = dRow.Item("justification_answer").ToString()
                End If

                rpt_DataLineManager.Tables("ArutiTable").Rows.Add(rpt_Rows)

                intEmployeeId = CInt(dRow.Item("employeeunkid"))
                intQuestionId = CInt(dRow.Item("questionnaireid"))
            Next

            objRpt = New ArutiReport.Designer.rptTrainingFeedbackFormReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription" _
                                        )

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptEmployeeFeedback").SetDataSource(rpt_DataEmployee)
            objRpt.Subreports("rptLineManagerFeedback").SetDataSource(rpt_DataLineManager)

            If rpt_DataEmployee.Tables("ArutiTable").Rows.Count <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If rpt_DataLineManager.Tables("ArutiTable").Rows.Count <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection4", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtEmp", Language.getMessage(mstrModuleName, 1, "Employee's Name : "))
            Call ReportFunction.TextChange(objRpt, "txtCourse", Language.getMessage(mstrModuleName, 2, "Course Name : "))
            Call ReportFunction.TextChange(objRpt, "txtLineManager", Language.getMessage(mstrModuleName, 3, "Line Manager's Name : "))
            Call ReportFunction.TextChange(objRpt, "txtCourseDate", Language.getMessage(mstrModuleName, 4, "Course Date : "))
            Call ReportFunction.TextChange(objRpt, "txtCompleteDate", Language.getMessage(mstrModuleName, 5, "Date Form was Completed : "))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 6, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 7, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeFeedback"), "txtEmployeeFeedback", Language.getMessage(mstrModuleName, 17, "Employee Feedback"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeFeedback"), "txtAnswer", Language.getMessage(mstrModuleName, 13, "Answer :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeFeedback"), "txtJustify", Language.getMessage(mstrModuleName, 14, "Justify :"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptLineManagerFeedback"), "txtManagerFeedback", Language.getMessage(mstrModuleName, 18, "Manager Feedback"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLineManagerFeedback"), "txtAnswer", Language.getMessage(mstrModuleName, 13, "Answer :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLineManagerFeedback"), "txtJustify", Language.getMessage(mstrModuleName, 14, "Justify :"))

            Call ReportFunction.TextChange(objRpt, "txtSignManager", Language.getMessage(mstrModuleName, 15, "Signature of Manager :"))
            Call ReportFunction.TextChange(objRpt, "txtSignEmployee", Language.getMessage(mstrModuleName, 16, "Signature of Employee :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_DetailReport", mstrModuleName)
            Return Nothing
        Finally
            objEvalAnswer = Nothing
        End Try
    End Function
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage("clseval_group_master", 2, "Pre-Training")
			Language.setMessage("clseval_group_master", 3, "Post-Training")
			Language.setMessage("clseval_group_master", 4, "Days after training attended.")
			Language.setMessage(mstrModuleName, 1, "Employee's Name :")
			Language.setMessage(mstrModuleName, 2, "Course Name :")
			Language.setMessage(mstrModuleName, 3, "Line Manager's Name :")
			Language.setMessage(mstrModuleName, 4, "Course Date :")
			Language.setMessage(mstrModuleName, 5, "Date Form was Completed :")
			Language.setMessage(mstrModuleName, 6, "Printed By :")
			Language.setMessage(mstrModuleName, 7, "Printed Date :")
			Language.setMessage(mstrModuleName, 8, "Calendar :")
			Language.setMessage(mstrModuleName, 9, "FeedbackMode :")
			Language.setMessage(mstrModuleName, 10, "Training :")
			Language.setMessage(mstrModuleName, 12, "Order By :")
			Language.setMessage(mstrModuleName, 13, "Answer :")
			Language.setMessage(mstrModuleName, 14, "Justify :")
			Language.setMessage(mstrModuleName, 15, "Signature of Manager :")
			Language.setMessage(mstrModuleName, 16, "Signature of Employee :")
			Language.setMessage(mstrModuleName, 17, "Employee Feedback")
			Language.setMessage(mstrModuleName, 18, "Line Manager Feedback")
			
		Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
