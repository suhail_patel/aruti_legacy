﻿'************************************************************************************************************************************
'Class Name : frmTrainingPriorityReport.vb
'Purpose    : 
'Written By : sOHAIL
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTrainingPriorityReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTrainingPriorityReport"
    Private objTPriorityReport As clsTrainingPriorityReport
    Private mintFirstOpenPeriod As Integer = 0

#End Region

#Region " Constructor "

    Public Sub New()
        objTPriorityReport = New clsTrainingPriorityReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objTPriorityReport.SetDefaultValue()
        InitializeComponent()

        '_Show_ExcelExtra_Menu = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objTPeriod As New clsTraining_Calendar_Master
        Dim objTPriority As New clsTraining_Priority_Master
        Dim objCommon As New clsCommon_Master
        Dim dsCombo As New DataSet

        Try

            mintFirstOpenPeriod = 0
            dsCombo = objTPeriod.getListForCombo("List", False, 1)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                mintFirstOpenPeriod = CInt(dsCombo.Tables(0).Rows(0).Item("calendarunkid"))
            End If
            dsCombo = objTPeriod.getListForCombo("List", True, 0)
            With cboPeriod
                .DisplayMember = "name"
                .ValueMember = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = mintFirstOpenPeriod.ToString
            End With

            dsCombo = objTPriority.getListForCombo("List", True)
            With cboTrainingPriority
                .ValueMember = "trpriorityunkid"
                .DisplayMember = "priority"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingCourse
                .DisplayMember = "name"
                .ValueMember = "masterunkid"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objTPeriod = Nothing
            objTPriority = Nothing
            objCommon = Nothing
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Training Calendar."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False
            End If

            'Sohail (28 May 2021) -- Start
            'NMB Enhancement : : Start data and end date filter on Training Priority Report.
            If dtpStartDate.Checked OrElse dtpEndDate.Checked Then
                Dim oPeriod As New clsTraining_Calendar_Master
                oPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)

                If dtpStartDate.Checked = True Then

                    If dtpStartDate.Value.Date < oPeriod._StartDate OrElse dtpStartDate.Value.Date > oPeriod._EndDate Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Start Date must be between start date and end date of selected training calendar."), enMsgBoxStyle.Information)
                        dtpStartDate.Select()
                        Return False
                    End If

                End If

                If dtpEndDate.Checked = True Then
                    If dtpEndDate.Value.Date < oPeriod._StartDate OrElse dtpEndDate.Value.Date > oPeriod._EndDate Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "End Date must be between start date and end date of selected training calendar."), enMsgBoxStyle.Information)
                        dtpEndDate.Select()
                        Return False
                    End If
                End If

                oPeriod = Nothing
            End If
            'Sohail (28 May 2021) -- End

            objTPriorityReport.SetDefaultValue()

            objTPriorityReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objTPriorityReport._PeriodName = cboPeriod.Text

            objTPriorityReport._PriorityId = CInt(cboTrainingPriority.SelectedValue)
            objTPriorityReport._PriorityName = cboTrainingPriority.Text

            objTPriorityReport._TrainingCourseID = CInt(cboTrainingCourse.SelectedValue)
            objTPriorityReport._TrainingCourseName = cboTrainingCourse.Text

            'Sohail (28 May 2021) -- Start
            'NMB Enhancement : : Start data and end date filter on Training Priority Report.
            If dtpStartDate.Checked Then
                objTPriorityReport._StartDate = dtpStartDate.Value.Date
            End If

            If dtpEndDate.Checked Then
                objTPriorityReport._EndDate = dtpEndDate.Value.Date
            End If
            'Sohail (28 May 2021) -- End

            Dim objPeriod As New clsTraining_Calendar_Master
            objPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)
            objTPriorityReport._PeriodStartDate = objPeriod._StartDate
            objTPriorityReport._PeriodEndDate = objPeriod._EndDate

            objPeriod = Nothing



            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objTPriorityReport.setDefaultOrderBy(0)
            txtOrderBy.Text = objTPriorityReport.OrderByDisplay

            cboTrainingCourse.SelectedValue = "0"
            cboTrainingPriority.SelectedValue = "0"
            'Sohail (28 May 2021) -- Start
            'NMB Enhancement : : Start data and end date filter on Training Priority Report.
            dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpEndDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            'Sohail (28 May 2021) -- End

            cboPeriod.SelectedValue = mintFirstOpenPeriod


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmTrainingPriorityReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTPriorityReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingPriorityReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingPriorityReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Me._Title = objTPriorityReport._ReportName
            Me._Message = objTPriorityReport._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDSpreadSheetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingPriorityReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmTrainingPriorityReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons"

    Private Sub frmTrainingPriorityReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If SetFilter() = False Then Exit Sub

            Dim dtPeriodStart As Date
            Dim dtPeriodEnd As Date
            Dim objPeriod As New clsTraining_Calendar_Master
            objPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)
            dtPeriodStart = objPeriod._StartDate
            dtPeriodEnd = objPeriod._EndDate

            objTPriorityReport.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingPriorityReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingPriorityReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If SetFilter() = False Then Exit Sub

            Dim dtPeriodStart As Date
            Dim dtPeriodEnd As Date
            Dim objPeriod As New clsTraining_Calendar_Master
            objPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)
            dtPeriodStart = objPeriod._StartDate
            dtPeriodEnd = objPeriod._EndDate

            objTPriorityReport.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                          dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingPriorityReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingPriorityReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingPriorityReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingPriorityReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingPriorityReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub


    Private Sub frmTrainingPriorityReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTrainingPriorityReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTrainingPriorityReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmTrainingPriorityReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub



#End Region

#Region "Control"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objTPriorityReport.setOrderBy(0)
            txtOrderBy.Text = objTPriorityReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ComboBox Event"

    Private Sub cboPeriod_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPeriod.KeyPress, cboTrainingPriority.KeyPress, cboTrainingCourse.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboPeriod.Name Then
                        .CodeMember = "name"
                    ElseIf cbo.Name = cboTrainingPriority.Name Then
                        .CodeMember = "priority"
                    Else
                        .CodeMember = "name"
                    End If
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

    

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblTrainingPriority.Text = Language._Object.getCaption(Me.lblTrainingPriority.Name, Me.lblTrainingPriority.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblTrainingCourse.Text = Language._Object.getCaption(Me.lblTrainingCourse.Name, Me.lblTrainingCourse.Text)
			Me.LblEndDate.Text = Language._Object.getCaption(Me.LblEndDate.Name, Me.LblEndDate.Text)
			Me.LblStartDate.Text = Language._Object.getCaption(Me.LblStartDate.Name, Me.LblStartDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Training Calendar.")
			Language.setMessage(mstrModuleName, 2, "Start Date must be between start date and end date of selected training calendar.")
			Language.setMessage(mstrModuleName, 3, "End Date must be between start date and end date of selected training calendar.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class