﻿'************************************************************************************************************************************
'Class Name : clsTalentRatioOfActiveStaffReport.vb
'Purpose    :
'Date       : 22-Mar-2021
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

Public Class clsTalentRatioOfActiveStaffReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsTalentRatioOfActiveStaffReport"
    Private mstrReportId As String = enArutiReport.Talent_Ratio_Of_Active_Staff_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintCycleUnkid As Integer = 0
    Private mstrCycleName As String = ""
    Private mdtAsOnDate As DateTime = Nothing
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvance_Filter As String = String.Empty



#End Region

#Region " Properties "

    Public WriteOnly Property _CycleUnkid() As Integer
        Set(ByVal value As Integer)
            mintCycleUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CycleName() As String
        Set(ByVal value As String)
            mstrCycleName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _DateAsOn() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintCycleUnkid = 0
            mstrCycleName = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterRetiredTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintViewIndex > 0 Then
                Me._FilterQuery &= " ORDER BY GName," & Me.OrderByQuery
            Else
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterRetiredTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           )


        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable
       
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty


        Try

            dtFinalTable = New DataTable("Talent")

            dtCol = New DataColumn("TotalTalentsInPipeline", System.Type.GetType("System.Int32"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Total Talents in Pipeline")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("TotalEmployeeCount", System.Type.GetType("System.Int32"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Total Employee Count")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            objDataOperation = New clsDataOperation

            Dim TotalTalentsInPipeline As Integer = 0
            Dim TotalEmployeeCount As Integer = 0

            TotalEmployeeCount = GetActiveEmployeeCount(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "EmployeeCount")
            TotalTalentsInPipeline = GetApprovedTalentCount(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintCycleUnkid, "EmployeeCount")

            StrQ = " select " & TotalTalentsInPipeline & " TotalTalentsInPipeline , " & TotalEmployeeCount & " TotalEmployeeCount "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim strCurrentEmp As String = ""
            Dim strPreviousEmp As String = ""
            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim iCnt As Integer = 0
            Dim intRowCount As Integer = dsList.Tables("List").Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables("List").Rows(ii)

                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("TotalTalentsInPipeline") = drRow.Item("TotalTalentsInPipeline")

                rpt_Row.Item("TotalEmployeeCount") = drRow.Item("TotalEmployeeCount")

                'If mintViewIndex > 0 Then
                '    rpt_Row.Item("GName") = drRow.Item("GName").ToString()
                'End If
                dtFinalTable.Rows.Add(rpt_Row)

            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "Cycle :") & " " & mstrCycleName, "s10bw")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)
            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = {200, 200, 200, 100}
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, "", "", " ", Nothing, "", False, rowsArrayHeader)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally


        End Try
    End Sub

    Public Function GetActiveEmployeeCount(ByVal xDatabaseName As String, _
                                    ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xPeriodStart As DateTime, _
                                    ByVal xPeriodEnd As DateTime, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal xOnlyApproved As Boolean, _
                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                    Optional ByVal strListName As String = "List", _
                                    Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                    Optional ByVal strFilterQuery As String = "", _
                                    Optional ByVal blnReinstatementDate As Boolean = False, _
                                    Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                    Optional ByVal blnAddApprovalCondition As Boolean = True) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If


            strQ += "SELECT " & _
                    " hremployee_master.employeeunkid As employeeunkid " & _
                    "FROM " & strDBName & "hremployee_master "


            strQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "		 gradegroupunkid " & _
                    "		,gradeunkid " & _
                    "		,gradelevelunkid " & _
                    "		,employeeunkid " & _
                    "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                    "	FROM " & strDBName & "prsalaryincrement_tran " & _
                    "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hremployee_shift_tran.employeeunkid " & _
                    "       ,hremployee_shift_tran.shiftunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS Rno " & _
                    "   FROM " & strDBName & "hremployee_shift_tran " & _
                    "   WHERE hremployee_shift_tran.isvoid = 0 " & _
                    "   AND hremployee_shift_tran.effectivedate IS NOT NULL AND CONVERT(NVARCHAR(8),hremployee_shift_tran.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.Rno = 1 "



            '********************** DATA FOR DATES CONDITION ************************' --- START
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            'strQ &= mstrAnalysis_Join


            

            strQ &= " WHERE 1=1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If


            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If IsNothing(dsList) = False AndAlso dsList.Tables(strListName).Rows.Count > 0 Then
                Return dsList.Tables(strListName).Rows.Count
            Else
                Return 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetActiveEmployeeCount", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing
        End Try
    End Function

    Public Function GetTalentPipelineCount(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal xCycleUnkid As Integer, _
                                     Optional ByVal strListName As String = "List", _
                                      Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                     Optional ByVal strFilterQuery As String = "", _
                                     Optional ByVal blnReinstatementDate As Boolean = False, _
                                     Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                     Optional ByVal blnAddApprovalCondition As Boolean = True) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim objtlsetting As New clstlsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim objPotentialTalentTran As New clsPotentialTalent_Tran
        Dim mdicSetting As New Dictionary(Of clstlsettings_master.enTalentConfiguration, String)

        Try
            mdicSetting = objtlsetting.GetSettingFromPeriod(xCycleUnkid)

            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)

            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If


            strQ &= "DECLARE @UserId AS INT;SET @UserId = @Uid "



            strQ += "SELECT " & _
                      "A.* " & _
                      ",ISNULL(Emp_suspension.suspensiondays, 0) AS suspensiondays " & _
                      ",CASE WHEN ISNULL(D.employeeunkid,0) > 0 THEN 1 ELSE 0 END AS IsDone " & _
                      ",'#FFFFFF' as color " & _
                    "FROM " & _
                    "( " & _
                    "SELECT " & _
                    "   hremployee_master.employeecode AS employeecode " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                    ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                    ",  hrjob_master.job_name " & _
                    ",  (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 AS age " & _
                    ",  0 AS exyr" & _
                    ", hrdepartment_master.name AS department " & _
                    ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.LEAVING, 112), '') AS termination_from_date " & _
                    ", ISNULL(CONVERT(CHAR(8), RET.RETIRE, 112), '') AS termination_to_date " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.EOC, 112), '') AS empl_enddate " & _
                    ",  0 as processmstunkid" & _
                    ", 0 as IsManual " & _
                    ", 0 as isdisapproved " & _
                        ", 0 as potentialtalenttranunkid " & _
                    "  FROM " & strDBName & "hremployee_master "



            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If


            '********************** DATA FOR DATES CONDITION ************************' --- START
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQ &= mstrAnalysis_Join

            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If


            '********************** Talent Seeting Conditions ************************' --- START

            strQ &= "JOIN (SELECT " & _
                    "emp_transfer.employeeunkid " & _
                    ",stationunkid " & _
                    ",deptgroupunkid " & _
                    ",departmentunkid " & _
                    ",sectiongroupunkid " & _
                    ",sectionunkid " & _
                    ",unitgroupunkid " & _
                    ",unitunkid " & _
                    ",teamunkid " & _
                    ",classgroupunkid " & _
                    ",classunkid " & _
                    ",emp_categorization.jobgroupunkid " & _
                    ",emp_categorization.jobunkid " & _
                    ",Emp_CCT.costcenterunkid " & _
                 "FROM (SELECT " & _
                           "Emp_TT.employeeunkid AS TrfEmpId " & _
                         ",ISNULL(Emp_TT.stationunkid, 0) AS stationunkid " & _
                         ",ISNULL(Emp_TT.deptgroupunkid, 0) AS deptgroupunkid " & _
                         ",ISNULL(Emp_TT.departmentunkid, 0) AS departmentunkid " & _
                         ",ISNULL(Emp_TT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                         ",ISNULL(Emp_TT.sectionunkid, 0) AS sectionunkid " & _
                         ",ISNULL(Emp_TT.unitgroupunkid, 0) AS unitgroupunkid " & _
                         ",ISNULL(Emp_TT.unitunkid, 0) AS unitunkid " & _
                         ",ISNULL(Emp_TT.teamunkid, 0) AS teamunkid " & _
                         ",ISNULL(Emp_TT.classgroupunkid, 0) AS classgroupunkid " & _
                         ",ISNULL(Emp_TT.classunkid, 0) AS classunkid " & _
                         ",CONVERT(CHAR(8), Emp_TT.effectivedate, 112) AS EfDt " & _
                         ",Emp_TT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_TT.employeeunkid ORDER BY Emp_TT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_transfer_tran AS Emp_TT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS emp_transfer " & _
                 "JOIN (SELECT " & _
                           "Emp_CT.employeeunkid AS CatEmpId " & _
                         ",Emp_CT.jobgroupunkid " & _
                         ",Emp_CT.jobunkid " & _
                         ",CONVERT(CHAR(8), Emp_CT.effectivedate, 112) AS CEfDt " & _
                         ",Emp_CT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CT.employeeunkid ORDER BY Emp_CT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_categorization_tran AS Emp_CT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & ") AS emp_categorization " & _
                      "ON emp_transfer.employeeunkid = emp_categorization.employeeunkid " & _
                 "JOIN (SELECT " & _
                           "Emp_CCT.employeeunkid AS CCTEmpId " & _
                         ",Emp_CCT.cctranheadvalueid AS costcenterunkid " & _
                         ",CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) AS CTEfDt " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CCT.employeeunkid ORDER BY Emp_CCT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_cctranhead_tran AS Emp_CCT " & _
                      "WHERE Emp_CCT.isvoid = 0 " & _
                      "AND Emp_CCT.istransactionhead = 0 " & _
                      "AND CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS Emp_CCT " & _
                      "ON emp_transfer.employeeunkid = Emp_CCT.CCTEmpId " & _
                 "WHERE emp_transfer.Rno = 1 " & _
                 "AND emp_categorization.Rno = 1 " & _
                 "AND emp_CCT.Rno = 1"


            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE) Then
                Dim allocation As String = mdicSetting(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE)
                Dim allocationType As String() = allocation.Split("|")

                Select Case CInt(allocationType(0))

                    Case CInt(enAllocation.BRANCH)
                        strQ &= "AND emp_transfer.stationunkid IN "

                    Case CInt(enAllocation.DEPARTMENT_GROUP)
                        strQ &= "AND emp_transfer.deptgroupunkid IN "

                    Case CInt(enAllocation.DEPARTMENT)
                        strQ &= "AND emp_transfer.departmentunkid IN "

                    Case CInt(enAllocation.SECTION_GROUP)
                        strQ &= "AND emp_transfer.sectiongroupunkid IN "

                    Case CInt(enAllocation.SECTION)
                        strQ &= "AND emp_transfer.sectionunkid IN "

                    Case CInt(enAllocation.UNIT_GROUP)
                        strQ &= "AND emp_transfer.unitgroupunkid IN "

                    Case CInt(enAllocation.UNIT)
                        strQ &= "AND emp_transfer.unitunkid IN "

                    Case CInt(enAllocation.JOB_GROUP)
                        strQ &= "AND emp_categorization.jobgroupunkid IN "

                    Case CInt(enAllocation.JOBS)
                        strQ &= "AND emp_categorization.jobunkid IN "

                    Case CInt(enAllocation.CLASS_GROUP)
                        strQ &= "AND emp_transfer.classgroupunkid IN "

                    Case CInt(enAllocation.CLASSES)
                        strQ &= "AND emp_transfer.classunkid IN "

                    Case CInt(enAllocation.COST_CENTER)
                        strQ &= "AND emp_CCT.costcenterunkid IN  "

                End Select

                strQ &= " (" & allocationType(1) & " ) "

            End If

            strQ &= " ) AS allocation " & _
                 "ON allocation.employeeunkid = hremployee_master.employeeunkid " & _
            "LEFT JOIN hrjob_master ON allocation.jobunkid = hrjob_master.jobunkid " & _
            "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = allocation.departmentunkid "

            strQ &= " WHERE 1=1 "

            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) Then
                strQ &= " and (CAST(CONVERT(CHAR(8), @Date ,112) AS DECIMAL)-CAST(CONVERT(CHAR(8),birthdate,112) AS DECIMAL))/10000 <= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) & " "
            End If

            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) Then
                strQ &= " AND hremployee_master.employeeunkid not in (SELECT " & _
                        " employeeunkid " & _
                        " FROM tlscreening_stages_tran " & _
                        " WHERE isvoid = 0 " & _
                        " AND cycleunkid = " & xCycleUnkid & " " & _
                        " GROUP BY employeeunkid " & _
                        "	HAVING count(employeeunkid) >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) & " ) "
            End If
            '================ Employee ID

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAsOnDate.AddDays(1)))

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If


            strQ &= ") as A " & _
                    "LEFT JOIN (SELECT " & _
                          "sus.EmpId " & _
                        ",sus.suspensiondays " & _
                     "FROM (SELECT " & _
                               "employeeunkid AS EmpId " & _
                               ",SUM(DATEDIFF(DAY, date1,  " & _
                               " CASE " & _
                               "    WHEN CONVERT(CHAR(8), date2, 112) IS NULL THEN CAST(@Date AS DATETIME) " & _
                               "    ELSE CAST(CONVERT(CHAR(8), date2 + 1, 112)AS DATETIME) " & _
                               " End " & _
                                " )) AS suspensiondays " & _
                          "FROM  " & strDBName & "hremployee_dates_tran " & _
                          "WHERE hremployee_dates_tran.datetypeunkid IN (" & enEmp_Dates_Transaction.DT_SUSPENSION & ") " & _
                          "AND isvoid = 0 " & _
                          "AND CONVERT(CHAR(8), date2, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " " & _
                          "GROUP BY hremployee_dates_tran.employeeunkid) AS sus) AS Emp_suspension " & _
                     "ON Emp_suspension.EmpId = A.employeeunkid "

            strQ &= "LEFT JOIN " & _
                      "( " & _
                        "SELECT " & _
                           "tlscreening_process_master.employeeunkid " & _
                          ",tlscreening_process_master.processmstunkid " & _
                        "FROM tlscreener_master AS TSM " & _
                          "JOIN tlscreening_stages_tran ON TSM.cycleunkid = tlscreening_stages_tran.cycleunkid " & _
                            "AND TSM.screenermstunkid = tlscreening_stages_tran.screenermstunkid " & _
                          "JOIN tlscreening_process_master ON tlscreening_stages_tran.processmstunkid = tlscreening_process_master.processmstunkid " & _
                        "WHERE TSM.isvoid = 0 AND TSM.cycleunkid = @cycleunkid and tlscreening_process_master.isvoid = 0 and tlscreening_stages_tran.isvoid= 0 " & _
                          "AND TSM.mapuserunkid = @UserId " & _
                      ") AS D ON A.employeeunkid = D.employeeunkid "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCycleUnkid)
            objDataOperation.AddParameter("@Uid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)


            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim ExpYearNo As String = String.Empty
            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) Then
                ExpYearNo = mdicSetting(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO)
            End If
            If ExpYearNo.Length = 0 Then ExpYearNo = "0"
            If CInt(ExpYearNo) > 0 Then

                Dim strEmpIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray())

                If strEmpIDs.Length > 0 Then


                    Dim dsService As DataSet = objEmpDates.GetEmployeeServiceDays(objDataOperation, xCompanyUnkid, xDatabaseName, strEmpIDs, mdtAsOnDate)

                    Dim dtService = New DataView(dsService.Tables(0), "fromdate <> '' ", "", DataViewRowState.CurrentRows).ToTable

                    Dim result = (From e In dsList.Tables(0).AsEnumerable() Group Join s In dtService.AsEnumerable() On e.Item("employeeunkid") Equals s.Item("employeeunkid") Into es = Group From s In es.DefaultIfEmpty() Order By e.Field(Of Integer)("employeeunkid") _
                                  Select New With { _
                                     Key .employeecode = e.Field(Of String)("employeecode") _
                                    , Key .employeename = e.Field(Of String)("employeename") _
                                    , Key .employeeunkid = e.Field(Of Integer)("employeeunkid") _
                                    , Key .isapproved = e.Field(Of Boolean)("isapproved") _
                                    , Key .EmpCodeName = e.Field(Of String)("EmpCodeName") _
                                    , Key .job_name = e.Field(Of String)("job_name") _
                                    , Key .age = e.Field(Of Integer)("age") _
                                    , Key .exyr = e.Field(Of Integer)("exyr") _
                                    , Key .suspensiondays = e.Field(Of Integer)("suspensiondays") _
                                    , Key .department = e.Field(Of String)("department") _
                                    , Key .appointeddate = e.Field(Of String)("appointeddate") _
                                    , Key .termination_from_date = e.Field(Of String)("termination_from_date") _
                                    , Key .termination_to_date = e.Field(Of String)("termination_to_date") _
                                    , Key .empl_enddate = e.Field(Of String)("empl_enddate") _
                                    , Key .FromDate = If(s Is Nothing, "", s.Field(Of String)("fromdate")) _
                                    , Key .ToDate = If(s Is Nothing, "", s.Field(Of String)("todate")) _
                                    , Key .ServiceDays = If(s Is Nothing, 0, s.Field(Of Integer)("ServiceDays")) _
                                    })

                    Dim intPrevEmp As Integer = 0
                    Dim dblTotDays As Double = 0

                    For Each drow In result

                        Dim StartDate As Date = eZeeDate.convertDate(drow.appointeddate)
                        Dim EndDate As Date = mdtAsOnDate


                        Dim intEmpId As Integer = drow.employeeunkid
                        Dim intCount As Integer = dtService.Select("employeeunkid = " & intEmpId & " ").Length

                        If intCount <= 0 Then 'No Dates record found                       
                            If drow.termination_from_date.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.termination_from_date) < EndDate, eZeeDate.convertDate(drow.termination_from_date), EndDate)
                            End If
                            If drow.termination_to_date.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.termination_to_date) < EndDate, eZeeDate.convertDate(drow.termination_to_date), EndDate)
                            End If
                            If drow.empl_enddate.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.empl_enddate) < EndDate, eZeeDate.convertDate(drow.empl_enddate), EndDate)
                            End If
                            dblTotDays = CDbl(DateDiff(DateInterval.Day, StartDate, EndDate.AddDays(1)))
                        ElseIf intCount = 1 Then
                            dblTotDays = drow.ServiceDays
                        Else
                            dblTotDays = (From p In dtService Where (p.Field(Of Integer)("employeeunkid") = intEmpId) Select (CDbl(p.Item("ServiceDays")))).Sum()
                        End If

                        For Each drRow As DataRow In dsList.Tables(0).Select("employeeunkid = " & intEmpId & " ")
                            drRow.Item("exyr") = (dblTotDays - drow.suspensiondays) / 365
                        Next


                    Next

                    If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) Then
                        dsList.Tables(0).DefaultView.RowFilter = "exyr >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) & " "
                    End If

                End If
            End If 'Hemant (08 Feb 2021)

            'S.SANDEEP |01-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : FILTER ISSUE TALENT & SUCCESSION
            '=========== Performance Score Settings - Start =====================
            'S.SANDEEP |17-FEB-2021| -- START
            'If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO) AndAlso mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.PERF_SCORE) Then
            Dim intPrdNo As Integer = 0
            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ANY_PERIOD) Or mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALL_PERIOD) Then
                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ANY_PERIOD) Then
                    If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO) Then
                        intPrdNo = mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO)
                    Else
                        intPrdNo = 1
                    End If
                ElseIf mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALL_PERIOD) Then
                    intPrdNo = 0
                End If
            End If
            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.PERF_SCORE) Then
                Dim dtPerformanceScoreEmployeeList As DataTable
                Dim objtlpipeline_master As New clstlpipeline_master

                dtPerformanceScoreEmployeeList = objtlpipeline_master.GetPerformanceScoreEmployeeList(xDatabaseName, mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO), mdicSetting(clstlsettings_master.enTalentConfiguration.PERF_SCORE), xCompanyUnkid, xYearUnkid, objDataOperation)
                If dtPerformanceScoreEmployeeList IsNot Nothing AndAlso dtPerformanceScoreEmployeeList.Rows.Count > 0 Then
                    Dim strPerformanceScoreEmpIds As String = String.Join(",", (From p In dtPerformanceScoreEmployeeList Select (p.Item("employeeunkid").ToString)).ToArray())
                    If strPerformanceScoreEmpIds.Length > 0 Then
                        dsList.Tables(0).DefaultView.RowFilter = "employeeunkid in ( " & strPerformanceScoreEmpIds & ")"
                        Dim dtPerformanceScore As DataTable = dsList.Tables(0).DefaultView.ToTable
                        dsList.Tables.Remove(dsList.Tables(0))
                        dsList.Tables.Add(dtPerformanceScore)
                    End If
                Else
                    dsList.Tables(0).Rows.Clear()
                End If
            End If
            '=========== Performance Score Settings - End =====================
            'S.SANDEEP |01-FEB-2021| -- END

            Dim dfView As DataView = dsList.Tables(0).DefaultView
            dfView.Sort = "employeename"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dfView.ToTable)

            '============================ Add Extra Potential Talent Employee(s) -- Start
            Dim strEmployeeList As String = ""
            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
                strEmployeeList = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray())
            End If

            Dim dtPTEmployee As DataTable = objPotentialTalentTran.GetList("List", xCycleUnkid, IIf(strEmployeeList.Length > 0, " AND employeeunkid not in (" & strEmployeeList & ")", "")).Tables(0)
            Dim strPotentialTalentEmployeelist As String = String.Join(",", (From p In dtPTEmployee Select (p.Item("employeeunkid").ToString)).ToArray())
            If strPotentialTalentEmployeelist.Length > 0 Then
                Dim dsPotentialTalent As DataSet
                objDataOperation.ClearParameters()

                strQ = "DECLARE @PTUserId AS INT; " & _
                       " SET @PTUserId = @Uid "


                strQ &= "SELECT " & _
                      "A.* " & _
                      ",CASE WHEN ISNULL(D.employeeunkid,0) > 0 THEN 1 ELSE 0 END AS IsDone " & _
                      ",'#FFFFFF' as color " & _
                    "FROM " & _
                       "( " & _
                        "SELECT " & _
                    "   hremployee_master.employeecode AS employeecode " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                    ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                    ",  hrjob_master.job_name " & _
                    ",  0 AS age " & _
                    ",  0 AS exyr" & _
                    ",  0 suspensiondays " & _
                    ", hrdepartment_master.name AS department " & _
                    ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                    ", '' AS termination_from_date " & _
                    ", '' AS termination_to_date " & _
                    ", '' AS empl_enddate " & _
                        ", 0 as processmstunkid" & _
                    ", 1 as IsManual " & _
                        ", 0 as isdisapproved " & _
                        ", isnull(tlpotentialtalent_tran.potentialtalenttranunkid,0) as potentialtalenttranunkid " & _
                    ",'#FFFFFF' as color " & _
                        "  FROM tlpotentialtalent_tran " & _
                        "  LEFT JOIN  hremployee_master " & _
                        "     ON tlpotentialtalent_tran.employeeunkid = hremployee_master.employeeunkid "


                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If


                If blnIncludeAccessFilterQry = True Then
                    If xUACQry.Trim.Length > 0 Then
                        strQ &= xUACQry
                    End If
                End If


                strQ &= " JOIN (SELECT " & _
                        "emp_transfer.employeeunkid " & _
                        ",stationunkid " & _
                        ",deptgroupunkid " & _
                        ",departmentunkid " & _
                        ",sectiongroupunkid " & _
                        ",sectionunkid " & _
                        ",unitgroupunkid " & _
                        ",unitunkid " & _
                        ",teamunkid " & _
                        ",classgroupunkid " & _
                        ",classunkid " & _
                        ",emp_categorization.jobgroupunkid " & _
                        ",emp_categorization.jobunkid " & _
                        ",Emp_CCT.costcenterunkid " & _
                     "FROM (SELECT " & _
                               "Emp_TT.employeeunkid AS TrfEmpId " & _
                             ",ISNULL(Emp_TT.stationunkid, 0) AS stationunkid " & _
                             ",ISNULL(Emp_TT.deptgroupunkid, 0) AS deptgroupunkid " & _
                             ",ISNULL(Emp_TT.departmentunkid, 0) AS departmentunkid " & _
                             ",ISNULL(Emp_TT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                             ",ISNULL(Emp_TT.sectionunkid, 0) AS sectionunkid " & _
                             ",ISNULL(Emp_TT.unitgroupunkid, 0) AS unitgroupunkid " & _
                             ",ISNULL(Emp_TT.unitunkid, 0) AS unitunkid " & _
                             ",ISNULL(Emp_TT.teamunkid, 0) AS teamunkid " & _
                             ",ISNULL(Emp_TT.classgroupunkid, 0) AS classgroupunkid " & _
                             ",ISNULL(Emp_TT.classunkid, 0) AS classunkid " & _
                             ",CONVERT(CHAR(8), Emp_TT.effectivedate, 112) AS EfDt " & _
                             ",Emp_TT.employeeunkid " & _
                             ",ROW_NUMBER() OVER (PARTITION BY Emp_TT.employeeunkid ORDER BY Emp_TT.effectivedate DESC) AS Rno " & _
                          "FROM  " & strDBName & "hremployee_transfer_tran AS Emp_TT " & _
                          "WHERE isvoid = 0 " & _
                          "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS emp_transfer " & _
                     "JOIN (SELECT " & _
                               "Emp_CT.employeeunkid AS CatEmpId " & _
                             ",Emp_CT.jobgroupunkid " & _
                             ",Emp_CT.jobunkid " & _
                             ",CONVERT(CHAR(8), Emp_CT.effectivedate, 112) AS CEfDt " & _
                             ",Emp_CT.employeeunkid " & _
                             ",ROW_NUMBER() OVER (PARTITION BY Emp_CT.employeeunkid ORDER BY Emp_CT.effectivedate DESC) AS Rno " & _
                          "FROM  " & strDBName & "hremployee_categorization_tran AS Emp_CT " & _
                          "WHERE isvoid = 0 " & _
                          "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & ") AS emp_categorization " & _
                          "ON emp_transfer.employeeunkid = emp_categorization.employeeunkid " & _
                     "JOIN (SELECT " & _
                               "Emp_CCT.employeeunkid AS CCTEmpId " & _
                             ",Emp_CCT.cctranheadvalueid AS costcenterunkid " & _
                             ",CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) AS CTEfDt " & _
                             ",ROW_NUMBER() OVER (PARTITION BY Emp_CCT.employeeunkid ORDER BY Emp_CCT.effectivedate DESC) AS Rno " & _
                          "FROM  " & strDBName & "hremployee_cctranhead_tran AS Emp_CCT " & _
                          "WHERE Emp_CCT.isvoid = 0 " & _
                          "AND Emp_CCT.istransactionhead = 0 " & _
                          "AND CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS Emp_CCT " & _
                          "ON emp_transfer.employeeunkid = Emp_CCT.CCTEmpId " & _
                     "WHERE emp_transfer.Rno = 1 " & _
                     "AND emp_categorization.Rno = 1 " & _
                     "AND emp_CCT.Rno = 1"

                strQ &= " ) AS allocation " & _
                    "ON allocation.employeeunkid = hremployee_master.employeeunkid " & _
               "LEFT JOIN hrjob_master ON allocation.jobunkid = hrjob_master.jobunkid " & _
               "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = allocation.departmentunkid "

                strQ &= " WHERE 1=1 and tlpotentialtalent_tran.cycleunkid = @cycleunkid and tlpotentialtalent_tran.isvoid = 0 "

                strQ &= " and hremployee_master.employeeunkid in (" & strPotentialTalentEmployeelist & ") "

                If xIncludeIn_ActiveEmployee = False Then
                    If xDataFilterQry.Trim.Length > 0 Then
                        strQ &= xDataFilterQry
                    End If
                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
                Else
                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                End If


                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) Then
                    strQ &= " AND hremployee_master.employeeunkid not in (SELECT " & _
                            " employeeunkid " & _
                            " FROM tlscreening_stages_tran " & _
                            " WHERE isvoid = 0 " & _
                            " AND cycleunkid = " & xCycleUnkid & " " & _
                            " GROUP BY employeeunkid " & _
                                "	HAVING count(employeeunkid) >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) & " ) "
                End If



                If strFilterQuery.Trim <> "" Then
                    strQ &= " AND " & strFilterQuery & " "
                End If

                strQ &= " ) as A " & _
                      "LEFT JOIN " & _
                      "( " & _
                        "SELECT " & _
                           "tlscreening_process_master.employeeunkid " & _
                          ",tlscreening_process_master.processmstunkid " & _
                        "FROM tlscreener_master AS TSM " & _
                          "JOIN tlscreening_stages_tran ON TSM.cycleunkid = tlscreening_stages_tran.cycleunkid " & _
                            "AND TSM.screenermstunkid = tlscreening_stages_tran.screenermstunkid " & _
                          "JOIN tlscreening_process_master ON tlscreening_stages_tran.processmstunkid = tlscreening_process_master.processmstunkid " & _
                        "WHERE TSM.isvoid = 0 AND TSM.cycleunkid = @cycleunkid and tlscreening_process_master.isvoid = 0 and tlscreening_stages_tran.isvoid= 0 " & _
                          "AND TSM.mapuserunkid = @PTUserId " & _
                      ") AS D ON A.employeeunkid = D.employeeunkid "


                objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCycleUnkid)
                objDataOperation.AddParameter("@Uid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)

                dsPotentialTalent = objDataOperation.ExecQuery(strQ, strListName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                dsList.Merge(dsPotentialTalent.Tables(0))
            End If

            '============================ Add Extra Potential Talent Employee(s) -- End


            If IsNothing(dsList) = False AndAlso dsList.Tables(strListName).Rows.Count > 0 Then
                Return dsList.Tables(strListName).Rows.Count
            Else
                Return 0
            End If



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetTalentPipelineCount", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function

    Public Function GetApprovedTalentCount(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal xCycleUnkid As Integer, _
                                     Optional ByVal strListName As String = "List", _
                                     Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                     Optional ByVal strFilterQuery As String = "", _
                                     Optional ByVal blnReinstatementDate As Boolean = False, _
                                     Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                     Optional ByVal blnAddApprovalCondition As Boolean = True) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim objtlsetting As New clstlsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim mdicSetting As New Dictionary(Of clstlsettings_master.enTalentConfiguration, String)

        Dim objtlcycle_master As New clstlstages_master
        Dim intMinOrder, intMaxOrder, intMaxTolastOrder As Integer
        objtlcycle_master.Get_Min_Max_FlowOrder(intMinOrder, intMaxOrder, intMaxTolastOrder, xCycleUnkid)

        Try
            mdicSetting = objtlsetting.GetSettingFromPeriod(xCycleUnkid)

            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)

            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If


            strQ &= " SELECT	* INTO #TotScr " & _
                    " FROM (SELECT " & _
                    "       employeeunkid " & _
                    "      ,COUNT(*) AS TotalScreener " & _
                    "      FROM tlscreening_stages_tran " & _
                    "      WHERE isvoid = 0 " & _
                    "     AND cycleunkid = " & xCycleUnkid & " " & _
                    " GROUP BY employeeunkid) AS A "

            strQ &= "SELECT " & _
                         "* INTO #TotScore " & _
                    "FROM (SELECT " & _
                              "processmstunkid " & _
                            ",SUM(result) AS TotalScore " & _
                         "FROM tlscreening_process_tran " & _
                         "WHERE isvoid = 0 " & _
                         "GROUP BY processmstunkid) AS B "

            strQ &= "SELECT " & _
                    "   hremployee_master.employeecode AS employeecode " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                    ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                    ",  (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 AS age " & _
                    ",  0 AS exyr" & _
                    ",  ISNULL(#TotScr.TotalScreener, 0) AS TotalScreener " & _
                    ",  tlscreening_process_master.stageunkid AS stageunkid " & _
                    ",  ISNULL(tlscreening_process_master.processmstunkid,0) as processmstunkid" & _
                    ", tlscreening_process_master.isdisapproved " & _
                    ", 0 AS IsDone " & _
                    ", tlscreening_process_master.ismatch " & _
                    ",tlratings_master.color as color " & _
                    ",case WHEN ISNULL(tlpotentialtalent_tran.potentialtalenttranunkid,0) > 0 THEN 1 ELSE 0 END as IsManual " & _
                    ",case WHEN ISNULL(tlpotentialtalent_tran.potentialtalenttranunkid,0) > 0 THEN tlpotentialtalent_tran.potentialtalenttranunkid ELSE 0 END as potentialtalenttranunkid "


            strQ &= " FROM " & strDBName & "..tlscreening_process_master " & _
                    " LEFT JOIN hremployee_master	ON hremployee_master.employeeunkid = tlscreening_process_master.employeeunkid " & _
                    " and tlscreening_process_master.cycleunkid = " & xCycleUnkid & " " & _
                    " and tlscreening_process_master.isvoid = 0 " & _
                    "LEFT JOIN #TotScore " & _
                    "   ON #TotScore.processmstunkid = tlscreening_process_master.processmstunkid " & _
                    "LEFT JOIN tlratings_master " & _
                    "   ON tlratings_master.stageunkid = tlscreening_process_master.stageunkid " & _
                    "   AND tlratings_master.isvoid = 0 " & _
                    "   AND tlscreening_process_master.isvoid = 0 " & _
                    " LEFT JOIN #TotScr ON #TotScr.employeeunkid = tlscreening_process_master.employeeunkid "

            strQ &= " LEFT JOIN tlpotentialtalent_tran " & _
                    "   ON tlscreening_process_master.employeeunkid = tlpotentialtalent_tran.employeeunkid " & _
                    "   AND tlscreening_process_master.cycleunkid = tlpotentialtalent_tran.cycleunkid "


           

            strQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "		 gradegroupunkid " & _
                    "		,gradeunkid " & _
                    "		,gradelevelunkid " & _
                    "		,employeeunkid " & _
                    "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                    "	FROM " & strDBName & "prsalaryincrement_tran " & _
                    "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hremployee_shift_tran.employeeunkid " & _
                    "       ,hremployee_shift_tran.shiftunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS Rno " & _
                    "   FROM " & strDBName & "hremployee_shift_tran " & _
                    "   WHERE hremployee_shift_tran.isvoid = 0 " & _
                    "   AND hremployee_shift_tran.effectivedate IS NOT NULL AND CONVERT(NVARCHAR(8),hremployee_shift_tran.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.Rno = 1 "



            '********************** DATA FOR DATES CONDITION ************************' --- START
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            'strQ &= mstrAnalysis_Join

            strQ &= " WHERE 1=1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            strQ &= " and tlscreening_process_master.ismatch = 1 and tlscreening_process_master.isvoid = 0 " & _
                    " and tlscreening_process_master.isapproved = 1 "

            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ) Then
                strQ &= " AND #TotScr.totalscreener >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ) & " "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAsOnDate.AddDays(1)))

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If

            strQ &= " AND ISNULL(#TotScore.TotalScore,0) / isnull(#TotScr.TotalScreener,0) BETWEEN tlratings_master.scorefrom AND tlratings_master.scoreto " & _
                    " DROP TABLE #TotScr " & _
                    " DROP TABLE #TotScore "


            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsNothing(dsList) = False AndAlso dsList.Tables(strListName).Rows.Count > 0 Then
                Return dsList.Tables(strListName).Rows.Count
            Else
                Return 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApprovedTalentCount", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Calendar :")
            Language.setMessage(mstrModuleName, 2, "Total Employee Count")
            Language.setMessage(mstrModuleName, 5, "Total Talents in Pipeline")
            Language.setMessage(mstrModuleName, 6, "Prepared By :")
            Language.setMessage(mstrModuleName, 7, "Checked By :")
            Language.setMessage(mstrModuleName, 8, "Approved By")
            Language.setMessage(mstrModuleName, 9, "Received By :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
