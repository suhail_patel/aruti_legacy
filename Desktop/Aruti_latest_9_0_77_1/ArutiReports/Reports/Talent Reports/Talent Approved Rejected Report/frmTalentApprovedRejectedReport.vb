﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmTalentApprovedRejectedReport
#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTalentApprovedRejectedReport"
    Private objTalentApprovedRejectedReport As clsTalentApprovedRejectedReport

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty
    Private dvEmployee As DataView
    Private mstrSearchEmpText As String = ""
    Dim strEmployeeId As String = ""
#End Region

#Region " Constructor "
    Public Sub New()
        objTalentApprovedRejectedReport = New clsTalentApprovedRejectedReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objTalentApprovedRejectedReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmTalentApprovedRejectedReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTalentApprovedRejectedReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentApprovedRejectedReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTalentApprovedRejectedReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Call Language.setLanguage(Me.Name)
            'OtherSettings()
            Call FillCombo()
            Call ResetValue()
            objbtnSearchEmployee.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentApprovedRejectedReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTalentApprovedRejectedReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentApprovedRejectedReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTalentApprovedRejectedReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentApprovedRejectedReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTalentApprovedRejectedReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTalentApprovedRejectedReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCycle As New clstlcycle_master

        Try
            dsCombo = objCycle.getListForCombo(CInt(FinancialYear._Object._YearUnkid), FinancialYear._Object._DatabaseName, FinancialYear._Object._Financial_Start_Date, "List", True, enStatusType.OPEN)
            With cboCycle
                .ValueMember = "cycleunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCycle = Nothing

        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboCycle.SelectedIndex = 0
            chkIncludeInactiveEmp.Checked = False
            If cboEmployee.DataSource IsNot Nothing AndAlso cboEmployee.Items.Count > 0 Then cboEmployee.SelectedIndex = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            objTalentApprovedRejectedReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Dim objCycle As New clstlcycle_master
        Try
            objTalentApprovedRejectedReport.SetDefaultValue()


            objTalentApprovedRejectedReport._CycleUnkid = cboCycle.SelectedValue
            objTalentApprovedRejectedReport._CycleName = cboCycle.Text

            objCycle._Cycleunkid = CInt(cboCycle.SelectedValue)
            objTalentApprovedRejectedReport._DateAsOn = CDate(objCycle._Start_Date)

            objTalentApprovedRejectedReport._CycleName = cboCycle.Text
            objTalentApprovedRejectedReport._EmployeeUnkid = cboEmployee.SelectedValue
            objTalentApprovedRejectedReport._EmployeeName = cboEmployee.Text
            objTalentApprovedRejectedReport._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            objTalentApprovedRejectedReport._ViewByIds = mstrStringIds
            objTalentApprovedRejectedReport._ViewIndex = mintViewIdx
            objTalentApprovedRejectedReport._ViewByName = mstrStringName
            objTalentApprovedRejectedReport._Analysis_Fields = mstrAnalysis_Fields
            objTalentApprovedRejectedReport._Analysis_Join = mstrAnalysis_Join
            objTalentApprovedRejectedReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTalentApprovedRejectedReport._Report_GroupName = mstrReport_GroupName
            objTalentApprovedRejectedReport._Advance_Filter = mstrAdvanceFilter
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try

            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If

            Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If CInt(cboCycle.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Cycle."), enMsgBoxStyle.Information)
                cboCycle.Focus()
                Exit Sub
            End If

            If SetFilter() = False Then Exit Sub

            objTalentApprovedRejectedReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport, _
                                                                0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Combobox Events"
    Private Sub cboCycle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCycle.SelectedIndexChanged
        Dim objCycle As New clstlcycle_master
        Dim objtlpipeline_master As New clstlpipeline_master
        Dim objStage As New clstlstages_master
        Dim dsList As New DataSet
        Dim dsCombo As New DataSet
        Try
            If CInt(cboCycle.SelectedValue) > 0 Then

                Dim FilterType As enScreeningFilterType = enScreeningFilterType.ALL

                objCycle._Cycleunkid = CInt(cboCycle.SelectedValue)
                objtlpipeline_master._DateAsOn = CDate(objCycle._Start_Date)

                dsCombo = objtlpipeline_master.GetAllPipeLineEmployeeList(FinancialYear._Object._DatabaseName, _
                                                                          User._Object._Userunkid, _
                                                                          FinancialYear._Object._YearUnkid, _
                                                                          Company._Object._Companyunkid, _
                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                          ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboCycle.SelectedValue), _
                                                                          "", -1, FilterType, "Emp", _
                                                                          False, mstrAdvanceFilter, True)
                Dim r As DataRow = dsCombo.Tables(0).NewRow

                r.Item(1) = "Select" ' 
                r.Item(2) = "0"

                dsCombo.Tables(0).Rows.InsertAt(r, 0)

                With cboEmployee
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "employeename"
                    .DataSource = dsCombo.Tables("Emp")
                    .SelectedValue = 0
                End With
                objbtnSearchEmployee.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCycle_SelectedIndexChanged", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objtlpipeline_master = Nothing
            objStage = Nothing
            objCycle = Nothing
        End Try
    End Sub
#End Region

#Region " Other Control's Events "

    'Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
    '    Dim frm As New frmViewAnalysis
    '    Try
    '        frm.displayDialog()
    '        mstrStringIds = frm._ReportBy_Ids
    '        mstrStringName = frm._ReportBy_Name
    '        mintViewIdx = frm._ViewIndex
    '        mstrAnalysis_Fields = frm._Analysis_Fields
    '        mstrAnalysis_Join = frm._Analysis_Join
    '        mstrAnalysis_OrderBy = frm._Analysis_OrderBy
    '        mstrReport_GroupName = frm._Report_GroupName
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
    '    Finally
    '        frm = Nothing
    '    End Try
    'End Sub

    Private Sub lnkAdvanceFilter_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    Private Sub objbtnSearchCycle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCycle.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboCycle.DataSource
            frm.ValueMember = cboCycle.ValueMember
            frm.DisplayMember = cboCycle.DisplayMember
            If frm.DisplayDialog Then
                cboCycle.SelectedValue = frm.SelectedValue
                cboCycle.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblCycle.Text = Language._Object.getCaption(Me.lblCycle.Name, Me.lblCycle.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select Cycle.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class