﻿'Class Name : clsTalentExitsReport.vb
'Purpose    :
'Date       : 23-Mar-2021
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsTalentExitsReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsTalentExitsReport"
    Private mstrReportId As String = enArutiReport.Talent_Exits_Report
    Private objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintCycleUnkid As Integer = 0
    Private mstrCycleName As String = ""
    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mdtExitDateFrom As Date = Nothing
    Private mdtExitDateTo As Date = Nothing
    Private mdtAsOnDate As DateTime = Nothing
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvance_Filter As String = String.Empty
#End Region

#Region " Properties "

    Public WriteOnly Property _CycleUnkid() As Integer
        Set(ByVal value As Integer)
            mintCycleUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CycleName() As String
        Set(ByVal value As String)
            mstrCycleName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ExitDateFrom() As Date
        Set(ByVal value As Date)
            mdtExitDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ExitDateTo() As Date
        Set(ByVal value As Date)
            mdtExitDateTo = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _DateAsOn() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintCycleUnkid = 0
            mstrCycleName = ""
            mintEmployeeUnkid = 0
            mstrEmployeeName = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeUnkid > 0 Then
                objDataOperation.AddParameter("@EmployeeUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid)
                Me._FilterQuery &= " AND hremployee_master.EmployeeUnkid = @EmployeeUnkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mdtExitDateFrom <> Nothing AndAlso mdtExitDateTo <> Nothing Then
                objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtExitDateFrom))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),TRM.LEAVING,112) >= @FromDate "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "From Date :") & " " & mdtExitDateFrom & " "

                objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtExitDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),TRM.LEAVING,112) <= @ToDate "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "To Date :") & " " & mdtExitDateTo & " "
            End If


          
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Function GetApprovedTalentList(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal xCycleUnkid As Integer, _
                                     Optional ByVal xEmployeeunkid As Integer = -1, _
                                     Optional ByVal xScreeningType As enScreeningFilterType = enScreeningFilterType.ALL, _
                                     Optional ByVal strListName As String = "List", _
                                     Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                     Optional ByVal strFilterQuery As String = "", _
                                     Optional ByVal blnReinstatementDate As Boolean = False, _
                                     Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                     Optional ByVal blnAddApprovalCondition As Boolean = True _
                                     ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objtlsetting As New clstlsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim mdicSetting As New Dictionary(Of clstlsettings_master.enTalentConfiguration, String)

        Dim objtlcycle_master As New clstlstages_master
        Dim intMinOrder, intMaxOrder, intMaxTolastOrder As Integer
        objtlcycle_master.Get_Min_Max_FlowOrder(intMinOrder, intMaxOrder, intMaxTolastOrder, xCycleUnkid)

        Try
            mdicSetting = objtlsetting.GetSettingFromPeriod(xCycleUnkid)
            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If

            strQ &= " SELECT * INTO #TotScr " & _
                    " FROM (SELECT " & _
                    "       employeeunkid " & _
                    "      ,COUNT(*) AS TotalScreener " & _
                    "      FROM tlscreening_stages_tran " & _
                    "      WHERE isvoid = 0 " & _
                    "     AND cycleunkid = " & xCycleUnkid & " " & _
                    " GROUP BY employeeunkid) AS A "

            strQ &= "SELECT " & _
                         "* INTO #TotScore " & _
                    "FROM (SELECT " & _
                              "processmstunkid " & _
                            ",SUM(result) AS TotalScore " & _
                         "FROM tlscreening_process_tran " & _
                         "WHERE isvoid = 0 " & _
                         "GROUP BY processmstunkid) AS B "

            strQ &= "SELECT " & _
                    "   hremployee_master.employeecode AS employeecode " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                    ",  ISNULL(employee_department.name, '') AS EmployeeDepartment " & _
                    ",  ISNULL(employee_section_grp.name, '') AS EmployeeSectionGrp " & _
                    ",  ISNULL(employee_class_grp.name, '') AS EmployeeClassGrp " & _
                    ",  ISNULL(employee_class.name, '') AS EmployeeClass " & _
                    ",  ISNULL(employee_grade_grp.name, '') AS EmployeeGraderGrp " & _
                    ",  ISNULL(employee_job.job_name, '') AS EmployeeJob " & _
                    ",  ISNULL(#TotScr.TotalScreener, 0) AS TotalScreener " & _
                    ",  tlscreening_process_master.stageunkid AS stageunkid " & _
                    ",  ISNULL(tlscreening_process_master.processmstunkid,0) as processmstunkid" & _
                    ",  TE.LEAVING AS ExitDate " & _
                    ",  TR.name as Reason " & _
                    " FROM " & strDBName & "..tlscreening_process_master " & _
                    " LEFT JOIN hremployee_master	ON hremployee_master.employeeunkid = tlscreening_process_master.employeeunkid " & _
                    " and tlscreening_process_master.cycleunkid = " & xCycleUnkid & " " & _
                    " and tlscreening_process_master.isvoid = 0 " & _
                    "LEFT JOIN #TotScore " & _
                    "   ON #TotScore.processmstunkid = tlscreening_process_master.processmstunkid " & _
                    "LEFT JOIN tlratings_master " & _
                    "   ON tlratings_master.stageunkid = tlscreening_process_master.stageunkid " & _
                    "   AND tlratings_master.isvoid = 0 " & _
                    "   AND tlscreening_process_master.isvoid = 0 " & _
                    " LEFT JOIN #TotScr ON #TotScr.employeeunkid = tlscreening_process_master.employeeunkid "

            strQ &= " LEFT JOIN tlpotentialtalent_tran " & _
                    "   ON tlscreening_process_master.employeeunkid = tlpotentialtalent_tran.employeeunkid " & _
                    "   AND tlscreening_process_master.cycleunkid = tlpotentialtalent_tran.cycleunkid "

            strQ &= " JOIN (SELECT " & _
                          "T.EmpId " & _
                        ",T.EOC " & _
                        ",T.LEAVING " & _
                        ",T.actionreasonunkid " & _
                     "FROM (SELECT " & _
                               "employeeunkid AS EmpId " & _
                             ",date1 AS EOC " & _
                             ",date2 AS LEAVING " & _
                             ",effectivedate " & _
                             ",isexclude_payroll " & _
                             ",actionreasonunkid " & _
                             ",otherreason " & _
                             ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                          "FROM " & strDBName & "..hremployee_dates_tran " & _
                          "WHERE datetypeunkid IN ('" & enEmp_Dates_Transaction.DT_TERMINATION & "') " & _
                          "AND isvoid = 0 " & _
                          "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "') AS T " & _
                     "WHERE T.xNo = 1) AS TE " & _
                     "ON TE.EmpId = hremployee_master.employeeunkid "


          

            strQ &= "LEFT JOIN (SELECT " & _
                    "stationunkid " & _
                    ",deptgroupunkid " & _
                    ",departmentunkid " & _
                    ",sectiongroupunkid " & _
                    ",sectionunkid " & _
                    ",unitgroupunkid " & _
                    ",unitunkid " & _
                    ",teamunkid " & _
                    ",classgroupunkid " & _
                    ",classunkid " & _
                    ",employeeunkid " & _
                    ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                 "FROM hremployee_transfer_tran " & _
                 "WHERE isvoid = 0 " & _
                 "AND CONVERT(CHAR(8), effectivedate, 112) <='" & eZeeDate.convertDate(xPeriodEnd) & "') AS Emp_transfer " & _
                 "ON Emp_transfer.employeeunkid = hremployee_master.employeeunkid " & _
                      "AND Emp_transfer.Rno = 1 " & _
            "LEFT JOIN (SELECT " & _
                      "jobgroupunkid " & _
                    ",jobunkid " & _
                    ",employeeunkid " & _
                    ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                 "FROM hremployee_categorization_tran " & _
                 "WHERE isvoid = 0 " & _
                 "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "') AS Emp_categorization " & _
                 "ON Emp_categorization.employeeunkid = hremployee_master.employeeunkid " & _
                      "AND Emp_categorization.Rno = 1 " & _
            "LEFT JOIN (SELECT " & _
                      "gradegroupunkid " & _
                    ",gradeunkid " & _
                    ",gradelevelunkid " & _
                    ",employeeunkid " & _
                    ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                 "FROM prsalaryincrement_tran " & _
                 "WHERE isvoid = 0 " & _
                 "AND isapproved = 1 " & _
                 "AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "') AS Emp_Grade " & _
                 "ON Emp_Grade.employeeunkid = hremployee_master.employeeunkid " & _
                      "AND Emp_Grade.Rno = 1 "



            '********************** DATA FOR DATES CONDITION ************************' --- START

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            'strQ &= mstrAnalysis_Join


            '********************** Talent Seeting Conditions ************************' --- START

           
            strQ &= "LEFT JOIN hrdepartment_master as employee_department " & _
                         "ON Emp_transfer.departmentunkid = employee_department.departmentunkid " & _
                              "AND employee_department.isactive = 1 " & _
                    " " & _
                    "LEFT JOIN hrjob_master as employee_job " & _
                         "ON Emp_categorization.jobunkid = employee_job.jobunkid " & _
                              "AND employee_job.isactive = 1 " & _
                    " " & _
                    "LEFT JOIN hrsectiongroup_master AS  employee_section_grp " & _
                         "ON Emp_transfer.sectiongroupunkid = employee_section_grp.sectiongroupunkid " & _
                              "AND employee_section_grp.isactive = 1 " & _
                    " " & _
                    "LEFT JOIN hrclassgroup_master as  employee_class_grp " & _
                         "ON Emp_transfer.classgroupunkid = employee_class_grp.classgroupunkid " & _
                              "AND employee_class_grp.isactive = 1 " & _
                    " " & _
                    "LEFT JOIN hrclasses_master as employee_class " & _
                         "ON Emp_transfer.classunkid = employee_class.classesunkid " & _
                              "AND employee_class.isactive = 1 " & _
                    " " & _
                    "LEFT JOIN hrgradegroup_master as employee_grade_grp " & _
                         "ON Emp_Grade.gradegroupunkid = employee_grade_grp.gradegroupunkid " & _
                              "AND employee_grade_grp.isactive = 1 " & _
                    " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = TE.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' "

            strQ &= " WHERE 1=1  "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            strQ &= " and tlscreening_process_master.ismatch = 1 and tlscreening_process_master.isvoid = 0 " & _
                    " and tlscreening_process_master.isapproved = 1 "

            'If xEmployeeunkid > 0 Then
            '    strQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)
            'End If


            Select Case xScreeningType
                Case enScreeningFilterType.ONLYMANUALLYADDED
                    strQ &= " AND ISNULL(tlpotentialtalent_tran.potentialtalenttranunkid, 0) > 0 "
                Case enScreeningFilterType.ONLYQUALIFIED
                    strQ &= " AND ISNULL(tlpotentialtalent_tran.potentialtalenttranunkid, 0) = 0 "
                Case Else
            End Select

            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ) Then
                strQ &= " AND #TotScr.totalscreener >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ) & " "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAsOnDate.AddDays(1)))

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If

            Call FilterTitleAndFilterQuery()
            strQ &= Me._FilterQuery

            strQ &= " AND ISNULL(#TotScore.TotalScore,0) / isnull(#TotScr.TotalScreener,0) BETWEEN tlratings_master.scorefrom AND tlratings_master.scoreto " & _
                    " DROP TABLE #TotScr " & _
                    " DROP TABLE #TotScore "

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dfView As DataView = dsList.Tables(0).DefaultView
            dfView.Sort = "employeename"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dfView.ToTable)

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetOtherStagesDetailList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           )


        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty


        Try

            objDataOperation = New clsDataOperation
            dsList = GetApprovedTalentList(xDatabaseName, xUserUnkid, _
                                          xYearUnkid, xCompanyUnkid, _
                                          xPeriodStart, xPeriodEnd, _
                                          xUserModeSetting, _
                                          xOnlyApproved, True, _
                                          mintCycleUnkid, mintEmployeeUnkid, enScreeningFilterType.ALL, "ApprovedData")


            dtFinalTable = New DataTable("Talent")

            dtCol = New DataColumn("EmployeeCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Employee Code")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Name", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Function", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Function")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Department", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Department")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Zone", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Zone")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Branch", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "Branch")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Title", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "Title")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Level", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Level")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("ExitDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 9, "Exit Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Reason", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 10, "Reason")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)


            Dim strCurrentEmp As String = ""
            Dim strPreviousEmp As String = ""
            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim iCnt As Integer = 0
            Dim intRowCount As Integer = dsList.Tables("ApprovedData").Rows.Count

            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables("ApprovedData").Rows(ii)


                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("EmployeeCode") = drRow.Item("employeecode")
                rpt_Row.Item("Name") = drRow.Item("employeename")
                rpt_Row.Item("Function") = drRow.Item("EmployeeDepartment")
                rpt_Row.Item("Department") = drRow.Item("EmployeeSectionGrp")
                rpt_Row.Item("Zone") = drRow.Item("EmployeeClassGrp")
                rpt_Row.Item("Branch") = drRow.Item("EmployeeClass")
                rpt_Row.Item("Title") = drRow.Item("EmployeeJob")
                rpt_Row.Item("Level") = drRow.Item("EmployeeGraderGrp")
                rpt_Row.Item("ExitDate") = CDate(drRow.Item("ExitDate")).ToShortDateString
                rpt_Row.Item("Reason") = drRow.Item("Reason")

                dtFinalTable.Rows.Add(rpt_Row)
            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Cycle :") & " " & mstrCycleName, "s10bw")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)
            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", False, rowsArrayHeader)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally


        End Try
    End Sub


#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee Code")
			Language.setMessage(mstrModuleName, 2, "Name")
			Language.setMessage(mstrModuleName, 3, "Function")
			Language.setMessage(mstrModuleName, 4, "Department")
			Language.setMessage(mstrModuleName, 5, "Zone")
			Language.setMessage(mstrModuleName, 6, "Branch")
			Language.setMessage(mstrModuleName, 7, "Title")
			Language.setMessage(mstrModuleName, 8, "Level")
			Language.setMessage(mstrModuleName, 9, "Exit Date")
			Language.setMessage(mstrModuleName, 10, "Reason")
			Language.setMessage(mstrModuleName, 11, "To")
			Language.setMessage(mstrModuleName, 12, "Exit Date From :")
			Language.setMessage(mstrModuleName, 14, "Cycle :")
			Language.setMessage(mstrModuleName, 15, "Prepared By :")
			Language.setMessage(mstrModuleName, 16, "Checked By :")
			Language.setMessage(mstrModuleName, 17, "Approved By")
			Language.setMessage(mstrModuleName, 18, "Received By :")
			Language.setMessage(mstrModuleName, 19, "Employee :")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
