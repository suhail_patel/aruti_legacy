﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmTalentExitsReport
#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTalentExitsReport"
    Private objTalentExitsReport As clsTalentExitsReport

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty
    Private dvEmployee As DataView
    Private mstrSearchEmpText As String = ""
    Dim strEmployeeId As String = ""
#End Region

#Region " Constructor "
    Public Sub New()
        objTalentExitsReport = New clsTalentExitsReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objTalentExitsReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmTalentExitsReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTalentExitsReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentExitsReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTalentExitsReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Call Language.setLanguage(Me.Name)
            'OtherSettings()
            Call FillCombo()
            Call ResetValue()
            objbtnSearchEmployee.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentExitsReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTalentExitsReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentExitsReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTalentExitsReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTalentExitsReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTalentExitsReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTalentExitsReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCycle As New clstlcycle_master

        Try
            dsCombo = objCycle.getListForCombo(CInt(FinancialYear._Object._YearUnkid), FinancialYear._Object._DatabaseName, FinancialYear._Object._Financial_Start_Date, "List", True, enStatusType.OPEN)
            With cboCycle
                .ValueMember = "cycleunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCycle = Nothing

        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboCycle.SelectedIndex = 0
            dtpExitsFrom.Checked = False
            dtpExitsTo.Checked = False
            If cboEmployee.DataSource IsNot Nothing AndAlso cboEmployee.Items.Count > 0 Then cboEmployee.SelectedIndex = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            objTalentExitsReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objTalentExitsReport.SetDefaultValue()

            objTalentExitsReport._CycleUnkid = cboCycle.SelectedValue
            objTalentExitsReport._CycleName = cboCycle.Text
            objTalentExitsReport._EmployeeUnkid = cboEmployee.SelectedValue
            objTalentExitsReport._EmployeeName = cboEmployee.Text
            If dtpExitsFrom.Checked = True AndAlso dtpExitsTo.Checked = True Then
                objTalentExitsReport._ExitDateFrom = dtpExitsFrom.Value.Date
                objTalentExitsReport._ExitDateTo = dtpExitsTo.Value.Date
            End If
            objTalentExitsReport._ViewByIds = mstrStringIds
            objTalentExitsReport._ViewIndex = mintViewIdx
            objTalentExitsReport._ViewByName = mstrStringName
            objTalentExitsReport._Analysis_Fields = mstrAnalysis_Fields
            objTalentExitsReport._Analysis_Join = mstrAnalysis_Join
            objTalentExitsReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTalentExitsReport._Report_GroupName = mstrReport_GroupName
            objTalentExitsReport._Advance_Filter = mstrAdvanceFilter
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Buttons Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub
            If CInt(cboCycle.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Cycle."), enMsgBoxStyle.Information)
                cboCycle.Focus()
                Exit Sub
            End If



            objTalentExitsReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport, _
                                                                0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchCycle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCycle.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboCycle.DataSource
            frm.ValueMember = cboCycle.ValueMember
            frm.DisplayMember = cboCycle.DisplayMember
            If frm.DisplayDialog Then
                cboCycle.SelectedValue = frm.SelectedValue
                cboCycle.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Combobox Events"
    Private Sub cboCycle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCycle.SelectedIndexChanged
        Dim objtlpipeline_master As New clstlpipeline_master
        Dim objStage As New clstlstages_master
        Dim dsList As New DataSet
        Dim dsCombo As New DataSet
        Try
            If CInt(cboCycle.SelectedValue) > 0 Then
                dsList = objStage.GetList("list", CInt(cboCycle.SelectedValue))
                Dim intmaxStage As Integer = -1
                Dim intminStage As Integer = -1
                Dim intmaxToLastStage As Integer = -1
                Dim mintStageUnkid As Integer = -1
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                    objStage.Get_Min_Max_FlowOrder(intminStage, intmaxStage, intmaxToLastStage, CInt(cboCycle.SelectedValue))
                    Dim drRow As DataRow = dsList.Tables(0).Select("floworder = " & intmaxStage & "")(0)
                    mintStageUnkid = CInt(drRow.Item("stageunkid"))
                End If

                Dim FilterType As enScreeningFilterType = enScreeningFilterType.ALL

                dsCombo = objtlpipeline_master.GetOtherStagesDetailList(FinancialYear._Object._DatabaseName, _
                                                                       User._Object._Userunkid, _
                                                                       FinancialYear._Object._YearUnkid, _
                                                                       Company._Object._Companyunkid, _
                                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                       ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                       True, CInt(cboCycle.SelectedValue), _
                                                                       mintStageUnkid, "", intmaxStage, -1, _
                                                                       FilterType, "Emp", False, mstrAdvanceFilter, True)

                Dim r As DataRow = dsCombo.Tables(0).NewRow

                r.Item(1) = "Select" ' 
                r.Item(2) = "0"

                dsCombo.Tables(0).Rows.InsertAt(r, 0)

                With cboEmployee
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "employeename"
                    .DataSource = dsCombo.Tables("Emp")
                    .SelectedValue = 0
                End With
                objbtnSearchEmployee.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCycle_SelectedIndexChanged", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objtlpipeline_master = Nothing
            objStage = Nothing
        End Try
    End Sub
#End Region

#Region " Other Control's Events "

    'Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
    '    Dim frm As New frmViewAnalysis
    '    Try
    '        frm.displayDialog()
    '        mstrStringIds = frm._ReportBy_Ids
    '        mstrStringName = frm._ReportBy_Name
    '        mintViewIdx = frm._ViewIndex
    '        mstrAnalysis_Fields = frm._Analysis_Fields
    '        mstrAnalysis_Join = frm._Analysis_Join
    '        mstrAnalysis_OrderBy = frm._Analysis_OrderBy
    '        mstrReport_GroupName = frm._Report_GroupName
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
    '    Finally
    '        frm = Nothing
    '    End Try
    'End Sub


    Private Sub lnkAdvanceFilter_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.lblCycle.Text = Language._Object.getCaption(Me.lblCycle.Name, Me.lblCycle.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.lblExitsTo.Text = Language._Object.getCaption(Me.lblExitsTo.Name, Me.lblExitsTo.Text)
			Me.lblExitsFrom.Text = Language._Object.getCaption(Me.lblExitsFrom.Name, Me.lblExitsFrom.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Cycle.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>


End Class