Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

'Last Message index= 2

Public Class frmTimesheet_AT_Report

    Private mstrModuleName As String = "frmTimesheet_AT_Report"
    Private objTimseSheet As clsTimesheet_AT_Report

#Region "Constructor"

    Public Sub New()
        objTimseSheet = New clsTimesheet_AT_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objTimseSheet.SetDefaultValue()
        InitializeComponent()

        'Pinkal (23-Aug-2016) -- Start
        'Enhancement - Changing Timesheet Audit Trail Report for Voltamp.
        '_Show_AdvanceFilter = True
        'Pinkal (23-Aug-2016) -- End
    End Sub

#End Region

#Region "Private Variables"

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region "Private Function"

    Public Function Validation() As Boolean
        Try
            If dtpLoginFromDate.Checked And dtpLoginToDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Login To Date. Login To Date is compulsory information."), enMsgBoxStyle.Information)
                dtpLoginToDate.Focus()
                Return False
            ElseIf dtpLoginFromDate.Checked = False And dtpLoginToDate.Checked Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Login From Date. Login From Date is compulsory information."), enMsgBoxStyle.Information)
                dtpLoginFromDate.Focus()
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

    Public Sub FillCombo()
        Try

            Dim objUser As New clsUserAddEdit
            Dim dsList As New DataSet
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'dsList = objUser.getComboList("User", True)

            'Nilay (01-Mar-2016) -- Start
            'dsList = objUser.getNewComboList("User", , True)
            dsList = objUser.getNewComboList("User", , True, , , , True)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            With cboUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objUser = Nothing

            Dim objEmployee As New clsEmployee_Master


            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes

            ' dsList = objEmployee.GetEmployeeList("Employee", True, False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsList = objEmployee.GetEmployeeList("Employee", True, False)
            'Else
            '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            'S.SANDEEP [04 JUN 2015] -- END

            'Pinkal (11-MAY-2012) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmployee = Nothing

            Dim objAudit As New clsMasterData
            dsList = objAudit.GetAuditTypeList("Audit", True, True, True)
            With cboAuditType
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objAudit = Nothing

            Dim objShift As New clsNewshift_master
            dsList = objShift.getListForCombo("List", True)
            With cboShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            dtpAuditFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpAuditToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date

            dtpLoginFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpLoginFromDate.Checked = False
            dtpLoginToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpLoginToDate.Checked = False

            cboUser.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboAuditType.SelectedIndex = 0
            cboShift.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1

            objTimseSheet.setDefaultOrderBy(0)
            txtOrderBy.Text = objTimseSheet.OrderByDisplay

            chkInactiveemp.Checked = False
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objTimseSheet.SetDefaultValue()

            objTimseSheet._UserId = cboUser.SelectedValue
            objTimseSheet._UName = cboUser.Text

            objTimseSheet._EmployeeId = cboEmployee.SelectedValue
            objTimseSheet._EmployeeName = cboEmployee.Text

            objTimseSheet._AuditTypeId = cboAuditType.SelectedIndex
            objTimseSheet._AudtiType = cboAuditType.Text

            objTimseSheet._AudtiFromDate = dtpAuditFromDate.Value.Date
            objTimseSheet._AuditToDate = dtpAuditToDate.Value.Date

            If dtpLoginFromDate.Checked Then
                objTimseSheet._LoginFromDate = dtpLoginFromDate.Value.Date
            End If

            If dtpLoginFromDate.Checked Then
                objTimseSheet._LoginToDate = dtpLoginToDate.Value.Date
            End If

            objTimseSheet._IsActive = chkInactiveemp.Checked
            objTimseSheet._Shiftunkid = cboShift.SelectedValue
            objTimseSheet._ShiftName = cboShift.Text
            objTimseSheet._ViewByIds = mstrStringIds
            objTimseSheet._ViewIndex = mintViewIdx
            objTimseSheet._ViewByName = mstrStringName
            objTimseSheet._Analysis_Fields = mstrAnalysis_Fields
            objTimseSheet._Analysis_Join = mstrAnalysis_Join
            objTimseSheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTimseSheet._Report_GroupName = mstrReport_GroupName
            objTimseSheet._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmTimesheet_AT_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTimseSheet = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheet_AT_Report_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmTimesheet_AT_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()

            'Pinkal (23-Aug-2016) -- Start
            'Enhancement - Changing Timesheet Audit Trail Report for Voltamp.
            'Me._Title = objTimseSheet._ReportName
            'Me._Message = objTimseSheet._ReportDesc
            eZeeHeader.Title = objTimseSheet._ReportName
            eZeeHeader.Message = objTimseSheet._ReportDesc
            'Pinkal (23-Aug-2016) -- End

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheet_AT_Report_Load", mstrModuleName)
        End Try
    End Sub


    'Pinkal (23-Aug-2016) -- Start
    'Enhancement - Changing Timesheet Audit Trail Report for Voltamp.

    'Private Sub frmTimesheet_AT_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
    '     Try
    '        If e.Control Then
    '            If e.KeyCode = Windows.Forms.Keys.R Then
    '                Call frmTimesheet_AT_Report_Report_Click(Nothing, Nothing)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "frmTimesheet_AT_Report_KeyDown", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (23-Aug-2016) -- End

    Private Sub frmTimesheet_AT_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheet_AT_Report_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

   'Pinkal (23-Aug-2016) -- Start
   'Enhancement - Changing Timesheet Audit Trail Report for Voltamp.

    'Private Sub frmTimesheet_AT_Report_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
    '    Try
    '        If Validation() Then
    '            If SetFilter() = False Then Exit Sub
    ' 
    '            'Shani(24-Aug-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objTimseSheet.generateReport(0, e.Type, enExportAction.None)
    '            objTimseSheet.generateReportNew(FinancialYear._Object._DatabaseName, _
    '                                            User._Object._Userunkid, _
    '                                            FinancialYear._Object._YearUnkid, _
    '                                            Company._Object._Companyunkid, _
    '                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                            ConfigParameter._Object._UserAccessModeSetting, True, _
    '                                            ConfigParameter._Object._ExportReportPath, _
    '                                            ConfigParameter._Object._OpenAfterExport, _
    '                                            0, e.Type, enExportAction.None)
    '            'Shani(24-Aug-2015) -- End

    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "frmTimesheet_AT_Report_Report_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub frmTimesheet_AT_Report_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
    '    Try
    '        If Validation() Then
    '            If SetFilter() = False Then Exit Sub

    '            'Shani(24-Aug-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'objTimseSheet.generateReport(0, enPrintAction.None, e.Type)
    '            objTimseSheet.generateReportNew(FinancialYear._Object._DatabaseName, _
    '                                            User._Object._Userunkid, _
    '                                            FinancialYear._Object._YearUnkid, _
    '                                            Company._Object._Companyunkid, _
    '                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                            ConfigParameter._Object._UserAccessModeSetting, True, _
    '                                            ConfigParameter._Object._ExportReportPath, _
    '                                            ConfigParameter._Object._OpenAfterExport, _
    '                                            0, enPrintAction.None, e.Type)
    '            'Shani(24-Aug-2015) -- End
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "frmTimesheet_AT_Report_Export_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub frmTimesheet_AT_Report_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
    '    Try
    '        Call ResetValue()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "frmTimesheet_AT_Report_Reset_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub frmTimesheet_AT_Report_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
    '    Try
    '        Me.Close()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "frmTimesheet_AT_Report_Cancel_Click", mstrModuleName)
    '    End Try

    'End Sub

    'Private Sub frmTimesheet_AT_Report_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
    '    Dim objfrm As New frmLanguage
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objfrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '        End If

    '        Call SetMessages()

    '        clsTimesheet_AT_Report.SetMessages()
    '        objfrm._Other_ModuleNames = "clsTimesheet_AT_Report"
    '        objfrm.displayDialog(Me)

    '        Call Language.setLanguage(Me.Name)
    '        Call SetLanguage()

    '    Catch ex As System.Exception
    '        Call DisplayError.Show(-1, ex.Message, "frmTimesheet_AT_Report_Language_Click", mstrModuleName)
    '    Finally
    '        objfrm.Dispose()
    '        objfrm = Nothing
    '    End Try
    'End Sub

    'Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
    '    Dim frm As New frmAdvanceSearch
    '    Try
    '        frm._Hr_EmployeeTable_Alias = "hremployee_master"
    '        frm.ShowDialog()
    '        mstrAdvanceFilter = frm._GetFilterString
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub


    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub
                objTimseSheet.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                     , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString()) _
                                                                     , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString()), ConfigParameter._Object._UserAccessModeSetting, True)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTimesheet_AT_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsTimesheet_AT_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchShift.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboShift.DataSource
            frm.ValueMember = cboShift.ValueMember
            frm.DisplayMember = cboShift.DisplayMember
            If frm.DisplayDialog Then
                cboShift.SelectedValue = frm.SelectedValue
                cboShift.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchShift_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objTimseSheet.setOrderBy(0)
            txtOrderBy.Text = objTimseSheet.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblLeaveTodate.Text = Language._Object.getCaption(Me.lblLeaveTodate.Name, Me.lblLeaveTodate.Text)
			Me.lblLoginFromDate.Text = Language._Object.getCaption(Me.lblLoginFromDate.Name, Me.lblLoginFromDate.Text)
			Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
			Me.lblAuditType.Text = Language._Object.getCaption(Me.lblAuditType.Name, Me.lblAuditType.Text)
			Me.lblAuditToDate.Text = Language._Object.getCaption(Me.lblAuditToDate.Name, Me.lblAuditToDate.Text)
			Me.lblUserName.Text = Language._Object.getCaption(Me.lblUserName.Name, Me.lblUserName.Text)
			Me.lblAuditDate.Text = Language._Object.getCaption(Me.lblAuditDate.Name, Me.lblAuditDate.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Login To Date. Login To Date is compulsory information.")
			Language.setMessage(mstrModuleName, 2, "Please select Login From Date. Login From Date is compulsory information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
