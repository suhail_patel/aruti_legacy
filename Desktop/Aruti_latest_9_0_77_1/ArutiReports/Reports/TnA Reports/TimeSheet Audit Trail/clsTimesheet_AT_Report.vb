'************************************************************************************************************************************
'Class Name : clsTimesheet_AT_Report.vb
'Purpose    :
'Date       : 26/02/2011
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsTimesheet_AT_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTimesheet_AT_Report"
    Private mstrReportId As String = enArutiReport.Timesheet_AT_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mdtAuditFromDate As DateTime = Nothing
    Private mdtAuditToDate As DateTime = Nothing
    Private mdtLoginFromDate As DateTime = Nothing
    Private mdtLoginToDate As DateTime = Nothing
    Private mintUserId As Integer = 0
    Private mintEmployeeId As Integer = 0
    Private mintAuditTypeId As Integer = 0
    Private mstrUserName As String = ""
    Private mstrEmployeeName As String = ""
    Private mstrAuditType As String = ""
    Private mstrOrderByQuery As String = ""
    Private mblnIsActive As Boolean = True

    'Pinkal (03-Nov-2012) -- Start
    'Enhancement : TRA Changes
    Private mintShiftunkid As Integer = 0
    Private mstrShiftName As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Pinkal (03-Nov-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _AudtiFromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditFromDate = value
        End Set
    End Property

    Public WriteOnly Property _AuditToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditToDate = value
        End Set
    End Property

    Public WriteOnly Property _LoginFromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtLoginFromDate = value
        End Set
    End Property

    Public WriteOnly Property _LoginToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtLoginToDate = value
        End Set
    End Property

    Public WriteOnly Property _UserId() As Integer
        Set(ByVal value As Integer)
            mintUserId = value
        End Set
    End Property

    Public WriteOnly Property _AuditTypeId() As Integer
        Set(ByVal value As Integer)
            mintAuditTypeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _UName() As String
        Set(ByVal value As String)
            mstrUserName = value
        End Set
    End Property

    Public WriteOnly Property _AudtiType() As String
        Set(ByVal value As String)
            mstrAuditType = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    'Pinkal (03-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _Shiftunkid() As Integer
        Set(ByVal value As Integer)
            mintShiftunkid = value
        End Set
    End Property

    Public WriteOnly Property _ShiftName() As String
        Set(ByVal value As String)
            mstrShiftName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'Pinkal (03-Nov-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mdtAuditFromDate = Nothing
            mdtAuditToDate = Nothing
            mdtLoginFromDate = Nothing
            mdtLoginToDate = Nothing
            mintUserId = 0
            mintAuditTypeId = 0
            mintEmployeeId = 0
            mstrUserName = ""
            mstrEmployeeName = ""
            mstrAuditType = ""
            mstrOrderByQuery = ""
            mblnIsActive = True

            'Pinkal (03-Nov-2012) -- Start
            'Enhancement : TRA Changes
            mintShiftunkid = 0
            mstrShiftName = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            'Pinkal (03-Nov-2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try
            objDataOperation.AddParameter("@AuditFromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditFromDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, " Audit From Date: ") & " " & mdtAuditFromDate.ToShortDateString & " "

            objDataOperation.AddParameter("@AuditToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditToDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, " Audit To Date: ") & " " & mdtAuditToDate.ToShortDateString & " "

            If mdtLoginFromDate <> Nothing And mdtLoginToDate <> Nothing Then

                Me._FilterQuery &= " AND CONVERT(CHAR(8),attnalogin_tran.logindate,112)  >= @LoginFromDate "
                objDataOperation.AddParameter("@LoginFromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtLoginFromDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, " Login From Date: ") & " " & mdtLoginFromDate.ToShortDateString & " "

                Me._FilterQuery &= " AND CONVERT(CHAR(8),attnalogin_tran.logindate,112)  <= @LoginToDate "
                objDataOperation.AddParameter("@LoginToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtLoginToDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, " Login To Date: ") & " " & mdtLoginToDate.ToShortDateString & " "
            End If

            If mintUserId > 0 Then
                Me._FilterQuery &= " AND attnalogin_tran.audituserunkid = @UserId "
                objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "User Name: ") & " " & mstrUserName & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND attnalogin_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee Name: ") & " " & mstrEmployeeName & " "
            End If

            If mintAuditTypeId > 0 Then
                Me._FilterQuery &= " AND attnalogin_tran.audittype = @AuditTypeId "
                objDataOperation.AddParameter("@AuditTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditTypeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, " Audit Type : ") & " " & mstrAuditType & " "
            End If


            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'Pinkal (11-MAY-2012) -- End

            'Pinkal (03-Nov-2012) -- Start
            'Enhancement : TRA Changes
            If mintShiftunkid > 0 Then
                objDataOperation.AddParameter("@ShiftId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid)
                Me._FilterQuery &= " AND tnashift_master.shiftunkid = @ShiftId"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 39, " Shift : ") & " " & mstrShiftName & " "
            End If
            'Pinkal (03-Nov-2012) -- End


            If Me.OrderByQuery <> "" Then
                If mintViewIndex <= 0 Then
                    mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery & ", hremployee_master.employeecode"
                Else
                    mstrOrderByQuery &= "  ORDER BY GName," & Me.OrderByQuery & ", hremployee_master.employeecode"
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub


    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            'objRpt = Generate_DetailReport()
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("[Audit Date]", Language.getMessage(mstrModuleName, 1, "Audit Date")))
            iColumn_DetailReport.Add(New IColumn("Employee", Language.getMessage(mstrModuleName, 31, "Employee")))
            iColumn_DetailReport.Add(New IColumn("[Login Date]", Language.getMessage(mstrModuleName, 3, "Login Date")))
            iColumn_DetailReport.Add(New IColumn("User", Language.getMessage(mstrModuleName, 32, "User")))
            iColumn_DetailReport.Add(New IColumn("[Audit Type]", Language.getMessage(mstrModuleName, 33, "Audit Type")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean)

        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@add", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Add"))
            objDataOperation.AddParameter("@edit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Edit"))
            objDataOperation.AddParameter("@delete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Delete"))
            objDataOperation.AddParameter("@login", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Login"))
            objDataOperation.AddParameter("@logout", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Logout"))
            objDataOperation.AddParameter("@true", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "True"))
            objDataOperation.AddParameter("@false", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "False"))
            objDataOperation.AddParameter("@System", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 40, "System"))
            objDataOperation.AddParameter("@RoundOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 44, "Rounnd Off"))



            StrQ = " SELECT DISTINCT  hremployee_master.employeecode AS 'Code' ,hremployee_master.firstname + ' ' + hremployee_master.surname 'Employee' , " & _
                       " CONVERT(CHAR(8),attnalogin_tran.logindate,112) 'Login Date' , " & _
                       " tnashift_master.shiftname AS 'Shift', " & _
                       " CASE WHEN attnalogin_tran.sourcetype = 4 OR attnalogin_tran.sourcetype = 9 THEN '' ELSE CASE WHEN attnalogin_tran.sourcetype = 7  THEN '' ELSE CONVERT(CHAR(8),attnalogin_tran.original_intime,108) END END As 'Original In Time', " & _
                       "  CASE WHEN  attnalogin_tran.sourcetype = 4 OR attnalogin_tran.sourcetype = 9 THEN '' ELSE CASE WHEN attnalogin_tran.sourcetype = 7  THEN '' ELSE CONVERT(CHAR(8),attnalogin_tran.original_outtime,108) END END AS 'Original Out Time', " & _
                       " CONVERT(CHAR(8),attnalogin_tran.checkintime,108) As 'In Time', " & _
                       " CONVERT(CHAR(8),attnalogin_tran.checkouttime,108) AS 'Out Time', " & _
                       " ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), attnalogin_tran.workhour/ 3600), 2) + ':'  " & _
                       " + RIGHT('00'+ CONVERT(VARCHAR(2),(attnalogin_tran.workhour)% 3600/60), 2), '00:00') AS 'Work Hr' , " & _
                       " ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), attnalogin_tran.breakhr/ 3600), 2) + ':'  " & _
                       " + RIGHT('00'+ CONVERT(VARCHAR(2),(attnalogin_tran.breakhr)% 3600/60), 2), '00:00') AS 'Break Hr' , " & _
                        " ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), attnalogin_tran.teahr/ 3600), 2) + ':'  " & _
                       " + RIGHT('00'+ CONVERT(VARCHAR(2),(attnalogin_tran.teahr)% 3600/60), 2), '00:00') AS 'Tea Hr' , " & _
                       " attnalogin_tran.Remark , " & _
                       " CASE WHEN inouttype = 0 THEN @login " & _
                            " WHEN inouttype = 1 THEN @logout " & _
                       " END 'Login Type' , " & _
                       " CASE WHEN ISNULL(attnalogin_tran.holdunkid,0) > 0 THEN @true" & _
                       " ELSE @false END 'OnHold'," & _
                       " CASE WHEN attnalogin_tran.audittype = 1 THEN " & _
                       "        CASE	WHEN attnalogin_tran.sourcetype = 4 THEN @System " & _
                       "                    WHEN attnalogin_tran.sourcetype = 2 OR attnalogin_tran.sourcetype = 5 OR attnalogin_tran.sourcetype = 6 OR attnalogin_tran.sourcetype = 7 OR	attnalogin_tran.sourcetype = 8 OR attnalogin_tran.sourcetype = 9 THEN CASE WHEN attnalogin_tran.operationtype = 0 OR attnalogin_tran.operationtype = 1 THEN @EDIT ELSE @RoundOff END " & _
                       "        ELSE @add " & _
                       "        END " & _
                       " WHEN attnalogin_tran.audittype = 2 THEN " & _
                       "        CASE	WHEN attnalogin_tran.sourcetype = 4 THEN @System " & _
                       "                    WHEN attnalogin_tran.sourcetype = 2 OR attnalogin_tran.sourcetype = 5 OR attnalogin_tran.sourcetype = 6 OR attnalogin_tran.sourcetype = 7 OR	attnalogin_tran.sourcetype = 8 OR attnalogin_tran.sourcetype = 9 THEN CASE WHEN attnalogin_tran.operationtype = 0 OR attnalogin_tran.operationtype = 1 THEN @EDIT ELSE @RoundOff END  " & _
                       "        ELSE @EDIT " & _
                       "        END " & _
                             " WHEN attnalogin_tran.audittype = 3 THEN @delete " & _
                       " End 'Audit Type' ," & _
                       " ISNULL(hrmsConfiguration..cfuser_master.username, '') AS 'User' , " & _
                       " CONVERT(VARCHAR(20), attnalogin_tran.auditdatetime, 112) 'Audit Date' , " & _
                       " LEFT(CONVERT(NVARCHAR(10),attnalogin_tran.auditdatetime,108),5) 'Audit Time' , " & _
                       " ip AS 'Machine IP', machine_name AS 'Machine' "

            '" CONVERT(VARCHAR(20), attnalogin_tran.auditdatetime, 108) 'Audit Time' , " & _

            'Pinkal (05-Dec-2016) -- Start
            'Enhancement - Working on Period related Issue faced by Voltamp 
            '" FORMAT(attnalogin_tran.auditdatetime,'HH:mm') 'Audit Time' , " & _
            'Pinkal (05-Dec-2016) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM attnalogin_tran " & _
                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = attnalogin_tran.employeeunkid " & _
                      "  LEFT JOIN  " & _
                      " ( " & _
                      "     SELECT  ROW_NUMBER() OVER ( PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS RowNo " & _
                      "    ,hremployee_shift_tran.employeeunkid " & _
                      "    ,hremployee_shift_tran.shiftunkid " & _
                      "    From " & _
                      "    hremployee_shift_tran " & _
                      "    WHERE hremployee_shift_tran.isvoid = 0 ) AS A ON a.employeeunkid = hremployee_master.employeeunkid  AND A.RowNo = 1 " & _
                       " LEFT JOIN tnashift_master ON tnashift_master.shiftunkid = A.shiftunkid " & _
                       " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = attnalogin_tran.audituserunkid "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE CONVERT(CHAR(8),attnalogin_tran.auditdatetime,112) >= @AuditFromDate AND " & _
                       " CONVERT(CHAR(8),attnalogin_tran.auditdatetime,112) <= @AuditToDate "


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dsList.Tables(0)

            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                mdtTableExcel.Columns.Remove("Id")
                mdtTableExcel.Columns.Remove("GName")
            End If


            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 27, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 28, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 29, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 30, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            '--------------------

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH

            mdtTableExcel.Columns("Audit Date").Caption = Language.getMessage(mstrModuleName, 1, "Audit Date")
            mdtTableExcel.Columns("Employee").Caption = Language.getMessage(mstrModuleName, 31, "Employee")
            mdtTableExcel.Columns("Login Date").Caption = Language.getMessage(mstrModuleName, 3, "Login Date")
            mdtTableExcel.Columns("User").Caption = Language.getMessage(mstrModuleName, 32, "User")
            mdtTableExcel.Columns("Audit Type").Caption = Language.getMessage(mstrModuleName, 33, "Audit Type")
            mdtTableExcel.Columns("Code").Caption = Language.getMessage(mstrModuleName, 13, "Code")
            mdtTableExcel.Columns("Shift").Caption = Language.getMessage(mstrModuleName, 14, "Shift")
            mdtTableExcel.Columns("In Time").Caption = Language.getMessage(mstrModuleName, 15, "In Time")
            mdtTableExcel.Columns("Out Time").Caption = Language.getMessage(mstrModuleName, 16, "Out Time")
            mdtTableExcel.Columns("Work Hr").Caption = Language.getMessage(mstrModuleName, 17, "Work Hr")
            mdtTableExcel.Columns("Break Hr").Caption = Language.getMessage(mstrModuleName, 34, "Break Hr")
            mdtTableExcel.Columns("Login Type").Caption = Language.getMessage(mstrModuleName, 35, "Login Type")
            mdtTableExcel.Columns("OnHold").Caption = Language.getMessage(mstrModuleName, 36, "OnHold")
            mdtTableExcel.Columns("Audit Time").Caption = Language.getMessage(mstrModuleName, 37, "Audit Time")
            mdtTableExcel.Columns("Machine IP").Caption = Language.getMessage(mstrModuleName, 38, "Machine IP")
            mdtTableExcel.Columns("Machine").Caption = Language.getMessage(mstrModuleName, 23, "Machine")
            mdtTableExcel.Columns("Original In Time").Caption = Language.getMessage(mstrModuleName, 42, "Original In Time")
            mdtTableExcel.Columns("Original Out Time").Caption = Language.getMessage(mstrModuleName, 43, "Original Out Time")


            mdtTableExcel.AsEnumerable.ToList.ForEach(Function(x) UpdateRow(x))

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Function UpdateRow(ByVal dr As DataRow) As Boolean
        Try
            dr("Audit Date") = FormatDateTime(eZeeDate.convertDate(dr("Audit Date").ToString()), DateFormat.ShortDate)
            dr("Login Date") = FormatDateTime(eZeeDate.convertDate(dr("Login Date")), DateFormat.ShortDate)
            dr.AcceptChanges()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: RemoveColumn; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Audit Date")
            Language.setMessage(mstrModuleName, 2, "Employee Name:")
            Language.setMessage(mstrModuleName, 3, "Login Date")
            Language.setMessage(mstrModuleName, 4, "User Name:")
            Language.setMessage(mstrModuleName, 5, " Audit Type :")
            Language.setMessage(mstrModuleName, 6, "Add")
            Language.setMessage(mstrModuleName, 7, "Edit")
            Language.setMessage(mstrModuleName, 8, "Delete")
            Language.setMessage(mstrModuleName, 9, "Login")
            Language.setMessage(mstrModuleName, 10, "Logout")
            Language.setMessage(mstrModuleName, 11, "True")
            Language.setMessage(mstrModuleName, 12, "False")
            Language.setMessage(mstrModuleName, 13, "Code")
            Language.setMessage(mstrModuleName, 14, "Shift")
            Language.setMessage(mstrModuleName, 15, "In Time")
            Language.setMessage(mstrModuleName, 16, "Out Time")
            Language.setMessage(mstrModuleName, 17, "Work Hr")
            Language.setMessage(mstrModuleName, 18, " Audit From Date:")
            Language.setMessage(mstrModuleName, 19, " Audit To Date:")
            Language.setMessage(mstrModuleName, 20, " Login From Date:")
            Language.setMessage(mstrModuleName, 21, " Login To Date:")
            Language.setMessage(mstrModuleName, 22, " Order By :")
            Language.setMessage(mstrModuleName, 23, "Machine")
            Language.setMessage(mstrModuleName, 27, "Prepared By :")
            Language.setMessage(mstrModuleName, 28, "Checked By :")
            Language.setMessage(mstrModuleName, 29, "Approved By :")
            Language.setMessage(mstrModuleName, 30, "Received By :")
            Language.setMessage(mstrModuleName, 31, "Employee")
            Language.setMessage(mstrModuleName, 32, "User")
            Language.setMessage(mstrModuleName, 33, "Audit Type")
            Language.setMessage(mstrModuleName, 34, "Break Hr")
            Language.setMessage(mstrModuleName, 35, "Login Type")
            Language.setMessage(mstrModuleName, 36, "OnHold")
            Language.setMessage(mstrModuleName, 37, "Audit Time")
            Language.setMessage(mstrModuleName, 38, "Machine IP")
            Language.setMessage(mstrModuleName, 39, " Shift :")
            Language.setMessage(mstrModuleName, 40, "System")
            Language.setMessage(mstrModuleName, 42, "Original In Time")
            Language.setMessage(mstrModuleName, 43, "Original Out Time")
            Language.setMessage(mstrModuleName, 44, "Rounnd Off")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
