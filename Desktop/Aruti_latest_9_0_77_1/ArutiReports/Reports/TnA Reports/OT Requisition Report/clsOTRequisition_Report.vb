'************************************************************************************************************************************
'Class Name : clsOTRequisition_Report.vb
'Purpose    :
'Date       : 15/01/2020
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsOTRequisition_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsOTRequisition_Report"
    Private mstrReportId As String = enArutiReport.OTRequisition_Report  '217
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "


    'Pinkal (15-Jan-2020) -- Start
    'Enhancements -  Working on OT Requisistion Reports for NMB.
    'Private mintReportId As Integer = 0
    'Private mstrReportTypeName As String = ""
    'Pinkal (15-Jan-2020) -- End

    Private mintPeriodId As Integer = 0
    Private mstrPeriod As String = ""
    Private mdtPeriodStartDate As DateTime = Nothing
    Private mdtPeriodEndDate As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintNormalOTTranHeadId As Integer = 0
    Private mintHolidayOTTranHeadId As Integer = 0
    Private mstrOrderByQuery As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True

#End Region

#Region " Properties "


    'Pinkal (15-Jan-2020) -- Start
    'Enhancements -  Working on OT Requisistion Reports for NMB.

    'Public WriteOnly Property _ReportId() As Integer
    '    Set(ByVal value As Integer)
    '        mintReportId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ReportTypeName() As String
    '    Set(ByVal value As String)
    '        mstrReportTypeName = value
    '    End Set
    'End Property

    'Pinkal (15-Jan-2020) -- End


    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _Period() As String
        Set(ByVal value As String)
            mstrPeriod = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _NormalOTTranHeadId() As Integer
        Set(ByVal value As Integer)
            mintNormalOTTranHeadId = value
        End Set
    End Property

    Public WriteOnly Property _HolidayOTTranHeadId() As Integer
        Set(ByVal value As Integer)
            mintHolidayOTTranHeadId = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
            'mintReportId = 0
            'mstrReportTypeName = ""
            'Pinkal (15-Jan-2020) -- End

            'mintPeriodId = 0
            'mstrPeriod = ""
            mdtPeriodStartDate = Nothing
            mdtPeriodEndDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""
            mblnIncludeAccessFilterQry = True

            'Pinkal (28-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            setDefaultOrderBy(0)
            'Pinkal (28-Sep-2020) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            If mintPeriodId > 0 Then

                'Pinkal (05-May-2020) -- Start
                'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
                ' Me._FilterQuery &= " AND tnaotrequisition_process_tran.periodunkid = @periodunkid "
                'Pinkal (05-May-2020) -- End
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Period: ") & " " & mstrPeriod & " "
            End If

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Period Start Date: ") & " " & mdtPeriodStartDate.Date & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Period End Date: ") & " " & mdtPeriodEndDate.Date & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND tnaot_requisition_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mintViewIndex > 0 Then
                If Me.OrderByQuery <> "" Then
                    mstrOrderByQuery &= "ORDER BY " & mstrAnalysis_OrderBy_GName & ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') , " & Me.OrderByQuery
                End If
            Else
                If Me.OrderByQuery <> "" Then
                    mstrOrderByQuery &= "  ORDER BY ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') , " & Me.OrderByQuery
                End If
            End If

            'Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, " Order By : ") & " " & Me.OrderByDisplay

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            'Pinkal (28-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            'Pinkal (28-Sep-2020) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_OTRequisitionDetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                                     , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                                     , ByVal xIncludeIn_ActiveEmployee As Boolean, Optional ByVal blnOnlyActive As Boolean = True)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try


            Company._Object._Companyunkid = xCompanyUnkid
            ConfigParameter._Object._Companyunkid = xCompanyUnkid

            User._Object._Userunkid = xUserUnkid

            objDataOperation = New clsDataOperation

            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)

            objDataOperation.ClearParameters()

            'Pinkal (05-May-2020) -- Start
            'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .

            StrQ = " SELECT DISTINCT " & _
                       " tnaot_requisition_tran.employeeunkid " & _
                       ", ISNULL(hremployee_master.employeecode, '') AS EmployeeCode " & _
                       ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                       ", ISNULL(hcg.name, '') AS ClassGrp " & _
                       ", ISNULL(hc.name, '') AS Class " & _
                       ", ISNULL(hjb.job_name, '') AS Job " & _
                       ", ISNULL(hgd.name, '') AS GradeGrp " & _
                       ", ISNULL(sal.basicsalary,0.00) As  basicsalary "

            If mintNormalOTTranHeadId > 0 Then
                StrQ &= ", ISNULL(NormalOT.NormalOTPay,0.00) AS NormalOT " & _
                             ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(MAX), ISNULL(NormalHR.actualot_hours,0) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (ISNULL(NormalHR.actualot_hours,0) % 3600) / 60), 2), '00:00') AS NormalHrs "
            End If

            If mintHolidayOTTranHeadId > 0 Then
                StrQ &= ", ISNULL(HolidayOT.HolidayOTPay,0.00) AS HolidayOT " & _
                             ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(MAX), ISNULL(HolidayHR.actualot_hours,0) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (ISNULL(HolidayHR.actualot_hours,0) % 3600) / 60), 2), '00:00') AS HolidayHrs "
            End If


            If mintNormalOTTranHeadId > 0 AndAlso mintHolidayOTTranHeadId > 0 Then
                StrQ &= ", ISNULL(NormalOT.NormalOTPay,0.00) +  ISNULL(HolidayOT.HolidayOTPay,0.00) AS TotalOT " & _
                             ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(MAX), (ISNULL(NormalHR.actualot_hours,0) + ISNULL(HolidayHR.actualot_hours,0)) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ((ISNULL(NormalHR.actualot_hours,0) + ISNULL(HolidayHR.actualot_hours,0)) % 3600) / 60), 2), '00:00') AS TotalHrs "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            'Pinkal (16-May-2020) -- Start
            'Enhancement NMB Leave Report Changes -   Working on Leave Reports required by NMB.

            StrQ &= " FROM tnaot_requisition_tran " & _
                         " LEFT JOIN tnaotrequisition_process_tran ON tnaotrequisition_process_tran.otrequisitiontranunkid = tnaot_requisition_tran.otrequisitiontranunkid  " & _
                         " AND tnaotrequisition_process_tran.isvoid = 0 AND tnaotrequisition_process_tran.isposted = 1 AND tnaotrequisition_process_tran.periodunkid = @periodunkid " & _
                         " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnaot_requisition_tran.employeeunkid " & _
                         " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "             stationunkid " & _
                          "            ,deptgroupunkid " & _
                          "            ,departmentunkid " & _
                          "            ,sectiongroupunkid " & _
                          "            ,sectionunkid " & _
                          "            ,unitgroupunkid " & _
                          "            ,unitunkid " & _
                          "            ,teamunkid " & _
                          "            ,classgroupunkid " & _
                          "             ,classunkid " & _
                          "            ,employeeunkid " & _
                          "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "       FROM hremployee_transfer_tran " & _
                          "       WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate " & _
                          " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                          " LEFT JOIN hrclassgroup_master hcg ON t.classgroupunkid = hcg.classgroupunkid " & _
                          " LEFT JOIN hrclasses_master hc ON t.classunkid = hc.classesunkid " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "             SELECT " & _
                          "                 jobgroupunkid " & _
                          "                ,jobunkid " & _
                          "                ,employeeunkid " & _
                          "                ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "             FROM hremployee_categorization_tran " & _
                          "             WHERE isvoid = 0	AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate  " & _
                          " ) AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                          " LEFT JOIN hrjob_master hjb ON hjb.jobunkid = j.jobunkid " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "           gradegroupunkid " & _
                          "          ,gradeunkid " & _
                          "          ,gradelevelunkid " & _
                          "          ,employeeunkid " & _
                          "          ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                          "        FROM prsalaryincrement_tran " & _
                          "        WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= @EmployeeAsOnDate " & _
                          " ) AS G ON G.employeeunkid = hremployee_master.employeeunkid	AND G.Rno = 1 " & _
                          " LEFT JOIN hrgradegroup_master hgd ON hgd.gradegroupunkid = G.gradegroupunkid " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "             prpayrollprocess_tran.employeeunkid " & _
                          "            ,ISNULL(prpayrollprocess_tran.amount,0.00) AS basicsalary " & _
                          "            ,prtnaleave_tran.payperiodunkid " & _
                          "         FROM prpayrollprocess_tran " & _
                          "         JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid AND prtranhead_master.isvoid = 0 " & _
                          "         JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid AND prtnaleave_tran.isvoid = 0 AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                          "         WHERE prpayrollprocess_tran.isvoid = 0 AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & _
                          "         AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") " & _
                          "         OR prtranhead_master.isbasicsalaryasotherearning = 1) " & _
                          " ) AS Sal ON sal.employeeunkid = hremployee_master.employeeunkid  AND Sal.payperiodunkid = @periodunkid "


            'Pinkal (04-Jul-2020) --Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement. [AND sal.payperiodunkid = tnaotrequisition_process_tran.periodunkid]

            If mintNormalOTTranHeadId > 0 Then

                StrQ &= " LEFT JOIN " & _
                             " ( " & _
                             "         SELECT " & _
                             "             prpayrollprocess_tran.employeeunkid " & _
                             "            ,ISNULL(prpayrollprocess_tran.amount,0.00) AS NormalOTPay " & _
                             "            ,prtnaleave_tran.payperiodunkid " & _
                             "         FROM prpayrollprocess_tran " & _
                             "         JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid AND prtranhead_master.isvoid = 0 " & _
                             "         JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid AND prtnaleave_tran.isvoid = 0 AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                             "         WHERE prpayrollprocess_tran.isvoid = 0 AND prtranhead_master.tranheadunkid = @NormalOTHeadId " & _
                             " ) AS NormalOT ON NormalOT.employeeunkid = hremployee_master.employeeunkid  AND NormalOT.payperiodunkid = @periodunkid " & _
                             " LEFT JOIN " & _
                             " ( " & _
                             "          SELECT " & _
                             "              tnaot_requisition_tran.employeeunkid " & _
                             "             ,ISNULL(SUM(tnaot_requisition_tran.actualot_hours),0) AS actualot_hours " & _
                             "          FROM tnaot_requisition_tran " & _
                             "          LEFT JOIN tnaotrequisition_process_tran ON tnaotrequisition_process_tran.otrequisitiontranunkid = tnaot_requisition_tran.otrequisitiontranunkid  " & _
                             "          AND tnaotrequisition_process_tran.isvoid = 0  AND tnaotrequisition_process_tran.isposted = 1" & _
                             "          WHERE tnaot_requisition_tran.isvoid = 0 AND tnaot_requisition_tran.statusunkid = 1 " & _
                             "          AND tnaotrequisition_process_tran.periodunkid = @periodunkid " & _
                             "           AND isholiday = 0 AND isweekend = 0 AND isdayoff = 0 " & _
                             "          GROUP BY tnaot_requisition_tran.employeeunkid " & _
                             " ) AS NormalHR ON NormalHR.employeeunkid = hremployee_master.employeeunkid "


                'Pinkal (04-Jul-2020) -- Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.[AND NormalOT.payperiodunkid = tnaotrequisition_process_tran.periodunkid]

            End If


            If mintHolidayOTTranHeadId > 0 Then

                StrQ &= " LEFT JOIN " & _
                             " ( " & _
                             "         SELECT " & _
                             "             prpayrollprocess_tran.employeeunkid " & _
                             "            ,ISNULL(prpayrollprocess_tran.amount,0.00) AS HolidayOTPay " & _
                             "            ,prtnaleave_tran.payperiodunkid " & _
                             "         FROM prpayrollprocess_tran " & _
                             "         JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid AND prtranhead_master.isvoid = 0 " & _
                             "         JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid AND prtnaleave_tran.isvoid = 0 AND prtnaleave_tran.payperiodunkid = @periodunkid" & _
                             "         WHERE prpayrollprocess_tran.isvoid = 0 AND prtranhead_master.tranheadunkid = @HolidayOTHeadId " & _
                             " ) AS HolidayOT ON HolidayOT.employeeunkid = hremployee_master.employeeunkid  AND HolidayOT.payperiodunkid = @periodunkid  " & _
                             " LEFT JOIN " & _
                             " ( " & _
                             "          SELECT " & _
                             "              tnaot_requisition_tran.employeeunkid " & _
                             "             ,ISNULL(SUM(tnaot_requisition_tran.actualot_hours),0) AS actualot_hours " & _
                             "          FROM tnaot_requisition_tran " & _
                             "          LEFT JOIN tnaotrequisition_process_tran ON tnaotrequisition_process_tran.otrequisitiontranunkid = tnaot_requisition_tran.otrequisitiontranunkid " & _
                             "          AND tnaotrequisition_process_tran.isvoid = 0 AND tnaotrequisition_process_tran.isposted = 1 " & _
                             "          WHERE tnaot_requisition_tran.isvoid = 0 AND tnaot_requisition_tran.statusunkid = 1  AND tnaotrequisition_process_tran.periodunkid = @periodunkid " & _
                             "          AND (isholiday = 1 OR isweekend = 1 OR isdayoff = 1)  " & _
                             "         GROUP BY tnaot_requisition_tran.employeeunkid " & _
                             " ) AS HolidayHR ON HolidayHR.employeeunkid = hremployee_master.employeeunkid "

                'Pinkal (16-May-2020) -- End


                'Pinkal (04-Jul-2020) -- Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.[AND HolidayOT.payperiodunkid = tnaotrequisition_process_tran.periodunkid]

            End If


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If


            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If


            'StrQ &= " WHERE tnaot_requisition_tran.isvoid = 0 AND tnaot_requisition_tran.statusunkid = 1 " & _
            '             " AND tnaotrequisition_process_tran.isvoid = 0 AND tnaotrequisition_process_tran.isposted = 1  "

            StrQ &= " WHERE tnaot_requisition_tran.isvoid = 0 AND tnaot_requisition_tran.statusunkid = 1 "

            'Pinkal (05-May-2020) -- End



            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@EmployeeAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
            objDataOperation.AddParameter("@NormalOTHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNormalOTTranHeadId)
            objDataOperation.AddParameter("@HolidayOTHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHolidayOTTranHeadId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dsList.Tables(0)
            mdtTableExcel.AcceptChanges()

            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If


            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
            End If

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            '--------------------

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            mdtTableExcel.Columns("EmployeeCode").Caption = Language.getMessage(mstrModuleName, 1, "Employee Code")
            mdtTableExcel.Columns("Employee").Caption = Language.getMessage(mstrModuleName, 2, "Employee")
            mdtTableExcel.Columns("ClassGrp").Caption = Language.getMessage(mstrModuleName, 4, "Class Group")
            mdtTableExcel.Columns("Class").Caption = Language.getMessage(mstrModuleName, 5, "Class")
            mdtTableExcel.Columns("Job").Caption = Language.getMessage(mstrModuleName, 6, "Job")
            mdtTableExcel.Columns("GradeGrp").Caption = Language.getMessage(mstrModuleName, 7, "Grade Group")
            mdtTableExcel.Columns("basicsalary").Caption = Language.getMessage(mstrModuleName, 8, "Basic Pay")

            If mintNormalOTTranHeadId > 0 Then
                mdtTableExcel.Columns("NormalOT").Caption = Language.getMessage(mstrModuleName, 9, "Normal OT")
                mdtTableExcel.Columns("NormalHrs").Caption = Language.getMessage(mstrModuleName, 10, "Normal Hrs")
            End If

            If mintHolidayOTTranHeadId > 0 Then
                mdtTableExcel.Columns("HolidayOT").Caption = Language.getMessage(mstrModuleName, 11, "Holiday OT")
                mdtTableExcel.Columns("HolidayHrs").Caption = Language.getMessage(mstrModuleName, 12, "Holiday Hrs")
            End If

            If mintNormalOTTranHeadId > 0 AndAlso mintHolidayOTTranHeadId > 0 Then
                mdtTableExcel.Columns("TotalOT").Caption = Language.getMessage(mstrModuleName, 13, "Total OT")
                mdtTableExcel.Columns("TotalHrs").Caption = Language.getMessage(mstrModuleName, 14, "Total Hrs")
            End If


            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, mstrReportTypeName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)
            'Pinkal (15-Jan-2020) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_OTRequisitionDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_OTRequisitionSummaryReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                                  , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                                  , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                                  , ByVal xIncludeIn_ActiveEmployee As Boolean, Optional ByVal blnOnlyActive As Boolean = True)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try

            Company._Object._Companyunkid = xCompanyUnkid
            ConfigParameter._Object._Companyunkid = xCompanyUnkid

            User._Object._Userunkid = xUserUnkid

            objDataOperation = New clsDataOperation

            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)

            objDataOperation.ClearParameters()

            StrQ = " SELECT DISTINCT " & _
                       " tnaot_requisition_tran.employeeunkid " & _
                       ", ISNULL(hremployee_master.employeecode, '') AS EmployeeCode " & _
                       ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                       ", ISNULL(hjb.job_name, '') AS Job " & _
                       ", ISNULL(hc.name, '') AS Class " & _
                       ", OTHRs.OTType " & _
                       ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(MAX), ISNULL(OTHRs.plannedot_hours,0) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (ISNULL(OTHRs.plannedot_hours,0) % 3600) / 60), 2), '00:00') AS plannedothrs " & _
                       ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(MAX), ISNULL(OTHRs.actualot_hours,0) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (ISNULL(OTHRs.actualot_hours,0) % 3600) / 60), 2), '00:00') AS actualothrs " & _
                       ", ISNULL(OTHRs.plannedot_hours ,0) AS plannedot_hours " & _
                       ", ISNULL(OTHRs.actualot_hours,0) AS actualot_hours "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " FROM tnaot_requisition_tran " & _
                         " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnaot_requisition_tran.employeeunkid " & _
                         " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "             stationunkid " & _
                          "            ,deptgroupunkid " & _
                          "            ,departmentunkid " & _
                          "            ,sectiongroupunkid " & _
                          "            ,sectionunkid " & _
                          "            ,unitgroupunkid " & _
                          "            ,unitunkid " & _
                          "            ,teamunkid " & _
                          "            ,classgroupunkid " & _
                          "             ,classunkid " & _
                          "            ,employeeunkid " & _
                          "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "       FROM hremployee_transfer_tran " & _
                          "       WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate " & _
                          " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                          " LEFT JOIN hrclasses_master hc ON t.classunkid = hc.classesunkid " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "             SELECT " & _
                          "                 jobgroupunkid " & _
                          "                ,jobunkid " & _
                          "                ,employeeunkid " & _
                          "                ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "             FROM hremployee_categorization_tran " & _
                          "             WHERE isvoid = 0	AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate  " & _
                          " ) AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                          " LEFT JOIN hrjob_master hjb ON hjb.jobunkid = j.jobunkid " & _
                          " JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "              employeeunkid " & _
                          "             ,ISNULL(SUM(plannedot_hours), 0) AS plannedot_hours " & _
                          "             ,ISNULL(SUM(actualot_hours),0) AS actualot_hours " & _
                          "             ,@NormalOT AS OTType  " & _
                          "          FROM tnaot_requisition_tran " & _
                          "          WHERE tnaot_requisition_tran.isvoid = 0 AND tnaot_requisition_tran.statusunkid = 1 " & _
                          "          AND isholiday = 0 AND isweekend = 0 AND isdayoff = 0 " & _
                          "          AND CONVERT(CHAR(8),tnaot_requisition_tran.requestdate,112) BETWEEN @startdate AND @enddate "

            If mintEmployeeId > 0 Then
                StrQ &= " AND tnaot_requisition_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "         GROUP BY employeeunkid " & _
                         "         UNION " & _
                         "         SELECT " & _
                         "              employeeunkid " & _
                         "             ,ISNULL(SUM(plannedot_hours), 0) AS plannedot_hours " & _
                         "             ,ISNULL(SUM(actualot_hours),0) AS actualot_hours " & _
                         "             ,@HolidayOT AS OTType  " & _
                         "         FROM tnaot_requisition_tran " & _
                         "         WHERE tnaot_requisition_tran.isvoid = 0 AND tnaot_requisition_tran.statusunkid = 1 " & _
                         "         AND  (isholiday = 1 OR isweekend = 1 OR isdayoff = 1) " & _
                         "        AND CONVERT(CHAR(8),tnaot_requisition_tran.requestdate,112) BETWEEN @startdate AND @enddate "


            If mintEmployeeId > 0 Then
                StrQ &= " AND tnaot_requisition_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "         GROUP BY employeeunkid " & _
                         " ) AS OTHRs ON OTHRs.employeeunkid = hremployee_master.employeeunkid "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If


            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE tnaot_requisition_tran.isvoid = 0 AND tnaot_requisition_tran.statusunkid = 1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@EmployeeAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
            objDataOperation.AddParameter("@NormalOT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "Normal OT"))
            objDataOperation.AddParameter("@HolidayOT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Holiday OT"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dsList.Tables(0)
            mdtTableExcel.AcceptChanges()

            Dim mstrTotalNormalPlannedOTHrs As String = CalculateTime(True, CInt(mdtTableExcel.AsEnumerable().Where(Function(x) x.Field(Of String)("OTType") = Language.getMessage(mstrModuleName, 23, "Normal OT")).Sum(Function(x) x.Field(Of Integer)("plannedot_hours")))).ToString("#00.00").Replace(".", ":")
            Dim mstrTotalNormalActualOTHrs As String = CalculateTime(True, CInt(mdtTableExcel.AsEnumerable().Where(Function(x) x.Field(Of String)("OTType") = Language.getMessage(mstrModuleName, 23, "Normal OT")).Sum(Function(x) x.Field(Of Integer)("actualot_hours")))).ToString("#00.00").Replace(".", ":")

            Dim mstrTotalHolidayPlannedOTHrs As String = CalculateTime(True, CInt(mdtTableExcel.AsEnumerable().Where(Function(x) x.Field(Of String)("OTType") = Language.getMessage(mstrModuleName, 24, "Holiday OT")).Sum(Function(x) x.Field(Of Integer)("plannedot_hours")))).ToString("#00.00").Replace(".", ":")
            Dim mstrTotalHolidayActualOTHrs As String = CalculateTime(True, CInt(mdtTableExcel.AsEnumerable().Where(Function(x) x.Field(Of String)("OTType") = Language.getMessage(mstrModuleName, 24, "Holiday OT")).Sum(Function(x) x.Field(Of Integer)("actualot_hours")))).ToString("#00.00").Replace(".", ":")


            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            If mdtTableExcel.Columns.Contains("plannedot_hours") Then
                mdtTableExcel.Columns.Remove("plannedot_hours")
            End If

            If mdtTableExcel.Columns.Contains("actualot_hours") Then
                mdtTableExcel.Columns.Remove("actualot_hours")
            End If


            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
            End If

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)



            row = New WorksheetRow()

            wcell = New WorksheetCell("", "FooterStyle")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "FooterStyle")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "FooterStyle")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 28, "Total"), "FooterStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Normal OT"), "FooterStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(mstrTotalNormalPlannedOTHrs, "FooterStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(mstrTotalNormalActualOTHrs, "FooterStyle")
            If mintViewIndex > 0 Then
                wcell.MergeAcross = 1
            End If
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            row = New WorksheetRow()

            wcell = New WorksheetCell("", "FooterStyle")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "FooterStyle")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "FooterStyle")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 28, "Total"), "FooterStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "Holiday OT"), "FooterStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(mstrTotalHolidayPlannedOTHrs, "FooterStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(mstrTotalHolidayActualOTHrs, "FooterStyle")
            If mintViewIndex > 0 Then
                wcell.MergeAcross = 1
            End If
            row.Cells.Add(wcell)

            rowsArrayFooter.Add(row)


            '--------------------


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            '--------------------

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            mdtTableExcel.Columns("EmployeeCode").Caption = Language.getMessage(mstrModuleName, 1, "Employee Code")
            mdtTableExcel.Columns("Employee").Caption = Language.getMessage(mstrModuleName, 2, "Employee")
            mdtTableExcel.Columns("Job").Caption = Language.getMessage(mstrModuleName, 6, "Job")
            mdtTableExcel.Columns("Class").Caption = Language.getMessage(mstrModuleName, 5, "Class")
            mdtTableExcel.Columns("OTType").Caption = Language.getMessage(mstrModuleName, 25, "OT Type")
            mdtTableExcel.Columns("plannedothrs").Caption = Language.getMessage(mstrModuleName, 26, "Planned OT Hrs")
            mdtTableExcel.Columns("actualothrs").Caption = Language.getMessage(mstrModuleName, 27, "Actual OT Hrs")


            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, mstrReportTypeName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)
            'Pinkal (15-Jan-2020) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_OTRequisitionDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 4, "Class Group")
            Language.setMessage(mstrModuleName, 5, "Class")
            Language.setMessage(mstrModuleName, 6, "Job")
            Language.setMessage(mstrModuleName, 7, "Grade Group")
            Language.setMessage(mstrModuleName, 8, "Basic Pay")
            Language.setMessage(mstrModuleName, 9, "Normal OT")
            Language.setMessage(mstrModuleName, 10, "Normal Hrs")
            Language.setMessage(mstrModuleName, 11, "Holiday OT")
            Language.setMessage(mstrModuleName, 12, "Holiday Hrs")
            Language.setMessage(mstrModuleName, 13, "Total OT")
            Language.setMessage(mstrModuleName, 14, "Total Hrs")
            Language.setMessage(mstrModuleName, 15, "Prepared By :")
            Language.setMessage(mstrModuleName, 16, "Checked By :")
            Language.setMessage(mstrModuleName, 17, "Approved By")
            Language.setMessage(mstrModuleName, 18, "Received By :")
            Language.setMessage(mstrModuleName, 19, "Period:")
            Language.setMessage(mstrModuleName, 20, "Employee:")
            Language.setMessage(mstrModuleName, 21, "Period Start Date:")
            Language.setMessage(mstrModuleName, 22, "Period End Date:")
            Language.setMessage(mstrModuleName, 23, "Normal OT")
            Language.setMessage(mstrModuleName, 24, "Holiday OT")
            Language.setMessage(mstrModuleName, 25, "OT Type")
            Language.setMessage(mstrModuleName, 26, "Planned OT Hrs")
            Language.setMessage(mstrModuleName, 27, "Actual OT Hrs")
            Language.setMessage(mstrModuleName, 28, "Total")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
