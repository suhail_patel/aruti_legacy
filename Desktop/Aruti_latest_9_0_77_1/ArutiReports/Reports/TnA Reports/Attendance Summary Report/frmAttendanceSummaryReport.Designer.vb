﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAttendanceSummaryReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowTotalhrs = New System.Windows.Forms.CheckBox
        Me.objbtnSearchLeave5 = New eZee.Common.eZeeGradientButton
        Me.cboLeave5 = New System.Windows.Forms.ComboBox
        Me.LblLeave5 = New System.Windows.Forms.Label
        Me.objStLine = New eZee.Common.eZeeStraightLine
        Me.objbtnSearchLeave4 = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchLeave3 = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchLeave2 = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchLeave1 = New eZee.Common.eZeeGradientButton
        Me.cboLeave4 = New System.Windows.Forms.ComboBox
        Me.cboLeave3 = New System.Windows.Forms.ComboBox
        Me.cboLeave2 = New System.Windows.Forms.ComboBox
        Me.cboLeave1 = New System.Windows.Forms.ComboBox
        Me.LblLeave4 = New System.Windows.Forms.Label
        Me.LblLeave3 = New System.Windows.Forms.Label
        Me.LblLeave2 = New System.Windows.Forms.Label
        Me.LblLeave1 = New System.Windows.Forms.Label
        Me.elLine1 = New eZee.Common.eZeeLine
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lnkSave = New System.Windows.Forms.LinkLabel
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.lvLeaveType = New eZee.Common.eZeeListView(Me.components)
        Me.colhLeave = New System.Windows.Forms.ColumnHeader
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 432)
        Me.NavPanel.Size = New System.Drawing.Size(628, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowTotalhrs)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLeave5)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeave5)
        Me.gbFilterCriteria.Controls.Add(Me.LblLeave5)
        Me.gbFilterCriteria.Controls.Add(Me.objStLine)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLeave4)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLeave3)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLeave2)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLeave1)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeave4)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeave3)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeave2)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeave1)
        Me.gbFilterCriteria.Controls.Add(Me.LblLeave4)
        Me.gbFilterCriteria.Controls.Add(Me.LblLeave3)
        Me.gbFilterCriteria.Controls.Add(Me.LblLeave2)
        Me.gbFilterCriteria.Controls.Add(Me.LblLeave1)
        Me.gbFilterCriteria.Controls.Add(Me.elLine1)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSave)
        Me.gbFilterCriteria.Controls.Add(Me.objchkAll)
        Me.gbFilterCriteria.Controls.Add(Me.txtSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lvLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lblToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmpName)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(604, 290)
        Me.gbFilterCriteria.TabIndex = 16
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowTotalhrs
        '
        Me.chkShowTotalhrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowTotalhrs.Location = New System.Drawing.Point(76, 246)
        Me.chkShowTotalhrs.Name = "chkShowTotalhrs"
        Me.chkShowTotalhrs.Size = New System.Drawing.Size(245, 17)
        Me.chkShowTotalhrs.TabIndex = 239
        Me.chkShowTotalhrs.Text = "Show Total Worked Hours"
        Me.chkShowTotalhrs.UseVisualStyleBackColor = True
        '
        'objbtnSearchLeave5
        '
        Me.objbtnSearchLeave5.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave5.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave5.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave5.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeave5.BorderSelected = False
        Me.objbtnSearchLeave5.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeave5.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeave5.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeave5.Location = New System.Drawing.Point(307, 218)
        Me.objbtnSearchLeave5.Name = "objbtnSearchLeave5"
        Me.objbtnSearchLeave5.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeave5.TabIndex = 237
        '
        'cboLeave5
        '
        Me.cboLeave5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeave5.DropDownWidth = 120
        Me.cboLeave5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeave5.FormattingEnabled = True
        Me.cboLeave5.Location = New System.Drawing.Point(75, 218)
        Me.cboLeave5.Name = "cboLeave5"
        Me.cboLeave5.Size = New System.Drawing.Size(229, 21)
        Me.cboLeave5.TabIndex = 236
        '
        'LblLeave5
        '
        Me.LblLeave5.BackColor = System.Drawing.Color.Transparent
        Me.LblLeave5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeave5.Location = New System.Drawing.Point(8, 221)
        Me.LblLeave5.Name = "LblLeave5"
        Me.LblLeave5.Size = New System.Drawing.Size(62, 15)
        Me.LblLeave5.TabIndex = 235
        Me.LblLeave5.Text = "Leave 5"
        '
        'objStLine
        '
        Me.objStLine.BackColor = System.Drawing.Color.Transparent
        Me.objStLine.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objStLine.ForeColor = System.Drawing.Color.DarkGray
        Me.objStLine.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine.Location = New System.Drawing.Point(330, 99)
        Me.objStLine.Name = "objStLine"
        Me.objStLine.Size = New System.Drawing.Size(5, 183)
        Me.objStLine.TabIndex = 233
        '
        'objbtnSearchLeave4
        '
        Me.objbtnSearchLeave4.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave4.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave4.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave4.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeave4.BorderSelected = False
        Me.objbtnSearchLeave4.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeave4.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeave4.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeave4.Location = New System.Drawing.Point(307, 191)
        Me.objbtnSearchLeave4.Name = "objbtnSearchLeave4"
        Me.objbtnSearchLeave4.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeave4.TabIndex = 232
        '
        'objbtnSearchLeave3
        '
        Me.objbtnSearchLeave3.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave3.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave3.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave3.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeave3.BorderSelected = False
        Me.objbtnSearchLeave3.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeave3.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeave3.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeave3.Location = New System.Drawing.Point(307, 165)
        Me.objbtnSearchLeave3.Name = "objbtnSearchLeave3"
        Me.objbtnSearchLeave3.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeave3.TabIndex = 231
        '
        'objbtnSearchLeave2
        '
        Me.objbtnSearchLeave2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeave2.BorderSelected = False
        Me.objbtnSearchLeave2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeave2.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeave2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeave2.Location = New System.Drawing.Point(307, 138)
        Me.objbtnSearchLeave2.Name = "objbtnSearchLeave2"
        Me.objbtnSearchLeave2.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeave2.TabIndex = 230
        '
        'objbtnSearchLeave1
        '
        Me.objbtnSearchLeave1.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave1.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave1.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeave1.BorderSelected = False
        Me.objbtnSearchLeave1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeave1.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeave1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeave1.Location = New System.Drawing.Point(307, 112)
        Me.objbtnSearchLeave1.Name = "objbtnSearchLeave1"
        Me.objbtnSearchLeave1.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeave1.TabIndex = 229
        '
        'cboLeave4
        '
        Me.cboLeave4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeave4.DropDownWidth = 120
        Me.cboLeave4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeave4.FormattingEnabled = True
        Me.cboLeave4.Location = New System.Drawing.Point(75, 191)
        Me.cboLeave4.Name = "cboLeave4"
        Me.cboLeave4.Size = New System.Drawing.Size(229, 21)
        Me.cboLeave4.TabIndex = 228
        '
        'cboLeave3
        '
        Me.cboLeave3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeave3.DropDownWidth = 120
        Me.cboLeave3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeave3.FormattingEnabled = True
        Me.cboLeave3.Location = New System.Drawing.Point(75, 165)
        Me.cboLeave3.Name = "cboLeave3"
        Me.cboLeave3.Size = New System.Drawing.Size(229, 21)
        Me.cboLeave3.TabIndex = 227
        '
        'cboLeave2
        '
        Me.cboLeave2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeave2.DropDownWidth = 120
        Me.cboLeave2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeave2.FormattingEnabled = True
        Me.cboLeave2.Location = New System.Drawing.Point(75, 138)
        Me.cboLeave2.Name = "cboLeave2"
        Me.cboLeave2.Size = New System.Drawing.Size(229, 21)
        Me.cboLeave2.TabIndex = 226
        '
        'cboLeave1
        '
        Me.cboLeave1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeave1.DropDownWidth = 120
        Me.cboLeave1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeave1.FormattingEnabled = True
        Me.cboLeave1.Location = New System.Drawing.Point(75, 112)
        Me.cboLeave1.Name = "cboLeave1"
        Me.cboLeave1.Size = New System.Drawing.Size(229, 21)
        Me.cboLeave1.TabIndex = 225
        '
        'LblLeave4
        '
        Me.LblLeave4.BackColor = System.Drawing.Color.Transparent
        Me.LblLeave4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeave4.Location = New System.Drawing.Point(8, 194)
        Me.LblLeave4.Name = "LblLeave4"
        Me.LblLeave4.Size = New System.Drawing.Size(62, 15)
        Me.LblLeave4.TabIndex = 224
        Me.LblLeave4.Text = "Leave 4"
        '
        'LblLeave3
        '
        Me.LblLeave3.BackColor = System.Drawing.Color.Transparent
        Me.LblLeave3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeave3.Location = New System.Drawing.Point(8, 168)
        Me.LblLeave3.Name = "LblLeave3"
        Me.LblLeave3.Size = New System.Drawing.Size(62, 15)
        Me.LblLeave3.TabIndex = 223
        Me.LblLeave3.Text = "Leave 3"
        '
        'LblLeave2
        '
        Me.LblLeave2.BackColor = System.Drawing.Color.Transparent
        Me.LblLeave2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeave2.Location = New System.Drawing.Point(8, 141)
        Me.LblLeave2.Name = "LblLeave2"
        Me.LblLeave2.Size = New System.Drawing.Size(62, 15)
        Me.LblLeave2.TabIndex = 222
        Me.LblLeave2.Text = "Leave 2"
        '
        'LblLeave1
        '
        Me.LblLeave1.BackColor = System.Drawing.Color.Transparent
        Me.LblLeave1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeave1.Location = New System.Drawing.Point(8, 115)
        Me.LblLeave1.Name = "LblLeave1"
        Me.LblLeave1.Size = New System.Drawing.Size(62, 15)
        Me.LblLeave1.TabIndex = 221
        Me.LblLeave1.Text = "Leave 1"
        '
        'elLine1
        '
        Me.elLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elLine1.Location = New System.Drawing.Point(348, 89)
        Me.elLine1.Name = "elLine1"
        Me.elLine1.Size = New System.Drawing.Size(244, 12)
        Me.elLine1.TabIndex = 219
        Me.elLine1.Text = "Consider Special Leave"
        Me.elLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(506, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 22
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lnkSave
        '
        Me.lnkSave.ActiveLinkColor = System.Drawing.Color.Maroon
        Me.lnkSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSave.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSave.LinkColor = System.Drawing.Color.Maroon
        Me.lnkSave.Location = New System.Drawing.Point(72, 270)
        Me.lnkSave.Name = "lnkSave"
        Me.lnkSave.Size = New System.Drawing.Size(90, 15)
        Me.lnkSave.TabIndex = 217
        Me.lnkSave.TabStop = True
        Me.lnkSave.Text = "&Save Selection"
        Me.lnkSave.VisitedLinkColor = System.Drawing.Color.Maroon
        '
        'objchkAll
        '
        Me.objchkAll.BackColor = System.Drawing.Color.Transparent
        Me.objchkAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAll.Location = New System.Drawing.Point(353, 136)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(217, 16)
        Me.objchkAll.TabIndex = 216
        Me.objchkAll.Text = "Leave"
        Me.objchkAll.UseVisualStyleBackColor = False
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(345, 108)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(250, 21)
        Me.txtSearch.TabIndex = 215
        '
        'lvLeaveType
        '
        Me.lvLeaveType.BackColorOnChecked = False
        Me.lvLeaveType.CheckBoxes = True
        Me.lvLeaveType.ColumnHeaders = Nothing
        Me.lvLeaveType.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLeave})
        Me.lvLeaveType.CompulsoryColumns = ""
        Me.lvLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvLeaveType.FullRowSelect = True
        Me.lvLeaveType.GridLines = True
        Me.lvLeaveType.GroupingColumn = Nothing
        Me.lvLeaveType.HideSelection = False
        Me.lvLeaveType.Location = New System.Drawing.Point(345, 133)
        Me.lvLeaveType.MinColumnWidth = 50
        Me.lvLeaveType.MultiSelect = False
        Me.lvLeaveType.Name = "lvLeaveType"
        Me.lvLeaveType.OptionalColumns = ""
        Me.lvLeaveType.ShowMoreItem = False
        Me.lvLeaveType.ShowSaveItem = False
        Me.lvLeaveType.ShowSelectAll = True
        Me.lvLeaveType.ShowSizeAllColumnsToFit = True
        Me.lvLeaveType.Size = New System.Drawing.Size(250, 148)
        Me.lvLeaveType.Sortable = True
        Me.lvLeaveType.TabIndex = 213
        Me.lvLeaveType.UseCompatibleStateImageBehavior = False
        Me.lvLeaveType.View = System.Windows.Forms.View.Details
        '
        'colhLeave
        '
        Me.colhLeave.Tag = "colhLeave"
        Me.colhLeave.Text = ""
        Me.colhLeave.Width = 244
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(330, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 207
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(76, 87)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(245, 17)
        Me.chkInactiveemp.TabIndex = 204
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(169, 36)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(62, 15)
        Me.lblToDate.TabIndex = 175
        Me.lblToDate.Text = "To Date"
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(237, 33)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(87, 21)
        Me.dtpToDate.TabIndex = 176
        '
        'lblEmpName
        '
        Me.lblEmpName.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(8, 63)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(62, 15)
        Me.lblEmpName.TabIndex = 167
        Me.lblEmpName.Text = "Employee"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(76, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(248, 21)
        Me.cboEmployee.TabIndex = 168
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(8, 36)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(62, 15)
        Me.lblFromDate.TabIndex = 151
        Me.lblFromDate.Text = "From Date"
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(76, 33)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(87, 21)
        Me.dtpFromDate.TabIndex = 152
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 362)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(328, 63)
        Me.gbSortBy.TabIndex = 17
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(283, 28)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(68, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(76, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(201, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'frmAttendanceSummaryReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(628, 487)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmAttendanceSummaryReport"
        Me.Text = "frmAttendanceSummaryReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Public WithEvents lblToDate As System.Windows.Forms.Label
    Public WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Private WithEvents lblEmpName As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Public WithEvents lblFromDate As System.Windows.Forms.Label
    Public WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lvLeaveType As eZee.Common.eZeeListView
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents colhLeave As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents lnkSave As System.Windows.Forms.LinkLabel
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents elLine1 As eZee.Common.eZeeLine
    Public WithEvents cboLeave4 As System.Windows.Forms.ComboBox
    Public WithEvents cboLeave3 As System.Windows.Forms.ComboBox
    Public WithEvents cboLeave2 As System.Windows.Forms.ComboBox
    Public WithEvents cboLeave1 As System.Windows.Forms.ComboBox
    Private WithEvents LblLeave4 As System.Windows.Forms.Label
    Private WithEvents LblLeave3 As System.Windows.Forms.Label
    Private WithEvents LblLeave2 As System.Windows.Forms.Label
    Private WithEvents LblLeave1 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchLeave4 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchLeave3 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchLeave2 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchLeave1 As eZee.Common.eZeeGradientButton
    Friend WithEvents objStLine As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnSearchLeave5 As eZee.Common.eZeeGradientButton
    Public WithEvents cboLeave5 As System.Windows.Forms.ComboBox
    Private WithEvents LblLeave5 As System.Windows.Forms.Label
    Friend WithEvents chkShowTotalhrs As System.Windows.Forms.CheckBox
End Class
