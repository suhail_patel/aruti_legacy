﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHolidayAttendanceReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkDayOff = New System.Windows.Forms.CheckBox
        Me.objELLine1 = New eZee.Common.eZeeLine
        Me.lblOT4HrsTo = New System.Windows.Forms.Label
        Me.lblOT4HrsFrom = New System.Windows.Forms.Label
        Me.txtOT4HrsTo = New eZee.TextBox.NumericTextBox
        Me.txtOT4HrsFrom = New eZee.TextBox.NumericTextBox
        Me.lblOT3HrsTo = New System.Windows.Forms.Label
        Me.lblOT3HrsFrom = New System.Windows.Forms.Label
        Me.txtOT3HrsTo = New eZee.TextBox.NumericTextBox
        Me.txtOT3HrsFrom = New eZee.TextBox.NumericTextBox
        Me.lblOT2HrsTo = New System.Windows.Forms.Label
        Me.lblOT2HrsFrom = New System.Windows.Forms.Label
        Me.txtOT2HrsTo = New eZee.TextBox.NumericTextBox
        Me.txtOT2HrsFrom = New eZee.TextBox.NumericTextBox
        Me.lblOT1HrsTo = New System.Windows.Forms.Label
        Me.lblOT1HrsFrom = New System.Windows.Forms.Label
        Me.txtOT1HrsTo = New eZee.TextBox.NumericTextBox
        Me.txtOT1HrsFrom = New eZee.TextBox.NumericTextBox
        Me.lblToHrs = New System.Windows.Forms.Label
        Me.lblWorkeHrs = New System.Windows.Forms.Label
        Me.txtWHrsTo = New eZee.TextBox.NumericTextBox
        Me.txtWHrsFrom = New eZee.TextBox.NumericTextBox
        Me.objbtnSearchPolicy = New eZee.Common.eZeeGradientButton
        Me.lblPolicy = New System.Windows.Forms.Label
        Me.cboPolicy = New System.Windows.Forms.ComboBox
        Me.objbtnSearchShift = New eZee.Common.eZeeGradientButton
        Me.LblShift = New System.Windows.Forms.Label
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 466)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeLine1)
        Me.gbFilterCriteria.Controls.Add(Me.Label1)
        Me.gbFilterCriteria.Controls.Add(Me.chkDayOff)
        Me.gbFilterCriteria.Controls.Add(Me.objELLine1)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT4HrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT4HrsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtOT4HrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtOT4HrsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT3HrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT3HrsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtOT3HrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtOT3HrsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT2HrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT2HrsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtOT2HrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtOT2HrsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT1HrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT1HrsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtOT1HrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtOT1HrsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblToHrs)
        Me.gbFilterCriteria.Controls.Add(Me.lblWorkeHrs)
        Me.gbFilterCriteria.Controls.Add(Me.txtWHrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtWHrsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPolicy)
        Me.gbFilterCriteria.Controls.Add(Me.lblPolicy)
        Me.gbFilterCriteria.Controls.Add(Me.cboPolicy)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchShift)
        Me.gbFilterCriteria.Controls.Add(Me.LblShift)
        Me.gbFilterCriteria.Controls.Add(Me.cboShift)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmpName)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(412, 321)
        Me.gbFilterCriteria.TabIndex = 17
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(314, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 24
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(204, 157)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(203, 13)
        Me.EZeeLine1.TabIndex = 239
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 157)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(203, 15)
        Me.Label1.TabIndex = 238
        Me.Label1.Text = "Please Enter Below All Criteria  in Hours "
        '
        'chkDayOff
        '
        Me.chkDayOff.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDayOff.Location = New System.Drawing.Point(254, 141)
        Me.chkDayOff.Name = "chkDayOff"
        Me.chkDayOff.Size = New System.Drawing.Size(118, 17)
        Me.chkDayOff.TabIndex = 236
        Me.chkDayOff.Text = "Include Day Off"
        Me.chkDayOff.UseVisualStyleBackColor = True
        '
        'objELLine1
        '
        Me.objELLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objELLine1.Location = New System.Drawing.Point(11, 142)
        Me.objELLine1.Name = "objELLine1"
        Me.objELLine1.Size = New System.Drawing.Size(237, 13)
        Me.objELLine1.TabIndex = 19
        Me.objELLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOT4HrsTo
        '
        Me.lblOT4HrsTo.BackColor = System.Drawing.Color.Transparent
        Me.lblOT4HrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT4HrsTo.Location = New System.Drawing.Point(214, 291)
        Me.lblOT4HrsTo.Name = "lblOT4HrsTo"
        Me.lblOT4HrsTo.Size = New System.Drawing.Size(57, 15)
        Me.lblOT4HrsTo.TabIndex = 235
        Me.lblOT4HrsTo.Text = "To"
        '
        'lblOT4HrsFrom
        '
        Me.lblOT4HrsFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblOT4HrsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT4HrsFrom.Location = New System.Drawing.Point(8, 291)
        Me.lblOT4HrsFrom.Name = "lblOT4HrsFrom"
        Me.lblOT4HrsFrom.Size = New System.Drawing.Size(99, 15)
        Me.lblOT4HrsFrom.TabIndex = 234
        Me.lblOT4HrsFrom.Text = "OT4 Hrs. From"
        '
        'txtOT4HrsTo
        '
        Me.txtOT4HrsTo.AllowNegative = True
        Me.txtOT4HrsTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOT4HrsTo.DigitsInGroup = 0
        Me.txtOT4HrsTo.Flags = 0
        Me.txtOT4HrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOT4HrsTo.Location = New System.Drawing.Point(277, 288)
        Me.txtOT4HrsTo.MaxDecimalPlaces = 2
        Me.txtOT4HrsTo.MaxWholeDigits = 9
        Me.txtOT4HrsTo.Name = "txtOT4HrsTo"
        Me.txtOT4HrsTo.Prefix = ""
        Me.txtOT4HrsTo.RangeMax = 1.7976931348623157E+308
        Me.txtOT4HrsTo.RangeMin = -1.7976931348623157E+308
        Me.txtOT4HrsTo.Size = New System.Drawing.Size(95, 21)
        Me.txtOT4HrsTo.TabIndex = 233
        Me.txtOT4HrsTo.Text = "0"
        Me.txtOT4HrsTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOT4HrsFrom
        '
        Me.txtOT4HrsFrom.AllowNegative = True
        Me.txtOT4HrsFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOT4HrsFrom.DigitsInGroup = 0
        Me.txtOT4HrsFrom.Flags = 0
        Me.txtOT4HrsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOT4HrsFrom.Location = New System.Drawing.Point(113, 288)
        Me.txtOT4HrsFrom.MaxDecimalPlaces = 2
        Me.txtOT4HrsFrom.MaxWholeDigits = 9
        Me.txtOT4HrsFrom.Name = "txtOT4HrsFrom"
        Me.txtOT4HrsFrom.Prefix = ""
        Me.txtOT4HrsFrom.RangeMax = 1.7976931348623157E+308
        Me.txtOT4HrsFrom.RangeMin = -1.7976931348623157E+308
        Me.txtOT4HrsFrom.Size = New System.Drawing.Size(95, 21)
        Me.txtOT4HrsFrom.TabIndex = 232
        Me.txtOT4HrsFrom.Text = "0"
        Me.txtOT4HrsFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOT3HrsTo
        '
        Me.lblOT3HrsTo.BackColor = System.Drawing.Color.Transparent
        Me.lblOT3HrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT3HrsTo.Location = New System.Drawing.Point(214, 264)
        Me.lblOT3HrsTo.Name = "lblOT3HrsTo"
        Me.lblOT3HrsTo.Size = New System.Drawing.Size(57, 15)
        Me.lblOT3HrsTo.TabIndex = 231
        Me.lblOT3HrsTo.Text = "To"
        '
        'lblOT3HrsFrom
        '
        Me.lblOT3HrsFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblOT3HrsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT3HrsFrom.Location = New System.Drawing.Point(8, 264)
        Me.lblOT3HrsFrom.Name = "lblOT3HrsFrom"
        Me.lblOT3HrsFrom.Size = New System.Drawing.Size(99, 15)
        Me.lblOT3HrsFrom.TabIndex = 230
        Me.lblOT3HrsFrom.Text = "OT3 Hrs. From"
        '
        'txtOT3HrsTo
        '
        Me.txtOT3HrsTo.AllowNegative = True
        Me.txtOT3HrsTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOT3HrsTo.DigitsInGroup = 0
        Me.txtOT3HrsTo.Flags = 0
        Me.txtOT3HrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOT3HrsTo.Location = New System.Drawing.Point(277, 261)
        Me.txtOT3HrsTo.MaxDecimalPlaces = 2
        Me.txtOT3HrsTo.MaxWholeDigits = 9
        Me.txtOT3HrsTo.Name = "txtOT3HrsTo"
        Me.txtOT3HrsTo.Prefix = ""
        Me.txtOT3HrsTo.RangeMax = 1.7976931348623157E+308
        Me.txtOT3HrsTo.RangeMin = -1.7976931348623157E+308
        Me.txtOT3HrsTo.Size = New System.Drawing.Size(95, 21)
        Me.txtOT3HrsTo.TabIndex = 229
        Me.txtOT3HrsTo.Text = "0"
        Me.txtOT3HrsTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOT3HrsFrom
        '
        Me.txtOT3HrsFrom.AllowNegative = True
        Me.txtOT3HrsFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOT3HrsFrom.DigitsInGroup = 0
        Me.txtOT3HrsFrom.Flags = 0
        Me.txtOT3HrsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOT3HrsFrom.Location = New System.Drawing.Point(113, 261)
        Me.txtOT3HrsFrom.MaxDecimalPlaces = 2
        Me.txtOT3HrsFrom.MaxWholeDigits = 9
        Me.txtOT3HrsFrom.Name = "txtOT3HrsFrom"
        Me.txtOT3HrsFrom.Prefix = ""
        Me.txtOT3HrsFrom.RangeMax = 1.7976931348623157E+308
        Me.txtOT3HrsFrom.RangeMin = -1.7976931348623157E+308
        Me.txtOT3HrsFrom.Size = New System.Drawing.Size(95, 21)
        Me.txtOT3HrsFrom.TabIndex = 228
        Me.txtOT3HrsFrom.Text = "0"
        Me.txtOT3HrsFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOT2HrsTo
        '
        Me.lblOT2HrsTo.BackColor = System.Drawing.Color.Transparent
        Me.lblOT2HrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT2HrsTo.Location = New System.Drawing.Point(214, 237)
        Me.lblOT2HrsTo.Name = "lblOT2HrsTo"
        Me.lblOT2HrsTo.Size = New System.Drawing.Size(57, 15)
        Me.lblOT2HrsTo.TabIndex = 227
        Me.lblOT2HrsTo.Text = "To"
        '
        'lblOT2HrsFrom
        '
        Me.lblOT2HrsFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblOT2HrsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT2HrsFrom.Location = New System.Drawing.Point(8, 237)
        Me.lblOT2HrsFrom.Name = "lblOT2HrsFrom"
        Me.lblOT2HrsFrom.Size = New System.Drawing.Size(99, 15)
        Me.lblOT2HrsFrom.TabIndex = 226
        Me.lblOT2HrsFrom.Text = "OT2 Hrs. From"
        '
        'txtOT2HrsTo
        '
        Me.txtOT2HrsTo.AllowNegative = True
        Me.txtOT2HrsTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOT2HrsTo.DigitsInGroup = 0
        Me.txtOT2HrsTo.Flags = 0
        Me.txtOT2HrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOT2HrsTo.Location = New System.Drawing.Point(277, 234)
        Me.txtOT2HrsTo.MaxDecimalPlaces = 2
        Me.txtOT2HrsTo.MaxWholeDigits = 9
        Me.txtOT2HrsTo.Name = "txtOT2HrsTo"
        Me.txtOT2HrsTo.Prefix = ""
        Me.txtOT2HrsTo.RangeMax = 1.7976931348623157E+308
        Me.txtOT2HrsTo.RangeMin = -1.7976931348623157E+308
        Me.txtOT2HrsTo.Size = New System.Drawing.Size(95, 21)
        Me.txtOT2HrsTo.TabIndex = 225
        Me.txtOT2HrsTo.Text = "0"
        Me.txtOT2HrsTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOT2HrsFrom
        '
        Me.txtOT2HrsFrom.AllowNegative = True
        Me.txtOT2HrsFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOT2HrsFrom.DigitsInGroup = 0
        Me.txtOT2HrsFrom.Flags = 0
        Me.txtOT2HrsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOT2HrsFrom.Location = New System.Drawing.Point(113, 234)
        Me.txtOT2HrsFrom.MaxDecimalPlaces = 2
        Me.txtOT2HrsFrom.MaxWholeDigits = 9
        Me.txtOT2HrsFrom.Name = "txtOT2HrsFrom"
        Me.txtOT2HrsFrom.Prefix = ""
        Me.txtOT2HrsFrom.RangeMax = 1.7976931348623157E+308
        Me.txtOT2HrsFrom.RangeMin = -1.7976931348623157E+308
        Me.txtOT2HrsFrom.Size = New System.Drawing.Size(95, 21)
        Me.txtOT2HrsFrom.TabIndex = 224
        Me.txtOT2HrsFrom.Text = "0"
        Me.txtOT2HrsFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOT1HrsTo
        '
        Me.lblOT1HrsTo.BackColor = System.Drawing.Color.Transparent
        Me.lblOT1HrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT1HrsTo.Location = New System.Drawing.Point(214, 210)
        Me.lblOT1HrsTo.Name = "lblOT1HrsTo"
        Me.lblOT1HrsTo.Size = New System.Drawing.Size(57, 15)
        Me.lblOT1HrsTo.TabIndex = 223
        Me.lblOT1HrsTo.Text = "To"
        '
        'lblOT1HrsFrom
        '
        Me.lblOT1HrsFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblOT1HrsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT1HrsFrom.Location = New System.Drawing.Point(8, 210)
        Me.lblOT1HrsFrom.Name = "lblOT1HrsFrom"
        Me.lblOT1HrsFrom.Size = New System.Drawing.Size(99, 15)
        Me.lblOT1HrsFrom.TabIndex = 222
        Me.lblOT1HrsFrom.Text = "OT1 Hrs. From"
        '
        'txtOT1HrsTo
        '
        Me.txtOT1HrsTo.AllowNegative = True
        Me.txtOT1HrsTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOT1HrsTo.DigitsInGroup = 0
        Me.txtOT1HrsTo.Flags = 0
        Me.txtOT1HrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOT1HrsTo.Location = New System.Drawing.Point(277, 207)
        Me.txtOT1HrsTo.MaxDecimalPlaces = 2
        Me.txtOT1HrsTo.MaxWholeDigits = 9
        Me.txtOT1HrsTo.Name = "txtOT1HrsTo"
        Me.txtOT1HrsTo.Prefix = ""
        Me.txtOT1HrsTo.RangeMax = 1.7976931348623157E+308
        Me.txtOT1HrsTo.RangeMin = -1.7976931348623157E+308
        Me.txtOT1HrsTo.Size = New System.Drawing.Size(95, 21)
        Me.txtOT1HrsTo.TabIndex = 221
        Me.txtOT1HrsTo.Text = "0"
        Me.txtOT1HrsTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOT1HrsFrom
        '
        Me.txtOT1HrsFrom.AllowNegative = True
        Me.txtOT1HrsFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOT1HrsFrom.DigitsInGroup = 0
        Me.txtOT1HrsFrom.Flags = 0
        Me.txtOT1HrsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOT1HrsFrom.Location = New System.Drawing.Point(113, 207)
        Me.txtOT1HrsFrom.MaxDecimalPlaces = 2
        Me.txtOT1HrsFrom.MaxWholeDigits = 9
        Me.txtOT1HrsFrom.Name = "txtOT1HrsFrom"
        Me.txtOT1HrsFrom.Prefix = ""
        Me.txtOT1HrsFrom.RangeMax = 1.7976931348623157E+308
        Me.txtOT1HrsFrom.RangeMin = -1.7976931348623157E+308
        Me.txtOT1HrsFrom.Size = New System.Drawing.Size(95, 21)
        Me.txtOT1HrsFrom.TabIndex = 220
        Me.txtOT1HrsFrom.Text = "0"
        Me.txtOT1HrsFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblToHrs
        '
        Me.lblToHrs.BackColor = System.Drawing.Color.Transparent
        Me.lblToHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToHrs.Location = New System.Drawing.Point(214, 183)
        Me.lblToHrs.Name = "lblToHrs"
        Me.lblToHrs.Size = New System.Drawing.Size(57, 15)
        Me.lblToHrs.TabIndex = 219
        Me.lblToHrs.Text = "To"
        '
        'lblWorkeHrs
        '
        Me.lblWorkeHrs.BackColor = System.Drawing.Color.Transparent
        Me.lblWorkeHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkeHrs.Location = New System.Drawing.Point(8, 183)
        Me.lblWorkeHrs.Name = "lblWorkeHrs"
        Me.lblWorkeHrs.Size = New System.Drawing.Size(99, 15)
        Me.lblWorkeHrs.TabIndex = 218
        Me.lblWorkeHrs.Text = "Worked Hrs. From"
        '
        'txtWHrsTo
        '
        Me.txtWHrsTo.AllowNegative = True
        Me.txtWHrsTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtWHrsTo.DigitsInGroup = 0
        Me.txtWHrsTo.Flags = 0
        Me.txtWHrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWHrsTo.Location = New System.Drawing.Point(277, 180)
        Me.txtWHrsTo.MaxDecimalPlaces = 2
        Me.txtWHrsTo.MaxWholeDigits = 9
        Me.txtWHrsTo.Name = "txtWHrsTo"
        Me.txtWHrsTo.Prefix = ""
        Me.txtWHrsTo.RangeMax = 1.7976931348623157E+308
        Me.txtWHrsTo.RangeMin = -1.7976931348623157E+308
        Me.txtWHrsTo.Size = New System.Drawing.Size(95, 21)
        Me.txtWHrsTo.TabIndex = 217
        Me.txtWHrsTo.Text = "0"
        Me.txtWHrsTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtWHrsFrom
        '
        Me.txtWHrsFrom.AllowNegative = True
        Me.txtWHrsFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtWHrsFrom.DigitsInGroup = 0
        Me.txtWHrsFrom.Flags = 0
        Me.txtWHrsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWHrsFrom.Location = New System.Drawing.Point(113, 180)
        Me.txtWHrsFrom.MaxDecimalPlaces = 2
        Me.txtWHrsFrom.MaxWholeDigits = 9
        Me.txtWHrsFrom.Name = "txtWHrsFrom"
        Me.txtWHrsFrom.Prefix = ""
        Me.txtWHrsFrom.RangeMax = 1.7976931348623157E+308
        Me.txtWHrsFrom.RangeMin = -1.7976931348623157E+308
        Me.txtWHrsFrom.Size = New System.Drawing.Size(95, 21)
        Me.txtWHrsFrom.TabIndex = 216
        Me.txtWHrsFrom.Text = "0"
        Me.txtWHrsFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnSearchPolicy
        '
        Me.objbtnSearchPolicy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPolicy.BorderSelected = False
        Me.objbtnSearchPolicy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPolicy.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPolicy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPolicy.Location = New System.Drawing.Point(378, 114)
        Me.objbtnSearchPolicy.Name = "objbtnSearchPolicy"
        Me.objbtnSearchPolicy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPolicy.TabIndex = 215
        '
        'lblPolicy
        '
        Me.lblPolicy.BackColor = System.Drawing.Color.Transparent
        Me.lblPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPolicy.Location = New System.Drawing.Point(8, 116)
        Me.lblPolicy.Name = "lblPolicy"
        Me.lblPolicy.Size = New System.Drawing.Size(99, 15)
        Me.lblPolicy.TabIndex = 213
        Me.lblPolicy.Text = "Policy"
        '
        'cboPolicy
        '
        Me.cboPolicy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPolicy.DropDownWidth = 120
        Me.cboPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPolicy.FormattingEnabled = True
        Me.cboPolicy.Location = New System.Drawing.Point(113, 114)
        Me.cboPolicy.Name = "cboPolicy"
        Me.cboPolicy.Size = New System.Drawing.Size(259, 21)
        Me.cboPolicy.TabIndex = 214
        '
        'objbtnSearchShift
        '
        Me.objbtnSearchShift.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchShift.BorderSelected = False
        Me.objbtnSearchShift.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchShift.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchShift.Location = New System.Drawing.Point(378, 87)
        Me.objbtnSearchShift.Name = "objbtnSearchShift"
        Me.objbtnSearchShift.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchShift.TabIndex = 211
        '
        'LblShift
        '
        Me.LblShift.BackColor = System.Drawing.Color.Transparent
        Me.LblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblShift.Location = New System.Drawing.Point(8, 89)
        Me.LblShift.Name = "LblShift"
        Me.LblShift.Size = New System.Drawing.Size(99, 15)
        Me.LblShift.TabIndex = 209
        Me.LblShift.Text = "Shift"
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.DropDownWidth = 120
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Location = New System.Drawing.Point(113, 87)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(259, 21)
        Me.cboShift.TabIndex = 210
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(378, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 207
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(214, 36)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(57, 15)
        Me.lblToDate.TabIndex = 175
        Me.lblToDate.Text = "To Date"
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(277, 33)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpToDate.TabIndex = 176
        '
        'lblEmpName
        '
        Me.lblEmpName.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(8, 62)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(99, 15)
        Me.lblEmpName.TabIndex = 167
        Me.lblEmpName.Text = "Employee"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(113, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(259, 21)
        Me.cboEmployee.TabIndex = 168
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(8, 36)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(99, 15)
        Me.lblFromDate.TabIndex = 151
        Me.lblFromDate.Text = "From Date"
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(113, 33)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpFromDate.TabIndex = 152
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 395)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(412, 63)
        Me.gbSortBy.TabIndex = 18
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(378, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(99, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(113, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(259, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'frmHolidayAttendanceReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 521)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbSortBy)
        Me.Name = "frmHolidayAttendanceReport"
        Me.Text = "frmHolidayAttendanceReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Public WithEvents lblToDate As System.Windows.Forms.Label
    Public WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Private WithEvents lblEmpName As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Public WithEvents lblFromDate As System.Windows.Forms.Label
    Public WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSearchShift As eZee.Common.eZeeGradientButton
    Private WithEvents LblShift As System.Windows.Forms.Label
    Public WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchPolicy As eZee.Common.eZeeGradientButton
    Private WithEvents lblPolicy As System.Windows.Forms.Label
    Public WithEvents cboPolicy As System.Windows.Forms.ComboBox
    Friend WithEvents chkDayOff As System.Windows.Forms.CheckBox
    Friend WithEvents objELLine1 As eZee.Common.eZeeLine
    Public WithEvents lblOT4HrsTo As System.Windows.Forms.Label
    Private WithEvents lblOT4HrsFrom As System.Windows.Forms.Label
    Friend WithEvents txtOT4HrsTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtOT4HrsFrom As eZee.TextBox.NumericTextBox
    Public WithEvents lblOT3HrsTo As System.Windows.Forms.Label
    Private WithEvents lblOT3HrsFrom As System.Windows.Forms.Label
    Friend WithEvents txtOT3HrsTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtOT3HrsFrom As eZee.TextBox.NumericTextBox
    Public WithEvents lblOT2HrsTo As System.Windows.Forms.Label
    Private WithEvents lblOT2HrsFrom As System.Windows.Forms.Label
    Friend WithEvents txtOT2HrsTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtOT2HrsFrom As eZee.TextBox.NumericTextBox
    Public WithEvents lblOT1HrsTo As System.Windows.Forms.Label
    Private WithEvents lblOT1HrsFrom As System.Windows.Forms.Label
    Friend WithEvents txtOT1HrsTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtOT1HrsFrom As eZee.TextBox.NumericTextBox
    Public WithEvents lblToHrs As System.Windows.Forms.Label
    Private WithEvents lblWorkeHrs As System.Windows.Forms.Label
    Friend WithEvents txtWHrsTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtWHrsFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Private WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
End Class
