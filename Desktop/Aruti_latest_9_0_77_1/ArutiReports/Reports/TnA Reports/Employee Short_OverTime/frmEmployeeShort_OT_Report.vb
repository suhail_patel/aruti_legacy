Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

'Last Message index= 1

Public Class frmEmployeeShort_OT_Report
    Private mstrModuleName As String = "frmEmployeeShort_OT_Report"
    Private objEmployeeTimeSheet As clsEmployeeShort_OT_Report

#Region "Constructor"
    Public Sub New()
        objEmployeeTimeSheet = New clsEmployeeShort_OT_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmployeeTimeSheet.SetDefaultValue()
        InitializeComponent()
        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End
    End Sub
#End Region

#Region "Private Variables"

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

    'Pinkal (30-Sep-2015) -- Start
    'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.
    Private mdtPYPeriodStartDate As Date = Nothing
    Private mdtPYPeriodEndDate As Date = Nothing
    Private mdtTnAPeriodStartDate As Date = Nothing
    Private mdtTnAPeriodEndDate As Date = Nothing
    'Pinkal (30-Sep-2015) -- End


#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Try
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Short Hours Report"))


            'Pinkal (30-Sep-2015) -- Start
            'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.
            If Company._Object._Countryunkid <> 162 AndAlso ConfigParameter._Object._PolicyManagementTNA = False Then  'Oman
                pnlFromToDate.Visible = True
                pnlTnAPeriod.Visible = False
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Over Time Report"))
            Else
                pnlFromToDate.Visible = False
                pnlTnAPeriod.Visible = True

                Dim objPeriod As New clscommom_period_Tran
                Dim dsFill As DataSet = Nothing
                dsFill = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date.Date, True)
                Dim drRow As DataRow = dsFill.Tables(0).NewRow
                drRow("periodunkid") = 0
                drRow("period_name") = "Select"
                dsFill.Tables(0).Rows.InsertAt(drRow, 0)

                cboTnAPeriod.ValueMember = "periodunkid"
                cboTnAPeriod.DisplayMember = "period_name"
                cboTnAPeriod.DataSource = dsFill.Tables(0)

            End If

            cboReportType.SelectedIndex = 0

            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            '   dsList = objEmp.GetEmployeeList("Employee", True, False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsList = objEmp.GetEmployeeList("Employee", True, False)
            'Else
            '    dsList = objEmp.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            'S.SANDEEP [04 JUN 2015] -- END


            'Pinkal (11-MAY-2012) -- End


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing
            dsList = Nothing

            Dim objShift As New clsNewshift_master
            dsList = objShift.getListForCombo("List", True)
            With cboShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            'Pinkal (30-Sep-2015) -- Start
            'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.
            cboTnAPeriod.SelectedValue = 0
            chkShowEachEmpOnNewPage.Checked = True
            'Pinkal (30-Sep-2015) -- End

            cboEmployee.SelectedValue = 0
            cboShift.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            txtFromhour.Text = ""
            txtTohour.Text = ""
            objEmployeeTimeSheet.setDefaultOrderBy(0)
            chkInactiveemp.Checked = False
            objEmployeeTimeSheet.setDefaultOrderBy(0)
            txtOrderBy.Text = objEmployeeTimeSheet.OrderByDisplay
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            'Pinkal (30-Sep-2015) -- Start
            'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.
            If Company._Object._Countryunkid = 162 Then 'Oman
                If CInt(cboTnAPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please select period."), enMsgBoxStyle.Information)
                    cboTnAPeriod.Select()
                    Return False
                End If
            End If
            'Pinkal (30-Sep-2015) -- End


            objEmployeeTimeSheet.SetDefaultValue()

            objEmployeeTimeSheet._ReportTypeId = cboReportType.SelectedIndex + 1
            objEmployeeTimeSheet._ReportTypeName = cboReportType.Text
            objEmployeeTimeSheet._EmpId = cboEmployee.SelectedValue
            objEmployeeTimeSheet._EmpName = cboEmployee.Text


            'Pinkal (30-Sep-2015) -- Start
            'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.
            If Company._Object._Countryunkid <> 162 Then ' Oman 
            objEmployeeTimeSheet._FromDate = dtpFromDate.Value.Date
            objEmployeeTimeSheet._ToDate = dtpToDate.Value.Date
            Else
                objEmployeeTimeSheet._FromDate = mdtTnAPeriodStartDate.Date
                objEmployeeTimeSheet._ToDate = mdtTnAPeriodEndDate.Date
                objEmployeeTimeSheet._PYPeriodStartDate = mdtPYPeriodStartDate.Date
                objEmployeeTimeSheet._PYPeriodEndDate = mdtPYPeriodEndDate.Date
            End If
            'Pinkal (30-Sep-2015) -- End

            If txtFromhour.Text <> "" Then
                Dim mintSec As Integer
                If txtFromhour.Text.Contains(".") Then
                    mintSec = CInt(txtFromhour.Text.Substring(0, txtFromhour.Text.IndexOf("."))) * 3600
                    mintSec += CInt(txtFromhour.Text.Substring(txtFromhour.Text.IndexOf(".") + 1)) * 60
                Else
                    mintSec = CInt(txtFromhour.Text) * 3600
                End If
                objEmployeeTimeSheet._Fromhrs = mintSec
            End If

            If txtTohour.Text <> "" Then
                Dim mintSec As Integer
                If txtTohour.Text.Contains(".") Then
                    mintSec = CInt(txtTohour.Text.Substring(0, txtTohour.Text.IndexOf("."))) * 3600
                    mintSec += CInt(txtTohour.Text.Substring(txtTohour.Text.IndexOf(".") + 1)) * 60
                Else
                    mintSec = CInt(txtTohour.Text) * 3600
                End If
                objEmployeeTimeSheet._Tohrs = mintSec
            End If


            objEmployeeTimeSheet._IsActive = chkInactiveemp.Checked
            objEmployeeTimeSheet._Shiftunkid = cboShift.SelectedValue
            objEmployeeTimeSheet._ShiftName = cboShift.Text
            objEmployeeTimeSheet._ViewByIds = mstrStringIds
            objEmployeeTimeSheet._ViewIndex = mintViewIdx
            objEmployeeTimeSheet._ViewByName = mstrStringName
            objEmployeeTimeSheet._Analysis_Fields = mstrAnalysis_Fields
            objEmployeeTimeSheet._Analysis_Join = mstrAnalysis_Join
            objEmployeeTimeSheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmployeeTimeSheet._Report_GroupName = mstrReport_GroupName
            objEmployeeTimeSheet._Advance_Filter = mstrAdvanceFilter


            'Pinkal (30-Sep-2015) -- Start
            'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.
            objEmployeeTimeSheet._CountryID = Company._Object._Countryunkid
            If pnlTnAPeriod.Visible Then
                objEmployeeTimeSheet._PeriodID = CInt(cboTnAPeriod.SelectedValue)
                objEmployeeTimeSheet._PeriodName = cboTnAPeriod.Text
            End If
            objEmployeeTimeSheet._ShowEachEmployeeOnNewPage = chkShowEachEmpOnNewPage.Checked
            'Pinkal (30-Sep-2015) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmDailyTimeSheet_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmployeeTimeSheet = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyTimeSheet_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmployeeTimeSheetReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Me._Title = objEmployeeTimeSheet._ReportName
            Me._Message = objEmployeeTimeSheet._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeTimeSheetReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmployeeTimeSheetReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeTimeSheetReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub frmEmployeeTimeSheetReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployeeTimeSheet.generateReport(0, e.Type, Aruti.Data.enExportAction.None)
            objEmployeeTimeSheet.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, e.Type, Aruti.Data.enExportAction.None)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeTimeSheetReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployeeTimeSheet.generateReport(0, enPrintAction.None, e.Type)
            objEmployeeTimeSheet.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeTimeSheetReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeTimeSheetReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmEmployeeTimeSheetReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeShort_OT_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeShort_OT_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEmployeeTimeSheetReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchShift.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboShift.DataSource
            frm.ValueMember = cboShift.ValueMember
            frm.DisplayMember = cboShift.DisplayMember
            If frm.DisplayDialog Then
                cboShift.SelectedValue = frm.SelectedValue
                cboShift.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchShift_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'Pinkal (30-Sep-2015) -- Start
    'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.

    Private Sub objBtnSearchTnAPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objBtnSearchTnAPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboTnAPeriod.DataSource
            frm.ValueMember = cboTnAPeriod.ValueMember
            frm.DisplayMember = cboTnAPeriod.DisplayMember
            If frm.DisplayDialog Then
                cboTnAPeriod.SelectedValue = frm.SelectedValue
                cboTnAPeriod.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnSearchTnAPeriod_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (30-Sep-2015) -- End


#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmployeeTimeSheet.setOrderBy(0)
            txtOrderBy.Text = objEmployeeTimeSheet.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region


    'Pinkal (30-Sep-2015) -- Start
    'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.

#Region "Combobox Event"

    Private Sub cboTnAPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTnAPeriod.SelectedIndexChanged
        Try
            If CInt(cboTnAPeriod.SelectedValue) <= 0 Then
                objTnAPeriod.Text = ""
                Exit Sub
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboTnAPeriod.SelectedValue)

            mdtPYPeriodStartDate = objPeriod._Start_Date
            mdtPYPeriodEndDate = objPeriod._End_Date

            mdtTnAPeriodStartDate = objPeriod._TnA_StartDate.Date
            mdtTnAPeriodEndDate = objPeriod._TnA_EndDate.Date

            objTnAPeriod.Text = objPeriod._TnA_StartDate.ToShortDateString() & Space(3) & "To" & Space(3) & objPeriod._TnA_EndDate.ToShortDateString()
            objPeriod = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTnAPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (30-Sep-2015) -- End




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lblToHour.Text = Language._Object.getCaption(Me.lblToHour.Name, Me.lblToHour.Text)
			Me.lblFromHrs.Text = Language._Object.getCaption(Me.lblFromHrs.Name, Me.lblFromHrs.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.Name, Me.LblShift.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.LblTnAPeriod.Text = Language._Object.getCaption(Me.LblTnAPeriod.Name, Me.LblTnAPeriod.Text)
            Me.LblDuration.Text = Language._Object.getCaption(Me.LblDuration.Name, Me.LblDuration.Text)
            Me.chkShowEachEmpOnNewPage.Text = Language._Object.getCaption(Me.chkShowEachEmpOnNewPage.Name, Me.chkShowEachEmpOnNewPage.Text)
            Me.pnlTnAPeriod.Text = Language._Object.getCaption(Me.pnlTnAPeriod.Name, Me.pnlTnAPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Short Hours Report")
			Language.setMessage(mstrModuleName, 2, "Over Time Report")
            Language.setMessage(mstrModuleName, 3, "Period is compulsory information.Please select period.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
