Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports


Public Class frmEmpTimesheetProjectSummaryReport


#Region "Private Variables"

    Private mstrModuleName As String = "frmEmpTimesheetProjectSummaryReport"
    Dim objEmpTimesheet As clsEmpBudgetTimesheetSummaryReport
    Private mstrReport_GroupName As String = ""
    Dim dvTrandhead As DataView = Nothing
    Dim mdtStartDate As DateTime = Nothing
    Dim mdtEndDate As DateTime = Nothing
    Dim mstrTranHeadIDs As String = ""
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""

#End Region

#Region "Constructor"

    Public Sub New()
        objEmpTimesheet = New clsEmpBudgetTimesheetSummaryReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpTimesheet.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try

            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "By Hours"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "By Percentage"))
            cboReportType.SelectedIndex = 0

            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date.Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objPeriod = Nothing

            dsList = Nothing
            Dim objEmp As New clsEmployee_Master
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing

            dsList = Nothing
            Dim objFundProject As New clsFundProjectCode
            dsList = objFundProject.GetComboList("Project", True, 0)
            With cboProjectCode
                .DisplayMember = "fundprojectname"
                .ValueMember = "fundprojectcodeunkid"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objFundProject = Nothing

            dsList = Nothing
         

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillTransactionHead()
        Dim dsList As DataSet = Nothing
        Try
            Dim objTransaction As New clsTransactionHead
            Dim mstrFilter As String = "trnheadtype_id in (" & enTranHeadType.EarningForEmployees & "," & enTranHeadType.DeductionForEmployee & ")"
            dsList = objTransaction.getComboList(FinancialYear._Object._DatabaseName, "List", False, 0, 0, -1, False, False, mstrFilter)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("ischeck") = False Then
                Dim dcColumn As New DataColumn("ischeck")
                dcColumn.DataType = GetType(System.Boolean)
                dcColumn.DefaultValue = False
                dsList.Tables(0).Columns.Add(dcColumn)
            End If

            dvTrandhead = dsList.Tables(0).DefaultView

            dgTransactionHead.AutoGenerateColumns = False
            objdgcolhSelect.DataPropertyName = "ischeck"
            dgcolhTranheadcode.DataPropertyName = "code"
            dgcolhTransactionHead.DataPropertyName = "name"
            objdgcolhTranHeadID.DataPropertyName = "tranheadunkid"
            dgTransactionHead.DataSource = dvTrandhead

            GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillTransactionHead", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboPeriod.SelectedIndex = 0
            cboProjectCode.SelectedIndex = 0
            chkShowReportInHtml.Checked = False
            txtTransactionHeadFilter.Text = ""
            objSelectAll.Checked = False
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            FillTransactionHead()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select period."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
                cboPeriod.Focus()
                Return False
            End If

            objEmpTimesheet.SetDefaultValue()


            objEmpTimesheet._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objEmpTimesheet._ReportTypeName = cboReportType.Text

            objEmpTimesheet._EmployeeID = CInt(cboEmployee.SelectedValue)
            objEmpTimesheet._EmployeeName = cboEmployee.Text

            Dim dRow As DataRowView = CType(cboEmployee.SelectedItem, DataRowView)
            If dRow IsNot Nothing AndAlso dRow.DataView.Count > 0 Then
                objEmpTimesheet._EmployeeCode = dRow("employeecode").ToString()
            End If

            Dim lstID As List(Of String) = (From p In CType(dgTransactionHead.DataSource, DataView) Where CBool(p("ischeck")) = True Select CStr(p("tranheadunkid"))).ToList()
            If lstID.Count > 0 Then objEmpTimesheet._TransactionHeadIDs = String.Join(",", lstID.ToArray)

            objEmpTimesheet._ProjectCodeId = CInt(cboProjectCode.SelectedValue)
            objEmpTimesheet._ProjectCode = cboProjectCode.Text

            objEmpTimesheet._PeriodId = CInt(cboPeriod.SelectedValue)
            objEmpTimesheet._PeriodName = cboPeriod.Text

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'objEmpTimesheet._StartDate = objPeriod._Start_Date.Date
            'objEmpTimesheet._EndDate = objPeriod._End_Date.Date
            'mdtStartDate = objPeriod._Start_Date.Date
            'mdtEndDate = objPeriod._End_Date.Date
            objEmpTimesheet._StartDate = objPeriod._TnA_StartDate.Date
            objEmpTimesheet._EndDate = objPeriod._TnA_EndDate.Date
            mdtStartDate = objPeriod._TnA_StartDate.Date
            mdtEndDate = objPeriod._TnA_EndDate.Date

            objPeriod = Nothing

            objEmpTimesheet._ViewReportInHTML = chkShowReportInHtml.Checked


            objEmpTimesheet._ViewByIds = mstrViewByIds
            objEmpTimesheet._ViewIndex = mintViewIndex
            objEmpTimesheet._ViewByName = mstrViewByName
            objEmpTimesheet._Analysis_Fields = mstrAnalysis_Fields
            objEmpTimesheet._Analysis_Join = mstrAnalysis_Join
            objEmpTimesheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpTimesheet._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objEmpTimesheet._Report_GroupName = mstrReport_GroupName

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            objEmpTimesheet._ShowEmpIdentifyNo = chkShowEmpIdentifyNo.Checked
            objEmpTimesheet._ShowLeaveTypeHours = chkShowLeaveTypeHrs.Checked
            objEmpTimesheet._ShowHolidayHours = chkShowHolidayHrs.Checked
            If chkShowExtraHrs.Visible Then
                objEmpTimesheet._ShowExtraHours = chkShowExtraHrs.Checked
            Else
                objEmpTimesheet._ShowExtraHours = False
            End If
            'Pinkal (28-Jul-2018) -- End

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            objEmpTimesheet._IgnoreZeroLeaveTypeHrs = chkIgnoreZeroLeaveTypehrs.Checked
            'Pinkal (13-Aug-2018) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet

        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Employee_Timesheet_Project_Summary_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                If dsList.Tables(0).Rows(0)("transactionheadid").ToString().Trim.Length > 0 Then
                    If dsList.Tables(0).Rows(0)("transactionheadid").ToString().Contains("|") Then
                        mstrTranHeadIDs = dsList.Tables(0).Rows(0)("transactionheadid").ToString().Substring(0, dsList.Tables(0).Rows(0)("transactionheadid").ToString().IndexOf("|"))
                        Dim ar() As String = dsList.Tables(0).Rows(0)("transactionheadid").ToString().Trim.Substring(dsList.Tables(0).Rows(0)("transactionheadid").ToString().IndexOf("|") + 1).Split("|")
                        If ar.Length > 0 Then
                            chkShowReportInHtml.Checked = CBool(ar(0))
                            chkShowEmpIdentifyNo.Checked = CBool(ar(1))
                            chkShowLeaveTypeHrs.Checked = CBool(ar(2))
                            chkShowHolidayHrs.Checked = CBool(ar(3))
                            chkShowExtraHrs.Checked = CBool(ar(4))

                            'Pinkal (13-Aug-2018) -- Start
                            'Enhancement - Changes For PACT [Ref #249,252]
                            chkIgnoreZeroLeaveTypehrs.Checked = CBool(ar(5))
                            'Pinkal (13-Aug-2018) -- End

                        End If
                    Else
                mstrTranHeadIDs = dsList.Tables(0).Rows(0)("transactionheadid").ToString()
                    End If
                    If mstrTranHeadIDs.Trim.Length > 0 Then
                Dim drRow() As DataRow = dvTrandhead.Table.Select("tranheadunkid in (" & mstrTranHeadIDs & ")")
                If drRow.Length > 0 Then
                    For Each dr As DataRow In drRow
                        dr("ischeck") = True
                        dr.AcceptChanges()
                    Next
                    dvTrandhead.Table.AcceptChanges()
                    SetCheckBoxValue()
                End If
            End If
                End If
                'Pinkal (28-Jul-2018) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = dvTrandhead.ToTable.Select("ischeck = true")

            RemoveHandler objSelectAll.CheckedChanged, AddressOf objSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                objSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgTransactionHead.Rows.Count Then
                objSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgTransactionHead.Rows.Count Then
                objSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objSelectAll.CheckedChanged, AddressOf objSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub


    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Private Sub SetVisibility(ByVal blnFlag As Boolean)
        Try
            chkShowHolidayHrs.Visible = blnFlag
            chkShowLeaveTypeHrs.Visible = blnFlag
            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            chkIgnoreZeroLeaveTypehrs.Visible = blnFlag
            'Pinkal (13-Aug-2018) -- End
            chkShowExtraHrs.Visible = blnFlag
            If blnFlag AndAlso ConfigParameter._Object._AllowOverTimeToEmpTimesheet Then
                chkShowExtraHrs.Visible = blnFlag
            Else
                chkShowExtraHrs.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Pinkal (28-Jul-2018) -- End


#End Region

#Region "Form's Events"

    Private Sub frmEmpTimesheetProjectSummaryReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpTimesheet = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpTimesheetProjectSummaryReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpTimesheetProjectSummaryReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objEmpTimesheet._ReportName
            eZeeHeader.Message = objEmpTimesheet._ReportDesc
            Call FillCombo()
            Call ResetValue()
            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            SetVisibility(True)
            'Pinkal (28-Jul-2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpTimesheetProjectSummaryReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpTimesheetProjectSummaryReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    'Call frmLvForm_ApprovalStatus_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpTimesheetProjectSummaryReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpTimesheetProjectSummaryReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpTimesheetProjectSummaryReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmpBudgetTimesheetReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpBudgetTimesheetReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select period."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
                cboPeriod.Focus()
                Exit Sub
            End If

            If SetFilter() = False Then Exit Sub

            objEmpTimesheet.Generate_EmployeeSummaryReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, mdtStartDate, mdtEndDate, _
                                                  ConfigParameter._Object._UserAccessModeSetting, True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboPeriod
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchProjectCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchProjectCode.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboProjectCode
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchProjectCode_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Link button Events"

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Dim mblnFlag As Boolean = False
        Try

            Dim lstID As List(Of String) = (From p In CType(dgTransactionHead.DataSource, DataView) Where CBool(p("ischeck")) = True Select CStr(p("tranheadunkid"))).ToList()

            If lstID.Count > 0 Then
                mstrTranHeadIDs = String.Join(",", lstID.ToArray)
            Else
                mstrTranHeadIDs = ""
            End If

                objUserDefRMode._Reportunkid = enArutiReport.Employee_Timesheet_Project_Summary_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0
                objUserDefRMode._Headtypeid = 1

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            'objUserDefRMode._EarningTranHeadIds = mstrTranHeadIDs & "|" & IIf(chkShowReportInHtml.Checked, 1, 0) & "|" & IIf(chkShowEmpIdentifyNo.Checked, 1, 0) & "|" & IIf(chkShowLeaveTypeHrs.Checked, 1, 0) & "|" & IIf(chkShowHolidayHrs.Checked, 1, 0) & "|" & IIf(chkShowExtraHrs.Checked, 1, 0) 
            objUserDefRMode._EarningTranHeadIds = mstrTranHeadIDs & "|" & IIf(chkShowReportInHtml.Checked, 1, 0) & "|" & IIf(chkShowEmpIdentifyNo.Checked, 1, 0) & "|" & IIf(chkShowLeaveTypeHrs.Checked, 1, 0) & "|" & IIf(chkShowHolidayHrs.Checked, 1, 0) & "|" & IIf(chkShowExtraHrs.Checked, 1, 0) & "|" & IIf(chkIgnoreZeroLeaveTypehrs.Checked, 1, 0)
            'Pinkal (13-Aug-2018) -- End

            'Pinkal (28-Jul-2018) -- End

                intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Timesheet_Project_Summary_Report, 0, 0, objUserDefRMode._Headtypeid)
                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    mblnFlag = objUserDefRMode.Insert()
                Else
                    mblnFlag = objUserDefRMode.Update()
                End If

                If mblnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully"), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(objUserDefRMode._Message)
                    Exit Sub
                End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrViewByIds = frm._ReportBy_Ids
            mstrViewByName = frm._ReportBy_Name
            mintViewIndex = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub


#End Region

#Region "Datagrid Events"

    Private Sub dgTransactionHead_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgTransactionHead.CellContentClick, dgTransactionHead.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhSelect.Index Then

                If dgTransactionHead.IsCurrentCellDirty Then
                    dgTransactionHead.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                dvTrandhead.Table.AcceptChanges()

                SetCheckBoxValue()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgTransactionHead_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox Events"

    Private Sub txtTransactionHeadFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTransactionHeadFilter.TextChanged
        Try
            If dvTrandhead Is Nothing Then Exit Sub
            dvTrandhead.RowFilter = "code  LIKE  '%" & txtTransactionHeadFilter.Text.Trim & "%' OR name LIKE  '%" & txtTransactionHeadFilter.Text.Trim & "%'"

            SetCheckBoxValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtTransactionHeadFilter_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Checkbox Events"

    Private Sub objSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSelectAll.CheckedChanged
        Try

            If dvTrandhead IsNot Nothing AndAlso dvTrandhead.Table.Rows.Count > 0 Then

                RemoveHandler dgTransactionHead.CellContentClick, AddressOf dgTransactionHead_CellContentClick
                For Each dr As DataRowView In dvTrandhead
                    dr("ischeck") = objSelectAll.Checked
                Next
                AddHandler dgTransactionHead.CellContentClick, AddressOf dgTransactionHead_CellContentClick
                dvTrandhead.Table.AcceptChanges()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

#Region "Combobox Events"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex = 0 Then
                SetVisibility(True)
            Else
                SetVisibility(False)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (28-Jul-2018) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.chkShowReportInHtml.Text = Language._Object.getCaption(Me.chkShowReportInHtml.Name, Me.chkShowReportInHtml.Text)
            Me.LblProjectCode.Text = Language._Object.getCaption(Me.LblProjectCode.Name, Me.LblProjectCode.Text)
            Me.LblReportType.Text = Language._Object.getCaption(Me.LblReportType.Name, Me.LblReportType.Text)
            Me.LblTransactionHead.Text = Language._Object.getCaption(Me.LblTransactionHead.Name, Me.LblTransactionHead.Text)
            Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)
            Me.dgcolhTranheadcode.HeaderText = Language._Object.getCaption(Me.dgcolhTranheadcode.Name, Me.dgcolhTranheadcode.HeaderText)
            Me.dgcolhTransactionHead.HeaderText = Language._Object.getCaption(Me.dgcolhTransactionHead.Name, Me.dgcolhTransactionHead.HeaderText)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.chkShowEmpIdentifyNo.Text = Language._Object.getCaption(Me.chkShowEmpIdentifyNo.Name, Me.chkShowEmpIdentifyNo.Text)
			Me.chkShowHolidayHrs.Text = Language._Object.getCaption(Me.chkShowHolidayHrs.Name, Me.chkShowHolidayHrs.Text)
			Me.chkShowLeaveTypeHrs.Text = Language._Object.getCaption(Me.chkShowLeaveTypeHrs.Name, Me.chkShowLeaveTypeHrs.Text)
			Me.chkShowExtraHrs.Text = Language._Object.getCaption(Me.chkShowExtraHrs.Name, Me.chkShowExtraHrs.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "By Hours")
            Language.setMessage(mstrModuleName, 2, "By Percentage")
            Language.setMessage(mstrModuleName, 3, "Period is compulsory information.Please Select period.")
            Language.setMessage(mstrModuleName, 4, "Selection Saved Successfully")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


  

  
End Class
