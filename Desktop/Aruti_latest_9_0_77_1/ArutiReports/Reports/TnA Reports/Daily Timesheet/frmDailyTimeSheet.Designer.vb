﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDailyTimeSheet
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboShiftType = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchShift = New eZee.Common.eZeeGradientButton
        Me.LblReportType = New System.Windows.Forms.Label
        Me.LblShift = New System.Windows.Forms.Label
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblDate = New System.Windows.Forms.Label
        Me.dtDate = New System.Windows.Forms.DateTimePicker
        Me.txtWorkedHourTo = New eZee.TextBox.NumericTextBox
        Me.lblWorkedHourTo = New System.Windows.Forms.Label
        Me.txtWorkedHourFrom = New eZee.TextBox.NumericTextBox
        Me.lblWorkedHourFrom = New System.Windows.Forms.Label
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbShowColumns = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowEmployeeStatus = New System.Windows.Forms.CheckBox
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.cboReportColumn = New System.Windows.Forms.ComboBox
        Me.lnkSaveChanges = New System.Windows.Forms.LinkLabel
        Me.lblShiftType = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.gbShowColumns.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 512)
        Me.NavPanel.Size = New System.Drawing.Size(767, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblShiftType)
        Me.gbFilterCriteria.Controls.Add(Me.cboShiftType)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchShift)
        Me.gbFilterCriteria.Controls.Add(Me.LblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.LblShift)
        Me.gbFilterCriteria.Controls.Add(Me.cboShift)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmpName)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtDate)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 65)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(381, 172)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboShiftType
        '
        Me.cboShiftType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShiftType.DropDownWidth = 120
        Me.cboShiftType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShiftType.FormattingEnabled = True
        Me.cboShiftType.Location = New System.Drawing.Point(82, 87)
        Me.cboShiftType.Name = "cboShiftType"
        Me.cboShiftType.Size = New System.Drawing.Size(261, 21)
        Me.cboShiftType.TabIndex = 214
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(284, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 22
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchShift
        '
        Me.objbtnSearchShift.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchShift.BorderSelected = False
        Me.objbtnSearchShift.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchShift.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchShift.Location = New System.Drawing.Point(349, 114)
        Me.objbtnSearchShift.Name = "objbtnSearchShift"
        Me.objbtnSearchShift.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchShift.TabIndex = 209
        '
        'LblReportType
        '
        Me.LblReportType.BackColor = System.Drawing.Color.Transparent
        Me.LblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblReportType.Location = New System.Drawing.Point(8, 36)
        Me.LblReportType.Name = "LblReportType"
        Me.LblReportType.Size = New System.Drawing.Size(68, 15)
        Me.LblReportType.TabIndex = 211
        Me.LblReportType.Text = "Report Type"
        '
        'LblShift
        '
        Me.LblShift.BackColor = System.Drawing.Color.Transparent
        Me.LblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblShift.Location = New System.Drawing.Point(8, 118)
        Me.LblShift.Name = "LblShift"
        Me.LblShift.Size = New System.Drawing.Size(68, 15)
        Me.LblShift.TabIndex = 207
        Me.LblShift.Text = "Shift"
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.DropDownWidth = 120
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Location = New System.Drawing.Point(82, 114)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(261, 21)
        Me.cboShift.TabIndex = 208
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 120
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(82, 33)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(261, 21)
        Me.cboReportType.TabIndex = 212
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(349, 141)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 205
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(193, 63)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(164, 17)
        Me.chkInactiveemp.TabIndex = 203
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lblEmpName
        '
        Me.lblEmpName.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(8, 145)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(68, 15)
        Me.lblEmpName.TabIndex = 167
        Me.lblEmpName.Text = "Employee"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(82, 141)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(261, 21)
        Me.cboEmployee.TabIndex = 168
        '
        'lblDate
        '
        Me.lblDate.BackColor = System.Drawing.Color.Transparent
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(8, 69)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(68, 15)
        Me.lblDate.TabIndex = 151
        Me.lblDate.Text = "Date"
        '
        'dtDate
        '
        Me.dtDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtDate.Location = New System.Drawing.Point(82, 60)
        Me.dtDate.Name = "dtDate"
        Me.dtDate.Size = New System.Drawing.Size(105, 21)
        Me.dtDate.TabIndex = 152
        '
        'txtWorkedHourTo
        '
        Me.txtWorkedHourTo.AllowNegative = False
        Me.txtWorkedHourTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtWorkedHourTo.DigitsInGroup = 0
        Me.txtWorkedHourTo.Flags = 65536
        Me.txtWorkedHourTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkedHourTo.Location = New System.Drawing.Point(265, 485)
        Me.txtWorkedHourTo.MaxDecimalPlaces = 4
        Me.txtWorkedHourTo.MaxWholeDigits = 9
        Me.txtWorkedHourTo.Name = "txtWorkedHourTo"
        Me.txtWorkedHourTo.Prefix = ""
        Me.txtWorkedHourTo.RangeMax = 1.7976931348623157E+308
        Me.txtWorkedHourTo.RangeMin = -1.7976931348623157E+308
        Me.txtWorkedHourTo.Size = New System.Drawing.Size(101, 21)
        Me.txtWorkedHourTo.TabIndex = 173
        Me.txtWorkedHourTo.Text = "0"
        Me.txtWorkedHourTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtWorkedHourTo.Visible = False
        '
        'lblWorkedHourTo
        '
        Me.lblWorkedHourTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkedHourTo.Location = New System.Drawing.Point(192, 489)
        Me.lblWorkedHourTo.Name = "lblWorkedHourTo"
        Me.lblWorkedHourTo.Size = New System.Drawing.Size(66, 13)
        Me.lblWorkedHourTo.TabIndex = 172
        Me.lblWorkedHourTo.Text = "To"
        Me.lblWorkedHourTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblWorkedHourTo.Visible = False
        '
        'txtWorkedHourFrom
        '
        Me.txtWorkedHourFrom.AllowNegative = False
        Me.txtWorkedHourFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtWorkedHourFrom.DigitsInGroup = 0
        Me.txtWorkedHourFrom.Flags = 65536
        Me.txtWorkedHourFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkedHourFrom.Location = New System.Drawing.Point(85, 485)
        Me.txtWorkedHourFrom.MaxDecimalPlaces = 4
        Me.txtWorkedHourFrom.MaxWholeDigits = 9
        Me.txtWorkedHourFrom.Name = "txtWorkedHourFrom"
        Me.txtWorkedHourFrom.Prefix = ""
        Me.txtWorkedHourFrom.RangeMax = 1.7976931348623157E+308
        Me.txtWorkedHourFrom.RangeMin = -1.7976931348623157E+308
        Me.txtWorkedHourFrom.Size = New System.Drawing.Size(101, 21)
        Me.txtWorkedHourFrom.TabIndex = 171
        Me.txtWorkedHourFrom.Text = "0"
        Me.txtWorkedHourFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtWorkedHourFrom.Visible = False
        '
        'lblWorkedHourFrom
        '
        Me.lblWorkedHourFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkedHourFrom.Location = New System.Drawing.Point(17, 489)
        Me.lblWorkedHourFrom.Name = "lblWorkedHourFrom"
        Me.lblWorkedHourFrom.Size = New System.Drawing.Size(68, 13)
        Me.lblWorkedHourFrom.TabIndex = 170
        Me.lblWorkedHourFrom.Text = "Working Hr."
        Me.lblWorkedHourFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblWorkedHourFrom.Visible = False
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 243)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(381, 63)
        Me.gbSortBy.TabIndex = 16
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(349, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(68, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(76, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(267, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'gbShowColumns
        '
        Me.gbShowColumns.BorderColor = System.Drawing.Color.Black
        Me.gbShowColumns.Checked = False
        Me.gbShowColumns.CollapseAllExceptThis = False
        Me.gbShowColumns.CollapsedHoverImage = Nothing
        Me.gbShowColumns.CollapsedNormalImage = Nothing
        Me.gbShowColumns.CollapsedPressedImage = Nothing
        Me.gbShowColumns.CollapseOnLoad = False
        Me.gbShowColumns.Controls.Add(Me.chkShowEmployeeStatus)
        Me.gbShowColumns.Controls.Add(Me.lblAllocation)
        Me.gbShowColumns.Controls.Add(Me.cboReportColumn)
        Me.gbShowColumns.Controls.Add(Me.lnkSaveChanges)
        Me.gbShowColumns.ExpandedHoverImage = Nothing
        Me.gbShowColumns.ExpandedNormalImage = Nothing
        Me.gbShowColumns.ExpandedPressedImage = Nothing
        Me.gbShowColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbShowColumns.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbShowColumns.HeaderHeight = 25
        Me.gbShowColumns.HeaderMessage = ""
        Me.gbShowColumns.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbShowColumns.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbShowColumns.HeightOnCollapse = 0
        Me.gbShowColumns.LeftTextSpace = 0
        Me.gbShowColumns.Location = New System.Drawing.Point(399, 65)
        Me.gbShowColumns.Name = "gbShowColumns"
        Me.gbShowColumns.OpenHeight = 300
        Me.gbShowColumns.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbShowColumns.ShowBorder = True
        Me.gbShowColumns.ShowCheckBox = False
        Me.gbShowColumns.ShowCollapseButton = False
        Me.gbShowColumns.ShowDefaultBorderColor = True
        Me.gbShowColumns.ShowDownButton = False
        Me.gbShowColumns.ShowHeader = True
        Me.gbShowColumns.Size = New System.Drawing.Size(329, 94)
        Me.gbShowColumns.TabIndex = 175
        Me.gbShowColumns.Temp = 0
        Me.gbShowColumns.Text = "Column(s) Display"
        Me.gbShowColumns.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowEmployeeStatus
        '
        Me.chkShowEmployeeStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmployeeStatus.Location = New System.Drawing.Point(114, 66)
        Me.chkShowEmployeeStatus.Name = "chkShowEmployeeStatus"
        Me.chkShowEmployeeStatus.Size = New System.Drawing.Size(200, 17)
        Me.chkShowEmployeeStatus.TabIndex = 215
        Me.chkShowEmployeeStatus.Text = "Show Employee Status"
        Me.chkShowEmployeeStatus.UseVisualStyleBackColor = True
        '
        'lblAllocation
        '
        Me.lblAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(13, 41)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(95, 15)
        Me.lblAllocation.TabIndex = 213
        Me.lblAllocation.Text = "Display On Report"
        '
        'cboReportColumn
        '
        Me.cboReportColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportColumn.DropDownWidth = 120
        Me.cboReportColumn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportColumn.FormattingEnabled = True
        Me.cboReportColumn.Location = New System.Drawing.Point(114, 38)
        Me.cboReportColumn.Name = "cboReportColumn"
        Me.cboReportColumn.Size = New System.Drawing.Size(200, 21)
        Me.cboReportColumn.TabIndex = 214
        '
        'lnkSaveChanges
        '
        Me.lnkSaveChanges.BackColor = System.Drawing.Color.Transparent
        Me.lnkSaveChanges.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSaveChanges.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSaveChanges.Location = New System.Drawing.Point(214, 4)
        Me.lnkSaveChanges.Name = "lnkSaveChanges"
        Me.lnkSaveChanges.Size = New System.Drawing.Size(112, 17)
        Me.lnkSaveChanges.TabIndex = 23
        Me.lnkSaveChanges.TabStop = True
        Me.lnkSaveChanges.Text = "Save Selection"
        Me.lnkSaveChanges.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblShiftType
        '
        Me.lblShiftType.BackColor = System.Drawing.Color.Transparent
        Me.lblShiftType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShiftType.Location = New System.Drawing.Point(8, 90)
        Me.lblShiftType.Name = "lblShiftType"
        Me.lblShiftType.Size = New System.Drawing.Size(68, 15)
        Me.lblShiftType.TabIndex = 216
        Me.lblShiftType.Text = "Shift Type"
        '
        'frmDailyTimeSheet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 567)
        Me.Controls.Add(Me.gbShowColumns)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.txtWorkedHourTo)
        Me.Controls.Add(Me.lblWorkedHourFrom)
        Me.Controls.Add(Me.txtWorkedHourFrom)
        Me.Controls.Add(Me.lblWorkedHourTo)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDailyTimeSheet"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Daily TimeSheet"
        Me.Controls.SetChildIndex(Me.lblWorkedHourTo, 0)
        Me.Controls.SetChildIndex(Me.txtWorkedHourFrom, 0)
        Me.Controls.SetChildIndex(Me.lblWorkedHourFrom, 0)
        Me.Controls.SetChildIndex(Me.txtWorkedHourTo, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbShowColumns, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbShowColumns.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblEmpName As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Public WithEvents lblDate As System.Windows.Forms.Label
    Public WithEvents dtDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents txtWorkedHourTo As eZee.TextBox.NumericTextBox
    Friend WithEvents lblWorkedHourTo As System.Windows.Forms.Label
    Friend WithEvents txtWorkedHourFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents lblWorkedHourFrom As System.Windows.Forms.Label
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchShift As eZee.Common.eZeeGradientButton
    Private WithEvents LblShift As System.Windows.Forms.Label
    Public WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Private WithEvents LblReportType As System.Windows.Forms.Label
    Public WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents gbShowColumns As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkShowEmployeeStatus As System.Windows.Forms.CheckBox
    Private WithEvents lblAllocation As System.Windows.Forms.Label
    Public WithEvents cboReportColumn As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSaveChanges As System.Windows.Forms.LinkLabel
    Public WithEvents cboShiftType As System.Windows.Forms.ComboBox
    Private WithEvents lblShiftType As System.Windows.Forms.Label
End Class
