'************************************************************************************************************************************
'Class Name : clsDailyTimeSheet.vb
'Purpose    :
'Date       :28/12/2010
'Written By :Vimal M. Gohil
'Modified   :Sandeep Sharma <04-OCT-2016>
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Vimal M. Gohil
''' Modified:Sandeep Sharma - 04-OCT-2016
''' </summary>
Public Class clsDailyTimeSheet
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsDailyTimeSheet"
    Private mstrReportId As String = enArutiReport.DailyTimeSheet
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
        'S.SANDEEP |29-MAR-2019| -- START
        'ENHANCEMENT : 0003630 {PAPAYE}.
        Call Create_ManualAttDetailReport()
        'S.SANDEEP |29-MAR-2019| -- END
    End Sub

#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mdtDate As DateTime = Nothing
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mstrWorkedHourFrom As String = ""
    Private mstrWorkedHourTo As String = ""
    Private mstrOrderByQuery As String = ""
    Private mblnIsActive As Boolean = True
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mintShiftunkid As Integer = 0
    Private mstrShiftName As String = ""
    Private mintSelectedAllocationId As Integer = 0
    Private mblnShowEmployeeStatus As Boolean = True
    Private mstrAllocationName As String = String.Empty
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (26-Nov-2018) -- Start
    'Enhancement - Working on Papaye Manual Attendance Register Report.
    Private mstrAdvanceFilter As String = ""
    'Pinkal (26-Nov-2018) -- End

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003630 {PAPAYE}.
    Private mintShiftTypeId As Integer = 0
    Private mstrShiftTypeName As String = String.Empty
    'S.SANDEEP |29-MAR-2019| -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _Date() As DateTime
        Set(ByVal value As DateTime)
            mdtDate = value
        End Set
    End Property

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _WorkedHourFrom() As String
        Set(ByVal value As String)
            mstrWorkedHourFrom = value
        End Set
    End Property

    Public WriteOnly Property _WorkedHourTo() As String
        Set(ByVal value As String)
            mstrWorkedHourTo = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _Shiftunkid() As Integer
        Set(ByVal value As Integer)
            mintShiftunkid = value
        End Set
    End Property

    Public WriteOnly Property _ShiftName() As String
        Set(ByVal value As String)
            mstrShiftName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _SelectedAllocationId() As Integer
        Set(ByVal value As Integer)
            mintSelectedAllocationId = value
        End Set
    End Property

    Public WriteOnly Property _ShowEmployeeStatus() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmployeeStatus = value
        End Set
    End Property

    Public WriteOnly Property _AllocationName() As String
        Set(ByVal value As String)
            mstrAllocationName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (26-Nov-2018) -- Start
    'Enhancement - Working on Papaye Manual Attendance Register Report.
    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property
    'Pinkal (26-Nov-2018) -- End

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003630 {PAPAYE}.
    Public WriteOnly Property _ShiftTypeId() As Integer
        Set(ByVal value As Integer)
            mintShiftTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ShiftTypeName() As String
        Set(ByVal value As String)
            mstrShiftTypeName = value
        End Set
    End Property
    'S.SANDEEP |29-MAR-2019| -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mdtDate = Nothing
            mintEmpId = 0
            mstrEmpName = ""
            mstrWorkedHourFrom = ""
            mstrWorkedHourTo = ""
            mstrOrderByQuery = ""
            mblnIsActive = True
            mintShiftunkid = 0
            mstrShiftName = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mintSelectedAllocationId = 0
            mblnShowEmployeeStatus = True
            mstrAllocationName = String.Empty
            Rpt = Nothing
            mintUserUnkid = -1
            mintCompanyUnkid = -1

            'Pinkal (26-Nov-2018) -- Start
            'Enhancement - Working on Papaye Manual Attendance Register Report.
            mstrAdvanceFilter = ""
            'Pinkal (26-Nov-2018) -- End

            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            mintShiftTypeId = 0
            mstrShiftTypeName = ""
            'S.SANDEEP |29-MAR-2019| -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try
            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, " Date: ") & " " & mdtDate.ToShortDateString & " "

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                'Pinkal (28-Dec-2018) -- Start
                'Bug - Worked on shift filter is not runing on Manual Attendance Register report for Papaye.
                'If mintReportTypeId <> 2 Then

                'Pinkal (16-Jul-2021)-- Start
                'Bug Resolved in Daily Timesheet Report.
                'If mintReportTypeId <> 2 AndAlso mintReportTypeId <> 3 Then
                If mintReportTypeId = 1 Then
                    'Pinkal (16-Jul-2021) -- End
                    'Pinkal (28-Dec-2018) -- End
                    Me._FilterQuery &= " AND tnalogin_tran.employeeunkid = @EmpId "
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, " Employee : ") & " " & mstrEmpName & " "
            End If

            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            If mintShiftTypeId > 0 Then
                objDataOperation.AddParameter("@ShiftTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftTypeId)
                If mintReportTypeId = 3 Then
                    Me._FilterQuery &= " AND Emp.shifttypeunkid = @ShiftTypeId "
                Else
                    Me._FilterQuery &= " AND tnashift_master.shifttypeunkid = @ShiftTypeId "
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 42, " Shift Type : ") & " " & mstrShiftTypeName & " "
            End If
            'S.SANDEEP |29-MAR-2019| -- END

            If mintShiftunkid > 0 Then
                objDataOperation.AddParameter("@ShiftId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid)
                If mintReportTypeId = 0 Then  'Daily Timesheet
                    Me._FilterQuery &= " AND tnashift_master.shiftunkid = @ShiftId "
                ElseIf mintReportTypeId = 1 Then 'On Duty Report
                    Me._FilterQuery &= " AND A.shiftunkid = @ShiftId "
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, " Shift : ") & " " & mstrShiftName & " "
            End If

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            If mintReportTypeId = 0 Then
                objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            ElseIf mintReportTypeId = 1 Then
                objRpt = Generate_OnDutyReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            ElseIf mintReportTypeId = 2 Then
                objRpt = Generate_OffDutyReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                'Pinkal (26-Nov-2018) -- Start
                'Enhancement - Working on Papaye Manual Attendance Register Report.
            ElseIf mintReportTypeId = 3 Then
                objRpt = Generate_ManualAttendanceRegisterReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                'Pinkal (26-Nov-2018) -- End
            End If

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            'OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            'OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
            Select Case intReportType
                Case 0
                    OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
                Case 3
                    OrderByDisplay = iColumn_ManualAttDetailReport.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_ManualAttDetailReport.ColumnItem(0).Name
            End Select
            'S.SANDEEP |29-MAR-2019| -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            'Call OrderByExecute(iColumn_DetailReport)
            Select Case intReportType
                Case 0  'Generate_DetailReport
                    Call OrderByExecute(iColumn_DetailReport)
                Case 3  'Manual Attendance Register
                    Call OrderByExecute(iColumn_ManualAttDetailReport)
            End Select
            'S.SANDEEP |29-MAR-2019| -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 1, "Employee")))
            iColumn_DetailReport.Add(New IColumn("InTime", Language.getMessage(mstrModuleName, 2, "In Time")))
            iColumn_DetailReport.Add(New IColumn("OutTime", Language.getMessage(mstrModuleName, 3, "Out Time")))
            iColumn_DetailReport.Add(New IColumn("BreakHour", Language.getMessage(mstrModuleName, 4, "Break")))
            iColumn_DetailReport.Add(New IColumn("WorkingHours", Language.getMessage(mstrModuleName, 5, "Working Hours")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003630 {PAPAYE}.
    Dim iColumn_ManualAttDetailReport As New IColumnCollection
    Public Property Field_ManualAttDetailReport() As IColumnCollection
        Get
            Return iColumn_ManualAttDetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_ManualAttDetailReport = value
        End Set
    End Property
    Private Sub Create_ManualAttDetailReport()
        Try
            iColumn_ManualAttDetailReport.Clear()
            iColumn_ManualAttDetailReport.Add(New IColumn("shiftname", Language.getMessage(mstrModuleName, 36, "Shift")))
            iColumn_ManualAttDetailReport.Add(New IColumn("Employee", Language.getMessage(mstrModuleName, 1, "Employee")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_ManualAttDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP |29-MAR-2019| -- END


    Private Function AddUpdate_DataRow(ByVal dr As DataRow, ByVal dsDataSet As DataSet, ByVal intRowindex As Integer) As Boolean
        Try
            Try
                dsDataSet.Tables(0).Rows(intRowindex).Item("Holiday") = dr.Item("Holiday")
                dsDataSet.Tables(0).Rows(intRowindex).Item("halloc") = dr.Item("allocation")
            Catch ex As Exception
                Dim dro As DataRow = dsDataSet.Tables(0).NewRow()
                dro.Item("Holiday") = dr.Item("Holiday")
                dro.Item("halloc") = dr.Item("allocation")
                dsDataSet.Tables(0).Rows.Add(dro)
            End Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AddUpdate_DataRow; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_Sub As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            objDataOperation.AddParameter("@HalfDay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Half Day"))
            objDataOperation.AddParameter("@NotEvenHalfDay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Not Even Half Day"))
            objDataOperation.AddParameter("@HalfDayWithShortHour", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Half Day With Short Hour"))
            objDataOperation.AddParameter("@Hold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Hold"))

            Dim strAllocationName As String = String.Empty
            Dim strAllocationJoin As String = String.Empty
            Dim strEmpTransacJoin As String = String.Empty

            Select Case mintSelectedAllocationId
                Case enAllocation.BRANCH
                    strAllocationName = ",ISNULL(st.name, '') "
                    strAllocationJoin = " LEFT JOIN hrstation_master st ON Tr.stationunkid = st.stationunkid "
                Case enAllocation.DEPARTMENT_GROUP
                    strAllocationName = ",ISNULL(dgm.name, '') "
                    strAllocationJoin = " LEFT JOIN hrdepartment_group_master dgm ON dgm.deptgroupunkid = Tr.deptgroupunkid "
                Case enAllocation.DEPARTMENT
                    strAllocationName = ",ISNULL(dm.name, '') "
                    strAllocationJoin = " LEFT JOIN hrdepartment_master dm ON Tr.departmentunkid = dm.departmentunkid "
                Case enAllocation.SECTION_GROUP
                    strAllocationName = ",ISNULL(sgm.name, '') "
                    strAllocationJoin = " LEFT JOIN hrsectiongroup_master sgm ON Tr.sectiongroupunkid = sgm.sectiongroupunkid "
                Case enAllocation.SECTION
                    strAllocationName = ",ISNULL(sm.name, '') "
                    strAllocationJoin = " LEFT JOIN hrsection_master sm ON Tr.sectionunkid = sm.sectionunkid "
                Case enAllocation.UNIT_GROUP
                    strAllocationName = ",ISNULL(ugm.name, '') "
                    strAllocationJoin = " LEFT JOIN hrunitgroup_master ugm ON Tr.unitgroupunkid = ugm.unitgroupunkid "
                Case enAllocation.UNIT
                    strAllocationName = ",ISNULL(um.name, '') "
                    strAllocationJoin = " LEFT JOIN hrunit_master um ON Tr.unitunkid = um.unitunkid "
                Case enAllocation.TEAM
                    strAllocationName = ",ISNULL(tm.name, '') "
                    strAllocationJoin = " LEFT JOIN hrteam_master tm ON Tr.teamunkid = tm.teamunkid "
                Case enAllocation.JOB_GROUP
                    strAllocationName = ",ISNULL(jgm.name, '') "
                    strAllocationJoin = " LEFT JOIN hrjobgroup_master jgm ON Jr.jobgroupunkid = jgm.jobgroupunkid "
                Case enAllocation.JOBS
                    strAllocationName = ",ISNULL(jm.job_name, '') "
                    strAllocationJoin = " LEFT JOIN hrjob_master jm ON Jr.jobunkid = jm.jobunkid "
                Case enAllocation.CLASS_GROUP
                    strAllocationName = ",ISNULL(cgm.name, '') "
                    strAllocationJoin = " LEFT JOIN hrclassgroup_master cgm ON Tr.classgroupunkid = cgm.classgroupunkid "
                Case enAllocation.CLASSES
                    strAllocationName = ",ISNULL(clm.name, '') "
                    strAllocationJoin = " LEFT JOIN hrclasses_master clm ON Tr.classunkid = clm.classesunkid "
                Case enAllocation.COST_CENTER
                    strAllocationName = ",ISNULL(pcm.costcentername, '') "
                    strAllocationJoin = " LEFT JOIN prcostcenter_master pcm ON Cr.costcenterunkid = pcm.costcenterunkid "
            End Select

            If mintSelectedAllocationId > 0 Then
                strEmpTransacJoin = "LEFT JOIN " & _
                                    "( " & _
                                    "   SELECT " & _
                                    "         hremployee_transfer_tran.stationunkid " & _
                                    "        ,hremployee_transfer_tran.deptgroupunkid " & _
                                    "        ,hremployee_transfer_tran.departmentunkid " & _
                                    "        ,hremployee_transfer_tran.sectiongroupunkid " & _
                                    "        ,hremployee_transfer_tran.sectionunkid " & _
                                    "        ,hremployee_transfer_tran.unitgroupunkid " & _
                                    "        ,hremployee_transfer_tran.unitunkid " & _
                                    "        ,hremployee_transfer_tran.teamunkid " & _
                                    "        ,hremployee_transfer_tran.classgroupunkid " & _
                                    "        ,hremployee_transfer_tran.classunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                                    "        ,hremployee_transfer_tran.employeeunkid " & _
                                    "   FROM hremployee_transfer_tran " & _
                                    "   WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= @Date "

                'Pinkal (16-Jul-2021)-- Start
                'Bug Resolved in Daily Timesheet Report.
                If mintEmpId > 0 Then
                    strEmpTransacJoin &= " AND hremployee_transfer_tran.employeeunkid = @EmpId "
                End If
                'Pinkal (16-Jul-2021)-- End


                strEmpTransacJoin &= ") AS Tr ON Tr.employeeunkid = hremployee_master.employeeunkid AND Tr.rno = 1 " & _
                                    "LEFT JOIN " & _
                                    "( " & _
                                    "   SELECT " & _
                                    "        hremployee_categorization_tran.jobgroupunkid " & _
                                    "       ,hremployee_categorization_tran.jobunkid " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                                    "       ,hremployee_categorization_tran.employeeunkid " & _
                                    "   FROM hremployee_categorization_tran " & _
                                    "   WHERE hremployee_categorization_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= @Date "


                'Pinkal (16-Jul-2021)-- Start
                'Bug Resolved in Daily Timesheet Report.
                If mintEmpId > 0 Then
                    strEmpTransacJoin &= " AND hremployee_categorization_tran.employeeunkid = @EmpId"
                End If
                'Pinkal (16-Jul-2021)-- End

                strEmpTransacJoin &= ") AS Jr ON Jr.employeeunkid = hremployee_master.employeeunkid AND Jr.rno = 1 " & _
                                    "LEFT JOIN " & _
                                    "( " & _
                                    "   SELECT " & _
                                    "        hremployee_cctranhead_tran.cctranheadvalueid " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_cctranhead_tran.employeeunkid ORDER BY hremployee_cctranhead_tran.effectivedate DESC) AS rno " & _
                                    "       ,hremployee_cctranhead_tran.employeeunkid " & _
                                    "   FROM hremployee_cctranhead_tran " & _
                                    "   WHERE hremployee_cctranhead_tran.isvoid = 0 " & _
                                                "   AND hremployee_cctranhead_tran.istransactionhead = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= @Date "

                'Pinkal (16-Jul-2021)-- Start
                'Bug Resolved in Daily Timesheet Report.
                If mintEmpId > 0 Then
                    strEmpTransacJoin &= " AND hremployee_cctranhead_tran.employeeunkid = @EmpId"
                End If
                'Pinkal (16-Jul-2021)-- End

                strEmpTransacJoin &= ") AS Cr ON Cr.employeeunkid = hremployee_master.employeeunkid AND Cr.rno = 1 "
            End If

            StrQ = " SELECT "

            StrQ &= "  tnalogin_tran.employeeunkid AS EmpId  " & _
            "      , hremployee_master.employeecode AS EmpCode  " & _
            "      , ISNULL(hremployee_master.firstname, '') + ' '  " & _
            "        + ISNULL(hremployee_master.othername, '') + ' '  " & _
            "        + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
            "      , CONVERT(CHAR(8), tnalogin_summary.login_date, 112) AS LoginDate  "


            StrQ &= " ,CASE WHEN MIN(CONVERT(CHAR(8),tnalogin_tran.roundoff_intime, 108))  IS NULL THEN MIN(CONVERT(CHAR(8),tnalogin_tran.checkintime, 108)) ELSE MIN(CONVERT(CHAR(8),tnalogin_tran.roundoff_intime, 108))  END AS InTime  " & _
                      "  , CASE WHEN MAX(CONVERT(CHAR(8),tnalogin_tran.roundoff_outtime, 108))  IS NULL THEN MAX(CONVERT(CHAR(8), tnalogin_tran.checkouttime, 108)) ELSE MAX(CONVERT(CHAR(8),tnalogin_tran.roundoff_outtime, 108)) END AS OutTime  "

            StrQ &= "      , CONVERT(DECIMAL(10, 2), FLOOR(( SUM(tnalogin_tran.breakhr) / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( SUM(tnalogin_tran.breakhr) / 60 ) % 60) / 100 )) AS BreakHour  " & _
            "      , CONVERT(DECIMAL(10, 2), FLOOR(( SUM(tnalogin_tran.workhour) / 60 )/ 60) + ( CONVERT(DECIMAL(10, 2), ( SUM(tnalogin_tran.workhour) / 60 ) % 60)/ 100 )) AS WorkingHours  " & _
            "      , CASE WHEN tnalogin_tran.holdunkid <> 0 THEN @Hold  " & _
                        "             WHEN SUM(tnalogin_tran.workhour) BETWEEN tnashift_tran.halfdayfromhrs  " & _
                        "                                              AND     tnashift_tran.halfdaytohrs  " & _
            "             THEN @HalfDay  " & _
                        "             WHEN SUM(tnalogin_tran.workhour) < tnashift_tran.halfdayfromhrs  " & _
            "             THEN @NotEvenHalfDay  " & _
                        "             WHEN SUM(tnalogin_tran.workhour) > tnashift_tran.halfdaytohrs  " & _
                        "                  AND SUM(tnalogin_tran.workhour) < ( tnashift_tran.workinghrs  " & _
                        "                                                      - tnashift_tran.calcshorttimebefore )  " & _
            "             THEN @HalfDayWithShortHour  " & _
            "             ELSE ' '  " & _
            "        END AS Employee_Status  " & _
                        "      , CONVERT(DECIMAL(10,2), FLOOR((tnashift_tran.halfdayfromhrs / 60)/60) + ( CONVERT(DECIMAL(10,2), ( tnashift_tran.halfdayfromhrs / 60) % 60) / 100)) AS FromHour " & _
                        "      , CONVERT(DECIMAL(10,2), FLOOR((tnashift_tran.halfdaytohrs / 60)/60) + ( CONVERT(DECIMAL(10,2), ( tnashift_tran.halfdaytohrs / 60) % 60) / 100)) AS ToHour "


            If strAllocationName.Trim.Length > 0 Then
                StrQ &= " " & strAllocationName & " AS allocation "
            Else
                StrQ &= ",'' AS allocation "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            StrQ &= " FROM   tnalogin_tran  " & _
                    "JOIN hremployee_master ON tnalogin_tran.employeeunkid = hremployee_master.employeeunkid  "

            If strEmpTransacJoin.Trim.Length > 0 Then
                StrQ &= " " & strEmpTransacJoin & " "
            End If

            If strAllocationJoin.Trim.Length > 0 Then
                StrQ &= " " & strAllocationJoin & " "
            End If

            StrQ &= " JOIN tnalogin_summary ON tnalogin_summary.employeeunkid = tnalogin_tran.employeeunkid  AND tnalogin_summary.login_date = tnalogin_tran.logindate " & _
                         " JOIN tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid  " & _
                         " JOIN tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            StrQ &= " WHERE  ISNULL(tnalogin_tran.isvoid, 0) = 0  " & _
                        "  AND tnashift_master.isactive = 1 AND tnashift_tran.dayid = " & GetWeekDayNumber(mdtDate.DayOfWeek.ToString()) & _
                        "  AND CONVERT(CHAR(8),tnalogin_summary.login_date,112) = @Date  "

            'Pinkal (16-Jul-2021)-- Start
            'Bug Resolved in Daily Timesheet Report.
            If mintEmpId > 0 Then
                StrQ &= " AND tnalogin_tran.employeeunkid = @EmpId "
            End If
            'Pinkal (16-Jul-2021)-- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= " GROUP BY tnalogin_tran.employeeunkid  " & _
            "      , hremployee_master.employeecode  " & _
            "      , ISNULL(hremployee_master.firstname, '') + ' '  " & _
            "        + ISNULL(hremployee_master.othername, '') + ' '  " & _
            "        + ISNULL(hremployee_master.surname, '')  " & _
            "      , CONVERT(CHAR(8), tnalogin_summary.login_date, 112)  " & _
            "      , tnalogin_tran.holdunkid  " & _
            "      , tnashift_tran.halfdayfromhrs  " & _
            "      , tnashift_tran.halfdaytohrs  " & _
            "      , tnashift_tran.workinghrs  " & _
            "      , tnashift_tran.calcshorttimebefore  "

            If strAllocationName.Trim.Length > 0 Then
                StrQ &= " " & strAllocationName & " "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace(" AS Id,", ",").Replace(" AS GName", "")
            End If

            StrQ &= mstrOrderByQuery


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim intSr As Integer = 1 : Dim StrGrpName As String = String.Empty
            If mintViewIndex > 0 Then
                Dim dtTable As DataTable = Nothing
                Dim dView As DataView = dsList.Tables("DataTable").DefaultView
                dView.Sort = "GName" : dtTable = dView.ToTable.Copy
                dsList.Tables.Remove("DataTable")
                dsList.Tables.Add(dtTable)
            End If

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("EmpId")
                rpt_Row.Item("Column2") = dtRow.Item("EmpCode")
                rpt_Row.Item("Column3") = dtRow.Item("EmpCode") & " - " & dtRow.Item("EmpName")
                If dtRow.Item("GName") <> StrGrpName Then
                    intSr = 1
                    StrGrpName = dtRow.Item("GName")
                End If
                rpt_Row.Item("Column16") = intSr.ToString
                intSr += 1
                rpt_Row.Item("Column15") = dtRow.Item("allocation")
                rpt_Row.Item("Column4") = dtRow.Item("LoginDate")

                If IsDBNull(dtRow.Item("InTime")) = False AndAlso dtRow.Item("InTime").ToString.Trim.Trim.Length > 0 Then
                    rpt_Row.Item("Column5") = CDate(dtRow.Item("InTime")).ToShortTimeString
                ElseIf IsDBNull(dtRow.Item("OutTime")) AndAlso dtRow.Item("OutTime").ToString.Trim.Trim.Length <= 0 Then
                    rpt_Row.Item("Column5") = "***"
                End If

                If IsDBNull(dtRow.Item("OutTime")) = False AndAlso dtRow.Item("OutTime").ToString.Trim.Trim.Length > 0 Then
                    rpt_Row.Item("Column6") = CDate(dtRow.Item("OutTime")).ToShortTimeString
                ElseIf IsDBNull(dtRow.Item("InTime")) AndAlso dtRow.Item("InTime").ToString.Trim.Trim.Length <= 0 Then
                    rpt_Row.Item("Column6") = "***"
                End If
                rpt_Row.Item("Column7") = dtRow.Item("BreakHour")
                rpt_Row.Item("Column8") = dtRow.Item("WorkingHours")
                rpt_Row.Item("Column9") = dtRow.Item("Employee_Status")
                rpt_Row.Item("Column10") = dtRow.Item("FromHour")
                rpt_Row.Item("Column11") = dtRow.Item("ToHour")
                rpt_Row.Item("Column12") = dtRow.Item("GName")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

            Next


            'START FOR SUBREPORT

            StrQ = " SELECT " & _
                      "  ISNULL(Planned,'') AS Planned " & _
                      ", ISNULL(UnPlanned,'') AS UnPlanned " & _
                      ", ISNULL(Paid.allocation,'') AS palloc " & _
                      ", ISNULL(Unpaid.allocation,'') AS ualloc " & _
                      " FROM " & _
                      " ( " & _
                      " SELECT " & _
                   "        ISNULL(hremployee_master.employeecode, '') + ' - '+ ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.surname, '') AS Planned "
            If strAllocationName.Trim.Length > 0 Then
                StrQ &= " " & strAllocationName & " AS allocation "
            Else
                StrQ &= ",'' AS allocation "
            End If
            StrQ &= "   ,CONVERT(CHAR(8),LVP.leavedate,112) AS Pdate " & _
                   "                ,ROW_NUMBER() OVER(ORDER BY hremployee_master.employeeunkid ASC) AS RowPId " & _
                         " FROM lvleaveIssue_master " & _
                         " JOIN lvleaveIssue_tran AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvp.ispaid = 1 " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid "
            StrQ &= "AND hremployee_master.employeeunkid NOT in (SELECT employeeunkid FROM tnalogin_tran WHERE CONVERT(CHAR(8),logindate,112) = @Date AND isvoid = 0) "

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.

            StrQ &= "  LEFT JOIN  " & _
                        " ( " & _
                        "      Select employeeunkid " & _
                        "     ,shiftunkid  " & _
                        "     ,effectivedate " & _
                        "     ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                        " FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                        " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  " & _
                        " JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "

            'Pinkal (16-Apr-2019) -- End


            If strEmpTransacJoin.Trim.Length > 0 Then
                StrQ &= " " & strEmpTransacJoin & " "
            End If

            If strAllocationJoin.Trim.Length > 0 Then
                StrQ &= " " & strAllocationJoin & " "
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            StrQ &= "WHERE CONVERT(CHAR(8),LVP.leavedate,112) = @Date AND LVP.isvoid = 0 "

            If mintEmpId > 0 Then
                StrQ &= " AND lvleaveIssue_master.employeeunkid  = @EmpLId "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.
            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery
            'Pinkal (16-Apr-2019) -- End


            StrQ &= ") AS Paid " & _
                    " FULL OUTER JOIN ( SELECT   Unplanned,allocation " & _
                         ", ROW_NUMBER() OVER ( ORDER BY employeeunkid ASC ) AS RowUId " & _
                         " FROM   " & _
                    " (    SELECT    hremployee_master.employeeunkid , ISNULL(hremployee_master.employeecode, '') + ' - '+ ISNULL(hremployee_master.firstname, '')  + ' ' + ISNULL(hremployee_master.surname,'') AS Unplanned "
            If strAllocationName.Trim.Length > 0 Then
                StrQ &= " " & strAllocationName & " AS allocation "
            Else
                StrQ &= ",'' AS allocation "
            End If
            StrQ &= " FROM lvleaveIssue_master " & _
                         " JOIN lvleaveIssue_tran AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid  AND lvp.ispaid = 0 " & _
                    " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid "

            StrQ &= " AND hremployee_master.employeeunkid NOT IN ( SELECT employeeunkid  FROM " & _
                    " tnalogin_summary WHERE  CONVERT(CHAR(8),login_date,112) = @Date  AND isunpaidleave = 1 ) "

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.

            StrQ &= "  LEFT JOIN  " & _
                        " ( " & _
                        "      Select employeeunkid " & _
                        "     ,shiftunkid  " & _
                        "     ,effectivedate " & _
                        "     ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                        " FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                        " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  " & _
                        " JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "

            'Pinkal (16-Apr-2019) -- End


            If strEmpTransacJoin.Trim.Length > 0 Then
                StrQ &= " " & strEmpTransacJoin & " "
            End If

            If strAllocationJoin.Trim.Length > 0 Then
                StrQ &= " " & strAllocationJoin & " "
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            StrQ &= " WHERE  CONVERT(CHAR(8),LVP.leavedate,112) = @Date AND LVP.isvoid = 0 "

            'Pinkal (16-Jul-2021)-- Start
            'Bug Resolved in Daily Timesheet Report.
            If mintEmpId > 0 Then
                StrQ &= " AND lvleaveIssue_master.employeeunkid  = @EmpId "
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If
            'Pinkal (16-Jul-2021)-- End
          
            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.
            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery
            'Pinkal (16-Apr-2019) -- End

            StrQ &= " UNION " & _
                    " SELECT    hremployee_master.employeeunkid , ISNULL(hremployee_master.employeecode, '') + ' - '+ ISNULL(hremployee_master.firstname, '')  + ' ' + ISNULL(hremployee_master.surname,'') AS Unplanned "
            If strAllocationName.Trim.Length > 0 Then
                StrQ &= " " & strAllocationName & " AS allocation "
            Else
                StrQ &= ",'' AS allocation "
            End If
            StrQ &= " FROM tnalogin_summary " & _
                    " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid  And isunpaidleave = 1 "

            StrQ &= " AND tnalogin_summary.employeeunkid NOT IN ( " & _
                                    " SELECT  employeeunkid FROM lvleaveIssue_master  JOIN lvleaveIssue_tran " & _
                                                        " AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvp.ispaid = 0 " & _
                                    " WHERE CONVERT(CHAR(8),leavedate,112) = @Date  AND lvp.isvoid = 0 " & _
                                    " UNION " & _
                                    " SELECT DISTINCT employeeunkid from tnalogin_tran WHERE CONVERT(CHAR(8), tnalogin_tran.logindate, 112) = @Date	 AND tnalogin_tran.isvoid = 0) "

            'Pinkal (16-Dec-2016) --  'Enhancement - Worked On Daily timesheet Report when Employee Logged In and Not Logged Out then That Employee will not come in Unplanned Leave Section AS Per Voltamp Requirement.
            ' " UNION  SELECT DISTINCT employeeunkid from tnalogin_tran WHERE CONVERT(CHAR(8), tnalogin_tran.logindate, 112) = @Date	 AND tnalogin_tran.isvoid = 0 ) "

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.

            StrQ &= "  LEFT JOIN  " & _
                        " ( " & _
                        "      Select employeeunkid " & _
                        "     ,shiftunkid  " & _
                        "     ,effectivedate " & _
                        "     ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                        " FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                        " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  " & _
                        " JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "

            'Pinkal (16-Apr-2019) -- End


            If strEmpTransacJoin.Trim.Length > 0 Then
                StrQ &= " " & strEmpTransacJoin & " "
            End If

            If strAllocationJoin.Trim.Length > 0 Then
                StrQ &= " " & strAllocationJoin & " "
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            StrQ &= " WHERE CONVERT(CHAR(8),tnalogin_summary.login_date,112) = @Date "

            'Pinkal (16-Jul-2021)-- Start
            'Bug Resolved in Daily Timesheet Report.
            If mintEmpId > 0 Then
                StrQ &= " AND tnalogin_summary.employeeunkid  = @EmpId "
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If
            'Pinkal (16-Jul-2021)-- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.
            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery
            'Pinkal (16-Apr-2019) -- End

            StrQ &= " UNION " & _
                    " SELECT    hremployee_master.employeeunkid,ISNULL(hremployee_master.employeecode, '') + ' - '+ ISNULL(hremployee_master.firstname,'') + ' '  " & _
                    " + ISNULL(hremployee_master.surname,'') AS UnPlanned "
            If strAllocationName.Trim.Length > 0 Then
                StrQ &= " " & strAllocationName & " AS allocation "
            Else
                StrQ &= ",'' AS allocation "
            End If

            StrQ &= " FROM hremployee_master "

            If strEmpTransacJoin.Trim.Length > 0 Then
                StrQ &= " " & strEmpTransacJoin & " "
            End If

            If strAllocationJoin.Trim.Length > 0 Then
                StrQ &= " " & strAllocationJoin & " "
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.

            StrQ &= "  LEFT JOIN  " & _
                        " ( " & _
                        "      Select employeeunkid " & _
                        "     ,shiftunkid  " & _
                        "     ,effectivedate " & _
                        "     ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                        " FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                        " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  " & _
                        " JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "

            'Pinkal (16-Apr-2019) -- End


            StrQ &= " WHERE     hremployee_master.employeeunkid NOT IN ( " & _
                    " Select employeeunkid FROM tnalogin_summary " & _
                    " WHERE  CONVERT(CHAR(8),tnalogin_summary.login_date,112) = @Date ) "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.
            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery
            'Pinkal (16-Apr-2019) -- End

            StrQ &= " INTERSECT " & _
                    "  SELECT  hremployee_master.employeeunkid,  ISNULL(hremployee_master.employeecode, '') + ' - '+ ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.surname,'') AS UnPlanned "
            If strAllocationName.Trim.Length > 0 Then
                StrQ &= " " & strAllocationName & " AS allocation "
            Else
                StrQ &= ",'' AS allocation "
            End If

            StrQ &= "  FROM hremployee_master "

            If strEmpTransacJoin.Trim.Length > 0 Then
                StrQ &= " " & strEmpTransacJoin & " "
            End If

            If strAllocationJoin.Trim.Length > 0 Then
                StrQ &= " " & strAllocationJoin & " "
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.

            StrQ &= "  LEFT JOIN  " & _
                        " ( " & _
                        "      Select employeeunkid " & _
                        "     ,shiftunkid  " & _
                        "     ,effectivedate " & _
                        "     ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                        " FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                        " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  " & _
                        " JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "

            'Pinkal (16-Apr-2019) -- End

            StrQ &= " WHERE  hremployee_master.employeeunkid NOT IN ( " & _
                                  " Select employeeunkid  FROM lvleaveIssue_master " & _
                                  " JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                    " WHERE CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) = @Date )"


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.
            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery
            'Pinkal (16-Apr-2019) -- End

            StrQ &= " ) AS m  WHERE 1 = 1"

            If mintEmpId > 0 Then
                StrQ &= " AND  m.employeeunkid  = @EmpLId "
            End If

            StrQ &= ") AS Unpaid ON Unpaid.RowUId = Paid.RowPId "

            objDataOperation.AddParameter("@EmpLId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "LeaveInfo")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            intSr = 1
            Dim dsHoliday As New DataSet
            StrQ = "DECLARE @DayId AS INT " & _
                    "SET @DayId = (SELECT DATEPART(dw,@Date) - 1) " & _
                    "SELECT " & _
                         "ISNULL(H.EName,'') AS Holiday " & _
                         "   ,ISNULL(H.allocation,'') AS allocation " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                              "A.EName " & _
                              ",A.allocation " & _
                         "FROM " & _
                         "( " & _
                              "SELECT " & _
                                   " hremployee_shift_tran.shiftunkid AS SftId " & _
                                   ",hremployee_shift_tran.employeeunkid AS EmpId " & _
                                   ",ROW_NUMBER()OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS rno " & _
                                   ",hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' '+ hremployee_master.surname AS EName "
            If strAllocationName.Trim.Length > 0 Then
                StrQ &= " " & strAllocationName & " AS allocation "
            Else
                StrQ &= ",'' AS allocation "
            End If
            StrQ &= "FROM hremployee_shift_tran " & _
                                   "JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_shift_tran.employeeunkid "

            If strEmpTransacJoin.Trim.Length > 0 Then
                StrQ &= " " & strEmpTransacJoin & " "
            End If

            If strAllocationJoin.Trim.Length > 0 Then
                StrQ &= " " & strAllocationJoin & " "
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If
            StrQ &= "WHERE hremployee_shift_tran.isvoid = 0 " & _
                                   "AND CONVERT(NVARCHAR(8),hremployee_shift_tran.effectivedate,112) <= @Date "
            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mintEmpId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If


            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.

            'StrQ &= ") AS A " & _
            '             "JOIN tnashift_tran ON tnashift_tran.shiftunkid = A.SftId " & _
            '             "WHERE A.rno = 1 AND tnashift_tran.isweekend = 1 AND tnashift_tran.dayid = @DayId "

            StrQ &= " ) AS A " & _
                       " JOIN tnashift_tran ON tnashift_tran.shiftunkid = A.SftId " & _
                       " JOIN tnashift_master ON tnashift_master.shiftunkid = A.SftId " & _
                       " WHERE A.rno = 1 AND tnashift_tran.isweekend = 1 AND tnashift_tran.dayid = @DayId "


            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            'Pinkal (16-Apr-2019) -- End


            If mintShiftunkid > 0 Then
                StrQ &= " AND A.SftId = @SftId "
            End If

            StrQ &= " UNION " & _
                         "SELECT " & _
                               "hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' '+ hremployee_master.surname AS EName "
            If strAllocationName.Trim.Length > 0 Then
                StrQ &= " " & strAllocationName & " AS allocation "
            Else
                StrQ &= ",'' AS allocation "
            End If
            StrQ &= "FROM hremployee_dayoff_tran " & _
                              "JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_dayoff_tran.employeeunkid "

            If strEmpTransacJoin.Trim.Length > 0 Then
                StrQ &= " " & strEmpTransacJoin & " "
            End If

            If strAllocationJoin.Trim.Length > 0 Then
                StrQ &= " " & strAllocationJoin & " "
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.

            StrQ &= "  LEFT JOIN  " & _
                        " ( " & _
                        "      Select employeeunkid " & _
                        "     ,shiftunkid  " & _
                        "     ,effectivedate " & _
                        "     ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                        " FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                        " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  " & _
                        " JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "

            'Pinkal (16-Apr-2019) -- End

            StrQ &= "WHERE hremployee_dayoff_tran.isvoid = 0 " & _
                         "AND CONVERT(NVARCHAR(8),hremployee_dayoff_tran.dayoffdate,112) = @Date "

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mintEmpId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.
            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery
            'Pinkal (16-Apr-2019) -- End

            StrQ &= " UNION " & _
                         "SELECT " & _
                               "hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' '+ hremployee_master.surname AS EName "
            If strAllocationName.Trim.Length > 0 Then
                StrQ &= " " & strAllocationName & " AS allocation "
            Else
                StrQ &= ",'' AS allocation "
            End If
            StrQ &= "FROM lvemployee_holiday " & _
                         "JOIN hremployee_master ON hremployee_master.employeeunkid = lvemployee_holiday.employeeunkid "

            If strEmpTransacJoin.Trim.Length > 0 Then
                StrQ &= " " & strEmpTransacJoin & " "
            End If

            If strAllocationJoin.Trim.Length > 0 Then
                StrQ &= " " & strAllocationJoin & " "
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.

            StrQ &= "  LEFT JOIN  " & _
                        " ( " & _
                        "      Select employeeunkid " & _
                        "     ,shiftunkid  " & _
                        "     ,effectivedate " & _
                        "     ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                        " FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                        " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  " & _
                        " JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "

            'Pinkal (16-Apr-2019) -- End

            StrQ &= "JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _
                         "WHERE lvholiday_master.isactive = 1 AND CONVERT(NVARCHAR(8),lvholiday_master.holidaydate,112) = @Date "

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mintEmpId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.
            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery
            'Pinkal (16-Apr-2019) -- End

            StrQ &= ") AS H WHERE 1 = 1 "


            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId.ToString)
            objDataOperation.AddParameter("@SftId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid)

            dsHoliday = objDataOperation.ExecQuery(StrQ, "LeaveInfo")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsHoliday.Tables("LeaveInfo") IsNot Nothing Then
                dsList.Tables("LeaveInfo").Columns.Add("Holiday", GetType(String))
                dsList.Tables("LeaveInfo").Columns.Add("halloc", GetType(String))
                Dim dhrow() As DataRow = dsHoliday.Tables("LeaveInfo").Select("")
                dhrow.ToList.ForEach(Function(x) AddUpdate_DataRow(x, dsList, dsHoliday.Tables("LeaveInfo").Rows.IndexOf(x)))
            End If

            rpt_Sub = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("LeaveInfo").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Sub.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("Planned")
                rpt_Row.Item("Column2") = dtRow.Item("UnPlanned")
                If dtRow.Item("Holiday").ToString.Trim.Length > 0 Then rpt_Row.Item("Column7") = dtRow.Item("Holiday")

                rpt_Row.Item("Column8") = dtRow.Item("palloc")
                rpt_Row.Item("Column9") = dtRow.Item("ualloc")
                rpt_Row.Item("Column10") = dtRow.Item("halloc")

                If dtRow.Item("Planned").ToString.Length > 0 Then rpt_Row.Item("Column4") = intSr.ToString
                If dtRow.Item("UnPlanned").ToString.Trim.Length > 0 Then rpt_Row.Item("Column5") = intSr.ToString
                If dtRow.Item("Holiday").ToString.Trim.Length > 0 Then rpt_Row.Item("Column6") = intSr.ToString
                intSr = intSr + 1

                rpt_Sub.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            'END FOR SUBREPORT


            objRpt = New ArutiReport.Designer.rptDailyTimeSheet

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 18, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptLeaveInfo").SetDataSource(rpt_Sub)

            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 1, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtInTime", Language.getMessage(mstrModuleName, 2, "In Time"))
            Call ReportFunction.TextChange(objRpt, "txtOutTime", Language.getMessage(mstrModuleName, 3, "Out Time"))
            Call ReportFunction.TextChange(objRpt, "txtBreak", Language.getMessage(mstrModuleName, 4, "Break"))
            Call ReportFunction.TextChange(objRpt, "txtWorkingHours", Language.getMessage(mstrModuleName, 5, "Working Hours"))
            Call ReportFunction.TextChange(objRpt, "txtEmpStatus", Language.getMessage(mstrModuleName, 6, "Employee Status"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtPlanned", Language.getMessage(mstrModuleName, 21, "Planned Leave"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtUnplanned", Language.getMessage(mstrModuleName, 22, "Unplanned Leave"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtAbsentDetail", Language.getMessage(mstrModuleName, 23, "Absent Detail"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtTotalPlanned", Language.getMessage(mstrModuleName, 24, "Total Planned"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtTotalUnplanned", Language.getMessage(mstrModuleName, 25, "Total Unplanned"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 7, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 8, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtMispunch", "*** : " & Language.getMessage(mstrModuleName, 27, "Mispunch"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            Call ReportFunction.EnableSuppress(objRpt, "txtEmpStatus", Not mblnShowEmployeeStatus)
            Call ReportFunction.EnableSuppress(objRpt, "Column91", Not mblnShowEmployeeStatus)
            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 33, "Sr.No."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtSrNoP", Language.getMessage(mstrModuleName, 33, "Sr.No."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtSrNoU", Language.getMessage(mstrModuleName, 33, "Sr.No."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtSrNoH", Language.getMessage(mstrModuleName, 33, "Sr.No."))

            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtHolidays", Language.getMessage(mstrModuleName, 30, "Holiday"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtTotalHolidays", Language.getMessage(mstrModuleName, 31, "Total Holiday"))

            If mintSelectedAllocationId > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtAllocationName", mstrAllocationName)
                Call ReportFunction.EnableSuppress(objRpt, "txtAllocationName", False)
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtPAllocationName", mstrAllocationName)
                Call ReportFunction.EnableSuppress(objRpt.Subreports("rptLeaveInfo"), "txtPAllocationName", False)
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtUAllocationName", mstrAllocationName)
                Call ReportFunction.EnableSuppress(objRpt.Subreports("rptLeaveInfo"), "txtUAllocationName", False)
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtHAllocationName", mstrAllocationName)
                Call ReportFunction.EnableSuppress(objRpt.Subreports("rptLeaveInfo"), "txtHAllocationName", False)
            Else
                Call ReportFunction.TextChange(objRpt, "txtAllocationName", mstrAllocationName)
                Call ReportFunction.EnableSuppress(objRpt, "txtAllocationName", True)
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtPAllocationName", mstrAllocationName)
                Call ReportFunction.EnableSuppress(objRpt.Subreports("rptLeaveInfo"), "txtPAllocationName", True)
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtUAllocationName", mstrAllocationName)
                Call ReportFunction.EnableSuppress(objRpt.Subreports("rptLeaveInfo"), "txtUAllocationName", True)
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtHAllocationName", mstrAllocationName)
                Call ReportFunction.EnableSuppress(objRpt.Subreports("rptLeaveInfo"), "txtHAllocationName", True)

                Dim intWidth As Integer = 0
                intWidth = objRpt.ReportDefinition.ReportObjects("txtEmpName").Width + objRpt.ReportDefinition.ReportObjects("txtAllocationName").Width
                Call ReportFunction.SetColumnOn(objRpt, "txtEmpName", "txtEmpName", intWidth)
                intWidth = objRpt.ReportDefinition.ReportObjects("Column31").Width + objRpt.ReportDefinition.ReportObjects("txtAllocationName").Width
                Call ReportFunction.SetColumnOn(objRpt, "Column31", "Column31", intWidth)

                intWidth = objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("txtPlanned").Width + objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("txtPAllocationName").Width
                Call ReportFunction.SetColumnOn(objRpt.Subreports("rptLeaveInfo"), "txtPlanned", "txtPlanned", intWidth)

                intWidth = objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("Column11").Width + objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("txtPAllocationName").Width
                Call ReportFunction.SetColumnOn(objRpt.Subreports("rptLeaveInfo"), "Column11", "Column11", intWidth)

                intWidth = objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("txtUnplanned").Width + objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("txtUAllocationName").Width
                Call ReportFunction.SetColumnOn(objRpt.Subreports("rptLeaveInfo"), "txtUnplanned", "txtUnplanned", intWidth)

                intWidth = objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("Column21").Width + objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("txtPAllocationName").Width
                Call ReportFunction.SetColumnOn(objRpt.Subreports("rptLeaveInfo"), "Column21", "Column21", intWidth)

                intWidth = objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("txtHolidays").Width + objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("txtHAllocationName").Width
                Call ReportFunction.SetColumnOn(objRpt.Subreports("rptLeaveInfo"), "txtHolidays", "txtHolidays", intWidth)
                intWidth = objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("txtUnplanned").Width + objRpt.Subreports("rptLeaveInfo").ReportDefinition.ReportObjects("txtHAllocationName").Width
                Call ReportFunction.SetColumnOn(objRpt.Subreports("rptLeaveInfo"), "Column71", "Column71", intWidth)
            End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Private Function Generate_OnDutyReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            objDataOperation.AddParameter("@HalfDay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Half Day"))
            objDataOperation.AddParameter("@NotEvenHalfDay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Not Even Half Day"))
            objDataOperation.AddParameter("@HalfDayWithShortHour", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Half Day With Short Hour"))
            objDataOperation.AddParameter("@Hold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Hold"))


            StrQ = " SELECT "

            'S.SANDEEP [12 OCT 2016] -- START
            'ENHANCEMENT : ACB REPORT CHANGES
            'StrQ &= "  tnalogin_tran.employeeunkid AS EmpId  " & _
            '"      , hremployee_master.employeecode AS EmpCode  " & _
            '             "      , hremployee_master.employeecode + ' - '  +  ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
            '"      , CONVERT(CHAR(8), tnalogin_tran.logindate, 112) AS LoginDate  "

            StrQ &= "  tnalogin_tran.employeeunkid AS EmpId  " & _
            "      , hremployee_master.employeecode AS EmpCode  " & _
                         "      , ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
            "      , CONVERT(CHAR(8), tnalogin_tran.logindate, 112) AS LoginDate  "
            'S.SANDEEP [12 OCT 2016] -- END


            StrQ &= " ,CASE WHEN MIN(CONVERT(CHAR(8),tnalogin_tran.roundoff_intime, 108))  IS NULL THEN MIN(CONVERT(CHAR(8),tnalogin_tran.checkintime, 108)) ELSE MIN(CONVERT(CHAR(8),tnalogin_tran.roundoff_intime, 108))  END AS InTime  " & _
                      "  , CASE WHEN MAX(CONVERT(CHAR(8),tnalogin_tran.roundoff_outtime, 108))  IS NULL THEN MAX(CONVERT(CHAR(8), tnalogin_tran.checkouttime, 108)) ELSE MAX(CONVERT(CHAR(8),tnalogin_tran.roundoff_outtime, 108)) END AS OutTime  "

            StrQ &= "      , CONVERT(DECIMAL(10, 2), FLOOR(( SUM(tnalogin_tran.breakhr) / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( SUM(tnalogin_tran.breakhr) / 60 ) % 60) / 100 )) AS BreakHour  " & _
                         "      , CONVERT(DECIMAL(10, 2), FLOOR(( SUM(tnalogin_tran.workhour) / 60 )/ 60) + ( CONVERT(DECIMAL(10, 2), ( SUM(tnalogin_tran.workhour) / 60 ) % 60)/ 100 )) AS WorkingHours  "

            'Pinkal (29-Sep-2016) -- Start
            'Enhancement - Implementing ACB Changes in TnA Module.
            StrQ &= ", A.shiftunkid "
            'Pinkal (29-Sep-2016) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            StrQ &= " FROM   tnalogin_tran  " & _
                         " JOIN hremployee_master ON tnalogin_tran.employeeunkid = hremployee_master.employeeunkid  "



            'Pinkal (29-Sep-2016) -- Start
            'Enhancement - Implementing ACB Changes in TnA Module.
            StrQ &= " LEFT JOIN " & _
                           " ( " & _
                           "        SELECT employeeunkid " & _
                           "        ,shiftunkid  " & _
                           "        ,effectivedate " & _
                           "        ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                           "       FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                           ") " & _
                           " AS A  On A.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  " & _
                           " JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "
            'S.SANDEEP |29-MAR-2019| -- START {0003630} -- END


            'Pinkal (29-Sep-2016) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            StrQ &= " WHERE  ISNULL(tnalogin_tran.isvoid, 0) = 0  " & _
                        "  AND Convert(char(8),tnalogin_tran.logindate,112) = @Date  "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            'S.SANDEEP [12 OCT 2016] -- START
            'ENHANCEMENT : ACB REPORT CHANGES
            'StrQ &= " GROUP BY tnalogin_tran.employeeunkid  " & _
            '"      , hremployee_master.employeecode  " & _
            '            "      , A.shiftunkid " & _
            '            "      ,  hremployee_master.employeecode + ' - '  +  ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '')  " & _
            '"      , CONVERT(CHAR(8), tnalogin_tran.logindate, 112)  "

            StrQ &= " GROUP BY tnalogin_tran.employeeunkid  " & _
            "      , hremployee_master.employeecode  " & _
                        "      , A.shiftunkid " & _
                    ",ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '')  " & _
            "      , CONVERT(CHAR(8), tnalogin_tran.logindate, 112)  "
            'S.SANDEEP [12 OCT 2016] -- END


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace(" AS Id,", ",").Replace(" AS GName", "")
            End If


            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("EmpId")
                rpt_Row.Item("Column2") = dtRow.Item("EmpCode")
                rpt_Row.Item("Column3") = dtRow.Item("EmpName")
                rpt_Row.Item("Column4") = dtRow.Item("LoginDate")

                If IsDBNull(dtRow.Item("InTime")) = False AndAlso dtRow.Item("InTime").ToString.Trim.Trim.Length > 0 Then
                    rpt_Row.Item("Column5") = CDate(dtRow.Item("InTime")).ToShortTimeString
                ElseIf IsDBNull(dtRow.Item("OutTime")) AndAlso dtRow.Item("OutTime").ToString.Trim.Trim.Length <= 0 Then
                    rpt_Row.Item("Column5") = "***"
                End If

                If IsDBNull(dtRow.Item("OutTime")) = False AndAlso dtRow.Item("OutTime").ToString.Trim.Trim.Length > 0 Then
                    rpt_Row.Item("Column6") = CDate(dtRow.Item("OutTime")).ToShortTimeString
                ElseIf IsDBNull(dtRow.Item("InTime")) AndAlso dtRow.Item("InTime").ToString.Trim.Trim.Length <= 0 Then
                    rpt_Row.Item("Column6") = "***"
                End If

                rpt_Row.Item("Column7") = dtRow.Item("BreakHour")
                rpt_Row.Item("Column8") = dtRow.Item("WorkingHours")
                rpt_Row.Item("Column12") = dtRow.Item("GName")
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next



            objRpt = New ArutiReport.Designer.rptDailyTimeSheetEmpOnDuty

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 18, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 1, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtInTime", Language.getMessage(mstrModuleName, 2, "In Time"))
            Call ReportFunction.TextChange(objRpt, "txtOutTime", Language.getMessage(mstrModuleName, 3, "Out Time"))
            Call ReportFunction.TextChange(objRpt, "txtBreak", Language.getMessage(mstrModuleName, 4, "Break"))
            Call ReportFunction.TextChange(objRpt, "txtWorkingHours", Language.getMessage(mstrModuleName, 5, "Working Hours"))
            Call ReportFunction.TextChange(objRpt, "txtTotalOnDutyEmployee", Language.getMessage(mstrModuleName, 28, "Total On Duty Employee :"))


            'Pinkal (29-Sep-2016) -- Start
            'Enhancement - Implementing ACB Changes in TnA Module.
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 29, "Sub Total :"))
            'Pinkal (29-Sep-2016) -- End

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 7, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 8, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate())

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            'S.SANDEEP [12 OCT 2016] -- START
            'ENHANCEMENT : ACB REPORT CHANGES
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 32, "Code"))
            'S.SANDEEP [12 OCT 2016] -- END


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_OnDutyReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Private Function Generate_OffDutyReport(ByVal strDatabaseName As String, _
                                          ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal dtPeriodStart As Date, _
                                          ByVal dtPeriodEnd As Date, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


            'S.SANDEEP [12 OCT 2016] -- START
            'ENHANCEMENT : ACB REPORT CHANGES
            'StrQ = "SELECT " & _
            '           "                   hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') AS Planned, '' AS Unplanned " & _
            '           "                ,ROW_NUMBER() OVER(ORDER BY hremployee_master.employeeunkid ASC) AS RowPId " & _
            '           "                  ,A.shiftunkid "

            StrQ = "SELECT " & _
                   "  hremployee_master.employeecode AS pcode " & _
                   " ,'' AS ucode " & _
                   " ,ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') AS Planned, '' AS Unplanned " & _
                       "                ,ROW_NUMBER() OVER(ORDER BY hremployee_master.employeeunkid ASC) AS RowPId " & _
                       "                  ,A.shiftunkid "
            'S.SANDEEP [12 OCT 2016] -- END


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            StrQ &= " FROM lvleaveIssue_master " & _
                       " JOIN lvleaveIssue_tran AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvp.ispaid = 1 " & _
                       " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
                       " AND hremployee_master.employeeunkid NOT in (SELECT employeeunkid FROM tnalogin_tran WHERE Convert(Char(8),logindate,112) = @Date AND isvoid = 0) " & _
                       "  LEFT JOIN  " & _
                        " ( " & _
                        "      Select employeeunkid " & _
                        "     ,shiftunkid  " & _
                        "     ,effectivedate " & _
                        "     ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                        " FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                        " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  " & _
                        " JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "
            'S.SANDEEP |29-MAR-2019| -- START {0003630} -- END

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If


            StrQ &= "WHERE LVP.leavedate = @Date AND LVP.isvoid = 0 "

            If mintShiftunkid > 0 Then
                StrQ &= " AND A.shiftunkid = " & mintShiftunkid
            End If

            If mintEmpId > 0 Then
                StrQ &= " AND lvleaveIssue_master.employeeunkid  = @EmpId "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            Dim dsPlannedList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            'S.SANDEEP [12 OCT 2016] -- START
            'ENHANCEMENT : ACB REPORT CHANGES
            'StrQ = " SELECT '' AS Planned, Unplanned " & _
            '             ", ROW_NUMBER() OVER ( ORDER BY employeeunkid ASC ) AS RowUId " & _
            '             ", shiftunkid " & _
            '             ", Id , GName " & _
            '             " FROM   " & _
            '             " (    SELECT    hremployee_master.employeeunkid ,  hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname, '')  + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS Unplanned " & _
            '             ", A.shiftunkid "

            StrQ = " SELECT '' AS pcode, employeecode AS ucode , '' AS Planned, Unplanned " & _
                         ", ROW_NUMBER() OVER ( ORDER BY employeeunkid ASC ) AS RowUId " & _
                         ", shiftunkid " & _
                         ", Id , GName " & _
                         " FROM   " & _
                         " (    SELECT  hremployee_master.employeeunkid ,  hremployee_master.employeecode, " & _
                         "      ISNULL(hremployee_master.firstname, '')  + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS Unplanned " & _
                         ", A.shiftunkid "
            'S.SANDEEP [12 OCT 2016] -- END


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM lvleaveIssue_master " & _
                         " JOIN lvleaveIssue_tran AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid  AND lvp.ispaid = 0 " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
                         " AND hremployee_master.employeeunkid NOT IN ( SELECT employeeunkid  FROM " & _
                         " tnalogin_tran WHERE Convert(Char(8),logindate,112) = @Date )   " & _
                         " LEFT JOIN  " & _
                         " ( " & _
                         "      SELECT employeeunkid " & _
                         "      ,shiftunkid " & _
                         "      ,effectivedate " & _
                         "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                         "    FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                         " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  " & _
                         " LEFT JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "

            'Pinkal (16-Apr-2019) -- 'Defect : filter not working on Shift Type Option on Daily Timesheet.


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            StrQ &= " WHERE  LVP.leavedate = @Date AND LVP.isvoid = 0 "


            If mintShiftunkid > 0 Then
                StrQ &= " AND A.shiftunkid = " & mintShiftunkid
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.
            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            'Pinkal (16-Apr-2019) -- End


            'S.SANDEEP [12 OCT 2016] -- START
            'ENHANCEMENT : ACB REPORT CHANGES
            'StrQ &= " UNION " & _
            '             " SELECT  hremployee_master.employeeunkid , hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '')   + ' ' + ISNULL(hremployee_master.surname,'') AS Unplanned " & _
            '             ", A.shiftunkid "

            StrQ &= " UNION " & _
                         " SELECT  hremployee_master.employeeunkid , hremployee_master.employeecode " & _
                         ",ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '')   + ' ' + ISNULL(hremployee_master.surname,'') AS Unplanned " & _
                         ", A.shiftunkid "
            'S.SANDEEP [12 OCT 2016] -- END


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            StrQ &= " FROM tnalogin_tran " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_tran.employeeunkid  " & _
                         " AND tnalogin_tran.employeeunkid NOT IN ( " & _
                         " SELECT  employeeunkid FROM lvleaveIssue_master  JOIN lvleaveIssue_tran " & _
                         " AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvp.ispaid = 0 " & _
                         " WHERE Convert(Char(8),leavedate,112) = @Date  AND lvp.isvoid = 0 ) " & _
                         " LEFT JOIN  " & _
                         " ( " & _
                         "      SELECT employeeunkid " & _
                         "     ,shiftunkid " & _
                         "     ,effectivedate " & _
                         "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                         "      FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                         " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1 " & _
                          " LEFT JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "

            'Pinkal (16-Apr-2019) -- 'Defect : filter not working on Shift Type Option on Daily Timesheet.

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            StrQ &= " WHERE Convert(Char(8),tnalogin_tran.logindate,112) = @Date AND tnalogin_tran.checkintime is NULL and tnalogin_tran.checkouttime is NULL"

            If mintShiftunkid > 0 Then
                StrQ &= " AND A.shiftunkid = " & mintShiftunkid
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.
            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery
            'Pinkal (16-Apr-2019) -- End


            'S.SANDEEP [12 OCT 2016] -- START
            'ENHANCEMENT : ACB REPORT CHANGES
            'StrQ &= " UNION " & _
            '             " SELECT    hremployee_master.employeeunkid, hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS UnPlanned " & _
            '             ", A.shiftunkid "

            StrQ &= " UNION " & _
                         " SELECT    hremployee_master.employeeunkid, hremployee_master.employeecode " & _
                         ",ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS UnPlanned " & _
                         ", A.shiftunkid "
            'S.SANDEEP [12 OCT 2016] -- END


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM hremployee_master " & _
                         " LEFT JOIN  " & _
                         " ( " & _
                         "      SELECT employeeunkid " & _
                         "     ,shiftunkid " & _
                         "     ,effectivedate " & _
                         "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                         "      FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                         " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1 " & _
                         " LEFT JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "

            'Pinkal (16-Apr-2019) -- 'Defect : filter not working on Shift Type Option on Daily Timesheet.

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            StrQ &= " WHERE     hremployee_master.employeeunkid NOT IN ( " & _
                         " Select employeeunkid FROM tnalogin_tran " & _
                         " WHERE   Convert(Char(8),logindate,112) = @Date ) "

            If mintShiftunkid > 0 Then
                StrQ &= " AND A.shiftunkid = " & mintShiftunkid
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.
            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery
            'Pinkal (16-Apr-2019) -- End


            'S.SANDEEP [12 OCT 2016] -- START
            'ENHANCEMENT : ACB REPORT CHANGES
            'StrQ &= " INTERSECT " & _
            '             "  SELECT  hremployee_master.employeeunkid, hremployee_master.employeecode + ' - '  +  ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS UnPlanned " & _
            '             ", A.shiftunkid "

            StrQ &= " INTERSECT " & _
                         "  SELECT  hremployee_master.employeeunkid, hremployee_master.employeecode " & _
                         "  ,ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS UnPlanned " & _
                         ", A.shiftunkid "
            'S.SANDEEP [12 OCT 2016] -- END


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            StrQ &= " FROM hremployee_master " & _
                         " LEFT JOIN  " & _
                         " ( " & _
                         "      SELECT employeeunkid " & _
                         "     ,shiftunkid " & _
                         "     ,effectivedate " & _
                         "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
                         "      FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
                         " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1 " & _
                         " LEFT JOIN tnashift_master ON A.shiftunkid = tnashift_master.shiftunkid "

            'Pinkal (16-Apr-2019) -- 'Defect : filter not working on Shift Type Option on Daily Timesheet.

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            StrQ &= " WHERE  hremployee_master.employeeunkid NOT IN ( " & _
                         " Select employeeunkid  FROM lvleaveIssue_master " & _
                         " JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                         " WHERE  Convert(Char(8),lvleaveIssue_tran.leavedate,112)   = @Date )"

            If mintShiftunkid > 0 Then
                StrQ &= " AND A.shiftunkid = " & mintShiftunkid
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Pinkal (16-Apr-2019) -- Start
            'Defect :Defect : filter not working on Shift Type Option on Daily Timesheet.
            objDataOperation.ClearParameters()
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            'Pinkal (16-Apr-2019) -- End



            StrQ &= " ) AS m  WHERE 1 = 1"

            If mintEmpId > 0 Then
                StrQ &= " AND  m.employeeunkid  = @EmpId "
            End If

            'FilterTitleAndFilterQuery()

            'StrQ &= Me._FilterQuery

            Dim dsUnplannedList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim dsFinal As DataSet = Nothing
            If dsPlannedList IsNot Nothing AndAlso dsUnplannedList IsNot Nothing Then

                dsFinal = dsPlannedList.Clone()
                If dsUnplannedList.Tables(0).Rows.Count > dsPlannedList.Tables(0).Rows.Count Then

                    If mintViewIndex <= 0 Then 'WHEN ALLOCATION IS NOT USED UNPLANNED EMPLOYEE MORE THAN PLANNED EMPLOYEE.
                        dsFinal = dsUnplannedList.Copy
                        dsPlannedList.Tables(0).AsEnumerable.ToList.ForEach(Function(x) UpdateRow(x, dsPlannedList, dsFinal, True))

                    ElseIf mintViewIndex > 0 Then

                        Dim objDept As New clsDepartment
                        Dim dsDepartment As DataSet = objDept.getComboList("List", False)
                        objDept = Nothing

                        Dim xAllocationId As Integer = 0
                        For Each drDept As DataRow In dsDepartment.Tables(0).Rows
                            Dim drUnplanned As DataRow() = dsUnplannedList.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
                            If drUnplanned.Length > 0 Then
                                For Each dr As DataRow In drUnplanned
                                    dsFinal.Tables(0).ImportRow(dr)
                                Next
                            End If

                            Dim drPlanned As DataRow() = dsPlannedList.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
                            If drPlanned.Length > 0 Then
                                Dim drRow As DataRow() = dsFinal.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
                                If drRow.Length > 0 Then
                                    drPlanned.ToList.ForEach(Function(x) UpdateGroupRow(x, drRow, True))
                                Else
                                    For Each dr As DataRow In drPlanned
                                        dsFinal.Tables(0).ImportRow(dr)
                                    Next
                                End If
                            End If
                            dsFinal.AcceptChanges()
                        Next

                    End If

                Else

                    If mintViewIndex <= 0 Then  'WHEN ALLOCATION IS NOT USED PLANNED EMPLOYEE MORE THAN UNPLANNED EMPLOYEE.
                        dsFinal = dsPlannedList.Copy
                        dsUnplannedList.Tables(0).AsEnumerable.ToList.ForEach(Function(x) UpdateRow(x, dsUnplannedList, dsFinal, False))

                    ElseIf mintViewIndex > 0 Then

                        Dim objDept As New clsDepartment
                        Dim dsDepartment As DataSet = objDept.getComboList("List", False)
                        objDept = Nothing

                        Dim xAllocationId As Integer = 0
                        For Each drDept As DataRow In dsDepartment.Tables(0).Rows
                            Dim drplanned As DataRow() = dsPlannedList.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
                            If drplanned.Length > 0 Then
                                For Each dr As DataRow In drplanned
                                    dsFinal.Tables(0).ImportRow(dr)
                                Next
                            End If

                            Dim drUnPlanned As DataRow() = dsUnplannedList.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
                            If drUnPlanned.Length > 0 Then
                                Dim drRow As DataRow() = dsFinal.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
                                If drRow.Length > 0 Then
                                    drUnPlanned.ToList.ForEach(Function(x) UpdateGroupRow(x, drRow, False))
                                Else
                                    For Each dr As DataRow In drUnPlanned
                                        dsFinal.Tables(0).ImportRow(dr)
                                    Next
                                End If
                            End If
                            dsFinal.AcceptChanges()
                        Next

                    End If

                End If

            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsFinal.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("Planned")
                rpt_Row.Item("Column2") = dtRow.Item("UnPlanned")


                'S.SANDEEP [12 OCT 2016] -- START
                'ENHANCEMENT : ACB REPORT CHANGES
                rpt_Row.Item("Column5") = dtRow.Item("pcode")
                rpt_Row.Item("Column6") = dtRow.Item("ucode")
                'S.SANDEEP [12 OCT 2016] -- END


                'Pinkal (29-Sep-2016) -- Start
                'Enhancement - Implementing ACB Changes in TnA Module.
                rpt_Row.Item("Column3") = dtRow.Item("GName")
                rpt_Row.Item("Column4") = dtRow.Item("Id")
                'Pinkal (29-Sep-2016) -- End


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptDailyTimeSheetOffDuty

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 18, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)
            Call ReportFunction.TextChange(objRpt, "txtPlanned", Language.getMessage(mstrModuleName, 21, "Planned Leave"))
            Call ReportFunction.TextChange(objRpt, "txtUnplanned", Language.getMessage(mstrModuleName, 22, "Unplanned Leave"))
            Call ReportFunction.TextChange(objRpt, "txtTotalPlanned", Language.getMessage(mstrModuleName, 24, "Total Planned"))
            Call ReportFunction.TextChange(objRpt, "txtTotalUnplanned", Language.getMessage(mstrModuleName, 25, "Total Unplanned"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 7, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 8, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPlannedSubTotal", Language.getMessage(mstrModuleName, 29, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtUnPlannedSubTotal", Language.getMessage(mstrModuleName, 29, "Sub Total :"))

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate())

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            'S.SANDEEP [12 OCT 2016] -- START
            'ENHANCEMENT : ACB REPORT CHANGES
            Call ReportFunction.TextChange(objRpt, "txtPEmpCode", Language.getMessage(mstrModuleName, 32, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtUEmpCode", Language.getMessage(mstrModuleName, 32, "Code"))
            'S.SANDEEP [12 OCT 2016] -- END

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_OffDutyReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (26-Nov-2018) -- Start
    'Enhancement - Working on Papaye Manual Attendance Register Report.

    Private Function Generate_ManualAttendanceRegisterReport(ByVal strDatabaseName As String, _
                                          ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal dtPeriodStart As Date, _
                                          ByVal dtPeriodEnd As Date, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, True, False, strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = " SELECT " & _
                       " Emp.employeeunkid " & _
                       ",Emp.Employee " & _
                       ",Emp.shiftunkid " & _
                       ",Emp.shiftname " & _
                       ",Emp.shifttypeunkid " & _
                       ",Id " & _
                       ",GName "

            StrQ &= " FROM  " & _
                         " ( " & _
                         "      SELECT " & _
                         "          hremployee_master.employeeunkid " & _
                         "         ,ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                         "         ,hremployee_shift_tran.shiftunkid " & _
                         "         ,ROW_NUMBER() OVER (PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS rno " & _
                         "         ,tnashift_master.shiftname " & _
                         "         ,tnashift_master.shifttypeunkid "
            'S.SANDEEP |29-MAR-2019| -- START {0003630 - shifttypeunkid}{PAPAYE}. -- END
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "      FROM hremployee_master " & _
                         "      LEFT JOIN hremployee_shift_tran ON hremployee_master.employeeunkid = hremployee_shift_tran.employeeunkid " & _
                         "      LEFT JOIN tnashift_master ON tnashift_master.shiftunkid = hremployee_shift_tran.shiftunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE  hremployee_shift_tran.isvoid = 0 AND CONVERT(CHAR(8), hremployee_shift_tran.effectivedate, 112) <= @Date "

            'Pinkal (28-Dec-2018) -- Start
            'Bug - Worked on shift filter is not runing on Manual Attendance Register report for Papaye.
            If mintEmpId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @EmpId "
            End If
            If mintShiftunkid > 0 Then
                StrQ &= " AND hremployee_shift_tran.shiftunkid = @ShiftId "
            End If
            'Pinkal (28-Dec-2018) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            'Pinkal (20-Apr-2019) -- Start
            'Defect - Solved Defect for Papaaye Manaul Attendance Register Report for Advance Filter Issue.
            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If
            'Pinkal (20-Apr-2019) -- End


            Call FilterTitleAndFilterQuery()

            StrQ &= " ) AS Emp  WHERE Emp.rno = 1 "

            StrQ &= Me._FilterQuery
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            StrQ &= mstrOrderByQuery
            'S.SANDEEP |29-MAR-2019| -- END

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("GName")
                rpt_Row.Item("Column2") = dtRow.Item("Employee")
                rpt_Row.Item("Column3") = dtRow.Item("shiftname")
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            objRpt = New ArutiReport.Designer.rptManualAttendanceRegister

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 18, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)
            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 34, "No."))
            Call ReportFunction.TextChange(objRpt, "txtstaffname", Language.getMessage(mstrModuleName, 35, "Staff Name"))
            Call ReportFunction.TextChange(objRpt, "txtShift", Language.getMessage(mstrModuleName, 36, "Shift"))
            Call ReportFunction.TextChange(objRpt, "txtStaffWrittenName", Language.getMessage(mstrModuleName, 37, "Staff Written Name"))
            Call ReportFunction.TextChange(objRpt, "txtTimeIn", Language.getMessage(mstrModuleName, 38, "Time In"))
            Call ReportFunction.TextChange(objRpt, "txtStaffSign", Language.getMessage(mstrModuleName, 39, "Staff Sign"))
            Call ReportFunction.TextChange(objRpt, "txtTimeOut", Language.getMessage(mstrModuleName, 40, "Time Out"))
            Call ReportFunction.TextChange(objRpt, "txtSupervisorSign", Language.getMessage(mstrModuleName, 41, "Supervisor Sign"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 7, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 8, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "txtHeading", mdtDate.ToLongDateString() & " - " & mdtDate.ToLongDateString())

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_ManualAttendanceRegisterReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (26-Nov-2018) -- End

    Public Function UpdateRow(ByVal dr As DataRow, ByVal dsList As DataSet, ByVal dsFinal As DataSet, ByVal mblnIsPlanned As Boolean) As Boolean
        Try
            If dsFinal IsNot Nothing AndAlso dsFinal.Tables(0).Rows.Count > 0 Then
                Dim xIndex As Integer = dsList.Tables(0).Rows.IndexOf(dr)
                If xIndex <= dsFinal.Tables(0).Rows.Count Then
                    If mblnIsPlanned Then
                        dsFinal.Tables(0).Rows(xIndex)("planned") = dr("planned").ToString()
                        'S.SANDEEP [12 OCT 2016] -- START
                        'ENHANCEMENT : ACB REPORT CHANGES
                        dsFinal.Tables(0).Rows(xIndex)("pcode") = dr("pcode").ToString()
                    Else
                        dsFinal.Tables(0).Rows(xIndex)("ucode") = dr("ucode").ToString()
                        'S.SANDEEP [12 OCT 2016] -- END
                        dsFinal.Tables(0).Rows(xIndex)("unplanned") = dr("unplanned").ToString()
                    End If
                    dsFinal.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRow; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Public Function UpdateGroupRow(ByVal dr As DataRow, ByVal drFinal As DataRow(), ByVal mblnIsPlanned As Boolean) As Boolean
        Try
            If dr IsNot Nothing AndAlso drFinal IsNot Nothing Then
                Dim xName As String = ""
                For Each drRow As DataRow In drFinal
                    If xName <> dr("planned").ToString() Then
                        If mblnIsPlanned Then
                            If drRow("planned").ToString.Trim.Length <= 0 Then
                                drRow("planned") = dr("planned").ToString()
                                'S.SANDEEP [12 OCT 2016] -- START
                                'ENHANCEMENT : ACB REPORT CHANGES
                                drRow("pcode") = dr("pcode").ToString()
                                'S.SANDEEP [12 OCT 2016] -- END
                            Else
                                Continue For
                            End If
                        Else
                            If drRow("unplanned").ToString.Trim.Length <= 0 Then
                                drRow("unplanned") = dr("unplanned").ToString()
                                'S.SANDEEP [12 OCT 2016] -- START
                                'ENHANCEMENT : ACB REPORT CHANGES
                                drRow("ucode") = dr("ucode").ToString()
                                'S.SANDEEP [12 OCT 2016] -- END
                            Else
                                Continue For
                            End If
                        End If
                        drRow.AcceptChanges()
                        xName = dr("planned").ToString()
                        Exit For
                    End If
                Next

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateGroupRow; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Public Function DeleteRow(ByVal dr As DataRow) As Boolean
        Try
            dr.Delete()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteRow; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function



#End Region

#Region "Messages"
    '1, "Employee"
    '2, "In Time"
    '3, "Out Time"
    '4, "Break"
    '5, "Working Hours"
    '6, "Employee Status"
    '7, "Printed By :"
    '8, "Printed Date :"
    '9, "Page :"
    '10, "Half Day"
    '11, "Not Even Half Day"
    '12, "Half Day With Short Hour"
    '13, "Hold"
    '14, " Date: "
    '15, " Employee : "
    '16, " Order By : "
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee")
            Language.setMessage(mstrModuleName, 2, "In Time")
            Language.setMessage(mstrModuleName, 3, "Out Time")
            Language.setMessage(mstrModuleName, 4, "Break")
            Language.setMessage(mstrModuleName, 5, "Working Hours")
            Language.setMessage(mstrModuleName, 6, "Employee Status")
            Language.setMessage(mstrModuleName, 7, "Printed By :")
            Language.setMessage(mstrModuleName, 8, "Printed Date :")
            Language.setMessage(mstrModuleName, 10, "Half Day")
            Language.setMessage(mstrModuleName, 11, "Not Even Half Day")
            Language.setMessage(mstrModuleName, 12, "Half Day With Short Hour")
            Language.setMessage(mstrModuleName, 13, "Hold")
            Language.setMessage(mstrModuleName, 14, " Date:")
            Language.setMessage(mstrModuleName, 15, " Employee :")
            Language.setMessage(mstrModuleName, 16, " Order By :")
            Language.setMessage(mstrModuleName, 17, "Prepared By :")
            Language.setMessage(mstrModuleName, 18, "Checked By :")
            Language.setMessage(mstrModuleName, 19, "Approved By :")
            Language.setMessage(mstrModuleName, 20, "Received By :")
            Language.setMessage(mstrModuleName, 21, "Planned Leave")
            Language.setMessage(mstrModuleName, 22, "Unplanned Leave")
            Language.setMessage(mstrModuleName, 23, "Absent Detail")
            Language.setMessage(mstrModuleName, 24, "Total Planned")
            Language.setMessage(mstrModuleName, 25, "Total Unplanned")
            Language.setMessage(mstrModuleName, 26, " Shift :")
            Language.setMessage(mstrModuleName, 27, "Mispunch")
            Language.setMessage(mstrModuleName, 28, "Total On Duty Employee :")
            Language.setMessage(mstrModuleName, 29, "Sub Total :")
            Language.setMessage(mstrModuleName, 30, "Holiday")
            Language.setMessage(mstrModuleName, 31, "Total Holiday")
            Language.setMessage(mstrModuleName, 32, "Code")
            Language.setMessage(mstrModuleName, 33, "Sr.No.")
            Language.setMessage(mstrModuleName, 34, "No.")
            Language.setMessage(mstrModuleName, 35, "Staff Name")
            Language.setMessage(mstrModuleName, 36, "Shift")
            Language.setMessage(mstrModuleName, 37, "Staff Written Name")
            Language.setMessage(mstrModuleName, 38, "Time In")
            Language.setMessage(mstrModuleName, 39, "Staff Sign")
            Language.setMessage(mstrModuleName, 40, "Time Out")
            Language.setMessage(mstrModuleName, 41, "Supervisor Sign")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class clsDailyTimeSheet
'    Inherits IReportData
'    Private Shared ReadOnly mstrModuleName As String = "clsDailyTimeSheet"
'    Private mstrReportId As String = enArutiReport.DailyTimeSheet
'    Dim objDataOperation As clsDataOperation

'#Region " Constructor "
'    Public Sub New()
'        Me.setReportData(CInt(mstrReportID),intLangId,intCompanyId)
'        Call Create_OnDetailReport()
'    End Sub
'#End Region

'#Region " Private variables "

'    Private mintReportTypeId As Integer = 0
'    Private mstrReportTypeName As String = ""

'    Private mdtDate As DateTime = Nothing

'    Private mintEmpId As Integer = 0
'    Private mstrEmpName As String = ""

'    Private mstrWorkedHourFrom As String = ""
'    Private mstrWorkedHourTo As String = ""

'    Private mstrOrderByQuery As String = ""


'    'Pinkal (24-Jun-2011) -- Start
'    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'    Private mblnIsActive As Boolean = True
'    'Pinkal (24-Jun-2011) -- End


'    'Pinkal (03-Nov-2012) -- Start
'    'Enhancement : TRA Changes

'    Private mintViewIndex As Integer = -1
'    Private mstrViewByIds As String = String.Empty
'    Private mstrViewByName As String = String.Empty
'    Private mstrAnalysis_Fields As String = ""
'    Private mstrAnalysis_Join As String = ""
'    Private mstrAnalysis_OrderBy As String = ""
'    Private mstrReport_GroupName As String = ""
'    Private mintShiftunkid As Integer = 0
'    Private mstrShiftName As String = ""

'    'Pinkal (03-Nov-2012) -- End




'#End Region

'#Region " Properties "
'    Public WriteOnly Property _ReportTypeId() As Integer
'        Set(ByVal value As Integer)
'            mintReportTypeId = value
'        End Set
'    End Property

'    Public WriteOnly Property _ReportTypeName() As String
'        Set(ByVal value As String)
'            mstrReportTypeName = value
'        End Set
'    End Property

'    Public WriteOnly Property _Date() As DateTime
'        Set(ByVal value As DateTime)
'            mdtDate = value
'        End Set
'    End Property

'    Public WriteOnly Property _EmpId() As Integer
'        Set(ByVal value As Integer)
'            mintEmpId = value
'        End Set
'    End Property

'    Public WriteOnly Property _EmpName() As String
'        Set(ByVal value As String)
'            mstrEmpName = value
'        End Set
'    End Property

'    Public WriteOnly Property _WorkedHourFrom() As String
'        Set(ByVal value As String)
'            mstrWorkedHourFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _WorkedHourTo() As String
'        Set(ByVal value As String)
'            mstrWorkedHourTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _IsActive() As Boolean
'        Set(ByVal value As Boolean)
'            mblnIsActive = value
'        End Set
'    End Property

'    'Pinkal (03-Nov-2012) -- Start
'    'Enhancement : TRA Changes

'    Public WriteOnly Property _Shiftunkid() As Integer
'        Set(ByVal value As Integer)
'            mintShiftunkid = value
'        End Set
'    End Property

'    Public WriteOnly Property _ShiftName() As String
'        Set(ByVal value As String)
'            mstrShiftName = value
'        End Set
'    End Property

'    Public WriteOnly Property _ViewIndex() As Integer
'        Set(ByVal value As Integer)
'            mintViewIndex = value
'        End Set
'    End Property

'    Public WriteOnly Property _ViewByIds() As String
'        Set(ByVal value As String)
'            mstrViewByIds = value
'        End Set
'    End Property

'    Public WriteOnly Property _ViewByName() As String
'        Set(ByVal value As String)
'            mstrViewByName = value
'        End Set
'    End Property

'    Public WriteOnly Property _Analysis_Fields() As String
'        Set(ByVal value As String)
'            mstrAnalysis_Fields = value
'        End Set
'    End Property

'    Public WriteOnly Property _Analysis_Join() As String
'        Set(ByVal value As String)
'            mstrAnalysis_Join = value
'        End Set
'    End Property

'    Public WriteOnly Property _Analysis_OrderBy() As String
'        Set(ByVal value As String)
'            mstrAnalysis_OrderBy = value
'        End Set
'    End Property

'    Public WriteOnly Property _Report_GroupName() As String
'        Set(ByVal value As String)
'            mstrReport_GroupName = value
'        End Set
'    End Property

'    'Pinkal (03-Nov-2012) -- End


'#End Region

'#Region "Public Function & Procedures "

'    Public Sub SetDefaultValue()
'        Try
'            mintReportTypeId = 0
'            mstrReportTypeName = ""

'            mdtDate = Nothing

'            mintEmpId = 0
'            mstrEmpName = ""

'            mstrWorkedHourFrom = ""
'            mstrWorkedHourTo = ""

'            mstrOrderByQuery = ""



'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'            mblnIsActive = True
'            'Pinkal (24-Jun-2011) -- End


'            'Pinkal (03-Nov-2012) -- Start
'            'Enhancement : TRA Changes
'            mintShiftunkid = 0
'            mstrShiftName = ""
'            mintViewIndex = -1
'            mstrViewByIds = ""
'            mstrOrderByQuery = ""
'            'Pinkal (03-Nov-2012) -- End


'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Sub FilterTitleAndFilterQuery()
'        Me._FilterQuery = ""
'        Me._FilterTitle = ""

'        Try
'            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate))
'            Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, " Date: ") & " " & mdtDate.ToShortDateString & " "

'            If mintEmpId > 0 Then
'                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
'                If mintReportTypeId <> 2 Then
'                Me._FilterQuery &= " AND tnalogin_tran.employeeunkid = @EmpId "
'                End If
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, " Employee : ") & " " & mstrEmpName & " "
'            End If

'            'Pinkal (29-Sep-2016) -- Start
'            'Enhancement - Implementing ACB Changes in TnA Module.
'            If mintShiftunkid > 0 Then
'                objDataOperation.AddParameter("@ShiftId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid)
'                If mintReportTypeId = 0 Then  'Daily Timesheet
'                Me._FilterQuery &= " AND tnashift_master.shiftunkid = @ShiftId"
'                ElseIf mintReportTypeId = 1 Then 'On Duty Report
'                    Me._FilterQuery &= " AND A.shiftunkid = @ShiftId"
'                End If
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, " Shift : ") & " " & mstrShiftName & " "
'            End If
'            'Pinkal (29-Sep-2016) -- End

'            If mblnIsActive = False Then
'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            End If
'            'Pinkal (11-MAY-2012) -- End


'            If Me.OrderByQuery <> "" Then
'                mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, " Order By : ") & " " & Me.OrderByDisplay
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
'        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        'Dim strReportExportFile As String = ""
'        'Try
'        '    objRpt = Generate_DetailReport()
'        '    If Not IsNothing(objRpt) Then
'        '        Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
'        '    End If
'        'Catch ex As Exception
'        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
'        'End Try
'    End Sub

'    'Shani(24-Aug-2015) -- Start
'    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
'        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        Dim strReportExportFile As String = ""
'        Try
'            'Pinkal (04-May-2016) -- Start
'            'Enhancement - Adding New Report Type For Hyatt Zanzibar.
'            If mintReportTypeId = 0 Then
'            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
'            ElseIf mintReportTypeId = 1 Then
'                objRpt = Generate_OnDutyReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
'            ElseIf mintReportTypeId = 2 Then
'                objRpt = Generate_OffDutyReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
'            End If
'            'Pinkal (04-May-2016) -- End
'            If Not IsNothing(objRpt) Then
'                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
'            End If
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
'        End Try
'    End Sub
'    'Shani(24-Aug-2015) -- End

'    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
'        OrderByDisplay = ""
'        OrderByQuery = ""
'        Try
'            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
'            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
'        Try
'            Call OrderByExecute(iColumn_DetailReport)
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Report Generation "

'    Dim iColumn_DetailReport As New IColumnCollection
'    Public Property Field_OnDetailReport() As IColumnCollection
'        Get
'            Return iColumn_DetailReport
'        End Get
'        Set(ByVal value As IColumnCollection)
'            iColumn_DetailReport = value
'        End Set
'    End Property

'    Private Sub Create_OnDetailReport()
'        Try
'            iColumn_DetailReport.Clear()
'            iColumn_DetailReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 1, "Employee")))
'            iColumn_DetailReport.Add(New IColumn("InTime", Language.getMessage(mstrModuleName, 2, "In Time")))
'            iColumn_DetailReport.Add(New IColumn("OutTime", Language.getMessage(mstrModuleName, 3, "Out Time")))
'            iColumn_DetailReport.Add(New IColumn("BreakHour", Language.getMessage(mstrModuleName, 4, "Break")))
'            iColumn_DetailReport.Add(New IColumn("WorkingHours", Language.getMessage(mstrModuleName, 5, "Working Hours")))
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    'Shani(24-Aug-2015) -- Start
'    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
'    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
'                                           ByVal intUserUnkid As Integer, _
'                                           ByVal intYearUnkid As Integer, _
'                                           ByVal intCompanyUnkid As Integer, _
'                                           ByVal dtPeriodStart As Date, _
'                                           ByVal dtPeriodEnd As Date, _
'                                           ByVal strUserModeSetting As String, _
'                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
'        'Shani(24-Aug-2015) -- End
'        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
'        Dim rpt_Sub As ArutiReport.Designer.dsArutiReport
'        Try
'            objDataOperation = New clsDataOperation

'            'Shani(24-Aug-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
'            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
'            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
'            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
'            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
'            'Shani(24-Aug-2015) -- End

'            objDataOperation.AddParameter("@HalfDay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Half Day"))
'            objDataOperation.AddParameter("@NotEvenHalfDay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Not Even Half Day"))
'            objDataOperation.AddParameter("@HalfDayWithShortHour", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Half Day With Short Hour"))
'            objDataOperation.AddParameter("@Hold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Hold"))


'            StrQ = " SELECT "

'            StrQ &= "  tnalogin_tran.employeeunkid AS EmpId  " & _
'            "      , hremployee_master.employeecode AS EmpCode  " & _
'            "      , ISNULL(hremployee_master.firstname, '') + ' '  " & _
'            "        + ISNULL(hremployee_master.othername, '') + ' '  " & _
'            "        + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
'            "      , CONVERT(CHAR(8), tnalogin_summary.login_date, 112) AS LoginDate  "


'            'Pinkal (15-Nov-2013) -- Start
'            'Enhancement : Oman Changes

'            'StrQ &= " , MIN(CONVERT(CHAR(8), tnalogin_tran.checkintime, 108)) AS InTime  " & _
'            '            "  , MAX(CONVERT(CHAR(8), tnalogin_tran.checkouttime, 108)) AS OutTime  "

'            StrQ &= " ,CASE WHEN MIN(CONVERT(CHAR(8),tnalogin_tran.roundoff_intime, 108))  IS NULL THEN MIN(CONVERT(CHAR(8),tnalogin_tran.checkintime, 108)) ELSE MIN(CONVERT(CHAR(8),tnalogin_tran.roundoff_intime, 108))  END AS InTime  " & _
'                      "  , CASE WHEN MAX(CONVERT(CHAR(8),tnalogin_tran.roundoff_outtime, 108))  IS NULL THEN MAX(CONVERT(CHAR(8), tnalogin_tran.checkouttime, 108)) ELSE MAX(CONVERT(CHAR(8),tnalogin_tran.roundoff_outtime, 108)) END AS OutTime  "

'            'Pinkal (15-Nov-2013) -- End


'            StrQ &= "      , CONVERT(DECIMAL(10, 2), FLOOR(( SUM(tnalogin_tran.breakhr) / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( SUM(tnalogin_tran.breakhr) / 60 ) % 60) / 100 )) AS BreakHour  " & _
'            "      , CONVERT(DECIMAL(10, 2), FLOOR(( SUM(tnalogin_tran.workhour) / 60 )/ 60) + ( CONVERT(DECIMAL(10, 2), ( SUM(tnalogin_tran.workhour) / 60 ) % 60)/ 100 )) AS WorkingHours  " & _
'            "      , CASE WHEN tnalogin_tran.holdunkid <> 0 THEN @Hold  " & _
'                        "             WHEN SUM(tnalogin_tran.workhour) BETWEEN tnashift_tran.halfdayfromhrs  " & _
'                        "                                              AND     tnashift_tran.halfdaytohrs  " & _
'            "             THEN @HalfDay  " & _
'                        "             WHEN SUM(tnalogin_tran.workhour) < tnashift_tran.halfdayfromhrs  " & _
'            "             THEN @NotEvenHalfDay  " & _
'                        "             WHEN SUM(tnalogin_tran.workhour) > tnashift_tran.halfdaytohrs  " & _
'                        "                  AND SUM(tnalogin_tran.workhour) < ( tnashift_tran.workinghrs  " & _
'                        "                                                      - tnashift_tran.calcshorttimebefore )  " & _
'            "             THEN @HalfDayWithShortHour  " & _
'            "             ELSE ' '  " & _
'            "        END AS Employee_Status  " & _
'                        "      , CONVERT(DECIMAL(10,2), FLOOR((tnashift_tran.halfdayfromhrs / 60)/60) + ( CONVERT(DECIMAL(10,2), ( tnashift_tran.halfdayfromhrs / 60) % 60) / 100)) AS FromHour " & _
'                        "      , CONVERT(DECIMAL(10,2), FLOOR((tnashift_tran.halfdaytohrs / 60)/60) + ( CONVERT(DECIMAL(10,2), ( tnashift_tran.halfdaytohrs / 60) % 60) / 100)) AS ToHour "


'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Fields
'            Else
'                StrQ &= ", 0 AS Id, '' AS GName "
'            End If


'            StrQ &= " FROM   tnalogin_tran  " & _
'            "        JOIN hremployee_master ON tnalogin_tran.employeeunkid = hremployee_master.employeeunkid  " & _
'            "        JOIN tnalogin_summary ON tnalogin_summary.employeeunkid = tnalogin_tran.employeeunkid  " & _
'            "                                 AND tnalogin_summary.login_date = tnalogin_tran.logindate  " & _
'            "        JOIN tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid  " & _
'                        "        JOIN tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Join
'            End If

'            'Shani(24-Aug-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If

'            'Shani(24-Aug-2015) -- End
'            StrQ &= " WHERE  ISNULL(tnalogin_tran.isvoid, 0) = 0  " & _
'                        "        AND tnashift_master.isactive = 1 AND tnashift_tran.dayid = " & GetWeekDayNumber(mdtDate.DayOfWeek.ToString()) & _
'                        "        AND tnalogin_summary.login_date = @Date  "

'            'Shani(24-Aug-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'If mblnIsActive = False Then
'            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'            'End If

'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= UserAccessLevel._AccessLevelFilterString
'            'End If

'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'            End If
'            End If
'            'Shani(24-Aug-2015) -- End

'            Call FilterTitleAndFilterQuery()

'            StrQ &= Me._FilterQuery

'            StrQ &= " GROUP BY tnalogin_tran.employeeunkid  " & _
'            "      , hremployee_master.employeecode  " & _
'            "      , ISNULL(hremployee_master.firstname, '') + ' '  " & _
'            "        + ISNULL(hremployee_master.othername, '') + ' '  " & _
'            "        + ISNULL(hremployee_master.surname, '')  " & _
'            "      , CONVERT(CHAR(8), tnalogin_summary.login_date, 112)  " & _
'            "      , tnalogin_tran.holdunkid  " & _
'            "      , tnashift_tran.halfdayfromhrs  " & _
'            "      , tnashift_tran.halfdaytohrs  " & _
'            "      , tnashift_tran.workinghrs  " & _
'            "      , tnashift_tran.calcshorttimebefore  "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Fields.Replace(" AS Id,", ",").Replace(" AS GName", "")
'            End If


'            StrQ &= mstrOrderByQuery

'            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
'            End If

'            rpt_Data = New ArutiReport.Designer.dsArutiReport

'            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
'                Dim rpt_Row As DataRow
'                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

'                rpt_Row.Item("Column1") = dtRow.Item("EmpId")
'                rpt_Row.Item("Column2") = dtRow.Item("EmpCode")
'                rpt_Row.Item("Column3") = dtRow.Item("EmpName")
'                rpt_Row.Item("Column4") = dtRow.Item("LoginDate")

'                'Pinkal (30-Dec-2014) -- Start
'                'Enhancement - CHANGES IN DAILY TIMESHEET REPORT FOR OCEAN LINK

'                If IsDBNull(dtRow.Item("InTime")) = False AndAlso dtRow.Item("InTime").ToString.Trim.Trim.Length > 0 Then
'                    rpt_Row.Item("Column5") = CDate(dtRow.Item("InTime")).ToShortTimeString
'                ElseIf IsDBNull(dtRow.Item("OutTime")) AndAlso dtRow.Item("OutTime").ToString.Trim.Trim.Length <= 0 Then
'                    rpt_Row.Item("Column5") = "***"
'                End If

'                If IsDBNull(dtRow.Item("OutTime")) = False AndAlso dtRow.Item("OutTime").ToString.Trim.Trim.Length > 0 Then
'                    rpt_Row.Item("Column6") = CDate(dtRow.Item("OutTime")).ToShortTimeString
'                ElseIf IsDBNull(dtRow.Item("InTime")) AndAlso dtRow.Item("InTime").ToString.Trim.Trim.Length <= 0 Then
'                    rpt_Row.Item("Column6") = "***"
'                End If
'                'Pinkal (30-Dec-2014) -- End

'                rpt_Row.Item("Column7") = dtRow.Item("BreakHour")
'                rpt_Row.Item("Column8") = dtRow.Item("WorkingHours")
'                rpt_Row.Item("Column9") = dtRow.Item("Employee_Status")
'                rpt_Row.Item("Column10") = dtRow.Item("FromHour")
'                rpt_Row.Item("Column11") = dtRow.Item("ToHour")


'                'Pinkal (03-Nov-2012) -- Start
'                'Enhancement : TRA Changes
'                rpt_Row.Item("Column12") = dtRow.Item("GName")
'                'Pinkal (03-Nov-2012) -- End


'                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
'            Next


'            'START FOR SUBREPORT



'            'Shani(24-Aug-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

'            StrQ = "SELECT " & _
'                     "ISNULL(Planned,'') AS Planned " & _
'                    ",ISNULL(UnPlanned,'') AS UnPlanned " & _
'                    "FROM " & _
'                    "( " & _
'                         "SELECT " & _
'                   "                 ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.surname, '') AS Planned " & _
'                          ",CONVERT(CHAR(8),LVP.leavedate,112) AS Pdate " & _
'                   "                ,ROW_NUMBER() OVER(ORDER BY hremployee_master.employeeunkid ASC) AS RowPId " & _
'                         "FROM lvleaveIssue_master " & _
'                              "JOIN lvleaveIssue_tran AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvp.ispaid = 1 " & _
'                   "                JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
'                   "                AND hremployee_master.employeeunkid NOT in (SELECT employeeunkid FROM tnalogin_tran WHERE logindate = @Date AND isvoid = 0) "

'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If

'            StrQ &= "WHERE LVP.leavedate = @Date AND LVP.isvoid = 0 "

'            If mintEmpId > 0 Then
'                StrQ &= " AND lvleaveIssue_master.employeeunkid  = @EmpLId "
'            End If

'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'            End If
'            End If

'            StrQ &= ") AS Paid " & _
'                         " FULL OUTER JOIN ( SELECT   Unplanned " & _
'                         ", ROW_NUMBER() OVER ( ORDER BY employeeunkid ASC ) AS RowUId " & _
'                         " FROM   " & _
'                         " (    SELECT    hremployee_master.employeeunkid , ISNULL(hremployee_master.firstname, '')  + ' ' + ISNULL(hremployee_master.surname,'') AS Unplanned " & _
'                         " FROM lvleaveIssue_master " & _
'                         " JOIN lvleaveIssue_tran AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid  AND lvp.ispaid = 0 " & _
'                         "      JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
'                         "              AND hremployee_master.employeeunkid NOT IN ( SELECT employeeunkid  FROM " & _
'                         "                    tnalogin_summary WHERE  login_date = @Date  AND isunpaidleave = 1 ) "

'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If

'            StrQ &= " WHERE  LVP.leavedate = @Date AND LVP.isvoid = 0 "

'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'            End If
'            End If

'            StrQ &= " UNION " & _
'                    " SELECT    hremployee_master.employeeunkid , ISNULL(hremployee_master.firstname, '')  + ' ' + ISNULL(hremployee_master.surname,'') AS Unplanned " & _
'                                        " FROM tnalogin_summary " & _
'                    " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid  And isunpaidleave = 1 " & _
'                                        " AND tnalogin_summary.employeeunkid NOT IN ( " & _
'                                    " SELECT  employeeunkid FROM lvleaveIssue_master  JOIN lvleaveIssue_tran " & _
'                                                        " AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvp.ispaid = 0 " & _
'                                    " WHERE leavedate = @Date  AND lvp.isvoid = 0 ) "
'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If

'            StrQ &= " WHERE     tnalogin_summary.login_date = @Date "

'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'            End If
'            End If

'            StrQ &= " UNION " & _
'                                    " SELECT    hremployee_master.employeeunkid,ISNULL(hremployee_master.firstname,'') + ' '  " & _
'                                    " + ISNULL(hremployee_master.surname,'') AS UnPlanned " & _
'                    " FROM hremployee_master "

'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If

'            StrQ &= " WHERE     hremployee_master.employeeunkid NOT IN ( " & _
'                    " Select employeeunkid FROM tnalogin_summary " & _
'                    " WHERE   tnalogin_summary.login_date = @Date ) "

'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'            End If
'            End If


'            StrQ &= " INTERSECT " & _
'                                  "  SELECT  hremployee_master.employeeunkid,  ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.surname,'') AS UnPlanned " & _
'                                  "  FROM hremployee_master "

'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If

'            StrQ &= " WHERE  hremployee_master.employeeunkid NOT IN ( " & _
'                                  " Select employeeunkid  FROM lvleaveIssue_master " & _
'                                  " JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
'                                  " WHERE   lvleaveIssue_tran.leavedate = @Date )"


'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'            End If
'            End If


'            StrQ &= " ) AS m  WHERE 1 = 1"

'            If mintEmpId > 0 Then
'                StrQ &= " AND  m.employeeunkid  = @EmpLId "
'            End If

'            StrQ &= ") AS Unpaid ON Unpaid.RowUId = Paid.RowPId "

'            objDataOperation.AddParameter("@EmpLId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId.ToString)

'            dsList = objDataOperation.ExecQuery(StrQ, "LeaveInfo")

'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
'            End If

'            rpt_Sub = New ArutiReport.Designer.dsArutiReport

'            For Each dtRow As DataRow In dsList.Tables("LeaveInfo").Rows
'                Dim rpt_Row As DataRow
'                rpt_Row = rpt_Sub.Tables("ArutiTable").NewRow

'                rpt_Row.Item("Column1") = dtRow.Item("Planned")
'                rpt_Row.Item("Column2") = dtRow.Item("UnPlanned")

'                rpt_Sub.Tables("ArutiTable").Rows.Add(rpt_Row)
'            Next

'            'END FOR SUBREPORT


'            objRpt = New ArutiReport.Designer.rptDailyTimeSheet

'            'Sandeep [ 10 FEB 2011 ] -- Start
'            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

'            Dim arrImageRow As DataRow = Nothing
'            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

'            ReportFunction.Logo_Display(objRpt, _
'                                        ConfigParameter._Object._IsDisplayLogo, _
'                                        ConfigParameter._Object._ShowLogoRightSide, _
'                                        "arutiLogo1", _
'                                        "arutiLogo2", _
'                                        arrImageRow, _
'                                        "txtCompanyName", _
'                                        "txtReportName", _
'                                        "txtFilterDescription", _
'                                        ConfigParameter._Object._GetLeftMargin, _
'                                        ConfigParameter._Object._GetRightMargin)

'            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

'            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
'                rpt_Data.Tables("ArutiTable").Rows.Add("")
'            End If


'            'Pinkal (03-Nov-2012) -- Start
'            'Enhancement : TRA Changes
'            If mintViewIndex < 0 Then
'                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
'                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
'            End If
'            'Pinkal (03-Nov-2012) -- End


'            If ConfigParameter._Object._IsShowPreparedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
'                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
'            End If

'            If ConfigParameter._Object._IsShowCheckedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 18, "Checked By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
'            End If

'            If ConfigParameter._Object._IsShowApprovedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
'            End If

'            If ConfigParameter._Object._IsShowReceivedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
'            End If
'            'Sandeep [ 10 FEB 2011 ] -- End

'            objRpt.SetDataSource(rpt_Data)
'            objRpt.Subreports("rptLeaveInfo").SetDataSource(rpt_Sub)

'            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 1, "Employee"))
'            Call ReportFunction.TextChange(objRpt, "txtInTime", Language.getMessage(mstrModuleName, 2, "In Time"))
'            Call ReportFunction.TextChange(objRpt, "txtOutTime", Language.getMessage(mstrModuleName, 3, "Out Time"))
'            Call ReportFunction.TextChange(objRpt, "txtBreak", Language.getMessage(mstrModuleName, 4, "Break"))
'            Call ReportFunction.TextChange(objRpt, "txtWorkingHours", Language.getMessage(mstrModuleName, 5, "Working Hours"))
'            Call ReportFunction.TextChange(objRpt, "txtEmpStatus", Language.getMessage(mstrModuleName, 6, "Employee Status"))
'            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtPlanned", Language.getMessage(mstrModuleName, 21, "Planned Leave"))
'            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtUnplanned", Language.getMessage(mstrModuleName, 22, "Unplanned Leave"))
'            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtAbsentDetail", Language.getMessage(mstrModuleName, 23, "Absent Detail"))
'            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtTotalPlanned", Language.getMessage(mstrModuleName, 24, "Total Planned"))
'            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveInfo"), "txtTotalUnplanned", Language.getMessage(mstrModuleName, 25, "Total Unplanned"))

'            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 7, "Printed By :"))
'            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 8, "Printed Date :"))


'            'Pinkal (11-MAY-2012) -- Start
'            'Enhancement : TRA Changes
'            'Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 9, "Page :"))
'            'Pinkal (11-MAY-2012) -- End




'            'Pinkal (03-Nov-2012) -- Start
'            'Enhancement : TRA Changes
'            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
'            'Pinkal (03-Nov-2012) -- End


'            'Pinkal (30-Dec-2014) -- Start
'            'Enhancement - CHANGES IN DAILY TIMESHEET REPORT FOR OCEAN LINK
'            Call ReportFunction.TextChange(objRpt, "txtMispunch", "*** : " & Language.getMessage(mstrModuleName, 27, "Mispunch"))
'            'Pinkal (30-Dec-2014) -- End



'            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
'            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

'            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
'            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
'            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

'            Return objRpt
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    'Pinkal (04-May-2016) -- Start
'    'Enhancement - Adding New Report Type For Hyatt Zanzibar.

'    Private Function Generate_OnDutyReport(ByVal strDatabaseName As String, _
'                                           ByVal intUserUnkid As Integer, _
'                                           ByVal intYearUnkid As Integer, _
'                                           ByVal intCompanyUnkid As Integer, _
'                                           ByVal dtPeriodStart As Date, _
'                                           ByVal dtPeriodEnd As Date, _
'                                           ByVal strUserModeSetting As String, _
'                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
'        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
'        Try
'            objDataOperation = New clsDataOperation

'            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
'            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
'            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
'            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
'            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

'            objDataOperation.AddParameter("@HalfDay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Half Day"))
'            objDataOperation.AddParameter("@NotEvenHalfDay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Not Even Half Day"))
'            objDataOperation.AddParameter("@HalfDayWithShortHour", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Half Day With Short Hour"))
'            objDataOperation.AddParameter("@Hold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Hold"))


'            StrQ = " SELECT "

'            StrQ &= "  tnalogin_tran.employeeunkid AS EmpId  " & _
'            "      , hremployee_master.employeecode AS EmpCode  " & _
'                         "      , hremployee_master.employeecode + ' - '  +  ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
'            "      , CONVERT(CHAR(8), tnalogin_tran.logindate, 112) AS LoginDate  "

'            StrQ &= " ,CASE WHEN MIN(CONVERT(CHAR(8),tnalogin_tran.roundoff_intime, 108))  IS NULL THEN MIN(CONVERT(CHAR(8),tnalogin_tran.checkintime, 108)) ELSE MIN(CONVERT(CHAR(8),tnalogin_tran.roundoff_intime, 108))  END AS InTime  " & _
'                      "  , CASE WHEN MAX(CONVERT(CHAR(8),tnalogin_tran.roundoff_outtime, 108))  IS NULL THEN MAX(CONVERT(CHAR(8), tnalogin_tran.checkouttime, 108)) ELSE MAX(CONVERT(CHAR(8),tnalogin_tran.roundoff_outtime, 108)) END AS OutTime  "

'            StrQ &= "      , CONVERT(DECIMAL(10, 2), FLOOR(( SUM(tnalogin_tran.breakhr) / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( SUM(tnalogin_tran.breakhr) / 60 ) % 60) / 100 )) AS BreakHour  " & _
'                         "      , CONVERT(DECIMAL(10, 2), FLOOR(( SUM(tnalogin_tran.workhour) / 60 )/ 60) + ( CONVERT(DECIMAL(10, 2), ( SUM(tnalogin_tran.workhour) / 60 ) % 60)/ 100 )) AS WorkingHours  "

'            'Pinkal (29-Sep-2016) -- Start
'            'Enhancement - Implementing ACB Changes in TnA Module.
'            StrQ &= ", A.shiftunkid "
'            'Pinkal (29-Sep-2016) -- End

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Fields
'            Else
'                StrQ &= ", 0 AS Id, '' AS GName "
'            End If


'            StrQ &= " FROM   tnalogin_tran  " & _
'                         " JOIN hremployee_master ON tnalogin_tran.employeeunkid = hremployee_master.employeeunkid  "



'            'Pinkal (29-Sep-2016) -- Start
'            'Enhancement - Implementing ACB Changes in TnA Module.
'            StrQ &= " LEFT JOIN " & _
'                           " ( " & _
'                           "        SELECT employeeunkid " & _
'                           "        ,shiftunkid  " & _
'                           "        ,effectivedate " & _
'                           "        ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
'                           "       FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
'                           ") " & _
'                           " AS A  On A.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  "

'            'Pinkal (29-Sep-2016) -- End

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Join
'            End If

'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If

'            StrQ &= " WHERE  ISNULL(tnalogin_tran.isvoid, 0) = 0  " & _
'                        "  AND Convert(char(8),tnalogin_tran.logindate,112) = @Date  "

'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'                End If
'            End If

'            Call FilterTitleAndFilterQuery()

'            StrQ &= Me._FilterQuery

'            StrQ &= " GROUP BY tnalogin_tran.employeeunkid  " & _
'            "      , hremployee_master.employeecode  " & _
'                        "      , A.shiftunkid " & _
'                        "      ,  hremployee_master.employeecode + ' - '  +  ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '')  " & _
'            "      , CONVERT(CHAR(8), tnalogin_tran.logindate, 112)  "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Fields.Replace(" AS Id,", ",").Replace(" AS GName", "")
'            End If


'            StrQ &= mstrOrderByQuery

'            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
'            End If

'            rpt_Data = New ArutiReport.Designer.dsArutiReport

'            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
'                Dim rpt_Row As DataRow
'                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

'                rpt_Row.Item("Column1") = dtRow.Item("EmpId")
'                rpt_Row.Item("Column2") = dtRow.Item("EmpCode")
'                rpt_Row.Item("Column3") = dtRow.Item("EmpName")
'                rpt_Row.Item("Column4") = dtRow.Item("LoginDate")

'                If IsDBNull(dtRow.Item("InTime")) = False AndAlso dtRow.Item("InTime").ToString.Trim.Trim.Length > 0 Then
'                    rpt_Row.Item("Column5") = CDate(dtRow.Item("InTime")).ToShortTimeString
'                ElseIf IsDBNull(dtRow.Item("OutTime")) AndAlso dtRow.Item("OutTime").ToString.Trim.Trim.Length <= 0 Then
'                    rpt_Row.Item("Column5") = "***"
'                End If

'                If IsDBNull(dtRow.Item("OutTime")) = False AndAlso dtRow.Item("OutTime").ToString.Trim.Trim.Length > 0 Then
'                    rpt_Row.Item("Column6") = CDate(dtRow.Item("OutTime")).ToShortTimeString
'                ElseIf IsDBNull(dtRow.Item("InTime")) AndAlso dtRow.Item("InTime").ToString.Trim.Trim.Length <= 0 Then
'                    rpt_Row.Item("Column6") = "***"
'                End If

'                rpt_Row.Item("Column7") = dtRow.Item("BreakHour")
'                rpt_Row.Item("Column8") = dtRow.Item("WorkingHours")
'                rpt_Row.Item("Column12") = dtRow.Item("GName")
'                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
'            Next



'            objRpt = New ArutiReport.Designer.rptDailyTimeSheetEmpOnDuty

'            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

'            Dim arrImageRow As DataRow = Nothing
'            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

'            ReportFunction.Logo_Display(objRpt, _
'                                        ConfigParameter._Object._IsDisplayLogo, _
'                                        ConfigParameter._Object._ShowLogoRightSide, _
'                                        "arutiLogo1", _
'                                        "arutiLogo2", _
'                                        arrImageRow, _
'                                        "txtCompanyName", _
'                                        "txtReportName", _
'                                        "txtFilterDescription", _
'                                        ConfigParameter._Object._GetLeftMargin, _
'                                        ConfigParameter._Object._GetRightMargin)

'            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

'            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
'                rpt_Data.Tables("ArutiTable").Rows.Add("")
'            End If


'            If mintViewIndex < 0 Then
'                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
'                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
'            End If

'            If ConfigParameter._Object._IsShowPreparedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
'                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
'            End If

'            If ConfigParameter._Object._IsShowCheckedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 18, "Checked By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
'            End If

'            If ConfigParameter._Object._IsShowApprovedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
'            End If

'            If ConfigParameter._Object._IsShowReceivedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
'            End If

'            objRpt.SetDataSource(rpt_Data)

'            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 1, "Employee"))
'            Call ReportFunction.TextChange(objRpt, "txtInTime", Language.getMessage(mstrModuleName, 2, "In Time"))
'            Call ReportFunction.TextChange(objRpt, "txtOutTime", Language.getMessage(mstrModuleName, 3, "Out Time"))
'            Call ReportFunction.TextChange(objRpt, "txtBreak", Language.getMessage(mstrModuleName, 4, "Break"))
'            Call ReportFunction.TextChange(objRpt, "txtWorkingHours", Language.getMessage(mstrModuleName, 5, "Working Hours"))
'            Call ReportFunction.TextChange(objRpt, "txtTotalOnDutyEmployee", Language.getMessage(mstrModuleName, 28, "Total On Duty Employee :"))


'            'Pinkal (29-Sep-2016) -- Start
'            'Enhancement - Implementing ACB Changes in TnA Module.
'            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 29, "Sub Total :"))
'            'Pinkal (29-Sep-2016) -- End

'            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 7, "Printed By :"))
'            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 8, "Printed Date :"))
'            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

'            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
'            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate())

'            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
'            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
'            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

'            Return objRpt
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Generate_OnDutyReport; Module Name: " & mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    'Pinkal (29-Sep-2016) -- Start
'    'Enhancement - Implementing ACB Changes in TnA Module.

'    'Private Function Generate_OffDutyReport(ByVal strDatabaseName As String, _
'    '                                      ByVal intUserUnkid As Integer, _
'    '                                      ByVal intYearUnkid As Integer, _
'    '                                      ByVal intCompanyUnkid As Integer, _
'    '                                      ByVal dtPeriodStart As Date, _
'    '                                      ByVal dtPeriodEnd As Date, _
'    '                                      ByVal strUserModeSetting As String, _
'    '                                      ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
'    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'    '    Dim StrQ As String = ""
'    '    Dim dsList As New DataSet
'    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
'    '    Try
'    '        objDataOperation = New clsDataOperation

'    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
'    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
'    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
'    '        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
'    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


'    '        StrQ = "SELECT " & _
'    '                   " ISNULL(Planned,'') AS Planned " & _
'    '                   ",ISNULL(UnPlanned,'') AS UnPlanned " & _
'    '                   ", Id , GName " & _
'    '                   " FROM " & _
'    '                   " ( " & _
'    '                   " SELECT " & _
'    '                   "                   hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') AS Planned " & _
'    '                   "                  ,CONVERT(CHAR(8),LVP.leavedate,112) AS Pdate " & _
'    '                   "                  ,ROW_NUMBER() OVER(ORDER BY hremployee_master.employeeunkid ASC) AS RowPId " & _
'    '                   "                  ,A.shiftunkid "

'    '        If mintViewIndex > 0 Then
'    '            StrQ &= mstrAnalysis_Fields
'    '        Else
'    '            StrQ &= ", 0 AS Id, '' AS GName "
'    '        End If


'    '        StrQ &= " FROM lvleaveIssue_master " & _
'    '                   " JOIN lvleaveIssue_tran AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvp.ispaid = 1 " & _
'    '                   " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
'    '                   " AND hremployee_master.employeeunkid NOT in (SELECT employeeunkid FROM tnalogin_tran WHERE Convert(Char(8),logindate,112) = @Date AND isvoid = 0) "

'    '        'Pinkal (29-Sep-2016) -- Start
'    '        'Enhancement - Implementing ACB Changes in TnA Module.

'    '        StrQ &= " LEFT JOIN  " & _
'    '                    " ( " & _
'    '                    "      Select employeeunkid " & _
'    '                    "     ,shiftunkid  " & _
'    '                    "     ,effectivedate " & _
'    '                    "     ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
'    '                    " FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
'    '                    " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  "

'    '        If mintViewIndex > 0 Then
'    '            StrQ &= mstrAnalysis_Join
'    '        End If

'    '        'Pinkal (29-Sep-2016) -- End


'    '        If xDateJoinQry.Trim.Length > 0 Then
'    '            StrQ &= xDateJoinQry & " "
'    '        End If

'    '        If xUACQry.Trim.Length > 0 Then
'    '            StrQ &= xUACQry & " "
'    '        End If


'    '        StrQ &= "WHERE LVP.leavedate = @Date AND LVP.isvoid = 0 "

'    '        'Pinkal (29-Sep-2016) -- Start
'    '        'Enhancement - Implementing ACB Changes in TnA Module.
'    '        If mintShiftunkid > 0 Then
'    '            StrQ &= " AND A.shiftunkid = " & mintShiftunkid
'    '        End If
'    '        'Pinkal (29-Sep-2016) -- End

'    '        If mintEmpId > 0 Then
'    '            StrQ &= " AND lvleaveIssue_master.employeeunkid  = @EmpId "
'    '        End If

'    '        If xUACFiltrQry.Trim.Length > 0 Then
'    '            StrQ &= " AND " & xUACFiltrQry & " "
'    '        End If

'    '        If mblnIsActive = False Then
'    '            If xDateFilterQry.Trim.Length > 0 Then
'    '                StrQ &= xDateFilterQry & " "
'    '            End If
'    '        End If

'    '        StrQ &= ") AS Paid " & _
'    '                     " FULL OUTER JOIN ( SELECT   Unplanned " & _
'    '                     ", ROW_NUMBER() OVER ( ORDER BY employeeunkid ASC ) AS RowUId " & _
'    '                     " FROM   " & _
'    '                     " (    SELECT    hremployee_master.employeeunkid ,  hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname, '')  + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS Unplanned " & _
'    '                     ", A.shiftunkid "

'    '        If mintViewIndex > 0 Then
'    '            StrQ &= mstrAnalysis_Fields
'    '        Else
'    '            StrQ &= ", 0 AS Id, '' AS GName "
'    '        End If

'    '        StrQ &= " FROM lvleaveIssue_master " & _
'    '                     " JOIN lvleaveIssue_tran AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid  AND lvp.ispaid = 0 " & _
'    '                     " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
'    '                     " AND hremployee_master.employeeunkid NOT IN ( SELECT employeeunkid  FROM " & _
'    '                     " tnalogin_tran WHERE Convert(Char(8),logindate,112) = @Date )   "

'    '        'Pinkal (29-Sep-2016) -- Start
'    '        'Enhancement - Implementing ACB Changes in TnA Module.

'    '        StrQ &= " LEFT JOIN  " & _
'    '                     " ( " & _
'    '                     "      SELECT employeeunkid " & _
'    '                     "      ,shiftunkid " & _
'    '                     "      ,effectivedate " & _
'    '                     "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
'    '                     "    FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
'    '                     " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  "


'    '        If mintViewIndex > 0 Then
'    '            StrQ &= mstrAnalysis_Join
'    '        End If

'    '        'Pinkal (29-Sep-2016) -- End

'    '        If xDateJoinQry.Trim.Length > 0 Then
'    '            StrQ &= xDateJoinQry & " "
'    '        End If

'    '        If xUACQry.Trim.Length > 0 Then
'    '            StrQ &= xUACQry & " "
'    '        End If

'    '        StrQ &= " WHERE  LVP.leavedate = @Date AND LVP.isvoid = 0 "

'    '        'Pinkal (29-Sep-2016) -- Start
'    '        'Enhancement - Implementing ACB Changes in TnA Module.
'    '        If mintShiftunkid > 0 Then
'    '            StrQ &= " AND A.shiftunkid = " & mintShiftunkid
'    '        End If
'    '        'Pinkal (29-Sep-2016) -- End

'    '        If xUACFiltrQry.Trim.Length > 0 Then
'    '            StrQ &= " AND " & xUACFiltrQry & " "
'    '        End If

'    '        If mblnIsActive = False Then
'    '            If xDateFilterQry.Trim.Length > 0 Then
'    '                StrQ &= xDateFilterQry & " "
'    '            End If
'    '        End If

'    '        StrQ &= " UNION " & _
'    '                     " SELECT  hremployee_master.employeeunkid , hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '')   + ' ' + ISNULL(hremployee_master.surname,'') AS Unplanned " & _
'    '                     ", A.shiftunkid "

'    '        If mintViewIndex > 0 Then
'    '            StrQ &= mstrAnalysis_Fields
'    '        Else
'    '            StrQ &= ", 0 AS Id, '' AS GName "
'    '        End If


'    '        StrQ &= " FROM tnalogin_tran " & _
'    '                     " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_tran.employeeunkid  " & _
'    '                     " AND tnalogin_tran.employeeunkid NOT IN ( " & _
'    '                     " SELECT  employeeunkid FROM lvleaveIssue_master  JOIN lvleaveIssue_tran " & _
'    '                     " AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvp.ispaid = 0 " & _
'    '                     " WHERE Convert(Char(8),leavedate,112) = @Date  AND lvp.isvoid = 0 ) "


'    '        'Pinkal (29-Sep-2016) -- Start
'    '        'Enhancement - Implementing ACB Changes in TnA Module.

'    '        StrQ &= " LEFT JOIN  " & _
'    '                     " ( " & _
'    '                     "      SELECT employeeunkid " & _
'    '                     "     ,shiftunkid " & _
'    '                     "     ,effectivedate " & _
'    '                     "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
'    '                     "      FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
'    '                     " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1 "


'    '        If mintViewIndex > 0 Then
'    '            StrQ &= mstrAnalysis_Join
'    '        End If

'    '        'Pinkal (29-Sep-2016) -- End

'    '        If xDateJoinQry.Trim.Length > 0 Then
'    '            StrQ &= xDateJoinQry & " "
'    '        End If

'    '        If xUACQry.Trim.Length > 0 Then
'    '            StrQ &= xUACQry & " "
'    '        End If

'    '        StrQ &= " WHERE Convert(Char(8),tnalogin_tran.logindate,112) = @Date AND tnalogin_tran.checkintime is NULL and tnalogin_tran.checkouttime is NULL"

'    '        'Pinkal (29-Sep-2016) -- Start
'    '        'Enhancement - Implementing ACB Changes in TnA Module.
'    '        If mintShiftunkid > 0 Then
'    '            StrQ &= " AND A.shiftunkid = " & mintShiftunkid
'    '        End If
'    '        'Pinkal (29-Sep-2016) -- End

'    '        If xUACFiltrQry.Trim.Length > 0 Then
'    '            StrQ &= " AND " & xUACFiltrQry & " "
'    '        End If

'    '        If mblnIsActive = False Then
'    '            If xDateFilterQry.Trim.Length > 0 Then
'    '                StrQ &= xDateFilterQry & " "
'    '            End If
'    '        End If

'    '        StrQ &= " UNION " & _
'    '                     " SELECT    hremployee_master.employeeunkid, hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS UnPlanned " & _
'    '                     ", A.shiftunkid "

'    '        If mintViewIndex > 0 Then
'    '            StrQ &= mstrAnalysis_Fields
'    '        Else
'    '            StrQ &= ", 0 AS Id, '' AS GName "
'    '        End If

'    '        StrQ &= " FROM hremployee_master "

'    '        'Pinkal (29-Sep-2016) -- Start
'    '        'Enhancement - Implementing ACB Changes in TnA Module.

'    '        StrQ &= " LEFT JOIN  " & _
'    '                     " ( " & _
'    '                     "      SELECT employeeunkid " & _
'    '                     "     ,shiftunkid " & _
'    '                     "     ,effectivedate " & _
'    '                     "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
'    '                     "      FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
'    '                     " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1 "

'    '        If mintViewIndex > 0 Then
'    '            StrQ &= mstrAnalysis_Join
'    '        End If

'    '        'Pinkal (29-Sep-2016) -- End

'    '        If xDateJoinQry.Trim.Length > 0 Then
'    '            StrQ &= xDateJoinQry & " "
'    '        End If

'    '        If xUACQry.Trim.Length > 0 Then
'    '            StrQ &= xUACQry & " "
'    '        End If

'    '        StrQ &= " WHERE     hremployee_master.employeeunkid NOT IN ( " & _
'    '                     " Select employeeunkid FROM tnalogin_tran " & _
'    '                     " WHERE   Convert(Char(8),logindate,112) = @Date ) "

'    '        'Pinkal (29-Sep-2016) -- Start
'    '        'Enhancement - Implementing ACB Changes in TnA Module.
'    '        If mintShiftunkid > 0 Then
'    '            StrQ &= " AND A.shiftunkid = " & mintShiftunkid
'    '        End If
'    '        'Pinkal (29-Sep-2016) -- End

'    '        If xUACFiltrQry.Trim.Length > 0 Then
'    '            StrQ &= " AND " & xUACFiltrQry & " "
'    '        End If

'    '        If mblnIsActive = False Then
'    '            If xDateFilterQry.Trim.Length > 0 Then
'    '                StrQ &= xDateFilterQry & " "
'    '            End If
'    '        End If


'    '        StrQ &= " INTERSECT " & _
'    '                     "  SELECT  hremployee_master.employeeunkid, hremployee_master.employeecode + ' - '  +  ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS UnPlanned " & _
'    '                     ", A.shiftunkid "

'    '        If mintViewIndex > 0 Then
'    '            StrQ &= mstrAnalysis_Fields
'    '        Else
'    '            StrQ &= ", 0 AS Id, '' AS GName "
'    '        End If


'    '        StrQ &= " FROM hremployee_master "

'    '        'Pinkal (29-Sep-2016) -- Start
'    '        'Enhancement - Implementing ACB Changes in TnA Module.

'    '        StrQ &= " LEFT JOIN  " & _
'    '                     " ( " & _
'    '                     "      SELECT employeeunkid " & _
'    '                     "     ,shiftunkid " & _
'    '                     "     ,effectivedate " & _
'    '                     "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
'    '                     "      FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
'    '                     " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1 "

'    '        If mintViewIndex > 0 Then
'    '            StrQ &= mstrAnalysis_Join
'    '        End If

'    '        'Pinkal (29-Sep-2016) -- End

'    '        If xDateJoinQry.Trim.Length > 0 Then
'    '            StrQ &= xDateJoinQry & " "
'    '        End If

'    '        If xUACQry.Trim.Length > 0 Then
'    '            StrQ &= xUACQry & " "
'    '        End If

'    '        StrQ &= " WHERE  hremployee_master.employeeunkid NOT IN ( " & _
'    '                     " Select employeeunkid  FROM lvleaveIssue_master " & _
'    '                     " JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
'    '                     " WHERE  Convert(Char(8),lvleaveIssue_tran.leavedate,112)   = @Date )"

'    '        'Pinkal (29-Sep-2016) -- Start
'    '        'Enhancement - Implementing ACB Changes in TnA Module.
'    '        If mintShiftunkid > 0 Then
'    '            StrQ &= " AND A.shiftunkid = " & mintShiftunkid
'    '        End If
'    '        'Pinkal (29-Sep-2016) -- End

'    '        If xUACFiltrQry.Trim.Length > 0 Then
'    '            StrQ &= " AND " & xUACFiltrQry & " "
'    '        End If

'    '        If mblnIsActive = False Then
'    '            If xDateFilterQry.Trim.Length > 0 Then
'    '                StrQ &= xDateFilterQry & " "
'    '            End If
'    '        End If


'    '        StrQ &= " ) AS m  WHERE 1 = 1"

'    '        If mintEmpId > 0 Then
'    '            StrQ &= " AND  m.employeeunkid  = @EmpId "
'    '        End If

'    '        StrQ &= ") AS Unpaid ON Unpaid.RowUId = Paid.RowPId "

'    '        FilterTitleAndFilterQuery()

'    '        StrQ &= Me._FilterQuery

'    '        dsList = objDataOperation.ExecQuery(StrQ, "LeaveInfo")

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
'    '        End If

'    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

'    '        For Each dtRow As DataRow In dsList.Tables("LeaveInfo").Rows
'    '            Dim rpt_Row As DataRow
'    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
'    '            rpt_Row.Item("Column1") = dtRow.Item("Planned")
'    '            rpt_Row.Item("Column2") = dtRow.Item("UnPlanned")

'    '            'Pinkal (29-Sep-2016) -- Start
'    '            'Enhancement - Implementing ACB Changes in TnA Module.
'    '            rpt_Row.Item("Column3") = dtRow.Item("GName")
'    '            rpt_Row.Item("Column4") = dtRow.Item("Id")
'    '            'Pinkal (29-Sep-2016) -- End


'    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
'    '        Next

'    '        objRpt = New ArutiReport.Designer.rptDailyTimeSheetOffDuty

'    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

'    '        Dim arrImageRow As DataRow = Nothing
'    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

'    '        ReportFunction.Logo_Display(objRpt, _
'    '                                    ConfigParameter._Object._IsDisplayLogo, _
'    '                                    ConfigParameter._Object._ShowLogoRightSide, _
'    '                                    "arutiLogo1", _
'    '                                    "arutiLogo2", _
'    '                                    arrImageRow, _
'    '                                    "txtCompanyName", _
'    '                                    "txtReportName", _
'    '                                    "txtFilterDescription", _
'    '                                    ConfigParameter._Object._GetLeftMargin, _
'    '                                    ConfigParameter._Object._GetRightMargin)

'    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

'    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
'    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
'    '        End If


'    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
'    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
'    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
'    '        Else
'    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
'    '        End If

'    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
'    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 18, "Checked By :"))
'    '        Else
'    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
'    '        End If

'    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
'    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
'    '        Else
'    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
'    '        End If

'    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
'    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
'    '        Else
'    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
'    '        End If

'    '        objRpt.SetDataSource(rpt_Data)
'    '        Call ReportFunction.TextChange(objRpt, "txtPlanned", Language.getMessage(mstrModuleName, 21, "Planned Leave"))
'    '        Call ReportFunction.TextChange(objRpt, "txtUnplanned", Language.getMessage(mstrModuleName, 22, "Unplanned Leave"))
'    '        Call ReportFunction.TextChange(objRpt, "txtTotalPlanned", Language.getMessage(mstrModuleName, 24, "Total Planned"))
'    '        Call ReportFunction.TextChange(objRpt, "txtTotalUnplanned", Language.getMessage(mstrModuleName, 25, "Total Unplanned"))
'    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 7, "Printed By :"))
'    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 8, "Printed Date :"))


'    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
'    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate())

'    '        Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
'    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
'    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

'    '        Return objRpt
'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_OffDutyReport; Module Name: " & mstrModuleName)
'    '        Return Nothing
'    '    End Try
'    'End Function


'    Private Function Generate_OffDutyReport(ByVal strDatabaseName As String, _
'                                          ByVal intUserUnkid As Integer, _
'                                          ByVal intYearUnkid As Integer, _
'                                          ByVal intCompanyUnkid As Integer, _
'                                          ByVal dtPeriodStart As Date, _
'                                          ByVal dtPeriodEnd As Date, _
'                                          ByVal strUserModeSetting As String, _
'                                          ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
'        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
'        Try
'            objDataOperation = New clsDataOperation

'            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
'            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
'            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
'            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
'            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


'            StrQ = "SELECT " & _
'                       "                   hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') AS Planned, '' AS Unplanned " & _
'                       "                ,ROW_NUMBER() OVER(ORDER BY hremployee_master.employeeunkid ASC) AS RowPId " & _
'                       "                  ,A.shiftunkid "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Fields
'            Else
'                StrQ &= ", 0 AS Id, '' AS GName "
'            End If


'            StrQ &= " FROM lvleaveIssue_master " & _
'                       " JOIN lvleaveIssue_tran AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvp.ispaid = 1 " & _
'                       " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
'                       " AND hremployee_master.employeeunkid NOT in (SELECT employeeunkid FROM tnalogin_tran WHERE Convert(Char(8),logindate,112) = @Date AND isvoid = 0) " & _
'                       "  LEFT JOIN  " & _
'                        " ( " & _
'                        "      Select employeeunkid " & _
'                        "     ,shiftunkid  " & _
'                        "     ,effectivedate " & _
'                        "     ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
'                        " FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
'                        " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Join
'            End If


'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If


'            StrQ &= "WHERE LVP.leavedate = @Date AND LVP.isvoid = 0 "

'            If mintShiftunkid > 0 Then
'                StrQ &= " AND A.shiftunkid = " & mintShiftunkid
'            End If

'            If mintEmpId > 0 Then
'                StrQ &= " AND lvleaveIssue_master.employeeunkid  = @EmpId "
'            End If

'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'                End If
'            End If


'            FilterTitleAndFilterQuery()

'            StrQ &= Me._FilterQuery

'            Dim dsPlannedList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
'            End If


'            StrQ = " SELECT '' AS Planned, Unplanned " & _
'                         ", ROW_NUMBER() OVER ( ORDER BY employeeunkid ASC ) AS RowUId " & _
'                         ", shiftunkid " & _
'                         ", Id , GName " & _
'                         " FROM   " & _
'                         " (    SELECT    hremployee_master.employeeunkid ,  hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname, '')  + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS Unplanned " & _
'                         ", A.shiftunkid "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Fields
'            Else
'                StrQ &= ", 0 AS Id, '' AS GName "
'            End If

'            StrQ &= " FROM lvleaveIssue_master " & _
'                         " JOIN lvleaveIssue_tran AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid  AND lvp.ispaid = 0 " & _
'                         " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
'                         " AND hremployee_master.employeeunkid NOT IN ( SELECT employeeunkid  FROM " & _
'                         " tnalogin_tran WHERE Convert(Char(8),logindate,112) = @Date )   " & _
'                         " LEFT JOIN  " & _
'                         " ( " & _
'                         "      SELECT employeeunkid " & _
'                         "      ,shiftunkid " & _
'                         "      ,effectivedate " & _
'                         "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
'                         "    FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
'                         " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  "


'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Join
'            End If

'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If

'            StrQ &= " WHERE  LVP.leavedate = @Date AND LVP.isvoid = 0 "


'            If mintShiftunkid > 0 Then
'                StrQ &= " AND A.shiftunkid = " & mintShiftunkid
'            End If

'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'                End If
'            End If

'            StrQ &= " UNION " & _
'                         " SELECT  hremployee_master.employeeunkid , hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '')   + ' ' + ISNULL(hremployee_master.surname,'') AS Unplanned " & _
'                         ", A.shiftunkid "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Fields
'            Else
'                StrQ &= ", 0 AS Id, '' AS GName "
'            End If


'            StrQ &= " FROM tnalogin_tran " & _
'                         " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_tran.employeeunkid  " & _
'                         " AND tnalogin_tran.employeeunkid NOT IN ( " & _
'                         " SELECT  employeeunkid FROM lvleaveIssue_master  JOIN lvleaveIssue_tran " & _
'                         " AS LVP ON LVP.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvp.ispaid = 0 " & _
'                         " WHERE Convert(Char(8),leavedate,112) = @Date  AND lvp.isvoid = 0 ) " & _
'                         " LEFT JOIN  " & _
'                         " ( " & _
'                         "      SELECT employeeunkid " & _
'                         "     ,shiftunkid " & _
'                         "     ,effectivedate " & _
'                         "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
'                         "      FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
'                         " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1 "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Join
'            End If

'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If

'            StrQ &= " WHERE Convert(Char(8),tnalogin_tran.logindate,112) = @Date AND tnalogin_tran.checkintime is NULL and tnalogin_tran.checkouttime is NULL"

'            If mintShiftunkid > 0 Then
'                StrQ &= " AND A.shiftunkid = " & mintShiftunkid
'            End If

'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'                End If
'            End If

'            StrQ &= " UNION " & _
'                         " SELECT    hremployee_master.employeeunkid, hremployee_master.employeecode + ' - '  + ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS UnPlanned " & _
'                         ", A.shiftunkid "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Fields
'            Else
'                StrQ &= ", 0 AS Id, '' AS GName "
'            End If

'            StrQ &= " FROM hremployee_master " & _
'                         " LEFT JOIN  " & _
'                         " ( " & _
'                         "      SELECT employeeunkid " & _
'                         "     ,shiftunkid " & _
'                         "     ,effectivedate " & _
'                         "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
'                         "      FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
'                         " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1 "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Join
'            End If

'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If

'            StrQ &= " WHERE     hremployee_master.employeeunkid NOT IN ( " & _
'                         " Select employeeunkid FROM tnalogin_tran " & _
'                         " WHERE   Convert(Char(8),logindate,112) = @Date ) "

'            If mintShiftunkid > 0 Then
'                StrQ &= " AND A.shiftunkid = " & mintShiftunkid
'            End If

'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'                End If
'            End If


'            StrQ &= " INTERSECT " & _
'                         "  SELECT  hremployee_master.employeeunkid, hremployee_master.employeecode + ' - '  +  ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname,'') AS UnPlanned " & _
'                         ", A.shiftunkid "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Fields
'            Else
'                StrQ &= ", 0 AS Id, '' AS GName "
'            End If


'            StrQ &= " FROM hremployee_master " & _
'                         " LEFT JOIN  " & _
'                         " ( " & _
'                         "      SELECT employeeunkid " & _
'                         "     ,shiftunkid " & _
'                         "     ,effectivedate " & _
'                         "      ,ROW_NUMBER() OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) As RowNo " & _
'                         "      FROM hremployee_shift_tran where effectivedate <= @Date and isvoid = 0 " & _
'                         " ) AS A On a.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1 "

'            If mintViewIndex > 0 Then
'                StrQ &= mstrAnalysis_Join
'            End If

'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry & " "
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry & " "
'            End If

'            StrQ &= " WHERE  hremployee_master.employeeunkid NOT IN ( " & _
'                         " Select employeeunkid  FROM lvleaveIssue_master " & _
'                         " JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
'                         " WHERE  Convert(Char(8),lvleaveIssue_tran.leavedate,112)   = @Date )"

'            If mintShiftunkid > 0 Then
'                StrQ &= " AND A.shiftunkid = " & mintShiftunkid
'            End If

'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If mblnIsActive = False Then
'                If xDateFilterQry.Trim.Length > 0 Then
'                    StrQ &= xDateFilterQry & " "
'                End If
'            End If


'            StrQ &= " ) AS m  WHERE 1 = 1"

'            If mintEmpId > 0 Then
'                StrQ &= " AND  m.employeeunkid  = @EmpId "
'            End If

'            'FilterTitleAndFilterQuery()

'            'StrQ &= Me._FilterQuery

'            Dim dsUnplannedList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
'            End If

'            Dim dsFinal As DataSet = Nothing
'            If dsPlannedList IsNot Nothing AndAlso dsUnplannedList IsNot Nothing Then

'                dsFinal = dsPlannedList.Clone()
'                If dsUnplannedList.Tables(0).Rows.Count > dsPlannedList.Tables(0).Rows.Count Then

'                    If mintViewIndex <= 0 Then 'WHEN ALLOCATION IS NOT USED UNPLANNED EMPLOYEE MORE THAN PLANNED EMPLOYEE.
'                        dsFinal = dsUnplannedList.Copy
'                        dsPlannedList.Tables(0).AsEnumerable.ToList.ForEach(Function(x) UpdateRow(x, dsPlannedList, dsFinal, True))

'                    ElseIf mintViewIndex > 0 Then

'                        Dim objDept As New clsDepartment
'                        Dim dsDepartment As DataSet = objDept.getComboList("List", False)
'                        objDept = Nothing

'                        Dim xAllocationId As Integer = 0
'                        For Each drDept As DataRow In dsDepartment.Tables(0).Rows
'                            Dim drUnplanned As DataRow() = dsUnplannedList.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
'                            If drUnplanned.Length > 0 Then
'                                For Each dr As DataRow In drUnplanned
'                                    dsFinal.Tables(0).ImportRow(dr)
'                                Next
'                            End If

'                            Dim drPlanned As DataRow() = dsPlannedList.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
'                            If drPlanned.Length > 0 Then
'                                Dim drRow As DataRow() = dsFinal.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
'                                If drRow.Length > 0 Then
'                                    drPlanned.ToList.ForEach(Function(x) UpdateGroupRow(x, drRow, True))
'                                Else
'                                    For Each dr As DataRow In drPlanned
'                                        dsFinal.Tables(0).ImportRow(dr)
'                                    Next
'                                End If
'                            End If
'                            dsFinal.AcceptChanges()
'                        Next

'                    End If

'                Else

'                    If mintViewIndex <= 0 Then  'WHEN ALLOCATION IS NOT USED PLANNED EMPLOYEE MORE THAN UNPLANNED EMPLOYEE.
'                        dsFinal = dsPlannedList.Copy
'                        dsUnplannedList.Tables(0).AsEnumerable.ToList.ForEach(Function(x) UpdateRow(x, dsUnplannedList, dsFinal, False))

'                    ElseIf mintViewIndex > 0 Then

'                        Dim objDept As New clsDepartment
'                        Dim dsDepartment As DataSet = objDept.getComboList("List", False)
'                        objDept = Nothing

'                        Dim xAllocationId As Integer = 0
'                        For Each drDept As DataRow In dsDepartment.Tables(0).Rows
'                            Dim drplanned As DataRow() = dsPlannedList.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
'                            If drplanned.Length > 0 Then
'                                For Each dr As DataRow In drplanned
'                                    dsFinal.Tables(0).ImportRow(dr)
'                                Next
'                            End If

'                            Dim drUnPlanned As DataRow() = dsUnplannedList.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
'                            If drUnPlanned.Length > 0 Then
'                                Dim drRow As DataRow() = dsFinal.Tables(0).Select("Id = " & CInt(drDept("departmentunkid")))
'                                If drRow.Length > 0 Then
'                                    drUnPlanned.ToList.ForEach(Function(x) UpdateGroupRow(x, drRow, False))
'                                Else
'                                    For Each dr As DataRow In drUnPlanned
'                                        dsFinal.Tables(0).ImportRow(dr)
'                                    Next
'                                End If
'                            End If
'                            dsFinal.AcceptChanges()
'                        Next

'                    End If

'                End If

'            End If

'            rpt_Data = New ArutiReport.Designer.dsArutiReport

'            For Each dtRow As DataRow In dsFinal.Tables(0).Rows
'                Dim rpt_Row As DataRow
'                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
'                rpt_Row.Item("Column1") = dtRow.Item("Planned")
'                rpt_Row.Item("Column2") = dtRow.Item("UnPlanned")

'                'Pinkal (29-Sep-2016) -- Start
'                'Enhancement - Implementing ACB Changes in TnA Module.
'                rpt_Row.Item("Column3") = dtRow.Item("GName")
'                rpt_Row.Item("Column4") = dtRow.Item("Id")
'                'Pinkal (29-Sep-2016) -- End


'                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
'            Next

'            objRpt = New ArutiReport.Designer.rptDailyTimeSheetOffDuty

'            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

'            Dim arrImageRow As DataRow = Nothing
'            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

'            ReportFunction.Logo_Display(objRpt, _
'                                        ConfigParameter._Object._IsDisplayLogo, _
'                                        ConfigParameter._Object._ShowLogoRightSide, _
'                                        "arutiLogo1", _
'                                        "arutiLogo2", _
'                                        arrImageRow, _
'                                        "txtCompanyName", _
'                                        "txtReportName", _
'                                        "txtFilterDescription", _
'                                        ConfigParameter._Object._GetLeftMargin, _
'                                        ConfigParameter._Object._GetRightMargin)

'            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

'            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
'                rpt_Data.Tables("ArutiTable").Rows.Add("")
'            End If

'            If mintViewIndex < 0 Then
'                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
'                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
'            End If


'            If ConfigParameter._Object._IsShowPreparedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
'                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
'            End If

'            If ConfigParameter._Object._IsShowCheckedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 18, "Checked By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
'            End If

'            If ConfigParameter._Object._IsShowApprovedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
'            End If

'            If ConfigParameter._Object._IsShowReceivedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
'            End If

'            objRpt.SetDataSource(rpt_Data)
'            Call ReportFunction.TextChange(objRpt, "txtPlanned", Language.getMessage(mstrModuleName, 21, "Planned Leave"))
'            Call ReportFunction.TextChange(objRpt, "txtUnplanned", Language.getMessage(mstrModuleName, 22, "Unplanned Leave"))
'            Call ReportFunction.TextChange(objRpt, "txtTotalPlanned", Language.getMessage(mstrModuleName, 24, "Total Planned"))
'            Call ReportFunction.TextChange(objRpt, "txtTotalUnplanned", Language.getMessage(mstrModuleName, 25, "Total Unplanned"))
'            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 7, "Printed By :"))
'            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 8, "Printed Date :"))

'            Call ReportFunction.TextChange(objRpt, "txtPlannedSubTotal", Language.getMessage(mstrModuleName, 29, "Sub Total :"))
'            Call ReportFunction.TextChange(objRpt, "txtUnPlannedSubTotal", Language.getMessage(mstrModuleName, 29, "Sub Total :"))

'            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
'            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
'            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate())

'            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
'            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
'            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

'            Return objRpt
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Generate_OffDutyReport; Module Name: " & mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    Public Function UpdateRow(ByVal dr As DataRow, ByVal dsList As DataSet, ByVal dsFinal As DataSet, ByVal mblnIsPlanned As Boolean) As Boolean
'        Try
'            If dsFinal IsNot Nothing AndAlso dsFinal.Tables(0).Rows.Count > 0 Then
'                Dim xIndex As Integer = dsList.Tables(0).Rows.IndexOf(dr)
'                If xIndex <= dsFinal.Tables(0).Rows.Count Then
'                    If mblnIsPlanned Then
'                        dsFinal.Tables(0).Rows(xIndex)("planned") = dr("planned").ToString()
'                    Else
'                        dsFinal.Tables(0).Rows(xIndex)("unplanned") = dr("unplanned").ToString()
'                    End If
'                    dsFinal.AcceptChanges()
'                End If
'            End If
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: UpdateRow; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'        Return True
'    End Function

'    Public Function UpdateGroupRow(ByVal dr As DataRow, ByVal drFinal As DataRow(), ByVal mblnIsPlanned As Boolean) As Boolean
'        Try
'            If dr IsNot Nothing AndAlso drFinal IsNot Nothing Then
'                Dim xName As String = ""
'                For Each drRow As DataRow In drFinal
'                    If xName <> dr("planned").ToString() Then
'                        If mblnIsPlanned Then
'                            If drRow("planned").ToString.Trim.Length <= 0 Then
'                                drRow("planned") = dr("planned").ToString()
'                            Else
'                                Continue For
'                            End If
'                        Else
'                            If drRow("unplanned").ToString.Trim.Length <= 0 Then
'                                drRow("unplanned") = dr("unplanned").ToString()
'                            Else
'                                Continue For
'                            End If
'                        End If
'                        drRow.AcceptChanges()
'                        xName = dr("planned").ToString()
'                        Exit For
'                    End If
'                Next

'            End If
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: UpdateGroupRow; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'        Return True
'    End Function

'    Public Function DeleteRow(ByVal dr As DataRow) As Boolean
'        Try
'            dr.Delete()
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: DeleteRow; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'        Return True
'    End Function

'    'Pinkal (29-Sep-2016) -- End


'#End Region

'#Region "Messages"
'    '1, "Employee"
'    '2, "In Time"
'    '3, "Out Time"
'    '4, "Break"
'    '5, "Working Hours"
'    '6, "Employee Status"
'    '7, "Printed By :"
'    '8, "Printed Date :"
'    '9, "Page :"
'    '10, "Half Day"
'    '11, "Not Even Half Day"
'    '12, "Half Day With Short Hour"
'    '13, "Hold"
'    '14, " Date: "
'    '15, " Employee : "
'    '16, " Order By : "
'#End Region
'    '<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'    Public Shared Sub SetMessages()
'        Try
'            Language.setMessage(mstrModuleName, 1, "Employee")
'            Language.setMessage(mstrModuleName, 2, "In Time")
'            Language.setMessage(mstrModuleName, 3, "Out Time")
'            Language.setMessage(mstrModuleName, 4, "Break")
'            Language.setMessage(mstrModuleName, 5, "Working Hours")
'            Language.setMessage(mstrModuleName, 6, "Employee Status")
'            Language.setMessage(mstrModuleName, 7, "Printed By :")
'            Language.setMessage(mstrModuleName, 8, "Printed Date :")
'            Language.setMessage(mstrModuleName, 10, "Half Day")
'            Language.setMessage(mstrModuleName, 11, "Not Even Half Day")
'            Language.setMessage(mstrModuleName, 12, "Half Day With Short Hour")
'            Language.setMessage(mstrModuleName, 13, "Hold")
'            Language.setMessage(mstrModuleName, 14, " Date:")
'            Language.setMessage(mstrModuleName, 15, " Employee :")
'            Language.setMessage(mstrModuleName, 16, " Order By :")
'            Language.setMessage(mstrModuleName, 17, "Prepared By :")
'            Language.setMessage(mstrModuleName, 18, "Checked By :")
'            Language.setMessage(mstrModuleName, 19, "Approved By :")
'            Language.setMessage(mstrModuleName, 20, "Received By :")
'            Language.setMessage(mstrModuleName, 21, "Planned Leave")
'            Language.setMessage(mstrModuleName, 22, "Unplanned Leave")
'            Language.setMessage(mstrModuleName, 23, "Absent Detail")
'            Language.setMessage(mstrModuleName, 24, "Total Planned")
'            Language.setMessage(mstrModuleName, 25, "Total Unplanned")
'            Language.setMessage(mstrModuleName, 26, " Shift :")
'            Language.setMessage(mstrModuleName, 27, "Mispunch")
'			Language.setMessage(mstrModuleName, 28, "Total On Duty Employee :")
'            Language.setMessage(mstrModuleName, 29, "Sub Total :")

'        Catch Ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
'        End Try
'    End Sub
'#End Region 'Language & UI Settings
'    '</Language>
'End Class
