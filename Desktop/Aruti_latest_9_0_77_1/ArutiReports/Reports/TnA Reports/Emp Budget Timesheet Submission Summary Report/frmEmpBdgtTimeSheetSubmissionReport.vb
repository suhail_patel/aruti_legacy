Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

Public Class frmEmpBdgtTimeSheetSubmissionReport

#Region "Private Variables"

    Private mstrModuleName As String = "frmEmpBdgtTimeSheetSubmissionReport"
    Private objEmpTimesheet As clsEmpBudgetTimesheetSubmissionReport
    Private dsAllocation As DataSet = Nothing
    Dim mdtStartDate As Date = Nothing
    Dim mdtEndDate As Date = Nothing
    Private mstrAllocationIds As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region "Private Function"

    Private Sub FillCombo()
        Try
            Dim objEmployee As New clsEmployee_Master

            Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsList.Tables(0)
            cboEmployee.SelectedValue = 0

            objEmployee = Nothing
            dsList = Nothing

            Dim objFromPeriod As New clscommom_period_Tran

            dsList = objFromPeriod.getListForCombo(enModuleReference.Payroll, _
                                               FinancialYear._Object._YearUnkid, _
                                               FinancialYear._Object._DatabaseName, _
                                               FinancialYear._Object._Database_Start_Date, "From_Period", True)
            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0

            End With


            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0).Copy()
                .SelectedValue = 0

            End With

            objFromPeriod = Nothing
            dsList = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillAllocation()
        Try
            Dim objMasterData As New clsMasterData
            dsAllocation = objMasterData.GetEAllocation_Notification("List")
            Dim lvItem As ListViewItem
            lvAlloction.Items.Clear()
            For Each drRow As DataRow In dsAllocation.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("name").ToString()
                lvItem.Tag = drRow("id")
                lvAlloction.Items.Add(lvItem)
            Next
            lvAlloction.GridLines = False

            If lvAlloction.Items.Count > 10 Then
                colhAllocation.Width = 315 - 18
            Else
                colhAllocation.Width = 315
            End If

            objMasterData = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAllocation", mstrModuleName)
        End Try
    End Sub

    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet

        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Employee_Timesheet_Submission_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                mstrAllocationIds = dsList.Tables(0).Rows(0)("transactionheadid").ToString()

                If mstrAllocationIds.Trim.Length > 0 Then

                    For Each ar In mstrAllocationIds.Trim.Split(",")
                        For Each lvItem As ListViewItem In lvAlloction.Items
                            If CInt(lvItem.Tag) = CInt(ar.Trim()) Then
                                lvItem.Checked = True
                                Exit For
                            End If
                        Next
                    Next

                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetValue() As Boolean
        Try

            If cboFromPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "From Period is compulsory infomation. Please select from period to continue."), enMsgBoxStyle.Information)
                cboFromPeriod.Focus()
                Return False
            ElseIf cboToPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "To Period is compulsory infomation. Please select to period to continue."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            End If


            objEmpTimesheet.SetDefaultValue()

            objEmpTimesheet._FromPeriodId = CInt(cboFromPeriod.SelectedValue)
            objEmpTimesheet._FromPeriodName = cboFromPeriod.Text

            objEmpTimesheet._ToPeriodId = CInt(cboToPeriod.SelectedValue)
            objEmpTimesheet._ToPeriodName = cboToPeriod.Text

            objEmpTimesheet._EmployeeID = CInt(cboEmployee.SelectedValue)
            objEmpTimesheet._EmployeeName = cboEmployee.Text


            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboFromPeriod.SelectedValue)
            mdtStartDate = objPeriod._TnA_StartDate.Date

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            mdtEndDate = objPeriod._TnA_EndDate.Date
            objPeriod = Nothing

            objEmpTimesheet._StartDate = mdtStartDate
            objEmpTimesheet._EndDate = mdtEndDate


            If mdtStartDate.Date > mdtEndDate.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "To Period cannot be less than From Period."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
                Return False
            End If

            Dim dRow As DataRowView = CType(cboEmployee.SelectedItem, DataRowView)
            If dRow IsNot Nothing AndAlso dRow.DataView.Count > 0 Then
                objEmpTimesheet._EmployeeCode = dRow("employeecode").ToString()
            End If


            Dim strColumns As String = ""
            Dim strJoins As String = ""
            Dim strJoinColumns As String = ""
            Dim strJobColumns As String = ""
            Dim strJobJoinColumns As String = ""
            Dim mblnIsCostCenter As Boolean = False


            For Each lvItem As ListViewItem In lvAlloction.CheckedItems

                Select Case CInt(lvItem.Tag)

                    Case enAllocation.BRANCH
                        strColumns = ",ISNULL(SM.name,'') AS [Branch] "
                        strJoinColumns = ",stationunkid "
                        strJoins = " LEFT JOIN hrstation_master AS SM ON SM.stationunkid = Alloc.stationunkid "

                    Case enAllocation.DEPARTMENT_GROUP
                        strColumns &= ",ISNULL(DGM.name,'') AS [Department Group] "
                        strJoinColumns &= ",deptgroupunkid "
                        strJoins &= " LEFT JOIN hrdepartment_group_master AS DGM ON Alloc.deptgroupunkid = DGM.deptgroupunkid "

                    Case enAllocation.DEPARTMENT
                        strColumns &= ",ISNULL(DM.name,'') AS [Department] "
                        strJoinColumns &= ",departmentunkid "
                        strJoins &= " LEFT JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid "

                    Case enAllocation.SECTION_GROUP
                        strColumns &= ",ISNULL(SGM.name,'') AS [Section Group] "
                        strJoinColumns &= ",sectiongroupunkid "
                        strJoins &= " LEFT JOIN hrsectiongroup_master AS SGM ON Alloc.sectiongroupunkid = SGM.sectiongroupunkid "

                    Case enAllocation.SECTION
                        strColumns &= ",ISNULL(SECM.name,'') AS [Section] "
                        strJoinColumns &= ",sectionunkid "
                        strJoins &= " LEFT JOIN hrsection_master AS SECM ON SECM.sectionunkid  = Alloc.sectionunkid "

                    Case enAllocation.UNIT_GROUP
                        strColumns &= ",ISNULL(UGM.name,'') AS [Unit Group] "
                        strJoinColumns &= ",unitgroupunkid "
                        strJoins &= " LEFT JOIN hrunitgroup_master AS UGM ON Alloc.unitgroupunkid = UGM.unitgroupunkid "

                    Case enAllocation.UNIT
                        strColumns &= ",ISNULL(UM.name,'') AS [Unit] "
                        strJoinColumns &= ",unitunkid "
                        strJoins &= " LEFT JOIN hrunit_master AS UM ON UM.unitunkid = Alloc.unitunkid "

                    Case enAllocation.TEAM
                        strColumns &= ",ISNULL(TM.name,'') AS [Team] "
                        strJoinColumns &= ",teamunkid "
                        strJoins &= " LEFT JOIN hrteam_master AS TM ON Alloc.teamunkid = TM.teamunkid "

                    Case enAllocation.JOB_GROUP
                        strColumns &= ",ISNULL(JGM.name,'') AS [Job Group] "
                        strJobColumns &= ",jobgroupunkid "
                        strJobJoinColumns &= " LEFT JOIN hrjobgroup_master AS JGM ON Jobs.jobgroupunkid = JGM.jobgroupunkid "

                    Case enAllocation.JOBS
                        strColumns &= ",ISNULL(JM.job_name,'') AS [Job] "
                        strJobColumns &= ",jobunkid "
                        strJobJoinColumns &= " JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid "

                    Case enAllocation.CLASS_GROUP
                        strColumns &= ",ISNULL(CGM.name,'') AS [Class Group]"
                        strJoinColumns &= ",classgroupunkid "
                        strJoins &= " LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid "

                    Case enAllocation.CLASSES
                        strColumns &= ",ISNULL(CM.name,'') AS [Class] "
                        strJoinColumns &= ",classunkid "
                        strJoins &= " LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid "

                    Case enAllocation.COST_CENTER
                        strColumns &= ",ISNULL(CCM.costcentername,'') AS [CostCenter] "
                        mblnIsCostCenter = True

                End Select

            Next


            objEmpTimesheet._AllocationIds = mstrAllocationIds
            objEmpTimesheet._AllocationColumns = strColumns
            objEmpTimesheet._AllocationJoinColumns = strJoinColumns.Trim
            objEmpTimesheet._AllocationJoins = strJoins
            objEmpTimesheet._JobColumns = strJobColumns.Trim()
            objEmpTimesheet._JobJoinColumns = strJobJoinColumns.Trim()
            objEmpTimesheet._IsCostCenterInAllocation = mblnIsCostCenter

            objEmpTimesheet._ViewByIds = mstrViewByIds
            objEmpTimesheet._ViewIndex = mintViewIndex
            objEmpTimesheet._ViewByName = mstrViewByName
            objEmpTimesheet._Analysis_Fields = mstrAnalysis_Fields
            objEmpTimesheet._Analysis_Join = mstrAnalysis_Join
            objEmpTimesheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpTimesheet._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objEmpTimesheet._Report_GroupName = mstrReport_GroupName
            objEmpTimesheet._Advance_Filter = mstrAdvanceFilter

            objEmpTimesheet._ExportReportPath = ConfigParameter._Object._ExportReportPath
            objEmpTimesheet._OpenAfterExport = ConfigParameter._Object._OpenAfterExport


            'Pinkal (18-Dec-2017) -- Start
            'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].
            objEmpTimesheet._ShowApprovedColumn = chkShowApprovedColumn.Checked
            objEmpTimesheet._ShowLeaveColumn = chkShowLeaveColumn.Checked
            'Pinkal (18-Dec-2017) -- End

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
            objEmpTimesheet._AllowOvertimeInEmpTimesheet = ConfigParameter._Object._AllowOverTimeToEmpTimesheet
            'Pinkal (28-Mar-2018) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub ResetValue()
        Try
            cboFromPeriod.SelectedValue = 0
            cboToPeriod.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            GetValue()
            mstrAdvanceFilter = String.Empty
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""

            'Pinkal (18-Dec-2017) -- Start
            'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].
            chkShowApprovedColumn.Checked = True
            chkShowLeaveColumn.Checked = True
            'Pinkal (18-Dec-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (25-Nov-2020) -- Start
    'Enhancement IHI - Working on Import Timesheet Approver.
#Region "Constructor"

    Public Sub New()
        objEmpTimesheet = New clsEmpBudgetTimesheetSubmissionReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objEmpTimesheet.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region
    'Pinkal (25-Nov-2020) -- End

#Region "Form's Events"

    Private Sub frmEmpBdgtTimeSheetSubmissionReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objEmpTimesheet._ReportName
            eZeeHeader.Message = objEmpTimesheet._ReportDesc
            Call FillCombo()
            Call FillAllocation()
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpBdgtTimeSheetSubmissionReport_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmpBdgtTimeSheetSubmissionReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpTimesheet = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpBdgtTimeSheetSubmissionReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpBdgtTimeSheetSubmissionReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpBudgetTimesheetReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmpBudgetTimesheetSubmissionReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpBudgetTimesheetSubmissionReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Dim mblnFlag As Boolean = False
        Try

            mstrAllocationIds = String.Join(",", lvAlloction.CheckedItems.Cast(Of ListViewItem).Select(Function(x) x.Tag.ToString).ToArray)

            If mstrAllocationIds.Trim.Length > 0 Then
                objUserDefRMode._Reportunkid = enArutiReport.Employee_Timesheet_Submission_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0
                objUserDefRMode._Headtypeid = 1
                objUserDefRMode._EarningTranHeadIds = mstrAllocationIds
                intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Timesheet_Submission_Report, 0, 0, objUserDefRMode._Headtypeid)

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    mblnFlag = objUserDefRMode.Insert()
                Else
                    mblnFlag = objUserDefRMode.Update()
                End If

                If mblnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(objUserDefRMode._Message)
                    Exit Sub
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetValue() = False Then Exit Sub
            objEmpTimesheet.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                        , Company._Object._Companyunkid, mdtEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchFromPeriod_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchFromPeriod.Click
        Try
            Dim objFrm As New frmCommonSearch
            objFrm.DataSource = CType(cboFromPeriod.DataSource, DataTable)
            objFrm.ValueMember = cboFromPeriod.ValueMember
            objFrm.DisplayMember = cboFromPeriod.DisplayMember

            If objFrm.DisplayDialog() Then
                cboFromPeriod.SelectedValue = objFrm.SelectedValue
            End If
            cboFromPeriod.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFromPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchToPeriod_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchToPeriod.Click
        Try
            Dim objFrm As New frmCommonSearch
            objFrm.DataSource = CType(cboToPeriod.DataSource, DataTable)
            objFrm.ValueMember = cboToPeriod.ValueMember
            objFrm.DisplayMember = cboToPeriod.DisplayMember

            If objFrm.DisplayDialog() Then
                cboToPeriod.SelectedValue = objFrm.SelectedValue
            End If
            cboToPeriod.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchToPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim objFrm As New frmCommonSearch
            objFrm.DataSource = CType(cboEmployee.DataSource, DataTable)
            objFrm.ValueMember = cboEmployee.ValueMember
            objFrm.DisplayMember = cboEmployee.DisplayMember
            objFrm.CodeMember = "employeeCode"

            If objFrm.DisplayDialog() Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Link Button"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Try
            Dim objLnkAnalys As New frmViewAnalysis
            objLnkAnalys.displayDialog()
            mstrViewByIds = objLnkAnalys._ReportBy_Ids
            mstrViewByName = objLnkAnalys._ReportBy_Name
            mintViewIndex = objLnkAnalys._ViewIndex
            mstrAnalysis_Fields = objLnkAnalys._Analysis_Fields
            mstrAnalysis_Join = objLnkAnalys._Analysis_Join
            mstrAnalysis_OrderBy = objLnkAnalys._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = objLnkAnalys._Analysis_OrderBy_GName
            mstrReport_GroupName = objLnkAnalys._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Events"

    Private Sub lvAlloction_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAlloction.ItemChecked
        Try
            If dsAllocation IsNot Nothing AndAlso dsAllocation.Tables(0).Rows.Count > 0 Then

                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

                If lvAlloction.CheckedItems.Count = dsAllocation.Tables(0).Rows.Count Then
                    chkSelectAll.CheckState = CheckState.Checked
                ElseIf lvAlloction.CheckedItems.Count > 0 AndAlso (lvAlloction.CheckedItems.Count < dsAllocation.Tables(0).Rows.Count) Then
                    chkSelectAll.CheckState = CheckState.Indeterminate
                Else
                    chkSelectAll.CheckState = CheckState.Unchecked
                End If

                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAlloction_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Events"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If lvAlloction.Items.Count > 0 Then
                RemoveHandler lvAlloction.ItemChecked, AddressOf lvAlloction_ItemChecked
                For Each lvItem As ListViewItem In lvAlloction.Items
                    lvItem.Checked = chkSelectAll.Checked
                Next
                AddHandler lvAlloction.ItemChecked, AddressOf lvAlloction_ItemChecked
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.LblToPeriod.Text = Language._Object.getCaption(Me.LblToPeriod.Name, Me.LblToPeriod.Text)
            Me.LblFromPeriod.Text = Language._Object.getCaption(Me.LblFromPeriod.Name, Me.LblFromPeriod.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
            Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
            Me.colhAllocation.Text = Language._Object.getCaption(CStr(Me.colhAllocation.Tag), Me.colhAllocation.Text)
            Me.chkShowLeaveColumn.Text = Language._Object.getCaption(Me.chkShowLeaveColumn.Name, Me.chkShowLeaveColumn.Text)
            Me.chkShowApprovedColumn.Text = Language._Object.getCaption(Me.chkShowApprovedColumn.Name, Me.chkShowApprovedColumn.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From Period is compulsory infomation. Please select from period to continue.")
            Language.setMessage(mstrModuleName, 2, "To Period is compulsory infomation. Please select to period to continue.")
            Language.setMessage(mstrModuleName, 3, "To Period cannot be less than From Period.")
            Language.setMessage(mstrModuleName, 4, "Selection Saved Successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
