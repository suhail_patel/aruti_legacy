'************************************************************************************************************************************
'Class Name : clsEmpBudgetTimesheetReport.vb
'Purpose    :
'Date       : 31/07/2017
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System
Imports System.Text
Imports System.IO

Public Class clsEmpBudgetTimesheetSubmissionReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmpBudgetTimesheetSubmissionReport"
    Private mstrReportId As String = enArutiReport.Employee_Timesheet_Submission_Report  '193
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintFromPeriodID As Integer = 0
    Private mstrFromPeriodName As String = ""
    Private mintToPeriodID As Integer = 0
    Private mstrToPeriodName As String = ""
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mstrEmployeeCode As String = ""
    Private mstrAllocationIds As String = ""
    Private mstrColumns As String = ""
    Private mstrJoinColumns As String = ""
    Private mstrJoins As String = ""
    Private mstrJobColumns As String = ""
    Private mstrJobJoinColumns As String = ""
    Private mblnIsCostCenter As Boolean = False
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrOrderByQuery As String = ""

    'Pinkal (18-Dec-2017) -- Start
    'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].
    Private mblnShowApprovedColumn As Boolean = True
    Private mblnShowLeaveColumn As Boolean = True
    'Pinkal (18-Dec-2017) -- End

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
    Private mblnAllowOvertimeInEmpTimesheet As Boolean = False
    'Pinkal (28-Mar-2018) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _FromPeriodId() As Integer
        Set(ByVal value As Integer)
            mintFromPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodName() As String
        Set(ByVal value As String)
            mstrFromPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As Date
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As Date
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _AllocationIds() As String
        Set(ByVal value As String)
            mstrAllocationIds = value
        End Set
    End Property

    Public WriteOnly Property _AllocationColumns() As String
        Set(ByVal value As String)
            mstrColumns = value
        End Set
    End Property

    Public WriteOnly Property _AllocationJoinColumns() As String
        Set(ByVal value As String)
            mstrJoinColumns = value
        End Set
    End Property

    Public WriteOnly Property _AllocationJoins() As String
        Set(ByVal value As String)
            mstrJoins = value
        End Set
    End Property

    Public WriteOnly Property _JobColumns() As String
        Set(ByVal value As String)
            mstrJobColumns = value
        End Set
    End Property

    Public WriteOnly Property _JobJoinColumns() As String
        Set(ByVal value As String)
            mstrJobJoinColumns = value
        End Set
    End Property

    Public WriteOnly Property _IsCostCenterInAllocation() As Boolean
        Set(ByVal value As Boolean)
            mblnIsCostCenter = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property


    'Pinkal (18-Dec-2017) -- Start
    'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].

    Public WriteOnly Property _ShowApprovedColumn() As Boolean
        Set(ByVal value As Boolean)
            mblnShowApprovedColumn = value
        End Set
    End Property

    Public WriteOnly Property _ShowLeaveColumn() As Boolean
        Set(ByVal value As Boolean)
            mblnShowLeaveColumn = value
        End Set
    End Property

    'Pinkal (18-Dec-2017) -- End

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
    Public WriteOnly Property _AllowOvertimeInEmpTimesheet() As Boolean
        Set(ByVal value As Boolean)
            mblnAllowOvertimeInEmpTimesheet = value
        End Set
    End Property

    'Pinkal (28-Mar-2018) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintFromPeriodID = 0
            mstrFromPeriodName = ""
            mintToPeriodID = 0
            mstrToPeriodName = ""
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrEmployeeCode = ""
            mstrAllocationIds = ""
            mstrColumns = ""
            mstrJoins = ""
            mstrJoinColumns = ""
            mstrJobColumns = ""
            mstrJobJoinColumns = ""
            mblnIsCostCenter = False
            mstrOrderByQuery = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrAnalysis_OrderBy = ""
            mstrAdvance_Filter = ""

            'Pinkal (18-Dec-2017) -- Start
            'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].
            mblnShowApprovedColumn = True
            mblnShowLeaveColumn = True
            'Pinkal (18-Dec-2017) -- End

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
            mblnAllowOvertimeInEmpTimesheet = False
            'Pinkal (28-Mar-2018) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            If mintFromPeriodID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "From Period: ") & " " & mstrFromPeriodName & " "
            End If

            If mintToPeriodID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "To Period: ") & " " & mstrToPeriodName & " "
                objDataOperation.AddParameter("@EndDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate)
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mintViewIndex > 0 Then
                mstrOrderByQuery &= " ORDER BY " & mstrAnalysis_OrderBy_GName & ",employeecode"
            Else
                mstrOrderByQuery &= " ORDER BY employeecode "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsOnDate As Date, ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, ByVal blnIsFromEmailAttachment As Boolean)

        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try

            If xUserUnkid <= 0 Then
                xUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = xUserUnkid


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsOnDate, mdtEmployeeAsOnDate, True, True, xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsOnDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsOnDate, xDatabaseName)

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()



            StrQ = " SELECT " & _
                      " hremployee_master.employeeunkid " & _
                      ",ISNULL(hremployee_master.employeecode,'') AS EmployeeCode " & _
                      ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee "


            If mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= mstrColumns
            End If


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",'' AS Id, '' AS GName "
            End If

            StrQ &= " FROM hremployee_master "


            If mstrJobColumns.Trim.Length > 0 AndAlso mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " LEFT JOIN " & _
                          "( " & _
                          "    SELECT " & _
                          "         employeeunkid " & _
                          "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno "

                StrQ &= mstrJobColumns

                StrQ &= "    FROM hremployee_categorization_tran " & _
                              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EndDate " & _
                              " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 "

                StrQ &= mstrJobJoinColumns

            End If

            If mblnIsCostCenter AndAlso mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " LEFT JOIN " & _
                          " ( " & _
                          "    SELECT " & _
                          "         cctranheadvalueid " & _
                          "        ,employeeunkid " & _
                          "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                          "    FROM hremployee_cctranhead_tran " & _
                          "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                         "    AND CONVERT(CHAR(8),effectivedate,112) <= @EndDate" & _
                         " ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                         " JOIN prcostcenter_master AS CCM ON CCM.costcenterunkid = CC.cctranheadvalueid "
            End If

            If mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " JOIN " & _
                                        " ( " & _
                                        "    SELECT " & _
                                        "       employeeunkid " & _
                                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno "

                StrQ &= mstrJoinColumns

                StrQ &= "  FROM hremployee_transfer_tran " & _
                            "  WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EndDate" & _
                            " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "

                StrQ &= mstrJoins

            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If


            StrQ &= " WHERE 1 = 1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'Pinkal (18-Feb-2020) -- Start
            'Bug THPS [0004550] -   THPS Inactive Employees appear on submission timesheet report.
            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If
            'Pinkal (18-Feb-2020) -- End


            FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            Dim dsEmployee As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If



            If dsEmployee IsNot Nothing AndAlso dsEmployee.Tables(0).Rows.Count > 0 Then
                If dsEmployee.Tables(0).Columns.Contains("SrNo") = False Then
                    Dim dcColumn As New DataColumn("SrNo", System.Type.GetType("System.String"))
                    dcColumn.Caption = Language.getMessage(mstrModuleName, 7, "Sr.No")
                    dcColumn.DefaultValue = ""
                    dsEmployee.Tables(0).Columns.Add(dcColumn)
                    dsEmployee.Tables(0).Columns("SrNo").SetOrdinal(0)
                End If

                If dsEmployee.Tables(0).Columns.Contains("Id") Then
                    dsEmployee.Tables(0).Columns.Remove("Id")
                End If

            End If

            Dim xColumnIndex As Integer = dsEmployee.Tables(0).Columns.Count


            Dim dctPeriod As New Dictionary(Of Integer, String)


            StrQ = " SELECT " & _
                       "  periodunkid " & _
                       "  ,period_code " & _
                       "  ,period_name " & _
                       "  ,Convert(Char(8),start_date,112) AS start_date " & _
                       "  ,Convert(Char(8),end_date,112) AS end_date " & _
                       " FROM cfcommon_period_tran  " & _
                       " WHERE CONVERT(Char(8),start_date,112) >= @StartDate AND CONVERT(CHAR(8),end_date,112) <= @EndDate " & _
                       " AND yearunkid = @yearunkid AND modulerefid  = @modulerefid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearUnkid)
            objDataOperation.AddParameter("@StartDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate)
            objDataOperation.AddParameter("@EndDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate)
            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, enModuleReference.Payroll)
            Dim dsPeriod As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsPeriod IsNot Nothing AndAlso dsPeriod.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsPeriod.Tables(0).Rows

                    If dsEmployee.Tables(0).Columns.Contains(dr("period_name").ToString()) = False Then
                        dsEmployee.Tables(0).Columns.Add(dr("period_name").ToString(), System.Type.GetType("System.String"))
                        dsEmployee.Tables(0).Columns(dr("period_name").ToString()).DefaultValue = ""
                        dsEmployee.Tables(0).Columns(dr("period_name").ToString()).ExtendedProperties.Add("PeriodId", CInt(dr("periodunkid")))

                        If dctPeriod.ContainsKey(CInt(dr("periodunkid"))) = False Then
                            dctPeriod.Add(CInt(dr("periodunkid")), dr("start_date").ToString() & "|" & dr("end_date").ToString())
                        End If

                    End If


                    'Pinkal (18-Dec-2017) -- Start
                    'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].

                    If mblnShowApprovedColumn Then
                        If dsEmployee.Tables(0).Columns.Contains("Approved_" & CInt(dr("periodunkid")).ToString()) = False Then
                            dsEmployee.Tables(0).Columns.Add("Approved_" & CInt(dr("periodunkid")).ToString(), System.Type.GetType("System.String"))
                            dsEmployee.Tables(0).Columns("Approved_" & CInt(dr("periodunkid")).ToString()).Caption = Language.getMessage(mstrModuleName, 9, "Approved")
                            dsEmployee.Tables(0).Columns("Approved_" & CInt(dr("periodunkid")).ToString()).DefaultValue = ""
                            dsEmployee.Tables(0).Columns("Approved_" & CInt(dr("periodunkid")).ToString()).ExtendedProperties.Add("ApprovedId", CInt(dr("periodunkid")))
                        End If
                    End If

                    If mblnShowLeaveColumn Then
                        If dsEmployee.Tables(0).Columns.Contains("Leave_" & CInt(dr("periodunkid")).ToString()) = False Then
                            dsEmployee.Tables(0).Columns.Add("Leave_" & CInt(dr("periodunkid")).ToString(), System.Type.GetType("System.String"))
                            dsEmployee.Tables(0).Columns("Leave_" & CInt(dr("periodunkid")).ToString()).Caption = Language.getMessage(mstrModuleName, 10, "Leave")
                            dsEmployee.Tables(0).Columns("Leave_" & CInt(dr("periodunkid")).ToString().ToString()).DefaultValue = ""
                            dsEmployee.Tables(0).Columns("Leave_" & CInt(dr("periodunkid")).ToString().ToString()).ExtendedProperties.Add("LeaveId", CInt(dr("periodunkid")))
                        End If
                    End If
                    'Pinkal (18-Dec-2017) -- End


                Next

            End If
            dsPeriod.Clear()
            dsPeriod = Nothing

            If dsEmployee.Tables(0).Columns.Contains("Submitted") = False Then
                Dim dcColumn As New DataColumn("Submitted", System.Type.GetType("System.Int32"))
                dcColumn.DefaultValue = 0
                dcColumn.Caption = Language.getMessage(mstrModuleName, 11, "Total Submitted")
                dsEmployee.Tables(0).Columns.Add(dcColumn)
                dcColumn = Nothing

                Dim dRow As DataRow = dsEmployee.Tables(0).NewRow
                dRow("SrNo") = ""
                dRow("employeeunkid") = -999
                dRow("Employeecode") = ""
                dRow("Employee") = Language.getMessage(mstrModuleName, 4, "Submitted")
                dsEmployee.Tables(0).Rows.Add(dRow)
                dRow = Nothing
            End If


            'Pinkal (18-Dec-2017) -- Start
            'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].

            If dsEmployee.Tables(0).Columns.Contains("NotSubmitted") = False Then
                Dim dcColumn As New DataColumn("NotSubmitted", System.Type.GetType("System.Int32"))
                dcColumn.DefaultValue = 0
                dcColumn.Caption = Language.getMessage(mstrModuleName, 12, "Total Not Submitted")
                dsEmployee.Tables(0).Columns.Add(dcColumn)

                Dim dRow As DataRow = dsEmployee.Tables(0).NewRow
                dRow("SrNo") = ""
                dRow("employeeunkid") = -1000
                dRow("Employeecode") = ""
                dRow("Employee") = Language.getMessage(mstrModuleName, 5, "Not Submitted")
                dsEmployee.Tables(0).Rows.Add(dRow)
                dRow = Nothing

                'dRow = dsEmployee.Tables(0).NewRow
                'dRow("SrNo") = ""
                'dRow("employeeunkid") = -1001
                'dRow("Employeecode") = ""
                'dRow("Employee") = Language.getMessage(mstrModuleName, 6, "Total")
                'dsEmployee.Tables(0).Rows.Add(dRow)
                'dRow = Nothing

            End If

            If mblnShowApprovedColumn AndAlso dsEmployee.Tables(0).Columns.Contains("Approved_Col") = False Then
                Dim dcColumn As New DataColumn("Approved_Col", System.Type.GetType("System.Int32"))
                dcColumn.DefaultValue = 0
                dcColumn.Caption = Language.getMessage(mstrModuleName, 13, "Total Approved")
                dsEmployee.Tables(0).Columns.Add(dcColumn)

                Dim dRow As DataRow = dsEmployee.Tables(0).NewRow
                dRow("SrNo") = ""
                dRow("employeeunkid") = -1001
                dRow("Employeecode") = ""
                dRow("Employee") = Language.getMessage(mstrModuleName, 9, "Approved")
                dsEmployee.Tables(0).Rows.Add(dRow)
                dRow = Nothing
            End If


            If mblnShowLeaveColumn AndAlso dsEmployee.Tables(0).Columns.Contains("Leave_Col") = False Then
                Dim dcColumn As New DataColumn("Leave_Col", System.Type.GetType("System.Int32"))
                dcColumn.DefaultValue = 0
                dcColumn.Caption = Language.getMessage(mstrModuleName, 14, "Total Leave")
                dsEmployee.Tables(0).Columns.Add(dcColumn)

                Dim dRow As DataRow = dsEmployee.Tables(0).NewRow
                dRow("SrNo") = ""
                dRow("employeeunkid") = -1002
                dRow("Employeecode") = ""
                dRow("Employee") = Language.getMessage(mstrModuleName, 10, "Leave")
                dsEmployee.Tables(0).Rows.Add(dRow)
                dRow = Nothing

            End If

            Dim dRowTotal As DataRow = dsEmployee.Tables(0).NewRow
            dRowTotal("SrNo") = ""
            dRowTotal("employeeunkid") = -1003
            dRowTotal("Employeecode") = ""
            dRowTotal("Employee") = Language.getMessage(mstrModuleName, 6, "Total")
            dsEmployee.Tables(0).Rows.Add(dRowTotal)
            dRowTotal = Nothing

            'Pinkal (18-Dec-2017) -- End



            Dim dctSubmittedColumnTotal As New Dictionary(Of String, Int32)
            Dim dctNoSubmittedColumnTotal As New Dictionary(Of String, Int32)


            'Pinkal (18-Dec-2017) -- Start
            'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].
            Dim dctApprovedColumnTotal As New Dictionary(Of String, Int32)
            Dim dctLeaveColumnTotal As New Dictionary(Of String, Int32)
            Dim mintSrNo As Integer = 0
            Dim xEmployeeID As Integer = 0
            Dim mstrFilter As String = ""
            mstrFilter = "ltbemployee_timesheet.issubmit_approval = 1 "
            'Pinkal (18-Dec-2017) -- End



            Dim objMasterData As New clsMasterData
            Dim objEmpBudgetTS As New clsBudgetEmp_timesheet


            'Pinkal (19-Feb-2020) -- Start
            'ENHANCEMENT THPS [0004551]:  Employee who appointed at middle of the month does not appear to  timesheet submission report.
            Dim objEmp As New clsEmployee_Master
            'Pinkal (19-Feb-2020) -- End

            For i As Integer = 0 To dsEmployee.Tables(0).Rows.Count - 1

                'Pinkal (18-Dec-2017) -- Start
                'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].
                'If CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")) > 0 Then dsEmployee.Tables(0).Rows(i)("SrNo") = i + 1
                'Pinkal (18-Dec-2017) -- End

                'Pinkal (19-Feb-2020) -- Start
                'ENHANCEMENT THPS [0004551]:  Employee who appointed at middle of the month does not appear to  timesheet submission report.
                objEmp._Employeeunkid(mdtEmployeeAsOnDate, Nothing) = CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid"))
                'Pinkal (19-Feb-2020) -- End

                For j As Integer = xColumnIndex To dsEmployee.Tables(0).Columns.Count - 1

                    If dsEmployee.Tables(0).Columns(j).ExtendedProperties.Count > 0 AndAlso CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")) > 0 Then

                        Dim mintPeriodId As Integer = CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("PeriodId"))

                        If mintPeriodId > 0 Then

                            If dctPeriod IsNot Nothing AndAlso dctPeriod.Count > 0 AndAlso dctPeriod.ContainsKey(mintPeriodId) Then

                                Dim ar() As String = dctPeriod(mintPeriodId).Trim().Split("|")

                                If ar.Length > 0 Then

                                    Dim mdtDate1 As Date = eZeeDate.convertDate(ar(0).ToString()).Date
                                    Dim mdtDate2 As Date = eZeeDate.convertDate(ar(1).ToString()).Date


                                    'Pinkal (19-Feb-2020) -- Start
                                    'ENHANCEMENT THPS [0004551]:  Employee who appointed at middle of the month does not appear to  timesheet submission report.
                                    'Dim mintPeriodDays As Integer =   DateDiff(DateInterval.Day, mdtDate1, mdtDate2) + 1
                                    Dim mintPeriodDays As Integer = 0
                                    If (objEmp._Appointeddate.Date >= mdtDate1 AndAlso objEmp._Appointeddate.Date <= mdtDate2) Then
                                        mintPeriodDays = DateDiff(DateInterval.Day, objEmp._Appointeddate.Date, mdtDate2) + 1
                                        mdtDate1 = objEmp._Appointeddate.Date
                                    ElseIf objEmp._Reinstatementdate <> Nothing AndAlso (objEmp._Reinstatementdate.Date >= mdtDate1 AndAlso objEmp._Reinstatementdate.Date <= mdtDate2) Then
                                        mintPeriodDays = DateDiff(DateInterval.Day, objEmp._Reinstatementdate.Date, mdtDate2) + 1
                                        mdtDate1 = objEmp._Reinstatementdate.Date
                                    Else
                                        mintPeriodDays = DateDiff(DateInterval.Day, mdtDate1, mdtDate2) + 1
                                    End If
                                    'Pinkal (19-Feb-2020) -- End


                                    Dim dsHLDWDays As DataSet = objMasterData.getEmpHolidayLeaveDayoffWeekendsList(CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")), mdtDate1, mdtDate2, True, True, True, True)


                                    'Pinkal (18-Dec-2017) -- Start
                                    'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].
                                    'Dim dsTimesheet As DataSet = objEmpBudgetTS.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtDate1, mdtDate2, xUserModeSetting _
                                    '                                    , True, False, CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")), False, True, mintPeriodId, Nothing, "issubmit_approval = 1", False, True)

                                    'Pinkal (28-Mar-2018) -- Start
                                    'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.

                                    'Dim dsTimesheet As DataSet = objEmpBudgetTS.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtDate1, mdtDate2, xUserModeSetting _
                                    '                                    , True, False, CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")), False, True, mintPeriodId, Nothing, mstrFilter, False, True)

                                    Dim dsTimesheet As DataSet = objEmpBudgetTS.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtDate1, mdtDate2, xUserModeSetting _
                                                                        , True, False, mblnAllowOvertimeInEmpTimesheet, CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")), False, True, mintPeriodId, Nothing, mstrFilter, False, True)

                                    'Pinkal (28-Mar-2018) -- End




                                    If CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")) > 0 Then
                                        dsEmployee.Tables(0).Rows(i)("SrNo") = i + 1
                                    End If


                                    'Pinkal (18-Dec-2017) -- End

                                    'Pinkal (11-Dec-2018) -- Start
                                    'Bug - Solved bug for Budget timesheet submission report.
                                    'Dim intTimesheetDays As Integer = dsTimesheet.Tables(0).Rows.Count
                                    Dim dtTable As DataTable = New DataView(dsTimesheet.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "employeeunkid", "activitydate", "ADate", "statusunkid")
                                    Dim intTimesheetDays As Integer = dtTable.Rows.Count
                                    'Pinkal (11-Dec-2018) -- End


                                    For Each dR As DataRow In dsHLDWDays.Tables(0).Rows
                                        Dim dEmpRow As DataRow() = dsTimesheet.Tables(0).Select("ADate='" & dR.Item("ddate").ToString & "'")
                                        If dEmpRow.Length > 0 Then
                                            intTimesheetDays = intTimesheetDays - 1
                                        End If
                                    Next

                                    mintPeriodDays = mintPeriodDays - dsHLDWDays.Tables(0).Rows.Count


                                    'Pinkal (18-Dec-2017) -- Start
                                    'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].

                                    If mintPeriodDays <= 0 AndAlso mblnShowLeaveColumn Then
                                        dsEmployee.Tables(0).Rows(i)("Leave_" & mintPeriodId.ToString()) = "*"
                                        dsEmployee.Tables(0).Rows(i)("Leave_Col") = CInt(dsEmployee.Tables(0).Rows(i)("Leave_Col")) + 1

                                        If dctLeaveColumnTotal.ContainsKey("Leave_" & mintPeriodId.ToString()) Then
                                            dctLeaveColumnTotal("Leave_" & mintPeriodId.ToString()) = CInt(dctLeaveColumnTotal("Leave_" & mintPeriodId.ToString())) + 1
                                        Else
                                            dctLeaveColumnTotal.Add("Leave_" & mintPeriodId.ToString(), 1)
                                        End If

                                        'Pinkal (18-Dec-2017) -- End

                                    ElseIf mintPeriodDays <> intTimesheetDays Then

                                        dsEmployee.Tables(0).Rows(i)(j) = "x"
                                        dsEmployee.Tables(0).Rows(i)("NotSubmitted") = CInt(dsEmployee.Tables(0).Rows(i)("NotSubmitted")) + 1

                                        If dctNoSubmittedColumnTotal.ContainsKey(dsEmployee.Tables(0).Columns(j).ColumnName) Then
                                            dctNoSubmittedColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName) = CInt(dctNoSubmittedColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName)) + 1
                                        Else
                                            dctNoSubmittedColumnTotal.Add(dsEmployee.Tables(0).Columns(j).ColumnName, 1)
                                        End If

                                        'Pinkal (18-Dec-2017) -- Start
                                        'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].
                                        If mblnShowApprovedColumn Then dsEmployee.Tables(0).Rows(i)("Approved_" & mintPeriodId.ToString()) = "x"
                                        'Pinkal (18-Dec-2017) -- End


                                    ElseIf mintPeriodDays = intTimesheetDays Then

                                        dsEmployee.Tables(0).Rows(i)(j) = "√"
                                        dsEmployee.Tables(0).Rows(i)("Submitted") = CInt(dsEmployee.Tables(0).Rows(i)("Submitted")) + 1

                                        If dctSubmittedColumnTotal.ContainsKey(dsEmployee.Tables(0).Columns(j).ColumnName) Then
                                            dctSubmittedColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName) = CInt(dctSubmittedColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName)) + 1
                                        Else
                                            dctSubmittedColumnTotal.Add(dsEmployee.Tables(0).Columns(j).ColumnName, 1)
                                        End If


                                        'Pinkal (18-Dec-2017) -- Start
                                        'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].
                                        If mblnShowApprovedColumn Then

                                            'Pinkal (11-Dec-2018) -- Start
                                            'Bug - Solved bug for Budget timesheet submission report.
                                            'Dim mintTotalApproved As Integer = dsTimesheet.Tables(0).Compute("COUNT(Statusunkid)", "Statusunkid = 1 AND employeeunkid = " & CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")))
                                            Dim mintTotalApproved As Integer = dtTable.Compute("COUNT(Statusunkid)", "Statusunkid = 1 AND employeeunkid = " & CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")))
                                            'Pinkal (11-Dec-2018) -- End

                                            If mintTotalApproved = mintPeriodDays Then
                                                dsEmployee.Tables(0).Rows(i)("Approved_" & mintPeriodId.ToString()) = "√"
                                                dsEmployee.Tables(0).Rows(i)("Approved_Col") = CInt(dsEmployee.Tables(0).Rows(i)("Approved_Col")) + 1

                                                If dctApprovedColumnTotal.ContainsKey("Approved_" & mintPeriodId.ToString()) Then
                                                    dctApprovedColumnTotal("Approved_" & mintPeriodId.ToString()) = CInt(dctApprovedColumnTotal("Approved_" & mintPeriodId.ToString())) + 1
                                                Else
                                                    dctApprovedColumnTotal.Add("Approved_" & mintPeriodId.ToString(), 1)
                                                End If
                                            Else
                                                dsEmployee.Tables(0).Rows(i)("Approved_" & mintPeriodId.ToString()) = "x"
                                            End If
                                        End If
                                        'Pinkal (18-Dec-2017) -- End


                                    End If

                                End If

                            End If

                        End If

                    End If

                    If dsEmployee.Tables(0).Rows(i)("employeeunkid") = -999 Then  'FOR SUBMITTED
                        If dctSubmittedColumnTotal.Count > 0 Then
                            If dctSubmittedColumnTotal.ContainsKey(dsEmployee.Tables(0).Columns(j).ColumnName) Then
                                dsEmployee.Tables(0).Rows(i)(j) = dctSubmittedColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName)
                            Else
                                dsEmployee.Tables(0).Rows(i)(j) = 0
                            End If
                        End If

                        If dsEmployee.Tables(0).Columns(j).ColumnName = "Submitted" Then
                            dsEmployee.Tables(0).Rows(i)("Submitted") = dsEmployee.Tables(0).Compute("SUM(Submitted)", "employeeunkid > 0")
                        End If

                    ElseIf dsEmployee.Tables(0).Rows(i)("employeeunkid") = -1000 Then   'FOR NOT SUBMITTED
                        If dctNoSubmittedColumnTotal.Count > 0 Then
                            If dctNoSubmittedColumnTotal.ContainsKey(dsEmployee.Tables(0).Columns(j).ColumnName) Then
                                dsEmployee.Tables(0).Rows(i)(j) = dctNoSubmittedColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName)
                            Else
                                dsEmployee.Tables(0).Rows(i)(j) = 0
                            End If
                        End If

                        If dsEmployee.Tables(0).Columns(j).ColumnName = "NotSubmitted" Then
                            dsEmployee.Tables(0).Rows(i)("NotSubmitted") = dsEmployee.Tables(0).Compute("SUM(NotSubmitted)", "employeeunkid > 0")
                        End If


                        'Pinkal (18-Dec-2017) -- Start
                        'Enhancement - Employee Budget timesheet report - (RefNo: 150)[Put Filter Status & Leave Code When Employee on Leave Whole month].

                    ElseIf mblnShowApprovedColumn AndAlso dsEmployee.Tables(0).Rows(i)("employeeunkid") = -1001 Then  'FOR TOTAL APPROVED
                        If dctApprovedColumnTotal.Count > 0 Then
                            If dctApprovedColumnTotal.ContainsKey(dsEmployee.Tables(0).Columns(j).ColumnName) Then
                                dsEmployee.Tables(0).Rows(i)(j) = dctApprovedColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName)
                            Else
                                dsEmployee.Tables(0).Rows(i)(j) = 0
                            End If
                        End If

                        If dsEmployee.Tables(0).Columns(j).ColumnName = "Approved_Col" Then
                            dsEmployee.Tables(0).Rows(i)("Approved_Col") = dsEmployee.Tables(0).Compute("SUM(Approved_Col)", "employeeunkid > 0")
                        End If


                    ElseIf mblnShowLeaveColumn AndAlso dsEmployee.Tables(0).Rows(i)("employeeunkid") = -1002 Then     'FOR TOTAL LEAVE
                        If dctLeaveColumnTotal.Count > 0 Then
                            If dctLeaveColumnTotal.ContainsKey(dsEmployee.Tables(0).Columns(j).ColumnName) Then
                                dsEmployee.Tables(0).Rows(i)(j) = dctLeaveColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName)
                            Else
                                dsEmployee.Tables(0).Rows(i)(j) = 0
                            End If
                        End If

                        If dsEmployee.Tables(0).Columns(j).ColumnName = "Leave_Col" Then
                            dsEmployee.Tables(0).Rows(i)("Leave_Col") = dsEmployee.Tables(0).Compute("SUM(Leave_Col)", "employeeunkid > 0")
                        End If

                    ElseIf dsEmployee.Tables(0).Rows(i)("employeeunkid") = -1003 Then  'FOR TOTAL SUBMITTED

                        dsEmployee.Tables(0).Rows(i)(j) = "0"

                        If dctSubmittedColumnTotal.Count > 0 Then  'FOR TOTAL SUBMITTED

                            If dctSubmittedColumnTotal.ContainsKey(dsEmployee.Tables(0).Columns(j).ColumnName) Then
                                dsEmployee.Tables(0).Rows(i)(j) = dctSubmittedColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName)
                            End If

                            If dsEmployee.Tables(0).Columns(j).ColumnName = "Submitted" Then
                                dsEmployee.Tables(0).Rows(i)("Submitted") = dsEmployee.Tables(0).Compute("SUM(Submitted)", "employeeunkid > 0")
                            End If

                        End If

                        If dctNoSubmittedColumnTotal.Count > 0 Then  'FOR TOTAL NOT SUBMITTED

                            If dctNoSubmittedColumnTotal.ContainsKey(dsEmployee.Tables(0).Columns(j).ColumnName) Then
                                dsEmployee.Tables(0).Rows(i)(j) = CInt(dsEmployee.Tables(0).Rows(i)(j)) + dctNoSubmittedColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName)
                            End If

                            If dsEmployee.Tables(0).Columns(j).ColumnName = "NotSubmitted" Then
                                dsEmployee.Tables(0).Rows(i)("NotSubmitted") = dsEmployee.Tables(0).Compute("SUM(NotSubmitted)", "employeeunkid > 0")
                            End If

                        End If


                        If mblnShowApprovedColumn AndAlso dctApprovedColumnTotal.Count > 0 Then 'FOR TOTAL APPROVED

                            If dctApprovedColumnTotal.ContainsKey(dsEmployee.Tables(0).Columns(j).ColumnName) Then
                                dsEmployee.Tables(0).Rows(i)(j) = CInt(dsEmployee.Tables(0).Rows(i)(j)) + dctApprovedColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName)
                            End If

                            If dsEmployee.Tables(0).Columns(j).ColumnName = "Approved_Col" Then
                                dsEmployee.Tables(0).Rows(i)("Approved_Col") = dsEmployee.Tables(0).Compute("SUM(Approved_Col)", "employeeunkid > 0")
                            End If

                        End If


                        If mblnShowLeaveColumn AndAlso dctLeaveColumnTotal.Count > 0 Then 'FOR TOTAL LEAVE

                            If dctLeaveColumnTotal.ContainsKey(dsEmployee.Tables(0).Columns(j).ColumnName) Then
                                dsEmployee.Tables(0).Rows(i)(j) = CInt(dsEmployee.Tables(0).Rows(i)(j)) + dctLeaveColumnTotal(dsEmployee.Tables(0).Columns(j).ColumnName)
                            End If

                            If dsEmployee.Tables(0).Columns(j).ColumnName = "Leave_Col" Then
                                dsEmployee.Tables(0).Rows(i)("Leave_Col") = dsEmployee.Tables(0).Compute("SUM(Leave_Col)", "employeeunkid > 0")
                            End If

                        End If


                    End If

                    'Pinkal (18-Dec-2017) -- End

                Next

            Next


            'Pinkal (19-Feb-2020) -- Start
            'ENHANCEMENT THPS [0004551]:  Employee who appointed at middle of the month does not appear to  timesheet submission report.
            objEmp = Nothing
            'Pinkal (19-Feb-2020) -- End

            dctPeriod.Clear()
            dctPeriod = Nothing
            objEmpBudgetTS = Nothing
            objMasterData = Nothing


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell
            Dim strBuilder As New StringBuilder
            Dim mdtTableExcel As DataTable = dsEmployee.Tables(0)

            mdtTableExcel.Columns("GName").SetOrdinal(mdtTableExcel.Columns("Employee").Ordinal + 1)

            mdtTableExcel.Columns("EmployeeCode").Caption = Language.getMessage(mstrModuleName, 8, "Employee Code")

            Dim mdtTable As DataTable = New DataView(mdtTableExcel, "employeeunkid > 0", "", DataViewRowState.CurrentRows).ToTable()


            If mdtTable.Columns.Contains("employeeunkid") Then
                mdtTable.Columns.Remove("employeeunkid")
            End If


            If mintViewIndex > 0 Then
                mdtTable.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTable.Columns.Contains("GName") Then
                    mdtTable.Columns.Remove("GName")
                End If
            End If

            row = New WorksheetRow()
            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            Dim mdtFooter As DataTable = New DataView(mdtTableExcel, "employeeunkid <=0", "", DataViewRowState.CurrentRows).ToTable()

            If mdtFooter.Columns.Contains("employeeunkid") Then
                mdtFooter.Columns.Remove("employeeunkid")
            End If

            If mintViewIndex > 0 Then
                mdtFooter.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtFooter.Columns.Contains("GName") Then
                    mdtFooter.Columns.Remove("GName")
                End If
            End If

            If mdtFooter IsNot Nothing AndAlso mdtFooter.Rows.Count > 0 Then

                For i As Integer = 0 To mdtFooter.Rows.Count - 1
                    row = New WorksheetRow()
                    For j As Integer = 0 To mdtFooter.Columns.Count - 1
                        wcell = New WorksheetCell(mdtFooter.Rows(i)(j).ToString(), "FooterStyle")
                        row.Cells.Add(wcell)
                    Next
                    rowsArrayFooter.Add(row)
                Next

            End If



            'SET EXCEL CELL WIDTH  

            If mintViewIndex > 0 Then
                xColumnIndex -= 1
            Else
                xColumnIndex -= 2
            End If

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTable.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = 0 Then
                    intArrayColumnWidth(i) = 80
                ElseIf i > 0 AndAlso xColumnIndex > i Then
                    intArrayColumnWidth(i) = 125
                ElseIf xColumnIndex <= i Then
                    intArrayColumnWidth(i) = 60
                End If
            Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTable, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From Period:")
            Language.setMessage(mstrModuleName, 2, "To Period:")
            Language.setMessage(mstrModuleName, 3, "Employee:")
            Language.setMessage(mstrModuleName, 4, "Submitted")
            Language.setMessage(mstrModuleName, 5, "Not Submitted")
            Language.setMessage(mstrModuleName, 6, "Total")
            Language.setMessage(mstrModuleName, 7, "Sr.No")
            Language.setMessage(mstrModuleName, 8, "Employee Code")
            Language.setMessage(mstrModuleName, 9, "Approved")
            Language.setMessage(mstrModuleName, 10, "Leave")
            Language.setMessage(mstrModuleName, 11, "Total Submitted")
            Language.setMessage(mstrModuleName, 12, "Total Not Submitted")
            Language.setMessage(mstrModuleName, 13, "Total Approved")
            Language.setMessage(mstrModuleName, 14, "Total Leave")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
