'************************************************************************************************************************************
'Class Name : clsEmpBudgetTimesheetReport.vb
'Purpose    :
'Date       : 16/02/2017
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System
Imports System.Text
Imports System.IO

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsEmpBudgetTimesheetReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmpBudgetTimesheetReport"
    Private mstrReportId As String = enArutiReport.Emplyoee_Project_Code_Timesheet_Report  '187
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "


    'Pinkal (22-Jul-2019) -- Start
    'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
    'Private mintEmployeeId As Integer = 0
    'Private mstrEmployeeName As String = ""
    'Private mstrEmployeeCode As String = ""
    Private mdtEmployee As DataTable = Nothing
    'Pinkal (22-Jul-2019) -- End

    
    Private mintPeriodID As Integer = 0
    Private mstrPeriodName As String = ""
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True

    'Pinkal (21-Mar-2017) -- Start
    'Enhancement - Working On Budget Timesheet Report.
    Private mblnViewHTMLReport As Boolean = False
   'Pinkal (21-Mar-2017) -- End


    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Private mblnShowSubmissionRemark As Boolean = False
    Private mstrSubmissionRemark As String = ""
    'Pinkal (28-Jul-2018) -- End

    Private mstrEmployeeIds As String = ""

#End Region

#Region " Properties "


    'Pinkal (22-Jul-2019) -- Start
    'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.

    'Public WriteOnly Property _EmployeeID() As Integer
    '    Set(ByVal value As Integer)
    '        mintEmployeeId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _EmployeeName() As String
    '    Set(ByVal value As String)
    '        mstrEmployeeName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _EmployeeCode() As String
    '    Set(ByVal value As String)
    '        mstrEmployeeCode = value
    '    End Set
    'End Property

    Public WriteOnly Property _dtEmployee() As DataTable
        Set(ByVal value As DataTable)
            mdtEmployee = value
        End Set
    End Property

    'Pinkal (22-Jul-2019) -- End

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As Date
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As Date
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    Public Property _ViewReportInHTML() As Boolean
        Get
            Return mblnViewHTMLReport
        End Get
        Set(ByVal value As Boolean)
            mblnViewHTMLReport = value
        End Set
    End Property

    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

    Public Property _ShowSubmissionRemark() As Boolean
        Get
            Return mblnShowSubmissionRemark
        End Get
        Set(ByVal value As Boolean)
            mblnShowSubmissionRemark = value
        End Set
    End Property

    'Pinkal (28-Jul-2018) -- End



#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            'Pinkal (22-Jul-2019) -- Start
            'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
            'mintEmployeeId = 0
            'mstrEmployeeName = ""
            'mstrEmployeeCode = ""
            mdtEmployee = Nothing
            'Pinkal (22-Jul-2019) -- End
            mintPeriodID = 0
            mstrPeriodName = ""
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mstrReport_GroupName = ""
            mblnViewHTMLReport = False
            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            mblnShowSubmissionRemark = False
            mstrSubmissionRemark = ""
            'Pinkal (28-Jul-2018) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            'Pinkal (22-Jul-2019) -- Start
            'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
            'If mintEmployeeId > 0 Then
            '    Me._FilterQuery &= " AND lvleaveform.employeeunkid = @employeeunkid "
            '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee: ") & " " & mstrEmployeeName & " "
            'End If
            'Pinkal (22-Jul-2019) -- End

            If mintPeriodID > 0 Then
                Me._FilterQuery &= " AND lvleaveform.leavetypeunkid = @leavetypeunkid "
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodID)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Leave Type: ") & " " & mstrPeriodName & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, ByVal blnIsFromEmailAttachment As Boolean)

        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            Dim dctTotalHrByCol As New Dictionary(Of String, Integer)


            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()



            'Pinkal (13-Apr-2017) -- Start
            'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.

            'StrQ = " SELECT DISTINCT " & _
            '           " bgfundprojectcode_master.fundprojectcodeunkid " & _
            '                  ",fundprojectcode " & _
            '                  ",fundprojectname " & _
            '           " FROM bgbudgetcodesfundsource_tran " & _
            '           " LEFT JOIN bgbudget_tran ON bgbudgetcodesfundsource_tran.budgettranunkid =  bgbudget_tran.budgettranunkid   " & _
            '           " LEFT JOIN bgbudget_master ON bgbudget_master.budgetunkid=bgbudget_tran.budgetunkid " & _
            '           " LEFT JOIN bgfundprojectcode_master ON bgfundprojectcode_master.fundprojectcodeunkid = bgbudgetcodesfundsource_tran.fundprojectcodeunkid  " & _
            '           " WHERE bgbudgetcodesfundsource_tran.isvoid = 0 And bgbudget_tran.isvoid = 0 And bgbudget_master.isvoid = 0 And bgbudgetcodesfundsource_tran.percentage > 0 " & _
            '           " AND bgbudget_master.allocationbyid = 0 AND  bgbudgetcodesfundsource_tran.periodunkid = @periodunkid and bgbudget_tran.allocationtranunkid = @employeeunkid " & _
            '           " AND bgbudget_master.isdefault =1"


            'Pinkal (02-Jul-2018) -- Start
            'Enhancement - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.

            'StrQ = " SELECT DISTINCT " & _
            '           " bgfundprojectcode_master.fundprojectcodeunkid " & _
            '          ",bgfundsource_master.fundsourceunkid " & _
            '          ",fundname + ' (' + fundcode + ')' AS Grantcode " & _
            '                     ",fundprojectcode " & _
            '                     ",fundprojectname " & _
            '           " FROM bgbudgetcodesfundsource_tran " & _
            '           " LEFT JOIN bgbudget_tran ON bgbudgetcodesfundsource_tran.budgettranunkid =  bgbudget_tran.budgettranunkid   " & _
            '           " LEFT JOIN bgbudget_master ON bgbudget_master.budgetunkid=bgbudget_tran.budgetunkid " & _
            '           " LEFT JOIN bgfundprojectcode_master ON bgfundprojectcode_master.fundprojectcodeunkid = bgbudgetcodesfundsource_tran.fundprojectcodeunkid  " & _
            '          " LEFT JOIN bgfundsource_master ON bgfundsource_master.fundsourceunkid = bgfundprojectcode_master.fundsourceunkid  " & _
            '           " WHERE bgbudgetcodesfundsource_tran.isvoid = 0 And bgbudget_tran.isvoid = 0 And bgbudget_master.isvoid = 0 And bgbudgetcodesfundsource_tran.percentage > 0 " & _
            '          " AND bgbudget_master.allocationbyid = 0 AND bgfundsource_master.isvoid = 0 " & _
            '          " AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid  AND bgbudget_tran.allocationtranunkid = @employeeunkid " & _
            '           " AND bgbudget_master.isdefault =1"



            StrQ = "DECLARE @daystable Table (dday int,ddate datetime,dname nvarchar(50)) " & _
                        "DECLARE @dw AS INT " & _
                        "SET @dw = DATEPART(dw,@start) " & _
                       "; WITH mdate AS " & _
                        "( " & _
                           "SELECT CAST(@start AS DATETIME) AS Dvalue " & _
                           "UNION ALL " & _
                           "SELECT  Dvalue + 1 FROM mdate WHERE   Dvalue + 1 <= @end " & _
                        ") " & _
                                "INSERT INTO @daystable " & _
                        "SELECT " & _
                           "DATEPART(DAY,Dvalue) AS dday " & _
                           ",dvalue AS ddate " & _
                           ",DATENAME(dw,Dvalue) AS dname " & _
                        "FROM mdate " & _
                        " SELECT  dday, ddate,CONVERT(char(8),ddate,112) AS Dvalue FROM @daystable "

            'Pinkal (19-Jun-2017) -- 'Enhancement - Implemented Leave/TnA Seperated Period .[CONVERT(char(8),ddate,112) AS Dvalue]


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@start", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate.Date)
            objDataOperation.AddParameter("@end", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate.Date)
            Dim dsDayList As DataSet = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            If mdtEmployee IsNot Nothing AndAlso mdtEmployee.Rows.Count > 0 Then

                If System.IO.Directory.Exists(mstrExportReportPath) = False Then
                    Dim dig As New System.Windows.Forms.FolderBrowserDialog
                    dig.Description = "Select Folder Where to export report."

                    If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                        mstrExportReportPath = dig.SelectedPath
                    Else
                        Exit Sub
                    End If
                End If

                For count As Integer = 0 To mdtEmployee.Rows.Count - 1

                    Dim mintEmployeeId As Integer = CInt(mdtEmployee.Rows(count)("employeeunkid"))
                    Dim mstrEmployeeCode As String = mdtEmployee.Rows(count)("employeecode").ToString()
                    Dim mstrEmployeeName As String = mdtEmployee.Rows(count)("employeename").ToString()

                    StrQ = " SELECT DISTINCT " & _
                               " bgfundprojectcode_master.fundprojectcodeunkid " & _
                               ",bgfundsource_master.fundsourceunkid " & _
                               ",fundname + ' (' + fundcode + ')' AS Grantcode " & _
                               ",fundprojectcode " & _
                               ",fundprojectname " & _
                               " FROM bgbudgetcodesfundsource_tran " & _
                               " LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodesfundsource_tran.budgetcodestranunkid =  bgbudgetcodes_tran.budgetcodestranunkid  " & _
                               " LEFT JOIN bgbudget_master ON bgbudget_master.budgetunkid = bgbudgetcodes_tran.budgetunkid  " & _
                               " LEFT JOIN bgfundprojectcode_master ON bgfundprojectcode_master.fundprojectcodeunkid = bgbudgetcodesfundsource_tran.fundprojectcodeunkid  " & _
                               " LEFT JOIN bgfundsource_master ON bgfundsource_master.fundsourceunkid = bgfundprojectcode_master.fundsourceunkid  " & _
                               " LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = bgbudgetcodesfundsource_tran.fundactivityunkid " & _
                               " WHERE bgbudgetcodesfundsource_tran.isvoid = 0 And bgbudgetcodes_tran.isvoid = 0 And bgbudget_master.isvoid = 0 And bgbudgetcodesfundsource_tran.percentage > 0 " & _
                               " AND bgbudget_master.allocationbyid = 0 AND  bgbudgetcodesfundsource_tran.periodunkid = @periodunkid and bgbudgetcodes_tran.allocationtranunkid = @employeeunkid " & _
                               " AND bgbudget_master.isdefault = 1 "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodID)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)

                    dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    End If


                    If dsList IsNot Nothing AndAlso dsDayList IsNot Nothing Then
                        For Each dr In dsDayList.Tables(0).Rows
                            dsList.Tables(0).Columns.Add(CInt(dr("Dvalue")).ToString("#00"), Type.GetType("System.String")).DefaultValue = ""
                            dsList.Tables(0).Columns(CInt(dr("Dvalue")).ToString("#00")).Caption = CInt(dr("dday")).ToString("#00")
                            dsList.Tables(0).Columns(CInt(dr("Dvalue")).ToString("#00")).ExtendedProperties.Add("ddate", CDate(dr("ddate")).Date)
                        Next

                        dsList.Tables(0).Columns.Add(Language.getMessage(mstrModuleName, 4, "Total"), Type.GetType("System.String")).DefaultValue = ""

                        Dim drRow As DataRow = dsList.Tables(0).NewRow
                        drRow("fundprojectcodeunkid") = -1
                        drRow("fundprojectcode") = Language.getMessage(mstrModuleName, 11, "Sub Total")
                        drRow("fundprojectname") = Language.getMessage(mstrModuleName, 11, "Sub Total")
                        dsList.Tables(0).Rows.InsertAt(drRow, dsList.Tables(0).Rows.Count)
                    End If



                    StrQ = " SELECT " & _
                              " projectcodeunkid " & _
                              ",ltbemployee_timesheet.employeeunkid " & _
                              ",ISNULL(hremployee_master.employeecode,'') AS EmployeeCode " & _
                              ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                              ",ISNULL(SM.name,'') AS [Branch] " & _
                              ",ISNULL(DGM.name,'') AS [Department Group] " & _
                              ",ISNULL(DM.name,'') AS [Department] " & _
                              ",ISNULL(SGM.name,'') AS [Section Group] " & _
                              ",ISNULL(SECM.name,'') AS [Section] " & _
                              ",ISNULL(UGM.name,'') AS [Unit Group] " & _
                              ",ISNULL(UM.name,'') AS [Unit] " & _
                              ",ISNULL(TM.name,'') AS [Team] " & _
                              ",ISNULL(CGM.name,'') AS [Class Group]" & _
                              ",ISNULL(CM.name,'') AS [Class] " & _
                              ",ISNULL(JGM.name,'') AS [Job Group] " & _
                              ",ISNULL(JM.job_name,'') AS [Job] " & _
                              ",ISNULL(CCM.costcentername,'') AS [CostCenter] " & _
                              ",ISNULL(GGM.name,'') AS [Grade Group] " & _
                              ",ISNULL(GM.name,'') AS [Grade] " & _
                              ",ISNULL(GLM.name,'') AS [Grade Level] " & _
                              ",ltbemployee_timesheet.activitydate " & _
                              ",Convert(Char(8),ltbemployee_timesheet.activitydate,112) AS Adate " & _
                              ",SUM(ltbemployee_timesheet.approvedactivity_hrs) AS ActivityHrsinMins " & _
                              ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),SUM(CAST(ltbemployee_timesheet.approvedactivity_hrs AS INT)) / 60), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (SUM(CAST(ltbemployee_timesheet.approvedactivity_hrs AS DECIMAL)) % 60)), 2), '00:00') As ActivityHrs " & _
                              ", ISNULL(ltbemployee_timesheet.submission_remark,'') AS submission_remark " & _
                              " FROM ltbemployee_timesheet " & _
                              " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = ltbemployee_timesheet.employeeunkid " & _
                              " LEFT JOIN " & _
                              "( " & _
                              "    SELECT " & _
                              "         jobunkid " & _
                              "        ,jobgroupunkid " & _
                              "        ,employeeunkid " & _
                              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                              "    FROM hremployee_categorization_tran " & _
                              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EndDate " & _
                              " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                              " LEFT JOIN hrjobgroup_master AS JGM ON Jobs.jobgroupunkid = JGM.jobgroupunkid " & _
                              " JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid " & _
                              " JOIN " & _
                              " ( " & _
                              "    SELECT " & _
                              "         gradegroupunkid " & _
                              "        ,gradeunkid " & _
                              "        ,gradelevelunkid " & _
                              "        ,employeeunkid " & _
                              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                              "    FROM prsalaryincrement_tran " & _
                              "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= @EndDate " & _
                              " ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                              " JOIN hrgradegroup_master AS GGM ON GGM.gradegroupunkid = Grds.gradegroupunkid " & _
                              " JOIN hrgrade_master AS GM ON Grds.gradeunkid = GM.gradeunkid " & _
                              " JOIN hrgradelevel_master AS GLM ON Grds.gradelevelunkid = GLM.gradelevelunkid " & _
                              " LEFT JOIN " & _
                              " ( " & _
                              "    SELECT " & _
                              "         cctranheadvalueid " & _
                              "        ,employeeunkid " & _
                              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                              "    FROM hremployee_cctranhead_tran " & _
                              "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                             "    AND CONVERT(CHAR(8),effectivedate,112) <= @EndDate" & _
                             " ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                             "JOIN prcostcenter_master AS CCM ON CCM.costcenterunkid = CC.cctranheadvalueid " & _
                             " JOIN " & _
                                        " ( " & _
                                        "    SELECT " & _
                                        "         stationunkid " & _
                                        "        ,deptgroupunkid " & _
                                        "        ,departmentunkid " & _
                                        "        ,sectiongroupunkid " & _
                                        "        ,sectionunkid " & _
                                        "        ,unitgroupunkid " & _
                                        "        ,unitunkid " & _
                                        "        ,teamunkid " & _
                                        "        ,classgroupunkid " & _
                                        "        ,classunkid " & _
                                        "        ,employeeunkid " & _
                                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                        "    FROM hremployee_transfer_tran " & _
                                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EndDate" & _
                                        " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                            " LEFT JOIN hrstation_master AS SM ON SM.stationunkid = Alloc.stationunkid " & _
                            " LEFT JOIN hrdepartment_group_master AS DGM ON Alloc.deptgroupunkid = DGM.deptgroupunkid " & _
                            " LEFT JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                            " LEFT JOIN hrsectiongroup_master AS SGM ON Alloc.sectiongroupunkid = SGM.sectiongroupunkid " & _
                            " LEFT JOIN hrsection_master AS SECM ON SECM.sectionunkid  = Alloc.sectionunkid " & _
                            " LEFT JOIN hrunitgroup_master AS UGM ON Alloc.unitgroupunkid = UGM.unitgroupunkid " & _
                            " LEFT JOIN hrunit_master AS UM ON UM.unitunkid = Alloc.unitunkid " & _
                            " LEFT JOIN hrteam_master AS TM ON Alloc.teamunkid = TM.teamunkid " & _
                            " LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid " & _
                            " LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                            " WHERE ltbemployee_timesheet.isvoid = 0 AND ltbemployee_timesheet.issubmit_approval = 1 " & _
                            " AND ltbemployee_timesheet.statusunkid = 1 AND ltbemployee_timesheet.employeeunkid = @employeeunkid " & _
                            " AND CONVERT(CHAR(8),ltbemployee_timesheet.activitydate,112) >= @StartDate AND CONVERT(CHAR(8),ltbemployee_timesheet.activitydate,112) <= @EndDate " & _
                            " GROUP BY  projectcodeunkid " & _
                            ",ltbemployee_timesheet.employeeunkid " & _
                            ",ltbemployee_timesheet.activitydate " & _
                            ",Convert(Char(8),ltbemployee_timesheet.activitydate,112) " & _
                            ",ISNULL(hremployee_master.employeecode,'') " & _
                            ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') " & _
                            ",ISNULL(SM.name,'') " & _
                            ",ISNULL(DGM.name,'')  " & _
                            ",ISNULL(DM.name,'')  " & _
                            ",ISNULL(SGM.name,'')  " & _
                            ",ISNULL(SECM.name,'')  " & _
                            ",ISNULL(UGM.name,'') " & _
                            ",ISNULL(UM.name,'') " & _
                            ",ISNULL(TM.name,'')  " & _
                            ",ISNULL(CGM.name,'') " & _
                            ",ISNULL(CM.name,'')  " & _
                            ",ISNULL(JGM.name,'')  " & _
                            ",ISNULL(JM.job_name,'')  " & _
                            ",ISNULL(CCM.costcentername,'')  " & _
                            ",ISNULL(GGM.name,'')  " & _
                            ",ISNULL(GM.name,'')  " & _
                            ",ISNULL(GLM.name,'')  " & _
                            ",ISNULL(JGM.name,'')  " & _
                            ",ISNULL(JM.job_name,'')  " & _
                            ",ISNULL(CCM.costcentername,'')  " & _
                            ",ISNULL(GGM.name,'')  " & _
                            ",ISNULL(GM.name,'') " & _
                            ",ISNULL(GLM.name,'')  " & _
                            ", ISNULL(ltbemployee_timesheet.submission_remark,'')  "

                    'Pinkal (28-Jul-2018) --  'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251][ ", ISNULL(ltbemployee_timesheet.submission_remark,'') AS submission_remark " & _]


                    'Pinkal (28-Mar-2018) -- Start
                    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                    '", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),SUM(CAST(ltbemployee_timesheet.approvedactivity_hrs)) / 60), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (SUM(CAST(ltbemployee_timesheet.approvedactivity_hrs)) % 60)), 2), '00:00') As ActivityHrs " & _
                    'Pinkal (28-Mar-2018) -- End

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                    objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate.Date))
                    objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate.Date))
                    Dim dsEmployee As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    End If


                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    Dim mdctSubmissionRemark As Dictionary(Of Integer, String) = Nothing
                    mstrSubmissionRemark = ""
                    'Pinkal (28-Jul-2018) -- End

                    If dsEmployee IsNot Nothing AndAlso dsEmployee.Tables(0).Rows.Count > 0 Then
                        Dim objEmpShift As New clsEmployee_Shift_Tran
                        Dim objShift As New clsshift_tran
                        Dim xRowTotal As Integer = 0


                        'Pinkal (28-Jul-2018) -- Start
                        'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                        mdctSubmissionRemark = New Dictionary(Of Integer, String)
                        'Pinkal (28-Jul-2018) -- End


                        For xRow As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                            xRowTotal = 0
                            For xcol As Integer = 0 To dsList.Tables(0).Columns.Count - 1

                                '/*  START FOR GETTING WEEKEND
                                Dim mintshiftID As Integer = objEmpShift.GetEmployee_Current_ShiftId(CDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")).Date, mintEmployeeId)
                                objShift.GetShiftTran(mintshiftID)
                                If CInt(dsList.Tables(0).Rows(xRow)("fundprojectcodeunkid")) > 0 Then
                                    Dim drWeekend() As DataRow = objShift._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")).Date), False, FirstDayOfWeek.Sunday).ToString()) & " AND isweekend = 1")
                                    If drWeekend.Length > 0 AndAlso dsList.Tables(0).Rows(xRow)(xcol).ToString().Length <= 0 Then
                                        dsList.Tables(0).Rows(xRow)(xcol) = Language.getMessage(mstrModuleName, 10, "WK")
                                    End If
                                End If
                                '/*  END FOR GETTING WEEKEND

                                For xEmp As Integer = 0 To dsEmployee.Tables(0).Rows.Count - 1

                                    'Pinkal (19-Jun-2017) -- Start
                                    'Enhancement - Implemented Leave/TnA Seperated Period .
                                    'If CInt(dsList.Tables(0).Rows(xRow)("fundprojectcodeunkid")) = CInt(dsEmployee.Tables(0).Rows(xEmp)("projectcodeunkid")) AndAlso _
                                    '    dsList.Tables(0).Columns(xcol).ColumnName = CDate(dsEmployee.Tables(0).Rows(xEmp)("activitydate")).ToString("dd") Then
                                    If CInt(dsList.Tables(0).Rows(xRow)("fundprojectcodeunkid")) = CInt(dsEmployee.Tables(0).Rows(xEmp)("projectcodeunkid")) AndAlso _
                                        dsList.Tables(0).Columns(xcol).ColumnName = eZeeDate.convertDate(dsEmployee.Tables(0).Rows(xEmp)("activitydate")).ToString() Then
                                        'Pinkal (19-Jun-2017) -- End

                                        dsList.Tables(0).Rows(xRow)(xcol) = dsEmployee.Tables(0).Rows(xEmp)("ActivityHrs").ToString()


                                        '/* START  CALCULATE TOTAL FOR EACH COLUMN
                                        Dim mintTotalSec As Integer = IIf(IsDBNull(dsEmployee.Tables(0).Compute("SUM(ActivityHrsinMins)", "Adate = '" & eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")) & "'")), 0, _
                                                                                    dsEmployee.Tables(0).Compute("SUM(ActivityHrsinMins)", "Adate = '" & eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")) & "'"))
                                        dsList.Tables(0).Rows(dsList.Tables(0).Rows.Count - 1)(xcol) = CalculateTime(True, (mintTotalSec * 60)).ToString("#00.00").Replace(".", ":")

                                        If dctTotalHrByCol.ContainsKey(eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate"))) = False Then
                                            dctTotalHrByCol.Add(eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")), (mintTotalSec * 60))
                                        End If

                                        '/* START  CALCULATE TOTAL FOR EACH COLUMN 


                                        '/* START  CALCULATE TOTAL FOR EACH ROW 
                                        xRowTotal += CInt(dsEmployee.Tables(0).Rows(xEmp)("ActivityHrsinMins"))
                                        dsList.Tables(0).Rows(xRow)(dsList.Tables(0).Columns.Count - 1) = CalculateTime(True, (xRowTotal * 60)).ToString("#00.00").Replace(".", ":")
                                        '/* START  CALCULATE TOTAL FOR EACH ROW 

                                        '/* START  CALCULATE PROJECT TOTAL FOR LAST COLUMN AND LAST ROW OF PROJECT SECTION
                                        If dsList.Tables(0).Columns(dsList.Tables(0).Columns.Count - 1).ExtendedProperties.Keys.Count <= 0 Then
                                            dsList.Tables(0).Columns(dsList.Tables(0).Columns.Count - 1).ExtendedProperties.Add("ProjectTotal", xRowTotal)
                                        Else
                                            dsList.Tables(0).Columns(dsList.Tables(0).Columns.Count - 1).ExtendedProperties.Item("ProjectTotal") = CInt(dsList.Tables(0).Columns(dsList.Tables(0).Columns.Count - 1).ExtendedProperties.Item("ProjectTotal")) + CInt(dsEmployee.Tables(0).Rows(xEmp)("ActivityHrsinMins"))
                                        End If

                                        dsList.Tables(0).Rows(dsList.Tables(0).Rows.Count - 1)(dsList.Tables(0).Columns.Count - 1) = CalculateTime(True, CInt(dsList.Tables(0).Columns(dsList.Tables(0).Columns.Count - 1).ExtendedProperties.Item("ProjectTotal")) * 60).ToString("#00.00").Replace(".", ":")

                                        '/* END  CALCULATE PROJECT TOTAL FOR LAST COLUMN AND LAST ROW OF PROJECT SECTION



                                        'Pinkal (28-Jul-2018) -- Start
                                        'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                                        If mdctSubmissionRemark.ContainsValue(dsEmployee.Tables(0).Rows(xEmp)("submission_remark").ToString()) = False Then
                                            mdctSubmissionRemark.Add(xEmp, dsEmployee.Tables(0).Rows(xEmp)("submission_remark").ToString().Trim())
                                        End If
                                        'Pinkal (28-Jul-2018) -- End


                                    End If

                                Next

                            Next

                        Next

                        dsList.AcceptChanges()

                    End If


                    Dim xTotalRow As Integer = dsList.Tables(0).Rows.Count


                    StrQ = " SELECT 0 AS leavetypeunkid,'Holiday' AS leavetypecode,'Holiday' as leavename" & _
                               " UNION " & _
                               " SELECT leavetypeunkid,leavetypecode,leavename from lvleavetype_master where isactive = 1 "
                    objDataOperation.ClearParameters()
                    Dim dsHoliday As DataSet = objDataOperation.ExecQuery(StrQ, "List")


                    If dsHoliday IsNot Nothing AndAlso dsHoliday.Tables(0).Rows.Count > 0 Then
                        Dim drRow As DataRow = Nothing
                        For Each dr As DataRow In dsHoliday.Tables(0).Rows
                            If dsList IsNot Nothing Then
                                drRow = dsList.Tables(0).NewRow
                                drRow("fundprojectcodeunkid") = CInt(dr("leavetypeunkid"))
                                drRow("fundprojectcode") = dr("leavetypecode").ToString()
                                drRow("fundprojectname") = dr("leavename").ToString
                                dsList.Tables(0).Rows.Add(drRow)
                            End If
                        Next

                        drRow = dsList.Tables(0).NewRow
                        drRow("fundprojectcodeunkid") = -999
                        drRow("fundprojectcode") = Language.getMessage(mstrModuleName, 4, "Total")
                        drRow("fundprojectname") = Language.getMessage(mstrModuleName, 4, "Total")
                        dsList.Tables(0).Rows.Add(drRow)

                    End If



                    If dsHoliday IsNot Nothing AndAlso dsHoliday.Tables(0).Rows.Count > 0 Then

                        Dim objEmpholiday As New clsemployee_holiday
                        Dim objLeaveIssue As New clsleaveissue_master
                        Dim xRowTotal As Integer = 0
                        Dim objEmpShift As New clsEmployee_Shift_Tran
                        Dim objShift As New clsNewshift_master
                        Dim mintFinalTotalLastCol As Integer = 0

                        For xRow As Integer = xTotalRow To dsList.Tables(0).Rows.Count - 1
                            xRowTotal = 0

                            For xcol As Integer = 0 To dsList.Tables(0).Columns.Count - 1


                                Dim mintshiftID As Integer = objEmpShift.GetEmployee_Current_ShiftId(CDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")).Date, mintEmployeeId)
                                Dim mintDayID As Integer = GetWeekDayNumber(WeekdayName(Weekday(CDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")).Date), False, FirstDayOfWeek.Sunday))
                                Dim mintWorkingHrs As Integer = objShift.GetShiftWorkingHours(mintshiftID, mintDayID)


                                If xRow = dsList.Tables(0).Rows.IndexOf(dsList.Tables(0).Rows(xTotalRow)) Then  'FOR HOLIDAY

                                    If objEmpholiday.GetEmployeeHoliday(mintEmployeeId, CDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")).Date).Rows.Count > 0 Then

CalculateLeave:
                                        dsList.Tables(0).Rows(xRow)(xcol) = CalculateTime(True, mintWorkingHrs).ToString("#00.00").Replace(".", ":")

                                        '/* START  CALCULATE TOTAL FOR EACH ROW 
                                        xRowTotal += mintWorkingHrs
                                        dsList.Tables(0).Rows(xRow)(dsList.Tables(0).Columns.Count - 1) = CalculateTime(True, xRowTotal).ToString("#00.00").Replace(".", ":")
                                        '/* END  CALCULATE TOTAL FOR EACH ROW 


                                        If dctTotalHrByCol.ContainsKey(eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate"))) = True Then
                                            dctTotalHrByCol.Item(eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate"))) = dctTotalHrByCol.Item(eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate"))) + mintWorkingHrs
                                        Else
                                            dctTotalHrByCol.Add(eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")), mintWorkingHrs)
                                        End If

                                    End If

                                Else 'FOR LEAVE

                                    If dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate") IsNot Nothing AndAlso CInt(dsList.Tables(0).Rows(xRow)("fundprojectcodeunkid")) > 0 _
                                     AndAlso objLeaveIssue.GetEmployeeTotalIssue(mintEmployeeId, CInt(dsList.Tables(0).Rows(xRow)("fundprojectcodeunkid")), True, CDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")).Date _
                                                                                                          , CDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")).Date, False, 0, -1) > 0 Then

                                        GoTo CalculateLeave
                                    End If

                                End If



                                If dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate") IsNot Nothing AndAlso dctTotalHrByCol.ContainsKey(eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate"))) = True Then
                                    dsList.Tables(0).Rows(dsList.Tables(0).Rows.Count - 1)(xcol) = CalculateTime(True, dctTotalHrByCol.Item(eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")))).ToString("#00.00").Replace(".", ":")
                                End If

                                '/* START  END  CALCULATE TOTAL FOR LAST ROW AND LAST COLUMN AND LAST CELL.
                                If xRow = dsList.Tables(0).Rows.Count - 1 Then
                                    If dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate") IsNot Nothing AndAlso dctTotalHrByCol.ContainsKey(eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate"))) = True Then
                                        mintFinalTotalLastCol += dctTotalHrByCol.Item(eZeeDate.convertDate(dsList.Tables(0).Columns(xcol).ExtendedProperties.Item("ddate")))
                                        dsList.Tables(0).Rows(dsList.Tables(0).Rows.Count - 1)(dsList.Tables(0).Columns.Count - 1) = CalculateTime(True, mintFinalTotalLastCol).ToString("#00.00").Replace(".", ":")
                                    End If
                                End If
                                '/* END  CALCULATE TOTAL FOR LAST ROW AND LAST COLUMN AND LAST CELL.

                            Next


                        Next

                        dsList.AcceptChanges()

                    End If

                    dctTotalHrByCol.Clear()
                    'dctTotalHrByCol = Nothing



                    'Pinkal (21-Mar-2017) -- Start
                    'Enhancement - Working On Budget Timesheet Report.

                    StrQ = " SELECT " & _
                               "  ISNULL(tsLevel.tslevelunkid, 0) AS tslevelunkid " & _
                               ", ISNULL(tsLevel.levelname, '') AS [Level] " & _
                               ", ISNULL(STUFF((SELECT DISTINCT ',' +  CAST( CASE WHEN tsapprover_master.isexternalapprover = 0 THEN  " & _
                               "                                                                            ISNULL(hremployee_master.employeecode, '') + ' - ' +  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'')  " & _
                               "                                                              ELSE " & _
                               "                                                              ISNULL(usr.username,'') END   AS NVARCHAR(max)) " & _
                               "                          FROM tsemptimesheet_approval " & _
                               "                          LEFT JOIN tsapprover_master ON tsemptimesheet_approval.tsapproverunkid = tsapprover_master.tsapproverunkid " & _
                               "                          LEFT JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid " & _
                               "                          LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tsapprover_master.employeeapproverunkid " & _
                               "                          LEFT JOIN hrapprover_usermapping ON tsapprover_master.tsapproverunkid = hrapprover_usermapping.approverunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Timesheet_Approver & _
                               "                          LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON hrapprover_usermapping.userunkid = Usr.userunkid " & _
                               "                          WHERE periodunkid = @periodunkid AND tsemptimesheet_approval.employeeunkid = @employeeunkid " & _
                               "                          AND tsemptimesheet_approval.isvoid = 0 AND tsemptimesheet_approval.statusunkid = 1 " & _
                               "                          AND tsemptimesheet_approval.iscancel = 0 AND tsapproverlevel_master.tslevelunkid = tsLevel.tslevelunkid   FOR XML PATH('')),1,1,''),'') AS Approver " & _
                                       ", ISNULL(STUFF((SELECT DISTINCT ',' +  CAST(tsemptimesheet_approval.tsapproverunkid  AS NVARCHAR(max))  " & _
                                       "                         FROM tsemptimesheet_approval " & _
                                       "                         LEFT JOIN tsapprover_master ON tsemptimesheet_approval.tsapproverunkid = tsapprover_master.tsapproverunkid  " & _
                                       "                         LEFT JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid  " & _
                                       "                         WHERE periodunkid = @periodunkid And tsemptimesheet_approval.employeeunkid = @employeeunkid And tsemptimesheet_approval.isvoid = 0 And tsemptimesheet_approval.statusunkid = 1 " & _
                                       "                         AND tsemptimesheet_approval.iscancel = 0 AND tsapproverlevel_master.tslevelunkid = tsLevel.tslevelunkid   FOR XML PATH('')),1,1,''),'') AS ApproverId  " & _
                                       ", ISNULL(STUFF((SELECT DISTINCT ',' +  CAST(tsemptimesheet_approval.approveremployeeunkid  AS NVARCHAR(max))  " & _
                                       "                         FROM tsemptimesheet_approval " & _
                                       "                         LEFT JOIN tsapprover_master ON tsemptimesheet_approval.tsapproverunkid = tsapprover_master.tsapproverunkid  " & _
                                       "                         LEFT JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid  " & _
                                       "                         WHERE periodunkid = @periodunkid And tsemptimesheet_approval.employeeunkid = @employeeunkid And tsemptimesheet_approval.isvoid = 0 And tsemptimesheet_approval.statusunkid = 1 " & _
                                       "                         AND tsemptimesheet_approval.iscancel = 0 AND tsapproverlevel_master.tslevelunkid = tsLevel.tslevelunkid   FOR XML PATH('')),1,1,''),'') AS ApproverEmpId  " & _
                               "  FROM tsapproverlevel_master tsLevel "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodID)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                    Dim dsApprover As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    End If
                    'Pinkal (21-Mar-2017) -- End



                    'Pinkal (22-Jul-2019) -- Start
                    'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
                    Dim mdtSubmissionDate As DateTime = Nothing

                    StrQ = " SELECT max(auditdatetime) AS FinalSubmissionDate " & _
                               " FROM atltbemployee_timesheet " & _
                               " WHERE issubmit_approval = 1 AND statusunkid = 2 AND audittype = 2 " & _
                               " AND CONVERT(CHAR(8),atltbemployee_timesheet.activitydate,112) BETWEEN @StartDate AND @EndDate " & _
                               " AND periodunkid = @periodunkid AND employeeunkid = @employeeunkid "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodID)
                    objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate.Date))
                    objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate.Date))
                    Dim dsSubmissionDate As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    End If

                    If dsSubmissionDate IsNot Nothing AndAlso dsSubmissionDate.Tables(0).Rows.Count > 0 Then
                        If IsDBNull(dsSubmissionDate.Tables(0).Rows(0)("FinalSubmissionDate")) = False Then
                            mdtSubmissionDate = CDate(dsSubmissionDate.Tables(0).Rows(0)("FinalSubmissionDate"))
                        End If
                    End If

                    dsSubmissionDate = Nothing


                    'Pinkal (22-Jul-2019) -- End



                    ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

                    Dim strarrGroupColumns As String() = Nothing
                    Dim rowsArrayHeader As New ArrayList
                    Dim rowsArrayFooter As New ArrayList
                    Dim row As WorksheetRow
                    Dim wcell As WorksheetCell
                    'Pinkal (21-Mar-2017) -- Start
                    Dim strBuilder As New StringBuilder
                    'Pinkal (21-Mar-2017) -- End

                    Dim mdtTableExcel As DataTable = dsList.Tables(0).Copy()


                    If mdtTableExcel.Columns.Contains("fundprojectcodeunkid") Then
                        mdtTableExcel.Columns.Remove("fundprojectcodeunkid")
                    End If

                    'Pinkal (13-Apr-2017) -- Start
                    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                    If mdtTableExcel.Columns.Contains("fundsourceunkid") Then
                        mdtTableExcel.Columns.Remove("fundsourceunkid")
                    End If
                    'Pinkal (13-Apr-2017) -- End


                    If mdtTableExcel.Columns.Contains("fundprojectcode") Then
                        mdtTableExcel.Columns.Remove("fundprojectcode")
                    End If


                    Dim objEmp As New clsEmployee_Master
                    Dim dsEmpList As DataSet = objEmp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtStartDate.Date, mdtEndDate.Date, _
                                                              xUserModeSetting, True, False, "List", False, mintEmployeeId, _
                                                              , , , IIf(xUserUnkid <= 0, False, True))




                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    If mblnShowSubmissionRemark Then
                        If mdctSubmissionRemark IsNot Nothing AndAlso mdctSubmissionRemark.Keys.Count > 0 Then
                            For Each pair In mdctSubmissionRemark
                                mstrSubmissionRemark &= mdctSubmissionRemark(pair.Key).ToString() & ","
                            Next
                        End If
                        If mstrSubmissionRemark.Trim.Length > 0 Then
                            mstrSubmissionRemark = mstrSubmissionRemark.Trim.Substring(0, mstrSubmissionRemark.Trim.Length - 1)
                        End If
                    End If
                    'Pinkal (28-Jul-2018) -- End

                    If mblnViewHTMLReport = False Then

                        row = New WorksheetRow()

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Name:") & " " & mstrEmployeeName & _
                                                                               Space(30) & Language.getMessage(mstrModuleName, 6, "Employee No:") & " " & mstrEmployeeCode & _
                                                                               Space(30) & Language.getMessage(mstrModuleName, 7, "Position / Title:") & " " & IIf(dsEmpList.Tables(0).Rows(0)("job_name").ToString.Length <= 0, "", dsEmpList.Tables(0).Rows(0)("job_name").ToString) & _
                                                                               Space(30) & Language.getMessage(mstrModuleName, 8, "Month:") & " " & mstrPeriodName & _
                                                                               Space(30) & Language.getMessage(mstrModuleName, 9, "Location:") & " " & IIf(dsEmpList.Tables(0).Rows(0)("station").ToString.Length <= 0, "", dsEmpList.Tables(0).Rows(0)("station").ToString), "s10bw")


                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        row.Cells.Add(wcell)
                        rowsArrayHeader.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s10bw")
                        row.Cells.Add(wcell)
                        rowsArrayHeader.Add(row)


                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s10bw")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s10bw")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)


                        'Pinkal (22-Jul-2019) -- Start
                        'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.

                        If mdtSubmissionDate <> Nothing Then
                            row = New WorksheetRow()
                            wcell = New WorksheetCell("", "s10bw")
                            row.Cells.Add(wcell)
                            rowsArrayFooter.Add(row)

                            row = New WorksheetRow()
                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Submitted DateTime :"), "s10bw")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(mdtSubmissionDate.ToString(), "s10bw")
                            wcell.MergeAcross = mdtTableExcel.Columns.Count - 2
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                        End If

                        If mblnShowSubmissionRemark Then

                            row = New WorksheetRow()
                            wcell = New WorksheetCell("", "s10bw")
                            row.Cells.Add(wcell)
                            rowsArrayFooter.Add(row)

                            row = New WorksheetRow()
                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Submission Remark :"), "s10bw")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(mstrSubmissionRemark, "s10bw")
                            wcell.MergeAcross = mdtTableExcel.Columns.Count - 2
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                        End If


                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s10bw")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)


                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Approver Information :"), "s10bw")

                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)


                        If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                            For Each dr As DataRow In dsApprover.Tables(0).Rows
                                If dr("Approver").ToString().Trim.Length > 0 Then

                                    Dim mdtFinalApprovalDate As DateTime = GetApproverApprovalDate(mintEmployeeId, CInt(dr("ApproverId")), CInt(dr("ApproverEmpId")))

                                    row = New WorksheetRow()
                                    wcell = New WorksheetCell(dr("Level").ToString(), "s10bw")
                                    wcell.MergeAcross = 1
                                    row.Cells.Add(wcell)

                                    If mdtFinalApprovalDate = Nothing Then
                                        wcell = New WorksheetCell(dr("Approver").ToString(), "s10bw")
                                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2
                                        row.Cells.Add(wcell)
                                    Else
                                        wcell = New WorksheetCell(dr("Approver").ToString() & Space(20) & Language.getMessage(mstrModuleName, 15, "Approved DateTime :") & mdtFinalApprovalDate.ToString(), "s10bw")
                                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2
                                        row.Cells.Add(wcell)
                                    End If

                                    rowsArrayFooter.Add(row)
                                End If
                            Next

                        End If

                        'Pinkal (22-Jul-2019) -- End



                    Else

                        strBuilder.Append("<TR>" & vbCrLf)
                        strBuilder.Append("<TD colspan=" & mdtTableExcel.Columns.Count - 1 & " style='font-size:13px;border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 5, "Name:") & " " & mstrEmployeeName & _
                                                                                                                                                                                                         "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 6, "Employee No:") & " " & mstrEmployeeCode & _
                                                                                                                                                                                                         "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 7, "Position / Title:") & " " & IIf(dsEmpList.Tables(0).Rows(0)("job_name").ToString.Length <= 0, "", dsEmpList.Tables(0).Rows(0)("job_name").ToString) & _
                                                                                                                                                                                                         "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 8, "Month:") & " " & mstrPeriodName & _
                                                                                                                                                                                                         "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 9, "Location:") & " " & IIf(dsEmpList.Tables(0).Rows(0)("station").ToString.Length <= 0, "", dsEmpList.Tables(0).Rows(0)("station").ToString) & _
                                                                                                                                                                                                "</B></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                        strBuilder.Remove(0, strBuilder.Length)

                        strBuilder.Append("<TR>" & vbCrLf)
                        strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)

                        strBuilder.Append("<TR>" & vbCrLf)
                        strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)


                        'Pinkal (22-Jul-2019) -- Start
                        'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
                        If mdtSubmissionDate <> Nothing Then
                            strBuilder.Append("<TR>" & vbCrLf)
                            strBuilder.Append("<TD style='border-width: 0px;'>&nbsp; </TD>" & vbCrLf)
                            strBuilder.Append("</TR>" & vbCrLf)

                            strBuilder.Append("<TR>" & vbCrLf)
                            strBuilder.Append("<TD Colspan = 1 style='border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 14, "Submitted DateTime :") & "</B></TD>" & vbCrLf)
                            strBuilder.Append("<TD Colspan = " & mdtTableExcel.Columns.Count - 1 & " style='border-width: 0px;'><B>" & mdtSubmissionDate.ToString() & "</B></TD>" & vbCrLf)
                            strBuilder.Append("</TR>" & vbCrLf)
                        End If



                        If mblnShowSubmissionRemark Then
                            strBuilder.Append("<TR>" & vbCrLf)
                            strBuilder.Append("<TD style='border-width: 0px;'>&nbsp; </TD>" & vbCrLf)
                            strBuilder.Append("</TR>" & vbCrLf)


                            strBuilder.Append("<TR>" & vbCrLf)
                            strBuilder.Append("<TD Colspan = 1 style='border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 12, "Submission Remark :") & "</B></TD>" & vbCrLf)
                            strBuilder.Append("<TD Colspan = " & mdtTableExcel.Columns.Count - 1 & " style='border-width: 0px;'><B>" & mstrSubmissionRemark & "</B></TD>" & vbCrLf)
                            strBuilder.Append("</TR>" & vbCrLf)
                        End If


                        strBuilder.Append("<TR>" & vbCrLf)
                        strBuilder.Append("<TD style='border-width: 0px;'>&nbsp; </TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)

                        strBuilder.Append("<TR>" & vbCrLf)
                        strBuilder.Append("<TD Colspan = " & mdtTableExcel.Rows.Count - 1 & " style='border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 12, "Approver Information :") & "</B> </TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)

                        strBuilder.Append("<TR>" & vbCrLf)
                        strBuilder.Append("<TD style='border-width: 0px;'>&nbsp; </TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)

                        If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                            For Each dr As DataRow In dsApprover.Tables(0).Rows
                                If dr("Approver").ToString().Trim.Length > 0 Then

                                    Dim mdtFinalApprovalDate As DateTime = GetApproverApprovalDate(mintEmployeeId, CInt(dr("ApproverId")), CInt(dr("ApproverEmpId")))

                                    If mdtFinalApprovalDate = Nothing Then
                                        strBuilder.Append("<TR>" & vbCrLf)
                                        strBuilder.Append("<TD Colspan = 2 style='border-width: 0px;'><B>" & dr("Level").ToString() & "</B></TD>" & vbCrLf)
                                        strBuilder.Append("<TD Colspan = " & mdtTableExcel.Rows.Count - 2 & " style='border-width: 0px;'><B>" & dr("Approver").ToString() & "</B></TD>" & vbCrLf)
                                        strBuilder.Append("</TR>" & vbCrLf)
                                    Else
                                        strBuilder.Append("<TR>" & vbCrLf)
                                        strBuilder.Append("<TD Colspan = 2 style='border-width: 0px;'><B>" & dr("Level").ToString() & "</B></TD>" & vbCrLf)
                                        strBuilder.Append("<TD Colspan = " & mdtTableExcel.Rows.Count - 2 & " style='border-width: 0px;'><B>" & dr("Approver").ToString() & "</B></TD>" & vbCrLf)
                                        strBuilder.Append("<TD Colspan = " & mdtTableExcel.Rows.Count - 9 & " style='border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 15, "Approved DateTime :") & mdtFinalApprovalDate.ToString() & "</B></TD>" & vbCrLf)
                                        strBuilder.Append("</TR>" & vbCrLf)
                                    End If

                                End If

                            Next

                        End If

                        'Pinkal (22-Jul-2019) -- End

                        If strBuilder.Length > 0 Then rowsArrayFooter.Add(strBuilder.ToString)
                        strBuilder.Remove(0, strBuilder.Length)

                    End If
                    'Pinkal (21-Mar-2017) -- End

                    '--------------------


                    mdtTableExcel.Columns("fundprojectname").Caption = Language.getMessage(mstrModuleName, 1, "Project Name")
                    'Pinkal (13-Apr-2017) -- Start
                    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                    mdtTableExcel.Columns("Grantcode").Caption = Language.getMessage(mstrModuleName, 13, "Grant Code")
                    mdtTableExcel.Columns("Grantcode").ExtendedProperties.Add("style", "s8b")
                    'Pinkal (13-Apr-2017) -- End

                    'Sohail (24 Feb 2017) -- Start
                    'Enhancement - 65.1 - Allow to set excel style on perticular column in Advance Excel.
                    mdtTableExcel.Columns("fundprojectname").ExtendedProperties.Add("style", "s8b")
                    'Sohail (24 Feb 2017) -- End


                    'SET EXCEL CELL WIDTH  
                    Dim intArrayColumnWidth As Integer() = Nothing
                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        If i <= 1 Then
                            intArrayColumnWidth(i) = 100
                        ElseIf i > 1 AndAlso i < intArrayColumnWidth.Length - 1 Then
                            intArrayColumnWidth(i) = 25
                        ElseIf i <= intArrayColumnWidth.Length - 1 Then
                            intArrayColumnWidth(i) = 50
                        End If
                    Next
                    'SET EXCEL CELL WIDTH


                    'Pinkal (22-Jul-2019) -- Start
                    'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
                    If mstrEmployeeCode.Contains("/") Then
                        mstrEmployeeCode = mstrEmployeeCode.Replace("/", "-")
                    ElseIf mstrEmployeeCode.Contains("\") Then
                        mstrEmployeeCode = mstrEmployeeCode.Replace("\", "-")
                    End If

                    Dim mblnPromptSavedMsg As Boolean = False
                    If count = mdtEmployee.Rows.Count - 1 Then mblnPromptSavedMsg = True

                    'If mblnViewHTMLReport Then
                    '    Call ReportExecute(xCompanyUnkid, enArutiReport.Emplyoee_Project_Code_Timesheet_Report, Nothing, enPrintAction.None, enExportAction.ExcelHTML, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False, True, mstrEmployeeCode & "_" & mstrEmployeeName & "_" & mstrPeriodName, blnIsFromEmailAttachment, True )
                    'Else
                    '    Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False, mstrEmployeeCode & "_" & mstrEmployeeName & "_" & mstrPeriodName, True)
                    'End If
                    If mblnViewHTMLReport Then
                        Call ReportExecute(xCompanyUnkid, enArutiReport.Emplyoee_Project_Code_Timesheet_Report, Nothing, enPrintAction.None, enExportAction.ExcelHTML, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False, True, mstrEmployeeCode & "_" & mstrEmployeeName & "_" & mstrPeriodName, blnIsFromEmailAttachment, True, mblnPromptSavedMsg)
                    Else
                        Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False, mstrEmployeeCode & "_" & mstrEmployeeName & "_" & mstrPeriodName, True, mblnPromptSavedMsg)
                    End If

                    'Pinkal (22-Jul-2019) -- End

                Next

            End If


            'Pinkal (22-Jul-2019) -- Start
            'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
            mstrExportReportPath = ConfigParameter._Object._ExportReportPath
            'Pinkal (22-Jul-2019) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (22-Jul-2019) -- Start
    'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.

    Public Function GetApproverApprovalDate(ByVal xEmployeeId As Integer, ByVal xApproverId As Integer, ByVal xApproverEmpId As Integer) As DateTime
        Dim mdtApprovalDate As DateTime = Nothing
        Try
            Dim StrQ As String = " SELECT max(auditdatetime) AS FinalApprovalDate " & _
                                           " FROM attsemptimesheet_approval " & _
                                           " JOIN ltbemployee_timesheet ON ltbemployee_timesheet.emptimesheetunkid = attsemptimesheet_approval.emptimesheetunkid AND ltbemployee_timesheet.isvoid = 0 " & _
                                           " WHERE attsemptimesheet_approval.statusunkid = 1 and attsemptimesheet_approval.iscancel= 0 AND attsemptimesheet_approval.audittype  =2 " & _
                                           " AND CONVERT(CHAR(8),ltbemployee_timesheet.activitydate,112) BETWEEN @StartDate AND @EndDate " & _
                                           " AND attsemptimesheet_approval.tsapproverunkid = @tsapproverunkid AND attsemptimesheet_approval.approveremployeeunkid = @approveremployeeunkid " & _
                                           " AND attsemptimesheet_approval.periodunkid = @periodunkid AND attsemptimesheet_approval.employeeunkid = @employeeunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodID)
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xApproverId)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xApproverEmpId)
            objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate.Date))
            objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate.Date))
            Dim dsFinalApprovalDate As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsFinalApprovalDate IsNot Nothing AndAlso dsFinalApprovalDate.Tables(0).Rows.Count > 0 Then
                If IsDBNull(dsFinalApprovalDate.Tables(0).Rows(0)("FinalApprovalDate")) = False Then
                    mdtApprovalDate = CDate(dsFinalApprovalDate.Tables(0).Rows(0)("FinalApprovalDate"))
                End If
            End If
            dsFinalApprovalDate = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverApprovalDate; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
        Return mdtApprovalDate
    End Function

    'Pinkal (22-Jul-2019) -- End


#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Project Name")
            Language.setMessage(mstrModuleName, 3, "Leave Type:")
            Language.setMessage(mstrModuleName, 4, "Total")
            Language.setMessage(mstrModuleName, 5, "Name:")
            Language.setMessage(mstrModuleName, 6, "Employee No:")
            Language.setMessage(mstrModuleName, 7, "Position / Title:")
            Language.setMessage(mstrModuleName, 8, "Month:")
            Language.setMessage(mstrModuleName, 9, "Location:")
            Language.setMessage(mstrModuleName, 10, "WK")
            Language.setMessage(mstrModuleName, 11, "Sub Total")
            Language.setMessage(mstrModuleName, 12, "Submission Remark :")
            Language.setMessage(mstrModuleName, 13, "Grant Code")
            Language.setMessage(mstrModuleName, 14, "Submitted DateTime :")
            Language.setMessage(mstrModuleName, 15, "Approved DateTime :")
            Language.setMessage(mstrModuleName, 12, "Approver Information :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
