﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeTimeSheetReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkDisplayShortHrsWhenEmpAbsent = New System.Windows.Forms.CheckBox
        Me.chkShowActualTimingAfterRoundOff = New System.Windows.Forms.CheckBox
        Me.chkShowShortHrsOnBaseHrs = New System.Windows.Forms.CheckBox
        Me.chkShowLateComingInShortHrs = New System.Windows.Forms.CheckBox
        Me.chkIncludeOFFHrsInBaseHrs = New System.Windows.Forms.CheckBox
        Me.chkShowTotalAbsentDays = New System.Windows.Forms.CheckBox
        Me.chkShowBaseHrs = New System.Windows.Forms.CheckBox
        Me.chkShift = New System.Windows.Forms.CheckBox
        Me.chkBreaktime = New System.Windows.Forms.CheckBox
        Me.lnkSave = New System.Windows.Forms.LinkLabel
        Me.chkShowTimingsIn24Hrs = New System.Windows.Forms.CheckBox
        Me.chkShowLeaveType = New System.Windows.Forms.CheckBox
        Me.chkShowHoliday = New System.Windows.Forms.CheckBox
        Me.objbtnSearchAttCode = New eZee.Common.eZeeGradientButton
        Me.LblAttCode = New System.Windows.Forms.Label
        Me.cboAttCode = New System.Windows.Forms.ComboBox
        Me.chkShowEachEmpOnNewPage = New System.Windows.Forms.CheckBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchShift = New eZee.Common.eZeeGradientButton
        Me.LblShift = New System.Windows.Forms.Label
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.chkShowAbsentAfterEmpTerminated = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 553)
        Me.NavPanel.Size = New System.Drawing.Size(767, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowAbsentAfterEmpTerminated)
        Me.gbFilterCriteria.Controls.Add(Me.chkDisplayShortHrsWhenEmpAbsent)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowActualTimingAfterRoundOff)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowShortHrsOnBaseHrs)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowLateComingInShortHrs)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeOFFHrsInBaseHrs)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowTotalAbsentDays)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowBaseHrs)
        Me.gbFilterCriteria.Controls.Add(Me.chkShift)
        Me.gbFilterCriteria.Controls.Add(Me.chkBreaktime)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSave)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowTimingsIn24Hrs)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowHoliday)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAttCode)
        Me.gbFilterCriteria.Controls.Add(Me.LblAttCode)
        Me.gbFilterCriteria.Controls.Add(Me.cboAttCode)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowEachEmpOnNewPage)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchShift)
        Me.gbFilterCriteria.Controls.Add(Me.LblShift)
        Me.gbFilterCriteria.Controls.Add(Me.cboShift)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lblToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmpName)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(430, 414)
        Me.gbFilterCriteria.TabIndex = 15
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkDisplayShortHrsWhenEmpAbsent
        '
        Me.chkDisplayShortHrsWhenEmpAbsent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDisplayShortHrsWhenEmpAbsent.Location = New System.Drawing.Point(76, 342)
        Me.chkDisplayShortHrsWhenEmpAbsent.Name = "chkDisplayShortHrsWhenEmpAbsent"
        Me.chkDisplayShortHrsWhenEmpAbsent.Size = New System.Drawing.Size(301, 19)
        Me.chkDisplayShortHrsWhenEmpAbsent.TabIndex = 237
        Me.chkDisplayShortHrsWhenEmpAbsent.Text = "Display Short Hrs When Employee is Absent"
        Me.chkDisplayShortHrsWhenEmpAbsent.UseVisualStyleBackColor = True
        '
        'chkShowActualTimingAfterRoundOff
        '
        Me.chkShowActualTimingAfterRoundOff.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowActualTimingAfterRoundOff.Location = New System.Drawing.Point(76, 364)
        Me.chkShowActualTimingAfterRoundOff.Name = "chkShowActualTimingAfterRoundOff"
        Me.chkShowActualTimingAfterRoundOff.Size = New System.Drawing.Size(301, 19)
        Me.chkShowActualTimingAfterRoundOff.TabIndex = 236
        Me.chkShowActualTimingAfterRoundOff.Text = "Show Actual Time After Gracing Time"
        Me.chkShowActualTimingAfterRoundOff.UseVisualStyleBackColor = True
        '
        'chkShowShortHrsOnBaseHrs
        '
        Me.chkShowShortHrsOnBaseHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowShortHrsOnBaseHrs.Location = New System.Drawing.Point(76, 299)
        Me.chkShowShortHrsOnBaseHrs.Name = "chkShowShortHrsOnBaseHrs"
        Me.chkShowShortHrsOnBaseHrs.Size = New System.Drawing.Size(301, 19)
        Me.chkShowShortHrsOnBaseHrs.TabIndex = 235
        Me.chkShowShortHrsOnBaseHrs.Text = "Show Short Hrs in Place of Base Hrs"
        Me.chkShowShortHrsOnBaseHrs.UseVisualStyleBackColor = True
        '
        'chkShowLateComingInShortHrs
        '
        Me.chkShowLateComingInShortHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLateComingInShortHrs.Location = New System.Drawing.Point(76, 320)
        Me.chkShowLateComingInShortHrs.Name = "chkShowLateComingInShortHrs"
        Me.chkShowLateComingInShortHrs.Size = New System.Drawing.Size(301, 19)
        Me.chkShowLateComingInShortHrs.TabIndex = 233
        Me.chkShowLateComingInShortHrs.Text = "Show Late Coming/ Early Leaving Consider As Short Hrs"
        Me.chkShowLateComingInShortHrs.UseVisualStyleBackColor = True
        '
        'chkIncludeOFFHrsInBaseHrs
        '
        Me.chkIncludeOFFHrsInBaseHrs.Checked = True
        Me.chkIncludeOFFHrsInBaseHrs.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIncludeOFFHrsInBaseHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeOFFHrsInBaseHrs.Location = New System.Drawing.Point(76, 257)
        Me.chkIncludeOFFHrsInBaseHrs.Name = "chkIncludeOFFHrsInBaseHrs"
        Me.chkIncludeOFFHrsInBaseHrs.Size = New System.Drawing.Size(240, 16)
        Me.chkIncludeOFFHrsInBaseHrs.TabIndex = 231
        Me.chkIncludeOFFHrsInBaseHrs.Text = "Include OFF Hours in Total Base Hours"
        Me.chkIncludeOFFHrsInBaseHrs.UseVisualStyleBackColor = True
        '
        'chkShowTotalAbsentDays
        '
        Me.chkShowTotalAbsentDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowTotalAbsentDays.Location = New System.Drawing.Point(76, 235)
        Me.chkShowTotalAbsentDays.Name = "chkShowTotalAbsentDays"
        Me.chkShowTotalAbsentDays.Size = New System.Drawing.Size(206, 16)
        Me.chkShowTotalAbsentDays.TabIndex = 229
        Me.chkShowTotalAbsentDays.Text = "Show Total Absent"
        Me.chkShowTotalAbsentDays.UseVisualStyleBackColor = True
        '
        'chkShowBaseHrs
        '
        Me.chkShowBaseHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowBaseHrs.Location = New System.Drawing.Point(292, 213)
        Me.chkShowBaseHrs.Name = "chkShowBaseHrs"
        Me.chkShowBaseHrs.Size = New System.Drawing.Size(127, 16)
        Me.chkShowBaseHrs.TabIndex = 228
        Me.chkShowBaseHrs.Text = "Show Base Hrs"
        Me.chkShowBaseHrs.UseVisualStyleBackColor = True
        '
        'chkShift
        '
        Me.chkShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShift.Location = New System.Drawing.Point(76, 213)
        Me.chkShift.Name = "chkShift"
        Me.chkShift.Size = New System.Drawing.Size(206, 16)
        Me.chkShift.TabIndex = 226
        Me.chkShift.Text = "Show Shift"
        Me.chkShift.UseVisualStyleBackColor = True
        '
        'chkBreaktime
        '
        Me.chkBreaktime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBreaktime.Location = New System.Drawing.Point(292, 168)
        Me.chkBreaktime.Name = "chkBreaktime"
        Me.chkBreaktime.Size = New System.Drawing.Size(127, 16)
        Me.chkBreaktime.TabIndex = 225
        Me.chkBreaktime.Text = "Show Break Time"
        Me.chkBreaktime.UseVisualStyleBackColor = True
        '
        'lnkSave
        '
        Me.lnkSave.ActiveLinkColor = System.Drawing.Color.Maroon
        Me.lnkSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSave.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSave.LinkColor = System.Drawing.Color.Maroon
        Me.lnkSave.Location = New System.Drawing.Point(335, 393)
        Me.lnkSave.Name = "lnkSave"
        Me.lnkSave.Size = New System.Drawing.Size(90, 15)
        Me.lnkSave.TabIndex = 223
        Me.lnkSave.TabStop = True
        Me.lnkSave.Text = "&Save Selection"
        Me.lnkSave.VisitedLinkColor = System.Drawing.Color.Maroon
        '
        'chkShowTimingsIn24Hrs
        '
        Me.chkShowTimingsIn24Hrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowTimingsIn24Hrs.Location = New System.Drawing.Point(76, 189)
        Me.chkShowTimingsIn24Hrs.Name = "chkShowTimingsIn24Hrs"
        Me.chkShowTimingsIn24Hrs.Size = New System.Drawing.Size(206, 18)
        Me.chkShowTimingsIn24Hrs.TabIndex = 221
        Me.chkShowTimingsIn24Hrs.Text = "Show Timing in 24 HRS Format"
        Me.chkShowTimingsIn24Hrs.UseVisualStyleBackColor = True
        '
        'chkShowLeaveType
        '
        Me.chkShowLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLeaveType.Location = New System.Drawing.Point(292, 191)
        Me.chkShowLeaveType.Name = "chkShowLeaveType"
        Me.chkShowLeaveType.Size = New System.Drawing.Size(127, 16)
        Me.chkShowLeaveType.TabIndex = 220
        Me.chkShowLeaveType.Text = "Show Leave Type"
        Me.chkShowLeaveType.UseVisualStyleBackColor = True
        '
        'chkShowHoliday
        '
        Me.chkShowHoliday.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowHoliday.Location = New System.Drawing.Point(292, 144)
        Me.chkShowHoliday.Name = "chkShowHoliday"
        Me.chkShowHoliday.Size = New System.Drawing.Size(127, 16)
        Me.chkShowHoliday.TabIndex = 219
        Me.chkShowHoliday.Text = "Show Holiday"
        Me.chkShowHoliday.UseVisualStyleBackColor = True
        '
        'objbtnSearchAttCode
        '
        Me.objbtnSearchAttCode.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAttCode.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAttCode.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAttCode.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAttCode.BorderSelected = False
        Me.objbtnSearchAttCode.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAttCode.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAttCode.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAttCode.Location = New System.Drawing.Point(404, 114)
        Me.objbtnSearchAttCode.Name = "objbtnSearchAttCode"
        Me.objbtnSearchAttCode.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAttCode.TabIndex = 217
        '
        'LblAttCode
        '
        Me.LblAttCode.BackColor = System.Drawing.Color.Transparent
        Me.LblAttCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAttCode.Location = New System.Drawing.Point(8, 118)
        Me.LblAttCode.Name = "LblAttCode"
        Me.LblAttCode.Size = New System.Drawing.Size(62, 13)
        Me.LblAttCode.TabIndex = 215
        Me.LblAttCode.Text = "AttCode"
        '
        'cboAttCode
        '
        Me.cboAttCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAttCode.DropDownWidth = 120
        Me.cboAttCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAttCode.FormattingEnabled = True
        Me.cboAttCode.Location = New System.Drawing.Point(76, 114)
        Me.cboAttCode.Name = "cboAttCode"
        Me.cboAttCode.Size = New System.Drawing.Size(322, 21)
        Me.cboAttCode.TabIndex = 216
        '
        'chkShowEachEmpOnNewPage
        '
        Me.chkShowEachEmpOnNewPage.Checked = True
        Me.chkShowEachEmpOnNewPage.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowEachEmpOnNewPage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEachEmpOnNewPage.Location = New System.Drawing.Point(76, 167)
        Me.chkShowEachEmpOnNewPage.Name = "chkShowEachEmpOnNewPage"
        Me.chkShowEachEmpOnNewPage.Size = New System.Drawing.Size(206, 18)
        Me.chkShowEachEmpOnNewPage.TabIndex = 213
        Me.chkShowEachEmpOnNewPage.Text = "Show Each Employee On New Page"
        Me.chkShowEachEmpOnNewPage.UseVisualStyleBackColor = True
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(269, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 23
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchShift
        '
        Me.objbtnSearchShift.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchShift.BorderSelected = False
        Me.objbtnSearchShift.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchShift.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchShift.Location = New System.Drawing.Point(404, 59)
        Me.objbtnSearchShift.Name = "objbtnSearchShift"
        Me.objbtnSearchShift.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchShift.TabIndex = 211
        '
        'LblShift
        '
        Me.LblShift.BackColor = System.Drawing.Color.Transparent
        Me.LblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblShift.Location = New System.Drawing.Point(8, 63)
        Me.LblShift.Name = "LblShift"
        Me.LblShift.Size = New System.Drawing.Size(62, 13)
        Me.LblShift.TabIndex = 209
        Me.LblShift.Text = "Shift"
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.DropDownWidth = 120
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Location = New System.Drawing.Point(76, 59)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(322, 21)
        Me.cboShift.TabIndex = 210
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(404, 87)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 207
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(76, 143)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(206, 18)
        Me.chkInactiveemp.TabIndex = 204
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(226, 35)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(51, 13)
        Me.lblToDate.TabIndex = 175
        Me.lblToDate.Text = "To Date"
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(285, 31)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpToDate.TabIndex = 176
        '
        'lblEmpName
        '
        Me.lblEmpName.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(8, 91)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(62, 13)
        Me.lblEmpName.TabIndex = 167
        Me.lblEmpName.Text = "Employee"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(76, 87)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(322, 21)
        Me.cboEmployee.TabIndex = 168
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(8, 35)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(62, 13)
        Me.lblFromDate.TabIndex = 151
        Me.lblFromDate.Text = "From Date"
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(76, 31)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpFromDate.TabIndex = 152
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(9, 486)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(430, 63)
        Me.gbSortBy.TabIndex = 16
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(404, 30)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(68, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(76, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(322, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'chkShowAbsentAfterEmpTerminated
        '
        Me.chkShowAbsentAfterEmpTerminated.Checked = True
        Me.chkShowAbsentAfterEmpTerminated.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowAbsentAfterEmpTerminated.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowAbsentAfterEmpTerminated.Location = New System.Drawing.Point(76, 279)
        Me.chkShowAbsentAfterEmpTerminated.Name = "chkShowAbsentAfterEmpTerminated"
        Me.chkShowAbsentAfterEmpTerminated.Size = New System.Drawing.Size(322, 17)
        Me.chkShowAbsentAfterEmpTerminated.TabIndex = 239
        Me.chkShowAbsentAfterEmpTerminated.Text = "Show Absent Days On Timesheet After Employee Terminated"
        Me.chkShowAbsentAfterEmpTerminated.UseVisualStyleBackColor = True
        '
        'frmEmployeeTimeSheetReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 608)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbSortBy)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeTimeSheetReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Daily TimeSheet"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblEmpName As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Public WithEvents lblFromDate As System.Windows.Forms.Label
    Public WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Public WithEvents lblToDate As System.Windows.Forms.Label
    Public WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchShift As eZee.Common.eZeeGradientButton
    Private WithEvents LblShift As System.Windows.Forms.Label
    Public WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchAttCode As eZee.Common.eZeeGradientButton
    Private WithEvents LblAttCode As System.Windows.Forms.Label
    Public WithEvents cboAttCode As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowEachEmpOnNewPage As System.Windows.Forms.CheckBox
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowHoliday As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowLeaveType As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowTimingsIn24Hrs As System.Windows.Forms.CheckBox
    Friend WithEvents lnkSave As System.Windows.Forms.LinkLabel
    Friend WithEvents chkShift As System.Windows.Forms.CheckBox
    Friend WithEvents chkBreaktime As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowBaseHrs As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowTotalAbsentDays As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeOFFHrsInBaseHrs As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowLateComingInShortHrs As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowShortHrsOnBaseHrs As System.Windows.Forms.CheckBox
    Friend WithEvents chkDisplayShortHrsWhenEmpAbsent As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowActualTimingAfterRoundOff As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowAbsentAfterEmpTerminated As System.Windows.Forms.CheckBox
End Class
