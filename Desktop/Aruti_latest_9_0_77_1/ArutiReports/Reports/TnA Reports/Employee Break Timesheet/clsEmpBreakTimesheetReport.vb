Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsEmpBreakTimesheetReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmpBreakTimesheetReport"
    Private mstrReportId As String = enArutiReport.Employee_Breaktime_Timesheet
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmpId As Integer = 0
    Private mintShiftunkid As Integer = 0
    Private mstrEmpName As String = ""
    Private mstrOrderByQuery As String = ""
    Private mblnIsActive As Boolean = True
    Private mstrShiftName As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrEmployeeCode As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnEachEmployeeOnPage As Boolean = False
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _Shiftunkid() As Integer
        Set(ByVal value As Integer)
            mintShiftunkid = value
        End Set
    End Property

    Public WriteOnly Property _ShiftName() As String
        Set(ByVal value As String)
            mstrShiftName = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _EmpCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _ShowEachEmployeeOnNewPage() As Boolean
        Set(ByVal value As Boolean)
            mblnEachEmployeeOnPage = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmpId = 0
            mintShiftunkid = 0
            mstrEmpName = ""
            mstrOrderByQuery = ""
            mblnIsActive = True
            mstrShiftName = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrEmployeeCode = ""
            mstrAdvanceFilter = ""
            mblnEachEmployeeOnPage = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try
            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, " From Date: ") & " " & mdtFromDate.ToShortDateString & " "

            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, " To Date: ") & " " & mdtToDate.ToShortDateString & " "

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                Me._FilterQuery &= " AND tnalogin_summary.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Code : ") & " " & mstrEmployeeCode & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Employee : ") & " " & mstrEmpName & " "
            End If

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If mintShiftunkid > 0 Then
                objDataOperation.AddParameter("@ShiftId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid)
                Me._FilterQuery &= " AND tnashift_master.shiftunkid = @ShiftId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Shift : ") & " " & mstrShiftName & " "
            End If

            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= "  ORDER BY employeecode," & Me.OrderByQuery
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, " Order By : ") & " " & Me.OrderByDisplay

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid
        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt
        '    '
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()

            iColumn_DetailReport.Add(New IColumn("tnalogin_summary.login_date", Language.getMessage(mstrModuleName, 1, "Date")))
            iColumn_DetailReport.Add(New IColumn(" ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumn_DetailReport.Add(New IColumn("A.breakin", Language.getMessage(mstrModuleName, 3, "Break In ")))
            iColumn_DetailReport.Add(New IColumn("A.breakout", Language.getMessage(mstrModuleName, 4, "Break Out")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport = Nothing
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mstrUserAccessFilter.Trim.Length <= 0 Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If
            'Shani(24-Aug-2015) -- End

            StrQ = "SELECT " & _
                    "  tnalogin_summary.employeeunkid " & _
                    ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
                    ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee " & _
                    ", tnalogin_summary.shiftunkid " & _
                    ", ISNULL(tnashift_master.shiftname,'') AS Shift " & _
                    ", tnalogin_summary.login_date " & _
                    ", A.breakin " & _
                    ", A.breakout " & _
                    ", ISNULL(A.BreakHrs,'00:00') AS BreakHrs " & _
                    ", ISNULL(Breaksec,0) AS Breaksec " & _
                    ", '00:00' AS TotBreakHrs "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM tnalogin_summary " & _
                         " LEFT JOIN ( " & _
                         "                     SELECT logindate " & _
                         "                     , tnalogin_tran.employeeunkid " & _
                         "                     , DATEADD(SECOND, breakhr * (-1), checkintime) AS breakin " & _
                         "                     , checkintime AS breakout " & _
                         "                     , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), tnalogin_tran.breakhr / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), tnalogin_tran.breakhr % 3600 / 60), 2), '00:00') 	AS BreakHrs " & _
                         "                     , ISNULL(tnalogin_tran.breakhr,0) AS Breaksec " & _
                         "                      FROM tnalogin_tran " & _
                         "                      WHERE checkintime Is Not NULL AND checkouttime IS NOT NULL AND isvoid = 0 AND breakhr > 59 " & _
                         "                  ) AS A  ON A.employeeunkid = tnalogin_summary.employeeunkid 	AND A.logindate = tnalogin_summary.login_date " & _
                         " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid " & _
                         " LEFT JOIN tnashift_master ON tnashift_master.shiftunkid =  tnalogin_summary.shiftunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Shani(24-Aug-2015) -- End

            StrQ &= " WHERE CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) BETWEEN @FromDate and @ToDate "


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvanceFilter & " "
            'End If

            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                 " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                 " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                 " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter.Trim.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim TotHolidays As Integer = 0
            Dim TotMispunch As Integer = 0
            Dim intEmployeID As Integer = 0
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows


                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("GName")
                rpt_Row.Item("Column2") = dtRow.Item("Id")
                rpt_Row.Item("Column3") = dtRow.Item("employeecode")
                rpt_Row.Item("Column4") = dtRow.Item("employee")
                rpt_Row.Item("Column5") = dtRow.Item("Shift")
                rpt_Row.Item("Column6") = CDate(dtRow.Item("login_date")).ToShortDateString
                If Not IsDBNull(dtRow.Item("breakin")) Then
                    rpt_Row.Item("Column7") = CDate(dtRow.Item("breakin")).ToShortTimeString
                End If
                If Not IsDBNull(dtRow.Item("breakout")) Then
                    rpt_Row.Item("Column8") = CDate(dtRow.Item("breakout")).ToShortTimeString
                End If
                rpt_Row.Item("Column9") = dtRow.Item("BreakHrs")
                rpt_Row.Item("Column10") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column11") = dtRow.Item("shiftunkid")
                rpt_Row.Item("Column12") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Breaksec)", "employeecode = '" & dtRow("employeecode") & "'"))).ToString("#00.00")
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next



            objRpt = New ArutiReport.Designer.rptEmpBreakTimeSheet

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 5, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 6, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 7, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 8, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 9, "Sr.No"))
            Call ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 10, "Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 11, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 12, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtShift", Language.getMessage(mstrModuleName, 13, "Shift"))
            Call ReportFunction.TextChange(objRpt, "txtBreakInTime", Language.getMessage(mstrModuleName, 3, "Break In"))
            Call ReportFunction.TextChange(objRpt, "txtBreakOutTime", Language.getMessage(mstrModuleName, 4, "Break Out"))
            Call ReportFunction.TextChange(objRpt, "txtTotalBreakhrs", Language.getMessage(mstrModuleName, 14, "Total Break Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtGrptotal", Language.getMessage(mstrModuleName, 15, "Total :"))

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If mblnEachEmployeeOnPage = True Then
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", True)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "if Previous ({ArutiTable.Column1}) <> {ArutiTable.Column1} then 0 Else 1 "
            Else
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", False)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "0"
            End If

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 16, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 17, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region


    'Pinkal (06-Aug-2015) -- Start


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Date")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "Break In")
            Language.setMessage(mstrModuleName, 4, "Break Out")
            Language.setMessage(mstrModuleName, 5, "Prepared By :")
            Language.setMessage(mstrModuleName, 6, "Checked By :")
            Language.setMessage(mstrModuleName, 7, "Approved By :")
            Language.setMessage(mstrModuleName, 8, "Received By :")
            Language.setMessage(mstrModuleName, 9, "Sr.No")
            Language.setMessage(mstrModuleName, 10, "Code :")
            Language.setMessage(mstrModuleName, 11, "Employee :")
            Language.setMessage(mstrModuleName, 12, "Date")
            Language.setMessage(mstrModuleName, 13, "Shift")
            Language.setMessage(mstrModuleName, 14, "Total Break Hrs")
            Language.setMessage(mstrModuleName, 15, "Total :")
            Language.setMessage(mstrModuleName, 16, "Printed By :")
            Language.setMessage(mstrModuleName, 17, "Printed Date :")
            Language.setMessage(mstrModuleName, 18, " From Date:")
            Language.setMessage(mstrModuleName, 19, " To Date:")
            Language.setMessage(mstrModuleName, 20, "Code :")
            Language.setMessage(mstrModuleName, 21, "Shift :")
            Language.setMessage(mstrModuleName, 22, " Order By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
