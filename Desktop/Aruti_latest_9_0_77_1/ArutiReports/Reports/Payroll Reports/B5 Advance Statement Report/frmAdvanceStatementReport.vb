Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

Public Class frmAdvanceStatementReport

#Region "Private Variables"

    Private mstrModuleName As String = "frmAdvanceStatementReport"
    Private objAdvanceStatement As clsAdvanceStatementReport
    Dim mdtStartDate As Date = Nothing
    Dim mdtEndDate As Date = Nothing
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region "Private Function"

    Private Sub FillCombo()
        Try
            Dim objEmployee As New clsEmployee_Master

            Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsList.Tables(0)
            cboEmployee.SelectedValue = 0
            objEmployee = Nothing


            Dim objMasterData As New clsMasterData
            dsList = objMasterData.GetLoan_Saving_Status("List", False, False)
            cboMemoStatus.ValueMember = "Id"
            cboMemoStatus.DisplayMember = "NAME"
            cboMemoStatus.DataSource = dsList.Tables(0)
            cboMemoStatus.SelectedValue = 0

            objMasterData = Nothing
            dsList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function SetValue() As Boolean
        Try
            objAdvanceStatement._AdvanceVoucherNo = txtAdvanceVoucherNo.Text.Trim()
            objAdvanceStatement._EmployeeID = CInt(cboEmployee.SelectedValue)
            objAdvanceStatement._EmployeeName = cboEmployee.Text
            objAdvanceStatement._VoucherFromDate = dtpVoucherFromDate.Value.Date
            objAdvanceStatement._VouncherToDate = dtpVoucherToDate.Value.Date
            If CInt(cboMemoStatus.SelectedValue) > 0 Then
                objAdvanceStatement._MemoStatusId = CInt(cboMemoStatus.SelectedValue)
                objAdvanceStatement._MemoStatus = cboMemoStatus.Text
            End If

            objAdvanceStatement._ViewByIds = mstrViewByIds
            objAdvanceStatement._ViewIndex = mintViewIndex
            objAdvanceStatement._ViewByName = mstrViewByName
            objAdvanceStatement._Analysis_Fields = mstrAnalysis_Fields
            objAdvanceStatement._Analysis_Join = mstrAnalysis_Join
            objAdvanceStatement._Analysis_OrderBy = mstrAnalysis_OrderBy
            objAdvanceStatement._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objAdvanceStatement._Report_GroupName = mstrReport_GroupName
            objAdvanceStatement._Advance_Filter = mstrAdvanceFilter

            objAdvanceStatement._ExportReportPath = ConfigParameter._Object._ExportReportPath
            objAdvanceStatement._OpenAfterExport = ConfigParameter._Object._OpenAfterExport


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub ResetValue()
        Try
            txtAdvanceVoucherNo.Text = ""
            cboEmployee.SelectedValue = 0
            dtpVoucherFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpVoucherToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            cboMemoStatus.SelectedIndex = 0
            mstrAdvanceFilter = String.Empty
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Form's Events"

    Private Sub frmAdvanceStatementReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objAdvanceStatement._ReportName
            eZeeHeader.Message = objAdvanceStatement._ReportDesc
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAdvanceStatementReport_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub frmAdvanceStatementReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAdvanceStatement = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAdvanceStatementReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAdvanceStatementReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAdvanceStatementReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAdvanceStatementReport.SetMessages()
            objfrm._Other_ModuleNames = "clsAdvanceStatementReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetValue() = False Then Exit Sub
            objAdvanceStatement.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                        , Company._Object._Companyunkid, dtpVoucherFromDate.Value.Date, dtpVoucherToDate.Value.Date _
                                                                        , ConfigParameter._Object._UserAccessModeSetting, True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim objFrm As New frmCommonSearch
            objFrm.DataSource = CType(cboEmployee.DataSource, DataTable)
            objFrm.ValueMember = cboEmployee.ValueMember
            objFrm.DisplayMember = cboEmployee.DisplayMember
            objFrm.CodeMember = "employeeCode"

            If objFrm.DisplayDialog() Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Link Button"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Try
            Dim objLnkAnalys As New frmViewAnalysis
            objLnkAnalys.displayDialog()
            mstrViewByIds = objLnkAnalys._ReportBy_Ids
            mstrViewByName = objLnkAnalys._ReportBy_Name
            mintViewIndex = objLnkAnalys._ViewIndex
            mstrAnalysis_Fields = objLnkAnalys._Analysis_Fields
            mstrAnalysis_Join = objLnkAnalys._Analysis_Join
            mstrAnalysis_OrderBy = objLnkAnalys._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = objLnkAnalys._Analysis_OrderBy_GName
            mstrReport_GroupName = objLnkAnalys._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LblFromPeriod.Text = Language._Object.getCaption(Me.LblFromPeriod.Name, Me.LblFromPeriod.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.LblVoucherToDate.Text = Language._Object.getCaption(Me.LblVoucherToDate.Name, Me.LblVoucherToDate.Text)
			Me.LblMemoStatus.Text = Language._Object.getCaption(Me.LblMemoStatus.Name, Me.LblMemoStatus.Text)
			Me.LblAdvanceVoucherNo.Text = Language._Object.getCaption(Me.LblAdvanceVoucherNo.Name, Me.LblAdvanceVoucherNo.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
