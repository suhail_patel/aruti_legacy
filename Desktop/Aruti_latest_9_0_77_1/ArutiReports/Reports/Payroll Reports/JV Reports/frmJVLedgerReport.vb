#Region " Imports "
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
Imports Aruti.Data
Imports ArutiReport
#End Region

Public Class frmJVLedgerReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmJVLedgerReport"
    Private objMainLedger As clsJVLedgerReport

    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty
    'S.SANDEEP [ 11 SEP 2012 ] -- END
    Private mintFirstOpenPeriod As Integer = 0 'Sohail (13 Mar 2013)
    Private mstrSearchText As String = ""  'Sohail (02 Sep 2016)

    'Sohail (01 Jun 2017) -- Start
    'Enhancement - 68.1 - Allocation filter on JV Report.
    Private mstrAdvanceFilter As String = String.Empty
    'Sohail (01 Jun 2017) -- End

    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
    Private dvScheme As DataView
    Private mintCount As Integer = 0
    'Sohail (07 Mar 2020) -- End

    'Sohail (10 Apr 2020) -- Start
    'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    'Sohail (10 Apr 2020) -- End

#End Region

#Region " Constructor "
    Public Sub New()
        objMainLedger = New clsJVLedgerReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        'Sohail (01 Jun 2017) -- Start
        'Enhancement - 68.1 - Allocation filter on JV Report.
        '_Show_AdvanceFilter = True
        If ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.FLEX_CUBE_JV) = False Then 'Sohail (24 May 2021)
        _Show_AdvanceFilter = True
        End If 'Sohail (24 May 2021)
        'Sohail (01 Jun 2017) -- End
        objMainLedger.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Enum "

#End Region

#Region " Public Functions "
    Public Sub FillCombo()
        Dim objMaster As New clsMasterData 'Sohail (13 Mar 2013)
        Try
            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objExRate As New clsExchangeRate
            'S.SANDEEP [ 11 SEP 2012 ] -- END

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Main Ledger"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "General Staff Ledger"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Cost Center Ledger"))
                'Sohail (02 Aug 2011) -- Start
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Combined JV"))
                'Sohail (02 Aug 2011) -- End
                .SelectedIndex = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid) 'Sohail (13 Mar 2013)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1) 'Sohail (13 Mar 2013)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, 0).Tables(0)
            cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0).Tables(0)
            'Sohail (21 Aug 2015) -- End
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DisplayMember = "Name"
            cboPeriod.SelectedValue = mintFirstOpenPeriod 'Sohail (13 Mar 2013)

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            Dim objBranch As New clsStation
            Dim dsList As New DataSet
            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objBranch = Nothing : dsList.Dispose()
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (06 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objCCGroup As New clspayrollgroup_master
            dsList = objCCGroup.getListForCombo(enPayrollGroupType.CostCenter, "CCGroup", True)
            With cboCCGroup
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("CCGroup")
                .SelectedValue = 0
            End With
            objCCGroup = Nothing
            'Sohail (06 Mar 2012) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objExRate.getComboList("ExRate", True)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 11 SEP 2012 ] -- END

            'Sohail (03 Mar 2014) -- Start
            'Enhancement - iScala2 JV for Aga Khan
            dsList = objMaster.GetComboListForCustomCCenter("CCenter")
            With cboCustomCCenter
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("CCenter")
                .SelectedValue = 0
            End With
            'Sohail (03 Mar 2014) -- End

            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
            Dim objHead As New clsTransactionHead
            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True)
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Head")
                .SelectedValue = 0
            End With
            objHead = Nothing 'Sohail (08 Jul 2017)
            'Sohail (02 Sep 2016) -- End

            'Sohail (08 Jul 2017) -- Start
            'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
            Dim objCC As New clscostcenter_master
            dsList = objCC.getComboList("CC", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsList.Tables("CC")
                .SelectedValue = 0
            End With
            objCC = Nothing
            'Sohail (08 Jul 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (13 Mar 2013) -- Start
            'TRA - ENHANCEMENT
        Finally
            objMaster = Nothing
            'Sohail (13 Mar 2013) -- End
        End Try
    End Sub

    'Sohail (25 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    Public Sub FillList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            lvTranHead.Items.Clear()
            objchkSelectAll.Checked = False
            Dim lvItem As ListViewItem

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("TranHead", False, enTranHeadType.EmployersStatutoryContributions)
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", False, enTranHeadType.EmployersStatutoryContributions)
            'Sohail (21 Aug 2015) -- End

            For Each dsRow As DataRow In dsList.Tables("TranHead").Rows
                lvItem = New ListViewItem

                lvItem.Tag = CInt(dsRow.Item("tranheadunkid"))

                lvItem.SubItems.Add(dsRow.Item("code").ToString)

                lvItem.SubItems.Add(dsRow.Item("name").ToString)
                lvItem.SubItems(colhEContribHead.Index).Tag = objTranHead.GetEmpContribPayableHeadID(CInt(dsRow.Item("tranheadunkid")))

                RemoveHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked
                lvTranHead.Items.Add(lvItem)
                AddHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objTranHead = Nothing
        End Try
    End Sub

    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
    Public Sub FillLoanScheme()
        Dim objLoanScheme As New clsLoan_Scheme
        Dim dsList As DataSet
        Try

            dgScheme.DataSource = Nothing

            dsList = objLoanScheme.getComboList(False, "Loan", -1, "", False)

            Dim dtTable As DataTable = New DataView(dsList.Tables("Loan"), "", "name", DataViewRowState.CurrentRows).ToTable

            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvScheme = dtTable.DefaultView
            dgScheme.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhunkid.DataPropertyName = "loanschemeunkid"
            dgColhSchemeCode.DataPropertyName = "Code"
            dgColhScheme.DataPropertyName = "name"

            dgScheme.DataSource = dvScheme
            dvScheme.Sort = "name "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLoanScheme", mstrModuleName)
        Finally
            objLoanScheme = Nothing
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAllLoanScheme.CheckedChanged, AddressOf objchkSelectAllLoanScheme_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgScheme.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            If mintCount <= 0 Then
                objchkSelectAllLoanScheme.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgScheme.Rows.Count Then
                objchkSelectAllLoanScheme.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgScheme.Rows.Count Then
                objchkSelectAllLoanScheme.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAllLoanScheme.CheckedChanged, AddressOf objchkSelectAllLoanScheme_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub
    'Sohail (07 Mar 2020) -- End

    Private Sub CheckAllHeads(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvTranHead.Items
                RemoveHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked 'Sohail (03 Nov 2010)
                lvItem.Checked = blnCheckAll
                AddHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked 'Sohail (03 Nov 2010)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllHeads", mstrModuleName)
        End Try
    End Sub
    'Sohail (25 Feb 2013) -- End

    Public Sub ResetValue()
        Try
            'Sohail (13 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'cboPeriod.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            'Sohail (13 Mar 2013) -- End
            txtDebitAmountFrom.Decimal = 0
            txtDebitAmountTo.Decimal = 0
            txtCreditAMountFrom.Decimal = 0
            txtCreditAMountTo.Decimal = 0

            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objMainLedger.OrderByDisplay


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

            'Sohail (02 Aug 2011) -- Start
            chkShowSummaryBottom.Checked = False
            chkShowSummaryNewPage.Checked = False
            'Sohail (02 Aug 2011) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            cboBranch.SelectedValue = 0
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            chkShowGroupByCCGroup.Checked = False 'Sohail (16 Dec 2011)
            cboCCGroup.SelectedValue = 0 'Sohail (06 Mar 2012)
            'chkIncludeEmployerContribution.Checked = False 'Sohail (16 Jul 2012)
            chkIncludeEmployerContribution.Checked = True 'Anjan (20 Jul 2012)

            'Sohail (25 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            chkIgnoreZero.Checked = True
            objchkSelectAll.Checked = True
            'Sohail (25 Feb 2013) -- End

            'Sohail (03 Mar 2014) -- Start
            'Enhancement - iScala2 JV for Aga Khan
            cboCustomCCenter.SelectedValue = 0
            chkShowColumnHeader.Checked = False
            'Sohail (03 Mar 2014) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GroupName = ""
            'Sohail (10 Apr 2020) -- End

            txtFromEmpID.Text = "0"
            txtToEmpID.Text = "0"
            objlblFromEmpID.Visible = False
            objlblToEmpID.Visible = False

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            lblExRate.Text = ""
            mdecEx_Rate = 1
            mstrCurr_Sign = String.Empty
            'S.SANDEEP [ 11 SEP 2012 ] -- END
            'Sohail (01 Jun 2017) -- Start
            'Enhancement - 68.1 - Allocation filter on JV Report.
            mstrAdvanceFilter = ""
            'Sohail (01 Jun 2017) -- End
            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
            cboTranHead.SelectedValue = 0
            'Sohail (08 Jul 2017) -- Start
            'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
            'Call SetDefaultSearchText()
            Call SetDefaultSearchText(cboTranHead)
            cboCostCenter.SelectedValue = 0
            Call SetDefaultSearchText(cboCostCenter)
            'Sohail (08 Jul 2017) -- End
            'Sohail (02 Sep 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValues", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter(ByRef dtPeriodStartDate As Date _
                              , ByRef dtPeriodEndDate As Date _
                              ) As Boolean
        'Sohail (21 Aug 2015) - [dtPeriodStartDate, dtPeriodEndDate]

        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Function
            End If
            'S.SANDEEP [ 11 SEP 2012 ] -- END

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.Text


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objMainLedger._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End

            'Sohail (02 Aug 2011) -- Start
            objMainLedger._ShowSummaryAtBootom = chkShowSummaryBottom.Checked
            objMainLedger._ShowSummaryOnNextPage = chkShowSummaryNewPage.Checked
            'Sohail (02 Aug 2011) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            objMainLedger._ShowGroupByCCenerGroup = chkShowGroupByCCGroup.Checked 'Sohail (16 Dec 2011)
            'Sohail (06 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            objMainLedger._CCenterGroupId = CInt(cboCCGroup.SelectedValue)
            objMainLedger._CCenterGroup_Name = cboCCGroup.Text
            'Sohail (06 Mar 2012) -- End

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked 'Sohail (16 Jul 2012)
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked 'Sohail (25 Feb 2013)

            'Sohail (11 May 2018) -- Start
            'CCK Enhancement - Ref # 235 : Salary Journal – Having employee details for debit entries in 72.1.
            objMainLedger._IncludeEmpCodeNameOnDebit = chkIncludeEmpCodeNameOnDebit.Checked
            'Sohail (11 May 2018) -- End

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            'Sohail (14 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            Dim strIDs As String = ""
            If lvTranHead.Visible = True Then
                'Sohail (18 Apr 2016) -- Start
                'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                'For Each lvItem As ListViewItem In lvTranHead.Items
                '    If lvItem.Checked = False AndAlso lvTranHead.CheckedItems.Count > 0 Then
                '        If strIDs.Trim = "" Then
                '            strIDs = lvItem.Tag.ToString & "," & lvItem.SubItems(colhEContribHead.Index).Tag.ToString
                '        Else
                '            strIDs &= "," & lvItem.Tag.ToString & "," & lvItem.SubItems(colhEContribHead.Index).Tag.ToString
                '        End If
                '    End If
                'Next
                If lvTranHead.CheckedItems.Count > 0 Then
                    strIDs = String.Join(",", (From lv In lvTranHead.Items.Cast(Of ListViewItem)() Where (lv.Checked = False) Select (lv.SubItems(colhEContribHead.Index).Tag.ToString)).ToArray)
                    End If
                'Sohail (18 Apr 2016) -- End
            End If
            objMainLedger._EmpContributionHeadUnCheckedIDs = strIDs
            'Sohail (14 Jun 2013) -- End

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            dtPeriodStartDate = objPeriod._Start_Date
            dtPeriodEndDate = objPeriod._End_Date
            'Sohail (21 Aug 2015) -- End

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            objMainLedger._PeriodCode = objPeriod._Period_Code
            'Sohail (18 Apr 2016) -- End
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date)) 'Sohail (08 Jul 2020)
            objPeriod = Nothing
            'Sohail (17 Apr 2012) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign
            'S.SANDEEP [ 11 SEP 2012 ] -- END
            'Sohail (01 Jun 2017) -- Start
            'Enhancement - 68.1 - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (01 Jun 2017) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'objMainLedger._Analysis_Join = mstrAnalysis_Join 'Sohail (08 Jul 2020)
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._FromEmpID = CInt(txtFromEmpID.Decimal)
            objMainLedger._ToEmpID = CInt(txtToEmpID.Decimal)

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        'Sohail (08 Jul 2017) - [cbo]
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 13, "Type to Search")
            'Sohail (08 Jul 2017) -- Start
            'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
            'With cboTranHead
            With cbo
                'Sohail (08 Jul 2017) -- End
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (02 Sep 2016) -- End

#End Region

#Region " Form's Events "

    Private Sub frmJVMainLedgerReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objMainLedger = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJVMainLedgerReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJVMainLedgerReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                    e.Handled = True
                Case Keys.R
                    If e.Control = True Then
                        Call frmJVLedgerReport_Report_Click(Nothing, Nothing)
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJVMainLedgerReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJVMainLedgerReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Message = objMainLedger._ReportDesc
            Me._Title = objMainLedger._ReportName

            Call FillCombo()
            'Sohail (07 Mar 2020) -- Start
            'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
            Call FillLoanScheme()
            'Sohail (07 Mar 2020) -- End
            Call ResetValue()

            'Sohail (25 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            If ConfigParameter._Object._AccountingSoftWare = enIntegration.SAP_JV Then
                Call FillList()
            End If
            'Sohail (25 Feb 2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJVMainLedgerReport_Load", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Buttons "

    Private Sub frmJVLedgerReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim dtPeriodStart As Date = Nothing
        Dim dtPeriodEnd As Date = Nothing
        'Sohail (21 Aug 2015) -- End
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If SetFilter() = False Then Exit Sub

            'objMainLedger.generateReport(0, e.Type, enExportAction.None)
            If SetFilter(dtPeriodStart, dtPeriodEnd) = False Then Exit Sub

            objMainLedger.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJVMainLedgerReport_Report_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmJVLedgerReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim dtPeriodStart As Date = Nothing
        Dim dtPeriodEnd As Date = Nothing
        'Sohail (21 Aug 2015) -- End
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If SetFilter() = False Then Exit Sub

            'objMainLedger.generateReport(0, enPrintAction.None, e.Type)
            If SetFilter(dtPeriodStart, dtPeriodEnd) = False Then Exit Sub

            objMainLedger.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJVMainLedgerReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJVLedgerReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJVMainLedgerReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
    Private Sub frmJVLedgerReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJVLedgerReport_Reset_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (02 Sep 2016) -- End

    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub frmJVLedgerReport_Language_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsJVLedgerReport.SetMessages()
            objfrm._Other_ModuleNames = "clsJVLedgerReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmJVLedgerReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub


    'Pinkal (03-Sep-2012) -- End

    'Sohail (01 Jun 2017) -- Start
    'Enhancement - 68.1 - Allocation filter on JV Report.
    Private Sub frmJVLedgerReport_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJVLedgerReport_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sohail (01 Jun 2017) -- End
#End Region

    'Sohail (02 Aug 2011) -- Start
#Region " CheckBox's Events "
    Private Sub chkShowSummaryBottom_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowSummaryBottom.CheckedChanged
        Try
            If chkShowSummaryBottom.Checked = True Then chkShowSummaryNewPage.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowSummaryBottom_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkShowSummaryNewPage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowSummaryNewPage.CheckedChanged
        Try
            If chkShowSummaryNewPage.Checked = True Then chkShowSummaryBottom.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowSummaryNewPage_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub chkShowGroupByCCGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowGroupByCCGroup.CheckedChanged
        Try
            If chkShowGroupByCCGroup.Checked = True Then
                cboCCGroup.Enabled = True
            Else
                cboCCGroup.Enabled = False
                cboCCGroup.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowGroupByCCGroup_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Mar 2012) -- End

    'Sohail (25 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllHeads(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (25 Feb 2013) -- End

    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
    Private Sub objchkSelectAllLoanScheme_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAllLoanScheme.CheckedChanged
        Try
            Call CheckAllScheme(objchkSelectAllLoanScheme.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAllLoanScheme_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (07 Mar 2020) -- End

#End Region
    'Sohail (02 Aug 2011) -- End

    'Sohail (25 Feb 2013) -- Start
    'TRA - ENHANCEMENT
#Region " Listview's Events "
    Private Sub lvTranHead_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvTranHead.ItemChecked
        Try
            If lvTranHead.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            ElseIf lvTranHead.CheckedItems.Count < lvTranHead.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            ElseIf lvTranHead.CheckedItems.Count = lvTranHead.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTranHead_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (25 Feb 2013) -- End

    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
#Region " GridView Events "

    Private Sub dgScheme_CurrentCellDirtyStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgScheme.CurrentCellDirtyStateChanged
        Try
            If dgScheme.IsCurrentCellDirty Then
                dgScheme.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgScheme_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgScheme_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgScheme.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgScheme_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgScheme_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgScheme.CellContentClick, dgScheme.CellContentDoubleClick
        Try

            If CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport OrElse CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanByproduct OrElse CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.SavingByproduct Then  'Loan Scheme With Bank Account Report, Loan Byproduct
                'Check the column index and if the check box is checked.
                If e.ColumnIndex = objdgcolhCheck.Index Then
                    Dim isChecked As Boolean = CType(Me.dgScheme(e.ColumnIndex, e.RowIndex).Value, Boolean)
                    If isChecked Then
                        'If check box is checked, uncheck all the rows, the current row would be checked later.
                        Dim intUnkId As Integer = CInt(dgScheme.Rows(e.RowIndex).Cells(objdgcolhunkid.Index).Value)
                        For Each row As DataGridViewRow In Me.dgScheme.Rows
                            If e.RowIndex <> dgScheme.Rows.IndexOf(row) Then
                                row.Cells(e.ColumnIndex).Value = False
                            End If
                        Next
                        
                    End If
                End If
            End If
            Dim mintCount As Integer = dvScheme.ToTable.Select("IsChecked=True").Length

            If mintCount <= 0 Then
                RemoveHandler objchkSelectAllLoanScheme.CheckedChanged, AddressOf objchkSelectAllLoanScheme_CheckedChanged
                objchkSelectAllLoanScheme.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAllLoanScheme.CheckedChanged, AddressOf objchkSelectAllLoanScheme_CheckedChanged
            ElseIf mintCount < dgScheme.Rows.Count Then
                RemoveHandler objchkSelectAllLoanScheme.CheckedChanged, AddressOf objchkSelectAllLoanScheme_CheckedChanged
                objchkSelectAllLoanScheme.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAllLoanScheme.CheckedChanged, AddressOf objchkSelectAllLoanScheme_CheckedChanged
            ElseIf mintCount = dgScheme.Rows.Count Then
                RemoveHandler objchkSelectAllLoanScheme.CheckedChanged, AddressOf objchkSelectAllLoanScheme_CheckedChanged
                objchkSelectAllLoanScheme.CheckState = CheckState.Checked
                AddHandler objchkSelectAllLoanScheme.CheckedChanged, AddressOf objchkSelectAllLoanScheme_CheckedChanged
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgScheme_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (07 Mar 2020) -- End

#Region " Controls "
    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objMainLedger.setOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objMainLedger.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (02 Aug 2011) -- Start
    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            'Sohail (02 May 2018) -- Start
            'TANAPA Issue #2231 : Issue on JV Report for Dynamic JV, Posting date is not visible in 72.1.
            lblPostingDate.Visible = False
            dtpPostingDate.Visible = False
            dtpPostingDate.Checked = False
            'Sohail (02 May 2018) -- End

            'Sohail (10 Aug 2020) -- Start
            'Freight Forwarders Kenya Enhancement : OLD-58 #  : New Accounting software Integration "Cargo Wise JV" for Freight Forwarders Kenya Ltd.
            lblCostCenter.Visible = False
            cboCostCenter.Visible = False
            'Sohail (10 Aug 2020) -- End

            'Sohail (11 May 2018) -- Start
            'CCK Enhancement - Ref # 235 : Salary Journal – Having employee details for debit entries in 72.1.
            chkIncludeEmpCodeNameOnDebit.Location = txtInvoiceRef.Location
            chkIncludeEmpCodeNameOnDebit.Visible = False
            'Sohail (11 May 2018) -- End

            'Sohail (26 Mar 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            lnkPayrollJournalReport.Visible = False
            lnkPayrollJournalExport.Visible = False
            If cboReportType.SelectedIndex = 3 Then
                lnkPayrollJournalReport.Visible = True
                lnkPayrollJournalExport.Visible = True
            End If
            'Sohail (26 Mar 2020) -- End

            If cboReportType.SelectedIndex = 0 OrElse cboReportType.SelectedIndex = 3 Then 'Main ledger, Combined JV
                chkShowSummaryBottom.Checked = False
                chkShowSummaryNewPage.Checked = False

                chkShowSummaryBottom.Visible = False
                chkShowSummaryNewPage.Visible = False
            Else
                chkShowSummaryBottom.Visible = True
                chkShowSummaryNewPage.Visible = True
            End If
            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (27 Jun 2014) -- Start
            'Enhancement - Enable Include Employer Contribution option for Staff, CC and Combined JV report for TANAPA. 
            'If cboReportType.SelectedIndex = 0 Then 'Main ledger
            '    'Anjan (20 Jul 2012)-Start
            '    'ENHANCEMENT : TRA COMMENTS on Rutta Request
            '    'chkIncludeEmployerContribution.Checked = false
            '    'chkIncludeEmployerContribution.Enabled = True
            '    chkIncludeEmployerContribution.Checked = True
                'chkIncludeEmployerContribution.Enabled = True
            '    'Anjan (20 Jul 2012)-End 
            'Else
                
            '    chkIncludeEmployerContribution.Checked = True
            '    chkIncludeEmployerContribution.Enabled = False
            'End If
                chkIncludeEmployerContribution.Checked = True
            'Sohail (27 Jun 2014) -- End
            'Sohail (16 Jul 2012) -- End

            'Sohail (07 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.Sun_Account Then
                lnkSunJVExport.Visible = True
            Else
                lnkSunJVExport.Visible = False
            End If
            'Sohail (07 Aug 2012) -- End

            'Sohail (17 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.iScala Then
                lnkiScalaJVExport.Visible = True
                'Sohail (25 Feb 2013) -- Start
                'TRA - ENHANCEMENT
                lnkiScalaJVExport.Location = lnkSunJVExport.Location
                lnkiScalaJVExport.Size = lnkSunJVExport.Size
                'Sohail (25 Feb 2013) -- End
            Else
                lnkiScalaJVExport.Visible = False
            End If
            'Sohail (17 Jan 2013) -- End

            'Sohail (25 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.SAP_JV Then
                lnkTBCJVExport.Visible = True
                lnkTBCJVExport.Location = lnkSunJVExport.Location
                lnkTBCJVExport.Size = lnkSunJVExport.Size

                gbFilterTBCJV.Visible = True
            Else
                lnkTBCJVExport.Visible = False

                gbFilterTBCJV.Visible = False
            End If
            'Sohail (25 Feb 2013) -- End

            'Sohail (19 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.Sun_Account_5 Then
                lnkSunJV5Export.Visible = True
                lnkSunJV5Export.Location = lnkSunJVExport.Location
                lnkSunJV5Export.Size = lnkSunJVExport.Size
            Else
                lnkSunJV5Export.Visible = False
            End If
            'Sohail (19 Mar 2013) -- End

            'Sohail (03 Mar 2014) -- Start
            'Enhancement - iScala2 JV for Aga Khan
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.iScala_2 Then
                lnkiScala2JVExport.Visible = True
                lnkiScala2JVExport.Location = lnkSunJVExport.Location
                lnkiScala2JVExport.Size = lnkSunJVExport.Size

                lblCustomCCenter.Visible = True
                cboCustomCCenter.Visible = True
                lblInvoiceRef.Visible = True
                txtInvoiceRef.Visible = True
                chkShowColumnHeader.Visible = True
            Else
                lnkiScala2JVExport.Visible = False

                lblCustomCCenter.Visible = False
                cboCustomCCenter.Visible = False
                cboCustomCCenter.SelectedValue = 0
                lblInvoiceRef.Visible = False
                txtInvoiceRef.Visible = False
                txtInvoiceRef.Text = ""
                chkShowColumnHeader.Visible = False
                chkShowColumnHeader.Checked = False
            End If
            'Sohail (03 Mar 2014) -- End

            'Sohail (06 Dec 2014) -- Start
            'Enhancement - New JV integration with XERO accounting.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.XERO Then
                lnkXEROJVExport.Visible = True
                lnkXEROJVExport.Location = lnkSunJVExport.Location
                lnkXEROJVExport.Size = lnkSunJVExport.Size
            Else
                lnkXEROJVExport.Visible = False
            End If
            'Sohail (06 Dec 2014) -- End

            'Sohail (19 Feb 2016) -- Start
            'Enhancement - New JV Integration with NetSuite ERP in 57.2 and 58.1 SP.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.NETSUITE_ERP Then
                lnkNetSuiteERPJVExport.Visible = True
                lnkNetSuiteERPJVExport.Location = lnkSunJVExport.Location
                lnkNetSuiteERPJVExport.Size = lnkSunJVExport.Size
            Else
                lnkNetSuiteERPJVExport.Visible = False
            End If
            'Sohail (19 Feb 2016) -- End

            'Sohail (06 Aug 2016) -- Start
            'Enhancement - 63.1 - New JV Integration with Flex Cube Retail.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.FLEX_CUBE_RETAIL Then
                lnkFlexCubeRetailJVExport.Visible = True
                lnkFlexCubeRetailJVExport.Location = lnkSunJVExport.Location
                lnkFlexCubeRetailJVExport.Size = lnkSunJVExport.Size
            Else
                lnkFlexCubeRetailJVExport.Visible = False
            End If
            'Sohail (06 Aug 2016) -- End

            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.SUN_ACCOUNT_PROJECT_JV Then
                lnkSunAccountProjectJVExport.Visible = True
                lnkSunAccountProjectJVExport.Location = lnkSunJVExport.Location
                lnkSunAccountProjectJVExport.Size = lnkSunJVExport.Size

                lblTranHead.Location = lblInvoiceRef.Location
                cboTranHead.Location = txtInvoiceRef.Location
                lblTranHead.Size = lblInvoiceRef.Size
                cboTranHead.Size = txtInvoiceRef.Size
                lblTranHead.Visible = True
                cboTranHead.Visible = True
            Else
                lnkSunAccountProjectJVExport.Visible = False
                lblTranHead.Visible = False
                cboTranHead.Visible = False
            End If
            'Sohail (02 Sep 2016) -- End

            'Sohail (08 Jul 2017) -- Start
            'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.DYNAMICS_NAV Then
                lnkDynamicsNavJVExport.Visible = True
                lnkDynamicsNavJVExport.Location = lnkSunJVExport.Location
                lnkDynamicsNavJVExport.Size = lnkSunJVExport.Size
                chkShowColumnHeader.Visible = True

                lblCostCenter.Location = lblInvoiceRef.Location
                cboCostCenter.Location = txtInvoiceRef.Location
                lblCostCenter.Size = lblInvoiceRef.Size
                cboCostCenter.Size = txtInvoiceRef.Size
                lblCostCenter.Visible = True
                cboCostCenter.Visible = True
                lblPostingDate.Visible = True
                dtpPostingDate.Visible = True
            Else
                lnkDynamicsNavJVExport.Visible = False
                chkShowColumnHeader.Visible = False
                chkShowColumnHeader.Checked = False
                'Sohail (02 May 2018) -- Start
                'TANAPA Issue #2231 : Issue on JV Report for Dynamic JV, Posting date is not visible in 72.1.
                'lblPostingDate.Visible = False
                'dtpPostingDate.Visible = False
                'dtpPostingDate.Checked = False
                'Sohail (02 May 2018) -- End

                lblCostCenter.Visible = False
                cboCostCenter.Visible = False
            End If
            'Sohail (08 Jul 2017) -- End

            'Sohail (17 Feb 2018) -- Start
            'AMANA Bank Enhancement : Ref. No. 172 - UPLOAD FILE FROM ARUTI TO FLEX CUBE - (RefNo: 171) in 70.1.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.FLEX_CUBE_UPLD) Then
                lnkFlexCubeUPLDJVExport.Visible = True
                lnkFlexCubeUPLDJVExport.Location = lnkSunJVExport.Location
                lnkFlexCubeUPLDJVExport.Size = lnkSunJVExport.Size

                lblPostingDate.Visible = True
                dtpPostingDate.Visible = True
            Else
                lnkFlexCubeUPLDJVExport.Visible = False

                'Sohail (02 May 2018) -- Start
                'TANAPA Issue #2231 : Issue on JV Report for Dynamic JV, Posting date is not visible in 72.1.
                'lblPostingDate.Visible = False
                'dtpPostingDate.Visible = False
                'dtpPostingDate.Checked = False
                'Sohail (02 May 2018) -- End
            End If
            'Sohail (17 Feb 2018) -- End

            'Sohail (21 Feb 2018) -- Start
            'CCK Enhancement : Ref. No. 174 - SAP Salary Journal - (RefNo: 174) in 70.1.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.SAP_JV_BS_ONE) Then
                lnkSAPJVBSOneExport.Visible = True
                lnkSAPJVBSOneExport.Location = lnkSunJVExport.Location
                lnkSAPJVBSOneExport.Size = lnkSunJVExport.Size

                'Sohail (11 May 2018) -- Start
                'CCK Enhancement - Ref # 235 : Salary Journal – Having employee details for debit entries in 72.1.
                chkIncludeEmpCodeNameOnDebit.Visible = True
                'Sohail (11 May 2018) -- End

            Else
                lnkSAPJVBSOneExport.Visible = False
            End If
            'Sohail (21 Feb 2018) -- End

            'Sohail (26 Nov 2018) -- Start
            'NMB Enhancement - Flex Cube JV Integration in 75.1.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.FLEX_CUBE_JV) Then
                lnkFlexCubeJVExport.Visible = True
                lnkFlexCubeJVExport.Location = lnkSunJVExport.Location
                lnkFlexCubeJVExport.Size = lnkSunJVExport.Size
                'Sohail (10 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Provide 2 different options for Flex Cube NMB JV to Export to CSV and Post to Oracle.
                lnkFlexCubeJVPostOracle.Visible = True
                'Sohail (07 Mar 2020) -- Start
                'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
                'lnkFlexCubeJVPostOracle.Location = gbFilterTBCJV.Location
                lnkFlexCubeJVPostOracle.Location = lnkFlexCubeJVExport.Location
                lnkFlexCubeJVPostOracle.Location = New Drawing.Point(lnkFlexCubeJVPostOracle.Location.X, lnkFlexCubeJVPostOracle.Location.Y + 20)
                'Sohail (07 Mar 2020) -- End
                lnkFlexCubeJVPostOracle.Size = lnkSunJVExport.Size
                'Sohail (10 Jan 2019) -- End
                'Sohail (24 Jun 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
                lnkShowJVBatchPosting.Visible = True
                'Sohail (07 Mar 2020) -- Start
                'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
                'lnkShowJVBatchPosting.Location = gbFilterTBCJV.Location
                'lnkShowJVBatchPosting.Location = New Drawing.Point(lnkShowJVBatchPosting.Location.X, lnkShowJVBatchPosting.Location.Y + 20)
                lnkShowJVBatchPosting.Location = lnkFlexCubeJVPostOracle.Location
                lnkShowJVBatchPosting.Location = New Drawing.Point(lnkShowJVBatchPosting.Location.X, lnkShowJVBatchPosting.Location.Y + 20)
                'Sohail (07 Mar 2020) -- End
                lnkShowJVBatchPosting.Size = lnkSunJVExport.Size
                'Sohail (24 Jun 2019) -- End
                'Sohail (07 Mar 2020) -- Start
                'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
                lnkFlexCubeLoanBatchExport.Visible = True
                lnkFlexCubeLoanBatchExport.Location = lnkShowJVBatchPosting.Location
                lnkFlexCubeLoanBatchExport.Location = New Drawing.Point(lnkFlexCubeLoanBatchExport.Location.X, lnkFlexCubeLoanBatchExport.Location.Y + 20)
                lnkFlexCubeLoanBatchExport.Size = lnkSunJVExport.Size
                pnlLoanSchemeList.Location = gbFilterTBCJV.Location
                pnlLoanSchemeList.Visible = True
                'Sohail (07 Mar 2020) -- End
                'Sohail (16 Oct 2019) -- Start
                'NMB Enhancement # : Add privileges for "Allow To Post Flexcube JV To Oracle" to prevent erroneous postings by other users to Payroll GL accounts.
                lnkFlexCubeJVPostOracle.Enabled = User._Object.Privilege._AllowToPostFlexcubeJVToOracle
                'Sohail (16 Oct 2019) -- End
                'Sohail (02 May 2020) -- Start
                'NMB Enhancement #: Posting date option to post VALUE_DATE in Flex cube JV.
                lblPostingDate.Visible = True
                dtpPostingDate.Visible = True
                'Sohail (02 May 2020) -- End
            Else
                lnkFlexCubeJVExport.Visible = False
                'Sohail (10 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Provide 2 different options for Flex Cube NMB JV to Export to CSV and Post to Oracle.
                lnkFlexCubeJVPostOracle.Visible = False
                'Sohail (10 Jan 2019) -- End
                'Sohail (24 Jun 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
                lnkShowJVBatchPosting.Visible = False
                'Sohail (24 Jun 2019) -- End
                'Sohail (07 Mar 2020) -- Start
                'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
                lnkFlexCubeLoanBatchExport.Visible = False
                pnlLoanSchemeList.Visible = False
                'Sohail (07 Mar 2020) -- End
            End If
            'Sohail (26 Nov 2018) -- End

            'Sohail (02 Mar 2019) -- Start
            'Mkombozi Bank Enhancement - Ref # 0002673 - 76.1 - BR-CBS JV Accounting integration to export data to CBS SQL database in xml format.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.BR_JV) Then
                lnkBRJVExport.Visible = True
                lnkBRJVExport.Location = lnkSunJVExport.Location
                lnkBRJVExport.Size = lnkSunJVExport.Size
                lnkBRJVPostSQL.Visible = True
                'Sohail (07 Mar 2020) -- Start
                'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
                'lnkBRJVPostSQL.Location = gbFilterTBCJV.Location
                lnkBRJVPostSQL.Location = lnkBRJVExport.Location
                lnkBRJVPostSQL.Location = New Drawing.Point(lnkBRJVPostSQL.Location.X, lnkBRJVPostSQL.Location.Y + 20)
                'Sohail (07 Mar 2020) -- End
                lnkBRJVPostSQL.Size = lnkSunJVExport.Size
            Else
                lnkBRJVExport.Visible = False
                lnkBRJVPostSQL.Visible = False
            End If
            'Sohail (02 Mar 2019) -- End

            'Hemant (15 Mar 2019) -- Start
            'MELIA ARUSHA ENHANCEMENT - Ref # 3593 - 76.1 : SAP ECC 6.0 JV integration.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.SAP_ECC_6_0_JV) Then
                lnkSAPECC6_0JVExport.Visible = True
                lnkSAPECC6_0JVExport.Location = lnkSunJVExport.Location
                lnkSAPECC6_0JVExport.Size = lnkSunJVExport.Size
            Else
                lnkSAPECC6_0JVExport.Visible = False
            End If
            'Hemant (15 Mar 2019) -- End

            'Hemant (18 Apr 2019) -- Start
            'MWAUWASA ENHANCEMENT - Ref # 3747 - 76.1 : SAGE 300 JV integration.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.SAGE_300_JV) Then
                lnkSAGE300JVExport.Visible = True
                lnkSAGE300JVExport.Location = lnkSunJVExport.Location
                lnkSAGE300JVExport.Size = lnkSunJVExport.Size
            Else
                lnkSAGE300JVExport.Visible = False
            End If
            'Hemant (18 Apr 2019) -- End

            'Hemant (29 May 2019) -- Start
            'MUWASA ENHANCEMENT - Ref # 3329 - 76.1 : PASTEL V2 COMBINED JV INTEGRATION.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.PASTEL_V2_JV) Then
                lnkPASTELV2JVExport.Visible = True
                lnkPASTELV2JVExport.Location = lnkSunJVExport.Location
                lnkPASTELV2JVExport.Size = lnkSunJVExport.Size
                lblPostingDate.Visible = True
                dtpPostingDate.Visible = True
            Else
                lnkPASTELV2JVExport.Visible = False
            End If
            'Hemant (29 May 2019) -- End

            'Sohail (08 Apr 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.SAGE_300_ERP_JV) Then
                lnkSAGE300ERPJVExport.Visible = True
                lnkSAGE300ERPJVExport.Location = lnkSunJVExport.Location
                lnkSAGE300ERPJVExport.Size = lnkSunJVExport.Size

                'Gajanan [15-April-2020] -- Start
                lnkSAGE300ERPImport.Visible = True
                lnkSAGE300ERPImport.Location = New Drawing.Point(lnkSAGE300ERPJVExport.Location.X, lnkSAGE300ERPJVExport.Location.Y + 20)
                lnkSAGE300ERPImport.Size = lnkSAGE300ERPJVExport.Size
                'Gajanan [15-April-2020] -- End
                'Sohail (02 May 2020) -- Start
                'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
                lnkSAGE300ERPImport.Width = lnkSAGE300ERPJVExport.Width + 50
                'Sohail (02 May 2020) -- End
            Else
                lnkSAGE300ERPJVExport.Visible = False
                'Gajanan [15-April-2020] -- Start
                lnkSAGE300ERPImport.Visible = False
                'Gajanan [15-April-2020] -- End

            End If
            'Sohail (08 Apr 2020) -- End

            'Sohail (22 Jun 2020) -- Start
            'CORAL BEACH CLUB enhancement : 0004743 : New JV Integration Coral Sun JV.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.CORAL_SUN_JV) Then
                lnkCoralSunJVExport.Visible = True
                lnkCoralSunJVExport.Location = lnkSunJVExport.Location
                lnkCoralSunJVExport.Size = lnkSunJVExport.Size
            Else
                lnkCoralSunJVExport.Visible = False
            End If
            'Sohail (22 Jun 2020) -- End

            'Hemant (13 Jul 2020) -- Start
            'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.QuickBook) Then
                lblPostingDate.Visible = True
                dtpPostingDate.Visible = True
            End If
            'Hemant (13 Jul 2020) -- End

            'Sohail (10 Aug 2020) -- Start
            'Freight Forwarders Kenya Enhancement : OLD-58 #  : New Accounting software Integration "Cargo Wise JV" for Freight Forwarders Kenya Ltd.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.CARGO_WISE_JV) Then
                lnkCargoWiseJVExport.Visible = True
                lnkCargoWiseJVExport.Location = lnkSunJVExport.Location
                lnkCargoWiseJVExport.Size = lnkSunJVExport.Size

                lblCostCenter.Location = lblInvoiceRef.Location
                cboCostCenter.Location = txtInvoiceRef.Location
                lblCostCenter.Size = lblInvoiceRef.Size
                cboCostCenter.Size = txtInvoiceRef.Size
                lblCostCenter.Visible = True
                cboCostCenter.Visible = True
            Else
                lnkCargoWiseJVExport.Visible = False
            End If
            'Sohail (10 Aug 2020) -- End

            'Sohail (04 Sep 2020) -- Start
            'Benchmark Enhancement : OLD-73 #  : New Accounting software Integration "SAP Standard JV".
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.SAP_STANDARD_JV) Then
                lnkSAPStandardJVExport.Visible = True
                lnkSAPStandardJVExport.Location = lnkSunJVExport.Location
                lnkSAPStandardJVExport.Size = lnkSunJVExport.Size
            Else
                lnkSAPStandardJVExport.Visible = False
            End If
            'Sohail (04 Sep 2020) -- End

            'Sohail (10 Sep 2020) -- Start
            'Oxygen Communication Ltd Enhancement : OLD-68 #  : New Accounting software Integration "SAGE Evolution JV".
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.SAGE_EVOLUTION_JV) Then
                lnkSAGEEvolutionJVExport.Visible = True
                lnkSAGEEvolutionJVExport.Location = lnkSunJVExport.Location
                lnkSAGEEvolutionJVExport.Size = lnkSunJVExport.Size

                lblCostCenter.Location = lblInvoiceRef.Location
                cboCostCenter.Location = txtInvoiceRef.Location
                lblCostCenter.Size = lblInvoiceRef.Size
                cboCostCenter.Size = txtInvoiceRef.Size
                lblCostCenter.Visible = True
                cboCostCenter.Visible = True
            Else
                lnkSAGEEvolutionJVExport.Visible = False
            End If
            'Sohail (10 Sep 2020) -- End

            'Sohail (16 Nov 2021) -- Start
            'Enhancement : OLD-492 : New Payroll JV Integration - Hakika Bank JV.
            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.HAKIKA_BANK_JV) Then
                lnkHakikaBankJVExport.Visible = True
                lnkHakikaBankJVExport.Location = lnkSunJVExport.Location
                lnkHakikaBankJVExport.Size = lnkSunJVExport.Size
            Else
                lnkHakikaBankJVExport.Visible = False
            End If
            'Sohail (16 Nov 2021) -- End

            'Sohail (27 May 2014) -- Start
            'Enhancement - Consolidate heads amount on Combined JV Report if mapped to one account. 
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objMainLedger.OrderByDisplay
            'Sohail (27 May 2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (02 Aug 2011) -- End

    'Sohail (07 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub lnkSunJVExport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSunJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try

                'Sohail (04 Dec 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
                'Sohail (04 Dec 2012) -- End

            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "Text File|*.txt"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.Text

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked 'Sohail (25 Feb 2013)

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (21 Aug 2015)

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            objMainLedger._PeriodCode = objPeriod._Period_Code
            'Sohail (18 Apr 2016) -- End

            'Nilay (16-Apr-2016) -- Start
            'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode
            'Nilay (16-Apr-2016) -- End

            'Sohail (04 Dec 2012) -- Start
            'TRA - ENHANCEMENT
            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign
            'Sohail (04 Dec 2012) -- End

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objMainLedger.Sun_JV_Export_CombinedJVReport(SaveDialog.FileName) = True Then
            If objMainLedger.Sun_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, ConfigParameter._Object._Accounting_TransactionReference, ConfigParameter._Object._Accounting_JournalType, ConfigParameter._Object._Accounting_JVGroupCode) = True Then
                'Sohail (21 Aug 2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing 'Sohail (21 Aug 2015)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSunJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (07 Aug 2012) -- End

    'Sohail (17 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub lnkiScalaJVExport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkiScalaJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "Excel File|*.xls"
                'SaveDialog.FilterIndex = 0
                'SaveDialog.ShowDialog()
                'If SaveDialog.FileName = "" Then
                '    Exit Try
                'End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = "iScala"

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked 'Sohail (25 Feb 2013)

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (21 Aug 2015)

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            objMainLedger._PeriodCode = objPeriod._Period_Code
            'Sohail (18 Apr 2016) -- End

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objMainLedger.iScala_JV_Export_CombinedJVReport(SaveDialog.FileName) = True Then
            If objMainLedger.iScala_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, ConfigParameter._Object._OpenAfterExport) = True Then
                'Sohail (21 Aug 2015) -- End
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing 'Sohail (21 Aug 2015)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkiScalaJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 Jan 2013) -- End

    'Sohail (03 Mar 2014) -- Start
    'Enhancement - iScala2 JV for Aga Khan
    Private Sub lnkiScala2JVExport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkiScala2JVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            ElseIf CInt(cboCustomCCenter.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please Select Custom Cost Center."), enMsgBoxStyle.Information)
                cboCustomCCenter.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "Excel File|*.xls"
                'SaveDialog.FilterIndex = 0
                'SaveDialog.ShowDialog()
                'If SaveDialog.FileName = "" Then
                '    Exit Try
                'End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = "iScala2"

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            objMainLedger._CustomCCetnterId = CInt(cboCustomCCenter.SelectedValue)
            objMainLedger._CustomCCetnterName = cboCustomCCenter.Text
            objMainLedger._InvoiceReference = txtInvoiceRef.Text.Trim
            objMainLedger._ShowColumnHeader = chkShowColumnHeader.Checked

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (21 Aug 2015)

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            objMainLedger._PeriodCode = objPeriod._Period_Code
            'Sohail (18 Apr 2016) -- End

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objMainLedger.iScala2_JV_Export_CombinedJVReport(SaveDialog.FileName) = True Then
            If objMainLedger.iScala2_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, ConfigParameter._Object._OpenAfterExport) = True Then
                'Sohail (21 Aug 2015) -- End
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing 'Sohail (21 Aug 2015)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkiScala2JVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 Mar 2014) -- End

    'Sohail (25 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub lnkTBCJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkTBCJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "CSV File|*.csv"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.Text

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim strIDs As String = ""
            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'For Each lvItem As ListViewItem In lvTranHead.Items
            '    If lvItem.Checked = False AndAlso lvTranHead.CheckedItems.Count > 0 Then
            '        If strIDs.Trim = "" Then
            '            strIDs = lvItem.Tag.ToString & "," & lvItem.SubItems(colhEContribHead.Index).Tag.ToString
            '        Else
            '            strIDs &= "," & lvItem.Tag.ToString & "," & lvItem.SubItems(colhEContribHead.Index).Tag.ToString
            '        End If
            '    End If
            'Next
            If lvTranHead.CheckedItems.Count > 0 Then
                strIDs = String.Join(",", (From lv In lvTranHead.Items.Cast(Of ListViewItem)() Where (lv.Checked = False) Select (lv.SubItems(colhEContribHead.Index).Tag.ToString)).ToArray)
            End If
            'Sohail (18 Apr 2016) -- End
            objMainLedger._EmpContributionHeadUnCheckedIDs = strIDs

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (21 Aug 2015)

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            objMainLedger._PeriodCode = objPeriod._Period_Code
            'Sohail (18 Apr 2016) -- End

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objMainLedger.TBC_JV_Export_CombinedJVReport(SaveDialog.FileName) = True Then
            If objMainLedger.TBC_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, ConfigParameter._Object._CurrentDateAndTime, SaveDialog.FileName) = True Then
                'Sohail (21 Aug 2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing 'Sohail (21 Aug 2015)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkTBCJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (25 Feb 2013) -- End

    'Sohail (19 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub lnkSunJV5Export_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSunJV5Export.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "Excel File|*.xls"
                'SaveDialog.FilterIndex = 0
                'SaveDialog.ShowDialog()
                'If SaveDialog.FileName = "" Then
                '    Exit Try
                'End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = "Sun_JV_5"

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (21 Aug 2015)

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            objMainLedger._PeriodCode = objPeriod._Period_Code
            'Sohail (18 Apr 2016) -- End

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objMainLedger.Sun5_JV_Export_CombinedJVReport(SaveDialog.FileName) = True Then
            If objMainLedger.Sun5_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, ConfigParameter._Object._Accounting_TransactionReference, SaveDialog.FileName, ConfigParameter._Object._OpenAfterExport) = True Then
                'Sohail (21 Aug 2015) -- End
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing 'Sohail (21 Aug 2015)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSunJV5Export_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (19 Mar 2013) -- End

    'Sohail (06 Dec 2014) -- Start
    'Enhancement - New JV integration with XERO accounting.
    Private Sub lnkXEROJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkXEROJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "CSV File|*.csv"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = "iScala"

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (21 Aug 2015)

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            objMainLedger._PeriodCode = objPeriod._Period_Code
            'Sohail (18 Apr 2016) -- End

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objMainLedger.XERO_JV_Export_CombinedJVReport(SaveDialog.FileName) = True Then
            If objMainLedger.XERO_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, ConfigParameter._Object._Accounting_TransactionReference, ConfigParameter._Object._Accounting_JournalType) = True Then
                'Sohail (21 Aug 2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing 'Sohail (21 Aug 2015)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkXEROJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Dec 2014) -- End

    'Sohail (19 Feb 2016) -- Start
    'Enhancement - New JV Integration with NetSuite ERP in 57.2 and 58.1 SP.
    Private Sub lnkNetSuiteERPJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkNetSuiteERPJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "CSV File|*.csv"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            objMainLedger._PeriodCode = objPeriod._Period_Code
            'Sohail (18 Apr 2016) -- End

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            If objMainLedger.NETSUITE_ERP_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkNetSuiteERPJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (19 Feb 2016) -- End

    'Sohail (06 Aug 2016) -- Start
    'Enhancement - 63.1 - New JV Integration with Flex Cube Retail.
    Private Sub lnkFlexCubeRetailJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkFlexCubeRetailJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "Text File|*.txt"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            If objMainLedger.FlexCubeRetail_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, "", "", "") = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkFlexCubeRetailJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Aug 2016) -- End

    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
    Private Sub lnkSunAccountProjectJVExport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSunAccountProjectJVExport.LinkClicked
        Dim objBudget As New clsBudget_MasterNew
        Dim dsList As DataSet
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            ElseIf CInt(cboTranHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please Select Transaction head. Transaction head is mandatory information."), enMsgBoxStyle.Information)
                cboTranHead.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "Excel File|*.xls"
                'SaveDialog.FilterIndex = 0
                'SaveDialog.ShowDialog()
                'If SaveDialog.FileName = "" Then
                '    Exit Try
                'End If
            End If


            dsList = objBudget.GetComboList("Budget", False, True)
            If dsList.Tables("Budget").Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, There is no default budget set."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = "Sun_JV_5"

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            If objMainLedger.SunAccountProject_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, ConfigParameter._Object._OpenAfterExport, CInt(dsList.Tables("Budget").Rows(0).Item("budgetunkid")), CInt(cboTranHead.SelectedValue)) = True Then
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSunAccountProjectJVExport_LinkClicked", mstrModuleName)
        Finally
            objBudget = Nothing
        End Try
    End Sub
    'Sohail (02 Sep 2016) -- End

    'Sohail (08 Jul 2017) -- Start
    'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
    Private Sub lnkDynamicsNavJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkDynamicsNavJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            ElseIf CInt(cboCostCenter.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please Select Cost Center. Cost Center is mandatory information."), enMsgBoxStyle.Information)
                cboCostCenter.Focus()
                Exit Try
            ElseIf dtpPostingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please set Posting Date. Posting Date is mandatory information."), enMsgBoxStyle.Information)
                dtpPostingDate.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "CSV File|*.csv"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            objMainLedger._ShowColumnHeader = chkShowColumnHeader.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            If objMainLedger.Dynamics_Nav_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, CInt(cboCostCenter.SelectedValue)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkDynamicsNavJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (08 Jul 2017) -- End

    'Sohail (17 Feb 2018) -- Start
    'AMANA Bank Enhancement : Ref. No. 172 - UPLOAD FILE FROM ARUTI TO FLEX CUBE - (RefNo: 171) in 70.1.
    Private Sub lnkFlexCubeUPLDJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkFlexCubeUPLDJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            ElseIf dtpPostingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please set Posting Date. Posting Date is mandatory information."), enMsgBoxStyle.Information)
                dtpPostingDate.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "Text File|*.txt"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            If objMainLedger.FlexCubeUPLD_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, "", "", "") = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkFlexCubeUPLDJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 Feb 2018) -- End

    'Sohail (21 Feb 2018) -- Start
    'CCK Enhancement : Ref. No. 174 - SAP Salary Journal - (RefNo: 174) in 70.1.
    Private Sub lnkSAPJVBSOneExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSAPJVBSOneExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "Text File|*.txt"
                'SaveDialog.FilterIndex = 0
                'SaveDialog.ShowDialog()
                'If SaveDialog.FileName = "" Then
                '    Exit Try
                'End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            'Sohail (11 May 2018) -- Start
            'CCK Enhancement - Ref # 235 : Salary Journal – Having employee details for debit entries in 72.1.
            objMainLedger._IncludeEmpCodeNameOnDebit = chkIncludeEmpCodeNameOnDebit.Checked
            'Sohail (11 May 2018) -- End

            'Sohail (23 Jun 2021) -- Start
            'NMB Iuuse : : Flex cube JV not balancing due to fmt currency.
            objMainLedger._FromEmpID = CInt(txtFromEmpID.Decimal)
            objMainLedger._ToEmpID = CInt(txtToEmpID.Decimal)
            'Sohail (23 Jun 2021) -- End

            If objMainLedger.SAP_JV_BS_One_Report(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, ConfigParameter._Object._OpenAfterExport) = True Then
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSAPJVBSOneExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Feb 2018) -- End

    'Sohail (26 Nov 2018) -- Start
    'NMB Enhancement - Flex Cube JV Integration in 75.1.
    Private Sub lnkFlexCubeJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkFlexCubeJVExport.LinkClicked, lnkFlexCubeJVPostOracle.LinkClicked
        'Sohail (10 Jan 2019) - [lnkFlexCubeJVPostOracle.LinkClicked]
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
                'Sohail (02 May 2020) -- Start
                'NMB Enhancement #: Posting date option to post VALUE_DATE in Flex cube JV.
            ElseIf dtpPostingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please set Posting Date. Posting Date is mandatory information."), enMsgBoxStyle.Information)
                dtpPostingDate.Focus()
                Exit Try
                'Sohail (02 May 2020) -- End
                'Sohail (13 Dec 2018) -- Start
                'NMB Enhancement - Exporting Flex Cube NMB JV to CSV in 75.1.
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "CSV File|*.csv"
                SaveDialog.FilterIndex = 0
                'Sohail (10 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Provide 2 different options for Flex Cube NMB JV to Export to CSV and Post to Oracle.
                'SaveDialog.ShowDialog()
                'If SaveDialog.FileName = "" Then
                '    Exit Try
                'End If
                If CType(sender, LinkLabel).Name = lnkFlexCubeJVExport.Name Then
                    SaveDialog.ShowDialog()
                    If SaveDialog.FileName = "" Then
                        Exit Try
                    End If
                End If
                'Sohail (10 Jan 2019) -- End
                'Sohail (13 Dec 2018) -- End
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            'Sohail (23 Jun 2021) -- Start
            'NMB Iuuse : : Flex cube JV not balancing due to fmt currency.
            objMainLedger._FromEmpID = CInt(txtFromEmpID.Decimal)
            objMainLedger._ToEmpID = CInt(txtToEmpID.Decimal)
            'Sohail (23 Jun 2021) -- End

            'Sohail (24 Jun 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
            'Sohail (07 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Allow exporting data if posting is already done in FLEX CUBE JV.
            'If objPeriod._FlexCube_BatchNo.Trim <> "" Then
            'If objPeriod._FlexCube_BatchNo.Trim <> "" AndAlso CType(sender, LinkLabel).Name = lnkFlexCubeJVPostOracle.Name Then
            '    'Sohail (07 Feb 2019) -- End
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Posting is already done for selected period with batch no") & " " & objPeriod._FlexCube_BatchNo & ".", enMsgBoxStyle.Information)
            '    Exit Try
            'End If
            If CType(sender, LinkLabel).Name = lnkFlexCubeJVPostOracle.Name Then
                Dim objPayroll As New clsPayrollProcessTran
                Dim objTnALeave As New clsTnALeaveTran
                Dim objPayment As New clsPayment_tran
                Dim objPayAuthorize As New clsPayment_authorize_tran
                Dim objJVPosting As New clsJVPosting_Tran
                Dim dsList As DataSet
                Dim intTotalPaymentCount As Integer = 0
                Dim intTotalAuthorizedtCount As Integer = 0
                Dim strAdvanceFilter As String = ""
                Dim strBatchNo As String = ""
                'Sohail (29 Aug 2019) -- Start
                'NMB Payroll UAT # TC008 - 76.1 - System should be able to restrict batch posting when it is holiday unless it is activated by user to allow batch posting on holidays.
                If User._Object.Privilege._AllowToPostJVOnHolidays = False Then
                    Dim objHoliday As New clsholiday_master
                    If objHoliday.isHoliday(ConfigParameter._Object._CurrentDateAndTime) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, You cannot post JV not holiday."), enMsgBoxStyle.Information)
                        Exit Try
                    End If
                End If
                'Sohail (29 Aug 2019) -- End
                'Sohail (10 Apr 2020) -- Start
                'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
                'If CInt(cboBranch.SelectedValue) > 0 Then
                '    dsList = objPayroll.Get_UnProcessed_Employee(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "Employee", CInt(cboPeriod.SelectedValue), True, False, " ADF.stationunkid <= 0 ")
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        Dim objValidation As New frmCommonValidationList
                '        dsList.Tables(0).Columns.Remove("employeeunkid")
                '        dsList.Tables(0).Columns("employeename").SetOrdinal(0)
                '        objValidation.displayDialog(False, Language.getMessage(mstrModuleName, 20, "Sorry, You cannot do branch wise posting. Reason: Branch is not assigned to some of the employees."), dsList.Tables(0))
                '        Exit Try
                '    End If

                '    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, , " AND stationunkid <= 0 ") = True Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, You cannot do branch wise posting. Reason: Non-Branch wise posting is already done for selected period."), enMsgBoxStyle.Information)
                '        Exit Try
                '    End If

                '    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), CInt(cboBranch.SelectedValue), str_BatchNo:=strBatchNo) = True Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, Posting is already done for selected branch and selected period with batch no") & " " & strBatchNo & ".", enMsgBoxStyle.Information)
                '        Exit Try
                '    End If
                'Else
                '    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, , " AND stationunkid > 0 ") = True Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, You cannot do posting without selecting any branch. Reason: Branch wise posting is already done for some branches for selected period."), enMsgBoxStyle.Information)
                '        Exit Try
                '    End If

                '    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, str_BatchNo:=strBatchNo) = True Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Posting is already done for selected period with batch no") & " " & strBatchNo & ".", enMsgBoxStyle.Information)
                '        Exit Try
                '    End If
                'End If
                'If CInt(cboBranch.SelectedValue) > 0 Then
                '    strAdvanceFilter = " ADF.stationunkid = " & CInt(cboBranch.SelectedValue) & " "
                'End If
                'dsList = objPayroll.Get_UnProcessed_Employee(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "Employee", CInt(cboPeriod.SelectedValue), True, False, strAdvanceFilter)
                'If dsList.Tables(0).Rows.Count > 0 Then
                '    Dim objValidation As New frmCommonValidationList
                '    dsList.Tables(0).Columns.Remove("employeeunkid")
                '    dsList.Tables(0).Columns("employeename").SetOrdinal(0)
                '    objValidation.displayDialog(False, Language.getMessage(mstrModuleName, 24, "Sorry, Process payroll is not done for the last date of the selected period for some of the employees."), dsList.Tables(0))
                '    Exit Try
                'End If
                'dsList = objTnALeave.Get_Balance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, "", "Balance", , CInt(cboPeriod.SelectedValue), , strAdvanceFilter)
                'If dsList.Tables("Balance").Select("balanceamount > 0").Length > 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, Some payslip's are still unpaid in this period. Please make payment for unpaid payslip."), enMsgBoxStyle.Information)
                '    Exit Try
                'End If

                'intTotalPaymentCount = objPayment.GetTotalPaymentCount(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboPeriod.SelectedValue), , strAdvanceFilter)
                'intTotalAuthorizedtCount = objPayAuthorize.GetAuthorizedTotalCount(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboPeriod.SelectedValue), , strAdvanceFilter)

                'If intTotalAuthorizedtCount <> intTotalPaymentCount Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, Payment authorization is not for some of the employee(s). Please authorize payment for all employees."), enMsgBoxStyle.Information)
                '    Exit Try
                'End If
                If mintViewIdx > 0 Then
                    dsList = objPayroll.Get_UnProcessed_Employee(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "Employee", CInt(cboPeriod.SelectedValue), True, False, " ADF." & mstrAnalysis_OrderBy.Split(".")(1) & " <= 0 ")
                    If dsList.Tables(0).Rows.Count > 0 Then
                        Dim objValidation As New frmCommonValidationList
                        dsList.Tables(0).Columns.Remove("employeeunkid")
                        dsList.Tables(0).Columns("employeename").SetOrdinal(0)
                        objValidation.displayDialog(False, Language.getMessage(mstrModuleName, 33, "Sorry, You cannot do #allocation# wise posting. Reason: #allocation# is not assigned to some of the employees.").Replace("#allocation#", mstrReport_GroupName.Replace(":", "")), dsList.Tables(0))
                        Exit Try
                    End If

                    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, "", , " AND allocationbyid <> " & mintViewIdx & " ") = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Sorry, You cannot do #allocation# wise posting. Reason: Non-#allocation# wise posting is already done for selected period.").Replace("#allocation#", mstrReport_GroupName.Replace(":", "")), enMsgBoxStyle.Information)
                        Exit Try
                    End If

                    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), mintViewIdx, mstrStringIds, str_BatchNo:=strBatchNo) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Posting is already done for selected #allocation# and selected period with batch no").Replace("#allocation#", mstrReport_GroupName.Replace(":", "")) & " " & strBatchNo & ".", enMsgBoxStyle.Information)
                        Exit Try
                    End If
                Else
                    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, "", , " AND allocationbyid > 0 ") = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Sorry, You cannot do posting without selecting any allocation. Reason: allocation wise posting is already done for selected period.").Replace("#allocation#", mstrReport_GroupName.Replace(":", "")), enMsgBoxStyle.Information)
                        Exit Try
                    End If

                    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, "", str_BatchNo:=strBatchNo) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Posting is already done for selected period with batch no") & " " & strBatchNo & ".", enMsgBoxStyle.Information)
                        Exit Try
                    End If
                End If
                If mintViewIdx > 0 Then
                    strAdvanceFilter = " ADF." & mstrAnalysis_OrderBy.Split(".")(1) & " IN (" & mstrStringIds & ") "
                End If
                dsList = objPayroll.Get_UnProcessed_Employee(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "Employee", CInt(cboPeriod.SelectedValue), True, False, strAdvanceFilter)
                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim objValidation As New frmCommonValidationList
                    dsList.Tables(0).Columns.Remove("employeeunkid")
                    dsList.Tables(0).Columns("employeename").SetOrdinal(0)
                    objValidation.displayDialog(False, Language.getMessage(mstrModuleName, 24, "Sorry, Process payroll is not done for the last date of the selected period for some of the employees."), dsList.Tables(0))
                    Exit Try
                End If
                dsList = objTnALeave.Get_Balance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, "", "Balance", , CInt(cboPeriod.SelectedValue), , strAdvanceFilter)
                If dsList.Tables("Balance").Select("balanceamount > 0").Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, Some payslip's are still unpaid in this period. Please make payment for unpaid payslip."), enMsgBoxStyle.Information)
                    Exit Try
                End If

                intTotalPaymentCount = objPayment.GetTotalPaymentCount(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboPeriod.SelectedValue), , strAdvanceFilter)
                intTotalAuthorizedtCount = objPayAuthorize.GetAuthorizedTotalCount(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboPeriod.SelectedValue), , strAdvanceFilter)

                If intTotalAuthorizedtCount <> intTotalPaymentCount Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, Payment authorization is not for some of the employee(s). Please authorize payment for all employees."), enMsgBoxStyle.Information)
                    Exit Try
                End If
                'Sohail (10 Apr 2020) -- End

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Are you sure you want to post Flex Cube JV to Oracle?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Try
                End If
                objMainLedger._DontAllowToPostIfHeadUnMapped = True
                objMainLedger._Loginemployeeunkid = 0
                objMainLedger._Isweb = False
                objMainLedger._FormName = mstrModuleName
                objMainLedger._ClientIP = getIP()
                objMainLedger._HostName = getHostName()
                objMainLedger._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'Sohail (07 Mar 2020) -- Start
                'NMB Enhancement # : Once Flex Cube JV is posted to oracle, trigger email to specified users mapped on configuration.
                objMainLedger._FlexCubeJVPostedOracleNotificationUserIds = ConfigParameter._Object._FlexCubeJVPostedOracleNotificationUserIds
                objMainLedger._LoginModeId = enLogin_Mode.DESKTOP
                'Sohail (07 Mar 2020) -- End
            End If
            'Sohail (24 Jun 2019) -- End

            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement - Exporting Flex Cube NMB JV to CSV in 75.1.
            'If objMainLedger.FlexCubeNMB_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, ConfigParameter._Object._OracleHostName, ConfigParameter._Object._OraclePortNo, ConfigParameter._Object._OracleServiceName, ConfigParameter._Object._OracleUserName, ConfigParameter._Object._OracleUserPassword) = True Then
            If objMainLedger.FlexCubeNMB_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, ConfigParameter._Object._OracleHostName, ConfigParameter._Object._OraclePortNo, ConfigParameter._Object._OracleServiceName, ConfigParameter._Object._OracleUserName, ConfigParameter._Object._OracleUserPassword) = True Then
                'Sohail (13 Dec 2018) -- End
                'Sohail (10 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Provide 2 different options for Flex Cube NMB JV to Export to CSV and Post to Oracle.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Data Posted Successfuly."), enMsgBoxStyle.Information)
                If SaveDialog.FileName.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Data Posted Successfuly."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                End If
                'Sohail (10 Jan 2019) -- End

                'Sohail (24 Jun 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
            Else
                If objMainLedger._ShowCommonValidationList = True AndAlso objMainLedger._CommonValidationTable IsNot Nothing Then
                    Dim dtTable As DataTable = objMainLedger._CommonValidationTable
                    'Sohail (10 Jun 2020) -- Start
                    'NMB Enhancement # : Validate employee bank accout no with bank branch using view shared by nmb and if fails then show list of bank accounts with export option in Flexcube JV.
                    Dim strTitle As String = ""
                    If dtTable.Columns.Contains("branch_code") = True AndAlso dtTable.Columns.Contains("account_no") = True Then
                        strTitle = Language.getMessage(mstrModuleName, 37, "Sorry, Some employee bank account no. are not matched with branch code.")
                        dtTable.Columns("remark").SetOrdinal(0)
                        dtTable.Columns("account_no").SetOrdinal(1)
                        For i As Integer = 3 To dtTable.Columns.Count - 1
                            dtTable.Columns.RemoveAt(3)
                        Next
                    Else
                        strTitle = Language.getMessage(mstrModuleName, 28, "Sorry, Some transaction heads are not mapped with any account. Please map heads with account to post Flex Cube JV to Oracle.")
                        'Sohail (10 Jun 2020) -- End
                    dtTable.Columns("trnheadname").SetOrdinal(0)
                    dtTable.Columns("tranheadcode").SetOrdinal(1)
                    For i As Integer = 2 To dtTable.Columns.Count - 1
                        dtTable.Columns.RemoveAt(2)
                    Next
                    dtTable.Columns("trnheadname").ColumnName = Language.getMessage(mstrModuleName, 29, "Head Name")
                    dtTable.Columns("tranheadcode").ColumnName = Language.getMessage(mstrModuleName, 30, "Head Code")
                    End If 'Sohail (10 Jun 2020)
                    Dim frmValidation As New frmCommonValidationList
                    'Sohail (10 Jun 2020) -- Start
                    'NMB Enhancement # : Validate employee bank accout no with bank branch using view shared by nmb and if fails then show list of bank accounts with export option in Flexcube JV.
                    'frmValidation.displayDialog(False, Language.getMessage(mstrModuleName, 28, "Sorry, Some transaction heads are not mapped with any account. Please map heads with account to post Flex Cube JV to Oracle."), dtTable)
                    frmValidation.displayDialog(False, strTitle, dtTable)
                    'Sohail (10 Jun 2020) -- End
                    Exit Try
                End If
                'Sohail (24 Jun 2019) -- End
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkFlexCubeJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Nov 2018) -- End

    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
    Private Sub lnkFlexCubeLoanBatchExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkFlexCubeLoanBatchExport.LinkClicked
        Dim strLoanIDs As String = ""
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                strLoanIDs = String.Join(",", (From p In dvScheme.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("loanschemeunkid").ToString)).ToArray)

                If strLoanIDs.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Please Select atleast one loan scheme."), enMsgBoxStyle.Information)
                    dgScheme.Focus()
                    Exit Try
                End If

                SaveDialog.FileName = ""
                SaveDialog.Filter = "CSV File|*.csv"
                SaveDialog.FilterIndex = 0

                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            'Sohail (23 Jun 2021) -- Start
            'NMB Iuuse : : Flex cube JV not balancing due to fmt currency.
            objMainLedger._FromEmpID = CInt(txtFromEmpID.Decimal)
            objMainLedger._ToEmpID = CInt(txtToEmpID.Decimal)
            'Sohail (23 Jun 2021) -- End

            If objMainLedger.FlexCubeNMB_LoanDeduction_Export(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, strLoanIDs) Then
                If SaveDialog.FileName.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Data Posted Successfuly."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                End If
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkFlexCubeLoanBatchExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (07 Mar 2020) -- End

    'Sohail (24 Jun 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
    Private Sub lnkShowJVBatchPosting_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkShowJVBatchPosting.LinkClicked
        Dim objFrm As New frmJVPosting
        Try
            objFrm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkShowJVBatchPosting_LinkClicked", mstrModuleName)
        Finally
            objFrm = Nothing
        End Try
    End Sub
    'Sohail (24 Jun 2019) -- End

    'Sohail (02 Mar 2019) -- Start
    'Mkombozi Bank Enhancement - Ref # 0002673 - 76.1 - BR-CBS JV Accounting integration to export data to CBS SQL database in xml format.
    Private Sub lnkBRJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkBRJVExport.LinkClicked, lnkBRJVPostSQL.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "CSV File|*.csv"
                SaveDialog.FilterIndex = 0
                If CType(sender, LinkLabel).Name = lnkBRJVExport.Name Then
                    SaveDialog.ShowDialog()
                    If SaveDialog.FileName = "" Then
                        Exit Try
                    End If
                End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            If objPeriod._FlexCube_BatchNo.Trim <> "" AndAlso CType(sender, LinkLabel).Name = lnkFlexCubeJVPostOracle.Name Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Posting is already done for selected period with batch no") & " " & objPeriod._FlexCube_BatchNo & ".", enMsgBoxStyle.Information)
                Exit Try
            End If

            If objMainLedger.BR_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, ConfigParameter._Object._SQLDataSource, ConfigParameter._Object._SQLDatabaseName, ConfigParameter._Object._SQLDatabaseOwnerName, ConfigParameter._Object._SQLUserName, ConfigParameter._Object._SQLUserPassword) = True Then
                If SaveDialog.FileName.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Data Posted Successfuly."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                End If
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkBRJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (02 Mar 2019) -- End


    'Hemant (15 Mar 2019) -- Start
    'MELIA ARUSHA ENHANCEMENT - Ref # 3593 - 76.1 : SAP ECC 6.0 JV integration.
    Private Sub lnkSAPECC6_0JVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSAPECC6_0JVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "CSV File|*.csv"
                'SaveDialog.FilterIndex = 0
                'If CType(sender, LinkLabel).Name = lnkSAPECC6_0JVExport.Name Then
                '    SaveDialog.ShowDialog()
                '    If SaveDialog.FileName = "" Then
                '        Exit Try
                '    End If
                'End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            'Hemant (27 Mar 2019) -- Start
            'If objPeriod._FlexCube_BatchNo.Trim <> "" AndAlso CType(sender, LinkLabel).Name = lnkFlexCubeJVPostOracle.Name Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Posting is already done for selected period with batch no") & " " & objPeriod._FlexCube_BatchNo & ".", enMsgBoxStyle.Information)
            '    Exit Try
            'End If
            'Hemant (27 Mar 2019) -- End
            If objMainLedger.SAP_ECC_6_0_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, True) = True Then


            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSAPECC6_0JVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Hemant (15 Mar 2019) -- End

    'Hemant (18 Apr 2019) -- Start
    'MWAUWASA ENHANCEMENT - Ref # 3747 - 76.1 : SAGE 300 JV integration.
    Private Sub lnkSAGE300JVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSAGE300JVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "CSV File|*.csv"
                'SaveDialog.FilterIndex = 0
                'If CType(sender, LinkLabel).Name = lnkSAPECC6_0JVExport.Name Then
                '    SaveDialog.ShowDialog()
                '    If SaveDialog.FileName = "" Then
                '        Exit Try
                '    End If
                'End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            If objMainLedger.SAGE_300_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, True) = True Then


            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSAGE300JVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Hemant (18 Apr 2019) -- End

    'Hemant (29 May 2019) -- Start
    'MUWASA ENHANCEMENT - Ref # 3329 - 76.1 : PASTEL V2 COMBINED JV INTEGRATION.
    Private Sub lnkPASTELV2JVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkPASTELV2JVExport.LinkClicked
        Try
            Dim objPeriod As New clscommom_period_Tran
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            ElseIf dtpPostingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please set Posting Date. Posting Date is mandatory information."), enMsgBoxStyle.Information)
                dtpPostingDate.Focus()
                Exit Try
            ElseIf cboPeriod.SelectedValue > 0 AndAlso dtpPostingDate.Checked = True Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                If objPeriod._Start_Date > dtpPostingDate.Value.Date Or objPeriod._End_Date < dtpPostingDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Posting Date should be in selected Period."), enMsgBoxStyle.Information)
                    dtpPostingDate.Focus()
                    Exit Try
                End If
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "CSV File|*.csv"
                'SaveDialog.FilterIndex = 0
                'If CType(sender, LinkLabel).Name = lnkPASTELV2JVExport.Name Then
                '    SaveDialog.ShowDialog()
                '    If SaveDialog.FileName = "" Then
                '        Exit Try
                '    End If
                'End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            If objMainLedger.PASTEL_V2_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, True) = True Then


            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkPASTELV2JVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Hemant (29 May 2019) -- End

    'Sohail (26 Mar 2020) -- Start
    'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
    Private Sub lnkPayrollJournalReport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkPayrollJournalReport.LinkClicked _
                                                                                                                                                 , lnkPayrollJournalExport.LinkClicked
        Try
            Dim objPeriod As New clscommom_period_Tran
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try

            Else
                Dim objFrm As New frmPayrollJournalColumns
                Dim strDilimiter As String
                Dim blnIsCrystalReport = True
                If CType(sender, LinkLabel).Name = lnkPayrollJournalExport.Name Then
                    blnIsCrystalReport = False
                End If

                If objFrm.displayDialog(enArutiReport.JournalVoucherLedger, blnIsCrystalReport, CInt(cboReportType.SelectedIndex)) = True Then

                    SaveDialog.FileName = ""

                    If CType(sender, LinkLabel).Name = lnkPayrollJournalExport.Name AndAlso objFrm._PayrollJournalExportModeId = enPayrollJournal_Export_Mode.CSV Then

                        If objFrm._PayrollJournalSaveAsTXT = True Then
                            SaveDialog.Filter = "TXT File|*.txt"
                        Else
                            SaveDialog.Filter = "CSV File|*.csv"
                        End If

                        If objFrm._PayrollJournalTabDelimiter = True Then
                            strDilimiter = vbTab
                        Else
                            strDilimiter = ","
                        End If

                        SaveDialog.FilterIndex = 0
                        SaveDialog.ShowDialog()
                        If SaveDialog.FileName = "" Then
                            Exit Try
                        End If

                    End If
                    objMainLedger._PeriodId = cboPeriod.SelectedValue
                    objMainLedger._PeriodName = cboPeriod.Text

                    objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
                    objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

                    objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
                    objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

                    objMainLedger._ReportId = cboReportType.SelectedIndex
                    objMainLedger._ReportName = lnkPayrollJournalReport.Text.Replace("&", "").Replace(".", "")

                    objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

                    objMainLedger._IsActive = chkInactiveemp.Checked

                    objMainLedger._BranchId = cboBranch.SelectedValue
                    objMainLedger._Branch_Name = cboBranch.Text

                    objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
                    objMainLedger._IgnoreZero = chkIgnoreZero.Checked
                    objMainLedger._PostingDate = dtpPostingDate.Value.Date

                    'Sohail (23 Nov 2020) -- Start
                    'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
                    objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
                    'Sohail (23 Nov 2020) -- End

                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    objMainLedger._PeriodStartDate = objPeriod._Start_Date
                    objMainLedger._PeriodEndDate = objPeriod._End_Date
                    objMainLedger._PeriodCode = objPeriod._Period_Code

                    objMainLedger._Ex_Rate = mdecEx_Rate
                    objMainLedger._Currency_Sign = mstrCurr_Sign

                    'Sohail (25 May 2021) -- Start
                    'Enhancement - Allocation filter on JV Report.
                    objMainLedger._Advance_Filter = mstrAdvanceFilter
                    'Sohail (25 May 2021) -- End

                    'Sohail (10 Apr 2020) -- Start
                    'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
                    objMainLedger._ViewByIds = mstrStringIds
                    objMainLedger._ViewIndex = mintViewIdx
                    objMainLedger._ViewByName = mstrStringName
                    objMainLedger._Analysis_Fields = mstrAnalysis_Fields
                    'Sohail (08 Jul 2020) -- Start
                    'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'objMainLedger._Analysis_Join = mstrAnalysis_Join
                    objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
                    'Sohail (08 Jul 2020) -- End
                    objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
                    objMainLedger._Report_GroupName = mstrReport_GroupName
                    objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
                    'Sohail (10 Apr 2020) -- End

                    objMainLedger._FromEmpID = CInt(txtFromEmpID.Decimal)
                    objMainLedger._ToEmpID = CInt(txtToEmpID.Decimal)

                    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing

                    If objMainLedger.Generate_PayrollJournalReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, True, objFrm._PayrollJournalExportModeId, objFrm._PayrollJournalColumnsIds, objFrm._PayrollJournalShowColumnHeader, objFrm._PayrollJournalSaveAsTXT, objFrm._PayrollJournalTabDelimiter, objFrm._JVDateFormat) = True Then
                        'Hemant (13 Jul 2020)  -- [ objFrm._JVDateFormat]
                        If CType(sender, LinkLabel).Name = lnkPayrollJournalExport.Name AndAlso objFrm._PayrollJournalExportModeId = enPayrollJournal_Export_Mode.CSV Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                        End If

                    End If

                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, LinkLabel).Name & "_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Mar 2020) -- End

    'Sohail (08 Apr 2020) -- Start
    'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
    Private Sub lnkSAGE300ERPJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSAGE300ERPJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "CSV File|*.csv"
                'SaveDialog.FilterIndex = 0
                'If CType(sender, LinkLabel).Name = lnkSAPECC6_0JVExport.Name Then
                '    SaveDialog.ShowDialog()
                '    If SaveDialog.FileName = "" Then
                '        Exit Try
                '    End If
                'End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code
            'Sohail (17 Apr 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode
            'Sohail (17 Apr 2020) -- End

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            If objMainLedger.SAGE_300_ERP_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, True) = True Then


            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSAGE300ERPJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (08 Apr 2020) -- End

    'Gajanan [15-April-2020] -- Start
    Private Sub lnkSAGE300ERPImport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSAGE300ERPImport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code
            'Sohail (21 May 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode
            'Sohail (21 May 2020) -- End

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            If objMainLedger.SAGE_300_ERP_Import_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, True) = True Then
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSAGE300ERPJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Gajanan [15-April-2020] -- End

    'Sohail (22 Jun 2020) -- Start
    'CORAL BEACH CLUB enhancement : 0004743 : New JV Integration Coral Sun JV.
    Private Sub lnkCoralSunJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCoralSunJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "CSV File|*.csv"
                'SaveDialog.FilterIndex = 0
                'If CType(sender, LinkLabel).Name = lnkSAPECC6_0JVExport.Name Then
                '    SaveDialog.ShowDialog()
                '    If SaveDialog.FileName = "" Then
                '        Exit Try
                '    End If
                'End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            If objMainLedger.Coral_Sun_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, True) = True Then

            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCoralSunJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (22 Jun 2020) -- End

    'Sohail (10 Aug 2020) -- Start
    'Freight Forwarders Kenya Enhancement : OLD-58 #  : New Accounting software Integration "Cargo Wise JV" for Freight Forwarders Kenya Ltd.
    Private Sub lnkCargoWiseJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCargoWiseJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            ElseIf CInt(cboCostCenter.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please Select Cost Center. Cost Center is mandatory information."), enMsgBoxStyle.Information)
                cboCostCenter.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "CSV File|*.csv"
                'SaveDialog.FilterIndex = 0
                'If CType(sender, LinkLabel).Name = lnkSAPECC6_0JVExport.Name Then
                '    SaveDialog.ShowDialog()
                '    If SaveDialog.FileName = "" Then
                '        Exit Try
                '    End If
                'End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            If objMainLedger.Cargo_Wise_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, True, CInt(cboCostCenter.SelectedValue), True) = True Then

            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCargoWiseJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (10 Aug 2020) -- End

    'Sohail (04 Sep 2020) -- Start
    'Benchmark Enhancement : OLD-73 #  : New Accounting software Integration "SAP Standard JV".
    Private Sub lnkSAPStandardJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSAPStandardJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "CSV File|*.csv"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            If objMainLedger.SAP_Standard_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, False, 0, False, True) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSAPStandardJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (04 Sep 2020) -- End

    'Sohail (10 Sep 2020) -- Start
    'Oxygen Communication Ltd Enhancement : OLD-68 #  : New Accounting software Integration "SAGE Evolution JV".
    Private Sub lnkSAGEEvolutionJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSAGEEvolutionJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            ElseIf CInt(cboCostCenter.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please Select Cost Center. Cost Center is mandatory information."), enMsgBoxStyle.Information)
                cboCostCenter.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "CSV File|*.csv"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            'Sohail (25 May 2021) -- Start
            'Enhancement - Allocation filter on JV Report.
            objMainLedger._Advance_Filter = mstrAdvanceFilter
            'Sohail (25 May 2021) -- End

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            If objMainLedger.SAGE_Evolution_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, False, CInt(cboCostCenter.SelectedValue), False) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSAGEEvolutionJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (10 Sep 2020) -- End

    'Sohail (16 Nov 2021) -- Start
    'Enhancement : OLD-492 : New Payroll JV Integration - Hakika Bank JV.
    Private Sub lnkHakikaBankJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkHakikaBankJVExport.LinkClicked
        Try
            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "CSV File|*.csv"
                'SaveDialog.FilterIndex = 0
                'If CType(sender, LinkLabel).Name = lnkSAPECC6_0JVExport.Name Then
                '    SaveDialog.ShowDialog()
                '    If SaveDialog.FileName = "" Then
                '        Exit Try
                '    End If
                'End If
            End If

            objMainLedger._PeriodId = cboPeriod.SelectedValue
            objMainLedger._PeriodName = cboPeriod.Text

            objMainLedger._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objMainLedger._DebitAmountTo = txtDebitAmountTo.Decimal

            objMainLedger._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objMainLedger._CreditAmountTo = txtCreditAMountTo.Decimal

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = ""

            objMainLedger._Accounting_Country = ConfigParameter._Object._Accounting_Country

            objMainLedger._IsActive = chkInactiveemp.Checked

            objMainLedger._BranchId = cboBranch.SelectedValue
            objMainLedger._Branch_Name = cboBranch.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.Value.Date

            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = ConfigParameter._Object._IgnoreNegativeNetPayEmployeesOnJV

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode

            objMainLedger._Ex_Rate = mdecEx_Rate
            objMainLedger._Currency_Sign = mstrCurr_Sign

            objMainLedger._Advance_Filter = mstrAdvanceFilter

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            objMainLedger._FromEmpID = CInt(txtFromEmpID.Decimal)
            objMainLedger._ToEmpID = CInt(txtToEmpID.Decimal)

            If objMainLedger.Hakika_Bank_JV_Export_CombinedJVReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, User._Object._Username, SaveDialog.FileName, ConfigParameter._Object._OpenAfterExport, Company._Object._Code) = True Then
                'Sohail (01 Dec 2021) - [Company._Object._Code]

            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkHakikaBankJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (16 Nov 2021) -- End

    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate
                    Dim dsList As DataSet
                    Dim objPeriod As New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    'Sohail (21 Aug 2015) -- End
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = dsList.Tables("ExRate").Rows(0)("currency_sign")
                        lblExRate.Text = Language.getMessage(mstrModuleName, 9, "Exchange Rate :") & " " & CDbl(mdecEx_Rate) & " "
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub
    'S.SANDEEP [ 11 SEP 2012 ] -- END

    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
    Private Sub cboTranHead_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTranHead.GotFocus
        Try
            With cboTranHead
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTranHead_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranHead_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTranHead.Leave
        Try
            If CInt(cboTranHead.SelectedValue) <= 0 Then
                'Sohail (08 Jul 2017) -- Start
                'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
                'Call SetDefaultSearchText()
                Call SetDefaultSearchText(cboTranHead)
                'Sohail (08 Jul 2017) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTranHead_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranHead_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboTranHead.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboTranHead.ValueMember
                    .DisplayMember = cboTranHead.DisplayMember
                    .DataSource = CType(cboTranHead.DataSource, DataTable)
                    .CodeMember = "code"

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cboTranHead.SelectedValue = frm.SelectedValue
                    cboTranHead.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboTranHead.Text = ""
                    cboTranHead.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTranHead_KeyPress", mstrModuleName)
        End Try
    End Sub
    'Sohail (02 Sep 2016) -- End

    'Sohail (08 Jul 2017) -- Start
    'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
    Private Sub cboCostCenter_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostCenter.GotFocus
        Try
            With cboCostCenter
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCostCenter_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostCenter_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostCenter.Leave
        Try
            If CInt(cboCostCenter.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboCostCenter)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCostCenter_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostCenter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboCostCenter.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboCostCenter.ValueMember
                    .DisplayMember = cboCostCenter.DisplayMember
                    .DataSource = CType(cboCostCenter.DataSource, DataTable)
                    'Sohail (02 Aug 2017) -- Start
                    'TANAPA Enhancement - 69.1 - Remove these columns highlighted RED  i.e  1, 2, 3, 4,5, 10,13. in Dynamics NAV JV Report as per Rutta's email.
                    '.CodeMember = "code"
                    .CodeMember = "costcentercode"
                    'Sohail (02 Aug 2017) -- End

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cboCostCenter.SelectedValue = frm.SelectedValue
                    cboCostCenter.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboCostCenter.Text = ""
                    cboCostCenter.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCostCenter_KeyPress", mstrModuleName)
        End Try
    End Sub
    'Sohail (08 Jul 2017) -- End

    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
    Private Sub txtSearchScheme_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchScheme.TextChanged
        Try
            If dvScheme IsNot Nothing Then
                dvScheme.RowFilter = "Code LIKE '%" & txtSearchScheme.Text.Replace("'", "''") & "%'  OR name LIKE '%" & txtSearchScheme.Text.Replace("'", "''") & "%'"
                dgScheme.Refresh()
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllScheme(ByVal blnCheckAll As Boolean)
        Try
            If dvScheme IsNot Nothing Then
                For Each dr As DataRowView In dvScheme
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvScheme.ToTable.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllScheme", mstrModuleName)
        End Try
    End Sub
    'Sohail (07 Mar 2020) -- End

    'Sohail (10 Apr 2020) -- Start
    'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
    Private Sub lnkAdvanceFilter_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdvanceFilter.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GroupName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (10 Apr 2020) -- End

    'Sohail (16 Nov 2021) -- Start
    'Enhancement : OLD-492 : New Payroll JV Integration - Hakika Bank JV.
    Private Sub lblReportType_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblReportType.DoubleClick
        Try
            objlblFromEmpID.Visible = Not objlblFromEmpID.Visible
            objlblToEmpID.Visible = Not objlblToEmpID.Visible
            txtFromEmpID.Visible = Not txtFromEmpID.Visible
            txtToEmpID.Visible = Not txtToEmpID.Visible
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lblReportType_DoubleClick", mstrModuleName)
        End Try
    End Sub
    'Sohail (16 Nov 2021) -- End

#End Region

#Region " Messages "
    '1, "Main Ledger"
    '2, "General Staff Ledger"
    '3, "Cost Center Ledger"
    '4, "Please Select Period."
    '5, "Comabined JV"
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterTBCJV.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterTBCJV.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblDebitAmountTo.Text = Language._Object.getCaption(Me.lblDebitAmountTo.Name, Me.lblDebitAmountTo.Text)
			Me.lblDebitAmountFrom.Text = Language._Object.getCaption(Me.lblDebitAmountFrom.Name, Me.lblDebitAmountFrom.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblCreditAMountTo.Text = Language._Object.getCaption(Me.lblCreditAMountTo.Name, Me.lblCreditAMountTo.Text)
			Me.lblCreditAMountFrom.Text = Language._Object.getCaption(Me.lblCreditAMountFrom.Name, Me.lblCreditAMountFrom.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.chkShowSummaryBottom.Text = Language._Object.getCaption(Me.chkShowSummaryBottom.Name, Me.chkShowSummaryBottom.Text)
			Me.chkShowSummaryNewPage.Text = Language._Object.getCaption(Me.chkShowSummaryNewPage.Name, Me.chkShowSummaryNewPage.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.chkShowGroupByCCGroup.Text = Language._Object.getCaption(Me.chkShowGroupByCCGroup.Name, Me.chkShowGroupByCCGroup.Text)
			Me.lblCCGroup.Text = Language._Object.getCaption(Me.lblCCGroup.Name, Me.lblCCGroup.Text)
			Me.chkIncludeEmployerContribution.Text = Language._Object.getCaption(Me.chkIncludeEmployerContribution.Name, Me.chkIncludeEmployerContribution.Text)
			Me.lnkSunJVExport.Text = Language._Object.getCaption(Me.lnkSunJVExport.Name, Me.lnkSunJVExport.Text)
			Me.lblExRate.Text = Language._Object.getCaption(Me.lblExRate.Name, Me.lblExRate.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.lnkiScalaJVExport.Text = Language._Object.getCaption(Me.lnkiScalaJVExport.Name, Me.lnkiScalaJVExport.Text)
			Me.lnkTBCJVExport.Text = Language._Object.getCaption(Me.lnkTBCJVExport.Name, Me.lnkTBCJVExport.Text)
			Me.chkIgnoreZero.Text = Language._Object.getCaption(Me.chkIgnoreZero.Name, Me.chkIgnoreZero.Text)
			Me.gbFilterTBCJV.Text = Language._Object.getCaption(Me.gbFilterTBCJV.Name, Me.gbFilterTBCJV.Text)
			Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
			Me.colhEContribHead.Text = Language._Object.getCaption(CStr(Me.colhEContribHead.Tag), Me.colhEContribHead.Text)
			Me.colhEContribHeadCode.Text = Language._Object.getCaption(CStr(Me.colhEContribHeadCode.Tag), Me.colhEContribHeadCode.Text)
			Me.lnkSunJV5Export.Text = Language._Object.getCaption(Me.lnkSunJV5Export.Name, Me.lnkSunJV5Export.Text)
			Me.lnkiScala2JVExport.Text = Language._Object.getCaption(Me.lnkiScala2JVExport.Name, Me.lnkiScala2JVExport.Text)
			Me.lblCustomCCenter.Text = Language._Object.getCaption(Me.lblCustomCCenter.Name, Me.lblCustomCCenter.Text)
			Me.chkShowColumnHeader.Text = Language._Object.getCaption(Me.chkShowColumnHeader.Name, Me.chkShowColumnHeader.Text)
			Me.lblInvoiceRef.Text = Language._Object.getCaption(Me.lblInvoiceRef.Name, Me.lblInvoiceRef.Text)
			Me.lnkXEROJVExport.Text = Language._Object.getCaption(Me.lnkXEROJVExport.Name, Me.lnkXEROJVExport.Text)
			Me.lnkNetSuiteERPJVExport.Text = Language._Object.getCaption(Me.lnkNetSuiteERPJVExport.Name, Me.lnkNetSuiteERPJVExport.Text)
			Me.lnkFlexCubeRetailJVExport.Text = Language._Object.getCaption(Me.lnkFlexCubeRetailJVExport.Name, Me.lnkFlexCubeRetailJVExport.Text)
			Me.lnkSunAccountProjectJVExport.Text = Language._Object.getCaption(Me.lnkSunAccountProjectJVExport.Name, Me.lnkSunAccountProjectJVExport.Text)
			Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.Name, Me.lblTranHead.Text)
			Me.lnkDynamicsNavJVExport.Text = Language._Object.getCaption(Me.lnkDynamicsNavJVExport.Name, Me.lnkDynamicsNavJVExport.Text)
			Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
			Me.lblPostingDate.Text = Language._Object.getCaption(Me.lblPostingDate.Name, Me.lblPostingDate.Text)
			Me.lnkFlexCubeUPLDJVExport.Text = Language._Object.getCaption(Me.lnkFlexCubeUPLDJVExport.Name, Me.lnkFlexCubeUPLDJVExport.Text)
			Me.lnkSAPJVBSOneExport.Text = Language._Object.getCaption(Me.lnkSAPJVBSOneExport.Name, Me.lnkSAPJVBSOneExport.Text)
			Me.chkIncludeEmpCodeNameOnDebit.Text = Language._Object.getCaption(Me.chkIncludeEmpCodeNameOnDebit.Name, Me.chkIncludeEmpCodeNameOnDebit.Text)
			Me.lnkFlexCubeJVExport.Text = Language._Object.getCaption(Me.lnkFlexCubeJVExport.Name, Me.lnkFlexCubeJVExport.Text)
			Me.lnkFlexCubeJVPostOracle.Text = Language._Object.getCaption(Me.lnkFlexCubeJVPostOracle.Name, Me.lnkFlexCubeJVPostOracle.Text)
			Me.lnkBRJVExport.Text = Language._Object.getCaption(Me.lnkBRJVExport.Name, Me.lnkBRJVExport.Text)
			Me.lnkBRJVPostSQL.Text = Language._Object.getCaption(Me.lnkBRJVPostSQL.Name, Me.lnkBRJVPostSQL.Text)
			Me.lnkSAPECC6_0JVExport.Text = Language._Object.getCaption(Me.lnkSAPECC6_0JVExport.Name, Me.lnkSAPECC6_0JVExport.Text)
			Me.lnkSAGE300JVExport.Text = Language._Object.getCaption(Me.lnkSAGE300JVExport.Name, Me.lnkSAGE300JVExport.Text)
			Me.lnkPASTELV2JVExport.Text = Language._Object.getCaption(Me.lnkPASTELV2JVExport.Name, Me.lnkPASTELV2JVExport.Text)
			Me.lnkShowJVBatchPosting.Text = Language._Object.getCaption(Me.lnkShowJVBatchPosting.Name, Me.lnkShowJVBatchPosting.Text)
			Me.lnkFlexCubeLoanBatchExport.Text = Language._Object.getCaption(Me.lnkFlexCubeLoanBatchExport.Name, Me.lnkFlexCubeLoanBatchExport.Text)
			Me.dgColhSchemeCode.HeaderText = Language._Object.getCaption(Me.dgColhSchemeCode.Name, Me.dgColhSchemeCode.HeaderText)
			Me.dgColhScheme.HeaderText = Language._Object.getCaption(Me.dgColhScheme.Name, Me.dgColhScheme.HeaderText)
			Me.lnkPayrollJournalExport.Text = Language._Object.getCaption(Me.lnkPayrollJournalExport.Name, Me.lnkPayrollJournalExport.Text)
			Me.lnkPayrollJournalReport.Text = Language._Object.getCaption(Me.lnkPayrollJournalReport.Name, Me.lnkPayrollJournalReport.Text)
			Me.lnkSAGE300ERPJVExport.Text = Language._Object.getCaption(Me.lnkSAGE300ERPJVExport.Name, Me.lnkSAGE300ERPJVExport.Text)
			Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
			Me.lnkSAGE300ERPImport.Text = Language._Object.getCaption(Me.lnkSAGE300ERPImport.Name, Me.lnkSAGE300ERPImport.Text)
			Me.lnkCoralSunJVExport.Text = Language._Object.getCaption(Me.lnkCoralSunJVExport.Name, Me.lnkCoralSunJVExport.Text)
			Me.lnkCargoWiseJVExport.Text = Language._Object.getCaption(Me.lnkCargoWiseJVExport.Name, Me.lnkCargoWiseJVExport.Text)
            Me.lnkSAPStandardJVExport.Text = Language._Object.getCaption(Me.lnkSAPStandardJVExport.Name, Me.lnkSAPStandardJVExport.Text)
			Me.lnkSAGEEvolutionJVExport.Text = Language._Object.getCaption(Me.lnkSAGEEvolutionJVExport.Name, Me.lnkSAGEEvolutionJVExport.Text)
			Me.lnkHakikaBankJVExport.Text = Language._Object.getCaption(Me.lnkHakikaBankJVExport.Name, Me.lnkHakikaBankJVExport.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Main Ledger")
			Language.setMessage(mstrModuleName, 2, "General Staff Ledger")
			Language.setMessage(mstrModuleName, 3, "Cost Center Ledger")
			Language.setMessage(mstrModuleName, 4, "Please Select Period.")
			Language.setMessage(mstrModuleName, 5, "Combined JV")
			Language.setMessage(mstrModuleName, 6, "Data Exported Successfuly.")
			Language.setMessage(mstrModuleName, 7, "Please Select Combined JV.")
			Language.setMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected.")
			Language.setMessage(mstrModuleName, 9, "Exchange Rate :")
			Language.setMessage(mstrModuleName, 10, "Please Select Custom Cost Center.")
			Language.setMessage(mstrModuleName, 11, "Please Select Transaction head. Transaction head is mandatory information.")
			Language.setMessage(mstrModuleName, 12, "Sorry, There is no default budget set.")
			Language.setMessage(mstrModuleName, 13, "Type to Search")
			Language.setMessage(mstrModuleName, 14, "Please Select Cost Center. Cost Center is mandatory information.")
			Language.setMessage(mstrModuleName, 15, "Please set Posting Date. Posting Date is mandatory information.")
			Language.setMessage(mstrModuleName, 16, "Data Posted Successfuly.")
			Language.setMessage(mstrModuleName, 17, "Sorry, Posting is already done for selected period with batch no")
			Language.setMessage(mstrModuleName, 18, "Data Exported Successfuly.")
			Language.setMessage(mstrModuleName, 19, "Posting Date should be in selected Period.")
			Language.setMessage(mstrModuleName, 24, "Sorry, Process payroll is not done for the last date of the selected period for some of the employees.")
			Language.setMessage(mstrModuleName, 25, "Sorry, Some payslip's are still unpaid in this period. Please make payment for unpaid payslip.")
			Language.setMessage(mstrModuleName, 26, "Sorry, Payment authorization is not for some of the employee(s). Please authorize payment for all employees.")
			Language.setMessage(mstrModuleName, 27, "Are you sure you want to post Flex Cube JV to Oracle?")
			Language.setMessage(mstrModuleName, 28, "Sorry, Some transaction heads are not mapped with any account. Please map heads with account to post Flex Cube JV to Oracle.")
			Language.setMessage(mstrModuleName, 29, "Head Name")
			Language.setMessage(mstrModuleName, 30, "Head Code")
			Language.setMessage(mstrModuleName, 31, "Sorry, You cannot post JV not holiday.")
			Language.setMessage(mstrModuleName, 32, "Please Select atleast one loan scheme.")
			Language.setMessage(mstrModuleName, 33, "Sorry, You cannot do #allocation# wise posting. Reason: #allocation# is not assigned to some of the employees.")
			Language.setMessage(mstrModuleName, 34, "Sorry, You cannot do #allocation# wise posting. Reason: Non-#allocation# wise posting is already done for selected period.")
			Language.setMessage(mstrModuleName, 35, "Sorry, Posting is already done for selected #allocation# and selected period with batch no")
			Language.setMessage(mstrModuleName, 36, "Sorry, You cannot do posting without selecting any allocation. Reason: allocation wise posting is already done for selected period.")
			Language.setMessage(mstrModuleName, 37, "Sorry, Some employee bank account no. are not matched with branch code.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
