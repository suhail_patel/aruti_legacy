'************************************************************************************************************************************
'Class Name : clsCoinAnalysisDReport.vb
'Purpose    :
'Date       :22/1/2011
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

Public Class clsCoinageAnalysis
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsCoinageAnalysis"
    Private mstrReportId As String = enArutiReport.CoinageAnalysisReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""
    Private dblColTot As Decimal()

    'Sandeep [ 09 MARCH 2011 ] -- Start
    Dim StrFinalPath As String = String.Empty
    'Sandeep [ 09 MARCH 2011 ] -- End 


    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIncludeInactiveEmp As Boolean = True
    Private mintExRateUnkid As Integer = 0
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    'S.SANDEEP [ 17 AUG 2011 ] -- END 

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrExRateName As String = String.Empty

    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (17 Apr 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End
#End Region

#Region " Properties "
    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property


    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _ExRateUnkid() As Integer
        Set(ByVal value As Integer)
            mintExRateUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch_Name() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property
    'S.SANDEEP [ 17 AUG 2011 ] -- END

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _ExRateName() As String
        Set(ByVal value As String)
            mstrExRateName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (17 Apr 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Public Functions and Procedures "
    Public Sub SetDefaultValue()

        Try
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintPeriodId = -1
            mstrPeriodName = ""
            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mintExRateUnkid = 0
            mblnIncludeInactiveEmp = False
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            mintBranchId = -1
            mstrBranchName = ""
            'S.SANDEEP [ 17 AUG 2011 ] -- END 


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try


    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " ,"
            End If

            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Period : ") & " " & mstrPeriodName & " "
            End If

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Branch :") & " " & mstrBranchName & " "
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mintExRateUnkid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Currency :") & " " & mstrExRateName & " "
            End If
            'Sohail (17 Apr 2012) -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= ""
                Me._FilterTitle &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        End Try

    End Sub

#End Region

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, True)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try

    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, True)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Function Export_to_Excel(ByVal strFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable, ByVal strCompanyName As String, ByVal strUsername As String, ByVal strFmtCurrency As String, ByVal imgImage As Image) As Boolean
        'Sohail (21 Aug 2015) - [strCompanyName, strUsername, strFmtCurrency, imgImage]

        Dim strStringBuilder As New StringBuilder
        Dim blnFlag As Boolean = False

        Try
            'HEADER PART
            strStringBuilder.Append(" <TITLE> " & Me._ReportName & " </TITLE> " & vbCrLf)
            strStringBuilder.Append(" <BODY> <FONT FACE=VERDANA FONT SIZE=2> " & vbCrLf)
            strStringBuilder.Append(" <BR> " & vbCrLf)
            strStringBuilder.Append(" <TABLE BORDER =0 WIDTH =140%> " & vbCrLf)

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strStringBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strStringBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If Company._Object._Image IsNot Nothing Then
                '    Company._Object._Image.Save(strImPath)
                'End If
                If imgImage IsNot Nothing Then
                    imgImage.Save(strImPath)
                End If
                'Sohail (21 Aug 2015) -- End
                strStringBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strStringBuilder.Append(" </TD> " & vbCrLf)
                strStringBuilder.Append(" </TR> " & vbCrLf)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End

            strStringBuilder.Append(" <TR> " & vbCrLf)
            strStringBuilder.Append(" <TD WIDTH = '10%'> ")
            strStringBuilder.Append(" <FONT SIZE = 2> <B>" & Language.getMessage(mstrModuleName, 8, "Prepared by :") & " </B> </FONT>" & vbCrLf)
            strStringBuilder.Append(" </TD> " & vbCrLf)
            strStringBuilder.Append(" <TD WIDTH ='10%' ALIGN='LEFT'> <FONT SIZE = 2>" & vbCrLf)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'strStringBuilder.Append(User._Object._Username & vbCrLf)
            strStringBuilder.Append(strUsername & vbCrLf)
            'Sohail (21 Aug 2015) -- End
            strStringBuilder.Append(" </FONT> </TD> " & vbCrLf)
            strStringBuilder.Append(" <TD WIDTH = '60%' COLSPAN = 18 ALIGN='CENTER' >" & vbCrLf)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'strStringBuilder.Append(" <FONT SIZE=3> <B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strStringBuilder.Append(" <FONT SIZE=3> <B> " & strCompanyName & " </B></FONT> " & vbCrLf)
            'Sohail (21 Aug 2015) -- End
            strStringBuilder.Append(" </TD> " & vbCrLf)
            strStringBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strStringBuilder.Append(" &nbsp; " & vbCrLf)
            strStringBuilder.Append(" </TD> " & vbCrLf)
            strStringBuilder.Append(" <TR> " & vbCrLf)
            'strStringBuilder.Append(" <TR VALIGN = 'MIDDLE' " & vbCrLf)
            strStringBuilder.Append(" <TD WIDTH='10%'>")
            strStringBuilder.Append(" <FONT SIZE =2 > <B>" & Language.getMessage(mstrModuleName, 9, "Date :") & " </B></FONT> " & vbCrLf)
            strStringBuilder.Append(" </TD> " & vbCrLf)
            strStringBuilder.Append(" <TD WIDTH='35' ALIGN='LEFT' > <FONT SIZE=2> " & vbCrLf)
            strStringBuilder.Append(Now.Date & vbCrLf)
            strStringBuilder.Append(" </FONT> </TD>")
            strStringBuilder.Append(" <TD WIDTH='60%' COLSPAN = 18 ALIGN='CENTER' > " & vbCrLf)
            strStringBuilder.Append(" <FONT SIZE=3> <B>" & Me._ReportName & "</B></FONT> " & vbCrLf)
            strStringBuilder.Append(" </TD> " & vbCrLf)
            strStringBuilder.Append(" <TD WIDTH='10%'>" & vbCrLf)
            strStringBuilder.Append(" &nbsp; " & vbCrLf)
            strStringBuilder.Append(" </TD> " & vbCrLf)
            strStringBuilder.Append(" </TR> " & vbCrLf)
            strStringBuilder.Append(" </TABLE> " & vbCrLf)
            strStringBuilder.Append(" <HR> " & vbCrLf)
            strStringBuilder.Append(" <B> " & vbCrLf)
            strStringBuilder.Append(" </HR> " & vbCrLf)
            'strStringBuilder.Append(" <TR> " & vbCrLf)
            strStringBuilder.Append(" <TD WIDTH ='10%'> ")
            strStringBuilder.Append(" <FONT SIZE =2 > <B><I>" & Me._FilterTitle & "</I> </B></FONT> " & vbCrLf)
            strStringBuilder.Append(" </TD> ")
            'strStringBuilder.Append(" </TR> " & vbCrLf)
            strStringBuilder.Append(" <HR> " & vbCrLf)
            strStringBuilder.Append(" <B> " & vbCrLf)
            strStringBuilder.Append(" </HR> " & vbCrLf)
            strStringBuilder.Append(" <BR> " & vbCrLf)
            strStringBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR = BLACK CELLSPACING = 0 CELLPADDING =3 WIDTH=140% > " & vbCrLf)
            strStringBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP>" & vbCrLf)

            'REPORT COLUMN CAPTION
            For j As Integer = 0 To objDataReader.Columns.Count - 1
                strStringBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN = MIDDLE > <FONT SIZE=2> <B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD> " & vbCrLf)
            Next

            strStringBuilder.Append(" </TR> " & vbCrLf)

            'DATA PART
            For i As Integer = 0 To objDataReader.Rows.Count - 1
                strStringBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1
                    'S.SANDEEP [ 17 AUG 2011 ] -- START
                    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                    'strStringBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    Select Case k
                        Case 0, 1
                            strStringBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                        Case Else
                            strStringBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End Select
                    'S.SANDEEP [ 17 AUG 2011 ] -- END 
                Next
                strStringBuilder.Append("</TR>" & vbCrLf)
            Next

            strStringBuilder.Append(" <TR> " & vbCrLf)

            For k As Integer = 0 To objDataReader.Columns.Count - 1
                If k = 0 Then
                    'S.SANDEEP [ 17 AUG 2011 ] -- START
                    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                    'strStringBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE = 2><B>" & Language.getMessage(mstrmodulename, 11, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
                    strStringBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE = 2><B>" & Language.getMessage(mstrModuleName, 3, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
                    'S.SANDEEP [ 17 AUG 2011 ] -- END 
                ElseIf k <= 1 Then
                    strStringBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE = 2><B>" & objDataReader.Rows.Count & "</B></FONT></TD>" & vbCrLf)
                    'S.SANDEEP [ 24 JUNE 2011 ] -- START
                    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
                ElseIf k = objDataReader.Columns.Count - 1 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'strStringBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE = 2><B>&nbsp;" & Format(CDec(dblColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                    strStringBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE = 2><B>&nbsp;" & Format(CDec(dblColTot(k)), strFmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                    'Sohail (21 Aug 2015) -- End
                Else
                    strStringBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE = 2><B>&nbsp;" & Convert.ToInt32(dblColTot(k)) & "</B></FONT></TD>" & vbCrLf)
                    'S.SANDEEP [ 24 JUNE 2011 ] -- END 
                End If
            Next
            strStringBuilder.Append("</TR>" & vbCrLf)
            strStringBuilder.Append("</TABLE>" & vbCrLf)
            strStringBuilder.Append("</HTML>" & vbCrLf)


            'Sandeep [ 09 MARCH 2011 ] -- Start
            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If
            'Sandeep [ 09 MARCH 2011 ] -- End 

            'Anjan (14 Apr 2011)-Start
            'Issue : To remove "\" from path which comes at end in path.
            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If
            'Anjan (14 Apr 2011)-End

            If SaveExcelFile(SavePath & "\" & strFileName & ".xls", strStringBuilder) Then
                'Sandeep [ 09 MARCH 2011 ] -- Start
                StrFinalPath = SavePath & "\" & strFileName & ".xls"
                'Sandeep [ 09 MARCH 2011 ] -- End 
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
        End Try

    End Function

    Private Function SaveExcelFile(ByVal fFilePath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fFilePath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelFile; Module Name: " & mstrModuleName)
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try

    End Function

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()

        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Sub Generate_CoinAgeAnalysis(ByVal xDatabaseName As String _
                                        , ByVal xUserUnkid As Integer _
                                        , ByVal xYearUnkid As Integer _
                                        , ByVal xCompanyUnkid As Integer _
                                        , ByVal xPeriodStart As Date _
                                        , ByVal xPeriodEnd As Date _
                                        , ByVal xUserModeSetting As String _
                                        , ByVal xOnlyApproved As Boolean _
                                        , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                        , ByVal blnApplyUserAccessFilter As Boolean _
                                        , ByVal strFmtCurrency As String _
                                        , ByVal xExportReportPath As String _
                                        , ByVal xOpenReportAfterExport As String _
                                        , ByVal strCompanyName As String _
                                        , ByVal strUsername As String _
                                        , ByVal imgImage As Image _
                                        )
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFmtCurrency, xExportReportPath, xOpenReportAfterExport, strUsername, imgImage]

        Dim dsDenomination As New DataSet
        'Dim dtTable As DataTable
        Dim dtFinalTable As DataTable
        Dim strQ As String = String.Empty
        Dim exForce As Exception = Nothing


        Try
            'Nilay (18-Mar-2015) -- Start
            'Issue : Aga Khan wanted to export reports on local computer and not on cloud server.
            'If ConfigParameter._Object._ExportReportPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Nilay (18-Mar-2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            dtFinalTable = New DataTable("CoinAge")
            Dim dCol As DataColumn

            dCol = New DataColumn("EmpCode")
            dCol.Caption = "Code"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("EmpName")
            dCol.Caption = "EmpName"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)


            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'strQ = "SELECT denomunkid " & _
            '                         ",denom_code " & _
            '                         ",denomination  " & _
            '                    "FROM cfdenomination_master " & _
            '                    "WHERE isactive=1 "
            'Dim dsList As New DataSet
            'dsList = objDataOperation.ExecQuery(strQ, "List")
            Dim dsList As New DataSet
            Dim objDenome As New clsDenomination
            dsList = objDenome.GetList("List", True, True, mintExRateUnkid)
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    dCol = New DataColumn("Column" & dtRow.Item("denomunkid"))
                    dCol.DefaultValue = 0
                    'S.SANDEEP [ 24 JUNE 2011 ] -- START
                    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

                    'S.SANDEEP [ 17 AUG 2011 ] -- START
                    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                    'dCol.Caption = Format(dtRow.Item("denomination"), "######################.##")
                    dCol.Caption = Format(dtRow.Item("denomination"), "######################.##") & "'s "
                    'S.SANDEEP [ 17 AUG 2011 ] -- END 

                    'S.SANDEEP [ 24 JUNE 2011 ] -- END 
                    dtFinalTable.Columns.Add(dCol)
                Next
            End If

            dCol = New DataColumn("Total")
            dCol.DefaultValue = 0
            dCol.Caption = "Total"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            strQ = ""

            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'strQ = " SELECT " & _
            '        " hremployee_master.employeecode AS code " & _
            '        " ,hremployee_master.firstname+''+hremployee_master.surname AS empname " & _
            '        " ,prcashdenomenation_tran.denominatonunkid AS denominationid " & _
            '        " ,prcashdenomenation_tran.denomination  " & _
            '        " ,prcashdenomenation_tran.quantity " & _
            '    " FROM " & _
            '         "prcashdenomenation_tran " & _
            '    " JOIN prcashdenomenation_master ON prcashdenomenation_tran.cashdenominationunkid = prcashdenomenation_master.cashdenominationunkid " & _
            '    " JOIN hremployee_master ON hremployee_master.employeeunkid= prcashdenomenation_master.employeeunkid " & _
            '    " JOIN cfdenomination_master ON cfdenomination_master.denomunkid=prcashdenomenation_tran.denominatonunkid " & _
            '    " WHERE prcashdenomenation_tran.isvoid = 0 AND prcashdenomenation_master.isvoid = 0 AND hremployee_master.isactive = 1 " & _
            '    "      AND periodunkid = @Period "

            strQ = "SELECT " & _
                    "	  hremployee_master.employeecode AS code " & _
                    "	 ,hremployee_master.firstname+''+hremployee_master.surname AS empname " & _
                    "	 ,prcashdenomenation_tran.denominatonunkid AS denominationid " & _
                    "	 ,prcashdenomenation_tran.denomination " & _
                    "	 ,prcashdenomenation_tran.quantity " & _
                    "FROM prcashdenomenation_tran " & _
                    "	JOIN prcashdenomenation_master ON prcashdenomenation_tran.cashdenominationunkid = prcashdenomenation_master.cashdenominationunkid " & _
                    "	JOIN hremployee_master ON hremployee_master.employeeunkid= prcashdenomenation_master.employeeunkid " & _
                    "	JOIN cfdenomination_master ON cfdenomination_master.denomunkid=prcashdenomenation_tran.denominatonunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            strQ &= "           LEFT JOIN " & _
                               "( " & _
                               "    SELECT " & _
                               "         stationunkid " & _
                               "        ,deptgroupunkid " & _
                               "        ,departmentunkid " & _
                               "        ,sectiongroupunkid " & _
                               "        ,sectionunkid " & _
                               "        ,unitgroupunkid " & _
                               "        ,unitunkid " & _
                               "        ,teamunkid " & _
                               "        ,classgroupunkid " & _
                               "        ,classunkid " & _
                               "        ,employeeunkid " & _
                               "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                               "    FROM hremployee_transfer_tran " & _
                               "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                               ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "WHERE prcashdenomenation_tran.isvoid = 0 " & _
                    "	AND prcashdenomenation_master.isvoid = 0 "

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmp = False Then
            '    'Sohail (17 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'strQ &= "	AND hremployee_master.isactive = 1 "
            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
            '    'Sohail (17 Apr 2012) -- End
            'End If
            ''Sohail (28 Jun 2011) -- Start
            ''Issue : According to prvilege that lower level user should not see superior level employees.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    'Sohail (17 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    strQ &= UserAccessLevel._AccessLevelFilterString
            '    'Sohail (17 Apr 2012) -- End
            'End If
            ''Sohail (28 Jun 2011) -- End
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ &= "	AND periodunkid = @Period " & _
            '        "   AND exchagerateunkid = @ExRateId "
            strQ &= "	AND periodunkid = @Period " & _
                    "   AND prcashdenomenation_master.countryunkid = @ExRateId "
            'Sohail (03 Sep 2012) -- End
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            If mintEmployeeId > 0 Then
                strQ &= " AND hremployee_master.employeeunkid = @Empid "
                objDataOperation.AddParameter("@Empid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'strQ &= " AND hremployee_master.stationunkid = @BranchId "
                strQ &= " AND T.stationunkid = @BranchId "
                'Sohail (21 Aug 2015) -- End
                objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 


            strQ &= " ORDER BY hremployee_master.employeeunkid , prcashdenomenation_tran.denominatonunkid "

            objDataOperation.AddParameter("@Period", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@ExRateId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExRateUnkid)

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If
            'Sohail (17 Apr 2012) -- End

            dsDenomination = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsDenomination.Tables(0).Rows.Count > 0 Then
                Dim rpt_Row As DataRow = Nothing
                Dim intCnt As Integer = 0
                Dim intNewCnt As Integer = 0
                Dim StrECode As String = String.Empty
                For Each dtRow As DataRow In dsDenomination.Tables(0).Rows
                    If StrECode <> dtRow.Item("code") Then
                        rpt_Row = dtFinalTable.NewRow
                        rpt_Row.Item("EmpCode") = dtRow.Item("code")
                        rpt_Row.Item("EmpName") = dtRow.Item("empname")
                        StrECode = dtRow.Item("code")
                        Dim dtTemp() As DataRow = dsDenomination.Tables(0).Select("code ='" & StrECode & "'")
                        intCnt = dtTemp.Length

                        'S.SANDEEP [ 24 JUNE 2011 ] -- START
                        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
                        'rpt_Row.Item("Column" & dtRow.Item("denominationid")) = Format(dtRow.Item("denomination"), GUI.fmtCurrency)
                        'rpt_Row.Item("Total") = Format((CDec(rpt_Row.Item("Total")) + CDec(dtRow.Item("denomination"))) * dtRow.Item("quantity"), GUI.fmtCurrency)
                        rpt_Row.Item("Column" & dtRow.Item("denominationid")) = dtRow.Item("quantity")
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Row.Item("Total") = Format(CDec(rpt_Row.Item("Total")) + (CDec(dtRow.Item("denomination")) * dtRow.Item("quantity")), GUI.fmtCurrency)
                        rpt_Row.Item("Total") = Format(CDec(rpt_Row.Item("Total")) + (CDec(dtRow.Item("denomination")) * dtRow.Item("quantity")), strFmtCurrency)
                        'Sohail (21 Aug 2015) -- End
                        'S.SANDEEP [ 24 JUNE 2011 ] -- END 
                    Else
                        'S.SANDEEP [ 24 JUNE 2011 ] -- START
                        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
                        'rpt_Row.Item("Column" & dtRow.Item("denominationid")) = Format(dtRow.Item("denomination"), GUI.fmtCurrency)
                        'rpt_Row.Item("Total") = Format((CDec(rpt_Row.Item("Total")) + CDec(dtRow.Item("denomination"))) * dtRow.Item("quantity"), GUI.fmtCurrency)
                        rpt_Row.Item("Column" & dtRow.Item("denominationid")) = dtRow.Item("quantity")
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Row.Item("Total") = Format(CDec(rpt_Row.Item("Total")) + (CDec(dtRow.Item("denomination")) * dtRow.Item("quantity")), GUI.fmtCurrency)
                        rpt_Row.Item("Total") = Format(CDec(rpt_Row.Item("Total")) + (CDec(dtRow.Item("denomination")) * dtRow.Item("quantity")), strFmtCurrency)
                        'Sohail (21 Aug 2015) -- End
                        'S.SANDEEP [ 24 JUNE 2011 ] -- END 
                    End If

                    intNewCnt += 1

                    If intCnt = intNewCnt Then
                        dtFinalTable.Rows.Add(rpt_Row)
                        intNewCnt = 0
                    End If
                Next
            End If

            Dim dblTotal As Decimal = 0
            Dim m As Integer = 2
            Dim n As Integer = 2
            dblColTot = New Decimal(dtFinalTable.Columns.Count) {}

            While m < dtFinalTable.Columns.Count
                For i As Integer = 0 To dtFinalTable.Rows.Count - 1
                    dblTotal += CDec(dtFinalTable.Rows(i)(m))
                Next

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dblColTot(n) = Format(CDec(dblTotal), GUI.fmtCurrency)
                dblColTot(n) = Format(CDec(dblTotal), strFmtCurrency)
                'Sohail (21 Aug 2015) -- End
                dblTotal = 0
                m = m + 1
                n = n + 1
            End While

            Call FilterTitleAndFilterQuery()

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtFinalTable) Then
            If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), xExportReportPath, dtFinalTable, strCompanyName, strUsername, strFmtCurrency, imgImage) Then
                'Sohail (21 Aug 2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
                'Sandeep [ 09 MARCH 2011 ] -- Start
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
                Call ReportFunction.Open_ExportedFile(xOpenReportAfterExport, StrFinalPath)
                'Sohail (21 Aug 2015) -- End
                'Sandeep [ 09 MARCH 2011 ] -- End 
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_CoinAgeAnalysis; Module Name: " & mstrModuleName)
        End Try

    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Period :")
            Language.setMessage(mstrModuleName, 3, "Grand Total :")
            Language.setMessage(mstrModuleName, 4, "Report successfully exported to Report export path")
            Language.setMessage(mstrModuleName, 5, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths.")
            Language.setMessage(mstrModuleName, 6, "Branch :")
            Language.setMessage(mstrModuleName, 7, "Currency :")
            Language.setMessage(mstrModuleName, 8, "Prepared by :")
            Language.setMessage(mstrModuleName, 9, "Date :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
