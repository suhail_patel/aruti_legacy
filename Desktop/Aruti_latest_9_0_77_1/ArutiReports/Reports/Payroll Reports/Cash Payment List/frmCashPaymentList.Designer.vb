﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCashPaymentList
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboReportMode = New System.Windows.Forms.ComboBox
        Me.lblReportMode = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.lblBranch = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.chkIncludeInactiveEmp = New System.Windows.Forms.CheckBox
        Me.chkShowSign = New System.Windows.Forms.CheckBox
        Me.cboPayAdvance = New System.Windows.Forms.ComboBox
        Me.lblType = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblAmountTo = New System.Windows.Forms.Label
        Me.txtAmountTo = New System.Windows.Forms.TextBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtAmount = New System.Windows.Forms.TextBox
        Me.lblEmpCode = New System.Windows.Forms.Label
        Me.cboEmployeeName = New System.Windows.Forms.ComboBox
        Me.lblEmployeeName = New System.Windows.Forms.Label
        Me.txtEmpcode = New System.Windows.Forms.TextBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.radLogo = New System.Windows.Forms.RadioButton
        Me.radCompanyDetails = New System.Windows.Forms.RadioButton
        Me.radLogoCompanyInfo = New System.Windows.Forms.RadioButton
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 461)
        Me.NavPanel.Size = New System.Drawing.Size(727, 55)
        Me.NavPanel.TabIndex = 2
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objelLine1)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.radLogo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.radCompanyDetails)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.radLogoCompanyInfo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboReportMode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblReportMode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCurrency)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCurrency)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkAnalysisBy)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblBranch)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboBranch)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkIncludeInactiveEmp)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkShowSign)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboPayAdvance)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblAmountTo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtAmountTo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblAmount)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtAmount)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmpCode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboEmployeeName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmployeeName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtEmpcode)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(12, 66)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(448, 312)
        Me.EZeeCollapsibleContainer1.TabIndex = 0
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Filter Criteria"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportMode
        '
        Me.cboReportMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportMode.DropDownWidth = 230
        Me.cboReportMode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportMode.FormattingEnabled = True
        Me.cboReportMode.Location = New System.Drawing.Point(94, 166)
        Me.cboReportMode.Name = "cboReportMode"
        Me.cboReportMode.Size = New System.Drawing.Size(126, 21)
        Me.cboReportMode.TabIndex = 226
        '
        'lblReportMode
        '
        Me.lblReportMode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportMode.Location = New System.Drawing.Point(6, 170)
        Me.lblReportMode.Name = "lblReportMode"
        Me.lblReportMode.Size = New System.Drawing.Size(80, 13)
        Me.lblReportMode.TabIndex = 225
        Me.lblReportMode.Text = "Report Mode"
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 230
        Me.cboCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(94, 139)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(128, 21)
        Me.cboCurrency.TabIndex = 7
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(8, 143)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(80, 13)
        Me.lblCurrency.TabIndex = 220
        Me.lblCurrency.Text = "Paid Currency"
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(347, 5)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 198
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(229, 116)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(53, 13)
        Me.lblBranch.TabIndex = 196
        Me.lblBranch.Text = "Branch"
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.DropDownWidth = 230
        Me.cboBranch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(288, 112)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(128, 21)
        Me.cboBranch.TabIndex = 6
        '
        'chkIncludeInactiveEmp
        '
        Me.chkIncludeInactiveEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmp.Location = New System.Drawing.Point(96, 193)
        Me.chkIncludeInactiveEmp.Name = "chkIncludeInactiveEmp"
        Me.chkIncludeInactiveEmp.Size = New System.Drawing.Size(211, 16)
        Me.chkIncludeInactiveEmp.TabIndex = 9
        Me.chkIncludeInactiveEmp.Text = "Include inactive employee"
        Me.chkIncludeInactiveEmp.UseVisualStyleBackColor = True
        '
        'chkShowSign
        '
        Me.chkShowSign.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowSign.Location = New System.Drawing.Point(288, 143)
        Me.chkShowSign.Name = "chkShowSign"
        Me.chkShowSign.Size = New System.Drawing.Size(152, 17)
        Me.chkShowSign.TabIndex = 8
        Me.chkShowSign.Text = "Show Signature"
        Me.chkShowSign.UseVisualStyleBackColor = True
        '
        'cboPayAdvance
        '
        Me.cboPayAdvance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayAdvance.DropDownWidth = 230
        Me.cboPayAdvance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayAdvance.FormattingEnabled = True
        Me.cboPayAdvance.Location = New System.Drawing.Point(94, 112)
        Me.cboPayAdvance.Name = "cboPayAdvance"
        Me.cboPayAdvance.Size = New System.Drawing.Size(128, 21)
        Me.cboPayAdvance.TabIndex = 5
        '
        'lblType
        '
        Me.lblType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.Location = New System.Drawing.Point(8, 116)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(80, 13)
        Me.lblType.TabIndex = 19
        Me.lblType.Text = "Net Pay/Adv."
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(288, 59)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(128, 21)
        Me.cboPeriod.TabIndex = 2
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(226, 63)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(56, 13)
        Me.lblPeriod.TabIndex = 5
        Me.lblPeriod.Text = "Period"
        '
        'lblAmountTo
        '
        Me.lblAmountTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmountTo.Location = New System.Drawing.Point(226, 90)
        Me.lblAmountTo.Name = "lblAmountTo"
        Me.lblAmountTo.Size = New System.Drawing.Size(56, 13)
        Me.lblAmountTo.TabIndex = 13
        Me.lblAmountTo.Text = "To"
        '
        'txtAmountTo
        '
        Me.txtAmountTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountTo.Location = New System.Drawing.Point(288, 86)
        Me.txtAmountTo.Name = "txtAmountTo"
        Me.txtAmountTo.Size = New System.Drawing.Size(128, 20)
        Me.txtAmountTo.TabIndex = 4
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(422, 32)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(8, 90)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(80, 13)
        Me.lblAmount.TabIndex = 11
        Me.lblAmount.Text = "Amount"
        '
        'txtAmount
        '
        Me.txtAmount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(94, 86)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(126, 20)
        Me.txtAmount.TabIndex = 3
        '
        'lblEmpCode
        '
        Me.lblEmpCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpCode.Location = New System.Drawing.Point(8, 63)
        Me.lblEmpCode.Name = "lblEmpCode"
        Me.lblEmpCode.Size = New System.Drawing.Size(80, 13)
        Me.lblEmpCode.TabIndex = 3
        Me.lblEmpCode.Text = "Emp.  Code"
        '
        'cboEmployeeName
        '
        Me.cboEmployeeName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeName.DropDownWidth = 150
        Me.cboEmployeeName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeName.FormattingEnabled = True
        Me.cboEmployeeName.Location = New System.Drawing.Point(94, 32)
        Me.cboEmployeeName.Name = "cboEmployeeName"
        Me.cboEmployeeName.Size = New System.Drawing.Size(322, 21)
        Me.cboEmployeeName.TabIndex = 0
        '
        'lblEmployeeName
        '
        Me.lblEmployeeName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeName.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployeeName.Name = "lblEmployeeName"
        Me.lblEmployeeName.Size = New System.Drawing.Size(80, 13)
        Me.lblEmployeeName.TabIndex = 0
        Me.lblEmployeeName.Text = "Emp. Name"
        '
        'txtEmpcode
        '
        Me.txtEmpcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpcode.Location = New System.Drawing.Point(94, 59)
        Me.txtEmpcode.Name = "txtEmpcode"
        Me.txtEmpcode.Size = New System.Drawing.Size(126, 20)
        Me.txtEmpcode.TabIndex = 1
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 384)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 61
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(448, 61)
        Me.gbSortBy.TabIndex = 1
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(94, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(322, 21)
        Me.txtOrderBy.TabIndex = 0
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(422, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(80, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(20, 222)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(333, 13)
        Me.objelLine1.TabIndex = 237
        Me.objelLine1.Text = "Logo Settings "
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radLogo
        '
        Me.radLogo.AutoSize = True
        Me.radLogo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLogo.Location = New System.Drawing.Point(94, 286)
        Me.radLogo.Name = "radLogo"
        Me.radLogo.Size = New System.Drawing.Size(48, 17)
        Me.radLogo.TabIndex = 236
        Me.radLogo.Text = "Logo"
        Me.radLogo.UseVisualStyleBackColor = True
        '
        'radCompanyDetails
        '
        Me.radCompanyDetails.AutoSize = True
        Me.radCompanyDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCompanyDetails.Location = New System.Drawing.Point(94, 262)
        Me.radCompanyDetails.Name = "radCompanyDetails"
        Me.radCompanyDetails.Size = New System.Drawing.Size(105, 17)
        Me.radCompanyDetails.TabIndex = 235
        Me.radCompanyDetails.Text = "Company Details"
        Me.radCompanyDetails.UseVisualStyleBackColor = True
        '
        'radLogoCompanyInfo
        '
        Me.radLogoCompanyInfo.AutoSize = True
        Me.radLogoCompanyInfo.Checked = True
        Me.radLogoCompanyInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLogoCompanyInfo.Location = New System.Drawing.Point(94, 238)
        Me.radLogoCompanyInfo.Name = "radLogoCompanyInfo"
        Me.radLogoCompanyInfo.Size = New System.Drawing.Size(152, 17)
        Me.radLogoCompanyInfo.TabIndex = 234
        Me.radLogoCompanyInfo.TabStop = True
        Me.radLogoCompanyInfo.Text = "Logo and Company Details"
        Me.radLogoCompanyInfo.UseVisualStyleBackColor = True
        '
        'frmCashPaymentList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(727, 516)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.Name = "frmCashPaymentList"
        Me.Text = "Cash Payment List"
        Me.Controls.SetChildIndex(Me.EZeeCollapsibleContainer1, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.PerformLayout()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtEmpcode As System.Windows.Forms.TextBox
    Friend WithEvents lblEmpCode As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeName As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeName As System.Windows.Forms.ComboBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblAmountTo As System.Windows.Forms.Label
    Friend WithEvents txtAmountTo As System.Windows.Forms.TextBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPayAdvance As System.Windows.Forms.ComboBox
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents chkShowSign As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeInactiveEmp As System.Windows.Forms.CheckBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboReportMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblReportMode As System.Windows.Forms.Label
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents radLogo As System.Windows.Forms.RadioButton
    Friend WithEvents radCompanyDetails As System.Windows.Forms.RadioButton
    Friend WithEvents radLogoCompanyInfo As System.Windows.Forms.RadioButton
End Class
