#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib

#End Region


Public Class frmCashPaymentList
#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmCashPaymentList"
    Private objCashPaymentList As clsCashPaymentList
    Private objEmp As clsEmployee_Master
    Private objPeriod As clscommom_period_Tran
    Private objCurrentPeriodId As clsMasterData
    Private mintCurrentPeriodId As Integer = 0

    'Sohail (26 Nov 2011) -- Start
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (26 Nov 2011) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Constructor "
    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        objCashPaymentList = New clsCashPaymentList(User._Object._Languageunkid,Company._Object._Companyunkid)
        objCashPaymentList.SetDefaultValue()
        ' Add any initialization after the InitializeComponent() call.


        'Pinkal (14-Dec-2012) -- Start
        'Enhancement : TRA Changes
        _Show_ExcelExtra_Menu = True
        'Pinkal (14-Dec-2012) -- End

        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End

    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objExRate As New clsExchangeRate 'Sohail (16 Dec 2011)

        Try
            objEmp = New clsEmployee_Master
            objPeriod = New clscommom_period_Tran
            objCurrentPeriodId = New clsMasterData
            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            Dim objBranch As New clsStation
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                             User._Object._Userunkid, _
            '                             FinancialYear._Object._YearUnkid, _
            '                             Company._Object._Companyunkid, _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             ConfigParameter._Object._UserAccessModeSetting, _
            '                             True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, True, "Employee", True)
            'Nilay (10-Feb-2016) -- End

            'Anjan [10 June 2015] -- End

            With cboEmployeeName
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Employee")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintCurrentPeriodId = objCurrentPeriodId.getCurrentPeriodID(enModuleReference.Payroll, ConfigParameter._Object._CurrentDateAndTime, 1)
            'Sohail (01 Feb 2016) -- Start
            'Enhancement - Show employee middle name as well for Cash Payment Report for KBC.
            'mintCurrentPeriodId = objCurrentPeriodId.getCurrentPeriodID(enModuleReference.Payroll, ConfigParameter._Object._CurrentDateAndTime, FinancialYear._Object._YearUnkid, 1)
            mintCurrentPeriodId = objCurrentPeriodId.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1, , True)
            'Sohail (01 Feb 2016) -- End
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = mintCurrentPeriodId
            End With

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objBranch = Nothing
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            cboPayAdvance.Items.Clear()
            cboPayAdvance.Items.Add(Language.getMessage(mstrModuleName, 4, "Net Pay"))
            cboPayAdvance.Items.Add(Language.getMessage(mstrModuleName, 5, "Advance"))
            cboPayAdvance.SelectedIndex = 0

            'Sohail (16 Dec 2011) -- Start
            dsList = objExRate.getComboList("Currency", True)
            With cboCurrency
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (03 Sep 2012) -- End
                .DisplayMember = "currency_name"
                .DataSource = dsList.Tables("Currency")
                .SelectedValue = 0
            End With
            'Sohail (16 Dec 2011) -- End

            'Sohail (02 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            With cboReportMode
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "DRAFT"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "AUTHORIZED"))
                .SelectedIndex = 0
            End With
            'Sohail (02 Jul 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally

            dsList.Dispose()
            dsList = Nothing

            objEmp = Nothing
            objPeriod = Nothing
            objCurrentPeriodId = Nothing
            objExRate = Nothing 'Sohail (16 Dec 2011)
        End Try

    End Sub

    Private Sub ResetValue()

        Try
            
            cboEmployeeName.SelectedValue = 0
            cboPeriod.SelectedValue = mintCurrentPeriodId
            txtEmpcode.Text = ""
            txtAmount.Text = ""
            txtAmountTo.Text = ""
            chkShowSign.CheckState = CheckState.Checked
            objCashPaymentList.setDefaultOrderBy(1)
            txtOrderBy.Text = objCashPaymentList.OrderByDisplay
            
            cboPayAdvance.SelectedIndex = 0


            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkIncludeInactiveEmp.Checked = False
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            cboBranch.SelectedValue = 0
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (26 Nov 2011) -- Start
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (26 Nov 2011) -- End

            cboCurrency.SelectedValue = 0 'Sohail (16 Dec 2011)

            cboReportMode.SelectedIndex = 0 'Sohail (02 Jul 2012)

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End

            'Sohail (29 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Cash Payment Report for KBC.
            Select Case ConfigParameter._Object._LogoOnPayslip
                Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                    radLogoCompanyInfo.Checked = True
                Case enLogoOnPayslip.COMPANY_DETAILS
                    radCompanyDetails.Checked = True
                Case enLogoOnPayslip.LOGO
                    radLogo.Checked = True
            End Select
            'Sohail (29 Jan 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try

    End Sub

    Private Function SetFilter() As Boolean

        Try
            objCashPaymentList.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objCashPaymentList._Period = CInt(cboPeriod.SelectedValue)
                objCashPaymentList._PeriodName = cboPeriod.Text
            End If

            If CInt(cboEmployeeName.SelectedValue) > 0 Then
                objCashPaymentList._EmployeeUnkId = CInt(cboEmployeeName.SelectedValue)
                objCashPaymentList._EmpName = cboEmployeeName.Text
            End If

            If Trim(txtEmpcode.Text) <> "" Then
                objCashPaymentList._EmpCode = txtEmpcode.Text
            End If

            If Trim(txtAmount.Text) <> "" And Trim(txtAmountTo.Text) <> "" Then
                objCashPaymentList._Amount = txtAmount.Text
                objCashPaymentList._AmountTo = txtAmountTo.Text
            End If

            If chkShowSign.CheckState = CheckState.Checked Then
                objCashPaymentList._IsShowSign = True
            Else
                objCashPaymentList._IsShowSign = False
            End If

            objCashPaymentList._NetPayAdvanceId = CInt(cboPayAdvance.SelectedIndex)
            objCashPaymentList._NetPayAdvance = cboPayAdvance.Text

            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objCashPaymentList._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 


            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            objCashPaymentList._BranchId = cboBranch.SelectedValue
            objCashPaymentList._Branch_Name = cboBranch.Text
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (26 Nov 2011) -- Start
            objCashPaymentList._ViewByIds = mstrStringIds
            objCashPaymentList._ViewIndex = mintViewIdx
            objCashPaymentList._ViewByName = mstrStringName
            objCashPaymentList._Analysis_Fields = mstrAnalysis_Fields
            objCashPaymentList._Analysis_Join = mstrAnalysis_Join
            objCashPaymentList._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCashPaymentList._Report_GroupName = mstrReport_GroupName
            'Sohail (26 Nov 2011) -- End

            'Sohail (16 Dec 2011) -- Start
            objCashPaymentList._CurrencyId = CInt(cboCurrency.SelectedValue)
            objCashPaymentList._CurrencyName = cboCurrency.Text
            'Sohail (16 Dec 2011) -- End

            'Sohail (20 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objCashPaymentList._PeriodStartDate = objPeriod._Start_Date
            objCashPaymentList._PeriodEndDate = objPeriod._End_Date
            objPeriod = Nothing
            'Sohail (20 Apr 2012) -- End

            'Sohail (29 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Cash Payment Report for KBC.
            objCashPaymentList._IsLogoCompanyInfo = radLogoCompanyInfo.Checked
            objCashPaymentList._IsOnlyLogo = radLogo.Checked
            objCashPaymentList._IsOnlyCompanyInfo = radCompanyDetails.Checked
            objCashPaymentList._PayslipTemplate = ConfigParameter._Object._PayslipTemplate
            'Sohail (29 Jan 2016) -- End

            'Sohail (02 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            objCashPaymentList._ReportModeId = CInt(cboReportMode.SelectedIndex)
            objCashPaymentList._ReportModeName = cboReportMode.Text
            'Sohail (02 Jul 2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objCashPaymentList._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try

    End Function
#End Region

#Region " Form Events "
    Private Sub frmBankRemittance_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCashPaymentList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollReport_FormClosed", mstrModuleName)
        End Try
    End Sub
    Private Sub frmBankRemittance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objCashPaymentList._ReportName
            Me._Message = objCashPaymentList._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollReport_Load", mstrModuleName)
        End Try
    End Sub
    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "
    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information.Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            If Not SetFilter() Then Exit Sub

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objCashPaymentList.generateReport(0, e.Type, enExportAction.None)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objCashPaymentList._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End
            objCashPaymentList.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information.Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            'Sohail (21 Aug 2015) -- End

            If Not SetFilter() Then Exit Sub

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objCashPaymentList.generateReport(0, enPrintAction.None, e.Type)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objCashPaymentList._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End
            objCashPaymentList.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployeeName.DataSource
            frm.SelectedValue = cboEmployeeName.SelectedValue
            frm.ValueMember = cboEmployeeName.ValueMember
            frm.DisplayMember = cboEmployeeName.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployeeName.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCashPaymentList.SetMessages()
            objfrm._Other_ModuleNames = "clsCashPaymentList"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End


#End Region

#Region " Other Control's Events "
    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click

        Try
            objCashPaymentList.setOrderBy(0)
            txtOrderBy.Text = objCashPaymentList.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    'Sohail (26 Nov 2011) -- Start
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            'Sohail (26 Nov 2011) -- Start
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Sohail (26 Nov 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (26 Nov 2011) -- End
#End Region


    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.lblEmpCode.Text = Language._Object.getCaption(Me.lblEmpCode.Name, Me.lblEmpCode.Text)
			Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
			Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblAmountTo.Text = Language._Object.getCaption(Me.lblAmountTo.Name, Me.lblAmountTo.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblType.Text = Language._Object.getCaption(Me.lblType.Name, Me.lblType.Text)
			Me.chkShowSign.Text = Language._Object.getCaption(Me.chkShowSign.Name, Me.chkShowSign.Text)
			Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.lblReportMode.Text = Language._Object.getCaption(Me.lblReportMode.Name, Me.lblReportMode.Text)
			Me.radLogo.Text = Language._Object.getCaption(Me.radLogo.Name, Me.radLogo.Text)
			Me.radCompanyDetails.Text = Language._Object.getCaption(Me.radCompanyDetails.Name, Me.radCompanyDetails.Text)
			Me.radLogoCompanyInfo.Text = Language._Object.getCaption(Me.radLogoCompanyInfo.Name, Me.radLogoCompanyInfo.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "DRAFT")
			Language.setMessage(mstrModuleName, 2, "AUTHORIZED")
			Language.setMessage(mstrModuleName, 3, "Period is mandatory information.Please select Period.")
			Language.setMessage(mstrModuleName, 4, "Net Pay")
			Language.setMessage(mstrModuleName, 5, "Advance")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
