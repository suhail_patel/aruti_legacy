﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAct_TimesheetReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tblpEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtAssessorEmp = New eZee.TextBox.AlphanumericTextBox
        Me.pnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblTo = New System.Windows.Forms.Label
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.gbShowSummary = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radSumm_Activity = New System.Windows.Forms.RadioButton
        Me.radSumm_UoM = New System.Windows.Forms.RadioButton
        Me.gbFilterCriteria.SuspendLayout()
        Me.tblpEmployee.SuspendLayout()
        Me.pnlEmp.SuspendLayout()
        CType(Me.dgvEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbShowSummary.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 463)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.tblpEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDate2)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDate1)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(333, 327)
        Me.gbFilterCriteria.TabIndex = 23
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tblpEmployee
        '
        Me.tblpEmployee.ColumnCount = 1
        Me.tblpEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpEmployee.Controls.Add(Me.txtAssessorEmp, 0, 0)
        Me.tblpEmployee.Controls.Add(Me.pnlEmp, 0, 1)
        Me.tblpEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpEmployee.Location = New System.Drawing.Point(11, 60)
        Me.tblpEmployee.Name = "tblpEmployee"
        Me.tblpEmployee.RowCount = 2
        Me.tblpEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpEmployee.Size = New System.Drawing.Size(311, 262)
        Me.tblpEmployee.TabIndex = 109
        '
        'txtAssessorEmp
        '
        Me.txtAssessorEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtAssessorEmp.Flags = 0
        Me.txtAssessorEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtAssessorEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtAssessorEmp.Name = "txtAssessorEmp"
        Me.txtAssessorEmp.Size = New System.Drawing.Size(305, 21)
        Me.txtAssessorEmp.TabIndex = 106
        '
        'pnlEmp
        '
        Me.pnlEmp.Controls.Add(Me.objchkEmployee)
        Me.pnlEmp.Controls.Add(Me.dgvEmployee)
        Me.pnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.pnlEmp.Name = "pnlEmp"
        Me.pnlEmp.Size = New System.Drawing.Size(305, 230)
        Me.pnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvEmployee
        '
        Me.dgvEmployee.AllowUserToAddRows = False
        Me.dgvEmployee.AllowUserToDeleteRows = False
        Me.dgvEmployee.AllowUserToResizeColumns = False
        Me.dgvEmployee.AllowUserToResizeRows = False
        Me.dgvEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvEmployee.ColumnHeadersHeight = 21
        Me.dgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvEmployee.MultiSelect = False
        Me.dgvEmployee.Name = "dgvEmployee"
        Me.dgvEmployee.RowHeadersVisible = False
        Me.dgvEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmployee.Size = New System.Drawing.Size(305, 230)
        Me.dgvEmployee.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(190, 36)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(35, 15)
        Me.lblTo.TabIndex = 82
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate2
        '
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(231, 33)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.Size = New System.Drawing.Size(91, 21)
        Me.dtpDate2.TabIndex = 81
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(8, 36)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(69, 15)
        Me.lblFromDate.TabIndex = 80
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate1
        '
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(83, 33)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.Size = New System.Drawing.Size(91, 21)
        Me.dtpDate1.TabIndex = 79
        '
        'gbShowSummary
        '
        Me.gbShowSummary.BorderColor = System.Drawing.Color.Black
        Me.gbShowSummary.Checked = False
        Me.gbShowSummary.CollapseAllExceptThis = False
        Me.gbShowSummary.CollapsedHoverImage = Nothing
        Me.gbShowSummary.CollapsedNormalImage = Nothing
        Me.gbShowSummary.CollapsedPressedImage = Nothing
        Me.gbShowSummary.CollapseOnLoad = False
        Me.gbShowSummary.Controls.Add(Me.radSumm_UoM)
        Me.gbShowSummary.Controls.Add(Me.radSumm_Activity)
        Me.gbShowSummary.ExpandedHoverImage = Nothing
        Me.gbShowSummary.ExpandedNormalImage = Nothing
        Me.gbShowSummary.ExpandedPressedImage = Nothing
        Me.gbShowSummary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbShowSummary.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbShowSummary.HeaderHeight = 25
        Me.gbShowSummary.HeaderMessage = ""
        Me.gbShowSummary.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbShowSummary.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbShowSummary.HeightOnCollapse = 0
        Me.gbShowSummary.LeftTextSpace = 0
        Me.gbShowSummary.Location = New System.Drawing.Point(12, 399)
        Me.gbShowSummary.Name = "gbShowSummary"
        Me.gbShowSummary.OpenHeight = 300
        Me.gbShowSummary.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbShowSummary.ShowBorder = True
        Me.gbShowSummary.ShowCheckBox = True
        Me.gbShowSummary.ShowCollapseButton = False
        Me.gbShowSummary.ShowDefaultBorderColor = True
        Me.gbShowSummary.ShowDownButton = False
        Me.gbShowSummary.ShowHeader = True
        Me.gbShowSummary.Size = New System.Drawing.Size(333, 58)
        Me.gbShowSummary.TabIndex = 24
        Me.gbShowSummary.Temp = 0
        Me.gbShowSummary.Text = "Show Summary"
        Me.gbShowSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radSumm_Activity
        '
        Me.radSumm_Activity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSumm_Activity.Location = New System.Drawing.Point(21, 33)
        Me.radSumm_Activity.Name = "radSumm_Activity"
        Me.radSumm_Activity.Size = New System.Drawing.Size(153, 17)
        Me.radSumm_Activity.TabIndex = 2
        Me.radSumm_Activity.Text = "Summary By Activity"
        Me.radSumm_Activity.UseVisualStyleBackColor = True
        '
        'radSumm_UoM
        '
        Me.radSumm_UoM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSumm_UoM.Location = New System.Drawing.Point(180, 33)
        Me.radSumm_UoM.Name = "radSumm_UoM"
        Me.radSumm_UoM.Size = New System.Drawing.Size(124, 17)
        Me.radSumm_UoM.TabIndex = 3
        Me.radSumm_UoM.Text = "Summary By UoM"
        Me.radSumm_UoM.UseVisualStyleBackColor = True
        '
        'frmAct_TimesheetReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 518)
        Me.Controls.Add(Me.gbShowSummary)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmAct_TimesheetReport"
        Me.Text = "frmAct_TimesheetReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbShowSummary, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.tblpEmployee.ResumeLayout(False)
        Me.tblpEmployee.PerformLayout()
        Me.pnlEmp.ResumeLayout(False)
        Me.pnlEmp.PerformLayout()
        CType(Me.dgvEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbShowSummary.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents tblpEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtAssessorEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbShowSummary As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents radSumm_UoM As System.Windows.Forms.RadioButton
    Friend WithEvents radSumm_Activity As System.Windows.Forms.RadioButton
End Class
