'************************************************************************************************************************************
'Class Name : frmAct_TimesheetReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAct_TimesheetReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAct_TimesheetReport"
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtEmployee As DataTable
    Private objActTimesheet As clsAct_TimesheetReport
    Private dtView As DataView

#End Region

#Region " Contructor "

    Public Sub New()
        objActTimesheet = New clsAct_TimesheetReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objActTimesheet.SetDefaultValue()
        InitializeComponent()
        _Show_AdvanceFilter = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub Fill_Employee()
        Dim objEMaster As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEMaster.GetEmployeeList("List", False, , , , , , , , , , , , , dtpDate1.Value.Date, dtpDate2.Value.Date, , , , , mstrAdvanceFilter)

            dsList = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         dtpDate1.Value.Date, _
                                         dtpDate2.Value.Date, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , , , , , , , , , , , , , , mstrAdvanceFilter)
            'Anjan [10 June 2015] -- End

            dsList.Tables("List").Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
            For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                dsList.Tables("List").Rows(i)("IsCheck") = False
            Next
            mdtEmployee = dsList.Tables("List").Copy

            dgvEmployee.AutoGenerateColumns = False
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "employeename"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            objdgcolhECheck.DataPropertyName = "IsCheck"
            dtView = mdtEmployee.DefaultView
            dgvEmployee.DataSource = dtView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Employee", mstrModuleName)
        Finally
            objEMaster = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpDate1.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpDate2.Value = ConfigParameter._Object._CurrentDateAndTime
            txtAssessorEmp.Text = ""
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAdvanceFilter = ""
            gbShowSummary.Checked = False
            radSumm_Activity.Checked = False
            radSumm_UoM.Checked = False
            Call Fill_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        objActTimesheet.SetDefaultValue()
        Try
            If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "To Date cannot be less than From Date."), enMsgBoxStyle.Information)
                dtpDate2.Focus()
            End If

            mdtEmployee.AcceptChanges()
            Dim dtmp() As DataRow = mdtEmployee.Select("IsCheck=true")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is mandatory information. Please select atleast one employee in order to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            If gbShowSummary.Checked = True Then
                If radSumm_Activity.Checked = False AndAlso radSumm_UoM.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Summary Type is mandatory Information. Please select Summary Type."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            Dim sEmpIds As String = String.Empty
            Dim selectedTags As List(Of String) = (From p In mdtEmployee.AsEnumerable() Where p.Field(Of Boolean)("IsCheck") = True Select (p.Item("employeeunkid").ToString)).ToList
            sEmpIds = String.Join(", ", selectedTags.ToArray())

            objActTimesheet._Date1 = dtpDate1.Value.Date
            objActTimesheet._Date2 = dtpDate2.Value.Date
            objActTimesheet._EmployeeIds = sEmpIds
            objActTimesheet._IsSummaryIncluded = gbShowSummary.Checked
            objActTimesheet._IsSummaryByActivity = Not radSumm_UoM.Checked

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmAct_TimesheetReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objActTimesheet = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAct_TimesheetReport_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAct_TimesheetReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Me._Title = objActTimesheet._ReportName
            Me._Message = objActTimesheet._ReportDesc

            Call Fill_Employee()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAct_TimesheetReport_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAct_TimesheetReport.SetMessages()
            objfrm._Other_ModuleNames = "clsAct_TimesheetReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objActTimesheet.generateReport(0, e.Type, enExportAction.None)
            objActTimesheet.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpDate1.Value.Date, dtpDate2.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objActTimesheet.generateReport(0, enPrintAction.None, e.Type)
            objActTimesheet.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpDate1.Value.Date, dtpDate2.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
            Call Fill_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub txtAssessorEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAssessorEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvEmployee.Rows.Count > 0 Then
                        If dgvEmployee.SelectedRows(0).Index = dgvEmployee.Rows(dgvEmployee.RowCount - 1).Index Then Exit Sub
                        dgvEmployee.Rows(dgvEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvEmployee.Rows.Count > 0 Then
                        If dgvEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvEmployee.Rows(dgvEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtAssessorEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAssessorEmp.TextChanged
        Dim strSearch As String = ""
        Try
            If txtAssessorEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEName.DataPropertyName & " LIKE '%" & txtAssessorEmp.Text & "%' OR " & _
                            dgcolhEcode.DataPropertyName & " LIKE '%" & txtAssessorEmp.Text & "%'"
            End If
            dtView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick

            For Each dr As DataRowView In dtView
                dr.Item("IsCheck") = CBool(objchkEmployee.CheckState)
            Next
            dgvEmployee.Refresh()
            AddHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmployee.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvEmployee.IsCurrentCellDirty Then
                    Me.dgvEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim intEmpId As Integer = CInt(dgvEmployee.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value)
                Dim dTemp() As DataRow = mdtEmployee.Select(objdgcolhEmpId.DataPropertyName & " = '" & intEmpId & "'")

                If dTemp.Length > 0 Then
                    If CBool(dTemp(0)(objdgcolhEmpId.DataPropertyName.ToString)) = True Then
                        mdtEmployee.Rows(mdtEmployee.Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName) = CBool(mdtEmployee.Rows(mdtEmployee.Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName))
                    Else
                        mdtEmployee.Rows(mdtEmployee.Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName) = Not CBool(mdtEmployee.Rows(mdtEmployee.Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName))
                    End If
                Else
                    mdtEmployee.Rows(e.RowIndex)(objdgcolhECheck.DataPropertyName) = Not CBool(mdtEmployee.Rows(e.RowIndex)(objdgcolhECheck.DataPropertyName))
                End If
                mdtEmployee.AcceptChanges()

                Dim drRow As DataRow() = mdtEmployee.Select("IsCheck = true", "")
                If drRow.Length > 0 Then
                    If mdtEmployee.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbShowSummary.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbShowSummary.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.gbShowSummary.Text = Language._Object.getCaption(Me.gbShowSummary.Name, Me.gbShowSummary.Text)
            Me.radSumm_UoM.Text = Language._Object.getCaption(Me.radSumm_UoM.Name, Me.radSumm_UoM.Text)
            Me.radSumm_Activity.Text = Language._Object.getCaption(Me.radSumm_Activity.Name, Me.radSumm_Activity.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is mandatory information. Please select atleast one employee in order to continue.")
            Language.setMessage(mstrModuleName, 2, "To Date cannot be less than From Date.")
            Language.setMessage(mstrModuleName, 3, "Summary Type is mandatory Information. Please select Summary Type.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
