#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
#End Region


Public Class frmPayrollHeadCount

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmPayrollHeadCount"
    Private objPayrollHeadCount As clsPayrollHeadCount
    Private mintCurrPeriodID As Integer
#End Region

#Region "Constructor"
    Public Sub New()
        objPayrollHeadCount = New clsPayrollHeadCount(User._Object._Languageunkid,Company._Object._Companyunkid)
        objPayrollHeadCount.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region "Public Function"
    Public Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsCombo As New DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                'Sohail (01 May 2013) -- Start
                'TRA - ENHANCEMENT
                '.SelectedValue = 0
                .SelectedValue = mintCurrPeriodID
                'Sohail (01 May 2013) -- End
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            cboPeriod.SelectedValue = mintCurrPeriodID
            chkInactiveemp.Checked = False

            objPayrollHeadCount.setDefaultOrderBy(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValues", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function SetFilter(ByRef xPeriodStartDate As Date _
                              , ByRef xPeriodEndDate As Date _
                              ) As Boolean
        'Sohail (21 Aug 2015) - [xPeriodStartDate, xPeriodEndDate]

        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim intPrevPeriodID As Integer
        Dim strBfDatabaseName As String
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            End If

            objPayrollHeadCount.SetDefaultValue()

            objPayrollHeadCount._PeriodId = cboPeriod.SelectedValue
            objPayrollHeadCount._PeriodName = cboPeriod.Text
            objPayrollHeadCount._IncludeInactiveEmployee = chkInactiveemp.Checked

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objPayrollHeadCount._PeriodStartDate = objPeriod._Start_Date
            objPayrollHeadCount._PeriodEndDate = objPeriod._End_Date

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            xPeriodStartDate = objPeriod._Start_Date
            xPeriodEndDate = objPeriod._End_Date
            'Sohail (21 Aug 2015) -- End

            If objPeriod._Start_Date = FinancialYear._Object._Database_Start_Date OrElse objPeriod._Yearunkid <> FinancialYear._Object._YearUnkid Then 'If Selected Period is current Year's First Period OR is Previous year's period.
                Dim objTempPeriod As New clscommom_period_Tran
                Dim dtTable As DataTable
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objTempPeriod.GetList("Period", enModuleReference.Payroll, True, , , True)
                dsList = objTempPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, , , True)
                'Sohail (21 Aug 2015) -- End
                dtTable = New DataView(dsList.Tables("Period"), "end_date < " & eZeeDate.convertDate(objPeriod._Start_Date) & " ", "end_date DESC", DataViewRowState.CurrentRows).ToTable

                If dtTable.Rows.Count > 0 Then

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'intPrevPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateAndTime.DateAdd(DateInterval.Day, -1, objPeriod._Start_Date), , CInt(dtTable.Rows(0).Item("yearunkid")))
                    intPrevPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateAndTime.DateAdd(DateInterval.Day, -1, objPeriod._Start_Date), CInt(dtTable.Rows(0).Item("yearunkid")))
                    'S.SANDEEP [04 JUN 2015] -- END

                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTempPeriod._Periodunkid = intPrevPeriodID
                objTempPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriodID
                'Sohail (21 Aug 2015) -- End
                dsList = objMaster.Get_Database_Year_List("Database", True, Company._Object._Companyunkid)
                dtTable = New DataView(dsList.Tables("Database"), "yearunkid = " & objTempPeriod._Yearunkid & " ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    strBfDatabaseName = dtTable.Rows(0).Item("database_name").ToString
                Else
                    strBfDatabaseName = FinancialYear._Object._DatabaseName
                End If
                objTempPeriod = Nothing
            Else
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'intPrevPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateAndTime.DateAdd(DateInterval.Day, -1, objPeriod._Start_Date))
                intPrevPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateAndTime.DateAdd(DateInterval.Day, -1, objPeriod._Start_Date), FinancialYear._Object._YearUnkid)
                'S.SANDEEP [04 JUN 2015] -- END

                strBfDatabaseName = FinancialYear._Object._DatabaseName
            End If


            If intPrevPeriodID > 0 Then
                objPeriod = New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = intPrevPeriodID
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriodID
                'Sohail (21 Aug 2015) -- End

                objPayrollHeadCount._PreviousPeriodId = intPrevPeriodID
                objPayrollHeadCount._BF_DatabaseName = strBfDatabaseName
                objPayrollHeadCount._PreviousPeriodStartDate = objPeriod._Start_Date
                objPayrollHeadCount._PreviousPeriodEndDate = objPeriod._End_Date
            Else
                objPayrollHeadCount._PreviousPeriodId = 0
                objPayrollHeadCount._BF_DatabaseName = strBfDatabaseName
                objPayrollHeadCount._PreviousPeriodStartDate = DateAndTime.DateAdd(DateInterval.Day, -1, objPeriod._Start_Date)
                objPayrollHeadCount._PreviousPeriodEndDate = DateAndTime.DateAdd(DateInterval.Day, -1, objPeriod._Start_Date)
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function
#End Region

#Region "Form's Events"

    Private Sub frmPayrollHeadCount_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPayrollHeadCount = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollHeadCount_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollHeadCount_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Me._Title = objPayrollHeadCount._ReportName
            Me._Message = objPayrollHeadCount._ReportDesc

            Call FillCombo()

            Dim objMaster As New clsMasterData
            'Sohail (01 May 2013) -- Start
            'TRA - ENHANCEMENT
            'mintCurrPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1)
            mintCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (01 May 2013) -- End
            cboPeriod.SelectedValue = mintCurrPeriodID
            objMaster = Nothing

            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollHeadCount_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollHeadCount_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmPayrollHeadCount_Report_Click(Nothing, Nothing)
                End If
            ElseIf e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollHeadCount_KeyDown", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Buttons"
    Private Sub frmPayrollHeadCount_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If SetFilter() = False Then Exit Sub

            'objPayrollHeadCount.generateReport(0, e.Type, Aruti.Data.enExportAction.None)
            Dim xPeriodStartDate As Date
            Dim xPeriodEndDate As Date
            If SetFilter(xPeriodStartDate, xPeriodEndDate) = False Then Exit Sub

            objPayrollHeadCount.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStartDate, xPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, Aruti.Data.enExportAction.None)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollHeadCount_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollHeadCount_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If SetFilter() = False Then Exit Sub

            'objPayrollHeadCount.generateReport(0, enPrintAction.None, e.Type)
            Dim xPeriodStartDate As Date
            Dim xPeriodEndDate As Date
            If SetFilter(xPeriodStartDate, xPeriodEndDate) = False Then Exit Sub

            objPayrollHeadCount.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStartDate, xPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollHeadCount_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollHeadCount_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollHeadCount_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollHeadCount_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollHeadCount_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollHeadCount_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayrollHeadCount.SetMessages()
            objfrm._Other_ModuleNames = "clsPayrollHeadCount"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Period.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
