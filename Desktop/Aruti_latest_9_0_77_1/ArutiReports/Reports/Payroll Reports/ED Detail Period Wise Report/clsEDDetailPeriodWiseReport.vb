Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

Public Class clsEDDetailPeriodWiseReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEDDetailPeriodWiseReport"
    Private mstrReportId As String = enArutiReport.EDDetail_PeriodWise_Report
    Dim objDataOperation As clsDataOperation

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintModeId As Integer = -1
    Private mstrModeName As String = ""
    Private mintHeadID As Integer = -1
    Private mstrHeadName As String = ""
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""
    Private mstrOrderByQuery As String = ""

    Private mblnIsActive As Boolean = True
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""

    Private mstrHeadCode As String = String.Empty


    Private mintToPeriodId As Integer = -1
    Private mstrToPeriodName As String = ""
    Private mstrPeriodID As String = ""

    Private mblnIncludeNegativeHeads As Boolean = True

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mstrAdvance_Filter As String = String.Empty

    Private mintBase_CurrencyId As Integer = ConfigParameter._Object._Base_CurrencyId
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    Private mintUserUnkid As Integer = User._Object._Userunkid
    Private mintCompanyUnkid As Integer = Company._Object._Companyunkid
    Private mstrTranDatabaseName As String = FinancialYear._Object._DatabaseName

    Private mintMembershipId As Integer = -1
    Private mstrMembershipName As String = ""

    Private marrDatabaseName As New ArrayList
#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ModeId() As Integer
        Set(ByVal value As Integer)
            mintModeId = value
        End Set
    End Property

    Public WriteOnly Property _ModeName() As String
        Set(ByVal value As String)
            mstrModeName = value
        End Set
    End Property

    Public WriteOnly Property _HeadId() As Integer
        Set(ByVal value As Integer)
            mintHeadID = value
        End Set
    End Property

    Public WriteOnly Property _HeadName() As String
        Set(ByVal value As String)
            mstrHeadName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _HeadCode() As String
        Set(ByVal value As String)
            mstrHeadCode = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _mstrPeriodID() As String
        Set(ByVal value As String)
            mstrPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _IncludeNegativeHeads() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeNegativeHeads = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _Base_CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBase_CurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TranDatabaseName() As String
        Set(ByVal value As String)
            mstrTranDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MemberShipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property


#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintModeId = -1
            mstrModeName = ""
            mintHeadID = -1
            mstrHeadName = ""
            mintPeriodId = -1
            mstrPeriodName = ""
            mintToPeriodId = -1
            mstrToPeriodName = ""
            mintMembershipId = -1
            mstrMembershipName = ""

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            mstrAdvance_Filter = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        mstrOrderByQuery = ""
        Try

            Dim mstrheadID As String = ""
            Dim mstrModeID As String = ""

            If mintMembershipId > 0 Then
                Dim objMemberShip As New clsmembership_master
                objMemberShip._Membershipunkid = mintMembershipId
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Membership : ") & " " & mstrMembershipName & " "
                mstrheadID = "," & objMemberShip._ETransHead_Id.ToString() & "," & objMemberShip._CTransHead_Id.ToString()
                mstrModeID = "," & enTranHeadType.EmployeesStatutoryDeductions & "," & enTranHeadType.EmployersStatutoryContributions
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkId = @employeeunkId "
                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintModeId > 0 Then
                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                'Me._FilterQuery &= " AND prtranhead_master.trnheadtype_id IN (" & mintModeId & mstrModeID & ")"
                If mintModeId = 99999 Then
                    Me._FilterQuery &= " AND prpayrollprocess_tran.activityunkid > 0 "
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    'Sohail (07 Jun 2021) -- Start
                    'KBC Enhancement : OLD-391 : Imprest posting to Payroll.
                    'Me._FilterQuery &= " AND prpayrollprocess_tran.crprocesstranunkid > 0 "
                    Me._FilterQuery &= " AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) "
                    'Sohail (07 JUn 2021) -- End
                    'Sohail (23 Jun 2015) -- End
                Else
                    Me._FilterQuery &= " AND prtranhead_master.trnheadtype_id IN (" & mintModeId & mstrModeID & ")"
                End If
                'Sohail (10 Sep 2014) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Mode : ") & " " & mstrModeName & " "
            End If

            If mintHeadID > 0 Then
                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                'Me._FilterQuery &= " AND prtranhead_master.tranheadunkid IN  ( " & mintHeadID & mstrheadID & ")"
                If mintModeId = 99999 Then
                    Me._FilterQuery &= " AND prpayrollprocess_tran.activityunkid IN  ( " & mintHeadID & mstrheadID & ")"
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    'Sohail (07 Jun 2021) -- Start
                    'KBC Enhancement : OLD-391 : Imprest posting to Payroll.
                    'Me._FilterQuery &= " AND cmexpense_master.expenseunkid IN  ( " & mintHeadID & mstrheadID & ")"
                    Me._FilterQuery &= " AND (cmexpense_master.expenseunkid IN  ( " & mintHeadID & mstrheadID & ") OR crretireexpense.expenseunkid IN  ( " & mintHeadID & mstrheadID & ")) "
                    'Sohail (07 JUn 2021) -- End
                    'Sohail (23 Jun 2015) -- End
                Else
                    Me._FilterQuery &= " AND prtranhead_master.tranheadunkid IN  ( " & mintHeadID & mstrheadID & ")"
                End If
                'Sohail (10 Sep 2014) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Head : ") & " " & mstrHeadName & " "
            End If

            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Period : ") & " " & mstrPeriodName & " "
            End If

            If mintToPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Period : ") & " " & mstrToPeriodName & " "
            End If

            Me._FilterQuery &= " AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodID & " ) "

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                If mintViewIndex > 0 Then
                    If Me.OrderByQuery.Trim = "" Then
                        mstrOrderByQuery &= "ORDER BY GNAME"
                    Else
                        mstrOrderByQuery &= "ORDER BY GNAME, " & Me.OrderByQuery
                    End If
                Else
                    mstrOrderByQuery &= "ORDER BY " & Me.OrderByQuery
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid
        '    User._Object._Userunkid = mintUserUnkid

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid
            User._Object._Userunkid = mintUserUnkid

            menExportAction = ExportAction
            mdtTableExcel = Nothing


            'objRpt = Generate_DetailReport()
            'Rpt = objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 9, "Emp Code")))
            iColumn_DetailReport.Add(New IColumn("surname", Language.getMessage(mstrModuleName, 10, "Surname")))
            iColumn_DetailReport.Add(New IColumn("firstname", Language.getMessage(mstrModuleName, 11, "First Name")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal mdicYearDBName As Dictionary(Of Integer, String) _
                                          , ByVal xUserUnkid As Integer _
                                          , ByVal xCompanyUnkid As Integer _
                                          , ByVal xPeriodStart As Date _
                                          , ByVal xPeriodEnd As Date _
                                          , ByVal xUserModeSetting As String _
                                          , ByVal xOnlyApproved As Boolean _
                                          , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                          , ByVal blnApplyUserAccessFilter As Boolean _
                                          , ByVal strfmtCurrency As String _
                                          , ByVal strUserName As String _
                                          , ByVal intBase_CurrencyId As Integer _
                                          , ByVal xExportReportPath As String _
                                          , ByVal xOpenAfterExport As Boolean _
                                          ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [mdicYearDBName, xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strfmtCurrency, strUserName, intBase_CurrencyId, xExportReportPath, xOpenAfterExport]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrInnerQ As String = ""
        Dim strDatabaseName As String = ""
        Dim dsList As New DataSet
        Dim dtFinalTable As DataTable
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mstrfmtCurrency = strfmtCurrency
            mintBase_CurrencyId = intBase_CurrencyId
            'Sohail (21 Aug 2015) -- End

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            Dim objPeriod As New clscommom_period_Tran

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = mintToPeriodId
            objPeriod._Periodunkid(strDatabaseName) = mintToPeriodId
            'Sohail (21 Aug 2015) -- End
            Dim strEndDate As String = eZeeDate.convertDate(objPeriod._End_Date)



            dtFinalTable = New DataTable("Payroll")
            Dim dCol As DataColumn

            dCol = New DataColumn("employeecode")
            dCol.Caption = "Code"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Surname")
            dCol.Caption = "Surname"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("FirstName")
            dCol.Caption = "First Name"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Initial")
            dCol.Caption = "Initial"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dCol)

            If mintMembershipId > 0 Then
                dCol = New DataColumn("membershipno")
                dCol.Caption = "Membership No"
                dCol.DataType = System.Type.GetType("System.String")
                dCol.DefaultValue = ""
                dtFinalTable.Columns.Add(dCol)
            End If

            For Each strPeriod As String In mstrPeriodID.Split(",")
                objPeriod = New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(strPeriod)
                objPeriod._Periodunkid(strDatabaseName) = CInt(strPeriod)
                'Sohail (21 Aug 2015) -- End

                dCol = New DataColumn("Column" & strPeriod.Trim)
                dCol.Caption = objPeriod._Period_Name
                dCol.DataType = System.Type.GetType("System.Decimal")
                dCol.DefaultValue = 0
                dtFinalTable.Columns.Add(dCol)
            Next
            objPeriod = Nothing

            dCol = New DataColumn("Total")
            dCol.Caption = "Total"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("ID")
            dCol.Caption = "GroupID"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("GName")
            dCol.Caption = "GroupName"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dCol)

            StrQ &= " SELECT   A.employeeunkid  " & _
                            ", A.employeecode " & _
                            ", A.surname " & _
                            ", A.firstname " & _
                            ", A.initial " & _
                            ", SUM(A.amount) AS amount " & _
                            ", A.referenceno " & _
                            ", A.Id " & _
                            ", A.GName " & _
                            ", A.payperiodunkid " & _
                            ", A.period_name " & _
                            ", A.ROWNO " & _
                            ", A.tranheadunkid  "

            If mintMembershipId > 0 Then
                StrQ &= "   , A.membershipunkid " & _
                        "   , A.membershipno "
            End If
            StrQ &= " FROM ( "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'For i = 0 To marrDatabaseName.Count - 1
            '    strDatabaseName = marrDatabaseName(i)
            Dim i As Integer = -1
            For Each key In mdicYearDBName
                strDatabaseName = key.Value
                i += 1
                'Sohail (21 Aug 2015) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , strDatabaseName)
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, strDatabaseName, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, strDatabaseName)
                'Sohail (21 Aug 2015) -- End

                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If

                StrInnerQ &= "SELECT    ISNULL(hremployee_master.employeeunkid, 0) employeeunkid " & _
                                     ", ISNULL(hremployee_master.employeecode, '') employeecode " & _
                                     ", ISNULL(hremployee_master.surname, '') surname " & _
                                     ", ISNULL(hremployee_master.firstname, '') firstname " & _
                                     ", ISNULL(hremployee_master.othername, '') initial " & _
                                     ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) amount " & _
                                     ", cfcommon_period_tran.period_name " & _
                                     ", prtnaleave_tran.payperiodunkid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]

                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                '", ISNULL(prearningdeduction_master.medicalrefno, '') referenceno " & _
                '", DENSE_RANK() OVER ( PARTITION BY prpayrollprocess_tran.employeeunkid, cfcommon_period_tran.end_date ORDER BY edperiod.end_date DESC ) AS ROWNO " & _
                '", ISNULL(prtranhead_master.tranheadunkid,0) tranheadunkid "
                If mintModeId = 99999 Then
                    StrInnerQ &= " , '' referenceno  " & _
                                 ", 1 AS ROWNO " & _
                                 ", prpayrollprocess_tran.activityunkid AS tranheadunkid "
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    StrInnerQ &= " , '' referenceno  " & _
                                ", 1 AS ROWNO " & _
                                ", ISNULL(cmexpense_master.expenseunkid, ISNULL(crretireexpense.expenseunkid, -1)) AS tranheadunkid "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                    'Sohail (23 Jun 2015) -- End
                Else
                    StrInnerQ &= " , ISNULL(prearningdeduction_master.medicalrefno, '') referenceno " & _
                                 ", DENSE_RANK() OVER ( PARTITION BY prpayrollprocess_tran.employeeunkid, cfcommon_period_tran.end_date ORDER BY ISNULL(edperiod.end_date, '" & strEndDate & "') DESC ) AS ROWNO " & _
                                 ", ISNULL(prpayrollprocess_tran.tranheadunkid,0) AS tranheadunkid "
                End If
                'Sohail (10 Sep 2014) -- End

                If mintMembershipId > 0 Then
                    StrInnerQ &= "   , ISNULL(hremployee_meminfo_tran.membershipunkid,0) membershipunkid " & _
                                    ", ISNULL(hremployee_meminfo_tran.membershipno,'') membershipno "

                End If


                If mintViewIndex > 0 Then
                    StrInnerQ &= mstrAnalysis_Fields
                Else
                    StrInnerQ &= ", 0 AS Id, '' AS GName "
                End If

                StrInnerQ &= "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid   " & _
                                        "LEFT JOIN " & strDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid "

                If mintMembershipId > 0 Then

                    StrInnerQ &= "      LEFT JOIN " & strDatabaseName & "..hremployee_Meminfo_tran ON prpayrollprocess_tran.employeeunkid = hremployee_Meminfo_tran.employeeunkid " & _
                                                "AND hremployee_Meminfo_tran.membershipunkid = " & mintMembershipId & _
                                                "AND hremployee_Meminfo_tran.isactive = 1 "

                End If

                StrInnerQ &= "          LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid "

                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                '"JOIN " & mstrTranDatabaseName & "..prearningdeduction_master ON prpayrollprocess_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
                '"AND prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
                '"LEFT JOIN " & mstrTranDatabaseName & "..cfcommon_period_tran AS edperiod ON edperiod.periodunkid = prearningdeduction_master.periodunkid " & _
                '"AND edperiod.modulerefid = " & enModuleReference.Payroll & _
                '                                "AND edperiod.end_date <=  cfcommon_period_tran.end_date " & _
                '                       "LEFT JOIN " & mstrTranDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "
                If mintModeId = 99999 Then
                    'Do Nothing
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    StrInnerQ &= "LEFT JOIN " & strDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                                        "AND prpayrollprocess_tran.crprocesstranunkid > 0 " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmclaim_retirement_master ON cmretire_process_tran.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid " & _
                                        "AND prpayrollprocess_tran.crretirementprocessunkid > 0 "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                    'Sohail (23 Jun 2015) -- End
                Else
                    StrInnerQ &= " LEFT JOIN " & strDatabaseName & "..prearningdeduction_master ON prpayrollprocess_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
                                       "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran AS edperiod ON edperiod.periodunkid = prearningdeduction_master.periodunkid " & _
                                       "LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "
                    'Sohail (12 Jan 2015) -- Start
                    'Issue : Amount is coming multiple times to real amount due to filter in join statement.
                    ' "AND edperiod.modulerefid = " & enModuleReference.Payroll & _
                    '"AND edperiod.end_date <=  cfcommon_period_tran.end_date " & _
                    'Sohail (12 Jan 2015) -- End

                End If
                'Sohail (10 Sep 2014) -- End

                StrInnerQ &= mstrAnalysis_Join

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                If xDateJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrInnerQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrInnerQ &= " WHERE  prtnaleave_tran.isvoid = 0 " & _
                                 " AND prpayrollprocess_tran.isvoid = 0 "
                'Sohail (23 Jun 2015) -- Removed [AND edperiod.modulerefid = " & enModuleReference.Payroll & _ AND edperiod.end_date <=  cfcommon_period_tran.end_date]
                'Sohail (12 Jan 2015) -- [AND edperiod.modulerefid = " & enModuleReference.Payroll & _ AND edperiod.end_date <=  cfcommon_period_tran.end_date]
                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                '" AND prearningdeduction_master.isvoid = 0 " & _
                '" AND ISNULL(prtranhead_master.isvoid, 0) = 0 "
                If mintModeId = 99999 Then
                    'Do Nothing
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    StrInnerQ &= "AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
                                 "AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
                                 "AND ISNULL(cmexpense_master.isactive, 1) = 1 " & _
                                 "AND ISNULL(cmretire_process_tran.isvoid, 0) = 0 " & _
                                 "AND ISNULL(cmclaim_retirement_master.isvoid, 0) = 0 " & _
                                 "AND ISNULL(crretireexpense.isactive, 1) = 1 "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                    'Sohail (23 Jun 2015) -- End
                Else
                    StrInnerQ &= " AND ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
                                 " AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                 " AND edperiod.modulerefid = " & enModuleReference.Payroll & _
                                 " AND edperiod.end_date <=  cfcommon_period_tran.end_date "
                    'Sohail (23 Jun 2015) - [AND edperiod.modulerefid = " & enModuleReference.Payroll & _ AND edperiod.end_date <=  cfcommon_period_tran.end_date]
                End If
                'Sohail (10 Sep 2014) -- End

                If mblnIncludeNegativeHeads = True Then
                    StrInnerQ &= " AND ISNULL(prpayrollprocess_tran.amount, 0) <> 0 "
                Else
                    StrInnerQ &= " AND ISNULL(prpayrollprocess_tran.amount, 0)  > 0 "
                End If

                'Sohail (20 Oct 2014) -- Start
                'Issue : Diffrence with head amount on payslip.
                'StrInnerQ &= " AND   CONVERT(CHAR(8),ISNULL(cfcommon_period_tran.end_date, '" & strEndDate & "'),112) <= '" & strEndDate & "' "
                'Sohail (23 Jun 2015) -- Start
                'Enhancement - Claim Request Expense on E&D Detail Reports.
                'StrInnerQ &= " AND   CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= '" & strEndDate & "' "
                If mintModeId = 99999 Then
                    'Do Nothing
                ElseIf mintModeId = 99998 Then
                    'Do Nothing
                Else
                    StrInnerQ &= " AND   CONVERT(CHAR(8), edperiod.end_date, 112) <= '" & strEndDate & "' "
                End If
                'Sohail (23 Jun 2015) -- End
                'Sohail (20 Oct 2014) -- End

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrInnerQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrInnerQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrInnerQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If i = 0 Then
                    Call FilterTitleAndFilterQuery()
                End If

                StrInnerQ &= Me._FilterQuery

            Next

            StrQ &= StrInnerQ

            StrQ &= "  ) AS A WHERE  ROWNO = 1 " & _
                    "GROUP BY A.employeeunkid " & _
                          ", A.employeecode " & _
                          ", A.surname " & _
                          ", A.firstname " & _
                          ", A.initial " & _
                          ", A.referenceno " & _
                          ", A.Id " & _
                          ", A.GName " & _
                          ", A.payperiodunkid " & _
                          ", A.period_name " & _
                          ", A.ROWNO " & _
                          ", A.tranheadunkid "

            If mintMembershipId > 0 Then
                StrQ &= ", A.membershipunkid " & _
                        ", A.membershipno "
            End If

            'StrQ &= mstrOrderByQuery
            If mintViewIndex > 0 Then
                StrQ &= "ORDER BY GName, employeecode, payperiodunkid "
            Else
                StrQ &= "ORDER BY employeecode, payperiodunkid "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim dtList As DataTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable()

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim objED As New clsEarningDeduction
            Dim strPrevGroup As String = ""
            Dim strPrevKey As String = ""
            Dim decSubTotal As Decimal = 0
            Dim strPrevPeriod As String = ""
            Dim decPeriodTotal As Decimal = 0

            For Each dtRow As DataRow In dtList.Rows
                Dim rpt_Row As DataRow

                'Sohail (03 Aug 2017) -- Start
                'Issue - 69.1 - There is no row at position at 0 when membership is selected.
                'If CInt(dtRow.Item("tranheadunkid")) <> mintHeadID Then
                '    strPrevKey = dtRow.Item("employeecode").ToString
                '    Continue For
                'End If
                'Sohail (03 Aug 2017) -- End

                If strPrevKey <> dtRow.Item("employeecode").ToString Then
                    rpt_Row = dtFinalTable.NewRow

                    rpt_Row.Item("GName") = dtRow.Item("GName")
                    rpt_Row.Item("employeecode") = dtRow.Item("employeecode")
                    rpt_Row.Item("surname") = dtRow.Item("surname")
                    rpt_Row.Item("firstname") = dtRow.Item("firstname")
                    rpt_Row.Item("initial") = dtRow.Item("initial")

                Else
                    rpt_Row = dtFinalTable.Rows(dtFinalTable.Rows.Count - 1)
                End If

                If mintMembershipId > 0 Then
                    rpt_Row.Item("membershipno") = dtRow.Item("membershipno")
                End If

                rpt_Row.Item("Column" & dtRow.Item("payperiodunkid").ToString) = Format(CDec(dtRow("amount")), mstrfmtCurrency)

                'Sohail (03 Aug 2017) -- Start
                'Issue - 69.1 - There is no row at position at 0 when membership is selected.
                'rpt_Row.Item("Total") = CDec(rpt_Row.Item("Total")) + CDec(Format(CDec(dtRow("amount")), mstrfmtCurrency))
                If CInt(dtRow.Item("tranheadunkid")) = mintHeadID Then
                    rpt_Row.Item("Total") = CDec(rpt_Row.Item("Total")) + CDec(Format(CDec(dtRow("amount")), mstrfmtCurrency))
                End If
                'Sohail (03 Aug 2017) -- End

                If strPrevKey <> dtRow.Item("employeecode").ToString Then
                    dtFinalTable.Rows.Add(rpt_Row)
                End If

                strPrevKey = dtRow.Item("employeecode").ToString


            Next

            dtFinalTable.AcceptChanges()

            dtFinalTable = New DataView(dtFinalTable, "", mstrOrderByQuery.Replace("ORDER BY", ""), DataViewRowState.CurrentRows).ToTable

            mdtTableExcel = dtFinalTable

            Dim mintColumn As Integer = 0

            mdtTableExcel.Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 9, "Emp Code")
            mdtTableExcel.Columns("employeecode").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("Surname").Caption = Language.getMessage(mstrModuleName, 10, "Surname")
            mdtTableExcel.Columns("Surname").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("FirstName").Caption = Language.getMessage(mstrModuleName, 11, "First Name")
            mdtTableExcel.Columns("FirstName").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("Initial").Caption = Language.getMessage(mstrModuleName, 12, "Initial")
            mdtTableExcel.Columns("Initial").SetOrdinal(mintColumn)
            mintColumn += 1

            If mintMembershipId > 0 Then
                mdtTableExcel.Columns("membershipno").Caption = Language.getMessage(mstrModuleName, 13, "Membership No")
                mdtTableExcel.Columns("membershipno").SetOrdinal(mintColumn)
                mintColumn += 1
            End If

            For Each strPeriod As String In mstrPeriodID.Split(",")
                mdtTableExcel.Columns("Column" & strPeriod.Trim).Caption = dtFinalTable.Columns("Column" & strPeriod.Trim).Caption
                mdtTableExcel.Columns("Column" & strPeriod.Trim).SetOrdinal(mintColumn)
                mintColumn += 1
            Next

            mdtTableExcel.Columns("Total").Caption = Language.getMessage(mstrModuleName, 8, "Total")
            mdtTableExcel.Columns("Total").SetOrdinal(mintColumn)
            mintColumn += 1

            If mintViewIndex > 0 Then
                mdtTableExcel.Columns("GName").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
                mdtTableExcel.Columns("GName").SetOrdinal(mintColumn)
                mintColumn += 1
            End If


            For i = mintColumn To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(mintColumn)
            Next




            Dim intArrayColumnWidth As Integer() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim blnShowGrandTotal As Boolean = True
            Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
            Dim objDicSubTotal As New Dictionary(Of Integer, Object)
            Dim objDicSubGroupTotal As New Dictionary(Of Integer, Object)
            Dim strarrGroupColumns As String() = Nothing
            Dim strGTotal As String = Language.getMessage(mstrModuleName, 21, "Total Count :")
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell
            Dim arrGroupCount() As String = {Language.getMessage(mstrModuleName, 16, "Period Count:")}
            Dim arrDicGroupExtraInfo() As Dictionary(Of Integer, Object) = Nothing


            If mdtTableExcel IsNot Nothing Then

                Dim intGroupColumn As Integer = 0

                If mintViewIndex > 0 Then
                    intGroupColumn = 0
                    Dim strGrpCols As String() = {"GName"}
                    strarrGroupColumns = strGrpCols
                    Dim arr() As String = {Language.getMessage(mstrModuleName, 20, "Sub Count :")}
                    arrGroupCount = arr

                    objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 14, "Sub Total:"))

                    Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal}
                    arrDicGroupExtraInfo = arrDic
                Else
                    Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal}
                    arrDicGroupExtraInfo = arrDic
                End If

                objDicGrandTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 19, "Grand Total :"))


                ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                For ii As Integer = 0 To intArrayColumnWidth.Length - 1
                    intArrayColumnWidth(ii) = 125
                Next

                '*******   REPORT FOOTER   ******
                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayFooter.Add(row)
                '----------------------


                If ConfigParameter._Object._IsShowCheckedBy = True Then
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Checked By :"), "s8bw")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                    rowsArrayFooter.Add(row)

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)

                End If

                If ConfigParameter._Object._IsShowApprovedBy = True Then

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Approved By :"), "s8bw")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                    rowsArrayFooter.Add(row)

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)

                End If

                If ConfigParameter._Object._IsShowReceivedBy = True Then
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                    rowsArrayFooter.Add(row)

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)
                End If

            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportExecute(objRpt, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", arrGroupCount, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, arrDicGroupExtraInfo, True)
            Call ReportExecute(objRpt, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", arrGroupCount, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, arrDicGroupExtraInfo, True)
            'Sohail (21 Aug 2015) -- End


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try

    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Mode :")
            Language.setMessage(mstrModuleName, 3, "Head :")
            Language.setMessage(mstrModuleName, 5, "Checked By :")
            Language.setMessage(mstrModuleName, 6, "Approved By :")
            Language.setMessage(mstrModuleName, 7, "Received By :")
            Language.setMessage(mstrModuleName, 8, "Total")
            Language.setMessage(mstrModuleName, 9, "Emp Code")
            Language.setMessage(mstrModuleName, 10, "Surname")
            Language.setMessage(mstrModuleName, 11, "First Name")
            Language.setMessage(mstrModuleName, 12, "Initial")
            Language.setMessage(mstrModuleName, 13, "Membership No")
            Language.setMessage(mstrModuleName, 14, "Sub Total:")
            Language.setMessage(mstrModuleName, 15, "Membership :")
            Language.setMessage(mstrModuleName, 16, "Period Count:")
            Language.setMessage(mstrModuleName, 17, "From Period :")
            Language.setMessage(mstrModuleName, 18, "To Period :")
            Language.setMessage(mstrModuleName, 19, "Grand Total :")
            Language.setMessage(mstrModuleName, 20, "Sub Count :")
            Language.setMessage(mstrModuleName, 21, "Total Count :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

   
End Class
