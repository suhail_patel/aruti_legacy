﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanAdvanceSaving
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowPrincipleBFCF = New System.Windows.Forms.CheckBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.chkShowLoanReceipt = New System.Windows.Forms.CheckBox
        Me.chkShowOnHold = New System.Windows.Forms.CheckBox
        Me.chkIgnoreZeroSavingDeduction = New System.Windows.Forms.CheckBox
        Me.chkShowInterestInfo = New System.Windows.Forms.CheckBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnReportTypeSearch = New eZee.Common.eZeeGradientButton
        Me.cboScheme = New System.Windows.Forms.ComboBox
        Me.lblScheme = New System.Windows.Forms.Label
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteriaScheme = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgScheme = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhSchemeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhScheme = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchScheme = New System.Windows.Forms.TextBox
        Me.objbtnSearchBankBranch = New eZee.Common.eZeeGradientButton
        Me.cboBankBranch = New System.Windows.Forms.ComboBox
        Me.LblBankBranch = New System.Windows.Forms.Label
        Me.objbtnSearchBankName = New eZee.Common.eZeeGradientButton
        Me.cboBankName = New System.Windows.Forms.ComboBox
        Me.lblBankName = New System.Windows.Forms.Label
        Me.lblSearchScheme = New System.Windows.Forms.Label
        Me.chkSelectallSchemeList = New System.Windows.Forms.CheckBox
        Me.lblSchemes = New System.Windows.Forms.Label
        Me.radLogo = New System.Windows.Forms.RadioButton
        Me.radCompanyDetails = New System.Windows.Forms.RadioButton
        Me.radLogoCompanyInfo = New System.Windows.Forms.RadioButton
        Me.chkSelectallPeriodList = New System.Windows.Forms.CheckBox
        Me.lblPeriods = New System.Windows.Forms.Label
        Me.lvPeriod = New System.Windows.Forms.ListView
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.colhStart = New System.Windows.Forms.ColumnHeader
        Me.colhEnd = New System.Windows.Forms.ColumnHeader
        Me.gbLogoSettings = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.gbFilterCriteriaScheme.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.dgScheme, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbLogoSettings.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 621)
        Me.NavPanel.Size = New System.Drawing.Size(1186, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowPrincipleBFCF)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.cboCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowLoanReceipt)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowOnHold)
        Me.gbFilterCriteria.Controls.Add(Me.chkIgnoreZeroSavingDeduction)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowInterestInfo)
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboBranch)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(530, 209)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowPrincipleBFCF
        '
        Me.chkShowPrincipleBFCF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowPrincipleBFCF.Location = New System.Drawing.Point(272, 185)
        Me.chkShowPrincipleBFCF.Name = "chkShowPrincipleBFCF"
        Me.chkShowPrincipleBFCF.Size = New System.Drawing.Size(169, 16)
        Me.chkShowPrincipleBFCF.TabIndex = 230
        Me.chkShowPrincipleBFCF.Text = "Show Principle BF/CF"
        Me.chkShowPrincipleBFCF.UseVisualStyleBackColor = True
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(225, 117)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(55, 15)
        Me.lblStatus.TabIndex = 228
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 230
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(286, 114)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(121, 21)
        Me.cboStatus.TabIndex = 227
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(225, 90)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(55, 15)
        Me.lblCurrency.TabIndex = 225
        Me.lblCurrency.Text = "Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 230
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(286, 87)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(121, 21)
        Me.cboCurrency.TabIndex = 3
        '
        'chkShowLoanReceipt
        '
        Me.chkShowLoanReceipt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLoanReceipt.Location = New System.Drawing.Point(272, 163)
        Me.chkShowLoanReceipt.Name = "chkShowLoanReceipt"
        Me.chkShowLoanReceipt.Size = New System.Drawing.Size(169, 16)
        Me.chkShowLoanReceipt.TabIndex = 9
        Me.chkShowLoanReceipt.Text = "Show Receipt/Written Off"
        Me.chkShowLoanReceipt.UseVisualStyleBackColor = True
        '
        'chkShowOnHold
        '
        Me.chkShowOnHold.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowOnHold.Location = New System.Drawing.Point(97, 186)
        Me.chkShowOnHold.Name = "chkShowOnHold"
        Me.chkShowOnHold.Size = New System.Drawing.Size(169, 17)
        Me.chkShowOnHold.TabIndex = 7
        Me.chkShowOnHold.Text = "Show On Hold"
        Me.chkShowOnHold.UseVisualStyleBackColor = True
        '
        'chkIgnoreZeroSavingDeduction
        '
        Me.chkIgnoreZeroSavingDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnoreZeroSavingDeduction.Location = New System.Drawing.Point(97, 163)
        Me.chkIgnoreZeroSavingDeduction.Name = "chkIgnoreZeroSavingDeduction"
        Me.chkIgnoreZeroSavingDeduction.Size = New System.Drawing.Size(169, 17)
        Me.chkIgnoreZeroSavingDeduction.TabIndex = 6
        Me.chkIgnoreZeroSavingDeduction.Text = "Ignore Zero Saving Deduction"
        Me.chkIgnoreZeroSavingDeduction.UseVisualStyleBackColor = True
        '
        'chkShowInterestInfo
        '
        Me.chkShowInterestInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowInterestInfo.Location = New System.Drawing.Point(272, 141)
        Me.chkShowInterestInfo.Name = "chkShowInterestInfo"
        Me.chkShowInterestInfo.Size = New System.Drawing.Size(169, 16)
        Me.chkShowInterestInfo.TabIndex = 8
        Me.chkShowInterestInfo.Text = "Show Interest Information"
        Me.chkShowInterestInfo.UseVisualStyleBackColor = True
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 117)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(84, 15)
        Me.lblBranch.TabIndex = 212
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(413, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 211
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.DropDownWidth = 230
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(98, 114)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(121, 21)
        Me.cboBranch.TabIndex = 4
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(98, 141)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(161, 16)
        Me.chkInactiveemp.TabIndex = 5
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lblReportType
        '
        Me.lblReportType.BackColor = System.Drawing.Color.Transparent
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(84, 15)
        Me.lblReportType.TabIndex = 65
        Me.lblReportType.Text = "Report Type"
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 230
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(98, 33)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(309, 21)
        Me.cboReportType.TabIndex = 0
        '
        'lblEmployee
        '
        Me.lblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(84, 15)
        Me.lblEmployee.TabIndex = 62
        Me.lblEmployee.Text = "Employee"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 230
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(98, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(309, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 90)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(84, 15)
        Me.lblPeriod.TabIndex = 59
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(98, 87)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(121, 21)
        Me.cboPeriod.TabIndex = 2
        '
        'objbtnReportTypeSearch
        '
        Me.objbtnReportTypeSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReportTypeSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnReportTypeSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnReportTypeSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnReportTypeSearch.BorderSelected = False
        Me.objbtnReportTypeSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnReportTypeSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnReportTypeSearch.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnReportTypeSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnReportTypeSearch.Location = New System.Drawing.Point(740, 594)
        Me.objbtnReportTypeSearch.Name = "objbtnReportTypeSearch"
        Me.objbtnReportTypeSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnReportTypeSearch.TabIndex = 214
        Me.objbtnReportTypeSearch.Visible = False
        '
        'cboScheme
        '
        Me.cboScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScheme.DropDownWidth = 230
        Me.cboScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboScheme.FormattingEnabled = True
        Me.cboScheme.Location = New System.Drawing.Point(613, 594)
        Me.cboScheme.Name = "cboScheme"
        Me.cboScheme.Size = New System.Drawing.Size(121, 21)
        Me.cboScheme.TabIndex = 2
        Me.cboScheme.Visible = False
        '
        'lblScheme
        '
        Me.lblScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScheme.Location = New System.Drawing.Point(552, 597)
        Me.lblScheme.Name = "lblScheme"
        Me.lblScheme.Size = New System.Drawing.Size(58, 15)
        Me.lblScheme.TabIndex = 1
        Me.lblScheme.Text = "Name"
        Me.lblScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblScheme.Visible = False
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(545, 182)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(468, 63)
        Me.gbSortBy.TabIndex = 35
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(413, 33)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 35)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(62, 17)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(98, 33)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(309, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'gbFilterCriteriaScheme
        '
        Me.gbFilterCriteriaScheme.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteriaScheme.Checked = False
        Me.gbFilterCriteriaScheme.CollapseAllExceptThis = False
        Me.gbFilterCriteriaScheme.CollapsedHoverImage = Nothing
        Me.gbFilterCriteriaScheme.CollapsedNormalImage = Nothing
        Me.gbFilterCriteriaScheme.CollapsedPressedImage = Nothing
        Me.gbFilterCriteriaScheme.CollapseOnLoad = False
        Me.gbFilterCriteriaScheme.Controls.Add(Me.pnlEmployeeList)
        Me.gbFilterCriteriaScheme.Controls.Add(Me.objbtnSearchBankBranch)
        Me.gbFilterCriteriaScheme.Controls.Add(Me.cboBankBranch)
        Me.gbFilterCriteriaScheme.Controls.Add(Me.LblBankBranch)
        Me.gbFilterCriteriaScheme.Controls.Add(Me.objbtnSearchBankName)
        Me.gbFilterCriteriaScheme.Controls.Add(Me.cboBankName)
        Me.gbFilterCriteriaScheme.Controls.Add(Me.lblBankName)
        Me.gbFilterCriteriaScheme.Controls.Add(Me.lblSearchScheme)
        Me.gbFilterCriteriaScheme.Controls.Add(Me.chkSelectallSchemeList)
        Me.gbFilterCriteriaScheme.Controls.Add(Me.lblSchemes)
        Me.gbFilterCriteriaScheme.ExpandedHoverImage = Nothing
        Me.gbFilterCriteriaScheme.ExpandedNormalImage = Nothing
        Me.gbFilterCriteriaScheme.ExpandedPressedImage = Nothing
        Me.gbFilterCriteriaScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteriaScheme.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteriaScheme.HeaderHeight = 25
        Me.gbFilterCriteriaScheme.HeaderMessage = ""
        Me.gbFilterCriteriaScheme.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteriaScheme.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteriaScheme.HeightOnCollapse = 0
        Me.gbFilterCriteriaScheme.LeftTextSpace = 0
        Me.gbFilterCriteriaScheme.Location = New System.Drawing.Point(9, 281)
        Me.gbFilterCriteriaScheme.Name = "gbFilterCriteriaScheme"
        Me.gbFilterCriteriaScheme.OpenHeight = 300
        Me.gbFilterCriteriaScheme.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteriaScheme.ShowBorder = True
        Me.gbFilterCriteriaScheme.ShowCheckBox = False
        Me.gbFilterCriteriaScheme.ShowCollapseButton = False
        Me.gbFilterCriteriaScheme.ShowDefaultBorderColor = True
        Me.gbFilterCriteriaScheme.ShowDownButton = False
        Me.gbFilterCriteriaScheme.ShowHeader = True
        Me.gbFilterCriteriaScheme.Size = New System.Drawing.Size(530, 280)
        Me.gbFilterCriteriaScheme.TabIndex = 36
        Me.gbFilterCriteriaScheme.Temp = 0
        Me.gbFilterCriteriaScheme.Text = "Filter Criteria"
        Me.gbFilterCriteriaScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgScheme)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearchScheme)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(5, 29)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(214, 248)
        Me.pnlEmployeeList.TabIndex = 228
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgScheme
        '
        Me.dgScheme.AllowUserToAddRows = False
        Me.dgScheme.AllowUserToDeleteRows = False
        Me.dgScheme.AllowUserToResizeRows = False
        Me.dgScheme.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgScheme.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgScheme.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgScheme.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgScheme.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgScheme.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhunkid, Me.dgColhSchemeCode, Me.dgColhScheme})
        Me.dgScheme.Location = New System.Drawing.Point(1, 28)
        Me.dgScheme.Name = "dgScheme"
        Me.dgScheme.RowHeadersVisible = False
        Me.dgScheme.RowHeadersWidth = 5
        Me.dgScheme.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgScheme.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgScheme.Size = New System.Drawing.Size(210, 217)
        Me.dgScheme.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhunkid
        '
        Me.objdgcolhunkid.HeaderText = "unkid"
        Me.objdgcolhunkid.Name = "objdgcolhunkid"
        Me.objdgcolhunkid.ReadOnly = True
        Me.objdgcolhunkid.Visible = False
        '
        'dgColhSchemeCode
        '
        Me.dgColhSchemeCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhSchemeCode.HeaderText = "Code"
        Me.dgColhSchemeCode.Name = "dgColhSchemeCode"
        Me.dgColhSchemeCode.ReadOnly = True
        Me.dgColhSchemeCode.Width = 70
        '
        'dgColhScheme
        '
        Me.dgColhScheme.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dgColhScheme.HeaderText = "Scheme Name"
        Me.dgColhScheme.Name = "dgColhScheme"
        Me.dgColhScheme.ReadOnly = True
        Me.dgColhScheme.Width = 99
        '
        'txtSearchScheme
        '
        Me.txtSearchScheme.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchScheme.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchScheme.Location = New System.Drawing.Point(1, 1)
        Me.txtSearchScheme.Name = "txtSearchScheme"
        Me.txtSearchScheme.Size = New System.Drawing.Size(210, 21)
        Me.txtSearchScheme.TabIndex = 12
        '
        'objbtnSearchBankBranch
        '
        Me.objbtnSearchBankBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBankBranch.BorderSelected = False
        Me.objbtnSearchBankBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBankBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchBankBranch.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBankBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBankBranch.Location = New System.Drawing.Point(503, 56)
        Me.objbtnSearchBankBranch.Name = "objbtnSearchBankBranch"
        Me.objbtnSearchBankBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBankBranch.TabIndex = 226
        '
        'cboBankBranch
        '
        Me.cboBankBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankBranch.DropDownWidth = 350
        Me.cboBankBranch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankBranch.FormattingEnabled = True
        Me.cboBankBranch.Location = New System.Drawing.Point(371, 56)
        Me.cboBankBranch.Name = "cboBankBranch"
        Me.cboBankBranch.Size = New System.Drawing.Size(126, 21)
        Me.cboBankBranch.TabIndex = 225
        '
        'LblBankBranch
        '
        Me.LblBankBranch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblBankBranch.Location = New System.Drawing.Point(262, 60)
        Me.LblBankBranch.Name = "LblBankBranch"
        Me.LblBankBranch.Size = New System.Drawing.Size(103, 13)
        Me.LblBankBranch.TabIndex = 224
        Me.LblBankBranch.Text = "Bank Branch"
        '
        'objbtnSearchBankName
        '
        Me.objbtnSearchBankName.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBankName.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankName.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankName.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBankName.BorderSelected = False
        Me.objbtnSearchBankName.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBankName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchBankName.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBankName.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBankName.Location = New System.Drawing.Point(503, 29)
        Me.objbtnSearchBankName.Name = "objbtnSearchBankName"
        Me.objbtnSearchBankName.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBankName.TabIndex = 222
        '
        'cboBankName
        '
        Me.cboBankName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankName.DropDownWidth = 350
        Me.cboBankName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankName.FormattingEnabled = True
        Me.cboBankName.Location = New System.Drawing.Point(371, 29)
        Me.cboBankName.Name = "cboBankName"
        Me.cboBankName.Size = New System.Drawing.Size(126, 21)
        Me.cboBankName.TabIndex = 216
        '
        'lblBankName
        '
        Me.lblBankName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankName.Location = New System.Drawing.Point(262, 33)
        Me.lblBankName.Name = "lblBankName"
        Me.lblBankName.Size = New System.Drawing.Size(103, 13)
        Me.lblBankName.TabIndex = 215
        Me.lblBankName.Text = "Emp. Bank Name"
        '
        'lblSearchScheme
        '
        Me.lblSearchScheme.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchScheme.Location = New System.Drawing.Point(234, 243)
        Me.lblSearchScheme.Name = "lblSearchScheme"
        Me.lblSearchScheme.Size = New System.Drawing.Size(112, 13)
        Me.lblSearchScheme.TabIndex = 7
        Me.lblSearchScheme.Text = "Search Scheme"
        Me.lblSearchScheme.Visible = False
        '
        'chkSelectallSchemeList
        '
        Me.chkSelectallSchemeList.AutoSize = True
        Me.chkSelectallSchemeList.Location = New System.Drawing.Point(286, 171)
        Me.chkSelectallSchemeList.Name = "chkSelectallSchemeList"
        Me.chkSelectallSchemeList.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectallSchemeList.TabIndex = 1
        Me.chkSelectallSchemeList.UseVisualStyleBackColor = True
        Me.chkSelectallSchemeList.Visible = False
        '
        'lblSchemes
        '
        Me.lblSchemes.BackColor = System.Drawing.Color.Transparent
        Me.lblSchemes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSchemes.Location = New System.Drawing.Point(283, 141)
        Me.lblSchemes.Name = "lblSchemes"
        Me.lblSchemes.Size = New System.Drawing.Size(103, 13)
        Me.lblSchemes.TabIndex = 0
        Me.lblSchemes.Text = "Scheme"
        Me.lblSchemes.Visible = False
        '
        'radLogo
        '
        Me.radLogo.AutoSize = True
        Me.radLogo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLogo.Location = New System.Drawing.Point(98, 82)
        Me.radLogo.Name = "radLogo"
        Me.radLogo.Size = New System.Drawing.Size(48, 17)
        Me.radLogo.TabIndex = 236
        Me.radLogo.Text = "Logo"
        Me.radLogo.UseVisualStyleBackColor = True
        '
        'radCompanyDetails
        '
        Me.radCompanyDetails.AutoSize = True
        Me.radCompanyDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCompanyDetails.Location = New System.Drawing.Point(98, 58)
        Me.radCompanyDetails.Name = "radCompanyDetails"
        Me.radCompanyDetails.Size = New System.Drawing.Size(105, 17)
        Me.radCompanyDetails.TabIndex = 235
        Me.radCompanyDetails.Text = "Company Details"
        Me.radCompanyDetails.UseVisualStyleBackColor = True
        '
        'radLogoCompanyInfo
        '
        Me.radLogoCompanyInfo.AutoSize = True
        Me.radLogoCompanyInfo.Checked = True
        Me.radLogoCompanyInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLogoCompanyInfo.Location = New System.Drawing.Point(98, 34)
        Me.radLogoCompanyInfo.Name = "radLogoCompanyInfo"
        Me.radLogoCompanyInfo.Size = New System.Drawing.Size(152, 17)
        Me.radLogoCompanyInfo.TabIndex = 234
        Me.radLogoCompanyInfo.TabStop = True
        Me.radLogoCompanyInfo.Text = "Logo and Company Details"
        Me.radLogoCompanyInfo.UseVisualStyleBackColor = True
        '
        'chkSelectallPeriodList
        '
        Me.chkSelectallPeriodList.AutoSize = True
        Me.chkSelectallPeriodList.Location = New System.Drawing.Point(740, 366)
        Me.chkSelectallPeriodList.Name = "chkSelectallPeriodList"
        Me.chkSelectallPeriodList.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectallPeriodList.TabIndex = 4
        Me.chkSelectallPeriodList.UseVisualStyleBackColor = True
        Me.chkSelectallPeriodList.Visible = False
        '
        'lblPeriods
        '
        Me.lblPeriods.BackColor = System.Drawing.Color.Transparent
        Me.lblPeriods.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriods.Location = New System.Drawing.Point(596, 367)
        Me.lblPeriods.Name = "lblPeriods"
        Me.lblPeriods.Size = New System.Drawing.Size(103, 13)
        Me.lblPeriods.TabIndex = 3
        Me.lblPeriods.Text = "Period"
        Me.lblPeriods.Visible = False
        '
        'lvPeriod
        '
        Me.lvPeriod.CheckBoxes = True
        Me.lvPeriod.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.colhStart, Me.colhEnd})
        Me.lvPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPeriod.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvPeriod.Location = New System.Drawing.Point(596, 386)
        Me.lvPeriod.Name = "lvPeriod"
        Me.lvPeriod.ShowItemToolTips = True
        Me.lvPeriod.Size = New System.Drawing.Size(159, 196)
        Me.lvPeriod.TabIndex = 1
        Me.lvPeriod.UseCompatibleStateImageBehavior = False
        Me.lvPeriod.View = System.Windows.Forms.View.Details
        Me.lvPeriod.Visible = False
        '
        'colhStart
        '
        Me.colhStart.Width = 0
        '
        'colhEnd
        '
        Me.colhEnd.Width = 0
        '
        'gbLogoSettings
        '
        Me.gbLogoSettings.BorderColor = System.Drawing.Color.Black
        Me.gbLogoSettings.Checked = False
        Me.gbLogoSettings.CollapseAllExceptThis = False
        Me.gbLogoSettings.CollapsedHoverImage = Nothing
        Me.gbLogoSettings.CollapsedNormalImage = Nothing
        Me.gbLogoSettings.CollapsedPressedImage = Nothing
        Me.gbLogoSettings.CollapseOnLoad = False
        Me.gbLogoSettings.Controls.Add(Me.radLogo)
        Me.gbLogoSettings.Controls.Add(Me.radLogoCompanyInfo)
        Me.gbLogoSettings.Controls.Add(Me.radCompanyDetails)
        Me.gbLogoSettings.ExpandedHoverImage = Nothing
        Me.gbLogoSettings.ExpandedNormalImage = Nothing
        Me.gbLogoSettings.ExpandedPressedImage = Nothing
        Me.gbLogoSettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLogoSettings.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLogoSettings.HeaderHeight = 25
        Me.gbLogoSettings.HeaderMessage = ""
        Me.gbLogoSettings.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLogoSettings.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLogoSettings.HeightOnCollapse = 0
        Me.gbLogoSettings.LeftTextSpace = 0
        Me.gbLogoSettings.Location = New System.Drawing.Point(545, 66)
        Me.gbLogoSettings.Name = "gbLogoSettings"
        Me.gbLogoSettings.OpenHeight = 300
        Me.gbLogoSettings.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLogoSettings.ShowBorder = True
        Me.gbLogoSettings.ShowCheckBox = False
        Me.gbLogoSettings.ShowCollapseButton = False
        Me.gbLogoSettings.ShowDefaultBorderColor = True
        Me.gbLogoSettings.ShowDownButton = False
        Me.gbLogoSettings.ShowHeader = True
        Me.gbLogoSettings.Size = New System.Drawing.Size(468, 110)
        Me.gbLogoSettings.TabIndex = 215
        Me.gbLogoSettings.Temp = 0
        Me.gbLogoSettings.Text = "Logo Settings"
        Me.gbLogoSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmLoanAdvanceSaving
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1186, 676)
        Me.Controls.Add(Me.gbLogoSettings)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbFilterCriteriaScheme)
        Me.Controls.Add(Me.chkSelectallPeriodList)
        Me.Controls.Add(Me.objbtnReportTypeSearch)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.lblPeriods)
        Me.Controls.Add(Me.lblScheme)
        Me.Controls.Add(Me.cboScheme)
        Me.Controls.Add(Me.lvPeriod)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanAdvanceSaving"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Loan Advance Saving Report"
        Me.Controls.SetChildIndex(Me.lvPeriod, 0)
        Me.Controls.SetChildIndex(Me.cboScheme, 0)
        Me.Controls.SetChildIndex(Me.lblScheme, 0)
        Me.Controls.SetChildIndex(Me.lblPeriods, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.Controls.SetChildIndex(Me.objbtnReportTypeSearch, 0)
        Me.Controls.SetChildIndex(Me.chkSelectallPeriodList, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteriaScheme, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbLogoSettings, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbFilterCriteriaScheme.ResumeLayout(False)
        Me.gbFilterCriteriaScheme.PerformLayout()
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.dgScheme, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbLogoSettings.ResumeLayout(False)
        Me.gbLogoSettings.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblScheme As System.Windows.Forms.Label
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Private WithEvents lblEmployee As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Private WithEvents lblReportType As System.Windows.Forms.Label
    Public WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents objbtnReportTypeSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents chkShowInterestInfo As System.Windows.Forms.CheckBox
    Friend WithEvents chkIgnoreZeroSavingDeduction As System.Windows.Forms.CheckBox
    Public WithEvents gbFilterCriteriaScheme As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblSearchScheme As System.Windows.Forms.Label
    Friend WithEvents chkSelectallPeriodList As System.Windows.Forms.CheckBox
    Private WithEvents lblPeriods As System.Windows.Forms.Label
    Friend WithEvents lvPeriod As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStart As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEnd As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkSelectallSchemeList As System.Windows.Forms.CheckBox
    Private WithEvents lblSchemes As System.Windows.Forms.Label
    Friend WithEvents chkShowLoanReceipt As System.Windows.Forms.CheckBox
    Friend WithEvents cboBankName As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankName As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchBankName As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchBankBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboBankBranch As System.Windows.Forms.ComboBox
    Friend WithEvents LblBankBranch As System.Windows.Forms.Label
    Friend WithEvents chkShowOnHold As System.Windows.Forms.CheckBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents radLogo As System.Windows.Forms.RadioButton
    Friend WithEvents radCompanyDetails As System.Windows.Forms.RadioButton
    Friend WithEvents radLogoCompanyInfo As System.Windows.Forms.RadioButton
    Friend WithEvents gbLogoSettings As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkShowPrincipleBFCF As System.Windows.Forms.CheckBox
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgScheme As System.Windows.Forms.DataGridView
    Private WithEvents txtSearchScheme As System.Windows.Forms.TextBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhSchemeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhScheme As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
