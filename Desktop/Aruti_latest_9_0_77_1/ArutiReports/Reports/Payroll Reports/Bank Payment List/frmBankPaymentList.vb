#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib

#End Region


Public Class frmBankPaymentList
#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmBankPaymentList"
    Private objBankPaymentList As clsBankPaymentList
    Private objBank As clspayrollgroup_master
    Private objCountry As clsMasterData
    Private objEmp As clsEmployee_Master
    Private objBankBranch As clsbankbranch_master
    Private objPeriod As clscommom_period_Tran
    Private objCurrentPeriodId As clsMasterData
    'Private mintCurrentPeriodId As Integer = 0 'Sohail (22 Mar 2013)
    'Sohail (08 Nov 2011) -- Start
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    'Sohail (08 Nov 2011) -- End

    'Sohail (26 Nov 2011) -- Start
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (26 Nov 2011) -- End
    Private mstrAnalysis_OrderBy_GroupName As String = "" 'Sohail (24 Jan 2013)

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End


    'Anjan [14 December 2016] -- Start
    'ENHANCEMENT : Including loan parameter in T24.
    Private objTransactionHead As clsTransactionHead
    'Anjan [14 December 2016] -- End
    'Sohail (12 Feb 2018) -- Start
    'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
    Private mstrSearchText As String = String.Empty
    'Sohail (12 Feb 2018) -- End


    Private mintFirstOpenPeriod As Integer = 0 'Sohail (22 Mar 2013)
#End Region

#Region " Constructor "
    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        objBankPaymentList = New clsBankPaymentList(User._Object._Languageunkid,Company._Object._Companyunkid)
        objBankPaymentList.SetDefaultValue()
        ' Add any initialization after the InitializeComponent() call.

        _Show_ExcelExtra_Menu = True 'Sohail (08 Dec 2012)

        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End

    End Sub
#End Region

    'Sohail (15 Mar 2014) -- Start
    'Enhancement - Save Selection.
#Region " Private Enum "
    Private Enum enHeadTypeId
        Company_Bank = 1
        Company_Branch = 2
        Company_Account_No = 3
        Report_Mode = 4
        Employee_Bank = 5
        Employee_Branch = 6
        Country = 7
        Paid_Currency = 8
        Show_Signatory_1 = 9
        Show_Signatory_2 = 10
        Show_Signatory_3 = 11
        Show_Defined_Signatory_1 = 12
        Show_Defined_Signatory_2 = 13
        Show_Defined_Signatory_3 = 14
        Show_Bank_Branch_Group = 15
        Show_Employee_Sign = 16
        Include_Inactive_Employee = 17
        Show_Period = 18
        Show_Sort_Code = 19
        Show_Separate_FName_Surname = 20
        Show_Bank_Code = 21
        Show_Branch_Code = 22
        Show_Account_Type = 23

        'Anjan [28 Mar 2014] -- Start
        'ENHANCEMENT : Requested by Rutta
        show_employee_code = 24
        'Anjan [28 Mar 2014 ] -- End

        'Anjan [14 December 2016] -- Start
        'ENHANCEMENT : Including loan parameter in T24.
        show_Loan_Head = 25
        'Anjan [14 December 2016] -- End

        'Shani(28-FEB-2017) -- Start
        'Enhancement - Add New Bank Payment List Integration for TAMA
        Letterhead = 26
        Address_to_Employee_Bank = 27
        'Shani(28-FEB-2017) -- End

        'Sohail (12 Feb 2018) -- Start
        'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
        Present_Days = 28
        Basic_Salary = 29
        Social_Security = 30
        'Sohail (12 Feb 2018) -- End
        'Sohail (16 Apr 2018) -- Start
        'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
        ExtraIncome = 31
        AbsentDays = 32
        IdentityType = 33
        PaymentType = 34
        'Sohail (16 Apr 2018) -- End
        'Sohail (19 Apr 2018) -- Start
        'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
        CustomCurrencyFormat = 35
        'Sohail (19 Apr 2018) -- End

        'Hemant (19 June 2019) -- Start
        'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
        Show_Selected_Bank_Info = 36
        'Hemant (19 June 2019) -- End
        'Hemant 20 Jul 2019) -- Start
        'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
        Show_Company_Group_Info = 37
        'Hemant 20 Jul 2019) -- End
        'Hemant (25 Jul 2019) -- Start
        'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
        Show_Company_Logo = 38
        'Hemant (25 Jul 2019) -- End
        'Hemant (16 Jul 2020) -- Start
        'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
        Save_As_TXT = 39
        'Hemant (16 Jul 2020) -- End
        'Sohail (08 Jul 2021) -- Start
        'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
        Membership = 40
        'Sohail (08 Jul 2021) -- End
    End Enum
#End Region
    'Sohail (15 Mar 2014) -- End

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objExRate As New clsExchangeRate 'Sohail (16 Dec 2011)
        Dim objCompanyBank As New clsCompany_Bank_tran 'Sohail (16 Jul 2012)
        Dim objMaster As New clsMasterData 'Nilay (10-Nov-2016)
        Dim objMembership As New clsmembership_master 'Sohail (08 Jul 2021)

        'Sohail (25 Jan 2013) -- Start
        'TRA - ENHANCEMENT
        'Dim objMember As New clsmembership_master 'Sohail (14 Mar 2013)
        Dim itm As ComboBoxValue
        'Sohail (25 Jan 2013) -- End

        Try
            objEmp = New clsEmployee_Master
            objBank = New clspayrollgroup_master
            objCountry = New clsMasterData
            objPeriod = New clscommom_period_Tran
            objCurrentPeriodId = New clsMasterData
            objTransactionHead = New clsTransactionHead

            'Anjan [12 December 2016] -- Start
            'ENHANCEMENT : Including NEW tables in close year.

            'Anjan [12 December 2016] -- End


            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                              User._Object._Userunkid, _
            '                              FinancialYear._Object._YearUnkid, _
            '                              Company._Object._Companyunkid, _
            '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                              ConfigParameter._Object._UserAccessModeSetting, _
            '                              True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, True, "Emp", True)
            'Nilay (10-Feb-2016) -- End

            'Anjan [10 June 2015] -- End

            With cboEmployeeName
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With


            dsList = objBank.getListForCombo(enPayrollGroupType.Bank, "List", True)
            With cboBankName
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objCountry.getCountryList("List", True)
            With cboCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'Sohail (22 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'mintCurrentPeriodId = objCountry.getCurrentPeriodID(enModuleReference.Payroll, ConfigParameter._Object._CurrentDateAndTime, 1)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objCountry.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objCountry.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (22 Mar 2013) -- End
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                'Sohail (22 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                '.SelectedValue = mintCurrentPeriodId
                .SelectedValue = mintFirstOpenPeriod
                'Sohail (22 Mar 2013) -- End
            End With

            'Sohail (16 Dec 2011) -- Start
            dsList = objExRate.getComboList("Currency", True)
            With cboCurrency
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (03 Sep 2012) -- End
                .DisplayMember = "currency_name"
                .DataSource = dsList.Tables("Currency")
                .SelectedValue = 0
            End With
            'Sohail (16 Dec 2011) -- End

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            dsList = objMaster.GetCondition(False, False, True, True, False)
            With cboCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedIndex = 0
            End With
            'Nilay (10-Nov-2016) -- End

            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            cboReportType.Items.Clear()
            'cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Bank Payment List")) 'Sohail (25 Jan 2013)
            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT - Change report name to Electronic Funds Transfer
            'cboReportType.Items.Add(Language.getMessage(mstrModuleName, 5, "Credit Advice Schedule"))
            'cboReportType.Items.Add(Language.getMessage(mstrModuleName, 3, "Electronic Funds Transfer")) 'Sohail (25 Jan 2013)
            'Sohail (16 Jul 2012) -- End

            'Sohail (10 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (25 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'If ConfigParameter._Object._AccountingSoftWare = enIntegration.StandardBank Then
            '    cboReportType.Items.Add(Language.getMessage(mstrModuleName, 10, "EFT Standard Bank"))
            'End If
            'Sohail (25 Jan 2013) -- End
            'Sohail (10 Jan 2013) -- End

            'Sohail (25 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (14 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'dsList = objMember.getListForCombo("Membership", True)
            'With cboMembership
            '    .ValueMember = "membershipunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = 0
            'End With
            'Sohail (14 Mar 2013) -- End

            itm = New ComboBoxValue(Language.getMessage(mstrModuleName, 2, "Bank Payment List"), CInt(enBankPaymentReport.Bank_payment_List))
            cboReportType.Items.Add(itm)

            itm = New ComboBoxValue(Language.getMessage(mstrModuleName, 3, "Electronic Funds Transfer"), CInt(enBankPaymentReport.Electronic_Fund_Transfer))
            cboReportType.Items.Add(itm)

            If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_StandardBank Then
                itm = New ComboBoxValue(Language.getMessage(mstrModuleName, 10, "EFT Standard Bank"), CInt(enBankPaymentReport.EFT_Standard_Bank))
                cboReportType.Items.Add(itm)
            End If

            'Sohail (14 Mar 2013) -- Start
            'TRA - ENHANCEMENT - Moved to PSPF Statutory Report
            'If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_PSPF Then
            '    itm = New ComboBoxValue(Language.getMessage(mstrModuleName, 11, "EFT PSPF"), CInt(enBankPaymentReport.EFT_PSPF))
            '    cboReportType.Items.Add(itm)
            'End If
            'Sohail (14 Mar 2013) -- End

            'Anjan [ 05 Feb 2013 ] -- Start
            'ENHANCEMENT : TRA CHANGES
            If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_StandardCharteredBank Then
                itm = New ComboBoxValue(Language.getMessage(mstrModuleName, 12, "EFT Standard Chartered Bank"), CInt(enBankPaymentReport.EFT_StandardCharteredBank))
                cboReportType.Items.Add(itm)
            End If

            If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_SFIBank Then
                itm = New ComboBoxValue(Language.getMessage(mstrModuleName, 13, "EFT SFI Bank"), CInt(enBankPaymentReport.EFT_SFIBANK))
                cboReportType.Items.Add(itm)
            End If

            'Anjan [ 05 Feb 2013 ] -- End


            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            itm = New ComboBoxValue(Language.getMessage(mstrModuleName, 20, "Bank Payment Voucher Report"), CInt(enBankPaymentReport.Bank_Payment_Voucher))
            cboReportType.Items.Add(itm)
            'Pinkal (12-Jun-2014) -- End
            'Sohail (25 Jan 2013) -- End

            'Sohail (25 Mar 2015) -- Start
            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance Payment.
            itm = New ComboBoxValue(Language.getMessage(mstrModuleName, 21, "EFT Advance Salary CSV"), CInt(enBankPaymentReport.EFT_ADVANCE_SALARY_CSV))
            cboReportType.Items.Add(itm)

            itm = New ComboBoxValue(Language.getMessage(mstrModuleName, 22, "EFT Advance Salary XSV"), CInt(enBankPaymentReport.EFT_ADVANCE_SALARY_XLS))
            cboReportType.Items.Add(itm)
            'Sohail (25 Mar 2015) -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            If Company._Object._Countryunkid = 162 Then 'OMAN Salary Format WPS
                itm = New ComboBoxValue(Language.getMessage(mstrModuleName, 27, "Salary Format WPS"), CInt(enBankPaymentReport.Salary_Format_WPS))
                cboReportType.Items.Add(itm)
            End If
            'Sohail (12 Feb 2018) -- End

            cboReportType.SelectedIndex = 0
            'Pinkal (11-MAY-2012) -- End

            'Sohail (02 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            With cboReportMode
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 8, "DRAFT"))
                .Items.Add(Language.getMessage(mstrModuleName, 9, "AUTHORIZED"))
                .SelectedIndex = 0
            End With
            'Sohail (02 Jul 2012) -- End

            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            dsList = objCompanyBank.GetComboList(Company._Object._Companyunkid, 1, "Bank")
            With cboCompanyBankName
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Bank")
                .SelectedValue = 0
            End With
            'Sohail (16 Jul 2012) -- End

            'Anjan [14 December 2016] -- Start
            'ENHANCEMENT : Including loan parameter in T24.
            dsList = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "LoanHead", True)
            With cboHeads
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("LoanHead")
                .SelectedValue = 0
            End With
            'Anjan [14 December 2016] -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            dsList = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, CInt(enTranHeadType.Informational))
            With cboPresentDays
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            'dsList = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, CInt(enTranHeadType.EarningForEmployees))
            dsList = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "List", True)
            'Sohail (16 Apr 2018) -- End
            With cboBasicSalary
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            
            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            With cboExtraIncome
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
            End With

            With cboAbsentDays
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
            End With
            'Sohail (16 Apr 2018) -- End

            dsList = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, , , , , , "trnheadtype_id IN (" & CInt(enTranHeadType.DeductionForEmployee) & ", " & CInt(enTranHeadType.EmployeesStatutoryDeductions) & ")")
            With cboSocialSecurity
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'Sohail (12 Feb 2018) -- End

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            dsList = objMembership.getListForCombo("Membership", True)
            With cboMembershipRepo
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Membership")
                .SelectedValue = 0
            End With
            'Sohail (08 Jul 2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally

            dsList.Dispose()
            dsList = Nothing

            objEmp = Nothing
            objBank = Nothing
            objCountry = Nothing
            objPeriod = Nothing
            objCurrentPeriodId = Nothing
            objExRate = Nothing 'Sohail (16 Dec 2011)
            'objMember = Nothing 'Sohail (25 Jan 2013) 'Sohail (14 Mar 2013)
            objMembership = Nothing 'Sohail (08 Jul 2021)
        End Try

    End Sub

    Private Sub ResetValue()

        Try
            cboBankName.SelectedValue = 0
            cboBranchName.SelectedValue = 0
            cboCountry.SelectedValue = 0
            cboEmployeeName.SelectedValue = 0
            'Sohail (22 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'cboPeriod.SelectedValue = mintCurrentPeriodId
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            'Sohail (22 Mar 2013) -- End
            txtEmpcode.Text = ""
            txtAmount.Text = ""
            txtAmountTo.Text = ""

            'Anjan (04 Apr 2011)-Start
            chkEmployeeSign.CheckState = CheckState.Unchecked
            chkSignatory1.CheckState = CheckState.Unchecked
            chkSignatory2.CheckState = CheckState.Unchecked
            chkSignatory3.CheckState = CheckState.Unchecked
            chkDefinedSignatory.Checked = False
            'Anjan (04 Apr 2011)-End
            chkShowFNameSeparately.Checked = False 'Sohail (21 Mar 2013)
            'Sohail (24 Dec 2013) -- Start
            'Enhancement - Oman
            If chkShowPayrollPeriod.Enabled = True Then
                chkShowPayrollPeriod.Checked = True
            Else
                chkShowPayrollPeriod.Checked = False
            End If
            If chkShowSortCode.Enabled = True Then
                '    chkShowSortCode.Checked = True
                'Else
                chkShowSortCode.Checked = False
            End If
            'Sohail (24 Dec 2013) -- End

            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'objBankPaymentList.setDefaultOrderBy(2)

            'Sohail (25 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'objBankPaymentList.setDefaultOrderBy(CInt(cboReportType.SelectedIndex))
            objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value))
            'Sohail (25 Jan 2013) -- End

            'Sohail (16 Jul 2012) -- End
            txtOrderBy.Text = objBankPaymentList.OrderByDisplay


            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkIncludeInactiveEmp.Checked = True
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            'Sohail (08 Nov 2011) -- Start
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            'Sohail (08 Nov 2011) -- End

            'Sohail (26 Nov 2011) -- Start
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (26 Nov 2011) -- End
            mstrAnalysis_OrderBy_GroupName = "" 'Sohail (24 Jan 2013)

            'Sohail (16 Dec 2011) -- Start
            cboCurrency.SelectedValue = 0
            chkShowGroupByBankBranch.Checked = False
            'Sohail (16 Dec 2011) -- End

            cboReportMode.SelectedIndex = 0 'Sohail (02 Jul 2012)

            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            cboCompanyBankName.SelectedValue = 0
            cboCompanyBranchName.SelectedValue = 0
            chkIncludeInactiveEmp.Checked = True
            chkIncludeInactiveEmp.Visible = False 'TRA -  if we give this Option there is a Chance of Skipping Paid employee
            'Sohail (16 Jul 2012) -- End
            cboCompanyAccountNo.SelectedValue = 0 'Sohail (21 Jul 2012)
            'cboMembership.SelectedValue = 0 'Sohail (25 Jan 2013) 'Sohail (14 Mar 2013)

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End


            'Anjan [03 Feb 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            chkShowBankCode.Checked = False
            chkShowBranchCode.Checked = False
            chkDefinedSignatory2.Checked = False
            chkDefinedSignatory3.Checked = False

            'Anjan [03 Feb 2014 ] -- End
            txtCutOffAmount.Text = "" 'Sohail (15 Mar 2014)
            chkShowAccountType.Checked = False  'Sohail (24 Mar 2014)
            chkShowReportHeader.Checked = True 'Sohail (01 Apr 2014)
            chkShowSelectedBankInfo.Checked = False 'Hemant (19 June 2019)
            chkShowCompanyGrpInfo.Checked = False 'Hemant (20 Jul 2019)
            chkShowCompanyLogoOnReport.Checked = False 'Hemant (25 Jul 2019)
            'Anjan [28 Mar 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            chkShowEmployeeCode.Checked = True
            'Anjan [28 Mar 2014 ] -- End

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            cboCondition.SelectedIndex = 0
            'Nilay (10-Nov-2016) -- End


            'Anjan [21 November 2016] -- Start
            'ENHANCEMENT : Including NEW ACB EFT integration report.
            txtID.Text = ""
            txtPasscode.Text = ""
            txtLink.Text = "http://172.29.1.64:9763/services/T24001/aruthe?"
            pnl_ID_Passcode.Visible = False
            'Anjan [21 November 2016] -- End

            'Anjan [14 December 2016] -- Start
            'ENHANCEMENT : Including loan parameter in T24.
            cboHeads.SelectedIndex = 0
            'Anjan [14 December 2016] -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            cboPresentDays.SelectedValue = 0
            cboBasicSalary.SelectedValue = 0
            cboSocialSecurity.SelectedValue = 0
            'Sohail (12 Feb 2018) -- End
            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            cboExtraIncome.SelectedValue = 0
            cboAbsentDays.SelectedValue = 0
            txtIdentityType.Text = ""
            txtPaymentType.Text = ""
            'Sohail (16 Apr 2018) -- End
            'Sohail (19 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            txtCustomCurrFormat.Text = ""
            'Sohail (19 Apr 2018) -- End
            'Hemant (16 Jul 2020) -- Start
            'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
            chkSaveAsTXT_WPS.Checked = False
            'Hemant (16 Jul 2020) -- End
            cboMembershipRepo.SelectedValue = 0 'Sohail (08 Jul 2021)
            Call GetValue() 'Sohail (15 Mar 2014)

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            If CInt(cboPresentDays.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboPresentDays)
            Else
                Call SetFontRegular(cboPresentDays)
            End If

            If CInt(cboBasicSalary.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboBasicSalary)
            Else
                Call SetFontRegular(cboBasicSalary)
            End If

            If CInt(cboSocialSecurity.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboSocialSecurity)
            Else
                Call SetFontRegular(cboSocialSecurity)
            End If
            'Sohail (12 Feb 2018) -- End

            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            If CInt(cboExtraIncome.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboExtraIncome)
            Else
                Call SetFontRegular(cboExtraIncome)
            End If

            If CInt(cboAbsentDays.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboAbsentDays)
            Else
                Call SetFontRegular(cboAbsentDays)
            End If
            'Sohail (16 Apr 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try

    End Sub

    Private Function SetFilter() As Boolean

        Try
            'Sohail (25 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (24 Mar 2014) -- Start
            'ENHANCEMENT - Set Currency selection mandatory for bank payment list as well and show currency sign in column AMOUNT Header.
            'If CInt(cboReportType.SelectedIndex) > 0 AndAlso CInt(cboCurrency.SelectedValue) <= 0 Then
            If CInt(cboCurrency.SelectedValue) <= 0 Then
                'Sohail (24 Mar 2014) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Currency is mandatory information. Please select Currency."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Return False
                'Sohail (16 Jul 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf CInt(cboCompanyBankName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Company Bank."), enMsgBoxStyle.Information)
                cboCompanyBankName.Focus()
                Return False
            ElseIf CInt(cboCompanyBranchName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Company Bank Branch."), enMsgBoxStyle.Information)
                cboCompanyBranchName.Focus()
                Return False
                'Sohail (16 Jul 2012) -- End
                'Sohail (21 Jul 2012) -- Start
                'TRA - ENHANCEMENT
            ElseIf CInt(cboCompanyAccountNo.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Company Bank Account."), enMsgBoxStyle.Information)
                cboCompanyAccountNo.Focus()
                Return False


                'Anjan [21 November 2016] -- Start
                'ENHANCEMENT : Including NEW ACB EFT integration report.
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
                'Anjan [21 November 2016] -- End


                'Sohail (21 Jul 2012) -- End

                'Sohail (25 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                'Sohail (14 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                'ElseIf CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.EFT_PSPF AndAlso CInt(cboMembership.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                '    cboMembership.Focus()
                'Sohail (14 Mar 2013) -- End

                'S.SANDEEP [ 07 JAN 2014 ] -- START
                'Sohail (15 Mar 2014) -- Start
                'Enhancement - Oman.
                'ElseIf CInt(cboBankName.SelectedValue) <= 0 AndAlso _
                '        Company._Object._Countryunkid = 162 AndAlso _
                '        CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Electronic_Fund_Transfer Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Please select Employee Bank."), enMsgBoxStyle.Information)
                '    cboBranchName.Focus()
                '    Return False
                'ElseIf CInt(cboBranchName.SelectedValue) <= 0 AndAlso _
                '       Company._Object._Countryunkid = 162 AndAlso _
                '       CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Electronic_Fund_Transfer Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Please select Employee Bank Branch."), enMsgBoxStyle.Information)
                '    cboBranchName.Focus()
                '    Return False
                'Sohail (15 Mar 2014) -- End
                'S.SANDEEP [ 07 JAN 2014 ] -- END

                'Return False
                'Sohail (25 Jan 2013) -- End
            End If
            'Sohail (25 May 2012) -- End

            objBankPaymentList.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objBankPaymentList._PeriodId = CInt(cboPeriod.SelectedValue)
            End If

            If CInt(cboBankName.SelectedValue) > 0 Then
                objBankPaymentList._BankId = CInt(cboBankName.SelectedValue)
                objBankPaymentList._BankName = cboBankName.Text
            End If

            If CInt(cboBranchName.SelectedValue) > 0 Then
                objBankPaymentList._BranchId = CInt(cboBranchName.SelectedValue)
                objBankPaymentList._BankBranchName = cboBranchName.Text
            End If

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            If CInt(cboCondition.SelectedValue) > 0 Then
                objBankPaymentList._ConditionId = CInt(cboCondition.SelectedValue)
                objBankPaymentList._ConditionText = cboCondition.Text
            End If
            'Nilay (10-Nov-2016) -- End

            If CInt(cboCountry.SelectedValue) > 0 Then
                objBankPaymentList._CountryId = CInt(cboCountry.SelectedValue)
                objBankPaymentList._CountryName = cboCountry.Text
            End If

            If CInt(cboEmployeeName.SelectedValue) > 0 Then
                objBankPaymentList._EmployeeUnkId = CInt(cboEmployeeName.SelectedValue)
                objBankPaymentList._EmpName = cboEmployeeName.Text
            End If

            If Trim(txtEmpcode.Text) <> "" Then
                objBankPaymentList._EmpCode = txtEmpcode.Text
            End If

            If Trim(txtAmount.Text) <> "" And Trim(txtAmountTo.Text) <> "" Then
                objBankPaymentList._Amount = txtAmount.Text
                objBankPaymentList._AmountTo = txtAmountTo.Text
            End If

            'Anjan (04 Apr 2011)-Start
            If chkEmployeeSign.Checked = True Then
                objBankPaymentList._IsEmployeeSign = True
            End If
            If chkSignatory1.Checked = True Then
                objBankPaymentList._IsSignatory1 = True
            End If
            If chkSignatory2.Checked = True Then
                objBankPaymentList._IsSignatory2 = True
            End If
            If chkSignatory3.Checked = True Then
                objBankPaymentList._IsSignatory3 = True
            End If

            'Anjan [ 22 Nov 2013 ] -- Start
            'ENHANCEMENT : Requested by Rutta
            If chkDefinedSignatory.Checked = True Then
                objBankPaymentList._IsDefinedSignatory = True
            End If

            'Anjan [22 Nov 2013 ] -- End


            'Anjan (04 Apr 2011)-End
            objBankPaymentList._ShowFNameSurNameInSeparateColumn = chkShowFNameSeparately.Checked 'Sohail (21 Mar 2013)
            'Sohail (24 Dec 2013) -- Start
            'Enhancement - Oman
            objBankPaymentList._ShowPayrollPeriod = chkShowPayrollPeriod.Checked
            objBankPaymentList._ShowSortCode = chkShowSortCode.Checked
            'Sohail (24 Dec 2013) -- End

            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objBankPaymentList._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            'Sohail (08 Nov 2011) -- Start
            objBankPaymentList._ViewByIds = mstrStringIds
            objBankPaymentList._ViewIndex = mintViewIdx
            objBankPaymentList._ViewByName = mstrStringName
            'Sohail (08 Nov 2011) -- End

            'Sohail (26 Nov 2011) -- Start
            objBankPaymentList._Analysis_Fields = mstrAnalysis_Fields
            objBankPaymentList._Analysis_Join = mstrAnalysis_Join
            objBankPaymentList._Analysis_OrderBy = mstrAnalysis_OrderBy
            objBankPaymentList._Report_GroupName = mstrReport_GroupName
            'Sohail (26 Nov 2011) -- End
            objBankPaymentList._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName 'Sohail (24 Jan 2013)

            'Sohail (16 Dec 2011) -- Start
            objBankPaymentList._CurrencyId = CInt(cboCurrency.SelectedValue)
            objBankPaymentList._CurrencyName = cboCurrency.Text
            objBankPaymentList._ShowGroupByBankBranch = chkShowGroupByBankBranch.Checked
            'Sohail (16 Dec 2011) -- End

            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objBankPaymentList._PeriodName = cboPeriod.Text
            'Anjan (20 Mar 2012)-End 

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objBankPaymentList._PeriodCode = objPeriod._Period_Code 'Sohail (24 Dec 2014) 
            objBankPaymentList._PeriodStartDate = objPeriod._Start_Date
            objBankPaymentList._PeriodEndDate = objPeriod._End_Date
            'Sohail (24 Mar 2018) -- Start
            'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar. [Salary month should be character for example Jan to be displayed as 01, Feb as 02, two digit number]
            objBankPaymentList._PeriodCustomCode = objPeriod._Sunjv_PeriodCode
            'Sohail (24 Mar 2018) -- End
            objPeriod = Nothing
            'Sohail (17 Apr 2012) -- End



            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            'Sohail (25 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'objBankPaymentList._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objBankPaymentList._ReportTypeId = CType(cboReportType.SelectedItem, ComboBoxValue).Value
            'Sohail (25 Jan 2013) -- End
            objBankPaymentList._ReportTypeName = cboReportType.Text
            'Pinkal (11-MAY-2012) -- End

            'Sohail (02 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            objBankPaymentList._ReportModeId = CInt(cboReportMode.SelectedIndex)
            objBankPaymentList._ReportModeName = cboReportMode.Text
            'Sohail (02 Jul 2012) -- End

            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            objBankPaymentList._CompanyBranchId = CInt(cboCompanyBranchName.SelectedValue)
            objBankPaymentList._CompanyBankBranchName = cboCompanyBranchName.SelectedText
            'Sohail (16 Jul 2012) -- End
            objBankPaymentList._CompanyBankAccountNo = cboCompanyAccountNo.Text 'Sohail (21 Jul 2012)

            'objBankPaymentList._MembershipId = CInt(cboMembership.SelectedValue) 'Sohail (14 Mar 2013)

            'Anjan [ 22 Nov 2013 ] -- Start
            'ENHANCEMENT : Requested by Rutta
            objBankPaymentList._CompanyBankGroupId = CInt(cboCompanyBankName.SelectedValue)

            'Anjan [ 22 Nov 2013 ] -- End

            'Sohail (04 Nov 2019) -- Start
            'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
            Dim objBankGroup As New clspayrollgroup_master
            objBankGroup._Groupmasterunkid = CInt(cboCompanyBankName.SelectedValue)
            objBankPaymentList._CompanyBankGroupCode = objBankGroup._Groupcode
            'Sohail (04 Nov 2019) -- End

            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            If cboChequeNo.SelectedIndex > 0 Then
                objBankPaymentList._ChequeNo = cboChequeNo.Text
            Else
                objBankPaymentList._ChequeNo = ""
            End If
            'Sohail (07 Dec 2013) -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            objBankPaymentList._PresentDaysId = CInt(cboPresentDays.SelectedValue)
            objBankPaymentList._PresentDaysName = cboPresentDays.SelectedText

            objBankPaymentList._BasicSalaryId = CInt(cboBasicSalary.SelectedValue)
            objBankPaymentList._BasicSalaryName = cboBasicSalary.SelectedText

            objBankPaymentList._SocialSecurityId = CInt(cboSocialSecurity.SelectedValue)
            objBankPaymentList._SocialSecurityName = cboSocialSecurity.SelectedText
            'Sohail (12 Feb 2018) -- End

            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            objBankPaymentList._ExtraIncomeId = CInt(cboExtraIncome.SelectedValue)
            objBankPaymentList._ExtraIncomeName = cboExtraIncome.SelectedText

            objBankPaymentList._AbsentDaysId = CInt(cboAbsentDays.SelectedValue)
            objBankPaymentList._AbsentDaysName = cboAbsentDays.SelectedText

            objBankPaymentList._IdentityType = txtIdentityType.Text.Trim
            objBankPaymentList._PaymentType = txtPaymentType.Text.Trim
            'Sohail (16 Apr 2018) -- End
            'Sohail (19 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            objBankPaymentList._CustomCurrencyFormat = txtCustomCurrFormat.Text.Trim
            'Sohail (19 Apr 2018) -- End
            'Hemant (16 Jul 2020) -- Start
            'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
            objBankPaymentList._SaveAsTXT = chkSaveAsTXT_WPS.Checked
            'Hemant (16 Jul 2020) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objBankPaymentList._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End


            'Anjan [03 Feb 2014] -- Start
            'ENHANCEMENT : Requested by Rutta.

            If chkShowBranchCode.Checked = True Then
                objBankPaymentList._ShowBranchCode = chkShowBranchCode.Checked
            End If

            If chkShowBankCode.Checked = True Then
                objBankPaymentList._ShowBankCode = chkShowBankCode.Checked
            End If


            If chkDefinedSignatory2.Checked = True Then
                objBankPaymentList._IsDefinedSignatory2 = True
            End If
            If chkDefinedSignatory3.Checked = True Then
                objBankPaymentList._IsDefinedSignatory3 = True
            End If
            'Anjan [03 Feb 2014 ] -- End

            objBankPaymentList._Cut_Off_Amount = txtCutOffAmount.Decimal   'Sohail (15 Mar 2014)
            'Sohail (24 Mar 2014) -- Start
            'ENHANCEMENT - Show Report Header / Account Type on Bank Payment List report.
            objBankPaymentList._ShowReportHeader = chkShowReportHeader.Checked
            objBankPaymentList._ShowAccountType = chkShowAccountType.Checked
            'Sohail (24 Mar 2014) -- End

            'Anjan [28 Mar 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            If chkShowEmployeeCode.Checked = True Then
                objBankPaymentList._ShowEmployeeCode = True
            End If
            'Anjan [28 Mar 2014 ] -- End

            'Sohail (01 Jun 2016) -- Start
            'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.
            objBankPaymentList._Posting_Date = dtpPostingDate.Value.Date
            'Sohail (01 Jun 2016) -- End

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            objBankPaymentList._EFTCitiDirectCountryCode = ConfigParameter._Object._EFTCitiDirectCountryCode
            objBankPaymentList._EFTCitiDirectSkipPriorityFlag = ConfigParameter._Object._EFTCitiDirectSkipPriorityFlag
            objBankPaymentList._EFTCitiDirectChargesIndicator = ConfigParameter._Object._EFTCitiDirectChargesIndicator
            objBankPaymentList._EFTCitiDirectAddPaymentDetail = ConfigParameter._Object._EFTCitiDirectAddPaymentDetail
            objBankPaymentList._RoundOff_Type = ConfigParameter._Object._RoundOff_Type
            'Sohail (09 Jan 2016) -- End

            'Shani(28-FEB-2017) -- Start
            'Enhancement - Add New Bank Payment List Integration for TAMA
            objBankPaymentList._IsLetterhead = chkLetterhead.Checked
            objBankPaymentList._IsAddress_To_Emp_Bank = chkAddresstoEmployeeBank.Checked
            'Shani(28-FEB-2017) -- End

            'Hemant (19 June 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            objBankPaymentList._ShowSelectedBankInfo = chkShowSelectedBankInfo.Checked
            'Hemant (19 June 2019) -- End
            'Hemant 20 Jul 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            objBankPaymentList._ShowCompanyGroupInfo = chkShowCompanyGrpInfo.Checked
            'Hemant 20 Jul 2019) -- End
            'Hemant (25 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
            If chkShowCompanyLogoOnReport.Visible = True Then
                objBankPaymentList._ShowCompanyLogo = chkShowCompanyLogoOnReport.Checked
            Else
                objBankPaymentList._ShowCompanyLogo = False
            End If
            'Hemant (25 Jul 2019) -- End

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            objBankPaymentList._MembershipUnkId = CInt(cboMembershipRepo.SelectedValue)
            objBankPaymentList._MembershipName = cboMembershipRepo.Text
            'Sohail (08 Jul 2021) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try

    End Function

    'Sohail (15 Mar 2014) -- Start
    'Enhancement - Save Selection.
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            'Sohail (09 Mar 2015) -- Start
            'Enhancement - New EFT Report Custom CSV Report.
            'dsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList)
            dsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList, 0)
            'Sohail (09 Mar 2015) -- End

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Company_Bank
                            cboCompanyBankName.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Company_Branch
                            cboCompanyBranchName.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Company_Account_No
                            cboCompanyAccountNo.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Report_Mode
                            cboReportMode.SelectedIndex = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Employee_Bank
                            cboBankName.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Employee_Branch
                            cboBranchName.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Country
                            cboCountry.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Paid_Currency
                            cboCurrency.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Signatory_1
                            chkSignatory1.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Signatory_2
                            chkSignatory2.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Signatory_3
                            chkSignatory3.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Defined_Signatory_1
                            chkDefinedSignatory.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Defined_Signatory_2
                            chkDefinedSignatory2.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Defined_Signatory_3
                            chkDefinedSignatory3.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Bank_Branch_Group
                            chkShowGroupByBankBranch.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Employee_Sign
                            chkEmployeeSign.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Include_Inactive_Employee
                            chkIncludeInactiveEmp.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Period
                            chkShowPayrollPeriod.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Sort_Code
                            chkShowSortCode.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Separate_FName_Surname
                            chkShowFNameSeparately.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Bank_Code
                            chkShowBankCode.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Branch_Code
                            chkShowBranchCode.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Account_Type
                            chkShowAccountType.Checked = CBool(dsRow.Item("transactionheadid"))

                            'Anjan [28 Mar 2014] -- Start
                            'ENHANCEMENT : Requested by Rutta
                        Case enHeadTypeId.show_employee_code
                            chkShowEmployeeCode.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Anjan [28 Mar 2014 ] -- End

                            'Anjan [14 December 2016] -- Start
                            'ENHANCEMENT : Including loan parameter in T24.
                        Case enHeadTypeId.show_Loan_Head
                            cboHeads.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Anjan [14 December 2016] -- End

                            'Shani(28-FEB-2017) -- Start
                            'Enhancement - Add New Bank Payment List Integration for TAMA
                        Case enHeadTypeId.Letterhead
                            chkLetterhead.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Address_to_Employee_Bank
                            chkAddresstoEmployeeBank.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Shani(28-FEB-2017) -- End

                            'Sohail (12 Feb 2018) -- Start
                            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                        Case enHeadTypeId.Present_Days
                            cboPresentDays.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Basic_Salary
                            cboBasicSalary.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Social_Security
                            cboSocialSecurity.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (12 Feb 2018) -- End

                            'Sohail (16 Apr 2018) -- Start
                            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                        Case CInt(enHeadTypeId.ExtraIncome)
                            cboExtraIncome.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.AbsentDays)
                            cboAbsentDays.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.IdentityType)
                            txtIdentityType.Text = dsRow.Item("transactionheadid").ToString

                        Case CInt(enHeadTypeId.PaymentType)
                            txtPaymentType.Text = dsRow.Item("transactionheadid").ToString
                            'Sohail (16 Apr 2018) -- End

                            'Sohail (19 Apr 2018) -- Start
                            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                        Case CInt(enHeadTypeId.CustomCurrencyFormat)
                            txtCustomCurrFormat.Text = dsRow.Item("transactionheadid").ToString
                            'Sohail (19 Apr 2018) -- End
                            'Hemant (16 Jul 2020) -- Start
                            'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
                        Case CInt(enHeadTypeId.Save_As_TXT)
                            chkSaveAsTXT_WPS.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Hemant (16 Jul 2020) -- End
                            'Hemant (19 June 2019) -- Start
                            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                        Case enHeadTypeId.Show_Selected_Bank_Info
                            chkShowSelectedBankInfo.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Hemant (19 June 2019) -- End
                            'Hemant 20 Jul 2019) -- Start
                            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                        Case enHeadTypeId.Show_Company_Group_Info
                            chkShowCompanyGrpInfo.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Hemant 20 Jul 2019) -- End
                            'Hemant (25 Jul 2019) -- Start
                            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
                        Case enHeadTypeId.Show_Company_Logo
                            chkShowCompanyLogoOnReport.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Hemant (25 Jul 2019) -- End

                            'Sohail (08 Jul 2021) -- Start
                            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
                        Case CInt(enHeadTypeId.Membership)
                            cboMembershipRepo.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (08 Jul 2021) -- End
                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (15 Mar 2014) -- End

    'Sohail (12 Feb 2018) -- Start
    'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 28, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetFontRegular(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Feb 2018) -- End

#End Region

#Region " Form Events "
    Private Sub frmBankRemittance_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBankPaymentList = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollReport_FormClosed", mstrModuleName)
        End Try
    End Sub
    Private Sub frmBankRemittance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objBankPaymentList._ReportName
            Me._Message = objBankPaymentList._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollReport_Load", mstrModuleName)
        End Try
    End Sub
    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "
    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information.Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            If Not SetFilter() Then Exit Sub


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer..
            objBankPaymentList._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes

            'objBankPaymentList.generateReport(0, e.Type, enExportAction.None)
            'Sohail (25 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'objBankPaymentList.generateReport(CInt(cboReportType.SelectedIndex), e.Type, enExportAction.None)


            'S.SANDEEP [ 07 JAN 2014 ] -- START
            'objBankPaymentList.generateReport(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value), e.Type, enExportAction.None)
            If cboReportType.SelectedIndex = 1 AndAlso Company._Object._Countryunkid = 162 Then 'OMAN EFT
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objBankPaymentList.generateReport(enBankPaymentReport.EFT_VOLTAMP, e.Type, enExportAction.None)

                objBankPaymentList.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, enBankPaymentReport.EFT_VOLTAMP, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
                'Sohail (21 Aug 2015) -- End

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            ElseIf CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Bank_Payment_Voucher Then 'BANK PAYMENT VOUCHER REPORT

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objBankPaymentList.generateReport(enBankPaymentReport.Bank_Payment_Voucher, enPrintAction.None, enExportAction.ExcelExtra)
                objBankPaymentList.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, enBankPaymentReport.Bank_Payment_Voucher, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._Base_CurrencyId)
                'Sohail (21 Aug 2015) -- End
                'Pinkal (12-Jun-2014) -- End

                'Sohail (12 Feb 2018) -- Start
                'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            ElseIf CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = CInt(enBankPaymentReport.Salary_Format_WPS) Then 'Salary Format WPS
                'Sohail (24 Mar 2018) -- Start
                'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar.
                'If objBankPaymentList.Generate_Salary_Format_WPS_Report(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, CInt(enBankPaymentReport.Salary_Format_WPS), True, ConfigParameter._Object._CurrencyFormat, "", ConfigParameter._Object._OpenAfterExport) = True Then

                'End If
                SaveDialog.FileName = ""
                'Hemant (16 Jul 2020) -- Start
                'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
                If chkSaveAsTXT_WPS.Checked = True Then
                    SaveDialog.Filter = "TXT File|*.txt"
                    'Hemant (25 Sep 2020) -- Start
                    'Enhancement (Allan Comments On JIRA) : This client wants this EFT in XLs and not CSV as provided.  Please assist. He miscommunicated this bit.
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
                    'Hemant (25 Sep 2020) -- End

                Else
                    'Hemant (16 Jul 2020) -- End
                    'Hemant (25 Sep 2020) -- Start
                    'Enhancement (Allan Comments On JIRA) : This client wants this EFT in XLs and not CSV as provided.  Please assist. He miscommunicated this bit.
                    'SaveDialog.Filter = "CSV File|*.csv"
                    'Hemant (25 Sep 2020) -- End
                End If 'Hemant (16 Jul 2020)
                'Hemant (25 Sep 2020) -- Start
                'Enhancement (Allan Comments On JIRA) : This client wants this EFT in XLs and not CSV as provided.  Please assist. He miscommunicated this bit.
                'SaveDialog.FilterIndex = 0
                'SaveDialog.ShowDialog()
                'If SaveDialog.FileName = "" Then
                '    Exit Try
                'End If
                'Hemant (25 Sep 2020) -- End                

                If objBankPaymentList.Generate_Salary_Format_WPS_Report(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, CInt(enBankPaymentReport.Salary_Format_WPS), True, ConfigParameter._Object._CurrencyFormat, SaveDialog.FileName, ConfigParameter._Object._OpenAfterExport, chkSaveAsTXT_WPS.Checked) = True Then
                    'Hemant (25 Sep 2020) -- [chkSaveAsTXT_WPS.Checked]
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                End If
                'Sohail (24 Mar 2018) -- End
                'Sohail (12 Feb 2018) -- End

            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objBankPaymentList.generateReport(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value), e.Type, enExportAction.None)
                objBankPaymentList.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value), e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
                'Sohail (21 Aug 2015) -- End
            End If
            'S.SANDEEP [ 07 JAN 2014 ] -- END


            'Sohail (25 Jan 2013) -- End
            'Pinkal (11-MAY-2012) -- End




        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer..
            objBankPaymentList._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            'Sohail (08 Dec 2012) -- Start
            'TRA - ENHANCEMENT
            'objBankPaymentList.generateReport(0, enPrintAction.None, e.Type)
            'Sohail (25 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'objBankPaymentList.generateReport(CInt(cboReportType.SelectedIndex), enPrintAction.None, e.Type)

            'S.SANDEEP [ 07 JAN 2014 ] -- START
            'objBankPaymentList.generateReport(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value), enPrintAction.None, e.Type)
            If cboReportType.SelectedIndex = 1 AndAlso Company._Object._Countryunkid = 162 Then 'OMAN EFT

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objBankPaymentList.generateReport(enBankPaymentReport.EFT_VOLTAMP, enPrintAction.None, e.Type)
                objBankPaymentList.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, enBankPaymentReport.EFT_VOLTAMP, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
                'Sohail (21 Aug 2015) -- End

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            ElseIf CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Bank_Payment_Voucher Then 'BANK PAYMENT VOUCHER REPORT

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objBankPaymentList.generateReport(enBankPaymentReport.Bank_Payment_Voucher, enPrintAction.None, e.Type)
                objBankPaymentList.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, enBankPaymentReport.Bank_Payment_Voucher, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
                'Sohail (21 Aug 2015) -- End
                'Pinkal (12-Jun-2014) -- End

            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objBankPaymentList.generateReport(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value), enPrintAction.None, e.Type)
                objBankPaymentList.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value), enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
                'Sohail (21 Aug 2015) -- End
            End If
            'S.SANDEEP [ 07 JAN 2014 ] -- END

            'Sohail (25 Jan 2013) -- End
            'Sohail (08 Dec 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployeeName.DataSource
            frm.SelectedValue = cboEmployeeName.SelectedValue
            frm.ValueMember = cboEmployeeName.ValueMember
            frm.DisplayMember = cboEmployeeName.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployeeName.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBankPaymentList.SetMessages()
            objfrm._Other_ModuleNames = "clsBankPaymentList"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End

    'Sohail (15 Mar 2014) -- Start
    'Enhancement - Save Selection.
    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            'For intHeadType As Integer = 1 To 27
            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                'Sohail (12 Feb 2018) -- End
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.BankPaymentList
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Company_Bank
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCompanyBankName.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Company_Branch
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCompanyBranchName.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Company_Account_No
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCompanyAccountNo.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Report_Mode
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboReportMode.SelectedIndex

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Employee_Bank
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBankName.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Employee_Branch
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBranchName.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Country
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCountry.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Paid_Currency
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCurrency.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Signatory_1
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkSignatory1.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Signatory_2
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkSignatory2.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Signatory_3
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkSignatory3.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Defined_Signatory_1
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkDefinedSignatory.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Defined_Signatory_2
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkDefinedSignatory2.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Defined_Signatory_3
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkDefinedSignatory3.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Bank_Branch_Group
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkShowGroupByBankBranch.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Employee_Sign
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkEmployeeSign.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Include_Inactive_Employee
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkIncludeInactiveEmp.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Period
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkShowPayrollPeriod.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Sort_Code
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkShowSortCode.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Separate_FName_Surname
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkShowFNameSeparately.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Bank_Code
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkShowBankCode.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Branch_Code
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkShowBranchCode.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_Account_Type
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkShowAccountType.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.show_employee_code
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkShowEmployeeCode.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.show_Loan_Head
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboHeads.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                        'Shani(28-FEB-2017) -- Start
                        'Enhancement - Add New Bank Payment List Integration for TAMA
                    Case enHeadTypeId.Letterhead
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkLetterhead.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)
                    Case enHeadTypeId.Address_to_Employee_Bank
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkAddresstoEmployeeBank.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)
                        'Shani(28-FEB-2017) -- End

                        'Sohail (12 Feb 2018) -- Start
                        'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                    Case enHeadTypeId.Present_Days
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboPresentDays.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Basic_Salary
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBasicSalary.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case enHeadTypeId.Social_Security
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboSocialSecurity.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)
                        'Sohail (12 Feb 2018) -- End

                        'Sohail (16 Apr 2018) -- Start
                        'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                    Case CInt(enHeadTypeId.ExtraIncome)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboExtraIncome.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.AbsentDays)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboAbsentDays.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.IdentityType)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtIdentityType.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.PaymentType)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtPaymentType.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)
                        'Sohail (16 Apr 2018) -- End

                        'Sohail (19 Apr 2018) -- Start
                        'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                    Case CInt(enHeadTypeId.CustomCurrencyFormat)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtCustomCurrFormat.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)
                        'Sohail (19 Apr 2018) -- End
                        'Hemant (16 Jul 2020) -- Start
                        'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
                    Case CInt(enHeadTypeId.Save_As_TXT)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkSaveAsTXT_WPS.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)
                        'Hemant (16 Jul 2020) -- End
                        'Hemant (19 June 2019) -- Start
                        'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                    Case enHeadTypeId.Show_Selected_Bank_Info
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkShowSelectedBankInfo.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)
                        'Hemant (19 June 2019) -- End
                        'Hemant 20 Jul 2019) -- Start
                        'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                    Case enHeadTypeId.Show_Company_Group_Info
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkShowCompanyGrpInfo.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)
                        'Hemant 20 Jul 2019) -- End
                        'Hemant (25 Jul 2019) -- Start
                        'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
                    Case enHeadTypeId.Show_Company_Logo
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(chkShowCompanyLogoOnReport.Checked).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)
                        'Hemant (25 Jul 2019) -- End

                        'Sohail (08 Jul 2021) -- Start
                        'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembershipRepo.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, 0, intHeadType)
                        'Sohail (08 Jul 2021) -- End

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (15 Mar 2014) -- End


#End Region

#Region " Other Controls "
    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click

        Try
            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'Anjan [ 05 Feb 2013 ] -- Start
            'ENHANCEMENT : TRA CHANGES
            'objBankPaymentList.setOrderBy(0)
            'Sohail (01 Apr 2014) -- Start
            'ENHANCEMENT - 
            'objBankPaymentList.setOrderBy(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value))
            If CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Electronic_Fund_Transfer AndAlso Company._Object._Countryunkid = 162 Then 'Oman EFT
                objBankPaymentList.setOrderBy(enBankPaymentReport.EFT_VOLTAMP)

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            ElseIf CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Bank_Payment_Voucher Then
                objBankPaymentList.setOrderBy(enBankPaymentReport.Bank_Payment_Voucher)
                'Pinkal (12-Jun-2014) -- End

            Else
            objBankPaymentList.setOrderBy(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value))
            End If
            'Sohail (01 Apr 2014) -- End
            'Anjan [ 05 Feb 2013 ] -- End
            'objBankPaymentList.setOrderBy(CInt(cboReportType.SelectedIndex))
            'Sohail (16 Jul 2012) -- End
            txtOrderBy.Text = objBankPaymentList.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub cboBankName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankName.SelectedIndexChanged
        objBankBranch = New clsbankbranch_master
        Dim dsList As New DataSet
        Try
            dsList = objBankBranch.getListForCombo("List", True, CInt(cboBankName.SelectedValue))
            With cboBranchName
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0

            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankName_SelectedIndexChanged", mstrModuleName)
        Finally
            objBankBranch = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Sub

    'Sohail (08 Nov 2011) -- Start
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            'Sohail (26 Nov 2011) -- Start
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Sohail (26 Nov 2011) -- End
            mstrAnalysis_OrderBy_GroupName = frm._Analysis_OrderBy_GName 'Sohail (24 Jan 2013)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (08 Nov 2011) -- End

    'Sohail (22 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub lnkEFTCityBankExport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFTCityBankExport.LinkClicked
        Try
            'Sohail (08 Dec 2014) -- Start
            'Enhancement - New EFT integration EFT CBA.
            'If CInt(cboReportType.SelectedIndex) > 0 AndAlso CInt(cboCurrency.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Currency is mandatory information. Please select Currency."), enMsgBoxStyle.Information)
            '    cboCurrency.Focus()
            '    Exit Try
            'ElseIf CInt(cboCompanyBankName.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Company Bank."), enMsgBoxStyle.Information)
            '    cboCompanyBankName.Focus()
            '    Exit Try
            'ElseIf CInt(cboCompanyBranchName.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Company Bank Branch."), enMsgBoxStyle.Information)
            '    cboCompanyBranchName.Focus()
            '    Exit Try
            '    'TRA - ENHANCEMENT
            'ElseIf CInt(cboCompanyAccountNo.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Company Bank Account."), enMsgBoxStyle.Information)
            '    cboCompanyAccountNo.Focus()
            '    Exit Try
            '    'Sohail (29 Mar 2013) -- Start
            '    'TRA - ENHANCEMENT
            'ElseIf CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Sohail (08 Dec 2014) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
                'Sohail (29 Mar 2013) -- End
                'Sohail (30 Apr 2013) -- Start
                'TRA - ENHANCEMENT
            ElseIf ConfigParameter._Object._DatafileExportPath = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Set Data File Export Path From Aruti Configuration -> Option -> Integration -> EFT Integration."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf ConfigParameter._Object._DatafileName = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please Set Data File Name From Aruti Configuration -> Option -> Integration -> EFT Integration."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
                'Sohail (30 Apr 2013) -- End

            End If

            'Sohail (08 Dec 2014) -- Start
            'Enhancement - New EFT integration EFT CBA.
            'objBankPaymentList.SetDefaultValue()
            'If CInt(cboPeriod.SelectedValue) > 0 Then
            '    objBankPaymentList._PeriodId = CInt(cboPeriod.SelectedValue)
            'End If

            'If CInt(cboBankName.SelectedValue) > 0 Then
            '    objBankPaymentList._BankId = CInt(cboBankName.SelectedValue)
            '    objBankPaymentList._BankName = cboBankName.Text
            'End If

            'If CInt(cboBranchName.SelectedValue) > 0 Then
            '    objBankPaymentList._BranchId = CInt(cboBranchName.SelectedValue)
            '    objBankPaymentList._BankBranchName = cboBranchName.Text
            'End If

            'If CInt(cboCountry.SelectedValue) > 0 Then
            '    objBankPaymentList._CountryId = CInt(cboCountry.SelectedValue)
            '    objBankPaymentList._CountryName = cboCountry.Text
            'End If

            'If CInt(cboEmployeeName.SelectedValue) > 0 Then
            '    objBankPaymentList._EmployeeUnkId = CInt(cboEmployeeName.SelectedValue)
            '    objBankPaymentList._EmpName = cboEmployeeName.Text
            'End If

            'If Trim(txtEmpcode.Text) <> "" Then
            '    objBankPaymentList._EmpCode = txtEmpcode.Text
            'End If

            'If Trim(txtAmount.Text) <> "" And Trim(txtAmountTo.Text) <> "" Then
            '    objBankPaymentList._Amount = txtAmount.Text
            '    objBankPaymentList._AmountTo = txtAmountTo.Text
            'End If

            'If chkEmployeeSign.Checked = True Then
            '    objBankPaymentList._IsEmployeeSign = True
            'End If
            'If chkSignatory1.Checked = True Then
            '    objBankPaymentList._IsSignatory1 = True
            'End If
            'If chkSignatory2.Checked = True Then
            '    objBankPaymentList._IsSignatory2 = True
            'End If
            'If chkSignatory3.Checked = True Then
            '    objBankPaymentList._IsSignatory3 = True
            'End If

            'objBankPaymentList._ShowFNameSurNameInSeparateColumn = chkShowFNameSeparately.Checked 'Sohail (21 Mar 2013)
            ''Sohail (24 Dec 2013) -- Start
            ''Enhancement - Oman
            'objBankPaymentList._ShowPayrollPeriod = chkShowPayrollPeriod.Checked
            'objBankPaymentList._ShowSortCode = chkShowSortCode.Checked
            ''Sohail (24 Dec 2013) -- End

            'objBankPaymentList._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked

            'objBankPaymentList._ViewByIds = mstrStringIds
            'objBankPaymentList._ViewIndex = mintViewIdx
            'objBankPaymentList._ViewByName = mstrStringName

            'objBankPaymentList._Analysis_Fields = mstrAnalysis_Fields
            'objBankPaymentList._Analysis_Join = mstrAnalysis_Join
            'objBankPaymentList._Analysis_OrderBy = mstrAnalysis_OrderBy
            'objBankPaymentList._Report_GroupName = mstrReport_GroupName
            'objBankPaymentList._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName 'Sohail (24 Jan 2013)

            'objBankPaymentList._CurrencyId = CInt(cboCurrency.SelectedValue)
            'objBankPaymentList._CurrencyName = cboCurrency.Text
            'objBankPaymentList._ShowGroupByBankBranch = chkShowGroupByBankBranch.Checked

            'objBankPaymentList._PeriodName = cboPeriod.Text

            'Dim objPeriod As New clscommom_period_Tran
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            'objBankPaymentList._PeriodStartDate = objPeriod._Start_Date
            'objBankPaymentList._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing

            'objBankPaymentList._ReportTypeId = CType(cboReportType.SelectedItem, ComboBoxValue).Value
            'objBankPaymentList._ReportTypeName = cboReportType.Text

            'objBankPaymentList._ReportModeId = CInt(cboReportMode.SelectedIndex)
            'objBankPaymentList._ReportModeName = cboReportMode.Text

            'objBankPaymentList._CompanyBranchId = CInt(cboCompanyBranchName.SelectedValue)
            'objBankPaymentList._CompanyBankBranchName = cboCompanyBranchName.SelectedText
            'objBankPaymentList._CompanyBankAccountNo = cboCompanyAccountNo.Text

            ''Sohail (07 Dec 2013) -- Start
            ''Enhancement - OMAN
            'If cboChequeNo.SelectedIndex > 0 Then
            '    objBankPaymentList._ChequeNo = cboChequeNo.Text
            'Else
            '    objBankPaymentList._ChequeNo = ""
            'End If
            ''Sohail (07 Dec 2013) -- End

            'objBankPaymentList._Advance_Filter = mstrAdvanceFilter
            'Sohail (08 Dec 2014) -- End

            'Sohail (30 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'SaveDialog.FileName = ""
            'SaveDialog.Filter = "Text File|*.txt"
            'SaveDialog.FilterIndex = 0
            'SaveDialog.ShowDialog()
            'If SaveDialog.FileName = "" Then
            '    Exit Try
            'End If

            'If objBankPaymentList.EFT_CityDirect_Export_GenerateReport(SaveDialog.FileName) = True Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            'End If
            Dim strConfigExpPath As String = ConfigParameter._Object._DatafileExportPath
            If IO.Directory.Exists(strConfigExpPath) = False Then
                SaveDialog.FileName = ""
                SaveDialog.Filter = "Text File|*.txt"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
                strConfigExpPath = SaveDialog.FileName
            Else
                strConfigExpPath = IO.Path.Combine(ConfigParameter._Object._DatafileExportPath, ConfigParameter._Object._DatafileName & ".txt")
            End If
            Cursor.Current = Cursors.WaitCursor
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objBankPaymentList.EFT_CityDirect_Export_GenerateReport(strConfigExpPath) = True Then
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_CityDirect_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, strConfigExpPath, Company._Object._Name, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._DatafileExportPath, ConfigParameter._Object._SMimeRunPath, ConfigParameter._Object._CurrencyFormat) = True Then
                'Sohail (21 Aug 2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            'Sohail (30 Apr 2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFTCityBankExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (22 Mar 2013) -- End

    'Sohail (08 Dec 2014) -- Start
    'Enhancement - New EFT integration EFT CBA.
    Private Sub lnkEFT_CBA_Export_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFT_CBA_Export.LinkClicked
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If


            'Sohail (12 Feb 2015) -- Start
            'Enhancement - EFT CBA with New CSV Format.
            'If objBankPaymentList.EFT_CBA_Export_GenerateReport("") = True Then
            '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            'End If
            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objBankPaymentList.EFT_CBA_Export_GenerateReport(SaveDialog.FileName) = True Then
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_CBA_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, ConfigParameter._Object._CurrencyFormat) = True Then
                'Sohail (21 Aug 2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            'Sohail (12 Feb 2015) -- End
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_CBA_Export_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkMobileMoneyEFTMPesaExport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkMobileMoneyEFTMPesaExport.LinkClicked
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If

            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objBankPaymentList.EFT_MPesa_Export_GenerateReport(SaveDialog.FileName) = True Then
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_MPesa_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, ConfigParameter._Object._CurrencyFormat) = True Then
                'Sohail (21 Aug 2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkMobileMoneyEFTMPesaExport_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (08 Dec 2014) -- End

    'Sohail (12 Dec 2014) -- Start
    'VFT Enhancement - New EFT Report EFT T24.
    Private Sub lnkEFT_T24_Export_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFT_T24_Export.LinkClicked
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If

            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objBankPaymentList.EFT_T24_Export_GenerateReport(SaveDialog.FileName) = True Then
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_T24_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, ConfigParameter._Object._CurrencyFormat) = True Then
                'Sohail (21 Aug 2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_T24_Export_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Dec 2014) -- End

    'Sohail (24 Dec 2014) -- Start
    'AMI Enhancement - New EFT Report EFT EXIM.
    Private Sub lnkEFT_EXIM_Export_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFT_EXIM_Export.LinkClicked
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objBankPaymentList.EFT_EXIM_Export_GenerateReport("") = True Then
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_EXIM_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, "", FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, ConfigParameter._Object._OpenAfterExport, ConfigParameter._Object._CurrencyFormat) = True Then
                'Sohail (21 Aug 2015) -- End
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_EXIM_Export_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (24 Dec 2014) -- End

    'Sohail (29 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub chkShowGroupByBankBranch_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowGroupByBankBranch.CheckedChanged
        Try
            objBankPaymentList._ShowGroupByBankBranch = chkShowGroupByBankBranch.Checked
            Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value))
            txtOrderBy.Text = objBankPaymentList.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowGroupByBankBranch_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Mar 2013) -- End

    'Anjan [28 Mar 2014] -- Start
    'ENHANCEMENT : Requested by Rutta
    Private Sub chkShowEmployeeCode_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowEmployeeCode.CheckedChanged
        Try
            objBankPaymentList._ShowEmployeeCode = chkShowEmployeeCode.Checked
            If chkShowGroupByBankBranch.Checked = True OrElse (CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Electronic_Fund_Transfer AndAlso Company._Object._Countryunkid = 162) Then
                objBankPaymentList.Create_OnDetailReportWithoutBankBranch()
            Else
                objBankPaymentList.Create_OnDetailReport()
            End If
            'Sohail (01 Apr 2014) -- Start
            'ENHANCEMENT - 
            'Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value))
            If CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Electronic_Fund_Transfer _
               AndAlso Company._Object._Countryunkid = 162 Then 'OMAN EFT 
                Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.EFT_VOLTAMP)
            Else
                Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value))
            End If
            'Sohail (01 Apr 2014) -- End
            txtOrderBy.Text = objBankPaymentList.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowEmployeeCode_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Anjan [28 Mar 2014 ] -- End

    'Sohail (09 Mar 2015) -- Start
    'Enhancement - New EFT Report Custom CSV Report.
    Private Sub lnkEFT_Custom_CSV_Export_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFT_Custom_CSV_Export.LinkClicked
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If


            Dim objFrm As New frmEFTCustomColumnsExport
            Dim strDilimiter As String 'Hemant (27 June 2019)
            'Sohail (25 Mar 2015) -- Start
            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance Payment.
            'If objFrm.displayDialog(enArutiReport.BankPaymentList, enEFT_Export_Mode.CSV, 0) = True Then
            If objFrm.displayDialog(enArutiReport.BankPaymentList, enEFT_Export_Mode.CSV, CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value)) = True Then
                'Sohail (25 Mar 2015) -- End

                SaveDialog.FileName = ""
                'Hemant (27 June 2019) -- Start
                'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                If objFrm._EFTSaveAsTXT = True Then
                    SaveDialog.Filter = "TXT File|*.txt"
                Else
                    'Hemant (27 June 2019) -- End
                SaveDialog.Filter = "CSV File|*.csv"
                End If

                'Hemant (27 June 2019) -- Start
                'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                If objFrm._EFTTabDelimiter = True Then
                    strDilimiter = vbTab
                Else
                    strDilimiter = ","
                End If
                'Hemant (27 June 2019) -- End
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If

                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                'If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(SaveDialog.FileName, objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader) = True Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                'End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                Select Case CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value)
                    Case enBankPaymentReport.Electronic_Fund_Transfer
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(SaveDialog.FileName, objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader) = True Then
                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                        'If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader, ConfigParameter._Object._CurrencyFormat) = True Then
                        If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader, ConfigParameter._Object._CurrencyFormat, objFrm._EFTMembershipUnkId, strDilimiter, objFrm._EFTDateFormat) = True Then
                            'Hemant (02 Jul 2020) -- [objFrm._EFTDateFormat]
                            'Hemant (27 June 2019) -- [strDilimiter]
                            'Sohail (13 Apr 2016) -- End
                            'Sohail (21 Aug 2015) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                End If

                    Case enBankPaymentReport.EFT_ADVANCE_SALARY_CSV
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objBankPaymentList.EFT_Advance_Salary_CSV_Export_GenerateReport(SaveDialog.FileName, objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader) = True Then
                        If objBankPaymentList.EFT_Advance_Salary_CSV_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader, ConfigParameter._Object._CurrencyFormat) = True Then
                            'Sohail (21 Aug 2015) -- End
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                        End If

                End Select
                'Sohail (25 Mar 2015) -- End
            End If
            objFrm = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_Custom_CSV_Export_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkEFT_Custom_XLS_Export_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFT_Custom_XLS_Export.LinkClicked
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If

            Dim objFrm As New frmEFTCustomColumnsExport
            'Sohail (25 Mar 2015) -- Start
            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance Payment.
            'If objFrm.displayDialog(enArutiReport.BankPaymentList, enEFT_Export_Mode.XLS, 0) = True Then
            If objFrm.displayDialog(enArutiReport.BankPaymentList, enEFT_Export_Mode.XLS, CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value)) = True Then
                'Sohail (25 Mar 2015) -- End
                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                'If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport("", objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader) = True Then
                '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                'End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                Select Case CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value)
                    Case enBankPaymentReport.Electronic_Fund_Transfer
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport("", objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader) = True Then
                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                        'If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, "", objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader, ConfigParameter._Object._OpenAfterExport, ConfigParameter._Object._CurrencyFormat) = True Then
                        If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, "", objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader, ConfigParameter._Object._OpenAfterExport, ConfigParameter._Object._CurrencyFormat, objFrm._EFTMembershipUnkId, objFrm._EFTDateFormat) = True Then
                            'Hemant (02 Jul 2020) -- [objFrm._EFTDateFormat]
                            'Sohail (13 Apr 2016) -- End
                            'Sohail (21 Aug 2015) -- End
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                End If
                    Case enBankPaymentReport.EFT_ADVANCE_SALARY_XLS
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objBankPaymentList.EFT_Advance_Salary_XLS_Export_GenerateReport("", objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader) = True Then
                        If objBankPaymentList.EFT_Advance_Salary_XLS_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, "", objFrm._EFTCustomColumnsIds, objFrm._EFTShowColumnHeader, ConfigParameter._Object._OpenAfterExport, ConfigParameter._Object._CurrencyFormat) = True Then
                            'Sohail (21 Aug 2015) -- End
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                        End If
                End Select
                'Sohail (25 Mar 2015) -- End
            End If
            objFrm = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_Custom_XLS_Export_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (09 Mar 2015) -- End

    'Sohail (01 Jun 2016) -- Start
    'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.
    Private Sub lnkEFT_FlexCubeRetailGEFU_Export_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFT_FlexCubeRetailGEFU_Export.LinkClicked
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), enMsgBoxStyle.Information)
                dtpPostingDate.Focus()
                Exit Try
            End If

            SaveDialog.FileName = ""
            SaveDialog.Filter = "Text File|*.txt"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_Flex_Cube_Retail_GEFU_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, ConfigParameter._Object._CurrencyFormat) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_FlexCubeRetailGEFU_Export_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (01 Jun 2016) -- End

    'Nilay (10-Nov-2016) -- Start
    'Enhancement : New EFT report : EFT ECO bank for OFFGRID
    Private Sub lnkEFT_ECO_Bank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_ECO_Bank.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), enMsgBoxStyle.Information)
                dtpPostingDate.Focus()
                Exit Try
            End If

            SaveDialog.FileName = ""
            SaveDialog.Filter = "Text File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_ECO_Bank(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                               objPeriod._Start_Date, objPeriod._End_Date, _
                                               ConfigParameter._Object._UserAccessModeSetting, True, _
                                               chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, _
                                               ConfigParameter._Object._CurrencyFormat) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_ECO_Bank_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (10-Nov-2016) -- End

    'Anjan [21 November 2016] -- Start
    'ENHANCEMENT : Including NEW ACB EFT integration report.
    Private Sub lnkEFT_T24_Web_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_T24_Web.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), enMsgBoxStyle.Information)
                dtpPostingDate.Focus()
                Exit Try
            End If
            pnl_ID_Passcode.Visible = True
            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_ECO_Bank_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan [21 November 2016] -- End

    'Shani(28-FEB-2017) -- Start
    'Enhancement - Add New Bank Payment List Integration for TAMA
    Private Sub lnkBankPaymentLetter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBankPaymentLetter.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try

            End If
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)


            objBankPaymentList.EFT_Bank_Payment_Letter_Report(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                                               FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                               objPeriod._Start_Date, objPeriod._End_Date, _
                                                               ConfigParameter._Object._UserAccessModeSetting, True, _
                                                               chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, _
                                                               ConfigParameter._Object._CurrencyFormat, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkBankPaymentLetter_Click", mstrModuleName)
        End Try
    End Sub
    'Shani(28-FEB-2017) -- End

    'Sohail (20 Sep 2017) -- Start
    'Paytech Enhancement - 69.1 - EFT file for barclays bank should be in csv format. And contain the codes representing a specific item. A sample EFT file was shared with Rutta.
    Private Sub lnkEFTBarclaysBankExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTBarclaysBankExport.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), enMsgBoxStyle.Information)
                dtpPostingDate.Focus()
                Exit Try
            End If

            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_Barclays_Bank(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                   objPeriod._Start_Date, objPeriod._End_Date, _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, _
                                                   ConfigParameter._Object._CurrencyFormat) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFTBarclaysBankExport_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (20 Sep 2017) -- End

    'Anjan [21 November 2016] -- Start
    'ENHANCEMENT : Including NEW ACB EFT integration report.
    Private Sub btnOk_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try

            If Trim(txtLink.Text) = "" Then
                'Sohail (12 Feb 2018) -- Start
                'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Link is mandatory information."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Link is mandatory information."), enMsgBoxStyle.Information)
                'Sohail (12 Feb 2018) -- End
                Exit Sub
            End If

            If Trim(txtID.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Id is mandatory information."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If Trim(txtPasscode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Pass Code is mandatory information."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Anjan [14 December 2016] -- Start
            'ENHANCEMENT : Including loan parameter in T24.
            objBankPaymentList._T24Link = txtLink.Text
            objBankPaymentList._LoanHead_ID = CInt(cboHeads.SelectedValue)
            'Anjan [14 December 2016] -- End

            objBankPaymentList._T24WEB_ID = txtID.Text
            objBankPaymentList._T24WEB_PassCode = txtPasscode.Text

            SaveDialog.FileName = ""
            SaveDialog.Filter = "Text File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_ACB_T24_WEB(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                               objPeriod._Start_Date, objPeriod._End_Date, _
                                               ConfigParameter._Object._UserAccessModeSetting, True, _
                                               chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, _
                                               ConfigParameter._Object._CurrencyFormat) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            txtID.Text = ""
            txtPasscode.Text = ""
            pnl_ID_Passcode.Visible = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOK_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            'Anjan [14 December 2016] -- Start
            'ENHANCEMENT : Including loan parameter in T24.
            cboHeads.SelectedValue = 0
            'Anjan [14 December 2016] -- End
            txtID.Text = ""
            txtPasscode.Text = ""
            pnl_ID_Passcode.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLoanheads_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLoanheads.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsTransactionHead
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With cboHeads
                objfrm.DataSource = CType(cboHeads.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan [21 November 2016] -- End

    'Sohail (12 Feb 2018) -- Start
    'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
    Private Sub EZeeCollapsibleContainer1_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles EZeeCollapsibleContainer1.Scroll
        Try
            EZeeCollapsibleContainer1.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EZeeCollapsibleContainer1_Scroll", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkOtherSetting_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkOtherSetting.LinkClicked
        Try
            pnlOtherSettings.Visible = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkOtherSetting_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            pnlOtherSettings.Visible = False
        Catch ex As Exception

        End Try
    End Sub
    'Sohail (12 Feb 2018) -- End

    'S.SANDEEP [04-May-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
    Private Sub lnkEFTNationalBankMalawi_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFTNationalBankMalawi.LinkClicked
        Dim objPeriod As New clscommom_period_Tran
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If

            objBankPaymentList._OpenAfterExport = ConfigParameter._Object._OpenAfterExport

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objBankPaymentList.EFT_National_Bank_Malawi_GenerateReport(FinancialYear._Object._DatabaseName, _
                                                                       User._Object._Userunkid, _
                                                                       FinancialYear._Object._YearUnkid, _
                                                                       Company._Object._Companyunkid, _
                                                                       objPeriod._Start_Date, _
                                                                       objPeriod._End_Date, _
                                                                       ConfigParameter._Object._UserAccessModeSetting, _
                                                                       True, _
                                                                       chkIncludeInactiveEmp.Checked, _
                                                                       True, "", ConfigParameter._Object._CurrencyFormat)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFTNationalBankMalawi_LinkClicked", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
    'S.SANDEEP [04-May-2018] -- END

    'S.SANDEEP [19-SEP-2018] -- START
    'ISSUE/ENHANCEMENT : {#0002533|ARUTI-334}
    Private Sub lnkEFTCityBank_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFTCityBank.LinkClicked
        Try
            If SetFilter() = False Then Exit Try
            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf ConfigParameter._Object._DatafileExportPath = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Set Data File Export Path From Aruti Configuration -> Option -> Integration -> EFT Integration."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf ConfigParameter._Object._DatafileName = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please Set Data File Name From Aruti Configuration -> Option -> Integration -> EFT Integration."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf ConfigParameter._Object._EFTCitiDirectCountryCode = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Please Set Country Code From Aruti Configuration -> Option -> Integration -> EFT Integration."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
                'ElseIf ConfigParameter._Object._EFTCitiDirectChargesIndicator = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Please Set Indicator From Aruti Configuration -> Option -> Integration -> EFT Integration."), enMsgBoxStyle.Information)
                '    cboReportMode.Focus()
                '    Exit Try
            ElseIf dtpPostingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), enMsgBoxStyle.Information)
                dtpPostingDate.Focus()
                Exit Try
            End If

            Dim strConfigExpPath As String = ConfigParameter._Object._DatafileExportPath
            If IO.Directory.Exists(strConfigExpPath) = False Then
                SaveDialog.FileName = ""
                SaveDialog.Filter = "Text File|*.txt"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
                strConfigExpPath = SaveDialog.FileName
            Else
                strConfigExpPath = IO.Path.Combine(ConfigParameter._Object._DatafileExportPath, ConfigParameter._Object._DatafileName & ".txt")
            End If
            Cursor.Current = Cursors.WaitCursor
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.DFT_CityBank_Report(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, strConfigExpPath, Company._Object._Name, dtpPostingDate.Value, ConfigParameter._Object._DatafileExportPath, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._EFTCitiDirectCountryCode) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFTCityBank_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [19-SEP-2018] -- END

    'Sohail (04 Nov 2019) -- Start
    'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
    Private Sub lnkEFT_FNB_Bank_Export_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFT_FNB_Bank_Export.LinkClicked
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If


            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_FNB_Bank_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, SaveDialog.FileName, ConfigParameter._Object._CurrencyFormat) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_FNB_Bank_Export_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (04 Nov 2019) -- End

    'Hemant (11 Dec 2019) -- Start
    'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
    Private Sub lnkEFTStandardCharteredBank_S2B_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFTStandardCharteredBank_S2B.LinkClicked
        Dim objPeriod As New clscommom_period_Tran
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
                'Sohail (15 Apr 2020) -- Start
                'SANLAM LIFE INSURANCE Enhancement # 0004324 : Roll back to XLS format and additional column Posting date in MM/DD/YYYY.
            ElseIf dtpPostingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), enMsgBoxStyle.Information)
                dtpPostingDate.Focus()
                Exit Try
                'Sohail (15 Apr 2020) -- End
            End If

            'Sohail (31 Mar 2020) -- Start
            'SANLAM LIFE INSURANCE Enhancement # 0004324 : Assist to change the File Extension from XLS to CSV.
            'Sohail (15 Apr 2020) -- Start
            'SANLAM LIFE INSURANCE Enhancement # 0004324 : Roll back to XLS format and additional column Posting date in MM/DD/YYYY.
            'SaveDialog.FileName = ""
            'SaveDialog.Filter = "CSV File|*.csv"
            'SaveDialog.FilterIndex = 0
            'SaveDialog.ShowDialog()
            'If SaveDialog.FileName = "" Then
            '    Exit Try
            'End If
            'Sohail (15 Apr 2020) -- End
            'Sohail (31 Mar 2020) -- End

            objBankPaymentList._OpenAfterExport = ConfigParameter._Object._OpenAfterExport

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_Standard_Chartered_Bank_S2B_Report(FinancialYear._Object._DatabaseName, _
                                                                       User._Object._Userunkid, _
                                                                       FinancialYear._Object._YearUnkid, _
                                                                       Company._Object._Companyunkid, _
                                                                       objPeriod._Start_Date, _
                                                                       objPeriod._End_Date, _
                                                                       ConfigParameter._Object._UserAccessModeSetting, _
                                                                       True, _
                                                                       chkIncludeInactiveEmp.Checked, _
                                                                          True, "", ConfigParameter._Object._CurrencyFormat) = True Then
                'Sohail (15 Apr 2020) - [aveDialog.FileName]=[""]
                'Sohail (31 Mar 2020) - [SaveDialog.FileName]

                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information) 'Sohail (15 Apr 2020)

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFTStandardCharteredBank_S2B_LinkClicked", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
    'Hemant (11 Dec 2019) -- End

    'Hemant (29 Jun 2020) -- Start
    'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
    Private Sub lnkEFT_ABSA_Bank_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFT_ABSA_Bank.LinkClicked
        Dim objPeriod As New clscommom_period_Tran
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If

            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            objBankPaymentList._OpenAfterExport = ConfigParameter._Object._OpenAfterExport

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_ABSA_Bank_Report(FinancialYear._Object._DatabaseName, _
                                                                       User._Object._Userunkid, _
                                                                       FinancialYear._Object._YearUnkid, _
                                                                       Company._Object._Companyunkid, _
                                                                       objPeriod._Start_Date, _
                                                                       objPeriod._End_Date, _
                                                                       ConfigParameter._Object._UserAccessModeSetting, _
                                                                       True, _
                                                                       chkIncludeInactiveEmp.Checked, _
                                                                          True, SaveDialog.FileName, ConfigParameter._Object._CurrencyFormat) = True Then

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_ABSA_Bank_LinkClicked", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
    'Hemant (29 Jun 2020) -- End

    'Hemant (24 Dec 2020) -- Start
    'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
    Private Sub lnkEFTNationalBankMalawiXLSX_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFTNationalBankMalawiXLSX.LinkClicked
        Dim objPeriod As New clscommom_period_Tran
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), enMsgBoxStyle.Information)
                dtpPostingDate.Focus()
                Exit Try
            End If

            SaveDialog.FileName = ""
            SaveDialog.Filter = "XLSX File|*.xlsx"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If


            objBankPaymentList._OpenAfterExport = ConfigParameter._Object._OpenAfterExport

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_National_Bank_Malawi_XLSX_GenerateReport(FinancialYear._Object._DatabaseName, _
                                                                       User._Object._Userunkid, _
                                                                       FinancialYear._Object._YearUnkid, _
                                                                       Company._Object._Companyunkid, _
                                                                       objPeriod._Start_Date, _
                                                                       objPeriod._End_Date, _
                                                                       ConfigParameter._Object._UserAccessModeSetting, _
                                                                       True, _
                                                                       chkIncludeInactiveEmp.Checked, _
                                                                       True, SaveDialog.FileName, ConfigParameter._Object._CurrencyFormat) = True Then

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFTNationalBankMalawiXLSX_LinkClicked", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
    'Hemant (24 Dec 2020) -- End

    'Sohail (08 Jul 2021) -- Start
    'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
    Private Sub lnkEFTEquityBankKenya_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFTEquityBankKenya.LinkClicked
        Dim objPeriod As New clscommom_period_Tran
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf CInt(cboMembershipRepo.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Please select membership to generate report."), enMsgBoxStyle.Information)
                cboMembershipRepo.Focus()
                Exit Try
            End If

            objBankPaymentList._OpenAfterExport = ConfigParameter._Object._OpenAfterExport

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objBankPaymentList.EFT_Equity_Bank_Kenya_GenerateReport(FinancialYear._Object._DatabaseName, _
                                                                       User._Object._Userunkid, _
                                                                       FinancialYear._Object._YearUnkid, _
                                                                       Company._Object._Companyunkid, _
                                                                       objPeriod._Start_Date, _
                                                                       objPeriod._End_Date, _
                                                                       ConfigParameter._Object._UserAccessModeSetting, _
                                                                       True, _
                                                                       chkIncludeInactiveEmp.Checked, _
                                                                       True, "", ConfigParameter._Object._CurrencyFormat)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFTEquityBankKenya_LinkClicked", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub lnkEFTNationalBankKenya_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFTNationalBankKenya.LinkClicked
        Dim objPeriod As New clscommom_period_Tran
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If

            objBankPaymentList._OpenAfterExport = ConfigParameter._Object._OpenAfterExport

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objBankPaymentList.EFT_National_Bank_Kenya_GenerateReport(FinancialYear._Object._DatabaseName, _
                                                                       User._Object._Userunkid, _
                                                                       FinancialYear._Object._YearUnkid, _
                                                                       Company._Object._Companyunkid, _
                                                                       objPeriod._Start_Date, _
                                                                       objPeriod._End_Date, _
                                                                       ConfigParameter._Object._UserAccessModeSetting, _
                                                                       True, _
                                                                       chkIncludeInactiveEmp.Checked, _
                                                                       True, "", ConfigParameter._Object._CurrencyFormat)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFTNationalBankKenya_LinkClicked", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
    'Sohail (08 Jul 2021) -- End

    'Sohail (27 Jul 2021) -- Start
    'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
    Private Sub lnkEFTCitiBankKenya_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFTCitiBankKenya.LinkClicked
        Try
            If SetFilter() = False Then Exit Try
            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf ConfigParameter._Object._DatafileExportPath = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Set Data File Export Path From Aruti Configuration -> Option -> Integration -> EFT Integration."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf ConfigParameter._Object._DatafileName = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please Set Data File Name From Aruti Configuration -> Option -> Integration -> EFT Integration."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            ElseIf ConfigParameter._Object._EFTCitiDirectCountryCode = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Please Set Country Code From Aruti Configuration -> Option -> Integration -> EFT Integration."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
                'ElseIf ConfigParameter._Object._EFTCitiDirectChargesIndicator = "" Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Please Set Indicator From Aruti Configuration -> Option -> Integration -> EFT Integration."), enMsgBoxStyle.Information)
                '    cboReportMode.Focus()
                '    Exit Try
                'ElseIf dtpPostingDate.Checked = False Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), enMsgBoxStyle.Information)
                '    dtpPostingDate.Focus()
                '    Exit Try
            End If

            Dim strConfigExpPath As String = ConfigParameter._Object._DatafileExportPath
            If IO.Directory.Exists(strConfigExpPath) = False Then
                SaveDialog.FileName = ""
                SaveDialog.Filter = "Text File|*.txt"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
                strConfigExpPath = SaveDialog.FileName
            Else
                strConfigExpPath = IO.Path.Combine(ConfigParameter._Object._DatafileExportPath, ConfigParameter._Object._DatafileName & ".txt")
            End If
            Cursor.Current = Cursors.WaitCursor
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_CityBankKenya_Export_GenerateReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, strConfigExpPath, Company._Object._Name, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._DatafileExportPath, ConfigParameter._Object._SMimeRunPath, ConfigParameter._Object._CurrencyFormat) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFTCityBank_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (27 Jul 2021) -- End

#End Region

    'Pinkal (11-MAY-2012) -- Start
    'Enhancement : TRA Changes

#Region "ComboBox Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            chkEmployeeSign.Checked = False
            chkShowGroupByBankBranch.Checked = False
            chkSignatory1.Checked = False
            chkSignatory2.Checked = False
            chkSignatory3.Checked = False
            cboCountry.SelectedIndex = 0
            'cboCurrency.SelectedIndex = 0 'Sohail (01 Apr 2014)
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            cboReportMode.Enabled = True 'Sohail (02 Jul 2012)
            chkShowFNameSeparately.Checked = False 'Sohail (21 Mar 2013)
            'Sohail (24 Dec 2013) -- Start
            'Enhancement - Oman
            chkShowPayrollPeriod.Checked = True
            'chkShowSortCode.Checked = True
            'Sohail (24 Mar 2014) -- Start
            'Issue - Provide Show Report Header option
            chkShowReportHeader.Visible = False
            chkShowReportHeader.Checked = False
            chkShowAccountType.Visible = False
            chkShowAccountType.Checked = False
            'Sohail (24 Mar 2014) -- End
            'Hemant (19 June 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            chkShowSelectedBankInfo.Visible = False
            'Hemant (19 June 2019) -- End
            'Hemant 20 Jul 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            chkShowCompanyGrpInfo.Visible = False
            'Hemant 20 Jul 2019) -- End
            'Hemant (25 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
            chkShowCompanyLogoOnReport.Visible = False
            'Hemant (25 Jul 2019) -- End

            'Sohail (24 Dec 2013) -- End
            'Sohail (29 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'If CInt(cboReportType.SelectedIndex) > 0 Then
            '    cboCountry.Enabled = False
            '    'Sohail (25 May 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'cboCurrency.Enabled = False
            '    cboCurrency.Enabled = True
            '    'Sohail (25 May 2012) -- End
            '    chkEmployeeSign.Enabled = False
            '    chkShowGroupByBankBranch.Enabled = False
            '    chkSignatory1.Enabled = False
            '    chkSignatory2.Enabled = False
            '    chkSignatory3.Enabled = False
            'Else
            '    cboCountry.Enabled = True
            '    cboCurrency.Enabled = True
            '    chkEmployeeSign.Enabled = True
            '    chkShowGroupByBankBranch.Enabled = True
            '    chkSignatory1.Enabled = True
            '    chkSignatory2.Enabled = True
            '    chkSignatory3.Enabled = True
            'End If
            cboCountry.Enabled = False
            cboCurrency.Enabled = True
            chkEmployeeSign.Enabled = False
            chkShowGroupByBankBranch.Enabled = False
            chkShowGroupByBankBranch.Text = Language.getMessage(mstrModuleName, 15, "Show Group By Bank / Branch")
            chkSignatory1.Enabled = False
            chkSignatory2.Enabled = False
            chkSignatory3.Enabled = False

            'Sohail (18 Nov 2016) -- Start
            lblPostingDate.Visible = False
            dtpPostingDate.Visible = False
            'Sohail (18 Nov 2016) -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            lnkOtherSetting.Visible = False
            'Sohail (12 Feb 2018) -- End

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            lblMembershipRepo.Visible = False
            cboMembershipRepo.Visible = False
            cboMembershipRepo.SelectedValue = 0
            'Sohail (08 Jul 2021) -- End

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            If CInt(cboReportType.SelectedIndex) >= 0 Then
                cboCondition.SelectedIndex = 0
            End If
            'Nilay (10-Nov-2016) -- End

            Select Case CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value)
                Case enBankPaymentReport.Bank_payment_List
                    cboCountry.Enabled = True
                    chkEmployeeSign.Enabled = True
                    chkShowGroupByBankBranch.Enabled = True
                    chkSignatory1.Enabled = True
                    chkSignatory2.Enabled = True
                    chkSignatory3.Enabled = True

                    'Anjan [03 Feb 2014] -- Start
                    'ENHANCEMENT : Requested by Rutta
                    chkDefinedSignatory.Enabled = True
                    chkDefinedSignatory2.Enabled = True
                    chkDefinedSignatory3.Enabled = True

                    chkShowBankCode.Checked = True
                    chkShowBranchCode.Checked = True


                    chkShowBankCode.Enabled = True
                    chkShowBranchCode.Enabled = True
                    'Anjan [03 Feb 2014 ] -- End

                    'Sohail (24 Mar 2014) -- Start
                    'Issue - Provide Show Report Header option
                    chkShowReportHeader.Visible = True
                    chkShowReportHeader.Checked = True
                    chkShowAccountType.Visible = True
                    chkShowAccountType.Checked = False
                    'Sohail (24 Mar 2014) -- End

                    'Anjan [14 Mar 2014] -- Start
                    'ENHANCEMENT : Requested by Rutta
                    chkShowSortCode.Enabled = True
                    'Anjan [14 Mar 2014 ] -- End


                Case enBankPaymentReport.Electronic_Fund_Transfer
                    chkShowGroupByBankBranch.Enabled = True
                    chkShowGroupByBankBranch.Text = Language.getMessage(mstrModuleName, 16, "Show Group By Bank")
                    'Sohail (01 Apr 2014) -- Start
                    'ENHANCEMENT - 
                    chkShowReportHeader.Visible = True
                    chkShowReportHeader.Checked = True
                    'Sohail (01 Apr 2014) -- End
                    'Hemant (19 June 2019) -- Start
                    'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                    chkShowSelectedBankInfo.Visible = True
                    'Hemant (19 June 2019) -- End
                    'Hemant 20 Jul 2019) -- Start
                    'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                    chkShowCompanyGrpInfo.Visible = True
                    'Hemant 20 Jul 2019) -- End
                    'Hemant (25 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Is it impossible to show the logo in the excel advance format?  
                    chkShowCompanyLogoOnReport.Visible = True
                    'Hemant (25 Jul 2019) -- End
                    'Sohail (12 Feb 2018) -- Start
                    'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                Case enBankPaymentReport.Salary_Format_WPS
                    lnkOtherSetting.Visible = True
                    'Sohail (12 Feb 2018) -- End

            End Select
            'Sohail (29 Mar 2013) -- End

            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (25 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'Call objBankPaymentList.setDefaultOrderBy(CInt(cboReportType.SelectedIndex))

            'Sohail (14 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'If CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.EFT_PSPF Then
            '    cboMembership.Enabled = True
            'Else
            '    cboMembership.SelectedValue = 0
            '    cboMembership.Enabled = False
            'End If
            'Sohail (14 Mar 2013) -- End
            'Sohail (25 Jan 2013) -- End

            'Sohail (21 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            If CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Electronic_Fund_Transfer Then
                chkShowFNameSeparately.Enabled = True
                'Sohail (24 Dec 2013) -- Start
                'Enhancement - Oman
                chkShowPayrollPeriod.Enabled = True
                chkShowSortCode.Enabled = True
                'Sohail (24 Dec 2013) -- End
                'Sohail (22 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_CityDirect Then
                    lnkEFTCityBankExport.Visible = True
                Else
                    lnkEFTCityBankExport.Visible = False
                End If
                'Sohail (22 Mar 2013) -- End

                'Sohail (08 Dec 2014) -- Start
                'Enhancement - New EFT integration EFT CBA.
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_CBA Then
                    lnkEFT_CBA_Export.Location = lnkEFTCityBankExport.Location
                    lnkEFT_CBA_Export.Size = lnkEFTCityBankExport.Size
                    lnkEFT_CBA_Export.Visible = True
                Else
                    lnkEFT_CBA_Export.Visible = False
                End If

                If ConfigParameter._Object._MobileMoneyEFTIntegration = enMobileMoneyEFTIntegration.MPESA Then
                    lnkMobileMoneyEFTMPesaExport.Visible = True
                Else
                    lnkMobileMoneyEFTMPesaExport.Visible = False
                End If
                'Sohail (08 Dec 2014) -- End

                'Sohail (12 Dec 2014) -- Start
                'VFT Enhancement - New EFT Report EFT T24.
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_T24 Then
                    lnkEFT_T24_Export.Location = lnkEFTCityBankExport.Location
                    lnkEFT_T24_Export.Size = lnkEFTCityBankExport.Size
                    lnkEFT_T24_Export.Visible = True
                Else
                    lnkEFT_T24_Export.Visible = False
                End If
                'Sohail (12 Dec 2014) -- End

                'Sohail (24 Dec 2014) -- Start
                'AMI Enhancement - New EFT Report EFT EXIM.
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_EXIM Then
                    lnkEFT_EXIM_Export.Location = lnkEFTCityBankExport.Location
                    lnkEFT_EXIM_Export.Size = lnkEFTCityBankExport.Size
                    lnkEFT_EXIM_Export.Visible = True
                Else
                    lnkEFT_EXIM_Export.Visible = False
                End If
                'Sohail (24 Dec 2014) -- End

                'Sohail (09 Mar 2015) -- Start
                'Enhancement - New EFT Report Custom CSV Report.
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_CUSTOM_CSV Then
                    lnkEFT_Custom_CSV_Export.Location = lnkEFTCityBankExport.Location
                    lnkEFT_Custom_CSV_Export.Size = lnkEFTCityBankExport.Size
                    lnkEFT_Custom_CSV_Export.Visible = True
                Else
                    lnkEFT_Custom_CSV_Export.Visible = False
                End If

                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_CUSTOM_XLS Then
                    lnkEFT_Custom_XLS_Export.Location = lnkEFTCityBankExport.Location
                    lnkEFT_Custom_XLS_Export.Size = lnkEFTCityBankExport.Size
                    lnkEFT_Custom_XLS_Export.Visible = True
                Else
                    lnkEFT_Custom_XLS_Export.Visible = False
                End If
                'Sohail (09 Mar 2015) -- End

                'Sohail (01 Jun 2016) -- Start
                'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_FLEX_CUBE_RETAIL Then
                    lnkEFT_FlexCubeRetailGEFU_Export.Location = lnkEFTCityBankExport.Location
                    lnkEFT_FlexCubeRetailGEFU_Export.Size = lnkEFTCityBankExport.Size
                    lnkEFT_FlexCubeRetailGEFU_Export.Visible = True
                    lblPostingDate.Visible = True
                    dtpPostingDate.Visible = True
                Else
                    lnkEFT_FlexCubeRetailGEFU_Export.Visible = False
                    'Sohail (18 Nov 2016) -- Start
                    'lblPostingDate.Visible = False
                    'dtpPostingDate.Visible = False
                    'Sohail (18 Nov 2016) -- End
                End If
                'Sohail (01 Jun 2016) -- End

                'Nilay (10-Nov-2016) -- Start
                'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_ECO_BANK Then
                    lnkEFT_ECO_Bank.Location = lnkEFTCityBankExport.Location
                    lnkEFT_ECO_Bank.Size = lnkEFTCityBankExport.Size
                    lnkEFT_ECO_Bank.Visible = True
                    lblPostingDate.Visible = True
                    dtpPostingDate.Visible = True
                Else
                    lnkEFT_ECO_Bank.Visible = False
                    'Sohail (18 Nov 2016) -- Start
                    'lblPostingDate.Visible = False
                    'dtpPostingDate.Visible = False
                    'Sohail (18 Nov 2016) -- End
                End If
                'Nilay (10-Nov-2016) -- End

                'S.SANDEEP [04-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_NATIONAL_BANK_OF_MALAWI Then
                    lnkEFTNationalBankMalawi.Location = lnkEFTCityBankExport.Location
                    lnkEFTNationalBankMalawi.Size = lnkEFTCityBankExport.Size
                    lnkEFTNationalBankMalawi.Visible = True
                Else
                    lnkEFTNationalBankMalawi.Visible = False
                End If
                'S.SANDEEP [04-May-2018] -- END

                'Anjan [21 November 2016] -- Start
                'ENHANCEMENT : Including NEW ACB EFT integration report.
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_ACB_T24WEB Then
                    lnkEFT_T24_Web.Location = lnkEFTCityBankExport.Location
                    lnkEFT_T24_Web.Size = lnkEFTCityBankExport.Size
                    lnkEFT_T24_Web.Visible = True
                    lblPostingDate.Visible = True
                    dtpPostingDate.Visible = True
                Else
                    lnkEFT_T24_Web.Visible = False
                End If
                'Anjan [21 November 2016] -- End

                'Shani(28-FEB-2017) -- Start
                'Enhancement - Add New Bank Payment List Integration for TAMA
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_BANK_PAYMENT_LETTER Then
                    lnkBankPaymentLetter.Location = lnkEFTCityBankExport.Location
                    lnkBankPaymentLetter.Size = lnkEFTCityBankExport.Size
                    lnkBankPaymentLetter.Visible = True
                    chkLetterhead.Visible = True
                    chkAddresstoEmployeeBank.Visible = True
                Else
                    lnkBankPaymentLetter.Visible = False
                    chkLetterhead.Visible = False
                    chkAddresstoEmployeeBank.Visible = False
                End If
                'Shani(28-FEB-2017) -- End

                'Sohail (20 Sep 2017) -- Start
                'Paytech Enhancement - 69.1 - EFT file for barclays bank should be in csv format. And contain the codes representing a specific item. A sample EFT file was shared with Rutta.
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_BARCLAYS_BANK Then
                    lnkEFTBarclaysBankExport.Location = lnkEFTCityBankExport.Location
                    lnkEFTBarclaysBankExport.Size = lnkEFTCityBankExport.Size
                    lnkEFTBarclaysBankExport.Visible = True
                    lblPostingDate.Visible = True
                    dtpPostingDate.Visible = True
                Else
                    lnkEFT_ECO_Bank.Visible = False
                End If
                'Sohail (20 Sep 2017) -- End


                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                chkShowEmployeeCode.Visible = True
                'Pinkal (12-Jun-2014) -- End


                'S.SANDEEP [19-SEP-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002533|ARUTI-334}
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.DFT_CITY_BANK Then
                    lnkEFTCityBank.Location = lnkEFTCityBankExport.Location
                    lnkEFTCityBank.Size = lnkEFTCityBankExport.Size
                    lnkEFTCityBank.Visible = True

                    lblPostingDate.Visible = True
                    dtpPostingDate.Visible = True
                Else
                    lnkEFTCityBank.Visible = False
                End If
                'S.SANDEEP [19-SEP-2018] -- END

                'Sohail (04 Nov 2019) -- Start
                'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_FNB_BANK Then
                    lnkEFT_FNB_Bank_Export.Location = lnkEFTCityBankExport.Location
                    lnkEFT_FNB_Bank_Export.Size = lnkEFTCityBankExport.Size
                    lnkEFT_FNB_Bank_Export.Visible = True
                Else
                    lnkEFT_FNB_Bank_Export.Visible = False
                End If
                'Sohail (04 Nov 2019) -- End

                'Hemant (11 Dec 2019) -- Start
                'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_STANDARD_CHARTERED_BANK_S2B Then
                    lnkEFTStandardCharteredBank_S2B.Location = lnkEFTCityBankExport.Location
                    lnkEFTStandardCharteredBank_S2B.Size = lnkEFTCityBankExport.Size
                    lnkEFTStandardCharteredBank_S2B.Visible = True

                    lblPostingDate.Visible = True
                    dtpPostingDate.Visible = True
                Else
                    lnkEFTStandardCharteredBank_S2B.Visible = False
                End If
                'Hemant (11 Dec 2019) -- End

                'Hemant (29 Jun 2020) -- Start
                'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_ABSA_BANK Then
                    lnkEFT_ABSA_Bank.Location = lnkEFTCityBankExport.Location
                    lnkEFT_ABSA_Bank.Size = lnkEFTCityBankExport.Size
                    lnkEFT_ABSA_Bank.Visible = True

                    lblPostingDate.Visible = True
                    dtpPostingDate.Visible = True
                Else
                    lnkEFT_ABSA_Bank.Visible = False
                End If
                'Hemant (29 Jun 2020) -- End

                'Hemant (24 Dec 2020) -- Start
                'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_NATIONAL_BANK_OF_MALAWI_XLSX Then
                    lnkEFTNationalBankMalawiXLSX.Location = lnkEFTCityBankExport.Location
                    lnkEFTNationalBankMalawiXLSX.Size = lnkEFTCityBankExport.Size
                    lnkEFTNationalBankMalawiXLSX.Visible = True

                    lblPostingDate.Visible = True
                    dtpPostingDate.Visible = True
                Else
                    lnkEFTNationalBankMalawiXLSX.Visible = False
                End If
                'Hemant (24 Dec 2020) -- End

                'Sohail (08 Jul 2021) -- Start
                'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_EQUITY_BANK_OF_KENYA Then
                    lnkEFTEquityBankKenya.Location = lnkEFTCityBankExport.Location
                    lnkEFTEquityBankKenya.Size = lnkEFTCityBankExport.Size
                    lnkEFTEquityBankKenya.Visible = True

                    lblMembershipRepo.Visible = True
                    cboMembershipRepo.Visible = True
                Else
                    lnkEFTEquityBankKenya.Visible = False
                End If

                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_NATIONAL_BANK_OF_KENYA Then
                    lnkEFTNationalBankKenya.Location = lnkEFTCityBankExport.Location
                    lnkEFTNationalBankKenya.Size = lnkEFTCityBankExport.Size
                    lnkEFTNationalBankKenya.Visible = True
                Else
                    lnkEFTNationalBankKenya.Visible = False
                End If
                'Sohail (08 Jul 2021) -- End

                'Sohail (27 Jul 2021) -- Start
                'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
                If ConfigParameter._Object._EFTIntegration = enEFTIntegration.EFT_CITI_BANK_KENYA Then
                    lnkEFTCitiBankKenya.Location = lnkEFTCityBankExport.Location
                    lnkEFTCitiBankKenya.Size = lnkEFTCityBankExport.Size
                    lnkEFTCitiBankKenya.Visible = True
                Else
                    lnkEFTCitiBankKenya.Visible = False
                End If
                'Sohail (27 Jul 2021) -- End

            Else
                chkShowFNameSeparately.Enabled = False
                lnkEFTCityBankExport.Visible = False 'Sohail (22 Mar 2013)

                'Sohail (08 Dec 2014) -- Start
                'Enhancement - New EFT integration EFT CBA || New Mobile Money EFT integration with MPESA.
                lnkEFT_CBA_Export.Visible = False
                lnkMobileMoneyEFTMPesaExport.Visible = False
                'Sohail (08 Dec 2014) -- End
                lnkEFT_T24_Export.Visible = False 'Sohail (12 Dec 2014)
                lnkEFT_EXIM_Export.Visible = False 'Sohail (24 Dec 2014)
                'Sohail (09 Mar 2015) -- Start
                'Enhancement - New EFT Report Custom CSV Report.
                lnkEFT_Custom_CSV_Export.Visible = False
                lnkEFT_Custom_XLS_Export.Visible = False
                'Sohail (09 Mar 2015) -- End
                'Sohail (01 Jun 2016) -- Start
                'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.
                lblPostingDate.Visible = False
                dtpPostingDate.Visible = False
                lnkEFT_FlexCubeRetailGEFU_Export.Visible = False
                'Sohail (01 Jun 2016) -- End

                'Sohail (24 Dec 2013) -- Start
                'Enhancement - Oman
                chkShowPayrollPeriod.Enabled = False
                chkShowPayrollPeriod.Checked = False
                If CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = 1 Then
                    chkShowSortCode.Enabled = True
                    'Else
                    '    chkShowSortCode.Enabled = False
                End If

                chkShowSortCode.Checked = False
                'Sohail (24 Dec 2013) -- End

                'Shani(28-FEB-2017) -- Start
                'Enhancement - Add New Bank Payment List Integration for TAMA
                lnkBankPaymentLetter.Visible = False
                chkLetterhead.Visible = False
                chkAddresstoEmployeeBank.Visible = False
                'Shani(28-FEB-2017) -- End
                'Sohail (20 Sep 2017) -- Start
                'Paytech Enhancement - 69.1 - EFT file for barclays bank should be in csv format. And contain the codes representing a specific item. A sample EFT file was shared with Rutta.
                lnkEFTBarclaysBankExport.Visible = False
                'Sohail (20 Sep 2017) -- End

                'S.SANDEEP [04-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
                lnkEFTNationalBankMalawi.Visible = False
                'S.SANDEEP [04-May-2018] -- END

                'S.SANDEEP [19-SEP-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002533|ARUTI-334}
                lnkEFTCityBank.Visible = False
                'S.SANDEEP [19-SEP-2018] -- END
                'Sohail (04 Nov 2019) -- Start
                'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
                lnkEFT_FNB_Bank_Export.Visible = False
                'Sohail (04 Nov 2019) -- End

                'Hemant (11 Dec 2019) -- Start
                'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
                lnkEFTStandardCharteredBank_S2B.Visible = False
                'Hemant (11 Dec 2019) -- End

                'Hemant (29 Jun 2020) -- Start
                'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
                lnkEFT_ABSA_Bank.Visible = False
                'Hemant (29 Jun 2020) -- End

                'Hemant (24 Dec 2020) -- Start
                'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
                lnkEFTNationalBankMalawiXLSX.Visible = False
                'Hemant (24 Dec 2020) -- End

                'Sohail (08 Jul 2021) -- Start
                'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
                lnkEFTEquityBankKenya.Visible = False
                lnkEFTNationalBankKenya.Visible = False
                'Sohail (08 Jul 2021) -- End
                'Sohail (27 Jul 2021) -- Start
                'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
                lnkEFTCitiBankKenya.Visible = False
                'Sohail (27 Jul 2021) -- End
            End If
            'Sohail (21 Mar 2013) -- End

            If CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.EFT_SFIBANK Then
                cboEmployeeName.SelectedValue = 0
                cboBankName.Enabled = False
                cboBranchName.SelectedValue = 0
                cboBranchName.Enabled = False
                txtEmpcode.Enabled = False
                chkShowSortCode.Enabled = True
            ElseIf CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.EFT_StandardCharteredBank Then
                cboEmployeeName.SelectedValue = 0
                cboBankName.Enabled = False
                cboBranchName.SelectedValue = 0
                cboBranchName.Enabled = False
                chkShowSortCode.Enabled = False
                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance Payment.
            ElseIf CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.EFT_ADVANCE_SALARY_CSV Then
                lnkEFT_Custom_CSV_Export.Location = lnkEFTCityBankExport.Location
                lnkEFT_Custom_CSV_Export.Size = lnkEFTCityBankExport.Size
                lnkEFT_Custom_CSV_Export.Visible = True
            ElseIf CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.EFT_ADVANCE_SALARY_XLS Then
                lnkEFT_Custom_XLS_Export.Location = lnkEFTCityBankExport.Location
                lnkEFT_Custom_XLS_Export.Size = lnkEFTCityBankExport.Size
                lnkEFT_Custom_XLS_Export.Visible = True
                'Sohail (25 Mar 2015) -- End

            End If

            'S.SANDEEP [ 07 JAN 2014 ] -- START
            'Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value))

            If (CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Electronic_Fund_Transfer _
               AndAlso Company._Object._Countryunkid = 162) OrElse CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Bank_Payment_Voucher Then 'OMAN EFT 

                'Pinkal (12-Jun-2014) -- Start  'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]  OrElse CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Bank_Payment_Voucher


                chkEmployeeSign.Checked = False
                chkShowGroupByBankBranch.Checked = False
                chkSignatory1.Checked = False
                chkSignatory2.Checked = False
                chkSignatory3.Checked = False
                cboCountry.SelectedIndex = 0
                'cboCurrency.SelectedIndex = 0 'Sohail (01 Apr 2014)
                mstrStringIds = ""
                mstrStringName = ""
                mintViewIdx = 0
                cboReportMode.Enabled = True
                chkShowFNameSeparately.Checked = False
                chkShowPayrollPeriod.Checked = True
                chkShowSortCode.Checked = True
                cboCountry.Enabled = False
                cboCurrency.Enabled = True
                chkEmployeeSign.Enabled = False
                chkShowGroupByBankBranch.Enabled = False
                chkSignatory1.Enabled = False
                chkSignatory2.Enabled = False
                chkSignatory3.Enabled = False
                chkShowPayrollPeriod.Checked = False
                chkShowSortCode.Checked = False
                chkShowPayrollPeriod.Enabled = False
                chkShowSortCode.Enabled = False
                chkShowFNameSeparately.Enabled = False
                chkDefinedSignatory.Enabled = False

                'Anjan [03 Feb 2014] -- Start
                'ENHANCEMENT : Requested by Rutta
                chkShowBankCode.Checked = False
                chkShowBranchCode.Checked = False
                chkDefinedSignatory2.Checked = False
                chkDefinedSignatory3.Checked = False

                chkShowBankCode.Enabled = False
                chkShowBranchCode.Enabled = False
                chkDefinedSignatory2.Enabled = False
                chkDefinedSignatory3.Enabled = False

                'Anjan [03 Feb 2014 ] -- End

                'Sohail (15 Mar 2014) -- Start
                'Enhancement - Grouping on cut off amount.
                lblCutOffAmount.Visible = True
                txtCutOffAmount.Visible = True
                'Sohail (15 Mar 2014) -- End


                'Sohail (01 Apr 2014) -- Start
                'ENHANCEMENT - 
                'Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Bank_payment_List)

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                If CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Bank_Payment_Voucher Then
                    Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Bank_Payment_Voucher)
                    chkShowReportHeader.Visible = True
                    chkShowReportHeader.Checked = True
                    chkShowEmployeeCode.Visible = False
                Else
                    Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.EFT_VOLTAMP)
                End If
                'Pinkal (12-Jun-2014) -- End


                'Sohail (01 Apr 2014) -- End
            Else
                'Sohail (15 Mar 2014) -- Start
                'Enhancement - Grouping on cut off amount.
                lblCutOffAmount.Visible = False
                txtCutOffAmount.Visible = False
                txtCutOffAmount.Text = ""

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                chkShowEmployeeCode.Visible = True
                'Pinkal (12-Jun-2014) -- End

                'Sohail (15 Mar 2014) -- End
                Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value))
            End If
            'S.SANDEEP [ 07 JAN 2014 ] -- END




            txtOrderBy.Text = objBankPaymentList.OrderByDisplay

            'Sohail (16 Jul 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (16 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboCompanyBankName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompanyBankName.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsList As New DataSet
        Try
            dsList = objCompanyBank.GetComboList(Company._Object._Companyunkid, 2, "List", " cfbankbranch_master.bankgroupunkid = " & CInt(cboCompanyBankName.SelectedValue) & " ")
            With cboCompanyBranchName
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompanyBankName_SelectedIndexChanged", mstrModuleName)
        Finally
            objBankBranch = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Sub
    'Sohail (16 Jul 2012) -- End

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboCompanyBranchName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompanyBranchName.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsCombos As New DataSet
        Try
            dsCombos = objCompanyBank.GetComboListBankAccount("Account", Company._Object._Companyunkid, True, CInt(cboCompanyBankName.SelectedValue), CInt(cboCompanyBranchName.SelectedValue))
            With cboCompanyAccountNo
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Account")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompanyBranchName_SelectedIndexChanged", mstrModuleName)
        Finally
            objCompanyBank = Nothing
        End Try
    End Sub
    'Sohail (21 Jul 2012) -- End

    'Sohail (07 Dec 2013) -- Start
    'Enhancement - OMAN
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPayment As New clsPayment_tran
        Dim dsCombos As DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objPayment.Get_DIST_ChequeNo("Cheque", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboPeriod.SelectedValue))
            dsCombos = objPayment.Get_DIST_ChequeNo("Cheque", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, FinancialYear._Object._DatabaseName, CInt(cboPeriod.SelectedValue))
            'S.SANDEEP [04 JUN 2015] -- END
            With cboChequeNo
                .ValueMember = "chequeno"
                .DisplayMember = "chequeno"
                .DataSource = dsCombos.Tables("Cheque")
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPayment = Nothing
        End Try
    End Sub
    'Sohail (07 Dec 2013) -- End

    'Sohail (12 Feb 2018) -- Start
    'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
    Private Sub cboPresentDays_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPresentDays.GotFocus _
                                                                                                , cboBasicSalary.GotFocus _
                                                                                                , cboSocialSecurity.GotFocus _
                                                                                                , cboExtraIncome.GotFocus _
                                                                                                , cboAbsentDays.GotFocus
        'Sohail (16 Apr 2018) - [cboExtraIncome, cboAbsentDays]

        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
            End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPresentDays_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPresentDays.KeyPress _
                                                                                                                        , cboBasicSalary.KeyPress _
                                                                                                                        , cboSocialSecurity.KeyPress _
                                                                                                                        , cboExtraIncome.KeyPress _
                                                                                                                        , cboAbsentDays.KeyPress
        'Sohail (16 Apr 2018) - [cboExtraIncome, cboAbsentDays]

        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
            End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPresentDays_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPresentDays.Leave _
                                                                                                , cboBasicSalary.Leave _
                                                                                                , cboSocialSecurity.Leave _
                                                                                                , cboExtraIncome.Leave _
                                                                                                , cboAbsentDays.Leave
        'Sohail (16 Apr 2018) - [cboExtraIncome, cboAbsentDays]

        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPresentDays_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPresentDays.SelectedIndexChanged _
                                                                                                                , cboBasicSalary.SelectedIndexChanged _
                                                                                                                , cboSocialSecurity.SelectedIndexChanged _
                                                                                                                , cboExtraIncome.SelectedIndexChanged _
                                                                                                                , cboAbsentDays.SelectedIndexChanged
        'Sohail (16 Apr 2018) - [cboExtraIncome, cboAbsentDays]

        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If CInt(cbo.SelectedValue) < 0 Then Call SetDefaultSearchText(cbo)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Feb 2018) -- End

#End Region

    'Pinkal (11-MAY-2012) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.lblEmpCode.Text = Language._Object.getCaption(Me.lblEmpCode.Name, Me.lblEmpCode.Text)
			Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
			Me.lblCountryName.Text = Language._Object.getCaption(Me.lblCountryName.Name, Me.lblCountryName.Text)
			Me.lblBranchName.Text = Language._Object.getCaption(Me.lblBranchName.Name, Me.lblBranchName.Text)
			Me.lblBankName.Text = Language._Object.getCaption(Me.lblBankName.Name, Me.lblBankName.Text)
			Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblAmountTo.Text = Language._Object.getCaption(Me.lblAmountTo.Name, Me.lblAmountTo.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.chkEmployeeSign.Text = Language._Object.getCaption(Me.chkEmployeeSign.Name, Me.chkEmployeeSign.Text)
			Me.chkSignatory3.Text = Language._Object.getCaption(Me.chkSignatory3.Name, Me.chkSignatory3.Text)
			Me.chkSignatory2.Text = Language._Object.getCaption(Me.chkSignatory2.Name, Me.chkSignatory2.Text)
			Me.chkSignatory1.Text = Language._Object.getCaption(Me.chkSignatory1.Name, Me.chkSignatory1.Text)
			Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)
			Me.chkShowGroupByBankBranch.Text = Language._Object.getCaption(Me.chkShowGroupByBankBranch.Name, Me.chkShowGroupByBankBranch.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lblReportMode.Text = Language._Object.getCaption(Me.lblReportMode.Name, Me.lblReportMode.Text)
			Me.lblCompanyBankName.Text = Language._Object.getCaption(Me.lblCompanyBankName.Name, Me.lblCompanyBankName.Text)
			Me.lblCompanyBranchName.Text = Language._Object.getCaption(Me.lblCompanyBranchName.Name, Me.lblCompanyBranchName.Text)
			Me.lblCompanyAccountNo.Text = Language._Object.getCaption(Me.lblCompanyAccountNo.Name, Me.lblCompanyAccountNo.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.chkShowFNameSeparately.Text = Language._Object.getCaption(Me.chkShowFNameSeparately.Name, Me.chkShowFNameSeparately.Text)
			Me.lnkEFTCityBankExport.Text = Language._Object.getCaption(Me.lnkEFTCityBankExport.Name, Me.lnkEFTCityBankExport.Text)
			Me.chkDefinedSignatory.Text = Language._Object.getCaption(Me.chkDefinedSignatory.Name, Me.chkDefinedSignatory.Text)
			Me.lblChequeNo.Text = Language._Object.getCaption(Me.lblChequeNo.Name, Me.lblChequeNo.Text)
			Me.chkShowSortCode.Text = Language._Object.getCaption(Me.chkShowSortCode.Name, Me.chkShowSortCode.Text)
			Me.chkShowPayrollPeriod.Text = Language._Object.getCaption(Me.chkShowPayrollPeriod.Name, Me.chkShowPayrollPeriod.Text)
			Me.chkDefinedSignatory3.Text = Language._Object.getCaption(Me.chkDefinedSignatory3.Name, Me.chkDefinedSignatory3.Text)
			Me.chkDefinedSignatory2.Text = Language._Object.getCaption(Me.chkDefinedSignatory2.Name, Me.chkDefinedSignatory2.Text)
			Me.chkShowBranchCode.Text = Language._Object.getCaption(Me.chkShowBranchCode.Name, Me.chkShowBranchCode.Text)
			Me.chkShowBankCode.Text = Language._Object.getCaption(Me.chkShowBankCode.Name, Me.chkShowBankCode.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblCutOffAmount.Text = Language._Object.getCaption(Me.lblCutOffAmount.Name, Me.lblCutOffAmount.Text)
			Me.chkShowAccountType.Text = Language._Object.getCaption(Me.chkShowAccountType.Name, Me.chkShowAccountType.Text)
			Me.chkShowEmployeeCode.Text = Language._Object.getCaption(Me.chkShowEmployeeCode.Name, Me.chkShowEmployeeCode.Text)
			Me.lnkEFT_CBA_Export.Text = Language._Object.getCaption(Me.lnkEFT_CBA_Export.Name, Me.lnkEFT_CBA_Export.Text)
			Me.lnkMobileMoneyEFTMPesaExport.Text = Language._Object.getCaption(Me.lnkMobileMoneyEFTMPesaExport.Name, Me.lnkMobileMoneyEFTMPesaExport.Text)
			Me.lnkEFT_T24_Export.Text = Language._Object.getCaption(Me.lnkEFT_T24_Export.Name, Me.lnkEFT_T24_Export.Text)
			Me.lnkEFT_EXIM_Export.Text = Language._Object.getCaption(Me.lnkEFT_EXIM_Export.Name, Me.lnkEFT_EXIM_Export.Text)
			Me.lnkEFT_Custom_CSV_Export.Text = Language._Object.getCaption(Me.lnkEFT_Custom_CSV_Export.Name, Me.lnkEFT_Custom_CSV_Export.Text)
			Me.lnkEFT_Custom_XLS_Export.Text = Language._Object.getCaption(Me.lnkEFT_Custom_XLS_Export.Name, Me.lnkEFT_Custom_XLS_Export.Text)
			Me.lnkEFT_FlexCubeRetailGEFU_Export.Text = Language._Object.getCaption(Me.lnkEFT_FlexCubeRetailGEFU_Export.Name, Me.lnkEFT_FlexCubeRetailGEFU_Export.Text)
			Me.lblPostingDate.Text = Language._Object.getCaption(Me.lblPostingDate.Name, Me.lblPostingDate.Text)
			Me.lnkEFT_ECO_Bank.Text = Language._Object.getCaption(Me.lnkEFT_ECO_Bank.Name, Me.lnkEFT_ECO_Bank.Text)
			Me.lnkEFT_T24_Web.Text = Language._Object.getCaption(Me.lnkEFT_T24_Web.Name, Me.lnkEFT_T24_Web.Text)
			Me.lblPasscode.Text = Language._Object.getCaption(Me.lblPasscode.Name, Me.lblPasscode.Text)
			Me.lblID.Text = Language._Object.getCaption(Me.lblID.Name, Me.lblID.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.lblLoanHeads.Text = Language._Object.getCaption(Me.lblLoanHeads.Name, Me.lblLoanHeads.Text)
			Me.lblLink.Text = Language._Object.getCaption(Me.lblLink.Name, Me.lblLink.Text)
			Me.lnkBankPaymentLetter.Text = Language._Object.getCaption(Me.lnkBankPaymentLetter.Name, Me.lnkBankPaymentLetter.Text)
			Me.chkLetterhead.Text = Language._Object.getCaption(Me.chkLetterhead.Name, Me.chkLetterhead.Text)
			Me.chkShowReportHeader.Text = Language._Object.getCaption(Me.chkShowReportHeader.Name, Me.chkShowReportHeader.Text)
			Me.chkAddresstoEmployeeBank.Text = Language._Object.getCaption(Me.chkAddresstoEmployeeBank.Name, Me.chkAddresstoEmployeeBank.Text)
			Me.lnkEFTBarclaysBankExport.Text = Language._Object.getCaption(Me.lnkEFTBarclaysBankExport.Name, Me.lnkEFTBarclaysBankExport.Text)
			Me.lnkOtherSetting.Text = Language._Object.getCaption(Me.lnkOtherSetting.Name, Me.lnkOtherSetting.Text)
			Me.lblPresentDays.Text = Language._Object.getCaption(Me.lblPresentDays.Name, Me.lblPresentDays.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblBasicSalary.Text = Language._Object.getCaption(Me.lblBasicSalary.Name, Me.lblBasicSalary.Text)
			Me.lblSocialSecurity.Text = Language._Object.getCaption(Me.lblSocialSecurity.Name, Me.lblSocialSecurity.Text)
			Me.lblExtraIncome.Text = Language._Object.getCaption(Me.lblExtraIncome.Name, Me.lblExtraIncome.Text)
			Me.lblIdentityType.Text = Language._Object.getCaption(Me.lblIdentityType.Name, Me.lblIdentityType.Text)
			Me.lblPaymentType.Text = Language._Object.getCaption(Me.lblPaymentType.Name, Me.lblPaymentType.Text)
			Me.lblAbsentDays.Text = Language._Object.getCaption(Me.lblAbsentDays.Name, Me.lblAbsentDays.Text)
            Me.lblCustomCurrFormat.Text = Language._Object.getCaption(Me.lblCustomCurrFormat.Name, Me.lblCustomCurrFormat.Text)
			Me.lnkEFTNationalBankMalawi.Text = Language._Object.getCaption(Me.lnkEFTNationalBankMalawi.Name, Me.lnkEFTNationalBankMalawi.Text)
			Me.lnkEFTCityBank.Text = Language._Object.getCaption(Me.lnkEFTCityBank.Name, Me.lnkEFTCityBank.Text)
			Me.chkShowSelectedBankInfo.Text = Language._Object.getCaption(Me.chkShowSelectedBankInfo.Name, Me.chkShowSelectedBankInfo.Text)
            Me.chkShowCompanyGrpInfo.Text = Language._Object.getCaption(Me.chkShowCompanyGrpInfo.Name, Me.chkShowCompanyGrpInfo.Text)
            Me.chkShowCompanyLogoOnReport.Text = Language._Object.getCaption(Me.chkShowCompanyLogoOnReport.Name, Me.chkShowCompanyLogoOnReport.Text)
            Me.lnkEFT_FNB_Bank_Export.Text = Language._Object.getCaption(Me.lnkEFT_FNB_Bank_Export.Name, Me.lnkEFT_FNB_Bank_Export.Text)
			Me.lnkEFTStandardCharteredBank_S2B.Text = Language._Object.getCaption(Me.lnkEFTStandardCharteredBank_S2B.Name, Me.lnkEFTStandardCharteredBank_S2B.Text)
			Me.lnkEFT_ABSA_Bank.Text = Language._Object.getCaption(Me.lnkEFT_ABSA_Bank.Name, Me.lnkEFT_ABSA_Bank.Text)
			Me.chkSaveAsTXT_WPS.Text = Language._Object.getCaption(Me.chkSaveAsTXT_WPS.Name, Me.chkSaveAsTXT_WPS.Text)
            Me.lnkEFTNationalBankMalawiXLSX.Text = Language._Object.getCaption(Me.lnkEFTNationalBankMalawiXLSX.Name, Me.lnkEFTNationalBankMalawiXLSX.Text)
			Me.lblMembershipRepo.Text = Language._Object.getCaption(Me.lblMembershipRepo.Name, Me.lblMembershipRepo.Text)
			Me.lnkEFTEquityBankKenya.Text = Language._Object.getCaption(Me.lnkEFTEquityBankKenya.Name, Me.lnkEFTEquityBankKenya.Text)
			Me.lnkEFTNationalBankKenya.Text = Language._Object.getCaption(Me.lnkEFTNationalBankKenya.Name, Me.lnkEFTNationalBankKenya.Text)
            Me.lnkEFTCitiBankKenya.Text = Language._Object.getCaption(Me.lnkEFTCitiBankKenya.Name, Me.lnkEFTCitiBankKenya.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is mandatory information.Please select Period.")
			Language.setMessage(mstrModuleName, 2, "Bank Payment List")
			Language.setMessage(mstrModuleName, 3, "Electronic Funds Transfer")
			Language.setMessage(mstrModuleName, 4, "Currency is mandatory information. Please select Currency.")
			Language.setMessage(mstrModuleName, 5, "Please select Company Bank.")
			Language.setMessage(mstrModuleName, 6, "Please select Company Bank Branch.")
			Language.setMessage(mstrModuleName, 7, "Please select Company Bank Account.")
			Language.setMessage(mstrModuleName, 8, "DRAFT")
			Language.setMessage(mstrModuleName, 9, "AUTHORIZED")
			Language.setMessage(mstrModuleName, 10, "EFT Standard Bank")
			Language.setMessage(mstrModuleName, 11, "Data Exported Successfuly.")
			Language.setMessage(mstrModuleName, 12, "EFT Standard Chartered Bank")
			Language.setMessage(mstrModuleName, 13, "EFT SFI Bank")
			Language.setMessage(mstrModuleName, 14, "Please select Authorize mode to generate report.")
			Language.setMessage(mstrModuleName, 15, "Show Group By Bank / Branch")
			Language.setMessage(mstrModuleName, 16, "Show Group By Bank")
			Language.setMessage(mstrModuleName, 17, "Please Set Data File Export Path From Aruti Configuration -> Option -> Integration -> EFT Integration.")
			Language.setMessage(mstrModuleName, 18, "Please Set Data File Name From Aruti Configuration -> Option -> Integration -> EFT Integration.")
			Language.setMessage(mstrModuleName, 19, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 20, "Bank Payment Voucher Report")
			Language.setMessage(mstrModuleName, 21, "EFT Advance Salary CSV")
			Language.setMessage(mstrModuleName, 22, "EFT Advance Salary XSV")
			Language.setMessage(mstrModuleName, 23, "Please set Posting Date.")
			Language.setMessage(mstrModuleName, 24, "Id is mandatory information.")
			Language.setMessage(mstrModuleName, 25, "Pass Code is mandatory information.")
			Language.setMessage(mstrModuleName, 26, "Please select Period.")
			Language.setMessage(mstrModuleName, 27, "Salary Format WPS")
			Language.setMessage(mstrModuleName, 28, "Type to Search")
			Language.setMessage(mstrModuleName, 29, "Link is mandatory information.")
			Language.setMessage(mstrModuleName, 30, "Please Set Country Code From Aruti Configuration -> Option -> Integration -> EFT Integration.")
			Language.setMessage(mstrModuleName, 31, "Please select membership to generate report.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
