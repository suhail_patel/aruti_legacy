﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmATLoanAdvanceReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblAppliedTo = New System.Windows.Forms.Label
        Me.lblTo = New System.Windows.Forms.Label
        Me.dtpATDTo = New System.Windows.Forms.DateTimePicker
        Me.lblAppliedFrom = New System.Windows.Forms.Label
        Me.lblApprover = New System.Windows.Forms.Label
        Me.cboApprover = New System.Windows.Forms.ComboBox
        Me.lblAuditFromDate = New System.Windows.Forms.Label
        Me.dtpATDFrom = New System.Windows.Forms.DateTimePicker
        Me.dtpAppliedTo = New System.Windows.Forms.DateTimePicker
        Me.lblAuditUser = New System.Windows.Forms.Label
        Me.lblAuditType = New System.Windows.Forms.Label
        Me.dtpAppliedFrom = New System.Windows.Forms.DateTimePicker
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblScheme = New System.Windows.Forms.Label
        Me.cboAuditUser = New System.Windows.Forms.ComboBox
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.cboAuditType = New System.Windows.Forms.ComboBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.chkIncludeInactiveEmp = New System.Windows.Forms.CheckBox
        Me.gbSortBy.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 504)
        Me.NavPanel.Size = New System.Drawing.Size(742, 55)
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 293)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(449, 63)
        Me.gbSortBy.TabIndex = 21
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(418, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(82, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(100, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(312, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeInactiveEmp)
        Me.gbFilterCriteria.Controls.Add(Me.lblAppliedTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpATDTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblAppliedFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprover)
        Me.gbFilterCriteria.Controls.Add(Me.cboApprover)
        Me.gbFilterCriteria.Controls.Add(Me.lblAuditFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpATDFrom)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAppliedTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblAuditUser)
        Me.gbFilterCriteria.Controls.Add(Me.lblAuditType)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAppliedFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblScheme)
        Me.gbFilterCriteria.Controls.Add(Me.cboAuditUser)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.cboAuditType)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(449, 221)
        Me.gbFilterCriteria.TabIndex = 20
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAppliedTo
        '
        Me.lblAppliedTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppliedTo.Location = New System.Drawing.Point(221, 144)
        Me.lblAppliedTo.Name = "lblAppliedTo"
        Me.lblAppliedTo.Size = New System.Drawing.Size(68, 15)
        Me.lblAppliedTo.TabIndex = 86
        Me.lblAppliedTo.Text = "To"
        Me.lblAppliedTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(223, 36)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(66, 15)
        Me.lblTo.TabIndex = 82
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpATDTo
        '
        Me.dtpATDTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpATDTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpATDTo.Location = New System.Drawing.Point(295, 33)
        Me.dtpATDTo.Name = "dtpATDTo"
        Me.dtpATDTo.Size = New System.Drawing.Size(117, 21)
        Me.dtpATDTo.TabIndex = 81
        '
        'lblAppliedFrom
        '
        Me.lblAppliedFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppliedFrom.Location = New System.Drawing.Point(8, 144)
        Me.lblAppliedFrom.Name = "lblAppliedFrom"
        Me.lblAppliedFrom.Size = New System.Drawing.Size(86, 15)
        Me.lblAppliedFrom.TabIndex = 85
        Me.lblAppliedFrom.Text = "Effective From"
        Me.lblAppliedFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(8, 170)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(86, 15)
        Me.lblApprover.TabIndex = 76
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApprover
        '
        Me.cboApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprover.DropDownWidth = 180
        Me.cboApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprover.FormattingEnabled = True
        Me.cboApprover.Location = New System.Drawing.Point(100, 168)
        Me.cboApprover.Name = "cboApprover"
        Me.cboApprover.Size = New System.Drawing.Size(312, 21)
        Me.cboApprover.TabIndex = 71
        '
        'lblAuditFromDate
        '
        Me.lblAuditFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuditFromDate.Location = New System.Drawing.Point(8, 36)
        Me.lblAuditFromDate.Name = "lblAuditFromDate"
        Me.lblAuditFromDate.Size = New System.Drawing.Size(86, 15)
        Me.lblAuditFromDate.TabIndex = 80
        Me.lblAuditFromDate.Text = "Audit Date"
        Me.lblAuditFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpATDFrom
        '
        Me.dtpATDFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpATDFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpATDFrom.Location = New System.Drawing.Point(100, 33)
        Me.dtpATDFrom.Name = "dtpATDFrom"
        Me.dtpATDFrom.Size = New System.Drawing.Size(117, 21)
        Me.dtpATDFrom.TabIndex = 79
        '
        'dtpAppliedTo
        '
        Me.dtpAppliedTo.Checked = False
        Me.dtpAppliedTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAppliedTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAppliedTo.Location = New System.Drawing.Point(295, 141)
        Me.dtpAppliedTo.Name = "dtpAppliedTo"
        Me.dtpAppliedTo.ShowCheckBox = True
        Me.dtpAppliedTo.Size = New System.Drawing.Size(117, 21)
        Me.dtpAppliedTo.TabIndex = 84
        '
        'lblAuditUser
        '
        Me.lblAuditUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuditUser.Location = New System.Drawing.Point(221, 90)
        Me.lblAuditUser.Name = "lblAuditUser"
        Me.lblAuditUser.Size = New System.Drawing.Size(68, 15)
        Me.lblAuditUser.TabIndex = 78
        Me.lblAuditUser.Text = "Audit User"
        Me.lblAuditUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAuditType
        '
        Me.lblAuditType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuditType.Location = New System.Drawing.Point(221, 117)
        Me.lblAuditType.Name = "lblAuditType"
        Me.lblAuditType.Size = New System.Drawing.Size(68, 15)
        Me.lblAuditType.TabIndex = 77
        Me.lblAuditType.Text = "Audit Type"
        Me.lblAuditType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAppliedFrom
        '
        Me.dtpAppliedFrom.Checked = False
        Me.dtpAppliedFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAppliedFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAppliedFrom.Location = New System.Drawing.Point(100, 141)
        Me.dtpAppliedFrom.Name = "dtpAppliedFrom"
        Me.dtpAppliedFrom.ShowCheckBox = True
        Me.dtpAppliedFrom.Size = New System.Drawing.Size(117, 21)
        Me.dtpAppliedFrom.TabIndex = 83
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 117)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(86, 15)
        Me.lblStatus.TabIndex = 75
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblScheme
        '
        Me.lblScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScheme.Location = New System.Drawing.Point(8, 90)
        Me.lblScheme.Name = "lblScheme"
        Me.lblScheme.Size = New System.Drawing.Size(86, 15)
        Me.lblScheme.TabIndex = 74
        Me.lblScheme.Text = "Scheme"
        Me.lblScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAuditUser
        '
        Me.cboAuditUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAuditUser.DropDownWidth = 180
        Me.cboAuditUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAuditUser.FormattingEnabled = True
        Me.cboAuditUser.Location = New System.Drawing.Point(295, 87)
        Me.cboAuditUser.Name = "cboAuditUser"
        Me.cboAuditUser.Size = New System.Drawing.Size(117, 21)
        Me.cboAuditUser.TabIndex = 72
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanScheme.DropDownWidth = 180
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(100, 87)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(117, 21)
        Me.cboLoanScheme.TabIndex = 70
        '
        'cboAuditType
        '
        Me.cboAuditType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAuditType.DropDownWidth = 180
        Me.cboAuditType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAuditType.FormattingEnabled = True
        Me.cboAuditType.Location = New System.Drawing.Point(295, 114)
        Me.cboAuditType.Name = "cboAuditType"
        Me.cboAuditType.Size = New System.Drawing.Size(117, 21)
        Me.cboAuditType.TabIndex = 69
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 180
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(100, 114)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(117, 21)
        Me.cboStatus.TabIndex = 67
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(418, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 62
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(86, 15)
        Me.lblEmployee.TabIndex = 60
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(100, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(312, 21)
        Me.cboEmployee.TabIndex = 61
        '
        'chkIncludeInactiveEmp
        '
        Me.chkIncludeInactiveEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmp.Location = New System.Drawing.Point(100, 195)
        Me.chkIncludeInactiveEmp.Name = "chkIncludeInactiveEmp"
        Me.chkIncludeInactiveEmp.Size = New System.Drawing.Size(312, 17)
        Me.chkIncludeInactiveEmp.TabIndex = 88
        Me.chkIncludeInactiveEmp.Text = "Include inactive employee"
        Me.chkIncludeInactiveEmp.UseVisualStyleBackColor = True
        '
        'frmATLoanAdvanceReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(742, 559)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmATLoanAdvanceReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblAppliedTo As System.Windows.Forms.Label
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpATDTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAppliedFrom As System.Windows.Forms.Label
    Friend WithEvents lblAuditFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpATDFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpAppliedTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAuditUser As System.Windows.Forms.Label
    Friend WithEvents lblAuditType As System.Windows.Forms.Label
    Friend WithEvents dtpAppliedFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblScheme As System.Windows.Forms.Label
    Friend WithEvents cboAuditUser As System.Windows.Forms.ComboBox
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents cboAuditType As System.Windows.Forms.ComboBox
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents cboApprover As System.Windows.Forms.ComboBox
    Friend WithEvents chkIncludeInactiveEmp As System.Windows.Forms.CheckBox
End Class
