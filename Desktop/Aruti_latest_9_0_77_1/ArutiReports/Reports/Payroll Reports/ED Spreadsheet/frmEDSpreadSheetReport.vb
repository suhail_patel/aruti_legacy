#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region


Public Class frmEDSpreadSheetReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEDSpreadSheetReport"
    Private objEDSpreadsheet As clsEDSpreadSheetReport

    Private mstrEarningsHeads As String = String.Empty
    Private mstrDeductionHeads As String = String.Empty
    Private mstrNonEarningDeductionTranHeadIds As String = String.Empty 'Sohail (09 Mar 2012)
    Private mblnIsFromForm As Boolean = False

    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = "" 'Sohail (08 Mar 2013)
    Private mstrReport_GroupName As String = ""
    'Sohail (09 Mar 2012) -- End

    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty
    'S.SANDEEP [ 11 SEP 2012 ] -- END
    'Sohail (18 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrCurrency_Rate As String = String.Empty
    Private mstrBaseCurrSign As String = String.Empty
    Private mintFirstOpenPeriod As Integer = 0
    'Sohail (18 Jun 2013) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Properties "
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    'Sohail (08 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property
    'Sohail (08 Mar 2013) -- End

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
#End Region
    'Sohail (09 Mar 2012) -- End

#Region " Constructor "
    Public Sub New()
        objEDSpreadsheet = New clsEDSpreadSheetReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEDSpreadsheet.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objperiod As New clscommom_period_Tran
            Dim dsList As New DataSet
            'S.SANDEEP [ 08 June 2011 ] -- START
            'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
            Dim objUserdefReport As New clsUserDef_ReportMode
            'S.SANDEEP [ 08 June 2011 ] -- START

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            Dim objBranch As New clsStation
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objExRate As New clsExchangeRate
            'S.SANDEEP [ 11 SEP 2012 ] -- END


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, False)
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'Anjan [10 June 2015] -- End

            'Pinkal (24-Jun-2011) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            'Sohail (18 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            Dim objMaster As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            objMaster = Nothing
            'Sohail (18 Jun 2013) -- End
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                'Sohail (18 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                '.SelectedValue = 0
                .SelectedValue = mintFirstOpenPeriod
                'Sohail (18 Jun 2013) -- End
            End With
            objperiod = Nothing

            'S.SANDEEP [ 30 May 2011 ] -- START
            'ISSUE : FINCA REQ.
            dsList = objUserdefReport.GetModeReportList
            With cboMode
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
            End With
            'S.SANDEEP [ 30 May 2011 ] -- END 

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 17 AUG 2011 ] -- END 


            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objExRate.getComboList("ExRate", True)
            'Sohail (18 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                End If
            End If
            'Sohail (18 Jun 2013) -- End
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 11 SEP 2012 ] -- END

            dsList = Nothing

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Detail Report"))
            'cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Analysis By Department Report")) 'Sohail (09 Mar 2012)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            'Sohail (07 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            objEDSpreadsheet.setDefaultOrderBy(0)
            txtOrderBy.Text = objEDSpreadsheet.OrderByDisplay
            'Sohail (07 Aug 2013) -- End

            cboEmployee.SelectedValue = 0
            'Sohail (18 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            'cboPeriod.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            'Sohail (18 Jun 2013) -- End
            cboReportType.SelectedIndex = 0


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End


            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            cboBranch.SelectedValue = 0
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (09 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = "" 'Sohail (08 Mar 2013)
            mstrReport_GroupName = ""
            'Sohail (09 Mar 2012) -- End

            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes
            chkIgnorezeroHead.Checked = True
            'Pinkal (05-Mar-2012) -- End

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            chkShowLoan.Checked = True
            chkShowAdvance.Checked = True
            chkShowSavings.Checked = True
            'Sohail (17 Apr 2012) -- End


            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            lblExRate.Text = ""
            mdecEx_Rate = 1
            mstrCurr_Sign = String.Empty
            'S.SANDEEP [ 11 SEP 2012 ] -- END
            mstrCurrency_Rate = "" 'Sohail (18 Jun 2013)

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End

            chkShowPaymentDetails.Checked = False 'Sohail (21 Nov 2014)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmEDSpreadSheetReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEDSpreadsheet = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDSpreadSheetReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDSpreadSheetReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            chkIgnorezeroHead.Checked = True
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDSpreadSheetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDSpreadSheetReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEDSpreadSheetReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDSpreadSheetReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEDSpreadSheetReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is compulsory infomation. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            End If
            'S.SANDEEP [ 11 SEP 2012 ] -- END

            objEDSpreadsheet.SetDefaultValue()

            objEDSpreadsheet._EmployeeId = cboEmployee.SelectedValue
            objEDSpreadsheet._EmployeeName = cboEmployee.Text

            objEDSpreadsheet._PeriodId = cboPeriod.SelectedValue
            objEDSpreadsheet._PeriodName = cboPeriod.Text

            objEDSpreadsheet._ReportTypeId = cboReportType.SelectedIndex
            objEDSpreadsheet._ReportTypeName = cboReportType.Text

            If mblnIsFromForm Then
                objEDSpreadsheet._EarningHeads = mstrEarningsHeads
                objEDSpreadsheet._DeductionHeads = mstrDeductionHeads
                objEDSpreadsheet._NonEarningDeductionHeads = mstrNonEarningDeductionTranHeadIds 'Sohail (09 Mar 2012)
            Else
                Dim objUserDefReport As New clsUserDef_ReportMode

                objUserDefReport._Reportmodeid = cboMode.SelectedValue
                objUserDefReport._Reporttypeid = cboReportType.SelectedIndex
                objUserDefReport._Reportunkid = enArutiReport.EDSpreadSheet

                objEDSpreadsheet._EarningHeads = objUserDefReport._EarningTranHeadIds
                objEDSpreadsheet._DeductionHeads = objUserDefReport._DeductionTranHeadIds
                objEDSpreadsheet._NonEarningDeductionHeads = objUserDefReport._NonEarningDeduction_HeadIds 'Sohail (09 Mar 2012)

                objUserDefReport = Nothing
            End If


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objEDSpreadsheet._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End


            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            objEDSpreadsheet._BranchId = cboBranch.SelectedValue
            objEDSpreadsheet._Branch_Name = cboBranch.Text
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (09 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            objEDSpreadsheet._ViewByIds = mstrViewByIds
            objEDSpreadsheet._ViewIndex = mintViewIndex
            objEDSpreadsheet._ViewByName = mstrViewByName
            objEDSpreadsheet._Analysis_Fields = mstrAnalysis_Fields
            objEDSpreadsheet._Analysis_Join = mstrAnalysis_Join
            objEDSpreadsheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEDSpreadsheet._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName 'Sohail (08 Mar 2013)
            objEDSpreadsheet._Report_GroupName = mstrReport_GroupName
            'Sohail (09 Mar 2012) -- End

            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes
            objEDSpreadsheet._IgnoreZeroHeads = chkIgnorezeroHead.Checked
            'Pinkal (05-Mar-2012) -- End

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            objEDSpreadsheet._ShowLoan = chkShowLoan.Checked
            objEDSpreadsheet._ShowAdvance = chkShowAdvance.Checked
            objEDSpreadsheet._ShowSavings = chkShowSavings.Checked
            'Sohail (21 Nov 2014) -- Start
            'FDRC Enhancement - Show Payment Detail on ED Spreadsheet Report.
            objEDSpreadsheet._ShowPaymentDetails = chkShowPaymentDetails.Checked
            objEDSpreadsheet._UserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'Sohail (21 Nov 2014) -- End

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objEDSpreadsheet._PeriodStartDate = objPeriod._Start_Date
            objEDSpreadsheet._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (21 Aug 2015)
            'Sohail (17 Apr 2012) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEDSpreadsheet._Ex_Rate = mdecEx_Rate
            objEDSpreadsheet._Currency_Sign = mstrCurr_Sign
            'S.SANDEEP [ 11 SEP 2012 ] -- END
            objEDSpreadsheet._Currency_Rate = mstrCurrency_Rate 'Sohail (18 Jun 2013)

            'Sohail (19 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            objEDSpreadsheet._ModeId = CInt(cboMode.SelectedValue)
            'Sohail (19 Oct 2012) -- End


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objEDSpreadsheet._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer..
            objEDSpreadsheet._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            Select Case cboReportType.SelectedIndex
                Case 0
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objEDSpreadsheet.Generate_EDSpreadsheetReport_New()
                    'Sohail (22 Sep 2017) -- Start
                    'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                    'objEDSpreadsheet.Generate_EDSpreadsheetReport_New(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
                    objEDSpreadsheet.Generate_EDSpreadsheetReport_New(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, ConfigParameter._Object._RoundOff_Type)
                    'Sohail (22 Sep 2017) -- End
                    'Sohail (21 Aug 2015) -- End
                    'Case 1
                    '    objEDSpreadsheet.Generate_ByAnalysisEDSpreadsheetReport()
            End Select

            objPeriod = Nothing 'Sohail (21 Aug 2015)

            'If cboReportType.SelectedIndex = 0 Then
            '    objEDSpreadsheet.Generate_EDSpreadsheetReport()
            'Else
            '    objEDSpreadsheet.Generate_ByAnalysisEDSpreadsheetReport()
            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

   
    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEDSpreadSheetReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEDSpreadSheetReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End


    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (27-Feb-2013) -- End

   
#End Region

#Region "Control"
    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            eZeeHeader.Title = objEDSpreadsheet._ReportName & " " & cboReportType.Text
            eZeeHeader.Message = objEDSpreadsheet._ReportDesc
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Vimal (27 Nov 2010) -- Start 
    Private Sub objbtnSearchEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        'Dim dsList As DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployee.GetEmployeeList("EmployeeList")
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objfrm.DataSource = dsList.Tables("EmployeeList")
                objfrm.DataSource = cboEmployee.DataSource
                'S.SANDEEP [04 JUN 2015] -- END
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub
    'Vimal (27 Nov 2010) -- End

    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrViewByIds = frm._ReportBy_Ids
            mstrViewByName = frm._ReportBy_Name
            mintViewIndex = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (09 Mar 2012) -- End

    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                            , cboPeriod.SelectedIndexChanged
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate
                    Dim dsList As DataSet
                    Dim objPeriod As New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    'Sohail (21 Aug 2015) -- End
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = dsList.Tables("ExRate").Rows(0)("currency_sign")
                        lblExRate.Text = Language.getMessage(mstrModuleName, 3, "Exchange Rate :") & " " & CDbl(mdecEx_Rate) & " "
                        mstrCurrency_Rate = Format(CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1")), GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2")).ToString & " " & mstrCurr_Sign & " " 'Sohail (18 Jun 2013)
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                    mstrCurrency_Rate = "" 'Sohail (18 Jun 2013)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub
    'S.SANDEEP [ 11 SEP 2012 ] -- END

    'Sohail (07 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEDSpreadsheet.setOrderBy(0)
            txtOrderBy.Text = objEDSpreadsheet.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (07 Aug 2013) -- End
#End Region
   
    'Messages
    '1, "Detail Report"
    '2, "Analysis By Department Report"
    '3, "Period is compulsory infomation. Please select period to continue."
   
    Private Sub lnkIncludeHeads_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkIncludeHeads.LinkClicked
        Dim frm As New frmUserDef_ReportMode
        Try
            frm.displayDialog(enArutiReport.EDSpreadSheet, cboMode.SelectedValue, cboReportType.SelectedIndex)

            mstrEarningsHeads = frm._Earning_HeadIds
            mstrDeductionHeads = frm._Deduction_HeadIds
            mstrNonEarningDeductionTranHeadIds = frm._NonEarningDeduction_HeadIds 'Sohail (09 Mar 2012)

            mblnIsFromForm = True

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "lnkIncludeHeads_LinkClicked", mstrModuleName)
        Finally
            frm.Dispose()
        End Try
    End Sub
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lnkIncludeHeads.Text = Language._Object.getCaption(Me.lnkIncludeHeads.Name, Me.lnkIncludeHeads.Text)
			Me.lblSelectMode.Text = Language._Object.getCaption(Me.lblSelectMode.Name, Me.lblSelectMode.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.chkIgnorezeroHead.Text = Language._Object.getCaption(Me.chkIgnorezeroHead.Name, Me.chkIgnorezeroHead.Text)
			Me.chkShowSavings.Text = Language._Object.getCaption(Me.chkShowSavings.Name, Me.chkShowSavings.Text)
			Me.chkShowAdvance.Text = Language._Object.getCaption(Me.chkShowAdvance.Name, Me.chkShowAdvance.Text)
			Me.chkShowLoan.Text = Language._Object.getCaption(Me.chkShowLoan.Name, Me.chkShowLoan.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.lblExRate.Text = Language._Object.getCaption(Me.lblExRate.Name, Me.lblExRate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.chkShowPaymentDetails.Text = Language._Object.getCaption(Me.chkShowPaymentDetails.Name, Me.chkShowPaymentDetails.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Detail Report")
                        Language.setMessage(mstrModuleName, 2, "Period is compulsory infomation. Please select period to continue.")
			Language.setMessage(mstrModuleName, 3, "Exchange Rate :")
			Language.setMessage(mstrModuleName, 4, "Sorry, No exchange rate is defined for this currency for the period selected.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

   
End Class
