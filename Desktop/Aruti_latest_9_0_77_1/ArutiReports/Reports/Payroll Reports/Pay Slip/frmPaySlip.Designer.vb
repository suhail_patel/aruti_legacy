﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPaySlip
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPaySlip))
        Me.gbFilterCriteriaMembership = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowLoanReceived = New System.Windows.Forms.CheckBox
        Me.chkShowBankAccountNo = New System.Windows.Forms.CheckBox
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.cboOT4Amount = New System.Windows.Forms.ComboBox
        Me.lblOT4Amount = New System.Windows.Forms.Label
        Me.cboOT3Amount = New System.Windows.Forms.ComboBox
        Me.lblOT3Amount = New System.Windows.Forms.Label
        Me.cboOT2Amount = New System.Windows.Forms.ComboBox
        Me.lblOT2Amount = New System.Windows.Forms.Label
        Me.cboOT1Amount = New System.Windows.Forms.ComboBox
        Me.lblOT1Amount = New System.Windows.Forms.Label
        Me.cboOT4Hours = New System.Windows.Forms.ComboBox
        Me.lblOT4Hours = New System.Windows.Forms.Label
        Me.cboOT3Hours = New System.Windows.Forms.ComboBox
        Me.lblOT3Hours = New System.Windows.Forms.Label
        Me.cboOT2Hours = New System.Windows.Forms.ComboBox
        Me.lblOT2Hours = New System.Windows.Forms.Label
        Me.cboOT1Hours = New System.Windows.Forms.ComboBox
        Me.lblOT1Hours = New System.Windows.Forms.Label
        Me.chkDependents = New System.Windows.Forms.CheckBox
        Me.chkDOB = New System.Windows.Forms.CheckBox
        Me.chkBasicSalary = New System.Windows.Forms.CheckBox
        Me.chkAge = New System.Windows.Forms.CheckBox
        Me.chkShowTwoPayslipPerPage = New System.Windows.Forms.CheckBox
        Me.chkShowInformationalHeads = New System.Windows.Forms.CheckBox
        Me.chkShowCategory = New System.Windows.Forms.CheckBox
        Me.chkShowEachPayslipOnNewPage = New System.Windows.Forms.CheckBox
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.chkShowCumulativeAccrual = New System.Windows.Forms.CheckBox
        Me.chkShowEmployerContrib = New System.Windows.Forms.CheckBox
        Me.chkShowAllHeads = New System.Windows.Forms.CheckBox
        Me.radLogo = New System.Windows.Forms.RadioButton
        Me.radCompanyDetails = New System.Windows.Forms.RadioButton
        Me.radLogoCompanyInfo = New System.Windows.Forms.RadioButton
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.chkShowSavingBalance = New System.Windows.Forms.CheckBox
        Me.chkShowLoanBalance = New System.Windows.Forms.CheckBox
        Me.chkIgnoreZeroHead = New System.Windows.Forms.CheckBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteriaMessage = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.rabPayPoint = New System.Windows.Forms.RadioButton
        Me.rabCostCenter = New System.Windows.Forms.RadioButton
        Me.rabClass = New System.Windows.Forms.RadioButton
        Me.rabGrade = New System.Windows.Forms.RadioButton
        Me.rabJob = New System.Windows.Forms.RadioButton
        Me.rabEmployee = New System.Windows.Forms.RadioButton
        Me.rabDept = New System.Windows.Forms.RadioButton
        Me.rabSection = New System.Windows.Forms.RadioButton
        Me.gbFilterCriteriaEmpPeriod = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.EZeeSearchResetbtn = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lnkAdvanceFilter = New System.Windows.Forms.LinkLabel
        Me.objlblEmpCount = New System.Windows.Forms.Label
        Me.lblSearchPeriod = New System.Windows.Forms.Label
        Me.lblSearchEmp = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkPeriodSelectAll = New System.Windows.Forms.CheckBox
        Me.dgPeriod = New System.Windows.Forms.DataGridView
        Me.objdgperiodcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhPeriodunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhPeriodCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPeriodStart = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPeriodEnd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchPeriod = New System.Windows.Forms.TextBox
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbEmpSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radDescending = New System.Windows.Forms.RadioButton
        Me.radAscending = New System.Windows.Forms.RadioButton
        Me.cboEmpSortBy = New System.Windows.Forms.ComboBox
        Me.lblEmpSortBy = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.chkShowRemainingNoofLoanInstallments = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteriaMembership.SuspendLayout()
        Me.gbFilterCriteriaMessage.SuspendLayout()
        Me.gbFilterCriteriaEmpPeriod.SuspendLayout()
        CType(Me.EZeeSearchResetbtn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dgPeriod, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbSortBy.SuspendLayout()
        Me.gbEmpSortBy.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 537)
        Me.NavPanel.Size = New System.Drawing.Size(930, 55)
        Me.NavPanel.TabIndex = 4
        '
        'gbFilterCriteriaMembership
        '
        Me.gbFilterCriteriaMembership.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteriaMembership.Checked = False
        Me.gbFilterCriteriaMembership.CollapseAllExceptThis = False
        Me.gbFilterCriteriaMembership.CollapsedHoverImage = Nothing
        Me.gbFilterCriteriaMembership.CollapsedNormalImage = Nothing
        Me.gbFilterCriteriaMembership.CollapsedPressedImage = Nothing
        Me.gbFilterCriteriaMembership.CollapseOnLoad = False
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowRemainingNoofLoanInstallments)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowLoanReceived)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowBankAccountNo)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.cboCurrency)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.lblCurrency)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.cboOT4Amount)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.lblOT4Amount)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.cboOT3Amount)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.lblOT3Amount)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.cboOT2Amount)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.lblOT2Amount)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.cboOT1Amount)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.lblOT1Amount)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.cboOT4Hours)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.lblOT4Hours)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.cboOT3Hours)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.lblOT3Hours)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.cboOT2Hours)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.lblOT2Hours)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.cboOT1Hours)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.lblOT1Hours)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkDependents)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkDOB)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkBasicSalary)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkAge)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowTwoPayslipPerPage)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowInformationalHeads)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowCategory)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowEachPayslipOnNewPage)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.objelLine1)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowCumulativeAccrual)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowEmployerContrib)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowAllHeads)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.radLogo)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.radCompanyDetails)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.radLogoCompanyInfo)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowSavingBalance)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkShowLoanBalance)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.chkIgnoreZeroHead)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.lblLeaveType)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.cboLeaveType)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.lblMembership)
        Me.gbFilterCriteriaMembership.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteriaMembership.ExpandedHoverImage = Nothing
        Me.gbFilterCriteriaMembership.ExpandedNormalImage = Nothing
        Me.gbFilterCriteriaMembership.ExpandedPressedImage = Nothing
        Me.gbFilterCriteriaMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteriaMembership.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteriaMembership.HeaderHeight = 25
        Me.gbFilterCriteriaMembership.HeaderMessage = ""
        Me.gbFilterCriteriaMembership.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteriaMembership.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteriaMembership.HeightOnCollapse = 0
        Me.gbFilterCriteriaMembership.LeftTextSpace = 0
        Me.gbFilterCriteriaMembership.Location = New System.Drawing.Point(9, 6)
        Me.gbFilterCriteriaMembership.Name = "gbFilterCriteriaMembership"
        Me.gbFilterCriteriaMembership.OpenHeight = 300
        Me.gbFilterCriteriaMembership.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteriaMembership.ShowBorder = True
        Me.gbFilterCriteriaMembership.ShowCheckBox = False
        Me.gbFilterCriteriaMembership.ShowCollapseButton = False
        Me.gbFilterCriteriaMembership.ShowDefaultBorderColor = True
        Me.gbFilterCriteriaMembership.ShowDownButton = False
        Me.gbFilterCriteriaMembership.ShowHeader = True
        Me.gbFilterCriteriaMembership.Size = New System.Drawing.Size(499, 385)
        Me.gbFilterCriteriaMembership.TabIndex = 0
        Me.gbFilterCriteriaMembership.Temp = 0
        Me.gbFilterCriteriaMembership.Text = "Filter Criteria"
        Me.gbFilterCriteriaMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowLoanReceived
        '
        Me.chkShowLoanReceived.Checked = True
        Me.chkShowLoanReceived.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowLoanReceived.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLoanReceived.Location = New System.Drawing.Point(255, 277)
        Me.chkShowLoanReceived.Name = "chkShowLoanReceived"
        Me.chkShowLoanReceived.Size = New System.Drawing.Size(179, 18)
        Me.chkShowLoanReceived.TabIndex = 230
        Me.chkShowLoanReceived.Text = "Show Loan Received"
        Me.chkShowLoanReceived.UseVisualStyleBackColor = True
        '
        'chkShowBankAccountNo
        '
        Me.chkShowBankAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowBankAccountNo.Location = New System.Drawing.Point(11, 277)
        Me.chkShowBankAccountNo.Name = "chkShowBankAccountNo"
        Me.chkShowBankAccountNo.Size = New System.Drawing.Size(168, 18)
        Me.chkShowBankAccountNo.TabIndex = 228
        Me.chkShowBankAccountNo.Text = "Show Bank Account No."
        Me.chkShowBankAccountNo.UseVisualStyleBackColor = True
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 230
        Me.cboCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(93, 300)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(126, 21)
        Me.cboCurrency.TabIndex = 225
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(11, 304)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(76, 13)
        Me.lblCurrency.TabIndex = 226
        Me.lblCurrency.Text = "Currency"
        '
        'cboOT4Amount
        '
        Me.cboOT4Amount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT4Amount.DropDownWidth = 200
        Me.cboOT4Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT4Amount.FormattingEnabled = True
        Me.cboOT4Amount.Location = New System.Drawing.Point(462, 360)
        Me.cboOT4Amount.Name = "cboOT4Amount"
        Me.cboOT4Amount.Size = New System.Drawing.Size(24, 21)
        Me.cboOT4Amount.TabIndex = 136
        Me.cboOT4Amount.Visible = False
        '
        'lblOT4Amount
        '
        Me.lblOT4Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT4Amount.Location = New System.Drawing.Point(377, 362)
        Me.lblOT4Amount.Name = "lblOT4Amount"
        Me.lblOT4Amount.Size = New System.Drawing.Size(79, 13)
        Me.lblOT4Amount.TabIndex = 144
        Me.lblOT4Amount.Text = "OT 4 Amount"
        Me.lblOT4Amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblOT4Amount.Visible = False
        '
        'cboOT3Amount
        '
        Me.cboOT3Amount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT3Amount.DropDownWidth = 200
        Me.cboOT3Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT3Amount.FormattingEnabled = True
        Me.cboOT3Amount.Location = New System.Drawing.Point(462, 333)
        Me.cboOT3Amount.Name = "cboOT3Amount"
        Me.cboOT3Amount.Size = New System.Drawing.Size(24, 21)
        Me.cboOT3Amount.TabIndex = 135
        Me.cboOT3Amount.Visible = False
        '
        'lblOT3Amount
        '
        Me.lblOT3Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT3Amount.Location = New System.Drawing.Point(377, 335)
        Me.lblOT3Amount.Name = "lblOT3Amount"
        Me.lblOT3Amount.Size = New System.Drawing.Size(79, 13)
        Me.lblOT3Amount.TabIndex = 143
        Me.lblOT3Amount.Text = "OT 3 Amount"
        Me.lblOT3Amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblOT3Amount.Visible = False
        '
        'cboOT2Amount
        '
        Me.cboOT2Amount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT2Amount.DropDownWidth = 200
        Me.cboOT2Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT2Amount.FormattingEnabled = True
        Me.cboOT2Amount.Location = New System.Drawing.Point(462, 311)
        Me.cboOT2Amount.Name = "cboOT2Amount"
        Me.cboOT2Amount.Size = New System.Drawing.Size(24, 21)
        Me.cboOT2Amount.TabIndex = 134
        Me.cboOT2Amount.Visible = False
        '
        'lblOT2Amount
        '
        Me.lblOT2Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT2Amount.Location = New System.Drawing.Point(377, 313)
        Me.lblOT2Amount.Name = "lblOT2Amount"
        Me.lblOT2Amount.Size = New System.Drawing.Size(79, 13)
        Me.lblOT2Amount.TabIndex = 142
        Me.lblOT2Amount.Text = "OT 2 Amount"
        Me.lblOT2Amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblOT2Amount.Visible = False
        '
        'cboOT1Amount
        '
        Me.cboOT1Amount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT1Amount.DropDownWidth = 200
        Me.cboOT1Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT1Amount.FormattingEnabled = True
        Me.cboOT1Amount.Location = New System.Drawing.Point(462, 296)
        Me.cboOT1Amount.Name = "cboOT1Amount"
        Me.cboOT1Amount.Size = New System.Drawing.Size(24, 21)
        Me.cboOT1Amount.TabIndex = 133
        Me.cboOT1Amount.Visible = False
        '
        'lblOT1Amount
        '
        Me.lblOT1Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT1Amount.Location = New System.Drawing.Point(377, 298)
        Me.lblOT1Amount.Name = "lblOT1Amount"
        Me.lblOT1Amount.Size = New System.Drawing.Size(79, 13)
        Me.lblOT1Amount.TabIndex = 141
        Me.lblOT1Amount.Text = "OT 1 Amount"
        Me.lblOT1Amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblOT1Amount.Visible = False
        '
        'cboOT4Hours
        '
        Me.cboOT4Hours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT4Hours.DropDownWidth = 200
        Me.cboOT4Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT4Hours.FormattingEnabled = True
        Me.cboOT4Hours.Location = New System.Drawing.Point(337, 362)
        Me.cboOT4Hours.Name = "cboOT4Hours"
        Me.cboOT4Hours.Size = New System.Drawing.Size(31, 21)
        Me.cboOT4Hours.TabIndex = 132
        Me.cboOT4Hours.Visible = False
        '
        'lblOT4Hours
        '
        Me.lblOT4Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT4Hours.Location = New System.Drawing.Point(252, 364)
        Me.lblOT4Hours.Name = "lblOT4Hours"
        Me.lblOT4Hours.Size = New System.Drawing.Size(79, 13)
        Me.lblOT4Hours.TabIndex = 140
        Me.lblOT4Hours.Text = "OT 4 Hours"
        Me.lblOT4Hours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblOT4Hours.Visible = False
        '
        'cboOT3Hours
        '
        Me.cboOT3Hours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT3Hours.DropDownWidth = 200
        Me.cboOT3Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT3Hours.FormattingEnabled = True
        Me.cboOT3Hours.Location = New System.Drawing.Point(337, 335)
        Me.cboOT3Hours.Name = "cboOT3Hours"
        Me.cboOT3Hours.Size = New System.Drawing.Size(31, 21)
        Me.cboOT3Hours.TabIndex = 131
        Me.cboOT3Hours.Visible = False
        '
        'lblOT3Hours
        '
        Me.lblOT3Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT3Hours.Location = New System.Drawing.Point(252, 337)
        Me.lblOT3Hours.Name = "lblOT3Hours"
        Me.lblOT3Hours.Size = New System.Drawing.Size(79, 13)
        Me.lblOT3Hours.TabIndex = 139
        Me.lblOT3Hours.Text = "OT 3 Hours"
        Me.lblOT3Hours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblOT3Hours.Visible = False
        '
        'cboOT2Hours
        '
        Me.cboOT2Hours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT2Hours.DropDownWidth = 200
        Me.cboOT2Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT2Hours.FormattingEnabled = True
        Me.cboOT2Hours.Location = New System.Drawing.Point(337, 313)
        Me.cboOT2Hours.Name = "cboOT2Hours"
        Me.cboOT2Hours.Size = New System.Drawing.Size(31, 21)
        Me.cboOT2Hours.TabIndex = 130
        Me.cboOT2Hours.Visible = False
        '
        'lblOT2Hours
        '
        Me.lblOT2Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT2Hours.Location = New System.Drawing.Point(252, 315)
        Me.lblOT2Hours.Name = "lblOT2Hours"
        Me.lblOT2Hours.Size = New System.Drawing.Size(79, 13)
        Me.lblOT2Hours.TabIndex = 138
        Me.lblOT2Hours.Text = "OT 2 Hours"
        Me.lblOT2Hours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblOT2Hours.Visible = False
        '
        'cboOT1Hours
        '
        Me.cboOT1Hours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT1Hours.DropDownWidth = 200
        Me.cboOT1Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT1Hours.FormattingEnabled = True
        Me.cboOT1Hours.Location = New System.Drawing.Point(337, 298)
        Me.cboOT1Hours.Name = "cboOT1Hours"
        Me.cboOT1Hours.Size = New System.Drawing.Size(31, 21)
        Me.cboOT1Hours.TabIndex = 129
        Me.cboOT1Hours.Visible = False
        '
        'lblOT1Hours
        '
        Me.lblOT1Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT1Hours.Location = New System.Drawing.Point(252, 300)
        Me.lblOT1Hours.Name = "lblOT1Hours"
        Me.lblOT1Hours.Size = New System.Drawing.Size(79, 13)
        Me.lblOT1Hours.TabIndex = 137
        Me.lblOT1Hours.Text = "OT 1 Hours"
        Me.lblOT1Hours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblOT1Hours.Visible = False
        '
        'chkDependents
        '
        Me.chkDependents.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDependents.Location = New System.Drawing.Point(255, 223)
        Me.chkDependents.Name = "chkDependents"
        Me.chkDependents.Size = New System.Drawing.Size(168, 18)
        Me.chkDependents.TabIndex = 120
        Me.chkDependents.Text = "Show Dependants"
        Me.chkDependents.UseVisualStyleBackColor = True
        '
        'chkDOB
        '
        Me.chkDOB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDOB.Location = New System.Drawing.Point(11, 199)
        Me.chkDOB.Name = "chkDOB"
        Me.chkDOB.Size = New System.Drawing.Size(168, 18)
        Me.chkDOB.TabIndex = 119
        Me.chkDOB.Text = "Show Birthdate"
        Me.chkDOB.UseVisualStyleBackColor = True
        '
        'chkBasicSalary
        '
        Me.chkBasicSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBasicSalary.Location = New System.Drawing.Point(11, 223)
        Me.chkBasicSalary.Name = "chkBasicSalary"
        Me.chkBasicSalary.Size = New System.Drawing.Size(168, 18)
        Me.chkBasicSalary.TabIndex = 118
        Me.chkBasicSalary.Text = "Show Monthly Salary"
        Me.chkBasicSalary.UseVisualStyleBackColor = True
        '
        'chkAge
        '
        Me.chkAge.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAge.Location = New System.Drawing.Point(255, 199)
        Me.chkAge.Name = "chkAge"
        Me.chkAge.Size = New System.Drawing.Size(168, 18)
        Me.chkAge.TabIndex = 117
        Me.chkAge.Text = "Show Age"
        Me.chkAge.UseVisualStyleBackColor = True
        '
        'chkShowTwoPayslipPerPage
        '
        Me.chkShowTwoPayslipPerPage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowTwoPayslipPerPage.Location = New System.Drawing.Point(255, 253)
        Me.chkShowTwoPayslipPerPage.Name = "chkShowTwoPayslipPerPage"
        Me.chkShowTwoPayslipPerPage.Size = New System.Drawing.Size(179, 18)
        Me.chkShowTwoPayslipPerPage.TabIndex = 115
        Me.chkShowTwoPayslipPerPage.Text = "Show Two Payslips Per Page"
        Me.chkShowTwoPayslipPerPage.UseVisualStyleBackColor = True
        '
        'chkShowInformationalHeads
        '
        Me.chkShowInformationalHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowInformationalHeads.Location = New System.Drawing.Point(11, 175)
        Me.chkShowInformationalHeads.Name = "chkShowInformationalHeads"
        Me.chkShowInformationalHeads.Size = New System.Drawing.Size(168, 18)
        Me.chkShowInformationalHeads.TabIndex = 114
        Me.chkShowInformationalHeads.Text = "Show Informational Heads"
        Me.chkShowInformationalHeads.UseVisualStyleBackColor = True
        '
        'chkShowCategory
        '
        Me.chkShowCategory.Checked = True
        Me.chkShowCategory.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowCategory.Location = New System.Drawing.Point(255, 175)
        Me.chkShowCategory.Name = "chkShowCategory"
        Me.chkShowCategory.Size = New System.Drawing.Size(156, 18)
        Me.chkShowCategory.TabIndex = 112
        Me.chkShowCategory.Text = "Show Category"
        Me.chkShowCategory.UseVisualStyleBackColor = True
        '
        'chkShowEachPayslipOnNewPage
        '
        Me.chkShowEachPayslipOnNewPage.Checked = True
        Me.chkShowEachPayslipOnNewPage.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowEachPayslipOnNewPage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEachPayslipOnNewPage.Location = New System.Drawing.Point(11, 253)
        Me.chkShowEachPayslipOnNewPage.Name = "chkShowEachPayslipOnNewPage"
        Me.chkShowEachPayslipOnNewPage.Size = New System.Drawing.Size(184, 18)
        Me.chkShowEachPayslipOnNewPage.TabIndex = 110
        Me.chkShowEachPayslipOnNewPage.Text = "Show Each Payslip On New Page"
        Me.chkShowEachPayslipOnNewPage.UseVisualStyleBackColor = True
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(17, 240)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(437, 13)
        Me.objelLine1.TabIndex = 109
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowCumulativeAccrual
        '
        Me.chkShowCumulativeAccrual.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowCumulativeAccrual.Location = New System.Drawing.Point(11, 151)
        Me.chkShowCumulativeAccrual.Name = "chkShowCumulativeAccrual"
        Me.chkShowCumulativeAccrual.Size = New System.Drawing.Size(168, 18)
        Me.chkShowCumulativeAccrual.TabIndex = 33
        Me.chkShowCumulativeAccrual.Text = "Show Cumulative/Accrual "
        Me.chkShowCumulativeAccrual.UseVisualStyleBackColor = True
        '
        'chkShowEmployerContrib
        '
        Me.chkShowEmployerContrib.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmployerContrib.Location = New System.Drawing.Point(11, 127)
        Me.chkShowEmployerContrib.Name = "chkShowEmployerContrib"
        Me.chkShowEmployerContrib.Size = New System.Drawing.Size(168, 18)
        Me.chkShowEmployerContrib.TabIndex = 11
        Me.chkShowEmployerContrib.Text = "Show Employer Contribution"
        Me.chkShowEmployerContrib.UseVisualStyleBackColor = True
        '
        'chkShowAllHeads
        '
        Me.chkShowAllHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowAllHeads.Location = New System.Drawing.Point(255, 58)
        Me.chkShowAllHeads.Name = "chkShowAllHeads"
        Me.chkShowAllHeads.Size = New System.Drawing.Size(156, 18)
        Me.chkShowAllHeads.TabIndex = 6
        Me.chkShowAllHeads.Text = "Show All Heads"
        Me.chkShowAllHeads.UseVisualStyleBackColor = True
        '
        'radLogo
        '
        Me.radLogo.AutoSize = True
        Me.radLogo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLogo.Location = New System.Drawing.Point(255, 152)
        Me.radLogo.Name = "radLogo"
        Me.radLogo.Size = New System.Drawing.Size(48, 17)
        Me.radLogo.TabIndex = 9
        Me.radLogo.TabStop = True
        Me.radLogo.Text = "Logo"
        Me.radLogo.UseVisualStyleBackColor = True
        '
        'radCompanyDetails
        '
        Me.radCompanyDetails.AutoSize = True
        Me.radCompanyDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCompanyDetails.Location = New System.Drawing.Point(255, 128)
        Me.radCompanyDetails.Name = "radCompanyDetails"
        Me.radCompanyDetails.Size = New System.Drawing.Size(105, 17)
        Me.radCompanyDetails.TabIndex = 8
        Me.radCompanyDetails.TabStop = True
        Me.radCompanyDetails.Text = "Company Details"
        Me.radCompanyDetails.UseVisualStyleBackColor = True
        '
        'radLogoCompanyInfo
        '
        Me.radLogoCompanyInfo.AutoSize = True
        Me.radLogoCompanyInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLogoCompanyInfo.Location = New System.Drawing.Point(255, 104)
        Me.radLogoCompanyInfo.Name = "radLogoCompanyInfo"
        Me.radLogoCompanyInfo.Size = New System.Drawing.Size(152, 17)
        Me.radLogoCompanyInfo.TabIndex = 7
        Me.radLogoCompanyInfo.TabStop = True
        Me.radLogoCompanyInfo.Text = "Logo and Company Details"
        Me.radLogoCompanyInfo.UseVisualStyleBackColor = True
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(255, 80)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(156, 18)
        Me.chkInactiveemp.TabIndex = 5
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'chkShowSavingBalance
        '
        Me.chkShowSavingBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowSavingBalance.Location = New System.Drawing.Point(11, 104)
        Me.chkShowSavingBalance.Name = "chkShowSavingBalance"
        Me.chkShowSavingBalance.Size = New System.Drawing.Size(156, 17)
        Me.chkShowSavingBalance.TabIndex = 4
        Me.chkShowSavingBalance.Text = "Show Saving Balance"
        Me.chkShowSavingBalance.UseVisualStyleBackColor = True
        '
        'chkShowLoanBalance
        '
        Me.chkShowLoanBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLoanBalance.Location = New System.Drawing.Point(11, 81)
        Me.chkShowLoanBalance.Name = "chkShowLoanBalance"
        Me.chkShowLoanBalance.Size = New System.Drawing.Size(156, 17)
        Me.chkShowLoanBalance.TabIndex = 3
        Me.chkShowLoanBalance.Text = "Show Loan Balance"
        Me.chkShowLoanBalance.UseVisualStyleBackColor = True
        '
        'chkIgnoreZeroHead
        '
        Me.chkIgnoreZeroHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnoreZeroHead.Location = New System.Drawing.Point(11, 58)
        Me.chkIgnoreZeroHead.Name = "chkIgnoreZeroHead"
        Me.chkIgnoreZeroHead.Size = New System.Drawing.Size(156, 17)
        Me.chkIgnoreZeroHead.TabIndex = 2
        Me.chkIgnoreZeroHead.Text = "Ignore Zero Value Heads"
        Me.chkIgnoreZeroHead.UseVisualStyleBackColor = True
        '
        'lblLeaveType
        '
        Me.lblLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(252, 35)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(79, 13)
        Me.lblLeaveType.TabIndex = 3
        Me.lblLeaveType.Text = "Leave Type"
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.DropDownWidth = 200
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(337, 31)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(126, 21)
        Me.cboLeaveType.TabIndex = 1
        '
        'lblMembership
        '
        Me.lblMembership.BackColor = System.Drawing.Color.Transparent
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(8, 36)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(79, 13)
        Me.lblMembership.TabIndex = 0
        Me.lblMembership.Text = "Membership"
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 200
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(93, 31)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(126, 21)
        Me.cboMembership.TabIndex = 0
        '
        'gbFilterCriteriaMessage
        '
        Me.gbFilterCriteriaMessage.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteriaMessage.Checked = False
        Me.gbFilterCriteriaMessage.CollapseAllExceptThis = False
        Me.gbFilterCriteriaMessage.CollapsedHoverImage = Nothing
        Me.gbFilterCriteriaMessage.CollapsedNormalImage = Nothing
        Me.gbFilterCriteriaMessage.CollapsedPressedImage = Nothing
        Me.gbFilterCriteriaMessage.CollapseOnLoad = False
        Me.gbFilterCriteriaMessage.Controls.Add(Me.rabPayPoint)
        Me.gbFilterCriteriaMessage.Controls.Add(Me.rabCostCenter)
        Me.gbFilterCriteriaMessage.Controls.Add(Me.rabClass)
        Me.gbFilterCriteriaMessage.Controls.Add(Me.rabGrade)
        Me.gbFilterCriteriaMessage.Controls.Add(Me.rabJob)
        Me.gbFilterCriteriaMessage.Controls.Add(Me.rabEmployee)
        Me.gbFilterCriteriaMessage.Controls.Add(Me.rabDept)
        Me.gbFilterCriteriaMessage.Controls.Add(Me.rabSection)
        Me.gbFilterCriteriaMessage.ExpandedHoverImage = Nothing
        Me.gbFilterCriteriaMessage.ExpandedNormalImage = Nothing
        Me.gbFilterCriteriaMessage.ExpandedPressedImage = Nothing
        Me.gbFilterCriteriaMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteriaMessage.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteriaMessage.HeaderHeight = 25
        Me.gbFilterCriteriaMessage.HeaderMessage = ""
        Me.gbFilterCriteriaMessage.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteriaMessage.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteriaMessage.HeightOnCollapse = 0
        Me.gbFilterCriteriaMessage.LeftTextSpace = 0
        Me.gbFilterCriteriaMessage.Location = New System.Drawing.Point(9, 397)
        Me.gbFilterCriteriaMessage.Name = "gbFilterCriteriaMessage"
        Me.gbFilterCriteriaMessage.OpenHeight = 300
        Me.gbFilterCriteriaMessage.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteriaMessage.ShowBorder = True
        Me.gbFilterCriteriaMessage.ShowCheckBox = False
        Me.gbFilterCriteriaMessage.ShowCollapseButton = False
        Me.gbFilterCriteriaMessage.ShowDefaultBorderColor = True
        Me.gbFilterCriteriaMessage.ShowDownButton = False
        Me.gbFilterCriteriaMessage.ShowHeader = True
        Me.gbFilterCriteriaMessage.Size = New System.Drawing.Size(499, 75)
        Me.gbFilterCriteriaMessage.TabIndex = 2
        Me.gbFilterCriteriaMessage.Temp = 0
        Me.gbFilterCriteriaMessage.Text = "Filter Criteria For Messages"
        Me.gbFilterCriteriaMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rabPayPoint
        '
        Me.rabPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabPayPoint.Location = New System.Drawing.Point(122, 56)
        Me.rabPayPoint.Name = "rabPayPoint"
        Me.rabPayPoint.Size = New System.Drawing.Size(83, 17)
        Me.rabPayPoint.TabIndex = 4
        Me.rabPayPoint.TabStop = True
        Me.rabPayPoint.Text = "Pay Point"
        Me.rabPayPoint.UseVisualStyleBackColor = True
        '
        'rabCostCenter
        '
        Me.rabCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabCostCenter.Location = New System.Drawing.Point(14, 56)
        Me.rabCostCenter.Name = "rabCostCenter"
        Me.rabCostCenter.Size = New System.Drawing.Size(83, 17)
        Me.rabCostCenter.TabIndex = 1
        Me.rabCostCenter.TabStop = True
        Me.rabCostCenter.Text = "Cost Center"
        Me.rabCostCenter.UseVisualStyleBackColor = True
        '
        'rabClass
        '
        Me.rabClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabClass.Location = New System.Drawing.Point(211, 56)
        Me.rabClass.Name = "rabClass"
        Me.rabClass.Size = New System.Drawing.Size(83, 17)
        Me.rabClass.TabIndex = 7
        Me.rabClass.TabStop = True
        Me.rabClass.Text = "Class"
        Me.rabClass.UseVisualStyleBackColor = True
        '
        'rabGrade
        '
        Me.rabGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabGrade.Location = New System.Drawing.Point(122, 33)
        Me.rabGrade.Name = "rabGrade"
        Me.rabGrade.Size = New System.Drawing.Size(83, 17)
        Me.rabGrade.TabIndex = 3
        Me.rabGrade.TabStop = True
        Me.rabGrade.Text = "Grade"
        Me.rabGrade.UseVisualStyleBackColor = True
        '
        'rabJob
        '
        Me.rabJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabJob.Location = New System.Drawing.Point(211, 33)
        Me.rabJob.Name = "rabJob"
        Me.rabJob.Size = New System.Drawing.Size(83, 17)
        Me.rabJob.TabIndex = 6
        Me.rabJob.TabStop = True
        Me.rabJob.Text = "Job"
        Me.rabJob.UseVisualStyleBackColor = True
        '
        'rabEmployee
        '
        Me.rabEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabEmployee.Location = New System.Drawing.Point(14, 33)
        Me.rabEmployee.Name = "rabEmployee"
        Me.rabEmployee.Size = New System.Drawing.Size(83, 17)
        Me.rabEmployee.TabIndex = 0
        Me.rabEmployee.TabStop = True
        Me.rabEmployee.Text = "Employee"
        Me.rabEmployee.UseVisualStyleBackColor = True
        '
        'rabDept
        '
        Me.rabDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabDept.Location = New System.Drawing.Point(300, 33)
        Me.rabDept.Name = "rabDept"
        Me.rabDept.Size = New System.Drawing.Size(83, 17)
        Me.rabDept.TabIndex = 2
        Me.rabDept.TabStop = True
        Me.rabDept.Text = "Department "
        Me.rabDept.UseVisualStyleBackColor = True
        '
        'rabSection
        '
        Me.rabSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabSection.Location = New System.Drawing.Point(300, 56)
        Me.rabSection.Name = "rabSection"
        Me.rabSection.Size = New System.Drawing.Size(83, 17)
        Me.rabSection.TabIndex = 5
        Me.rabSection.TabStop = True
        Me.rabSection.Text = "Section"
        Me.rabSection.UseVisualStyleBackColor = True
        '
        'gbFilterCriteriaEmpPeriod
        '
        Me.gbFilterCriteriaEmpPeriod.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteriaEmpPeriod.Checked = False
        Me.gbFilterCriteriaEmpPeriod.CollapseAllExceptThis = False
        Me.gbFilterCriteriaEmpPeriod.CollapsedHoverImage = Nothing
        Me.gbFilterCriteriaEmpPeriod.CollapsedNormalImage = Nothing
        Me.gbFilterCriteriaEmpPeriod.CollapsedPressedImage = Nothing
        Me.gbFilterCriteriaEmpPeriod.CollapseOnLoad = False
        Me.gbFilterCriteriaEmpPeriod.Controls.Add(Me.EZeeSearchResetbtn)
        Me.gbFilterCriteriaEmpPeriod.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteriaEmpPeriod.Controls.Add(Me.lnkAdvanceFilter)
        Me.gbFilterCriteriaEmpPeriod.Controls.Add(Me.objlblEmpCount)
        Me.gbFilterCriteriaEmpPeriod.Controls.Add(Me.lblSearchPeriod)
        Me.gbFilterCriteriaEmpPeriod.Controls.Add(Me.lblSearchEmp)
        Me.gbFilterCriteriaEmpPeriod.Controls.Add(Me.Panel1)
        Me.gbFilterCriteriaEmpPeriod.Controls.Add(Me.pnlEmployeeList)
        Me.gbFilterCriteriaEmpPeriod.ExpandedHoverImage = Nothing
        Me.gbFilterCriteriaEmpPeriod.ExpandedNormalImage = Nothing
        Me.gbFilterCriteriaEmpPeriod.ExpandedPressedImage = Nothing
        Me.gbFilterCriteriaEmpPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteriaEmpPeriod.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteriaEmpPeriod.HeaderHeight = 25
        Me.gbFilterCriteriaEmpPeriod.HeaderMessage = ""
        Me.gbFilterCriteriaEmpPeriod.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteriaEmpPeriod.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteriaEmpPeriod.HeightOnCollapse = 0
        Me.gbFilterCriteriaEmpPeriod.LeftTextSpace = 0
        Me.gbFilterCriteriaEmpPeriod.Location = New System.Drawing.Point(514, 163)
        Me.gbFilterCriteriaEmpPeriod.Name = "gbFilterCriteriaEmpPeriod"
        Me.gbFilterCriteriaEmpPeriod.OpenHeight = 300
        Me.gbFilterCriteriaEmpPeriod.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteriaEmpPeriod.ShowBorder = True
        Me.gbFilterCriteriaEmpPeriod.ShowCheckBox = False
        Me.gbFilterCriteriaEmpPeriod.ShowCollapseButton = False
        Me.gbFilterCriteriaEmpPeriod.ShowDefaultBorderColor = True
        Me.gbFilterCriteriaEmpPeriod.ShowDownButton = False
        Me.gbFilterCriteriaEmpPeriod.ShowHeader = True
        Me.gbFilterCriteriaEmpPeriod.Size = New System.Drawing.Size(407, 308)
        Me.gbFilterCriteriaEmpPeriod.TabIndex = 1
        Me.gbFilterCriteriaEmpPeriod.Temp = 0
        Me.gbFilterCriteriaEmpPeriod.Text = "Filter Criteria"
        Me.gbFilterCriteriaEmpPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeSearchResetbtn
        '
        Me.EZeeSearchResetbtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EZeeSearchResetbtn.BackColor = System.Drawing.Color.Transparent
        Me.EZeeSearchResetbtn.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.EZeeSearchResetbtn.Image = CType(resources.GetObject("EZeeSearchResetbtn.Image"), System.Drawing.Image)
        Me.EZeeSearchResetbtn.Location = New System.Drawing.Point(380, 0)
        Me.EZeeSearchResetbtn.Name = "EZeeSearchResetbtn"
        Me.EZeeSearchResetbtn.ResultMessage = ""
        Me.EZeeSearchResetbtn.SearchMessage = ""
        Me.EZeeSearchResetbtn.Size = New System.Drawing.Size(24, 24)
        Me.EZeeSearchResetbtn.TabIndex = 311
        Me.EZeeSearchResetbtn.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(355, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 287
        Me.objbtnSearch.TabStop = False
        '
        'lnkAdvanceFilter
        '
        Me.lnkAdvanceFilter.BackColor = System.Drawing.Color.Transparent
        Me.lnkAdvanceFilter.Location = New System.Drawing.Point(244, 4)
        Me.lnkAdvanceFilter.Name = "lnkAdvanceFilter"
        Me.lnkAdvanceFilter.Size = New System.Drawing.Size(97, 16)
        Me.lnkAdvanceFilter.TabIndex = 310
        Me.lnkAdvanceFilter.TabStop = True
        Me.lnkAdvanceFilter.Text = "Advance Filter"
        Me.lnkAdvanceFilter.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objlblEmpCount
        '
        Me.objlblEmpCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblEmpCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmpCount.Location = New System.Drawing.Point(163, 1)
        Me.objlblEmpCount.Name = "objlblEmpCount"
        Me.objlblEmpCount.Size = New System.Drawing.Size(82, 20)
        Me.objlblEmpCount.TabIndex = 161
        Me.objlblEmpCount.Text = "( 0 )"
        Me.objlblEmpCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSearchPeriod
        '
        Me.lblSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchPeriod.Location = New System.Drawing.Point(90, 4)
        Me.lblSearchPeriod.Name = "lblSearchPeriod"
        Me.lblSearchPeriod.Size = New System.Drawing.Size(50, 15)
        Me.lblSearchPeriod.TabIndex = 15
        Me.lblSearchPeriod.Text = "Search Period"
        Me.lblSearchPeriod.Visible = False
        '
        'lblSearchEmp
        '
        Me.lblSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchEmp.Location = New System.Drawing.Point(146, 4)
        Me.lblSearchEmp.Name = "lblSearchEmp"
        Me.lblSearchEmp.Size = New System.Drawing.Size(30, 14)
        Me.lblSearchEmp.TabIndex = 14
        Me.lblSearchEmp.Text = "Search Employee"
        Me.lblSearchEmp.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkPeriodSelectAll)
        Me.Panel1.Controls.Add(Me.dgPeriod)
        Me.Panel1.Controls.Add(Me.txtSearchPeriod)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(223, 27)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(179, 278)
        Me.Panel1.TabIndex = 10
        '
        'objchkPeriodSelectAll
        '
        Me.objchkPeriodSelectAll.AutoSize = True
        Me.objchkPeriodSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkPeriodSelectAll.Name = "objchkPeriodSelectAll"
        Me.objchkPeriodSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkPeriodSelectAll.TabIndex = 18
        Me.objchkPeriodSelectAll.UseVisualStyleBackColor = True
        '
        'dgPeriod
        '
        Me.dgPeriod.AllowUserToAddRows = False
        Me.dgPeriod.AllowUserToDeleteRows = False
        Me.dgPeriod.AllowUserToResizeRows = False
        Me.dgPeriod.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgPeriod.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgPeriod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgPeriod.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgPeriod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgPeriod.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgperiodcolhCheck, Me.objdgcolhPeriodunkid, Me.dgColhPeriodCode, Me.dgColhPeriod, Me.dgcolhPeriodStart, Me.dgcolhPeriodEnd})
        Me.dgPeriod.Location = New System.Drawing.Point(0, 28)
        Me.dgPeriod.Name = "dgPeriod"
        Me.dgPeriod.RowHeadersVisible = False
        Me.dgPeriod.RowHeadersWidth = 5
        Me.dgPeriod.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgPeriod.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPeriod.Size = New System.Drawing.Size(176, 247)
        Me.dgPeriod.TabIndex = 286
        '
        'objdgperiodcolhCheck
        '
        Me.objdgperiodcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgperiodcolhCheck.HeaderText = ""
        Me.objdgperiodcolhCheck.Name = "objdgperiodcolhCheck"
        Me.objdgperiodcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgperiodcolhCheck.Width = 25
        '
        'objdgcolhPeriodunkid
        '
        Me.objdgcolhPeriodunkid.HeaderText = "periodunkid"
        Me.objdgcolhPeriodunkid.Name = "objdgcolhPeriodunkid"
        Me.objdgcolhPeriodunkid.ReadOnly = True
        Me.objdgcolhPeriodunkid.Visible = False
        '
        'dgColhPeriodCode
        '
        Me.dgColhPeriodCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhPeriodCode.HeaderText = "Code"
        Me.dgColhPeriodCode.Name = "dgColhPeriodCode"
        Me.dgColhPeriodCode.ReadOnly = True
        Me.dgColhPeriodCode.Width = 50
        '
        'dgColhPeriod
        '
        Me.dgColhPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dgColhPeriod.HeaderText = "Period Name"
        Me.dgColhPeriod.Name = "dgColhPeriod"
        Me.dgColhPeriod.ReadOnly = True
        Me.dgColhPeriod.Width = 92
        '
        'dgcolhPeriodStart
        '
        Me.dgcolhPeriodStart.HeaderText = "Period Start"
        Me.dgcolhPeriodStart.Name = "dgcolhPeriodStart"
        Me.dgcolhPeriodStart.ReadOnly = True
        Me.dgcolhPeriodStart.Visible = False
        '
        'dgcolhPeriodEnd
        '
        Me.dgcolhPeriodEnd.HeaderText = "Period End"
        Me.dgcolhPeriodEnd.Name = "dgcolhPeriodEnd"
        Me.dgcolhPeriodEnd.ReadOnly = True
        Me.dgcolhPeriodEnd.Visible = False
        '
        'txtSearchPeriod
        '
        Me.txtSearchPeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchPeriod.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchPeriod.Location = New System.Drawing.Point(0, 3)
        Me.txtSearchPeriod.Name = "txtSearchPeriod"
        Me.txtSearchPeriod.Size = New System.Drawing.Size(176, 21)
        Me.txtSearchPeriod.TabIndex = 12
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgEmployee)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearchEmp)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(3, 27)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(214, 278)
        Me.pnlEmployeeList.TabIndex = 9
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhEmployeeunkid, Me.dgColhEmpCode, Me.dgColhEmployee})
        Me.dgEmployee.Location = New System.Drawing.Point(1, 28)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmployee.Size = New System.Drawing.Size(210, 247)
        Me.dgEmployee.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhEmployeeunkid
        '
        Me.objdgcolhEmployeeunkid.HeaderText = "employeeunkid"
        Me.objdgcolhEmployeeunkid.Name = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.ReadOnly = True
        Me.objdgcolhEmployeeunkid.Visible = False
        '
        'dgColhEmpCode
        '
        Me.dgColhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmpCode.HeaderText = "Emp. Code"
        Me.dgColhEmpCode.Name = "dgColhEmpCode"
        Me.dgColhEmpCode.ReadOnly = True
        Me.dgColhEmpCode.Width = 70
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dgColhEmployee.HeaderText = "Employee Name"
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        Me.dgColhEmployee.Width = 108
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(1, 1)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(210, 21)
        Me.txtSearchEmp.TabIndex = 12
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(513, 94)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(408, 63)
        Me.gbSortBy.TabIndex = 3
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(353, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(68, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(93, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(254, 21)
        Me.txtOrderBy.TabIndex = 0
        '
        'gbEmpSortBy
        '
        Me.gbEmpSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbEmpSortBy.Checked = False
        Me.gbEmpSortBy.CollapseAllExceptThis = False
        Me.gbEmpSortBy.CollapsedHoverImage = Nothing
        Me.gbEmpSortBy.CollapsedNormalImage = Nothing
        Me.gbEmpSortBy.CollapsedPressedImage = Nothing
        Me.gbEmpSortBy.CollapseOnLoad = False
        Me.gbEmpSortBy.Controls.Add(Me.radDescending)
        Me.gbEmpSortBy.Controls.Add(Me.radAscending)
        Me.gbEmpSortBy.Controls.Add(Me.cboEmpSortBy)
        Me.gbEmpSortBy.Controls.Add(Me.lblEmpSortBy)
        Me.gbEmpSortBy.ExpandedHoverImage = Nothing
        Me.gbEmpSortBy.ExpandedNormalImage = Nothing
        Me.gbEmpSortBy.ExpandedPressedImage = Nothing
        Me.gbEmpSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmpSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmpSortBy.HeaderHeight = 25
        Me.gbEmpSortBy.HeaderMessage = ""
        Me.gbEmpSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmpSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmpSortBy.HeightOnCollapse = 0
        Me.gbEmpSortBy.LeftTextSpace = 0
        Me.gbEmpSortBy.Location = New System.Drawing.Point(514, 6)
        Me.gbEmpSortBy.Name = "gbEmpSortBy"
        Me.gbEmpSortBy.OpenHeight = 300
        Me.gbEmpSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmpSortBy.ShowBorder = True
        Me.gbEmpSortBy.ShowCheckBox = False
        Me.gbEmpSortBy.ShowCollapseButton = False
        Me.gbEmpSortBy.ShowDefaultBorderColor = True
        Me.gbEmpSortBy.ShowDownButton = False
        Me.gbEmpSortBy.ShowHeader = True
        Me.gbEmpSortBy.Size = New System.Drawing.Size(407, 82)
        Me.gbEmpSortBy.TabIndex = 4
        Me.gbEmpSortBy.Temp = 0
        Me.gbEmpSortBy.Text = "Employee Sort By"
        Me.gbEmpSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radDescending
        '
        Me.radDescending.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDescending.Location = New System.Drawing.Point(182, 59)
        Me.radDescending.Name = "radDescending"
        Me.radDescending.Size = New System.Drawing.Size(83, 17)
        Me.radDescending.TabIndex = 6
        Me.radDescending.Text = "Descending"
        Me.radDescending.UseVisualStyleBackColor = True
        '
        'radAscending
        '
        Me.radAscending.Checked = True
        Me.radAscending.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAscending.Location = New System.Drawing.Point(93, 59)
        Me.radAscending.Name = "radAscending"
        Me.radAscending.Size = New System.Drawing.Size(83, 17)
        Me.radAscending.TabIndex = 5
        Me.radAscending.TabStop = True
        Me.radAscending.Text = "Ascending"
        Me.radAscending.UseVisualStyleBackColor = True
        '
        'cboEmpSortBy
        '
        Me.cboEmpSortBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpSortBy.DropDownWidth = 120
        Me.cboEmpSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpSortBy.FormattingEnabled = True
        Me.cboEmpSortBy.Location = New System.Drawing.Point(93, 32)
        Me.cboEmpSortBy.Name = "cboEmpSortBy"
        Me.cboEmpSortBy.Size = New System.Drawing.Size(174, 21)
        Me.cboEmpSortBy.TabIndex = 4
        '
        'lblEmpSortBy
        '
        Me.lblEmpSortBy.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpSortBy.Location = New System.Drawing.Point(8, 36)
        Me.lblEmpSortBy.Name = "lblEmpSortBy"
        Me.lblEmpSortBy.Size = New System.Drawing.Size(79, 13)
        Me.lblEmpSortBy.TabIndex = 0
        Me.lblEmpSortBy.Text = "Emp. Sort By"
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.gbFilterCriteriaMembership)
        Me.Panel2.Controls.Add(Me.gbEmpSortBy)
        Me.Panel2.Controls.Add(Me.gbSortBy)
        Me.Panel2.Controls.Add(Me.gbFilterCriteriaEmpPeriod)
        Me.Panel2.Controls.Add(Me.gbFilterCriteriaMessage)
        Me.Panel2.Location = New System.Drawing.Point(0, 60)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(929, 475)
        Me.Panel2.TabIndex = 5
        '
        'chkShowRemainingNoofLoanInstallments
        '
        Me.chkShowRemainingNoofLoanInstallments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowRemainingNoofLoanInstallments.Location = New System.Drawing.Point(14, 331)
        Me.chkShowRemainingNoofLoanInstallments.Name = "chkShowRemainingNoofLoanInstallments"
        Me.chkShowRemainingNoofLoanInstallments.Size = New System.Drawing.Size(205, 17)
        Me.chkShowRemainingNoofLoanInstallments.TabIndex = 232
        Me.chkShowRemainingNoofLoanInstallments.Text = "Show Remaining Loan Installments"
        Me.chkShowRemainingNoofLoanInstallments.UseVisualStyleBackColor = True
        '
        'frmPaySlip
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(930, 592)
        Me.Controls.Add(Me.Panel2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPaySlip"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Salary Slip"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.Panel2, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbFilterCriteriaMembership.ResumeLayout(False)
        Me.gbFilterCriteriaMembership.PerformLayout()
        Me.gbFilterCriteriaMessage.ResumeLayout(False)
        Me.gbFilterCriteriaEmpPeriod.ResumeLayout(False)
        CType(Me.EZeeSearchResetbtn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgPeriod, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbEmpSortBy.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents rabPayPoint As System.Windows.Forms.RadioButton
    Friend WithEvents rabCostCenter As System.Windows.Forms.RadioButton
    Friend WithEvents rabClass As System.Windows.Forms.RadioButton
    Friend WithEvents rabGrade As System.Windows.Forms.RadioButton
    Friend WithEvents rabJob As System.Windows.Forms.RadioButton
    Friend WithEvents rabEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents rabDept As System.Windows.Forms.RadioButton
    Friend WithEvents rabSection As System.Windows.Forms.RadioButton
    Private WithEvents lblMembership As System.Windows.Forms.Label
    Public WithEvents cboMembership As System.Windows.Forms.ComboBox
    Private WithEvents lblLeaveType As System.Windows.Forms.Label
    Public WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Public WithEvents gbFilterCriteriaMembership As eZee.Common.eZeeCollapsibleContainer
    Public WithEvents gbFilterCriteriaEmpPeriod As eZee.Common.eZeeCollapsibleContainer
    Public WithEvents gbFilterCriteriaMessage As eZee.Common.eZeeCollapsibleContainer
    Public WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Public WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Friend WithEvents chkIgnoreZeroHead As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowSavingBalance As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowLoanBalance As System.Windows.Forms.CheckBox
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents radLogoCompanyInfo As System.Windows.Forms.RadioButton
    Friend WithEvents radLogo As System.Windows.Forms.RadioButton
    Friend WithEvents radCompanyDetails As System.Windows.Forms.RadioButton
    Friend WithEvents chkShowAllHeads As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowEmployerContrib As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowCumulativeAccrual As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowEachPayslipOnNewPage As System.Windows.Forms.CheckBox
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Public WithEvents gbEmpSortBy As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblEmpSortBy As System.Windows.Forms.Label
    Public WithEvents cboEmpSortBy As System.Windows.Forms.ComboBox
    Friend WithEvents radDescending As System.Windows.Forms.RadioButton
    Friend WithEvents radAscending As System.Windows.Forms.RadioButton
    Friend WithEvents chkShowCategory As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowInformationalHeads As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowTwoPayslipPerPage As System.Windows.Forms.CheckBox
    Friend WithEvents chkBasicSalary As System.Windows.Forms.CheckBox
    Friend WithEvents chkAge As System.Windows.Forms.CheckBox
    Friend WithEvents chkDependents As System.Windows.Forms.CheckBox
    Friend WithEvents chkDOB As System.Windows.Forms.CheckBox
    Friend WithEvents cboOT4Amount As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT4Amount As System.Windows.Forms.Label
    Friend WithEvents cboOT3Amount As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT3Amount As System.Windows.Forms.Label
    Friend WithEvents cboOT2Amount As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT2Amount As System.Windows.Forms.Label
    Friend WithEvents cboOT1Amount As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT1Amount As System.Windows.Forms.Label
    Friend WithEvents cboOT4Hours As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT4Hours As System.Windows.Forms.Label
    Friend WithEvents cboOT3Hours As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT3Hours As System.Windows.Forms.Label
    Friend WithEvents cboOT2Hours As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT2Hours As System.Windows.Forms.Label
    Friend WithEvents cboOT1Hours As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT1Hours As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents chkShowBankAccountNo As System.Windows.Forms.CheckBox
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Private WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkPeriodSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgPeriod As System.Windows.Forms.DataGridView
    Private WithEvents txtSearchPeriod As System.Windows.Forms.TextBox
    Private WithEvents lblSearchPeriod As System.Windows.Forms.Label
    Private WithEvents lblSearchEmp As System.Windows.Forms.Label
    Friend WithEvents objlblEmpCount As System.Windows.Forms.Label
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgperiodcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhPeriodunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhPeriodCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPeriodStart As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPeriodEnd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkAdvanceFilter As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents EZeeSearchResetbtn As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents chkShowLoanReceived As System.Windows.Forms.CheckBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents chkShowRemainingNoofLoanInstallments As System.Windows.Forms.CheckBox
End Class
