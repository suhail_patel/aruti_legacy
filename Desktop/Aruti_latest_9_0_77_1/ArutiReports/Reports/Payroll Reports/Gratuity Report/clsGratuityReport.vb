Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

Public Class clsGratuityReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsGratuityReport"
    Private mstrReportId As String = enArutiReport.Gratuity_Report
    Dim objDataOperation As clsDataOperation

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintGratuityHeadID As Integer = -1
    'Private mstrGratuityHeadCode As String = String.Empty
    Private mstrGratuityHeadName As String = ""
    Private mintCurrGratuityHeadID As Integer = -1
    Private mstrCurrGratuityHeadName As String = ""

    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    Private mstrOrderByQuery As String = ""
    Private mblnIsActive As Boolean = True


    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrPeriodID As String = ""

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mstrAdvance_Filter As String = String.Empty

    Private mintBase_CurrencyId As Integer = ConfigParameter._Object._Base_CurrencyId
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    Private mintUserUnkid As Integer = User._Object._Userunkid
    Private mintCompanyUnkid As Integer = Company._Object._Companyunkid
    Private mstrTranDatabaseName As String = FinancialYear._Object._DatabaseName

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _HeadId() As Integer
        Set(ByVal value As Integer)
            mintGratuityHeadID = value
        End Set
    End Property

    'Public WriteOnly Property _HeadCode() As String
    '    Set(ByVal value As String)
    '        mstrGratuityHeadCode = value
    '    End Set
    'End Property

    Public WriteOnly Property _HeadName() As String
        Set(ByVal value As String)
            mstrGratuityHeadName = value
        End Set
    End Property

    Public WriteOnly Property _Current_HeadId() As Integer
        Set(ByVal value As Integer)
            mintCurrGratuityHeadID = value
        End Set
    End Property

    Public WriteOnly Property _Current_HeadName() As String
        Set(ByVal value As String)
            mstrCurrGratuityHeadName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _mstrPeriodID() As String
        Set(ByVal value As String)
            mstrPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _Base_CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBase_CurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TranDatabaseName() As String
        Set(ByVal value As String)
            mstrTranDatabaseName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintGratuityHeadID = -1
            mstrGratuityHeadName = ""
            mintCurrGratuityHeadID = -1
            mstrCurrGratuityHeadName = ""
            mintPeriodId = -1
            mstrPeriodName = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            mstrAdvance_Filter = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub




    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            Dim mstrheadID As String = ""
            Dim mstrModeID As String = ""


            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If


            If mintGratuityHeadID > 0 Then
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGratuityHeadID)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Gratuity Head : ") & " " & mstrGratuityHeadName & " "
            End If

            If mintCurrGratuityHeadID > 0 Then
                objDataOperation.AddParameter("@currtranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrGratuityHeadID)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Current Month Gratuity Head : ") & " " & mstrCurrGratuityHeadName & " "
            End If

            If mintPeriodId > 0 Then
                objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Period : ") & " " & mstrPeriodName & " "
            End If


            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                If mintViewIndex > 0 Then
                    If Me.OrderByQuery.Trim = "" Then
                        Me._FilterQuery &= "ORDER BY GNAME"
                    Else
                        Me._FilterQuery &= "ORDER BY GNAME, " & Me.OrderByQuery
                    End If

                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid
        '    User._Object._Userunkid = mintUserUnkid


        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing



        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then

        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        Dim blnShowGrandTotal As Boolean = True
        '        Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
        '        Dim objDicSubTotal As New Dictionary(Of Integer, Object)
        '        Dim objDicSubGroupTotal As New Dictionary(Of Integer, Object)
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim strGTotal As String = Language.getMessage(mstrModuleName, 21, "Total Count :")
        '        Dim row As WorksheetRow
        '        Dim wcell As WorksheetCell
        '        Dim arrGroupCount() As String = {Language.getMessage(mstrModuleName, 27, "Period Count:")}
        '        Dim arrDicGroupExtraInfo() As Dictionary(Of Integer, Object) = Nothing


        '        If mdtTableExcel IsNot Nothing Then

        '            Dim intGroupColumn As Integer = 0

        '            If mintViewIndex > 0 Then

        '                Dim strGrpCols As String() = {"column1", "column10"}
        '                strarrGroupColumns = strGrpCols
        '                Dim arr() As String = {Language.getMessage(mstrModuleName, 33, "Sub Count :"), Language.getMessage(mstrModuleName, 27, "Period Count:")}
        '                arrGroupCount = arr

        '                objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 20, "Sub Total:"))
        '                objDicSubGroupTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

        '                Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal, objDicSubGroupTotal}
        '                arrDicGroupExtraInfo = arrDic
        '            Else

        '                Dim strGrpCols As String() = {"column10"}
        '                strarrGroupColumns = strGrpCols
        '                objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

        '                Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal}
        '                arrDicGroupExtraInfo = arrDic
        '            End If

        '            objDicGrandTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 19, "Grand Total :"))


        '            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '                intArrayColumnWidth(i) = 125
        '            Next

        '            '*******   REPORT FOOTER   ******
        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s8w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)
        '            '----------------------


        '            If ConfigParameter._Object._IsShowCheckedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Checked By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowApprovedBy = True Then

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Approved By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowReceivedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)
        '            End If

        '        End If

        '        'Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)

        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", arrGroupCount, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, arrDicGroupExtraInfo, True)

        '    End If


        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Dim objUser As New clsUserAddEdit

        Try

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid
            User._Object._Userunkid = mintUserUnkid


            menExportAction = ExportAction
            mdtTableExcel = Nothing



            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objConfig._CurrencyFormat, objUser._Username)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then

                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim blnShowGrandTotal As Boolean = True
                Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
                Dim objDicSubTotal As New Dictionary(Of Integer, Object)
                Dim objDicSubGroupTotal As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim strGTotal As String = Language.getMessage(mstrModuleName, 21, "Total Count :")
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell
                Dim arrGroupCount() As String = {Language.getMessage(mstrModuleName, 27, "Period Count:")}
                Dim arrDicGroupExtraInfo() As Dictionary(Of Integer, Object) = Nothing


                If mdtTableExcel IsNot Nothing Then

                    Dim intGroupColumn As Integer = 0

                    If mintViewIndex > 0 Then

                        Dim strGrpCols As String() = {"column1", "column10"}
                        strarrGroupColumns = strGrpCols
                        Dim arr() As String = {Language.getMessage(mstrModuleName, 33, "Sub Count :"), Language.getMessage(mstrModuleName, 27, "Period Count:")}
                        arrGroupCount = arr

                        objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 20, "Sub Total:"))
                        objDicSubGroupTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

                        Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal, objDicSubGroupTotal}
                        arrDicGroupExtraInfo = arrDic
                    Else

                        Dim strGrpCols As String() = {"column10"}
                        strarrGroupColumns = strGrpCols
                        objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

                        Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal}
                        arrDicGroupExtraInfo = arrDic
                    End If

                    objDicGrandTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 19, "Grand Total :"))


                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next

                    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------


                    If ConfigParameter._Object._IsShowCheckedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Checked By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowApprovedBy = True Then

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Approved By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowReceivedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    End If

                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", arrGroupCount, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, arrDicGroupExtraInfo, True)

            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
            objConfig = Nothing
            objUser = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'iColumn_DetailReport.Add(New IColumn("tblBasic.end_date", Language.getMessage(mstrModuleName, 9, "Period")))
            iColumn_DetailReport.Add(New IColumn("tblBasic.employeecode", Language.getMessage(mstrModuleName, 9, "Emp Code")))
            iColumn_DetailReport.Add(New IColumn("tblBasic.employeename", Language.getMessage(mstrModuleName, 10, "Emp Name")))
            iColumn_DetailReport.Add(New IColumn("tblBasic.appointeddate", Language.getMessage(mstrModuleName, 11, "Appointed Date")))
            iColumn_DetailReport.Add(New IColumn("tblBasic.basicsalary", Language.getMessage(mstrModuleName, 13, "Basic Salary")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub



    Public Function Generate_DetailReport(ByVal xDatabaseName As String _
                                         , ByVal xUserUnkid As Integer _
                                         , ByVal xYearUnkid As Integer _
                                         , ByVal xCompanyUnkid As Integer _
                                         , ByVal xPeriodStart As Date _
                                         , ByVal xPeriodEnd As Date _
                                         , ByVal xUserModeSetting As String _
                                         , ByVal xOnlyApproved As Boolean _
                                         , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                         , ByVal blnApplyUserAccessFilter As Boolean _
                                         , ByVal strFmtCurrency As String _
                                         , ByVal strUsername As String _
                                        ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFmtCurrency, strUsername]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            mstrfmtCurrency = strFmtCurrency
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'StrQ = "SELECT  tblBasic.payperiodunkid " & _
            '              ", tblBasic.period_name " & _
            '              ", tblBasic.start_date " & _
            '              ", tblBasic.end_date " & _
            '              ", tblBasic.employeeunkid " & _
            '              ", tblBasic.employeecode " & _
            '              ", tblBasic.employeename " & _
            '              ", tblBasic.appointeddate " & _
            '              ", tblBasic.termination_from_date " & _
            '              ", tblBasic.termination_to_date " & _
            '              ", tblBasic.empl_enddate " & _
            '              ", tblBasic.basicsalary " & _
            '              ", ISNULL(tblGratuity.gratuity, 0) AS gratuity " & _
            '              ", ISNULL(tblCurrGratuity.currentgratuity, 0) AS currentgratuity " & _
            '              ", tblBasic.Id " & _
            '              ", tblBasic.GName " & _
            '        "FROM    ( SELECT    prtnaleave_tran.payperiodunkid " & _
            '                          ", cfcommon_period_tran.period_name " & _
            '                          ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
            '                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
            '                          ", prpayrollprocess_tran.employeeunkid " & _
            '                          ", hremployee_master.employeecode " & _
            '                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
            '                          ", CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
            '                          ", CONVERT(CHAR(8), termination_from_date, 112) AS termination_from_date " & _
            '                          ", CONVERT(CHAR(8), termination_to_date, 112) AS termination_to_date " & _
            '                          ", CONVERT(CHAR(8), empl_enddate, 112) AS empl_enddate " & _
            '                          ", SUM(amount) AS basicsalary " & _
            '                          ", 0 AS gratuity " & _
            '                          ", 0 AS currentgratuity "
            StrQ = "SELECT  tblBasic.payperiodunkid " & _
                          ", tblBasic.period_name " & _
                          ", tblBasic.start_date " & _
                          ", tblBasic.end_date " & _
                          ", tblBasic.employeeunkid " & _
                          ", tblBasic.employeecode " & _
                          ", tblBasic.employeename " & _
                          ", tblBasic.appointeddate " & _
                          ", tblBasic.termination_from_date " & _
                          ", tblBasic.termination_to_date " & _
                          ", tblBasic.empl_enddate " & _
                          ", tblBasic.basicsalary " & _
                          ", ISNULL(tblGratuity.gratuity, 0) AS gratuity " & _
                          ", ISNULL(tblCurrGratuity.currentgratuity, 0) AS currentgratuity " & _
                          ", tblBasic.Id " & _
                          ", tblBasic.GName " & _
                    "FROM    ( SELECT    prtnaleave_tran.payperiodunkid " & _
                                      ", cfcommon_period_tran.period_name " & _
                                      ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                                      ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                                      ", prpayrollprocess_tran.employeeunkid " & _
                                      ", hremployee_master.employeecode " & _
                                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                                      ", CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
                                      ", ISNULL(ETERM.termination_from_date,'') As termination_from_date " & _
                                      ", ISNULL(ERET.termination_to_date,'') As termination_to_date " & _
                                      ", ISNULL(ETERM.empl_enddate,'') AS empl_enddate " & _
                                      ", SUM(amount) AS basicsalary " & _
                                      ", 0 AS gratuity " & _
                                      ", 0 AS currentgratuity "
            'Sohail (21 Aug 2015) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "          FROM      prpayrollprocess_tran " & _
                                        "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                        "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                        "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid "

            StrQ &= mstrAnalysis_Join

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 TERM.TEEmpId " & _
                    "		,TERM.empl_enddate " & _
                    "		,TERM.termination_from_date " & _
                    "		,TERM.TEfDt " & _
                    "		,TERM.isexclude_payroll " & _
                    "		,TERM.TR_REASON " & _
                    "		,TERM.changereasonunkid " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            TRM.employeeunkid AS TEEmpId " & _
                    "           ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                    "           ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                    "           ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                    "           ,TRM.isexclude_payroll " & _
                    "			,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                    "           ,TRM.changereasonunkid " & _
                    "       FROM hremployee_dates_tran AS TRM " & _
                    "			LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                    "		WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS TERM WHERE TERM.Rno = 1 " & _
                    ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 RET.REmpId " & _
                    "		,RET.termination_to_date " & _
                    "		,RET.REfDt " & _
                    "		,RET.RET_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            RTD.employeeunkid AS REmpId " & _
                    "           ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                    "           ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                    "       FROM hremployee_dates_tran AS RTD " & _
                    "			LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                    "		WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS RET WHERE RET.Rno = 1 " & _
                    ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "          WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid = @payperiodunkid " & _
                                        "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            'Sohail (21 Aug 2015) -- End

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= "          GROUP BY  prtnaleave_tran.payperiodunkid " & _
                                      ", cfcommon_period_tran.period_name " & _
                                      ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) " & _
                                      ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) " & _
                                      ", prpayrollprocess_tran.employeeunkid " & _
                                      ", hremployee_master.employeecode " & _
                                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
                                      ", CONVERT(CHAR(8), appointeddate, 112) " & _
                                      ", ISNULL(ETERM.termination_from_date,'') " & _
                                      ", ISNULL(ERET.termination_to_date,'') " & _
                                      ", ISNULL(ETERM.empl_enddate,'') " & _
                            ") AS tblBasic " & _
                            "LEFT JOIN ( SELECT  prtnaleave_tran.payperiodunkid " & _
                                              ", cfcommon_period_tran.period_name " & _
                                              ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                                              ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                                              ", prpayrollprocess_tran.employeeunkid " & _
                                              ", hremployee_master.employeecode " & _
                                              ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                                              ", CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
                                              ", ISNULL(ETERM.termination_from_date,'') As termination_from_date " & _
                                              ", ISNULL(ERET.termination_to_date,'') As termination_to_date " & _
                                              ", ISNULL(ETERM.empl_enddate,'') AS empl_enddate " & _
                                              ", 0 AS basicsalary " & _
                                              ", amount AS gratuity " & _
                                              ", 0 AS currentgratuity "
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Removed
            '", CONVERT(CHAR(8), termination_from_date, 112) AS termination_from_date " & _
            '  ", CONVERT(CHAR(8), termination_to_date, 112) AS termination_to_date " & _
            '  ", CONVERT(CHAR(8), empl_enddate, 112) AS empl_enddate " & _
            'Sohail (21 Aug 2015) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                    FROM    prpayrollprocess_tran " & _
                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid "

            StrQ &= mstrAnalysis_Join

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 TERM.TEEmpId " & _
                    "		,TERM.empl_enddate " & _
                    "		,TERM.termination_from_date " & _
                    "		,TERM.TEfDt " & _
                    "		,TERM.isexclude_payroll " & _
                    "		,TERM.TR_REASON " & _
                    "		,TERM.changereasonunkid " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            TRM.employeeunkid AS TEEmpId " & _
                    "           ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                    "           ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                    "           ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                    "           ,TRM.isexclude_payroll " & _
                    "			,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                    "           ,TRM.changereasonunkid " & _
                    "       FROM hremployee_dates_tran AS TRM " & _
                    "			LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                    "		WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS TERM WHERE TERM.Rno = 1 " & _
                    ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 RET.REmpId " & _
                    "		,RET.termination_to_date " & _
                    "		,RET.REfDt " & _
                    "		,RET.RET_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            RTD.employeeunkid AS REmpId " & _
                    "           ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                    "           ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                    "       FROM hremployee_dates_tran AS RTD " & _
                    "			LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                    "		WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS RET WHERE RET.Rno = 1 " & _
                    ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid = @payperiodunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            'Sohail (21 Aug 2015) -- End

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= "           ) AS tblGratuity ON tblBasic.payperiodunkid = tblGratuity.payperiodunkid " & _
                                                          "AND tblBasic.employeeunkid = tblGratuity.employeeunkid " & _
                    "LEFT JOIN ( SELECT  prtnaleave_tran.payperiodunkid " & _
                                                      ", cfcommon_period_tran.period_name " & _
                                                      ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                                                      ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                                                      ", prpayrollprocess_tran.employeeunkid " & _
                                                      ", hremployee_master.employeecode " & _
                                                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                                                      ", CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
                                                      ", ISNULL(ETERM.termination_from_date,'') As termination_from_date " & _
                                                      ", ISNULL(ERET.termination_to_date,'') As termination_to_date " & _
                                                      ", ISNULL(ETERM.empl_enddate,'') AS empl_enddate " & _
                                                      ", 0 AS basicsalary " & _
                                                      ", 0 AS gratuity " & _
                                                      ", amount AS currentgratuity "
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Removed
            '", CONVERT(CHAR(8), termination_from_date, 112) AS termination_from_date " & _
            '", CONVERT(CHAR(8), termination_to_date, 112) AS termination_to_date " & _
            '", CONVERT(CHAR(8), empl_enddate, 112) AS empl_enddate " & _
            'Sohail (21 Aug 2015) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                    FROM    prpayrollprocess_tran " & _
                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid "

            StrQ &= mstrAnalysis_Join

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 TERM.TEEmpId " & _
                    "		,TERM.empl_enddate " & _
                    "		,TERM.termination_from_date " & _
                    "		,TERM.TEfDt " & _
                    "		,TERM.isexclude_payroll " & _
                    "		,TERM.TR_REASON " & _
                    "		,TERM.changereasonunkid " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            TRM.employeeunkid AS TEEmpId " & _
                    "           ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                    "           ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                    "           ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                    "           ,TRM.isexclude_payroll " & _
                    "			,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                    "           ,TRM.changereasonunkid " & _
                    "       FROM hremployee_dates_tran AS TRM " & _
                    "			LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                    "		WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS TERM WHERE TERM.Rno = 1 " & _
                    ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 RET.REmpId " & _
                    "		,RET.termination_to_date " & _
                    "		,RET.REfDt " & _
                    "		,RET.RET_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            RTD.employeeunkid AS REmpId " & _
                    "           ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                    "           ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                    "       FROM hremployee_dates_tran AS RTD " & _
                    "			LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                    "		WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS RET WHERE RET.Rno = 1 " & _
                    ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid = @payperiodunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = @currtranheadunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            'Sohail (21 Aug 2015) -- End

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= "           ) AS tblCurrGratuity ON tblBasic.payperiodunkid = tblCurrGratuity.payperiodunkid " & _
                                                          "AND tblBasic.employeeunkid = tblCurrGratuity.employeeunkid "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim strPrevGroup As String = ""
            Dim decMonthGratuityTotal As Decimal = 0
            Dim decGratuityTotal As Decimal = 0
            Dim decMonthGratuitySubTotal As Decimal = 0
            Dim decGratuitySubTotal As Decimal = 0
            Dim strPrevPeriod As String = ""
            Dim dtAppointment As Date
            Dim dtTerminationFrom As Date
            Dim dtTerminationTo As Date
            Dim dtEmplEnd As Date
            Dim intYearOfService As Integer = 0
            Dim intMonthsOfService As Integer = 0
            Dim intDaysOfService As Integer = 0

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("GName")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employeename")
                rpt_Row.Item("Column4") = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString).ToShortDateString
                rpt_Row.Item("Column5") = Format(dtRow.Item("basicsalary"), mstrfmtCurrency)
                rpt_Row.Item("Column6") = Format(dtRow.Item("currentgratuity"), mstrfmtCurrency)
                rpt_Row.Item("Column7") = Format(dtRow.Item("gratuity"), mstrfmtCurrency)

                dtAppointment = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If IsDBNull(dtRow.Item("termination_from_date")) = True Then
                If IsDBNull(dtRow.Item("termination_from_date")) = True OrElse dtRow.Item("termination_from_date").ToString.Trim = "" Then
                    'Sohail (21 Aug 2015) -- End
                    dtTerminationFrom = Nothing
                Else
                    dtTerminationFrom = eZeeDate.convertDate(dtRow.Item("termination_from_date").ToString)
                End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If IsDBNull(dtRow.Item("termination_to_date")) = True Then
                If IsDBNull(dtRow.Item("termination_to_date")) = True OrElse dtRow.Item("termination_to_date").ToString.Trim = "" Then
                    'Sohail (21 Aug 2015) -- End
                    dtTerminationTo = Nothing
                Else
                    dtTerminationTo = eZeeDate.convertDate(dtRow.Item("termination_to_date").ToString)
                End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If IsDBNull(dtRow.Item("empl_enddate")) = True Then
                If IsDBNull(dtRow.Item("empl_enddate")) = True OrElse dtRow.Item("empl_enddate").ToString.Trim = "" Then
                    'Sohail (21 Aug 2015) -- End
                    dtEmplEnd = Nothing
                Else
                    dtEmplEnd = eZeeDate.convertDate(dtRow.Item("empl_enddate").ToString)
                End If

                Dim EndDate As Date = mdtPeriodEndDate
                If dtTerminationFrom.Date <> Nothing Then
                    EndDate = IIf(dtTerminationFrom.Date < mdtPeriodEndDate, dtTerminationFrom.Date, mdtPeriodEndDate)
                End If
                If dtTerminationTo.Date <> Nothing Then
                    EndDate = IIf(dtTerminationTo.Date < EndDate, dtTerminationTo.Date, EndDate)
                End If
                If dtEmplEnd.Date <> Nothing Then
                    EndDate = IIf(dtEmplEnd.Date < EndDate, dtEmplEnd.Date, EndDate)
                End If

                intYearOfService = Math.Floor((CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000)
                intMonthsOfService = (Math.Floor(DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12) - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), CInt(IIf((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 <= 0, -11, 1)), 0))
                intDaysOfService = Math.Floor(DateDiff(DateInterval.Day, DateAdd(DateInterval.Month, ((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 - CInt(IIf(CInt(DatePart(DateInterval.Day, dtAppointment.Date)) > CInt(DatePart(DateInterval.Day, EndDate.AddDays(1))), CInt(IIf((DateDiff(DateInterval.Month, dtAppointment.Date, EndDate.AddDays(1)) + 12) Mod 12 <= 0, -11, 1)), 0))), DateAdd(DateInterval.Year, (CInt(eZeeDate.convertDate(EndDate.AddDays(1))) - CInt(eZeeDate.convertDate(dtAppointment.Date))) / 10000, dtAppointment.Date)), EndDate.AddDays(1)))

                rpt_Row.Item("Column10") = intYearOfService
                rpt_Row.Item("Column11") = intMonthsOfService
                rpt_Row.Item("Column12") = intDaysOfService

                If mintViewIndex > 0 Then
                    If strPrevGroup <> dtRow.Item("GName").ToString Then
                        decMonthGratuitySubTotal = CDec(Format(dtRow.Item("currentgratuity"), mstrfmtCurrency))
                        decGratuitySubTotal = CDec(Format(dtRow.Item("gratuity"), mstrfmtCurrency))
                    Else
                        decMonthGratuitySubTotal += CDec(Format(dtRow.Item("currentgratuity"), mstrfmtCurrency))
                        decGratuitySubTotal += CDec(Format(dtRow.Item("gratuity"), mstrfmtCurrency))
                    End If
                    strPrevGroup = dtRow.Item("GName").ToString
                End If

                decMonthGratuityTotal += CDec(Format(dtRow.Item("currentgratuity"), mstrfmtCurrency))
                decGratuityTotal += CDec(Format(dtRow.Item("gratuity"), mstrfmtCurrency))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

            Next


            objRpt = New ArutiReport.Designer.rptGratuity


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 25, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUsername)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 5, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 6, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 8, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 22, "Emp No."))
            Call ReportFunction.TextChange(objRpt, "txtSurName", Language.getMessage(mstrModuleName, 10, "Emp Name"))
            Call ReportFunction.TextChange(objRpt, "txtAppointed", Language.getMessage(mstrModuleName, 11, "Appointed Date"))
            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 32, "Year"))
            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 28, "Month"))
            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 29, "Days"))
            Call ReportFunction.TextChange(objRpt, "txtBasic", Language.getMessage(mstrModuleName, 30, "Basic Salary"))
            Call ReportFunction.TextChange(objRpt, "txtCurrGratuity", Language.getMessage(mstrModuleName, 31, "Curr. Month Gratuity"))
            Call ReportFunction.TextChange(objRpt, "txtGratuity", Language.getMessage(mstrModuleName, 34, "Gratuity"))


            Call ReportFunction.TextChange(objRpt, "txtTotalMonthGratuity", Format(decMonthGratuityTotal, mstrfmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotalGratuity", Format(decGratuityTotal, mstrfmtCurrency))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 17, "Page :"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 18, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 19, "Grand Total :"))
            Call ReportFunction.TextChange(objRpt, "txtSubCount", Language.getMessage(mstrModuleName, 33, "Sub Count :"))
            Call ReportFunction.TextChange(objRpt, "txtTotalCount", Language.getMessage(mstrModuleName, 21, "Total Count :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)


            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables(0)

                Dim mintColumn As Integer = 0

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 22, "Emp No.")
                mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 10, "Emp Name")
                mdtTableExcel.Columns("Column3").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 11, "Appointed Date")
                mdtTableExcel.Columns("Column4").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 32, "Year")
                mdtTableExcel.Columns("Column5").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 28, "Month")
                mdtTableExcel.Columns("Column81").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 29, "Days")
                mdtTableExcel.Columns("Column7").SetOrdinal(mintColumn)
                mintColumn += 1


                mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 30, "Basic Salary")
                mdtTableExcel.Columns("Column10").SetOrdinal(mintColumn)
                mintColumn += 1

                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns("Column1").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
                    mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
                    mintColumn += 1
                End If


                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next


            End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try

    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 3, "Gratuity Head :")
            Language.setMessage(mstrModuleName, 5, "Checked By :")
            Language.setMessage(mstrModuleName, 6, "Approved By :")
            Language.setMessage(mstrModuleName, 7, "Received By :")
            Language.setMessage(mstrModuleName, 8, "Received By :")
            Language.setMessage(mstrModuleName, 9, "Emp Code")
            Language.setMessage(mstrModuleName, 10, "Emp Name")
            Language.setMessage(mstrModuleName, 11, "Appointed Date")
            Language.setMessage(mstrModuleName, 13, "Basic Salary")
            Language.setMessage(mstrModuleName, 15, "Printed By :")
            Language.setMessage(mstrModuleName, 16, "Printed Date :")
            Language.setMessage(mstrModuleName, 17, "Page :")
            Language.setMessage(mstrModuleName, 18, "Sub Total :")
            Language.setMessage(mstrModuleName, 19, "Grand Total :")
            Language.setMessage(mstrModuleName, 20, "Sub Total:")
            Language.setMessage(mstrModuleName, 21, "Total Count :")
            Language.setMessage(mstrModuleName, 22, "Emp No.")
            Language.setMessage(mstrModuleName, 23, "Period :")
            Language.setMessage(mstrModuleName, 24, "Current Month Gratuity Head :")
            Language.setMessage(mstrModuleName, 25, "Prepared By :")
            Language.setMessage(mstrModuleName, 26, "Period Total:")
            Language.setMessage(mstrModuleName, 27, "Period Count:")
            Language.setMessage(mstrModuleName, 28, "Month")
            Language.setMessage(mstrModuleName, 29, "Days")
            Language.setMessage(mstrModuleName, 30, "Basic Salary")
            Language.setMessage(mstrModuleName, 31, "Curr. Month Gratuity")
            Language.setMessage(mstrModuleName, 32, "Year")
            Language.setMessage(mstrModuleName, 33, "Sub Count :")
            Language.setMessage(mstrModuleName, 34, "Gratuity")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
