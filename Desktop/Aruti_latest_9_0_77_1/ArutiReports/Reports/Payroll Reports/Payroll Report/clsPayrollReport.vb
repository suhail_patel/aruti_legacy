'************************************************************************************************************************************
'Class Name : clsCostCenterErDReport.vb
'Purpose    :
'Date       :17/11/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter


''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsPayrollReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPayrollReport"
    Private mstrReportId As String = enArutiReport.PayrollReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    'Sohail (05 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mstrPeriodIdList As String
    Private mintToPeriodId As Integer = -1
    Private mstrToPeriodName As String = ""
    'Sohail (05 Apr 2012) -- End

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""
    Dim dblColTot As Decimal()

    'Sandeep [ 09 MARCH 2011 ] -- Start
    Dim StrFinalPath As String = String.Empty
    'Sandeep [ 09 MARCH 2011 ] -- End 


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    'S.SANDEEP [ 17 AUG 2011 ] -- END 

    'Sohail (26 Nov 2011) -- Start
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (26 Nov 2011) -- End



    'Pinkal (05-Mar-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnIgnorezeroHeads As Boolean = False
    'Pinkal (05-Mar-2012) -- End

    'Sohail (16 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (16 Apr 2012) -- End

    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrCurrency_Sign As String = String.Empty
    Private mdecEx_Rate As Decimal = 0
    'S.SANDEEP [ 11 SEP 2012 ] -- END
    Private mstrCurrency_Rate As String = String.Empty 'Sohail (16 Mar 2013)
    Private mstrUnSelectedHeadIDs As String = String.Empty 'Sohail (29 Apr 2014)

    'Sohail (08 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String
    'Sohail (08 Dec 2012) -- End

    'Pinkal (14-Dec-2012) -- Start
    'Enhancement : TRA Changes
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    'Pinkal (14-Dec-2012) -- End


    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

    'Sohail (25 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrCurrentDatabaseName As String = FinancialYear._Object._DatabaseName
    Private mintBase_CurrencyId As Integer = ConfigParameter._Object._Base_CurrencyId
    Private mblnSetPayslipPaymentApproval As Boolean = ConfigParameter._Object._SetPayslipPaymentApproval
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    'Sohail (25 Mar 2013) -- End

    Private mstrUserAccessFilter As String = "" 'Sohail (31 Jan 2014)

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Sohail (20 Aug 2014) -- Start
    'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
    Private marrDatabaseName As New ArrayList
    Private mblnShowPaymentDetails As Boolean = False
    'Sohail (20 Aug 2014) -- End
    Private mblnIncludeEmployerContribution As Boolean = False 'Sohail (12 Sep 2014)

    'S.SANDEEP [ 29 SEP 2014 ] -- START
    Private mDicAllocationDetails As Dictionary(Of Integer, String)
    Private mstrCustomReportHeadsIds As String = String.Empty
    'S.SANDEEP [ 29 SEP 2014 ] -- END
    'Sohail (03 Feb 2016) -- Start
    'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
    Private mblnShowLoansInSeparateColumns As Boolean = False
    Private mblnShowSavingsInSeparateColumns As Boolean = False
    'Sohail (03 Feb 2016) -- End
    Private mblnApplyUserAccessFilter As Boolean = True 'Sohail (13 Feb 2016) 

    'Nilay (12-Oct-2016) -- Start
    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
    Private mstrCostCenterCode As String = String.Empty
    'Nilay (12-Oct-2016) -- End

    'Hemant (31 Aug 2018) -- Start
    'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
    Private mblnShowCRInSeparateColumns As Boolean = False
    'Hemant (31 Aug 2018)) -- End

    'Sohail (10 Jul 2019) -- Start
    'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
    Private mintMembershipUnkId As Integer = 0
    Private mstrMembershipName As String = String.Empty
    Private mblnShowEmpNameInSeprateColumns As Boolean = False
    'Sohail (10 Jul 2019) -- End

#End Region

#Region " Properties "

    'Sohail (05 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIdList() As String
        Set(ByVal value As String)
            mstrPeriodIdList = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property
    'Sohail (05 Apr 2012) -- End

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End


    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch_Name() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property
    'S.SANDEEP [ 17 AUG 2011 ] -- END 

    'Sohail (26 Nov 2011) -- Start
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (26 Nov 2011) -- End

    'Pinkal (05-Mar-2012) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _IgnoreZeroHeads() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnorezeroHeads = value
        End Set
    End Property
    'Pinkal (05-Mar-2012) -- End

    'Sohail (16 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (16 Apr 2012) -- End

    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Currency_Sign() As String
        Set(ByVal value As String)
            mstrCurrency_Sign = value
        End Set
    End Property

    Public WriteOnly Property _Ex_Rate() As Decimal
        Set(ByVal value As Decimal)
            mdecEx_Rate = value
        End Set
    End Property
    'S.SANDEEP [ 11 SEP 2012 ] -- END

    'Sohail (29 Apr 2014) -- Start
    'Enhancement - Export Excel Report with Company Logo 
    Public WriteOnly Property _UnSelectedHeadIDs() As String
        Set(ByVal value As String)
            mstrUnSelectedHeadIDs = value
        End Set
    End Property
    'Sohail (29 Apr 2014) -- End

    'Sohail (16 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Currency_Rate() As String
        Set(ByVal value As String)
            mstrCurrency_Rate = value
        End Set
    End Property
    'Sohail (16 Mar 2013) -- End

    'Sohail (08 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _FromDatabaseName() As String
        Set(ByVal value As String)
            mstrFromDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _ToDatabaseName() As String
        Set(ByVal value As String)
            mstrToDatabaseName = value
        End Set
    End Property
    'Sohail (08 Dec 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

    'Sohail (25 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _CurrentDatabaseName() As String
        Set(ByVal value As String)
            mstrCurrentDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _Base_CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBase_CurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _SetPayslipPaymentApproval() As Boolean
        Set(ByVal value As Boolean)
            mblnSetPayslipPaymentApproval = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property
    'Sohail (25 Mar 2013) -- End

    'Sohail (31 Jan 2014) -- Start
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'Sohail (31 Jan 2014) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Sohail (20 Aug 2014) -- Start
    'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _ShowPaymentDetails() As Boolean
        Set(ByVal value As Boolean)
            mblnShowPaymentDetails = value
        End Set
    End Property
    'Sohail (20 Aug 2014) -- End

    'Sohail (12 Sep 2014) -- Start
    'Enhancement - Include Employer Contribution on Payroll Report.
    Public WriteOnly Property _IncludeEmployerContribution() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeEmployerContribution = value
        End Set
    End Property
    'Sohail (12 Sep 2014) -- End

    'S.SANDEEP [ 29 SEP 2014 ] -- START
    Public WriteOnly Property _AllocationDetails() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mDicAllocationDetails = value
        End Set
    End Property

    Public WriteOnly Property _CustomReportHeadsIds() As String
        Set(ByVal value As String)
            mstrCustomReportHeadsIds = value
        End Set
    End Property
    'S.SANDEEP [ 29 SEP 2014 ] -- END

    'Sohail (03 Feb 2016) -- Start
    'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
    Public WriteOnly Property _ShowLoansInSeparateColumns() As Boolean
        Set(ByVal value As Boolean)
            mblnShowLoansInSeparateColumns = value
        End Set
    End Property

    Public WriteOnly Property _ShowSavingsInSeparateColumns() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSavingsInSeparateColumns = value
        End Set
    End Property
    'Sohail (03 Feb 2016) -- End

    'Hemant (31 Aug 2018) -- Start
    'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
    Public WriteOnly Property _ShowCRInSeparateColumns() As Boolean
        Set(ByVal value As Boolean)
            mblnShowCRInSeparateColumns = value
        End Set
    End Property
    'Hemant (31 Aug 2018)) -- End

    'Sohail (13 Feb 2016) -- Start
    'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property
    'Sohail ((13 Feb 2016) -- End

    'Nilay (12-Oct-2016) -- Start
    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
    Public WriteOnly Property _CostCenterCode() As String
        Set(ByVal value As String)
            mstrCostCenterCode = value
        End Set
    End Property
    'Nilay (12-Oct-2016) -- End

    'Sohail (10 Jul 2019) -- Start
    'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
    Public WriteOnly Property _MembershipUnkId() As Integer
        Set(ByVal value As Integer)
            mintMembershipUnkId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _ShowEmpNameInSeprateColumns() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmpNameInSeprateColumns = value
        End Set
    End Property
    'Sohail (10 Jul 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintPeriodId = -1
            mstrPeriodName = ""

            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            mintReportId = 0
            mstrReportTypeName = ""
            mstrPeriodIdList = ""
            'Sohail (05 Apr 2012) -- End


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End



            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            mintBranchId = -1
            mstrBranchName = ""
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (26 Nov 2011) -- Start
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (26 Nov 2011) -- End


            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes
            mblnIgnorezeroHeads = False
            'Pinkal (05-Mar-2012) -- End
            mstrUnSelectedHeadIDs = "" 'Sohail (29 Apr 2014)

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrCurrency_Sign = ""
            mdecEx_Rate = 0
            'S.SANDEEP [ 11 SEP 2012 ] -- END
            mstrCurrency_Rate = "" 'Sohail (16 Mar 2013)

            'Sohail (08 Dec 2012) -- Start
            'TRA - ENHANCEMENT
            mstrFromDatabaseName = FinancialYear._Object._DatabaseName
            mstrToDatabaseName = FinancialYear._Object._DatabaseName
            'Sohail (08 Dec 2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

            'Sohail (20 Aug 2014) -- Start
            'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
            marrDatabaseName.Clear()
            mblnShowPaymentDetails = False
            'Sohail (20 Aug 2014) -- End
            mblnIncludeEmployerContribution = False 'Sohail (12 Sep 2014)
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            mblnShowLoansInSeparateColumns = False
            mblnShowSavingsInSeparateColumns = False
            'Sohail (03 Feb 2016) -- End

            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            mblnShowCRInSeparateColumns = False
            'Hemant (31 Aug 2018)) -- End

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            mintMembershipUnkId = 0
            mstrMembershipName = ""
            mblnShowEmpNameInSeprateColumns = False
            'Sohail (10 Jul 2019) -- End

            mblnApplyUserAccessFilter = True 'Sohail (13 Feb 2016) 
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Period : ") & " " & mstrPeriodName & " "
            End If

            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mintToPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, " To ") & " " & mstrToPeriodName & " "
            End If
            'Sohail (05 Apr 2012) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Branch :") & " " & mstrBranchName & " "
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 


            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrCurrency_Sign.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Currency :") & " " & mstrCurrency_Sign & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Exchange Rate:") & " " & CDbl(mdecEx_Rate) & " "
            End If
            'S.SANDEEP [ 11 SEP 2012 ] -- END


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                'Sohail (26 Nov 2011) -- Start
                'Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY " & mstrAnalysis_OrderBy & ", " & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
                'Sohail (26 Nov 2011) -- End
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
        'Sohail (21 Aug 2015) -- End
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overloads Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        'Sohail (16 Jun 2012) -- Start
        'TRA - ENHANCEMENT
        Dim intColCount As Integer = objDataReader.Columns.Count
        Dim intRowCount As Integer = objDataReader.Rows.Count
        'Sohail (16 Jun 2012) -- End
        Try

            'HEADER PART
            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'strBuilder.Append(" <TITLE> " & Me._ReportName & " </TITLE> " & vbCrLf)
            strBuilder.Append(" <TITLE> " & mstrReportTypeName & " </TITLE> " & vbCrLf)
            'Sohail (05 Apr 2012) -- End
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If
                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 5, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='60%' colspan=15 align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='60%' colspan=15  align='center' > " & vbCrLf)
            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT

            'Sohail (05 Apr 2012) -- End
            'strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & mstrReportTypeName & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            Dim intZeroColumn As Integer = 0
            'Sohail (16 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            Dim arrZeroColIndx As New ArrayList
            'Sohail (16 Jun 2012) -- End
            'Sohail (05 Apr 2012) -- End

            'Report Column Caption
            'Sohail (16 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            For j As Integer = 0 To intColCount - 1
                'For j As Integer = 0 To objDataReader.Columns.Count - 1
                'Sohail (16 Jun 2012) -- End

                'Pinkal (05-Mar-2012) -- Start
                'Enhancement : TRA Changes

                If mblnIgnorezeroHeads Then

                    If objDataReader.Columns(j).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(j).ColumnName <> "EmpName" AndAlso objDataReader.Columns(j).ColumnName <> "GrpID" AndAlso objDataReader.Columns(j).ColumnName <> "FirstName" AndAlso objDataReader.Columns(j).ColumnName <> "OtherName" AndAlso objDataReader.Columns(j).ColumnName <> "Surname" AndAlso objDataReader.Columns(j).ColumnName <> "MembershipName" _
                           AndAlso objDataReader.Columns(j).ColumnName <> "GrpName" AndAlso objDataReader.Columns(j).ColumnName <> "TDD" AndAlso objDataReader.Columns(j).ColumnName <> "NetPay" Then
                        'Sohail (10 Jul 2019) - [FirstName, OtherName, Surname, MembershipName]

                        Dim drRow As DataRow() = Nothing
                        If objDataReader.Columns(j).DataType Is Type.GetType("System.String") Then
                            drRow = objDataReader.Select(objDataReader.Columns(j).ColumnName & " = '0' OR " & objDataReader.Columns(j).ColumnName & " = '0.00'")
                            If drRow.Length = objDataReader.Rows.Count Then
                                'Sohail (05 Apr 2012) -- Start
                                'TRA - ENHANCEMENT
                                intZeroColumn += 1
                                'Sohail (16 Jun 2012) -- Start
                                'TRA - ENHANCEMENT
                                arrZeroColIndx.Add(j)
                                'Sohail (16 Jun 2012) -- End
                                'Sohail (05 Apr 2012) -- End
                                Continue For
                            End If
                        End If

                    End If
                End If

                'Pinkal (05-Mar-2012) -- End

                'Sohail (26 Nov 2011) -- Start

                'Sohail (05 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'If j = 2 OrElse j = 3 Then
                '    '*** Do Nothing (GroupID and GroupName column)
                '    'strBuilder.Append("<TD BORDER=1 WIDTH='0px' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                'Else
                '    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                'End If
                If j = 4 OrElse j = 5 Then
                    '*** Do Nothing (GroupID and GroupName column)
                Else
                    If mintReportId = 0 Then 'Payroll Report
                        If j = 0 OrElse j = 1 Then
                            'Period Id, Period Name
                        Else
                            strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                        End If
                    ElseIf mintReportId = 1 Then 'Personal Salary Calculation
                        If j = 0 OrElse j = 2 OrElse j = 3 Then
                            'Period Id, Emp code, Emp Name
                        Else
                            strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                        End If
                    End If
                End If
                'Sohail (05 Apr 2012) -- End
            Next

            strBuilder.Append(" </TR> " & vbCrLf)


            'Data Part
            'Sohail (26 Nov 2011) -- Start
            Dim strPrevGrpID As String = ""
            'Sohail (16 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim GroupArrayTotal(objDataReader.Columns.Count - 5) As Decimal
            Dim GroupArrayTotal(intColCount - 5) As Decimal
            'Sohail (16 Jun 2012) -- End
            'Sohail (26 Nov 2011) -- End
            'Sohail (16 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            For i As Integer = 0 To intRowCount - 1
                'For i As Integer = 0 To objDataReader.Rows.Count - 1
                'Sohail (16 Jun 2012) -- End

                'Sohail (26 Nov 2011) -- Start
                If strPrevGrpID <> objDataReader.Rows(i)("GrpId").ToString Then

                    If i > 0 Then
                        '**** Sub Total ****
                        strBuilder.Append(" <TR> " & vbCrLf)
                        'Sohail (16 Jun 2012) -- Start
                        'TRA - ENHANCEMENT
                        For p As Integer = 0 To intColCount - 1
                            'For p As Integer = 0 To objDataReader.Columns.Count - 1
                            'Sohail (16 Jun 2012) -- End


                            'Pinkal (05-Mar-2012) -- Start
                            'Enhancement : TRA Changes

                            If mblnIgnorezeroHeads Then

                                'Sohail (16 Jun 2012) -- Start
                                'TRA - ENHANCEMENT
                                'If objDataReader.Columns(p).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(p).ColumnName <> "EmpName" AndAlso objDataReader.Columns(p).ColumnName <> "GrpID" _
                                '       AndAlso objDataReader.Columns(p).ColumnName <> "GrpName" AndAlso objDataReader.Columns(p).ColumnName <> "TDD" AndAlso objDataReader.Columns(p).ColumnName <> "NetPay" Then

                                '    Dim drRow As DataRow() = Nothing
                                '    If objDataReader.Columns(p).DataType Is Type.GetType("System.String") Then
                                '        drRow = objDataReader.Select(objDataReader.Columns(p).ColumnName & " = '0' OR " & objDataReader.Columns(p).ColumnName & " = '0.00'")
                                '        If drRow.Length = objDataReader.Rows.Count Then
                                '            Continue For
                                '        End If
                                '    End If

                                'End If
                                If arrZeroColIndx.Contains(p) = True Then
                                    Continue For
                                End If
                            End If

                            'Pinkal (05-Mar-2012) -- End

                            Select Case p
                                Case 0
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 3, "Sub Total :") & "</B></FONT></TD>" & vbCrLf)
                                Case 1
                                    'Sohail (05 Apr 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>" & vbCrLf)
                                    If mintReportId = 0 Then 'Payroll Report

                                        'S.SANDEEP [ 18 SEP 2012 ] -- START
                                        'ENHANCEMENT : TRA CHANGES
                                        'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>" & vbCrLf)
                                        Dim iCnt As Integer = objDataReader.Compute("COUNT(EmpCode)", "GrpID = '" & strPrevGrpID & "'")
                                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><b>" & iCnt.ToString & "</b></FONT></TD>" & vbCrLf)
                                        'S.SANDEEP [ 18 SEP 2012 ] -- END


                                    End If
                                    'Sohail (05 Apr 2012) -- End

                                    'Sohail (05 Apr 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    'Case 2 To 3
                                Case 2 To 5
                                    'Sohail (05 Apr 2012) -- End
                                    '*** Do Nothing (GroupID and GroupName column)
                                Case Else
                                    'Sohail (05 Apr 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(GroupArrayTotal(p - 4)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(GroupArrayTotal(p - 6)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                                    'Sohail (05 Apr 2012) -- End
                            End Select
                        Next
                        strBuilder.Append(" </TR> " & vbCrLf)
                        'Sohail (05 Apr 2012) -- Start
                        'TRA - ENHANCEMENT
                        'ReDim GroupArrayTotal(objDataReader.Columns.Count - 5) 
                        'Sohail (16 Jun 2012) -- Start
                        'TRA - ENHANCEMENT
                        'ReDim GroupArrayTotal(objDataReader.Columns.Count - 7)
                        ReDim GroupArrayTotal(intColCount - 7)
                        'Sohail (16 Jun 2012) -- End
                        'Sohail (05 Apr 2012) -- End
                    End If

                    '**** Group Header ****
                    strBuilder.Append(" <TR> " & vbCrLf)
                    'Sohail (05 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'strBuilder.Append("<TD colspan=" & objDataReader.Columns.Count - 2 & " BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & mstrReport_GroupName & " " & objDataReader.Rows(i)("GrpName") & "</B></FONT></TD>" & vbCrLf)
                    'Sohail (05 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    If mintReportId = 1 Then 'Personal Salary Calculation
                        strBuilder.Append("<TD></TD>" & vbCrLf)
                    End If
                    'Sohail (16 Jun 2012) -- Start
                    'TRA - ENHANCEMENT
                    'strBuilder.Append("<TD colspan=" & objDataReader.Columns.Count - 6 & " BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & mstrReport_GroupName & " " & objDataReader.Rows(i)("GrpName") & "</B></FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD colspan=" & intColCount - 6 & " BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & mstrReport_GroupName & " " & objDataReader.Rows(i)("GrpName") & "</B></FONT></TD>" & vbCrLf)
                    'Sohail (16 Jun 2012) -- End
                    'Sohail (05 Apr 2012) -- End
                    'Sohail (05 Apr 2012) -- End
                    strBuilder.Append(" </TR> " & vbCrLf)
                End If
                'Sohail (26 Nov 2011) -- End

                strBuilder.Append(" <TR> " & vbCrLf)
                'Sohail (16 Jun 2012) -- Start
                'TRA - ENHANCEMENT
                For k As Integer = 0 To intColCount - 1
                    'For k As Integer = 0 To objDataReader.Columns.Count - 1
                    'Sohail (16 Jun 2012) -- End
                    'S.SANDEEP [ 17 AUG 2011 ] -- START
                    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                    'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)

                    'Pinkal (05-Mar-2012) -- Start
                    'Enhancement : TRA Changes

                    If mblnIgnorezeroHeads Then

                        'Sohail (16 Jun 2012) -- Start
                        'TRA - ENHANCEMENT
                        'If objDataReader.Columns(k).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(k).ColumnName <> "EmpName" AndAlso objDataReader.Columns(k).ColumnName <> "GrpID" _
                        '                               AndAlso objDataReader.Columns(k).ColumnName <> "GrpName" AndAlso objDataReader.Columns(k).ColumnName <> "TDD" AndAlso objDataReader.Columns(k).ColumnName <> "NetPay" Then

                        '    Dim drRow As DataRow() = Nothing
                        '    If objDataReader.Columns(k).DataType Is Type.GetType("System.String") Then
                        '        drRow = objDataReader.Select(objDataReader.Columns(k).ColumnName & " = '0' OR " & objDataReader.Columns(k).ColumnName & " = '0.00'")
                        '        If drRow.Length = objDataReader.Rows.Count Then
                        '            Continue For
                        '        End If
                        '    End If

                        'End If
                        If arrZeroColIndx.Contains(k) = True Then
                            Continue For
                        End If
                        'Sohail (16 Jun 2012) -- End
                    End If

                    'Pinkal (05-Mar-2012) -- End

                    Select Case k
                        'Sohail (05 Apr 2012) -- Start
                        'TRA - ENHANCEMENT
                        Case 0 'Period Id
                            'Nothing
                        Case 1 'Period Name
                            If mintReportId = 1 Then 'Personal Salary Calculation
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><b> &nbsp;" & objDataReader.Rows(i)(k) & "</b></FONT></TD>" & vbCrLf)
                            End If
                        Case 2 To 3 'Emp Code , Emp Name
                            If mintReportId = 0 Then 'Payroll Report
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                            End If
                        Case 4 To 5 'GrpID, GrpName
                            '*** Do Nothing (GroupID and GroupName column)

                            'Case 0, 1
                            '    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                            '    'Sohail (26 Nov 2011) -- Start 
                            'Case 2 To 3
                            '    '*** Do Nothing (GroupID and GroupName column)
                            '    'Sohail (26 Nov 2011) -- End
                            'Sohail (05 Apr 2012) -- End
                        Case Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2> &nbsp;" & Format(CDec(objDataReader.Rows(i)(k)), GUI.fmtCurrency) & "</FONT></TD>" & vbCrLf)

                    End Select
                    'S.SANDEEP [ 17 AUG 2011 ] -- END 

                    'Sohail (26 Nov 2011) -- Start
                    'Sohail (05 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'If mintViewIndex > 0 AndAlso k > 3 Then 'i > 0 AndAlso 
                    '    GroupArrayTotal(k - 4) += CDec(objDataReader.Rows(i)(k))
                    'End If
                    If mintViewIndex > 0 AndAlso k > 5 Then 'i > 0 AndAlso 'k > 3
                        GroupArrayTotal(k - 6) += CDec(objDataReader.Rows(i)(k))
                    End If
                    'Sohail (05 Apr 2012) -- End
                    'Sohail (26 Nov 2011) -- End
                Next
                strBuilder.Append(" </TR> " & vbCrLf)

                strPrevGrpID = objDataReader.Rows(i)("GrpId").ToString 'Sohail (26 Nov 2011)
            Next

            'Sohail (26 Nov 2011) -- Start
            If mintViewIndex > 0 Then
                '**** Sub Total for Last Group ****
                strBuilder.Append(" <TR> " & vbCrLf)
                'Sohail (16 Jun 2012) -- Start
                'TRA - ENHANCEMENT
                For p As Integer = 0 To intColCount - 1
                    'For p As Integer = 0 To objDataReader.Columns.Count - 1
                    'Sohail (16 Jun 2012) -- End

                    'Pinkal (05-Mar-2012) -- Start
                    'Enhancement : TRA Changes

                    If mblnIgnorezeroHeads Then

                        'Sohail (16 Jun 2012) -- Start
                        'TRA - ENHANCEMENT
                        'If objDataReader.Columns(p).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(p).ColumnName <> "EmpName" AndAlso objDataReader.Columns(p).ColumnName <> "GrpID" _
                        '                               AndAlso objDataReader.Columns(p).ColumnName <> "GrpName" AndAlso objDataReader.Columns(p).ColumnName <> "TDD" AndAlso objDataReader.Columns(p).ColumnName <> "NetPay" Then

                        '    Dim drRow As DataRow() = Nothing
                        '    If objDataReader.Columns(p).DataType Is Type.GetType("System.String") Then
                        '        drRow = objDataReader.Select(objDataReader.Columns(p).ColumnName & " = '0' OR " & objDataReader.Columns(p).ColumnName & " = '0.00'")
                        '        If drRow.Length = objDataReader.Rows.Count Then
                        '            Continue For
                        '        End If
                        '    End If

                        'End If
                        If arrZeroColIndx.Contains(p) = True Then
                            Continue For
                        End If
                        'Sohail (16 Jun 2012) -- End
                    End If

                    'Pinkal (05-Mar-2012) -- End

                    Select Case p
                        Case 0
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 3, "Sub Total :") & "</B></FONT></TD>" & vbCrLf)
                        Case 1
                            'Sohail (05 Apr 2012) -- Start
                            'TRA - ENHANCEMENT
                            'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>" & vbCrLf)
                            If mintReportId = 0 Then 'Payroll Report
                                'S.SANDEEP [ 18 SEP 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>" & vbCrLf)
                                Dim iCnt As Integer = objDataReader.Compute("COUNT(EmpCode)", "GrpID = '" & strPrevGrpID & "'")
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><b>" & iCnt.ToString & "</b></FONT></TD>" & vbCrLf)
                                'S.SANDEEP [ 18 SEP 2012 ] -- END
                            End If
                            'Sohail (05 Apr 2012) -- End
                            'Sohail (05 Apr 2012) -- Start
                            'TRA - ENHANCEMENT
                            'Case 2 To 3
                        Case 2 To 5
                            'Sohail (05 Apr 2012) -- End
                            '*** Do Nothing (GroupID and GroupName column)
                        Case Else
                            'Sohail (05 Apr 2012) -- Start
                            'TRA - ENHANCEMENT
                            'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(GroupArrayTotal(p - 4)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(GroupArrayTotal(p - 6)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                            'Sohail (05 Apr 2012) -- End
                    End Select
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            End If
            'Sohail (26 Nov 2011) -- End

            strBuilder.Append(" <TR> " & vbCrLf)
            'Sohail (16 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            For k As Integer = 0 To intColCount - 1
                'For k As Integer = 0 To objDataReader.Columns.Count - 1
                'Sohail (16 Jun 2012) -- End
                'Sohail (26 Nov 2011) -- Start
                'If k = 0 Then
                '    'S.SANDEEP [ 17 AUG 2011 ] -- START
                '    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                '    'strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
                '    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
                '    'S.SANDEEP [ 17 AUG 2011 ] -- END 
                'ElseIf k <= 1 Then
                '    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & "" & "</B></FONT></TD>" & vbCrLf)
                'Else
                '    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(dblColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                'End If

                'Pinkal (05-Mar-2012) -- Start
                'Enhancement : TRA Changes

                If mblnIgnorezeroHeads Then

                    'Sohail (16 Jun 2012) -- Start
                    'TRA - ENHANCEMENT
                    'If objDataReader.Columns(k).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(k).ColumnName <> "EmpName" AndAlso objDataReader.Columns(k).ColumnName <> "GrpID" _
                    '                           AndAlso objDataReader.Columns(k).ColumnName <> "GrpName" AndAlso objDataReader.Columns(k).ColumnName <> "TDD" AndAlso objDataReader.Columns(k).ColumnName <> "NetPay" Then

                    '    Dim drRow As DataRow() = Nothing
                    '    If objDataReader.Columns(k).DataType Is Type.GetType("System.String") Then
                    '        drRow = objDataReader.Select(objDataReader.Columns(k).ColumnName & " = '0' OR " & objDataReader.Columns(k).ColumnName & " = '0.00'")
                    '        If drRow.Length = objDataReader.Rows.Count Then
                    '            Continue For
                    '        End If
                    '    End If

                    'End If
                    If arrZeroColIndx.Contains(k) = True Then
                        Continue For
                    End If
                    'Sohail (16 Jun 2012) -- End
                End If

                'Pinkal (05-Mar-2012) -- End

                Select Case k
                    Case 0
                        strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 4, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
                    Case 1
                        'Sohail (05 Apr 2012) -- Start
                        'TRA - ENHANCEMENT
                        'strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & "" & "</B></FONT></TD>" & vbCrLf)
                        If mintReportId = 0 Then 'Payroll Report
                            strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & intRowCount & "</B></FONT></TD>" & vbCrLf)
                        End If
                        'Sohail (05 Apr 2012) -- End
                        'Sohail (05 Apr 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Case 2 To 3
                    Case 2 To 5
                        'Sohail (05 Apr 2012) -- End
                        '*** Do Nothing (GroupID and GroupName column)
                    Case Else
                        strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(dblColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                End Select
                'Sohail (26 Nov 2011) -- End
            Next
            strBuilder.Append(" </TR>  " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)


            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT 
            'Sohail (16 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            'strBuilder = New StringBuilder(strBuilder.ToString.Replace("colspan=" & objDataReader.Columns.Count - 6 & "", "colspan=" & objDataReader.Columns.Count - 6 - intZeroColumn & ""))
            strBuilder = New StringBuilder(strBuilder.ToString.Replace("colspan=" & intColCount - 6 & "", "colspan=" & intColCount - 6 - intZeroColumn & ""))
            'Sohail (16 Jun 2012) -- End
            'Sohail (05 Apr 2012) -- End

            'Sandeep [ 09 MARCH 2011 ] -- Start
            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            Else

                'Anjan (14 Apr 2011)-Start
                'Issue : To remove "\" from path which comes at end in path.
                If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                    SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
                End If
                'Anjan (14 Apr 2011)-End

            End If
            'Sandeep [ 09 MARCH 2011 ] -- End 

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                'Sandeep [ 09 MARCH 2011 ] -- Start
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                'Sandeep [ 09 MARCH 2011 ] -- End 
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try

            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function


    'Pinkal (14-Dec-2012) -- Start
    'Enhancement : TRA Changes

    Private Function IgnoreZeroHead(ByVal objDataReader As DataTable) As DataTable
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Dim intColCount As Integer = objDataReader.Columns.Count
        Dim intRowCount As Integer = objDataReader.Rows.Count
        Try

            Dim intZeroColumn As Integer = 0
            Dim arrZeroColIndx As New ArrayList

            'Report Column Caption
SetColumnCount:
            intColCount = objDataReader.Columns.Count
            For j As Integer = 0 To intColCount - 1
                If mblnIgnorezeroHeads Then

                    If objDataReader.Columns(j).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(j).ColumnName <> "EmpName" AndAlso objDataReader.Columns(j).ColumnName <> "GrpID" AndAlso objDataReader.Columns(j).ColumnName <> "FirstName" AndAlso objDataReader.Columns(j).ColumnName <> "OtherName" AndAlso objDataReader.Columns(j).ColumnName <> "Surname" AndAlso objDataReader.Columns(j).ColumnName <> "MembershipName" _
                           AndAlso objDataReader.Columns(j).ColumnName <> "GrpName" AndAlso objDataReader.Columns(j).ColumnName <> "TDD" AndAlso objDataReader.Columns(j).ColumnName <> "NetPay" Then
                        'Sohail (10 Jul 2019) - [FirstName, OtherName, Surname, MembershipName]

                        Dim drRow As DataRow() = Nothing
                        If objDataReader.Columns(j).DataType Is Type.GetType("System.Decimal") Then
                            drRow = objDataReader.Select(objDataReader.Columns(j).ColumnName & " = 0 OR " & objDataReader.Columns(j).ColumnName & " = 0.00")
                            If drRow.Length = objDataReader.Rows.Count Then
                                objDataReader.Columns.RemoveAt(j)
                                GoTo SetColumnCount
                            End If
                        End If

                    End If
                End If

            Next

            'Sohail (12 Apr 2016) -- Start
            'Enhancement - IPNI - 58.1 - Don't show employee if he has all heads with Zero amount and if Ignore Zero Heads ticked on Payroll Report.
            Dim decCols() As String = (From p In objDataReader.Columns.Cast(Of DataColumn)() Where (p.DataType Is Type.GetType("System.Decimal")) Select (p.ColumnName.ToString & " = 0 AND ")).ToArray
            If decCols.Length > 0 Then
                Dim s As String = String.Join("", decCols)
                Dim r() As DataRow = objDataReader.Select(s.Substring(0, s.Length - 4))
                For Each dr As DataRow In r
                    objDataReader.Rows.Remove(dr)
                Next
            End If
            'Sohail (12 Apr 2016) -- End

            Return objDataReader

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IgnoreZeroHead; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (14-Dec-2012) -- End


    'S.SANDEEP [ 29 SEP 2014 ] -- START
    Public Function GetAllocationList(Optional ByVal xFilter As String = "") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation
            StrQ = " SELECT " & _
                   "     Id " & _
                   "    ,Name " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT 1 AS Id, @BRANCH AS Name " & _
                   "    UNION SELECT 2 AS Id,  @DEPARTMENT_GROUP AS Name " & _
                   "    UNION SELECT 3 AS Id,  @DEPARTMENT AS Name " & _
                   "    UNION SELECT 4 AS Id,  @SECTION_GROUP AS Name " & _
                   "    UNION SELECT 5 AS Id,  @SECTION AS Name " & _
                   "    UNION SELECT 6 AS Id,  @UNIT_GROUP AS Name " & _
                   "    UNION SELECT 7 AS Id,  @UNIT AS Name " & _
                   "    UNION SELECT 8 AS Id,  @TEAM AS Name " & _
                   "    UNION SELECT 9 AS Id,  @JOB_GROUP AS Name " & _
                   "    UNION SELECT 10 AS Id, @JOBS AS Name " & _
                   "    UNION SELECT 11 AS Id, @CLASS_GROUP AS Name " & _
                   "    UNION SELECT 12 AS Id, @CLASSES AS Name " & _
                   "    UNION SELECT 13 AS Id, @Appointmentdate AS Name " & _
                   "    UNION SELECT 14 AS Id, @Birthdate AS Name " & _
                   "    UNION SELECT 15 AS Id, @Confirmationdate AS Name " & _
                   "    UNION SELECT 16 AS Id, @Reitrementdate AS Name " & _
                   "    UNION SELECT 17 AS Id, @EOCdate AS Name " & _
                   "    UNION SELECT 18 AS Id, @Costcenter AS Name " & _
                   "    UNION SELECT 19 AS Id, @Gradegroup AS Name " & _
                   "    UNION SELECT 20 AS Id, @Grades AS Name " & _
                   "    UNION SELECT 21 AS Id, @Gradelevel AS Name " & _
                   "    UNION SELECT 22 AS Id, @Employmenttype AS Name " & _
                   "    UNION SELECT 23 AS Id, @Gender AS Name " & _
                   "    UNION SELECT 24 AS Id, @MaritalStatus AS Name " & _
                   "    UNION SELECT 25 AS Id, @CostcenterCode AS Name " & _
                   "    UNION SELECT 26 AS Id, @CostcenterCustomCode AS Name " & _
                   ") AS A WHERE 1 = 1 "
            'Sohail (03 Jan 2020) - [CostcenterCode, CostcenterCustomCode]

            If xFilter.Trim.Length > 0 Then
                StrQ &= "AND " & xFilter
            End If

            objData.AddParameter("@BRANCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 53, "Branch"))
            objData.AddParameter("@DEPARTMENT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 54, "Department Group"))
            objData.AddParameter("@DEPARTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 55, "Department"))
            objData.AddParameter("@SECTION_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 56, "Section Group"))
            objData.AddParameter("@SECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 57, "Section"))
            objData.AddParameter("@UNIT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 58, "Unit Group"))
            objData.AddParameter("@UNIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 59, "Unit"))
            objData.AddParameter("@TEAM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 60, "Team"))
            objData.AddParameter("@JOB_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 61, "Job Group"))
            objData.AddParameter("@JOBS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 62, "Jobs"))
            objData.AddParameter("@CLASS_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 63, "Class Group"))
            objData.AddParameter("@CLASSES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 64, "Classes"))
            objData.AddParameter("@Appointmentdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "Appointment Date"))
            objData.AddParameter("@Birthdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "Birthdate"))
            objData.AddParameter("@Confirmationdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "Confirmation Date"))
            objData.AddParameter("@Reitrementdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 28, "Retirement Date"))
            objData.AddParameter("@EOCdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 29, "End Of Contract Date"))
            objData.AddParameter("@Costcenter", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 30, "CostCenter"))
            objData.AddParameter("@Gradegroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "Grade Group"))
            objData.AddParameter("@Grades", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "Grades"))
            objData.AddParameter("@Gradelevel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 33, "Grade Level"))
            objData.AddParameter("@Employmenttype", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 34, "Employment Type"))
            objData.AddParameter("@Gender", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 35, "Gender"))
            objData.AddParameter("@MaritalStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 36, "Marital Status"))
            'Sohail (03 Jan 2020) -- Start
            'NMB Enhancement # : Option for Cost centre code column on custom payroll report.
            objData.AddParameter("@CostcenterCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 72, "Cost Center Code"))
            objData.AddParameter("@CostcenterCustomCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 73, "Cost Center Custom Code"))
            'Sohail (03 Jan 2020) -- End

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAllocationList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'S.SANDEEP [ 29 SEP 2014 ] -- END

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'Sohail (07 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 18, "Employee Code")))

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ')", Language.getMessage(mstrModuleName, 34, "Employee Name")))
            If mblnFirstNamethenSurname = True Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ')", Language.getMessage(mstrModuleName, 19, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '')+ ' ' + ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 19, "Employee Name")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END


            'Sohail (07 Aug 2013) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Sandeep (29 Nov 2010)-Start
    'Issue : This code is for dynamic columns . do not delete
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass

    '    Call Generate_PayrollReport()

    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim dsHeadList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim strTranHead As String = String.Empty
    '    Dim strBottom As String = String.Empty
    '    Dim intRptObjCnt As Integer = 0
    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ = "DECLARE @tranname VARCHAR(MAX)   " & _
    '        "SELECT @tranname = COALESCE(@tranname + ',' + prtranhead_master.trnheadname, " & _
    '        " prtranhead_master.trnheadname ) " & _
    '        "FROM prtranhead_master " & _
    '        "WHERE   prtranhead_master.isvoid = 0   " & _
    '        "AND prtranhead_master.tranheadunkid IN (" & mstrTranHeadIds & ")" & _
    '        "SELECT @tranname AS TranHead "

    '        dsHeadList = objDataOperation.ExecQuery(StrQ, "Lst")

    '        If dsHeadList.Tables(0).Rows.Count > 0 Then
    '            strTranHead = dsHeadList.Tables(0).Rows(0)("TranHead")
    '        Else
    '            strTranHead = String.Empty
    '        End If

    '        strSepTranHead = strTranHead.Split(",")

    '        StrQ = "DECLARE @query VARCHAR(MAX)  " & _
    '               "SET @query = N'SELECT PeriodName, EmpCode, EmpName "


    '        For Each strTH As String In strTranHead.Split(",")
    '            strBottom &= ", [" & strTH & "]"
    '            StrQ &= ", ISNULL([" & strTH & "],0) As [" & strTH & "]"
    '        Next

    '        StrQ &= " FROM (SELECT  TH.PeriodId   " & _
    '                "			 , TH.PeriodName   " & _
    '                "			 , CC.EmpId   " & _
    '                "			 , CC.EmpCode   " & _
    '                "			 , CC.EmpName   " & _
    '                "			 , TH.TranHeadName   " & _
    '                "			 , TH.Amount   " & _
    '                "	   FROM    ( SELECT hremployee_master.employeeunkid AS EmpId    " & _
    '                "					  , hremployee_master.employeecode AS EmpCode    " & _
    '                "					  , (ISNULL(hremployee_master.firstname,SPACE(2))+ SPACE(2) +ISNULL(hremployee_master.othername,SPACE(2))+SPACE(2) +ISNULL(hremployee_master.surname,SPACE(2))) AS EmpName    " & _
    '                "				 FROM hremployee_master    " & _
    '                "				 WHERE hremployee_master.isactive = 1   " & _
    '                "				) AS CC   " & _
    '                "			JOIN ( SELECT     PeriodId    " & _
    '                "							, PeriodName    " & _
    '                "							, EmpId    " & _
    '                "							, TranHeadName   " & _
    '                "							, ISNULL(SUM(Amount),0) AS Amount   " & _
    '                "					FROM      ( SELECT    cfcommon_period_tran.periodunkid AS PeriodId   " & _
    '                "										, cfcommon_period_tran.period_name AS PeriodName   " & _
    '                "										, hremployee_master.employeeunkid AS EmpId   " & _
    '                "										, prtranhead_master.trnheadname AS TranHeadName   " & _
    '                "										, ISNULL(prpayrollprocess_tran.amount, 0) AS Amount   " & _
    '                "								FROM      prpayrollprocess_tran   " & _
    '                "								LEFT JOIN hremployee_master  ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid   " & _
    '                "								LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid   " & _
    '                "								LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid   " & _
    '                "								LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid   " & _
    '                "								WHERE     hremployee_master.isactive = 1   " & _
    '                "										AND prpayrollprocess_tran.isvoid = 0   " & _
    '                "										AND prpayrollprocess_tran.tranheadunkid <> -1   " & _
    '                "							) AS CCED1   " & _
    '                "							GROUP BY  PeriodId  , PeriodName , EmpId , TranHeadName   " & _
    '                "				) AS TH   ON  CC.EmpId = TH.EmpId   " & _
    '                "		) CCED   " & _
    '                "		Pivot   " & _
    '                "		( SUM([Amount]) FOR TranHeadName IN (" & Mid(strBottom, 2) & " )) As Pvt    "

    '        If mintEmployeeId > 0 Then
    '            StrQ &= "       WHERE  EmpId = " & mintEmployeeId
    '        End If

    '        StrQ &= "	ORDER BY PeriodId; '  " & _
    '                "EXECUTE(@query)  "


    '        Call FilterTitleAndFilterQuery()


    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        objRpt = New ArutiReport.Designer.rptPayroll

    '        For j As Integer = 0 To objRpt.ReportDefinition.ReportObjects().Count - 1
    '            If objRpt.ReportDefinition.ReportObjects(j).Name.Contains("txtHead") Then
    '                intRptObjCnt += 1
    '            End If
    '        Next

    '        If mintTranHeadCount > intRptObjCnt Then
    '            If ConfigParameter._Object._ExportReportPath = "" Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please set the export report Path."), enMsgBoxStyle.Information)
    '                Exit Try
    '            End If

    '            Dim dblTotal As Double = 0
    '            Dim dblColTot As Double()
    '            Dim m As Integer = 3
    '            dblColTot = New Double(dsList.Tables(0).Columns.Count + 1) {}

    '            While m < dsList.Tables(0).Columns.Count
    '                For t As Integer = 0 To dsList.Tables(0).Rows.Count - 1
    '                    dblTotal += cdec(dsList.Tables(0).Rows(t)(m))
    '                Next
    '                dblColTot(m) = dblTotal
    '                dblTotal = 0
    '                m += 1
    '            End While


    '            Dim strHTML As String = String.Empty

    '            strHTML &= "<TITLE>Check In Report </TITLE>"
    '            strHTML &= "<BODY><FONT FACE =VERDANA FONT SIZE=2>"
    '            strHTML &= "<BR>"
    '            strHTML &= "<TABLE BORDER=0 WIDTH=140%> " & _
    '                       " <TR>" & _
    '                       " <TD width='10%'> " & _
    '                       "<FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 12, "Prepared By :") & " </B></FONT>" & _
    '                       "</TD> " & _
    '                       "<TD WIDTH='10%' align='left' ><FONT SIZE=2> " & _
    '                       " " & User._Object._Username & "" & _
    '                       "</FONT></TD> " & _
    '                       "<TD WIDTH='60%' colspan=15 align='center' > " & _
    '                       "<FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & _
    '                       "</TD> " & _
    '                        "<TD WIDTH='10%'> " & _
    '                       " &nbsp; " & _
    '                       "</TD> " & _
    '                       "<TD WIDTH='10%'> " & _
    '                       " &nbsp; " & _
    '                       "</TD> " & _
    '                       "</TR> " & _
    '                       "<TR valign='middle'> " & _
    '                       "<TD width='10%'> " & _
    '                       "<FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 13, "Date :") & "</B></FONT>" & _
    '                       " </TD> " & _
    '                       "<TD WIDTH='35' align='left'><FONT SIZE=2> " & _
    '                       "" & Now.Date & " " & _
    '                       "</FONT></TD> " & _
    '                       "<TD width='60%' colspan=15  align='center' > " & _
    '                       "<FONT SIZE=3><b> " & Me._ReportName & "</B></FONT> " & _
    '                       "</TD> " & _
    '                       "<TD WIDTH='10%'> " & _
    '                       "&nbsp; " & _
    '                       "</TD>  " & _
    '                        "</TR> " & _
    '                        "</TABLE>"
    '            strHTML &= "<HR>"

    '            strHTML &= "<B> " & Me._FilterTitle & " </B><BR>" & vbCrLf
    '            strHTML &= "</HR>"
    '            strHTML &= "<BR>"
    '            strHTML &= "<TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%>"
    '            strHTML &= "<TR ALIGN = CENTER VALIGN=TOP>"
    '            For j As Integer = 0 To dsList.Tables("DataTable").Columns.Count - 1
    '                strHTML &= "<TD colspan=3 BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & dsList.Tables("DataTable").Columns(j).ColumnName & "</B></FONT></TD>"
    '            Next
    '            strHTML &= "</TR>"

    '            For i As Integer = 0 To dsList.Tables("DataTable").Rows.Count - 1
    '                strHTML &= "<TR>"
    '                For k As Integer = 0 To dsList.Tables("DataTable").Columns.Count - 1
    '                    strHTML &= "<TD colspan=3 BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & dsList.Tables("DataTable").Rows(i)(k) & "</FONT></TD>"
    '                Next
    '                strHTML &= "</TR> "
    '            Next

    '            strHTML &= "<TR>"
    '            For k As Integer = 0 To dsList.Tables("DataTable").Columns.Count - 1
    '                If k > 2 Then
    '                    strHTML &= "<TD colspan=3 BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & dblColTot(k) & "</FONT></TD>"
    '                ElseIf k = 0 Then
    '                    strHTML &= "<TD colspan=3 BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Grand Total :") & "</B></FONT></TD>"
    '                Else
    '                    strHTML &= "<TD colspan=3 BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>"
    '                End If
    '            Next
    '            strHTML &= "</TR> "

    '            strHTML &= "</TABLE>" & _
    '                          "</HTML>"


    '            Dim strExportPath As String = ConfigParameter._Object._ExportReportPath

    '            If System.IO.Directory.Exists(strExportPath) = False Then
    '                Dim dig As New Windows.Forms.FolderBrowserDialog
    '                dig.Description = "Select Folder Where to export report."

    '                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
    '                    strExportPath = dig.SelectedPath
    '                Else
    '                    Exit Try
    '                End If
    '            End If
    '            Dim strReportExportFile As String = strExportPath & "\" & Me._ReportName.Replace(" ", "_") & "_" & _
    '                                    Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")

    '            System.IO.File.WriteAllText(strReportExportFile & ".xls", strHTML)

    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)

    '            Exit Try

    '        Else

    '            rpt_Data = New ArutiReport.Designer.dsArutiReport

    '            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '                Dim rpt_Row As DataRow
    '                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '                rpt_Row.Item("Column1") = dtRow.Item("PeriodName")
    '                rpt_Row.Item("Column2") = dtRow.Item("EmpCode")
    '                rpt_Row.Item("Column3") = dtRow.Item("EmpName")

    '                Call Set_Data(rpt_Row, dtRow, mintTranHeadCount)

    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            Next

    '            ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 8, "Period"))
    '            ReportFunction.TextChange(objRpt, "txtEmp_Code", Language.getMessage(mstrModuleName, 9, "Employee Code"))
    '            ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 10, " Employee Name"))

    '            For i As Integer = 1 To intRptObjCnt
    '                If i <= mintTranHeadCount Then
    '                    ReportFunction.TextChange(objRpt, "txtHead" & i.ToString, strSepTranHead(i - 1))
    '                Else
    '                    ReportFunction.EnableSuppress(objRpt, "txtHead" & i.ToString, True)
    '                    ReportFunction.EnableSuppress(objRpt, "frmlGrpHead" & i.ToString & "1", True)
    '                    ReportFunction.EnableSuppress(objRpt, "frmlGrandHead" & i.ToString & "1", True)
    '                End If
    '            Next

    '            objRpt.SetDataSource(rpt_Data)


    '            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 3, "Printed Date :"))
    '            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 4, "Printed By :"))
    '            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 5, "Page :"))
    '            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 6, "Group Total :"))
    '            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 7, "Grand Total :"))

    '            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        End If

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    'Private Sub Set_Data(ByVal rpt_Row As DataRow, ByVal dtRow As DataRow, ByVal intTranHeadCount As Integer)
    '    Try
    '        Dim intCnt As Integer = 4
    '        For i As Integer = 0 To intTranHeadCount - 1
    '            rpt_Row.Item("Column" & intCnt.ToString) = Format(cdec(dtRow.Item(strSepTranHead(i))), GUI.fmtCurrency)
    '            intCnt += 1
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Set_Data; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub
    'Sandeep (29 Nov 2010)-End

    'Sohail (16 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    'Public Sub Generate_PayrollReport()
    '    Dim dsEarning As New DataSet
    '    Dim dsDeduction As New DataSet
    '    Dim dtFinalTable As DataTable
    '    Dim dtTable As DataTable
    '    Dim dtDeduction As DataTable
    '    Dim exForce As Exception
    '    Dim StrQ As String = String.Empty
    '    Try

    '        If ConfigParameter._Object._ExportReportPath = "" Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        Dim start As DateTime = DateAndTime.Now

    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()


    '        dtFinalTable = New DataTable("Payroll")
    '        Dim dCol As DataColumn

    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        dtFinalTable.Columns.Add("PeriodId", System.Type.GetType("System.Int32")).DefaultValue = 0
    '        dtFinalTable.Columns.Add("Period", System.Type.GetType("System.String")).DefaultValue = ""
    '        'Sohail (05 Apr 2012) -- End

    '        dCol = New DataColumn("EmpCode")
    '        dCol.Caption = "Code"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        dCol = New DataColumn("EmpName")
    '        dCol.Caption = "Employee"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        'Sohail (26 Nov 2011) -- Start
    '        dCol = New DataColumn("GrpID")
    '        dCol.Caption = "GroupID"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        dCol = New DataColumn("GrpName")
    '        dCol.Caption = "GroupName"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)
    '        'Sohail (26 Nov 2011) -- End

    '        StrQ = "SELECT " & _
    '                   "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '                   ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '                   "FROM prtranhead_master " & _
    '                   "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '                   "AND prtranhead_master.trnheadtype_id = 1 ORDER BY prtranhead_master.tranheadunkid "

    '        Dim dsList As New DataSet
    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            For Each dtRow As DataRow In dsList.Tables("List").Rows
    '                dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
    '                dCol.DefaultValue = 0
    '                dCol.Caption = dtRow.Item("Tname")
    '                dtFinalTable.Columns.Add(dCol)
    '            Next
    '        End If
    '        '------------------ Add Gross Pay Column
    '        dCol = New DataColumn("TGP")
    '        dCol.Caption = "Gross Pay"
    '        dCol.DefaultValue = 0
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        ''/* Deduction Part */

    '        StrQ = "SELECT " & _
    '                   "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '                   ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '                   "FROM prtranhead_master " & _
    '                   "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '                   "AND prtranhead_master.trnheadtype_id IN (2,3) ORDER BY prtranhead_master.tranheadunkid "

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            For Each dtRow As DataRow In dsList.Tables("List").Rows
    '                dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
    '                dCol.Caption = dtRow.Item("Tname")
    '                dCol.DefaultValue = 0
    '                'dCol.DataType = System.Type.GetType("System.Decimal")
    '                dtFinalTable.Columns.Add(dCol)
    '            Next
    '        End If

    '        ''------------------ Add Loan Advance Savings
    '        'dCol = New DataColumn("LAS")
    '        'dCol.DefaultValue = 0
    '        'dCol.Caption = "Loan/Advance/Savings"
    '        'dCol.DataType = System.Type.GetType("System.Decimal")
    '        'dtFinalTable.Columns.Add(dCol)

    '        '------------ Loan
    '        dCol = New DataColumn("Loan")
    '        dCol.Caption = "Loan"
    '        dCol.DefaultValue = 0
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        '------------ Advance
    '        dCol = New DataColumn("Advance")
    '        dCol.Caption = "Advance"
    '        dCol.DefaultValue = 0
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        '------------ Savings
    '        dCol = New DataColumn("Savings")
    '        dCol.Caption = "Savings"
    '        dCol.DefaultValue = 0
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        '------------------ Add Total Deduction Column
    '        dCol = New DataColumn("TDD")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Total Deduction"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        '------------------ Add Net Pay Column
    '        dCol = New DataColumn("NetPay")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Net Pay"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)


    '        'Sohail (26 Nov 2011) -- Start
    '        'StrQ = "SELECT " & _
    '        '            "	 cfcommon_period_tran.period_name AS PeriodName " & _
    '        '            "	,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,''))AS EmpName " & _
    '        '            "	,hremployee_master.employeeunkid AS EmpId " & _
    '        '            "	,cfcommon_period_tran.periodunkid AS PeriodId " & _
    '        '            "   ,ISNULL(hremployee_master.employeecode,'') As Code " & _
    '        '            "FROM  prpayrollprocess_tran " & _
    '        '            "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '            "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "	JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '            "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '            "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '            "	AND cfcommon_period_tran.isactive = 1 " & _
    '        '            "	AND cfcommon_period_tran.periodunkid = @PeriodId "
    '        StrQ = "SELECT DISTINCT " & _
    '                      "  payperiodunkid AS PeriodId " & _
    '                      ", period_name AS PeriodName " & _
    '                      ", vwPayroll.employeeunkid AS EmpId " & _
    '                      ", vwPayroll.employeename AS EmpName " & _
    '                      ", vwPayroll.employeecode AS Code "

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ",'' AS Id, '' AS GName "
    '        End If

    '        StrQ &= "FROM    vwPayroll " & _
    '                "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Join
    '        End If

    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ &= "WHERE   payperiodunkid = @PeriodId "
    '        StrQ &= "WHERE   payperiodunkid IN ( " & mstrPeriodIdList & " ) "
    '        'Sohail (05 Apr 2012) -- End
    '        'Sohail (26 Nov 2011) -- End

    '        If mintEmployeeId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId  "
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId) 'Sohail (26 Nov 2011)
    '        End If


    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId"
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId) 'Sohail (26 Nov 2011)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            'Sohail (16 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "	AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
    '            'Sohail (16 Apr 2012) -- End
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        'Sohail (26 Nov 2011) -- Start
    '        'StrQ &= "GROUP BY " & _
    '        '            "	cfcommon_period_tran.period_name " & _
    '        '            "	,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')) " & _
    '        '            "	,hremployee_master.employeeunkid " & _
    '        '            "	,cfcommon_period_tran.periodunkid " & _
    '        '            "  ,ISNULL(hremployee_master.employeecode,'') " & _
    '        '            "ORDER BY  " & _
    '        '            "	hremployee_master.employeeunkid "
    '        If mintViewIndex > 0 Then
    '            StrQ &= " ORDER BY " & mstrAnalysis_OrderBy & ", vwPayroll.employeeunkid "
    '        Else
    '            StrQ &= " ORDER BY vwPayroll.employeeunkid "
    '        End If
    '        'Sohail (26 Nov 2011) -- End

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        'Sohail (26 Nov 2011) -- Start
    '        'If mintEmployeeId > 0 Then
    '        '    objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
    '        'End If


    '        ''S.SANDEEP [ 17 AUG 2011 ] -- START
    '        ''ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        'If mintBranchId > 0 Then
    '        '    objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        'End If
    '        ''S.SANDEEP [ 17 AUG 2011 ] -- END 
    '        'Sohail (26 Nov 2011) -- End

    '        dsEarning = objDataOperation.ExecQuery(StrQ, "ERNS")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dsEarningTotal As New DataSet

    '        objDataOperation.ClearParameters()
    '        'Sohail (26 Nov 2011) -- Start
    '        'StrQ = "SELECT " & _
    '        '                "	 hremployee_master.employeeunkid AS EmpId " & _
    '        '                "	,prtranhead_master.tranheadunkid AS TranId " & _
    '        '                "	,SUM(amount) AS Amount " & _
    '        '                "FROM  prpayrollprocess_tran " & _
    '        '                "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '                "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                "	JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '                "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '                "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '                "	AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '                "	AND prtranhead_master.trnheadtype_id = 1 " & _
    '        '                "	AND prtnaleave_tran.payperiodunkid = @PeriodId "
    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ = "SELECT  hremployee_master.employeeunkid AS EmpId " & _
    '        '              ", tranheadunkid AS TranId " & _
    '        '                "	,SUM(amount) AS Amount " & _
    '        '        "FROM    vwPayroll " & _
    '        '        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "WHERE   trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
    '        '                "AND payperiodunkid = @PeriodId "
    '        StrQ = "SELECT  payperiodunkid AS PeriodId " & _
    '                      ", hremployee_master.employeeunkid AS EmpId " & _
    '                      ", tranheadunkid AS TranId " & _
    '                        "	,SUM(amount) AS Amount " & _
    '                "FROM    vwPayroll " & _
    '                "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                "WHERE   trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
    '                        "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
    '        'Sohail (05 Apr 2012) -- End
    '        'Sohail (26 Nov 2011) -- End

    '        If mintEmployeeId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId) 'Sohail (26 Nov 2011)
    '        End If

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId"
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId) 'Sohail (26 Nov 2011)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            'Sohail (16 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "	AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
    '            'Sohail (16 Apr 2012) -- End
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        'Sohail (26 Nov 2011) -- Start
    '        'StrQ &= "GROUP BY  hremployee_master.employeeunkid " & _
    '        '                ",prtranhead_master.tranheadunkid " & _
    '        '                "ORDER BY  " & _
    '        '                "hremployee_master.employeeunkid " & _
    '        '                ",prtranhead_master.tranheadunkid "
    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ &= "GROUP BY  hremployee_master.employeeunkid " & _
    '        '               ", tranheadunkid " & _
    '        '                "ORDER BY  " & _
    '        '                "hremployee_master.employeeunkid " & _
    '        '               ", tranheadunkid "
    '        StrQ &= "GROUP BY payperiodunkid " & _
    '                      ", hremployee_master.employeeunkid " & _
    '                       ", tranheadunkid " & _
    '                "ORDER BY hremployee_master.employeeunkid " & _
    '                       ", tranheadunkid "
    '        'Sohail (05 Apr 2012) -- End
    '        'Sohail (26 Nov 2011) -- End


    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        'Sohail (26 Nov 2011) -- Start
    '        'If mintEmployeeId > 0 Then
    '        '    objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
    '        'End If

    '        ''S.SANDEEP [ 17 AUG 2011 ] -- START
    '        ''ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        'If mintBranchId > 0 Then
    '        '    objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        'End If
    '        ''S.SANDEEP [ 17 AUG 2011 ] -- END 
    '        'Sohail (26 Nov 2011) -- End

    '        dsEarningTotal = objDataOperation.ExecQuery(StrQ, "ETotal")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dsDeductionTotal As New DataSet

    '        objDataOperation.ClearParameters()
    '        'Sohail (26 Nov 2011) -- Start
    '        'StrQ = "SELECT " & _
    '        '            "	  EmpId AS EmpId " & _
    '        '            "	, TranId AS TranId " & _
    '        '            "	, Amount AS Amount " & _
    '        '            "	, Mid AS Mid " & _
    '        '            "FROM " & _
    '        '            "	( " & _
    '        '            "		SELECT " & _
    '        '            "			 hremployee_master.employeeunkid AS EmpId " & _
    '        '            "			,prtranhead_master.tranheadunkid AS TranId " & _
    '        '            "			,SUM(amount) AS Amount " & _
    '        '            "			,0 AS Mid " & _
    '        '            "		FROM  prpayrollprocess_tran " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '            "			JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '            "		WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '            "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '            "			AND prpayrollprocess_tran.add_deduct IN (2) " & _
    '        '            "			AND prtnaleave_tran.payperiodunkid = @PeriodId "


    '        ''Pinkal (24-Jun-2011) -- Start
    '        ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        'If mblnIsActive = False Then
    '        '    StrQ &= "	AND hremployee_master.isactive = 1 "
    '        'End If

    '        ''Pinkal (24-Jun-2011) -- End

    '        ''S.SANDEEP [ 17 AUG 2011 ] -- START
    '        ''ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        'If mintBranchId > 0 Then
    '        '    StrQ &= " AND hremployee_master.stationunkid = @BranchId"
    '        'End If
    '        ''S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        ''Sohail (28 Jun 2011) -- Start
    '        ''Issue : According to prvilege that lower level user should not see superior level employees.
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If
    '        ''Sohail (28 Jun 2011) -- End
    '        'StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '        '            "			,prtranhead_master.tranheadunkid " & _
    '        '            "	UNION ALL " & _
    '        '            "		SELECT " & _
    '        '            "			  hremployee_master.employeeunkid AS EmpId " & _
    '        '            "			,-1 AS TranId " & _
    '        '            "			,SUM(amount) AS Amount " & _
    '        '            "			,1 AS Mid " & _
    '        '            "		FROM  prpayrollprocess_tran " & _
    '        '            "			JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '            "			JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "		WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '            "			AND lnloan_advance_tran.isloan = 1 " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '            "			AND prpayrollprocess_tran.add_deduct IN (2) " & _
    '        '            "			AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '        '            "			AND prtnaleave_tran.payperiodunkid = @PeriodId "

    '        ''Pinkal (24-Jun-2011) -- Start
    '        ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        'If mblnIsActive = False Then
    '        '    StrQ &= " AND hremployee_master.isactive = 1 "
    '        'End If

    '        '    'Pinkal (24-Jun-2011) -- End

    '        ''S.SANDEEP [ 17 AUG 2011 ] -- START
    '        ''ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        'If mintBranchId > 0 Then
    '        '    StrQ &= " AND hremployee_master.stationunkid = @BranchId"
    '        'End If
    '        ''S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        ''Sohail (28 Jun 2011) -- Start
    '        ''Issue : According to prvilege that lower level user should not see superior level employees.
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If
    '        ''Sohail (28 Jun 2011) -- End
    '        'StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '        '            "	UNION ALL " & _
    '        '            "		SELECT " & _
    '        '            "			 hremployee_master.employeeunkid AS EmpId " & _
    '        '            "			,-1 AS TranId " & _
    '        '            "			,SUM(amount) AS Amount " & _
    '        '            "			,2 AS Mid " & _
    '        '            "		FROM  prpayrollprocess_tran " & _
    '        '            "			JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '            "			JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "		WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '            "			AND lnloan_advance_tran.isloan = 0 " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '            "			AND prpayrollprocess_tran.add_deduct IN (2) " & _
    '        '            "			AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '        '                "			AND prtnaleave_tran.payperiodunkid = @PeriodId "


    '        ''Pinkal (24-Jun-2011) -- Start
    '        ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        'If mblnIsActive = False Then
    '        '    StrQ &= " AND hremployee_master.isactive = 1 "
    '        'End If

    '        ''Pinkal (24-Jun-2011) -- End

    '        ''S.SANDEEP [ 17 AUG 2011 ] -- START
    '        ''ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        'If mintBranchId > 0 Then
    '        '    StrQ &= " AND hremployee_master.stationunkid = @BranchId"
    '        'End If
    '        ''S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        ''Sohail (28 Jun 2011) -- Start
    '        ''Issue : According to prvilege that lower level user should not see superior level employees.
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If
    '        ''Sohail (28 Jun 2011) -- End
    '        'StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '        '            "	UNION ALL " & _
    '        '            "		SELECT " & _
    '        '            "			 hremployee_master.employeeunkid AS EmpId " & _
    '        '            "			,-1 AS TranId " & _
    '        '            "			,SUM(amount) AS Amount " & _
    '        '            "			,3 AS Mid " & _
    '        '            "		FROM  prpayrollprocess_tran " & _
    '        '            "			JOIN svsaving_tran ON prpayrollprocess_tran.savingtranunkid = svsaving_tran.savingtranunkid " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '            "			JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "		WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '        '            "			AND prpayrollprocess_tran.add_deduct IN (2)  " & _
    '        '            "			AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '        '                "			AND prtnaleave_tran.payperiodunkid = @PeriodId "

    '        ''Pinkal (24-Jun-2011) -- Start
    '        ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        'If mblnIsActive = False Then
    '        '    StrQ &= " AND hremployee_master.isactive = 1 "
    '        'End If

    '        ''Pinkal (24-Jun-2011) -- End


    '        ''S.SANDEEP [ 17 AUG 2011 ] -- START
    '        ''ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        'If mintBranchId > 0 Then
    '        '    StrQ &= " AND hremployee_master.stationunkid = @BranchId"
    '        'End If
    '        ''S.SANDEEP [ 17 AUG 2011 ] -- END 


    '        ''Sohail (28 Jun 2011) -- Start
    '        ''Issue : According to prvilege that lower level user should not see superior level employees.
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If
    '        ''Sohail (28 Jun 2011) -- End
    '        'StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '        '            ") AS Deduction "


    '        ''StrQ = "SELECT " & _
    '        ''            "	 EmpId AS EmpId " & _
    '        ''            "	,TranId AS TranId " & _
    '        ''            "	,Amount AS Amount " & _
    '        ''            "FROM " & _
    '        ''            "( " & _
    '        ''            "	SELECT " & _
    '        ''            "		 hremployee_master.employeeunkid AS EmpId " & _
    '        ''            "		,prtranhead_master.tranheadunkid AS TranId " & _
    '        ''            "		,SUM(amount) AS Amount " & _
    '        ''            "	FROM  prpayrollprocess_tran " & _
    '        ''            "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        ''            "		JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        ''            "		JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        ''            "	WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        ''            "		AND hremployee_master.isactive = 1 " & _
    '        ''            "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        ''            "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        ''            "		AND prpayrollprocess_tran.add_deduct IN (2) " & _
    '        ''            "		AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '        ''            "	GROUP BY  hremployee_master.employeeunkid " & _
    '        ''            "	,prtranhead_master.tranheadunkid " & _
    '        ''            "UNION ALL " & _
    '        ''            "	SELECT " & _
    '        ''            "		 hremployee_master.employeeunkid AS EmpId " & _
    '        ''            "		,-1 AS TranId " & _
    '        ''            "		,SUM(amount) AS Amount " & _
    '        ''            "	FROM  prpayrollprocess_tran " & _
    '        ''            "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        ''            "		JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        ''            "	WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        ''            "		AND hremployee_master.isactive = 1 " & _
    '        ''            "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        ''            "		AND prpayrollprocess_tran.add_deduct IN (2) " & _
    '        ''            "       AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '        ''            "		AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '        ''            "	GROUP BY  hremployee_master.employeeunkid " & _
    '        ''            ") AS Deduction "
    '        'If mintEmployeeId > 0 Then
    '        '    StrQ &= "	WHERE EmpId = @EmpId "
    '        'End If
    '        'StrQ &= "ORDER BY EmpId,TranId "
    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ = "SELECT  hremployee_master.employeeunkid AS EmpId " & _
    '        '              ", tranheadunkid AS TranId " & _
    '        '            "			,SUM(amount) AS Amount " & _
    '        '            "			,0 AS Mid " & _
    '        '        "FROM    vwPayroll " & _
    '        '                "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "WHERE   GroupID IN ( " & enViewPayroll_Group.Deduction & " ) " & _
    '        '                "AND payperiodunkid = @PeriodId "
    '        StrQ = "SELECT  payperiodunkid AS PeriodId " & _
    '                      ", hremployee_master.employeeunkid AS EmpId " & _
    '                      ", tranheadunkid AS TranId " & _
    '                    "			,SUM(amount) AS Amount " & _
    '                    "			,0 AS Mid " & _
    '                "FROM    vwPayroll " & _
    '                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                "WHERE   GroupID IN ( " & enViewPayroll_Group.Deduction & " ) " & _
    '                        "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
    '        'Sohail (05 Apr 2012) -- End

    '        'Pinkal (05-Mar-2012) -- Start
    '        'Enhancement : TRA Changes
    '        If mintEmployeeId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
    '        End If
    '        'Pinkal (05-Mar-2012) -- End


    '        If mblnIsActive = False Then
    '            'Sohail (16 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "	AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
    '            'Sohail (16 Apr 2012) -- End
    '        End If
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId"
    '        End If

    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '        '              ", tranheadunkid " & _
    '        '            "	UNION ALL " & _
    '        '        "SELECT  hremployee_master.employeeunkid AS EmpId " & _
    '        '            "			,-1 AS TranId " & _
    '        '            "			,SUM(amount) AS Amount " & _
    '        '            "			,1 AS Mid " & _
    '        '        "FROM    vwPayroll " & _
    '        '                "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "WHERE   GroupID IN ( " & enViewPayroll_Group.Loan & " ) " & _
    '        '                "AND payperiodunkid = @PeriodId "
    '        StrQ &= "GROUP BY payperiodunkid " & _
    '                      ", hremployee_master.employeeunkid " & _
    '                      ", tranheadunkid " & _
    '                    "	UNION ALL " & _
    '                "SELECT  payperiodunkid AS PeriodId " & _
    '                      ", hremployee_master.employeeunkid AS EmpId " & _
    '                    "			,-1 AS TranId " & _
    '                    "			,SUM(amount) AS Amount " & _
    '                    "			,1 AS Mid " & _
    '                "FROM    vwPayroll " & _
    '                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                "WHERE   GroupID IN ( " & enViewPayroll_Group.Loan & " ) " & _
    '                        "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
    '        'Sohail (05 Apr 2012) -- End

    '        'Pinkal (05-Mar-2012) -- Start
    '        'Enhancement : TRA Changes
    '        If mintEmployeeId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
    '        End If
    '        'Pinkal (05-Mar-2012) -- End

    '        If mblnIsActive = False Then
    '            'Sohail (16 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            'Sohail (16 Apr 2012) -- End
    '        End If
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId"
    '        End If

    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '        '            "	UNION ALL " & _
    '        '        "SELECT  hremployee_master.employeeunkid AS EmpId " & _
    '        '            "			,-1 AS TranId " & _
    '        '            "			,SUM(amount) AS Amount " & _
    '        '            "			,2 AS Mid " & _
    '        '        "FROM    vwPayroll " & _
    '        '                "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "WHERE   GroupID IN ( " & enViewPayroll_Group.Advance & " ) " & _
    '        '                "AND payperiodunkid = @PeriodId "
    '        StrQ &= "GROUP BY payperiodunkid " & _
    '                      ", hremployee_master.employeeunkid " & _
    '                    "	UNION ALL " & _
    '                "SELECT  payperiodunkid AS PeriodId " & _
    '                      ", hremployee_master.employeeunkid AS EmpId " & _
    '                    "			,-1 AS TranId " & _
    '                    "			,SUM(amount) AS Amount " & _
    '                    "			,2 AS Mid " & _
    '                "FROM    vwPayroll " & _
    '                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                "WHERE   GroupID IN ( " & enViewPayroll_Group.Advance & " ) " & _
    '                        "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
    '        'Sohail (05 Apr 2012) -- End

    '        'Pinkal (05-Mar-2012) -- Start
    '        'Enhancement : TRA Changes
    '        If mintEmployeeId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
    '        End If
    '        'Pinkal (05-Mar-2012) -- End

    '        If mblnIsActive = False Then
    '            'Sohail (16 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            'Sohail (16 Apr 2012) -- End
    '        End If
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId"
    '        End If

    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '        '            "	UNION ALL " & _
    '        '        "SELECT  hremployee_master.employeeunkid AS EmpId " & _
    '        '            "			,-1 AS TranId " & _
    '        '            "			,SUM(amount) AS Amount " & _
    '        '            "			,3 AS Mid " & _
    '        '        "FROM    vwPayroll " & _
    '        '                "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "WHERE   GroupID IN ( " & enViewPayroll_Group.Saving & " ) " & _
    '        '                "AND payperiodunkid = @PeriodId "
    '        StrQ &= "GROUP BY payperiodunkid " & _
    '                      ", hremployee_master.employeeunkid " & _
    '                    "	UNION ALL " & _
    '                "SELECT  payperiodunkid AS PeriodId " & _
    '                      ", hremployee_master.employeeunkid AS EmpId " & _
    '                    "			,-1 AS TranId " & _
    '                    "			,SUM(amount) AS Amount " & _
    '                    "			,3 AS Mid " & _
    '                "FROM    vwPayroll " & _
    '                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                "WHERE   GroupID IN ( " & enViewPayroll_Group.Saving & " ) " & _
    '                        "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
    '        'Sohail (05 Apr 2012) -- End

    '        'Pinkal (05-Mar-2012) -- Start
    '        'Enhancement : TRA Changes
    '        If mintEmployeeId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
    '        End If
    '        'Pinkal (05-Mar-2012) -- End

    '        If mblnIsActive = False Then
    '            'Sohail (16 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            'Sohail (16 Apr 2012) -- End
    '        End If
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId"
    '        End If

    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ &= "		GROUP BY  hremployee_master.employeeunkid " & _
    '        '        "ORDER BY hremployee_master.employeeunkid " & _
    '        '              ", tranheadunkid "
    '        StrQ &= "GROUP BY payperiodunkid " & _
    '                      ", hremployee_master.employeeunkid " & _
    '                "ORDER BY hremployee_master.employeeunkid " & _
    '                      ", tranheadunkid "
    '        'Sohail (05 Apr 2012) -- End
    '        'Sohail (26 Nov 2011) -- End

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        If mintEmployeeId > 0 Then
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
    '        End If

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        dsDeductionTotal = objDataOperation.ExecQuery(StrQ, "Deduction")


    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'Dim mdicEarning As New Dictionary(Of Integer, Integer)
    '        Dim mdicEarning As New Dictionary(Of String, String)
    '        Dim strKey As String
    '        'Sohail (05 Apr 2012) -- End
    '        For Each drRow As DataRow In dsEarning.Tables("ERNS").Rows

    '            'Sohail (05 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'If mdicEarning.ContainsKey(drRow.Item("EmpId")) Then Continue For
    '            'mdicEarning.Add(drRow.Item("EmpId"), drRow.Item("EmpId"))

    '            'dtTable = New DataView(dsEarningTotal.Tables("ETotal"), "EmpId = " & drRow.Item("EmpId"), "", DataViewRowState.CurrentRows).ToTable
    '            'dtDeduction = New DataView(dsDeductionTotal.Tables("Deduction"), "EmpId = " & drRow.Item("EmpId"), "", DataViewRowState.CurrentRows).ToTable
    '            strKey = drRow.Item("PeriodId").ToString & "_" & drRow.Item("EmpId")
    '            If mdicEarning.ContainsKey(strKey) Then Continue For
    '            mdicEarning.Add(strKey, strKey)

    '            dtTable = New DataView(dsEarningTotal.Tables("ETotal"), "PeriodId = " & drRow.Item("PeriodId") & " AND EmpId = " & drRow.Item("EmpId") & " ", "", DataViewRowState.CurrentRows).ToTable
    '            dtDeduction = New DataView(dsDeductionTotal.Tables("Deduction"), "PeriodId = " & drRow.Item("PeriodId") & " AND EmpId = " & drRow.Item("EmpId") & " AND EmpId = " & drRow.Item("EmpId") & " ", "", DataViewRowState.CurrentRows).ToTable
    '            'Sohail (05 Apr 2012) -- End



    '            Dim rpt_Row As DataRow
    '            rpt_Row = dtFinalTable.NewRow

    '            'Sohail (05 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            rpt_Row.Item("PeriodId") = drRow.Item("PeriodId")
    '            rpt_Row.Item("Period") = drRow.Item("PeriodName")
    '            'Sohail (05 Apr 2012) -- End

    '            rpt_Row.Item("EmpCode") = drRow.Item("Code")
    '            rpt_Row.Item("EmpName") = drRow.Item("EmpName")
    '            For Each dtRow As DataRow In dtTable.Rows
    '                rpt_Row.Item("Column" & dtRow.Item("TranId")) = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
    '                rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(dtRow.Item("Amount")), GUI.fmtCurrency)

    '            Next

    '            For Each dtDRow As DataRow In dtDeduction.Rows
    '                'If dtDRow.Item("TranId").ToString = "-1" Then
    '                '    rpt_Row.Item("LAS") = Format(cdec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                'Else
    '                '    rpt_Row.Item("Column" & dtDRow.Item("TranId")) = Format(cdec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                'End If
    '                If dtDRow.Item("Mid").ToString = "1" Then   'LOAN
    '                    rpt_Row.Item("Loan") = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                ElseIf dtDRow.Item("Mid").ToString = "2" Then 'ADVANCE
    '                    rpt_Row.Item("Advance") = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                ElseIf dtDRow.Item("Mid").ToString = "3" Then 'SAVINGS
    '                    rpt_Row.Item("Savings") = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                Else
    '                    rpt_Row.Item("Column" & dtDRow.Item("TranId")) = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                End If
    '                rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '            Next
    '            'rpt_Row.Item("NetPay") = Format(cdec(rpt_Row("TGP")), GUI.fmtCurrency) - Format(cdec(rpt_Row("TDD")), GUI.fmtCurrency)
    '            rpt_Row.Item("NetPay") = Format(CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD")), GUI.fmtCurrency)

    '            'Anjan (29 Dec 2010)-Start
    '            'rpt_Row.Item("NetPay") = Format(cdec(rpt_Row.Item("NetPay")), GUI.fmtCurrency)
    '            rpt_Row.Item("NetPay") = Format(Rounding.BRound(CDec(rpt_Row.Item("NetPay")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency)
    '            'Anjan (29 Dec 2010)-End

    '            'Sohail (26 Nov 2011) -- Start
    '            rpt_Row.Item("GrpID") = drRow.Item("Id")
    '            rpt_Row.Item("GrpName") = drRow.Item("GName")
    '            'Sohail (26 Nov 2011) -- End

    '            dtFinalTable.Rows.Add(rpt_Row)
    '        Next

    '        dtFinalTable.AcceptChanges()


    '        Dim dblTotal As Decimal = 0
    '        'Sohail (26 Nov 2011) -- Start
    '        'Dim m As Integer = 2
    '        'Dim n As Integer = 2
    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'Dim m As Integer = 4
    '        'Dim n As Integer = 4
    '        Dim m As Integer = 6
    '        Dim n As Integer = 6
    '        'Sohail (05 Apr 2012) -- End
    '        'Sohail (26 Nov 2011) -- End
    '        dblColTot = New Decimal(dtFinalTable.Columns.Count) {}

    '        While m < dtFinalTable.Columns.Count
    '            For t As Integer = 0 To dtFinalTable.Rows.Count - 1
    '                dblTotal += CDec(dtFinalTable.Rows(t)(m))
    '            Next
    '            dblColTot(n) = Format(CDec(dblTotal), GUI.fmtCurrency)
    '            dblTotal = 0
    '            m += 1
    '            n += 1
    '        End While

    '        Call FilterTitleAndFilterQuery()

    '        'Sohail (05 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        If Export_to_Excel(mstrReportTypeName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtFinalTable) Then
    '            'If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtFinalTable) Then
    '            'Sohail (05 Apr 2012) -- End
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Report successfully exported to Report export path"), enMsgBoxStyle.Information, (Now - start).TotalSeconds.ToString)
    '            'Sandeep [ 09 MARCH 2011 ] -- Start
    '            Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
    '            'Sandeep [ 09 MARCH 2011 ] -- End 
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_PayrollReport; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Sub Generate_PayrollReport(Optional ByVal blnExcelHTML As Boolean = False)
    Public Sub Generate_PayrollReport(ByVal mdicYearDBName As Dictionary(Of Integer, String) _
                                      , ByVal xUserUnkid As Integer _
                                      , ByVal xCompanyUnkid As Integer _
                                      , ByVal xPeriodStart As Date _
                                      , ByVal xPeriodEnd As Date _
                                      , ByVal xUserModeSetting As String _
                                      , ByVal xOnlyApproved As Boolean _
                                      , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                      , ByVal blnApplyUserAccessFilter As Boolean _
                                      , ByVal intBase_CurrencyId As Integer _
                                      , ByVal blnSetPayslipPaymentApproval As Boolean _
                                      , ByVal strfmtCurrency As String _
                                      , ByVal strExportReportPath As String _
                                      , ByVal blnOpenAfterExport As Boolean _
                                      , ByVal blnExcelHTML As Boolean _
                                      , ByVal dblRoundOff_Type As Double _
                                      )
        'Sohail (01 Mar 2016) - [dblRoundOff_Type]
        'Sohail (21 Aug 2015) -- End
        Dim dsPayroll As New DataSet
        Dim dsDeduction As New DataSet
        Dim dtFinalTable As DataTable
        'Sohail (16 Jun 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim dtTable As DataTable
        'Dim dtDeduction As DataTable
        'Sohail (16 Jun 2012) -- End
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Dim StrInnerQ As String = String.Empty 'Sohail (08 Dec 2012)
        Dim objActivity As New clsActivity_Master 'Sohail (21 Jun 2013) 
        Dim objCRExpense As New clsExpense_Master 'Sohail (12 Nov 2014)
        'Sohail (20 Aug 2014) -- Start
        'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
        Dim mdicEmpBank As New Dictionary(Of String, String)
        Dim dsPaymetDetail As DataSet
        Dim strCurrKey As String = ""
        'Sohail (20 Aug 2014) -- End

        'Pinkal (13-Oct-2014) -- Start
        'Enhancement -  SEPERATE BANK,ACCOUNT NO AND PAYMENT DETAIL COLUMN REQUESTED BY MR.ANDREW AND MR.RUTTA
        Dim mdicEmpAccount As New Dictionary(Of String, String)
        Dim mdicEmpPayment As New Dictionary(Of String, String)
        'Pinkal (13-Oct-2014) -- End

        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mintBase_CurrencyId = intBase_CurrencyId
            mblnSetPayslipPaymentApproval = blnSetPayslipPaymentApproval
            'Sohail (03 Nov 2021) -- Start
            'HJIF Issue : HJIF-AH-4234 : TRANSACTION HEADS PROBLEM NSSF (Bring difference of 0.003 when you compare with 10% of gross and total of NSSF).
            'mstrfmtCurrency = strfmtCurrency
            mstrfmtCurrency = strfmtCurrency & "###"
            'Sohail (03 Nov 2021) -- End
            mstrExportReportPath = strExportReportPath
            mblnOpenAfterExport = blnOpenAfterExport
            'Sohail (21 Aug 2015) -- End

            'Sohail (25 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'If ConfigParameter._Object._ExportReportPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Sohail (25 Mar 2013) -- End

            'Sohail (31 Jan 2014) -- Start
            If mstrUserAccessFilter.Trim = "" Then
                mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            End If
            'Sohail (31 Jan 2014) -- End

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()


            dtFinalTable = New DataTable("Payroll")
            Dim dCol As DataColumn

            'S.SANDEEP [ 29 SEP 2014 ] -- START
            'dtFinalTable.Columns.Add("PeriodId", System.Type.GetType("System.Int32")).DefaultValue = 0
            'dtFinalTable.Columns.Add("Period", System.Type.GetType("System.String")).DefaultValue = ""

            'dCol = New DataColumn("EmpCode")
            'dCol.Caption = "Code"
            'dCol.DataType = System.Type.GetType("System.String")
            'dtFinalTable.Columns.Add(dCol)

            'dCol = New DataColumn("EmpName")
            'dCol.Caption = "Employee"
            'dCol.DataType = System.Type.GetType("System.String")
            'dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("PeriodId")
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Int32")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Period")
            dCol.Caption = Language.getMessage(mstrModuleName, 39, "Period")
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("EmpCode")
            dCol.Caption = Language.getMessage(mstrModuleName, 40, "Code")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("EmpName")
            dCol.Caption = Language.getMessage(mstrModuleName, 41, "Employee")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)
            'S.SANDEEP [ 29 SEP 2014 ] -- END



            dCol = New DataColumn("GrpID")
            dCol.Caption = "GroupID"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("GrpName")
            dCol.Caption = "GroupName"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            StrQ = "SELECT " & _
                       "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
                       ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
                       "FROM " & mstrCurrentDatabaseName & "..prtranhead_master " & _
                       "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
                       "AND prtranhead_master.trnheadtype_id = 1 ORDER BY prtranhead_master.trnheadcode "
            'Hemant (23 May 2019) -- [ORDER BY prtranhead_master.tranheadunkid --> ORDER BY prtranhead_master.trnheadcode ]
            'Sohail (25 Mar 2013) - [mstrCurrentDatabaseName]

            Dim dsList As New DataSet
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("List").Rows
                    dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
                    'Pinkal (14-Dec-2012) -- Start
                    'Enhancement : TRA Changes
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    'Pinkal (14-Dec-2012) -- End
                    dCol.DefaultValue = 0
                    dCol.Caption = dtRow.Item("Tname")

                    dtFinalTable.Columns.Add(dCol)
                Next
            End If

            'Sohail (21 Jun 2013) -- Start
            'TRA - ENHANCEMENT - Pay Per Actvity [Earning]
            'Hemant (23 May 2019) -- Start
            'Support Issue Id # 0003829(KCMU) : Allow the payroll transaction heads to be arranged due to Transaction head code on payroll reports.
            'dsList = objActivity.getComboList("Activity", False, , enTranHeadType.EarningForEmployees)
            dsList = objActivity.getComboList("Activity", False, , enTranHeadType.EarningForEmployees, " Order By code ")
            'Hemant (23 May 2019) -- End
            If dsList.Tables("Activity").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("Activity").Rows
                    dCol = New DataColumn("ColumnPPA" & dtRow.Item("activityunkid"))
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = dtRow.Item("name")

                    dtFinalTable.Columns.Add(dCol)
                Next
            End If
            'Sohail (21 Jun 2013) -- End

            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
            'Sohail (29 Mar 2017) -- Start
            'PACRA Enhancement - 65.1 - Include all types of Claim Expense in Reports.
            'dsList = objCRExpense.getComboList(enExpenseType.EXP_LEAVE, False, "Expense", , , enTranHeadType.EarningForEmployees)
            'Hemant (23 May 2019) -- Start
            'Support Issue Id # 0003829(KCMU) : Allow the payroll transaction heads to be arranged due to Transaction head code on payroll reports.
            'dsList = objCRExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.EarningForEmployees)
            dsList = objCRExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.EarningForEmployees, , " Order By code ")
            'Hemant (23 May 2019) -- End
            'Sohail (29 Mar 2017) -- End
            If dsList.Tables("Expense").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("Expense").Rows
                    dCol = New DataColumn("ColumnCR" & dtRow.Item("Id"))
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = dtRow.Item("name")

                    dtFinalTable.Columns.Add(dCol)
                Next
            End If
            'Sohail (12 Nov 2014) -- End

            '------------------ Add Gross Pay Column
            dCol = New DataColumn("TGP")
            'S.SANDEEP [ 29 SEP 2014 ] -- START
            'dCol.Caption = "Gross Pay"
            dCol.Caption = Language.getMessage(mstrModuleName, 45, "Gross Pay")
            'S.SANDEEP [ 29 SEP 2014 ] -- END
            dCol.DefaultValue = 0

            'Pinkal (14-Dec-2012) -- Start
            'Enhancement : TRA Changes
            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")
            'Pinkal (14-Dec-2012) -- End


            dtFinalTable.Columns.Add(dCol)

            ''/* Deduction Part */

            StrQ = "SELECT " & _
                       "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
                       ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
                       "FROM " & mstrCurrentDatabaseName & "..prtranhead_master " & _
                       "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
                       "AND prtranhead_master.trnheadtype_id IN (2,3) ORDER BY prtranhead_master.trnheadcode "
            'Hemant (23 May 2019) -- [ORDER BY prtranhead_master.tranheadunkid --> ORDER BY prtranhead_master.trnheadcode ]
            'Sohail (25 Mar 2013) - [mstrCurrentDatabaseName]

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("List").Rows
                    dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))

                    'Pinkal (14-Dec-2012) -- Start
                    'Enhancement : TRA Changes
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    'Pinkal (14-Dec-2012) -- End

                    dCol.Caption = dtRow.Item("Tname")
                    dCol.DefaultValue = 0
                    'dCol.DataType = System.Type.GetType("System.Decimal")
                    dtFinalTable.Columns.Add(dCol)
                Next
            End If

            'Sohail (21 Jun 2013) -- Start
            'TRA - ENHANCEMENT - Pay Per Actvity [Deduction]
            'Hemant (23 May 2019) -- Start
            'Support Issue Id # 0003829(KCMU) : Allow the payroll transaction heads to be arranged due to Transaction head code on payroll reports.
            'dsList = objActivity.getComboList("Activity", False, , enTranHeadType.DeductionForEmployee)
            dsList = objActivity.getComboList("Activity", False, , enTranHeadType.DeductionForEmployee, " Order By code ")
            'Hemant (23 May 2019) -- End
            If dsList.Tables("Activity").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("Activity").Rows
                    dCol = New DataColumn("ColumnPPA" & dtRow.Item("activityunkid"))
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = dtRow.Item("name")

                    dtFinalTable.Columns.Add(dCol)
                Next
            End If
            'Sohail (21 Jun 2013) -- End

            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
            'Sohail (29 Mar 2017) -- Start
            'PACRA Enhancement - 65.1 - Include all types of Claim Expense in Reports.
            'dsList = objCRExpense.getComboList(enExpenseType.EXP_LEAVE, False, "Expense", , , enTranHeadType.DeductionForEmployee)
            'Hemant (23 May 2019) -- Start
            'Support Issue Id # 0003829(KCMU) : Allow the payroll transaction heads to be arranged due to Transaction head code on payroll reports.
            'dsList = objCRExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.DeductionForEmployee)
            dsList = objCRExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.DeductionForEmployee, , " Order By Code ")
            'Hemant (23 May 2019) -- End
            'Sohail (29 Mar 2017) -- End
            If dsList.Tables("Expense").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("Expense").Rows
                    dCol = New DataColumn("ColumnCR" & dtRow.Item("Id"))
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = dtRow.Item("name")

                    dtFinalTable.Columns.Add(dCol)
                Next
            End If
            'Sohail (12 Nov 2014) -- End

            ''------------------ Add Loan Advance Savings
            'dCol = New DataColumn("LAS")
            'dCol.DefaultValue = 0
            'dCol.Caption = "Loan/Advance/Savings"
            'dCol.DataType = System.Type.GetType("System.Decimal")
            'dtFinalTable.Columns.Add(dCol)

            '------------ Loan
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'dCol = New DataColumn("Loan")
            ''S.SANDEEP [ 29 SEP 2014 ] -- START
            ''dCol.Caption = "Loan"
            'dCol.Caption = Language.getMessage(mstrModuleName, 46, "Loan")
            ''S.SANDEEP [ 29 SEP 2014 ] -- END
            'dCol.DefaultValue = 0

            ''Pinkal (14-Dec-2012) -- Start
            ''Enhancement : TRA Changes
            ''dCol.DataType = System.Type.GetType("System.String")
            'dCol.DataType = System.Type.GetType("System.Decimal")
            ''Pinkal (14-Dec-2012) -- End

            'dtFinalTable.Columns.Add(dCol)
            If mblnShowLoansInSeparateColumns = False Then
            dCol = New DataColumn("Loan")
            dCol.Caption = Language.getMessage(mstrModuleName, 46, "Loan")
            dCol.DefaultValue = 0
                dCol.DataType = System.Type.GetType("System.Decimal")

                dtFinalTable.Columns.Add(dCol)
            Else
                Dim objLoanScheme As New clsLoan_Scheme
                'Hemant (23 May 2019) -- Start
                'Support Issue Id # 0003829(KCMU) : Allow the payroll transaction heads to be arranged due to Transaction head code on payroll reports.
                'dsList = objLoanScheme.getComboList(False, "Loan")
                dsList = objLoanScheme.getComboList(False, "Loan", , , , , " Order by Code")
                'Hemant (23 May 2019) -- End
                For Each dtRow As DataRow In dsList.Tables("Loan").Rows
                    dCol = New DataColumn("ColumnLoan" & dtRow.Item("LoanSchemeUnkId"))
            dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = dtRow.Item("name")

            dtFinalTable.Columns.Add(dCol)
                Next
            End If
            'Sohail (03 Feb 2016) -- End
           

            '------------ Advance
            dCol = New DataColumn("Advance")
            'S.SANDEEP [ 29 SEP 2014 ] -- START
            'dCol.Caption = "Advance"
            dCol.Caption = Language.getMessage(mstrModuleName, 47, "Advance")
            'S.SANDEEP [ 29 SEP 2014 ] -- END
            dCol.DefaultValue = 0

            'Pinkal (14-Dec-2012) -- Start
            'Enhancement : TRA Changes
            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")
            'Pinkal (14-Dec-2012) -- End
            dtFinalTable.Columns.Add(dCol)

            '------------ Savings
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'dCol = New DataColumn("Savings")
            ''S.SANDEEP [ 29 SEP 2014 ] -- START
            ''dCol.Caption = "Savings"
            'dCol.Caption = Language.getMessage(mstrModuleName, 48, "Savings")
            ''S.SANDEEP [ 29 SEP 2014 ] -- END
            'dCol.DefaultValue = 0

            ''Pinkal (14-Dec-2012) -- Start
            ''Enhancement : TRA Changes
            ''dCol.DataType = System.Type.GetType("System.String")
            'dCol.DataType = System.Type.GetType("System.Decimal")
            ''Pinkal (14-Dec-2012) -- End


            'dtFinalTable.Columns.Add(dCol)
            If mblnShowSavingsInSeparateColumns = False Then
            dCol = New DataColumn("Savings")
            dCol.Caption = Language.getMessage(mstrModuleName, 48, "Savings")
            dCol.DefaultValue = 0
                dCol.DataType = System.Type.GetType("System.Decimal")

                dtFinalTable.Columns.Add(dCol)
            Else
                Dim objSavingScheme As New clsSavingScheme
                'Hemant (23 May 2019) -- Start
                'Support Issue Id # 0003829(KCMU) : Allow the payroll transaction heads to be arranged due to Transaction head code on payroll reports.
                'dsList = objSavingScheme.getComboList(False, "Saving")
                dsList = objSavingScheme.getComboList(False, "Saving", , , " Order By Code ")
                'Hemant (23 May 2019) -- End
                For Each dtRow As DataRow In dsList.Tables("Saving").Rows
                    dCol = New DataColumn("ColumnSavings" & dtRow.Item("savingschemeunkid"))
            dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = dtRow.Item("name")

            dtFinalTable.Columns.Add(dCol)
                Next
            End If
            'Sohail (03 Feb 2016) -- End

            '------------------ Add Total Deduction Column
            dCol = New DataColumn("TDD")
            dCol.DefaultValue = 0
            'S.SANDEEP [ 29 SEP 2014 ] -- START
            'dCol.Caption = "Total Deduction"
            dCol.Caption = Language.getMessage(mstrModuleName, 49, "Total Deduction")
            'S.SANDEEP [ 29 SEP 2014 ] -- END

            'Pinkal (14-Dec-2012) -- Start
            'Enhancement : TRA Changes
            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")
            'Pinkal (14-Dec-2012) -- End


            dtFinalTable.Columns.Add(dCol)

            '------------------ Add Net Pay Column
            dCol = New DataColumn("NetPay")
            dCol.DefaultValue = 0
            'S.SANDEEP [ 29 SEP 2014 ] -- START
            'dCol.Caption = "Net Pay"
            dCol.Caption = Language.getMessage(mstrModuleName, 50, "Net Pay")
            'S.SANDEEP [ 29 SEP 2014 ] -- END

            'Pinkal (14-Dec-2012) -- Start
            'Enhancement : TRA Changes
            'dCol.DataType = System.Type.GetType("System.String")
            dCol.DataType = System.Type.GetType("System.Decimal")
            'Pinkal (14-Dec-2012) -- End


            dtFinalTable.Columns.Add(dCol)

            'Sohail (18 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            '------------------ Add Net B/F Column
            dCol = New DataColumn("Openingbalance")
            dCol.DefaultValue = 0
            'S.SANDEEP [ 29 SEP 2014 ] -- START
            'dCol.Caption = "Net B/F"
            dCol.Caption = Language.getMessage(mstrModuleName, 51, "Net B/F")
            'S.SANDEEP [ 29 SEP 2014 ] -- END
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)

            '------------------ Add Total Net Pay Column
            dCol = New DataColumn("TotNetPay")
            dCol.DefaultValue = 0
            'S.SANDEEP [ 29 SEP 2014 ] -- START
            'dCol.Caption = "Total Net Pay"
            dCol.Caption = Language.getMessage(mstrModuleName, 52, "Total Net Pay")
            'S.SANDEEP [ 29 SEP 2014 ] -- END
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)
            'Sohail (18 Jun 2013) -- End

            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            '------------------ Add Net Pay Rounding Adjustment Column
            Dim objHead As New clsTransactionHead
            Dim intNetPayRoundingAdjustmentID As Integer = objHead.GetNetPayRoundingAdjustmentHeadID()
            If intNetPayRoundingAdjustmentID > 0 Then
                objHead._Tranheadunkid(mstrCurrentDatabaseName) = intNetPayRoundingAdjustmentID
                dCol = New DataColumn("Column" & intNetPayRoundingAdjustmentID.ToString)
                dCol.DefaultValue = 0
                dCol.Caption = objHead._Trnheadname
                dCol.DataType = System.Type.GetType("System.Decimal")
                dtFinalTable.Columns.Add(dCol)
            End If
            'Sohail (18 Apr 2016) -- End

            'Sohail (12 Sep 2014) -- Start
            'Enhancement - Include Employer Contribution on Payroll Report.
            If mblnIncludeEmployerContribution = True Then
                StrQ = "SELECT " & _
                      "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
                      ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
                      "FROM " & mstrCurrentDatabaseName & "..prtranhead_master " & _
                      "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
                      "AND prtranhead_master.trnheadtype_id IN (" & enTranHeadType.EmployersStatutoryContributions & ") ORDER BY ISNULL(prtranhead_master.trnheadcode,'') "
                'Hemant (23 May 2019) -- [ORDER BY ISNULL(prtranhead_master.trnheadname --> ORDER BY ISNULL(prtranhead_master.trnheadcode]
                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If dsList.Tables("List").Rows.Count > 0 Then
                    For Each dtRow As DataRow In dsList.Tables("List").Rows
                        dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
                        dCol.DataType = System.Type.GetType("System.Decimal")

                        dCol.Caption = dtRow.Item("Tname")
                        dCol.DefaultValue = 0
                        dtFinalTable.Columns.Add(dCol)
                    Next
                End If
            End If
            'Sohail (12 Sep 2014) -- End

            'Pinkal (13-Oct-2014) -- Start
            'Enhancement -  SEPERATE BANK,ACCOUNT NO AND PAYMENT DETAIL COLUMN REQUESTED BY MR.ANDREW AND MR.RUTTA
            dCol = New DataColumn("Bank")
            dCol.Caption = Language.getMessage(mstrModuleName, 65, "Bank")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("AccountNo")
            dCol.Caption = Language.getMessage(mstrModuleName, 66, "Account #")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)
            'Pinkal (13-Oct-2014) -- End



            'Sohail (20 Aug 2014) -- Start
            'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
            dCol = New DataColumn("PaymentDetail")
            'S.SANDEEP [ 29 SEP 2014 ] -- START
            'dCol.Caption = "Payment Detail"
            dCol.Caption = Language.getMessage(mstrModuleName, 44, "Payment Detail")
            'S.SANDEEP [ 29 SEP 2014 ] -- END
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)
            'Sohail (20 Aug 2014) -- End



            'Sohail (14 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'Sohail (25 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
            'Sohail (25 Mar 2013) -- End
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (14 Aug 2012) -- End
            'Sohail (01 Mar 2017) -- Start
            'TRA Issue - 64.1 - JV Dr and Cr balance were not matching due to round off function used with default 2 decimals and jv total was not matching with payroll summary gross pay and payroll report net pay.
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            'Sohail (29 Nov 2017) -- End
            'Sohail (01 Mar 2017) -- End
            'Sohail (03 Nov 2021) -- Start
            'HJIF Issue : HJIF-AH-4234 : TRANSACTION HEADS PROBLEM NSSF (Bring difference of 0.003 when you compare with 10% of gross and total of NSSF).
            decDecimalPlaces = 6
            'Sohail (03 Nov 2021) -- End

            'Sohail (20 Jan 2020) -- Start
            'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
            StrQ = ""
            For Each key In mdicYearDBName

                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , key.Value)
                If mblnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, key.Value, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, key.Value)

                StrQ &= "SELECT   hremployee_master.employeeunkid "
                StrQ &= "INTO #" & key.Value & " "
                StrQ &= "FROM  " & key.Value & "..hremployee_master "

                StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE 1 = 1 "

                If mintEmployeeId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mintBranchId > 0 Then
                    StrQ &= " AND T.stationunkid = @BranchId "
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

            Next
            'Sohail (20 Jan 2020) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT  PeriodId " & _
            '              ", period_name AS PeriodName " & _
            '              ", EmpId " & _
            '              ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ') AS EmpName " & _
            '              ", hremployee_master.employeecode AS Code " & _
            '              ", TranId " & _
            '              ", Amount " & _
            '              ", Mid "

            StrQ &= "; SELECT  PeriodId " & _
                          ", ISNULL(period_name, '') AS PeriodName " & _
                          ", EmpId "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ') AS EmpName "
            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            StrQ &= ", hremployee_master.employeecode AS Code " & _
                          ", TranId "
            If mintReportId = 0 Then
                StrQ &= ", (Amount * " & mdecEx_Rate & ") AS Amount " & _
                        ", Mid " & _
                        ", (Openingbalance * " & mdecEx_Rate & ") AS Openingbalance "
                'Sohail (18 Jun 2013) - [Openingbalance]
            Else
                StrQ &= ", Amount " & _
                          ", Mid " & _
                          ", Openingbalance "
                'Sohail (18 Jun 2013) - [Openingbalance]
            End If
            'S.SANDEEP [ 11 SEP 2012 ] -- END


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",'' AS Id, '' AS GName "
            End If

            'Sohail (08 Dec 2012) -- Start
            'TRA - ENHANCEMENT
            'StrQ &= "FROM    ( SELECT DISTINCT " & _
            '                            "payperiodunkid AS PeriodId " & _
            '                          ", prtnaleave_tran.employeeunkid AS EmpId " & _
            '                          ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
            '                          ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
            '                          ", 10 AS Mid " & _
            '                  "FROM      prpayrollprocess_tran " & _
            '                            "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                            "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                            "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
            '                                                      "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '                  "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '                            "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                            "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
            StrQ &= "FROM    ( "

            'Sohail (20 Aug 2014) -- Start
            'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'For i As Integer = 0 To marrDatabaseName.Count - 1
            '   mstrFromDatabaseName = marrDatabaseName(i)
            Dim i As Integer = -1
            For Each key In mdicYearDBName
                mstrFromDatabaseName = key.Value
                i += 1
                'Sohail (21 Aug 2015) -- End

                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                ''Sohail (21 Aug 2015) -- Start
                ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                'xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrFromDatabaseName)
                ''Sohail (13 Feb 2016) -- Start
                ''Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
                ''Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                'If mblnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                ''Sohail ((13 Feb 2016) -- End
                'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrFromDatabaseName)
                ''Sohail (21 Aug 2015) -- End
                'Sohail (20 Jan 2020) -- End

                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If
                'Sohail (20 Aug 2014) -- End


                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                If mintReportId = 2 Then 'Employee Payroll Summary

                    StrInnerQ &= " SELECT DISTINCT " & _
                                                "0 AS PeriodId " & _
                                              ", prtnaleave_tran.employeeunkid AS EmpId " & _
                                              ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                                              ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                              ", CASE prtranhead_master.trnheadtype_id WHEN " & enTranHeadType.Informational & " THEN 13 ELSE 10 END AS Mid " & _
                                              ", SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                                      "FROM      " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                                "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                                "LEFT JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (18 Apr 2016) - [10 AS Mid] = [CASE prtranhead_master.trnheadtype_id WHEN " & enTranHeadType.Informational & " THEN 13 ELSE 10 END AS Mid]

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtranhead_master.isvoid = 0 " & _
                                                "AND (prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR prtranhead_master.typeof_id = " & enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT & ") " & _
                                                "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                    'Sohail (18 Apr 2016) - [AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ] = [AND (prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR prtranhead_master.typeof_id = " & enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT & ") ]
                Else
                    'Sohail (01 Sep 2014) -- End

                    StrInnerQ &= " SELECT DISTINCT " & _
                                                "payperiodunkid AS PeriodId " & _
                                              ", prtnaleave_tran.employeeunkid AS EmpId " & _
                                              ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                                              ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                              ", CASE prtranhead_master.trnheadtype_id WHEN " & enTranHeadType.Informational & " THEN 13 ELSE 10 END AS Mid " & _
                                              ", openingbalance AS Openingbalance " & _
                                      "FROM      " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                                "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                                "LEFT JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (18 Apr 2016) - [10 AS Mid] = [CASE prtranhead_master.trnheadtype_id WHEN " & enTranHeadType.Informational & " THEN 13 ELSE 10 END AS Mid]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtranhead_master.isvoid = 0 " & _
                                                "AND (prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR prtranhead_master.typeof_id = " & enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT & ") " & _
                                                "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                    'Sohail (18 Apr 2016) - [AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ] = [AND (prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR prtranhead_master.typeof_id = " & enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT & ") ]
                    'Sohail (18 Jun 2013) - [openingbalance]
                    'Sohail (25 Mar 2013) - [mstrCurrentDatabaseName]
                    'Sohail (08 Dec 2012) -- End
                End If 'Sohail (01 Sep 2014)



                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If mintEmployeeId > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                '    'Sohail (08 Dec 2012) -- End
                '    objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                'End If

                'If mintBranchId > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                '    'Sohail (21 Aug 2015) -- Start
                '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                '    'Sohail (21 Aug 2015) -- End
                '    'Sohail (08 Dec 2012) -- End
                '    objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
                'End If

                ''Sohail (21 Aug 2015) -- Start
                ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If blnApplyUserAccessFilter = True Then
                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrInnerQ &= " AND " & xUACFiltrQry
                '    End If
                'End If

                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrInnerQ &= xDateFilterQry
                '    End If
                'End If
                ''Sohail (21 Aug 2015) -- End


                ''Pinkal (27-Feb-2013) -- Start
                ''Enhancement : TRA Changes
                'If mstrAdvance_Filter.Trim.Length > 0 Then
                '    StrInnerQ &= " AND " & mstrAdvance_Filter
                'End If
                ''Pinkal (27-Feb-2013) -- End
                'Sohail (20 Jan 2020) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '    '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '    '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '    '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                '    'Sohail (08 Dec 2012) -- End
                'End If
                'Sohail (21 Aug 2015) -- End

                'Sohail (31 Jan 2014) -- Start
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= UserAccessLevel._AccessLevelFilterString
                '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                '    'Sohail (08 Dec 2012) -- End
                'End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End
                'Sohail (31 Jan 2014) -- End


                'Sohail (08 Dec 2012) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " GROUP BY payperiodunkid " & _
                '                ", prtnaleave_tran.employeeunkid " & _
                '                ", prpayrollprocess_tran.tranheadunkid " & _
                '        "UNION ALL " & _
                '        "SELECT  payperiodunkid AS PeriodId " & _
                '              ", hremployee_master.employeeunkid AS EmpId " & _
                '              ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                '              ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                '              ", 0 AS Mid " & _
                '        "FROM    prpayrollprocess_tran " & _
                '                "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                '                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                '                                          "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '        "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '                "AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & " ) " & _
                '                "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                If mintReportId = 2 Then 'Employee Payroll Summary

                    StrInnerQ &= " GROUP BY prtnaleave_tran.employeeunkid " & _
                                    ", prpayrollprocess_tran.tranheadunkid " & _
                                    ", prtranhead_master.trnheadtype_id " & _
                            "UNION ALL " & _
                            "SELECT  0 AS PeriodId " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                  ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                  ", 0 AS Mid " & _
                                  ", SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                           "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                   "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                    "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (18 Apr 2016) - [GROUP BY prtranhead_master.trnheadtype_id]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND prtranhead_master.isvoid = 0 " & _
                                    "AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & " ) " & _
                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                Else
                    'Sohail (01 Sep 2014) -- End
                    StrInnerQ &= " GROUP BY payperiodunkid " & _
                                    ", prtnaleave_tran.employeeunkid " & _
                                    ", prpayrollprocess_tran.tranheadunkid " & _
                                    ", prtranhead_master.trnheadtype_id " & _
                                    ", openingbalance " & _
                            "UNION ALL " & _
                            "SELECT  payperiodunkid AS PeriodId " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                  ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                  ", 0 AS Mid " & _
                                  ", openingbalance AS Openingbalance " & _
                           "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                   "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                    "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (18 Apr 2016) - [GROUP BY prtranhead_master.trnheadtype_id]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND prtranhead_master.isvoid = 0 " & _
                                    "AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & " ) " & _
                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                End If 'Sohail (01 Sep 2014)
                'Sohail (18 Jun 2013) - [openingbalance]
                'Sohail (25 Mar 2013) - [mstrCurrentDatabaseName]
                'Sohail (08 Dec 2012) -- End

                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If mintEmployeeId > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                '    'Sohail (08 Dec 2012) -- End
                'End If

                ''Sohail (21 Aug 2015) -- Start
                ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If blnApplyUserAccessFilter = True Then
                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrInnerQ &= " AND " & xUACFiltrQry
                '    End If
                'End If

                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrInnerQ &= xDateFilterQry
                '    End If
                'End If
                ''Sohail (21 Aug 2015) -- End

                ''Pinkal (27-Feb-2013) -- Start
                ''Enhancement : TRA Changes
                'If mstrAdvance_Filter.Trim.Length > 0 Then
                '    StrInnerQ &= " AND " & mstrAdvance_Filter
                'End If
                ''Pinkal (27-Feb-2013) -- End
                'Sohail (20 Jan 2020) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                '    'Sohail (08 Dec 2012) -- End
                'End If
                'Sohail (21 Aug 2015) -- End

                'Sohail (31 Jan 2014) -- Start
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= UserAccessLevel._AccessLevelFilterString
                '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                '    'Sohail (08 Dec 2012) -- End
                'End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End
                'Sohail (31 Jan 2014) -- End

                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If mintBranchId > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                '    'Sohail (21 Aug 2015) -- Start
                '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                '    'Sohail (21 Aug 2015) -- End
                '    'Sohail (08 Dec 2012) -- End
                'End If
                'Sohail (20 Jan 2020) -- End

                'Sohail (08 Dec 2012) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " GROUP BY payperiodunkid " & _
                '              ", hremployee_master.employeeunkid " & _
                '              ", prpayrollprocess_tran.tranheadunkid " & _
                '        "UNION ALL " & _
                '        "SELECT  payperiodunkid AS PeriodId " & _
                '              ", hremployee_master.employeeunkid AS EmpId " & _
                '              ", -1 AS TranId " & _
                '              ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                '              ", 1 AS Mid " & _
                '        "FROM    prpayrollprocess_tran " & _
                '                "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                '                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                "JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                '                                            "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                '        "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '                "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                '                "AND lnloan_advance_tran.isloan = 1 " & _
                '                "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                If mintReportId = 2 Then 'Employee Payroll Summary

                    'Sohail (03 Feb 2016) -- Start
                    'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                    'StrInnerQ &= " GROUP BY hremployee_master.employeeunkid " & _
                    '              ", prpayrollprocess_tran.tranheadunkid " & _
                    '        "UNION ALL " & _
                    '        "SELECT  0 AS PeriodId " & _
                    '              ", hremployee_master.employeeunkid AS EmpId " & _
                    '              ", -1 AS TranId " & _
                    '              ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                    '              ", 1 AS Mid " & _
                    '              ", SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                    '        "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                    '                "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    '                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    '                "LEFT JOIN " & mstrFromDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                "LEFT JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                    '                                            "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
                    StrInnerQ &= " GROUP BY #" & mstrFromDatabaseName & ".employeeunkid " & _
                                  ", prpayrollprocess_tran.tranheadunkid " & _
                            "UNION ALL " & _
                            "SELECT  0 AS PeriodId " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                  ", " & IIf(mblnShowLoansInSeparateColumns = True, "lnloan_advance_tran.loanschemeunkid", "-1").ToString & " AS TranId " & _
                                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                  ", 1 AS Mid " & _
                                  ", SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                            "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                    "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (03 Feb 2016) -- End

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND lnloan_advance_tran.isvoid = 0 " & _
                                    "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                    "AND lnloan_advance_tran.isloan = 1 " & _
                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                Else
                    'Sohail (01 Sep 2014) -- End

                    'Sohail (03 Feb 2016) -- Start
                    'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                    'StrInnerQ &= " GROUP BY payperiodunkid " & _
                    '              ", hremployee_master.employeeunkid " & _
                    '              ", prpayrollprocess_tran.tranheadunkid " & _
                    '              ", openingbalance " & _
                    '        "UNION ALL " & _
                    '        "SELECT  payperiodunkid AS PeriodId " & _
                    '              ", hremployee_master.employeeunkid AS EmpId " & _
                    '              ", -1 AS TranId " & _
                    '              ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                    '              ", 1 AS Mid " & _
                    '              ", openingbalance AS Openingbalance " & _
                    '        "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                    '                "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    '                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    '                "LEFT JOIN " & mstrFromDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                "LEFT JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                    '                                            "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
                    StrInnerQ &= " GROUP BY payperiodunkid " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid " & _
                                  ", prpayrollprocess_tran.tranheadunkid " & _
                                  ", openingbalance " & _
                            "UNION ALL " & _
                            "SELECT  payperiodunkid AS PeriodId " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                  ", " & IIf(mblnShowLoansInSeparateColumns = True, "lnloan_advance_tran.loanschemeunkid", "-1").ToString & " AS TranId " & _
                                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                  ", 1 AS Mid " & _
                                  ", openingbalance AS Openingbalance " & _
                            "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                    "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (03 Feb 2016) -- End

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND lnloan_advance_tran.isvoid = 0 " & _
                                    "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                    "AND lnloan_advance_tran.isloan = 1 " & _
                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                End If 'Sohail (01 Sep 2014)

                'Sohail (18 Jun 2013) - [openingbalance]
                'Sohail (25 Mar 2013) - [mstrCurrentDatabaseName]
                'Sohail (08 Dec 2012) -- End
                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If mintEmployeeId > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                '    'Sohail (08 Dec 2012) -- End
                'End If


                ''Pinkal (27-Feb-2013) -- Start
                ''Enhancement : TRA Changes
                'If mstrAdvance_Filter.Trim.Length > 0 Then
                '    StrInnerQ &= " AND " & mstrAdvance_Filter
                'End If
                ''Pinkal (27-Feb-2013) -- End

                ''Sohail (21 Aug 2015) -- Start
                ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If blnApplyUserAccessFilter = True Then
                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrInnerQ &= " AND " & xUACFiltrQry
                '    End If
                'End If

                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrInnerQ &= xDateFilterQry
                '    End If
                'End If
                ''Sohail (21 Aug 2015) -- End
                'Sohail (20 Jan 2020) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                '    'Sohail (08 Dec 2012) -- End
                'End If

                ''Sohail (31 Jan 2014) -- Start
                ''If UserAccessLevel._AccessLevel.Length > 0 Then
                ''    'Sohail (08 Dec 2012) -- Start
                ''    'TRA - ENHANCEMENT
                ''    'StrQ &= UserAccessLevel._AccessLevelFilterString
                ''    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                ''    'Sohail (08 Dec 2012) -- End
                ''End If
                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End
                'Sohail (31 Jan 2014) -- End

                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If mintBranchId > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                '    'Sohail (21 Aug 2015) -- Start
                '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                '    'Sohail (21 Aug 2015) -- End
                '    'Sohail (08 Dec 2012) -- End
                'End If
                'Sohail (20 Jan 2020) -- End

                'Sohail (08 Dec 2012) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " GROUP BY payperiodunkid " & _
                '              ", hremployee_master.employeeunkid " & _
                '        "UNION ALL " & _
                '        "SELECT  payperiodunkid AS PeriodId " & _
                '              ", hremployee_master.employeeunkid AS EmpId " & _
                '              ", -1 AS TranId " & _
                '              ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                '              ", 2 AS Mid " & _
                '        "FROM    prpayrollprocess_tran " & _
                '                "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                '                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                "JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                '                                            "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                '        "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '                "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                '                "AND lnloan_advance_tran.isloan = 0 " & _
                '                "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                If mintReportId = 2 Then 'Employee Payroll Summary

                    StrInnerQ &= " GROUP BY #" & mstrFromDatabaseName & ".employeeunkid " & _
                                        " " & IIf(mblnShowLoansInSeparateColumns = True, ", lnloan_advance_tran.loanschemeunkid", "").ToString & " " & _
                            "UNION ALL " & _
                            "SELECT  0 AS PeriodId " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                  ", -1 AS TranId " & _
                                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                  ", 2 AS Mid " & _
                                  ", SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                            "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                    "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (03 Feb 2016) - [" " & IIf(mblnShowLoansInSeparateColumns = True, ", lnloan_advance_tran.loanschemeunkid", "").ToString & " " & _]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND lnloan_advance_tran.isvoid = 0 " & _
                                    "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                    "AND lnloan_advance_tran.isloan = 0 " & _
                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                Else
                    'Sohail (01 Sep 2014) -- End
                    StrInnerQ &= " GROUP BY payperiodunkid " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid " & _
                                  ", openingbalance " & _
                                  " " & IIf(mblnShowLoansInSeparateColumns = True, ", lnloan_advance_tran.loanschemeunkid", "").ToString & " " & _
                            "UNION ALL " & _
                            "SELECT  payperiodunkid AS PeriodId " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                  ", -1 AS TranId " & _
                                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                  ", 2 AS Mid " & _
                                  ", openingbalance AS Openingbalance " & _
                            "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                    "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (03 Feb 2016) - [" " & IIf(mblnShowLoansInSeparateColumns = True, ", lnloan_advance_tran.loanschemeunkid", "").ToString & " " & _]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND lnloan_advance_tran.isvoid = 0 " & _
                                    "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                    "AND lnloan_advance_tran.isloan = 0 " & _
                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                End If 'Sohail (01 Sep 2014)
                'Sohail (18 Jun 2013) - [openingbalance]
                'Sohail (25 Mar 2013) - [mstrCurrentDatabaseName]
                'Sohail (08 Dec 2012) -- End

                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If mintEmployeeId > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                '    'Sohail (08 Dec 2012) -- End
                'End If


                ''Pinkal (27-Feb-2013) -- Start
                ''Enhancement : TRA Changes
                'If mstrAdvance_Filter.Trim.Length > 0 Then
                '    StrInnerQ &= " AND " & mstrAdvance_Filter
                'End If
                ''Pinkal (27-Feb-2013) -- End
                'Sohail (20 Jan 2020) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                '    'Sohail (08 Dec 2012) -- End
                'End If

                ''Sohail (31 Jan 2014) -- Start
                ''If UserAccessLevel._AccessLevel.Length > 0 Then
                ''    'Sohail (08 Dec 2012) -- Start
                ''    'TRA - ENHANCEMENT
                ''    'StrQ &= UserAccessLevel._AccessLevelFilterString
                ''    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                ''    'Sohail (08 Dec 2012) -- End
                ''End If
                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                ''Sohail (31 Jan 2014) -- End
                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If blnApplyUserAccessFilter = True Then
                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrInnerQ &= " AND " & xUACFiltrQry
                '    End If
                'End If

                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrInnerQ &= xDateFilterQry
                '    End If
                'End If
                'Sohail (20 Jan 2020) -- End
                'Sohail (21 Aug 2015) -- End

                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If mintBranchId > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                '    'Sohail (21 Aug 2015) -- Start
                '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                '    'Sohail (21 Aug 2015) -- End
                '    'Sohail (08 Dec 2012) -- End
                'End If
                'Sohail (20 Jan 2020) -- End

                'Sohail (08 Dec 2012) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " GROUP BY payperiodunkid " & _
                '              ", hremployee_master.employeeunkid " & _
                '            "	UNION ALL " & _
                '            "SELECT  payperiodunkid AS PeriodId " & _
                '                  ", hremployee_master.employeeunkid AS EmpId " & _
                '                  ", -1 AS TranId " & _
                '                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                '                  ", 3 AS Mid " & _
                '            "FROM    prpayrollprocess_tran " & _
                '                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '            "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '                    "AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                '                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                If mintReportId = 2 Then 'Employee Payroll Summary
                    'Sohail (03 Feb 2016) -- Start
                    'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                    'StrInnerQ &= " GROUP BY hremployee_master.employeeunkid " & _
                    '            "	UNION ALL " & _
                    '            "SELECT  0 AS PeriodId " & _
                    '                  ", hremployee_master.employeeunkid AS EmpId " & _
                    '                  ", -1 AS TranId " & _
                    '                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                    '                  ", 3 AS Mid " & _
                    '                  ", SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                    '            "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                    '                    "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    '                    "LEFT JOIN " & mstrFromDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "
                    StrInnerQ &= " GROUP BY #" & mstrFromDatabaseName & ".employeeunkid " & _
                                "	UNION ALL " & _
                                "SELECT  0 AS PeriodId " & _
                                      ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                      ", " & IIf(mblnShowSavingsInSeparateColumns = True, "svsaving_tran.savingschemeunkid", "-1").ToString & " AS TranId " & _
                                      ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                      ", 3 AS Mid " & _
                                      ", SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                                "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                        "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..svsaving_tran ON svsaving_tran.savingtranunkid = prpayrollprocess_tran.savingtranunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (03 Feb 2016) -- End

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND svsaving_tran.isvoid = 0 " & _
                                        "AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                Else
                    'Sohail (01 Sep 2014) -- End

                    'Sohail (03 Feb 2016) -- Start
                    'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                    'StrInnerQ &= " GROUP BY payperiodunkid " & _
                    '              ", hremployee_master.employeeunkid " & _
                    '              ", openingbalance " & _
                    '            "	UNION ALL " & _
                    '            "SELECT  payperiodunkid AS PeriodId " & _
                    '                  ", hremployee_master.employeeunkid AS EmpId " & _
                    '                  ", -1 AS TranId " & _
                    '                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                    '                  ", 3 AS Mid " & _
                    '                  ", openingbalance AS Openingbalance " & _
                    '            "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                    '                    "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    '                    "LEFT JOIN " & mstrFromDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "
                    StrInnerQ &= " GROUP BY payperiodunkid " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid " & _
                                  ", openingbalance " & _
                                "	UNION ALL " & _
                                "SELECT  prtnaleave_tran.payperiodunkid AS PeriodId " & _
                                      ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                      ", " & IIf(mblnShowSavingsInSeparateColumns = True, "svsaving_tran.savingschemeunkid", "-1").ToString & " AS TranId " & _
                                      ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                      ", 3 AS Mid " & _
                                      ", openingbalance AS Openingbalance " & _
                                "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                        "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..svsaving_tran ON svsaving_tran.savingtranunkid = prpayrollprocess_tran.savingtranunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (03 Feb 2016) -- End

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND svsaving_tran.isvoid = 0 " & _
                                        "AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                End If 'Sohail (01 Sep 2014)
                'Sohail (18 Jun 2013) - [openingbalance]
                'Sohail (25 Mar 2013) - [mstrCurrentDatabaseName]
                'Sohail (08 Dec 2012) -- End

                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If mintEmployeeId > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                '    'Sohail (08 Dec 2012) -- End
                'End If

                ''Pinkal (27-Feb-2013) -- Start
                ''Enhancement : TRA Changes
                'If mstrAdvance_Filter.Trim.Length > 0 Then
                '    StrInnerQ &= " AND " & mstrAdvance_Filter
                'End If
                ''Pinkal (27-Feb-2013) -- End
                'Sohail (20 Jan 2020) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '    '      " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                '    'Sohail (08 Dec 2012) -- End
                'End If

                ''Sohail (31 Jan 2014) -- Start
                ''If UserAccessLevel._AccessLevel.Length > 0 Then
                ''    'Sohail (08 Dec 2012) -- Start
                ''    'TRA - ENHANCEMENT
                ''    'StrQ &= UserAccessLevel._AccessLevelFilterString
                ''    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                ''    'Sohail (08 Dec 2012) -- End
                ''End If
                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                ''Sohail (31 Jan 2014) -- End
                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If blnApplyUserAccessFilter = True Then
                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrInnerQ &= " AND " & xUACFiltrQry
                '    End If
                'End If

                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrInnerQ &= xDateFilterQry
                '    End If
                'End If
                ''Sohail (21 Aug 2015) -- End

                'If mintBranchId > 0 Then
                '    'Sohail (08 Dec 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'StrQ &= " AND hremployee_master.stationunkid = @BranchId"
                '    'Sohail (21 Aug 2015) -- Start
                '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                '    'Sohail (21 Aug 2015) -- End
                '    'Sohail (08 Dec 2012) -- End
                'End If
                'Sohail (20 Jan 2020) -- End

                'Sohail (08 Dec 2012) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " GROUP BY payperiodunkid " & _
                '              ", hremployee_master.employeeunkid "
                'StrQ &= ") AS payroll " & _
                '        "JOIN hremployee_master ON payroll.EmpId = hremployee_master.employeeunkid " & _
                '        "JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = payroll.PeriodId " & _
                '                                     "AND cfcommon_period_tran.isactive = 1 " & _
                '                                     "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "
                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                If mintReportId = 2 Then 'Employee Payroll Summary
                    StrInnerQ &= " GROUP BY #" & mstrFromDatabaseName & ".employeeunkid " & _
                                    " " & IIf(mblnShowSavingsInSeparateColumns = True, ", svsaving_tran.savingschemeunkid", "").ToString & " "
                    'Sohail (03 Feb 2016) - [" " & IIf(mblnShowSavingsInSeparateColumns = True, "svsaving_tran.savingschemeunkid", "").ToString & " "]
                Else
                    'Sohail (01 Sep 2014) -- End
                    StrInnerQ &= " GROUP BY prtnaleave_tran.payperiodunkid " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid " & _
                                  ", openingbalance " & _
                                  " " & IIf(mblnShowSavingsInSeparateColumns = True, ", svsaving_tran.savingschemeunkid", "").ToString & " "
                    'Sohail (03 Feb 2016) - [" " & IIf(mblnShowSavingsInSeparateColumns = True, "svsaving_tran.savingschemeunkid", "").ToString & " "]
                End If 'Sohail (01 Sep 2014)
                'Sohail (18 Jun 2013) - [openingbalance]

                'Sohail (21 Jun 2013) -- Start
                'TRA - ENHANCEMENT - Pay Per Activity
                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                If mintReportId = 2 Then 'Employee Payroll Summary
                    StrInnerQ &= "UNION ALL " & _
                                "SELECT  0 AS PeriodId " & _
                                      ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                      ", practivity_master.activityunkid AS TranId " & _
                                      ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                      ", CASE practivity_master.trnheadtype_id WHEN 1 THEN 11 ELSE 12 END AS Mid " & _
                                      ", SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                                "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                        "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..practivity_master ON practivity_master.activityunkid =  prpayrollprocess_tran.activityunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND practivity_master.isactive = 1 " & _
                                        "AND prpayrollprocess_tran.activityunkid > 0 " & _
                                            "AND practivity_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") " & _
                                        "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                Else
                    'Sohail (01 Sep 2014) -- End
                    StrInnerQ &= "UNION ALL " & _
                            "SELECT  payperiodunkid AS PeriodId " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                  ", practivity_master.activityunkid AS TranId " & _
                                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                  ", CASE practivity_master.trnheadtype_id WHEN 1 THEN 11 ELSE 12 END AS Mid " & _
                                  ", openingbalance AS Openingbalance " & _
                            "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                    "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..practivity_master ON practivity_master.activityunkid =  prpayrollprocess_tran.activityunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND practivity_master.isactive = 1 " & _
                                    "AND prpayrollprocess_tran.activityunkid > 0 " & _
                                        "AND practivity_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") " & _
                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                    'Sohail (19 Aug 2014) - [AND practivity_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") ]
                End If 'Sohail (01 Sep 2014) -- End


                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If mintEmployeeId > 0 Then
                '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                'End If

                'If mstrAdvance_Filter.Trim.Length > 0 Then
                '    StrInnerQ &= " AND " & mstrAdvance_Filter
                'End If
                'Sohail (20 Jan 2020) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If
                ''Sohail (31 Jan 2014) -- Start
                ''If UserAccessLevel._AccessLevel.Length > 0 Then
                ''    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                ''End If
                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                ''Sohail (31 Jan 2014) -- End
                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If blnApplyUserAccessFilter = True Then
                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrInnerQ &= " AND " & xUACFiltrQry
                '    End If
                'End If

                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrInnerQ &= xDateFilterQry
                '    End If
                'End If
                ''Sohail (21 Aug 2015) -- End

                'If mintBranchId > 0 Then
                '    'Sohail (21 Aug 2015) -- Start
                '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                '    'Sohail (21 Aug 2015) -- End
                'End If
                'Sohail (20 Jan 2020) -- End

                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                If mintReportId = 2 Then 'Employee Payroll Summary
                    StrInnerQ &= " GROUP BY #" & mstrFromDatabaseName & ".employeeunkid " & _
                                  ", practivity_master.activityunkid " & _
                                  ", practivity_master.trnheadtype_id " & _
                                  ", prpayrollprocess_tran.activityunkid "
                Else
                    StrInnerQ &= " GROUP BY payperiodunkid " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid " & _
                                  ", practivity_master.activityunkid " & _
                                  ", practivity_master.trnheadtype_id " & _
                                  ", prpayrollprocess_tran.activityunkid " & _
                                  ", openingbalance "
                End If
                'Sohail (01 Sep 2014) -- End

                'Sohail (21 Jun 2013) -- End

                'Sohail (12 Nov 2014) -- Start
                'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                If mintReportId = 2 Then 'Employee Payroll Summary
                    StrInnerQ &= "UNION ALL " & _
                                "SELECT  0 AS PeriodId " & _
                                      ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                      ", ISNULL(cmexpense_master.expenseunkid, crretireexpense.expenseunkid) AS TranId " & _
                                      ", SUM(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                      ", CASE WHEN cmexpense_master.expenseunkid > 0 THEN CASE cmexpense_master.trnheadtype_id WHEN 1 THEN 14 ELSE 15 END ELSE CASE WHEN prpayrollprocess_tran.add_deduct = 1 THEN 14 ELSE 15 END END AS Mid " & _
                                      ", SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                                "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                        "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_retirement_master ON cmretire_process_tran.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                    'Sohail (27 Apr 2021) - [SUM(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(prpayrollprocess_tran.amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
                                        "AND ISNULL(cmexpense_master.isactive, 1) = 1 " & _
                                        "AND ISNULL(cmretire_process_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(cmclaim_retirement_master.isvoid, 0) = 0 " & _
                                        "AND ISNULL(crretireexpense.isactive, 1) = 1 " & _
                                        "AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) " & _
                                        "AND 1 = CASE WHEN prpayrollprocess_tran.crprocesstranunkid > 0 AND cmexpense_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") THEN 1 WHEN prpayrollprocess_tran.crretirementprocessunkid > 0 THEN 1 ELSE 0 END " & _
                                        "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                Else
                    StrInnerQ &= "UNION ALL " & _
                            "SELECT  payperiodunkid AS PeriodId " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                  ", ISNULL(cmexpense_master.expenseunkid, crretireexpense.expenseunkid) AS TranId " & _
                                  ", SUM(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                  ", CASE WHEN cmexpense_master.expenseunkid > 0 THEN CASE cmexpense_master.trnheadtype_id WHEN 1 THEN 14 ELSE 15 END ELSE CASE WHEN prpayrollprocess_tran.add_deduct = 1 THEN 14 ELSE 15 END END AS Mid " & _
                                  ", openingbalance AS Openingbalance " & _
                            "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                    "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                    "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_retirement_master ON cmretire_process_tran.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                    'Sohail (27 Apr 2021) - [SUM(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(prpayrollprocess_tran.amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    ''Sohail (21 Aug 2015) -- End
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
                                    "AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
                                    "AND ISNULL(cmexpense_master.isactive, 1) = 1 " & _
                                    "AND ISNULL(cmretire_process_tran.isvoid, 0) = 0 " & _
                                    "AND ISNULL(cmclaim_retirement_master.isvoid, 0) = 0 " & _
                                    "AND ISNULL(crretireexpense.isactive, 1) = 1 " & _
                                    "AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) " & _
                                    "AND 1 = CASE WHEN prpayrollprocess_tran.crprocesstranunkid > 0 AND cmexpense_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") THEN 1 WHEN prpayrollprocess_tran.crretirementprocessunkid > 0 THEN 1 ELSE 0 END " & _
                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                End If

                'Sohail (13 Jan 2020) -- Start
                'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
                'If mintEmployeeId > 0 Then
                '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                'End If

                'If mstrAdvance_Filter.Trim.Length > 0 Then
                '    StrInnerQ &= " AND " & mstrAdvance_Filter
                'End If
                'Sohail (13 Jan 2020) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If blnApplyUserAccessFilter = True Then
                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrInnerQ &= " AND " & xUACFiltrQry
                '    End If
                'End If

                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrInnerQ &= xDateFilterQry
                '    End If
                'End If
                ''Sohail (21 Aug 2015) -- End

                'If mintBranchId > 0 Then
                '    'Sohail (21 Aug 2015) -- Start
                '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                '    'Sohail (21 Aug 2015) -- End
                'End If
                'Sohail (20 Jan 2020) -- End

                If mintReportId = 2 Then 'Employee Payroll Summary
                    StrInnerQ &= " GROUP BY #" & mstrFromDatabaseName & ".employeeunkid " & _
                                  ", cmexpense_master.expenseunkid " & _
                                  ", cmexpense_master.trnheadtype_id " & _
                                  ", crretireexpense.expenseunkid " & _
                                  ", prpayrollprocess_tran.add_deduct "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                Else
                    StrInnerQ &= " GROUP BY payperiodunkid " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid " & _
                                  ", cmexpense_master.expenseunkid " & _
                                  ", cmexpense_master.trnheadtype_id " & _
                                  ", crretireexpense.expenseunkid " & _
                                  ", prpayrollprocess_tran.add_deduct " & _
                                  ", openingbalance "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                End If
                'Sohail (12 Nov 2014) -- End

                'Sohail (12 Sep 2014) -- Start
                'Enhancement - Include Employer Contribution on Payroll Report.
                If mblnIncludeEmployerContribution = True Then
                    If mintReportId = 2 Then 'Employee Payroll Summary

                        StrInnerQ &= "    UNION ALL " & _
                                         "SELECT DISTINCT " & _
                                                    "0 AS PeriodId " & _
                                                  ", prtnaleave_tran.employeeunkid AS EmpId " & _
                                                  ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                                                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                                  ", 13 AS Mid " & _
                                                  ", SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                                          "FROM      " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
                        'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                        'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]

                        'Sohail (20 Jan 2020) -- Start
                        'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'StrInnerQ &= "LEFT JOIN " & _
                        '            "( " & _
                        '            "    SELECT " & _
                        '            "         stationunkid " & _
                        '            "        ,deptgroupunkid " & _
                        '            "        ,departmentunkid " & _
                        '            "        ,sectiongroupunkid " & _
                        '            "        ,sectionunkid " & _
                        '            "        ,unitgroupunkid " & _
                        '            "        ,unitunkid " & _
                        '            "        ,teamunkid " & _
                        '            "        ,classgroupunkid " & _
                        '            "        ,classunkid " & _
                        '            "        ,employeeunkid " & _
                        '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                        '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                        'If xDateJoinQry.Trim.Length > 0 Then
                        '    StrInnerQ &= xDateJoinQry
                        'End If

                        ''S.SANDEEP [15 NOV 2016] -- START
                        ''If xUACQry.Trim.Length > 0 Then
                        ''    StrInnerQ &= xUACQry
                        ''End If
                        'If blnApplyUserAccessFilter = True Then
                        'If xUACQry.Trim.Length > 0 Then
                        '    StrInnerQ &= xUACQry
                        'End If
                        'End If
                        ''S.SANDEEP [15 NOV 2016] -- END

                        'If xAdvanceJoinQry.Trim.Length > 0 Then
                        '    StrInnerQ &= xAdvanceJoinQry
                        'End If
                        ''Sohail (21 Aug 2015) -- End
                        'Sohail (20 Jan 2020) -- End

                        StrInnerQ &= "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployersStatutoryContributions & " " & _
                                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                    Else

                        StrInnerQ &= "     UNION ALL " & _
                                          "SELECT DISTINCT " & _
                                                    "payperiodunkid AS PeriodId " & _
                                                  ", prtnaleave_tran.employeeunkid AS EmpId " & _
                                                  ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                                                  ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                                  ", 13 AS Mid " & _
                                                  ", openingbalance AS Openingbalance " & _
                                          "FROM      " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                                    "LEFT JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
                        'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                        'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]

                        'Sohail (20 Jan 2020) -- Start
                        'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'StrInnerQ &= "LEFT JOIN " & _
                        '            "( " & _
                        '            "    SELECT " & _
                        '            "         stationunkid " & _
                        '            "        ,deptgroupunkid " & _
                        '            "        ,departmentunkid " & _
                        '            "        ,sectiongroupunkid " & _
                        '            "        ,sectionunkid " & _
                        '            "        ,unitgroupunkid " & _
                        '            "        ,unitunkid " & _
                        '            "        ,teamunkid " & _
                        '            "        ,classgroupunkid " & _
                        '            "        ,classunkid " & _
                        '            "        ,employeeunkid " & _
                        '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                        '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                        'If xDateJoinQry.Trim.Length > 0 Then
                        '    StrInnerQ &= xDateJoinQry
                        'End If

                        ''S.SANDEEP [15 NOV 2016] -- START
                        ''If xUACQry.Trim.Length > 0 Then
                        ''    StrInnerQ &= xUACQry
                        ''End If
                        'If blnApplyUserAccessFilter = True Then
                        'If xUACQry.Trim.Length > 0 Then
                        '    StrInnerQ &= xUACQry
                        'End If
                        'End If
                        ''S.SANDEEP [15 NOV 2016] -- END

                        'If xAdvanceJoinQry.Trim.Length > 0 Then
                        '    StrInnerQ &= xAdvanceJoinQry
                        'End If
                        ''Sohail (21 Aug 2015) -- End
                        'Sohail (20 Jan 2020) -- End

                        StrInnerQ &= "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployersStatutoryContributions & " " & _
                                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                    End If

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    'If mintEmployeeId > 0 Then
                    '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                    'End If

                    'If mstrAdvance_Filter.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & mstrAdvance_Filter
                    'End If
                    'Sohail (20 Jan 2020) -- End

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If mblnIsActive = False Then
                    '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                    '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                    '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                    '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                    'End If

                    'If mstrUserAccessFilter.Length > 0 Then
                    '    StrInnerQ &= mstrUserAccessFilter
                    'End If
                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    'If blnApplyUserAccessFilter = True Then
                    '    If xUACFiltrQry.Trim.Length > 0 Then
                    '        StrInnerQ &= " AND " & xUACFiltrQry
                    '    End If
                    'End If

                    'If xIncludeIn_ActiveEmployee = False Then
                    '    If xDateFilterQry.Trim.Length > 0 Then
                    '        StrInnerQ &= xDateFilterQry
                    '    End If
                    'End If
                    ''Sohail (21 Aug 2015) -- End

                    'If mintBranchId > 0 Then
                    '    'Sohail (21 Aug 2015) -- Start
                    '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    '    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                    '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                    '    'Sohail (21 Aug 2015) -- End
                    'End If
                    'Sohail (20 Jan 2020) -- End

                    If mintReportId = 2 Then 'Employee Payroll Summary
                        StrInnerQ &= " GROUP BY prtnaleave_tran.employeeunkid " & _
                                      ", prpayrollprocess_tran.tranheadunkid "
                    Else
                        StrInnerQ &= " GROUP BY payperiodunkid " & _
                                      ", prtnaleave_tran.employeeunkid " & _
                                      ", prpayrollprocess_tran.tranheadunkid " & _
                                      ", openingbalance "
                    End If
                End If
                'Sohail (12 Sep 2014) -- End


                'Sohail (20 Aug 2014) -- Start
                'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
                'If mstrToDatabaseName <> "" AndAlso mstrToDatabaseName <> mstrFromDatabaseName Then
                '    Dim strInnerQTo As String = StrInnerQ.Replace(mstrFromDatabaseName, mstrToDatabaseName)
                '    StrInnerQ &= " UNION ALL " & strInnerQTo
                'End If

            Next
            'Sohail (20 Aug 2014) -- End

            StrQ &= StrInnerQ
            StrQ &= ") AS payroll " & _
                    "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON payroll.EmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & mstrCurrentDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = payroll.PeriodId " & _
                                                 "AND cfcommon_period_tran.isactive = 1 " & _
                                                 "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "
            'Sohail (25 Mar 2013) - [mstrCurrentDatabaseName]
            'Sohail (08 Dec 2012) -- End

            StrQ &= mstrAnalysis_Join


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            'Sohail (24 Feb 2017) -- Start
            'Issue - 64.1 - The multi-part identifier "ADF.teamunkid" could not be found while applying Advance Filter on Payroll Report and Global Void Payment.
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " WHERE " & mstrAdvance_Filter
            'End If
            'Sohail (24 Feb 2017) -- End
            'Pinkal (27-Feb-2013) -- End

            If mintViewIndex > 0 Then
                'Sohail (08 Dec 2012) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " ORDER BY " & mstrAnalysis_OrderBy & ", hremployee_master.employeeunkid " & _
                '                ", TranId "
                'Sohail (07 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " ORDER BY " & mstrAnalysis_OrderBy & ", cfcommon_period_tran.end_date, hremployee_master.employeeunkid " & _
                '               ", TranId "
                If Me.OrderByQuery <> "" Then
                    'Sohail (20 Aug 2014) -- Start
                    'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
                    'StrQ &= " ORDER BY " & mstrAnalysis_OrderBy & ", cfcommon_period_tran.end_date, " & Me.OrderByQuery & ", TranId "
                    StrQ &= " ORDER BY " & mstrAnalysis_Fields.Split(",")(2).Replace("AS GName", "") & ", cfcommon_period_tran.end_date, " & Me.OrderByQuery & ", TranId "
                    'Sohail (20 Aug 2014) -- End
                Else
                    'Sohail (20 Aug 2014) -- Start
                    'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
                    'StrQ &= " ORDER BY " & mstrAnalysis_OrderBy & ", cfcommon_period_tran.end_date, hremployee_master.employeecode " & ", TranId "

                    'Pinkal (27-APR-2018) -- Start
                    'Bug - 0002225: Analysis By not working on Aruti selfservice for Payroll reports i.e Custom payroll report template 2.
                    'StrQ &= " ORDER BY " & mstrAnalysis_Fields.Split(",")(2).Replace("AS GNAME", "") & ", cfcommon_period_tran.end_date, hremployee_master.employeecode " & ", TranId "
                    StrQ &= " ORDER BY " & mstrAnalysis_Fields.Split(",")(2).Replace("AS GName", "") & ", cfcommon_period_tran.end_date, hremployee_master.employeecode " & ", TranId "
                    'Pinkal (27-APR-2018) -- END

                    'Sohail (20 Aug 2014) -- End
                End If
                'Sohail (07 Aug 2013) -- End

                'Sohail (08 Dec 2012) -- End
            Else
                'Sohail (08 Dec 2012) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= "ORDER BY hremployee_master.employeeunkid " & _
                '                ", TranId "
                'Sohail (07 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= "ORDER BY cfcommon_period_tran.end_date, hremployee_master.employeeunkid " & _
                '                ", TranId "
                If Me.OrderByQuery <> "" Then
                    StrQ &= "ORDER BY cfcommon_period_tran.end_date, " & Me.OrderByQuery & ", TranId "
                Else
                    StrQ &= "ORDER BY cfcommon_period_tran.end_date, hremployee_master.employeecode " & ", TranId "
                End If
                'Sohail (07 Aug 2013) -- End

                'Sohail (08 Dec 2012) -- End
            End If

            'Sohail (20 Jan 2020) -- Start
            'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
            For Each key In mdicYearDBName
                StrQ &= " DROP TABLE #" & key.Value & " "
            Next
            'Sohail (20 Jan 2020) -- End

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If mintBranchId > 0 Then
                objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
            End If

            dsPayroll = objDataOperation.ExecQuery(StrQ, "payroll")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (20 Aug 2014) -- Start
            'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
            StrInnerQ = ""
            StrQ = "SELECT  TotalPaymentDetails.PeriodId " & _
                          ", TotalPaymentDetails.EmpId " & _
                          ", TotalPaymentDetails.EmpBankTranId " & _
                          ", TotalPaymentDetails.BankName " & _
                          ", TotalPaymentDetails.BranchName " & _
                          ", TotalPaymentDetails.AccountNo " & _
                          ", TotalPaymentDetails.Amount " & _
                          ", TotalPaymentDetails.PaidAmount " & _
                          ", TotalPaymentDetails.basecurrencyid " & _
                          ", TotalPaymentDetails.paidcurrencyid " & _
                          ", TotalPaymentDetails.baseexchangerate " & _
                          ", TotalPaymentDetails.expaidrate " & _
                          ", TotalPaymentDetails.BaseSign " & _
                          ", TotalPaymentDetails.PaidSign " & _
                          ", TotalPaymentDetails.exchange_rate1 " & _
                          ", TotalPaymentDetails.exchange_rate2 " & _
                   "FROM  ( "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'For i = 0 To marrDatabaseName.Count - 1
            '    mstrFromDatabaseName = marrDatabaseName(i)
            i = -1
            For Each key In mdicYearDBName
                mstrFromDatabaseName = key.Value
                i += 1
                'Sohail (21 Aug 2015) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrFromDatabaseName)
                'Sohail (13 Feb 2016) -- Start
                'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
                'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                If mblnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                'Sohail ((13 Feb 2016) -- End
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrFromDatabaseName)

                'Sohail (21 Aug 2015) -- End
                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If

                StrInnerQ &= "SELECT  prempsalary_tran.employeeunkid AS EmpId " & _
                                    ", prpayment_tran.periodunkid AS PeriodId " & _
                                    ", prempsalary_tran.empbanktranid AS EmpBankTranId " & _
                                    ", hrmsConfiguration..cfpayrollgroup_master.groupname AS BankName " & _
                                    ", hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
                                    ", premployee_bank_tran.accountno AS AccountNo " & _
                                    ", prempsalary_tran.amount AS Amount " & _
                                    ", prempsalary_tran.expaidamt AS PaidAmount " & _
                                    ", prpayment_tran.basecurrencyid " & _
                                    ", prpayment_tran.paidcurrencyid " & _
                                    ", prpayment_tran.baseexchangerate " & _
                                    ", prpayment_tran.expaidrate " & _
                                    ", base.currency_sign AS BaseSign " & _
                                    ", paid.currency_sign AS PaidSign " & _
                                    ", paid.exchange_rate1 " & _
                                    ", paid.exchange_rate2 " & _
                             "FROM  " & mstrFromDatabaseName & "..prempsalary_tran " & _
                                    "JOIN " & mstrFromDatabaseName & "..premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid " & _
                                    "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                                    "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                                    "JOIN " & mstrFromDatabaseName & "..prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                    "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                    "JOIN " & mstrFromDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prempsalary_tran.employeeunkid " & _
                                    "JOIN " & mstrFromDatabaseName & "..cfexchange_rate AS base ON base.exchangerateunkid = prpayment_tran.basecurrencyid " & _
                                    "JOIN " & mstrFromDatabaseName & "..cfexchange_rate AS paid ON paid.exchangerateunkid = prpayment_tran.paidcurrencyid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrInnerQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrInnerQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrInnerQ &= xUACQry
                End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrInnerQ &= "WHERE ISNULL(premployee_bank_tran.isvoid, 0) = 0 " & _
                                    "AND ISNULL(prempsalary_tran.isvoid, 0) = 0 " & _
                                    "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                    "AND hrmsConfiguration..cfbankbranch_master.isactive = 1 " & _
                                    "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1 " & _
                                    "AND hrmsConfiguration..cfpayrollgroup_master.grouptype_id = 3 " & _
                                    "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                    "AND prpayment_tran.periodunkid IN (" & mstrPeriodIdList & ") "
                'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

                If mintEmployeeId > 0 Then
                    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrInnerQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrInnerQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrInnerQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                    StrInnerQ &= " AND T.stationunkid = @BranchId "
                    'Sohail (21 Aug 2015) -- End
                End If

                StrInnerQ &= "UNION ALL " & _
                             "SELECT  prpayment_tran.employeeunkid AS EmpId " & _
                                    ", prpayment_tran.periodunkid AS PeriodId " & _
                                    ", -1 AS EmpBankTranId " & _
                                    ", @CashPayment AS BankName " & _
                                    ", '' AS BranchName " & _
                                    ", '' AS AccountNo " & _
                                    ", prpayment_tran.amount AS Amount " & _
                                    ", prpayment_tran.expaidamt AS PaidAmount " & _
                                    ", prpayment_tran.basecurrencyid " & _
                                    ", prpayment_tran.paidcurrencyid " & _
                                    ", prpayment_tran.baseexchangerate " & _
                                    ", prpayment_tran.expaidrate " & _
                                    ", base.currency_sign AS BaseSign " & _
                                    ", paid.currency_sign AS PaidSign " & _
                                    ", paid.exchange_rate1 " & _
                                    ", paid.exchange_rate2 " & _
                             "FROM  " & mstrFromDatabaseName & "..prpayment_tran " & _
                                    "JOIN " & mstrFromDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prpayment_tran.employeeunkid " & _
                                    "JOIN " & mstrFromDatabaseName & "..cfexchange_rate AS base ON base.exchangerateunkid = prpayment_tran.basecurrencyid " & _
                                    "JOIN " & mstrFromDatabaseName & "..cfexchange_rate AS paid ON paid.exchangerateunkid = prpayment_tran.paidcurrencyid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrInnerQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrInnerQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrInnerQ &= xUACQry
                End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrInnerQ &= "WHERE ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                    "AND prpayment_tran.referenceid = 3 " & _
                                    "AND prpayment_tran.paymentmode = 1 " & _
                                    "AND prpayment_tran.periodunkid IN (" & mstrPeriodIdList & ") "

                If mintEmployeeId > 0 Then
                    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrInnerQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                    StrInnerQ &= " AND T.stationunkid = @BranchId "
                    'Sohail (21 Aug 2015) -- End
                End If

                StrInnerQ &= "UNION ALL " & _
                             "SELECT  prtnaleave_tran.employeeunkid AS EmpId " & _
                                    ", prtnaleave_tran.payperiodunkid AS PeriodId " & _
                                    ", -2 AS EmpBankTranId " & _
                                    ", @SalaryOnHold AS BankName " & _
                                    ", '' AS BranchName " & _
                                    ", '' AS AccountNo " & _
                                    ", prtnaleave_tran.balanceamount AS Amount " & _
                                    ", prtnaleave_tran.balanceamount AS PaidAmount " & _
                                    ", 0 AS basecurrencyid " & _
                                    ", 0 AS paidcurrencyid " & _
                                    ", 0 AS baseexchangerate " & _
                                    ", 0 AS expaidrate " & _
                                    ", '' AS BaseSign " & _
                                    ", '' AS PaidSign " & _
                                    ", 0 AS exchange_rate1 " & _
                                    ", 0 AS exchange_rate2 " & _
                             "FROM  " & mstrFromDatabaseName & "..prtnaleave_tran " & _
                                    "JOIN " & mstrFromDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prtnaleave_tran.employeeunkid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrInnerQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrInnerQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrInnerQ &= xUACQry
                End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrInnerQ &= "WHERE prtnaleave_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIdList & ") " & _
                                    "AND prtnaleave_tran.balanceamount <> 0 "

                If mintEmployeeId > 0 Then
                    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrInnerQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrInnerQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrInnerQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                    StrInnerQ &= " AND T.stationunkid = @BranchId "
                    'Sohail (21 Aug 2015) -- End
                End If

            Next

            StrQ &= StrInnerQ

            StrQ &= ") AS TotalPaymentDetails " & _
                    "GROUP BY  TotalPaymentDetails.PeriodId " & _
                        ",TotalPaymentDetails.EmpId " & _
                        ",TotalPaymentDetails.EmpBankTranId " & _
                        ",TotalPaymentDetails.BankName " & _
                        ",TotalPaymentDetails.BranchName " & _
                        ",TotalPaymentDetails.AccountNo " & _
                        ",TotalPaymentDetails.Amount " & _
                        ", TotalPaymentDetails.PaidAmount " & _
                        ",TotalPaymentDetails.basecurrencyid " & _
                  ", TotalPaymentDetails.paidcurrencyid " & _
                  ", TotalPaymentDetails.baseexchangerate " & _
                  ", TotalPaymentDetails.expaidrate " & _
                  ", TotalPaymentDetails.BaseSign " & _
                  ", TotalPaymentDetails.PaidSign " & _
                  ", TotalPaymentDetails.exchange_rate1 " & _
                  ", TotalPaymentDetails.exchange_rate2 "

            objDataOperation.AddParameter("@CashPayment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Cash Payment"))
            objDataOperation.AddParameter("@SalaryOnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "Salary On Hold"))

            dsPaymetDetail = objDataOperation.ExecQuery(StrQ, "Payment")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            For Each dtBank As DataRow In dsPaymetDetail.Tables("Payment").Rows
                strCurrKey = dtBank.Item("PeriodId") & "_" & dtBank.Item("EmpId")
                If mdicEmpBank.ContainsKey(strCurrKey) Then

                    'Pinkal (13-Oct-2014) -- Start
                    'Enhancement -  SEPERATE BANK,ACCOUNT NO AND PAYMENT DETAIL COLUMN REQUESTED BY MR.ANDREW AND MR.RUTTA

                    'If dtBank("EmpBankTranId") > 0 Then
                    '    mdicEmpBank(strCurrKey) &= ", " & Language.getMessage(mstrModuleName, 22, "Bank :") & dtBank.Item("BankName") & ", " & Language.getMessage(mstrModuleName, 23, "Branch :") & dtBank.Item("BranchName") & ", " & Language.getMessage(mstrModuleName, 24, "Account # :") & dtBank.Item("AccountNo") & ", " & Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                    'Else
                    '    mdicEmpBank(strCurrKey) &= ", " & dtBank.Item("BankName") & ", " & Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                    'End If

                    If dtBank("EmpBankTranId") > 0 Then
                        mdicEmpBank(strCurrKey) &= "#10;" & Language.getMessage(mstrModuleName, 22, "Bank :") & dtBank.Item("BankName") & ", " & Language.getMessage(mstrModuleName, 23, "Branch :") & dtBank.Item("BranchName")
                        'Sohail (09 Dec 2014) -- Start
                        'Error - Given key was not found in dictionary when employee is paid rounding amount. (bank details + salary on hold -- and first entry is salary on hold no accountno detail in dictionary).
                        'mdicEmpAccount(strCurrKey) &= "#10;" & Language.getMessage(mstrModuleName, 24, "Account # :") & dtBank.Item("AccountNo")
                        If mdicEmpAccount.ContainsKey(strCurrKey) = True Then
                            mdicEmpAccount(strCurrKey) &= "#10;" & Language.getMessage(mstrModuleName, 24, "Account # :") & dtBank.Item("AccountNo")
                        Else
                            mdicEmpAccount.Add(strCurrKey, "#10;" & Language.getMessage(mstrModuleName, 24, "Account # :") & dtBank.Item("AccountNo"))
                        End If
                        'Sohail (09 Dec 2014) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                        mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), mstrfmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                        'Sohail (21 Aug 2015) -- End
                    Else
                        mdicEmpBank(strCurrKey) &= "#10;" & dtBank.Item("BankName")
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                        mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), mstrfmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                        'Sohail (21 Aug 2015) -- End
                    End If

                    'Pinkal (13-Oct-2014) -- End

                    'If mdicPaidCurrency.Item(strCurrKey)(6) = 0 AndAlso mdicPaidCurrency.Item(strCurrKey)(7) = 0 Then 'If Salary on Hold (exchange_rate1=0 and exchange_rate2=0)
                    '    marrPayment = New ArrayList
                    '    marrPayment.Add(dtBank.Item("basecurrencyid"))
                    '    marrPayment.Add(dtBank.Item("paidcurrencyid"))
                    '    marrPayment.Add(dtBank.Item("baseexchangerate"))
                    '    marrPayment.Add(dtBank.Item("expaidrate"))
                    '    marrPayment.Add(dtBank.Item("BaseSign"))
                    '    marrPayment.Add(dtBank.Item("PaidSign"))
                    '    marrPayment.Add(dtBank.Item("exchange_rate1"))
                    '    marrPayment.Add(dtBank.Item("exchange_rate2"))
                    '    mdicPaidCurrency.Item(strCurrKey) = marrPayment
                    'End If
                Else

                    'Pinkal (13-Oct-2014) -- Start
                    'Enhancement -  SEPERATE BANK,ACCOUNT NO AND PAYMENT DETAIL COLUMN REQUESTED BY MR.ANDREW AND MR.RUTTA


                    'If dtBank("EmpBankTranId") > 0 Then
                    '    mdicEmpBank.Add(strCurrKey, Language.getMessage(mstrModuleName, 22, "Bank :") & dtBank.Item("BankName") & ", " & Language.getMessage(mstrModuleName, 23, "Branch :") & dtBank.Item("BranchName") & ", " & Language.getMessage(mstrModuleName, 24, "Account # :") & dtBank.Item("AccountNo") & ", " & Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")")
                    'Else
                    '    If CInt(dtBank.Item("EmpBankTranId")) = -2 AndAlso CDec(dtBank.Item("amount")) < 0 AndAlso dsPaymetDetail.Tables("Payment").Select("PeriodId = " & dtBank.Item("PeriodId") & " AND EmpId = " & dtBank.Item("EmpId") & " ").Length > 1 Then
                    '        Continue For 'Over Payment; Show Payment detail only
                    '    End If
                    '    If CInt(dtBank.Item("EmpBankTranId")) = -2 AndAlso dsPaymetDetail.Tables("Payment").Select("PeriodId = " & dtBank.Item("PeriodId") & " AND EmpId = " & dtBank.Item("EmpId") & " ").Length > 1 Then
                    '        mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName") & ", " & " (" & Format(dtBank.Item("amount"), GUI.fmtCurrency) & ")")
                    '    Else
                    '        If CInt(dtBank.Item("EmpBankTranId")) = -2 Then 'Salary On Hold
                    '            mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName") & ", " & " (" & Format(dtBank.Item("amount"), GUI.fmtCurrency) & ")")
                    '        Else
                    '            mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName") & ", " & Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")")
                    '        End If
                    '    End If
                    'End If

                    If dtBank("EmpBankTranId") > 0 Then
                        mdicEmpBank.Add(strCurrKey, Language.getMessage(mstrModuleName, 22, "Bank :") & dtBank.Item("BankName") & ", " & Language.getMessage(mstrModuleName, 23, "Branch :") & dtBank.Item("BranchName"))
                        mdicEmpAccount.Add(strCurrKey, Language.getMessage(mstrModuleName, 24, "Account # :") & dtBank.Item("AccountNo") & ",")
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                        mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), mstrfmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                        'Sohail (21 Aug 2015) -- End
                    Else
                        If CInt(dtBank.Item("EmpBankTranId")) = -2 AndAlso CDec(dtBank.Item("amount")) < 0 AndAlso dsPaymetDetail.Tables("Payment").Select("PeriodId = " & dtBank.Item("PeriodId") & " AND EmpId = " & dtBank.Item("EmpId") & " ").Length > 1 Then
                            Continue For 'Over Payment; Show Payment detail only
                        End If
                        If CInt(dtBank.Item("EmpBankTranId")) = -2 AndAlso dsPaymetDetail.Tables("Payment").Select("PeriodId = " & dtBank.Item("PeriodId") & " AND EmpId = " & dtBank.Item("EmpId") & " ").Length > 1 Then
                            mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName"))
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), GUI.fmtCurrency) & "),")
                            mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), mstrfmtCurrency) & "),")
                            'Sohail (21 Aug 2015) -- End
                        Else
                            If CInt(dtBank.Item("EmpBankTranId")) = -2 Then 'Salary On Hold
                                mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName"))
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), GUI.fmtCurrency) & "),")
                                mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), mstrfmtCurrency) & "),")
                                'Sohail (21 Aug 2015) -- End
                            Else
                                mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName"))
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                                mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), mstrfmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                                'Sohail (21 Aug 2015) -- End
                            End If
                        End If
                    End If

                    'Pinkal (13-Oct-2014) -- End

                    '        marrPayment = New ArrayList
                    '        marrPayment.Add(dtBank.Item("basecurrencyid"))
                    '        marrPayment.Add(dtBank.Item("paidcurrencyid"))
                    '        marrPayment.Add(dtBank.Item("baseexchangerate"))
                    '        marrPayment.Add(dtBank.Item("expaidrate"))
                    '        marrPayment.Add(dtBank.Item("BaseSign"))
                    '        marrPayment.Add(dtBank.Item("PaidSign"))
                    '        marrPayment.Add(dtBank.Item("exchange_rate1"))
                    '        marrPayment.Add(dtBank.Item("exchange_rate2"))
                    '        mdicPaidCurrency.Add(strKey, marrPayment)
                End If
            Next
            'Sohail (20 Aug 2014) -- End

            Dim strKey As String = ""
            Dim strPrevKey As String = ""
            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intRowCount As Integer = dsPayroll.Tables("payroll").Rows.Count

            For ii As Integer = 0 To intRowCount - 1

                drRow = dsPayroll.Tables("payroll").Rows(ii)

                strKey = drRow.Item("PeriodId").ToString & "_" & drRow.Item("EmpId")

                If strPrevKey <> strKey Then
                    rpt_Row = dtFinalTable.NewRow

                    rpt_Row.Item("PeriodId") = drRow.Item("PeriodId")
                    rpt_Row.Item("Period") = drRow.Item("PeriodName")

                    rpt_Row.Item("EmpCode") = drRow.Item("Code")
                    rpt_Row.Item("EmpName") = drRow.Item("EmpName")
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Row.Item("Openingbalance") = Format(CDec(drRow.Item("Openingbalance")), GUI.fmtCurrency) 'Sohail (18 Jun 2013)
                    'Sohail (15 Jun 2021) -- Start
                    'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                    rpt_Row.Item("Openingbalance") = Format(CDec(drRow.Item("Openingbalance")), mstrfmtCurrency)
                    'rpt_Row.Item("Openingbalance") = CDec(drRow.Item("Openingbalance"))
                    'Sohail (15 Jun 2021) -- End
                    'Sohail (21 Aug 2015) -- End

                    rpt_Row.Item("GrpID") = drRow.Item("Id")
                    rpt_Row.Item("GrpName") = drRow.Item("GName")
                    'Sohail (20 Aug 2014) -- Start
                    'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.

                    'Pinkal (13-Oct-2014) -- Start
                    'Enhancement -  SEPERATE BANK,ACCOUNT NO AND PAYMENT DETAIL COLUMN REQUESTED BY MR.ANDREW AND MR.RUTTA

                    'If mdicEmpBank.ContainsKey(strKey) = True Then
                    '    rpt_Row.Item("PaymentDetail") = mdicEmpBank.Item(strKey)
                    'Else
                    '    rpt_Row.Item("PaymentDetail") = ""
                    'End If

                    If mdicEmpBank.ContainsKey(strKey) = True Then
                        If mdicEmpBank.Item(strKey).Contains("#10;") = False Then
                            rpt_Row.Item("Bank") = mdicEmpBank.Item(strKey).Replace(",", "")
                        Else
                            rpt_Row.Item("Bank") = mdicEmpBank.Item(strKey)
                        End If
                    Else
                        rpt_Row.Item("Bank") = ""
                    End If

                    If mdicEmpAccount.ContainsKey(strKey) Then
                        If mdicEmpAccount.Item(strKey).Contains("#10;") = False Then
                            rpt_Row.Item("AccountNo") = mdicEmpAccount.Item(strKey).Replace(",", "")
                        Else
                            rpt_Row.Item("AccountNo") = mdicEmpAccount.Item(strKey)
                        End If
                    Else
                        rpt_Row.Item("AccountNo") = ""
                    End If

                    If mdicEmpPayment.ContainsKey(strKey) = True Then
                        If mdicEmpPayment.Item(strKey).Contains("#10;") = False Then
                            rpt_Row.Item("PaymentDetail") = mdicEmpPayment.Item(strKey).Replace(",", "")
                        Else
                            rpt_Row.Item("PaymentDetail") = mdicEmpPayment.Item(strKey)
                        End If
                    Else
                        rpt_Row.Item("PaymentDetail") = ""
                    End If

                    'Pinkal (13-Oct-2014) -- End

                    'Sohail (20 Aug 2014) -- End
                Else
                    rpt_Row = dtFinalTable.Rows(dtFinalTable.Rows.Count - 1)
                End If


                If drRow.Item("Mid").ToString = "10" Then 'Earning
                    'Sohail (25 Mar 2013) -- Start
                    'TRA - ENHANCEMENT
                    'rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                    'Sohail (15 Jun 2021) -- Start
                    'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                    rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    'rpt_Row.Item("Column" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                    'Sohail (15 Jun 2021) -- End
                    'Sohail (25 Mar 2013) -- End
                    'Sohail (14 Aug 2012) -- Start
                    'TRA - ENHANCEMENT
                    'rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                    'Sohail (25 Mar 2013) -- Start
                    'TRA - ENHANCEMENT
                    'rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(Format(drRow.Item("Amount"), GUI.fmtCurrency))
                    'Sohail (17 May 2013) -- Start
                    'TRA - ENHANCEMENT - TWAWEZA ROUNDING ISSUE - PAYROLL REPORT AND BANK PAYMENT/PAYROLL SUMMARY DOES NOT MATCH
                    'rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(Format(drRow.Item("Amount"), mstrfmtCurrency))
                    'Sohail (21 Nov 2013) -- Start
                    'Issue - TWAWEZA ROUNDING ISSUE - PAYROLL REPORT AND PAYROLL SUMMARY $ Currency report DOES NOT MATCH
                    'rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                    'Sohail (01 Mar 2017) -- Start
                    'TRA Issue - 64.1 - JV Dr and Cr balance were not matching due to round off function used with default 2 decimals and jv total was not matching with payroll summary gross pay and payroll report net pay.
                    'rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(Format(drRow.Item("Amount"), mstrfmtCurrency))
                    rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                    'Sohail (01 Mar 2017) -- End
                    'Sohail (21 Nov 2013) -- End
                    'Sohail (17 May 2013) -- End
                    'Sohail (25 Mar 2013) -- End
                    'Sohail (14 Aug 2012) -- End

                    'Sohail (21 Jun 2013) -- Start
                    'TRA - ENHANCEMENT
                ElseIf drRow.Item("Mid").ToString = "11" Then 'Activity Earning
                    'Sohail (15 Jun 2021) -- Start
                    'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                    rpt_Row.Item("ColumnPPA" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    'rpt_Row.Item("ColumnPPA" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                    'Sohail (15 Jun 2021) -- End
                    rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                    'Sohail (21 Jun 2013) -- End

                    'Sohail (12 Nov 2014) -- Start
                    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                ElseIf drRow.Item("Mid").ToString = "14" Then 'CR Expense Earning
                    'Sohail (15 Jun 2021) -- Start
                    'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                    rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    'rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                    'Sohail (15 Jun 2021) -- End
                    rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                    'Sohail (12 Nov 2014) -- End

                    'Sohail (12 Sep 2014) -- Start
                    'Enhancement - Include Employer Contribution on Payroll Report.
                ElseIf drRow.Item("Mid").ToString = "13" Then 'Employer Contribution
                    'Sohail (15 Jun 2021) -- Start
                    'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                    rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
'                    rpt_Row.Item("Column" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                    'Sohail (15 Jun 2021) -- End
                    'Sohail (12 Sep 2014) -- End

                Else
                    If drRow.Item("Mid").ToString = "1" Then   'LOAN
                        'Sohail (25 Mar 2013) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Loan") = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        'Sohail (03 Feb 2016) -- Start
                        'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                        'rpt_Row.Item("Loan") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        If mblnShowLoansInSeparateColumns = True Then
                            'Sohail (15 Jun 2021) -- Start
                            'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                            rpt_Row.Item("ColumnLoan" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                            'rpt_Row.Item("ColumnLoan" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                            'Sohail (15 Jun 2021) -- End
                        Else
                            'Sohail (15 Jun 2021) -- Start
                            'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                            rpt_Row.Item("Loan") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                            'rpt_Row.Item("Loan") = CDec(drRow.Item("Amount"))
                            'Sohail (15 Jun 2021) -- End
                        End If
                        'Sohail (03 Feb 2016) -- End
                        'Sohail (25 Mar 2013) -- End
                    ElseIf drRow.Item("Mid").ToString = "2" Then 'ADVANCE
                        'Sohail (25 Mar 2013) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Advance") = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        'Sohail (15 Jun 2021) -- Start
                        'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                        rpt_Row.Item("Advance") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'rpt_Row.Item("Advance") = CDec(drRow.Item("Amount"))
                        'Sohail (15 Jun 2021) -- End
                        'Sohail (25 Mar 2013) -- End
                    ElseIf drRow.Item("Mid").ToString = "3" Then 'SAVINGS
                        'Sohail (25 Mar 2013) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Savings") = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        'Sohail (03 Feb 2016) -- Start
                        'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                        'rpt_Row.Item("Savings") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        If mblnShowSavingsInSeparateColumns = True Then
                            'Sohail (15 Jun 2021) -- Start
                            'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                            rpt_Row.Item("ColumnSavings" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                            'rpt_Row.Item("ColumnSavings" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                            'Sohail (15 Jun 2021) -- End
                        Else
                            'Sohail (15 Jun 2021) -- Start
                            'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                            rpt_Row.Item("Savings") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                            'rpt_Row.Item("Savings") = CDec(drRow.Item("Amount"))
                            'Sohail (15 Jun 2021) -- End
                        End If
                        'Sohail (03 Feb 2016) -- End
                        'Sohail (25 Mar 2013) -- End

                        'Sohail (21 Jun 2013) -- Start
                        'TRA - ENHANCEMENT
                    ElseIf drRow.Item("Mid").ToString = "12" Then 'Activity Deduction
                        'Sohail (15 Jun 2021) -- Start
                        'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                        rpt_Row.Item("ColumnPPA" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'rpt_Row.Item("ColumnPPA" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                        'Sohail (15 Jun 2021) -- End
                        'Sohail (21 Jun 2013) -- End

                        'Sohail (12 Nov 2014) -- Start
                        'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                    ElseIf drRow.Item("Mid").ToString = "15" Then 'CR Expense Deduction
                        'Sohail (15 Jun 2021) -- Start
                        'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                        rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                        'Sohail (15 Jun 2021) -- End
                        'Sohail (12 Nov 2014) -- End

                    Else 'Deduction
                        'Sohail (25 Mar 2013) -- Start
                        'TRA - ENHANCEMENT
                        'rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        'Sohail (15 Jun 2021) -- Start
                        'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                        rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'rpt_Row.Item("Column" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                        'Sohail (15 Jun 2021) -- End
                        'Sohail (25 Mar 2013) -- End
                    End If
                    'Sohail (14 Aug 2012) -- Start
                    'TRA - ENHANCEMENT
                    'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                    'Sohail (25 Mar 2013) -- Start
                    'TRA - ENHANCEMENT
                    'rpt_Row.Item("TDD") = CDec(rpt_Row.Item("TDD")) + CDec(Format(drRow.Item("Amount"), GUI.fmtCurrency))
                    'Sohail (17 May 2013) -- Start
                    'TRA - ENHANCEMENT - TWAWEZA ROUNDING ISSUE - PAYROLL REPORT AND BANK PAYMENT/PAYROLL SUMMARY DOES NOT MATCH
                    'rpt_Row.Item("TDD") = CDec(rpt_Row.Item("TDD")) + CDec(Format(drRow.Item("Amount"), mstrfmtCurrency))
                    rpt_Row.Item("TDD") = CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount"))
                    'Sohail (17 May 2013) -- End
                    'Sohail (25 Mar 2013) -- End
                    'Sohail (14 Aug 2012) -- End
                End If

                'Sohail (25 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                'rpt_Row.Item("NetPay") = Format(CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD")), GUI.fmtCurrency)
                'Sohail (01 Mar 2017) -- Start
                'TRA Issue - 64.1 - JV Dr and Cr balance were not matching due to round off function used with default 2 decimals and jv total was not matching with payroll summary gross pay and payroll report net pay.
                'rpt_Row.Item("NetPay") = Format(CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD")), mstrfmtCurrency)
                rpt_Row.Item("NetPay") = CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD"))
                'Sohail (01 Mar 2017) -- End
                'Sohail (25 Mar 2013) -- End

                'Sohail (01 Mar 2016) -- Start
                'Enhancement - Rounding issue for Net Pay on Payroll Report, not matching with Payslip Report.
                ''rpt_Row.Item("NetPay") = Format(Rounding.BRound(CDec(rpt_Row.Item("NetPay")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency)
                'Sohail (01 Mar 2017) -- Start
                'TRA Issue - 64.1 - JV Dr and Cr balance were not matching due to round off function used with default 2 decimals and jv total was not matching with payroll summary gross pay and payroll report net pay.
                'rpt_Row.Item("NetPay") = Format(Rounding.BRound(CDec(rpt_Row.Item("NetPay")), dblRoundOff_Type), mstrfmtCurrency)
                'Sohail (01 Mar 2017) -- End
                'Sohail (01 Mar 2016) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("TotNetPay") = Format(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("Openingbalance")), GUI.fmtCurrency) 'Sohail (18 Jun 2013)
                'Sohail (22 Sep 2017) -- Start
                'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                'rpt_Row.Item("TotNetPay") = Format(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("Openingbalance")), mstrfmtCurrency) 'Sohail (18 Jun 2013)
                rpt_Row.Item("TotNetPay") = CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("Openingbalance"))
                'Sohail (22 Sep 2017) -- End

                'Anjan (16 Jun 2017) - Start
                'For FINCA nigeria rounding issue
                'rpt_Row.Item("TotNetPay") = CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("Openingbalance")) 'Sohail (18 Jun 2013)
                'Anjan (16 Jun 2017) - End 
                'Sohail (21 Aug 2015) -- End

                'Sohail (01 Mar 2016) -- Start
                'Enhancement - Rounding issue for Net Pay on Payroll Report, not matching with Payslip Report.

                'Sohail (22 Sep 2017) -- Start
                'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                'rpt_Row.Item("TotNetPay") = Format(Rounding.BRound(CDec(rpt_Row.Item("TotNetPay")), dblRoundOff_Type), mstrfmtCurrency)
                rpt_Row.Item("TotNetPay") = Rounding.BRound(CDec(rpt_Row.Item("TotNetPay")), dblRoundOff_Type).ToString()
                'Sohail (22 Sep 2017) -- End                

                'Anjan (16 Jun 2017) - Start
                'For FINCA nigeria rounding issue
                'rpt_Row.Item("TotNetPay") = Format(Rounding.BRound(CDec(rpt_Row.Item("TotNetPay")), dblRoundOff_Type), mstrfmtCurrency)
                'Anjan (16 Jun 2017) - End 
                'Sohail (01 Mar 2016) -- End

                If strPrevKey <> strKey Then
                    dtFinalTable.Rows.Add(rpt_Row)
                End If
                strPrevKey = strKey
            Next

            dtFinalTable.AcceptChanges()


            Dim dblTotal As Decimal = 0
            Dim m As Integer = 6
            Dim n As Integer = 6
            dblColTot = New Decimal(dtFinalTable.Columns.Count) {}

            While m < dtFinalTable.Columns.Count - 3
                For t As Integer = 0 To dtFinalTable.Rows.Count - 1
                    dblTotal += CDec(dtFinalTable.Rows(t)(m))
                Next
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dblColTot(n) = Format(CDec(dblTotal), GUI.fmtCurrency)
                'Sohail (15 Jun 2021) -- Start
                'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                dblColTot(n) = Format(CDec(dblTotal), mstrfmtCurrency)
                'dblColTot(n) = CDec(dblTotal)
                'Sohail (15 Jun 2021) -- End
                'Sohail (21 Aug 2015) -- End
                dblTotal = 0
                m += 1
                n += 1
            End While

            Call FilterTitleAndFilterQuery()



            'Pinkal (14-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'If Export_to_Excel(mstrReportTypeName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtFinalTable) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
            '    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
            'End If

            Dim strGTotal As String = Language.getMessage(mstrModuleName, 4, "Grand Total :")
            Dim strSubTotal As String = ""
            Dim objDic As New Dictionary(Of Integer, Object)
            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList 'Sohail (11 Mar 2013)
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell = Nothing
            'Sohail (29 Apr 2014) -- Start
            'Enhancement - Export Excel Report with Company Logo 
            Dim strBuilder As New StringBuilder
            'Sohail (29 Apr 2014) -- End

            If mblnIgnorezeroHeads Then
                mdtTableExcel = IgnoreZeroHead(dtFinalTable)
            Else
                mdtTableExcel = dtFinalTable
            End If
            'START TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 

            mdtTableExcel.Columns.RemoveAt(0)
            If mintViewIndex <= 0 Then
                mdtTableExcel.Columns.Remove("GrpID")
                mdtTableExcel.Columns.Remove("GrpName")
            Else
                mdtTableExcel.Columns.Remove("GrpID")
                mdtTableExcel.Columns("GrpName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GrpName"}
                strarrGroupColumns = strGrpCols
                strSubTotal = Language.getMessage(mstrModuleName, 3, "Sub Total :")
            End If
            'Sohail (29 Apr 2014) -- Start
            'Enhancement - Export Excel Report with Company Logo 
            If mintReportId = 1 Then
                mdtTableExcel.Columns.Remove("EmpCode")
                mdtTableExcel.Columns.Remove("EmpName")
                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            ElseIf mintReportId = 2 Then
                mdtTableExcel.Columns.Remove("Period")
                'Sohail (01 Sep 2014) -- End
            End If
            'Sohail (29 Apr 2014) -- End

            'END TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 
            'Sohail (29 Apr 2014) -- Start
            'Enhancement - Export Excel Report with Company Logo 
            For Each strID In mstrUnSelectedHeadIDs.Split(",")
                If mdtTableExcel.Columns.Contains("Column" & strID) Then
                    mdtTableExcel.Columns.Remove("Column" & strID)
                End If
            Next

            'Sohail (20 Aug 2014) -- Start
            'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
            If mblnShowPaymentDetails = False Then
                'Pinkal (13-Oct-2014) -- Start
                'Enhancement -  SEPERATE BANK,ACCOUNT NO AND PAYMENT DETAIL COLUMN REQUESTED BY MR.ANDREW AND MR.RUTTA
                mdtTableExcel.Columns.Remove("Bank")
                mdtTableExcel.Columns.Remove("AccountNo")
                'Pinkal (13-Oct-2014) -- END
                mdtTableExcel.Columns.Remove("PaymentDetail")
            End If
            'Sohail (20 Aug 2014) -- End

            Dim intColCount As Integer = mdtTableExcel.Columns.Count
            If strarrGroupColumns IsNot Nothing AndAlso strarrGroupColumns.Length > 0 Then
                intColCount -= strarrGroupColumns.Length
            End If
            'Sohail (29 Apr 2014) -- End

            If mintReportId = 0 Then

                'Sohail (25 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintEmployeeId > 0 Then
                    If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName, "s10bw")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        rowsArrayHeader.Add(row)
                        'Sohail (29 Apr 2014) -- Start
                        'Enhancement - Export Excel Report with Company Logo 
                    Else
                        strBuilder.Append("<TR>" & vbCrLf)
                        strBuilder.Append("<TD colspan=" & intColCount & " style='font-size:13px;border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & "</B></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                        If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                        strBuilder.Remove(0, strBuilder.Length)
                    End If
                    'Sohail (29 Apr 2014) -- End
                End If
                'Sohail (25 Mar 2013) -- End

                If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "Period :") & mstrPeriodName, "s10bw")
                    wcell.MergeAcross = 1 'Sohail (16 Mar 2013)
                    row.Cells.Add(wcell)

                    'Sohail (16 Mar 2013) -- Start
                    'TRA - ENHANCEMENT
                    If mstrCurrency_Sign.Trim <> "" AndAlso mstrCurrency_Rate.Trim <> "" Then
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Currency Rate :") & " " & mstrCurrency_Rate, "s10bw")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)
                    End If
                    'Sohail (16 Mar 2013) -- End

                    rowsArrayHeader.Add(row)
                    'Sohail (29 Apr 2014) -- Start
                    'Enhancement - Export Excel Report with Company Logo 
                Else
                    strBuilder.Append("<TR>" & vbCrLf)
                    strBuilder.Append("<TD colspan=2 style='font-size:13px;border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 2, "Period :") & mstrPeriodName & "</B></TD>" & vbCrLf)
                    If mstrCurrency_Sign.Trim <> "" AndAlso mstrCurrency_Rate.Trim <> "" Then
                        strBuilder.Append("<TD colspan=2 style='font-size:13px;border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 17, "Currency Rate :") & " " & mstrCurrency_Rate & "</B></TD>" & vbCrLf)
                    End If
                    strBuilder.Append("</TR>" & vbCrLf)
                    If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                    strBuilder.Remove(0, strBuilder.Length)
                End If
                'Sohail (29 Apr 2014) -- End

                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                'ElseIf mintReportId = 1 Then
            ElseIf mintReportId = 1 OrElse mintReportId = 2 Then
                'Sohail (01 Sep 2014) -- End


                'Sohail (29 Apr 2014) -- Start
                'Enhancement - Export Excel Report with Company Logo 
                'mdtTableExcel.Columns.Remove("EmpCode")
                'mdtTableExcel.Columns.Remove("EmpName")
                'Sohail (29 Apr 2014) -- End

                If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                    If mintEmployeeId > 0 Then 'Sohail (01 Sep 2014)
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName, "s10bw")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        rowsArrayHeader.Add(row)
                    End If 'Sohail (01 Sep 2014)

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "Period :") & " " & mstrPeriodName & Language.getMessage(mstrModuleName, 10, " To ") & mstrToPeriodName, "s10bw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayHeader.Add(row)
                    'Sohail (29 Apr 2014) -- Start
                    'Enhancement - Export Excel Report with Company Logo 
                Else
                    If mintEmployeeId > 0 Then 'Sohail (01 Sep 2014)
                        strBuilder.Append("<TR>" & vbCrLf)
                        strBuilder.Append("<TD colspan=" & intColCount & " style='font-size:13px;border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & "</B></TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                    End If 'Sohail (01 Sep 2014)
                    If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                    strBuilder.Remove(0, strBuilder.Length)

                    strBuilder.Append("<TR>" & vbCrLf)
                    strBuilder.Append("<TD colspan=" & intColCount & " style='font-size:13px;border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 2, "Period :") & " " & mstrPeriodName & Language.getMessage(mstrModuleName, 10, " To ") & mstrToPeriodName & "</B></TD>" & vbCrLf)
                    strBuilder.Append("</TR>" & vbCrLf)
                    If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                    strBuilder.Remove(0, strBuilder.Length)
                End If
                'Sohail (29 Apr 2014) -- End
            End If


            'Sohail (11 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (25 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'If ConfigParameter._Object._SetPayslipPaymentApproval = True Then
            If mblnSetPayslipPaymentApproval = True Then
                'Sohail (25 Mar 2013) -- End


                'Sohail/Anjan (25 Mar 2013) -- Start
                'Issue : as per Rutta, twaweza does wants to see approved/checked by before end of payroll period after payment is done.
                'Dim objPayroll As New clsPayrollProcessTran
                'dsList = objPayroll.Get_UnProcessed_Employee("UnProcessedEmp", mintPeriodId, True)
                'If dsList.Tables("UnProcessedEmp").Rows.Count = 0 Then
                'Sohail/Anjan (25 Mar 2013) -- End



                Dim objTnALeave As New clsTnALeaveTran
                Dim dtTable As DataTable

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objTnALeave.Get_Balance_List("Balance", , , mintPeriodId, "")
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(mstrFromDatabaseName) = mintPeriodId
                dsList = objTnALeave.Get_Balance_List(mstrFromDatabaseName, xUserUnkid, objPeriod._Yearunkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, "", "Balance", , mintPeriodId, "")
                'Sohail (21 Aug 2015) -- End
                dtTable = New DataView(dsList.Tables("Balance"), "balanceamount > 0", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count = 0 Then 'If payment is done for all employees
                    Dim objPayment As New clsPayment_tran
                    Dim objLevel As New clsPaymentApproverlevel_master
                    Dim objPaymentApproval As New clsPayment_approval_tran
                    Dim objPayAuthorize As New clsPayment_authorize_tran
                    Dim dsLevel As DataSet
                    Dim dsAuthorizeUser As DataSet
                    Dim intTotalPaymentCount As Integer
                    Dim intTotalApprovedtCount As Integer
                    Dim intTotalAuthorizedtCount As Integer
                    Dim intMaxRowCount As Integer
                    Dim intCount As Integer = 0

                    'Sohail (24 Jun 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
                    'intTotalPaymentCount = objPayment.GetTotalPaymentCount(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                    intTotalPaymentCount = objPayment.GetTotalPaymentCount(mstrFromDatabaseName, xUserUnkid, objPeriod._Yearunkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                    'Sohail (24 Jun 2019) -- End

                    If blnExcelHTML = False Then 'Sohail (29 Apr 2014)


                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s10bw")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                        '--------------------

                        row = New WorksheetRow()

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Prepared By"), "s8bw")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s10bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Approved By"), "s8bw")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s10bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Authorized By"), "s8bw")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)
                        '--------------------

                        'Sohail (29 Apr 2014) -- Start
                        'Enhancement - Export Excel Report with Company Logo 
                    Else
                        strBuilder.Append("<TR>" & vbCrLf)
                        strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)

                        strBuilder.Append("<TR>" & vbCrLf)

                        strBuilder.Append("<TD colspan=2 style='border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 13, "Prepared By") & "</B></TD>" & vbCrLf)
                        strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                        strBuilder.Append("<TD colspan=2 style='border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 14, "Approved By") & "</B></TD>" & vbCrLf)
                        strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                        strBuilder.Append("<TD colspan=2 style='border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 15, "Authorized By") & "</B></TD>" & vbCrLf)

                        strBuilder.Append("</TR>" & vbCrLf)
                    End If
                    'Sohail (29 Apr 2014) -- End

                    intMaxRowCount = objLevel.GetLevelListCount()
                    dsLevel = objLevel.GetList("Level", True, True)
                    'intCount = objPayAuthorize.GetAuthorizedTotalCount(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                    intCount = objPayAuthorize.GetDISTINCTauthorizedUsersCount(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                    If intCount > intMaxRowCount Then
                        intMaxRowCount = intCount
                    End If
                    dsAuthorizeUser = objPayAuthorize.GetDISTINCTauthorizedUsers("Users", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)


                    For i = 0 To intMaxRowCount - 1
                        row = New WorksheetRow()
                        row.AutoFitHeight = True
                        'Sohail (29 Apr 2014) -- Start
                        'Enhancement - Export Excel Report with Company Logo 
                        If blnExcelHTML = True Then
                            strBuilder.Append("<TR>" & vbCrLf)
                        End If
                        'Sohail (29 Apr 2014) -- End

                        '*** Prepared By
                        If i = 0 Then
                            'Sohail (01 Feb 2014) -- Start
                            'dsList = objPayment.GetLastPaymentDetail("LastPayment", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                            'If dsList.Tables("LastPayment").Rows.Count > 0 Then
                            '    'Sohail (16 Mar 2013) -- Start
                            '    'TRA - ENHANCEMENT
                            '    'wcell = New WorksheetCell(dsList.Tables("LastPayment").Rows(0)("username").ToString & ", " & CDate(dsList.Tables("LastPayment").Rows(0)("paymentdate")), "s8w")
                            '    wcell = New WorksheetCell(dsList.Tables("LastPayment").Rows(0)("userfullname").ToString & ", " & CDate(dsList.Tables("LastPayment").Rows(0)("paymentdate")), "s8w")
                            '    'Sohail (16 Mar 2013) -- End
                            'Else
                            '    wcell = New WorksheetCell("", "s8w")
                            'End If
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'dsList = objPayment.GetDistinctPaymentUsers("LastPayment", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                            objPeriod = New clscommom_period_Tran
                            objPeriod._Periodunkid(mstrFromDatabaseName) = mintPeriodId
                            dsList = objPayment.GetDistinctPaymentUsers(mstrFromDatabaseName, xUserUnkid, objPeriod._Yearunkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "LastPayment", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId, True)
                            'Sohail (21 Aug 2015) -- End
                            If dsList.Tables("LastPayment").Rows.Count > 0 Then
                                Dim strDesc As String = ""
                                For Each dsRow As DataRow In dsList.Tables("LastPayment").Rows
                                    If strDesc.Trim = "" Then
                                        strDesc = dsRow("userfullname").ToString & ", " & CDate(dsRow("paymentdate"))
                                    Else
                                        strDesc &= "; " & dsRow("userfullname").ToString & ", " & CDate(dsRow("paymentdate"))
                                    End If

                                Next
                                If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                                    wcell = New WorksheetCell(strDesc, "s8w")
                                    'Sohail (29 Apr 2014) -- Start
                                    'Enhancement - Export Excel Report with Company Logo 
                                Else
                                    strBuilder.Append("<TD style='border-width: 0px;' colspan=2> " & strDesc & " </TD>" & vbCrLf)
                                End If
                                'Sohail (29 Apr 2014) -- End

                            Else
                                If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                                    wcell = New WorksheetCell("", "s8w")
                                    'Sohail (29 Apr 2014) -- Start
                                    'Enhancement - Export Excel Report with Company Logo 
                                Else
                                    strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                                End If
                                'Sohail (29 Apr 2014) -- End
                            End If

                            'Sohail (01 Feb 2014) -- End
                        Else
                            If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                                wcell = New WorksheetCell("", "s8w")
                                'Sohail (29 Apr 2014) -- Start
                                'Enhancement - Export Excel Report with Company Logo 
                            Else
                                strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                            End If
                            'Sohail (29 Apr 2014) -- End
                        End If
                        If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s10bw")
                            row.Cells.Add(wcell)
                            'Sohail (29 Apr 2014) -- Start
                            'Enhancement - Export Excel Report with Company Logo 
                        Else
                            strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                        End If
                        'Sohail (29 Apr 2014) -- End

                        '*** Approved By
                        If i <= dsLevel.Tables("Level").Rows.Count - 1 Then
                            intTotalApprovedtCount = objPaymentApproval.GetTotalApprovedCount(mintPeriodId, CInt(dsLevel.Tables("Level").Rows(i)("levelunkid")))

                            If intTotalApprovedtCount = intTotalPaymentCount Then
                                dsList = objPaymentApproval.GetLastApprovedDetail("List", mintPeriodId, CInt(dsLevel.Tables("Level").Rows(i)("levelunkid")))
                                If dsList.Tables("List").Rows.Count > 0 Then
                                    Dim strApproverDetail As String = ""
                                    For Each dsRow As DataRow In dsList.Tables("List").Rows
                                        If strApproverDetail.Trim = "" Then
                                            'Sohail (16 Mar 2013) -- Start
                                            'TRA - ENHANCEMENT
                                            'strApproverDetail = dsRow.Item("levelname").ToString & " : " & dsRow.Item("approvername").ToString & "," & CDate(dsRow.Item("approval_date"))
                                            strApproverDetail = dsRow.Item("levelname").ToString & " : " & dsRow.Item("approverfullname").ToString & "," & CDate(dsRow.Item("approval_date"))
                                            'Sohail (16 Mar 2013) -- End
                                        Else
                                            'Sohail (16 Mar 2013) -- Start
                                            'TRA - ENHANCEMENT
                                            'strApproverDetail &= "; " & dsRow.Item("approvername").ToString & "," & CDate(dsRow.Item("approval_date"))
                                            strApproverDetail &= "; " & dsRow.Item("approverfullname").ToString & "," & CDate(dsRow.Item("approval_date"))
                                            'Sohail (16 Mar 2013) -- End
                                        End If
                                    Next
                                    If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                                        wcell = New WorksheetCell(strApproverDetail, "s8w")
                                        'Sohail (29 Apr 2014) -- Start
                                        'Enhancement - Export Excel Report with Company Logo 
                                    Else
                                        strBuilder.Append("<TD style='border-width: 0px;' colspan=2>" & strApproverDetail & "</TD>" & vbCrLf)
                                    End If
                                    'Sohail (29 Apr 2014) -- End
                                Else
                                    If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                                        wcell = New WorksheetCell("", "s8w")
                                        'Sohail (29 Apr 2014) -- Start
                                        'Enhancement - Export Excel Report with Company Logo 
                                    Else
                                        strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                                    End If
                                    'Sohail (29 Apr 2014) -- End

                                End If
                            Else
                                If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                                    wcell = New WorksheetCell(dsLevel.Tables("Level").Rows(i)("levelname").ToString & " : " & Language.getMessage(mstrModuleName, 16, "Pending"), "s8w")
                                    'Sohail (29 Apr 2014) -- Start
                                    'Enhancement - Export Excel Report with Company Logo 
                                Else
                                    strBuilder.Append("<TD style='border-width: 0px;' colspan=2>" & dsLevel.Tables("Level").Rows(i)("levelname").ToString & " : " & Language.getMessage(mstrModuleName, 16, "Pending") & "</TD>" & vbCrLf)
                                End If
                                'Sohail (29 Apr 2014) -- End
                            End If
                        Else
                            If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                                wcell = New WorksheetCell("", "s8w")
                                'Sohail (29 Apr 2014) -- Start
                                'Enhancement - Export Excel Report with Company Logo 
                            Else
                                strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                            End If
                            'Sohail (29 Apr 2014) -- End
                        End If
                        If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s10bw")
                            row.Cells.Add(wcell)
                            'Sohail (29 Apr 2014) -- Start
                            'Enhancement - Export Excel Report with Company Logo 
                        Else
                            strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                        End If
                        'Sohail (29 Apr 2014) -- End



                        '*** Authorized By
                        If i <= dsAuthorizeUser.Tables("Users").Rows.Count - 1 Then
                            'Sohail (24 Jun 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
                            'intTotalAuthorizedtCount = objPayAuthorize.GetAuthorizedTotalCount(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                            intTotalAuthorizedtCount = objPayAuthorize.GetAuthorizedTotalCount(mstrFromDatabaseName, xUserUnkid, objPeriod._Yearunkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                            'Sohail (24 Jun 2019) -- End

                            If intTotalAuthorizedtCount = intTotalPaymentCount Then
                                dsList = objPayAuthorize.GetLastAuthorizationDetail("List", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId, CInt(dsAuthorizeUser.Tables("Users").Rows(i).Item("userunkid")))
                                If dsList.Tables("List").Rows.Count > 0 Then
                                    Dim strAuthorizeDetail As String = ""
                                    For Each dsRow As DataRow In dsList.Tables("List").Rows
                                        If strAuthorizeDetail.Trim = "" Then
                                            'Sohail (16 Mar 2013) -- Start
                                            'TRA - ENHANCEMENT
                                            'strAuthorizeDetail = dsRow.Item("username").ToString & "," & CDate(dsRow.Item("authorize_date"))
                                            strAuthorizeDetail = dsRow.Item("userfullname").ToString & "," & CDate(dsRow.Item("authorize_date"))
                                            'Sohail (16 Mar 2013) -- End
                                        Else
                                            'Sohail (16 Mar 2013) -- Start
                                            'TRA - ENHANCEMENT
                                            'strAuthorizeDetail &= "; " & dsRow.Item("username").ToString & "," & CDate(dsRow.Item("authorize_date"))
                                            strAuthorizeDetail &= "; " & dsRow.Item("userfullname").ToString & "," & CDate(dsRow.Item("authorize_date"))
                                            'Sohail (16 Mar 2013) -- End
                                        End If
                                    Next
                                    If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                                        wcell = New WorksheetCell(strAuthorizeDetail, "s8w")
                                        'Sohail (29 Apr 2014) -- Start
                                        'Enhancement - Export Excel Report with Company Logo 
                                    Else
                                        strBuilder.Append("<TD style='border-width: 0px;' colspan=2>" & strAuthorizeDetail & "</TD>" & vbCrLf)
                                    End If
                                    'Sohail (29 Apr 2014) -- End
                                Else
                                    If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                                        wcell = New WorksheetCell("", "s8w")
                                        'Sohail (29 Apr 2014) -- Start
                                        'Enhancement - Export Excel Report with Company Logo 
                                    Else
                                        strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                                    End If
                                    'Sohail (29 Apr 2014) -- End
                                End If
                            Else
                                If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                                    wcell = New WorksheetCell("", "s8w")
                                    'Sohail (29 Apr 2014) -- Start
                                    'Enhancement - Export Excel Report with Company Logo 
                                Else
                                    strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                                End If
                                'Sohail (29 Apr 2014) -- End
                            End If
                        Else
                            If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                                wcell = New WorksheetCell("", "s8w")
                                'Sohail (29 Apr 2014) -- Start
                                'Enhancement - Export Excel Report with Company Logo 
                            Else
                                strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                            End If
                            'Sohail (29 Apr 2014) -- End
                        End If
                        If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s10bw")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '--------------------
                            'Sohail (29 Apr 2014) -- Start
                            'Enhancement - Export Excel Report with Company Logo 
                        Else
                            strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                            strBuilder.Append("</TR>" & vbCrLf)
                        End If
                        'Sohail (29 Apr 2014) -- End

                    Next

                End If
                'End If

                'Sohail (29 Apr 2014) -- Start
                'Enhancement - Export Excel Report with Company Logo 
                If blnExcelHTML = True Then
                    If strBuilder.Length > 0 Then rowsArrayFooter.Add(strBuilder.ToString)
                    strBuilder.Remove(0, strBuilder.Length)
                End If
                'Sohail (29 Apr 2014) -- End
            End If
            'Sohail (11 Mar 2013) -- End


            'SET EXCEL CELL WIDTH
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For ii As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(ii) = 125
            Next
            'Sohail (20 Aug 2014) -- Start
            'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
            If mblnShowPaymentDetails = True Then
                intArrayColumnWidth(mdtTableExcel.Columns.Count - 1) = 200
            End If
            'Sohail (20 Aug 2014) -- End

            'SET EXCEL CELL WIDTH

            'Sohail (25 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, mstrReportTypeName & "(" & mstrCurrency_Sign & ")", "", " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, Nothing)
            If blnExcelHTML = False Then 'Sohail (29 Apr 2014)
                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, mstrReportTypeName & "(" & mstrCurrency_Sign & ")", "", " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, Nothing)
                'Sohail (29 Apr 2014) -- Start
                'Enhancement - Export Excel Report with Company Logo 
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportExecute(Company._Object._Companyunkid, CInt(mstrReportId), Nothing, enPrintAction.None, enExportAction.ExcelHTML, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, mstrReportTypeName & "(" & mstrCurrency_Sign & ")", "", " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, Nothing)
                Call ReportExecute(xCompanyUnkid, CInt(mstrReportId), Nothing, enPrintAction.None, enExportAction.ExcelHTML, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, mstrReportTypeName & "(" & mstrCurrency_Sign & ")", "", " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, Nothing)
                'Sohail (21 Aug 2015) -- End
            End If
            'Sohail (29 Apr 2014) -- End

            'Sohail (25 Mar 2013) -- End

            'Pinkal (14-Dec-2012) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_PayrollReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (16 Jun 2012) -- End

    'S.SANDEEP [ 29 SEP 2014 ] -- START
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Sub Generate_CustomPayrollHeadsReport()
    Public Sub Generate_CustomPayrollHeadsReport(ByVal mdicYearDBName As Dictionary(Of Integer, String) _
                                                 , ByVal xUserUnkid As Integer _
                                                 , ByVal xCompanyUnkid As Integer _
                                                 , ByVal xPeriodStart As Date _
                                                 , ByVal xPeriodEnd As Date _
                                                 , ByVal xUserModeSetting As String _
                                                 , ByVal xOnlyApproved As Boolean _
                                                 , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                 , ByVal blnApplyUserAccessFilter As Boolean _
                                                 , ByVal intBase_CurrencyId As Integer _
                                                 , ByVal blnSetPayslipPaymentApproval As Boolean _
                                                 , ByVal strfmtCurrency As String _
                                                 , ByVal strExportReportPath As String _
                                                 , ByVal blnOpenAfterExport As Boolean _
                                                 )
        'Sohail (21 Aug 2015) -- End
        Dim StrQ As String = String.Empty
        Dim StrInnerQ As String = String.Empty
        Dim exForce As Exception
        Dim dtFinalTable As DataTable
        Dim dsPayroll As New DataSet
        Dim xTotalStartCol As Integer = 0
        Dim dsPaymetDetail As New DataSet
        Dim mdicEmpBank As New Dictionary(Of String, String)
        Dim mdicEmpAccount As New Dictionary(Of String, String)
        Dim mdicEmpPayment As New Dictionary(Of String, String)
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mintBase_CurrencyId = intBase_CurrencyId
            mblnSetPayslipPaymentApproval = blnSetPayslipPaymentApproval
            mstrfmtCurrency = strfmtCurrency
            mstrExportReportPath = strExportReportPath
            mblnOpenAfterExport = blnOpenAfterExport
            'Sohail (21 Aug 2015) -- End
            'Sohail (21 Aug 2015) -- End

            If mstrUserAccessFilter.Trim = "" Then
                mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            End If
            dtFinalTable = New DataTable("Payroll") : Dim dCol As DataColumn

            dCol = New DataColumn("PeriodId")
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Int32")
            dtFinalTable.Columns.Add(dCol)
            xTotalStartCol += 1

            dCol = New DataColumn("Period")
            dCol.Caption = Language.getMessage(mstrModuleName, 39, "Period")
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)
            xTotalStartCol += 1

            dCol = New DataColumn("EmpCode")
            dCol.Caption = Language.getMessage(mstrModuleName, 40, "Code")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)
            xTotalStartCol += 1

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            'dCol = New DataColumn("EmpName")
            'dCol.Caption = Language.getMessage(mstrModuleName, 41, "Employee")
            'dCol.DataType = System.Type.GetType("System.String")
            'dtFinalTable.Columns.Add(dCol)
            'xTotalStartCol += 1
            If mblnShowEmpNameInSeprateColumns = False Then
            dCol = New DataColumn("EmpName")
            dCol.Caption = Language.getMessage(mstrModuleName, 41, "Employee")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)
            xTotalStartCol += 1
            Else
                dCol = New DataColumn("FirstName")
                dCol.Caption = Language.getMessage(mstrModuleName, 68, "First Name")
                dCol.DataType = System.Type.GetType("System.String")
                dtFinalTable.Columns.Add(dCol)
                xTotalStartCol += 1

                dCol = New DataColumn("MiddleName")
                dCol.Caption = Language.getMessage(mstrModuleName, 69, "Middle Name")
                dCol.DataType = System.Type.GetType("System.String")
                dtFinalTable.Columns.Add(dCol)
                xTotalStartCol += 1

                dCol = New DataColumn("Surname")
                dCol.Caption = Language.getMessage(mstrModuleName, 70, "Surname")
                dCol.DataType = System.Type.GetType("System.String")
                dtFinalTable.Columns.Add(dCol)
                xTotalStartCol += 1
            End If
           
            If mintMembershipUnkId > 0 Then
                dCol = New DataColumn("MembershipName")
                dCol.Caption = mstrMembershipName & " " & Language.getMessage(mstrModuleName, 71, "No.")
                dCol.DataType = System.Type.GetType("System.String")
                dtFinalTable.Columns.Add(dCol)
                xTotalStartCol += 1
            End If
            'Sohail (10 Jul 2019) -- End

            If mDicAllocationDetails.Keys.Count > 0 Then
                For Each xKey As Integer In mDicAllocationDetails.Keys
                    dCol = New DataColumn("Col_" & xKey.ToString)
                    dCol.Caption = mDicAllocationDetails(xKey)
                    dCol.DataType = System.Type.GetType("System.String")
                    dtFinalTable.Columns.Add(dCol)
                    xTotalStartCol += 1
                Next
            End If

            dCol = New DataColumn("GrpID")
            dCol.Caption = "GroupID"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)
            xTotalStartCol += 1

            dCol = New DataColumn("GrpName")
            dCol.Caption = "GroupName"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)
            xTotalStartCol += 1

            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'Dim xHeadVal() As String = mstrCustomReportHeadsIds.Split(",")
            Dim xHeadVal() As String = (From p As String In mstrCustomReportHeadsIds.Split(",") Where (IsNumeric(p)) Select (p.ToString)).ToArray
            Dim arrLoan() As String = (From p As String In mstrCustomReportHeadsIds.Split(",") Where (p.StartsWith("Loan")) Select (p.Substring(4).ToString)).ToArray
            Dim arrAdvance() As String = (From p As String In mstrCustomReportHeadsIds.Split(",") Where (p.StartsWith("Advance")) Select (p.Substring(7).ToString)).ToArray
            Dim arrSaving() As String = (From p As String In mstrCustomReportHeadsIds.Split(",") Where (p.StartsWith("Saving")) Select (p.Substring(6).ToString)).ToArray

            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            Dim arrCR() As String = (From p As String In mstrCustomReportHeadsIds.Split(",") Where (p.StartsWith("CR")) Select (p.Substring(2).ToString)).ToArray
            'Hemant (31 Aug 2018)) -- End

            'Sohail (03 Feb 2016) -- End
            Dim IsAssigned As Boolean = False
            'Sohail (29 Mar 2017) -- Start
            'CCK Enhancement - 65.2 - On Custom payroll report, loans are showing after the TOTAL Deduction column. They should show before the total deduction column.
            'For xId As Integer = 0 To xHeadVal.Length - 1
            '    'Nilay (12-Oct-2016) -- Start
            '    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
            '    'StrQ = "SELECT @HeadId = tranheadunkid,@HeadName = trnheadname FROM " & mstrCurrentDatabaseName & "..prtranhead_master WHERE tranheadunkid = '" & xHeadVal(xId) & "' "
            '    StrQ = "SELECT " & _
            '                "  @HeadId = tranheadunkid " & _
            '                ", @HeadName = trnheadname " & _
            '                ", @HeadCode = trnheadcode " & _
            '           "FROM " & mstrCurrentDatabaseName & "..prtranhead_master " & _
            '           "WHERE tranheadunkid = '" & xHeadVal(xId) & "' "
            '    'Nilay (12-Oct-2016) -- End

            '    objDataOperation.ClearParameters() : Dim xHeadId As Integer = -1 : Dim xHeadName As String = String.Empty
            '    'Nilay (12-Oct-2016) -- Start
            '    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
            '    Dim xHeadCode As String = String.Empty
            '    'Nilay (12-Oct-2016) -- End

            '    objDataOperation.AddParameter("@HeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, xHeadId, ParameterDirection.InputOutput)
            '    objDataOperation.AddParameter("@HeadName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xHeadName, ParameterDirection.InputOutput)
            '    'Nilay (12-Oct-2016) -- Start
            '    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
            '    objDataOperation.AddParameter("@HeadCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xHeadCode, ParameterDirection.InputOutput)
            '    'Nilay (12-Oct-2016) -- End

            '    objDataOperation.ExecNonQuery(StrQ)

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    xHeadId = objDataOperation.GetParameterValue("@HeadId")
            '    xHeadName = objDataOperation.GetParameterValue("@HeadName")
            '    'Nilay (12-Oct-2016) -- Start
            '    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
            '    xHeadCode = objDataOperation.GetParameterValue("@HeadCode")
            '    'Nilay (12-Oct-2016) -- End

            '    dCol = New DataColumn("Column" & xHeadId.ToString)
            '    dCol.DataType = System.Type.GetType("System.Decimal")
            '    dCol.DefaultValue = 0
            '    'Nilay (12-Oct-2016) -- Start
            '    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
            '    'dCol.Caption = xHeadName
            '    dCol.Caption = xHeadName & IIf(mstrCostCenterCode.Trim <> "", "#10;" & mstrCostCenterCode & xHeadCode, "")
            '    'Nilay (12-Oct-2016) -- End

            '    dtFinalTable.Columns.Add(dCol)
            '    If IsAssigned = False Then
            '        xTotalStartCol += 1
            '        IsAssigned = True
            '    End If
            'Next

            ''Sohail (03 Feb 2016) -- Start
            ''Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'Dim ds As DataSet
            'If arrLoan.Length > 0 Then
            '    If mblnShowLoansInSeparateColumns = False Then
            '        dCol = New DataColumn("Loan")
            '        dCol.Caption = Language.getMessage(mstrModuleName, 46, "Loan")
            '        dCol.DefaultValue = 0
            '        dCol.DataType = System.Type.GetType("System.Decimal")

            '        dtFinalTable.Columns.Add(dCol)
            '    Else
            '        Dim objLoan As New clsLoan_Scheme
            '        ds = objLoan.getComboList(False, "List", , "lnloan_scheme_master.loanschemeunkid IN (" & String.Join(",", arrLoan) & ") ")

            '        For Each dsRow As DataRow In ds.Tables("List").Rows

            '            dCol = New DataColumn("ColumnLoan" & dsRow.Item("loanschemeunkid").ToString)
            '            dCol.DataType = System.Type.GetType("System.Decimal")
            '            dCol.DefaultValue = 0
            '            dCol.Caption = dsRow.Item("name").ToString
            '            dtFinalTable.Columns.Add(dCol)
            '            'If IsAssigned = False Then
            '            '    xTotalStartCol += 1
            '            '    IsAssigned = True
            '            'End If

            '        Next
            '    End If
            'End If

            'If arrAdvance.Length > 0 Then

            '    dCol = New DataColumn("ColumnAdvance1")
            '    dCol.DataType = System.Type.GetType("System.Decimal")
            '    dCol.DefaultValue = 0
            '    dCol.Caption = Language.getMessage(mstrModuleName, 47, "Advance")
            '    dtFinalTable.Columns.Add(dCol)
            '    'If IsAssigned = False Then
            '    '    xTotalStartCol += 1
            '    '    IsAssigned = True
            '    'End If

            'End If

            'If arrSaving.Length > 0 Then
            '    If mblnShowSavingsInSeparateColumns = False Then
            '        dCol = New DataColumn("Savings")
            '        dCol.Caption = Language.getMessage(mstrModuleName, 48, "Savings")
            '        dCol.DefaultValue = 0
            '        dCol.DataType = System.Type.GetType("System.Decimal")

            '        dtFinalTable.Columns.Add(dCol)
            '    Else
            '        Dim objSaving As New clsSavingScheme
            '        ds = objSaving.getComboList(False, "List", , "svsavingscheme_master.savingschemeunkid IN (" & String.Join(",", arrSaving) & ") ")

            '        For Each dsRow As DataRow In ds.Tables("List").Rows

            '            dCol = New DataColumn("ColumnSavings" & dsRow.Item("savingschemeunkid").ToString)
            '            dCol.DataType = System.Type.GetType("System.Decimal")
            '            dCol.DefaultValue = 0
            '            dCol.Caption = dsRow.Item("name").ToString
            '            dtFinalTable.Columns.Add(dCol)
            '            'If IsAssigned = False Then
            '            '    xTotalStartCol += 1
            '            '    IsAssigned = True
            '            'End If

            '        Next
            '    End If
            'End If
            ''Sohail (03 Feb 2016) -- End
            Dim ds As DataSet = Nothing
            Dim dsLoan As DataSet = Nothing
            Dim dsSaving As DataSet = Nothing

            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            Dim dsCR As DataSet = Nothing
            'Hemant (31 Aug 2018)) -- End

            If xHeadVal.Length > 0 Then
                Dim objHead As New clsTransactionHead
                ds = objHead.getComboList(mstrCurrentDatabaseName, "Heads", False, , , , True, , "prtranhead_master.tranheadunkid IN (" & String.Join(",", xHeadVal) & ") ", True, True, True)
            End If
            If arrLoan.Length > 0 AndAlso mblnShowLoansInSeparateColumns = True Then
                Dim objLoan As New clsLoan_Scheme
                dsLoan = objLoan.getComboList(False, "List", , "lnloan_scheme_master.loanschemeunkid IN (" & String.Join(",", arrLoan) & ") ")
            End If
            If arrSaving.Length > 0 AndAlso mblnShowSavingsInSeparateColumns = True Then
                Dim objSaving As New clsSavingScheme
                dsSaving = objSaving.getComboList(False, "List", , "svsavingscheme_master.savingschemeunkid IN (" & String.Join(",", arrSaving) & ") ")
                End If
            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            If arrCR.Length > 0 AndAlso mblnShowCRInSeparateColumns = True Then
                Dim objExpense As New clsExpense_Master
                dsCR = objExpense.getComboList(-1, False, "List", , , , "cmexpense_master.expenseunkid IN (" & String.Join(",", arrCR) & ") ")
            End If
            'Hemant (31 Aug 2018)) -- End

            For Each strID As String In mstrCustomReportHeadsIds.Split(",")

                Dim dr() As DataRow = Nothing
                If strID.StartsWith("Loan") = True Then

                If mblnShowLoansInSeparateColumns = False Then
                        If dtFinalTable.Columns.Contains("Loan") = False Then
                    dCol = New DataColumn("Loan")
                    dCol.Caption = Language.getMessage(mstrModuleName, 46, "Loan")
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")

                    dtFinalTable.Columns.Add(dCol)
                        End If
                Else
                        dr = dsLoan.Tables(0).Select("loanschemeunkid = " & CInt(strID.Substring(4)) & " ")
                        If dr.Length > 0 Then

                            dCol = New DataColumn("ColumnLoan" & dr(0).Item("loanschemeunkid").ToString)
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                            dCol.Caption = dr(0).Item("name").ToString
                        dtFinalTable.Columns.Add(dCol)

                End If
            End If
                ElseIf strID.StartsWith("Advance") = True Then
                dCol = New DataColumn("ColumnAdvance1")
                dCol.DataType = System.Type.GetType("System.Decimal")
                dCol.DefaultValue = 0
                dCol.Caption = Language.getMessage(mstrModuleName, 47, "Advance")
                dtFinalTable.Columns.Add(dCol)

                ElseIf strID.StartsWith("Saving") = True Then

                    'Sohail (13 Jan 2020) -- Start
                    'NMB Issue # : Object refrence error on custom payroll report.
                    'If mblnShowLoansInSeparateColumns = False Then
                    If mblnShowSavingsInSeparateColumns = False Then
                        'Sohail (13 Jan 2020) -- End
                        If dtFinalTable.Columns.Contains("Savings") = False Then
                    dCol = New DataColumn("Savings")
                    dCol.Caption = Language.getMessage(mstrModuleName, 48, "Savings")
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")

                    dtFinalTable.Columns.Add(dCol)
                        End If
                    Else
                        dr = dsSaving.Tables(0).Select("savingschemeunkid = " & CInt(strID.Substring(6)) & " ")
                        If dr.Length > 0 Then
                            dCol = New DataColumn("ColumnSavings" & dr(0).Item("savingschemeunkid").ToString)
                            dCol.DataType = System.Type.GetType("System.Decimal")
                            dCol.DefaultValue = 0
                            dCol.Caption = dr(0).Item("name").ToString
                            dtFinalTable.Columns.Add(dCol)
                        End If
                    End If

                    'Hemant (31 Aug 2018) -- Start
                    'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
                ElseIf strID.StartsWith("CR") = True Then

                    If mblnShowCRInSeparateColumns = False Then
                        If dtFinalTable.Columns.Contains("CR") = False Then
                            dCol = New DataColumn("CR")
                            dCol.Caption = Language.getMessage(mstrModuleName, 67, "Claims & Expense")
                            dCol.DefaultValue = 0
                            dCol.DataType = System.Type.GetType("System.Decimal")

                            dtFinalTable.Columns.Add(dCol)
                        End If
                    Else
                        dr = dsCR.Tables(0).Select("id = " & CInt(strID.Substring(2)) & " ")
                        If dr.Length > 0 Then
                            dCol = New DataColumn("ColumnCR" & dr(0).Item("id").ToString)
                            dCol.DataType = System.Type.GetType("System.Decimal")
                            dCol.DefaultValue = 0
                            dCol.Caption = dr(0).Item("name").ToString
                            dtFinalTable.Columns.Add(dCol)
                        End If
                    End If
                    'Hemant (31 Aug 2018)) -- End

                Else
                    dr = ds.Tables(0).Select("tranheadunkid = " & CInt(strID) & " ")

                    If dr.Length > 0 Then
                        dCol = New DataColumn("Column" & strID.ToString)
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                        dCol.Caption = dr(0).Item("name").ToString & IIf(mstrCostCenterCode.Trim <> "", "#10;" & mstrCostCenterCode & dr(0).Item("code").ToString, "")

                        dtFinalTable.Columns.Add(dCol)
                        If IsAssigned = False Then
                            xTotalStartCol += 1
                            IsAssigned = True
                        End If
                    End If

                End If

                    Next
            'Sohail (29 Mar 2017) -- End

            dCol = New DataColumn("Bank")
            dCol.Caption = Language.getMessage(mstrModuleName, 42, "Bank Details")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Account")
            dCol.Caption = Language.getMessage(mstrModuleName, 43, "Account#")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("PaymentDetail")
            dCol.Caption = Language.getMessage(mstrModuleName, 44, "Payment Detail")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            'Sohail (03 Nov 2021) -- Start
            'HJIF Issue : HJIF-AH-4234 : TRANSACTION HEADS PROBLEM NSSF (Bring difference of 0.003 when you compare with 10% of gross and total of NSSF).
            decDecimalPlaces = 6
            'Sohail (03 Nov 2021) -- End

            'Sohail (21 Sep 2016) -- Start
            'Issue - 63.1 - Error 'Incorrect syntax near (' when no payroll heads selected.
            If xHeadVal.Length <= 0 Then
                Dim tmp() As String = {"-999"}
                xHeadVal = tmp
            End If
            'Sohail (21 Sep 2016) -- End

            'Sohail (20 Jan 2020) -- Start
            'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
            StrQ = ""
            'Sohail (17 Jun 2020) -- Start
            'NMB Issue # : Performance issue on custom payroll report.
            StrQ &= "SELECT   hremployee_master.employeeunkid " & _
                            ", hremployee_master.employeecode " & _
                            ", hremployee_master.appointeddate  " & _
                            ",ISNULL(SM.name,'') AS Col_1 " & _
                            ",ISNULL(DGM.name,'') AS Col_2 " & _
                            ",ISNULL(DM.name,'') AS Col_3 " & _
                            ",ISNULL(SECG.name,'') AS Col_4 " & _
                            ",ISNULL(SEC.name,'') AS Col_5 " & _
                            ",ISNULL(UGM.name,'') AS Col_6 " & _
                            ",ISNULL(UM.name,'') AS Col_7 " & _
                            ",ISNULL(TM.name,'') AS Col_8 " & _
                            ",ISNULL(JGM.name,'') AS Col_9 " & _
                            ",ISNULL(JM.job_name,'') AS Col_10 " & _
                            ",ISNULL(CGM.name,'') AS Col_11 " & _
                            ",ISNULL(CM.name,'') AS Col_12 " & _
                            ",ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') AS Col_13 " & _
                            ",ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS Col_14  " & _
                            ",ISNULL(CONVERT(CHAR(8),ECNF.confirmation_date,112),'') AS Col_15 " & _
                            ",ISNULL(CONVERT(CHAR(8),ERET.termination_to_date,112),'') AS Col_16 " & _
                            ",ISNULL(CONVERT(CHAR(8),ETERM.empl_enddate,112),'') AS Col_17 " & _
                            ",ISNULL(CC.costcentername,'') AS Col_18 " & _
                            ",ISNULL(GGM.name,'') AS Col_19 " & _
                            ",ISNULL(GM.name,'') AS Col_20 " & _
                            ",ISNULL(GLM.name,'') AS Col_21 " & _
                            ",ISNULL(etype.name,'') AS Col_22 " & _
                            ",CASE WHEN gender = 0 THEN '' " & _
                            "      WHEN gender = 1 THEN @Male " & _
                            "      WHEN gender = 2 THEN @Female END AS Col_23 " & _
                            ",ISNULL(mtype.name,'') AS Col_24 " & _
                            ",ISNULL(CC.costcentercode,'') AS Col_25 " & _
                            ",ISNULL(CC.customcode,'') AS Col_26 "

            If mblnShowEmpNameInSeprateColumns = False Then
                If mblnFirstNamethenSurname = True Then
                    StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                Else
                    StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
                End If
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                        ", ISNULL(hremployee_master.othername, '') AS othername " & _
                        ", ISNULL(hremployee_master.surname, '') AS surname "
            End If

            If mintMembershipUnkId > 0 Then
                StrQ &= ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipno "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",'' AS Id, '' AS GName "
            End If

            StrQ &= "INTO #CurrEmp "
            StrQ &= "FROM  " & mstrCurrentDatabaseName & "..hremployee_master "
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 gradegroupunkid " & _
                    "		,gradeunkid " & _
                    "		,gradelevelunkid " & _
                    "		,employeeunkid AS GEmpId " & _
                    "	FROM " & _
                    "	( " & _
                    "		SELECT " & _
                    "			 gradegroupunkid " & _
                    "			,gradeunkid " & _
                    "			,gradelevelunkid " & _
                    "			,employeeunkid " & _
                    "			,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                    "		FROM prsalaryincrement_tran " & _
                    "		WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "	) AS GRD WHERE GRD.Rno = 1 " & _
                    ") AS EGRD ON EGRD.GEmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "        Trf.TrfEmpId " & _
                    "       ,ISNULL(Trf.stationunkid,0) AS stationunkid " & _
                    "       ,ISNULL(Trf.deptgroupunkid,0) AS deptgroupunkid " & _
                    "       ,ISNULL(Trf.departmentunkid,0) AS departmentunkid " & _
                    "       ,ISNULL(Trf.sectiongroupunkid,0) AS sectiongroupunkid " & _
                    "       ,ISNULL(Trf.sectionunkid,0) AS sectionunkid " & _
                    "       ,ISNULL(Trf.unitgroupunkid,0) AS unitgroupunkid " & _
                    "       ,ISNULL(Trf.unitunkid,0) AS unitunkid " & _
                    "       ,ISNULL(Trf.teamunkid,0) AS teamunkid " & _
                    "       ,ISNULL(Trf.classgroupunkid,0) AS classgroupunkid " & _
                    "       ,ISNULL(Trf.classunkid,0) AS classunkid " & _
                    "       ,ISNULL(Trf.EfDt,'') AS EfDt " & _
                    "       ,Trf.ETT_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            ETT.employeeunkid AS TrfEmpId " & _
                    "           ,ISNULL(ETT.stationunkid,0) AS stationunkid " & _
                    "           ,ISNULL(ETT.deptgroupunkid,0) AS deptgroupunkid " & _
                    "           ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
                    "           ,ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
                    "           ,ISNULL(ETT.sectionunkid,0) AS sectionunkid " & _
                    "           ,ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
                    "           ,ISNULL(ETT.unitunkid,0) AS unitunkid " & _
                    "           ,ISNULL(ETT.teamunkid,0) AS teamunkid " & _
                    "           ,ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
                    "           ,ISNULL(ETT.classunkid,0) AS classunkid " & _
                    "           ,CONVERT(CHAR(8),ETT.effectivedate,112) AS EfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                    "           ,CASE WHEN ETT.rehiretranunkid > 0 THEN RTT.name ELSE TTC.name END AS ETT_REASON " & _
                    "       FROM hremployee_transfer_tran AS ETT " & _
                    "           LEFT JOIN cfcommon_master AS RTT ON RTT.masterunkid = ETT.changereasonunkid AND RTT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "           LEFT JOIN cfcommon_master AS TTC ON TTC.masterunkid = ETT.changereasonunkid AND TTC.mastertype = '" & clsCommon_Master.enCommonMaster.TRANSFERS & "' " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS Trf WHERE Trf.Rno = 1 " & _
                    ") AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid AND ETRF.EfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "     Cat.CatEmpId " & _
                    "    ,Cat.jobgroupunkid " & _
                    "    ,Cat.jobunkid " & _
                    "    ,Cat.CEfDt " & _
                    "    ,Cat.RECAT_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            ECT.employeeunkid AS CatEmpId " & _
                    "           ,ECT.jobgroupunkid " & _
                    "           ,ECT.jobunkid " & _
                    "           ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                    "           ,CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
                    "       FROM hremployee_categorization_tran AS ECT " & _
                    "           LEFT JOIN cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "           LEFT JOIN cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS Cat WHERE Cat.Rno = 1 " & _
                    ") AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 CCT.CCTEmpId " & _
                    "		,CCT.costcenterunkid " & _
                    "		,CCT.CTEfDt " & _
                    "		,CC_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            CCT.employeeunkid AS CCTEmpId " & _
                    "           ,CCT.cctranheadvalueid AS costcenterunkid " & _
                    "           ,CONVERT(CHAR(8),CCT.effectivedate,112) AS CTEfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN CCT.rehiretranunkid > 0 THEN ISNULL(RCS.name,'') ELSE ISNULL(CTC.name,'') END AS CC_REASON " & _
                    "       FROM hremployee_cctranhead_tran AS CCT " & _
                    "			LEFT JOIN cfcommon_master AS RCS ON RCS.masterunkid = CCT.changereasonunkid AND RCS.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS CTC ON CTC.masterunkid = CCT.changereasonunkid AND CTC.mastertype = '" & clsCommon_Master.enCommonMaster.COST_CENTER & "' " & _
                    "		WHERE CCT.isvoid = 0 AND CCT.istransactionhead = 0 AND CONVERT(CHAR(8),CCT.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   )CCT WHERE CCT.Rno = 1 " & _
                    ")AS ECCT ON ECCT.CCTEmpId = hremployee_master.employeeunkid AND ECCT.CTEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    		 PROB.PDEmpId " & _
                    "    		,PROB.probation_from_date " & _
                    "    		,PROB.probation_to_date " & _
                    "    		,PROB.PDEfDt " & _
                    "    		,PROB.PB_REASON " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             PDT.employeeunkid AS PDEmpId " & _
                    "            ,CONVERT(CHAR(8),PDT.date1,112) AS probation_from_date " & _
                    "            ,CONVERT(CHAR(8),PDT.date2,112) AS probation_to_date " & _
                    "            ,CONVERT(CHAR(8),PDT.effectivedate,112) AS PDEfDt " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY PDT.employeeunkid ORDER BY PDT.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN PDT.rehiretranunkid > 0 THEN ISNULL(RPB.name,'') ELSE ISNULL(PBC.name,'') END AS PB_REASON " & _
                    "        FROM hremployee_dates_tran AS PDT " & _
                    "            LEFT JOIN cfcommon_master AS RPB ON RPB.masterunkid = PDT.changereasonunkid AND RPB.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS PBC ON PBC.masterunkid = PDT.changereasonunkid AND PBC.mastertype = '" & clsCommon_Master.enCommonMaster.PROBATION & "' " & _
                    "        WHERE isvoid = 0 AND PDT.datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' AND CONVERT(CHAR(8),PDT.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS PROB WHERE PROB.Rno = 1 " & _
                    ") AS EPROB ON EPROB.PDEmpId = hremployee_master.employeeunkid AND EPROB.PDEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    		 TERM.TEEmpId " & _
                    "    		,TERM.empl_enddate " & _
                    "    		,TERM.termination_from_date " & _
                    "    		,TERM.TEfDt " & _
                    "    		,TERM.isexclude_payroll " & _
                    "    		,TERM.TR_REASON " & _
                    "    		,TERM.changereasonunkid " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             TRM.employeeunkid AS TEEmpId " & _
                    "            ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                    "            ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                    "            ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                    "            ,TRM.isexclude_payroll " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                    "            ,TRM.changereasonunkid " & _
                    "        FROM hremployee_dates_tran AS TRM " & _
                    "            LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                    "        WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS TERM WHERE TERM.Rno = 1 " & _
                    ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    	 CNF.CEmpId " & _
                    "    	,CNF.confirmation_date " & _
                    "       ,CNF.CEfDt " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             CNF.employeeunkid AS CEmpId " & _
                    "            ,CONVERT(CHAR(8),CNF.date1,112) AS confirmation_date " & _
                    "            ,CONVERT(CHAR(8),CNF.effectivedate,112) AS CEfDt " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY CNF.employeeunkid ORDER BY CNF.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN CNF.rehiretranunkid > 0 THEN ISNULL(CTE.name,'') ELSE ISNULL(CEC.name,'') END AS CF_REASON " & _
                    "            ,CNF.changereasonunkid " & _
                    "        FROM hremployee_dates_tran AS CNF " & _
                    "            LEFT JOIN cfcommon_master AS CTE ON CTE.masterunkid = CNF.changereasonunkid AND CTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS CEC ON CEC.masterunkid = CNF.changereasonunkid AND CEC.mastertype = '" & clsCommon_Master.enCommonMaster.CONFIRMATION & "' " & _
                    "        WHERE isvoid = 0 AND CNF.datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),CNF.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS CNF WHERE CNF.Rno = 1 " & _
                    ") AS ECNF ON ECNF.CEmpId = hremployee_master.employeeunkid AND ECNF.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    	 RET.REmpId " & _
                    "    	,RET.termination_to_date " & _
                    "    	,RET.REfDt " & _
                    "    	,RET.RET_REASON " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             RTD.employeeunkid AS REmpId " & _
                    "            ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                    "            ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                    "        FROM hremployee_dates_tran AS RTD " & _
                    "            LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                    "        WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS RET WHERE RET.Rno = 1 " & _
                    ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrstation_master AS SM on SM.stationunkid = ETRF.stationunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrdepartment_group_master AS DGM on DGM.deptgroupunkid = ETRF.deptgroupunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrdepartment_master AS DM ON DM.departmentunkid = ETRF.departmentunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrsectiongroup_master AS SECG ON ETRF.sectiongroupunkid = SECG.sectiongroupunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrsection_master AS SEC ON SEC.sectionunkid = ETRF.sectionunkid  " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrunitgroup_master AS UGM ON ETRF.unitgroupunkid = UGM.unitgroupunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrunit_master AS UM on UM.unitunkid = ETRF.unitunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrteam_master AS TM ON ETRF.teamunkid = TM.teamunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrjobgroup_master AS JGM on JGM.jobgroupunkid = ERECAT.jobgroupunkid  " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrjob_master AS JM ON JM.jobunkid = ERECAT.jobunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrclassgroup_master AS CGM ON CGM.classgroupunkid = ETRF.classgroupunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrclasses_master AS CM on CM.classesunkid = ETRF.classunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..prcostcenter_master AS CC on CC.costcenterunkid = ECCT.costcenterunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrgradegroup_master AS GGM on GGM.gradegroupunkid = EGRD.gradegroupunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrgrade_master AS GM on GM.gradeunkid = EGRD.gradeunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrgradelevel_master AS GLM on GLM.gradelevelunkid = EGRD.gradelevelunkid  " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..cfcommon_master as mtype ON mtype.masterunkid = hremployee_master.maritalstatusunkid AND mtype.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..cfcommon_master AS etype ON etype.masterunkid = hremployee_master.employmenttypeunkid AND etype.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " "
                    
            StrQ &= mstrAnalysis_Join

            If mintMembershipUnkId > 0 Then
                StrQ &= "LEFT JOIN " & mstrCurrentDatabaseName & "..hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                "AND hremployee_meminfo_tran.membershipunkid = @membershipunkid "
            End If
            'Sohail (17 Jun 2020) -- End

            For Each key In mdicYearDBName

                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , key.Value)
                If mblnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, key.Value, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, key.Value)

                StrQ &= "SELECT   hremployee_master.employeeunkid "
                StrQ &= "INTO #" & key.Value & " "
                StrQ &= "FROM  " & key.Value & "..hremployee_master "

                StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE 1 = 1 "

                If mintEmployeeId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mintBranchId > 0 Then
                    StrQ &= " AND T.stationunkid = @BranchId "
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

            Next
            'Sohail (20 Jan 2020) -- End

            StrQ &= "; SELECT  PeriodId " & _
                   ", ISNULL(period_name, '') AS PeriodName " & _
                   ", EmpId "

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            If mintMembershipUnkId > 0 Then
                'Sohail (17 Jun 2020) -- Start
                'NMB Issue # : Performance issue on custom payroll report.
                'StrQ &= ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipno "
                StrQ &= ", #CurrEmp.membershipno "
                'Sohail (17 Jun 2020) -- End
            End If
            'Sohail (10 Jul 2019) -- End

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            'If mblnFirstNamethenSurname = True Then
            '    StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
            'Else
            '    StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
            'End If
            'Sohail (17 Jun 2020) -- Start
            'NMB Issue # : Performance issue on custom payroll report.
            'If mblnShowEmpNameInSeprateColumns = False Then
            '    If mblnFirstNamethenSurname = True Then
            '        StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
            '    Else
            '        StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
            '    End If
            'Else
            '    StrQ &= ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
            '            ", ISNULL(hremployee_master.othername, '') AS othername " & _
            '            ", ISNULL(hremployee_master.surname, '') AS surname "
            'End If
            If mblnShowEmpNameInSeprateColumns = False Then
                StrQ &= ", #CurrEmp.EmpName "
            Else
                StrQ &= ", #CurrEmp.firstname " & _
                        ", #CurrEmp.othername " & _
                        ", #CurrEmp.surname "
                'Sohail (26 Oct 2020) - [#CurrEmp..surname] = [#CurrEmp.surname] (Invalid column name '')
            End If
            'Sohail (17 Jun 2020) -- End
            'Sohail (10 Jul 2019) -- End

            StrQ &= ",#CurrEmp.employeecode AS Code " & _
                    ",TranId " & _
                    ", CASE ismonetary WHEN 1 THEN (Amount * " & mdecEx_Rate & ") ELSE Amount END AS Amount " & _
                    ",Mid " & _
                    ", CASE ismonetary WHEN 1 THEN (Openingbalance * " & mdecEx_Rate & ") ELSE Openingbalance END AS Openingbalance " & _
                    ",#CurrEmp.Col_1 " & _
                    ",#CurrEmp.Col_2 " & _
                    ",#CurrEmp.Col_3 " & _
                    ",#CurrEmp.Col_4 " & _
                    ",#CurrEmp.Col_5 " & _
                    ",#CurrEmp.Col_6 " & _
                    ",#CurrEmp.Col_7 " & _
                    ",#CurrEmp.Col_8 " & _
                    ",#CurrEmp.Col_9 " & _
                    ",#CurrEmp.Col_10 " & _
                    ",#CurrEmp.Col_11 " & _
                    ",#CurrEmp.Col_12 " & _
                    ",#CurrEmp.Col_13 " & _
                    ",#CurrEmp.Col_14  " & _
                    ",#CurrEmp.Col_15 " & _
                    ",#CurrEmp.Col_16 " & _
                    ",#CurrEmp.Col_17 " & _
                    ",#CurrEmp.Col_18 " & _
                    ",#CurrEmp.Col_19 " & _
                    ",#CurrEmp.Col_20 " & _
                    ",#CurrEmp.Col_21 " & _
                    ",#CurrEmp.Col_22 " & _
                    ",#CurrEmp.Col_23 " & _
                    ",#CurrEmp.Col_24 " & _
                    ",#CurrEmp.Col_25 " & _
                    ",#CurrEmp.Col_26 "
            'Sohail (03 Jan 2020) - [costcentercode, customcode]
            'Sohail (30 Oct 2018) - [,(Amount * " & mdecEx_Rate & ") AS Amount] = [, CASE ismonetary WHEN 1 THEN (Amount * " & mdecEx_Rate & ") ELSE Amount END AS Amount]
            '                       [,(Openingbalance * " & mdecEx_Rate & ") AS Openingbalance] = [, CASE ismonetary WHEN 1 THEN (Openingbalance * " & mdecEx_Rate & ") ELSE Openingbalance END AS Openingbalance]

            'Sohail (17 Jun 2020) -- Start
            'NMB Issue # : Performance issue on custom payroll report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ",'' AS Id, '' AS GName "
            'End If
            StrQ &= ",#CurrEmp.Id, #CurrEmp.GName "
            'Sohail (17 Jun 2020) -- End
            StrQ &= "FROM    ( "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'For i As Integer = 0 To marrDatabaseName.Count - 1
            '    mstrFromDatabaseName = marrDatabaseName(i)
            Dim i As Integer = -1
            For Each key In mdicYearDBName
                mstrFromDatabaseName = key.Value
                i += 1
                'Sohail (21 Aug 2015) -- End

                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                ''Sohail (21 Aug 2015) -- Start
                ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                'xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrFromDatabaseName)
                ''Sohail (13 Feb 2016) -- Start
                ''Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
                ''Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                'If mblnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                ''Sohail ((13 Feb 2016) -- End
                'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrFromDatabaseName)
                ''Sohail (21 Aug 2015) -- End
                'Sohail (20 Jan 2020) -- End

                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If

                StrInnerQ &= "SELECT DISTINCT " & _
                             "  payperiodunkid AS PeriodId " & _
                             " ,prtnaleave_tran.employeeunkid AS EmpId " & _
                             " ,prpayrollprocess_tran.tranheadunkid AS TranId " & _
                             " ,SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                             " ,10 AS Mid " & _
                             " ,openingbalance AS Openingbalance " & _
                             " , CASE trnheadtype_id WHEN " & CInt(enTranHeadType.Informational) & " THEN ismonetary ELSE 1 END AS ismonetary " & _
                             "FROM " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                             "  LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "  JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                             "  LEFT JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
                'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                'Sohail (30 Oct 2018) - [ismonetary]

                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                ''Sohail (21 Aug 2015) -- Start
                ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrInnerQ &= "LEFT JOIN " & _
                '            "( " & _
                '            "    SELECT " & _
                '            "         stationunkid " & _
                '            "        ,deptgroupunkid " & _
                '            "        ,departmentunkid " & _
                '            "        ,sectiongroupunkid " & _
                '            "        ,sectionunkid " & _
                '            "        ,unitgroupunkid " & _
                '            "        ,unitunkid " & _
                '            "        ,teamunkid " & _
                '            "        ,classgroupunkid " & _
                '            "        ,classunkid " & _
                '            "        ,employeeunkid " & _
                '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrInnerQ &= xDateJoinQry
                'End If

                ''S.SANDEEP [15 NOV 2016] -- START
                ''If xUACQry.Trim.Length > 0 Then
                ''    StrInnerQ &= xUACQry
                ''End If
                'If blnApplyUserAccessFilter = True Then
                'If xUACQry.Trim.Length > 0 Then
                '    StrInnerQ &= xUACQry
                'End If
                'End If
                ''S.SANDEEP [15 NOV 2016] -- END

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrInnerQ &= xAdvanceJoinQry
                'End If
                ''Sohail (21 Aug 2015) -- End
                'Sohail (20 Jan 2020) -- End

                StrInnerQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                             "  AND prtnaleave_tran.isvoid = 0 " & _
                             "  AND prtranhead_master.isvoid = 0 " & _
                             "  AND prtranhead_master.tranheadunkid IN(" & String.Join(",", xHeadVal) & ") " & _
                             "  AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                'Sohail (03 Feb 2016) - ["  AND prtranhead_master.tranheadunkid IN(" & mstrCustomReportHeadsIds & ") " & _] = ["  AND prtranhead_master.tranheadunkid IN(" & String.Join(",", xHeadVal) & ") " & _]

                'Sohail (20 Jan 2020) -- Start
                'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                'If mintEmployeeId > 0 Then
                '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                '    objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                'End If
                'If mintBranchId > 0 Then
                '    'Sohail (21 Aug 2015) -- Start
                '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                '    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                '    'Sohail (21 Aug 2015) -- End
                '    objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
                'End If
                'If mstrAdvance_Filter.Trim.Length > 0 Then
                '    StrInnerQ &= " AND " & mstrAdvance_Filter
                'End If
                ''Sohail (21 Aug 2015) -- Start
                ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                ''If mblnIsActive = False Then
                ''    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                ''                 " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                ''                 " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                ''                 " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                ''End If
                ''If mstrUserAccessFilter.Length > 0 Then
                ''    StrInnerQ &= mstrUserAccessFilter
                ''End If
                'If blnApplyUserAccessFilter = True Then
                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrInnerQ &= " AND " & xUACFiltrQry
                'End If
                'End If

                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrInnerQ &= xDateFilterQry
                '    End If
                'End If
                ''Sohail (21 Aug 2015) -- End
                ''Sohail (20 Jan 2020) -- End
                StrInnerQ &= " GROUP BY payperiodunkid " & _
                             ", prtnaleave_tran.employeeunkid " & _
                             ", prpayrollprocess_tran.tranheadunkid " & _
                             ", openingbalance " & _
                             ", trnheadtype_id " & _
                             ", ismonetary "
                'Sohail (30 Oct 2018) - [trnheadtype_id, ismonetary]

                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                If arrLoan.Length > 0 Then
                    StrInnerQ &= "UNION ALL " & _
                                 "SELECT  payperiodunkid AS PeriodId " & _
                                       ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                       ", " & IIf(mblnShowLoansInSeparateColumns = True, "lnloan_advance_tran.loanschemeunkid", "-1").ToString & " AS TranId " & _
                                       ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                       ", 1 AS Mid " & _
                                       ", openingbalance AS Openingbalance " & _
                                       ", 1 AS ismonetary " & _
                                 "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                         "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                         "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                         "LEFT JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (30 Oct 2018) - [ismonetary]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND lnloan_advance_tran.isvoid = 0 " & _
                                    "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                    "AND lnloan_advance_tran.isloan = 1 " & _
                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) " & _
                                    "AND lnloan_advance_tran.loanschemeunkid IN (" & String.Join(",", arrLoan) & ") "

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    'If mintEmployeeId > 0 Then
                    '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                    'End If

                    'If mstrAdvance_Filter.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & mstrAdvance_Filter
                    'End If

                    'If blnApplyUserAccessFilter = True Then
                    '    If xUACFiltrQry.Trim.Length > 0 Then
                    '        StrInnerQ &= " AND " & xUACFiltrQry
                    '    End If
                    'End If

                    'If xIncludeIn_ActiveEmployee = False Then
                    '    If xDateFilterQry.Trim.Length > 0 Then
                    '        StrInnerQ &= xDateFilterQry
                    '    End If
                    'End If

                    'If mintBranchId > 0 Then
                    '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                    'End If
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= " GROUP BY payperiodunkid " & _
                                 ", #" & mstrFromDatabaseName & ".employeeunkid " & _
                                 ", openingbalance " & _
                                 " " & IIf(mblnShowLoansInSeparateColumns = True, ", lnloan_advance_tran.loanschemeunkid", "").ToString & " "

                End If

                If arrAdvance.Length > 0 Then
                    StrInnerQ &= "UNION ALL " & _
                                 "SELECT  payperiodunkid AS PeriodId " & _
                                       ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                       ", -1 AS TranId " & _
                                       ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                       ", 2 AS Mid " & _
                                       ", openingbalance AS Openingbalance " & _
                                       ", 1 AS ismonetary " & _
                                 "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                         "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                         "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                         "LEFT JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (30 Oct 2018) - [ismonetary]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND lnloan_advance_tran.isvoid = 0 " & _
                                    "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                    "AND lnloan_advance_tran.isloan = 0 " & _
                                    "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    'If mintEmployeeId > 0 Then
                    '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                    'End If

                    'If mstrAdvance_Filter.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & mstrAdvance_Filter
                    'End If

                    'If blnApplyUserAccessFilter = True Then
                    '    If xUACFiltrQry.Trim.Length > 0 Then
                    '        StrInnerQ &= " AND " & xUACFiltrQry
                    '    End If
                    'End If

                    'If xIncludeIn_ActiveEmployee = False Then
                    '    If xDateFilterQry.Trim.Length > 0 Then
                    '        StrInnerQ &= xDateFilterQry
                    '    End If
                    'End If

                    'If mintBranchId > 0 Then
                    '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                    'End If
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= " GROUP BY payperiodunkid " & _
                                ", #" & mstrFromDatabaseName & ".employeeunkid " & _
                                ", openingbalance "
                End If

                If arrSaving.Length > 0 Then
                    StrInnerQ &= " UNION ALL " & _
                                 "SELECT  prtnaleave_tran.payperiodunkid AS PeriodId " & _
                                       ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                       ", " & IIf(mblnShowSavingsInSeparateColumns = True, "svsaving_tran.savingschemeunkid", "-1").ToString & " AS TranId " & _
                                       ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                       ", 3 AS Mid " & _
                                       ", openingbalance AS Openingbalance " & _
                                       ", 1 AS ismonetary " & _
                                 "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                         "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                         "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                         "LEFT JOIN " & mstrFromDatabaseName & "..svsaving_tran ON svsaving_tran.savingtranunkid = prpayrollprocess_tran.savingtranunkid "
                    'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (30 Oct 2018) - [ismonetary]

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If

                    ''S.SANDEEP [15 NOV 2016] -- START
                    ''If xUACQry.Trim.Length > 0 Then
                    ''    StrInnerQ &= xUACQry
                    ''End If
                    'If blnApplyUserAccessFilter = True Then
                    'If xUACQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xUACQry
                    'End If
                    'End If
                    ''S.SANDEEP [15 NOV 2016] -- END

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND svsaving_tran.isvoid = 0 " & _
                                        "AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIdList & " ) " & _
                                        "AND svsaving_tran.savingschemeunkid IN (" & String.Join(",", arrSaving) & ") "

                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    'If mintEmployeeId > 0 Then
                    '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                    'End If

                    'If mstrAdvance_Filter.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & mstrAdvance_Filter
                    'End If

                    'If blnApplyUserAccessFilter = True Then
                    '    If xUACFiltrQry.Trim.Length > 0 Then
                    '        StrInnerQ &= " AND " & xUACFiltrQry
                    '    End If
                    'End If

                    'If xIncludeIn_ActiveEmployee = False Then
                    '    If xDateFilterQry.Trim.Length > 0 Then
                    '        StrInnerQ &= xDateFilterQry
                    '    End If
                    'End If

                    'If mintBranchId > 0 Then
                    '    StrInnerQ &= " AND T.stationunkid = @BranchId "
                    'End If
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= " GROUP BY prtnaleave_tran.payperiodunkid " & _
                                 ", #" & mstrFromDatabaseName & ".employeeunkid " & _
                                 ", openingbalance " & _
                                 " " & IIf(mblnShowSavingsInSeparateColumns = True, ", svsaving_tran.savingschemeunkid", "").ToString & " "
                End If
                'Sohail (03 Feb 2016) -- End

                'Hemant (31 Aug 2018) -- Start
                'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
                If arrCR.Length > 0 Then
                    StrInnerQ &= "UNION ALL " & _
                                "SELECT  payperiodunkid AS PeriodId " & _
                                      ", #" & mstrFromDatabaseName & ".employeeunkid AS EmpId " & _
                                      ", ISNULL(cmexpense_master.expenseunkid, crretireexpense.expenseunkid) AS TranId " & _
                                      ", SUM(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                      ", CASE WHEN cmexpense_master.expenseunkid > 0 THEN CASE cmexpense_master.trnheadtype_id WHEN 1 THEN 14 ELSE 15 END ELSE CASE WHEN prpayrollprocess_tran.add_deduct = 1 THEN 14 ELSE 15 END END AS Mid " & _
                                      ", SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                                      ", 1 AS ismonetary " & _
                                "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                        "JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                        "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_retirement_master ON cmretire_process_tran.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                    'Sohail (27 Apr 2021) - [SUM(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(prpayrollprocess_tran.amount)]
                    'Sohail (20 Jan 2020) - [LEFT JOIN hremployee_master] = [JOIN #" & mstrFromDatabaseName & "]
                    'Sohail (30 Oct 2018) - [ismonetary]
                   
                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    'StrInnerQ &= "LEFT JOIN " & _
                    '            "( " & _
                    '            "    SELECT " & _
                    '            "         stationunkid " & _
                    '            "        ,deptgroupunkid " & _
                    '            "        ,departmentunkid " & _
                    '            "        ,sectiongroupunkid " & _
                    '            "        ,sectionunkid " & _
                    '            "        ,unitgroupunkid " & _
                    '            "        ,unitunkid " & _
                    '            "        ,teamunkid " & _
                    '            "        ,classgroupunkid " & _
                    '            "        ,classunkid " & _
                    '            "        ,employeeunkid " & _
                    '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                    '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    '            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xDateJoinQry
                    'End If
                   
                    'If blnApplyUserAccessFilter = True Then
                    '    If xUACQry.Trim.Length > 0 Then
                    '        StrInnerQ &= xUACQry
                    '    End If
                    'End If


                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrInnerQ &= xAdvanceJoinQry
                    'End If
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
                                        "AND ISNULL(cmexpense_master.isactive, 1) = 1 " & _
                                        "AND ISNULL(cmretire_process_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(cmclaim_retirement_master.isvoid, 0) = 0 " & _
                                        "AND ISNULL(crretireexpense.isactive, 1) = 1 " & _
                                        "AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) " & _
                                        "AND 1 = CASE WHEN prpayrollprocess_tran.crprocesstranunkid > 0 AND cmexpense_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") THEN 1 WHEN prpayrollprocess_tran.crretirementprocessunkid > 0 THEN 1 ELSE 0 END " & _
                                        "AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                    'Sohail (20 Jan 2020) -- Start
                    'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
                    'If mintEmployeeId > 0 Then
                    '    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                    'End If

                    'If mstrAdvance_Filter.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & mstrAdvance_Filter
                    'End If
                    
                    'If blnApplyUserAccessFilter = True Then
                    '    If xUACFiltrQry.Trim.Length > 0 Then
                    '        StrInnerQ &= " AND " & xUACFiltrQry
                    '    End If
                    'End If

                    'If xIncludeIn_ActiveEmployee = False Then
                    '    If xDateFilterQry.Trim.Length > 0 Then
                    '        StrInnerQ &= xDateFilterQry
                    '    End If
                    'End If

                    'If mintBranchId > 0 Then
                    '    StrInnerQ &= " AND T.stationunkid = @BranchId "

                    'End If
                    'Sohail (20 Jan 2020) -- End

                    StrInnerQ &= " GROUP BY payperiodunkid " & _
                                  ", #" & mstrFromDatabaseName & ".employeeunkid " & _
                                  ", cmexpense_master.expenseunkid " & _
                                  ", cmexpense_master.trnheadtype_id " & _
                                  ", crretireexpense.expenseunkid " & _
                                  ", prpayrollprocess_tran.add_deduct " & _
                                  ", openingbalance "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                End If
                'Hemant (31 Aug 2018)) -- End


            Next
            StrQ &= StrInnerQ
            StrQ &= ") AS payroll " & _
                    " JOIN #CurrEmp ON payroll.EmpId = #CurrEmp.employeeunkid " & _
                    "/*LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 gradegroupunkid " & _
                    "		,gradeunkid " & _
                    "		,gradelevelunkid " & _
                    "		,employeeunkid AS GEmpId " & _
                    "	FROM " & _
                    "	( " & _
                    "		SELECT " & _
                    "			 gradegroupunkid " & _
                    "			,gradeunkid " & _
                    "			,gradelevelunkid " & _
                    "			,employeeunkid " & _
                    "			,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                    "		FROM prsalaryincrement_tran " & _
                    "		WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "	) AS GRD WHERE GRD.Rno = 1 " & _
                    ") AS EGRD ON EGRD.GEmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "        Trf.TrfEmpId " & _
                    "       ,ISNULL(Trf.stationunkid,0) AS stationunkid " & _
                    "       ,ISNULL(Trf.deptgroupunkid,0) AS deptgroupunkid " & _
                    "       ,ISNULL(Trf.departmentunkid,0) AS departmentunkid " & _
                    "       ,ISNULL(Trf.sectiongroupunkid,0) AS sectiongroupunkid " & _
                    "       ,ISNULL(Trf.sectionunkid,0) AS sectionunkid " & _
                    "       ,ISNULL(Trf.unitgroupunkid,0) AS unitgroupunkid " & _
                    "       ,ISNULL(Trf.unitunkid,0) AS unitunkid " & _
                    "       ,ISNULL(Trf.teamunkid,0) AS teamunkid " & _
                    "       ,ISNULL(Trf.classgroupunkid,0) AS classgroupunkid " & _
                    "       ,ISNULL(Trf.classunkid,0) AS classunkid " & _
                    "       ,ISNULL(Trf.EfDt,'') AS EfDt " & _
                    "       ,Trf.ETT_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            ETT.employeeunkid AS TrfEmpId " & _
                    "           ,ISNULL(ETT.stationunkid,0) AS stationunkid " & _
                    "           ,ISNULL(ETT.deptgroupunkid,0) AS deptgroupunkid " & _
                    "           ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
                    "           ,ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
                    "           ,ISNULL(ETT.sectionunkid,0) AS sectionunkid " & _
                    "           ,ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
                    "           ,ISNULL(ETT.unitunkid,0) AS unitunkid " & _
                    "           ,ISNULL(ETT.teamunkid,0) AS teamunkid " & _
                    "           ,ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
                    "           ,ISNULL(ETT.classunkid,0) AS classunkid " & _
                    "           ,CONVERT(CHAR(8),ETT.effectivedate,112) AS EfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                    "           ,CASE WHEN ETT.rehiretranunkid > 0 THEN RTT.name ELSE TTC.name END AS ETT_REASON " & _
                    "       FROM hremployee_transfer_tran AS ETT " & _
                    "           LEFT JOIN cfcommon_master AS RTT ON RTT.masterunkid = ETT.changereasonunkid AND RTT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "           LEFT JOIN cfcommon_master AS TTC ON TTC.masterunkid = ETT.changereasonunkid AND TTC.mastertype = '" & clsCommon_Master.enCommonMaster.TRANSFERS & "' " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS Trf WHERE Trf.Rno = 1 " & _
                    ") AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid AND ETRF.EfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "     Cat.CatEmpId " & _
                    "    ,Cat.jobgroupunkid " & _
                    "    ,Cat.jobunkid " & _
                    "    ,Cat.CEfDt " & _
                    "    ,Cat.RECAT_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            ECT.employeeunkid AS CatEmpId " & _
                    "           ,ECT.jobgroupunkid " & _
                    "           ,ECT.jobunkid " & _
                    "           ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                    "           ,CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
                    "       FROM hremployee_categorization_tran AS ECT " & _
                    "           LEFT JOIN cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "           LEFT JOIN cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS Cat WHERE Cat.Rno = 1 " & _
                    ") AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 CCT.CCTEmpId " & _
                    "		,CCT.costcenterunkid " & _
                    "		,CCT.CTEfDt " & _
                    "		,CC_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            CCT.employeeunkid AS CCTEmpId " & _
                    "           ,CCT.cctranheadvalueid AS costcenterunkid " & _
                    "           ,CONVERT(CHAR(8),CCT.effectivedate,112) AS CTEfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN CCT.rehiretranunkid > 0 THEN ISNULL(RCS.name,'') ELSE ISNULL(CTC.name,'') END AS CC_REASON " & _
                    "       FROM hremployee_cctranhead_tran AS CCT " & _
                    "			LEFT JOIN cfcommon_master AS RCS ON RCS.masterunkid = CCT.changereasonunkid AND RCS.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS CTC ON CTC.masterunkid = CCT.changereasonunkid AND CTC.mastertype = '" & clsCommon_Master.enCommonMaster.COST_CENTER & "' " & _
                    "		WHERE CCT.isvoid = 0 AND CCT.istransactionhead = 0 AND CONVERT(CHAR(8),CCT.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   )CCT WHERE CCT.Rno = 1 " & _
                    ")AS ECCT ON ECCT.CCTEmpId = hremployee_master.employeeunkid AND ECCT.CTEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrstation_master AS SM on SM.stationunkid = ETRF.stationunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrdepartment_group_master AS DGM on DGM.deptgroupunkid = ETRF.deptgroupunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrdepartment_master AS DM ON DM.departmentunkid = ETRF.departmentunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrsectiongroup_master AS SECG ON ETRF.sectiongroupunkid = SECG.sectiongroupunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrsection_master AS SEC ON SEC.sectionunkid = ETRF.sectionunkid  " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrunitgroup_master AS UGM ON ETRF.unitgroupunkid = UGM.unitgroupunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrunit_master AS UM on UM.unitunkid = ETRF.unitunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrteam_master AS TM ON ETRF.teamunkid = TM.teamunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrjobgroup_master AS JGM on JGM.jobgroupunkid = ERECAT.jobgroupunkid  " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrjob_master AS JM ON JM.jobunkid = ERECAT.jobunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrclassgroup_master AS CGM ON CGM.classgroupunkid = ETRF.classgroupunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrclasses_master AS CM on CM.classesunkid = ETRF.classunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..prcostcenter_master AS CC on CC.costcenterunkid = ECCT.costcenterunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrgradegroup_master AS GGM on GGM.gradegroupunkid = EGRD.gradegroupunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrgrade_master AS GM on GM.gradeunkid = EGRD.gradeunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrgradelevel_master AS GLM on GLM.gradelevelunkid = EGRD.gradelevelunkid  " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..cfcommon_master as mtype ON mtype.masterunkid = hremployee_master.maritalstatusunkid AND mtype.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..cfcommon_master AS etype ON etype.masterunkid = hremployee_master.employmenttypeunkid AND etype.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "*/ " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = payroll.PeriodId " & _
                    "   AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "
            'Sohail (17 Jun 2020) -- Start
            'NMB Issue # : Performance issue on custom payroll report.
            'StrQ &= mstrAnalysis_Join

            ''Sohail (10 Jul 2019) -- Start
            ''Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            'If mintMembershipUnkId > 0 Then
            '    StrQ &= "LEFT JOIN " & mstrCurrentDatabaseName & "..hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
            '                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
            '                    "AND hremployee_meminfo_tran.membershipunkid = @membershipunkid "
            'End If
            ''Sohail (10 Jul 2019) -- End
            'Sohail (17 Jun 2020) -- End

            'Sohail (24 Feb 2017) -- Start
            'Issue - 64.1 - The multi-part identifier "ADF.teamunkid" could not be found while applying Advance Filter on Payroll Report and Global Void Payment.
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " WHERE " & mstrAdvance_Filter
            'End If
            'Sohail (24 Feb 2017) -- End

            'Sohail (17 Jun 2020) -- Start
            'NMB Issue # : Performance issue on custom payroll report.
            Me.OrderByQuery = Me.OrderByQuery.Replace("hremployee_master", "#CurrEmp")
            'Sohail (17 Jun 2020) -- End

            If mintViewIndex > 0 Then
                If Me.OrderByQuery <> "" Then
                    'Sohail (17 Jun 2020) -- Start
                    'NMB Issue # : Performance issue on custom payroll report.
                    'StrQ &= " ORDER BY " & mstrAnalysis_Fields.Split(",")(2).Replace("AS GName", "") & ", cfcommon_period_tran.end_date, " & Me.OrderByQuery & ", TranId "
                    StrQ &= " ORDER BY #CurrEmp.GName, cfcommon_period_tran.end_date, " & Me.OrderByQuery & ", TranId "
                    'Sohail (17 Jun 2020) -- End
                Else
                    'Pinkal (27-APR-2018) -- Start
                    'Bug - 0002225: Analysis By not working on Aruti selfservice for Payroll reports i.e Custom payroll report template 2.
                    'StrQ &= " ORDER BY " & mstrAnalysis_Fields.Split(",")(2).Replace("AS GNAME", "") & ", cfcommon_period_tran.end_date, hremployee_master.employeecode " & ", TranId "
                    'Sohail (17 Jun 2020) -- Start
                    'NMB Issue # : Performance issue on custom payroll report.
                    'StrQ &= " ORDER BY " & mstrAnalysis_Fields.Split(",")(2).Replace("AS GName", "") & ", cfcommon_period_tran.end_date, hremployee_master.employeecode " & ", TranId "
                    StrQ &= " ORDER BY #CurrEmp.GName, cfcommon_period_tran.end_date, #CurrEmp.employeecode " & ", TranId "
                    'Sohail (17 Jun 2020) -- End
                    'Pinkal (27-APR-2018) -- End
                End If
            Else
                If Me.OrderByQuery <> "" Then
                    StrQ &= "ORDER BY cfcommon_period_tran.end_date, " & Me.OrderByQuery & ", TranId "
                Else
                    'Sohail (17 Jun 2020) -- Start
                    'NMB Issue # : Performance issue on custom payroll report.
                    'StrQ &= "ORDER BY cfcommon_period_tran.end_date, hremployee_master.employeecode " & ", TranId "
                    StrQ &= "ORDER BY cfcommon_period_tran.end_date, #CurrEmp.employeecode " & ", TranId "
                    'Sohail (17 Jun 2020) -- End
                End If
            End If

            'Sohail (20 Jan 2020) -- Start
            'SLMC performance issue # 0004427 : Generating Payroll Report takes Very Long. 
            For Each key In mdicYearDBName
                StrQ &= " DROP TABLE #" & key.Value & " "
            Next
            'Sohail (20 Jan 2020) -- End

            'Sohail (17 Jun 2020) -- Start
            'NMB Issue # : Performance issue on custom payroll report.
            StrQ &= " DROP TABLE #CurrEmp "
            'Sohail (17 Jun 2020) -- End

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If mintBranchId > 0 Then
                objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
            End If

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            If mintMembershipUnkId > 0 Then
                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipUnkId)
            End If
            'Sohail (10 Jul 2019) -- End

            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 38, "Female"))

            dsPayroll = objDataOperation.ExecQuery(StrQ, "payroll")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrInnerQ = ""
            StrQ = "SELECT  TotalPaymentDetails.PeriodId " & _
                          ", TotalPaymentDetails.EmpId " & _
                          ", TotalPaymentDetails.EmpBankTranId " & _
                          ", TotalPaymentDetails.BankName " & _
                          ", TotalPaymentDetails.BranchName " & _
                          ", TotalPaymentDetails.AccountNo " & _
                          ", TotalPaymentDetails.Amount " & _
                          ", TotalPaymentDetails.PaidAmount " & _
                          ", TotalPaymentDetails.basecurrencyid " & _
                          ", TotalPaymentDetails.paidcurrencyid " & _
                          ", TotalPaymentDetails.baseexchangerate " & _
                          ", TotalPaymentDetails.expaidrate " & _
                          ", TotalPaymentDetails.BaseSign " & _
                          ", TotalPaymentDetails.PaidSign " & _
                          ", TotalPaymentDetails.exchange_rate1 " & _
                          ", TotalPaymentDetails.exchange_rate2 " & _
                   "FROM  ( "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'For i = 0 To marrDatabaseName.Count - 1
            '    mstrFromDatabaseName = marrDatabaseName(i)
            i = -1
            For Each key In mdicYearDBName
                mstrFromDatabaseName = key.Value
                i += 1
                'Sohail (21 Aug 2015) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrFromDatabaseName)
                'Sohail (13 Feb 2016) -- Start
                'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
                'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                If mblnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                'Sohail ((13 Feb 2016) -- End
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrFromDatabaseName)
                'Sohail (21 Aug 2015) -- End

                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If

                StrInnerQ &= "SELECT  prempsalary_tran.employeeunkid AS EmpId " & _
                                    ", prpayment_tran.periodunkid AS PeriodId " & _
                                    ", prempsalary_tran.empbanktranid AS EmpBankTranId " & _
                                    ", hrmsConfiguration..cfpayrollgroup_master.groupname AS BankName " & _
                                    ", hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
                                    ", premployee_bank_tran.accountno AS AccountNo " & _
                                    ", prempsalary_tran.amount AS Amount " & _
                                    ", prempsalary_tran.expaidamt AS PaidAmount " & _
                                    ", prpayment_tran.basecurrencyid " & _
                                    ", prpayment_tran.paidcurrencyid " & _
                                    ", prpayment_tran.baseexchangerate " & _
                                    ", prpayment_tran.expaidrate " & _
                                    ", base.currency_sign AS BaseSign " & _
                                    ", paid.currency_sign AS PaidSign " & _
                                    ", paid.exchange_rate1 " & _
                                    ", paid.exchange_rate2 " & _
                             "FROM  " & mstrFromDatabaseName & "..prempsalary_tran " & _
                                    "JOIN " & mstrFromDatabaseName & "..premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid " & _
                                    "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                                    "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                                    "JOIN " & mstrFromDatabaseName & "..prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                    "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                    "JOIN " & mstrFromDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prempsalary_tran.employeeunkid " & _
                                    "JOIN " & mstrFromDatabaseName & "..cfexchange_rate AS base ON base.exchangerateunkid = prpayment_tran.basecurrencyid " & _
                                    "JOIN " & mstrFromDatabaseName & "..cfexchange_rate AS paid ON paid.exchangerateunkid = prpayment_tran.paidcurrencyid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrInnerQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrInnerQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrInnerQ &= xUACQry
                End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrInnerQ &= "WHERE ISNULL(premployee_bank_tran.isvoid, 0) = 0 " & _
                                    "AND ISNULL(prempsalary_tran.isvoid, 0) = 0 " & _
                                    "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                    "AND hrmsConfiguration..cfbankbranch_master.isactive = 1 " & _
                                    "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1 " & _
                                    "AND hrmsConfiguration..cfpayrollgroup_master.grouptype_id = 3 " & _
                                    "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                    "AND prpayment_tran.periodunkid IN (" & mstrPeriodIdList & ") "
                'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

                If mintEmployeeId > 0 Then
                    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrInnerQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrInnerQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrInnerQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                    StrInnerQ &= " AND T.stationunkid = @BranchId "
                    'Sohail (21 Aug 2015) -- End
                End If

                StrInnerQ &= "UNION ALL " & _
                             "SELECT  prpayment_tran.employeeunkid AS EmpId " & _
                                    ", prpayment_tran.periodunkid AS PeriodId " & _
                                    ", -1 AS EmpBankTranId " & _
                                    ", @CashPayment AS BankName " & _
                                    ", '' AS BranchName " & _
                                    ", '' AS AccountNo " & _
                                    ", prpayment_tran.amount AS Amount " & _
                                    ", prpayment_tran.expaidamt AS PaidAmount " & _
                                    ", prpayment_tran.basecurrencyid " & _
                                    ", prpayment_tran.paidcurrencyid " & _
                                    ", prpayment_tran.baseexchangerate " & _
                                    ", prpayment_tran.expaidrate " & _
                                    ", base.currency_sign AS BaseSign " & _
                                    ", paid.currency_sign AS PaidSign " & _
                                    ", paid.exchange_rate1 " & _
                                    ", paid.exchange_rate2 " & _
                             "FROM  " & mstrFromDatabaseName & "..prpayment_tran " & _
                                    "JOIN " & mstrFromDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prpayment_tran.employeeunkid " & _
                                    "JOIN " & mstrFromDatabaseName & "..cfexchange_rate AS base ON base.exchangerateunkid = prpayment_tran.basecurrencyid " & _
                                    "JOIN " & mstrFromDatabaseName & "..cfexchange_rate AS paid ON paid.exchangerateunkid = prpayment_tran.paidcurrencyid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrInnerQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrInnerQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrInnerQ &= xUACQry
                End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrInnerQ &= "WHERE ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                    "AND prpayment_tran.referenceid = 3 " & _
                                    "AND prpayment_tran.paymentmode = 1 " & _
                                    "AND prpayment_tran.periodunkid IN (" & mstrPeriodIdList & ") "

                If mintEmployeeId > 0 Then
                    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrInnerQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrInnerQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrInnerQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                    StrInnerQ &= " AND T.stationunkid = @BranchId "
                    'Sohail (21 Aug 2015) -- End
                End If

                StrInnerQ &= "UNION ALL " & _
                             "SELECT  prtnaleave_tran.employeeunkid AS EmpId " & _
                                    ", prtnaleave_tran.payperiodunkid AS PeriodId " & _
                                    ", -2 AS EmpBankTranId " & _
                                    ", @SalaryOnHold AS BankName " & _
                                    ", '' AS BranchName " & _
                                    ", '' AS AccountNo " & _
                                    ", prtnaleave_tran.balanceamount AS Amount " & _
                                    ", prtnaleave_tran.balanceamount AS PaidAmount " & _
                                    ", 0 AS basecurrencyid " & _
                                    ", 0 AS paidcurrencyid " & _
                                    ", 0 AS baseexchangerate " & _
                                    ", 0 AS expaidrate " & _
                                    ", '' AS BaseSign " & _
                                    ", '' AS PaidSign " & _
                                    ", 0 AS exchange_rate1 " & _
                                    ", 0 AS exchange_rate2 " & _
                             "FROM  " & mstrFromDatabaseName & "..prtnaleave_tran " & _
                                    "JOIN " & mstrFromDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prtnaleave_tran.employeeunkid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrInnerQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrInnerQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrInnerQ &= xUACQry
                End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrInnerQ &= "WHERE prtnaleave_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIdList & ") " & _
                                    "AND prtnaleave_tran.balanceamount <> 0 "

                If mintEmployeeId > 0 Then
                    StrInnerQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrInnerQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrInnerQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrInnerQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrInnerQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrInnerQ &= " AND hremployee_master.stationunkid = @BranchId "
                    StrInnerQ &= " AND T.stationunkid = @BranchId "
                    'Sohail (21 Aug 2015) -- End
                End If

            Next

            StrQ &= StrInnerQ

            StrQ &= ") AS TotalPaymentDetails " & _
                    "GROUP BY  TotalPaymentDetails.PeriodId " & _
                        ",TotalPaymentDetails.EmpId " & _
                        ",TotalPaymentDetails.EmpBankTranId " & _
                        ",TotalPaymentDetails.BankName " & _
                        ",TotalPaymentDetails.BranchName " & _
                        ",TotalPaymentDetails.AccountNo " & _
                        ",TotalPaymentDetails.Amount " & _
                        ", TotalPaymentDetails.PaidAmount " & _
                        ",TotalPaymentDetails.basecurrencyid " & _
                  ", TotalPaymentDetails.paidcurrencyid " & _
                  ", TotalPaymentDetails.baseexchangerate " & _
                  ", TotalPaymentDetails.expaidrate " & _
                  ", TotalPaymentDetails.BaseSign " & _
                  ", TotalPaymentDetails.PaidSign " & _
                  ", TotalPaymentDetails.exchange_rate1 " & _
                  ", TotalPaymentDetails.exchange_rate2 "

            objDataOperation.AddParameter("@CashPayment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Cash Payment"))
            objDataOperation.AddParameter("@SalaryOnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "Salary On Hold"))

            dsPaymetDetail = objDataOperation.ExecQuery(StrQ, "Payment")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim strCurrKey As String = String.Empty
            For Each dtBank As DataRow In dsPaymetDetail.Tables("Payment").Rows
                strCurrKey = dtBank.Item("PeriodId") & "_" & dtBank.Item("EmpId")
                If mdicEmpBank.ContainsKey(strCurrKey) Then
                    If dtBank("EmpBankTranId") > 0 Then
                        mdicEmpBank(strCurrKey) &= "#10;" & Language.getMessage(mstrModuleName, 22, "Bank :") & dtBank.Item("BankName") & " - " & Language.getMessage(mstrModuleName, 23, "Branch :") & dtBank.Item("BranchName")
                        'Sohail (09 Dec 2014) -- Start
                        'Error - Given key was not found in dictionary when employee is paid rounding amount. (bank details + salary on hold -- and first entry is salary on hold no accountno detail in dictionary).
                        'mdicEmpAccount(strCurrKey) &= "#10;" & Language.getMessage(mstrModuleName, 24, "Account # :") & dtBank.Item("AccountNo")
                        If mdicEmpAccount.ContainsKey(strCurrKey) = True Then
                            mdicEmpAccount(strCurrKey) &= "#10;" & Language.getMessage(mstrModuleName, 24, "Account # :") & dtBank.Item("AccountNo")
                        Else
                            mdicEmpAccount.Add(strCurrKey, "#10;" & Language.getMessage(mstrModuleName, 24, "Account # :") & dtBank.Item("AccountNo"))
                        End If
                        'Sohail (09 Dec 2014) -- End
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                        mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), mstrfmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                        'Sohail (21 Aug 2015) -- End
                    Else
                        mdicEmpBank(strCurrKey) &= "#10;" & dtBank.Item("BankName")
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                        mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), mstrfmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                        'Sohail (21 Aug 2015) -- End
                    End If
                Else
                    If dtBank("EmpBankTranId") > 0 Then
                        mdicEmpBank.Add(strCurrKey, Language.getMessage(mstrModuleName, 22, "Bank :") & dtBank.Item("BankName") & " - " & Language.getMessage(mstrModuleName, 23, "Branch :") & dtBank.Item("BranchName") & ",")
                        mdicEmpAccount.Add(strCurrKey, Language.getMessage(mstrModuleName, 24, "Account # :") & dtBank.Item("AccountNo") & ",")
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                        mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), mstrfmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                        'Sohail (21 Aug 2015) -- End
                    Else
                        If CInt(dtBank.Item("EmpBankTranId")) = -2 AndAlso CDec(dtBank.Item("amount")) < 0 AndAlso dsPaymetDetail.Tables("Payment").Select("PeriodId = " & dtBank.Item("PeriodId") & " AND EmpId = " & dtBank.Item("EmpId") & " ").Length > 1 Then
                            Continue For 'Over Payment; Show Payment detail only
                        End If
                        If CInt(dtBank.Item("EmpBankTranId")) = -2 AndAlso dsPaymetDetail.Tables("Payment").Select("PeriodId = " & dtBank.Item("PeriodId") & " AND EmpId = " & dtBank.Item("EmpId") & " ").Length > 1 Then
                            mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName"))
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), GUI.fmtCurrency) & "),")
                            mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), mstrfmtCurrency) & "),")
                            'Sohail (21 Aug 2015) -- End
                        Else
                            If CInt(dtBank.Item("EmpBankTranId")) = -2 Then 'Salary On Hold
                                mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName"))
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), GUI.fmtCurrency) & "),")
                                mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), mstrfmtCurrency) & "),")
                                'Sohail (21 Aug 2015) -- End
                            Else
                                mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName"))
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                                mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), mstrfmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                                'Sohail (21 Aug 2015) -- End
                            End If
                        End If
                    End If
                End If
            Next

            Dim drRow As DataRow : Dim rpt_Row As DataRow = Nothing : Dim strKey As String = "" : Dim strPrevKey As String = ""
            Dim intRowCount As Integer = dsPayroll.Tables("payroll").Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsPayroll.Tables("payroll").Rows(ii)
                strKey = drRow.Item("PeriodId").ToString & "_" & drRow.Item("EmpId")
                If strPrevKey <> strKey Then
                    rpt_Row = dtFinalTable.NewRow
                    rpt_Row.Item("PeriodId") = drRow.Item("PeriodId")
                    rpt_Row.Item("Period") = drRow.Item("PeriodName")

                    rpt_Row.Item("EmpCode") = drRow.Item("Code")
                    'Sohail (10 Jul 2019) -- Start
                    'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
                    'rpt_Row.Item("EmpName") = drRow.Item("EmpName")
                    If mblnShowEmpNameInSeprateColumns = False Then
                    rpt_Row.Item("EmpName") = drRow.Item("EmpName")
                    Else
                        rpt_Row.Item("FirstName") = drRow.Item("FirstName")
                        rpt_Row.Item("MiddleName") = drRow.Item("OtherNAme")
                        rpt_Row.Item("Surname") = drRow.Item("Surname")
                    End If
                    If mintMembershipUnkId > 0 Then
                        rpt_Row.Item("MembershipName") = drRow.Item("MembershipNo")
                    End If
                    'Sohail (10 Jul 2019) -- End

                    If dtFinalTable.Columns.Contains("Col_1") Then
                        rpt_Row.Item("Col_1") = drRow.Item("Col_1")
                    End If

                    If dtFinalTable.Columns.Contains("Col_2") Then
                        rpt_Row.Item("Col_2") = drRow.Item("Col_2")
                    End If

                    If dtFinalTable.Columns.Contains("Col_3") Then
                        rpt_Row.Item("Col_3") = drRow.Item("Col_3")
                    End If

                    If dtFinalTable.Columns.Contains("Col_4") Then
                        rpt_Row.Item("Col_4") = drRow.Item("Col_4")
                    End If

                    If dtFinalTable.Columns.Contains("Col_5") Then
                        rpt_Row.Item("Col_5") = drRow.Item("Col_5")
                    End If

                    If dtFinalTable.Columns.Contains("Col_6") Then
                        rpt_Row.Item("Col_6") = drRow.Item("Col_6")
                    End If

                    If dtFinalTable.Columns.Contains("Col_7") Then
                        rpt_Row.Item("Col_7") = drRow.Item("Col_7")
                    End If

                    If dtFinalTable.Columns.Contains("Col_8") Then
                        rpt_Row.Item("Col_8") = drRow.Item("Col_8")
                    End If

                    If dtFinalTable.Columns.Contains("Col_9") Then
                        rpt_Row.Item("Col_9") = drRow.Item("Col_9")
                    End If

                    If dtFinalTable.Columns.Contains("Col_10") Then
                        rpt_Row.Item("Col_10") = drRow.Item("Col_10")
                    End If

                    If dtFinalTable.Columns.Contains("Col_11") Then
                        rpt_Row.Item("Col_11") = drRow.Item("Col_11")
                    End If

                    If dtFinalTable.Columns.Contains("Col_12") Then
                        rpt_Row.Item("Col_12") = drRow.Item("Col_12")
                    End If

                    If dtFinalTable.Columns.Contains("Col_13") Then
                        If drRow.Item("Col_13").ToString.Trim.Length > 0 Then
                            rpt_Row.Item("Col_13") = eZeeDate.convertDate(drRow.Item("Col_13").ToString).ToShortDateString
                        End If
                    End If

                    If dtFinalTable.Columns.Contains("Col_14") Then
                        If drRow.Item("Col_14").ToString.Trim.Length > 0 Then
                            rpt_Row.Item("Col_14") = eZeeDate.convertDate(drRow.Item("Col_14").ToString).ToShortDateString
                        End If
                    End If

                    If dtFinalTable.Columns.Contains("Col_15") Then
                        If drRow.Item("Col_15").ToString.Trim.Length > 0 Then
                            rpt_Row.Item("Col_15") = eZeeDate.convertDate(drRow.Item("Col_15").ToString).ToShortDateString
                        End If
                    End If

                    If dtFinalTable.Columns.Contains("Col_16") Then
                        If drRow.Item("Col_16").ToString.Trim.Length > 0 Then
                            rpt_Row.Item("Col_16") = eZeeDate.convertDate(drRow.Item("Col_16").ToString).ToShortDateString
                        End If
                    End If

                    If dtFinalTable.Columns.Contains("Col_17") Then
                        If drRow.Item("Col_17").ToString.Trim.Length > 0 Then
                            rpt_Row.Item("Col_17") = eZeeDate.convertDate(drRow.Item("Col_17").ToString).ToShortDateString
                        End If
                    End If

                    If dtFinalTable.Columns.Contains("Col_18") Then
                        rpt_Row.Item("Col_18") = drRow.Item("Col_18")
                    End If

                    If dtFinalTable.Columns.Contains("Col_19") Then
                        rpt_Row.Item("Col_19") = drRow.Item("Col_19")
                    End If

                    If dtFinalTable.Columns.Contains("Col_20") Then
                        rpt_Row.Item("Col_20") = drRow.Item("Col_20")
                    End If

                    If dtFinalTable.Columns.Contains("Col_21") Then
                        rpt_Row.Item("Col_21") = drRow.Item("Col_21")
                    End If

                    If dtFinalTable.Columns.Contains("Col_22") Then
                        rpt_Row.Item("Col_22") = drRow.Item("Col_22")
                    End If

                    If dtFinalTable.Columns.Contains("Col_23") Then
                        rpt_Row.Item("Col_23") = drRow.Item("Col_23")
                    End If

                    If dtFinalTable.Columns.Contains("Col_24") Then
                        rpt_Row.Item("Col_24") = drRow.Item("Col_24")
                    End If

                    'Sohail (03 Jan 2020) -- Start
                    'NMB Enhancement # : Option for Cost centre code column on custom payroll report.
                    If dtFinalTable.Columns.Contains("Col_25") Then
                        rpt_Row.Item("Col_25") = drRow.Item("Col_25")
                    End If

                    If dtFinalTable.Columns.Contains("Col_26") Then
                        rpt_Row.Item("Col_26") = drRow.Item("Col_26")
                    End If
                    'Sohail (03 Jan 2020) -- End

                    If mdicEmpBank.ContainsKey(strKey) = True Then
                        If mdicEmpBank.Item(strKey).Contains("#10;") = False Then
                            rpt_Row.Item("Bank") = mdicEmpBank.Item(strKey).Replace(",", "")
                        Else
                            rpt_Row.Item("Bank") = mdicEmpBank.Item(strKey)
                        End If
                    Else
                        rpt_Row.Item("Bank") = ""
                    End If

                    If mdicEmpAccount.ContainsKey(strKey) = True Then
                        If mdicEmpAccount.Item(strKey).Contains("#10;") = False Then
                            rpt_Row.Item("Account") = mdicEmpAccount.Item(strKey).Replace(",", "")
                        Else
                            rpt_Row.Item("Account") = mdicEmpAccount.Item(strKey)
                        End If
                    Else
                        rpt_Row.Item("Account") = ""
                    End If

                    If mdicEmpPayment.ContainsKey(strKey) = True Then
                        If mdicEmpPayment.Item(strKey).Contains("#10;") = False Then
                            rpt_Row.Item("PaymentDetail") = mdicEmpPayment.Item(strKey).Replace(",", "")
                        Else
                            rpt_Row.Item("PaymentDetail") = mdicEmpPayment.Item(strKey)
                        End If
                    Else
                        rpt_Row.Item("PaymentDetail") = ""
                    End If

                    rpt_Row.Item("GrpID") = drRow.Item("Id")
                    rpt_Row.Item("GrpName") = drRow.Item("GName")
                Else
                    rpt_Row = dtFinalTable.Rows(dtFinalTable.Rows.Count - 1)
                End If
                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                'rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                If drRow.Item("Mid").ToString = "1" Then   'LOAN
                    If mblnShowLoansInSeparateColumns = True Then
                        'Sohail (15 Jun 2021) -- Start
                        'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                        rpt_Row.Item("ColumnLoan" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'rpt_Row.Item("ColumnLoan" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                        'Sohail (15 Jun 2021) -- End
                    Else
                        'Sohail (15 Jun 2021) -- Start
                        'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                        rpt_Row.Item("Loan") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'rpt_Row.Item("Loan") = CDec(drRow.Item("Amount"))
                        'Sohail (15 Jun 2021) -- End
                    End If
                ElseIf drRow.Item("Mid").ToString = "2" Then 'ADVANCE
                    'Sohail (18 Apr 2016) -- Start
                    'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                    'rpt_Row.Item("Advance") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    'Sohail (15 Jun 2021) -- Start
                    'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                    rpt_Row.Item("ColumnAdvance1") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    'rpt_Row.Item("ColumnAdvance1") = CDec(drRow.Item("Amount"))
                    'Sohail (15 Jun 2021) -- End
                    'Sohail (18 Apr 2016) -- End
                ElseIf drRow.Item("Mid").ToString = "3" Then 'SAVINGS
                    If mblnShowSavingsInSeparateColumns = True Then
                        'Sohail (15 Jun 2021) -- Start
                        'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                        rpt_Row.Item("ColumnSavings" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'rpt_Row.Item("ColumnSavings" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                        'Sohail (15 Jun 2021) -- End
                    Else
                        'Sohail (15 Jun 2021) -- Start
                        'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                        rpt_Row.Item("Savings") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'rpt_Row.Item("Savings") = CDec(drRow.Item("Amount"))
                        'Sohail (15 Jun 2021) -- End
                    End If
                ElseIf drRow.Item("Mid").ToString = "12" Then 'Activity Deduction
                    'Sohail (15 Jun 2021) -- Start
                    'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                    rpt_Row.Item("ColumnPPA" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    'rpt_Row.Item("ColumnPPA" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                    'Sohail (15 Jun 2021) -- End
                    'Hemant (31 Aug 2018) -- Start
                    'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
                    'ElseIf drRow.Item("Mid").ToString = "15" Then 'CR Expense Deduction
                ElseIf drRow.Item("Mid").ToString = "15" Or drRow.Item("Mid").ToString = "14" Then 'CR Expense Deduction
                    'rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    If mblnShowCRInSeparateColumns = True Then
                        'Sohail (15 Jun 2021) -- Start
                        'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                        rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                        'Sohail (15 Jun 2021) -- End
                    Else
                        'Sohail (15 Jun 2021) -- Start
                        'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                        rpt_Row.Item("CR") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'rpt_Row.Item("CR") = CDec(drRow.Item("Amount"))
                        'Sohail (15 Jun 2021) -- End
                    End If
                    'Hemant (31 Aug 2018)) -- End
                Else
                    'Sohail (15 Jun 2021) -- Start
                    'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                    rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    'rpt_Row.Item("Column" & drRow.Item("TranId")) = CDec(drRow.Item("Amount"))
                    'Sohail (15 Jun 2021) -- End
                End If
                'Sohail (03 Feb 2016) -- End
                If strPrevKey <> strKey Then
                    dtFinalTable.Rows.Add(rpt_Row)
                End If
                strPrevKey = strKey
            Next

            dtFinalTable.AcceptChanges()

            Dim dblTotal As Decimal = 0
            Dim m As Integer = xTotalStartCol
            Dim n As Integer = xTotalStartCol
            dblColTot = New Decimal(dtFinalTable.Columns.Count) {}
            Dim count As Integer = 0

            While m < dtFinalTable.Columns.Count - 3
                For t As Integer = 0 To dtFinalTable.Rows.Count - 1
                    dblTotal += CDec(dtFinalTable.Rows(t)(m))
                Next
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dblColTot(n) = Format(CDec(dblTotal), GUI.fmtCurrency)
                'Sohail (15 Jun 2021) -- Start
                'MUWAS Issue : : Net Pay total not matching wih payroll report and jv report.
                dblColTot(n) = Format(CDec(dblTotal), mstrfmtCurrency)
                'dblColTot(n) = CDec(dblTotal)
                'Sohail (15 Jun 2021) -- End
                'Sohail (21 Aug 2015) -- End
                dblTotal = 0
                m += 1
                n += 1
            End While

            Call FilterTitleAndFilterQuery()

            Dim strGTotal As String = Language.getMessage(mstrModuleName, 4, "Grand Total :")
            Dim strSubTotal As String = ""
            Dim objDic As New Dictionary(Of Integer, Object)
            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim wcell As WorksheetCell = Nothing
            Dim strBuilder As New StringBuilder
            'Nilay (12-Oct-2016) -- Start
            'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
            'Dim row As WorksheetRow
            'Nilay (12-Oct-2016) -- End

            If mblnIgnorezeroHeads Then
                mdtTableExcel = IgnoreZeroHead(dtFinalTable)
            Else
                mdtTableExcel = dtFinalTable
            End If

            'Nilay (12-Oct-2016) -- Start
            'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
           
            'Nilay (12-Oct-2016) -- End

            mdtTableExcel.Columns.RemoveAt(0)
            If mintViewIndex <= 0 Then
                mdtTableExcel.Columns.Remove("GrpID")
                mdtTableExcel.Columns.Remove("GrpName")
            Else
                mdtTableExcel.Columns.Remove("GrpID")
                mdtTableExcel.Columns("GrpName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GrpName"}
                strarrGroupColumns = strGrpCols
                strSubTotal = Language.getMessage(mstrModuleName, 3, "Sub Total :")
            End If
            If mintReportId = 1 Then
                mdtTableExcel.Columns.Remove("EmpCode")
                'Sohail (10 Jul 2019) -- Start
                'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
                'mdtTableExcel.Columns.Remove("EmpName")
                If mblnShowEmpNameInSeprateColumns = False Then
                mdtTableExcel.Columns.Remove("EmpName")
                Else
                    mdtTableExcel.Columns.Remove("FirstName")
                    mdtTableExcel.Columns.Remove("MiddleName")
                    mdtTableExcel.Columns.Remove("Surname")
                End If
                If mdtTableExcel.Columns.Contains("MembershipName") = True Then
                    mdtTableExcel.Columns.Remove("MembershipName")
                End If
                'Sohail (10 Jul 2019) -- End
            ElseIf mintReportId = 2 Then
                mdtTableExcel.Columns.Remove("Period")
            End If

            If mblnShowPaymentDetails = False Then
                mdtTableExcel.Columns.Remove("Bank")
                mdtTableExcel.Columns.Remove("Account")
                mdtTableExcel.Columns.Remove("PaymentDetail")
            End If

            Dim intColCount As Integer = mdtTableExcel.Columns.Count
            If strarrGroupColumns IsNot Nothing AndAlso strarrGroupColumns.Length > 0 Then
                intColCount -= strarrGroupColumns.Length
            End If

            If mintEmployeeId > 0 Then
                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan=" & intColCount & " style='font-size:13px;border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & "</B></TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)
            End If

            strBuilder.Append("<TR>" & vbCrLf)
            strBuilder.Append("<TD colspan=2 style='font-size:13px;border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 2, "Period :") & mstrPeriodName & "</B></TD>" & vbCrLf)
            If mstrCurrency_Sign.Trim <> "" AndAlso mstrCurrency_Rate.Trim <> "" Then
                strBuilder.Append("<TD colspan=2 style='font-size:13px;border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 17, "Currency Rate :") & " " & mstrCurrency_Rate & "</B></TD>" & vbCrLf)
            End If
            strBuilder.Append("</TR>" & vbCrLf)
            If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
            strBuilder.Remove(0, strBuilder.Length)

            If mblnSetPayslipPaymentApproval = True Then
                Dim objTnALeave As New clsTnALeaveTran
                Dim dtTable As DataTable : Dim dsList As New DataSet
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objTnALeave.Get_Balance_List("Balance", , , mintPeriodId, "")
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(mstrFromDatabaseName) = mintPeriodId
                dsList = objTnALeave.Get_Balance_List(mstrFromDatabaseName, xUserUnkid, objPeriod._Yearunkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, "", "Balance", , mintPeriodId, "")
                'Sohail (21 Aug 2015) -- End
                dtTable = New DataView(dsList.Tables("Balance"), "balanceamount > 0", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count = 0 Then 'If payment is done for all employees
                    Dim objPayment As New clsPayment_tran
                    Dim objLevel As New clsPaymentApproverlevel_master
                    Dim objPaymentApproval As New clsPayment_approval_tran
                    Dim objPayAuthorize As New clsPayment_authorize_tran
                    Dim dsLevel As DataSet
                    Dim dsAuthorizeUser As DataSet
                    Dim intTotalPaymentCount As Integer
                    Dim intTotalApprovedtCount As Integer
                    Dim intTotalAuthorizedtCount As Integer
                    Dim intMaxRowCount As Integer
                    Dim intCount As Integer = 0
                    'Sohail (24 Jun 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
                    'intTotalPaymentCount = objPayment.GetTotalPaymentCount(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                    intTotalPaymentCount = objPayment.GetTotalPaymentCount(mstrFromDatabaseName, xUserUnkid, objPeriod._Yearunkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                    'Sohail (24 Jun 2019) -- End

                    strBuilder.Append("<TR>" & vbCrLf)
                    strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                    strBuilder.Append("</TR>" & vbCrLf)

                    strBuilder.Append("<TR>" & vbCrLf)

                    strBuilder.Append("<TD colspan=2 style='border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 13, "Prepared By") & "</B></TD>" & vbCrLf)
                    strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                    strBuilder.Append("<TD colspan=2 style='border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 14, "Approved By") & "</B></TD>" & vbCrLf)
                    strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                    strBuilder.Append("<TD colspan=2 style='border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 15, "Authorized By") & "</B></TD>" & vbCrLf)

                    strBuilder.Append("</TR>" & vbCrLf)


                    intMaxRowCount = objLevel.GetLevelListCount()
                    dsLevel = objLevel.GetList("Level", True, True)
                    intCount = objPayAuthorize.GetDISTINCTauthorizedUsersCount(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                    If intCount > intMaxRowCount Then
                        intMaxRowCount = intCount
                    End If
                    dsAuthorizeUser = objPayAuthorize.GetDISTINCTauthorizedUsers("Users", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)

                    For i = 0 To intMaxRowCount - 1
                        strBuilder.Append("<TR>" & vbCrLf)
                        '*** Prepared By
                        If i = 0 Then
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'dsList = objPayment.GetDistinctPaymentUsers("LastPayment", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                            objPeriod = New clscommom_period_Tran
                            objPeriod._Periodunkid(mstrFromDatabaseName) = mintPeriodId
                            dsList = objPayment.GetDistinctPaymentUsers(mstrFromDatabaseName, xUserUnkid, objPeriod._Yearunkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "LastPayment", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId, True)
                            'Sohail (21 Aug 2015) -- End
                            If dsList.Tables("LastPayment").Rows.Count > 0 Then
                                Dim strDesc As String = ""
                                For Each dsRow As DataRow In dsList.Tables("LastPayment").Rows
                                    If strDesc.Trim = "" Then
                                        strDesc = dsRow("userfullname").ToString & ", " & CDate(dsRow("paymentdate"))
                                    Else
                                        strDesc &= "; " & dsRow("userfullname").ToString & ", " & CDate(dsRow("paymentdate"))
                                    End If
                                Next
                                strBuilder.Append("<TD style='border-width: 0px;' colspan=2> " & strDesc & " </TD>" & vbCrLf)
                            Else
                                strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                            End If
                        Else
                            strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                        End If
                        strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)

                        '*** Approved By
                        If i <= dsLevel.Tables("Level").Rows.Count - 1 Then
                            intTotalApprovedtCount = objPaymentApproval.GetTotalApprovedCount(mintPeriodId, CInt(dsLevel.Tables("Level").Rows(i)("levelunkid")))
                            If intTotalApprovedtCount = intTotalPaymentCount Then
                                dsList = objPaymentApproval.GetLastApprovedDetail("List", mintPeriodId, CInt(dsLevel.Tables("Level").Rows(i)("levelunkid")))
                                If dsList.Tables("List").Rows.Count > 0 Then
                                    Dim strApproverDetail As String = ""
                                    For Each dsRow As DataRow In dsList.Tables("List").Rows
                                        If strApproverDetail.Trim = "" Then
                                            strApproverDetail = dsRow.Item("levelname").ToString & " : " & dsRow.Item("approverfullname").ToString & "," & CDate(dsRow.Item("approval_date"))
                                        Else
                                            strApproverDetail &= "; " & dsRow.Item("approverfullname").ToString & "," & CDate(dsRow.Item("approval_date"))
                                        End If
                                    Next
                                    strBuilder.Append("<TD style='border-width: 0px;' colspan=2>" & strApproverDetail & "</TD>" & vbCrLf)
                                Else
                                    strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                                End If
                            Else
                                strBuilder.Append("<TD style='border-width: 0px;' colspan=2>" & dsLevel.Tables("Level").Rows(i)("levelname").ToString & " : " & Language.getMessage(mstrModuleName, 16, "Pending") & "</TD>" & vbCrLf)
                            End If
                        Else
                            strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                        End If
                        strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)


                        '*** Authorized By
                        If i <= dsAuthorizeUser.Tables("Users").Rows.Count - 1 Then
                            'Sohail (24 Jun 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
                            'intTotalAuthorizedtCount = objPayAuthorize.GetAuthorizedTotalCount(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                            intTotalAuthorizedtCount = objPayAuthorize.GetAuthorizedTotalCount(mstrFromDatabaseName, xUserUnkid, objPeriod._Yearunkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                            'Sohail (24 Jun 2019) -- End

                            If intTotalAuthorizedtCount = intTotalPaymentCount Then
                                dsList = objPayAuthorize.GetLastAuthorizationDetail("List", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId, CInt(dsAuthorizeUser.Tables("Users").Rows(i).Item("userunkid")))
                                If dsList.Tables("List").Rows.Count > 0 Then
                                    Dim strAuthorizeDetail As String = ""
                                    For Each dsRow As DataRow In dsList.Tables("List").Rows
                                        If strAuthorizeDetail.Trim = "" Then
                                            strAuthorizeDetail = dsRow.Item("userfullname").ToString & "," & CDate(dsRow.Item("authorize_date"))
                                        Else
                                            strAuthorizeDetail &= "; " & dsRow.Item("userfullname").ToString & "," & CDate(dsRow.Item("authorize_date"))
                                        End If
                                    Next
                                    strBuilder.Append("<TD style='border-width: 0px;' colspan=2>" & strAuthorizeDetail & "</TD>" & vbCrLf)
                                Else
                                    strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                                End If
                            Else
                                strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                            End If
                        Else
                            strBuilder.Append("<TD style='border-width: 0px;' colspan=2> &nbsp; </TD>" & vbCrLf)
                        End If
                        strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                        strBuilder.Append("</TR>" & vbCrLf)
                    Next
                End If
            End If

            If strBuilder.Length > 0 Then rowsArrayFooter.Add(strBuilder.ToString)
            strBuilder.Remove(0, strBuilder.Length)

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For ii As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(ii) = 125
            Next

            If mblnShowPaymentDetails = True Then
                intArrayColumnWidth(mdtTableExcel.Columns.Count - 1) = 200
                intArrayColumnWidth(mdtTableExcel.Columns.Count - 2) = 200
                intArrayColumnWidth(mdtTableExcel.Columns.Count - 3) = 200
            End If

            'Nilay (12-Oct-2016) -- Start
            'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
            If mintReportId = 4 Then

            End If
            'Nilay (12-Oct-2016) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportExecute(Company._Object._Companyunkid, CInt(mstrReportId), Nothing, enPrintAction.None, enExportAction.ExcelHTML, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, mstrReportTypeName & "(" & mstrCurrency_Sign & ")", "", " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, Nothing)

            'Nilay (12-Oct-2016) -- Start
            'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
            'Call ReportExecute(xCompanyUnkid, CInt(mstrReportId), Nothing, enPrintAction.None, enExportAction.ExcelHTML, mstrExportReportPath, _
            '                   mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, _
            '                   mstrReportTypeName & "(" & mstrCurrency_Sign & ")", "", " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, _
            '                   Nothing)
            Call ReportExecute(xCompanyUnkid, CInt(mstrReportId), Nothing, enPrintAction.None, enExportAction.ExcelHTML, mstrExportReportPath, _
                               mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, _
                               mstrReportTypeName & IIf(mstrCurrency_Rate.Trim <> "", "(" & mstrCurrency_Sign & ")", ""), "", " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, _
                               Nothing)
            'Nilay (12-Oct-2016) -- End

            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_CustomPayrollHeadsReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 29 SEP 2014 ] -- END

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee :")
			Language.setMessage(mstrModuleName, 2, "Period :")
			Language.setMessage(mstrModuleName, 3, "Sub Total :")
			Language.setMessage(mstrModuleName, 4, "Grand Total :")
			Language.setMessage(mstrModuleName, 5, "Prepared By :")
			Language.setMessage(mstrModuleName, 6, "Date :")
			Language.setMessage(mstrModuleName, 9, "Branch :")
			Language.setMessage(mstrModuleName, 10, " To")
			Language.setMessage(mstrModuleName, 11, "Currency :")
			Language.setMessage(mstrModuleName, 12, "Exchange Rate:")
			Language.setMessage(mstrModuleName, 13, "Prepared By")
			Language.setMessage(mstrModuleName, 14, "Approved By")
			Language.setMessage(mstrModuleName, 15, "Authorized By")
			Language.setMessage(mstrModuleName, 16, "Pending")
			Language.setMessage(mstrModuleName, 17, "Currency Rate :")
			Language.setMessage(mstrModuleName, 18, "Employee Code")
			Language.setMessage(mstrModuleName, 19, "Employee Name")
			Language.setMessage(mstrModuleName, 20, "Cash Payment")
			Language.setMessage(mstrModuleName, 21, "Salary On Hold")
			Language.setMessage(mstrModuleName, 22, "Bank :")
			Language.setMessage(mstrModuleName, 23, "Branch :")
			Language.setMessage(mstrModuleName, 24, "Account # :")
			Language.setMessage(mstrModuleName, 25, "Appointment Date")
			Language.setMessage(mstrModuleName, 26, "Birthdate")
			Language.setMessage(mstrModuleName, 27, "Confirmation Date")
			Language.setMessage(mstrModuleName, 28, "Retirement Date")
			Language.setMessage(mstrModuleName, 29, "End Of Contract Date")
			Language.setMessage(mstrModuleName, 30, "CostCenter")
			Language.setMessage(mstrModuleName, 31, "Grade Group")
			Language.setMessage(mstrModuleName, 32, "Grades")
			Language.setMessage(mstrModuleName, 33, "Grade Level")
			Language.setMessage(mstrModuleName, 34, "Employment Type")
			Language.setMessage(mstrModuleName, 35, "Gender")
			Language.setMessage(mstrModuleName, 36, "Marital Status")
			Language.setMessage(mstrModuleName, 37, "Male")
			Language.setMessage(mstrModuleName, 38, "Female")
			Language.setMessage(mstrModuleName, 39, "Period")
			Language.setMessage(mstrModuleName, 40, "Code")
			Language.setMessage(mstrModuleName, 41, "Employee")
			Language.setMessage(mstrModuleName, 42, "Bank Details")
			Language.setMessage(mstrModuleName, 43, "Account#")
			Language.setMessage(mstrModuleName, 44, "Payment Detail")
			Language.setMessage(mstrModuleName, 45, "Gross Pay")
			Language.setMessage(mstrModuleName, 46, "Loan")
			Language.setMessage(mstrModuleName, 47, "Advance")
			Language.setMessage(mstrModuleName, 48, "Savings")
			Language.setMessage(mstrModuleName, 49, "Total Deduction")
			Language.setMessage(mstrModuleName, 50, "Net Pay")
			Language.setMessage(mstrModuleName, 51, "Net B/F")
			Language.setMessage(mstrModuleName, 52, "Total Net Pay")
			Language.setMessage(mstrModuleName, 53, "Branch")
			Language.setMessage(mstrModuleName, 54, "Department Group")
			Language.setMessage(mstrModuleName, 55, "Department")
			Language.setMessage(mstrModuleName, 56, "Section Group")
			Language.setMessage(mstrModuleName, 57, "Section")
			Language.setMessage(mstrModuleName, 58, "Unit Group")
			Language.setMessage(mstrModuleName, 59, "Unit")
			Language.setMessage(mstrModuleName, 60, "Team")
			Language.setMessage(mstrModuleName, 61, "Job Group")
			Language.setMessage(mstrModuleName, 62, "Jobs")
			Language.setMessage(mstrModuleName, 63, "Class Group")
			Language.setMessage(mstrModuleName, 64, "Classes")
			Language.setMessage(mstrModuleName, 65, "Bank")
			Language.setMessage(mstrModuleName, 66, "Account #")
			Language.setMessage(mstrModuleName, 67, "Claims & Expense")
			Language.setMessage(mstrModuleName, 68, "First Name")
			Language.setMessage(mstrModuleName, 69, "Middle Name")
			Language.setMessage(mstrModuleName, 70, "Surname")
			Language.setMessage(mstrModuleName, 71, "No.")
			Language.setMessage(mstrModuleName, 72, "Cost Center Code")
			Language.setMessage(mstrModuleName, 73, "Cost Center Custom Code")

		Catch Ex As Exception
			Throw New Exception(ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

