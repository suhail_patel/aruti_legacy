#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
#End Region

Public Class frmPayrollTotalVariance

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmPayrollTotalVariance"
    Private objPayrollReconciliation As clsPayrollTotalVariance

    Private dicHeadType As New Dictionary(Of Integer, String) 'Sohail (20 Jun 2012)

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String
    'Sohail (21 Jul 2012) -- End

    Private mintFirstOpenPeriod As Integer = 0 'Sohail (17 Sep 2014)

    'Pinkal (09-Jan-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Pinkal (09-Jan-2013) -- End
    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
    Private dvHeads As DataView
    Private mstrSearchEmpText As String = ""
    Private mstrInfoHeadIDs As String = ""
    'Sohail (13 Jan 2020) -- End

#End Region

#Region "Constructor"
    Public Sub New()
        objPayrollReconciliation = New clsPayrollTotalVariance(User._Object._Languageunkid,Company._Object._Companyunkid)
        objPayrollReconciliation.SetDefaultValue()
        InitializeComponent()


        'Pinkal (15-Jan-2013) -- Start
        'Enhancement : TRA Changes
        _Show_ExcelExtra_Menu = True
        'Pinkal (15-Jan-2013) -- End

    End Sub
#End Region

    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
#Region " Private Enum "
    Private Enum enHeadTypeId
        TranHeadType = 1
        IgnoreZeroVariance = 2
        ShowVariancePercColumn = 3
        InfoHeadIDs = 4
        IncludeInactiveEmployee = 5
    End Enum
#End Region
    'Sohail (13 Jan 2020) -- End

#Region "Public Function"
    Public Sub FillCombo()
        'Sohail (20 Jun 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        'Sohail (20 Jun 2012) -- End
        Dim objPeriod As New clscommom_period_Tran 'Sohail (21 Jul 2012)
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid) 'Sohail (17 Sep 2014)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1) 'Sohail (17 Sep 2014)
            'S.SANDEEP [04 JUN 2015] -- END

            'Vimal (27 Nov 2010) -- Start 
            'cboPeriodFrom.DataSource = (New clscommom_period_Tran).getListForCombo(1, , , False).Tables(0)
            'Sohail (21 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'cboPeriodFrom.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period1", False).Tables(0)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            'Sohail (21 Jul 2012) -- End
            'Vimal (27 Nov 2010) -- End
            cboPeriodFrom.ValueMember = "periodunkid"
            cboPeriodFrom.DisplayMember = "name"
            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'cboPeriodFrom.SelectedValue = 0
            cboPeriodFrom.SelectedValue = mintFirstOpenPeriod
            'Sohail (17 Sep 2014) -- End
            cboPeriodFrom.DataSource = dsList.Tables(0) 'Sohail (21 Jul 2012

            'Vimal (27 Nov 2010) -- Start 
            'cboPeriodTo.DataSource = (New clscommom_period_Tran).getListForCombo(1, , , False).Tables(0)
            'cboPeriodTo.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period2", False).Tables(0) 'Sohail (21 Jul 2012
            'Vimal (27 Nov 2010) -- End
            cboPeriodTo.ValueMember = "periodunkid"
            cboPeriodTo.DisplayMember = "name"
            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'cboPeriodTo.SelectedValue = 0
            cboPeriodTo.SelectedValue = mintFirstOpenPeriod
            'Sohail (17 Sep 2014) -- End
            cboPeriodTo.DataSource = dsList.Tables(0).Copy 'Sohail (21 Jul 2012

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            dsList = objMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("HeadType")
                .SelectedValue = 0
            End With
            dicHeadType.Clear()
            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                dicHeadType.Add(CInt(dsList.Tables(0).Rows(i).Item("Id")), dsList.Tables(0).Rows(i).Item("Name").ToString)
            Next
            'Sohail (20 Jun 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
    Private Sub FillList()
        Dim objHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            RemoveHandler txtSearchHead.TextChanged, AddressOf txtSearchHead_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchHead.TextChanged, AddressOf txtSearchHead_TextChanged

            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", False, enTranHeadType.Informational)

            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If

            If mstrInfoHeadIDs.Trim <> "" Then
                Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (mstrInfoHeadIDs.Split(",").Contains(p.Item("tranheadunkid").ToString)) Select (p)).ToList
                For Each r As DataRow In lstRow
                    r.Item("IsChecked") = True
                Next
                dsList.Tables(0).AcceptChanges()
            End If

            dvHeads = dsList.Tables(0).DefaultView
            dgHeads.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhTranheadunkid.DataPropertyName = "tranheadunkid"
            dgColhHeadCode.DataPropertyName = "code"
            dgColhHeadName.DataPropertyName = "name"

            dgHeads.DataSource = dvHeads
            dvHeads.Sort = "IsChecked DESC, name "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgHeads.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            Dim mintCount As Integer = dvHeads.Table.Select("IsChecked = 1 ").Length
            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dvHeads.Table.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dvHeads.Table.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = Language.getMessage(mstrModuleName, 3, "Search Heads")
            With txtSearchHead
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllHeads(ByVal blnCheckAll As Boolean)
        Try
            If dvHeads IsNot Nothing Then
                For Each dr As DataRowView In dvHeads
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvHeads.ToTable.AcceptChanges()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllHeads", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.PayrollTotalVariance)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.TranHeadType
                            cboTrnHeadType.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.IgnoreZeroVariance
                            chkIgnoreZeroVariance.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.ShowVariancePercColumn
                            chkShowVariancePercentageColumns.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.InfoHeadIDs
                            mstrInfoHeadIDs = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.IncludeInactiveEmployee
                            chkInactiveemp.Checked = CBool(dsRow.Item("transactionheadid"))

                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub
    'Sohail (13 Jan 2020) -- End

    Public Sub ResetValue()
        Try

            If cboPeriodFrom.Items.Count > 0 Then
                'Sohail (17 Sep 2014) -- Start
                'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                'cboPeriodFrom.SelectedIndex = 0
                cboPeriodFrom.SelectedValue = mintFirstOpenPeriod
                'Sohail (17 Sep 2014) -- End
                'cboPeriodTo.SelectedIndex = 0
            End If


            'S.SANDEEP [ 10 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If cboPeriodTo.Items.Count >= 1 Then
            '    cboPeriodTo.SelectedIndex = 1
            'End If

            'If cboPeriodTo.Items.Count > 2 Then
            '    cboPeriodTo.SelectedIndex = 1
            'Else
            '    cboPeriodTo.SelectedIndex = 0
            'End If
            'S.SANDEEP [ 10 JULY 2012 ] -- END

              If cboPeriodTo.Items.Count >= 1 Then
                'Sohail (21 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                'cboPeriodTo.SelectedIndex = 1
                'Sohail (17 Sep 2014) -- Start
                'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                'cboPeriodTo.SelectedIndex = 0
                cboPeriodTo.SelectedValue = mintFirstOpenPeriod
                'Sohail (17 Sep 2014) -- End
                'Sohail (21 Jul 2012) -- End
            End If
            If cboPeriodFrom.SelectedIndex > 0 Then cboPeriodFrom.SelectedIndex = cboPeriodTo.SelectedIndex - 1 'Sohail (17 Sep 2014)

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            cboTrnHeadType.SelectedValue = 0

            objPayrollReconciliation.setDefaultOrderBy(0)
            txtOrderBy.Text = objPayrollReconciliation.OrderByDisplay
            'Sohail (20 Jun 2012) -- End

            chkInactiveemp.Checked = False

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            mstrStringIds = ""
            mintViewIdx = -1
            mstrStringName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Pinkal (15-Jan-2013) -- End

            'Anjan (07 Jun 2018) -- Start
            'Internal Enhancement - Including Ignore Zero Variance setting.
            chkIgnoreZeroVariance.Checked = True
            'Anjan (04 Jun 2018) -- End

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            Call GetValue()
            'Sohail (13 Jan 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValues", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            mstrInfoHeadIDs = ""
            'Sohail (13 Jan 2020) -- End

            'Sohail (21 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If cboPeriodFrom.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriodFrom.Focus()
                Exit Function
            End If
            If cboPeriodTo.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriodTo.Focus()
                Exit Function
            End If
            If cboPeriodTo.SelectedIndex <= cboPeriodFrom.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, " To Period cannot be less than or equal to Form Period"), enMsgBoxStyle.Information)
                cboPeriodTo.Focus()
                Exit Function
            End If
            'Sohail (21 Jul 2012) -- End

            objPayrollReconciliation._PeriodId1 = cboPeriodFrom.SelectedValue
            objPayrollReconciliation._PeriodName1 = cboPeriodFrom.Text

            objPayrollReconciliation._PeriodId2 = cboPeriodTo.SelectedValue
            objPayrollReconciliation._PeriodName2 = cboPeriodTo.Text

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            objPayrollReconciliation._TranHeadTypeId = cboTrnHeadType.SelectedValue
            objPayrollReconciliation._TranHeadTypeName = cboTrnHeadType.Text

            objPayrollReconciliation._DicTranHeadType = dicHeadType
            'Sohail (20 Jun 2012) -- End

            'Sohail (21 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            objPayrollReconciliation._FromDatabaseName = mstrFromDatabaseName
            objPayrollReconciliation._ToDatabaseName = mstrToDatabaseName
            'Sohail (21 Jul 2012) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            objPayrollReconciliation._FromYearUnkId = CInt(CType(cboPeriodFrom.SelectedItem, DataRowView).Item("yearunkid"))
            objPayrollReconciliation._ToYearUnkId = CInt(CType(cboPeriodTo.SelectedItem, DataRowView).Item("yearunkid"))
            'Sohail (21 Aug 2015) -- End

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            mstrInfoHeadIDs = String.Join(",", (From p In dvHeads.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("tranheadunkid").ToString)).ToArray)
            objPayrollReconciliation._InfoHeadIDs = mstrInfoHeadIDs
            'Sohail (13 Jan 2020) -- End

            objPayrollReconciliation._IncludeActiveEmployee = chkInactiveemp.Checked

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            objPayrollReconciliation._ViewByIds = mstrStringIds
            objPayrollReconciliation._ViewIndex = mintViewIdx
            objPayrollReconciliation._ViewByName = mstrStringName
            objPayrollReconciliation._Analysis_Fields = mstrAnalysis_Fields
            objPayrollReconciliation._Analysis_Join = mstrAnalysis_Join
            objPayrollReconciliation._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPayrollReconciliation._Report_GroupName = mstrReport_GroupName
            'Pinkal (15-Jan-2013) -- End

            'Anjan (07 Jun 2018) -- Start
            'Internal Enhancement - Including Ignore Zero Variance setting.
            objPayrollReconciliation._IgnoreZeroVariance = chkIgnoreZeroVariance.Checked
            'Anjan (04 Jun 2018) -- End

            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            objPayrollReconciliation._ShowVariancePercentageColumns = chkShowVariancePercentageColumns.Checked
            'Hemant (22 Jan 2019) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function
#End Region

#Region "Form's Events"

    Private Sub frmPayrollTotalVarianceReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPayrollReconciliation = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollTotalVarianceReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollTotalVarianceReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objPayrollReconciliation._ReportName
            Me._Message = objPayrollReconciliation._ReportDesc

            Call FillCombo()
            Call ResetValue()
            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            Call FillList()
            'Sohail (13 Jan 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollTotalVarianceReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollTotalVarianceReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmPayrollTotalVarianceReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollTotalVarianceReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollTotalVarianceReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollTotalVarianceReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons"
    Private Sub frmPayrollTotalVarianceReport_Report_Click(ByVal sender As System.Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles MyBase.Report_Click
        Try
            'Sohail (21 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'If cboPeriodFrom.SelectedValue >= cboPeriodTo.SelectedValue Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period To should be greater than Period From."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Sohail (21 Jul 2012) -- End

            If SetFilter() = False Then Exit Sub
            'Vimal (27 Nov 2010) -- Start 
            'objPayrollReconciliation.generateReport(Aruti.Data.enReportType.Standard, Aruti.Data.enPrintAction.Preview, Aruti.Data.enExportAction.None)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPayrollReconciliation.generateReport(0, Aruti.Data.enPrintAction.Preview, Aruti.Data.enExportAction.None)
            Dim objFromPeriod As New clscommom_period_Tran
            Dim objYear As New clsCompany_Master
            objYear._YearUnkid = CInt(CType(cboPeriodFrom.SelectedItem, DataRowView).Item("yearunkid"))
            objFromPeriod._Periodunkid(objYear._DatabaseName) = CInt(cboPeriodFrom.SelectedValue)
            Dim objToPeriod As New clscommom_period_Tran
            objYear = New clsCompany_Master
            objYear._YearUnkid = CInt(CType(cboPeriodTo.SelectedItem, DataRowView).Item("yearunkid"))
            objToPeriod._Periodunkid(objYear._DatabaseName) = CInt(cboPeriodTo.SelectedValue)

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            objPayrollReconciliation._FromPeriodStart = objFromPeriod._Start_Date
            objPayrollReconciliation._FromPeriodEnd = objFromPeriod._End_Date
            objPayrollReconciliation._ToPeriodStart = objToPeriod._Start_Date
            objPayrollReconciliation._ToPeriodEnd = objToPeriod._End_Date
            'Sohail (13 Jan 2020) -- End

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objPayrollReconciliation._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objToPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            objPayrollReconciliation.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objFromPeriod._Start_Date, objToPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, Aruti.Data.enPrintAction.Preview, Aruti.Data.enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End
            'Vimal (27 Nov 2010) -- End

          

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollTotalVarianceReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollTotalVarianceReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub
            'Vimal (27 Nov 2010) -- Start 
            'objPayrollReconciliation.generateReport(Aruti.Data.enReportType.Standard, enPrintAction.None, e.Type)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPayrollReconciliation.generateReport(0, enPrintAction.None, e.Type)
            Dim objFromPeriod As New clscommom_period_Tran
            Dim objYear As New clsCompany_Master
            objYear._YearUnkid = CInt(CType(cboPeriodFrom.SelectedItem, DataRowView).Item("yearunkid"))
            objFromPeriod._Periodunkid(objYear._DatabaseName) = CInt(cboPeriodFrom.SelectedValue)
            Dim objToPeriod As New clscommom_period_Tran
            objYear = New clsCompany_Master
            objYear._YearUnkid = CInt(CType(cboPeriodTo.SelectedItem, DataRowView).Item("yearunkid"))
            objToPeriod._Periodunkid(objYear._DatabaseName) = CInt(cboPeriodTo.SelectedValue)

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            objPayrollReconciliation._FromPeriodStart = objFromPeriod._Start_Date
            objPayrollReconciliation._FromPeriodEnd = objFromPeriod._End_Date
            objPayrollReconciliation._ToPeriodStart = objToPeriod._Start_Date
            objPayrollReconciliation._ToPeriodEnd = objToPeriod._End_Date
            'Sohail (13 Jan 2020) -- End

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objPayrollReconciliation._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objToPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            objPayrollReconciliation.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objFromPeriod._Start_Date, objToPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End
            'Vimal (27 Nov 2010) -- End

            'Anjan (07 Jun 2018) -- Start
            'Internal Enhancement - Including Ignore Zero Variance setting.
            objPayrollReconciliation._IgnoreZeroVariance = chkIgnoreZeroVariance.Checked
            'Anjan (04 Jun 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollTotalVarianceReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollTotalVarianceReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollTotalVarianceReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollTotalVarianceReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollTotalVarianceReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub frmPayrollTotalVarianceReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayrollTotalVariance.SetMessages()
            objfrm._Other_ModuleNames = "clsPayrollTotalVariance"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmPayrollTotalVarianceReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End

    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()
                Dim intUnkid As Integer = -1
                objUserDefRMode._Reportunkid = enArutiReport.PayrollTotalVariance
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.TranHeadType
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboTrnHeadType.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollTotalVariance, 0, 0, intHeadType)

                    Case enHeadTypeId.IgnoreZeroVariance
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkIgnoreZeroVariance.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollTotalVariance, 0, 0, intHeadType)

                    Case enHeadTypeId.ShowVariancePercColumn
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkShowVariancePercentageColumns.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollTotalVariance, 0, 0, intHeadType)

                    Case enHeadTypeId.InfoHeadIDs
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = mstrInfoHeadIDs

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollTotalVariance, 0, 0, intHeadType)

                    Case enHeadTypeId.IncludeInactiveEmployee
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkInactiveemp.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PayrollTotalVariance, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
            Next
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (13 Jan 2020) -- End


#End Region

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Combobox's Events "
    Private Sub cboPeriodFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriodFrom.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriodFrom.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriodFrom.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrFromDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrFromDatabaseName = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriodFrom_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try
    End Sub

    Private Sub cboPeriodTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriodTo.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriodTo.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriodTo.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrToDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrToDatabaseName = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriodTo_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try
    End Sub

    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Try
            If CInt(cboTrnHeadType.SelectedValue) <= 0 OrElse CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.Informational Then
                gbHeads.Enabled = True
            Else
                gbHeads.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllHeads(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (13 Jan 2020) -- End

#End Region
    'Sohail (21 Jul 2012) -- End

    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
#Region " GridView Events "

    Private Sub dgHeads_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgHeads.CurrentCellDirtyStateChanged, dgHeads.CurrentCellDirtyStateChanged
        Try
            If dgHeads.IsCurrentCellDirty Then
                dgHeads.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgHeads_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgHeads_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgHeads.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgHeads_CellValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Textbox Events "

    Private Sub txtSearchHead_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchHead.GotFocus
        Try
            With txtSearchHead
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchHead_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchHead_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchHead.Leave
        Try
            If txtSearchHead.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchHead_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchHead_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchHead.TextChanged
        Try
            If txtSearchHead.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvHeads IsNot Nothing Then
                dvHeads.RowFilter = "code LIKE '%" & txtSearchHead.Text.Replace("'", "''") & "%'  OR name LIKE '%" & txtSearchHead.Text.Replace("'", "''") & "%'"
                dgHeads.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchHead_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (13 Jan 2020) -- End

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objPayrollReconciliation.setOrderBy(0)
            txtOrderBy.Text = objPayrollReconciliation.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (15-Jan-2013) -- Start
    'Enhancement : TRA Changes

#Region "LinkButton Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm._DatabaseName = FinancialYear._Object._DatabaseName 'Sohail (22 Jan 2021) 
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try

    End Sub

#End Region

    'Pinkal (15-Jan-2013) -- End




    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbHeads.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbHeads.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPeriodTo.Text = Language._Object.getCaption(Me.lblPeriodTo.Name, Me.lblPeriodTo.Text)
			Me.lblPeriodFrom.Text = Language._Object.getCaption(Me.lblPeriodFrom.Name, Me.lblPeriodFrom.Text)
			Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.Name, Me.lblTrnHeadType.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.chkIgnoreZeroVariance.Text = Language._Object.getCaption(Me.chkIgnoreZeroVariance.Name, Me.chkIgnoreZeroVariance.Text)
            Me.chkShowVariancePercentageColumns.Text = Language._Object.getCaption(Me.chkShowVariancePercentageColumns.Name, Me.chkShowVariancePercentageColumns.Text)
			Me.gbHeads.Text = Language._Object.getCaption(Me.gbHeads.Name, Me.gbHeads.Text)
			Me.dgColhHeadCode.HeaderText = Language._Object.getCaption(Me.dgColhHeadCode.Name, Me.dgColhHeadCode.HeaderText)
			Me.dgColhHeadName.HeaderText = Language._Object.getCaption(Me.dgColhHeadName.Name, Me.dgColhHeadName.HeaderText)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue.")
			Language.setMessage(mstrModuleName, 2, " To Period cannot be less than or equal to Form Period")
			Language.setMessage(mstrModuleName, 3, "Search Heads")
			Language.setMessage(mstrModuleName, 4, "Selection Saved Successfully")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
