'************************************************************************************************************************************
'Class Name : clsLongServiceAwardReport.vb
'Purpose    :
'Date       :23/05/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsLongServiceAwardReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLongServiceAwardReport"
    Private mstrReportId As String = enArutiReport.Long_Service_Award_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mstrEmployeeCode As String = String.Empty
    Private mintConditionId As Integer = 0
    Private mstrCondition As String = String.Empty
    Private mintStartingYear As Integer = 0
    Private mintEndingYear As Integer = 0
    Private mintTranheadunkid As Integer = 0
    Private mstrTranheadname As String = String.Empty

    Private mblnIncludeInActiveEmployee As Boolean = False

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1

    Private mstrUserAccessFilter As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _ConditionId() As Integer
        Set(ByVal value As Integer)
            mintConditionId = value
        End Set
    End Property

    Public WriteOnly Property _Condition() As String
        Set(ByVal value As String)
            mstrCondition = value
        End Set
    End Property

    Public WriteOnly Property _StartingYear() As Integer
        Set(ByVal value As Integer)
            mintStartingYear = value
        End Set
    End Property

    Public WriteOnly Property _EndingYear() As Integer
        Set(ByVal value As Integer)
            mintEndingYear = value
        End Set
    End Property

    Public WriteOnly Property _Tranheadunkid() As Integer
        Set(ByVal value As Integer)
            mintTranheadunkid = value
        End Set
    End Property

    Public WriteOnly Property _Tranheadname() As String
        Set(ByVal value As String)
            mstrTranheadname = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInActiveEmployee = value
        End Set
    End Property


    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property


    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrEmployeeCode = ""
            mintConditionId = 0
            mstrCondition = String.Empty
            mintStartingYear = 0
            mintEndingYear = 0
            mintTranheadunkid = 0
            mstrTranheadname = ""

            mblnIncludeInActiveEmployee = False

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            mstrAdvance_Filter = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "As On Period :") & " " & mstrPeriodName & " "

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Employee :") & " " & mstrEmployeeName & " "
            End If

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Transaction Head :") & " " & mstrTranheadname & " "

            objDataOperation.AddParameter("@next_startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate.AddDays(1)))
            If mblnIncludeInActiveEmployee = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If mintConditionId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Year Of Service") & " " & mstrCondition & " " & mintStartingYear.ToString
                If mintConditionId = enComparison_Operator.BETWEEN Then
                    Me._FilterTitle &= " " & Language.getMessage(mstrModuleName, 21, "AND") & " " & mintEndingYear.ToString
                End If
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Order By :") & " " & Me.OrderByDisplay & " "
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= "ORDER BY " & mstrAnalysis_OrderBy & "," & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
            Else
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= "ORDER BY " & mstrAnalysis_OrderBy
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt


        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("hremployee_master.employeecode", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("hremployee_master.appointeddate", Language.getMessage(mstrModuleName, 6, "Appointed Date")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim blnFlag As Boolean = False
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ = "SELECT  hremployee_master.employeeunkid " & _
    '                      ", employeecode " & _
    '                      ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS employeename " & _
    '                      ", CONVERT(CHAR(8), appointeddate, 112) AS AppDate " & _
    '                      ", appointeddate AS OAppDate " & _
    '                      ", ISNULL(CONVERT(CHAR(8), birthdate, 112), '') AS birthdate " & _
    '                      ", ISNULL(CONVERT(CHAR(8), termination_to_date, 112), '') AS retirementdate " & _
    '                      ", hremployee_master.jobunkid " & _
    '                      ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
    '                      ", hremployee_master.stationunkid AS BranchId " & _
    '                      ", ISNULL(hrstation_master.name, '') AS BranchName " & _
    '                      ", ( CAST(CONVERT(CHAR(8), @next_startdate, 112) AS INT) - CAST(CONVERT(CHAR(8), appointeddate, 112) AS INT) ) / 10000 AS Years " & _
    '                      ", ( DATEDIFF(MONTH, CONVERT(CHAR(8), appointeddate, 112), @next_startdate) ) - CASE WHEN DAY(appointeddate) > DAY(@next_startdate) THEN 1 ELSE 0 END AS Months " & _
    '                      ", ISNULL(prpayrollprocess_tran.amount, 0) AS amount "

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If

    '        StrQ &= "FROM    hremployee_master " & _
    '                        "LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '                        "LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
    '                        "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                           "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                           "AND ISNULL(prpayrollprocess_tran.tranheadunkid, @tranheadunkid) = @tranheadunkid " & _
    '                        "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                                           "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "WHERE   isapproved = 1 " & _
    '                        "AND ISNULL(prtnaleave_tran.payperiodunkid, @payperiodunkid) = @payperiodunkid "

    '        If mblnIncludeInActiveEmployee = False Then
    '            StrQ &= "AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    "AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    "AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    "AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= "AND hremployee_master.employeeunkid = @employeeunkid "
    '        End If

    '        If mintConditionId > 0 Then
    '            'Sohail (30 Jun 2014) -- Start
    '            'Enhancement - show only exact no of years employee. i. e. sont shoiw 15 years + 1 month employee for = 15 condition 
    '            'StrQ &= "AND ( ( CAST(CONVERT(CHAR(8), @next_startdate, 112) AS INT) - CAST(CONVERT(CHAR(8), appointeddate, 112) AS INT) ) / 10000 ) " & mstrCondition & " " & mintStartingYear & " "
    '            StrQ &= "AND ( DATEDIFF(MONTH, CONVERT(CHAR(8), appointeddate, 112), @next_startdate) ) - CASE WHEN DAY(appointeddate) > DAY(@next_startdate) THEN 1 ELSE 0 END  " & mstrCondition & " " & mintStartingYear * 12 & " "
    '            'Sohail (30 Jun 2014) -- End

    '            If mintConditionId = enComparison_Operator.BETWEEN Then
    '                'Sohail (30 Jun 2014) -- Start
    '                'Enhancement - show only exact no of years employee. i. e. sont shoiw 15 years + 1 month employee for = 15 condition
    '                'StrQ &= "AND " & mintEndingYear & " "
    '                StrQ &= "AND " & mintEndingYear * 12 & " "
    '                'Sohail (30 Jun 2014) -- End
    '            End If
    '        End If

    '        If mstrUserAccessFilter = "" Then
    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '        Else
    '            StrQ &= mstrUserAccessFilter
    '        End If

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If


    '        Call FilterTitleAndFilterQuery()
    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If





    '        rpt_Data = New ArutiReport.Designer.dsArutiReport
    '        Dim rpt_Rows As DataRow = Nothing

    '        For Each dFRow As DataRow In dsList.Tables(0).Rows
    '            rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
    '            rpt_Rows.Item("Column1") = dFRow.Item("employeecode")
    '            rpt_Rows.Item("Column2") = dFRow.Item("employeename")
    '            If dFRow.Item("AppDate").ToString.Trim <> "" Then
    '                rpt_Rows.Item("Column3") = eZeeDate.convertDate(dFRow.Item("AppDate").ToString).ToShortDateString
    '            Else
    '                rpt_Rows.Item("Column3") = ""
    '            End If
    '            rpt_Rows.Item("Column4") = dFRow.Item("Years")
    '            rpt_Rows.Item("Column5") = dFRow.Item("Months")
    '            rpt_Rows.Item("Column6") = Format(dFRow.Item("Amount"), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column7") = dFRow.Item("GName")
    '            If dFRow.Item("birthdate").ToString.Trim <> "" Then
    '                rpt_Rows.Item("Column8") = eZeeDate.convertDate(dFRow.Item("birthdate").ToString).ToShortDateString
    '            Else
    '                rpt_Rows.Item("Column8") = ""
    '            End If

    '            If dFRow.Item("retirementdate").ToString.Trim <> "" Then
    '                rpt_Rows.Item("Column9") = eZeeDate.convertDate(dFRow.Item("retirementdate").ToString).ToShortDateString
    '            Else
    '                rpt_Rows.Item("Column9") = ""
    '            End If
    '            rpt_Rows.Item("Column10") = dFRow.Item("jobunkid").ToString
    '            rpt_Rows.Item("Column11") = dFRow.Item("job_name").ToString
    '            rpt_Rows.Item("Column12") = dFRow.Item("BranchId").ToString
    '            rpt_Rows.Item("Column13") = dFRow.Item("BranchName").ToString

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '        Next


    '        objRpt = New ArutiReport.Designer.rptLongServiceAwardReport

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmplCode", Language.getMessage(mstrModuleName, 1, "Employee Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmplName", Language.getMessage(mstrModuleName, 2, "Employee Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 3, "Job"))
    '        Call ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 4, "Branch"))
    '        Call ReportFunction.TextChange(objRpt, "txtBirthdate", Language.getMessage(mstrModuleName, 5, "Birthdate"))
    '        Call ReportFunction.TextChange(objRpt, "txtAppointedDate", Language.getMessage(mstrModuleName, 6, "Appointed Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 7, "Years"))
    '        Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 8, "Amount"))

    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 9, "Grand Total :"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 12, "Page :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)


    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 13, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 14, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 15, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 16, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            If mdtPeriodStartDate <> Nothing Then dtPeriodStart = mdtPeriodStartDate
            If mdtPeriodEndDate <> Nothing Then dtPeriodEnd = mdtPeriodEndDate

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "SELECT  hremployee_master.employeeunkid " & _
                          ", employeecode " & _
                          ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS employeename " & _
                          ", CONVERT(CHAR(8), appointeddate, 112) AS AppDate " & _
                          ", appointeddate AS OAppDate " & _
                          ", ISNULL(CONVERT(CHAR(8), birthdate, 112), '') AS birthdate " & _
                          ", ISNULL(CONVERT(CHAR(8), RT.termination_to_date, 112), '') AS retirementdate " & _
                          ", JM.jobunkid " & _
                          ", ISNULL(JM.job_name, '') AS job_name " & _
                          ", SM.stationunkid AS BranchId " & _
                          ", ISNULL(SM.name, '') AS BranchName " & _
                          ", ( CAST(CONVERT(CHAR(8), @next_startdate, 112) AS INT) - CAST(CONVERT(CHAR(8), appointeddate, 112) AS INT) ) / 10000 AS Years " & _
                          ", ( DATEDIFF(MONTH, CONVERT(CHAR(8), appointeddate, 112), @next_startdate) ) - CASE WHEN DAY(appointeddate) > DAY(@next_startdate) THEN 1 ELSE 0 END AS Months " & _
                          ", ISNULL(prpayrollprocess_tran.amount, 0) AS amount "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hremployee_master " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,jobgroupunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN hrstation_master AS SM ON SM.stationunkid = Alloc.stationunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         date1 AS termination_to_date " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_dates_tran " & _
                    "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ")AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 " & _
                    "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.employeeunkid " & _
                    "   AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "   AND ISNULL(prpayrollprocess_tran.tranheadunkid, @tranheadunkid) = @tranheadunkid "
            'Sohail (03 Dec 2021) - [LEFT JOIN prpayrollprocess_tran then LEFT JOIN prtnaleave_tran]

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= mstrAnalysis_Join

            StrQ &= "WHERE ISNULL(prtnaleave_tran.payperiodunkid, @payperiodunkid) = @payperiodunkid "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mintEmployeeId > 0 Then
                StrQ &= "AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If mintConditionId > 0 Then
                StrQ &= "AND ( DATEDIFF(MONTH, CONVERT(CHAR(8), appointeddate, 112), @next_startdate) ) - CASE WHEN DAY(appointeddate) > DAY(@next_startdate) THEN 1 ELSE 0 END  " & mstrCondition & " " & mintStartingYear * 12 & " "
                If mintConditionId = enComparison_Operator.BETWEEN Then
                    StrQ &= "AND " & mintEndingYear * 12 & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If


            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If





            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim rpt_Rows As DataRow = Nothing

            For Each dFRow As DataRow In dsList.Tables(0).Rows
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dFRow.Item("employeecode")
                rpt_Rows.Item("Column2") = dFRow.Item("employeename")
                If dFRow.Item("AppDate").ToString.Trim <> "" Then
                    rpt_Rows.Item("Column3") = eZeeDate.convertDate(dFRow.Item("AppDate").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column3") = ""
                End If
                rpt_Rows.Item("Column4") = dFRow.Item("Years")
                rpt_Rows.Item("Column5") = dFRow.Item("Months")
                rpt_Rows.Item("Column6") = Format(dFRow.Item("Amount"), GUI.fmtCurrency)
                rpt_Rows.Item("Column7") = dFRow.Item("GName")
                If dFRow.Item("birthdate").ToString.Trim <> "" Then
                    rpt_Rows.Item("Column8") = eZeeDate.convertDate(dFRow.Item("birthdate").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column8") = ""
                End If

                If dFRow.Item("retirementdate").ToString.Trim <> "" Then
                    rpt_Rows.Item("Column9") = eZeeDate.convertDate(dFRow.Item("retirementdate").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column9") = ""
                End If
                rpt_Rows.Item("Column10") = dFRow.Item("jobunkid").ToString
                rpt_Rows.Item("Column11") = dFRow.Item("job_name").ToString
                rpt_Rows.Item("Column12") = dFRow.Item("BranchId").ToString
                rpt_Rows.Item("Column13") = dFRow.Item("BranchName").ToString

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next


            objRpt = New ArutiReport.Designer.rptLongServiceAwardReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmplCode", Language.getMessage(mstrModuleName, 1, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmplName", Language.getMessage(mstrModuleName, 2, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 3, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 4, "Branch"))
            Call ReportFunction.TextChange(objRpt, "txtBirthdate", Language.getMessage(mstrModuleName, 5, "Birthdate"))
            Call ReportFunction.TextChange(objRpt, "txtAppointedDate", Language.getMessage(mstrModuleName, 6, "Appointed Date"))
            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 7, "Years"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 8, "Amount"))

            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 9, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 12, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 13, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 14, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 15, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 16, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'S.SANDEEP [04 JUN 2015] -- END


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code")
            Language.setMessage(mstrModuleName, 2, "Employee Name")
            Language.setMessage(mstrModuleName, 3, "Job")
            Language.setMessage(mstrModuleName, 4, "Branch")
            Language.setMessage(mstrModuleName, 5, "Birthdate")
            Language.setMessage(mstrModuleName, 6, "Appointed Date")
            Language.setMessage(mstrModuleName, 7, "Years")
            Language.setMessage(mstrModuleName, 8, "Amount")
            Language.setMessage(mstrModuleName, 9, "Grand Total :")
            Language.setMessage(mstrModuleName, 10, "Printed By :")
            Language.setMessage(mstrModuleName, 11, "Printed Date :")
            Language.setMessage(mstrModuleName, 12, "Page :")
            Language.setMessage(mstrModuleName, 13, "Prepared By :")
            Language.setMessage(mstrModuleName, 14, "Checked By :")
            Language.setMessage(mstrModuleName, 15, "Approved By :")
            Language.setMessage(mstrModuleName, 16, "Received By :")
            Language.setMessage(mstrModuleName, 17, "As On Period :")
            Language.setMessage(mstrModuleName, 18, "Employee :")
            Language.setMessage(mstrModuleName, 19, "Transaction Head :")
            Language.setMessage(mstrModuleName, 20, "Year Of Service")
            Language.setMessage(mstrModuleName, 21, "AND")
            Language.setMessage(mstrModuleName, 22, "Order By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
