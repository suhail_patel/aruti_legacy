'************************************************************************************************************************************
'Class Name : clsEmployee_Salary_Change_Report.vb
'Purpose    :
'Date       :12 JULY 2011
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter


#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsEmployee_Salary_Change_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_Salary_Change_Report"
    Private mstrReportId As String = enArutiReport.Employee_Salary_Change_Report    '97
    Dim objDataOperation As clsDataOperation

#Region "Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_DetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mstrEmpName As String = String.Empty
    Private mintEmployeeunkid As Integer = -1
    Private mdecCurrentScaleFrom As Decimal = 0
    Private mdecCurrentScaleTo As Decimal = 0
    Private mdecIncrementFrom As Decimal = 0
    Private mdecIncrementTo As Decimal = 0
    Private mdecNewScaleFrom As Decimal = 0
    Private mdecNewScaleTo As Decimal = 0
    Private mblnIncludeInactiveEmp As Boolean = True
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mdtIncrementDateFrom As Date = Nothing
    Private mdtIncrementDateTo As Date = Nothing



    'Pinkal (14-Dec-2012) -- Start
    'Enhancement : TRA Changes
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    'Pinkal (14-Dec-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End


    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    'Pinkal (24-May-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END


    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private mintReasonId As Integer = 0
    Private mstrReason As String = ""
    'Pinkal (06-May-2014) -- End

    'Sohail (18 Aug 2014) -- Start
    'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    Private mstrPeriodIDs As String = ""
    Private marrDatabaseName As New ArrayList
    Private mstrCurrentDatabaseName As String = FinancialYear._Object._DatabaseName
    'Sohail (18 Aug 2014) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _Emp_Name() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _Employeeunkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _Current_ScaleFrom() As Decimal
        Set(ByVal value As Decimal)
            mdecCurrentScaleFrom = value
        End Set
    End Property

    Public WriteOnly Property _Current_ScaleTo() As Decimal
        Set(ByVal value As Decimal)
            mdecCurrentScaleTo = value
        End Set
    End Property

    Public WriteOnly Property _Increment_From() As Decimal
        Set(ByVal value As Decimal)
            mdecIncrementFrom = value
        End Set
    End Property

    Public WriteOnly Property _Increment_To() As Decimal
        Set(ByVal value As Decimal)
            mdecIncrementTo = value
        End Set
    End Property

    Public WriteOnly Property _NewScale_From() As Decimal
        Set(ByVal value As Decimal)
            mdecNewScaleFrom = value
        End Set
    End Property

    Public WriteOnly Property _NewScale_To() As Decimal
        Set(ByVal value As Decimal)
            mdecNewScaleTo = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _IncrementDateFrom() As Date
        Set(ByVal value As Date)
            mdtIncrementDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _IncrementDateTo() As Date
        Set(ByVal value As Date)
            mdtIncrementDateTo = value
        End Set
    End Property

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (24-May-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END


    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _ReasonId() As Integer
        Set(ByVal value As Integer)
            mintReasonId = value
        End Set
    End Property

    Public WriteOnly Property _Reason() As String
        Set(ByVal value As String)
            mstrReason = value
        End Set
    End Property

    'Pinkal (06-May-2014) -- End

    'Sohail (18 Aug 2014) -- Start
    'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIDs() As String
        Set(ByVal value As String)
            mstrPeriodIDs = value
        End Set
    End Property

    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _CurrentDatabaseName() As String
        Set(ByVal value As String)
            mstrCurrentDatabaseName = value
        End Set
    End Property
    'Sohail (18 Aug 2014) -- End


#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrEmpName = String.Empty
            mintEmployeeunkid = -1
            mdecCurrentScaleFrom = 0
            mdecCurrentScaleTo = 0
            mdecIncrementFrom = 0
            mdecIncrementTo = 0
            mdecNewScaleFrom = 0
            mdecNewScaleTo = 0
            mblnIncludeInactiveEmp = True
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mdtIncrementDateFrom = Nothing
            mdtIncrementDateTo = Nothing

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End


            'Pinkal (06-May-2014) -- Start
            'Enhancement : TRA Changes
            mintReasonId = 0
            mstrReason = ""
            'Pinkal (06-May-2014) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mdtIncrementDateFrom <> Nothing AndAlso mdtIncrementDateTo <> Nothing Then
                objDataOperation.AddParameter("@mdtFromDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtIncrementDateFrom).ToString)
                objDataOperation.AddParameter("@mdtFromTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtIncrementDateTo).ToString)
                Me._FilterQuery &= " AND Last_Inc_Date BETWEEN @mdtFromDate AND @mdtFromTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Increment Date : ") & " " & mdtIncrementDateFrom.Date & " " & _
                                   Language.getMessage(mstrModuleName, 3, "To") & " " & mdtIncrementDateTo.Date & " "
            End If

            If mintEmployeeunkid > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                Me._FilterQuery &= " AND EmpId = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmpName & " "
            End If

            If mdecCurrentScaleFrom > 0 AndAlso mdecCurrentScaleTo > 0 Then
                objDataOperation.AddParameter("@CSF", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentScaleFrom)
                objDataOperation.AddParameter("@CST", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentScaleTo)
                Me._FilterQuery &= " AND Current_Scale >= @CSF AND Current_Scale <= @CST "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Current Scale From : ") & " " & mdecCurrentScaleFrom & " " & _
                                  Language.getMessage(mstrModuleName, 5, "To") & " " & mdecCurrentScaleTo & " "
            End If

            If mdecIncrementFrom > 0 AndAlso mdecIncrementTo > 0 Then
                objDataOperation.AddParameter("@INCF", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrementFrom)
                objDataOperation.AddParameter("@INCT", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecIncrementTo)
                Me._FilterQuery &= " AND Last_Inc_Date >= @INCF AND Last_Inc_Date <= @INCT "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Increment From : ") & " " & mdecIncrementFrom & " " & _
                                  Language.getMessage(mstrModuleName, 3, "To") & " " & mdecIncrementTo & " "
            End If

            If mdecNewScaleFrom > 0 AndAlso mdecNewScaleTo > 0 Then
                objDataOperation.AddParameter("@NSF", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNewScaleFrom)
                objDataOperation.AddParameter("@NST", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNewScaleTo)
                Me._FilterQuery &= " AND New_Scale >= @NSF AND New_Scale <= @NST "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "New Scale From : ") & " " & mdecNewScaleFrom & " " & _
                                  Language.getMessage(mstrModuleName, 5, "To") & " " & mdecNewScaleTo & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                'Sohail (18 Aug 2014) -- Start
                'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
                'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
                'Sohail (18 Aug 2014) -- End
            End If


            'Pinkal (06-May-2014) -- Start
            'Enhancement : TRA Changes

            If mintReasonId > 0 Then
                objDataOperation.AddParameter("@reasonid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonId)
                Me._FilterQuery &= " AND ReasonId = @reasonid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, "Reason : ") & " " & mstrReason & " "
            End If

            'Pinkal (06-May-2014) -- End


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Order By :") & " " & Me.OrderByDisplay
                Me._FilterQuery &= " ORDER BY GName, " & Me.OrderByQuery
            Else
                Me._FilterQuery &= " ORDER BY GName "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    'Pinkal (14-Dec-2012) -- Start
        '    'Enhancement : TRA Changes

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    'Pinkal (14-Dec-2012) -- End


        '    'Pinkal (24-May-2013) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (24-May-2013) -- End




        '    If Not IsNothing(objRpt) Then

        '        'Pinkal (14-Dec-2012) -- Start
        '        'Enhancement : TRA Changes

        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        Dim blnShowGrandTotal As Boolean = True
        '        Dim objDic As New Dictionary(Of Integer, Object)
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim strGTotal As String = Language.getMessage(mstrModuleName, 20, "Grand Total :")
        '        Dim strSubTotal As String = Language.getMessage(mstrModuleName, 21, "Sub Total :")
        '        Dim row As WorksheetRow
        '        Dim wcell As WorksheetCell

        '        If mdtTableExcel IsNot Nothing Then


        '            If mintViewIndex > 0 Then
        '                Dim strGrpCols As String() = {"column10"}
        '                strarrGroupColumns = strGrpCols
        '            End If


        '            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '                intArrayColumnWidth(i) = 125
        '            Next


        '            '*******   REPORT FOOTER   ******
        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s8w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)
        '            '----------------------


        '            If ConfigParameter._Object._IsShowCheckedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowApprovedBy = True Then

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowReceivedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)
        '            End If

        '        End If

        '        ' Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)

        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic)

        '    End If

        '    'Pinkal (14-Dec-2012) -- End

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            objRpt = Generate_DetailReport(xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then

                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim blnShowGrandTotal As Boolean = True
                Dim objDic As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim strGTotal As String = Language.getMessage(mstrModuleName, 20, "Grand Total :")
                Dim strSubTotal As String = Language.getMessage(mstrModuleName, 21, "Sub Total :")
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell

                If mdtTableExcel IsNot Nothing Then
                    If mintViewIndex > 0 Then
                        Dim strGrpCols As String() = {"column10"}
                        strarrGroupColumns = strGrpCols
                    End If

                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next

                    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------

                    If ConfigParameter._Object._IsShowCheckedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowApprovedBy = True Then

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowReceivedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    End If

                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic)

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""

        Try
            OrderByDisplay = iColumn_Detail.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_Detail.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

        Try
            Call OrderByExecute(iColumn_Detail)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_Detail As New IColumnCollection
    Public Property Field_DetailReport() As IColumnCollection
        Get
            Return iColumn_Detail
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_Detail = value
        End Set
    End Property

    Private Sub Create_DetailReport()
        Try
            iColumn_Detail.Clear()
            iColumn_Detail.Add(New IColumn("incrementdate", Language.getMessage(mstrModuleName, 9, "Increment Date")))
            iColumn_Detail.Add(New IColumn("ECode", Language.getMessage(mstrModuleName, 10, "Employee Code")))
            iColumn_Detail.Add(New IColumn("EName", Language.getMessage(mstrModuleName, 11, "Employee Name")))
            iColumn_Detail.Add(New IColumn("Current_Scale", Language.getMessage(mstrModuleName, 12, "Current Scale")))
            iColumn_Detail.Add(New IColumn("Increment", Language.getMessage(mstrModuleName, 13, "Increment Amount")))
            iColumn_Detail.Add(New IColumn("New_Scale", Language.getMessage(mstrModuleName, 14, "New Scale")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim strQ As String = ""
    '    'Sohail (18 Aug 2014) -- Start
    '    'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
    '    Dim StrInnerQ As String = ""
    '    Dim strDatabaseName As String = ""
    '    'Sohail (18 Aug 2014) -- End
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As New ArutiReport.Designer.dsArutiReport
    '    objDataOperation = New clsDataOperation
    '    Try
    '        'S.SANDEEP [ 04 JULY 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'strQ = "SELECT " & _
    '        '        "	 ECode AS ECode " & _
    '        '        "	,EName AS EName " & _
    '        '        "	,Grade AS Grade " & _
    '        '        "	,Job AS Job " & _
    '        '        "	,Last_Inc_Date AS Last_Inc_Date " & _
    '        '        "	,Current_Scale AS Current_Scale " & _
    '        '        "	,Increment AS Increment " & _
    '        '        "	,New_Scale AS New_Scale " & _
    '        '        "	,Reason AS Reason " & _
    '        '        "	,EmpId AS EmpId " & _
    '        '        "	,incrementdate AS incrementdate " & _
    '        '        "   ,GName AS GName " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '        "	SELECT " & _
    '        '        "		 employeecode AS ECode " & _
    '        '        "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '        '        "		,ISNULL(EGrd.name,'') AS Grade " & _
    '        '        "		,ISNULL(EJOB.job_name,'') AS Job " & _
    '        '        "		,CONVERT(CHAR(8),incrementdate,112) AS Last_Inc_Date " & _
    '        '        "		,incrementdate AS incrementdate " & _
    '        '        "		,ISNULL(newscale,0) AS Current_Scale " & _
    '        '        "		,ISNULL(prwages_tran.increment,0) AS Increment " & _
    '        '        "		,ISNULL(newscale,0)+ ISNULL(prwages_tran.increment,0) AS New_Scale " & _
    '        '        "		,ISNULL(cfcommon_master.name,'') AS Reason " & _
    '        '        "		,ROW_NUMBER() OVER(PARTITION BY hremployee_master.employeeunkid ORDER BY incrementdate DESC) AS Rno " & _
    '        '        "		,hremployee_master.employeeunkid AS EmpId "
    '        'If mintViewIndex > 0 Then
    '        '    strQ &= mstrAnalysis_Fields
    '        'Else
    '        '    strQ &= ", '' AS GName "
    '        'End If
    '        'strQ &= "	FROM prsalaryincrement_tran " & _
    '        '        "		LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = prsalaryincrement_tran.reason_id AND mastertype = 29 " & _
    '        '        "		JOIN hremployee_master ON prsalaryincrement_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "		JOIN hrjob_master EJOB ON hremployee_master.jobunkid = EJOB.jobunkid " & _
    '        '        "		JOIN hrgrade_master AS EGrd ON hremployee_master.gradeunkid = EGrd.gradeunkid " & _
    '        '        "		JOIN hrgradelevel_master AS GL ON EGrd.gradeunkid = GL.gradeunkid " & _
    '        '        "		JOIN prwages_tran ON GL.gradelevelunkid = prwages_tran.gradelevelunkid " & _
    '        '        "			AND EGrd.gradeunkid = prwages_tran.gradeunkid "
    '        'If mintViewIndex > 0 Then
    '        '    strQ &= mstrAnalysis_Join
    '        'End If

    '        'If mblnIncludeInactiveEmp = False Then
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        'End If

    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If

    '        'strQ &= "	WHERE isfromemployee = 0 " & _
    '        '        ") AS A WHERE Rno = 1 "

    '        'Sohail (18 Aug 2014) -- Start
    '        'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
    '        'strQ = "SELECT " & _
    '        '        "	 ECode AS ECode " & _
    '        '        "	,EName AS EName " & _
    '        '        "	,Grade AS Grade " & _
    '        '        "	,Job AS Job " & _
    '        '        "	,Last_Inc_Date AS Last_Inc_Date " & _
    '        '        "	,Current_Scale AS Current_Scale " & _
    '        '        "	,Increment AS Increment " & _
    '        '        "	,New_Scale AS New_Scale " & _
    '        '        "	,reasonid AS ReasonId " & _
    '        '        "	,Reason AS Reason " & _
    '        '        "	,EmpId AS EmpId " & _
    '        '        "	,incrementdate AS incrementdate " & _
    '        '        "   ,GName AS GName " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '        "	SELECT " & _
    '        '        "		 employeecode AS ECode "
    '        ''S.SANDEEP [ 26 MAR 2014 ] -- START
    '        ''hremployee_master.surname+' ' + hremployee_master.firstname AS empname
    '        'If mblnFirstNamethenSurname = False Then
    '        '    strQ &= "		,ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS EName "
    '        'Else
    '        '    strQ &= "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName "
    '        'End If

    '        'strQ &= "		,ISNULL(EGrd.name,'') AS Grade " & _
    '        '        "		,ISNULL(EJOB.job_name,'') AS Job " & _
    '        '        "		,CONVERT(CHAR(8),incrementdate,112) AS Last_Inc_Date " & _
    '        '        "		,incrementdate AS incrementdate " & _
    '        '        "		,ISNULL(currentscale,0) AS Current_Scale " & _
    '        '        "		,ISNULL(increment,0) AS Increment " & _
    '        '        "		,ISNULL(newscale,0) AS New_Scale " & _
    '        '        "       , ISNULL(prsalaryincrement_tran.reason_id, 0) AS reasonid " & _
    '        '        "		,ISNULL(cfcommon_master.name,'') AS Reason " & _
    '        '        "		,hremployee_master.employeeunkid AS EmpId "
    '        'If mintViewIndex > 0 Then
    '        '    strQ &= mstrAnalysis_Fields
    '        'Else
    '        '    strQ &= ", '' AS GName "
    '        'End If
    '        'strQ &= "	FROM prsalaryincrement_tran " & _
    '        '        "		LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = prsalaryincrement_tran.reason_id AND mastertype = " & clsCommon_Master.enCommonMaster.SALINC_REASON & " " & _
    '        '        "		JOIN hremployee_master ON prsalaryincrement_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "		JOIN hrjob_master EJOB ON hremployee_master.jobunkid = EJOB.jobunkid " & _
    '        '        "		JOIN hrgrade_master AS EGrd ON hremployee_master.gradeunkid = EGrd.gradeunkid "
    '        'If mintViewIndex > 0 Then
    '        '    strQ &= mstrAnalysis_Join
    '        'End If

    '        ''Pinkal (27-Feb-2013) -- Start
    '        ''Enhancement : TRA Changes
    '        'If mstrAdvance_Filter.Trim.Length > 0 Then
    '        '    strQ &= " AND " & mstrAdvance_Filter
    '        'End If
    '        ''Pinkal (27-Feb-2013) -- End

    '        'If mblnIncludeInactiveEmp = False Then
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        'End If


    '        ''Pinkal (24-May-2013) -- Start
    '        ''Enhancement : TRA Changes

    '        ''If UserAccessLevel._AccessLevel.Length > 0 Then
    '        ''    strQ &= UserAccessLevel._AccessLevelFilterString
    '        ''End If

    '        'If mstrUserAccessFilter = "" Then
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        'Else
    '        '    strQ &= mstrUserAccessFilter
    '        'End If

    '        ''Pinkal (24-May-2013) -- End


    '        'strQ &= " WHERE isfromemployee = 0 AND currentscale <> newscale AND prsalaryincrement_tran.isvoid = 0 " & _
    '        '        " AND prsalaryincrement_tran.isapproved =1 " & _
    '        '        ") AS A WHERE 1 = 1 "
    '        strQ = "SELECT  ECode AS ECode  " & _
    '                "	,EName AS EName " & _
    '                "	,Grade AS Grade " & _
    '                "	,Job AS Job " & _
    '                "	,Last_Inc_Date AS Last_Inc_Date " & _
    '                "	,Current_Scale AS Current_Scale " & _
    '                "	,Increment AS Increment " & _
    '                "	,New_Scale AS New_Scale " & _
    '                "	,reasonid AS ReasonId " & _
    '                "	,Reason AS Reason " & _
    '                "	,EmpId AS EmpId " & _
    '                "	,incrementdate AS incrementdate " & _
    '                "   ,GName AS GName " & _
    '                "FROM    ( "

    '        For i = 0 To marrDatabaseName.Count - 1
    '            strDatabaseName = marrDatabaseName(i)

    '            If i > 0 Then
    '                StrInnerQ &= " UNION ALL "
    '            End If

    '            StrInnerQ &= "    SELECT    employeecode AS ECode  " & _
    '                                      ", ISNULL(EGrd.name, '') AS Grade " & _
    '                "		,ISNULL(EJOB.job_name,'') AS Job " & _
    '                "		,CONVERT(CHAR(8),incrementdate,112) AS Last_Inc_Date " & _
    '                "		,incrementdate AS incrementdate " & _
    '                "		,ISNULL(currentscale,0) AS Current_Scale " & _
    '                "		,ISNULL(increment,0) AS Increment " & _
    '                "		,ISNULL(newscale,0) AS New_Scale " & _
    '                "       , ISNULL(prsalaryincrement_tran.reason_id, 0) AS reasonid " & _
    '                "		,ISNULL(cfcommon_master.name,'') AS Reason " & _
    '                "		,hremployee_master.employeeunkid AS EmpId "

    '            If mblnFirstNamethenSurname = False Then
    '                StrInnerQ &= "		,ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS EName "
    '            Else
    '                StrInnerQ &= "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName "
    '            End If

    '            If mintViewIndex > 0 Then
    '                StrInnerQ &= mstrAnalysis_Fields
    '            Else
    '                StrInnerQ &= ", '' AS GName "
    '            End If

    '            StrInnerQ &= "     FROM      " & strDatabaseName & "..prsalaryincrement_tran " & _
    '                                        "LEFT JOIN " & strDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = prsalaryincrement_tran.reason_id " & _
    '                                                                     "AND mastertype = " & clsCommon_Master.enCommonMaster.SALINC_REASON & " " & _
    '                                        "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON prsalaryincrement_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                        "JOIN " & mstrCurrentDatabaseName & "..hrjob_master EJOB ON hremployee_master.jobunkid = EJOB.jobunkid " & _
    '                                        "JOIN " & mstrCurrentDatabaseName & "..hrgrade_master AS EGrd ON hremployee_master.gradeunkid = EGrd.gradeunkid "

    '            If mintViewIndex > 0 Then
    '                StrInnerQ &= mstrAnalysis_Join
    '            End If

    '            StrInnerQ &= "     WHERE     isfromemployee = 0 " & _
    '                                        "AND currentscale <> newscale " & _
    '                                        "AND prsalaryincrement_tran.isvoid = 0 " & _
    '                                        "AND prsalaryincrement_tran.isapproved = 1 " & _
    '                                        "AND periodunkid IN (" & mstrPeriodIDs & ") "
    '            '                           'Sohail (04 Dec 2014) - [AND periodunkid IN (" & mstrPeriodIDs & ") ]

    '            If mstrAdvance_Filter.Trim.Length > 0 Then
    '                StrInnerQ &= " AND " & mstrAdvance_Filter
    '            End If

    '            If mblnIncludeInactiveEmp = False Then
    '                StrInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '            End If

    '            If mstrUserAccessFilter = "" Then
    '                If UserAccessLevel._AccessLevel.Length > 0 Then
    '                    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
    '                End If
    '            Else
    '                StrInnerQ &= mstrUserAccessFilter
    '            End If

    '        Next

    '        strQ &= StrInnerQ

    '        strQ &= "   ) AS A " & _
    '                    "WHERE   1 = 1 "
    '        'Sohail (18 Aug 2014) -- End
    '        '       'Sohail (24 Sep 2012) - [isapproved]
    '        'S.SANDEEP [ 04 JULY 2012 ] -- END



    '        Call FilterTitleAndFilterQuery()

    '        strQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(strQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim StrGrpName As String = ""
    '        Dim mdecCurrSalTotal As Decimal = 0
    '        Dim mdecIncTotal As Decimal = 0
    '        Dim mdecNewSalTotal As Decimal = 0

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("ECode")
    '            rpt_Row.Item("Column2") = dtRow.Item("EName")
    '            rpt_Row.Item("Column3") = dtRow.Item("Grade")
    '            rpt_Row.Item("Column4") = dtRow.Item("Job")
    '            rpt_Row.Item("Column5") = eZeeDate.convertDate(dtRow.Item("Last_Inc_Date").ToString).ToShortDateString
    '            rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("Current_Scale")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("Increment")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("New_Scale")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column9") = dtRow.Item("Reason")
    '            rpt_Row.Item("Column10") = dtRow.Item("GName")

    '            If StrGrpName <> dtRow.Item("GName") Then
    '                mdecCurrSalTotal = 0 : mdecIncTotal = 0 : mdecNewSalTotal = 0
    '                StrGrpName = dtRow.Item("GName")
    '            End If

    '            mdecCurrSalTotal = mdecCurrSalTotal + CDec(dtRow.Item("Current_Scale"))
    '            mdecIncTotal = mdecIncTotal + CDec(dtRow.Item("Increment"))
    '            mdecNewSalTotal = mdecNewSalTotal + CDec(dtRow.Item("New_Scale"))

    '            rpt_Row.Item("Column11") = Format(mdecCurrSalTotal, GUI.fmtCurrency)
    '            rpt_Row.Item("Column12") = Format(mdecIncTotal, GUI.fmtCurrency)
    '            rpt_Row.Item("Column13") = Format(mdecNewSalTotal, GUI.fmtCurrency)

    '            rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("Current_Scale")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column82") = Format(CDec(dtRow.Item("Increment")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column83") = Format(CDec(dtRow.Item("New_Scale")), GUI.fmtCurrency)

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptSalary_Increment_Report

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 15, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 16, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 17, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 18, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 10, "Employee Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 11, "Employee Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrade", Language.getMessage(mstrModuleName, 22, "Grade"))
    '        Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 23, "Job"))
    '        Call ReportFunction.TextChange(objRpt, "txtLastIncDate", Language.getMessage(mstrModuleName, 24, "Last Increment Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtCurrentSacale", Language.getMessage(mstrModuleName, 12, "Current Scale"))
    '        Call ReportFunction.TextChange(objRpt, "txtIncrement", Language.getMessage(mstrModuleName, 13, "Increment Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtNewScale", Language.getMessage(mstrModuleName, 14, "New Scale"))
    '        Call ReportFunction.TextChange(objRpt, "txtReason", Language.getMessage(mstrModuleName, 19, "Reason : "))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 20, "Grand Total : "))
    '        Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 21, "Sub Total : "))
    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

    '        If dsList.Tables("DataTable").Rows.Count > 0 Then 'Sohail (03 Sep 2012)

    '            Call ReportFunction.TextChange(objRpt, "txtCurr_Sal_Total", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtInc_Total", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column82)", "")), GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtNewTotal", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column83)", "")), GUI.fmtCurrency))
    '        Else
    '            Call ReportFunction.TextChange(objRpt, "txtCurr_Sal_Total", "")
    '            Call ReportFunction.TextChange(objRpt, "txtInc_Total", "")
    '            Call ReportFunction.TextChange(objRpt, "txtNewTotal", "")

    '        End If 'Sohail (03 Sep 2012)

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If menExportAction = enExportAction.ExcelExtra Then
    '            mdtTableExcel = rpt_Data.Tables(0)

    '            Dim mintColumn As Integer = 0

    '            mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 10, "Employee Code")
    '            mdtTableExcel.Columns("Column1").SetOrdinal(0)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 11, "Employee Name")
    '            mdtTableExcel.Columns("Column2").SetOrdinal(1)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 22, "Grade")
    '            mdtTableExcel.Columns("Column3").SetOrdinal(2)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 23, "Job")
    '            mdtTableExcel.Columns("Column4").SetOrdinal(3)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 24, "Last Increment Date")
    '            mdtTableExcel.Columns("Column5").SetOrdinal(4)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 12, "Current Scale")
    '            mdtTableExcel.Columns("Column81").SetOrdinal(5)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 13, "Increment Amount")
    '            mdtTableExcel.Columns("Column82").SetOrdinal(6)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column83").Caption = Language.getMessage(mstrModuleName, 14, "New Scale")
    '            mdtTableExcel.Columns("Column83").SetOrdinal(7)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 19, "Reason :")
    '            mdtTableExcel.Columns("Column9").SetOrdinal(8)
    '            mintColumn += 1

    '            If mintViewIndex > 0 Then
    '                mdtTableExcel.Columns("Column10").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
    '                mdtTableExcel.Columns("Column10").SetOrdinal(9)
    '                mintColumn += 1
    '            End If


    '            For i = mintColumn To mdtTableExcel.Columns.Count - 1
    '                mdtTableExcel.Columns.RemoveAt(mintColumn)
    '            Next

    '        End If


    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Private Function Generate_DetailReport(ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim strQ As String = ""
        Dim StrInnerQ As String = ""
        Dim strDatabaseName As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As New ArutiReport.Designer.dsArutiReport
        objDataOperation = New clsDataOperation
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtPeriodStartDate, mdtPeriodEndDate, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtPeriodEndDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtPeriodEndDate, strDatabaseName)

            strQ = "SELECT " & _
                    "    ECode AS ECode  " & _
                    "	,EName AS EName " & _
                    "	,Grade AS Grade " & _
                    "	,Job AS Job " & _
                    "	,Last_Inc_Date AS Last_Inc_Date " & _
                    "	,Current_Scale AS Current_Scale " & _
                    "	,Increment AS Increment " & _
                    "	,New_Scale AS New_Scale " & _
                    "	,reasonid AS ReasonId " & _
                    "	,Reason AS Reason " & _
                    "	,EmpId AS EmpId " & _
                    "	,incrementdate AS incrementdate " & _
                    "   ,GName AS GName " & _
                    "FROM " & _
                    "( "

            For i = 0 To marrDatabaseName.Count - 1
                strDatabaseName = marrDatabaseName(i)

                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If

                StrInnerQ &= " SELECT " & _
                             "   employeecode AS ECode  " & _
                             "  ,ISNULL(EGrd.name, '') AS Grade " & _
                             "  ,ISNULL(EJOB.job_name,'') AS Job " & _
                             "  ,CONVERT(CHAR(8),incrementdate,112) AS Last_Inc_Date " & _
                             "  ,incrementdate AS incrementdate " & _
                             "  ,ISNULL(currentscale,0) AS Current_Scale " & _
                             "  ,ISNULL(increment,0) AS Increment " & _
                             "  ,ISNULL(newscale,0) AS New_Scale " & _
                             "  ,ISNULL(prsalaryincrement_tran.reason_id, 0) AS reasonid " & _
                             "  ,ISNULL(cfcommon_master.name,'') AS Reason " & _
                             "  ,hremployee_master.employeeunkid AS EmpId "

                If mblnFirstNamethenSurname = False Then
                    StrInnerQ &= "	,ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS EName "
                Else
                    StrInnerQ &= "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName "
                End If

                If mintViewIndex > 0 Then
                    StrInnerQ &= mstrAnalysis_Fields
                Else
                    StrInnerQ &= ", '' AS GName "
                End If

                StrInnerQ &= "FROM  " & strDatabaseName & "..prsalaryincrement_tran " & _
                             "  LEFT JOIN " & strDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = prsalaryincrement_tran.reason_id " & _
                             "      AND mastertype = " & clsCommon_Master.enCommonMaster.SALINC_REASON & " " & _
                             "  JOIN " & strDatabaseName & "..hremployee_master ON prsalaryincrement_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "  LEFT JOIN " & _
                             "  ( " & _
                             "      SELECT " & _
                             "           jobunkid " & _
                             "          ,employeeunkid " & _
                             "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                             "      FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
                             "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                             "  ) AS Jobs ON Jobs.employeeunkid = " & strDatabaseName & "..hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                             "  JOIN " & strDatabaseName & "..hrjob_master EJOB ON Jobs.jobunkid = EJOB.jobunkid " & _
                             "  JOIN " & _
                             "  ( " & _
                             "      SELECT " & _
                             "           gradeunkid " & _
                             "          ,employeeunkid " & _
                             "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                             "      FROM " & strDatabaseName & "..prsalaryincrement_tran " & _
                             "      WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                             "  ) AS Grds ON Grds.employeeunkid = " & strDatabaseName & "..hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                             "  JOIN " & strDatabaseName & "..hrgrade_master AS EGrd ON Grds.gradeunkid = EGrd.gradeunkid "


                If xDateJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrInnerQ &= xUACQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xAdvanceJoinQry
                End If

                If mintViewIndex > 0 Then
                    StrInnerQ &= mstrAnalysis_Join & " "
                End If

                StrInnerQ &= "WHERE isfromemployee = 0 AND currentscale <> newscale " & _
                             "  AND prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                             "  AND periodunkid IN (" & mstrPeriodIDs & ") "
                '            
                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrInnerQ &= " AND " & mstrAdvance_Filter
                End If

                If mblnIncludeInactiveEmp = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrInnerQ &= xDateFilterQry & " "
                    End If
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrInnerQ &= " AND " & xUACFiltrQry & " "
                End If

            Next

            strQ &= StrInnerQ

            strQ &= "   ) AS A " & _
                        "WHERE   1 = 1 "

            Call FilterTitleAndFilterQuery()

            strQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim StrGrpName As String = ""
            Dim mdecCurrSalTotal As Decimal = 0
            Dim mdecIncTotal As Decimal = 0
            Dim mdecNewSalTotal As Decimal = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("ECode")
                rpt_Row.Item("Column2") = dtRow.Item("EName")
                rpt_Row.Item("Column3") = dtRow.Item("Grade")
                rpt_Row.Item("Column4") = dtRow.Item("Job")
                rpt_Row.Item("Column5") = eZeeDate.convertDate(dtRow.Item("Last_Inc_Date").ToString).ToShortDateString
                rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("Current_Scale")), GUI.fmtCurrency)
                rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("Increment")), GUI.fmtCurrency)
                rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("New_Scale")), GUI.fmtCurrency)
                rpt_Row.Item("Column9") = dtRow.Item("Reason")
                rpt_Row.Item("Column10") = dtRow.Item("GName")

                If StrGrpName <> dtRow.Item("GName") Then
                    mdecCurrSalTotal = 0 : mdecIncTotal = 0 : mdecNewSalTotal = 0
                    StrGrpName = dtRow.Item("GName")
                End If

                mdecCurrSalTotal = mdecCurrSalTotal + CDec(dtRow.Item("Current_Scale"))
                mdecIncTotal = mdecIncTotal + CDec(dtRow.Item("Increment"))
                mdecNewSalTotal = mdecNewSalTotal + CDec(dtRow.Item("New_Scale"))

                rpt_Row.Item("Column11") = Format(mdecCurrSalTotal, GUI.fmtCurrency)
                rpt_Row.Item("Column12") = Format(mdecIncTotal, GUI.fmtCurrency)
                rpt_Row.Item("Column13") = Format(mdecNewSalTotal, GUI.fmtCurrency)

                rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("Current_Scale")), GUI.fmtCurrency)
                rpt_Row.Item("Column82") = Format(CDec(dtRow.Item("Increment")), GUI.fmtCurrency)
                rpt_Row.Item("Column83") = Format(CDec(dtRow.Item("New_Scale")), GUI.fmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptSalary_Increment_Report

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 15, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 16, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 17, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 18, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 10, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 11, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtGrade", Language.getMessage(mstrModuleName, 22, "Grade"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 23, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtLastIncDate", Language.getMessage(mstrModuleName, 24, "Last Increment Date"))
            Call ReportFunction.TextChange(objRpt, "txtCurrentSacale", Language.getMessage(mstrModuleName, 12, "Current Scale"))
            Call ReportFunction.TextChange(objRpt, "txtIncrement", Language.getMessage(mstrModuleName, 13, "Increment Amount"))
            Call ReportFunction.TextChange(objRpt, "txtNewScale", Language.getMessage(mstrModuleName, 14, "New Scale"))
            Call ReportFunction.TextChange(objRpt, "txtReason", Language.getMessage(mstrModuleName, 19, "Reason : "))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 20, "Grand Total : "))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 21, "Sub Total : "))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            If dsList.Tables("DataTable").Rows.Count > 0 Then

                Call ReportFunction.TextChange(objRpt, "txtCurr_Sal_Total", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtInc_Total", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column82)", "")), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtNewTotal", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column83)", "")), GUI.fmtCurrency))
            Else
                Call ReportFunction.TextChange(objRpt, "txtCurr_Sal_Total", "")
                Call ReportFunction.TextChange(objRpt, "txtInc_Total", "")
                Call ReportFunction.TextChange(objRpt, "txtNewTotal", "")

            End If

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables(0)

                Dim mintColumn As Integer = 0

                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 10, "Employee Code")
                mdtTableExcel.Columns("Column1").SetOrdinal(0)
                mintColumn += 1

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 11, "Employee Name")
                mdtTableExcel.Columns("Column2").SetOrdinal(1)
                mintColumn += 1

                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 22, "Grade")
                mdtTableExcel.Columns("Column3").SetOrdinal(2)
                mintColumn += 1

                mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 23, "Job")
                mdtTableExcel.Columns("Column4").SetOrdinal(3)
                mintColumn += 1

                mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 24, "Last Increment Date")
                mdtTableExcel.Columns("Column5").SetOrdinal(4)
                mintColumn += 1

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 12, "Current Scale")
                mdtTableExcel.Columns("Column81").SetOrdinal(5)
                mintColumn += 1

                mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 13, "Increment Amount")
                mdtTableExcel.Columns("Column82").SetOrdinal(6)
                mintColumn += 1

                mdtTableExcel.Columns("Column83").Caption = Language.getMessage(mstrModuleName, 14, "New Scale")
                mdtTableExcel.Columns("Column83").SetOrdinal(7)
                mintColumn += 1

                mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 19, "Reason :")
                mdtTableExcel.Columns("Column9").SetOrdinal(8)
                mintColumn += 1

                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns("Column10").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
                    mdtTableExcel.Columns("Column10").SetOrdinal(9)
                    mintColumn += 1
                End If


                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next

            End If


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Current Scale From :")
            Language.setMessage(mstrModuleName, 3, "To")
            Language.setMessage(mstrModuleName, 4, "Increment From :")
            Language.setMessage(mstrModuleName, 5, "To")
            Language.setMessage(mstrModuleName, 6, "New Scale From :")
            Language.setMessage(mstrModuleName, 7, "Order By :")
            Language.setMessage(mstrModuleName, 8, "Increment Date :")
            Language.setMessage(mstrModuleName, 9, "Increment Date")
            Language.setMessage(mstrModuleName, 10, "Employee Code")
            Language.setMessage(mstrModuleName, 11, "Employee Name")
            Language.setMessage(mstrModuleName, 12, "Current Scale")
            Language.setMessage(mstrModuleName, 13, "Increment Amount")
            Language.setMessage(mstrModuleName, 14, "New Scale")
            Language.setMessage(mstrModuleName, 15, "Prepared By :")
            Language.setMessage(mstrModuleName, 16, "Checked By :")
            Language.setMessage(mstrModuleName, 17, "Approved By :")
            Language.setMessage(mstrModuleName, 18, "Received By :")
            Language.setMessage(mstrModuleName, 19, "Reason :")
            Language.setMessage(mstrModuleName, 20, "Grand Total :")
            Language.setMessage(mstrModuleName, 21, "Sub Total :")
            Language.setMessage(mstrModuleName, 22, "Grade")
            Language.setMessage(mstrModuleName, 23, "Job")
            Language.setMessage(mstrModuleName, 24, "Last Increment Date")
            Language.setMessage(mstrModuleName, 25, "Reason :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
