'************************************************************************************************************************************
'Class Name : clsATProcessPendingLA.vb
'Purpose    :
'Date       :03/01/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsATProcessPendingLA
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsATProcessPendingLA"
    Private mstrReportId As String = enArutiReport.ATProcessPendingLAReport '43
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Property Variables "

    Private mstrEmployeeName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mintLAStateId As Integer = 0
    Private mstrLAStateName As String = String.Empty
    Private mintAuditTypeId As Integer = 0
    Private mstrAuditTypeName As String = String.Empty
    Private mintLoanSchemeId As Integer = 0
    Private mstrLoanSchemeName As String = String.Empty
    Private mstrApproverId As Integer = 0
    Private mstrApproverName As String = String.Empty
    Private mintLAStatusId As Integer = 0
    Private mstrLoanStatusName As String = String.Empty
    Private mintATUserId As Integer = 0
    Private mstrATUserName As String = String.Empty
    Private mdtAuditDateFrom As Date = Nothing
    Private mdtAuditDateTo As Date = Nothing
    Private mdtAppliedDateFrom As Date = Nothing
    Private mdtAppliedDateTo As Date = Nothing

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIncludeInactiveEmp As Boolean = True
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    'S.SANDEEP [ 17 AUG 2011 ] -- END 

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (17 Apr 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _LAStateId() As Integer
        Set(ByVal value As Integer)
            mintLAStateId = value
        End Set
    End Property

    Public WriteOnly Property _LAStateName() As String
        Set(ByVal value As String)
            mstrLAStateName = value
        End Set
    End Property

    Public WriteOnly Property _AuditTypeId() As Integer
        Set(ByVal value As Integer)
            mintAuditTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AuditTypeName() As String
        Set(ByVal value As String)
            mstrAuditTypeName = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeId() As Integer
        Set(ByVal value As Integer)
            mintLoanSchemeId = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeName() As String
        Set(ByVal value As String)
            mstrLoanSchemeName = value
        End Set
    End Property

    Public WriteOnly Property _ApproverId() As Integer
        Set(ByVal value As Integer)
            mstrApproverId = value
        End Set
    End Property

    Public WriteOnly Property _ApproverName() As String
        Set(ByVal value As String)
            mstrApproverName = value
        End Set
    End Property

    Public WriteOnly Property _LAStatusId() As Integer
        Set(ByVal value As Integer)
            mintLAStatusId = value
        End Set
    End Property

    Public WriteOnly Property _LoanStatusName() As String
        Set(ByVal value As String)
            mstrLoanStatusName = value
        End Set
    End Property

    Public WriteOnly Property _ATUserId() As Integer
        Set(ByVal value As Integer)
            mintATUserId = value
        End Set
    End Property

    Public WriteOnly Property _ATUserName() As String
        Set(ByVal value As String)
            mstrATUserName = value
        End Set
    End Property

    Public WriteOnly Property _AuditDateFrom() As Date
        Set(ByVal value As Date)
            mdtAuditDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _AuditDateTo() As Date
        Set(ByVal value As Date)
            mdtAuditDateTo = value
        End Set
    End Property

    Public WriteOnly Property _AppliedDateFrom() As Date
        Set(ByVal value As Date)
            mdtAppliedDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _AppliedDateTo() As Date
        Set(ByVal value As Date)
            mdtAppliedDateTo = value
        End Set
    End Property

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch_Name() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property
    'S.SANDEEP [ 17 AUG 2011 ] -- END

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (17 Apr 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrEmployeeName = ""
            mintEmployeeId = 0
            mintLAStateId = 0
            mstrLAStateName = ""
            mintAuditTypeId = 0
            mstrAuditTypeName = ""
            mintLoanSchemeId = 0
            mstrLoanSchemeName = ""
            mstrApproverId = 0
            mstrApproverName = ""
            mintLAStatusId = 0
            mstrLoanStatusName = ""
            mintATUserId = 0
            mstrATUserName = ""
            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            mintBranchId = -1
            mstrBranchName = ""
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Advance"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 71, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 72, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 74, "Rejected"))
            objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 83, "Assigned"))
            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))
            objDataOperation.AddParameter("@ADD", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Add"))
            objDataOperation.AddParameter("@EDIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Edit"))
            objDataOperation.AddParameter("@DELETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Delete"))
            objDataOperation.AddParameter("@FinalApproved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 44, "Final Approved"))


            objDataOperation.AddParameter("@AuditDateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditDateFrom))
            objDataOperation.AddParameter("@AuditDateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditDateTo))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 37, "Audit Date From :") & "" & mdtAuditDateFrom.Date & " " & _
                               Language.getMessage(mstrModuleName, 38, "To") & " " & mdtAuditDateTo.Date & " "

            If mdtAppliedDateFrom <> Nothing AndAlso mdtAppliedDateTo <> Nothing Then
                objDataOperation.AddParameter("@AppliedDateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAppliedDateFrom))
                objDataOperation.AddParameter("@AppliedDateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAppliedDateTo))
                Me._FilterQuery &= " AND PDate BETWEEN @AppliedDateFrom AND @AppliedDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 39, "Applied Date From :") & " " & mdtAppliedDateFrom.Date & " " & _
                                   Language.getMessage(mstrModuleName, 38, "To") & " " & mdtAppliedDateTo.Date & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND EmpId = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Employee :") & " " & mstrEmployeeName & " "
            End If
            If mintLAStateId > 0 Then
                objDataOperation.AddParameter("@LAStateId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLAStateId)
                Me._FilterQuery &= " AND ALStateId = @LAStateId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, "State :") & " " & mstrLAStateName & " "
            End If

            If mintAuditTypeId > 0 Then
                objDataOperation.AddParameter("@AuditTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditTypeId)
                Me._FilterQuery &= " AND ATypeId = @AuditTypeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "Audit Type :") & " " & mstrAuditTypeName & " "
            End If

            If mintLoanSchemeId > 0 Then
                objDataOperation.AddParameter("@LoanSchemeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeId)
                Me._FilterQuery &= " AND ALSchemeId = @LoanSchemeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Loan Scheme :") & " " & mstrLoanSchemeName & " "
            End If

            If mstrApproverId > 0 Then
                objDataOperation.AddParameter("@ApproverId", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrApproverId)
                Me._FilterQuery &= " AND ApprId = @ApproverId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "Approver :") & " " & mstrApproverName & " "
            End If

            If mintLAStatusId > 0 Then
                objDataOperation.AddParameter("@LAStatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLAStatusId)
                Me._FilterQuery &= " AND LAStatusId = @LAStatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Status :") & " " & mstrLoanStatusName & " "
            End If

            If mintATUserId > 0 Then
                objDataOperation.AddParameter("@ATUserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintATUserId)
                Me._FilterQuery &= " AND ATUserId = @ATUserId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "Audit User :") & " " & mstrATUserName & " "
            End If

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 43, "Branch :") & " " & mstrBranchName & " "
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If
            'Sohail (17 Apr 2012) -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Order By :") & " " & Me.OrderByDisplay & " "
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Me._FilterQuery &= "ORDER BY EmpId,ATypeId, " & Me.OrderByQuery
                Me._FilterQuery &= "ORDER BY EmpId, ATypeId, " & Me.OrderByQuery & ", ATTime, SortKey"
                'Nilay (10-Oct-2015) -- End

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        Try
            'objRpt = Generate_DetailReport()
            'If Not IsNothing(objRpt) Then
            '    Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, False)
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, _
                                           Optional ByVal ExportAction As enExportAction = enExportAction.None, _
                                           Optional ByVal xBaseCurrencyId As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""

        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, False)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Nilay (10-Oct-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function GetTypeList(Optional ByVal StrList As String = "List") As DataSet
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = ""
        Try

            objDataOperation = New clsDataOperation

            StrQ = "SELECT 0 AS Id, @Select AS Name " & _
                   "UNION SELECT 1 aS Id, @Loan AS Name " & _
                   "UNION SELECT 2 aS Id, @Advance AS Name "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 40, "Select"))
            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 41, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 42, "Advance"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTypeList; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ATDate", Language.getMessage(mstrModuleName, 33, "Audit Date")))
            iColumn_DetailReport.Add(New IColumn("ATTime", Language.getMessage(mstrModuleName, 34, "Audit Time")))
            iColumn_DetailReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 35, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("AType", Language.getMessage(mstrModuleName, 17, "Audit Type")))
            iColumn_DetailReport.Add(New IColumn("ATUser", Language.getMessage(mstrModuleName, 19, "Audit User")))
            iColumn_DetailReport.Add(New IColumn("LAmount", Language.getMessage(mstrModuleName, 13, "Loan Amount")))
            iColumn_DetailReport.Add(New IColumn("AAmount", Language.getMessage(mstrModuleName, 14, "Approved Amount")))


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Private Function Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As DateTime, _
                                           ByVal xPeriodEnd As DateTime, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim strAllocationJoin As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            Dim StrQ1 As String = String.Empty
            Dim StrQ2 As String = String.Empty
            Dim StrQFinal As String = String.Empty
            Dim StrQCondition As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            strAllocationJoin &= "LEFT JOIN " & _
                                 "( " & _
                                 "      SELECT " & _
                                 "           stationunkid " & _
                                 "          ,employeeunkid " & _
                                 "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                 "      FROM hremployee_transfer_tran " & _
                                 "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                 ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "

            StrQ &= "SELECT " & _
                      " EmpName AS EmpName " & _
                    "    ,EmpId AS EmpId " & _
                    "    ,Ecode AS Ecode " & _
                    "    ,AppNo AS AppNo " & _
                    "   ,ALSchemeId AS ALSchemeId " & _
                    "    ,ALScheme AS ALScheme " & _
                      ",LAmount AS LAmount " & _
                      ",AAmount AS AAmount " & _
                      ",ApprName AS ApprName " & _
                    "    ,CASE WHEN ATPL.SortKey = 9999 AND LAStatusId = 2 THEN @FinalApproved ELSE LAStatus END AS LAStatus " & _
                      ",AType AS AType " & _
                      ",ATDate AS ATDate " & _
                      ",ATTime AS ATTime " & _
                      ",ATUser AS ATUser " & _
                      ",MachineIP AS MachineIP " & _
                      ",MName AS MName " & _
                      ",ATypeId AS ATypeId " & _
                    "    ,PDate " & _
                    "    ,SortKey " & _
                    "FROM " & _
                    "( "

            StrQ1 = "  SELECT " & _
                              " ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
                    "        ,ISNULL(hremployee_master.employeeunkid,'') AS EmpId " & _
                    "        ,ISNULL(hremployee_master.employeecode,'') as Ecode " & _
                    "        ,atlnloan_process_pending_loan.application_no AS AppNo " & _
                        "   ,ISNULL(atlnloan_process_pending_loan.loanschemeunkid,0) AS ALSchemeId " & _
                    "        ,CASE WHEN atlnloan_process_pending_loan.isloan = 1 THEN @Loan " & _
                    "              WHEN atlnloan_process_pending_loan.isloan = 0 THEN @Advance " & _
                    "         END  + ' --> ' + " & _
                    "         CASE WHEN atlnloan_process_pending_loan.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') " & _
                    "              WHEN atlnloan_process_pending_loan.isloan = 0 THEN @Advance " & _
                    "         END AS ALScheme " & _
                    "        ,loan_amount AS LAmount " & _
                    "        ,approved_amount AS AAmount " & _
                    "        ,CASE WHEN loan_statusunkid=1 THEN '' " & _
                        "         WHEN loan_statusunkid <> 1 THEN ISNULL(lnapproverlevel_master.lnlevelname,'') + ' - ' + #APPROVER_NAME# " & _
                    "         END AS ApprName " & _
                              ",CASE WHEN loan_statusunkid = 1 THEN @Pending " & _
                              "      WHEN loan_statusunkid = 2 THEN @Approved " & _
                              "      WHEN loan_statusunkid = 3 THEN @Rejected  " & _
                    "              WHEN loan_statusunkid=4 THEN @Assigned " & _
                    "         END AS LAStatus " & _
                              ",CASE WHEN audittype = 1 THEN @ADD " & _
                              "      WHEN audittype = 2 THEN @EDIT " & _
                    "              WHEN audittype=3 THEN @DELETE " & _
                    "         END AS AType " & _
                              ",CONVERT(CHAR(8), auditdatetime, 112) AS ATDate " & _
                              ",CONVERT(CHAR(8), auditdatetime, 108) AS ATTime " & _
                    "        ,hrmsConfiguration..cfuser_master.username AS ATUser " & _
                    "        ,ip AS MachineIP " & _
                    "        ,machine_name AS MName " & _
                    "        ,CASE WHEN isloan = 1 THEN 1 " & _
                    "           WHEN isloan = 0 THEN 2 END AS ALStateId " & _
                              ",audittype AS ATypeId " & _
                    "        ,CONVERT(char(8),atlnloan_process_pending_loan.application_date,112) AS PDate " & _
                    "        ,CASE WHEN approverunkid <= 0 AND loan_statusunkid=1 THEN 1 " & _
                    "              WHEN approverunkid > 0 AND loan_statusunkid <> 1 THEN 9999 " & _
                    "         END AS SortKey " & _
                    "        ,loan_statusunkid AS LAStatusId " & _
                    "        ,ISNULL(lnloanapprover_master.lnapproverunkid, 0) AS ApprId  " & _
                    ",hrmsConfiguration..cfuser_master.userunkid AS ATUserId " & _
                         "FROM atlnloan_process_pending_loan " & _
                    "        LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atlnloan_process_pending_loan.audituserunkid" & _
                    "        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = atlnloan_process_pending_loan.employeeunkid " & _
                    "        LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = atlnloan_process_pending_loan.loanschemeunkid " & _
                    "        LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = atlnloan_process_pending_loan.approverunkid " & _
                        "   LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                        "   #APPROVER_JOIN# " & _
                        "   #ALLOCATION_JOIN# "

            StrQFinal = StrQ1

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ1 &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ1 &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ1 &= xAdvanceJoinQry
            End If

            StrQCondition &= "   WHERE CONVERT(CHAR(8), atlnloan_process_pending_loan.auditdatetime, 112) BETWEEN @AuditDateFrom AND @AuditDateTo "

            StrQCondition &= " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQCondition &= " AND " & mstrAdvance_Filter
            End If

            If mintBranchId > 0 Then
                StrQCondition &= " AND Alloc.stationunkid = " & mintBranchId
            End If

            StrQ1 &= StrQCondition

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ1 &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ1 &= " AND " & xUACFiltrQry
            End If

            StrQ1 = StrQ1.Replace("#APPROVER_NAME#", "ISNULL(LAApprover.firstname, '') + ' ' + ISNULL(LAApprover.othername, '') + ' ' + ISNULL(LAApprover.surname, '') ")
            StrQ1 = StrQ1.Replace("#APPROVER_JOIN#", "LEFT JOIN hremployee_master AS LAApprover ON LAApprover.employeeunkid = lnloanapprover_master.approverempunkid AND LAApprover.isapproved = 1 ")
            StrQ1 = StrQ1.Replace("#ALLOCATION_JOIN#", strAllocationJoin)
            StrQ1 = StrQ1.Replace("#EXT_APPROVER#", "0")

            StrQ &= StrQ1

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows

                StrQ1 = StrQFinal

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    StrQ1 = StrQ1.Replace("#APPROVER_NAME#", "ISNULL(UM.username,'') ")
                    StrQ1 = StrQ1.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid  = lnloanapprover_master.approverempunkid ")
                    StrQ1 = StrQ1.Replace("#ALLOCATION_JOIN#", strAllocationJoin)
                    StrQ1 &= StrQCondition
                Else
                    StrQ1 = StrQ1.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(LAApprover.firstname, '') + ' ' + ISNULL(LAApprover.othername, '') + ' ' + ISNULL(LAApprover.surname, '') = ' ' THEN ISNULL(UM.username,'') " & _
                                                                 "ELSE ISNULL(LAApprover.firstname, '') + ' ' + ISNULL(LAApprover.othername, '') + ' ' + ISNULL(LAApprover.surname, '') END ")
                    StrQ1 = StrQ1.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid = lnloanapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master AS LAApprover ON LAApprover.employeeunkid = UM.employeeunkid ")
                    StrQ1 = StrQ1.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                    StrQ1 = StrQ1.Replace("#ALLOCATION_JOIN#", strAllocationJoin)

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ1 &= xDateJoinQry
                    End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ1 &= xUACQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ1 &= xAdvanceJoinQry
                    End If

                    StrQ1 &= StrQCondition

                    If mblnIncludeInactiveEmp = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQ1 &= xDateFilterQry
                        End If
                    End If

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ1 &= " AND " & xUACFiltrQry
                    End If

                End If

                StrQ1 = StrQ1.Replace("#EXT_APPROVER#", "1")

                StrQ1 &= " AND UM.companyunkid = " & dRow("companyunkid")

                StrQ &= " UNION " & StrQ1
            Next

            StrQFinal = ""
            StrQCondition = ""

            StrQ &= "UNION "

            StrQ2 = "  SELECT DISTINCT " & _
                    "         ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                    "        ,ISNULL(hremployee_master.employeeunkid,'') AS EmpId " & _
                    "        ,ISNULL(hremployee_master.employeecode,'') as Ecode " & _
                    "        ,atlnloan_process_pending_loan.application_no AS AppNo " & _
                        "   ,ISNULL(atlnloan_process_pending_loan.loanschemeunkid,0) AS ALSchemeId " & _
                    "        ,CASE WHEN atlnloan_process_pending_loan.isloan = 1 THEN @Loan " & _
                    "              WHEN atlnloan_process_pending_loan.isloan = 0 THEN @Advance END + ' --> ' + " & _
                    "         CASE WHEN atlnloan_process_pending_loan.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') " & _
                    "              WHEN atlnloan_process_pending_loan.isloan = 0 THEN @Advance END AS ALScheme " & _
                    "        ,ISNULL(atlnloan_process_pending_loan.loan_amount, 0) AS LAmount " & _
                    "        ,CASE WHEN atlnloanapproval_process_tran.statusunkid = 1 THEN 0 " & _
                    "              ELSE atlnloanapproval_process_tran.loan_amount " & _
                    "         END AS AAmount " & _
                        "   ,CASE WHEN atlnloanapproval_process_tran.statusunkid = 1 AND atlnloan_process_pending_loan.loan_statusunkid = 1 THEN ISNULL(lnapproverlevel_master.lnlevelname,'') + ' - ' + #APPROVER_NAME# " & _
                        "         WHEN atlnloan_process_pending_loan.loan_statusunkid = 1 AND atlnloanapproval_process_tran.statusunkid = 2 THEN ISNULL(lnapproverlevel_master.lnlevelname,'') + ' - ' + #APPROVER_NAME# " & _
                    "         END AS ApprName " & _
                    "        ,CASE WHEN atlnloanapproval_process_tran.statusunkid=1 THEN @Pending " & _
                    "              WHEN atlnloanapproval_process_tran.statusunkid=2 THEN @Approved " & _
                    "              WHEN atlnloanapproval_process_tran.statusunkid=3 THEN @Rejected " & _
                    "              WHEN atlnloanapproval_process_tran.statusunkid=4 THEN @Assigned " & _
                    "         END  AS LAStatus " & _
                    "        ,CASE WHEN atlnloanapproval_process_tran.audittype = 1 THEN @ADD " & _
                    "              WHEN atlnloanapproval_process_tran.audittype = 2 THEN @EDIT " & _
                    "              WHEN atlnloanapproval_process_tran.audittype = 3 THEN @DELETE " & _
                    "         END AS AType " & _
                    "        ,CONVERT(CHAR(8), atlnloanapproval_process_tran.auditdatetime, 112) AS ATDate " & _
                    "        ,CONVERT(CHAR(8), atlnloanapproval_process_tran.auditdatetime, 108) AS ATTime " & _
                    "        ,ISNULL(hrmsConfiguration..cfuser_master.username, '') AS ATUser " & _
                    "        ,ISNULL(atlnloanapproval_process_tran.ip, '') AS MachineIP " & _
                    "        ,ISNULL(atlnloanapproval_process_tran.machine_name, '') AS MName " & _
                    "        ,CASE WHEN isloan = 1 THEN 1 " & _
                    "           WHEN isloan = 0 THEN 2 END AS ALStateId " & _
                    "        ,atlnloanapproval_process_tran.audittype AS ATypeId " & _
                    "        ,CONVERT(char(8),atlnloan_process_pending_loan.application_date,112) AS PDate " & _
                    "        ,2 AS SortKey " & _
                    "        ,loan_statusunkid AS LAStatusId " & _
                    "        ,ISNULL(lnloanapprover_master.lnapproverunkid, 0) AS ApprId  " & _
                    ",hrmsConfiguration..cfuser_master.userunkid AS ATUserId " & _
                    "    FROM atlnloanapproval_process_tran " & _
                    "        LEFT JOIN atlnloan_process_pending_loan ON atlnloan_process_pending_loan.processpendingloanunkid = atlnloanapproval_process_tran.processpendingloanunkid " & _
                    "   LEFT JOIN lnloanapprover_master ON lnloanapprover_master.approverempunkid = atlnloanapproval_process_tran.approverempunkid " & _
                    "   LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                    "   #APPROVER_JOIN# " & _
                    "        LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = atlnloan_process_pending_loan.loanschemeunkid " & _
                    "        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = atlnloan_process_pending_loan.employeeunkid " & _
                        "   LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atlnloanapproval_process_tran.audituserunkid " & _
                        "   #ALLOCATION_JOIN# "

            StrQFinal &= StrQ2

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ2 &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ2 &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ2 &= xAdvanceJoinQry
            End If

            StrQCondition &= "    WHERE CONVERT(CHAR(8), atlnloanapproval_process_tran.auditdatetime, 112) BETWEEN @AuditDateFrom AND @AuditDateTo "

            StrQCondition &= " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            StrQCondition &= " AND atlnloanapproval_process_tran.priority = lnapproverlevel_master.priority "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQCondition &= " AND " & mstrAdvance_Filter
            End If

            If mintBranchId > 0 Then
                StrQCondition &= " AND Alloc.stationunkid = " & mintBranchId
            End If

            StrQ2 &= StrQCondition

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ2 &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ2 &= " AND " & xUACFiltrQry
            End If

            StrQ2 = StrQ2.Replace("#APPROVER_NAME#", "ISNULL(LAApprover.firstname,'') + ' ' + ISNULL(LAApprover.surname,'') ")
            StrQ2 = StrQ2.Replace("#APPROVER_JOIN#", "LEFT JOIN hremployee_master AS LAApprover ON LAApprover.employeeunkid = atlnloanapproval_process_tran.approverempunkid ")
            StrQ2 = StrQ2.Replace("#ALLOCATION_JOIN#", strAllocationJoin)
            StrQ2 = StrQ2.Replace("#EXT_APPROVER#", "0")

            StrQ &= StrQ2

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows

                StrQ2 = StrQFinal

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    StrQ2 = StrQ2.Replace("#APPROVER_NAME#", "ISNULL(UM.username,'') ")
                    StrQ2 = StrQ2.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid  = lnloanapprover_master.approverempunkid ")
                    StrQ2 = StrQ2.Replace("#ALLOCATION_JOIN#", strAllocationJoin)
                    StrQ2 &= StrQCondition
                Else
                    StrQ2 = StrQ2.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(LAApprover.firstname, '') + ' ' + ISNULL(LAApprover.othername, '') + ' ' + ISNULL(LAApprover.surname, '') = ' ' THEN ISNULL(UM.username,'') " & _
                                                                 "ELSE ISNULL(LAApprover.firstname, '') + ' ' + ISNULL(LAApprover.othername, '') + ' ' + ISNULL(LAApprover.surname, '') END ")
                    StrQ2 = StrQ2.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid = lnloanapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master AS LAApprover ON LAApprover.employeeunkid = UM.employeeunkid ")
                    StrQ2 = StrQ2.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                    StrQ2 = StrQ2.Replace("#ALLOCATION_JOIN#", strAllocationJoin)

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ2 &= xDateJoinQry
                    End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ2 &= xUACQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ2 &= xAdvanceJoinQry
                    End If

                    StrQ2 &= StrQCondition

                    If mblnIncludeInactiveEmp = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQ2 &= xDateFilterQry
                        End If
                    End If

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ2 &= " AND " & xUACFiltrQry
                    End If

                End If

                StrQ2 = StrQ2.Replace("#EXT_APPROVER#", "1")

                StrQ2 &= " AND UM.companyunkid = " & dRow("companyunkid")

                StrQ &= " UNION " & StrQ2
            Next

            StrQ &= ") AS ATPL " & _
                    "WHERE 1 = 1 " & _
                    "AND ATPL.ApprName IS NOT NULL "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("EmpName")
                rpt_Rows.Item("Column2") = dtRow.Item("ALScheme")
                rpt_Rows.Item("Column3") = Format(CDec(dtRow.Item("LAmount")), GUI.fmtCurrency)
                rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("AAmount")), GUI.fmtCurrency)
                rpt_Rows.Item("Column5") = dtRow.Item("ApprName")
                rpt_Rows.Item("Column6") = dtRow.Item("LAStatus")
                rpt_Rows.Item("Column7") = dtRow.Item("AType")
                rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("ATDate").ToString).ToShortDateString & vbCrLf & dtRow.Item("ATTime")
                rpt_Rows.Item("Column9") = dtRow.Item("ATUser")
                rpt_Rows.Item("Column10") = dtRow.Item("MachineIP")
                rpt_Rows.Item("Column11") = dtRow.Item("MName")
                rpt_Rows.Item("Column12") = dtRow.Item("Ecode")
                rpt_Rows.Item("Column13") = dtRow.Item("EmpId")
                rpt_Rows.Item("Column14") = eZeeDate.convertDate(dtRow.Item("PDate").ToString).ToShortDateString
                'Nilay (28-Aug-2015) -- Start
                rpt_Rows.Item("Column15") = dtRow.Item("AppNo")
                'Nilay (28-Aug-2015) -- End

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptATProcessPendingLoan


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 6, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 7, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 8, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 9, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 10, "Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 11, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtLoanAdvance", Language.getMessage(mstrModuleName, 12, "Loan/Advance"))
            Call ReportFunction.TextChange(objRpt, "txtLoanAmount", Language.getMessage(mstrModuleName, 13, "Loan Amount"))
            Call ReportFunction.TextChange(objRpt, "txtApprvAmount", Language.getMessage(mstrModuleName, 14, "Approved Amt"))
            Call ReportFunction.TextChange(objRpt, "txtApprvName", Language.getMessage(mstrModuleName, 15, "Approver Name"))
            Call ReportFunction.TextChange(objRpt, "txtLAStatus", Language.getMessage(mstrModuleName, 16, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtAuditType", Language.getMessage(mstrModuleName, 17, "Audit Type"))
            Call ReportFunction.TextChange(objRpt, "txtATDateTime", Language.getMessage(mstrModuleName, 18, "Audit Date & Time"))
            Call ReportFunction.TextChange(objRpt, "txtATUser", Language.getMessage(mstrModuleName, 19, "Audit User"))
            Call ReportFunction.TextChange(objRpt, "txtIPAddress", Language.getMessage(mstrModuleName, 20, "IP Address"))
            Call ReportFunction.TextChange(objRpt, "txtMachine", Language.getMessage(mstrModuleName, 21, "Machine Name"))
            Call ReportFunction.TextChange(objRpt, "txtApplicationDate", Language.getMessage(mstrModuleName, 36, "Applied Date"))
            Call ReportFunction.TextChange(objRpt, "txtAppNo", Language.getMessage(mstrModuleName, 44, "Application No. "))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 22, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 23, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 24, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Private Function Generate_DetailReport(ByVal xDatabaseName As String, _
    '                                       ByVal xUserUnkid As Integer, _
    '                                       ByVal xYearUnkid As Integer, _
    '                                       ByVal xCompanyUnkid As Integer, _
    '                                       ByVal xPeriodStart As DateTime, _
    '                                       ByVal xPeriodEnd As DateTime, _
    '                                       ByVal xUserModeSetting As String, _
    '                                       ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim strAllocationJoin As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport

    '    Try
    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
    '        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

    '        objDataOperation = New clsDataOperation

    '        strAllocationJoin &= "LEFT JOIN " & _
    '                             "( " & _
    '                             "      SELECT " & _
    '                             "           stationunkid " & _
    '                             "          ,employeeunkid " & _
    '                             "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                             "      FROM hremployee_transfer_tran " & _
    '                             "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                             ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "

    '        StrQ &= "SELECT " & _
    '                "    EmpName AS EmpName " & _
    '                "   ,EmpId AS EmpId " & _
    '                "   ,Ecode AS Ecode " & _
    '                "   ,AppNo AS AppNo " & _
    '                "   ,ALScheme AS ALScheme " & _
    '                "   ,LAmount AS LAmount " & _
    '                "   ,AAmount AS AAmount " & _
    '                "   ,ApprName AS ApprName " & _
    '                "   ,CASE WHEN ATPL.SortKey = 9999 AND loan_statusunkid = 2 THEN @FinalApproved ELSE LAStatus END AS LAStatus " & _
    '                "   ,AType AS AType " & _
    '                "   ,ATDate AS ATDate " & _
    '                "   ,ATTime AS ATTime " & _
    '                "   ,ATUser AS ATUser " & _
    '                "   ,MachineIP AS MachineIP " & _
    '                "   ,MName AS MName " & _
    '                "   ,ATypeId AS ATypeId " & _
    '                "   ,PDate " & _
    '                "   ,SortKey " & _
    '                "FROM " & _
    '                "( " & _
    '                    "SELECT " & _
    '                    "   ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                    "   ,ISNULL(hremployee_master.employeeunkid,'') AS EmpId " & _
    '                    "   ,ISNULL(hremployee_master.employeecode,'') as Ecode " & _
    '                    "   ,atlnloan_process_pending_loan.application_no AS AppNo " & _
    '                    "   ,CASE WHEN atlnloan_process_pending_loan.isloan = 1 THEN @Loan " & _
    '                    "         WHEN atlnloan_process_pending_loan.isloan = 0 THEN @Advance " & _
    '                    "    END  + ' --> ' + " & _
    '                    "    CASE WHEN atlnloan_process_pending_loan.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') " & _
    '                    "         WHEN atlnloan_process_pending_loan.isloan = 0 THEN @Advance " & _
    '                    "    END AS ALScheme " & _
    '                    "   ,loan_amount AS LAmount " & _
    '                    "   ,approved_amount AS AAmount " & _
    '                    "   ,CASE WHEN loan_statusunkid=1 THEN '' " & _
    '                    "         WHEN loan_statusunkid <> 1 THEN ISNULL(LAApprover.firstname, '') + ' ' + ISNULL(LAApprover.othername, '') + ' ' + ISNULL(LAApprover.surname, '') " & _
    '                    "    END AS ApprName " & _
    '                    "   ,CASE WHEN loan_statusunkid = 1 THEN @Pending " & _
    '                    "      WHEN loan_statusunkid = 2 THEN @Approved " & _
    '                    "      WHEN loan_statusunkid = 3 THEN @Rejected  " & _
    '                    "      WHEN loan_statusunkid=4 THEN @Assigned " & _
    '                    "    END AS LAStatus " & _
    '                    "   ,CASE WHEN audittype = 1 THEN @ADD " & _
    '                    "      WHEN audittype = 2 THEN @EDIT " & _
    '                    "      WHEN audittype=3 THEN @DELETE " & _
    '                    "    END AS AType " & _
    '                    "   ,CONVERT(CHAR(8), auditdatetime, 112) AS ATDate " & _
    '                    "   ,CONVERT(CHAR(8), auditdatetime, 108) AS ATTime " & _
    '                    "   ,hrmsConfiguration..cfuser_master.username AS ATUser " & _
    '                    "   ,ip AS MachineIP " & _
    '                    "   ,machine_name AS MName " & _
    '                    "   ,audittype AS ATypeId " & _
    '                    "   ,CONVERT(char(8),atlnloan_process_pending_loan.application_date,112) AS PDate " & _
    '                    "   ,CASE WHEN approverunkid <= 0 AND loan_statusunkid=1 THEN 1 " & _
    '                    "         WHEN approverunkid > 0 AND loan_statusunkid <> 1 THEN 9999 " & _
    '                    "    END AS SortKey " & _
    '                    "   ,loan_statusunkid " & _
    '                    "FROM atlnloan_process_pending_loan " & _
    '                    "   LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atlnloan_process_pending_loan.audituserunkid" & _
    '                    "   LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = atlnloan_process_pending_loan.employeeunkid " & _
    '                    "   LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = atlnloan_process_pending_loan.loanschemeunkid " & _
    '                    "   LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = atlnloan_process_pending_loan.approverunkid " & _
    '                    "   LEFT JOIN hremployee_master AS LAApprover ON LAApprover.employeeunkid = lnloanapprover_master.approverempunkid "

    '        StrQ &= strAllocationJoin

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= xDateJoinQry
    '        End If

    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= xAdvanceJoinQry
    '        End If

    '        StrQ &= "   WHERE CONVERT(CHAR(8), atlnloan_process_pending_loan.auditdatetime, 112) BETWEEN @AuditDateFrom AND @AuditDateTo "

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If

    '        If mblnIncludeInactiveEmp = False Then
    '            If xDateFilterQry.Trim.Length > 0 Then
    '                StrQ &= xDateFilterQry
    '            End If
    '        End If

    '        If mintBranchId > 0 Then
    '            StrQ &= " AND Alloc.stationunkid = " & mintBranchId
    '        End If

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        StrQ &= "UNION " & _
    '                "    SELECT DISTINCT " & _
    '                "         ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                "        ,ISNULL(hremployee_master.employeeunkid,'') AS EmpId " & _
    '                "        ,ISNULL(hremployee_master.employeecode,'') as Ecode " & _
    '                "        ,atlnloan_process_pending_loan.application_no AS AppNo " & _
    '                "        ,CASE WHEN atlnloan_process_pending_loan.isloan = 1 THEN @Loan " & _
    '                "              WHEN atlnloan_process_pending_loan.isloan = 0 THEN @Advance END + ' --> ' + " & _
    '                "         CASE WHEN atlnloan_process_pending_loan.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') " & _
    '                "              WHEN atlnloan_process_pending_loan.isloan = 0 THEN @Advance END AS ALScheme " & _
    '                "        ,ISNULL(atlnloan_process_pending_loan.loan_amount, 0) AS LAmount " & _
    '                "        ,CASE WHEN atlnloanapproval_process_tran.statusunkid = 1 THEN 0 " & _
    '                "              ELSE atlnloanapproval_process_tran.loan_amount " & _
    '                "         END AS AAmount " & _
    '                "        ,CASE WHEN atlnloanapproval_process_tran.statusunkid = 1 AND atlnloan_process_pending_loan.loan_statusunkid = 1 THEN LAApprover.firstname +' '+ LAApprover.surname " & _
    '                "              WHEN atlnloan_process_pending_loan.loan_statusunkid = 1 AND atlnloanapproval_process_tran.statusunkid = 2 THEN LAApprover.firstname +' '+ LAApprover.surname " & _
    '                "         END AS ApprName " & _
    '                "        ,CASE WHEN atlnloanapproval_process_tran.statusunkid=1 THEN @Pending " & _
    '                "              WHEN atlnloanapproval_process_tran.statusunkid=2 THEN @Approved " & _
    '                "              WHEN atlnloanapproval_process_tran.statusunkid=3 THEN @Rejected " & _
    '                "              WHEN atlnloanapproval_process_tran.statusunkid=4 THEN @Assigned " & _
    '                "         END  AS LAStatus " & _
    '                "        ,CASE WHEN atlnloanapproval_process_tran.audittype = 1 THEN @ADD " & _
    '                "              WHEN atlnloanapproval_process_tran.audittype = 2 THEN @EDIT " & _
    '                "              WHEN atlnloanapproval_process_tran.audittype = 3 THEN @DELETE " & _
    '                "         END AS AType " & _
    '                "        ,CONVERT(CHAR(8), atlnloanapproval_process_tran.auditdatetime, 112) AS ATDate " & _
    '                "        ,CONVERT(CHAR(8), atlnloanapproval_process_tran.auditdatetime, 108) AS ATTime " & _
    '                "        ,ISNULL(hrmsConfiguration..cfuser_master.username, '') AS ATUser " & _
    '                "        ,ISNULL(atlnloanapproval_process_tran.ip, '') AS MachineIP " & _
    '                "        ,ISNULL(atlnloanapproval_process_tran.machine_name, '') AS MName " & _
    '                "        ,atlnloanapproval_process_tran.audittype AS ATypeId " & _
    '                "        ,CONVERT(char(8),atlnloan_process_pending_loan.application_date,112) AS PDate " & _
    '                "        ,2 AS SortKey " & _
    '                "        ,loan_statusunkid " & _
    '                "    FROM atlnloanapproval_process_tran " & _
    '                "        LEFT JOIN atlnloan_process_pending_loan ON atlnloan_process_pending_loan.processpendingloanunkid = atlnloanapproval_process_tran.processpendingloanunkid " & _
    '                "        LEFT JOIN hremployee_master AS LAApprover ON LAApprover.employeeunkid = atlnloanapproval_process_tran.approverempunkid " & _
    '                "        LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = atlnloan_process_pending_loan.loanschemeunkid " & _
    '                "        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = atlnloan_process_pending_loan.employeeunkid " & _
    '                "        LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atlnloanapproval_process_tran.audituserunkid "

    '        StrQ &= strAllocationJoin

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= xDateJoinQry
    '        End If

    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= xAdvanceJoinQry
    '        End If

    '        StrQ &= "    WHERE CONVERT(CHAR(8), atlnloanapproval_process_tran.auditdatetime, 112) BETWEEN @AuditDateFrom AND @AuditDateTo "

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If

    '        If mblnIncludeInactiveEmp = False Then
    '            If xDateFilterQry.Trim.Length > 0 Then
    '                StrQ &= xDateFilterQry
    '            End If
    '        End If

    '        If mintBranchId > 0 Then
    '            StrQ &= " AND Alloc.stationunkid = " & mintBranchId
    '        End If

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        StrQ &= ") AS ATPL " & _
    '                "WHERE 1 = 1 " & _
    '                "AND ATPL.ApprName IS NOT NULL "

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Rows.Item("Column1") = dtRow.Item("EmpName")
    '            rpt_Rows.Item("Column2") = dtRow.Item("ALScheme")
    '            rpt_Rows.Item("Column3") = Format(CDec(dtRow.Item("LAmount")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("AAmount")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column5") = dtRow.Item("ApprName")
    '            rpt_Rows.Item("Column6") = dtRow.Item("LAStatus")
    '            rpt_Rows.Item("Column7") = dtRow.Item("AType")
    '            rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("ATDate").ToString).ToShortDateString & vbCrLf & dtRow.Item("ATTime")
    '            rpt_Rows.Item("Column9") = dtRow.Item("ATUser")
    '            rpt_Rows.Item("Column10") = dtRow.Item("MachineIP")
    '            rpt_Rows.Item("Column11") = dtRow.Item("MName")
    '            rpt_Rows.Item("Column12") = dtRow.Item("Ecode")
    '            rpt_Rows.Item("Column13") = dtRow.Item("EmpId")
    '            rpt_Rows.Item("Column14") = eZeeDate.convertDate(dtRow.Item("PDate").ToString).ToShortDateString
    '            Nilay (28-Aug-2015) -- Start
    '            rpt_Rows.Item("Column15") = dtRow.Item("AppNo")
    '            Nilay (28-Aug-2015) -- End

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptATProcessPendingLoan


    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 6, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 7, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 8, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 9, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If


    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 10, "Code :"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 11, "Employee :"))
    '        Call ReportFunction.TextChange(objRpt, "txtLoanAdvance", Language.getMessage(mstrModuleName, 12, "Loan/Advance"))
    '        Call ReportFunction.TextChange(objRpt, "txtLoanAmount", Language.getMessage(mstrModuleName, 13, "Loan Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtApprvAmount", Language.getMessage(mstrModuleName, 14, "Approved Amt"))
    '        Call ReportFunction.TextChange(objRpt, "txtApprvName", Language.getMessage(mstrModuleName, 15, "Approver Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtLAStatus", Language.getMessage(mstrModuleName, 16, "Status"))
    '        Call ReportFunction.TextChange(objRpt, "txtAuditType", Language.getMessage(mstrModuleName, 17, "Audit Type"))
    '        Call ReportFunction.TextChange(objRpt, "txtATDateTime", Language.getMessage(mstrModuleName, 18, "Audit Date & Time"))
    '        Call ReportFunction.TextChange(objRpt, "txtATUser", Language.getMessage(mstrModuleName, 19, "Audit User"))
    '        Call ReportFunction.TextChange(objRpt, "txtIPAddress", Language.getMessage(mstrModuleName, 20, "IP Address"))
    '        Call ReportFunction.TextChange(objRpt, "txtMachine", Language.getMessage(mstrModuleName, 21, "Machine Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtApplicationDate", Language.getMessage(mstrModuleName, 36, "Applied Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtAppNo", Language.getMessage(mstrModuleName, 44, "Application No. "))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 22, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 23, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 24, "Page :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
    'Nilay (01-Mar-2016) -- End



    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation


    '        'S.SANDEEP [ 24 JUNE 2011 ] -- START
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'StrQ = "SELECT " & _
    '        '          " EmpName AS EmpName " & _
    '        '          ",ALState + ' --> ' + ALScheme AS ALScheme " & _
    '        '          ",LAmount AS LAmount " & _
    '        '          ",AAmount AS AAmount " & _
    '        '          ",ApprName AS ApprName " & _
    '        '          ",LAStatus AS LAStatus " & _
    '        '          ",AType AS AType " & _
    '        '          ",ATDate AS ATDate " & _
    '        '          ",ATTime AS ATTime " & _
    '        '          ",ATUser AS ATUser " & _
    '        '          ",MachineIP AS MachineIP " & _
    '        '          ",MName AS MName " & _
    '        '          ",Ecode AS Ecode " & _
    '        '          ",EmpId AS EmpId " & _
    '        '          ",ATypeId AS ATypeId " & _
    '        '          ",PDate AS PDate " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '             "SELECT " & _
    '        '                  " ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '        '                  ",CASE WHEN isloan = 1 THEN @Loan " & _
    '        '                  "      WHEN isloan = 0 THEN @Advance END AS ALState " & _
    '        '                  ",CASE WHEN isloan = 1 THEN ISNULL(lnloan_scheme_master.name, '') " & _
    '        '                  "      WHEN isloan = 0 THEN @Advance END AS ALScheme " & _
    '        '                  ",ISNULL(loan_amount, 0) AS LAmount " & _
    '        '                  ",CASE WHEN audittype IN ( 1, 3 ) THEN 0 " & _
    '        '                  "      WHEN audittype = 2 AND loan_statusunkid IN ( 1, 3 ) THEN 0 " & _
    '        '                  " ELSE ISNULL(approved_amount, 0) END AS AAmount " & _
    '        '                  ",ISNULL(LAApprover.firstname, '') + ' '+ ISNULL(LAApprover.othername, '') + ' '+ ISNULL(LAApprover.surname, '') AS ApprName " & _
    '        '                  ",CASE WHEN loan_statusunkid = 1 THEN @Pending " & _
    '        '                  "      WHEN loan_statusunkid = 2 THEN @Approved " & _
    '        '                  "      WHEN loan_statusunkid = 3 THEN @Rejected  " & _
    '        '                  "      WHEN loan_statusunkid = 3 THEN @Assigned END AS LAStatus " & _
    '        '                  ",CASE WHEN audittype = 1 THEN @ADD " & _
    '        '                  "      WHEN audittype = 2 THEN @EDIT " & _
    '        '                  "      WHEN audittype = 3 THEN @DELETE END AS AType " & _
    '        '                  ",CONVERT(CHAR(8), auditdatetime, 112) AS ATDate " & _
    '        '                  ",CONVERT(CHAR(8), auditdatetime, 108) AS ATTime " & _
    '        '                  ",ISNULL(hrmsConfiguration..cfuser_master.username, '') AS ATUser " & _
    '        '                  ",ISNULL(ip, '') AS MachineIP " & _
    '        '                  ",ISNULL(machine_name, '') AS MName " & _
    '        '                  ",hremployee_master.employeeunkid AS EmpId " & _
    '        '                  ",CASE WHEN isloan = 1 THEN 1 " & _
    '        '                  "      WHEN isloan = 0 THEN 2 END AS ALStateId " & _
    '        '                  ",audittype AS ATypeId " & _
    '        '                  ",ISNULL(lnloan_scheme_master.loanschemeunkid, 0) AS ALSchemeId " & _
    '        '                  ",ISNULL(LAApprover.employeeunkid, 0) AS ApprId " & _
    '        '                  ",loan_statusunkid AS LAStatusId " & _
    '        '                  ",hrmsConfiguration..cfuser_master.userunkid AS ATUserId " & _
    '        '                  ",ISNULL(hremployee_master.employeecode,'') AS Ecode " & _
    '        '                  ",CONVERT(CHAR(8), application_date, 112) AS PDate " & _
    '        '             "FROM atlnloan_process_pending_loan " & _
    '        '                  "LEFT JOIN hrmsConfiguration..cfuser_master ON atlnloan_process_pending_loan.audituserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
    '        '                  "LEFT JOIN lnloan_approver_tran ON atlnloan_process_pending_loan.approverunkid = lnloan_approver_tran.approverunkid " & _
    '        '                  "LEFT JOIN hremployee_master AS LAApprover ON lnloan_approver_tran.approverunkid = LAApprover.employeeunkid " & _
    '        '                  "LEFT JOIN lnloan_scheme_master ON atlnloan_process_pending_loan.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
    '        '                  "JOIN hremployee_master ON atlnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
    '        '             "WHERE CONVERT(CHAR(8), auditdatetime, 112) BETWEEN @AuditDateFrom AND @AuditDateTo " & _
    '        '        ") AS ATPL " & _
    '        '        "WHERE 1 = 1 "

    '        StrQ = "SELECT " & _
    '                  " EmpName AS EmpName " & _
    '                  ",ALState + ' --> ' + ALScheme AS ALScheme " & _
    '                  ",LAmount AS LAmount " & _
    '                  ",AAmount AS AAmount " & _
    '                  ",ApprName AS ApprName " & _
    '                  ",LAStatus AS LAStatus " & _
    '                  ",AType AS AType " & _
    '                  ",ATDate AS ATDate " & _
    '                  ",ATTime AS ATTime " & _
    '                  ",ATUser AS ATUser " & _
    '                  ",MachineIP AS MachineIP " & _
    '                  ",MName AS MName " & _
    '                  ",Ecode AS Ecode " & _
    '                  ",EmpId AS EmpId " & _
    '                  ",ATypeId AS ATypeId " & _
    '                  ",PDate AS PDate " & _
    '                "FROM " & _
    '                "( " & _
    '                     "SELECT " & _
    '                          " ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                          ",CASE WHEN isloan = 1 THEN @Loan " & _
    '                          "      WHEN isloan = 0 THEN @Advance END AS ALState " & _
    '                          ",CASE WHEN isloan = 1 THEN ISNULL(lnloan_scheme_master.name, '') " & _
    '                          "      WHEN isloan = 0 THEN @Advance END AS ALScheme " & _
    '                          ",ISNULL(loan_amount, 0) AS LAmount " & _
    '                          ",CASE WHEN audittype IN ( 1, 3 ) THEN 0 " & _
    '                          "      WHEN audittype = 2 AND loan_statusunkid IN ( 1, 3 ) THEN 0 " & _
    '                          " ELSE ISNULL(approved_amount, 0) END AS AAmount " & _
    '                          ",ISNULL(LAApprover.firstname, '') + ' '+ ISNULL(LAApprover.othername, '') + ' '+ ISNULL(LAApprover.surname, '') AS ApprName " & _
    '                          ",CASE WHEN loan_statusunkid = 1 THEN @Pending " & _
    '                          "      WHEN loan_statusunkid = 2 THEN @Approved " & _
    '                          "      WHEN loan_statusunkid = 3 THEN @Rejected  " & _
    '                          "      WHEN loan_statusunkid = 3 THEN @Assigned END AS LAStatus " & _
    '                          ",CASE WHEN audittype = 1 THEN @ADD " & _
    '                          "      WHEN audittype = 2 THEN @EDIT " & _
    '                          "      WHEN audittype = 3 THEN @DELETE END AS AType " & _
    '                          ",CONVERT(CHAR(8), auditdatetime, 112) AS ATDate " & _
    '                          ",CONVERT(CHAR(8), auditdatetime, 108) AS ATTime " & _
    '                          ",ISNULL(hrmsConfiguration..cfuser_master.username, '') AS ATUser " & _
    '                          ",ISNULL(ip, '') AS MachineIP " & _
    '                          ",ISNULL(machine_name, '') AS MName " & _
    '                          ",hremployee_master.employeeunkid AS EmpId " & _
    '                          ",CASE WHEN isloan = 1 THEN 1 " & _
    '                          "      WHEN isloan = 0 THEN 2 END AS ALStateId " & _
    '                          ",audittype AS ATypeId " & _
    '                          ",ISNULL(lnloan_scheme_master.loanschemeunkid, 0) AS ALSchemeId " & _
    '                          ",ISNULL(LAApprover.employeeunkid, 0) AS ApprId " & _
    '                          ",loan_statusunkid AS LAStatusId " & _
    '                          ",hrmsConfiguration..cfuser_master.userunkid AS ATUserId " & _
    '                          ",ISNULL(hremployee_master.employeecode,'') AS Ecode " & _
    '                          ",CONVERT(CHAR(8), application_date, 112) AS PDate " & _
    '                     "FROM atlnloan_process_pending_loan " & _
    '                          "LEFT JOIN hrmsConfiguration..cfuser_master ON atlnloan_process_pending_loan.audituserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
    '                          "LEFT JOIN lnloan_approver_tran ON atlnloan_process_pending_loan.approverunkid = lnloan_approver_tran.approverunkid " & _
    '                          "LEFT JOIN hremployee_master AS LAApprover ON lnloan_approver_tran.approverunkid = LAApprover.employeeunkid " & _
    '                          "LEFT JOIN lnloan_scheme_master ON atlnloan_process_pending_loan.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
    '                          "JOIN hremployee_master ON atlnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "	WHERE CONVERT(CHAR(8), auditdatetime, 112) BETWEEN @AuditDateFrom AND @AuditDateTo "


    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End


    '        If mblnIncludeInactiveEmp = False Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.isactive  = 1 AND LAApprover.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]

    '            StrQ &= " AND CONVERT(CHAR(8),LAApprover.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),LAApprover.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),LAApprover.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),LAApprover.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            'Sohail (17 Apr 2012) -- End
    '        End If

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = " & mintBranchId
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 


    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= ") AS ATPL " & _
    '                "WHERE 1 = 1 "
    '        'S.SANDEEP [ 24 JUNE 2011 ] -- END 



    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then

    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Rows.Item("Column1") = dtRow.Item("EmpName")
    '            rpt_Rows.Item("Column2") = dtRow.Item("ALScheme")
    '            rpt_Rows.Item("Column3") = Format(CDec(dtRow.Item("LAmount")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("AAmount")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column5") = dtRow.Item("ApprName")
    '            rpt_Rows.Item("Column6") = dtRow.Item("LAStatus")
    '            rpt_Rows.Item("Column7") = dtRow.Item("AType")
    '            rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("ATDate").ToString).ToShortDateString & vbCrLf & dtRow.Item("ATTime")
    '            rpt_Rows.Item("Column9") = dtRow.Item("ATUser")
    '            rpt_Rows.Item("Column10") = dtRow.Item("MachineIP")
    '            rpt_Rows.Item("Column11") = dtRow.Item("MName")
    '            rpt_Rows.Item("Column12") = dtRow.Item("Ecode")
    '            rpt_Rows.Item("Column13") = dtRow.Item("EmpId")
    '            rpt_Rows.Item("Column14") = eZeeDate.convertDate(dtRow.Item("PDate").ToString).ToShortDateString

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptATProcessPendingLoan

    '        'Sandeep [ 10 FEB 2011 ] -- Start
    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 6, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 7, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 8, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 9, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If
    '        'Sandeep [ 10 FEB 2011 ] -- End 

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 10, "Code :"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 11, "Employee :"))
    '        Call ReportFunction.TextChange(objRpt, "txtLoanAdvance", Language.getMessage(mstrModuleName, 12, "Loan/Advance"))
    '        Call ReportFunction.TextChange(objRpt, "txtLoanAmount", Language.getMessage(mstrModuleName, 13, "Loan Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtApprvAmount", Language.getMessage(mstrModuleName, 14, "Approved Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtApprvName", Language.getMessage(mstrModuleName, 15, "Approver Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtLAStatus", Language.getMessage(mstrModuleName, 16, "Status"))
    '        Call ReportFunction.TextChange(objRpt, "txtAuditType", Language.getMessage(mstrModuleName, 17, "Audit Type"))
    '        Call ReportFunction.TextChange(objRpt, "txtATDateTime", Language.getMessage(mstrModuleName, 18, "Audit Date & Time"))
    '        Call ReportFunction.TextChange(objRpt, "txtATUser", Language.getMessage(mstrModuleName, 19, "Audit User"))
    '        Call ReportFunction.TextChange(objRpt, "txtIPAddress", Language.getMessage(mstrModuleName, 20, "IP Address"))
    '        Call ReportFunction.TextChange(objRpt, "txtMachine", Language.getMessage(mstrModuleName, 21, "Machine Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtApplicationDate", Language.getMessage(mstrModuleName, 36, "Applied Date"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 22, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 23, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 24, "Page :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    'Nilay (10-Oct-2015) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Advance")
            Language.setMessage(mstrModuleName, 2, "Loan")
            Language.setMessage(mstrModuleName, 3, "Add")
            Language.setMessage(mstrModuleName, 4, "Edit")
            Language.setMessage(mstrModuleName, 5, "Delete")
            Language.setMessage(mstrModuleName, 6, "Prepared By :")
            Language.setMessage(mstrModuleName, 7, "Checked By :")
            Language.setMessage(mstrModuleName, 8, "Approved By :")
            Language.setMessage(mstrModuleName, 9, "Received By :")
            Language.setMessage(mstrModuleName, 10, "Code :")
            Language.setMessage(mstrModuleName, 11, "Employee :")
            Language.setMessage(mstrModuleName, 12, "Loan/Advance")
            Language.setMessage(mstrModuleName, 13, "Loan Amount")
            Language.setMessage(mstrModuleName, 14, "Approved Amount")
            Language.setMessage(mstrModuleName, 15, "Approver Name")
            Language.setMessage(mstrModuleName, 16, "Status")
            Language.setMessage(mstrModuleName, 17, "Audit Type")
            Language.setMessage(mstrModuleName, 18, "Audit Date & Time")
            Language.setMessage(mstrModuleName, 19, "Audit User")
            Language.setMessage(mstrModuleName, 20, "IP Address")
            Language.setMessage(mstrModuleName, 21, "Machine Name")
            Language.setMessage(mstrModuleName, 22, "Printed By :")
            Language.setMessage(mstrModuleName, 23, "Printed Date :")
            Language.setMessage(mstrModuleName, 24, "Page :")
            Language.setMessage(mstrModuleName, 25, "State :")
            Language.setMessage(mstrModuleName, 26, "Audit Type :")
            Language.setMessage(mstrModuleName, 27, "Loan Scheme :")
            Language.setMessage(mstrModuleName, 28, "Approver :")
            Language.setMessage(mstrModuleName, 30, "Status :")
            Language.setMessage(mstrModuleName, 31, "Audit User :")
            Language.setMessage(mstrModuleName, 32, "Order By :")
            Language.setMessage(mstrModuleName, 33, "Audit Date")
            Language.setMessage(mstrModuleName, 34, "Audit Time")
            Language.setMessage(mstrModuleName, 35, "Employee Name")
            Language.setMessage(mstrModuleName, 36, "Applied Date")
            Language.setMessage(mstrModuleName, 37, "Audit Date From :")
            Language.setMessage(mstrModuleName, 38, "To")
            Language.setMessage(mstrModuleName, 39, "Applied Date From :")
            Language.setMessage(mstrModuleName, 40, "Select")
            Language.setMessage(mstrModuleName, 41, "Loan")
            Language.setMessage(mstrModuleName, 42, "Advance")
            Language.setMessage(mstrModuleName, 43, "Branch :")
            Language.setMessage("clsProcess_pending_loan", 71, "Pending")
            Language.setMessage("clsProcess_pending_loan", 72, "Approved")
            Language.setMessage("clsProcess_pending_loan", 74, "Rejected")
            Language.setMessage("clsProcess_pending_loan", 83, "Assigned")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
