'************************************************************************************************************************************
'Class Name : frmATProcessPendingLA.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmATProcessPendingLA

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmATProcessPendingLA"
    Private objATProcessPending As clsATProcessPendingLA
    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Contructor "

    Public Sub New()
        objATProcessPending = New clsATProcessPendingLA(User._Object._Languageunkid,Company._Object._Companyunkid)
        objATProcessPending.SetDefaultValue()
        InitializeComponent()
        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objAppr As New clsLoanApprover_master
        Dim objProcessLoan As New clsProcess_pending_loan
        Dim objScheme As New clsLoan_Scheme
        Dim objAUser As New clsUserAddEdit
        Dim objCMaster As New clsMasterData
        'S.SANDEEP [ 17 AUG 2011 ] -- START
        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
        Dim objBranch As New clsStation
        'S.SANDEEP [ 17 AUG 2011 ] -- END 

        Dim dsCombo As New DataSet
        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmp.GetEmployeeList("List", True, True)
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'Anjan [10 June 2015] -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
            End With

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objAppr.GetList("List", True, True)
            'dsCombo = objAppr.GetList(FinancialYear._Object._DatabaseName, _
            '                          User._Object._Userunkid, _
            '                          FinancialYear._Object._YearUnkid, _
            '                          Company._Object._Countryunkid, _
            '                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                          ConfigParameter._Object._UserAccessModeSetting, _
            '                          True, _
            '                          ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                          "List", True, True)
            dsCombo = objAppr.getListForCombo("List", True, True, False)
            'Nilay (10-Oct-2015) -- End

            With cboApprover
                .ValueMember = "lnapproverunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objProcessLoan.GetLoan_Status("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objScheme.getComboList(True, "List")
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'dsCombo = objAUser.getComboList("List", True)

            'Nilay (01-Mar-2016) -- Start
            'dsCombo = objAUser.getNewComboList("List", , True)
            dsCombo = objAUser.getNewComboList("List", , True, , , , True)
            'Nilay (01-Mar-2016) -- End

            'Nilay (01-Mar-2016) -- End

            With cboAuditUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objCMaster.GetAuditTypeList("List", True, True, True)
            With cboAuditType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objATProcessPending.GetTypeList("List")
            With cboState
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            dsCombo = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objEmp = Nothing
            objAppr = Nothing
            objProcessLoan = Nothing
            objScheme = Nothing
            objAUser = Nothing
            objCMaster = Nothing
            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            objBranch = Nothing
            'S.SANDEEP [ 17 AUG 2011 ] -- END 
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpAppliedFrom.Checked = False
            dtpAppliedTo.Checked = False
            cboApprover.SelectedValue = 0
            cboAuditType.SelectedValue = 0
            cboAuditUser.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboLoanScheme.SelectedValue = 0
            cboState.SelectedValue = 0
            cboStatus.SelectedValue = 0
            objATProcessPending.setDefaultOrderBy(0)
            txtOrderBy.Text = objATProcessPending.OrderByDisplay
            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkIncludeInactiveEmp.Checked = False
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            cboBranch.SelectedValue = 0
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objATProcessPending.SetDefaultValue()

            If dtpAppliedFrom.Checked = True AndAlso dtpAppliedTo.Checked = True Then
                objATProcessPending._AppliedDateFrom = dtpAppliedFrom.Value.Date
                objATProcessPending._AppliedDateTo = dtpAppliedTo.Value.Date
            End If

            objATProcessPending._AuditDateFrom = dtpATDFrom.Value.Date
            objATProcessPending._AuditDateTo = dtpATDTo.Value.Date

            objATProcessPending._ApproverId = cboApprover.SelectedValue
            objATProcessPending._ApproverName = cboApprover.Text

            objATProcessPending._ATUserId = cboAuditUser.SelectedValue
            objATProcessPending._ATUserName = cboAuditUser.Text

            objATProcessPending._AuditTypeId = cboAuditType.SelectedValue
            objATProcessPending._AuditTypeName = cboAuditType.Text

            objATProcessPending._EmployeeId = cboEmployee.SelectedValue
            objATProcessPending._EmployeeName = cboEmployee.Text

            objATProcessPending._LAStateId = cboState.SelectedValue
            objATProcessPending._LAStateName = cboState.Text

            objATProcessPending._LoanSchemeId = cboLoanScheme.SelectedValue
            objATProcessPending._LoanSchemeName = cboLoanScheme.Text

            objATProcessPending._LAStatusId = cboStatus.SelectedValue
            objATProcessPending._LoanStatusName = cboStatus.Text


            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objATProcessPending._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            objATProcessPending._BranchId = cboBranch.SelectedValue
            objATProcessPending._Branch_Name = cboBranch.Text
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            objATProcessPending._PeriodStartDate = dtpATDFrom.Value.Date
            objATProcessPending._PeriodEndDate = dtpATDTo.Value.Date
            'Sohail (17 Apr 2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objATProcessPending._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmAssessmentListReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objATProcessPending = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeSkillsReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentListReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objATProcessPending._ReportName
            Me._Message = objATProcessPending._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentListReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objATProcessPending.generateReport(0, e.Type, enExportAction.None)
            objATProcessPending.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, _
                                                  ConfigParameter._Object._ExportReportPath, _
                                                  ConfigParameter._Object._OpenAfterExport, _
                                                  0, e.Type, enExportAction.None)
            'Nilay (10-Oct-2015) -- End


        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objATProcessPending.generateReport(0, enPrintAction.None, e.Type)
            objATProcessPending.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, _
                                                  ConfigParameter._Object._ExportReportPath, _
                                                  ConfigParameter._Object._OpenAfterExport, _
                                                  0, enPrintAction.None, e.Type)
            'Nilay (10-Oct-2015) -- End


        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsATProcessPendingLA.SetMessages()
            objfrm._Other_ModuleNames = "clsATProcessPendingLA"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End


    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End


#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objATProcessPending.setOrderBy(0)
            txtOrderBy.Text = objATProcessPending.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblAuditUser.Text = Language._Object.getCaption(Me.lblAuditUser.Name, Me.lblAuditUser.Text)
			Me.lblAuditType.Text = Language._Object.getCaption(Me.lblAuditType.Name, Me.lblAuditType.Text)
			Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblScheme.Text = Language._Object.getCaption(Me.lblScheme.Name, Me.lblScheme.Text)
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
			Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.lblAuditFromDate.Text = Language._Object.getCaption(Me.lblAuditFromDate.Name, Me.lblAuditFromDate.Text)
			Me.lblAppliedTo.Text = Language._Object.getCaption(Me.lblAppliedTo.Name, Me.lblAppliedTo.Text)
			Me.lblAppliedFrom.Text = Language._Object.getCaption(Me.lblAppliedFrom.Name, Me.lblAppliedFrom.Text)
			Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
