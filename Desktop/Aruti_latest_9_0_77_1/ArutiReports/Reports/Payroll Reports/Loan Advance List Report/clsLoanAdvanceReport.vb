'************************************************************************************************************************************
'Class Name : clsAssetDeclaration.vb
'Purpose    :
'Date       :05/04/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class
''' Developer: Sohail
''' </summary>

Public Class clsLoanAdvanceReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLoanAdvanceReport"
    Dim objDataOperation As clsDataOperation
    Dim mstrCompanyCode As String = Company._Object._Code
    Dim mstrCompanyName As String = Company._Object._Name

#Region " Private variables "
    Private mdtLoanAdvaceList As DataTable
    Private mdicStatus As Dictionary(Of Integer, String)
    'S.SANDEEP [ 29 May 2013 ] -- START
    'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'S.SANDEEP [ 29 May 2013 ] -- END
    Private mdtLoanBalance As DataTable 'Sohail (29 Jun 2015)

    'SHANI (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Private mintCountryUnkid As Integer = -1
    'SHANI (07 May 2015) -- End 

#End Region

#Region " Properties "
    Public WriteOnly Property _Data_LoanAdvance() As DataTable
        Set(ByVal value As DataTable)
            mdtLoanAdvaceList = value
        End Set
    End Property

    'Sohail (29 Jun 2015) -- Start
    'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
    Public WriteOnly Property _Data_LoanBalance() As DataTable
        Set(ByVal value As DataTable)
            mdtLoanBalance = value
        End Set
    End Property
    'Sohail (29 Jun 2015) -- End

    Public WriteOnly Property _Status_Dic() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicStatus = value
        End Set
    End Property
    'S.SANDEEP [ 29 May 2013 ] -- START
    'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 29 May 2013 ] -- END

    'SHANI (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Public WriteOnly Property _CountryUnkid() As Integer
        Set(ByVal value As Integer)
            mintCountryUnkid = value
        End Set
    End Property
    'SHANI (07 May 2015) -- End 

#End Region

#Region " Report Generation "
    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            ''S.SANDEEP [ 29 May 2013 ] -- START
            ''ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'If mintCompanyUnkid <= 0 Then
            '    mintCompanyUnkid = Company._Object._Companyunkid
            'End If
            'Company._Object._Companyunkid = mintCompanyUnkid
            'ConfigParameter._Object._Companyunkid = mintCompanyUnkid
            'If mintUserUnkid <= 0 Then
            '    mintUserUnkid = User._Object._Userunkid
            'End If
            'User._Object._Userunkid = mintUserUnkid
            ''S.SANDEEP [ 29 May 2013 ] -- END

            'objRpt = Generate_DetailReport()


            ''S.SANDEEP [ 29 May 2013 ] -- START
            ''ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'Rpt = objRpt
            ''S.SANDEEP [ 29 May 2013 ] -- END


            'If Not IsNothing(objRpt) Then
            '    Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, _
                                           Optional ByVal ExportAction As enExportAction = enExportAction.None, _
                                           Optional ByVal xBaseCurrencyId As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""

        Try

            'If mintCompanyUnkid <= 0 Then
            '    mintCompanyUnkid = Company._Object._Companyunkid
            'End If
            'Company._Object._Companyunkid = mintCompanyUnkid
            'ConfigParameter._Object._Companyunkid = mintCompanyUnkid
            'If mintUserUnkid <= 0 Then
            '    mintUserUnkid = User._Object._Userunkid
            'End If
            'User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport()

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Nilay (10-Oct-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""

        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rptRow As DataRow


        Try
            objDataOperation = New clsDataOperation

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim decLoanTotal As Decimal = 0
            Dim decAdvanceTotal As Decimal = 0
            'Sohail (29 Jun 2015) -- Start
            'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
            Dim decTotalLoanBalance As Decimal = 0
            Dim decTotalLoanPaid As Decimal = 0
            Dim decTotalAdvanceBalance As Decimal = 0
            Dim decTotalAdvancePaid As Decimal = 0
            'Sohail (29 Jun 2015) -- End

            If mdtLoanAdvaceList.Rows.Count > 0 Then
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                'Decimal.TryParse(mdtLoanAdvaceList.Compute("SUM(Amount)", "Loan_AdvanceUnkId = 1").ToString, decLoanTotal)
                'Decimal.TryParse(mdtLoanAdvaceList.Compute("SUM(Amount)", "Loan_AdvanceUnkId = 2").ToString, decAdvanceTotal)

                'SHANI (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                If mintCountryUnkid > 0 Then
                    decLoanTotal = (From p In mdtLoanAdvaceList Where (CInt(p.Item("Loan_AdvanceUnkId")) = 1) Select (CDec(p.Item("Amount")))).DefaultIfEmpty.Sum()
                    decAdvanceTotal = (From p In mdtLoanAdvaceList Where (CInt(p.Item("Loan_AdvanceUnkId")) = 2) Select (CDec(p.Item("Amount")))).DefaultIfEmpty.Sum()
                Else
                    decLoanTotal = (From p In mdtLoanAdvaceList Where (CInt(p.Item("Loan_AdvanceUnkId")) = 1) Select (CDec(p.Item("basecurrency_amount")))).DefaultIfEmpty.Sum()
                    decAdvanceTotal = (From p In mdtLoanAdvaceList Where (CInt(p.Item("Loan_AdvanceUnkId")) = 2) Select (CDec(p.Item("basecurrency_amount")))).DefaultIfEmpty.Sum()
                End If
                decTotalLoanBalance = (From p In mdtLoanAdvaceList Where (CInt(p.Item("Loan_AdvanceUnkId")) = 1) Select (CDec(p.Item("loanbalance")))).DefaultIfEmpty.Sum()
                decTotalAdvanceBalance = (From p In mdtLoanAdvaceList Where (CInt(p.Item("Loan_AdvanceUnkId")) = 2) Select (CDec(p.Item("loanbalance")))).DefaultIfEmpty.Sum()

                'Varsha Rana (12-Sept-2017) -- Start
                'Enhancement - Loan Topup in ESS
                'decTotalLoanPaid = decLoanTotal - decTotalLoanBalance
                'decTotalAdvancePaid = decAdvanceTotal - decTotalAdvanceBalance
                decTotalLoanPaid = (From p In mdtLoanAdvaceList Where (CInt(p.Item("Loan_AdvanceUnkId")) = 1) Select (CDec(p.Item("paidlaon")))).DefaultIfEmpty.Sum()
                decTotalAdvancePaid = (From p In mdtLoanAdvaceList Where (CInt(p.Item("Loan_AdvanceUnkId")) = 2) Select (CDec(p.Item("paidlaon")))).DefaultIfEmpty.Sum()
                'Varsha Rana (12-Sept-2017) -- End



                'SHANI (07 May 2015) -- End

                'Sohail (07 May 2015) -- End
            End If

            mdtLoanAdvaceList = New DataView(mdtLoanAdvaceList, "", "EmpName", DataViewRowState.CurrentRows).ToTable



            For Each dtRow As DataRow In mdtLoanAdvaceList.Rows
                rptRow = rpt_Data.Tables("ArutiTable").NewRow

                rptRow.Item("column1") = dtRow.Item("Loan_AdvanceUnkId").ToString
                rptRow.Item("column2") = dtRow.Item("Loan_Advance").ToString
                rptRow.Item("column3") = dtRow.Item("VocNo").ToString
                rptRow.Item("column4") = dtRow.Item("employeeunkid").ToString
                rptRow.Item("column5") = dtRow.Item("empcode").ToString
                rptRow.Item("column6") = dtRow.Item("EmpName").ToString
                rptRow.Item("column7") = dtRow.Item("loanschemeunkid").ToString
                rptRow.Item("column8") = dtRow.Item("loancode").ToString
                rptRow.Item("column9") = dtRow.Item("LoanScheme").ToString

                'SHANI (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                If mintCountryUnkid > 0 Then
                    rptRow.Item("column10") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                Else
                    rptRow.Item("column10") = Format(CDec(dtRow.Item("basecurrency_amount")), GUI.fmtCurrency)
                End If

                'SHANI (07 May 2015) -- End
                rptRow.Item("column11") = dtRow.Item("PeriodName").ToString
                rptRow.Item("column13") = dtRow.Item("installments").ToString 'Nilay (15-Dec-2015) -- [Intallments] Replaced by [installments]

                'Sohail (29 Jun 2015) -- Start
                'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                Dim intUnkID As Integer = CInt(dtRow.Item("loanadvancetranunkid"))
                Dim dr_Row As List(Of DataRow) = (From p In mdtLoanBalance Where (CInt(p.Item("loanadvancetranunkid")) = intUnkID) Select (p)).ToList

                'SHANI (11 JUL 2015) -- Start
                'Enhancement : Replacing all List view with Grid view on Close Year Wizard
                'If dr_Row.Count > 0 Then
                '    rptRow.Item("column14") = Format(CDec(dr_Row(dr_Row.Count - 1).Item("LoanAmountCF").ToString), GUI.fmtCurrency)
                '    rptRow.Item("column15") = Format(CDec(dtRow.Item("Amount")) - CDec(dr_Row(dr_Row.Count - 1).Item("LoanAmountCF")), GUI.fmtCurrency)

                '    decTotalBalance += CDec(Format(CDec(dr_Row(dr_Row.Count - 1).Item("LoanAmountCF").ToString), GUI.fmtCurrency))
                '    decTotalPaid += CDec(Format(CDec(dtRow.Item("Amount")) - CDec(dr_Row(dr_Row.Count - 1).Item("LoanAmountCF")), GUI.fmtCurrency))
                'Else
                '    rptRow.Item("column14") = Format(CDec(dtRow.Item("balance_amount").ToString), GUI.fmtCurrency)
                '    rptRow.Item("column15") = Format(CDec(dtRow.Item("Amount")) - CDec(dtRow.Item("balance_amount")), GUI.fmtCurrency)

                '    decTotalBalance += CDec(Format(CDec(dtRow.Item("balance_amount").ToString), GUI.fmtCurrency))
                '    decTotalPaid += CDec(Format(CDec(dtRow.Item("Amount")) - CDec(dtRow.Item("balance_amount")), GUI.fmtCurrency))
                'End If

                If dr_Row.Count > 0 Then

                    'SHANI (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    rptRow.Item("column14") = Format(CDec(dtRow.Item("loanbalance")), GUI.fmtCurrency)
                    rptRow.Item("column15") = Format(CDec(dtRow.Item("paidlaon")), GUI.fmtCurrency)
                    'SHANI (07 May 2015) -- End

                    'decTotalBalance += CDec(Format(CDec(dr_Row(0).Item("LastProjectedBalance").ToString), GUI.fmtCurrency))
                    'decTotalPaid += CDec(Format(CDec(dr_Row(0).Item("loan_amount")) - CDec(dr_Row(0).Item("LastProjectedBalance")), GUI.fmtCurrency))
                Else
                    rptRow.Item("column14") = Format(CDec(0), GUI.fmtCurrency)
                    rptRow.Item("column15") = Format(CDec(0), GUI.fmtCurrency)

                    'decTotalBalance += CDec(Format(CDec(0), GUI.fmtCurrency))
                    'decTotalPaid += CDec(Format(CDec(0), GUI.fmtCurrency))
                End If
                'SHANI (11 JUL 2015) -- End
                'Sohail (29 Jun 2015) -- End

                'Select Case CInt(dtRow.Item("loan_statusunkid"))
                '    Case 1 'InProgress
                '        rptRow.Item("column12") = "InProgress"
                '    Case 2 'OnHold
                '        rptRow.Item("column12") = "OnHold"
                '    Case 3 'WrittenOff
                '        rptRow.Item("column12") = "WrittenOff"
                '    Case 4 'Completed
                '        rptRow.Item("column12") = "Completed"
                'End Select
                rptRow.Item("column12") = mdicStatus.Item(CInt(dtRow.Item("loan_statusunkid"))).ToString

                If CInt(dtRow.Item("Loan_AdvanceUnkId")) = 1 Then 'Loan
                    rptRow.Item("column21") = Format(decLoanTotal, GUI.fmtCurrency)
                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    rptRow.Item("column22") = Format(decTotalLoanBalance, GUI.fmtCurrency)
                    rptRow.Item("column23") = Format(decTotalLoanPaid, GUI.fmtCurrency)
                    'Sohail (07 May 2015) -- End
                Else 'Advance
                    rptRow.Item("column21") = Format(decAdvanceTotal, GUI.fmtCurrency)
                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    rptRow.Item("column22") = Format(decTotalAdvanceBalance, GUI.fmtCurrency)
                    rptRow.Item("column23") = Format(decTotalAdvancePaid, GUI.fmtCurrency)
                    'Sohail (07 May 2015) -- End
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rptRow)
            Next


            objRpt = New ArutiReport.Designer.rptLoanAdvance

            'Dim arrImageRow As DataRow = Nothing
            'arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            'ReportFunction.Logo_Display(objRpt, _
            '                                   True, _
            '                                    False, _
            '                                    "arutiLogo1", _
            '                                    "arutiLogo2", _
            '                                    arrImageRow, _
            '                                    , _
            '                                    , _
            '                                    , _
            '                                    ConfigParameter._Object._GetLeftMargin, _
            '                                    ConfigParameter._Object._GetRightMargin, _
            '                                    ConfigParameter._Object._TOPPageMarging)

            'rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            Dim objImg(0) As Object
            If Company._Object._Image IsNot Nothing Then
                objImg(0) = eZeeDataType.image2Data(Company._Object._Image)
            Else
                Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                objImg(0) = eZeeDataType.image2Data(imgBlank)
            End If
            arrImageRow.Item("arutiLogo") = objImg(0)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtCompanyName", mstrCompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Call ReportFunction.TextChange(objRpt, "lblReportCaption", Language.getMessage(mstrModuleName, 1, "Loan Advance Report"))
            Call ReportFunction.TextChange(objRpt, "lblVoc", Language.getMessage(mstrModuleName, 2, "Voc. #"))
            Call ReportFunction.TextChange(objRpt, "lblEmpCode", Language.getMessage(mstrModuleName, 3, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "lblEmpName", Language.getMessage(mstrModuleName, 4, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "lblLoanCode", Language.getMessage(mstrModuleName, 5, "Loan Code"))
            Call ReportFunction.TextChange(objRpt, "lblLoanName", Language.getMessage(mstrModuleName, 6, "Loan Scheme Name"))
            Call ReportFunction.TextChange(objRpt, "lblAmount", Language.getMessage(mstrModuleName, 7, "Amount"))
            Call ReportFunction.TextChange(objRpt, "lblPeriod", Language.getMessage(mstrModuleName, 8, "Period"))
            Call ReportFunction.TextChange(objRpt, "lblStatus", Language.getMessage(mstrModuleName, 9, "Status"))
            Call ReportFunction.TextChange(objRpt, "lblTotal", Language.getMessage(mstrModuleName, 10, "Total"))
            Call ReportFunction.TextChange(objRpt, "txtInstallments", Language.getMessage(mstrModuleName, 11, "Installments"))
            'Sohail (29 Jun 2015) -- Start
            'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
            Call ReportFunction.TextChange(objRpt, "txtBalance", Language.getMessage(mstrModuleName, 12, "Curr. Principal Balance"))
            Call ReportFunction.TextChange(objRpt, "txtPaidLoan", Language.getMessage(mstrModuleName, 13, "Paid Loan"))
            'Sohail (29 Jun 2015) -- End



            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Loan Advance Report")
            Language.setMessage(mstrModuleName, 2, "Voc. #")
            Language.setMessage(mstrModuleName, 3, "Employee Code")
            Language.setMessage(mstrModuleName, 4, "Employee Name")
            Language.setMessage(mstrModuleName, 5, "Loan Code")
            Language.setMessage(mstrModuleName, 6, "Loan Scheme Name")
            Language.setMessage(mstrModuleName, 7, "Amount")
            Language.setMessage(mstrModuleName, 8, "Period")
            Language.setMessage(mstrModuleName, 9, "Status")
            Language.setMessage(mstrModuleName, 10, "Total")
            Language.setMessage(mstrModuleName, 11, "Installments")
            Language.setMessage(mstrModuleName, 12, "Curr. Principal Balance")
            Language.setMessage(mstrModuleName, 13, "Paid Loan")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
