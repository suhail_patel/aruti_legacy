'************************************************************************************************************************************
'Class Name : frmATPaymentTranReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmATPaymentTranReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmATPaymentTranReport"
    Private objATPayment As clsATPaymentTranReport

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Contructor "

    Public Sub New()
        objATPayment = New clsATPaymentTranReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objATPayment.SetDefaultValue()
        InitializeComponent()
        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objAUser As New clsUserAddEdit
        Dim objCMaster As New clsMasterData
        'S.SANDEEP [ 17 AUG 2011 ] -- START
        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
        Dim objBranch As New clsStation
        'S.SANDEEP [ 17 AUG 2011 ] -- END 

        Dim dsCombo As New DataSet
        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmp.GetEmployeeList("List", True, True)
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'Anjan [10 June 2015] -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
            End With
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'dsCombo = objAUser.getComboList("List", True)

            'Nilay (01-Mar-2016) -- Start
            'dsCombo = objAUser.getNewComboList("List", , True)
            dsCombo = objAUser.getNewComboList("List", , True, , , , True)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            With cboAuditUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objCMaster.GetAuditTypeList("List", True, True, True)
            With cboAuditType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objCMaster.GetPaymentMode("List")
            With cboPaymentMode
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objCMaster.GetPaymentBy("List")
            With cboPaymentBy
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objATPayment.Get_PaySection("List")
            With cboSection
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objATPayment.Get_TypeOfPay("List")
            With cboType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            dsCombo = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objEmp = Nothing
            objAUser = Nothing
            objCMaster = Nothing
            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            objBranch = Nothing
            'S.SANDEEP [ 17 AUG 2011 ] -- END 
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpPayFrom.Checked = False
            dtpPayTo.Checked = False
            cboAuditType.SelectedValue = 0
            cboAuditUser.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboPaymentBy.SelectedValue = 0
            cboPaymentMode.SelectedValue = 0
            cboSection.SelectedValue = 0
            cboType.SelectedValue = 0
            objATPayment.setDefaultOrderBy(0)
            txtOrderBy.Text = objATPayment.OrderByDisplay

            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkIncludeInactiveEmp.Checked = False
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 


            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            cboBranch.SelectedValue = 0
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objATPayment.SetDefaultValue()

            objATPayment._ATUserId = cboAuditUser.SelectedValue
            objATPayment._ATUserName = cboAuditUser.Text

            objATPayment._ATypeId = cboAuditType.SelectedValue
            objATPayment._ATypeName = cboAuditType.Text

            objATPayment._AuditFromDate = dtpATDFrom.Value
            objATPayment._AuditToDate = dtpATDTo.Value

            objATPayment._EmpId = cboEmployee.SelectedValue
            objATPayment._EmpName = cboEmployee.Text

            objATPayment._PayById = cboPaymentBy.SelectedValue
            objATPayment._PayByName = cboPaymentBy.Text

            If dtpPayFrom.Checked = True AndAlso dtpPayTo.Checked = True Then
                objATPayment._PayFromDate = dtpPayFrom.Value
                objATPayment._PayToDate = dtpPayTo.Value
            End If
            
            objATPayment._PayModeId = cboPaymentMode.SelectedValue
            objATPayment._PayModeName = cboPaymentMode.Text

            objATPayment._PaySecId = cboSection.SelectedValue
            objATPayment._PaySecName = cboSection.Text

            objATPayment._PayTypeId = cboType.SelectedValue
            objATPayment._PayTypeName = cboType.Text


            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objATPayment._InIncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 


            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            objATPayment._BranchId = cboBranch.SelectedValue
            objATPayment._Branch_Name = cboBranch.Text
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            objATPayment._PeriodStartDate = dtpATDFrom.Value.Date
            objATPayment._PeriodEndDate = dtpATDTo.Value.Date
            'Sohail (17 Apr 2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objATPayment._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmATPaymentTranReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objATPayment = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmATPaymentTranReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmATPaymentTranReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objATPayment._ReportName
            Me._Message = objATPayment._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmATPaymentTranReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub
            objATPayment.generateReport(0, e.Type, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            objATPayment.generateReport(0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsATPaymentTranReport.SetMessages()
            objfrm._Other_ModuleNames = "clsATPaymentTranReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub



    'Pinkal (03-Sep-2012) -- End


    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End


#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objATPayment.setOrderBy(0)
            txtOrderBy.Text = objATPayment.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.lblAuditFromDate.Text = Language._Object.getCaption(Me.lblAuditFromDate.Name, Me.lblAuditFromDate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblAuditUser.Text = Language._Object.getCaption(Me.lblAuditUser.Name, Me.lblAuditUser.Text)
			Me.lblAuditType.Text = Language._Object.getCaption(Me.lblAuditType.Name, Me.lblAuditType.Text)
			Me.lblAppliedTo.Text = Language._Object.getCaption(Me.lblAppliedTo.Name, Me.lblAppliedTo.Text)
			Me.lblPayDateFrom.Text = Language._Object.getCaption(Me.lblPayDateFrom.Name, Me.lblPayDateFrom.Text)
			Me.lblPaymentBy.Text = Language._Object.getCaption(Me.lblPaymentBy.Name, Me.lblPaymentBy.Text)
			Me.lblPaymentMode.Text = Language._Object.getCaption(Me.lblPaymentMode.Name, Me.lblPaymentMode.Text)
			Me.lblType.Text = Language._Object.getCaption(Me.lblType.Name, Me.lblType.Text)
			Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
