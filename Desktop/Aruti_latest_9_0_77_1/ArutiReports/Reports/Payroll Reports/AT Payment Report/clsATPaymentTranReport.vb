'************************************************************************************************************************************
'Class Name : clsATPaymentTranReport.vb
'Purpose    :
'Date       :03/04/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsATPaymentTranReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsATPaymentTranReport"
    Private mstrReportId As String = enArutiReport.ATPaymentTranReport  '44
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Property Variables "

    Private mdtPayFromDate As Date = Nothing
    Private mdtPayToDate As Date = Nothing
    Private mdtAuditFromDate As Date = Nothing
    Private mdtAuditToDate As Date = Nothing
    Private mintPaySecId As Integer = 0
    Private mstrPaySecName As String = String.Empty
    Private mintPayModeId As Integer = 0
    Private mstrPayModeName As String = String.Empty
    Private mintPayById As Integer = 0
    Private mstrPayByName As String = String.Empty
    Private mintPayTypeId As Integer = 0
    Private mstrPayTypeName As String = String.Empty
    Private mintATUserId As Integer = 0
    Private mstrATUserName As String = String.Empty
    Private mintATypeId As Integer = 0
    Private mstrATypeName As String = String.Empty
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = String.Empty

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnInIncludeInactiveEmp As Boolean = True
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    'S.SANDEEP [ 17 AUG 2011 ] -- END 

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (17 Apr 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End



#End Region

#Region " Properties "

    Public WriteOnly Property _PayFromDate() As Date
        Set(ByVal value As Date)
            mdtPayFromDate = value
        End Set
    End Property

    Public WriteOnly Property _PayToDate() As Date
        Set(ByVal value As Date)
            mdtPayToDate = value
        End Set
    End Property

    Public WriteOnly Property _AuditFromDate() As Date
        Set(ByVal value As Date)
            mdtAuditFromDate = value
        End Set
    End Property

    Public WriteOnly Property _AuditToDate() As Date
        Set(ByVal value As Date)
            mdtAuditToDate = value
        End Set
    End Property

    Public WriteOnly Property _PaySecId() As Integer
        Set(ByVal value As Integer)
            mintPaySecId = value
        End Set
    End Property

    Public WriteOnly Property _PaySecName() As String
        Set(ByVal value As String)
            mstrPaySecName = value
        End Set
    End Property

    Public WriteOnly Property _PayModeId() As Integer
        Set(ByVal value As Integer)
            mintPayModeId = value
        End Set
    End Property

    Public WriteOnly Property _PayModeName() As String
        Set(ByVal value As String)
            mstrPayModeName = value
        End Set
    End Property

    Public WriteOnly Property _PayById() As Integer
        Set(ByVal value As Integer)
            mintPayById = value
        End Set
    End Property

    Public WriteOnly Property _PayByName() As String
        Set(ByVal value As String)
            mstrPayByName = value
        End Set
    End Property

    Public WriteOnly Property _PayTypeId() As Integer
        Set(ByVal value As Integer)
            mintPayTypeId = value
        End Set
    End Property

    Public WriteOnly Property _PayTypeName() As String
        Set(ByVal value As String)
            mstrPayTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ATUserId() As Integer
        Set(ByVal value As Integer)
            mintATUserId = value
        End Set
    End Property

    Public WriteOnly Property _ATUserName() As String
        Set(ByVal value As String)
            mstrATUserName = value
        End Set
    End Property

    Public WriteOnly Property _ATypeId() As Integer
        Set(ByVal value As Integer)
            mintATypeId = value
        End Set
    End Property

    Public WriteOnly Property _ATypeName() As String
        Set(ByVal value As String)
            mstrATypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _InIncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnInIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch_Name() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property
    'S.SANDEEP [ 17 AUG 2011 ] -- END

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (17 Apr 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtPayFromDate = Nothing
            mdtPayToDate = Nothing
            mdtAuditFromDate = Nothing
            mdtAuditToDate = Nothing
            mintPaySecId = 0
            mstrPaySecName = ""
            mintPayModeId = 0
            mstrPayModeName = ""
            mintPayById = 0
            mstrPayByName = ""
            mintPayTypeId = 0
            mstrPayTypeName = ""
            mintATUserId = 0
            mstrATUserName = ""
            mintATypeId = 0
            mstrATypeName = ""
            mintEmpId = 0
            mstrEmpName = ""

            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnInIncludeInactiveEmp = True
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            mintBranchId = -1
            mstrBranchName = ""
            'S.SANDEEP [ 17 AUG 2011 ] -- END

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 34, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 35, "Advance"))
            objDataOperation.AddParameter("@PaySlip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 36, "PaySlip"))
            objDataOperation.AddParameter("@Savings", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "Savings"))

            objDataOperation.AddParameter("@Cash", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 38, "Cash"))
            objDataOperation.AddParameter("@Cheque", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 39, "Cheque"))
            objDataOperation.AddParameter("@Transfer", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 40, "Transfer"))

            objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 41, "Value"))
            objDataOperation.AddParameter("@Percent", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 42, "Percent"))

            objDataOperation.AddParameter("@Payment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 44, "Payment"))
            objDataOperation.AddParameter("@Received", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 43, "Received"))
            objDataOperation.AddParameter("@Withdraw", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 45, "Withdrawal"))
            objDataOperation.AddParameter("@Repayment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 46, "Repayment"))

            objDataOperation.AddParameter("@Add", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 47, "Add"))
            objDataOperation.AddParameter("@Edit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 48, "Edit"))
            objDataOperation.AddParameter("@Delete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 49, "Delete"))

            objDataOperation.AddParameter("@FDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditFromDate))
            objDataOperation.AddParameter("@TDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditToDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Audit Date From :") & " " & mdtAuditFromDate.Date & " " & _
                               Language.getMessage(mstrModuleName, 25, "To") & " " & mdtAuditToDate.Date & " "

            If mdtPayFromDate <> Nothing AndAlso mdtPayToDate <> Nothing Then
                objDataOperation.AddParameter("@mdtPayFromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtPayFromDate))
                objDataOperation.AddParameter("@mdtPayToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtPayToDate))
                Me._FilterQuery &= " AND PayDate BETWEEN @mdtPayFromDate AND @mdtPayToDate "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "Pay Date From :") & " " & mdtPayFromDate.Date & " " & _
                                   Language.getMessage(mstrModuleName, 25, "To") & " " & mdtPayToDate.Date & " "
            End If

            If mintPaySecId > 0 Then
                objDataOperation.AddParameter("@PaySecId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaySecId)
                Me._FilterQuery &= " AND PayRefId = @PaySecId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Section :") & " " & mstrPaySecName & " "
            End If

            If mintPayModeId > 0 Then
                objDataOperation.AddParameter("@PayModeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayModeId)
                Me._FilterQuery &= " AND PayModeId = @PayModeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "Pay Mode :") & " " & mstrPayModeName & " "
            End If

            If mintPayById > 0 Then
                objDataOperation.AddParameter("@PayById", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayById)
                Me._FilterQuery &= " AND PayById = @PayById "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "Pay By :") & " " & mstrPayByName & " "
            End If

            If mintPayTypeId > 0 Then
                objDataOperation.AddParameter("@PayTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayTypeId)
                Me._FilterQuery &= " AND PayTypeId =@PayTypeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Payment Type :") & " " & mstrPayTypeName & " "
            End If

            If mintATUserId > 0 Then
                objDataOperation.AddParameter("@ATUserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintATUserId)
                Me._FilterQuery &= " AND ATUserId = @ATUserId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "Audit User :") & " " & mstrATUserName & " "
            End If

            If mintATypeId > 0 Then
                objDataOperation.AddParameter("@ATypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintATypeId)
                Me._FilterQuery &= " AND ATypeId = @ATypeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Audit Type :") & " " & mstrATypeName & " "
            End If

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                Me._FilterQuery &= " AND EmpId = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Employee :") & " " & mstrEmpName & " "
            End If

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 53, "Branch :") & " " & mstrBranchName & " "
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnInIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If
            'Sohail (17 Apr 2012) -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY EmpId,ATypeId," & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport()
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Get_PaySection(Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 0 AS Id, @Select AS Name " & _
                   "UNION SELECT 1 AS Id, @LoanSec AS Name " & _
                   "UNION SELECT 2 AS Id, @AdvanceSec AS Name " & _
                   "UNION SELECT 3 AS Id, @PaySlipSec AS Name " & _
                   "UNION SELECT 4 AS Id, @SavingSec AS Name "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 50, "Select"))
            objDataOperation.AddParameter("@LoanSec", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 34, "Loan"))
            objDataOperation.AddParameter("@AdvanceSec", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 35, "Advance"))
            objDataOperation.AddParameter("@PaySlipSec", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 36, "PaySlip"))
            objDataOperation.AddParameter("@SavingSec", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "Savings"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_PaySection; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function Get_TypeOfPay(Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 0 AS Id, @Select AS Name " & _
                   "UNION SELECT 1 AS Id, @Pay AS Name " & _
                   "UNION SELECT 2 AS Id, @Rev AS Name " & _
                   "UNION SELECT 3 AS Id, @WD AS Name " & _
                   "UNION SELECT 4 AS Id, @Repay AS Name "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 51, "Select"))
            objDataOperation.AddParameter("@Pay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 44, "Payment"))
            objDataOperation.AddParameter("@Rev", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 43, "Received"))
            objDataOperation.AddParameter("@WD", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 45, "Withdrawal"))
            objDataOperation.AddParameter("@Repay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 46, "Repayment"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_TypeOfPay; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()

            iColumn_DetailReport.Add(New IColumn("Ename", Language.getMessage(mstrModuleName, 21, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("PayDate", Language.getMessage(mstrModuleName, 7, "Pay Date")))
            iColumn_DetailReport.Add(New IColumn("Pay_Section", Language.getMessage(mstrModuleName, 8, "Section")))
            iColumn_DetailReport.Add(New IColumn("PayMode", Language.getMessage(mstrModuleName, 9, "Pay Mode")))
            iColumn_DetailReport.Add(New IColumn("PayBy", Language.getMessage(mstrModuleName, 10, "Pay By")))
            iColumn_DetailReport.Add(New IColumn("PayType", Language.getMessage(mstrModuleName, 11, "Pay Type")))
            iColumn_DetailReport.Add(New IColumn("ATUser", Language.getMessage(mstrModuleName, 13, "Audit User")))
            iColumn_DetailReport.Add(New IColumn("AType", Language.getMessage(mstrModuleName, 14, "Audit Type")))
            iColumn_DetailReport.Add(New IColumn("AuditDate", Language.getMessage(mstrModuleName, 22, "Audit Date")))
            iColumn_DetailReport.Add(New IColumn("ATime", Language.getMessage(mstrModuleName, 23, "Audit Time")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation


            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'StrQ = "SELECT " & _
            '     " ECode AS ECode " & _
            '     ",Ename AS Ename " & _
            '     ",PayDate AS PayDate " & _
            '     ",Pay_Section AS Pay_Section " & _
            '     ",PayMode AS PayMode " & _
            '     ",PayBy AS PayBy " & _
            '     ",PayType AS PayType " & _
            '     ",PaidFor AS PaidFor " & _
            '     ",ATUser AS ATUser " & _
            '     ",AType AS AType " & _
            '     ",ADate AS ADate " & _
            '     ",ATime AS ATime " & _
            '     ",PayDetails AS PayDetails " & _
            '     ",ChequeNo AS ChequeNo " & _
            '     ",EmpId AS EmpId " & _
            '     ",ATypeId AS ATypeId " & _
            '     ",AuditDate AS AuditDate " & _
            '     ",Amount AS Amount " & _
            '    "FROM " & _
            '    "( " & _
            '         "SELECT " & _
            '              " hremployee_master.employeecode AS ECode " & _
            '              ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
            '              ",CONVERT(CHAR(8),paymentdate,112) AS PayDate " & _
            '              ",CASE WHEN referenceid = 1 THEN @Loan " & _
            '                     "WHEN referenceid = 2 THEN @Advance " & _
            '                     "WHEN referenceid = 3 THEN @PaySlip " & _
            '                     "WHEN referenceid = 4 THEN @Savings " & _
            '              " END AS Pay_Section " & _
            '              ",CASE WHEN paymentmode = 1 THEN @Cash " & _
            '                     "WHEN paymentmode = 2 THEN @Cheque " & _
            '                     "WHEN paymentmode = 3 THEN @Transfer " & _
            '              " END AS PayMode " & _
            '              ",CASE WHEN paymentby = 1 THEN @Value " & _
            '                     "WHEN paymentby = 2 THEN @Percent " & _
            '              " END AS PayBy " & _
            '              ",ISNULL(amount,0) AS Amount " & _
            '              ",CASE WHEN paytypeid = 1 THEN @Payment " & _
            '                     "WHEN paytypeid = 2 THEN @Received " & _
            '                     "WHEN paytypeid = 3 THEN @Withdraw " & _
            '                     "WHEN paytypeid = 4 THEN @Repayment " & _
            '              " END AS PayType " & _
            '              ",CASE WHEN referenceid = 1 THEN @Loan+' --> '+ISNULL(lnloan_scheme_master.name,'') " & _
            '                     "WHEN referenceid = 2 THEN @Advance+' --> '+@Advance " & _
            '                     "WHEN referenceid = 3 THEN @PaySlip+' --> '+@PaySlip " & _
            '                     "WHEN referenceid = 4 THEN @Savings+' --> '+ISNULL(svsavingscheme_master.savingschemename,'') " & _
            '              " END AS PaidFor " & _
            '              ",ISNULL(hrmsConfiguration..cfuser_master.username,'') AS ATUser " & _
            '              ",CASE WHEN audittype = 1 THEN @Add " & _
            '                     "WHEN audittype = 2 THEN @Edit " & _
            '                     "WHEN audittype = 3 THEN @Delete " & _
            '              " END AS AType " & _
            '              ",CONVERT(CHAR(8),auditdatetime,112) AS ADate " & _
            '              ",CONVERT(CHAR(8),auditdatetime,108) AS ATime " & _
            '              ",CASE WHEN paymentmode = 1 THEN '' " & _
            '                     "WHEN paymentmode = 2 THEN ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupname,'')+' -- '+ISNULL(hrmsConfiguration..cfbankbranch_master.branchname,'') " & _
            '                     "WHEN paymentmode = 3 THEN ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupname,'')+' -- '+ISNULL(hrmsConfiguration..cfbankbranch_master.branchname,'') " & _
            '              " END AS PayDetails " & _
            '              ",CASE WHEN paymentmode = 1 THEN '' " & _
            '                     "WHEN paymentmode = 2 THEN ISNULL(chequeno,'') " & _
            '                     "WHEN paymentmode = 3 THEN ISNULL(chequeno,'') " & _
            '              " END AS ChequeNo " & _
            '              ",hremployee_master.employeeunkid AS EmpId " & _
            '              ",atprpayment_tran.referenceid AS PayRefId " & _
            '              ",atprpayment_tran.paymentmode AS PayModeId " & _
            '              ",atprpayment_tran.paymentby AS PayById " & _
            '              ",atprpayment_tran.audittype AS ATypeId " & _
            '              ",hrmsConfiguration..cfuser_master.userunkid AS ATUserId " & _
            '              ",atprpayment_tran.paytypeid AS PayTypeId " & _
            '              ",auditdatetime AS AuditDate " & _
            '         "FROM atprpayment_tran " & _
            '              "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON atprpayment_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
            '              "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id = " & enPayrollGroupType.Bank & " " & _
            '              "JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atprpayment_tran.audituserunkid " & _
            '              "LEFT JOIN svsaving_tran ON atprpayment_tran.referencetranunkid = svsaving_tran.savingtranunkid " & _
            '              "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
            '              "LEFT JOIN lnloan_advance_tran ON atprpayment_tran.referencetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
            '              "LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
            '              "JOIN hremployee_master ON atprpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '         "WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN @FDate AND @TDate " & _
            '    ") AS ATPAYMENT " & _
            '    "WHERE 1 = 1 "

            StrQ = "SELECT " & _
                     " ECode AS ECode " & _
                     ",Ename AS Ename " & _
                     ",PayDate AS PayDate " & _
                     ",Pay_Section AS Pay_Section " & _
                     ",PayMode AS PayMode " & _
                     ",PayBy AS PayBy " & _
                     ",PayType AS PayType " & _
                     ",PaidFor AS PaidFor " & _
                     ",ATUser AS ATUser " & _
                     ",AType AS AType " & _
                     ",ADate AS ADate " & _
                     ",ATime AS ATime " & _
                     ",PayDetails AS PayDetails " & _
                     ",ChequeNo AS ChequeNo " & _
                     ",EmpId AS EmpId " & _
                     ",ATypeId AS ATypeId " & _
                     ",AuditDate AS AuditDate " & _
                     ",Amount AS Amount " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                              " hremployee_master.employeecode AS ECode " & _
                              ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
                              ",CONVERT(CHAR(8),paymentdate,112) AS PayDate " & _
                              ",CASE WHEN referenceid = 1 THEN @Loan " & _
                                     "WHEN referenceid = 2 THEN @Advance " & _
                                     "WHEN referenceid = 3 THEN @PaySlip " & _
                    "			  WHEN referenceid = 4 THEN @Savings END AS Pay_Section " & _
                              ",CASE WHEN paymentmode = 1 THEN @Cash " & _
                                     "WHEN paymentmode = 2 THEN @Cheque " & _
                    "			  WHEN paymentmode = 3 THEN @Transfer END AS PayMode " & _
                              ",CASE WHEN paymentby = 1 THEN @Value " & _
                    "			  WHEN paymentby = 2 THEN @Percent END AS PayBy " & _
                              ",ISNULL(amount,0) AS Amount " & _
                              ",CASE WHEN paytypeid = 1 THEN @Payment " & _
                                     "WHEN paytypeid = 2 THEN @Received " & _
                                     "WHEN paytypeid = 3 THEN @Withdraw " & _
                    "			  WHEN paytypeid = 4 THEN @Repayment END AS PayType " & _
                              ",CASE WHEN referenceid = 1 THEN @Loan+' --> '+ISNULL(lnloan_scheme_master.name,'') " & _
                                     "WHEN referenceid = 2 THEN @Advance+' --> '+@Advance " & _
                                     "WHEN referenceid = 3 THEN @PaySlip+' --> '+@PaySlip " & _
                    "			  WHEN referenceid = 4 THEN @Savings+' --> '+ISNULL(svsavingscheme_master.savingschemename,'') END AS PaidFor " & _
                              ",ISNULL(hrmsConfiguration..cfuser_master.username,'') AS ATUser " & _
                              ",CASE WHEN audittype = 1 THEN @Add " & _
                                     "WHEN audittype = 2 THEN @Edit " & _
                    "			  WHEN audittype = 3 THEN @Delete END AS AType " & _
                              ",CONVERT(CHAR(8),auditdatetime,112) AS ADate " & _
                              ",CONVERT(CHAR(8),auditdatetime,108) AS ATime " & _
                              ",CASE WHEN paymentmode = 1 THEN '' " & _
                                     "WHEN paymentmode = 2 THEN ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupname,'')+' -- '+ISNULL(hrmsConfiguration..cfbankbranch_master.branchname,'') " & _
                    "			  WHEN paymentmode = 3 THEN ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupname,'')+' -- '+ISNULL(hrmsConfiguration..cfbankbranch_master.branchname,'') END AS PayDetails " & _
                              ",CASE WHEN paymentmode = 1 THEN '' " & _
                                     "WHEN paymentmode = 2 THEN ISNULL(chequeno,'') " & _
                    "			  WHEN paymentmode = 3 THEN ISNULL(chequeno,'') END AS ChequeNo " & _
                              ",hremployee_master.employeeunkid AS EmpId " & _
                              ",atprpayment_tran.referenceid AS PayRefId " & _
                              ",atprpayment_tran.paymentmode AS PayModeId " & _
                              ",atprpayment_tran.paymentby AS PayById " & _
                              ",atprpayment_tran.audittype AS ATypeId " & _
                              ",hrmsConfiguration..cfuser_master.userunkid AS ATUserId " & _
                              ",atprpayment_tran.paytypeid AS PayTypeId " & _
                              ",auditdatetime AS AuditDate " & _
                         "FROM atprpayment_tran " & _
                              "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON atprpayment_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                              "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id = " & enPayrollGroupType.Bank & " " & _
                              "JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atprpayment_tran.audituserunkid " & _
                              "LEFT JOIN svsaving_tran ON atprpayment_tran.referencetranunkid = svsaving_tran.savingtranunkid " & _
                              "LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
                              "LEFT JOIN lnloan_advance_tran ON atprpayment_tran.referencetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                              "LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
                              "JOIN hremployee_master ON atprpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "	WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN @FDate AND @TDate "


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End

            If mblnInIncludeInactiveEmp = False Then
                'Sohail (17 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " AND hremployee_master.isactive = 1 "
                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
                'Sohail (17 Apr 2012) -- End
            End If


            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                StrQ &= " AND hremployee_master.stationunkid = " & mintBranchId
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 


            'Sohail (28 Jun 2011) -- Start
            'Issue : According to prvilege that lower level user should not see superior level employees.
            If UserAccessLevel._AccessLevel.Length > 0 Then
                'Sohail (17 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
                StrQ &= UserAccessLevel._AccessLevelFilterString
                'Sohail (17 Apr 2012) -- End
            End If
            'Sohail (28 Jun 2011) -- End
            StrQ &= ") AS ATPAYMENT " & _
                    "WHERE 1 = 1 "
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 



            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("ECode")
                rpt_Rows.Item("Column2") = dtRow.Item("Ename")
                rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("PayDate").ToString).ToShortDateString
                rpt_Rows.Item("Column4") = dtRow.Item("Pay_Section")
                rpt_Rows.Item("Column5") = dtRow.Item("PayMode")
                rpt_Rows.Item("Column6") = dtRow.Item("PayBy")
                rpt_Rows.Item("Column7") = dtRow.Item("PayType")
                rpt_Rows.Item("Column8") = dtRow.Item("PaidFor")
                rpt_Rows.Item("Column9") = dtRow.Item("ATUser")
                rpt_Rows.Item("Column10") = dtRow.Item("AType")
                rpt_Rows.Item("Column11") = eZeeDate.convertDate(dtRow.Item("ADate").ToString).ToShortDateString & vbCrLf & dtRow.Item("ATime")
                rpt_Rows.Item("Column12") = dtRow.Item("PayDetails")
                rpt_Rows.Item("Column13") = dtRow.Item("ChequeNo")
                rpt_Rows.Item("Column14") = dtRow.Item("EmpId")
                rpt_Rows.Item("Column15") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptATPaymentReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 5, "Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 6, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtPayDate", Language.getMessage(mstrModuleName, 7, "Pay Date"))
            Call ReportFunction.TextChange(objRpt, "txtPaySection", Language.getMessage(mstrModuleName, 8, "Section"))
            Call ReportFunction.TextChange(objRpt, "txtPayMode", Language.getMessage(mstrModuleName, 9, "Pay Mode"))
            Call ReportFunction.TextChange(objRpt, "txtPayBy", Language.getMessage(mstrModuleName, 10, "Pay By"))
            Call ReportFunction.TextChange(objRpt, "txtPayType", Language.getMessage(mstrModuleName, 11, "Pay Type"))
            Call ReportFunction.TextChange(objRpt, "txtPaidFor", Language.getMessage(mstrModuleName, 12, "Paid For"))
            Call ReportFunction.TextChange(objRpt, "txtAuditUser", Language.getMessage(mstrModuleName, 13, "Audit User"))
            Call ReportFunction.TextChange(objRpt, "txtAuditType", Language.getMessage(mstrModuleName, 14, "Audit Type"))
            Call ReportFunction.TextChange(objRpt, "txtAuditDateTime", Language.getMessage(mstrModuleName, 15, "Audit Date & Time"))
            Call ReportFunction.TextChange(objRpt, "txtPayDetails", Language.getMessage(mstrModuleName, 16, "Paid Details"))
            Call ReportFunction.TextChange(objRpt, "txtCheque", Language.getMessage(mstrModuleName, 17, "Cheque No."))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 52, "Amount"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 18, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 19, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 20, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By :")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Code :")
            Language.setMessage(mstrModuleName, 6, "Employee :")
            Language.setMessage(mstrModuleName, 7, "Pay Date")
            Language.setMessage(mstrModuleName, 8, "Section")
            Language.setMessage(mstrModuleName, 9, "Pay Mode")
            Language.setMessage(mstrModuleName, 10, "Pay By")
            Language.setMessage(mstrModuleName, 11, "Pay Type")
            Language.setMessage(mstrModuleName, 12, "Paid For")
            Language.setMessage(mstrModuleName, 13, "Audit User")
            Language.setMessage(mstrModuleName, 14, "Audit Type")
            Language.setMessage(mstrModuleName, 15, "Audit Date & Time")
            Language.setMessage(mstrModuleName, 16, "Paid Details")
            Language.setMessage(mstrModuleName, 17, "Cheque No.")
            Language.setMessage(mstrModuleName, 18, "Printed By :")
            Language.setMessage(mstrModuleName, 19, "Printed Date :")
            Language.setMessage(mstrModuleName, 20, "Page :")
            Language.setMessage(mstrModuleName, 21, "Employee Name")
            Language.setMessage(mstrModuleName, 22, "Audit Date")
            Language.setMessage(mstrModuleName, 23, "Audit Time")
            Language.setMessage(mstrModuleName, 24, "Audit Date From :")
            Language.setMessage(mstrModuleName, 25, "To")
            Language.setMessage(mstrModuleName, 26, "Pay Date From :")
            Language.setMessage(mstrModuleName, 27, "Section :")
            Language.setMessage(mstrModuleName, 28, "Pay Mode :")
            Language.setMessage(mstrModuleName, 29, "Pay By :")
            Language.setMessage(mstrModuleName, 30, "Payment Type :")
            Language.setMessage(mstrModuleName, 31, "Audit User :")
            Language.setMessage(mstrModuleName, 32, "Audit Type :")
            Language.setMessage(mstrModuleName, 33, "Order By :")
            Language.setMessage(mstrModuleName, 34, "Loan")
            Language.setMessage(mstrModuleName, 35, "Advance")
            Language.setMessage(mstrModuleName, 36, "PaySlip")
            Language.setMessage(mstrModuleName, 37, "Savings")
            Language.setMessage(mstrModuleName, 38, "Cash")
            Language.setMessage(mstrModuleName, 39, "Cheque")
            Language.setMessage(mstrModuleName, 40, "Transfer")
            Language.setMessage(mstrModuleName, 41, "Value")
            Language.setMessage(mstrModuleName, 42, "Percent")
            Language.setMessage(mstrModuleName, 43, "Received")
            Language.setMessage(mstrModuleName, 44, "Payment")
            Language.setMessage(mstrModuleName, 45, "Withdrawal")
            Language.setMessage(mstrModuleName, 46, "Repayment")
            Language.setMessage(mstrModuleName, 47, "Add")
            Language.setMessage(mstrModuleName, 48, "Edit")
            Language.setMessage(mstrModuleName, 49, "Delete")
            Language.setMessage(mstrModuleName, 50, "Select")
            Language.setMessage(mstrModuleName, 51, "Select")
            Language.setMessage(mstrModuleName, 52, "Amount")
            Language.setMessage(mstrModuleName, 53, "Branch :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
End Class
