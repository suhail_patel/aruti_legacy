#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmSalaryBudgetBreakdownByPeriod

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSalaryBudgetBreakdownByPeriod"
    Private objSalaryBudget As clsSalaryBudgetBreakdownByPeriod

    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String


    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty
    Private mintFirstOpenPeriod As Integer = 0
#End Region

#Region " Constructor "

    Public Sub New()
        objSalaryBudget = New clsSalaryBudgetBreakdownByPeriod(User._Object._Languageunkid,Company._Object._Companyunkid)
        objSalaryBudget.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objBudget As New clsBudget_MasterNew
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet
        Try
            'Sohail (02 Aug 2017) -- Start
            'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
            'dsCombo = objBudget.GetComboList("Budget", False, True)
            dsCombo = objBudget.GetComboList("Budget", False, False)
            Dim intDefaultBudget As Integer = objBudget.GetDefaultBudgetID()
            'Sohail (02 Aug 2017) -- End
            With cboBudget
                .ValueMember = "budgetunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Budget")
                'Sohail (02 Aug 2017) -- Start
                'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
                'If .Items.Count > 0 Then .SelectedIndex = 0
                If .Items.Count > 0 Then
                    If intDefaultBudget > 0 Then
                        .SelectedValue = 0
                    Else
                        .SelectedIndex = 0
                    End If
                End If
                'Sohail (02 Aug 2017) -- End
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objBudget = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            objSalaryBudget.setDefaultOrderBy(0)
            txtOrderBy.Text = objSalaryBudget.OrderByDisplay
            If cboBudget.Items.Count > 0 Then cboBudget.SelectedIndex = 0

            mstrStringIds = ""
            mintViewIdx = -1
            mstrStringName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            mstrAdvanceFilter = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmSalaryBudgetBreakdownt_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSalaryBudget = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryBudgetBreakdownt_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSalaryBudgetBreakdownt_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            eZeeHeader.Title = objSalaryBudget._ReportName
            eZeeHeader.Message = objSalaryBudget._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryBudgetBreakdownt_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

    Private Sub cboBudget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBudget.SelectedIndexChanged
        Dim objBudgetCodes As New clsBudgetcodes_master
        Dim dsCombo As DataSet
        Try
            dsCombo = objBudgetCodes.GetComboListPeriod("Period", True, CInt(cboBudget.SelectedValue))
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBudget_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If cboBudget.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, There is no Default budget. Please set Default budget from Budget List screen"), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf cboBudget.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Budget is compulsory infomation. Please select Budget to continue."), enMsgBoxStyle.Information)
                cboBudget.Focus()
                Exit Sub
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory infomation. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If


            objSalaryBudget.SetDefaultValue()

            objSalaryBudget._BudgetId = cboBudget.SelectedValue
            objSalaryBudget._BudgetName = cboBudget.Text

            objSalaryBudget._PeriodId = cboPeriod.SelectedValue
            objSalaryBudget._PeriodName = cboPeriod.Text

            'objReconciliation._ViewByIds = mstrStringIds
            'objReconciliation._ViewIndex = mintViewIdx
            'objReconciliation._ViewByName = mstrStringName
            'objReconciliation._Analysis_Fields = mstrAnalysis_Fields
            'objReconciliation._Analysis_Join = mstrAnalysis_Join
            'objReconciliation._Analysis_OrderBy = mstrAnalysis_OrderBy
            'objReconciliation._Report_GroupName = mstrReport_GroupName


            'objReconciliation._Advance_Filter = mstrAdvanceFilter

            'Sohail (02 Aug 2017) -- Start
            'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
            'If objSalaryBudget.Export_Report(User._Object._Userunkid, Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, True, False, True, GUI.fmtCurrency, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport) = False Then
            If objSalaryBudget.Export_Report(User._Object._Userunkid, Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, True, False, True, GUI.fmtCurrency, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                'Sohail (02 Aug 2017) -- End
                If objSalaryBudget._Message.Trim <> "" Then
                    eZeeMsgBox.Show(objSalaryBudget._Message, enMsgBoxStyle.Information)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayrollVariance.SetMessages()
            objfrm._Other_ModuleNames = "clsPayrollVariance"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objSalaryBudget.setOrderBy(0)
            txtOrderBy.Text = objSalaryBudget.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region


#Region "LinkButton Event"



#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor



           


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

          

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, There is no Default budget. Please set Default budget from Budget List screen")
            Language.setMessage(mstrModuleName, 2, "Budget is compulsory infomation. Please select Budget to continue.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
