Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

Public Class clsEndOfServiceReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEndOfServiceReport"
    Private mstrReportId As String = enArutiReport.End_Of_Service_Report
    Dim objDataOperation As clsDataOperation

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mstrOriginalHeadIDs As String = ""

    Private mintGrossPayHeadID As Integer = -1
    Private mstrGrossPayHeadName As String = ""

    Private mintOT1HourHeadID As Integer = -1
    Private mstrOT1HourHeadName As String = ""
    Private mintOT2HourHeadID As Integer = -1
    Private mstrOT2HourHeadName As String = ""
    Private mintOT3HourHeadID As Integer = -1
    Private mstrOT3HourHeadName As String = ""
    Private mintOT4HourHeadID As Integer = -1
    Private mstrOT4HourHeadName As String = ""
    Private mintOT1AmountHeadID As Integer = -1
    Private mstrOT1AmountHeadName As String = ""
    Private mintOT2AmountHeadID As Integer = -1
    Private mstrOT2AmountHeadName As String = ""
    Private mintOT3AmountHeadID As Integer = -1
    Private mstrOT3AmountHeadName As String = ""
    Private mintOT4AmountHeadID As Integer = -1
    Private mstrOT4AmountHeadName As String = ""

    Private mintLeaveTypeID As Integer = -1
    Private mstrLeaveTypeName As String = ""
    Private mintLeaveBalanceHeadID As Integer = -1
    Private mstrLeaveBalanceHeadName As String = ""

    Private mintGratuityFirst3YearsID As Integer = -1
    Private mstrGratuityFirst3YearsName As String = ""
    Private mintGratuityBalanceYearsID As Integer = -1
    Private mstrGratuityBalanceYearsName As String = ""
    Private mintGratuityBalanceMonthsID As Integer = -1
    Private mstrGratuityBalanceMonthsName As String = ""
    Private mintGratuityBalanceDaysID As Integer = -1
    Private mstrGratuityBalanceDaysName As String = ""
    Private mintGratuityBalanceYearsAmtID As Integer = -1
    Private mstrGratuityBalanceYearsAmtName As String = ""
    Private mintGratuityBalanceMonthsAmtID As Integer = -1
    Private mstrGratuityBalanceMonthsAmtName As String = ""
    Private mintGratuityBalanceDaysAmtID As Integer = -1
    Private mstrGratuityBalanceDaysAmtName As String = ""

    Private mdtDatabaseStartDate As DateTime = FinancialYear._Object._Database_Start_Date
    Private mdtDatabaseEndDate As DateTime = FinancialYear._Object._Database_End_Date
    Private mintLeaveBalanceSetting As Integer = 0
    Private mintIsFin_Close As Integer = -1
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    Private mdtTnAEndDate As Date 'Sohail (07 Jan 2014)

    Private mstrOrderByQuery As String = ""

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""




    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mstrAdvance_Filter As String = String.Empty

    Private mintBase_CurrencyId As Integer = ConfigParameter._Object._Base_CurrencyId
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    Private mintUserUnkid As Integer = User._Object._Userunkid
    Private mintCompanyUnkid As Integer = Company._Object._Companyunkid
    Private mstrTranDatabaseName As String = FinancialYear._Object._DatabaseName


    Private marrDatabaseName As New ArrayList

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END


    'Pinkal (24-May-2014) -- Start
    'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
    Private mintYearUnkId As Integer = 0
    'Pinkal (24-May-2014) -- End

    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
    Private mintLeaveAccrueTenureSetting As Integer = enLeaveAccrueTenureSetting.Yearly
    Private mintLeaveAccrueDaysAfterEachMonth As Integer = 0
    'Pinkal (18-Nov-2016) -- End




#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _OriginalHeadIDs() As String
        Set(ByVal value As String)
            mstrOriginalHeadIDs = value
        End Set
    End Property

    Public WriteOnly Property _GrossPayHeadId() As Integer
        Set(ByVal value As Integer)
            mintGrossPayHeadID = value
        End Set
    End Property

    Public WriteOnly Property _GrossPayHeadName() As String
        Set(ByVal value As String)
            mstrGrossPayHeadName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeID() As Integer
        Set(ByVal value As Integer)
            mintLeaveTypeID = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeName() As String
        Set(ByVal value As String)
            mstrLeaveTypeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceHeadID() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceHeadID = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceHeadName() As String
        Set(ByVal value As String)
            mstrLeaveBalanceHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OT1HourHeadId() As Integer
        Set(ByVal value As Integer)
            mintOT1HourHeadID = value
        End Set
    End Property

    Public WriteOnly Property _OT1HourHeadName() As String
        Set(ByVal value As String)
            mstrOT1HourHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OT2HourHeadId() As Integer
        Set(ByVal value As Integer)
            mintOT2HourHeadID = value
        End Set
    End Property

    Public WriteOnly Property _OT2HourHeadName() As String
        Set(ByVal value As String)
            mstrOT2HourHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OT3HourHeadId() As Integer
        Set(ByVal value As Integer)
            mintOT3HourHeadID = value
        End Set
    End Property

    Public WriteOnly Property _OT3HourHeadName() As String
        Set(ByVal value As String)
            mstrOT3HourHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OT4HourHeadId() As Integer
        Set(ByVal value As Integer)
            mintOT4HourHeadID = value
        End Set
    End Property

    Public WriteOnly Property _OT4HourHeadName() As String
        Set(ByVal value As String)
            mstrOT4HourHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OT1AmountHeadId() As Integer
        Set(ByVal value As Integer)
            mintOT1AmountHeadID = value
        End Set
    End Property

    Public WriteOnly Property _OT1AmountHeadName() As String
        Set(ByVal value As String)
            mstrOT1AmountHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OT2AmountHeadId() As Integer
        Set(ByVal value As Integer)
            mintOT2AmountHeadID = value
        End Set
    End Property

    Public WriteOnly Property _OT2AmountHeadName() As String
        Set(ByVal value As String)
            mstrOT2AmountHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OT3AmountHeadId() As Integer
        Set(ByVal value As Integer)
            mintOT3AmountHeadID = value
        End Set
    End Property

    Public WriteOnly Property _OT3AmountHeadName() As String
        Set(ByVal value As String)
            mstrOT3AmountHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OT4AmountHeadId() As Integer
        Set(ByVal value As Integer)
            mintOT4AmountHeadID = value
        End Set
    End Property

    Public WriteOnly Property _OT4AmountHeadName() As String
        Set(ByVal value As String)
            mstrOT4AmountHeadName = value
        End Set
    End Property

    Public WriteOnly Property _GratuityFirst3YearsId() As Integer
        Set(ByVal value As Integer)
            mintGratuityFirst3YearsID = value
        End Set
    End Property

    Public WriteOnly Property _GratuityFirst3YearsName() As String
        Set(ByVal value As String)
            mstrGratuityFirst3YearsName = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceYearsId() As Integer
        Set(ByVal value As Integer)
            mintGratuityBalanceYearsID = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceYearsName() As String
        Set(ByVal value As String)
            mstrGratuityBalanceYearsName = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceMonthsId() As Integer
        Set(ByVal value As Integer)
            mintGratuityBalanceMonthsID = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceMonthsName() As String
        Set(ByVal value As String)
            mstrGratuityBalanceMonthsName = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceDaysId() As Integer
        Set(ByVal value As Integer)
            mintGratuityBalanceDaysID = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceDaysName() As String
        Set(ByVal value As String)
            mstrGratuityBalanceDaysName = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceYearsAmtId() As Integer
        Set(ByVal value As Integer)
            mintGratuityBalanceYearsAmtID = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceYearsAmtName() As String
        Set(ByVal value As String)
            mstrGratuityBalanceYearsAmtName = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceMonthsAmtId() As Integer
        Set(ByVal value As Integer)
            mintGratuityBalanceMonthsAmtID = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceMonthsAmtName() As String
        Set(ByVal value As String)
            mstrGratuityBalanceMonthsAmtName = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceDaysAmtId() As Integer
        Set(ByVal value As Integer)
            mintGratuityBalanceDaysAmtID = value
        End Set
    End Property

    Public WriteOnly Property _GratuityBalanceDaysAmtName() As String
        Set(ByVal value As String)
            mstrGratuityBalanceDaysAmtName = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseStartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtDatabaseStartDate = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtDatabaseEndDate = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    Public WriteOnly Property _IsFin_Close() As Integer
        Set(ByVal value As Integer)
            mintIsFin_Close = value
        End Set
    End Property




    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    'Sohail (07 Jan 2014) -- Start
    'Enhancement - Separate TnA Periods from Payroll Periods
    Public WriteOnly Property _TnAEndDate() As Date
        Set(ByVal value As Date)
            mdtTnAEndDate = value
        End Set
    End Property
    'Sohail (07 Jan 2014) -- End

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _Base_CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBase_CurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TranDatabaseName() As String
        Set(ByVal value As String)
            mstrTranDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END


    'Pinkal (24-May-2014) -- Start
    'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearUnkId = value
        End Set
    End Property

    'Pinkal (24-May-2014) -- End


    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

    Public WriteOnly Property _LeaveAccrueTenureSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueTenureSetting = value
        End Set
    End Property

    Public WriteOnly Property _LeaveAccrueDaysAfterEachMonth() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueDaysAfterEachMonth = value
        End Set
    End Property

    'Pinkal (18-Nov-2016) -- End

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mintPeriodId = -1
            mstrPeriodName = ""
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mstrOriginalHeadIDs = ""
            mintGrossPayHeadID = 0
            mstrGrossPayHeadName = ""
            mintOT1HourHeadID = -1
            mstrOT1HourHeadName = ""
            mintOT2HourHeadID = -1
            mstrOT2HourHeadName = ""
            mintOT3HourHeadID = -1
            mstrOT3HourHeadName = ""
            mintOT4HourHeadID = -1
            mstrOT4HourHeadName = ""
            mintOT1AmountHeadID = -1
            mstrOT1AmountHeadName = ""
            mintOT2AmountHeadID = -1
            mstrOT2AmountHeadName = ""
            mintOT3AmountHeadID = -1
            mstrOT3AmountHeadName = ""
            mintOT4AmountHeadID = -1
            mstrOT4AmountHeadName = ""
            mintLeaveTypeID = 0
            mstrLeaveTypeName = ""
            mintLeaveBalanceHeadID = 0
            mstrLeaveBalanceHeadName = ""
            mintGratuityFirst3YearsID = 0
            mstrGratuityFirst3YearsName = ""
            mintGratuityBalanceYearsID = 0
            mstrGratuityBalanceYearsName = ""
            mintGratuityBalanceMonthsID = 0
            mstrGratuityBalanceMonthsName = ""
            mintGratuityBalanceDaysID = 0
            mstrGratuityBalanceDaysName = ""
            mintGratuityBalanceYearsAmtID = 0
            mstrGratuityBalanceYearsAmtName = ""
            mintGratuityBalanceMonthsAmtID = 0
            mstrGratuityBalanceMonthsAmtName = ""
            mintGratuityBalanceDaysAmtID = 0
            mstrGratuityBalanceDaysAmtName = ""


            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""



            mstrAdvance_Filter = ""


            'Pinkal (24-May-2014) -- Start
            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
            mintYearUnkId = 0
            'Pinkal (24-May-2014) -- End



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        mstrOrderByQuery = ""
        Try

            Dim mstrheadID As String = ""
            Dim mstrModeID As String = ""


            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Period : ") & " " & mstrPeriodName & " "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintGrossPayHeadID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Gross Pay Head : ") & " " & mstrGrossPayHeadName & " "
            End If
            objDataOperation.AddParameter("@grossheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrossPayHeadID)

            If mintOT1HourHeadID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "OT1 Hour Head : ") & " " & mstrOT1HourHeadName & " "
            End If
            objDataOperation.AddParameter("@OT1HoursID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOT1HourHeadID)

            If mintOT1AmountHeadID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "OT1 Amount Head : ") & " " & mstrOT1AmountHeadName & " "
            End If
            objDataOperation.AddParameter("@OT1AmountID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOT1AmountHeadID)

            If mintOT2HourHeadID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "OT2 Hour Head : ") & " " & mstrOT2HourHeadName & " "
            End If
            objDataOperation.AddParameter("@OT2HoursID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOT2HourHeadID)

            If mintOT2AmountHeadID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "OT2 Amount Head : ") & " " & mstrOT2AmountHeadName & " "
            End If
            objDataOperation.AddParameter("@OT2AmountID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOT2AmountHeadID)

            If mintOT3HourHeadID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 34, "OT3 Hour Head : ") & " " & mstrOT3HourHeadName & " "
            End If
            objDataOperation.AddParameter("@OT3HoursID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOT3HourHeadID)

            If mintOT3AmountHeadID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, "OT3 Amount Head : ") & " " & mstrOT3AmountHeadName & " "
            End If
            objDataOperation.AddParameter("@OT3AmountID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOT3AmountHeadID)

            If mintOT4HourHeadID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 36, "OT4 Hour Head : ") & " " & mstrOT4HourHeadName & " "
            End If
            objDataOperation.AddParameter("@OT4HoursID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOT4HourHeadID)

            If mintOT4AmountHeadID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 37, "OT4 Amount Head : ") & " " & mstrOT4AmountHeadName & " "
            End If
            objDataOperation.AddParameter("@OT4AmountID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOT4AmountHeadID)

            If mintLeaveTypeID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 38, "Leave Type : ") & " " & mstrLeaveTypeName & " "
            End If
            objDataOperation.AddParameter("@LeaveTypeID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveTypeID)

            If mintLeaveBalanceHeadID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 39, "Leave Balance Head : ") & " " & mstrLeaveBalanceHeadName & " "
            End If
            objDataOperation.AddParameter("@LeaveBalanceHeadID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveBalanceHeadID)

            If mintGratuityFirst3YearsID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 40, "Gratuity for First 3 Years : ") & " " & mstrGratuityFirst3YearsName & " "
            End If
            objDataOperation.AddParameter("@First3YearsID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGratuityFirst3YearsID)

            If mintGratuityBalanceYearsID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 41, "Gratuity for Balance Years : ") & " " & mstrGratuityBalanceYearsName & " "
            End If
            objDataOperation.AddParameter("@BalanceYearID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGratuityBalanceYearsID)

            If mintGratuityBalanceMonthsID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 42, "Gratuity for Balance Months : ") & " " & mstrGratuityBalanceMonthsName & " "
            End If
            objDataOperation.AddParameter("@BalanceMonthID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGratuityBalanceMonthsID)

            If mintGratuityBalanceDaysID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 43, "Gratuity for Balance Days : ") & " " & mstrGratuityBalanceDaysName & " "
            End If
            objDataOperation.AddParameter("@BalanceDaysID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGratuityBalanceDaysID)

            If mintGratuityBalanceYearsAmtID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 44, "Gratuity for Balance Years Amount : ") & " " & mstrGratuityBalanceYearsAmtName & " "
            End If
            objDataOperation.AddParameter("@BalanceYearAmtID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGratuityBalanceYearsAmtID)

            If mintGratuityBalanceMonthsAmtID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 45, "Gratuity for Balance Months Amount : ") & " " & mstrGratuityBalanceMonthsAmtName & " "
            End If
            objDataOperation.AddParameter("@BalanceMonthAmtID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGratuityBalanceMonthsAmtID)

            If mintGratuityBalanceDaysAmtID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 46, "Gratuity for Balance Days Amount : ") & " " & mstrGratuityBalanceDaysAmtName & " "
            End If
            objDataOperation.AddParameter("@BalanceDaysAmtID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGratuityBalanceDaysAmtID)


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                If mintViewIndex > 0 Then
                    If Me.OrderByQuery.Trim = "" Then
                        'mstrOrderByQuery &= "ORDER BY payperiodunkid,GNAME"
                    Else
                        'mstrOrderByQuery &= "ORDER BY payperiodunkid,GNAME, " & Me.OrderByQuery
                    End If

                Else
                    'mstrOrderByQuery &= "ORDER BY payperiodunkid," & Me.OrderByQuery
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid
        '    User._Object._Userunkid = mintUserUnkid

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then


        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        Dim blnShowGrandTotal As Boolean = True
        '        Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
        '        Dim objDicSubTotal As New Dictionary(Of Integer, Object)
        '        Dim objDicSubGroupTotal As New Dictionary(Of Integer, Object)
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim strGTotal As String = Language.getMessage(mstrModuleName, 21, "Total Count :")

        '        Dim row As WorksheetRow
        '        Dim wcell As WorksheetCell
        '        Dim arrGroupCount() As String = {Language.getMessage(mstrModuleName, 27, "Period Count:")}
        '        Dim arrDicGroupExtraInfo() As Dictionary(Of Integer, Object) = Nothing


        '        If mdtTableExcel IsNot Nothing Then

        '            Dim intGroupColumn As Integer = 0

        '            If mintViewIndex > 0 Then
        '                intGroupColumn = mdtTableExcel.Columns.Count - 1
        '                Dim strGrpCols As String() = {"column1", "column10"}
        '                strarrGroupColumns = strGrpCols
        '                Dim arr() As String = {Language.getMessage(mstrModuleName, 20, "Sub Count :"), Language.getMessage(mstrModuleName, 27, "Period Count:")}
        '                arrGroupCount = arr

        '                objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 47, "Sub Total:"))
        '                objDicSubGroupTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

        '                Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal, objDicSubGroupTotal}
        '                arrDicGroupExtraInfo = arrDic
        '            Else
        '                intGroupColumn = mdtTableExcel.Columns.Count - 2
        '                Dim strGrpCols As String() = {"column10"}
        '                strarrGroupColumns = strGrpCols
        '                objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

        '                Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal}
        '                arrDicGroupExtraInfo = arrDic
        '            End If

        '            objDicGrandTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 19, "Grand Total :"))


        '            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '                intArrayColumnWidth(i) = 125
        '            Next

        '            '*******   REPORT FOOTER   ******
        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s8w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)
        '            '----------------------


        '            If ConfigParameter._Object._IsShowCheckedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Checked By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowApprovedBy = True Then

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Approved By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowReceivedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)
        '            End If

        '        End If

        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", arrGroupCount, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, arrDicGroupExtraInfo, True)

        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            objRpt = Generate_DetailReport()
            Rpt = objRpt

            If Not IsNothing(objRpt) Then

                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim blnShowGrandTotal As Boolean = True
                Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
                Dim objDicSubTotal As New Dictionary(Of Integer, Object)
                Dim objDicSubGroupTotal As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim strGTotal As String = Language.getMessage(mstrModuleName, 21, "Total Count :")

                Dim row As WorksheetRow
                Dim wcell As WorksheetCell
                Dim arrGroupCount() As String = {Language.getMessage(mstrModuleName, 27, "Period Count:")}
                Dim arrDicGroupExtraInfo() As Dictionary(Of Integer, Object) = Nothing

                If mdtTableExcel IsNot Nothing Then

                    Dim intGroupColumn As Integer = 0

                    If mintViewIndex > 0 Then
                        intGroupColumn = mdtTableExcel.Columns.Count - 1
                        Dim strGrpCols As String() = {"column1", "column10"}
                        strarrGroupColumns = strGrpCols
                        Dim arr() As String = {Language.getMessage(mstrModuleName, 20, "Sub Count :"), Language.getMessage(mstrModuleName, 27, "Period Count:")}
                        arrGroupCount = arr

                        objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 47, "Sub Total:"))
                        objDicSubGroupTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

                        Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal, objDicSubGroupTotal}
                        arrDicGroupExtraInfo = arrDic
                    Else
                        intGroupColumn = mdtTableExcel.Columns.Count - 2
                        Dim strGrpCols As String() = {"column10"}
                        strarrGroupColumns = strGrpCols
                        objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

                        Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal}
                        arrDicGroupExtraInfo = arrDic
                    End If

                    objDicGrandTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 19, "Grand Total :"))

                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next

                    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------

                    If ConfigParameter._Object._IsShowCheckedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Checked By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowApprovedBy = True Then

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Approved By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowReceivedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    End If

                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", arrGroupCount, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, arrDicGroupExtraInfo, True)

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 9, "Emp No")))
            iColumn_DetailReport.Add(New IColumn("surname", Language.getMessage(mstrModuleName, 10, "Surname")))
            iColumn_DetailReport.Add(New IColumn("firstname", Language.getMessage(mstrModuleName, 11, "First Name")))
            iColumn_DetailReport.Add(New IColumn("amount", Language.getMessage(mstrModuleName, 13, "Amount")))
            iColumn_DetailReport.Add(New IColumn("referenceno", Language.getMessage(mstrModuleName, 14, "Reference No.")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


    'Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrInnerQ As String = ""
    '    Dim strDatabaseName As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim OT_Data As ArutiReport.Designer.dsArutiReport
    '    Dim Leave_Data As ArutiReport.Designer.dsArutiReport
    '    Dim Gratuity_Data As ArutiReport.Designer.dsArutiReport
    '    Dim mdecNetToPay As Decimal = 0

    '    Try
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0

    '        objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        'Dim objPeriod As New clscommom_period_Tran


    '        'objPeriod = Nothing

    '        '*** Salary Details
    '        StrQ = "SELECT  hremployee_master.employeeunkid " & _
    '                      ", employeecode "
    '        'S.SANDEEP [ 26 MAR 2014 ] -- START
    '        '",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EMP_NAME " 
    '        If mblnFirstNamethenSurname = False Then
    '            '", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' '  + ISNULL(surname, '') AS employeename " & _
    '            StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' '  + ISNULL(othername, '') AS employeename "
    '        Else
    '            StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' '  + ISNULL(surname, '') AS employeename "
    '        End If

    '        StrQ &= ", CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
    '                      ", CONVERT(CHAR(8), termination_from_date, 112) AS termination_from_date " & _
    '                      ", CONVERT(CHAR(8), termination_to_date, 112) AS termination_to_date " & _
    '                      ", CONVERT(CHAR(8), empl_enddate, 112) AS empl_enddate " & _
    '                      ", ISNULL(hrjob_master.job_code, '') AS job_code " & _
    '                      ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
    '                      ", ISNULL(tableOrigin.trnheadcode, '') AS HeadCode " & _
    '                      ", ISNULL(tableOrigin.trnheadname, '') AS HeadName " & _
    '                      ", ISNULL(tableOrigin.amount, 0) AS Amount " & _
    '                "FROM    hremployee_master " & _
    '                        "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid IN ( " & mstrOriginalHeadIDs & " ) "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableOrigin ON hremployee_master.employeeunkid = tableOrigin.employeeunkid " & _
    '                "WHERE 1 = 1 "

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
    '        End If

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If


    '        rpt_Data = New ArutiReport.Designer.dsArutiReport


    '        Dim strPrevKey As String = ""
    '        Dim decHeadsTotal As Decimal = 0
    '        Dim dtAppointment As Date = Nothing
    '        Dim dtTerminationFrom As Date = Nothing
    '        Dim dtTerminationTo As Date = Nothing
    '        Dim dtEmplEnd As Date = Nothing
    '        Dim mdtStartDate As Date = Nothing
    '        Dim mdtLastDateOfWorking As Date = Nothing
    '        Dim intWorkingDaysInPeriod As Integer = 0

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
    '            rpt_Row.Item("Column2") = dtRow.Item("employeecode")
    '            rpt_Row.Item("Column3") = dtRow.Item("employeename")
    '            rpt_Row.Item("Column4") = dtRow.Item("job_name")

    '            If strPrevKey <> dtRow.Item("employeecode").ToString Then
    '                If IsDBNull(dtRow.Item("appointeddate")) = False Then
    '                    dtAppointment = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("termination_from_date")) = False Then
    '                    dtTerminationFrom = eZeeDate.convertDate(dtRow.Item("termination_from_date").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("termination_to_date")) = False Then
    '                    dtTerminationTo = eZeeDate.convertDate(dtRow.Item("termination_to_date").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("empl_enddate")) = False Then
    '                    dtEmplEnd = eZeeDate.convertDate(dtRow.Item("empl_enddate").ToString)
    '                End If

    '                mdtStartDate = IIf(dtAppointment.Date > mdtPeriodStartDate, dtAppointment.Date, mdtPeriodStartDate)
    '                mdtLastDateOfWorking = mdtPeriodEndDate
    '                If dtTerminationFrom.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtTerminationFrom.Date < mdtPeriodEndDate, dtTerminationFrom.Date, mdtPeriodEndDate)
    '                End If
    '                If dtTerminationTo.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtTerminationTo.Date < mdtLastDateOfWorking, dtTerminationTo.Date, mdtLastDateOfWorking)
    '                End If
    '                If dtEmplEnd.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtEmplEnd.Date < mdtLastDateOfWorking, dtEmplEnd.Date, mdtLastDateOfWorking)
    '                End If
    '                intWorkingDaysInPeriod = DateDiff(DateInterval.Day, mdtStartDate, mdtLastDateOfWorking.AddDays(1))
    '            End If

    '            rpt_Row.Item("Column5") = mdtLastDateOfWorking.ToShortDateString
    '            rpt_Row.Item("Column6") = intWorkingDaysInPeriod

    '            rpt_Row.Item("Column7") = dtRow.Item("HeadCode")
    '            rpt_Row.Item("Column8") = dtRow.Item("HeadName")


    '            decHeadsTotal += CDec(Format(CDec(dtRow.Item("Amount")), mstrfmtCurrency))

    '            rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("Amount")), mstrfmtCurrency)

    '            rpt_Row.Item("Column10") = Format(decHeadsTotal, mstrfmtCurrency)


    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            strPrevKey = dtRow.Item("employeecode").ToString

    '        Next

    '        '*** OT Details
    '        objDataOperation.ClearParameters()

    '        StrQ = "SELECT  hremployee_master.employeeunkid " & _
    '                      ", employeecode " & _
    '                      ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' '  + ISNULL(surname, '') AS employeename " & _
    '                      ", CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
    '                      ", CONVERT(CHAR(8), termination_from_date, 112) AS termination_from_date " & _
    '                      ", CONVERT(CHAR(8), termination_to_date, 112) AS termination_to_date " & _
    '                      ", CONVERT(CHAR(8), empl_enddate, 112) AS empl_enddate " & _
    '                      ", ISNULL(tableGross.trnheadcode, '') AS GrossHeadCode " & _
    '                      ", ISNULL(tableGross.trnheadname, '') AS GrossHeadName " & _
    '                      ", ISNULL(tableGross.amount, 0) AS GrossAmount " & _
    '                      ", ISNULL(tableOT1Amount.trnheadcode, '') AS OT1HeadCode " & _
    '                      ", ISNULL(tableOT1Amount.trnheadname, '') AS OT1HeadName " & _
    '                      ", ISNULL(tableOT1Amount.amount, 0) AS OT1Amount " & _
    '                      ", ISNULL(tableOT1Hours.amount, 0) AS OT1Hrs " & _
    '                      ", ISNULL(tableOT2Amount.trnheadcode, '') AS OT2HeadCode " & _
    '                      ", ISNULL(tableOT2Amount.trnheadname, '') AS OT2HeadName " & _
    '                      ", ISNULL(tableOT2Amount.amount, 0) AS OT2Amount " & _
    '                      ", ISNULL(tableOT2Hours.amount, 0) AS OT2Hrs " & _
    '                      ", ISNULL(tableOT3Amount.trnheadcode, '') AS OT3HeadCode " & _
    '                      ", ISNULL(tableOT3Amount.trnheadname, '') AS OT3HeadName " & _
    '                      ", ISNULL(tableOT3Amount.amount, 0) AS OT3Amount " & _
    '                      ", ISNULL(tableOT3Hours.amount, 0) AS OT3Hrs " & _
    '                      ", ISNULL(tableOT4Amount.trnheadcode, '') AS OT4HeadCode " & _
    '                      ", ISNULL(tableOT4Amount.trnheadname, '') AS OT4HeadName " & _
    '                      ", ISNULL(tableOT4Amount.amount, 0) AS OT4Amount " & _
    '                      ", ISNULL(tableOT4Hours.amount, 0) AS OT4Hrs " & _
    '                "FROM    hremployee_master " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @grossheadunkid "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableGross ON hremployee_master.employeeunkid = tableGross.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @OT1AmountID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableOT1Amount ON hremployee_master.employeeunkid = tableOT1Amount.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @OT1HoursID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableOT1Hours ON hremployee_master.employeeunkid = tableOT1Hours.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @OT2AmountID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableOT2Amount ON hremployee_master.employeeunkid = tableOT2Amount.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @OT2HoursID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableOT2Hours ON hremployee_master.employeeunkid = tableOT2Hours.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @OT3AmountID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableOT3Amount ON hremployee_master.employeeunkid = tableOT3Amount.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @OT3HoursID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableOT3Hours ON hremployee_master.employeeunkid = tableOT3Hours.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @OT4AmountID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableOT4Amount ON hremployee_master.employeeunkid = tableOT4Amount.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @OT4HoursID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableOT4Hours ON hremployee_master.employeeunkid = tableOT4Hours.employeeunkid " & _
    '                "WHERE 1 = 1 "

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
    '        End If


    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        OT_Data = New ArutiReport.Designer.dsArutiReport

    '        strPrevKey = ""
    '        dtAppointment = Nothing
    '        dtTerminationFrom = Nothing
    '        dtTerminationTo = Nothing
    '        dtEmplEnd = Nothing
    '        mdtStartDate = Nothing
    '        mdtLastDateOfWorking = Nothing
    '        intWorkingDaysInPeriod = 0

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            Dim rpt_Row As DataRow
    '            rpt_Row = OT_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
    '            rpt_Row.Item("Column2") = dtRow.Item("employeecode")
    '            rpt_Row.Item("Column3") = dtRow.Item("employeename")

    '            If strPrevKey <> dtRow.Item("employeecode").ToString Then
    '                If IsDBNull(dtRow.Item("appointeddate")) = False Then
    '                    dtAppointment = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("termination_from_date")) = False Then
    '                    dtTerminationFrom = eZeeDate.convertDate(dtRow.Item("termination_from_date").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("termination_to_date")) = False Then
    '                    dtTerminationTo = eZeeDate.convertDate(dtRow.Item("termination_to_date").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("empl_enddate")) = False Then
    '                    dtEmplEnd = eZeeDate.convertDate(dtRow.Item("empl_enddate").ToString)
    '                End If

    '                mdtStartDate = IIf(dtAppointment.Date > mdtPeriodStartDate, dtAppointment.Date, mdtPeriodStartDate)
    '                mdtLastDateOfWorking = mdtPeriodEndDate
    '                If dtTerminationFrom.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtTerminationFrom.Date < mdtPeriodEndDate, dtTerminationFrom.Date, mdtPeriodEndDate)
    '                End If
    '                If dtTerminationTo.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtTerminationTo.Date < mdtLastDateOfWorking, dtTerminationTo.Date, mdtLastDateOfWorking)
    '                End If
    '                If dtEmplEnd.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtEmplEnd.Date < mdtLastDateOfWorking, dtEmplEnd.Date, mdtLastDateOfWorking)
    '                End If
    '                intWorkingDaysInPeriod = DateDiff(DateInterval.Day, mdtStartDate, mdtLastDateOfWorking.AddDays(1))
    '            End If

    '            rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("GrossAmount")), mstrfmtCurrency)
    '            mdecNetToPay += CDec(Format(CDec(dtRow.Item("GrossAmount")), mstrfmtCurrency))

    '            rpt_Row.Item("Column5") = mdtLastDateOfWorking.ToShortDateString
    '            rpt_Row.Item("Column6") = intWorkingDaysInPeriod

    '            If mintOT1AmountHeadID > 0 AndAlso dtRow.Item("OT1HeadCode").ToString.Trim <> "" Then
    '                rpt_Row.Item("Column7") = dtRow.Item("OT1HeadCode")
    '                rpt_Row.Item("Column8") = dtRow.Item("OT1HeadName")
    '                rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("OT1Hrs")), mstrfmtCurrency)
    '                If CDec(dtRow.Item("OT1Hrs")) = 0 Then
    '                    rpt_Row.Item("Column10") = Format(0, mstrfmtCurrency)
    '                Else
    '                    rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("OT1Amount")) / CDec(dtRow.Item("OT1Hrs")), mstrfmtCurrency)
    '                End If
    '                rpt_Row.Item("Column11") = Format(CDec(dtRow.Item("OT1Amount")), mstrfmtCurrency)
    '                mdecNetToPay += CDec(Format(CDec(dtRow.Item("OT1Amount")), mstrfmtCurrency))
    '            Else
    '                rpt_Row.Item("Column7") = ""
    '                rpt_Row.Item("Column8") = ""
    '                rpt_Row.Item("Column9") = ""
    '                rpt_Row.Item("Column10") = ""
    '                rpt_Row.Item("Column11") = ""
    '            End If

    '            If mintOT2AmountHeadID > 0 AndAlso dtRow.Item("OT2HeadCode").ToString.Trim <> "" Then
    '                rpt_Row.Item("Column12") = dtRow.Item("OT2HeadCode")
    '                rpt_Row.Item("Column13") = dtRow.Item("OT2HeadName")
    '                rpt_Row.Item("Column14") = Format(CDec(dtRow.Item("OT2Hrs")), mstrfmtCurrency)
    '                If CDec(dtRow.Item("OT2Hrs")) = 0 Then
    '                    rpt_Row.Item("Column15") = Format(0, mstrfmtCurrency)
    '                Else
    '                    rpt_Row.Item("Column15") = Format(CDec(dtRow.Item("OT2Amount")) / CDec(dtRow.Item("OT2Hrs")), mstrfmtCurrency)
    '                End If
    '                rpt_Row.Item("Column16") = Format(CDec(dtRow.Item("OT2Amount")), mstrfmtCurrency)
    '                mdecNetToPay += CDec(Format(CDec(dtRow.Item("OT2Amount")), mstrfmtCurrency))
    '            Else
    '                rpt_Row.Item("Column12") = ""
    '                rpt_Row.Item("Column13") = ""
    '                rpt_Row.Item("Column14") = ""
    '                rpt_Row.Item("Column15") = ""
    '                rpt_Row.Item("Column16") = ""
    '            End If

    '            If mintOT3AmountHeadID > 0 AndAlso dtRow.Item("OT3HeadCode").ToString.Trim <> "" Then
    '                rpt_Row.Item("Column17") = dtRow.Item("OT3HeadCode")
    '                rpt_Row.Item("Column18") = dtRow.Item("OT3HeadName")
    '                rpt_Row.Item("Column19") = Format(CDec(dtRow.Item("OT3Hrs")), mstrfmtCurrency)
    '                If CDec(dtRow.Item("OT3Hrs")) = 0 Then
    '                    rpt_Row.Item("Column20") = Format(0, mstrfmtCurrency)
    '                Else
    '                    rpt_Row.Item("Column20") = Format(CDec(dtRow.Item("OT3Amount")) / CDec(dtRow.Item("OT3Hrs")), mstrfmtCurrency)
    '                End If
    '                rpt_Row.Item("Column21") = Format(CDec(dtRow.Item("OT3Amount")), mstrfmtCurrency)
    '                mdecNetToPay += CDec(Format(CDec(dtRow.Item("OT3Amount")), mstrfmtCurrency))
    '            Else
    '                rpt_Row.Item("Column17") = ""
    '                rpt_Row.Item("Column18") = ""
    '                rpt_Row.Item("Column19") = ""
    '                rpt_Row.Item("Column20") = ""
    '                rpt_Row.Item("Column21") = ""
    '            End If

    '            If mintOT4AmountHeadID > 0 AndAlso dtRow.Item("OT4HeadCode").ToString.Trim <> "" Then
    '                rpt_Row.Item("Column22") = dtRow.Item("OT4HeadCode")
    '                rpt_Row.Item("Column23") = dtRow.Item("OT4HeadName")
    '                rpt_Row.Item("Column24") = Format(CDec(dtRow.Item("OT4Hrs")), mstrfmtCurrency)
    '                If CDec(dtRow.Item("OT4Hrs")) = 0 Then
    '                    rpt_Row.Item("Column25") = Format(0, mstrfmtCurrency)
    '                Else
    '                    rpt_Row.Item("Column25") = Format(CDec(dtRow.Item("OT4Amount")) / CDec(dtRow.Item("OT4Hrs")), mstrfmtCurrency)
    '                End If
    '                rpt_Row.Item("Column26") = Format(CDec(dtRow.Item("OT4Amount")), mstrfmtCurrency)
    '                mdecNetToPay += CDec(Format(CDec(dtRow.Item("OT4Amount")), mstrfmtCurrency))
    '            Else
    '                rpt_Row.Item("Column22") = ""
    '                rpt_Row.Item("Column23") = ""
    '                rpt_Row.Item("Column24") = ""
    '                rpt_Row.Item("Column25") = ""
    '                rpt_Row.Item("Column26") = ""
    '            End If


    '            OT_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            strPrevKey = dtRow.Item("employeecode").ToString

    '        Next


    '        '*** Leave Details
    '        objDataOperation.ClearParameters()

    '        StrQ = "SELECT  hremployee_master.employeeunkid " & _
    '                     ", employeecode " & _
    '                     ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' '  + ISNULL(surname, '') AS employeename " & _
    '                     ", CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
    '                     ", CONVERT(CHAR(8), termination_from_date, 112) AS termination_from_date " & _
    '                     ", CONVERT(CHAR(8), termination_to_date, 112) AS termination_to_date " & _
    '                     ", CONVERT(CHAR(8), empl_enddate, 112) AS empl_enddate " & _
    '                     ", ISNULL(hrjob_master.job_code, '') AS job_code " & _
    '                     ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
    '                     ", ISNULL(tableLeave.trnheadcode, '') AS HeadCode " & _
    '                     ", ISNULL(tableLeave.trnheadname, '') AS HeadName " & _
    '                     ", ISNULL(tableLeave.amount, 0) AS Amount " & _
    '               "FROM    hremployee_master " & _
    '                       "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
    '                       "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                         ", trnheadcode " & _
    '                                         ", trnheadname " & _
    '                                         ", amount " & _
    '                                   "FROM    prpayrollprocess_tran " & _
    '                                           "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                           "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                           "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                   "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                           "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                       "AND prtranhead_master.tranheadunkid = @LeaveBalanceHeadID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableLeave ON hremployee_master.employeeunkid = tableLeave.employeeunkid " & _
    '                "WHERE 1 = 1 "

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
    '        End If

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        Dim objLeaveBalance As New clsleavebalance_tran
    '        Dim dsLeaveList As DataSet = Nothing

    '        Leave_Data = New ArutiReport.Designer.dsArutiReport

    '        strPrevKey = ""
    '        dtAppointment = Nothing
    '        dtTerminationFrom = Nothing
    '        dtTerminationTo = Nothing
    '        dtEmplEnd = Nothing
    '        mdtStartDate = Nothing
    '        mdtLastDateOfWorking = Nothing
    '        intWorkingDaysInPeriod = 0

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            Dim rpt_Row As DataRow
    '            rpt_Row = Leave_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
    '            rpt_Row.Item("Column2") = dtRow.Item("employeecode")
    '            rpt_Row.Item("Column3") = dtRow.Item("employeename")
    '            rpt_Row.Item("Column4") = dtRow.Item("job_name")

    '            If strPrevKey <> dtRow.Item("employeecode").ToString Then
    '                If IsDBNull(dtRow.Item("appointeddate")) = False Then
    '                    dtAppointment = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("termination_from_date")) = False Then
    '                    dtTerminationFrom = eZeeDate.convertDate(dtRow.Item("termination_from_date").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("termination_to_date")) = False Then
    '                    dtTerminationTo = eZeeDate.convertDate(dtRow.Item("termination_to_date").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("empl_enddate")) = False Then
    '                    dtEmplEnd = eZeeDate.convertDate(dtRow.Item("empl_enddate").ToString)
    '                End If

    '                mdtStartDate = IIf(dtAppointment.Date > mdtPeriodStartDate, dtAppointment.Date, mdtPeriodStartDate)
    '                mdtLastDateOfWorking = mdtPeriodEndDate
    '                If dtTerminationFrom.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtTerminationFrom.Date < mdtPeriodEndDate, dtTerminationFrom.Date, mdtPeriodEndDate)
    '                End If
    '                If dtTerminationTo.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtTerminationTo.Date < mdtLastDateOfWorking, dtTerminationTo.Date, mdtLastDateOfWorking)
    '                End If
    '                If dtEmplEnd.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtEmplEnd.Date < mdtLastDateOfWorking, dtEmplEnd.Date, mdtLastDateOfWorking)
    '                End If
    '                intWorkingDaysInPeriod = DateDiff(DateInterval.Day, mdtStartDate, mdtLastDateOfWorking.AddDays(1))
    '            End If

    '            rpt_Row.Item("Column5") = mdtLastDateOfWorking.ToShortDateString
    '            rpt_Row.Item("Column6") = intWorkingDaysInPeriod

    '            'Sohail (07 Jan 2014) -- Start
    '            'Enhancement - Separate TnA Periods from Payroll Periods
    '            'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtPeriodEndDate, mintLeaveTypeID, CInt(dtRow.Item("employeeunkid")), mdtDatabaseStartDate, mdtDatabaseEndDate, , , mintLeaveBalanceSetting, mintIsFin_Close)

    '            'Pinkal (24-May-2014) -- Start
    '            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
    '            'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtTnAEndDate, mintLeaveTypeID, CInt(dtRow.Item("employeeunkid")), mdtDatabaseStartDate, mdtDatabaseEndDate, , , mintLeaveBalanceSetting, mintIsFin_Close)

    '            'Pinkal (21-Jul-2014) -- Start
    '            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    '            'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtTnAEndDate, mintLeaveTypeID, CInt(dtRow.Item("employeeunkid")), mdtDatabaseStartDate, mdtDatabaseEndDate, , , mintLeaveBalanceSetting, mintIsFin_Close, mintYearUnkId)
    '            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtTnAEndDate, mintLeaveTypeID, CInt(dtRow.Item("employeeunkid")), mdtDatabaseStartDate, mdtDatabaseEndDate, , , mintLeaveBalanceSetting, mintIsFin_Close, mintYearUnkId)
    '            ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtTnAEndDate, mintLeaveTypeID, CInt(dtRow.Item("employeeunkid")), mdtDatabaseStartDate, mdtDatabaseEndDate, True, True, mintLeaveBalanceSetting, mintIsFin_Close, mintYearUnkId)
    '            End If
    '            'Pinkal (21-Jul-2014) -- End


    '            'Pinkal (24-May-2014) -- End


    '            'Sohail (07 Jan 2014) -- End

    '            rpt_Row.Item("Column7") = mstrLeaveTypeName & " - " & Language.getMessage(mstrModuleName, 49, "No. of Days Leave Credit as of")
    '            If dsLeaveList.Tables(0).Rows.Count > 0 Then
    '                rpt_Row.Item("Column8") = Format(CDec(dsLeaveList.Tables(0).Rows(0).Item("Balance")), mstrfmtCurrency)
    '            Else
    '                rpt_Row.Item("Column8") = Format(0, mstrfmtCurrency)
    '            End If

    '            rpt_Row.Item("Column9") = Format(dtRow.Item("Amount"), mstrfmtCurrency)
    '            mdecNetToPay += CDec(Format(dtRow.Item("Amount"), mstrfmtCurrency))

    '            Leave_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            strPrevKey = dtRow.Item("employeecode").ToString

    '        Next


    '        '*** Gratuity Details
    '        objDataOperation.ClearParameters()

    '        StrQ = "SELECT  hremployee_master.employeeunkid " & _
    '                      ", employeecode " & _
    '                      ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' '  + ISNULL(surname, '') AS employeename " & _
    '                      ", CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
    '                      ", CONVERT(CHAR(8), termination_from_date, 112) AS termination_from_date " & _
    '                      ", CONVERT(CHAR(8), termination_to_date, 112) AS termination_to_date " & _
    '                      ", CONVERT(CHAR(8), empl_enddate, 112) AS empl_enddate " & _
    '                      ", ISNULL(tableFirst.trnheadcode, '') AS FirstHeadCode " & _
    '                      ", ISNULL(tableFirst.trnheadname, '') AS FirstHeadName " & _
    '                      ", ISNULL(tableFirst.amount, 0) AS FirstAmount " & _
    '                      ", ISNULL(tableYear.trnheadcode, '') AS YearHeadCode " & _
    '                      ", ISNULL(tableYear.trnheadname, '') AS YearHeadName " & _
    '                      ", ISNULL(tableYear.amount, 0) AS YearAmount " & _
    '                      ", ISNULL(tableMonth.trnheadcode, '') AS MonthHeadCode " & _
    '                      ", ISNULL(tableMonth.trnheadname, '') AS MonthHeadName " & _
    '                      ", ISNULL(tableMonth.amount, 0) AS MonthAmount " & _
    '                      ", ISNULL(tableDays.trnheadcode, '') AS DaysHeadCode " & _
    '                      ", ISNULL(tableDays.trnheadname, '') AS DaysHeadName " & _
    '                      ", ISNULL(tableDays.amount, 0) AS DaysAmount " & _
    '                      ", ISNULL(tableYearAmt.trnheadcode, '') AS YearAmtHeadCode " & _
    '                      ", ISNULL(tableYearAmt.trnheadname, '') AS YearAmtHeadName " & _
    '                      ", ISNULL(tableYearAmt.amount, 0) AS YearAmtAmount " & _
    '                      ", ISNULL(tableMonthAmt.trnheadcode, '') AS MonthAmtHeadCode " & _
    '                      ", ISNULL(tableMonthAmt.trnheadname, '') AS MonthAmtHeadName " & _
    '                      ", ISNULL(tableMonthAmt.amount, 0) AS MonthAmtAmount " & _
    '                      ", ISNULL(tableDaysAmt.trnheadcode, '') AS DaysAmtHeadCode " & _
    '                      ", ISNULL(tableDaysAmt.trnheadname, '') AS DaysAmtHeadName " & _
    '                      ", ISNULL(tableDaysAmt.amount, 0) AS DaysAmtAmount " & _
    '                "FROM    hremployee_master " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @First3YearsID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If


    '        StrQ &= ") AS tableFirst ON hremployee_master.employeeunkid = tableFirst.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @BalanceYearID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableYear ON hremployee_master.employeeunkid = tableYear.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @BalanceMonthID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableMonth ON hremployee_master.employeeunkid = tableMonth.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @BalanceDaysID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableDays ON hremployee_master.employeeunkid = tableDays.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @BalanceYearAmtID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableYearAmt ON hremployee_master.employeeunkid = tableYearAmt.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @BalanceMonthAmtID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableMonthAmt ON hremployee_master.employeeunkid = tableMonthAmt.employeeunkid " & _
    '                        "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid " & _
    '                                          ", trnheadcode " & _
    '                                          ", trnheadname " & _
    '                                          ", amount " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtranhead_master.tranheadunkid = @BalanceDaysAmtID "

    '        If mintPeriodId > 0 Then
    '            StrQ &= " AND payperiodunkid = @periodunkid "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        StrQ &= ") AS tableDaysAmt ON hremployee_master.employeeunkid = tableDaysAmt.employeeunkid " & _
    '                "WHERE 1 = 1 "

    '        If mintEmployeeId > 0 Then
    '            StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
    '        End If

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If


    '        Gratuity_Data = New ArutiReport.Designer.dsArutiReport

    '        strPrevKey = ""
    '        dtAppointment = Nothing
    '        dtTerminationFrom = Nothing
    '        dtTerminationTo = Nothing
    '        dtEmplEnd = Nothing
    '        mdtStartDate = Nothing
    '        mdtLastDateOfWorking = Nothing
    '        intWorkingDaysInPeriod = 0

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            Dim rpt_Row As DataRow
    '            rpt_Row = Gratuity_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
    '            rpt_Row.Item("Column2") = dtRow.Item("employeecode")
    '            rpt_Row.Item("Column3") = dtRow.Item("employeename")
    '            'rpt_Row.Item("Column4") = dtRow.Item("job_name")

    '            If strPrevKey <> dtRow.Item("employeecode").ToString Then
    '                If IsDBNull(dtRow.Item("appointeddate")) = False Then
    '                    dtAppointment = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("termination_from_date")) = False Then
    '                    dtTerminationFrom = eZeeDate.convertDate(dtRow.Item("termination_from_date").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("termination_to_date")) = False Then
    '                    dtTerminationTo = eZeeDate.convertDate(dtRow.Item("termination_to_date").ToString)
    '                End If
    '                If IsDBNull(dtRow.Item("empl_enddate")) = False Then
    '                    dtEmplEnd = eZeeDate.convertDate(dtRow.Item("empl_enddate").ToString)
    '                End If

    '                mdtStartDate = IIf(dtAppointment.Date > mdtPeriodStartDate, dtAppointment.Date, mdtPeriodStartDate)
    '                mdtLastDateOfWorking = mdtPeriodEndDate
    '                If dtTerminationFrom.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtTerminationFrom.Date < mdtPeriodEndDate, dtTerminationFrom.Date, mdtPeriodEndDate)
    '                End If
    '                If dtTerminationTo.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtTerminationTo.Date < mdtLastDateOfWorking, dtTerminationTo.Date, mdtLastDateOfWorking)
    '                End If
    '                If dtEmplEnd.Date <> Nothing Then
    '                    mdtLastDateOfWorking = IIf(dtEmplEnd.Date < mdtLastDateOfWorking, dtEmplEnd.Date, mdtLastDateOfWorking)
    '                End If
    '                intWorkingDaysInPeriod = DateDiff(DateInterval.Day, mdtStartDate, mdtLastDateOfWorking.AddDays(1))
    '            End If

    '            rpt_Row.Item("Column5") = dtAppointment.Date.ToShortDateString
    '            rpt_Row.Item("Column6") = mdtLastDateOfWorking.ToShortDateString

    '            rpt_Row.Item("Column7") = dtRow.Item("FirstHeadCode")
    '            rpt_Row.Item("Column8") = dtRow.Item("FirstHeadName")
    '            rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("Firstamount")), mstrfmtCurrency)
    '            mdecNetToPay += CDec(Format(CDec(dtRow.Item("Firstamount")), mstrfmtCurrency))

    '            rpt_Row.Item("Column10") = dtRow.Item("YearAmtHeadCode")
    '            rpt_Row.Item("Column11") = dtRow.Item("YearAmtHeadName")
    '            rpt_Row.Item("Column12") = Format(CDec(dtRow.Item("YearAmtamount")), mstrfmtCurrency)
    '            mdecNetToPay += CDec(Format(CDec(dtRow.Item("YearAmtamount")), mstrfmtCurrency))

    '            rpt_Row.Item("Column13") = dtRow.Item("MonthAmtHeadCode")
    '            rpt_Row.Item("Column14") = dtRow.Item("MonthAmtHeadName")
    '            rpt_Row.Item("Column15") = Format(CDec(dtRow.Item("MonthAmtamount")), mstrfmtCurrency)
    '            mdecNetToPay += CDec(Format(CDec(dtRow.Item("MonthAmtamount")), mstrfmtCurrency))

    '            rpt_Row.Item("Column16") = dtRow.Item("DaysAmtHeadCode")
    '            rpt_Row.Item("Column17") = dtRow.Item("DaysAmtHeadName")
    '            rpt_Row.Item("Column18") = Format(CDec(dtRow.Item("DaysAmtamount")), mstrfmtCurrency)
    '            mdecNetToPay += CDec(Format(CDec(dtRow.Item("DaysAmtamount")), mstrfmtCurrency))

    '            rpt_Row.Item("Column19") = dtRow.Item("YearHeadCode")
    '            rpt_Row.Item("Column20") = dtRow.Item("YearHeadName")
    '            rpt_Row.Item("Column21") = Format(CDec(dtRow.Item("Yearamount")), mstrfmtCurrency)

    '            rpt_Row.Item("Column22") = dtRow.Item("MonthHeadCode")
    '            rpt_Row.Item("Column23") = dtRow.Item("MonthHeadName")
    '            rpt_Row.Item("Column24") = Format(CDec(dtRow.Item("Monthamount")), mstrfmtCurrency)

    '            rpt_Row.Item("Column25") = dtRow.Item("DaysHeadCode")
    '            rpt_Row.Item("Column26") = dtRow.Item("DaysHeadName")
    '            rpt_Row.Item("Column27") = Format(CDec(dtRow.Item("Daysamount")), mstrfmtCurrency)

    '            Gratuity_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            strPrevKey = dtRow.Item("employeecode").ToString

    '        Next

    '        '******************
    '        objRpt = New ArutiReport.Designer.rptEndOfService


    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        'If mintViewIndex <= 0 Then
    '        '    Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
    '        '    Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
    '        'End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 2, "Prepared By"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 24, "Verified By"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 25, "Approved By"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 8, "Received By"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        Call ReportFunction.TextChange(objRpt, "lblEmployeeSign", Language.getMessage(mstrModuleName, 28, "Employee Sign"))

    '        objRpt.SetDataSource(rpt_Data)
    '        objRpt.Subreports("rptEosOT").SetDataSource(OT_Data)
    '        objRpt.Subreports("rptEosLeave").SetDataSource(Leave_Data)
    '        objRpt.Subreports("rptEosGratuity").SetDataSource(Gratuity_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 22, "Emp No."))
    '        Call ReportFunction.TextChange(objRpt, "txtSurName", Language.getMessage(mstrModuleName, 29, "Salary Details"))
    '        Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 11, "First Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtInitial", Language.getMessage(mstrModuleName, 12, "Initial"))
    '        Call ReportFunction.TextChange(objRpt, "txtReferenceNo", Language.getMessage(mstrModuleName, 14, "Reference No."))
    '        Call ReportFunction.TextChange(objRpt, "lblNetToPay", Language.getMessage(mstrModuleName, 30, "Net To Pay"))
    '        Call ReportFunction.TextChange(objRpt, "txtNetToPay", Format(mdecNetToPay, mstrfmtCurrency))

    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosOT"), "lblSalaryPeriod", Language.getMessage(mstrModuleName, 48, "Salary for the Month :"))
    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosOT"), "txtSalaryPeriod", mstrPeriodName)
    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosOT"), "lblNoOfDays", Language.getMessage(mstrModuleName, 50, "No. Of Days :"))
    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosOT"), "lblHour", Language.getMessage(mstrModuleName, 51, "Hours"))
    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosOT"), "lblHourRate", Language.getMessage(mstrModuleName, 52, "Hourly Rate"))

    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosLeave"), "lblLeaveDetail", Language.getMessage(mstrModuleName, 53, "Leave Wages /Saalry"))
    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosLeave"), "txtLeaveBalance", Language.getMessage(mstrModuleName, 54, "(Leave Balance)"))

    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblGratuity", Language.getMessage(mstrModuleName, 55, "Gratuity"))
    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblJoiningDate", Language.getMessage(mstrModuleName, 56, "Date of Joining :"))
    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblRelievingDate", Language.getMessage(mstrModuleName, 57, "Date of Relieving :"))
    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblForFirstThreeYears", Language.getMessage(mstrModuleName, 58, "For First Three Years :"))
    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblBalanceYears", Language.getMessage(mstrModuleName, 59, "Balance Years :"))
    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblBalanceMonths", Language.getMessage(mstrModuleName, 60, "Balance Months :"))
    '        Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblBalanceDays", Language.getMessage(mstrModuleName, 61, "Balance Days :"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 17, "Page :"))

    '        'Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 25, "Period :"))
    '        'Call ReportFunction.TextChange(objRpt, "txtPeriodTotal", Language.getMessage(mstrModuleName, 26, "Period Total:"))
    '        'Call ReportFunction.TextChange(objRpt, "txtPeriodCount", Language.getMessage(mstrModuleName, 27, "Period Count:"))



    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


    '        If menExportAction = enExportAction.ExcelExtra Then
    '            mdtTableExcel = rpt_Data.Tables(0)

    '            Dim mintColumn As Integer = 0

    '            mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 22, "Emp No.")
    '            mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 10, "Surname")
    '            mdtTableExcel.Columns("Column3").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 11, "First Name")
    '            mdtTableExcel.Columns("Column4").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 12, "Initial")
    '            mdtTableExcel.Columns("Column5").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 13, "Amount")
    '            mdtTableExcel.Columns("Column81").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 14, "Reference No.")
    '            mdtTableExcel.Columns("Column7").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 4, "Period :")
    '            mdtTableExcel.Columns("Column10").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            If mintViewIndex > 0 Then
    '                mdtTableExcel.Columns("Column1").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
    '                mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
    '                mintColumn += 1
    '            End If


    '            For i = mintColumn To mdtTableExcel.Columns.Count - 1
    '                mdtTableExcel.Columns.RemoveAt(mintColumn)
    '            Next


    '        End If

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try

    'End Function

    Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrInnerQ As String = ""
        Dim strDatabaseName As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim OT_Data As ArutiReport.Designer.dsArutiReport
        Dim Leave_Data As ArutiReport.Designer.dsArutiReport
        Dim Gratuity_Data As ArutiReport.Designer.dsArutiReport
        Dim mdecNetToPay As Decimal = 0

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            '*** Salary Details
            StrQ = "SELECT  hremployee_master.employeeunkid " & _
                          ", employeecode "
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' '  + ISNULL(othername, '') AS employeename "
            Else
                StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' '  + ISNULL(surname, '') AS employeename "
            End If

            StrQ &= "   ,CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
                    "   ,CONVERT(CHAR(8), EocDt.termination_from_date, 112) AS termination_from_date " & _
                    "   ,CONVERT(CHAR(8), RetrDt.termination_to_date, 112) AS termination_to_date " & _
                    "   ,CONVERT(CHAR(8), EocDt.empl_enddate, 112) AS empl_enddate " & _
                    "   ,ISNULL(hrjob_master.job_code, '') AS job_code " & _
                    "   ,ISNULL(hrjob_master.job_name, '') AS job_name " & _
                    "   ,ISNULL(tableOrigin.trnheadcode, '') AS HeadCode " & _
                    "   ,ISNULL(tableOrigin.trnheadname, '') AS HeadName " & _
                    "   ,ISNULL(tableOrigin.amount, 0) AS Amount " & _
                    "FROM hremployee_master " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            jobunkid " & _
                    "           ,jobgroupunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "   LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            date1 AS empl_enddate " & _
                    "           ,date2 AS termination_from_date " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_dates_tran " & _
                    "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    "   )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            date1 AS termination_to_date " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_dates_tran " & _
                    "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    "   )AS RetrDt ON RetrDt.employeeunkid = hremployee_master.employeeunkid AND RetrDt.rno = 1 " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid IN ( " & mstrOriginalHeadIDs & " ) "
            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableOrigin ON hremployee_master.employeeunkid = tableOrigin.employeeunkid " & _
                    "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim strPrevKey As String = ""
            Dim decHeadsTotal As Decimal = 0
            Dim dtAppointment As Date = Nothing
            Dim dtTerminationFrom As Date = Nothing
            Dim dtTerminationTo As Date = Nothing
            Dim dtEmplEnd As Date = Nothing
            Dim mdtStartDate As Date = Nothing
            Dim mdtLastDateOfWorking As Date = Nothing
            Dim intWorkingDaysInPeriod As Integer = 0

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employeename")
                rpt_Row.Item("Column4") = dtRow.Item("job_name")

                If strPrevKey <> dtRow.Item("employeecode").ToString Then
                    If IsDBNull(dtRow.Item("appointeddate")) = False Then
                        dtAppointment = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString)
                    End If
                    If IsDBNull(dtRow.Item("termination_from_date")) = False Then
                        dtTerminationFrom = eZeeDate.convertDate(dtRow.Item("termination_from_date").ToString)
                    End If
                    If IsDBNull(dtRow.Item("termination_to_date")) = False Then
                        dtTerminationTo = eZeeDate.convertDate(dtRow.Item("termination_to_date").ToString)
                    End If
                    If IsDBNull(dtRow.Item("empl_enddate")) = False Then
                        dtEmplEnd = eZeeDate.convertDate(dtRow.Item("empl_enddate").ToString)
                    End If

                    mdtStartDate = IIf(dtAppointment.Date > mdtPeriodStartDate, dtAppointment.Date, mdtPeriodStartDate)
                    mdtLastDateOfWorking = mdtPeriodEndDate
                    If dtTerminationFrom.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtTerminationFrom.Date < mdtPeriodEndDate, dtTerminationFrom.Date, mdtPeriodEndDate)
                    End If
                    If dtTerminationTo.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtTerminationTo.Date < mdtLastDateOfWorking, dtTerminationTo.Date, mdtLastDateOfWorking)
                    End If
                    If dtEmplEnd.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtEmplEnd.Date < mdtLastDateOfWorking, dtEmplEnd.Date, mdtLastDateOfWorking)
                    End If
                    intWorkingDaysInPeriod = DateDiff(DateInterval.Day, mdtStartDate, mdtLastDateOfWorking.AddDays(1))
                End If

                rpt_Row.Item("Column5") = mdtLastDateOfWorking.ToShortDateString
                rpt_Row.Item("Column6") = intWorkingDaysInPeriod

                rpt_Row.Item("Column7") = dtRow.Item("HeadCode")
                rpt_Row.Item("Column8") = dtRow.Item("HeadName")


                decHeadsTotal += CDec(Format(CDec(dtRow.Item("Amount")), mstrfmtCurrency))

                rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("Amount")), mstrfmtCurrency)

                rpt_Row.Item("Column10") = Format(decHeadsTotal, mstrfmtCurrency)


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                strPrevKey = dtRow.Item("employeecode").ToString

            Next

            '*** OT Details
            objDataOperation.ClearParameters()

            StrQ = "SELECT " & _
                   "     hremployee_master.employeeunkid " & _
                   "    ,employeecode " & _
                   "    ,ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' '  + ISNULL(surname, '') AS employeename " & _
                   "    ,CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
                   "    ,CONVERT(CHAR(8), EocDt.termination_from_date, 112) AS termination_from_date " & _
                   "    ,CONVERT(CHAR(8), RetrDt.termination_to_date, 112) AS termination_to_date " & _
                   "    ,CONVERT(CHAR(8), EocDt.empl_enddate, 112) AS empl_enddate " & _
                   "    ,ISNULL(tableGross.trnheadcode, '') AS GrossHeadCode " & _
                   "    ,ISNULL(tableGross.trnheadname, '') AS GrossHeadName " & _
                   "    ,ISNULL(tableGross.amount, 0) AS GrossAmount " & _
                   "    ,ISNULL(tableOT1Amount.trnheadcode, '') AS OT1HeadCode " & _
                   "    ,ISNULL(tableOT1Amount.trnheadname, '') AS OT1HeadName " & _
                   "    ,ISNULL(tableOT1Amount.amount, 0) AS OT1Amount " & _
                   "    ,ISNULL(tableOT1Hours.amount, 0) AS OT1Hrs " & _
                   "    ,ISNULL(tableOT2Amount.trnheadcode, '') AS OT2HeadCode " & _
                   "    ,ISNULL(tableOT2Amount.trnheadname, '') AS OT2HeadName " & _
                   "    ,ISNULL(tableOT2Amount.amount, 0) AS OT2Amount " & _
                   "    ,ISNULL(tableOT2Hours.amount, 0) AS OT2Hrs " & _
                   "    ,ISNULL(tableOT3Amount.trnheadcode, '') AS OT3HeadCode " & _
                   "    ,ISNULL(tableOT3Amount.trnheadname, '') AS OT3HeadName " & _
                   "    ,ISNULL(tableOT3Amount.amount, 0) AS OT3Amount " & _
                   "    ,ISNULL(tableOT3Hours.amount, 0) AS OT3Hrs " & _
                   "    ,ISNULL(tableOT4Amount.trnheadcode, '') AS OT4HeadCode " & _
                   "    ,ISNULL(tableOT4Amount.trnheadname, '') AS OT4HeadName " & _
                   "    ,ISNULL(tableOT4Amount.amount, 0) AS OT4Amount " & _
                   "    ,ISNULL(tableOT4Hours.amount, 0) AS OT4Hrs " & _
                   "FROM hremployee_master " & _
                   "   LEFT JOIN " & _
                   "   ( " & _
                   "       SELECT " & _
                   "            date1 AS empl_enddate " & _
                   "           ,date2 AS termination_from_date " & _
                   "           ,employeeunkid " & _
                   "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "       FROM hremployee_dates_tran " & _
                   "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "   )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                   "   LEFT JOIN " & _
                   "   ( " & _
                   "       SELECT " & _
                   "            date1 AS termination_to_date " & _
                   "           ,employeeunkid " & _
                   "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "       FROM hremployee_dates_tran " & _
                   "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "    )AS RetrDt ON RetrDt.employeeunkid = hremployee_master.employeeunkid AND RetrDt.rno = 1 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             prpayrollprocess_tran.employeeunkid " & _
                   "            ,trnheadcode " & _
                   "            ,trnheadname " & _
                   "            ,amount " & _
                   "        FROM prpayrollprocess_tran " & _
                   "            LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                   "            LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                   "            LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                   "        WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                   "            AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                   "            AND prtranhead_master.tranheadunkid = @grossheadunkid "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableGross ON hremployee_master.employeeunkid = tableGross.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @OT1AmountID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableOT1Amount ON hremployee_master.employeeunkid = tableOT1Amount.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @OT1HoursID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableOT1Hours ON hremployee_master.employeeunkid = tableOT1Hours.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @OT2AmountID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableOT2Amount ON hremployee_master.employeeunkid = tableOT2Amount.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @OT2HoursID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableOT2Hours ON hremployee_master.employeeunkid = tableOT2Hours.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @OT3AmountID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableOT3Amount ON hremployee_master.employeeunkid = tableOT3Amount.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @OT3HoursID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableOT3Hours ON hremployee_master.employeeunkid = tableOT3Hours.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @OT4AmountID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableOT4Amount ON hremployee_master.employeeunkid = tableOT4Amount.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @OT4HoursID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableOT4Hours ON hremployee_master.employeeunkid = tableOT4Hours.employeeunkid " & _
                    "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If


            Call FilterTitleAndFilterQuery()

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            OT_Data = New ArutiReport.Designer.dsArutiReport

            strPrevKey = ""
            dtAppointment = Nothing
            dtTerminationFrom = Nothing
            dtTerminationTo = Nothing
            dtEmplEnd = Nothing
            mdtStartDate = Nothing
            mdtLastDateOfWorking = Nothing
            intWorkingDaysInPeriod = 0

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = OT_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employeename")

                If strPrevKey <> dtRow.Item("employeecode").ToString Then
                    If IsDBNull(dtRow.Item("appointeddate")) = False Then
                        dtAppointment = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString)
                    End If
                    If IsDBNull(dtRow.Item("termination_from_date")) = False Then
                        dtTerminationFrom = eZeeDate.convertDate(dtRow.Item("termination_from_date").ToString)
                    End If
                    If IsDBNull(dtRow.Item("termination_to_date")) = False Then
                        dtTerminationTo = eZeeDate.convertDate(dtRow.Item("termination_to_date").ToString)
                    End If
                    If IsDBNull(dtRow.Item("empl_enddate")) = False Then
                        dtEmplEnd = eZeeDate.convertDate(dtRow.Item("empl_enddate").ToString)
                    End If

                    mdtStartDate = IIf(dtAppointment.Date > mdtPeriodStartDate, dtAppointment.Date, mdtPeriodStartDate)
                    mdtLastDateOfWorking = mdtPeriodEndDate
                    If dtTerminationFrom.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtTerminationFrom.Date < mdtPeriodEndDate, dtTerminationFrom.Date, mdtPeriodEndDate)
                    End If
                    If dtTerminationTo.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtTerminationTo.Date < mdtLastDateOfWorking, dtTerminationTo.Date, mdtLastDateOfWorking)
                    End If
                    If dtEmplEnd.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtEmplEnd.Date < mdtLastDateOfWorking, dtEmplEnd.Date, mdtLastDateOfWorking)
                    End If
                    intWorkingDaysInPeriod = DateDiff(DateInterval.Day, mdtStartDate, mdtLastDateOfWorking.AddDays(1))
                End If

                rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("GrossAmount")), mstrfmtCurrency)
                mdecNetToPay += CDec(Format(CDec(dtRow.Item("GrossAmount")), mstrfmtCurrency))

                rpt_Row.Item("Column5") = mdtLastDateOfWorking.ToShortDateString
                rpt_Row.Item("Column6") = intWorkingDaysInPeriod

                If mintOT1AmountHeadID > 0 AndAlso dtRow.Item("OT1HeadCode").ToString.Trim <> "" Then
                    rpt_Row.Item("Column7") = dtRow.Item("OT1HeadCode")
                    rpt_Row.Item("Column8") = dtRow.Item("OT1HeadName")
                    rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("OT1Hrs")), mstrfmtCurrency)
                    If CDec(dtRow.Item("OT1Hrs")) = 0 Then
                        rpt_Row.Item("Column10") = Format(0, mstrfmtCurrency)
                    Else
                        rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("OT1Amount")) / CDec(dtRow.Item("OT1Hrs")), mstrfmtCurrency)
                    End If
                    rpt_Row.Item("Column11") = Format(CDec(dtRow.Item("OT1Amount")), mstrfmtCurrency)
                    mdecNetToPay += CDec(Format(CDec(dtRow.Item("OT1Amount")), mstrfmtCurrency))
                Else
                    rpt_Row.Item("Column7") = ""
                    rpt_Row.Item("Column8") = ""
                    rpt_Row.Item("Column9") = ""
                    rpt_Row.Item("Column10") = ""
                    rpt_Row.Item("Column11") = ""
                End If

                If mintOT2AmountHeadID > 0 AndAlso dtRow.Item("OT2HeadCode").ToString.Trim <> "" Then
                    rpt_Row.Item("Column12") = dtRow.Item("OT2HeadCode")
                    rpt_Row.Item("Column13") = dtRow.Item("OT2HeadName")
                    rpt_Row.Item("Column14") = Format(CDec(dtRow.Item("OT2Hrs")), mstrfmtCurrency)
                    If CDec(dtRow.Item("OT2Hrs")) = 0 Then
                        rpt_Row.Item("Column15") = Format(0, mstrfmtCurrency)
                    Else
                        rpt_Row.Item("Column15") = Format(CDec(dtRow.Item("OT2Amount")) / CDec(dtRow.Item("OT2Hrs")), mstrfmtCurrency)
                    End If
                    rpt_Row.Item("Column16") = Format(CDec(dtRow.Item("OT2Amount")), mstrfmtCurrency)
                    mdecNetToPay += CDec(Format(CDec(dtRow.Item("OT2Amount")), mstrfmtCurrency))
                Else
                    rpt_Row.Item("Column12") = ""
                    rpt_Row.Item("Column13") = ""
                    rpt_Row.Item("Column14") = ""
                    rpt_Row.Item("Column15") = ""
                    rpt_Row.Item("Column16") = ""
                End If

                If mintOT3AmountHeadID > 0 AndAlso dtRow.Item("OT3HeadCode").ToString.Trim <> "" Then
                    rpt_Row.Item("Column17") = dtRow.Item("OT3HeadCode")
                    rpt_Row.Item("Column18") = dtRow.Item("OT3HeadName")
                    rpt_Row.Item("Column19") = Format(CDec(dtRow.Item("OT3Hrs")), mstrfmtCurrency)
                    If CDec(dtRow.Item("OT3Hrs")) = 0 Then
                        rpt_Row.Item("Column20") = Format(0, mstrfmtCurrency)
                    Else
                        rpt_Row.Item("Column20") = Format(CDec(dtRow.Item("OT3Amount")) / CDec(dtRow.Item("OT3Hrs")), mstrfmtCurrency)
                    End If
                    rpt_Row.Item("Column21") = Format(CDec(dtRow.Item("OT3Amount")), mstrfmtCurrency)
                    mdecNetToPay += CDec(Format(CDec(dtRow.Item("OT3Amount")), mstrfmtCurrency))
                Else
                    rpt_Row.Item("Column17") = ""
                    rpt_Row.Item("Column18") = ""
                    rpt_Row.Item("Column19") = ""
                    rpt_Row.Item("Column20") = ""
                    rpt_Row.Item("Column21") = ""
                End If

                If mintOT4AmountHeadID > 0 AndAlso dtRow.Item("OT4HeadCode").ToString.Trim <> "" Then
                    rpt_Row.Item("Column22") = dtRow.Item("OT4HeadCode")
                    rpt_Row.Item("Column23") = dtRow.Item("OT4HeadName")
                    rpt_Row.Item("Column24") = Format(CDec(dtRow.Item("OT4Hrs")), mstrfmtCurrency)
                    If CDec(dtRow.Item("OT4Hrs")) = 0 Then
                        rpt_Row.Item("Column25") = Format(0, mstrfmtCurrency)
                    Else
                        rpt_Row.Item("Column25") = Format(CDec(dtRow.Item("OT4Amount")) / CDec(dtRow.Item("OT4Hrs")), mstrfmtCurrency)
                    End If
                    rpt_Row.Item("Column26") = Format(CDec(dtRow.Item("OT4Amount")), mstrfmtCurrency)
                    mdecNetToPay += CDec(Format(CDec(dtRow.Item("OT4Amount")), mstrfmtCurrency))
                Else
                    rpt_Row.Item("Column22") = ""
                    rpt_Row.Item("Column23") = ""
                    rpt_Row.Item("Column24") = ""
                    rpt_Row.Item("Column25") = ""
                    rpt_Row.Item("Column26") = ""
                End If


                OT_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                strPrevKey = dtRow.Item("employeecode").ToString

            Next


            '*** Leave Details
            objDataOperation.ClearParameters()

            StrQ = "SELECT " & _
                   "     hremployee_master.employeeunkid " & _
                   "    ,employeecode " & _
                   "    ,ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' '  + ISNULL(surname, '') AS employeename " & _
                   "    ,CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
                   "    ,CONVERT(CHAR(8), EocDt.termination_from_date, 112) AS termination_from_date " & _
                   "    ,CONVERT(CHAR(8), RetrDt.termination_to_date, 112) AS termination_to_date " & _
                   "    ,CONVERT(CHAR(8), EocDt.empl_enddate, 112) AS empl_enddate " & _
                   "    ,ISNULL(hrjob_master.job_code, '') AS job_code " & _
                   "    ,ISNULL(hrjob_master.job_name, '') AS job_name " & _
                   "    ,ISNULL(tableLeave.trnheadcode, '') AS HeadCode " & _
                   "    ,ISNULL(tableLeave.trnheadname, '') AS HeadName " & _
                   "    ,ISNULL(tableLeave.amount, 0) AS Amount " & _
                   "FROM hremployee_master " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "       SELECT " & _
                   "            jobunkid " & _
                   "           ,jobgroupunkid " & _
                   "           ,employeeunkid " & _
                   "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "       FROM hremployee_categorization_tran " & _
                   "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "    ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                   "    LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "       SELECT " & _
                   "            date1 AS empl_enddate " & _
                   "           ,date2 AS termination_from_date " & _
                   "           ,employeeunkid " & _
                   "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "       FROM hremployee_dates_tran " & _
                   "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "    )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "       SELECT " & _
                   "            date1 AS termination_to_date " & _
                   "           ,employeeunkid " & _
                   "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "       FROM hremployee_dates_tran " & _
                   "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "    )AS RetrDt ON RetrDt.employeeunkid = hremployee_master.employeeunkid AND RetrDt.rno = 1 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             prpayrollprocess_tran.employeeunkid " & _
                   "            ,trnheadcode " & _
                   "            ,trnheadname " & _
                   "            ,amount " & _
                   "        FROM prpayrollprocess_tran " & _
                   "            LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                   "            LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                   "            LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                   "        WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                   "            AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                   "            AND prtranhead_master.tranheadunkid = @LeaveBalanceHeadID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableLeave ON hremployee_master.employeeunkid = tableLeave.employeeunkid " & _
                    "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim objLeaveBalance As New clsleavebalance_tran
            Dim dsLeaveList As DataSet = Nothing

            Leave_Data = New ArutiReport.Designer.dsArutiReport

            strPrevKey = ""
            dtAppointment = Nothing
            dtTerminationFrom = Nothing
            dtTerminationTo = Nothing
            dtEmplEnd = Nothing
            mdtStartDate = Nothing
            mdtLastDateOfWorking = Nothing
            intWorkingDaysInPeriod = 0

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = Leave_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employeename")
                rpt_Row.Item("Column4") = dtRow.Item("job_name")

                If strPrevKey <> dtRow.Item("employeecode").ToString Then
                    If IsDBNull(dtRow.Item("appointeddate")) = False Then
                        dtAppointment = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString)
                    End If
                    If IsDBNull(dtRow.Item("termination_from_date")) = False Then
                        dtTerminationFrom = eZeeDate.convertDate(dtRow.Item("termination_from_date").ToString)
                    End If
                    If IsDBNull(dtRow.Item("termination_to_date")) = False Then
                        dtTerminationTo = eZeeDate.convertDate(dtRow.Item("termination_to_date").ToString)
                    End If
                    If IsDBNull(dtRow.Item("empl_enddate")) = False Then
                        dtEmplEnd = eZeeDate.convertDate(dtRow.Item("empl_enddate").ToString)
                    End If

                    mdtStartDate = IIf(dtAppointment.Date > mdtPeriodStartDate, dtAppointment.Date, mdtPeriodStartDate)
                    mdtLastDateOfWorking = mdtPeriodEndDate
                    If dtTerminationFrom.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtTerminationFrom.Date < mdtPeriodEndDate, dtTerminationFrom.Date, mdtPeriodEndDate)
                    End If
                    If dtTerminationTo.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtTerminationTo.Date < mdtLastDateOfWorking, dtTerminationTo.Date, mdtLastDateOfWorking)
                    End If
                    If dtEmplEnd.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtEmplEnd.Date < mdtLastDateOfWorking, dtEmplEnd.Date, mdtLastDateOfWorking)
                    End If
                    intWorkingDaysInPeriod = DateDiff(DateInterval.Day, mdtStartDate, mdtLastDateOfWorking.AddDays(1))
                End If

                rpt_Row.Item("Column5") = mdtLastDateOfWorking.ToShortDateString
                rpt_Row.Item("Column6") = intWorkingDaysInPeriod

                'Sohail (07 Jan 2014) -- Start
                'Enhancement - Separate TnA Periods from Payroll Periods
                'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtPeriodEndDate, mintLeaveTypeID, CInt(dtRow.Item("employeeunkid")), mdtDatabaseStartDate, mdtDatabaseEndDate, , , mintLeaveBalanceSetting, mintIsFin_Close)

                'Pinkal (24-May-2014) -- Start
                'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
                'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtTnAEndDate, mintLeaveTypeID, CInt(dtRow.Item("employeeunkid")), mdtDatabaseStartDate, mdtDatabaseEndDate, , , mintLeaveBalanceSetting, mintIsFin_Close)

                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtTnAEndDate, mintLeaveTypeID, CInt(dtRow.Item("employeeunkid")), mdtDatabaseStartDate, mdtDatabaseEndDate, , , mintLeaveBalanceSetting, mintIsFin_Close, mintYearUnkId)
                If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                    'Pinkal (18-Nov-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                    'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtTnAEndDate, mintLeaveTypeID, CInt(dtRow.Item("employeeunkid")), mdtDatabaseStartDate, mdtDatabaseEndDate, , , mintLeaveBalanceSetting, mintIsFin_Close, mintYearUnkId)
                    dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtTnAEndDate, mintLeaveTypeID, CInt(dtRow.Item("employeeunkid")), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtDatabaseStartDate, mdtDatabaseEndDate, , , mintLeaveBalanceSetting, mintIsFin_Close, mintYearUnkId)
                    'Pinkal (18-Nov-2016) -- End


                ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtTnAEndDate, mintLeaveTypeID, CInt(dtRow.Item("employeeunkid")), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtDatabaseStartDate, mdtDatabaseEndDate, True, True, mintLeaveBalanceSetting, mintIsFin_Close, mintYearUnkId)
                End If
                'Pinkal (21-Jul-2014) -- End


                'Pinkal (24-May-2014) -- End


                'Sohail (07 Jan 2014) -- End

                rpt_Row.Item("Column7") = mstrLeaveTypeName & " - " & Language.getMessage(mstrModuleName, 49, "No. of Days Leave Credit as of")
                If dsLeaveList.Tables(0).Rows.Count > 0 Then
                    rpt_Row.Item("Column8") = Format(CDec(dsLeaveList.Tables(0).Rows(0).Item("Balance")), mstrfmtCurrency)
                Else
                    rpt_Row.Item("Column8") = Format(0, mstrfmtCurrency)
                End If

                rpt_Row.Item("Column9") = Format(dtRow.Item("Amount"), mstrfmtCurrency)
                mdecNetToPay += CDec(Format(dtRow.Item("Amount"), mstrfmtCurrency))

                Leave_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                strPrevKey = dtRow.Item("employeecode").ToString

            Next


            '*** Gratuity Details
            objDataOperation.ClearParameters()

            StrQ = "SELECT " & _
                   "     hremployee_master.employeeunkid " & _
                   "    ,employeecode " & _
                   "    ,ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' '  + ISNULL(surname, '') AS employeename " & _
                   "    ,CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
                   "    ,CONVERT(CHAR(8), EocDt.termination_from_date, 112) AS termination_from_date " & _
                   "    ,CONVERT(CHAR(8), RetrDt.termination_to_date, 112) AS termination_to_date " & _
                   "    ,CONVERT(CHAR(8), EocDt.empl_enddate, 112) AS empl_enddate " & _
                   "    ,ISNULL(tableFirst.trnheadcode, '') AS FirstHeadCode " & _
                   "    ,ISNULL(tableFirst.trnheadname, '') AS FirstHeadName " & _
                   "    ,ISNULL(tableFirst.amount, 0) AS FirstAmount " & _
                   "    ,ISNULL(tableYear.trnheadcode, '') AS YearHeadCode " & _
                   "    ,ISNULL(tableYear.trnheadname, '') AS YearHeadName " & _
                   "    ,ISNULL(tableYear.amount, 0) AS YearAmount " & _
                   "    ,ISNULL(tableMonth.trnheadcode, '') AS MonthHeadCode " & _
                   "    ,ISNULL(tableMonth.trnheadname, '') AS MonthHeadName " & _
                   "    ,ISNULL(tableMonth.amount, 0) AS MonthAmount " & _
                   "    ,ISNULL(tableDays.trnheadcode, '') AS DaysHeadCode " & _
                   "    ,ISNULL(tableDays.trnheadname, '') AS DaysHeadName " & _
                   "    ,ISNULL(tableDays.amount, 0) AS DaysAmount " & _
                   "    ,ISNULL(tableYearAmt.trnheadcode, '') AS YearAmtHeadCode " & _
                   "    ,ISNULL(tableYearAmt.trnheadname, '') AS YearAmtHeadName " & _
                   "    ,ISNULL(tableYearAmt.amount, 0) AS YearAmtAmount " & _
                   "    ,ISNULL(tableMonthAmt.trnheadcode, '') AS MonthAmtHeadCode " & _
                   "    ,ISNULL(tableMonthAmt.trnheadname, '') AS MonthAmtHeadName " & _
                   "    ,ISNULL(tableMonthAmt.amount, 0) AS MonthAmtAmount " & _
                   "    ,ISNULL(tableDaysAmt.trnheadcode, '') AS DaysAmtHeadCode " & _
                   "    ,ISNULL(tableDaysAmt.trnheadname, '') AS DaysAmtHeadName " & _
                   "    ,ISNULL(tableDaysAmt.amount, 0) AS DaysAmtAmount " & _
                   "FROM hremployee_master " & _
                   "   LEFT JOIN " & _
                   "   ( " & _
                   "       SELECT " & _
                   "            date1 AS empl_enddate " & _
                   "           ,date2 AS termination_from_date " & _
                   "           ,employeeunkid " & _
                   "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "       FROM hremployee_dates_tran " & _
                   "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "   )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                   "   LEFT JOIN " & _
                   "   ( " & _
                   "       SELECT " & _
                   "            date1 AS termination_to_date " & _
                   "           ,employeeunkid " & _
                   "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "       FROM hremployee_dates_tran " & _
                   "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "   )AS RetrDt ON RetrDt.employeeunkid = hremployee_master.employeeunkid AND RetrDt.rno = 1 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             prpayrollprocess_tran.employeeunkid " & _
                   "            ,trnheadcode " & _
                   "            ,trnheadname " & _
                   "            ,amount " & _
                   "        FROM prpayrollprocess_tran " & _
                   "        LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                   "        LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                   "        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                   "        WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                   "            AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                   "            AND prtranhead_master.tranheadunkid = @First3YearsID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If


            StrQ &= "   ) AS tableFirst ON hremployee_master.employeeunkid = tableFirst.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @BalanceYearID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableYear ON hremployee_master.employeeunkid = tableYear.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @BalanceMonthID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableMonth ON hremployee_master.employeeunkid = tableMonth.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @BalanceDaysID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableDays ON hremployee_master.employeeunkid = tableDays.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @BalanceYearAmtID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableYearAmt ON hremployee_master.employeeunkid = tableYearAmt.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT  " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @BalanceMonthAmtID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableMonthAmt ON hremployee_master.employeeunkid = tableMonthAmt.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            prpayrollprocess_tran.employeeunkid " & _
                    "           ,trnheadcode " & _
                    "           ,trnheadname " & _
                    "           ,amount " & _
                    "       FROM prpayrollprocess_tran " & _
                    "           LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "           LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    "       WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "           AND prtranhead_master.tranheadunkid = @BalanceDaysAmtID "

            If mintPeriodId > 0 Then
                StrQ &= " AND payperiodunkid = @periodunkid "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            StrQ &= "   ) AS tableDaysAmt ON hremployee_master.employeeunkid = tableDaysAmt.employeeunkid " & _
                    "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            Gratuity_Data = New ArutiReport.Designer.dsArutiReport

            strPrevKey = ""
            dtAppointment = Nothing
            dtTerminationFrom = Nothing
            dtTerminationTo = Nothing
            dtEmplEnd = Nothing
            mdtStartDate = Nothing
            mdtLastDateOfWorking = Nothing
            intWorkingDaysInPeriod = 0

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = Gratuity_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employeename")
                'rpt_Row.Item("Column4") = dtRow.Item("job_name")

                If strPrevKey <> dtRow.Item("employeecode").ToString Then
                    If IsDBNull(dtRow.Item("appointeddate")) = False Then
                        dtAppointment = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString)
                    End If
                    If IsDBNull(dtRow.Item("termination_from_date")) = False Then
                        dtTerminationFrom = eZeeDate.convertDate(dtRow.Item("termination_from_date").ToString)
                    End If
                    If IsDBNull(dtRow.Item("termination_to_date")) = False Then
                        dtTerminationTo = eZeeDate.convertDate(dtRow.Item("termination_to_date").ToString)
                    End If
                    If IsDBNull(dtRow.Item("empl_enddate")) = False Then
                        dtEmplEnd = eZeeDate.convertDate(dtRow.Item("empl_enddate").ToString)
                    End If

                    mdtStartDate = IIf(dtAppointment.Date > mdtPeriodStartDate, dtAppointment.Date, mdtPeriodStartDate)
                    mdtLastDateOfWorking = mdtPeriodEndDate
                    If dtTerminationFrom.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtTerminationFrom.Date < mdtPeriodEndDate, dtTerminationFrom.Date, mdtPeriodEndDate)
                    End If
                    If dtTerminationTo.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtTerminationTo.Date < mdtLastDateOfWorking, dtTerminationTo.Date, mdtLastDateOfWorking)
                    End If
                    If dtEmplEnd.Date <> Nothing Then
                        mdtLastDateOfWorking = IIf(dtEmplEnd.Date < mdtLastDateOfWorking, dtEmplEnd.Date, mdtLastDateOfWorking)
                    End If
                    intWorkingDaysInPeriod = DateDiff(DateInterval.Day, mdtStartDate, mdtLastDateOfWorking.AddDays(1))
                End If

                rpt_Row.Item("Column5") = dtAppointment.Date.ToShortDateString
                rpt_Row.Item("Column6") = mdtLastDateOfWorking.ToShortDateString

                rpt_Row.Item("Column7") = dtRow.Item("FirstHeadCode")
                rpt_Row.Item("Column8") = dtRow.Item("FirstHeadName")
                rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("Firstamount")), mstrfmtCurrency)
                mdecNetToPay += CDec(Format(CDec(dtRow.Item("Firstamount")), mstrfmtCurrency))

                rpt_Row.Item("Column10") = dtRow.Item("YearAmtHeadCode")
                rpt_Row.Item("Column11") = dtRow.Item("YearAmtHeadName")
                rpt_Row.Item("Column12") = Format(CDec(dtRow.Item("YearAmtamount")), mstrfmtCurrency)
                mdecNetToPay += CDec(Format(CDec(dtRow.Item("YearAmtamount")), mstrfmtCurrency))

                rpt_Row.Item("Column13") = dtRow.Item("MonthAmtHeadCode")
                rpt_Row.Item("Column14") = dtRow.Item("MonthAmtHeadName")
                rpt_Row.Item("Column15") = Format(CDec(dtRow.Item("MonthAmtamount")), mstrfmtCurrency)
                mdecNetToPay += CDec(Format(CDec(dtRow.Item("MonthAmtamount")), mstrfmtCurrency))

                rpt_Row.Item("Column16") = dtRow.Item("DaysAmtHeadCode")
                rpt_Row.Item("Column17") = dtRow.Item("DaysAmtHeadName")
                rpt_Row.Item("Column18") = Format(CDec(dtRow.Item("DaysAmtamount")), mstrfmtCurrency)
                mdecNetToPay += CDec(Format(CDec(dtRow.Item("DaysAmtamount")), mstrfmtCurrency))

                rpt_Row.Item("Column19") = dtRow.Item("YearHeadCode")
                rpt_Row.Item("Column20") = dtRow.Item("YearHeadName")
                rpt_Row.Item("Column21") = Format(CDec(dtRow.Item("Yearamount")), mstrfmtCurrency)

                rpt_Row.Item("Column22") = dtRow.Item("MonthHeadCode")
                rpt_Row.Item("Column23") = dtRow.Item("MonthHeadName")
                rpt_Row.Item("Column24") = Format(CDec(dtRow.Item("Monthamount")), mstrfmtCurrency)

                rpt_Row.Item("Column25") = dtRow.Item("DaysHeadCode")
                rpt_Row.Item("Column26") = dtRow.Item("DaysHeadName")
                rpt_Row.Item("Column27") = Format(CDec(dtRow.Item("Daysamount")), mstrfmtCurrency)

                Gratuity_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                strPrevKey = dtRow.Item("employeecode").ToString

            Next

            '******************
            objRpt = New ArutiReport.Designer.rptEndOfService


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            'If mintViewIndex <= 0 Then
            '    Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
            '    Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            'End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 2, "Prepared By"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 24, "Verified By"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 25, "Approved By"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 8, "Received By"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            Call ReportFunction.TextChange(objRpt, "lblEmployeeSign", Language.getMessage(mstrModuleName, 28, "Employee Sign"))

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptEosOT").SetDataSource(OT_Data)
            objRpt.Subreports("rptEosLeave").SetDataSource(Leave_Data)
            objRpt.Subreports("rptEosGratuity").SetDataSource(Gratuity_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 22, "Emp No."))
            Call ReportFunction.TextChange(objRpt, "txtSurName", Language.getMessage(mstrModuleName, 29, "Salary Details"))
            Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 11, "First Name"))
            Call ReportFunction.TextChange(objRpt, "txtInitial", Language.getMessage(mstrModuleName, 12, "Initial"))
            Call ReportFunction.TextChange(objRpt, "txtReferenceNo", Language.getMessage(mstrModuleName, 14, "Reference No."))
            Call ReportFunction.TextChange(objRpt, "lblNetToPay", Language.getMessage(mstrModuleName, 30, "Net To Pay"))
            Call ReportFunction.TextChange(objRpt, "txtNetToPay", Format(mdecNetToPay, mstrfmtCurrency))

            Call ReportFunction.TextChange(objRpt.Subreports("rptEosOT"), "lblSalaryPeriod", Language.getMessage(mstrModuleName, 48, "Salary for the Month :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEosOT"), "txtSalaryPeriod", mstrPeriodName)
            Call ReportFunction.TextChange(objRpt.Subreports("rptEosOT"), "lblNoOfDays", Language.getMessage(mstrModuleName, 50, "No. Of Days :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEosOT"), "lblHour", Language.getMessage(mstrModuleName, 51, "Hours"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEosOT"), "lblHourRate", Language.getMessage(mstrModuleName, 52, "Hourly Rate"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptEosLeave"), "lblLeaveDetail", Language.getMessage(mstrModuleName, 53, "Leave Wages /Salary"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEosLeave"), "txtLeaveBalance", Language.getMessage(mstrModuleName, 54, "(Leave Balance)"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblGratuity", Language.getMessage(mstrModuleName, 55, "Gratuity"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblJoiningDate", Language.getMessage(mstrModuleName, 56, "Date of Joining :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblRelievingDate", Language.getMessage(mstrModuleName, 57, "Date of Relieving :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblForFirstThreeYears", Language.getMessage(mstrModuleName, 58, "For First Three Years :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblBalanceYears", Language.getMessage(mstrModuleName, 59, "Balance Years :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblBalanceMonths", Language.getMessage(mstrModuleName, 60, "Balance Months :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEosGratuity"), "lblBalanceDays", Language.getMessage(mstrModuleName, 61, "Balance Days :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 17, "Page :"))

            'Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 25, "Period :"))
            'Call ReportFunction.TextChange(objRpt, "txtPeriodTotal", Language.getMessage(mstrModuleName, 26, "Period Total:"))
            'Call ReportFunction.TextChange(objRpt, "txtPeriodCount", Language.getMessage(mstrModuleName, 27, "Period Count:"))



            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables(0)

                Dim mintColumn As Integer = 0

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 22, "Emp No.")
                mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 10, "Surname")
                mdtTableExcel.Columns("Column3").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 11, "First Name")
                mdtTableExcel.Columns("Column4").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 12, "Initial")
                mdtTableExcel.Columns("Column5").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 13, "Amount")
                mdtTableExcel.Columns("Column81").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 14, "Reference No.")
                mdtTableExcel.Columns("Column7").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 4, "Period :")
                mdtTableExcel.Columns("Column10").SetOrdinal(mintColumn)
                mintColumn += 1

                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns("Column1").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
                    mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
                    mintColumn += 1
                End If


                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next


            End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try

    End Function
    'S.SANDEEP [04 JUN 2015] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Prepared By")
            Language.setMessage(mstrModuleName, 3, "Gross Pay Head :")
            Language.setMessage(mstrModuleName, 4, "Period :")
            Language.setMessage(mstrModuleName, 5, "Checked By :")
            Language.setMessage(mstrModuleName, 6, "Approved By :")
            Language.setMessage(mstrModuleName, 7, "Received By :")
            Language.setMessage(mstrModuleName, 8, "Received By")
            Language.setMessage(mstrModuleName, 9, "Emp No")
            Language.setMessage(mstrModuleName, 10, "Surname")
            Language.setMessage(mstrModuleName, 11, "First Name")
            Language.setMessage(mstrModuleName, 12, "Initial")
            Language.setMessage(mstrModuleName, 13, "Amount")
            Language.setMessage(mstrModuleName, 14, "Reference No.")
            Language.setMessage(mstrModuleName, 15, "Printed By :")
            Language.setMessage(mstrModuleName, 16, "Printed Date :")
            Language.setMessage(mstrModuleName, 17, "Page :")
            Language.setMessage(mstrModuleName, 18, "OT1 Hour Head :")
            Language.setMessage(mstrModuleName, 19, "Grand Total :")
            Language.setMessage(mstrModuleName, 20, "Sub Count :")
            Language.setMessage(mstrModuleName, 21, "Total Count :")
            Language.setMessage(mstrModuleName, 22, "Emp No.")
            Language.setMessage(mstrModuleName, 23, "Period :")
            Language.setMessage(mstrModuleName, 24, "Verified By")
            Language.setMessage(mstrModuleName, 25, "Approved By")
            Language.setMessage(mstrModuleName, 26, "Period Total:")
            Language.setMessage(mstrModuleName, 27, "Period Count:")
            Language.setMessage(mstrModuleName, 28, "Employee Sign")
            Language.setMessage(mstrModuleName, 29, "Salary Details")
            Language.setMessage(mstrModuleName, 30, "Net To Pay")
            Language.setMessage(mstrModuleName, 31, "OT2 Hour Head :")
            Language.setMessage(mstrModuleName, 32, "OT2 Amount Head :")
            Language.setMessage(mstrModuleName, 33, "OT1 Amount Head :")
            Language.setMessage(mstrModuleName, 34, "OT3 Hour Head :")
            Language.setMessage(mstrModuleName, 35, "OT3 Amount Head :")
            Language.setMessage(mstrModuleName, 36, "OT4 Hour Head :")
            Language.setMessage(mstrModuleName, 37, "OT4 Amount Head :")
            Language.setMessage(mstrModuleName, 38, "Leave Type :")
            Language.setMessage(mstrModuleName, 39, "Leave Balance Head :")
            Language.setMessage(mstrModuleName, 40, "Gratuity for First 3 Years :")
            Language.setMessage(mstrModuleName, 41, "Gratuity for Balance Years :")
            Language.setMessage(mstrModuleName, 42, "Gratuity for Balance Months :")
            Language.setMessage(mstrModuleName, 43, "Gratuity for Balance Days :")
            Language.setMessage(mstrModuleName, 44, "Gratuity for Balance Years Amount :")
            Language.setMessage(mstrModuleName, 45, "Gratuity for Balance Months Amount :")
            Language.setMessage(mstrModuleName, 46, "Gratuity for Balance Days Amount :")
            Language.setMessage(mstrModuleName, 47, "Sub Total:")
            Language.setMessage(mstrModuleName, 48, "Salary for the Month :")
            Language.setMessage(mstrModuleName, 49, "No. of Days Leave Credit as of")
            Language.setMessage(mstrModuleName, 50, "No. Of Days :")
            Language.setMessage(mstrModuleName, 51, "Hours")
            Language.setMessage(mstrModuleName, 52, "Hourly Rate")
            Language.setMessage(mstrModuleName, 53, "Leave Wages /Saalry")
            Language.setMessage(mstrModuleName, 54, "(Leave Balance)")
            Language.setMessage(mstrModuleName, 55, "Gratuity")
            Language.setMessage(mstrModuleName, 56, "Date of Joining :")
            Language.setMessage(mstrModuleName, 57, "Date of Relieving :")
            Language.setMessage(mstrModuleName, 58, "For First Three Years :")
            Language.setMessage(mstrModuleName, 59, "Balance Years :")
            Language.setMessage(mstrModuleName, 60, "Balance Months :")
            Language.setMessage(mstrModuleName, 61, "Balance Days :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
