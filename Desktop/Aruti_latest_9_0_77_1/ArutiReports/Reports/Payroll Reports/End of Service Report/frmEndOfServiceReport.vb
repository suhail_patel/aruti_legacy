'************************************************************************************************************************************
'Class Name : frmEndOfServiceReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEndOfServiceReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEndOfServiceReport"
    Private objEndOfService As clsEndOfServiceReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrOriginalHeadIDs As String = ""

    Private mstrAdvanceFilter As String = String.Empty

    Private mstrTranDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0

#End Region

#Region " Constructor "

    Public Sub New()
        objEndOfService = New clsEndOfServiceReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEndOfService.SetDefaultValue()
        InitializeComponent()


        _Show_ExcelExtra_Menu = True

        'S.SANDEEP [04 JUN 2015] -- START
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        '_Show_AdvanceFilter = True
        'S.SANDEEP [04 JUN 2015] -- END


    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Original_Head = 1
        GrossPay = 2
        OT_1_Hour = 3
        OT_2_Hour = 4
        OT_3_Hour = 5
        OT_4_Hour = 6
        OT_1_Amount = 7
        OT_2_Amount = 8
        OT_3_Amount = 9
        OT_4_Amount = 10
        Leave_Type = 11
        Leave_Balance = 12
        First_Three = 13
        Balance_Year = 14
        Balance_Month = 15
        Balance_Days = 16
        Balance_Year_Amt = 17
        Balance_Month_Amt = 18
        Balance_Days_Amt = 19
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objperiod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim objTranHead As New clsTransactionHead
        Dim objLeave As New clsleavetype_master
        Dim dsList As DataSet
        Try
            '**************************************************************************************************************
            '********  PLEASE ADD REQUIRED CHANGES REGARDING NEW COMBOBOX IN cboGrossPay_Validating() EVENT  WITHOUT FAIL
            '**************************************************************************************************************

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid) 'Sohail (25 Mar 2013)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1) 'Sohail (25 Mar 2013)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Period")
                .SelectedValue = mintFirstOpenPeriod
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("Heads", True, enTranHeadType.Informational, , , , , " calctype_id NOT IN (" & enCalcType.NET_PAY & ", " & enCalcType.TOTAL_DEDUCTION & ", " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & ", " & enCalcType.TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_CASH_BENEFIT_TOTAL & ")")
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, enTranHeadType.Informational, , , , , " calctype_id NOT IN (" & enCalcType.NET_PAY & ", " & enCalcType.TOTAL_DEDUCTION & ", " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & ", " & enCalcType.TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_CASH_BENEFIT_TOTAL & ")")
            'Sohail (21 Aug 2015) -- End
            With cboGrossPay
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("Heads", True, enTranHeadType.Informational, , , , , " calctype_id NOT IN (" & enCalcType.NET_PAY & ", " & enCalcType.TOTAL_EARNING & ", " & enCalcType.TOTAL_DEDUCTION & ", " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & ", " & enCalcType.TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_CASH_BENEFIT_TOTAL & ")")
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, enTranHeadType.Informational, , , , , " calctype_id NOT IN (" & enCalcType.NET_PAY & ", " & enCalcType.TOTAL_EARNING & ", " & enCalcType.TOTAL_DEDUCTION & ", " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & ", " & enCalcType.TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_CASH_BENEFIT_TOTAL & ")")
            'Sohail (21 Aug 2015) -- End
            With cboOT1Hours
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT2Hours
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT3Hours
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT4Hours
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboForFirst3Years
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboBalanceYear
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboBalanceMonth
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboBalanceDays
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboBalanceYearAmt
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboBalanceMonthAmt
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboBalanceDaysAmt
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboLeaveBalanceHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("Heads", True, enTranHeadType.EarningForEmployees, , , , , " typeof_id NOT IN (" & enTypeOf.Salary & ")")
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, enTranHeadType.EarningForEmployees, , , , , " typeof_id NOT IN (" & enTypeOf.Salary & ")")
            'Sohail (21 Aug 2015) -- End
            With cboOT1Amount
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT2Amount
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT3Amount
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOT4Amount
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Heads").Copy
                .SelectedValue = 0
            End With


            dsList = objLeave.getListForCombo("Leave", True)
            With cboLeaveType
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Leave")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objperiod = Nothing
            objMaster = Nothing
            objTranHead = Nothing
            objLeave = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            dsList = objTranHead.GetList("Heads", , enTranHeadType.Informational, True, , , , " calctype_id NOT IN (" & enCalcType.NET_PAY & ", " & enCalcType.TOTAL_EARNING & ", " & enCalcType.TOTAL_DEDUCTION & ", " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & ", " & enCalcType.TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_CASH_BENEFIT_TOTAL & ")")
            lvOriginalHeads.Items.Clear()

            Dim lvItem As ListViewItem

            For Each dsRow As DataRow In dsList.Tables("Heads").Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = CInt(dsRow.Item("tranheadunkid"))

                If mstrOriginalHeadIDs.Trim <> "" Then
                    If mstrOriginalHeadIDs.Split(",").Contains(dsRow.Item("tranheadunkid").ToString) = True Then
                        lvItem.Checked = True
                    End If
                End If

                lvItem.SubItems.Add(dsRow.Item("trnheadcode").ToString)
                lvItem.SubItems.Add(dsRow.Item("trnheadname").ToString)
                lvItem.SubItems.Add(dsRow.Item("HeadType").ToString)

                lvOriginalHeads.Items.Add(lvItem)

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        Dim strPeriodIDs As String = ""
        Dim arrDatabaseName As New ArrayList
        Dim i As Integer = 0
        Dim intPrevYearID As Integer = 0

        Try
            mstrOriginalHeadIDs = ""

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Function
            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Function
            ElseIf lvOriginalHeads.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please tick atleast one transaction head."), enMsgBoxStyle.Information)
                lvOriginalHeads.Select()
                Exit Function
            ElseIf CInt(cboGrossPay.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Gross Pay Head."), enMsgBoxStyle.Information)
                cboGrossPay.Select()
                Exit Function
            ElseIf CInt(cboLeaveType.SelectedValue) > 0 AndAlso CInt(cboLeaveBalanceHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Select Leave Balance Head."), enMsgBoxStyle.Information)
                cboLeaveBalanceHead.Select()
                Exit Function
            End If

            objEndOfService.SetDefaultValue()

            objEndOfService._PeriodId = CInt(cboPeriod.SelectedValue)
            objEndOfService._PeriodName = cboPeriod.Text

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objEndOfService._PeriodStartDate = objPeriod._Start_Date
            objEndOfService._PeriodEndDate = objPeriod._End_Date
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            objEndOfService._TnAEndDate = objPeriod._TnA_EndDate
            'Sohail (07 Jan 2014) -- End

            objPeriod = Nothing

            objEndOfService._EmployeeId = CInt(cboEmployee.SelectedValue)
            objEndOfService._EmployeeName = cboEmployee.Text

            Dim allHeads As List(Of String) = (From p In lvOriginalHeads.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToList
            mstrOriginalHeadIDs = String.Join(",", allHeads.ToArray)
            objEndOfService._OriginalHeadIDs = mstrOriginalHeadIDs

            objEndOfService._GrossPayHeadId = CInt(cboGrossPay.SelectedValue)
            objEndOfService._GrossPayHeadName = cboGrossPay.Text

            objEndOfService._OT1HourHeadId = CInt(cboOT1Hours.SelectedValue)
            objEndOfService._OT1HourHeadName = cboOT1Hours.Text

            objEndOfService._OT1AmountHeadId = CInt(cboOT1Amount.SelectedValue)
            objEndOfService._OT1AmountHeadName = cboOT1Amount.Text

            objEndOfService._OT2HourHeadId = CInt(cboOT2Hours.SelectedValue)
            objEndOfService._OT2HourHeadName = cboOT2Hours.Text

            objEndOfService._OT2AmountHeadId = CInt(cboOT2Amount.SelectedValue)
            objEndOfService._OT2AmountHeadName = cboOT2Amount.Text

            objEndOfService._OT3HourHeadId = CInt(cboOT3Hours.SelectedValue)
            objEndOfService._OT3HourHeadName = cboOT3Hours.Text

            objEndOfService._OT3AmountHeadId = CInt(cboOT3Amount.SelectedValue)
            objEndOfService._OT3AmountHeadName = cboOT3Amount.Text

            objEndOfService._OT4HourHeadId = CInt(cboOT4Hours.SelectedValue)
            objEndOfService._OT4HourHeadName = cboOT4Hours.Text

            objEndOfService._OT4AmountHeadId = CInt(cboOT4Amount.SelectedValue)
            objEndOfService._OT4AmountHeadName = cboOT4Amount.Text

            objEndOfService._LeaveTypeID = CInt(cboLeaveType.SelectedValue)
            objEndOfService._LeaveTypeName = cboLeaveType.Text

            objEndOfService._LeaveBalanceHeadID = CInt(cboLeaveBalanceHead.SelectedValue)
            objEndOfService._LeaveBalanceHeadName = cboLeaveBalanceHead.Text

            objEndOfService._GratuityFirst3YearsId = CInt(cboForFirst3Years.SelectedValue)
            objEndOfService._GratuityFirst3YearsName = cboForFirst3Years.Text

            objEndOfService._GratuityBalanceYearsId = CInt(cboBalanceYear.SelectedValue)
            objEndOfService._GratuityBalanceYearsName = cboBalanceYear.Text

            objEndOfService._GratuityBalanceMonthsId = CInt(cboBalanceMonth.SelectedValue)
            objEndOfService._GratuityBalanceMonthsName = cboBalanceMonth.Text

            objEndOfService._GratuityBalanceDaysId = CInt(cboBalanceDays.SelectedValue)
            objEndOfService._GratuityBalanceDaysName = cboBalanceDays.Text

            objEndOfService._GratuityBalanceYearsAmtId = CInt(cboBalanceYearAmt.SelectedValue)
            objEndOfService._GratuityBalanceYearsAmtName = cboBalanceYearAmt.Text

            objEndOfService._GratuityBalanceMonthsAmtId = CInt(cboBalanceMonthAmt.SelectedValue)
            objEndOfService._GratuityBalanceMonthsAmtName = cboBalanceMonthAmt.Text

            objEndOfService._GratuityBalanceDaysAmtId = CInt(cboBalanceDaysAmt.SelectedValue)
            objEndOfService._GratuityBalanceDaysAmtName = cboBalanceDaysAmt.Text



            objEndOfService._ViewByIds = mstrStringIds
            objEndOfService._ViewIndex = mintViewIdx
            objEndOfService._ViewByName = mstrStringName
            objEndOfService._Analysis_Fields = mstrAnalysis_Fields
            objEndOfService._Analysis_Join = mstrAnalysis_Join
            objEndOfService._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEndOfService._Report_GroupName = mstrReport_GroupName

            objEndOfService._Advance_Filter = mstrAdvanceFilter

            objEndOfService._DatabaseStartDate = FinancialYear._Object._Database_Start_Date
            objEndOfService._DatabaseEndDate = FinancialYear._Object._Database_End_Date
            objEndOfService._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objEndOfService._IsFin_Close = FinancialYear._Object._IsFin_Close


            'Pinkal (24-May-2014) -- Start
            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
            objEndOfService._YearId = FinancialYear._Object._YearUnkid
            'Pinkal (24-May-2014) -- End


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            objEndOfService._LeaveAccrueTenureSetting = ConfigParameter._Object._LeaveAccrueTenureSetting
            objEndOfService._LeaveAccrueDaysAfterEachMonth = ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth
            'Pinkal (18-Nov-2016) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objEndOfService.setDefaultOrderBy(0)
            txtOrderBy.Text = objEndOfService.OrderByDisplay
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboEmployee.SelectedIndex = 0
            cboGrossPay.SelectedValue = 0
            cboOT1Hours.SelectedValue = 0
            cboOT1Amount.SelectedValue = 0
            cboOT2Hours.SelectedValue = 0
            cboOT2Amount.SelectedValue = 0
            cboOT3Hours.SelectedValue = 0
            cboOT3Amount.SelectedValue = 0
            cboOT4Hours.SelectedValue = 0
            cboOT4Amount.SelectedValue = 0
            cboLeaveType.SelectedValue = 0
            cboLeaveBalanceHead.SelectedValue = 0
            cboForFirst3Years.SelectedValue = 0
            cboBalanceYear.SelectedValue = 0
            cboBalanceMonth.SelectedValue = 0
            cboBalanceDays.SelectedValue = 0
            cboBalanceYearAmt.SelectedValue = 0
            cboBalanceMonthAmt.SelectedValue = 0
            cboBalanceDaysAmt.SelectedValue = 0

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            'chkInactiveemp.Checked = False
            'chkIncludeNegativeHeads.Checked = True

            'cboToPeriod.SelectedValue = mintFirstOpenPeriod

            mstrAdvanceFilter = ""


            'cboMembership.SelectedValue = 0

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.End_Of_Service_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Original_Head
                            mstrOriginalHeadIDs = dsRow.Item("transactionheadid").ToString


                        Case enHeadTypeId.GrossPay
                            cboGrossPay.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OT_1_Hour
                            cboOT1Hours.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OT_2_Hour
                            cboOT2Hours.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OT_3_Hour
                            cboOT3Hours.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OT_4_Hour
                            cboOT4Hours.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OT_1_Amount
                            cboOT1Amount.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OT_2_Amount
                            cboOT2Amount.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OT_3_Amount
                            cboOT3Amount.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OT_4_Amount
                            cboOT4Amount.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Leave_Type
                            cboLeaveType.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Leave_Balance
                            cboLeaveBalanceHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.First_Three
                            cboForFirst3Years.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Balance_Year
                            cboBalanceYear.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Balance_Month
                            cboBalanceMonth.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Balance_Days
                            cboBalanceDays.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Balance_Year_Amt
                            cboBalanceYearAmt.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Balance_Month_Amt
                            cboBalanceMonthAmt.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Balance_Days_Amt
                            cboBalanceDaysAmt.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub CheckAllHeads(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvOriginalHeads.Items()
                RemoveHandler lvOriginalHeads.ItemChecked, AddressOf lvOriginalHeads_ItemChecked 'Sohail (03 Nov 2010)
                lvItem.Checked = blnCheckAll
                AddHandler lvOriginalHeads.ItemChecked, AddressOf lvOriginalHeads_ItemChecked 'Sohail (03 Nov 2010)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllHeads", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmEndOfServiceReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEndOfService = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEndOfServiceReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEndOfServiceReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Me._Title = objEndOfService._ReportName
            Me._Message = objEndOfService._ReportDesc
            Call FillCombo()
            Call ResetValue()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDSpreadSheetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEndOfServiceReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEndOfServiceReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub frmEndOfServiceReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If SetFilter() = False Then Exit Sub

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEndOfService.generateReport(0, e.Type, enExportAction.None)
            objEndOfService.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                              ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
                                              0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEndOfServiceReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEndOfServiceReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEndOfService.generateReport(0, enPrintAction.None, e.Type)
            objEndOfService.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                              ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
                                              0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEndOfServiceReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEndOfServiceReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEndOfServiceReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEndOfServiceReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEndOfServiceReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub


    Private Sub frmEndOfServiceReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEndOfServiceReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEndOfServiceReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEndOfServiceReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To 19
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.End_Of_Service_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Original_Head
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = mstrOriginalHeadIDs

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.GrossPay
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboGrossPay.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OT_1_Hour
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOT1Hours.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OT_2_Hour
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOT2Hours.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OT_3_Hour
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOT3Hours.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OT_4_Hour
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOT4Hours.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OT_1_Amount
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOT1Amount.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OT_2_Amount
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOT2Amount.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OT_3_Amount
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOT3Amount.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OT_4_Amount
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOT4Amount.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Leave_Type
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboLeaveType.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Leave_Balance
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboLeaveBalanceHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.First_Three
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboForFirst3Years.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Balance_Year
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBalanceYear.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Balance_Month
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBalanceMonth.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Balance_Days
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBalanceDays.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Balance_Year_Amt
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBalanceYearAmt.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Balance_Month_Amt
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBalanceMonthAmt.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Balance_Days_Amt
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBalanceDaysAmt.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.End_Of_Service_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " LinkLabel Event "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region " Control "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEndOfService.setOrderBy(0)
            txtOrderBy.Text = objEndOfService.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllHeads(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvOriginalHeads_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvOriginalHeads.ItemChecked
        Try
            If lvOriginalHeads.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvOriginalHeads.CheckedItems.Count < lvOriginalHeads.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvOriginalHeads.CheckedItems.Count = lvOriginalHeads.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTranHead_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " ComboBox Event "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objperiod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet
        Dim strFilter As String = ""
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                strFilter = " ( CONVERT(CHAR(8), termination_from_date, 112) BETWEEN @startdate AND @enddate OR CONVERT(CHAR(8), termination_to_date, 112) BETWEEN @startdate AND @enddate OR CONVERT(CHAR(8), empl_enddate, 112) BETWEEN @startdate AND @enddate ) "
            End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objperiod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, False, , , , , , , , , , , , objperiod._Start_Date, objperiod._End_Date, , , , , strFilter)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           objperiod._Start_Date, _
                                           objperiod._End_Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , strFilter)
            'Anjan [10 June 2015] -- End


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            objperiod = Nothing
            objEmp = Nothing
        End Try
    End Sub

    Private Sub cboGrossPay_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboGrossPay.Validating, _
                                                                                                                         cboOT1Hours.Validating, cboOT1Amount.Validating, _
                                                                                                                         cboOT2Hours.Validating, cboOT2Amount.Validating, _
                                                                                                                         cboOT3Hours.Validating, cboOT3Amount.Validating, _
                                                                                                                         cboOT4Hours.Validating, cboOT4Amount.Validating, _
                                                                                                                         cboForFirst3Years.Validating, cboBalanceYear.Validating, _
                                                                                                                         cboBalanceMonth.Validating, cboBalanceDays.Validating, _
                                                                                                                         cboBalanceYearAmt.Validating, cboBalanceMonthAmt.Validating, _
                                                                                                                         cboBalanceDaysAmt.Validating, _
                                                                                                                         cboLeaveBalanceHead.Validating

        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteria.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboEmployee.Name AndAlso t.Name <> cboPeriod.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrossPay_Validating", mstrModuleName)
        End Try
    End Sub
#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
			Me.colhTranCode.Text = Language._Object.getCaption(CStr(Me.colhTranCode.Tag), Me.colhTranCode.Text)
			Me.colhTranName.Text = Language._Object.getCaption(CStr(Me.colhTranName.Tag), Me.colhTranName.Text)
			Me.colhTranHeadType.Text = Language._Object.getCaption(CStr(Me.colhTranHeadType.Tag), Me.colhTranHeadType.Text)
			Me.lblOT1Hours.Text = Language._Object.getCaption(Me.lblOT1Hours.Name, Me.lblOT1Hours.Text)
			Me.lblOT4Hours.Text = Language._Object.getCaption(Me.lblOT4Hours.Name, Me.lblOT4Hours.Text)
			Me.lblOT3Hours.Text = Language._Object.getCaption(Me.lblOT3Hours.Name, Me.lblOT3Hours.Text)
			Me.lblOT2Hours.Text = Language._Object.getCaption(Me.lblOT2Hours.Name, Me.lblOT2Hours.Text)
			Me.lblOT4Amount.Text = Language._Object.getCaption(Me.lblOT4Amount.Name, Me.lblOT4Amount.Text)
			Me.lblOT3Amount.Text = Language._Object.getCaption(Me.lblOT3Amount.Name, Me.lblOT3Amount.Text)
			Me.lblOT2Amount.Text = Language._Object.getCaption(Me.lblOT2Amount.Name, Me.lblOT2Amount.Text)
			Me.lblOT1Amount.Text = Language._Object.getCaption(Me.lblOT1Amount.Name, Me.lblOT1Amount.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
			Me.lblGrossPay.Text = Language._Object.getCaption(Me.lblGrossPay.Name, Me.lblGrossPay.Text)
			Me.lblForFirst3Years.Text = Language._Object.getCaption(Me.lblForFirst3Years.Name, Me.lblForFirst3Years.Text)
			Me.lblBalanceDays.Text = Language._Object.getCaption(Me.lblBalanceDays.Name, Me.lblBalanceDays.Text)
			Me.lblBalanceMonth.Text = Language._Object.getCaption(Me.lblBalanceMonth.Name, Me.lblBalanceMonth.Text)
			Me.lblBalanceYear.Text = Language._Object.getCaption(Me.lblBalanceYear.Name, Me.lblBalanceYear.Text)
			Me.lblLeaveBalanceHead.Text = Language._Object.getCaption(Me.lblLeaveBalanceHead.Name, Me.lblLeaveBalanceHead.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
			Me.lblBalanceDaysAmt.Text = Language._Object.getCaption(Me.lblBalanceDaysAmt.Name, Me.lblBalanceDaysAmt.Text)
			Me.lblBalanceMonthAmt.Text = Language._Object.getCaption(Me.lblBalanceMonthAmt.Name, Me.lblBalanceMonthAmt.Text)
			Me.lblBalanceYearAmt.Text = Language._Object.getCaption(Me.lblBalanceYearAmt.Name, Me.lblBalanceYearAmt.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Period.")
			Language.setMessage(mstrModuleName, 2, "Please Select Employee.")
			Language.setMessage(mstrModuleName, 3, "Please tick atleast one transaction head.")
			Language.setMessage(mstrModuleName, 4, "Please Select Gross Pay Head.")
			Language.setMessage(mstrModuleName, 5, "Please Select Leave Balance Head.")
			Language.setMessage(mstrModuleName, 6, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 7, "Sorry, This transaction head is already mapped.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
