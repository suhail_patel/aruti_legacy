#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class frmSalaryReconciliationReport

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmSalaryReconciliationReport"
    Private objESC As clsSalaryReconciliationReport
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty

    Private mstrTranDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0

#End Region

#Region " Constructor "

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        objESC = New clsSalaryReconciliationReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objESC.SetDefaultValue()

        'Sohail (30 Dec 2015) -- Start
        'Enhancement - Custom Advance Excel for Salary Reconciliation for FDRC.
        '_Show_ExcelExtra_Menu = False
        _Show_ExcelExtra_Menu = True
        'Sohail (30 Dec 2015) -- End

        _Show_AdvanceFilter = True

    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Try

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'Anjan [10 June 2015] -- End

            With cboEmployeeName
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With


            Dim objCommon As New clsCommon_Master
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "SalReason")
            With cboReason
                .ValueMember = "masterunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("SalReason")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0).Copy
                .SelectedValue = mintFirstOpenPeriod
            End With
            If cboFromPeriod.SelectedIndex > 0 Then cboFromPeriod.SelectedIndex = cboToPeriod.SelectedIndex - 1

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployeeName.SelectedValue = 0
            txtAmount.Text = ""
            txtAmountTo.Text = ""
            txtIncrement.Text = ""
            txtIncrementTo.Text = ""
            txtNewScale.Text = ""
            txtNewScaleTo.Text = ""
            dtpDateFrom.Checked = False
            dtpDateTo.Checked = False
            objESC.setDefaultOrderBy(0)
            txtOrderBy.Text = objESC.OrderByDisplay
            cboFromPeriod.SelectedValue = mintFirstOpenPeriod
            cboToPeriod.SelectedValue = mintFirstOpenPeriod
            If cboFromPeriod.SelectedIndex > 0 Then cboFromPeriod.SelectedIndex = cboToPeriod.SelectedIndex - 1

            mstrAdvanceFilter = ""

            cboReason.SelectedIndex = 0


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        'Dim dsList As DataSet
        Dim strPeriodIDs As String = ""
        Dim arrDatabaseName As New ArrayList
        Dim i As Integer = 0
        Dim intPrevYearID As Integer = 0
        Try

            'Sohail (10 May 2015) -- Start
            'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
            'Sohail (14 Nov 2017) -- Start
            'Enhancement - 70.1 - Salary Reconciliation Report for all Allocations.
            'If mintViewIdx <= 0 Then

            '    mintViewIdx = 1
            '    mstrAnalysis_Fields = ", hrstation_master.stationunkid AS Id, hrstation_master.name AS GName "
            '    mstrAnalysis_Join = "JOIN ( " & _
            '                            "SELECT      stationunkid     ,deptgroupunkid     ,departmentunkid     ,sectiongroupunkid     ,sectionunkid     ,unitgroupunkid     ,unitunkid     ,teamunkid     ,classgroupunkid     ,classunkid     ,employeeunkid " & _
            '                            "FROM ( SELECT              stationunkid " & _
            '                                                      ",deptgroupunkid " & _
            '                                                      ",departmentunkid " & _
            '                                                      ",sectiongroupunkid " & _
            '                                                      ",sectionunkid " & _
            '                                                      ",unitgroupunkid " & _
            '                                                      ",unitunkid " & _
            '                                                      ",teamunkid " & _
            '                                                      ",classgroupunkid " & _
            '                                                      ",classunkid " & _
            '                                                      ",employeeunkid " & _
            '                                                      ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '                                    "FROM #emptransfer " & _
            '                                    "WHERE isvoid = 0 " & _
            '                                    "AND CONVERT(CHAR(8),effectivedate,112) <= @toperiodenddate ) AS A WHERE A.rno = 1 " & _
            '                            ") AS VB_TRF ON VB_TRF.employeeunkid = hremployee_master.employeeunkid  JOIN hrstation_master ON hrstation_master.stationunkid = hremployee_master.stationunkid  "

            '    mstrAnalysis_OrderBy = "hrstation_master.stationunkid "
            '    mstrReport_GroupName = "Branch :"

            'End If
            'Sohail (14 Nov 2017) -- End
            'Sohail (10 May 2015) -- End

            If CInt(cboFromPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select From Period."), enMsgBoxStyle.Information)
                cboFromPeriod.Select()
                Exit Function
            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select To Period."), enMsgBoxStyle.Information)
                cboToPeriod.Select()
                Exit Function
            ElseIf cboFromPeriod.SelectedIndex >= cboToPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period."), enMsgBoxStyle.Information)
                cboFromPeriod.Select()
                Exit Function
            ElseIf mintViewIdx <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please set Analysis By."), enMsgBoxStyle.Information)
                cboToPeriod.Select()
                Exit Function
            Else
                If dtpDateFrom.Checked = True Then
                    If eZeeDate.convertDate(dtpDateFrom.Value.Date) < CType(cboFromPeriod.SelectedItem, DataRowView).Item("start_date").ToString OrElse eZeeDate.convertDate(dtpDateFrom.Value.Date) > CType(cboToPeriod.SelectedItem, DataRowView).Item("end_date").ToString Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, From Date should be between From Period start date and end date."), enMsgBoxStyle.Information)
                        dtpDateFrom.Select()
                        Exit Function
                    End If
                End If

                If dtpDateTo.Checked = True Then
                    If eZeeDate.convertDate(dtpDateTo.Value.Date) < CType(cboFromPeriod.SelectedItem, DataRowView).Item("start_date").ToString OrElse eZeeDate.convertDate(dtpDateTo.Value.Date) > CType(cboToPeriod.SelectedItem, DataRowView).Item("end_date").ToString Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, To Date should be between To Period start date and end date."), enMsgBoxStyle.Information)
                        dtpDateTo.Select()
                        Exit Function
                    End If
                End If
            End If

            objESC._Employeeunkid = cboEmployeeName.SelectedValue
            objESC._Emp_Name = cboEmployeeName.Text

            If Trim(txtAmount.Text) <> "" And Trim(txtAmountTo.Text) <> "" Then
                objESC._Current_ScaleFrom = txtAmount.Text
                objESC._Current_ScaleTo = txtAmountTo.Text
            End If

            If Trim(txtIncrement.Text) <> "" And Trim(txtIncrementTo.Text) <> "" Then
                objESC._Increment_From = txtIncrement.Text
                objESC._Increment_To = txtIncrementTo.Text
            End If

            If Trim(txtNewScale.Text) <> "" And Trim(txtNewScaleTo.Text) <> "" Then
                objESC._NewScale_From = txtNewScale.Text
                objESC._NewScale_To = txtNewScaleTo.Text
            End If

            objESC._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked


            'For Each dsRow As DataRow In CType(cboFromPeriod.DataSource, DataTable).Rows
            '    If i >= cboFromPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
            '        strPeriodIDs &= ", " & dsRow.Item("periodunkid").ToString
            '        If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
            '            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
            '            If dsList.Tables("Database").Rows.Count > 0 Then
            '                arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
            '            End If
            '        End If
            '        intPrevYearID = CInt(dsRow.Item("yearunkid"))
            '    End If
            '    i += 1
            'Next
            'If strPeriodIDs.Trim <> "" Then strPeriodIDs = strPeriodIDs.Substring(2)
            'If arrDatabaseName.Count <= 0 Then Return False

            'objESC._PeriodIDs = strPeriodIDs
            'objESC._Arr_DatabaseName = arrDatabaseName

            objESC._FromPeriodUnkID = CInt(cboFromPeriod.SelectedValue)
            objESC._FromPeriodName = cboFromPeriod.Text
            objESC._ToPeriodUnkID = CInt(cboToPeriod.SelectedValue)
            objESC._ToPeriodName = cboToPeriod.Text

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboFromPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboFromPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objESC._FromPeriodStartDate = objPeriod._Start_Date
            objESC._FromPeriodEndDate = objPeriod._End_Date
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objESC._FromDatabaseName = objPeriod.GetDatabaseName(objPeriod._Yearunkid)
            objESC._FromDatabaseName = objPeriod.GetDatabaseName(objPeriod._Yearunkid, Company._Object._Companyunkid)
            'Sohail (21 Aug 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboToPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objESC._ToPeriodStartDate = objPeriod._Start_Date
            objESC._ToPeriodEndDate = objPeriod._End_Date
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objESC._ToDatabaseName = objPeriod.GetDatabaseName(objPeriod._Yearunkid)
            objESC._ToDatabaseName = objPeriod.GetDatabaseName(objPeriod._Yearunkid, Company._Object._Companyunkid)
            'Sohail (21 Aug 2015) -- End
            objPeriod = Nothing

            objESC._ViewByIds = mstrStringIds
            objESC._ViewIndex = mintViewIdx
            objESC._ViewByName = mstrStringName
            objESC._Analysis_Fields = mstrAnalysis_Fields
            objESC._Analysis_Join = mstrAnalysis_Join
            objESC._Analysis_OrderBy = mstrAnalysis_OrderBy
            objESC._Report_GroupName = mstrReport_GroupName

            If dtpDateFrom.Checked = True Then
                objESC._IncrementDateFrom = dtpDateFrom.Value
            End If

            If dtpDateTo.Checked = True Then
                objESC._IncrementDateTo = dtpDateTo.Value
            End If


            objESC._Advance_Filter = mstrAdvanceFilter

            objESC._ReasonId = CInt(cboReason.SelectedValue)
            objESC._Reason = cboReason.Text


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form Events "

    Private Sub frmSalaryReconciliationReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objESC = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryReconciliationReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSalaryReconciliationReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Me._Title = objESC._ReportName
            Me._Message = objESC._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryReconciliationReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objESC.generateReport(0, e.Type, enExportAction.None)
            Dim dtPeriodStart As Date
            Dim dtPeriodEnd As Date
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboFromPeriod.SelectedValue)
            dtPeriodStart = objPeriod._Start_Date
            objPeriod = New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            dtPeriodEnd = objPeriod._End_Date
            'Sohail (10 May 2015) -- Start
            'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
            'objESC.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, chkIncludeInactiveEmp.Checked, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            objESC.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'Sohail (10 May 2015) -- End
            'Sohail (21 Aug 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objESC.generateReport(0, enPrintAction.None, e.Type)
            Dim dtPeriodStart As Date
            Dim dtPeriodEnd As Date
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboFromPeriod.SelectedValue)
            dtPeriodStart = objPeriod._Start_Date
            objPeriod = New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            dtPeriodEnd = objPeriod._End_Date
            'Sohail (10 May 2015) -- Start
            'Enhancement - 60.1 - Salary Reconciliation Report as per 58.1 allocation structure.
            'objESC.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, chkIncludeInactiveEmp.Checked, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            objESC.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Sohail (10 May 2015) -- End
            'Sohail (21 Aug 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployeeName.DataSource
            frm.SelectedValue = cboEmployeeName.SelectedValue
            frm.ValueMember = cboEmployeeName.ValueMember
            frm.DisplayMember = cboEmployeeName.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployeeName.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub objbtnSearchReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchReason.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboReason.DataSource
            frm.SelectedValue = cboReason.SelectedValue
            frm.ValueMember = cboReason.ValueMember
            frm.DisplayMember = cboReason.DisplayMember
            If frm.DisplayDialog Then
                cboReason.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReason_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSalaryReconciliationReport.SetMessages()
            objfrm._Other_ModuleNames = "clsSalaryReconciliationReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


#End Region

#Region " Other Control's Events "

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click

        Try
            objESC.setOrderBy(0)
            txtOrderBy.Text = objESC.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)
            Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.Name, Me.lblFromPeriod.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
            Me.lblDateFrom.Text = Language._Object.getCaption(Me.lblDateFrom.Name, Me.lblDateFrom.Text)
            Me.lblDateTo.Text = Language._Object.getCaption(Me.lblDateTo.Name, Me.lblDateTo.Text)
            Me.lblNewScaleTo.Text = Language._Object.getCaption(Me.lblNewScaleTo.Name, Me.lblNewScaleTo.Text)
            Me.lblNewScale.Text = Language._Object.getCaption(Me.lblNewScale.Name, Me.lblNewScale.Text)
            Me.lblIncrementTo.Text = Language._Object.getCaption(Me.lblIncrementTo.Name, Me.lblIncrementTo.Text)
            Me.lblIncrement.Text = Language._Object.getCaption(Me.lblIncrement.Name, Me.lblIncrement.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
            Me.lblAmountTo.Text = Language._Object.getCaption(Me.lblAmountTo.Name, Me.lblAmountTo.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
            Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select From Period.")
            Language.setMessage(mstrModuleName, 2, "Please Select To Period.")
            Language.setMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period.")
            Language.setMessage(mstrModuleName, 4, "Please set Analysis By.")
            Language.setMessage(mstrModuleName, 5, "Sorry, From Date should be between From Period start date and end date.")
            Language.setMessage(mstrModuleName, 6, "Sorry, To Date should be between To Period start date and end date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
