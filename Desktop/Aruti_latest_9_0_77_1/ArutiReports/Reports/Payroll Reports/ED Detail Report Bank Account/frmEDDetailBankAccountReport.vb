'************************************************************************************************************************************
'Class Name : frmEDDetailBankAccountReport.vb
'Purpose    : 
'Written By : Suhail
'Modified   : 03/12/2014
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmEDDetailBankAccountReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEDDetailBankAccountReport"
    Private objEDDetail As clsEDDetailBankAccountReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private dsPeriod As DataSet = Nothing
    Private mstrTranDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0

    Private mstrAdvanceFilter As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Constructor "

    Public Sub New()
        objEDDetail = New clsEDDetailBankAccountReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objEDDetail.SetDefaultValue()
        InitializeComponent()


        _Show_ExcelExtra_Menu = True

        _Show_AdvanceFilter = True

    End Sub

#End Region

    'Sohail (22 Jan 2019) -- Start
    'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
#Region " Private Enum "
    Private Enum enHeadTypeId
        Employee_Bank = 1
        Employee_Branch = 2
        Include_Inactive_Employee = 3
        Include_Negative_Heads = 4
        'Sohail (11 Feb 2021) -- Start
        'SCANIA TZ Enhancement : OLD-273 : E&D Detailed report with Bank Account to include Full Employee Name.
        ShowEmpNameInSingleColumn = 5
        EmpNameFormat = 6
        'Sohail (11 Feb 2021) -- End
    End Enum
#End Region
    'Sohail (22 Jan 2019) -- End

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim objBank As New clspayrollgroup_master
        Dim objMember As New clsmembership_master
        Dim dsList As DataSet
        Try

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ' dsList = objEmp.GetEmployeeList("Emp", True, False)
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'Anjan [10 June 2015] -- End


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsPeriod.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With


            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsPeriod.Tables(0).Copy
                .SelectedValue = mintFirstOpenPeriod
            End With
            objPeriod = Nothing

            dsList = objMaster.getComboListForHeadType("List")
            'Sohail (11 Nov 2021) -- Start
            'TRA Enhancement : OLD-494 : Earnings and Informational heads mode selection on ED Detailed Report with Bank Account.
            'Dim dtTable As DataTable = New DataView(dsList.Tables("List"), "Id IN (0, " & enTranHeadType.DeductionForEmployee & ") ", "", DataViewRowState.CurrentRows).ToTable
            Dim dtTable As DataTable = New DataView(dsList.Tables("List"), "Id IN (0, " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.Informational & ") ", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (11 Nov 2021) -- End
            Dim dr As DataRow = dtTable.NewRow
            dr.Item("Id") = 99999
            dr.Item("NAME") = Language.getMessage(mstrModuleName, 7, "Advance")
            dtTable.Rows.Add(dr)
            'Sohail (23 Jun 2015) -- Start
            'Enhancement - Claim Request Expense on E&D Detail Reports.
            dr = dtTable.NewRow
            dr.Item("Id") = 99998
            dr.Item("Name") = Language.getMessage(mstrModuleName, 8, "Claim Request Expense")
            dtTable.Rows.Add(dr)
            'Sohail (23 Jun 2015) -- End
            With cboMode
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                '.DataSource = dsList.Tables(0)
                .DataSource = dtTable
            End With

            dsList = objBank.getListForCombo(enPayrollGroupType.Bank, "List", True)
            With cboEmpBank
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objMember.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (25 May 2021) -- Start
            'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
            Dim objCompanyBank As New clsCompany_Bank_tran
            dsList = objCompanyBank.GetComboListBankAccount("Account", Company._Object._Companyunkid, True)
            With cboCompanyAccountNo
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (25 May 2021) -- End

            dsList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objPeriod = Nothing
            objMaster = Nothing
            objBank = Nothing
            objMember = Nothing
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim strPeriodIDs As String = ""
        Dim arrDatabaseName As New ArrayList
        Dim i As Integer = 0
        Dim intPrevYearID As Integer = 0
        'Sohail (22 Jan 2019) -- Start
        'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
        Dim arrYearWisePeriodIDs As New ArrayList
        'Sohail (22 Jan 2019) -- End

        Try
            If CInt(cboMode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Mode."), enMsgBoxStyle.Information)
                cboMode.Select()
                Exit Function
            ElseIf CInt(cboMode.SelectedValue) > 0 AndAlso CInt(cboMode.SelectedValue) <> 99999 AndAlso CInt(cboHeades.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Head."), enMsgBoxStyle.Information)
                cboHeades.Select()
                Exit Function
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Function
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 And CInt(cboToPeriod.SelectedValue) > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select From Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Function

            ElseIf CInt(cboPeriod.SelectedValue) > 0 And CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Select To Period."), enMsgBoxStyle.Information)
                cboToPeriod.Select()
                Exit Function

            End If

            If cboToPeriod.SelectedIndex < cboPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, " To Period cannot be less than From Period"), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Function
            End If

            objEDDetail.SetDefaultValue()

            objEDDetail._EmployeeId = CInt(cboEmployee.SelectedValue)
            objEDDetail._EmployeeName = cboEmployee.Text

            objEDDetail._ModeId = CInt(cboMode.SelectedValue)
            objEDDetail._ModeName = cboMode.Text

            objEDDetail._EmpBankId = CInt(cboEmpBank.SelectedValue)
            objEDDetail._EmpBankName = cboEmpBank.Text

            objEDDetail._EmpBranchId = CInt(cboEmpBranch.SelectedValue)
            objEDDetail._EmpBranchName = cboEmpBranch.Text

            objEDDetail._HeadId = CInt(cboHeades.SelectedValue)
            objEDDetail._HeadName = cboHeades.Text
            If cboHeades.DataSource IsNot Nothing Then
                objEDDetail._HeadCode = CType(cboHeades.SelectedItem, DataRowView).Item("code").ToString
            End If

            objEDDetail._PeriodId = CInt(cboPeriod.SelectedValue)
            objEDDetail._PeriodName = cboPeriod.Text

            objEDDetail._ViewByIds = mstrStringIds
            objEDDetail._ViewIndex = mintViewIdx
            objEDDetail._ViewByName = mstrStringName
            objEDDetail._Analysis_Fields = mstrAnalysis_Fields
            objEDDetail._Analysis_Join = mstrAnalysis_Join
            objEDDetail._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEDDetail._Report_GroupName = mstrReport_GroupName

            objEDDetail._IsActive = chkInactiveemp.Checked
            objEDDetail._IncludeNegativeHeads = chkIncludeNegativeHeads.Checked

            'Sohail (25 May 2021) -- Start
            'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
            If CInt(cboCompanyAccountNo.SelectedValue) > 0 Then
                objEDDetail._CompanyAccountNo = cboCompanyAccountNo.Text
            Else
                objEDDetail._CompanyAccountNo = ""
            End If
            'Sohail (25 May 2021) -- End

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objEDDetail._PeriodStartDate = objPeriod._Start_Date

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboToPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objEDDetail._PeriodEndDate = objPeriod._End_Date

            objEDDetail._ToPeriodId = CInt(cboToPeriod.SelectedValue)
            objEDDetail._ToPeriodName = cboToPeriod.Text

            For Each dsRow As DataRow In dsPeriod.Tables(0).Rows
                If i >= cboPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strPeriodIDs &= ", " & dsRow.Item("periodunkid").ToString
                    If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                        dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
                        If dsList.Tables("Database").Rows.Count > 0 Then
                            arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                            'Sohail (22 Jan 2019) -- Start
                            'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                            arrYearWisePeriodIDs.Add(dsRow.Item("periodunkid").ToString)
                            'Sohail (22 Jan 2019) -- End
                        End If
                        'Sohail (22 Jan 2019) -- Start
                        'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                    Else
                        arrYearWisePeriodIDs.Item(arrYearWisePeriodIDs.Count - 1) &= "," & dsRow.Item("periodunkid").ToString
                        'Sohail (22 Jan 2019) -- End
                    End If
                    intPrevYearID = CInt(dsRow.Item("yearunkid"))
                End If
                i += 1
            Next
            If strPeriodIDs.Trim <> "" Then strPeriodIDs = strPeriodIDs.Substring(2)
            If arrDatabaseName.Count <= 0 Then Return False

            objEDDetail._mstrPeriodID = strPeriodIDs
            objEDDetail._Arr_DatabaseName = arrDatabaseName
            'Sohail (22 Jan 2019) -- Start
            'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
            objEDDetail._Arr_YearWisePeriodIDs = arrYearWisePeriodIDs
            'Sohail (22 Jan 2019) -- End

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objEDDetail._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            objPeriod = Nothing

            objEDDetail._Advance_Filter = mstrAdvanceFilter

            objEDDetail._MembershipId = CInt(cboMembership.SelectedValue)
            objEDDetail._MemberShipName = cboMembership.Text

            'Sohail (11 Feb 2021) -- Start
            'SCANIA TZ Enhancement : OLD-273 : E&D Detailed report with Bank Account to include Full Employee Name.
            objEDDetail._ShowEmpNameInSingleColumn = chkShowEmpInSingleColumn.Checked
            objEDDetail._EmpNameFormat = txtNameFormat.Text
            'Sohail (11 Feb 2021) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objEDDetail.setDefaultOrderBy(0)
            txtOrderBy.Text = objEDDetail.OrderByDisplay
            cboEmployee.SelectedIndex = 0
            cboMode.SelectedIndex = 0
            cboHeades.SelectedIndex = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboEmpBank.SelectedValue = 0
            cboEmpBranch.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            chkInactiveemp.Checked = False
            chkIncludeNegativeHeads.Checked = True

            cboToPeriod.SelectedValue = mintFirstOpenPeriod

            mstrAdvanceFilter = ""

            cboMembership.SelectedValue = 0

            'Sohail (11 Feb 2021) -- Start
            'SCANIA TZ Enhancement : OLD-273 : E&D Detailed report with Bank Account to include Full Employee Name.
            chkShowEmpInSingleColumn.Checked = False
            txtNameFormat.Text = "#firstname# #middlename# #surname#"
            txtNameFormat.Enabled = False
            'Sohail (11 Feb 2021) -- End

            'Sohail (22 Jan 2019) -- Start
            'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
            Call GetValue()
            'Sohail (22 Jan 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    'Sohail (22 Jan 2019) -- Start
    'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try

            dsList = objUserDefRMode.GetList("List", enArutiReport.EDDetail_Bank_Account_Report, 0)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Employee_Bank
                            cboEmpBank.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Employee_Branch
                            cboEmpBranch.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Include_Inactive_Employee
                            chkInactiveemp.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Include_Negative_Heads
                            chkIncludeNegativeHeads.Checked = CBool(dsRow.Item("transactionheadid"))

                            'Sohail (11 Feb 2021) -- Start
                            'SCANIA TZ Enhancement : OLD-273 : E&D Detailed report with Bank Account to include Full Employee Name.
                        Case enHeadTypeId.ShowEmpNameInSingleColumn
                            chkShowEmpInSingleColumn.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.EmpNameFormat
                            txtNameFormat.Text = dsRow.Item("transactionheadid").ToString
                            'Sohail (11 Feb 2021) -- End


                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (22 Jan 2019) -- End

#End Region

#Region " Form's Events "

    Private Sub frmEDDetailReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEDDetail = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDDetailReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Me._Title = objEDDetail._ReportName
            Me._Message = objEDDetail._ReportDesc
            Call FillCombo()
            Call ResetValue()

            'Sohail (12 May 2021) -- Start
            'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
            'Sohail (27 Jul 2021) -- Start
            'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
            'If ConfigParameter._Object._EFTIntegration = CInt(enEFTIntegration.EFT_CityDirect) Then
            If ConfigParameter._Object._EFTIntegration = CInt(enEFTIntegration.EFT_CITI_BANK_KENYA) Then
                'Sohail (27 Jul 2021) -- End
                lnkAdvanceEFTReport.Visible = True
            Else
                lnkAdvanceEFTReport.Visible = False
            End If
            'Sohail (12 May 2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDSpreadSheetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEDDetailReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboHeades
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If SetFilter() = False Then Exit Sub

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEDDetail.generateReport(0, e.Type, enExportAction.None)
            objEDDetail.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
                                          0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDDetailReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If SetFilter() = False Then Exit Sub

            'Sohail (22 Jan 2019) -- Start
            'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
            If e.Type = enExportAction.ExcelExtra Then

                Dim objFrm As New frmEFTCustomColumnsExport

                If objFrm.displayDialog(enArutiReport.EDDetail_Bank_Account_Report, enEFT_Export_Mode.XLS, 1) = True Then

                    objEDDetail._EFTCustomXLSColumnsIds = objFrm._EFTCustomColumnsIds
                    objEDDetail._EFTMembershipId = objFrm._EFTMembershipUnkId
                    objEDDetail._EFTShowColumnHeader = objFrm._EFTShowColumnHeader

                    If objFrm._EFTCustomColumnsIds.Split(",").Contains(CInt(enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO).ToString) = True Then
                        objEDDetail._MembershipId = objFrm._EFTMembershipUnkId
                    Else
                        objEDDetail._MembershipId = 0
                    End If
                Else
                    objFrm = Nothing
                    Exit Try
                End If
                objFrm = Nothing

            End If
            'Sohail (22 Jan 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEDDetail.generateReport(0, enPrintAction.None, e.Type)
            objEDDetail.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
                                          0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDDetailReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (12 May 2021) -- Start
    'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
    Private Sub lnkAdvanceEFTReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceEFTReport.Click
        Try
            If CInt(cboPeriod.SelectedValue) <> CInt(cboToPeriod.SelectedValue) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, From period and To period must be same."), enMsgBoxStyle.Information)
                Exit Try
                'Sohail (25 May 2021) -- Start
                'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
            ElseIf CInt(cboCompanyAccountNo.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select company bank account number."), enMsgBoxStyle.Information)
                cboCompanyAccountNo.Focus()
                Exit Try
                'Sohail (25 May 2021) -- End
            End If

            If SetFilter() = False Then Exit Sub

            objEDDetail._ExportToEFTCityDirectTXT = True

            SaveDialog.FileName = ""
            SaveDialog.Filter = "Text File|*.txt"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            objEDDetail.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          SaveDialog.FileName, ConfigParameter._Object._OpenAfterExport, _
                                          0, enPrintAction.None, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)

            If objEDDetail._IsExportEFTCitiDirectSuccess - True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Data Exported Successfuly."), enMsgBoxStyle.Information)
                If ConfigParameter._Object._OpenAfterExport = True Then
                    Process.Start(SaveDialog.FileName)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvanceEFTReport_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 May 2021) -- End

    Private Sub frmEDDetailReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDDetailReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDDetailReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEDDetailReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEDDetailBankAccountReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEDDetailBankAccountReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEDDetailReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMembership.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboMembership
                objfrm.DataSource = cboMembership.DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMembership_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    'Sohail (22 Jan 2019) -- Start
    'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.EDDetail_Bank_Account_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Employee_Bank
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmpBank.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.EDDetail_Bank_Account_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Employee_Branch
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmpBranch.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.EDDetail_Bank_Account_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Include_Inactive_Employee
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(Int(chkInactiveemp.Checked)).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.EDDetail_Bank_Account_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Include_Negative_Heads
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(Int(chkIncludeNegativeHeads.Checked)).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.EDDetail_Bank_Account_Report, 0, 0, intHeadType)

                        'Sohail (11 Feb 2021) -- Start
                        'SCANIA TZ Enhancement : OLD-273 : E&D Detailed report with Bank Account to include Full Employee Name.
                    Case enHeadTypeId.ShowEmpNameInSingleColumn
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(Int(chkShowEmpInSingleColumn.Checked)).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.EDDetail_Bank_Account_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.EmpNameFormat
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtNameFormat.Text

                        intUnkid = objUserDefRMode.isExist(enArutiReport.EDDetail_Bank_Account_Report, 0, 0, intHeadType)
                        'Sohail (11 Feb 2021) -- End

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (22 Jan 2019) -- End

#End Region

#Region " LinkLabel Event "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'Sohail (22 Jan 2019) -- Start
    'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
    Private Sub lnkIncludeOtherColumns_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkIncludeOtherColumns.LinkClicked

        Try
            If SetFilter() = False Then Exit Sub

            Dim objFrm As New frmEFTCustomColumnsExport

            If objFrm.displayDialog(enArutiReport.EDDetail_Bank_Account_Report, enEFT_Export_Mode.XLS, 1) = True Then

                objEDDetail._EFTCustomXLSColumnsIds = objFrm._EFTCustomColumnsIds
                objEDDetail._EFTMembershipId = objFrm._EFTMembershipUnkId
                objEDDetail._EFTShowColumnHeader = objFrm._EFTShowColumnHeader

                If objFrm._EFTCustomColumnsIds.Split(",").Contains(CInt(enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO).ToString) = True Then
                    objEDDetail._MembershipId = objFrm._EFTMembershipUnkId
                Else
                    objEDDetail._MembershipId = 0
                End If

                objEDDetail.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
                                          0, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._Base_CurrencyId)

            End If
            objFrm = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkIncludeOtherColumns_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (22 Jan 2019) -- End

#End Region

#Region " Control "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEDDetail.setOrderBy(0)
            txtOrderBy.Text = objEDDetail.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (11 Feb 2021) -- Start
    'SCANIA TZ Enhancement : OLD-273 : E&D Detailed report with Bank Account to include Full Employee Name.
    Private Sub objbtnKeywords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnKeywords.Click
        Dim frm As New frmRemark
        Try
            frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 9, "Awailable Keywords")
            frm.Text = frm.objgbRemarks.Text
            Dim strRemarks As String = ""
            Dim strB As New StringBuilder
            strB.Length = 0

            strB.AppendLine("#firstname#")
            strB.AppendLine("#middlename#")
            strB.AppendLine("#surname#")
            strRemarks = strB.ToString

            frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, eZee.Common.eZeeGradientButton).Name & "_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub chkShowEmpInSingleColumn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowEmpInSingleColumn.CheckedChanged
        Try
            txtNameFormat.Enabled = chkShowEmpInSingleColumn.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowEmpInSingleColumn_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Feb 2021) -- End

#End Region

#Region " ComboBox Event "

    Private Sub cboMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Dim objTransactionhead As New clsTransactionHead
        Dim dsFill As DataSet = Nothing
        Try
            If CInt(cboMode.SelectedValue) = 99999 Then 'Advance
                With cboHeades
                    .DataSource = Nothing
                End With
                'Sohail (23 Jun 2015) -- Start
                'Enhancement - Claim Request Expense on E&D Detail Reports.
            ElseIf CInt(cboMode.SelectedValue) = 99998 Then 'Claim Request
                Dim objExpense As New clsExpense_Master
                dsFill = objExpense.getComboList(0, True, "List")
                With cboHeades
                    .DataSource = Nothing
                    cboHeades.ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsFill.Tables("List")
                End With
                'Sohail (23 Jun 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsFill = objTransactionhead.getComboList("List", True, CInt(cboMode.SelectedValue), , , , , , , , True)
                dsFill = objTransactionhead.getComboList(FinancialYear._Object._DatabaseName, "List", True, CInt(cboMode.SelectedValue), , , , , , , True)
                'Sohail (21 Aug 2015) -- End
                With cboHeades
                    .DataSource = Nothing
                    .ValueMember = "tranheadunkid"
                    .DisplayMember = "Name"
                    .DataSource = dsFill.Tables("List")
                End With
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        Finally
            objTransactionhead = Nothing
        End Try
    End Sub

    Private Sub cboEmpBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpBank.SelectedIndexChanged
        Dim objEmpBank As New clsbankbranch_master
        Dim dsList As DataSet
        Try
            dsList = objEmpBank.getListForCombo("List", True, CInt(cboEmpBank.SelectedValue))
            With cboEmpBranch
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpBank_SelectedIndexChanged", mstrModuleName)
        Finally
            objEmpBank = Nothing
            dsList = Nothing
        End Try
    End Sub

#End Region


    

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LblMembership.Text = Language._Object.getCaption(Me.LblMembership.Name, Me.LblMembership.Text)
			Me.chkIncludeNegativeHeads.Text = Language._Object.getCaption(Me.chkIncludeNegativeHeads.Name, Me.chkIncludeNegativeHeads.Text)
			Me.LblTo.Text = Language._Object.getCaption(Me.LblTo.Name, Me.LblTo.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblHeads.Text = Language._Object.getCaption(Me.lblHeads.Name, Me.lblHeads.Text)
			Me.lblSelectMode.Text = Language._Object.getCaption(Me.lblSelectMode.Name, Me.lblSelectMode.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblEmpBank.Text = Language._Object.getCaption(Me.lblEmpBank.Name, Me.lblEmpBank.Text)
			Me.lblEmpBranch.Text = Language._Object.getCaption(Me.lblEmpBranch.Name, Me.lblEmpBranch.Text)
			Me.lnkIncludeOtherColumns.Text = Language._Object.getCaption(Me.lnkIncludeOtherColumns.Name, Me.lnkIncludeOtherColumns.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.chkShowEmpInSingleColumn.Text = Language._Object.getCaption(Me.chkShowEmpInSingleColumn.Name, Me.chkShowEmpInSingleColumn.Text)
			Me.lblNameFormat.Text = Language._Object.getCaption(Me.lblNameFormat.Name, Me.lblNameFormat.Text)
			Me.lnkAdvanceEFTReport.Text = Language._Object.getCaption(Me.lnkAdvanceEFTReport.Name, Me.lnkAdvanceEFTReport.Text)
			Me.lblCompanyAccountNo.Text = Language._Object.getCaption(Me.lblCompanyAccountNo.Name, Me.lblCompanyAccountNo.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Mode.")
			Language.setMessage(mstrModuleName, 2, "Please Select Head.")
			Language.setMessage(mstrModuleName, 3, "Please Select Period.")
			Language.setMessage(mstrModuleName, 4, "Please Select From Period.")
			Language.setMessage(mstrModuleName, 5, "Please Select To Period.")
			Language.setMessage(mstrModuleName, 6, " To Period cannot be less than From Period")
			Language.setMessage(mstrModuleName, 7, "Advance")
            Language.setMessage(mstrModuleName, 8, "Claim Request Expense")
			Language.setMessage(mstrModuleName, 9, "Awailable Keywords")
			Language.setMessage(mstrModuleName, 10, "Sorry, From period and To period must be same.")
			Language.setMessage(mstrModuleName, 11, "Please select company bank account number.")
			Language.setMessage(mstrModuleName, 19, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 11, "Data Exported Successfuly.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
