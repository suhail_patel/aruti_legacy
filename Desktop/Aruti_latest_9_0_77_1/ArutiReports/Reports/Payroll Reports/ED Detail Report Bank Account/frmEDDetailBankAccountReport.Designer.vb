﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEDDetailBankAccountReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEDDetailBankAccountReport))
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAdvanceEFTReport = New System.Windows.Forms.LinkLabel
        Me.lblNameFormat = New System.Windows.Forms.Label
        Me.objbtnKeywords = New eZee.Common.eZeeGradientButton
        Me.txtNameFormat = New eZee.TextBox.AlphanumericTextBox
        Me.chkShowEmpInSingleColumn = New System.Windows.Forms.CheckBox
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkIncludeOtherColumns = New System.Windows.Forms.LinkLabel
        Me.lblEmpBranch = New System.Windows.Forms.Label
        Me.cboEmpBranch = New System.Windows.Forms.ComboBox
        Me.lblEmpBank = New System.Windows.Forms.Label
        Me.cboEmpBank = New System.Windows.Forms.ComboBox
        Me.objbtnSearchMembership = New eZee.Common.eZeeGradientButton
        Me.LblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.chkIncludeNegativeHeads = New System.Windows.Forms.CheckBox
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.LblTo = New System.Windows.Forms.Label
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblHeads = New System.Windows.Forms.Label
        Me.cboHeades = New System.Windows.Forms.ComboBox
        Me.lblSelectMode = New System.Windows.Forms.Label
        Me.cboMode = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.SaveDialog = New System.Windows.Forms.SaveFileDialog
        Me.cboCompanyAccountNo = New System.Windows.Forms.ComboBox
        Me.lblCompanyAccountNo = New System.Windows.Forms.Label
        Me.gbSortBy.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 553)
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 486)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(401, 63)
        Me.gbSortBy.TabIndex = 23
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(373, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(81, 15)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(95, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(272, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboCompanyAccountNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblCompanyAccountNo)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAdvanceEFTReport)
        Me.gbFilterCriteria.Controls.Add(Me.lblNameFormat)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnKeywords)
        Me.gbFilterCriteria.Controls.Add(Me.txtNameFormat)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowEmpInSingleColumn)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.lnkIncludeOtherColumns)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmpBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmpBranch)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmpBank)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmpBank)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchMembership)
        Me.gbFilterCriteria.Controls.Add(Me.LblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeNegativeHeads)
        Me.gbFilterCriteria.Controls.Add(Me.cboToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.LblTo)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.lblHeads)
        Me.gbFilterCriteria.Controls.Add(Me.cboHeades)
        Me.gbFilterCriteria.Controls.Add(Me.lblSelectMode)
        Me.gbFilterCriteria.Controls.Add(Me.cboMode)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(401, 414)
        Me.gbFilterCriteria.TabIndex = 22
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAdvanceEFTReport
        '
        Me.lnkAdvanceEFTReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAdvanceEFTReport.Location = New System.Drawing.Point(92, 353)
        Me.lnkAdvanceEFTReport.Name = "lnkAdvanceEFTReport"
        Me.lnkAdvanceEFTReport.Size = New System.Drawing.Size(146, 13)
        Me.lnkAdvanceEFTReport.TabIndex = 262
        Me.lnkAdvanceEFTReport.TabStop = True
        Me.lnkAdvanceEFTReport.Text = "Advance EFT Report"
        '
        'lblNameFormat
        '
        Me.lblNameFormat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNameFormat.Location = New System.Drawing.Point(8, 312)
        Me.lblNameFormat.Name = "lblNameFormat"
        Me.lblNameFormat.Size = New System.Drawing.Size(81, 15)
        Me.lblNameFormat.TabIndex = 260
        Me.lblNameFormat.Text = "Name Format"
        Me.lblNameFormat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnKeywords
        '
        Me.objbtnKeywords.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywords.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywords.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywords.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywords.BorderSelected = False
        Me.objbtnKeywords.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywords.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywords.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywords.Location = New System.Drawing.Point(373, 309)
        Me.objbtnKeywords.Name = "objbtnKeywords"
        Me.objbtnKeywords.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywords.TabIndex = 258
        '
        'txtNameFormat
        '
        Me.txtNameFormat.Enabled = False
        Me.txtNameFormat.Flags = 0
        Me.txtNameFormat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNameFormat.HideSelection = False
        Me.txtNameFormat.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtNameFormat.Location = New System.Drawing.Point(95, 309)
        Me.txtNameFormat.Name = "txtNameFormat"
        Me.txtNameFormat.Size = New System.Drawing.Size(272, 21)
        Me.txtNameFormat.TabIndex = 257
        '
        'chkShowEmpInSingleColumn
        '
        Me.chkShowEmpInSingleColumn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmpInSingleColumn.Location = New System.Drawing.Point(95, 290)
        Me.chkShowEmpInSingleColumn.Name = "chkShowEmpInSingleColumn"
        Me.chkShowEmpInSingleColumn.Size = New System.Drawing.Size(272, 17)
        Me.chkShowEmpInSingleColumn.TabIndex = 256
        Me.chkShowEmpInSingleColumn.Text = "Show Employee Name in single column"
        Me.chkShowEmpInSingleColumn.UseVisualStyleBackColor = True
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(95, 374)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 254
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'lnkIncludeOtherColumns
        '
        Me.lnkIncludeOtherColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkIncludeOtherColumns.Location = New System.Drawing.Point(92, 333)
        Me.lnkIncludeOtherColumns.Name = "lnkIncludeOtherColumns"
        Me.lnkIncludeOtherColumns.Size = New System.Drawing.Size(275, 13)
        Me.lnkIncludeOtherColumns.TabIndex = 40
        Me.lnkIncludeOtherColumns.TabStop = True
        Me.lnkIncludeOtherColumns.Text = "&Include Other Columns..."
        '
        'lblEmpBranch
        '
        Me.lblEmpBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpBranch.Location = New System.Drawing.Point(8, 167)
        Me.lblEmpBranch.Name = "lblEmpBranch"
        Me.lblEmpBranch.Size = New System.Drawing.Size(81, 15)
        Me.lblEmpBranch.TabIndex = 38
        Me.lblEmpBranch.Text = "Emp. Branch"
        Me.lblEmpBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmpBranch
        '
        Me.cboEmpBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpBranch.DropDownWidth = 230
        Me.cboEmpBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpBranch.FormattingEnabled = True
        Me.cboEmpBranch.Location = New System.Drawing.Point(95, 165)
        Me.cboEmpBranch.Name = "cboEmpBranch"
        Me.cboEmpBranch.Size = New System.Drawing.Size(272, 21)
        Me.cboEmpBranch.TabIndex = 37
        '
        'lblEmpBank
        '
        Me.lblEmpBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpBank.Location = New System.Drawing.Point(8, 140)
        Me.lblEmpBank.Name = "lblEmpBank"
        Me.lblEmpBank.Size = New System.Drawing.Size(81, 15)
        Me.lblEmpBank.TabIndex = 36
        Me.lblEmpBank.Text = "Emp. Bank"
        Me.lblEmpBank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmpBank
        '
        Me.cboEmpBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpBank.DropDownWidth = 230
        Me.cboEmpBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpBank.FormattingEnabled = True
        Me.cboEmpBank.Location = New System.Drawing.Point(95, 138)
        Me.cboEmpBank.Name = "cboEmpBank"
        Me.cboEmpBank.Size = New System.Drawing.Size(272, 21)
        Me.cboEmpBank.TabIndex = 35
        '
        'objbtnSearchMembership
        '
        Me.objbtnSearchMembership.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMembership.BorderSelected = False
        Me.objbtnSearchMembership.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMembership.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMembership.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMembership.Location = New System.Drawing.Point(373, 192)
        Me.objbtnSearchMembership.Name = "objbtnSearchMembership"
        Me.objbtnSearchMembership.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMembership.TabIndex = 33
        '
        'LblMembership
        '
        Me.LblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMembership.Location = New System.Drawing.Point(8, 194)
        Me.LblMembership.Name = "LblMembership"
        Me.LblMembership.Size = New System.Drawing.Size(81, 15)
        Me.LblMembership.TabIndex = 32
        Me.LblMembership.Text = "Membership"
        Me.LblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 230
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(95, 192)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(272, 21)
        Me.cboMembership.TabIndex = 31
        '
        'chkIncludeNegativeHeads
        '
        Me.chkIncludeNegativeHeads.Checked = True
        Me.chkIncludeNegativeHeads.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIncludeNegativeHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeNegativeHeads.Location = New System.Drawing.Point(95, 268)
        Me.chkIncludeNegativeHeads.Name = "chkIncludeNegativeHeads"
        Me.chkIncludeNegativeHeads.Size = New System.Drawing.Size(192, 16)
        Me.chkIncludeNegativeHeads.TabIndex = 29
        Me.chkIncludeNegativeHeads.Text = "Include Negative Heads"
        Me.chkIncludeNegativeHeads.UseVisualStyleBackColor = True
        '
        'cboToPeriod
        '
        Me.cboToPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToPeriod.DropDownWidth = 200
        Me.cboToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(265, 111)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(102, 21)
        Me.cboToPeriod.TabIndex = 27
        '
        'LblTo
        '
        Me.LblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTo.Location = New System.Drawing.Point(203, 114)
        Me.LblTo.Name = "LblTo"
        Me.LblTo.Size = New System.Drawing.Size(47, 15)
        Me.LblTo.TabIndex = 26
        Me.LblTo.Text = "To"
        Me.LblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(95, 246)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(192, 16)
        Me.chkInactiveemp.TabIndex = 4
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(373, 84)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 24
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(303, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 22
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeads
        '
        Me.lblHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeads.Location = New System.Drawing.Point(8, 86)
        Me.lblHeads.Name = "lblHeads"
        Me.lblHeads.Size = New System.Drawing.Size(81, 15)
        Me.lblHeads.TabIndex = 10
        Me.lblHeads.Text = "Heads"
        Me.lblHeads.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboHeades
        '
        Me.cboHeades.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeades.DropDownWidth = 230
        Me.cboHeades.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHeades.FormattingEnabled = True
        Me.cboHeades.Location = New System.Drawing.Point(95, 84)
        Me.cboHeades.Name = "cboHeades"
        Me.cboHeades.Size = New System.Drawing.Size(272, 21)
        Me.cboHeades.TabIndex = 2
        '
        'lblSelectMode
        '
        Me.lblSelectMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectMode.Location = New System.Drawing.Point(8, 60)
        Me.lblSelectMode.Name = "lblSelectMode"
        Me.lblSelectMode.Size = New System.Drawing.Size(81, 15)
        Me.lblSelectMode.TabIndex = 6
        Me.lblSelectMode.Text = "Mode Selection"
        Me.lblSelectMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMode
        '
        Me.cboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMode.FormattingEnabled = True
        Me.cboMode.Location = New System.Drawing.Point(95, 57)
        Me.cboMode.Name = "cboMode"
        Me.cboMode.Size = New System.Drawing.Size(272, 21)
        Me.cboMode.TabIndex = 1
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(95, 111)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(102, 21)
        Me.cboPeriod.TabIndex = 3
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 114)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(81, 15)
        Me.lblPeriod.TabIndex = 8
        Me.lblPeriod.Text = "From Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(373, 30)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 5
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 230
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(95, 30)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(272, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 33)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(81, 15)
        Me.lblEmployee.TabIndex = 3
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCompanyAccountNo
        '
        Me.cboCompanyAccountNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompanyAccountNo.DropDownWidth = 230
        Me.cboCompanyAccountNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompanyAccountNo.FormattingEnabled = True
        Me.cboCompanyAccountNo.Location = New System.Drawing.Point(96, 219)
        Me.cboCompanyAccountNo.Name = "cboCompanyAccountNo"
        Me.cboCompanyAccountNo.Size = New System.Drawing.Size(271, 21)
        Me.cboCompanyAccountNo.TabIndex = 264
        '
        'lblCompanyAccountNo
        '
        Me.lblCompanyAccountNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyAccountNo.Location = New System.Drawing.Point(11, 223)
        Me.lblCompanyAccountNo.Name = "lblCompanyAccountNo"
        Me.lblCompanyAccountNo.Size = New System.Drawing.Size(79, 39)
        Me.lblCompanyAccountNo.TabIndex = 265
        Me.lblCompanyAccountNo.Text = "Company Account No."
        '
        'frmEDDetailBankAccountReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 608)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmEDDetailBankAccountReport"
        Me.Text = "frmEDDetailBankAccountReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchMembership As eZee.Common.eZeeGradientButton
    Friend WithEvents LblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents chkIncludeNegativeHeads As System.Windows.Forms.CheckBox
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents LblTo As System.Windows.Forms.Label
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblHeads As System.Windows.Forms.Label
    Friend WithEvents cboHeades As System.Windows.Forms.ComboBox
    Friend WithEvents lblSelectMode As System.Windows.Forms.Label
    Friend WithEvents cboMode As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblEmpBank As System.Windows.Forms.Label
    Friend WithEvents cboEmpBank As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpBranch As System.Windows.Forms.Label
    Friend WithEvents cboEmpBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lnkIncludeOtherColumns As System.Windows.Forms.LinkLabel
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents chkShowEmpInSingleColumn As System.Windows.Forms.CheckBox
    Friend WithEvents txtNameFormat As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnKeywords As eZee.Common.eZeeGradientButton
    Friend WithEvents lblNameFormat As System.Windows.Forms.Label
    Friend WithEvents lnkAdvanceEFTReport As System.Windows.Forms.LinkLabel
    Friend WithEvents SaveDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents cboCompanyAccountNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompanyAccountNo As System.Windows.Forms.Label
End Class
