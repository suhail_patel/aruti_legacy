'Class Name : clsMonthlyFinancePlanReport.vb
'Purpose    :
'Date       :10/11/2017
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsMonthlyFinancePlanReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsMonthlyFinancePlanReport"
    Private mstrReportId As String = enArutiReport.Monthly_Finance_Plan_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mstrBudgetCodesIDs As String = String.Empty
    Private mstrBudgetCodesPeriodIDs As String = String.Empty
    Private mstrBudgetCodesPeriodNames As String = String.Empty
    Private mstrBudgetIDs As String = String.Empty
    Private mstrBudgetNames As String = String.Empty
    Private mintBudgetViewByID As Integer = 0
    Private mintAllocationByID As Integer = 0
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date

    Private mintTranHeadId As Integer = -1
    Private mstrTranHeadName As String = String.Empty

    Private mdtTableExcel As DataTable
    Private mstrMessage As String = ""

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_TableName As String = ""
    Private mstrAnalysis_CodeField As String = ""
    Private m_intViewIndex As Integer = -1
    Private m_strViewByIds As String = ""
    Private m_strViewByName As String = ""
    Private m_strAnalysis_Fields As String = ""
    Private m_strAnalysis_Join As String = ""
    Private m_strAnalysis_OrderBy As String = ""
    Private m_strReport_GroupName As String = ""
#End Region

#Region " Properties "

    Public WriteOnly Property _BudgetCodesIDs() As String
        Set(ByVal value As String)
            mstrBudgetCodesIDs = value
        End Set
    End Property

    Public WriteOnly Property _BudgetCodesPeriodIDs() As String
        Set(ByVal value As String)
            mstrBudgetCodesPeriodIDs = value
        End Set
    End Property

    Public WriteOnly Property _BudgetCodesPeriodNames() As String
        Set(ByVal value As String)
            mstrBudgetCodesPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _BudgetIDs() As String
        Set(ByVal value As String)
            mstrBudgetIDs = value
        End Set
    End Property

    Public WriteOnly Property _BudgetNames() As String
        Set(ByVal value As String)
            mstrBudgetNames = value
        End Set
    End Property

    Public WriteOnly Property _BudgetViewByID() As Integer
        Set(ByVal value As Integer)
            mintBudgetViewByID = value
        End Set
    End Property

    Public WriteOnly Property _AllocationByID() As Integer
        Set(ByVal value As Integer)
            mintAllocationByID = value
        End Set
    End Property

    Public WriteOnly Property _Period_StartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStart = value
        End Set
    End Property

    Public WriteOnly Property _Period_EndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEnd = value
        End Set
    End Property

    Public WriteOnly Property _TranHeadId() As Integer
        Set(ByVal value As Integer)
            mintTranHeadId = value
        End Set
    End Property

    Public WriteOnly Property _TranHeadName() As String
        Set(ByVal value As String)
            mstrTranHeadName = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            m_intViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            m_strViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            m_strViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            m_strAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            m_strAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            m_strAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            m_strReport_GroupName = value
        End Set
    End Property

#End Region

#Region " Enum "
    Public Enum enReportType
        Monthly_Employee_Finance_Plan = 1
    End Enum
#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try

            mstrBudgetCodesIDs = ""
            mstrBudgetCodesPeriodIDs = ""
            mstrBudgetCodesPeriodNames = ""
            mstrBudgetIDs = ""
            mstrBudgetNames = ""
            mintBudgetViewByID = 0
            mintAllocationByID = 0
            mstrMessage = ""
            mintTranHeadId = -1
            mstrTranHeadName = ""
            'mstrAdvance_Filter = ""



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            Me._FilterTitle &= ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 7, "Employee Code")))
            'If mblnFirstNamethenSurname = True Then
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ')", Language.getMessage(mstrModuleName, 8, "Employee Name")))
            'Else
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '')+ ' ' + ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 9, "Employee Name")))
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Export_Report(ByVal strReportType As String _
                                  , ByVal xUserUnkid As Integer _
                                  , ByVal xCompanyUnkid As Integer _
                                  , ByVal xUserModeSetting As String _
                                  , ByVal xOnlyApproved As Boolean _
                                  , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                  , ByVal blnApplyUserAccessFilter As Boolean _
                                  , ByVal strfmtCurrency As String _
                                  , ByVal xExportReportPath As String _
                                  , ByVal xOpenAfterExport As Boolean _
                                  , ByVal xDatabaseName As String _
                                  , ByVal xYearUnkid As Integer _
                                  )

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As DataSet
        Dim objBudget As New clsBudget_MasterNew
        Dim objBudget_Tran As New clsBudget_TranNew
        Dim objBudgetCodes As New clsBudgetcodes_master
        Dim objBudgetCodes_Tran As New clsBudgetcodes_Tran
        Dim objFundProjectCode As New clsFundProjectCode
        Dim objActivity As New clsfundactivity_Tran
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objPeriod As New clscommom_period_Tran

        Dim mdicFund As New Dictionary(Of Integer, Decimal)
        Dim dsAllHeads As DataSet = Nothing
        Dim mintViewById As Integer
        Dim mdicHeadMapping As New Dictionary(Of Integer, Integer)
        Dim mstrAllocationTranUnkIDs As String = ""
        Dim mstrEmployeeIDs As String = ""
        Dim dtPeriodStart As Date
        Dim dtPeriodEnd As Date
        Dim objTnALeave As New clsTnALeaveTran

        Try

            dtPeriodStart = mdtPeriodStart
            dtPeriodEnd = mdtPeriodEnd
            mintViewById = mintBudgetViewByID
            mintViewIdx = mintAllocationByID

            If CInt(mintViewById) = enBudgetViewBy.Allocation Then
                mstrAllocationTranUnkIDs = objBudgetCodes_Tran.getEmployeeIds(mstrBudgetCodesIDs)
                frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, dtPeriodEnd, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                If mintViewIdx = 0 Then mintViewIdx = -1
            ElseIf CInt(mintViewById) = enBudgetViewBy.Employee Then
                mstrEmployeeIDs = objBudgetCodes_Tran.getEmployeeIds(mstrBudgetCodesIDs)
                frmEmpSelection.GetAnalysisByDetails("frmEmpSelection", mstrEmployeeIDs, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                mintViewIdx = 0
                mstrAllocationTranUnkIDs = mstrEmployeeIDs
            End If

            dsAllHeads = Nothing
            dsList = objFundProjectCode.GetList("FundProjectCode", , True)
            mdicFund = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Total = CDec(p.Item("currentceilingbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)

            Dim dicFundCode As Dictionary(Of Integer, String) = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Code = p.Item("fundprojectcode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)

            dsList = objActivity.GetComboList("Activity", True)
            Dim d_row() As DataRow = dsList.Tables(0).Select("fundactivityunkid = 0")
            If d_row.Length > 0 Then
                d_row(0).Item("activitycode") = ""
                d_row(0).Item("activityname") = ""
                dsList.Tables(0).AcceptChanges()
            End If
            Dim dicActivityCode As Dictionary(Of Integer, String) = (From p In dsList.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activitycode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)

            dsList = objBudgetCodes.GetBudgetAllocation(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrBudgetCodesIDs, mintBudgetViewByID, enBudgetPresentation.TransactionWise, 0, "Budget", True, "", dsAllHeads, True, mstrAnalysis_CodeField)

            Dim strExpression As String = ""
            For Each pair In mdicFund
                strExpression &= " + [|_" & pair.Key.ToString & "]"
                dsList.Tables(0).Columns.Add("A||_" & pair.Key.ToString, System.Type.GetType("System.String")).DefaultValue = ""
            Next
            If strExpression.Trim <> "" Then
                dsList.Tables(0).Columns.Add("colhTotal", System.Type.GetType("System.Decimal"), strExpression.Substring(2)).DefaultValue = 0
            End If
            'dsList.Tables(0).Columns.Add("colhDiff", System.Type.GetType("System.Decimal"), "budgetamount - colhTotal").DefaultValue = 0

            mdtTableExcel = dsList.Tables(0)

            Dim lst_Row As List(Of DataRow) = (From p In mdtTableExcel Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("colhTotal")) <> 100) Select (p)).ToList
            If lst_Row.Count > 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Total Percentage should be 100 for all transactions.")
                Return False
            End If

            'Dim dsBalance As DataSet = Nothing
            'If mintTranHeadId > 0 Then

            '    Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            '    xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            '    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , False, xDatabaseName)
            '    If blnApplyUserAccessFilter = True Then
            '        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            '    End If
            '    Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, xDatabaseName)

            '    StrQ = "SELECT  prtnaleave_tran.tnaleavetranunkid  " & _
            '              ", prtnaleave_tran.payperiodunkid " & _
            '              ", prtnaleave_tran.employeeunkid " & _
            '              ", prpayrollprocess_tran.payrollprocesstranunkid " & _
            '              ", prpayrollprocess_tran.tranheadunkid " & _
            '              ", ISNULL(prpayrollprocess_tran.amount, 0) AS total_amount " & _
            '              ", ADF.* " & _
            '        "FROM    prtnaleave_tran " & _
            '                "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

            '    If xDateJoinQry.Trim.Length > 0 Then
            '        StrQ &= xDateJoinQry
            '    End If

            '    If blnApplyUserAccessFilter = True Then
            '        If xUACQry.Trim.Length > 0 Then
            '            StrQ &= xUACQry
            '        End If
            '    End If

            '    If xAdvanceJoinQry.Trim.Length > 0 Then
            '        StrQ &= xAdvanceJoinQry
            '    End If

            '    StrQ &= "WHERE   prtnaleave_tran.isvoid = 0 " & _
            '                "AND prpayrollprocess_tran.isvoid = 0 " & _
            '                "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid " & _
            '                "AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") "

            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If

            '    objDataOperation = New clsDataOperation

            '    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadId)

            '    dsBalance = objDataOperation.ExecQuery(StrQ, "List")

            '    If objDataOperation.ErrorMessage <> "" Then
            '        Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            '    End If

            'Else
            '    Dim sf As New List(Of clsSQLFilterCollection)
            '    sf.Add(New clsSQLFilterCollection(" AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") ", "", SqlDbType.Int, eZeeDataType.INT_SIZE, 0))
            '    dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, 0, "", , , sf)
            'End If

            Dim dsEmp As DataSet = Nothing
            If m_intViewIndex > 0 Then
                Dim objEmp As New clsEmployee_Master
                dsEmp = objEmp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, "Emp", False, -1, False, "", False, True, True)
            End If
            mdtTableExcel.Columns.Add("G_Id", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTableExcel.Columns.Add("G_Name", System.Type.GetType("System.String")).DefaultValue = ""

            Dim dicBudgetPeriod As New Dictionary(Of Integer, String)
            For Each strID As String In mstrBudgetIDs.Split(",")
                If dicBudgetPeriod.ContainsKey(CInt(strID)) = False Then
                    dicBudgetPeriod.Add(CInt(strID), objBudgetPeriod.GetPeriodIDs(CInt(strID)))
                End If
            Next
            Dim mdtExcel As DataTable = mdtTableExcel.Clone
            Dim d_r As DataRow = Nothing
            Dim strPrevKey As String = ""
            Dim strCurrKey As String = ""
            Dim intPrevPeriodId As Integer = 0
            Dim intCurrPeriodId As Integer = 0

            Dim decTotal As Decimal = 0
            For Each dsRow As DataRow In mdtTableExcel.Rows

                dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")), strfmtCurrency)
                For Each pair In dicFundCode
                    decTotal = 0

                    'If CDec(dsRow.Item("|_" & pair.Key.ToString)) > 0 Then
                    '    Dim intAllocUnkId As Integer = CInt(dsRow.Item("allocationtranunkid"))
                    '    Dim intPeriodID As Integer = CInt(dsRow.Item("periodunkid"))
                    '    Dim decNetPay As Decimal = (From p In dsBalance.Tables(0) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso CInt(p.Item("payperiodunkid")) = intPeriodID AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                    '    decTotal += decNetPay * CDec(dsRow.Item("|_" & pair.Key.ToString)) / 100
                    'End If
                    decTotal += CDec(dsRow.Item("budgetamount")) * CDec(dsRow.Item("|_" & pair.Key.ToString)) / 100

                    dsRow.Item("|_" & pair.Key.ToString) = Format(decTotal, strfmtCurrency)

                    If dicActivityCode.ContainsKey(CInt(dsRow.Item("||_" & pair.Key.ToString))) = True Then
                        dsRow.Item("A||_" & pair.Key.ToString) = dicActivityCode.Item(CInt(dsRow.Item("||_" & pair.Key.ToString)))
                    Else
                        dsRow.Item("A||_" & pair.Key.ToString) = ""
                    End If
                Next

                If dicBudgetPeriod.ContainsKey(CInt(dsRow.Item("budgetunkid"))) = True Then
                    dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")) / dicBudgetPeriod.Item(CInt(dsRow.Item("budgetunkid"))).Split(",").ToArray.Count, strfmtCurrency)
                Else
                    dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")), strfmtCurrency)
                End If

                If m_intViewIndex > 0 Then
                    Dim dr() As DataRow = dsEmp.Tables(0).Select("employeeunkid = " & CInt(dsRow.Item("allocationtranunkid")) & " ")
                    dsRow.Item("G_Id") = 0
                    dsRow.Item("G_Name") = ""
                    If dr.Length > 0 Then
                        dsRow.Item("G_Id") = CInt(dr(0)(m_strAnalysis_OrderBy.Split(CChar("."))(1).ToString().Trim()))

                        Select Case m_intViewIndex
                            Case enAnalysisReport.Branch
                                dsRow.Item("G_Name") = dr(0)("station").ToString

                            Case enAnalysisReport.Department
                                dsRow.Item("G_Name") = dr(0)("DeptName").ToString

                            Case enAnalysisReport.Section
                                dsRow.Item("G_Name") = dr(0)("Section").ToString

                            Case enAnalysisReport.Unit
                                dsRow.Item("G_Name") = dr(0)("Unit").ToString

                            Case enAnalysisReport.Job
                                dsRow.Item("G_Name") = dr(0)("Job_Name").ToString

                            Case enAnalysisReport.CostCenter
                                dsRow.Item("G_Name") = dr(0)("CostCenter").ToString

                            Case enAnalysisReport.SectionGroup
                                dsRow.Item("G_Name") = dr(0)("SectionGroup").ToString

                            Case enAnalysisReport.UnitGroup
                                dsRow.Item("G_Name") = dr(0)("UnitGroup").ToString

                            Case enAnalysisReport.Team
                                dsRow.Item("G_Name") = dr(0)("Team").ToString

                            Case enAnalysisReport.JobGroup
                                dsRow.Item("G_Name") = dr(0)("JobGroup").ToString

                            Case enAnalysisReport.ClassGroup
                                dsRow.Item("G_Name") = dr(0)("ClassGroup").ToString

                            Case enAnalysisReport.Classs
                                dsRow.Item("G_Name") = dr(0)("Class").ToString

                            Case enAnalysisReport.DepartmentGroup
                                dsRow.Item("G_Name") = dr(0)("DeptGroup").ToString

                            Case enAnalysisReport.GradeGroup
                                dsRow.Item("G_Name") = dr(0)("GradeGroup").ToString

                            Case enAnalysisReport.Grade
                                dsRow.Item("G_Name") = dr(0)("Grade").ToString

                            Case enAnalysisReport.GradeLevel
                                dsRow.Item("G_Name") = dr(0)("GradeLevel").ToString

                        End Select

                    End If
                End If

                strCurrKey = dsRow.Item("allocationtranunkid").ToString() & "_" & dsRow.Item("jobunkid").ToString()
                intCurrPeriodId = CInt(dsRow.Item("periodunkid"))

                If strPrevKey <> strCurrKey Then
                    d_r = mdtExcel.NewRow
                    d_r.ItemArray = dsRow.ItemArray

                    mdtExcel.Rows.Add(d_r)
                Else
                    d_r = mdtExcel.Rows(mdtExcel.Rows.Count - 1)

                    For Each pair In dicFundCode
                        d_r.Item("|_" & pair.Key.ToString) += CDec(dsRow.Item("|_" & pair.Key.ToString))
                        If dsRow.Item("A||_" & pair.Key.ToString).ToString.Trim.Length > 0 Then
                            d_r.Item("A||_" & pair.Key.ToString) += ", " & dsRow.Item("A||_" & pair.Key.ToString)
                        End If
                    Next

                    If intPrevPeriodId <> intCurrPeriodId Then
                        d_r.Item("budgetamount") += CDec(dsRow.Item("budgetamount"))
                    End If

                    mdtExcel.AcceptChanges()
                End If
                strPrevKey = strCurrKey
                intPrevPeriodId = intCurrPeriodId

            Next
            mdtTableExcel.AcceptChanges()

            If m_intViewIndex > 0 Then
                mdtTableExcel = New DataView(mdtExcel, "allocationtranunkid > 0 AND G_Id IN (" & m_strViewByIds & ") ", "G_Name, G_Id", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTableExcel = New DataView(mdtExcel, "allocationtranunkid > 0 ", "G_Name, G_Id", DataViewRowState.CurrentRows).ToTable
            End If

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell


            Dim mintColumn As Integer = 0
            mdtTableExcel.Columns("GCode").Caption = Language.getMessage(mstrModuleName, 2, "Code") 'mstrReport_GroupName.Replace(" :", "")
            mdtTableExcel.Columns("GCode").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName.Replace(" :", "")
            mdtTableExcel.Columns("GName").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("budgetamount").Caption = Language.getMessage(mstrModuleName, 3, "Budget Amount")
            mdtTableExcel.Columns("budgetamount").SetOrdinal(mintColumn)
            mintColumn += 1

            'If mdtTableExcel.Columns.Contains("colhTotal") = True Then
            '    mdtTableExcel.Columns("colhTotal").Caption = Language.getMessage(mstrModuleName, 4, "Actual Amount")
            '    mdtTableExcel.Columns("colhTotal").SetOrdinal(mintColumn)
            '    mintColumn += 1
            'End If

            'If mdtTableExcel.Columns.Contains("colhDiff") = True Then
            '    mdtTableExcel.Columns("colhDiff").Caption = Language.getMessage(mstrModuleName, 5, "Difference")
            '    mdtTableExcel.Columns("colhDiff").SetOrdinal(mintColumn)
            '    mintColumn += 1
            'End If

            For Each pair In dicFundCode
                mdtTableExcel.Columns("|_" & pair.Key.ToString).Caption = pair.Value.ToString
                mdtTableExcel.Columns("|_" & pair.Key.ToString).SetOrdinal(mintColumn)
                mintColumn += 1

                'mdtTableExcel.Columns("A||_" & pair.Key.ToString).Caption = Language.getMessage(mstrModuleName, 5, "Activity Code")
                'mdtTableExcel.Columns("A||_" & pair.Key.ToString).SetOrdinal(mintColumn)
                'mintColumn += 1
            Next

            If m_intViewIndex > 0 Then
                mdtTableExcel.Columns("G_Name").Caption = m_strReport_GroupName.Replace(":", "")
                mdtTableExcel.Columns("G_Name").SetOrdinal(mintColumn)
                mintColumn += 1

                Dim arrGrp() As String = {"G_Name"}
                strarrGroupColumns = arrGrp
            End If


            For i = mintColumn To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(mintColumn)
            Next


            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 4, "Budget Details") & " : " & mstrBudgetNames & " ", "s9w")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "PD Details") & " : " & mstrBudgetCodesPeriodNames, "s9w")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "HeaderStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell("", "HeaderStyle")
            row.Cells.Add(wcell)

            'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Actual Vs Budget"), "HeaderStyle")
            wcell = New WorksheetCell("", "HeaderStyle")
            'wcell.MergeAcross = 2
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Donor Fund Allocation"), "HeaderStyle")
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1 - 3
            row.Cells.Add(wcell)

            rowsArrayHeader.Add(row)

            'SET EXCEL CELL WIDTH
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, strReportType, "", " ", Nothing, "", True, rowsArrayHeader)

            Return True

        Catch ex As Exception
            exForce = New Exception(ex.Message & "; Procedure Name: Export_Report; Module Name: " & mstrModuleName)
            Throw exForce
            Return False
        End Try
    End Function


#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Total Percentage should be 100 for all transactions.")
            Language.setMessage(mstrModuleName, 2, "Code")
            Language.setMessage(mstrModuleName, 3, "Budget Amount")
            Language.setMessage(mstrModuleName, 4, "Budget Details")
            Language.setMessage(mstrModuleName, 5, "PD Details")
            Language.setMessage(mstrModuleName, 6, "Donor Fund Allocation")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
