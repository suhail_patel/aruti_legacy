'************************************************************************************************************************************
'Class Name : clsTerminationPackageReport.vb
'Purpose    :
'Date       :18 Oct 2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsTerminationPackageReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTerminationPackageReport"
    Private mstrReportId As String = enArutiReport.Termination_Package_Report
    Dim objDataOperation As clsDataOperation

#Region "Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_DetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mstrEmpName As String = String.Empty
    Private mintEmployeeunkid As Integer = -1
    Private mblnIncludeInactiveEmp As Boolean = True
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mdtIncrementDateFrom As Date = Nothing
    Private mdtIncrementDateTo As Date = Nothing



    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mstrAdvance_Filter As String = String.Empty


    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

    Private mintProcessPeriodUnkId As Integer = 0
    Private mstrProcessPeriodName As String = ""
    Private mintPeriodUnkId As Integer = 0
    Private mstrPeriodName As String = ""
    Private mdtProcessPeriodStartDate As Date
    Private mdtProcessPeriodEndDate As Date
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    Private mstrProcessDatabaseName As String = FinancialYear._Object._DatabaseName

    Private mintLeaveTypeId As Integer = 0
    Private mstrLeaveTypeName As String = ""

    Private mintCurrencyCountryId As Integer = 0
    Private mstrCurrencyName As String = ""
    Private mstrCurrency_Sign As String = String.Empty
    Private mdecEx_Rate As Decimal = 0
    Private mstrCurrency_Rate As String = String.Empty
    'Sohail (08 Jul 2016) -- Start
    'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
    Private mstrBaseCurrency_Sign As String = String.Empty
    'Sohail (08 Jul 2016) -- End

    Private mdtDatabaseStartDate As DateTime = Nothing
    Private mdtDatabaseEndDate As DateTime = Nothing
    Private mintLeaveBalanceSetting As Integer = 0
    Private mintIsFin_Close As Integer = -1
    Private mintYearUnkId As Integer = 0


    'Shani [ 24 NOV 2014 ] -- START
    Private mintSalaryHeadID As Integer = 0
    Private mintCongePreavisHeadID As Integer = 0
    Private mintDaysInPeriodHeadID As Integer = 0
    Private mintNotificationAllowanceHeadID As Integer = 0
    Private mint13thCheckHeadID As Integer = 0
    Private mintBaseTaxableHeadID As Integer = 0   'TODO :5: Base Taxable head
    Private mintBaseImposableHeadId As Integer = 0 'TODO :20: Base Imposable head
    Private mintNetPayHeadID As Integer = 0 'TODO :1: Net Pay head
    Private mstrNewHrsAmtMapping As String = "" ' Dim s As String = "13,32|53,32|17,32|18,32" 'TODO
    Private mstrContributionIDs As String = "" 'Dim strContributionIDs As String = "19, 21, 22, 26, 69, 30" 'TODO
    Private mstrDeductionIDs As String = "" ' Dim strDeductionIDs As String = "19, 21, 55" 'TODO
    Private mdtNoticeDate As Date = Nothing
    Private mintTerminationFactorID As Integer = 0
    Private mstrTerminationFactorName As String = String.Empty
    Private mintChildHeadID As Integer = 0
    Private mintDependentHeadID As Integer = 0
    'Shani [ 24 NOV 2014 ] -- END
    'Sohail (08 Jul 2016) -- Start
    'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
    Private mintFamilyAllowanceHeadID As Integer = 0
    Private mintLogementHeadID As Integer = 0
    Private mintLoanRepaymentHeadID As Integer = 0
    Private mstrLoanRepaymentHeadName As String = String.Empty
    Private mintAdvanceRepaymentHeadID As Integer = 0
    Private mstrAdvanceRepaymentHeadName As String = String.Empty
    Private mintOtherDeductionsHeadID As Integer = 0
    Private mstrOtherDeductionsHeadName As String = String.Empty
    Private mintIPRNetHeadID As Integer = 0
    Private mstrIPRNetHeadName As String = String.Empty
    Private mintINSS35HeadID As Integer = 0
    Private mstrINSS35HeadName As String = String.Empty
    Private mintRateOfNoticeHeadID As Integer = 0
    Private marrBonusHead() As String = {}
    'Sohail (08 Jul 2016) -- End

    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
    Private mintLeaveAccrueTenureSetting As Integer = enLeaveAccrueTenureSetting.Yearly
    Private mintLeaveAccrueDaysAfterEachMonth As Integer = 0
    'Pinkal (18-Nov-2016) -- End



#End Region

#Region " Properties "

    Public WriteOnly Property _Emp_Name() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _Employeeunkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _ProcessPeriodUnkID() As Integer
        Set(ByVal value As Integer)
            mintProcessPeriodUnkId = value
        End Set
    End Property

    Public WriteOnly Property _ProcessPeriodName() As String
        Set(ByVal value As String)
            mstrProcessPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodUnkID() As Integer
        Set(ByVal value As Integer)
            mintPeriodUnkId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ProcessPeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtProcessPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _ProcessPeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtProcessPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _IncrementDateFrom() As Date
        Set(ByVal value As Date)
            mdtIncrementDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _IncrementDateTo() As Date
        Set(ByVal value As Date)
            mdtIncrementDateTo = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _ProcessDatabaseName() As String
        Set(ByVal value As String)
            mstrProcessDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeId() As Integer
        Set(ByVal value As Integer)
            mintLeaveTypeId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeName() As String
        Set(ByVal value As String)
            mstrLeaveTypeName = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyCountryId() As Integer
        Set(ByVal value As Integer)
            mintCurrencyCountryId = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyName() As String
        Set(ByVal value As String)
            mstrCurrencyName = value
        End Set
    End Property

    Public WriteOnly Property _Currency_Sign() As String
        Set(ByVal value As String)
            mstrCurrency_Sign = value
        End Set
    End Property

    Public WriteOnly Property _Ex_Rate() As Decimal
        Set(ByVal value As Decimal)
            mdecEx_Rate = value
        End Set
    End Property

    Public WriteOnly Property _Currency_Rate() As String
        Set(ByVal value As String)
            mstrCurrency_Rate = value
        End Set
    End Property

    'Sohail (08 Jul 2016) -- Start
    'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
    Public WriteOnly Property _BaseCurrency_Sign() As String
        Set(ByVal value As String)
            mstrBaseCurrency_Sign = value
        End Set
    End Property
    'Sohail (08 Jul 2016) -- End

    Public WriteOnly Property _DatabaseStartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtDatabaseStartDate = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtDatabaseEndDate = value
        End Set
    End Property

    Public WriteOnly Property _YearUnkId() As Integer
        Set(ByVal value As Integer)
            mintYearUnkId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    Public WriteOnly Property _IsFin_Close() As Integer
        Set(ByVal value As Integer)
            mintIsFin_Close = value
        End Set
    End Property


    'Shani [ 24 NOV 2014 ] -- START
    Public WriteOnly Property _CongePreavisHeadID() As Integer
        Set(ByVal value As Integer)
            mintCongePreavisHeadID = value
        End Set
    End Property

    Public WriteOnly Property _SalaryHeadID() As Integer
        Set(ByVal value As Integer)
            mintSalaryHeadID = value
        End Set
    End Property

    Public WriteOnly Property _DaysInPeriodHeadID() As Integer
        Set(ByVal value As Integer)
            mintDaysInPeriodHeadID = value
        End Set
    End Property

    Public WriteOnly Property _13thCheckHeadID() As Integer
        Set(ByVal value As Integer)
            mint13thCheckHeadID = value
        End Set
    End Property

    Public WriteOnly Property _NotificationAllowanceHeadID() As Integer
        Set(ByVal value As Integer)
            mintNotificationAllowanceHeadID = value
        End Set
    End Property

    Public WriteOnly Property _BaseTaxableHeadID() As Integer
        Set(ByVal value As Integer)
            mintBaseTaxableHeadID = value
        End Set
    End Property

    Public WriteOnly Property _BaseImposableHeadId() As Integer
        Set(ByVal value As Integer)
            mintBaseImposableHeadId = value
        End Set
    End Property

    Public WriteOnly Property _NetPayHeadID() As Integer
        Set(ByVal value As Integer)
            mintNetPayHeadID = value
        End Set
    End Property

    Public WriteOnly Property _NewHrsAmtMapping() As String
        Set(ByVal value As String)
            mstrNewHrsAmtMapping = value
        End Set
    End Property

    Public WriteOnly Property _ContributionIDs() As String
        Set(ByVal value As String)
            mstrContributionIDs = value
        End Set
    End Property

    Public WriteOnly Property _DeductionIDs() As String
        Set(ByVal value As String)
            mstrDeductionIDs = value
        End Set
    End Property

    Public WriteOnly Property _NoticeDate() As Date
        Set(ByVal value As Date)
            mdtNoticeDate = value
        End Set
    End Property

    Public WriteOnly Property _TerminationFactorID() As Integer
        Set(ByVal value As Integer)
            mintTerminationFactorID = value
        End Set
    End Property

    Public WriteOnly Property _TerminationFactorName() As String
        Set(ByVal value As String)
            mstrTerminationFactorName = value
        End Set
    End Property

    Public WriteOnly Property _ChildHead_ID() As Integer
        Set(ByVal value As Integer)
            mintChildHeadID = value
        End Set
    End Property

    Public WriteOnly Property _DependentHead_ID() As Integer
        Set(ByVal value As Integer)
            mintDependentHeadID = value
        End Set
    End Property
    'Shani [ 24 NOV 2014 ] -- END

    'Sohail (08 Jul 2016) -- Start
    'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
    Public WriteOnly Property _FamilyAllowanceHead_ID() As Integer
        Set(ByVal value As Integer)
            mintFamilyAllowanceHeadID = value
        End Set
    End Property

    Public WriteOnly Property _LogementHead_ID() As Integer
        Set(ByVal value As Integer)
            mintLogementHeadID = value
        End Set
    End Property

    Public WriteOnly Property _LoanRepaymentHead_ID() As Integer
        Set(ByVal value As Integer)
            mintLoanRepaymentHeadID = value
        End Set
    End Property

    Public WriteOnly Property _LoanRepaymentHeadName() As String
        Set(ByVal value As String)
            mstrLoanRepaymentHeadName = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceRepaymentHead_ID() As Integer
        Set(ByVal value As Integer)
            mintAdvanceRepaymentHeadID = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceRepaymentHeadName() As String
        Set(ByVal value As String)
            mstrAdvanceRepaymentHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OtherDeductionsHead_ID() As Integer
        Set(ByVal value As Integer)
            mintOtherDeductionsHeadID = value
        End Set
    End Property

    Public WriteOnly Property _OtherDeductionsHeadName() As String
        Set(ByVal value As String)
            mstrOtherDeductionsHeadName = value
        End Set
    End Property

    Public WriteOnly Property _RateOfNoticeHead_ID() As Integer
        Set(ByVal value As Integer)
            mintRateOfNoticeHeadID = value
        End Set
    End Property

    Public WriteOnly Property _BonusHead_IDs() As String()
        Set(ByVal value As String())
            marrBonusHead = value
        End Set
    End Property

    Public WriteOnly Property _IPRNetHead_ID() As Integer
        Set(ByVal value As Integer)
            mintIPRNetHeadID = value
        End Set
    End Property

    Public WriteOnly Property _IPRNetHeadName() As String
        Set(ByVal value As String)
            mstrIPRNetHeadName = value
        End Set
    End Property

    Public WriteOnly Property _INSS35Head_ID() As Integer
        Set(ByVal value As Integer)
            mintINSS35HeadID = value
        End Set
    End Property

    Public WriteOnly Property _INSS35HeadName() As String
        Set(ByVal value As String)
            mstrINSS35HeadName = value
        End Set
    End Property
    'Sohail (08 Jul 2016) -- End

    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

    Public WriteOnly Property _LeaveAccrueTenureSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueTenureSetting = value
        End Set
    End Property

    Public WriteOnly Property _LeaveAccrueDaysAfterEachMonth() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueDaysAfterEachMonth = value
        End Set
    End Property

    'Pinkal (18-Nov-2016) -- End

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrEmpName = String.Empty
            mintEmployeeunkid = -1
            mblnIncludeInactiveEmp = True
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mdtIncrementDateFrom = Nothing
            mdtIncrementDateTo = Nothing

            mintCurrencyCountryId = 0
            mstrCurrencyName = ""

            mintLeaveTypeId = 0
            mstrLeaveTypeName = ""

            mstrAdvance_Filter = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, enModuleReference.Payroll)

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPeriodUnkId)

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Period :") & " " & mstrPeriodName & " "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Empoyee :") & " " & mstrEmpName & " "

            'Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "To :") & " " & mstrToPeriodName & " "


            objDataOperation.AddParameter("@Appointed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Appointed"))
            objDataOperation.AddParameter("@TransferIn", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Transfer In"))
            objDataOperation.AddParameter("@TransferOut", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Transfer Out"))
            objDataOperation.AddParameter("@Reinstated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Reinstated"))
            objDataOperation.AddParameter("@Exit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Exit"))


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Order By :") & " " & Me.OrderByDisplay
                Me._FilterQuery &= " ORDER BY GName, " & Me.OrderByQuery
            Else
                Me._FilterQuery &= " ORDER BY GName "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    'Shani(24-Aug-2015) -- Start
        '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        '    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
        '    'objRpt = Generate_DetailReport()
        '    objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, xIncludeIn_ActiveEmployee)
        '    'Shani(24-Aug-2015) -- End
        '    Rpt = objRpt


        '    If Not IsNothing(objRpt) Then


        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        'Dim blnShowGrandTotal As Boolean = True
        '        Dim objDic As New Dictionary(Of Integer, Object)
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim strGTotal As String = Language.getMessage(mstrModuleName, 66, "Grand Total :")
        '        'Dim strSubTotal As String = Language.getMessage(mstrModuleName, 11, "Sub Total :")
        '        'Dim row As WorksheetRow
        '        'Dim wcell As WorksheetCell

        '        'If mdtTableExcel IsNot Nothing Then


        '        '    If mintViewIndex > 0 Then
        '        '        Dim strGrpCols As String() = {"column10"}
        '        '        strarrGroupColumns = strGrpCols
        '        '    End If


        '        '    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '        '    For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '        '        intArrayColumnWidth(i) = 125
        '        '    Next


        '        '    '*******   REPORT FOOTER   ******
        '        '    row = New WorksheetRow()
        '        '    wcell = New WorksheetCell("", "s8w")
        '        '    row.Cells.Add(wcell)
        '        '    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '        '    rowsArrayFooter.Add(row)
        '        '    '----------------------


        '        '    If ConfigParameter._Object._IsShowCheckedBy = True Then
        '        '        row = New WorksheetRow()
        '        '        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
        '        '        row.Cells.Add(wcell)

        '        '        wcell = New WorksheetCell("", "s8w")
        '        '        row.Cells.Add(wcell)
        '        '        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '        '        rowsArrayFooter.Add(row)

        '        '        row = New WorksheetRow()
        '        '        wcell = New WorksheetCell("", "s8w")
        '        '        row.Cells.Add(wcell)
        '        '        rowsArrayFooter.Add(row)

        '        '    End If

        '        '    If ConfigParameter._Object._IsShowApprovedBy = True Then

        '        '        row = New WorksheetRow()
        '        '        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By :"), "s8bw")
        '        '        row.Cells.Add(wcell)

        '        '        wcell = New WorksheetCell("", "s8w")
        '        '        row.Cells.Add(wcell)
        '        '        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '        '        rowsArrayFooter.Add(row)

        '        '        row = New WorksheetRow()
        '        '        wcell = New WorksheetCell("", "s8w")
        '        '        row.Cells.Add(wcell)
        '        '        rowsArrayFooter.Add(row)

        '        '    End If

        '        '    If ConfigParameter._Object._IsShowReceivedBy = True Then
        '        '        row = New WorksheetRow()
        '        '        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
        '        '        row.Cells.Add(wcell)

        '        '        wcell = New WorksheetCell("", "s8w")
        '        '        row.Cells.Add(wcell)
        '        '        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '        '        rowsArrayFooter.Add(row)

        '        '        row = New WorksheetRow()
        '        '        wcell = New WorksheetCell("", "s8w")
        '        '        row.Cells.Add(wcell)
        '        '        rowsArrayFooter.Add(row)
        '        '    End If

        '        'End If

        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic)

        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions

        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objConfig._Companyunkid = xCompanyUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, xPeriodStart, xPeriodEnd, xOnlyApproved, mblnIncludeInactiveEmp, True, objConfig._CurrencyFormat)

            Rpt = objRpt


            If Not IsNothing(objRpt) Then


                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim objDic As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim strGTotal As String = Language.getMessage(mstrModuleName, 66, "Grand Total :")

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic)

            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End



    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""

        Try
            OrderByDisplay = iColumn_Detail.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_Detail.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

        Try
            Call OrderByExecute(iColumn_Detail)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_Detail As New IColumnCollection
    Public Property Field_DetailReport() As IColumnCollection
        Get
            Return iColumn_Detail
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_Detail = value
        End Set
    End Property

    Private Sub Create_DetailReport()
        Try
            iColumn_Detail.Clear()
            iColumn_Detail.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 10, "Employee Code")))
            iColumn_Detail.Add(New IColumn("employeename", Language.getMessage(mstrModuleName, 11, "Employee Name")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal xDatabaseName As String _
                                           , ByVal xUserUnkid As Integer _
                                           , ByVal xYearUnkid As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xPeriodStart As Date _
                                           , ByVal xPeriodEnd As Date _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                           , ByVal blnApplyUserAccessFilter As Boolean _
                                           , ByVal strfmtCurrency As String _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strfmtCurrency]
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim strQ As String = ""
        Dim dsList As DataSet
        Dim exForce As Exception
        Dim rpt_Data As New ArutiReport.Designer.dsArutiReport
        Dim strAllocationJoin As String = ""
        Dim mintFirstPeriodHeadCount As Integer = 0

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        'mstrProcessDatabaseName = xDatabaseName 'Sohail (08 Jul 2016
        'Sohail (21 Aug 2015) -- End

        objDataOperation = New clsDataOperation
        Try


            strQ = "SELECT  prpayrollprocess_tran.employeeunkid  " & _
                          ", employeecode " & _
                          ", REPLACE(ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, ''), '  ', ' ') AS employeename  " & _
                          ", T.departmentunkid " & _
                          ", ISNULL(hrdepartment_master.name, '') AS departmentname " & _
                          ", J.jobunkid " & _
                          ", ISNULL(job_name, '') AS job_name " & _
                          ", ISNULL(toc.name, '') AS employmenttype " & _
                          ", CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
                          ", CONVERT(CHAR(8), ETERM.termination_from_date, 112) AS termination_from_date " & _
                          ", CONVERT(CHAR(8), ETERM.empl_enddate, 112) AS empl_enddate " & _
                          ", CONVERT(CHAR(8), ERET.termination_to_date, 112) AS termination_to_date " & _
                          ", payperiodunkid " & _
                          ", period_name " & _
                          ", prpayrollprocess_tran.employeeunkid " & _
                          ", prpayrollprocess_tran.tranheadunkid " & _
                          ", trnheadcode " & _
                          ", trnheadname " & _
                          ", amount " & _
                          ", CONVERT(CHAR(8), start_date, 112) AS start_date " & _
                          ", CONVERT(CHAR(8), end_date, 112) AS end_date " & _
                    "FROM    " & mstrProcessDatabaseName & "..prpayrollprocess_tran " & _
                            "LEFT JOIN " & mstrProcessDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "LEFT JOIN " & mstrProcessDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                           "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                            "LEFT JOIN " & mstrProcessDatabaseName & "..cfcommon_period_tran ON periodunkid = payperiodunkid " & _
                            "LEFT JOIN " & mstrProcessDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            strQ &= "LEFT JOIN " & _
                      "( " & _
                      "    SELECT " & _
                      "         stationunkid " & _
                      "        ,deptgroupunkid " & _
                      "        ,departmentunkid " & _
                      "        ,sectiongroupunkid " & _
                      "        ,sectionunkid " & _
                      "        ,unitgroupunkid " & _
                      "        ,unitunkid " & _
                      "        ,teamunkid " & _
                      "        ,classgroupunkid " & _
                      "        ,classunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                      "    FROM hremployee_transfer_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                      "LEFT JOIN " & _
                      "( " & _
                      "    SELECT " & _
                      "         jobgroupunkid " & _
                      "        ,jobunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                      "    FROM hremployee_categorization_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 "

            'Sohail (17 May 2015) -- Start
            'Enhancement - 60.1 - Changes in Termination Package Report as per 58.1 Allocation table changes and channging all drop down list to searchable drop down list for FDRC.
            strQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 TERM.TEEmpId " & _
                    "		,TERM.empl_enddate " & _
                    "		,TERM.termination_from_date " & _
                    "		,TERM.TEfDt " & _
                    "		,TERM.isexclude_payroll " & _
                    "		,TERM.TR_REASON " & _
                    "		,TERM.changereasonunkid " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            TRM.employeeunkid AS TEEmpId " & _
                    "           ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                    "           ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                    "           ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                    "           ,TRM.isexclude_payroll " & _
                    "			,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                    "           ,TRM.changereasonunkid " & _
                    "       FROM hremployee_dates_tran AS TRM " & _
                    "			LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                    "		WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS TERM WHERE TERM.Rno = 1 " & _
                    ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 RET.REmpId " & _
                    "		,RET.termination_to_date " & _
                    "		,RET.REfDt " & _
                    "		,RET.RET_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            RTD.employeeunkid AS REmpId " & _
                    "           ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                    "           ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                    "       FROM hremployee_dates_tran AS RTD " & _
                    "			LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                    "		WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS RET WHERE RET.Rno = 1 " & _
                    ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) "
            'Sohail (17 May 2015) -- End

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "LEFT JOIN " & mstrProcessDatabaseName & "..hrdepartment_master ON hrdepartment_master.departmentunkid = T.departmentunkid " & _
                            "LEFT JOIN " & mstrProcessDatabaseName & "..hrjob_master ON J.jobunkid = hrjob_master.jobunkid " & _
                            "LEFT JOIN " & mstrProcessDatabaseName & "..cfcommon_master AS toc ON toc.masterunkid = employmenttypeunkid " & _
                                                                "AND toc.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " " & _
                    "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                            "AND cfcommon_period_tran.isactive = 1 " & _
                            "AND modulerefid = @modulerefid " & _
                            "AND payperiodunkid = @payperiodunkid " & _
                            "AND prpayrollprocess_tran.employeeunkid = @employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End


            Call FilterTitleAndFilterQuery()

            'strQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Jul 2016) -- Start
            'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
            Dim dsCurrPeriod As DataSet = Nothing

            If mintPeriodUnkId <> mintProcessPeriodUnkId Then
                dsCurrPeriod = objDataOperation.ExecQuery(strQ.Replace(mstrProcessDatabaseName, xDatabaseName).Replace("= @payperiodunkid", "= " & mintPeriodUnkId), "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Sohail (08 Jul 2016) -- End


            Dim dsNotice As New ArutiReport.Designer.dsArutiReport
            Dim dsBonus As New ArutiReport.Designer.dsArutiReport
            Dim dsLeave As New ArutiReport.Designer.dsArutiReport
            Dim dsCongePreavis As New ArutiReport.Designer.dsArutiReport
            Dim ds13thCheck As New ArutiReport.Designer.dsArutiReport
            Dim dsBrut As New ArutiReport.Designer.dsArutiReport
            Dim dsDeduction As New ArutiReport.Designer.dsArutiReport
            Dim dsContribution As New ArutiReport.Designer.dsArutiReport
            Dim dsLvBalance As New DataSet
            Dim dsEmpMembership As DataSet

            strQ = "SELECT  employeeunkid AS MEmpId  " & _
              ", Membrship = ISNULL(STUFF((SELECT    '; ' " & _
                                                    "+ ISNULL(hrmembership_master.membershipname, '') + ' -> ' " & _
                                                    "+ ISNULL(hremployee_meminfo_tran.membershipno, '') " & _
                                          "FROM      hremployee_master AS b " & _
                                                    "LEFT JOIN hremployee_meminfo_tran ON b.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                                                    "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                          "WHERE     membershipname IS NOT NULL " & _
                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                                    "AND b.employeeunkid = a.employeeunkid " & _
                                                    "AND b.employeeunkid IN (" & mintEmployeeunkid.ToString & ") " & _
                                                    "AND isappearonpayslip = 1 "


            strQ &= "                           FOR   XML PATH('')  " & _
                                                       ", ROOT('MyString') " & _
                                                       ", TYPE) " & _
                      ".value('/MyString[1]', 'varchar(max)'), 1, 2, ''), '') " & _
                    "FROM    hremployee_meminfo_tran AS a " & _
                    "LEFT JOIN hrmembership_master ON a.membershipunkid = hrmembership_master.membershipunkid " & _
                    "WHERE   a.isactive = 1 " & _
                            "AND ISNULL(a.isdeleted, 0) = 0 " & _
                            "AND a.employeeunkid IN (" & mintEmployeeunkid.ToString & ") " & _
                            "AND isappearonpayslip = 1 "

            strQ &= "GROUP BY employeeunkid "

            dsEmpMembership = objDataOperation.ExecQuery(strQ, "MemInfo")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdicMembrshipData As New Dictionary(Of Integer, String)

            mdicMembrshipData = (From p In dsEmpMembership.Tables(0).AsEnumerable() Select New With {Key .ID = CInt(p.Item("MEmpId").ToString), Key .NAME = p.Item("Membrship").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)


            Dim StrGrpName As String = ""
            Dim mdecBaseTaxable As Decimal = 0
            Dim mdecBaseImposable As Decimal = 0
            Dim mdecNetPay As Decimal = 0
            Dim mdecNetPayUSD As Decimal = 0
            Dim rpt_Row As DataRow
            Dim dr_Row As DataRow


            Dim dicHourMapping As New Dictionary(Of Integer, Integer)
            If mstrNewHrsAmtMapping.Trim <> "" Then
                Dim arrHourMapping() As String = mstrNewHrsAmtMapping.Split("|")

                For Each strMapping In arrHourMapping
                    If dicHourMapping.ContainsKey(CInt(strMapping.Split(",")(0))) = False Then
                        dicHourMapping.Add(CInt(strMapping.Split(",")(1)), CInt(strMapping.Split(",")(0)))
                    End If
                Next
            End If

            Dim decPrestationTotal As Decimal = 0
            Dim decPrestationCDFTotal As Decimal = 0
            Dim decPrestationUSDTotal As Decimal = 0
            Dim decNoticeCDFTotal As Decimal = 0
            Dim decNoticeUSDTotal As Decimal = 0
            Dim decBonusCDFTotal As Decimal = 0
            Dim decBonusUSDTotal As Decimal = 0
            Dim decLeaveCDFTotal As Decimal = 0
            Dim decLeaveUSDTotal As Decimal = 0
            Dim decCongeCDFTotal As Decimal = 0
            Dim decCongeUSDTotal As Decimal = 0
            Dim dec13thCheckCDFTotal As Decimal = 0
            Dim dec13thCheckUSDTotal As Decimal = 0
            Dim decBrutCDFTotal As Decimal = 0
            Dim decBrutUSDTotal As Decimal = 0
            Dim decDeductionCDFTotal As Decimal = 0
            Dim decDeductionUSDTotal As Decimal = 0
            Dim decContributionCDFTotal As Decimal = 0
            Dim decContributionUSDTotal As Decimal = 0
            'Sohail (08 Jul 2016) -- Start
            'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
            'Dim decFamilyAllowanceCDFTotal As Decimal = 0
            'Dim decFamilyAllowanceUSDTotal As Decimal = 0
            'Sohail (08 Jul 2016) -- End

            Dim decSalaryHeadAmount As Decimal = 0
            Dim decCongePreavisHeadAmount As Decimal = 0
            Dim decNotivr As Decimal = 0
            Dim decChild As Decimal = 0
            Dim decDependent As Decimal = 0
            Dim dec13ThCheck As Decimal = 0
            'Sohail (08 Jul 2016) -- Start
            'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
            Dim decFamilyAllowance As Decimal = 0
            Dim decLoanRepayment As Decimal = 0
            Dim decAdvanceRepayment As Decimal = 0
            Dim decOtherDeductions As Decimal = 0
            Dim decIPRNet As Decimal = 0
            Dim decINSS35 As Decimal = 0
            'Sohail (08 Jul 2016) -- End
            Dim decDaysInPeriod As Decimal = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintDaysInPeriodHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            decSalaryHeadAmount = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintSalaryHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            decCongePreavisHeadAmount = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintCongePreavisHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            'Sohail (08 Jul 2016) -- Start
            'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
            'decCongePreavisHeadAmount = 3.67 'TODO : Remove this line
            If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso decCongePreavisHeadAmount = 0 Then
                decCongePreavisHeadAmount = (From p In dsCurrPeriod.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintCongePreavisHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            End If
            Dim decNotificationAllowance As Decimal = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintNotificationAllowanceHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso decNotificationAllowance = 0 Then
                decNotificationAllowance = (From p In dsCurrPeriod.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintNotificationAllowanceHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            End If
            'Sohail (08 Jul 2016) -- End
            Dim decRateofTheDay As Decimal '= (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintRateOfTheDayHeadId) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            decRateofTheDay = 10 'TODO : Remove this line
            'Sohail (08 Jul 2016) -- Start
            'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
            'decNotivr = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintTerminationFactorID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            decNotivr = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintRateOfNoticeHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso decNotivr = 0 Then
                decNotivr = (From p In dsCurrPeriod.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintRateOfNoticeHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            End If
            'Sohail (08 Jul 2016) -- End
            decChild = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintChildHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            'Sohail (08 Jul 2016) -- Start
            'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
            If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso decChild = 0 Then
                decChild = (From p In dsCurrPeriod.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintChildHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            End If
            'Sohail (08 Jul 2016) -- End
            decDependent = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintDependentHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            'Sohail (08 Jul 2016) -- Start
            'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
            If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso decDependent = 0 Then
                decDependent = (From p In dsCurrPeriod.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintDependentHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            End If
            'Sohail (08 Jul 2016) -- End
            Dim lst13thCheck As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mint13thCheckHeadID) Select (p)).ToList 'TODO :251: 13th check head
            If lst13thCheck.Count > 0 Then
                dec13ThCheck = CDec(lst13thCheck(0).Item("amount"))
            End If
            'Sohail (08 Jul 2016) -- Start
            'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
            If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso dec13ThCheck = 0 Then
                lst13thCheck = (From p In dsCurrPeriod.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mint13thCheckHeadID) Select (p)).ToList 'TODO :251: 13th check head
                If lst13thCheck.Count > 0 Then
                    dec13ThCheck = CDec(lst13thCheck(0).Item("amount"))
                End If
            End If

            Dim lstFamilyAllowance As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintFamilyAllowanceHeadID) Select (p)).ToList
            If lstFamilyAllowance.Count > 0 Then
                decFamilyAllowance = CDec(lstFamilyAllowance(0).Item("amount"))
            End If

            If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso decFamilyAllowance = 0 Then
                lstFamilyAllowance = (From p In dsCurrPeriod.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintFamilyAllowanceHeadID) Select (p)).ToList
                If lstFamilyAllowance.Count > 0 Then
                    decFamilyAllowance = CDec(lstFamilyAllowance(0).Item("amount"))
                End If
            End If

            mdecBaseImposable = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintBaseImposableHeadId) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum() 'TODO :20: Base Imposable head
            If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso mdecBaseImposable = 0 Then
                mdecBaseImposable = (From p In dsCurrPeriod.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintBaseImposableHeadId) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum()
            End If
            'Sohail (08 Jul 2016) -- End

            Dim startdate As String = ""
            Dim enddate As String = ""
            Dim intEmpWorkingDaysInPeriod As Integer = 0
            If dsList.Tables("DataTable").Rows.Count > 0 Then
                Dim objEmpShift As New clsEmployee_Shift_Tran
                'Sohail (08 Jul 2016) -- Start
                'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
                'startdate = eZeeDate.convertDate(eZeeDate.convertDate(dsList.Tables("DataTable").Rows(0).Item("end_date").ToString).AddDays(1))
                startdate = eZeeDate.convertDate(mdtPeriodStartDate)
                'Sohail (08 Jul 2016) -- End
                startdate = IIf(dsList.Tables("DataTable").Rows(0).Item("appointeddate").ToString > startdate, dsList.Tables("DataTable").Rows(0).Item("appointeddate").ToString, startdate)
                enddate = dsList.Tables("DataTable").Rows(0).Item("termination_to_date").ToString
                If IsDBNull(dsList.Tables("DataTable").Rows(0).Item("termination_from_date")) = False Then
                    enddate = IIf(dsList.Tables("DataTable").Rows(0).Item("termination_from_date").ToString < enddate, dsList.Tables("DataTable").Rows(0).Item("termination_from_date").ToString, enddate)
                End If
                If IsDBNull(dsList.Tables("DataTable").Rows(0).Item("empl_enddate")) = False Then
                    enddate = IIf(dsList.Tables("DataTable").Rows(0).Item("empl_enddate").ToString < enddate, dsList.Tables("DataTable").Rows(0).Item("empl_enddate").ToString, enddate)
                End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                'objEmpShift.GetWorkingDaysAndHours(CInt(dsList.Tables("DataTable").Rows(0).Item("employeeunkid")), eZeeDate.convertDate(startdate), eZeeDate.convertDate(enddate), intEmpWorkingDaysInPeriod, 0)
                objEmpShift.GetWorkingDaysAndHours(xDatabaseName, _
                                                   xUserUnkid, _
                                                   xYearUnkid, _
                                                   xCompanyUnkid, _
                                                   xUserModeSetting, _
                                                   CInt(dsList.Tables("DataTable").Rows(0).Item("employeeunkid")), _
                                                   eZeeDate.convertDate(startdate), _
                                                   eZeeDate.convertDate(enddate), _
                                                   intEmpWorkingDaysInPeriod, 0)
                'Shani(24-Aug-2015) -- End
            End If

            Dim objLeaveBalance As New clsleavebalance_tran
            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsLvBalance = objLeaveBalance.GetLeaveBalanceInfo(mdtPeriodEndDate, mintLeaveTypeId, mintEmployeeunkid, mdtDatabaseStartDate, mdtDatabaseEndDate, , , mintLeaveBalanceSetting, mintIsFin_Close, mintYearUnkId)
                dsLvBalance = objLeaveBalance.GetLeaveBalanceInfo(mdtPeriodEndDate, mintLeaveTypeId, mintEmployeeunkid, mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtDatabaseStartDate, mdtDatabaseEndDate, , , mintLeaveBalanceSetting, mintIsFin_Close, mintYearUnkId)
                'Pinkal (18-Nov-2016) -- End


            ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                'Pinkal (25-Jul-2015) -- Start
                'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 
                objLeaveBalance._DBStartdate = mdtDatabaseStartDate.Date
                objLeaveBalance._DBEnddate = mdtDatabaseEndDate.Date
                'Pinkal (25-Jul-2015) -- End

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsLvBalance = objLeaveBalance.GetLeaveBalanceInfo(mdtPeriodEndDate, mintLeaveTypeId, mintEmployeeunkid, mdtDatabaseStartDate, mdtDatabaseEndDate, True, True, mintLeaveBalanceSetting, mintIsFin_Close, mintYearUnkId) 'TODO : 118: mintYearUnkId
                dsLvBalance = objLeaveBalance.GetLeaveBalanceInfo(mdtPeriodEndDate, mintLeaveTypeId, mintEmployeeunkid, mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtDatabaseStartDate, mdtDatabaseEndDate, True, True, mintLeaveBalanceSetting, mintIsFin_Close, mintYearUnkId) 'TODO : 118: mintYearUnkId
                'Pinkal (18-Nov-2016) -- End

            End If


            For Each pair In dicHourMapping
                Dim intHourHeadID As Integer = pair.Value
                Dim intAmtHeadID As Integer = pair.Key



                Dim lst As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = intAmtHeadID) Select (p)).ToList

                For Each dtRow As DataRow In lst
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                    rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                    rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                    rpt_Row.Item("Column3") = dtRow.Item("employeename")
                    rpt_Row.Item("Column4") = dtRow.Item("departmentunkid")
                    rpt_Row.Item("Column5") = dtRow.Item("departmentname")
                    rpt_Row.Item("Column6") = dtRow.Item("jobunkid")
                    rpt_Row.Item("Column7") = dtRow.Item("job_name")
                    rpt_Row.Item("Column8") = dtRow.Item("employmenttype")
                    rpt_Row.Item("Column9") = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString).ToShortDateString
                    rpt_Row.Item("Column10") = dtRow.Item("payperiodunkid")
                    rpt_Row.Item("Column11") = dtRow.Item("period_name")
                    rpt_Row.Item("Column12") = dtRow.Item("start_date")
                    rpt_Row.Item("Column13") = dtRow.Item("end_date")
                    rpt_Row.Item("Column14") = dtRow.Item("tranheadunkid")
                    rpt_Row.Item("Column15") = dtRow.Item("trnheadcode")
                    rpt_Row.Item("Column16") = dtRow.Item("trnheadname")
                    rpt_Row.Item("Column20") = eZeeDate.convertDate(startdate).ToShortDateString
                    rpt_Row.Item("Column21") = eZeeDate.convertDate(enddate).ToShortDateString
                    rpt_Row.Item("Column22") = intEmpWorkingDaysInPeriod
                    'Shani [ 24 NOV 2014 ] -- START
                    rpt_Row.Item("Column32") = CInt(decNotivr)
                    rpt_Row.Item("Column33") = mstrTerminationFactorName
                    rpt_Row.Item("Column34") = CInt(decChild)
                    rpt_Row.Item("Column35") = CInt(decDependent)
                    rpt_Row.Item("Column36") = mdtNoticeDate.ToShortDateString
                    'Shani [ 24 NOV 2014 ] -- END
                    'Sohail (08 Jul 2016) -- Start
                    'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
                    'rpt_Row.Item("Column37") = decSalaryHeadAmount
                    'rpt_Row.Item("Column38") = decSalaryHeadAmount * 12 + dec13ThCheck
                    'rpt_Row.Item("Column37") = Format(decSalaryHeadAmount, strfmtCurrency) & " (" & Format(decSalaryHeadAmount / mdecEx_Rate, strfmtCurrency) & " " & mstrBaseCurrency_Sign & ")"
                    rpt_Row.Item("Column37") = Format(decSalaryHeadAmount / mdecEx_Rate, strfmtCurrency)
                    'rpt_Row.Item("Column38") = Format(decSalaryHeadAmount * 12 + dec13ThCheck, strfmtCurrency) & " (" & Format((decSalaryHeadAmount * 12 + dec13ThCheck) / mdecEx_Rate, strfmtCurrency) & " " & mstrBaseCurrency_Sign & ")"
                    rpt_Row.Item("Column38") = Format((decSalaryHeadAmount * 12 + dec13ThCheck) / mdecEx_Rate, strfmtCurrency)
                    rpt_Row.Item("Column39") = Format(decSalaryHeadAmount, strfmtCurrency)
                    rpt_Row.Item("Column40") = Format((decSalaryHeadAmount * 12 + dec13ThCheck), strfmtCurrency)
                    rpt_Row.Item("Column41") = mstrBaseCurrency_Sign
                    rpt_Row.Item("Column42") = mstrCurrency_Sign
                    'Sohail (08 Jul 2016) -- End

                    'Dim lstHour As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = intHourHeadID) Select (p)).ToList
                    'If lstHour.Count > 0 Then
                    '    rpt_Row.Item("Column17") = Format(CDec(lstHour(0).Item("amount")), GUI.fmtCurrency)
                    'Else
                    '    rpt_Row.Item("Column17") = ""
                    'End If
                    Dim objShift As New clsEmployee_Shift_Tran
                    Dim intWorkingDays As Integer = 0

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                    'objShift.GetWeekendDaysAndHours(mintEmployeeunkid, mdtPeriodStartDate, mdtPeriodEndDate, intWorkingDays, 0)
                    'Sohail (08 Jul 2016) -- Start
                    'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
                    'objShift.GetWeekendDaysAndHours(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, mintEmployeeunkid, mdtPeriodStartDate, mdtPeriodEndDate, intWorkingDays, 0)
                    objShift.GetWorkingDaysAndHours(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, mintEmployeeunkid, mdtPeriodStartDate, mdtPeriodEndDate, intWorkingDays, 0)
                    'Sohail (08 Jul 2016) -- End
                    'Shani(24-Aug-2015) -- End
                    'Sohail (08 Jul 2016) -- Start
                    'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
                    'rpt_Row.Item("Column17") = intWorkingDays
                    If CInt(dtRow.Item("tranheadunkid")) = mintLogementHeadID Then
                        rpt_Row.Item("Column17") = 26
                    Else
                        rpt_Row.Item("Column17") = intWorkingDays
                    End If
                    'Sohail (08 Jul 2016) -- End


                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Row.Item("Column18") = Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency)
                    rpt_Row.Item("Column18") = Format(CDec(dtRow.Item("amount")), strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End

                    If mdicMembrshipData.ContainsKey(CInt(dtRow.Item("employeeunkid"))) = True Then
                        rpt_Row.Item("Column19") = mdicMembrshipData.Item(CInt(dtRow.Item("employeeunkid")))
                    Else
                        rpt_Row.Item("Column19") = ""
                    End If

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Row.Item("Column23") = Format(CDec(dtRow.Item("amount")) / decDaysInPeriod, GUI.fmtCurrency) 'TODO :26: Days in Period Head
                    'rpt_Row.Item("Column24") = Format(CDec(rpt_Row.Item("Column17")) * CDec(rpt_Row.Item("Column23")), GUI.fmtCurrency)
                    'rpt_Row.Item("Column23") = Format(CDec(dtRow.Item("amount")) / decDaysInPeriod, strfmtCurrency) 'TODO :26: Days in Period Head
                    If decDaysInPeriod <> 0 Then
                        rpt_Row.Item("Column23") = Format(CDec(dtRow.Item("amount")) / decDaysInPeriod, strfmtCurrency) 'TODO :26: Days in Period Head
                    Else
                        rpt_Row.Item("Column23") = Format(0, strfmtCurrency) 'TODO :26: Days in Period Head
                    End If
                    rpt_Row.Item("Column24") = Format(CDec(rpt_Row.Item("Column17")) * CDec(rpt_Row.Item("Column23")), strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                    If mdecEx_Rate <> 0 Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Row.Item("Column25") = Format(CDec(rpt_Row.Item("Column24")) * mdecEx_Rate, GUI.fmtCurrency)
                        rpt_Row.Item("Column25") = Format(CDec(rpt_Row.Item("Column24")) * mdecEx_Rate, strfmtCurrency)
                        'Sohail (21 Aug 2015) -- End
                    Else
                        rpt_Row.Item("Column25") = ""
                    End If

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'decPrestationTotal += CDec(Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency))
                    'decPrestationCDFTotal += CDec(Format(CDec(IIf(rpt_Row.Item("Column24").ToString.Trim = "", 0, rpt_Row.Item("Column24"))), GUI.fmtCurrency))
                    'decPrestationUSDTotal += CDec(Format(CDec(IIf(rpt_Row.Item("Column25").ToString.Trim = "", 0, rpt_Row.Item("Column25"))), GUI.fmtCurrency))
                    decPrestationTotal += CDec(Format(CDec(dtRow.Item("amount")), strfmtCurrency))
                    decPrestationCDFTotal += CDec(Format(CDec(IIf(rpt_Row.Item("Column24").ToString.Trim = "", 0, rpt_Row.Item("Column24"))), strfmtCurrency))
                    decPrestationUSDTotal += CDec(Format(CDec(IIf(rpt_Row.Item("Column25").ToString.Trim = "", 0, rpt_Row.Item("Column25"))), strfmtCurrency))
                    'Sohail (21 Aug 2015) -- End

                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                    '--------------------------------------------------



                    dr_Row = dsNotice.Tables(0).NewRow

                    dr_Row.Item("Column1") = dtRow.Item("employeeunkid")
                    dr_Row.Item("Column2") = dtRow.Item("employeecode")
                    dr_Row.Item("Column3") = dtRow.Item("employeename")
                    dr_Row.Item("Column4") = dtRow.Item("departmentunkid")
                    dr_Row.Item("Column5") = dtRow.Item("departmentname")
                    dr_Row.Item("Column6") = dtRow.Item("jobunkid")
                    dr_Row.Item("Column7") = dtRow.Item("job_name")
                    dr_Row.Item("Column8") = dtRow.Item("employmenttype")
                    dr_Row.Item("Column9") = dtRow.Item("appointeddate")
                    dr_Row.Item("Column10") = dtRow.Item("payperiodunkid")
                    dr_Row.Item("Column11") = dtRow.Item("period_name")
                    dr_Row.Item("Column12") = dtRow.Item("start_date")
                    dr_Row.Item("Column13") = dtRow.Item("end_date")
                    dr_Row.Item("Column14") = dtRow.Item("tranheadunkid")
                    dr_Row.Item("Column15") = dtRow.Item("trnheadcode")
                    dr_Row.Item("Column16") = dtRow.Item("trnheadname")
                    dr_Row.Item("Column20") = eZeeDate.convertDate(startdate).ToShortDateString
                    dr_Row.Item("Column21") = eZeeDate.convertDate(enddate).ToShortDateString
                    dr_Row.Item("Column22") = intEmpWorkingDaysInPeriod

                    Dim lstNotice As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = intHourHeadID) Select (p)).ToList
                    If lstNotice.Count > 0 Then
                        'Sohail (08 Jul 2016) -- Start
                        'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
                        'dr_Row.Item("Column17") = mintNotificationAllowanceHeadID.ToString 'TODO :10: notification allowance (factor) head
                        'dr_Row.Item("Column17") = Format(lstNotice(0).Item("amount"), "0")
                        dr_Row.Item("Column17") = Format(decNotificationAllowance, "0")
                        'Sohail (08 Jul 2016) -- End
                    Else
                        'dr_Row.Item("Column17") = "0"
                        dr_Row.Item("Column17") = Format(decNotificationAllowance, "0")
                    End If


                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dr_Row.Item("Column18") = Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency)
                    dr_Row.Item("Column18") = Format(CDec(dtRow.Item("amount")), strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End

                    If mdicMembrshipData.ContainsKey(CInt(dtRow.Item("employeeunkid"))) = True Then
                        'Sohail (08 Jul 2016) -- Start
                        'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
                        'rpt_Row.Item("Column19") = mdicMembrshipData.Item(CInt(dtRow.Item("employeeunkid")))
                        dr_Row.Item("Column19") = mdicMembrshipData.Item(CInt(dtRow.Item("employeeunkid")))
                        'Sohail (08 Jul 2016) -- End
                    Else
                        'Sohail (08 Jul 2016) -- Start
                        'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
                        'rpt_Row.Item("Column19") = ""
                        dr_Row.Item("Column19") = ""
                        'Sohail (08 Jul 2016) -- End
                    End If

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dr_Row.Item("Column23") = Format(CDec(dtRow.Item("amount")) / decDaysInPeriod, GUI.fmtCurrency) 'TODO :26: Days in Period Head
                    'dr_Row.Item("Column24") = Format(CDec(dr_Row.Item("Column17")) * CDec(dr_Row.Item("Column23")), GUI.fmtCurrency)
                    'dr_Row.Item("Column23") = Format(CDec(dtRow.Item("amount")) / decDaysInPeriod, strfmtCurrency) 'TODO :26: Days in Period Head
                    If decDaysInPeriod <> 0 Then
                        dr_Row.Item("Column23") = Format(CDec(dtRow.Item("amount")) / decDaysInPeriod, strfmtCurrency) 'TODO :26: Days in Period Head
                    Else
                        dr_Row.Item("Column23") = Format(0, strfmtCurrency) 'TODO :26: Days in Period Head
                    End If
                    dr_Row.Item("Column24") = Format(CDec(dr_Row.Item("Column17")) * CDec(dr_Row.Item("Column23")), strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                    If mdecEx_Rate <> 0 Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'dr_Row.Item("Column25") = Format(CDec(dr_Row.Item("Column24")) * mdecEx_Rate, GUI.fmtCurrency)
                        dr_Row.Item("Column25") = Format(CDec(dr_Row.Item("Column24")) * mdecEx_Rate, strfmtCurrency)
                        'Sohail (21 Aug 2015) -- End
                    Else
                        dr_Row.Item("Column25") = ""
                    End If

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'decNoticeCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
                    'decNoticeUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
                    decNoticeCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
                    decNoticeUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
                    'Sohail (21 Aug 2015) -- End

                    dsNotice.Tables("ArutiTable").Rows.Add(dr_Row)
                    '------------------------------------------------------------------------------


                    'Sohail (08 Jul 2016) -- Start
                    'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
                    'dr_Row = dsBonus.Tables(0).NewRow

                    'dr_Row.Item("Column1") = dtRow.Item("employeeunkid")
                    'dr_Row.Item("Column2") = dtRow.Item("employeecode")
                    'dr_Row.Item("Column3") = dtRow.Item("employeename")
                    'dr_Row.Item("Column4") = dtRow.Item("departmentunkid")
                    'dr_Row.Item("Column5") = dtRow.Item("departmentname")
                    'dr_Row.Item("Column6") = dtRow.Item("jobunkid")
                    'dr_Row.Item("Column7") = dtRow.Item("job_name")
                    'dr_Row.Item("Column8") = dtRow.Item("employmenttype")
                    'dr_Row.Item("Column9") = dtRow.Item("appointeddate")
                    'dr_Row.Item("Column10") = dtRow.Item("payperiodunkid")
                    'dr_Row.Item("Column11") = dtRow.Item("period_name")
                    'dr_Row.Item("Column12") = dtRow.Item("start_date")
                    'dr_Row.Item("Column13") = dtRow.Item("end_date")
                    'dr_Row.Item("Column14") = dtRow.Item("tranheadunkid")
                    'dr_Row.Item("Column15") = dtRow.Item("trnheadcode")
                    'dr_Row.Item("Column16") = dtRow.Item("trnheadname")
                    'dr_Row.Item("Column20") = eZeeDate.convertDate(startdate).ToShortDateString
                    'dr_Row.Item("Column21") = eZeeDate.convertDate(enddate).ToShortDateString
                    'dr_Row.Item("Column22") = intEmpWorkingDaysInPeriod

                    'Dim lstBonus As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = intHourHeadID) Select (p)).ToList
                    'If lstBonus.Count > 0 Then
                    '    dr_Row.Item("Column17") = mintNotificationAllowanceHeadID 'TODO :0: notification allowance (factor) head
                    'Else
                    '    dr_Row.Item("Column17") = "0"
                    'End If


                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    ''dr_Row.Item("Column18") = Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency)
                    'dr_Row.Item("Column18") = Format(CDec(dtRow.Item("amount")), strfmtCurrency)
                    ''Sohail (21 Aug 2015) -- End

                    'If mdicMembrshipData.ContainsKey(CInt(dtRow.Item("employeeunkid"))) = True Then
                    '    rpt_Row.Item("Column19") = mdicMembrshipData.Item(CInt(dtRow.Item("employeeunkid")))
                    'Else
                    '    rpt_Row.Item("Column19") = ""
                    'End If

                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    ''dr_Row.Item("Column23") = Format(CDec(dtRow.Item("amount")) / decDaysInPeriod, GUI.fmtCurrency) 'TODO :26: Days in Period Head
                    ''dr_Row.Item("Column24") = Format(CDec(dr_Row.Item("Column17")) * CDec(dr_Row.Item("Column23")), GUI.fmtCurrency)
                    'dr_Row.Item("Column23") = Format(CDec(dtRow.Item("amount")) / decDaysInPeriod, strfmtCurrency) 'TODO :26: Days in Period Head
                    'dr_Row.Item("Column24") = Format(CDec(dr_Row.Item("Column17")) * CDec(dr_Row.Item("Column23")), strfmtCurrency)
                    ''Sohail (21 Aug 2015) -- End
                    'If mdecEx_Rate <> 0 Then
                    '    'Sohail (21 Aug 2015) -- Start
                    '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    '    'dr_Row.Item("Column25") = Format(CDec(dr_Row.Item("Column24")) * mdecEx_Rate, GUI.fmtCurrency)
                    '    dr_Row.Item("Column25") = Format(CDec(dr_Row.Item("Column24")) * mdecEx_Rate, strfmtCurrency)
                    '    'Sohail (21 Aug 2015) -- End
                    'Else
                    '    dr_Row.Item("Column25") = ""
                    'End If

                    ''Sohail (21 Aug 2015) -- Start
                    ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    ''decBonusCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
                    ''decBonusUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
                    'decBonusCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
                    'decBonusUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
                    ''Sohail (21 Aug 2015) -- End

                    'dsBonus.Tables("ArutiTable").Rows.Add(dr_Row)
                    'Sohail (08 Jul 2016) -- End

                Next

            Next

            'Sohail (08 Jul 2016) -- Start
            'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
            Dim lstBonus As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (marrBonusHead.Contains(p.Item("tranheadunkid").ToString)) Select (p)).ToList
            If lstBonus.Count > 0 Then
                For Each dtRow As DataRow In lstBonus

                    dr_Row = dsBonus.Tables(0).NewRow

                    dr_Row.Item("Column1") = dtRow.Item("employeeunkid")
                    dr_Row.Item("Column2") = dtRow.Item("employeecode")
                    dr_Row.Item("Column3") = dtRow.Item("employeename")
                    dr_Row.Item("Column4") = dtRow.Item("departmentunkid")
                    dr_Row.Item("Column5") = dtRow.Item("departmentname")
                    dr_Row.Item("Column6") = dtRow.Item("jobunkid")
                    dr_Row.Item("Column7") = dtRow.Item("job_name")
                    dr_Row.Item("Column8") = dtRow.Item("employmenttype")
                    dr_Row.Item("Column9") = dtRow.Item("appointeddate")
                    dr_Row.Item("Column10") = dtRow.Item("payperiodunkid")
                    dr_Row.Item("Column11") = dtRow.Item("period_name")
                    dr_Row.Item("Column12") = dtRow.Item("start_date")
                    dr_Row.Item("Column13") = dtRow.Item("end_date")
                    dr_Row.Item("Column14") = dtRow.Item("tranheadunkid")
                    dr_Row.Item("Column15") = dtRow.Item("trnheadcode")
                    dr_Row.Item("Column16") = dtRow.Item("trnheadname")
                    dr_Row.Item("Column20") = eZeeDate.convertDate(startdate).ToShortDateString
                    dr_Row.Item("Column21") = eZeeDate.convertDate(enddate).ToShortDateString
                    dr_Row.Item("Column22") = intEmpWorkingDaysInPeriod

                    dr_Row.Item("Column17") = "0"
                    dr_Row.Item("Column18") = "" 'Format(CDec(dtRow.Item("amount")), strfmtCurrency)

                    If mdicMembrshipData.ContainsKey(CInt(dtRow.Item("employeeunkid"))) = True Then
                        dr_Row.Item("Column19") = mdicMembrshipData.Item(CInt(dtRow.Item("employeeunkid")))
                    Else
                        dr_Row.Item("Column19") = ""
                    End If

                    dr_Row.Item("Column23") = "" 'Format(CDec(dtRow.Item("amount")) / decDaysInPeriod, strfmtCurrency)
                    dr_Row.Item("Column24") = Format(CDec(dtRow.Item("amount")), strfmtCurrency)
                    If mdecEx_Rate <> 0 Then
                        dr_Row.Item("Column25") = Format(CDec(dr_Row.Item("Column24")) * mdecEx_Rate, strfmtCurrency)
                    Else
                        dr_Row.Item("Column25") = ""
                    End If

                    decBonusCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
                    decBonusUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))

                    dsBonus.Tables("ArutiTable").Rows.Add(dr_Row)
                Next
            End If
            '------------------------------------------------------------------------------------
            'Sohail (08 Jul 2016) -- End

            dr_Row = dsLeave.Tables(0).NewRow
            dr_Row.Item("Column1") = mintEmployeeunkid
            dr_Row.Item("Column16") = "Balance de congé "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dr_Row.Item("Column23") = Format(decSalaryHeadAmount / decDaysInPeriod, GUI.fmtCurrency) 'TODO :26: Days in Period Head
            'dr_Row.Item("Column23") = Format(decSalaryHeadAmount / decDaysInPeriod, strfmtCurrency) 'TODO :26: Days in Period Head
            If decDaysInPeriod <> 0 Then
                dr_Row.Item("Column23") = Format(decSalaryHeadAmount / decDaysInPeriod, strfmtCurrency) 'TODO :26: Days in Period Head
            Else
                dr_Row.Item("Column23") = Format(0, strfmtCurrency)
            End If
            'Sohail (21 Aug 2015) -- End
            If dsLvBalance.Tables(0).Rows.Count > 0 Then
                dr_Row.Item("Column17") = CDec(dsLvBalance.Tables(0).Rows(0).Item("Balance"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dr_Row.Item("Column24") = Format(CDec(dr_Row.Item("Column17")) * CDec(dr_Row.Item("Column23")), GUI.fmtCurrency)
                dr_Row.Item("Column24") = Format(CDec(dr_Row.Item("Column17")) * CDec(dr_Row.Item("Column23")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
            Else
                dr_Row.Item("Column17") = ""
                dr_Row.Item("Column24") = ""
            End If
            dr_Row.Item("Column18") = ""
            If mdecEx_Rate <> 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, GUI.fmtCurrency)
                dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
            Else
                dr_Row.Item("Column25") = ""
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'decLeaveCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
            'decLeaveUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
            decLeaveCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decLeaveUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
            'Sohail (21 Aug 2015) -- End

            dsLeave.Tables(0).Rows.Add(dr_Row)

            dr_Row = dsLeave.Tables(0).NewRow
            dr_Row.Item("Column1") = mintEmployeeunkid
            If lstFamilyAllowance.Count > 0 Then
                dr_Row.Item("Column16") = lstFamilyAllowance(0).Item("trnheadname").ToString
            Else
                dr_Row.Item("Column16") = ""
            End If
            dr_Row.Item("Column18") = Format(decChild, "00")
            'dr_Row.Item("Column23") = Format(decFamilyAllowance / decDaysInPeriod, strfmtCurrency)
            If decDaysInPeriod <> 0 Then
                dr_Row.Item("Column23") = Format(decFamilyAllowance / decDaysInPeriod, strfmtCurrency)
            Else
                dr_Row.Item("Column23") = Format(0, strfmtCurrency)
            End If
            If dsLvBalance.Tables(0).Rows.Count > 0 Then
                dr_Row.Item("Column17") = CDec(dsLvBalance.Tables(0).Rows(0).Item("Balance"))
                dr_Row.Item("Column24") = Format(CDec(dr_Row.Item("Column17")) * CDec(dr_Row.Item("Column23")), strfmtCurrency)
            Else
                dr_Row.Item("Column17") = ""
                dr_Row.Item("Column24") = ""
            End If
            dr_Row.Item("Column18") = ""
            If mdecEx_Rate <> 0 Then
                dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
            Else
                dr_Row.Item("Column25") = ""
            End If

            decLeaveCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decLeaveUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))

            dsLeave.Tables(0).Rows.Add(dr_Row)
            '-----------------------------------------------


            dr_Row = dsCongePreavis.Tables(0).NewRow
            dr_Row.Item("Column1") = mintEmployeeunkid
            dr_Row.Item("Column16") = Language.getMessage(mstrModuleName, 40, "5. Conge Préavis")

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dr_Row.Item("Column23") = Format(decSalaryHeadAmount / decDaysInPeriod, GUI.fmtCurrency) 'TODO :26: Days in Period Head
            'dr_Row.Item("Column23") = Format(decSalaryHeadAmount / decDaysInPeriod, strfmtCurrency) 'TODO :26: Days in Period Head
            If decDaysInPeriod <> 0 Then
                dr_Row.Item("Column23") = Format(decSalaryHeadAmount / decDaysInPeriod, strfmtCurrency) 'TODO :26: Days in Period Head
            Else
                dr_Row.Item("Column23") = Format(0, strfmtCurrency) 'TODO :26: Days in Period Head
            End If
            'Sohail (21 Aug 2015) -- End
            If decCongePreavisHeadAmount > 0 Then
                dr_Row.Item("Column17") = decCongePreavisHeadAmount
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dr_Row.Item("Column24") = Format(CDec(dr_Row.Item("Column17")) * CDec(dr_Row.Item("Column23")), GUI.fmtCurrency)
                dr_Row.Item("Column24") = Format(CDec(dr_Row.Item("Column17")) * CDec(dr_Row.Item("Column23")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
            Else
                dr_Row.Item("Column17") = ""
                dr_Row.Item("Column24") = ""
            End If
            dr_Row.Item("Column18") = ""
            If mdecEx_Rate <> 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, GUI.fmtCurrency)
                dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
            Else
                dr_Row.Item("Column25") = ""
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'decCongeCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
            'decCongeUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
            decCongeCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decCongeUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
            'Sohail (21 Aug 2015) -- End

            dsCongePreavis.Tables(0).Rows.Add(dr_Row)
            '-----------------------------------------------



            dr_Row = ds13thCheck.Tables(0).NewRow
            dr_Row.Item("Column1") = mintEmployeeunkid
            dr_Row.Item("Column16") = Language.getMessage(mstrModuleName, 42, "6. 13th Check (prorata temporis)")
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dr_Row.Item("Column17") = Format(1, GUI.fmtCurrency)
            'dr_Row.Item("Column18") = Format(dec13ThCheck, GUI.fmtCurrency)
            dr_Row.Item("Column17") = Format(1, strfmtCurrency)
            dr_Row.Item("Column18") = Format(dec13ThCheck, strfmtCurrency)
            'Sohail (21 Aug 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dr_Row.Item("Column23") = Format(DateDiff(DateInterval.Month, mdtDatabaseStartDate, mdtPeriodStartDate), GUI.fmtCurrency) 'TODO :26: Days in Period Head
            'dr_Row.Item("Column24") = Format(CDec(dr_Row.Item("Column17")) * CDec(dr_Row.Item("Column18")) * (CDec(dr_Row.Item("Column23")) / 12), GUI.fmtCurrency)
            dr_Row.Item("Column23") = Format(DateDiff(DateInterval.Month, mdtDatabaseStartDate, mdtPeriodStartDate), strfmtCurrency) 'TODO :26: Days in Period Head
            dr_Row.Item("Column24") = Format(CDec(dr_Row.Item("Column17")) * CDec(dr_Row.Item("Column18")) * (CDec(dr_Row.Item("Column23")) / 12), strfmtCurrency)
            'Sohail (21 Aug 2015) -- End


            If mdecEx_Rate <> 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, GUI.fmtCurrency)
                dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
            Else
                dr_Row.Item("Column25") = ""
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dec13thCheckCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
            'dec13thCheckUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
            dec13thCheckCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            dec13thCheckUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
            'Sohail (21 Aug 2015) -- End

            ds13thCheck.Tables(0).Rows.Add(dr_Row)
            '-----------------------------------------------



            dr_Row = dsBrut.Tables(0).NewRow
            dr_Row.Item("Column1") = mintEmployeeunkid
            dr_Row.Item("Column16") = Language.getMessage(mstrModuleName, 28, "1. Prestation")
            dr_Row.Item("Column17") = ""
            dr_Row.Item("Column18") = ""
            dr_Row.Item("Column23") = ""
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dr_Row.Item("Column24") = Format(decPrestationCDFTotal, GUI.fmtCurrency)
            'dr_Row.Item("Column25") = Format(decPrestationUSDTotal, GUI.fmtCurrency)
            'decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
            'decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
            dr_Row.Item("Column24") = Format(decPrestationCDFTotal, strfmtCurrency)
            dr_Row.Item("Column25") = Format(decPrestationUSDTotal, strfmtCurrency)
            decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
            'Sohail (21 Aug 2015) -- End
            dsBrut.Tables(0).Rows.Add(dr_Row)

            dr_Row = dsBrut.Tables(0).NewRow
            dr_Row.Item("Column1") = mintEmployeeunkid
            dr_Row.Item("Column16") = Language.getMessage(mstrModuleName, 34, "2. Notice")
            dr_Row.Item("Column17") = ""
            dr_Row.Item("Column18") = ""
            dr_Row.Item("Column23") = ""
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dr_Row.Item("Column24") = Format(decNoticeCDFTotal, GUI.fmtCurrency)
            'dr_Row.Item("Column25") = Format(decNoticeUSDTotal, GUI.fmtCurrency)
            'decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
            'decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
            dr_Row.Item("Column24") = Format(decNoticeCDFTotal, strfmtCurrency)
            dr_Row.Item("Column25") = Format(decNoticeUSDTotal, strfmtCurrency)
            decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
            'Sohail (21 Aug 2015) -- End
            dsBrut.Tables(0).Rows.Add(dr_Row)

            dr_Row = dsBrut.Tables(0).NewRow
            dr_Row.Item("Column1") = mintEmployeeunkid
            dr_Row.Item("Column16") = Language.getMessage(mstrModuleName, 36, "3. Bonus")
            dr_Row.Item("Column17") = ""
            dr_Row.Item("Column18") = ""
            dr_Row.Item("Column23") = ""
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dr_Row.Item("Column24") = Format(decBonusCDFTotal, GUI.fmtCurrency)
            'dr_Row.Item("Column25") = Format(decBonusUSDTotal, GUI.fmtCurrency)
            'decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
            'decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
            dr_Row.Item("Column24") = Format(decBonusCDFTotal, strfmtCurrency)
            dr_Row.Item("Column25") = Format(decBonusUSDTotal, strfmtCurrency)
            decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
            'Sohail (21 Aug 2015) -- End
            dsBrut.Tables(0).Rows.Add(dr_Row)

            dr_Row = dsBrut.Tables(0).NewRow
            dr_Row.Item("Column1") = mintEmployeeunkid
            dr_Row.Item("Column16") = "Leave non taken"
            dr_Row.Item("Column17") = ""
            dr_Row.Item("Column18") = ""
            dr_Row.Item("Column23") = ""
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dr_Row.Item("Column24") = Format(decLeaveCDFTotal, GUI.fmtCurrency)
            'dr_Row.Item("Column25") = Format(decLeaveUSDTotal, GUI.fmtCurrency)
            'decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
            'decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
            dr_Row.Item("Column24") = Format(decLeaveCDFTotal, strfmtCurrency)
            dr_Row.Item("Column25") = Format(decLeaveUSDTotal, strfmtCurrency)
            decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
            'Sohail (21 Aug 2015) -- End
            dsBrut.Tables(0).Rows.Add(dr_Row)

            dr_Row = dsBrut.Tables(0).NewRow
            dr_Row.Item("Column1") = mintEmployeeunkid
            dr_Row.Item("Column16") = "Congé préavis"
            dr_Row.Item("Column17") = ""
            dr_Row.Item("Column18") = ""
            dr_Row.Item("Column23") = ""
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dr_Row.Item("Column24") = Format(decCongeCDFTotal, GUI.fmtCurrency)
            'dr_Row.Item("Column25") = Format(decCongeUSDTotal, GUI.fmtCurrency)
            'decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
            'decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
            dr_Row.Item("Column24") = Format(decCongeCDFTotal, strfmtCurrency)
            dr_Row.Item("Column25") = Format(decCongeUSDTotal, strfmtCurrency)
            decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
            'Sohail (21 Aug 2015) -- End
            dsBrut.Tables(0).Rows.Add(dr_Row)

            dr_Row = dsBrut.Tables(0).NewRow
            dr_Row.Item("Column1") = mintEmployeeunkid
            dr_Row.Item("Column16") = "13th Check (prorata temporisé)"
            dr_Row.Item("Column17") = ""
            dr_Row.Item("Column18") = ""
            dr_Row.Item("Column23") = ""
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dr_Row.Item("Column24") = Format(dec13thCheckCDFTotal, GUI.fmtCurrency)
            'dr_Row.Item("Column25") = Format(dec13thCheckUSDTotal, GUI.fmtCurrency)
            'decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
            'decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
            dr_Row.Item("Column24") = Format(dec13thCheckCDFTotal, strfmtCurrency)
            dr_Row.Item("Column25") = Format(dec13thCheckUSDTotal, strfmtCurrency)
            decBrutCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decBrutUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
            'Sohail (21 Aug 2015) -- End
            dsBrut.Tables(0).Rows.Add(dr_Row)
            '-----------------------------------------------


            'Sohail (08 Jul 2016) -- Start
            'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
            'mdecBaseTaxable = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintBaseTaxableHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum() 'TODO :5: Base Taxable head
            'mdecBaseImposable = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintBaseImposableHeadId) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum() 'TODO :20: Base Imposable head
            'mdecNetPay = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintNetPayHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum() 'TODO :1: Net Pay head
            mdecBaseTaxable = (From p In rpt_Data.Tables("ArutiTable") Where (CInt(p.Item("Column14")) = mintBaseTaxableHeadID) Select CDec(p.Item("Column24"))).DefaultIfEmpty().Sum() 'TODO :5: Base Taxable head
            mdecBaseTaxable += (From p In dsNotice.Tables(0) Where (CInt(p.Item("Column14")) = mintBaseTaxableHeadID) Select CDec(p.Item("Column24"))).DefaultIfEmpty().Sum() 'TODO :5: Base Taxable head
            mdecBaseTaxable += decBonusCDFTotal 'TODO :5: Base Taxable head
            'Sohail (31 Jan 2018) -- Start
            'Internal issue : Input string was not in correct format on Termination Package report in 70.1. 
            'mdecBaseTaxable += CDec(dsLeave.Tables(0).Rows(0).Item("Column24")) 'TODO :5: Base Taxable head
            mdecBaseTaxable += CDec(If(String.IsNullOrEmpty(dsLeave.Tables(0).Rows(0)("Column24").ToString()), 0, Convert.ToDecimal(dsLeave.Tables(0).Rows(0)("Column24")))) 'TODO :5: Base Taxable head
            'Sohail (31 Jan 2018) -- End
            mdecBaseTaxable += decCongeCDFTotal
            mdecBaseTaxable += dec13ThCheck

            mdecNetPay = (From p In dsList.Tables(0) Where (CInt(p.Item("tranheadunkid")) = mintNetPayHeadID) Select CDec(p.Item("amount"))).DefaultIfEmpty().Sum() 'TODO :1: Net Pay head

            Dim mdecNetBaseImposable As Decimal = mdecBaseTaxable - mdecBaseImposable
            Dim mdecIPRBrut As Decimal = (mdecNetBaseImposable * 10) / 100
            Dim mdecReductionTax As Decimal = (mdecIPRBrut * 2) / 100
            Dim mdecIPRNet As Decimal = mdecIPRBrut - mdecReductionTax
            'Sohail (08 Jul 2016) -- End

            For Each strID As String In mstrDeductionIDs.Split(",")
                Dim intID As Integer = CInt(strID)

                dr_Row = dsDeduction.Tables(0).NewRow

                dr_Row.Item("Column1") = mintEmployeeunkid

                dr_Row.Item("Column17") = ""
                dr_Row.Item("Column18") = ""
                dr_Row.Item("Column23") = ""

                Dim lstDeduction As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = intID) Select (p)).ToList
                If lstDeduction.Count > 0 Then
                    dr_Row.Item("Column16") = lstDeduction(0).Item("trnheadname").ToString
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dr_Row.Item("Column24") = Format(CDec(lstDeduction(0).Item("amount")), GUI.fmtCurrency)
                    dr_Row.Item("Column24") = Format(CDec(lstDeduction(0).Item("amount")), strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                Else
                    dr_Row.Item("Column16") = ""
                    dr_Row.Item("Column24") = "0"
                End If

                If mdecEx_Rate <> 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, GUI.fmtCurrency)
                    dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                Else
                    dr_Row.Item("Column25") = ""
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dr_Row.Item("Column26") = Format(mdecBaseTaxable, GUI.fmtCurrency)
                'dr_Row.Item("Column27") = Format(mdecBaseImposable, GUI.fmtCurrency)
                'dr_Row.Item("Column30") = Format(mdecNetPay, GUI.fmtCurrency)
                dr_Row.Item("Column26") = Format(mdecBaseTaxable, strfmtCurrency)
                'Sohail (08 Jul 2016) -- Start
                'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
                'dr_Row.Item("Column27") = Format(mdecBaseImposable, strfmtCurrency)
                dr_Row.Item("Column27") = Format(mdecNetBaseImposable, strfmtCurrency)
                'Sohail (08 Jul 2016) -- End
                dr_Row.Item("Column30") = Format(mdecNetPay, strfmtCurrency)
                'Sohail (21 Aug 2015) -- End

                If mdecEx_Rate <> 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dr_Row.Item("Column28") = Format(CDec(IIf(dr_Row.Item("Column26").ToString.Trim = "", "0", dr_Row.Item("Column26").ToString)) * mdecEx_Rate, GUI.fmtCurrency)
                    'dr_Row.Item("Column29") = Format(CDec(IIf(dr_Row.Item("Column27").ToString.Trim = "", "0", dr_Row.Item("Column27").ToString)) * mdecEx_Rate, GUI.fmtCurrency)
                    'Sohail (08 Jul 2016) -- Start
                    'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
                    'dr_Row.Item("Column28") = Format(CDec(IIf(dr_Row.Item("Column26").ToString.Trim = "", "0", dr_Row.Item("Column26").ToString)) * mdecEx_Rate, strfmtCurrency)
                    'dr_Row.Item("Column29") = Format(CDec(IIf(dr_Row.Item("Column27").ToString.Trim = "", "0", dr_Row.Item("Column27").ToString)) * mdecEx_Rate, strfmtCurrency)
                    dr_Row.Item("Column28") = Format(mdecBaseTaxable * mdecEx_Rate, strfmtCurrency)
                    dr_Row.Item("Column29") = Format(mdecNetBaseImposable * mdecEx_Rate, strfmtCurrency)
                    'Sohail (08 Jul 2016) -- End
                    'Sohail (21 Aug 2015) -- End
                    mdecNetPayUSD = CDec(IIf(dr_Row.Item("Column30").ToString.Trim = "", "0", dr_Row.Item("Column30").ToString)) * mdecEx_Rate
                Else
                    dr_Row.Item("Column28") = ""
                    dr_Row.Item("Column29") = ""
                    mdecNetPayUSD = 0
                End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dr_Row.Item("Column31") = Format(mdecNetPayUSD, GUI.fmtCurrency)

                'decDeductionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
                'decDeductionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))
                dr_Row.Item("Column31") = Format(mdecNetPayUSD, strfmtCurrency)

                decDeductionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
                decDeductionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))
                'Sohail (21 Aug 2015) -- End

                dsDeduction.Tables(0).Rows.Add(dr_Row)

            Next

            'Sohail (08 Jul 2016) -- Start
            'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
            dr_Row = dsDeduction.Tables(0).NewRow

            dr_Row.Item("Column1") = mintEmployeeunkid

            dr_Row.Item("Column17") = ""
            dr_Row.Item("Column18") = ""
            dr_Row.Item("Column23") = ""

            dr_Row.Item("Column16") = Language.getMessage(mstrModuleName, 67, "IPR Brut")
            dr_Row.Item("Column24") = Format(mdecIPRBrut, strfmtCurrency)

            If mdecEx_Rate <> 0 Then
                dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
            Else
                dr_Row.Item("Column25") = ""
            End If

            dr_Row.Item("Column26") = Format(mdecBaseTaxable, strfmtCurrency)
            dr_Row.Item("Column27") = Format(mdecNetBaseImposable, strfmtCurrency)
            dr_Row.Item("Column30") = Format(mdecNetPay, strfmtCurrency)

            If mdecEx_Rate <> 0 Then
                dr_Row.Item("Column28") = Format(mdecBaseTaxable * mdecEx_Rate, strfmtCurrency)
                dr_Row.Item("Column29") = Format(mdecNetBaseImposable * mdecEx_Rate, strfmtCurrency)
                mdecNetPayUSD = CDec(IIf(dr_Row.Item("Column30").ToString.Trim = "", "0", dr_Row.Item("Column30").ToString)) * mdecEx_Rate
            Else
                dr_Row.Item("Column28") = ""
                dr_Row.Item("Column29") = ""
                mdecNetPayUSD = 0
            End If

            dr_Row.Item("Column31") = Format(mdecNetPayUSD, strfmtCurrency)

            decDeductionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decDeductionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))

            dsDeduction.Tables(0).Rows.Add(dr_Row)

            dr_Row = dsDeduction.Tables(0).NewRow

            dr_Row.Item("Column1") = mintEmployeeunkid

            dr_Row.Item("Column17") = ""
            dr_Row.Item("Column18") = ""
            dr_Row.Item("Column23") = ""

            dr_Row.Item("Column16") = Language.getMessage(mstrModuleName, 69, "No dependants pour reduction Tax")
            dr_Row.Item("Column24") = Format(mdecReductionTax, strfmtCurrency)

            If mdecEx_Rate <> 0 Then
                dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
            Else
                dr_Row.Item("Column25") = ""
            End If

            dr_Row.Item("Column26") = Format(mdecBaseTaxable, strfmtCurrency)
            dr_Row.Item("Column27") = Format(mdecNetBaseImposable, strfmtCurrency)
            dr_Row.Item("Column30") = Format(mdecNetPay, strfmtCurrency)

            If mdecEx_Rate <> 0 Then
                dr_Row.Item("Column28") = Format(mdecBaseTaxable * mdecEx_Rate, strfmtCurrency)
                dr_Row.Item("Column29") = Format(mdecNetBaseImposable * mdecEx_Rate, strfmtCurrency)
                mdecNetPayUSD = CDec(IIf(dr_Row.Item("Column30").ToString.Trim = "", "0", dr_Row.Item("Column30").ToString)) * mdecEx_Rate
            Else
                dr_Row.Item("Column28") = ""
                dr_Row.Item("Column29") = ""
                mdecNetPayUSD = 0
            End If

            dr_Row.Item("Column31") = Format(mdecNetPayUSD, strfmtCurrency)

            decDeductionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decDeductionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))

            dsDeduction.Tables(0).Rows.Add(dr_Row)

            dr_Row = dsDeduction.Tables(0).NewRow

            dr_Row.Item("Column1") = mintEmployeeunkid

            dr_Row.Item("Column17") = ""
            dr_Row.Item("Column18") = ""
            dr_Row.Item("Column23") = ""

            dr_Row.Item("Column16") = Language.getMessage(mstrModuleName, 68, "IPR Net")
            dr_Row.Item("Column24") = Format(mdecIPRNet, strfmtCurrency)

            If mdecEx_Rate <> 0 Then
                dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
            Else
                dr_Row.Item("Column25") = ""
            End If

            dr_Row.Item("Column26") = Format(mdecBaseTaxable, strfmtCurrency)
            dr_Row.Item("Column27") = Format(mdecNetBaseImposable, strfmtCurrency)
            dr_Row.Item("Column30") = Format(mdecNetPay, strfmtCurrency)

            If mdecEx_Rate <> 0 Then
                dr_Row.Item("Column28") = Format(mdecBaseTaxable * mdecEx_Rate, strfmtCurrency)
                dr_Row.Item("Column29") = Format(mdecNetBaseImposable * mdecEx_Rate, strfmtCurrency)
                mdecNetPayUSD = CDec(IIf(dr_Row.Item("Column30").ToString.Trim = "", "0", dr_Row.Item("Column30").ToString)) * mdecEx_Rate
            Else
                dr_Row.Item("Column28") = ""
                dr_Row.Item("Column29") = ""
                mdecNetPayUSD = 0
            End If

            dr_Row.Item("Column31") = Format(mdecNetPayUSD, strfmtCurrency)

            decDeductionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
            decDeductionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))

            dsDeduction.Tables(0).Rows.Add(dr_Row)
            '----------------------------------------

            If mintLoanRepaymentHeadID > 0 Then
                Dim lstLoanRepayment As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintLoanRepaymentHeadID) Select (p)).ToList
                If lstLoanRepayment.Count > 0 Then
                    decLoanRepayment = CDec(lstLoanRepayment(0).Item("amount"))
                End If

                If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso decLoanRepayment = 0 Then
                    lstLoanRepayment = (From p In dsCurrPeriod.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintLoanRepaymentHeadID) Select (p)).ToList
                    If lstLoanRepayment.Count > 0 Then
                        decLoanRepayment = CDec(lstLoanRepayment(0).Item("amount"))
                    End If
                End If

                dr_Row = dsDeduction.Tables(0).NewRow

                dr_Row.Item("Column1") = mintEmployeeunkid

                dr_Row.Item("Column17") = ""
                dr_Row.Item("Column18") = ""
                dr_Row.Item("Column23") = ""

                dr_Row.Item("Column16") = mstrLoanRepaymentHeadName
                dr_Row.Item("Column24") = Format(decLoanRepayment, strfmtCurrency)

                If mdecEx_Rate <> 0 Then
                    dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
                Else
                    dr_Row.Item("Column25") = ""
                End If

                dr_Row.Item("Column26") = Format(mdecBaseTaxable, strfmtCurrency)
                dr_Row.Item("Column27") = Format(mdecNetBaseImposable, strfmtCurrency)
                dr_Row.Item("Column30") = Format(mdecNetPay, strfmtCurrency)

                If mdecEx_Rate <> 0 Then
                    dr_Row.Item("Column28") = Format(mdecBaseTaxable * mdecEx_Rate, strfmtCurrency)
                    dr_Row.Item("Column29") = Format(mdecNetBaseImposable * mdecEx_Rate, strfmtCurrency)
                    mdecNetPayUSD = CDec(IIf(dr_Row.Item("Column30").ToString.Trim = "", "0", dr_Row.Item("Column30").ToString)) * mdecEx_Rate
                Else
                    dr_Row.Item("Column28") = ""
                    dr_Row.Item("Column29") = ""
                    mdecNetPayUSD = 0
                End If

                dr_Row.Item("Column31") = Format(mdecNetPayUSD, strfmtCurrency)

                decDeductionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
                decDeductionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))

                dsDeduction.Tables(0).Rows.Add(dr_Row)
            End If
            '----------------------------------------

            If mintAdvanceRepaymentHeadID > 0 Then
                Dim lstAdvanceRepayment As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintAdvanceRepaymentHeadID) Select (p)).ToList
                If lstAdvanceRepayment.Count > 0 Then
                    decAdvanceRepayment = CDec(lstAdvanceRepayment(0).Item("amount"))
                End If

                If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso decAdvanceRepayment = 0 Then
                    lstAdvanceRepayment = (From p In dsCurrPeriod.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintAdvanceRepaymentHeadID) Select (p)).ToList
                    If lstAdvanceRepayment.Count > 0 Then
                        decAdvanceRepayment = CDec(lstAdvanceRepayment(0).Item("amount"))
                    End If
                End If

                dr_Row = dsDeduction.Tables(0).NewRow

                dr_Row.Item("Column1") = mintEmployeeunkid

                dr_Row.Item("Column17") = ""
                dr_Row.Item("Column18") = ""
                dr_Row.Item("Column23") = ""

                dr_Row.Item("Column16") = mstrAdvanceRepaymentHeadName
                dr_Row.Item("Column24") = Format(decAdvanceRepayment, strfmtCurrency)

                If mdecEx_Rate <> 0 Then
                    dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
                Else
                    dr_Row.Item("Column25") = ""
                End If

                dr_Row.Item("Column26") = Format(mdecBaseTaxable, strfmtCurrency)
                dr_Row.Item("Column27") = Format(mdecNetBaseImposable, strfmtCurrency)
                dr_Row.Item("Column30") = Format(mdecNetPay, strfmtCurrency)

                If mdecEx_Rate <> 0 Then
                    dr_Row.Item("Column28") = Format(mdecBaseTaxable * mdecEx_Rate, strfmtCurrency)
                    dr_Row.Item("Column29") = Format(mdecNetBaseImposable * mdecEx_Rate, strfmtCurrency)
                    mdecNetPayUSD = CDec(IIf(dr_Row.Item("Column30").ToString.Trim = "", "0", dr_Row.Item("Column30").ToString)) * mdecEx_Rate
                Else
                    dr_Row.Item("Column28") = ""
                    dr_Row.Item("Column29") = ""
                    mdecNetPayUSD = 0
                End If

                dr_Row.Item("Column31") = Format(mdecNetPayUSD, strfmtCurrency)

                decDeductionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
                decDeductionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))

                dsDeduction.Tables(0).Rows.Add(dr_Row)
            End If
            '----------------------------------------

            If mintOtherDeductionsHeadID > 0 Then
                Dim lstOtherDeductions As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintOtherDeductionsHeadID) Select (p)).ToList
                If lstOtherDeductions.Count > 0 Then
                    decOtherDeductions = CDec(lstOtherDeductions(0).Item("amount"))
                End If

                If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso decOtherDeductions = 0 Then
                    lstOtherDeductions = (From p In dsCurrPeriod.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintOtherDeductionsHeadID) Select (p)).ToList
                    If lstOtherDeductions.Count > 0 Then
                        decOtherDeductions = CDec(lstOtherDeductions(0).Item("amount"))
                    End If
                End If

                dr_Row = dsDeduction.Tables(0).NewRow

                dr_Row.Item("Column1") = mintEmployeeunkid

                dr_Row.Item("Column17") = ""
                dr_Row.Item("Column18") = ""
                dr_Row.Item("Column23") = ""

                dr_Row.Item("Column16") = mstrOtherDeductionsHeadName
                dr_Row.Item("Column24") = Format(decOtherDeductions, strfmtCurrency)

                If mdecEx_Rate <> 0 Then
                    dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
                Else
                    dr_Row.Item("Column25") = ""
                End If

                dr_Row.Item("Column26") = Format(mdecBaseTaxable, strfmtCurrency)
                dr_Row.Item("Column27") = Format(mdecNetBaseImposable, strfmtCurrency)
                dr_Row.Item("Column30") = Format(mdecNetPay, strfmtCurrency)

                If mdecEx_Rate <> 0 Then
                    dr_Row.Item("Column28") = Format(mdecBaseTaxable * mdecEx_Rate, strfmtCurrency)
                    dr_Row.Item("Column29") = Format(mdecNetBaseImposable * mdecEx_Rate, strfmtCurrency)
                    mdecNetPayUSD = CDec(IIf(dr_Row.Item("Column30").ToString.Trim = "", "0", dr_Row.Item("Column30").ToString)) * mdecEx_Rate
                Else
                    dr_Row.Item("Column28") = ""
                    dr_Row.Item("Column29") = ""
                    mdecNetPayUSD = 0
                End If

                dr_Row.Item("Column31") = Format(mdecNetPayUSD, strfmtCurrency)

                decDeductionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
                decDeductionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))

                dsDeduction.Tables(0).Rows.Add(dr_Row)
            End If
            '-------------------

            If mintIPRNetHeadID > 0 Then
                Dim lstIPRNet As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintIPRNetHeadID) Select (p)).ToList
                If lstIPRNet.Count > 0 Then
                    decIPRNet = CDec(lstIPRNet(0).Item("amount"))
                End If

                If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso decIPRNet = 0 Then
                    lstIPRNet = (From p In dsCurrPeriod.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintIPRNetHeadID) Select (p)).ToList
                    If lstIPRNet.Count > 0 Then
                        decIPRNet = CDec(lstIPRNet(0).Item("amount"))
                    End If
                End If

                dr_Row = dsContribution.Tables(0).NewRow

                dr_Row.Item("Column1") = mintEmployeeunkid

                dr_Row.Item("Column17") = ""
                dr_Row.Item("Column18") = ""
                dr_Row.Item("Column23") = ""

                dr_Row.Item("Column16") = mstrIPRNetHeadName
                dr_Row.Item("Column24") = Format(decIPRNet, strfmtCurrency)

                If mdecEx_Rate <> 0 Then
                    dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
                Else
                    dr_Row.Item("Column25") = ""
                End If

                decContributionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
                decContributionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))

                dr_Row.Item("Column26") = Format(decContributionCDFTotal, strfmtCurrency)
                dr_Row.Item("Column27") = Format(decContributionUSDTotal, strfmtCurrency)

                dr_Row.Item("Column28") = Format(mdecNetPay + decContributionCDFTotal, strfmtCurrency)
                dr_Row.Item("Column29") = Format(mdecNetPayUSD + decContributionUSDTotal, strfmtCurrency)

                dsContribution.Tables(0).Rows.Add(dr_Row)
            End If
            '--------------------

            If mintINSS35HeadID > 0 Then
                Dim lstINSS35 As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintINSS35HeadID) Select (p)).ToList
                If lstINSS35.Count > 0 Then
                    decINSS35 = CDec(lstINSS35(0).Item("amount"))
                End If

                If mintPeriodUnkId <> mintProcessPeriodUnkId AndAlso decINSS35 = 0 Then
                    lstINSS35 = (From p In dsCurrPeriod.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = mintINSS35HeadID) Select (p)).ToList
                    If lstINSS35.Count > 0 Then
                        decINSS35 = CDec(lstINSS35(0).Item("amount"))
                    End If
                End If

                dr_Row = dsContribution.Tables(0).NewRow

                dr_Row.Item("Column1") = mintEmployeeunkid

                dr_Row.Item("Column17") = ""
                dr_Row.Item("Column18") = ""
                dr_Row.Item("Column23") = ""

                dr_Row.Item("Column16") = mstrINSS35HeadName
                dr_Row.Item("Column24") = Format(decINSS35, strfmtCurrency)

                If mdecEx_Rate <> 0 Then
                    dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
                Else
                    dr_Row.Item("Column25") = ""
                End If

                decContributionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
                decContributionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))

                dr_Row.Item("Column26") = Format(decContributionCDFTotal, strfmtCurrency)
                dr_Row.Item("Column27") = Format(decContributionUSDTotal, strfmtCurrency)

                dr_Row.Item("Column28") = Format(mdecNetPay + decContributionCDFTotal, strfmtCurrency)
                dr_Row.Item("Column29") = Format(mdecNetPayUSD + decContributionUSDTotal, strfmtCurrency)

                dsContribution.Tables(0).Rows.Add(dr_Row)
            End If
            'Sohail (08 Jul 2016) -- End

            For Each strID As String In mstrContributionIDs.Split(",")
                Dim intID As Integer = CInt(strID)

                dr_Row = dsContribution.Tables(0).NewRow

                dr_Row.Item("Column1") = mintEmployeeunkid

                dr_Row.Item("Column17") = ""
                dr_Row.Item("Column18") = ""
                dr_Row.Item("Column23") = ""

                Dim lstContribution As List(Of DataRow) = (From p In dsList.Tables("DataTable") Where (CInt(p.Item("tranheadunkid")) = intID) Select (p)).ToList
                If lstContribution.Count > 0 Then
                    dr_Row.Item("Column16") = lstContribution(0).Item("trnheadname").ToString
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dr_Row.Item("Column24") = Format(CDec(lstContribution(0).Item("amount")), GUI.fmtCurrency)
                    dr_Row.Item("Column24") = Format(CDec(lstContribution(0).Item("amount")), strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                Else
                    dr_Row.Item("Column16") = ""
                    dr_Row.Item("Column24") = "0"
                End If

                If mdecEx_Rate <> 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, GUI.fmtCurrency)
                    dr_Row.Item("Column25") = Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", "0", dr_Row.Item("Column24").ToString)) * mdecEx_Rate, strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                Else
                    dr_Row.Item("Column25") = ""
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'decContributionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), GUI.fmtCurrency))
                'decContributionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), GUI.fmtCurrency))

                'dr_Row.Item("Column26") = Format(decContributionCDFTotal, GUI.fmtCurrency)
                'dr_Row.Item("Column27") = Format(decContributionUSDTotal, GUI.fmtCurrency)

                'dr_Row.Item("Column28") = Format(mdecNetPay + decContributionCDFTotal, GUI.fmtCurrency)
                'dr_Row.Item("Column29") = Format(mdecNetPayUSD + decContributionUSDTotal, GUI.fmtCurrency)
                decContributionCDFTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column24").ToString.Trim = "", 0, dr_Row.Item("Column24"))), strfmtCurrency))
                decContributionUSDTotal += CDec(Format(CDec(IIf(dr_Row.Item("Column25").ToString.Trim = "", 0, dr_Row.Item("Column25"))), strfmtCurrency))

                dr_Row.Item("Column26") = Format(decContributionCDFTotal, strfmtCurrency)
                dr_Row.Item("Column27") = Format(decContributionUSDTotal, strfmtCurrency)

                dr_Row.Item("Column28") = Format(mdecNetPay + decContributionCDFTotal, strfmtCurrency)
                dr_Row.Item("Column29") = Format(mdecNetPayUSD + decContributionUSDTotal, strfmtCurrency)
                'Sohail (21 Aug 2015) -- End

                dsContribution.Tables(0).Rows.Add(dr_Row)

            Next

            Dim objEmpBank As New clsEmployeeBanks
            Dim strBranch As String = ""
            Dim strAccountNo As String = ""
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Dim dsBank As DataSet = objEmpBank.GetList("List", mintEmployeeunkid, mdtPeriodEndDate)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsBank As DataSet = objEmpBank.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , mintEmployeeunkid, mdtPeriodEndDate)
            Dim dsBank As DataSet = objEmpBank.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtPeriodStartDate, mdtPeriodEndDate, xUserModeSetting, True, mblnIncludeInactiveEmp, "List", True, , mintEmployeeunkid, mdtPeriodEndDate)
            'Shani(24-Aug-2015) -- End

            'Sohail (21 Aug 2015) -- End
            For Each dsRow As DataRow In dsBank.Tables("List").Rows
                strBranch &= ", " & dsRow.Item("BranchName").ToString
                strAccountNo &= ", " & dsRow.Item("accountno").ToString
            Next
            objEmpBank = Nothing
            If strBranch.Trim <> "" Then strBranch = strBranch.Substring(1)
            If strAccountNo.Trim <> "" Then strAccountNo = strAccountNo.Substring(1)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then

                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                'rpt_Row.Item("Column1") = rpt_Data.Tables("ArutiTable")(0).Item("employeeunkid")
                'rpt_Row.Item("Column2") = rpt_Data.Tables("ArutiTable")(0).Item("employeecode")
                'rpt_Row.Item("Column3") = rpt_Data.Tables("ArutiTable")(0).Item("employeename")
                'rpt_Row.Item("Column4") = rpt_Data.Tables("ArutiTable")(0).Item("departmentunkid")
                'rpt_Row.Item("Column5") = rpt_Data.Tables("ArutiTable")(0).Item("departmentname")
                'rpt_Row.Item("Column6") = rpt_Data.Tables("ArutiTable")(0).Item("jobunkid")
                'rpt_Row.Item("Column7") = rpt_Data.Tables("ArutiTable")(0).Item("job_name")
                'rpt_Row.Item("Column8") = rpt_Data.Tables("ArutiTable")(0).Item("employmenttype")
                'rpt_Row.Item("Column9") = rpt_Data.Tables("ArutiTable")(0).Item("appointeddate")
                'rpt_Row.Item("Column10") = rpt_Data.Tables("ArutiTable")(0).Item("payperiodunkid")
                'rpt_Row.Item("Column11") = rpt_Data.Tables("ArutiTable")(0).Item("period_name")
                'rpt_Row.Item("Column12") = rpt_Data.Tables("ArutiTable")(0).Item("start_date")
                'rpt_Row.Item("Column13") = rpt_Data.Tables("ArutiTable")(0).Item("end_date")
                'rpt_Row.Item("Column14") = rpt_Data.Tables("ArutiTable")(0).Item("tranheadunkid")
                'rpt_Row.Item("Column15") = rpt_Data.Tables("ArutiTable")(0).Item("trnheadcode")
                'rpt_Row.Item("Column16") = rpt_Data.Tables("ArutiTable")(0).Item("trnheadname")
                'rpt_Row.Item("Column17") = rpt_Data.Tables("ArutiTable")(0).Item("amount")

                'If mdicMembrshipData.ContainsKey(CInt(rpt_Data.Tables("ArutiTable")(0).Item("employeeunkid"))) = True Then
                '    rpt_Row.Item("Column18") = mdicMembrshipData.Item(CInt(rpt_Data.Tables("ArutiTable")(0).Item("employeeunkid")))
                'Else
                '    rpt_Row.Item("Column18") = ""
                'End If


                'rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            End If

            'For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

            '    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

            '    rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
            '    rpt_Row.Item("Column2") = dtRow.Item("employeecode")
            '    rpt_Row.Item("Column3") = dtRow.Item("employeename")
            '    rpt_Row.Item("Column4") = dtRow.Item("departmentunkid")
            '    rpt_Row.Item("Column5") = dtRow.Item("departmentname")
            '    rpt_Row.Item("Column6") = dtRow.Item("jobunkid")
            '    rpt_Row.Item("Column7") = dtRow.Item("job_name")
            '    rpt_Row.Item("Column8") = dtRow.Item("employmenttype")
            '    rpt_Row.Item("Column9") = dtRow.Item("appointeddate")
            '    rpt_Row.Item("Column10") = dtRow.Item("payperiodunkid")
            '    rpt_Row.Item("Column11") = dtRow.Item("period_name")
            '    rpt_Row.Item("Column12") = dtRow.Item("start_date")
            '    rpt_Row.Item("Column13") = dtRow.Item("end_date")
            '    rpt_Row.Item("Column14") = dtRow.Item("tranheadunkid")
            '    rpt_Row.Item("Column15") = dtRow.Item("trnheadcode")
            '    rpt_Row.Item("Column16") = dtRow.Item("trnheadname")
            '    rpt_Row.Item("Column17") = dtRow.Item("amount")

            '    If mdicMembrshipData.ContainsKey(CInt(dtRow.Item("employeeunkid"))) = True Then
            '        rpt_Row.Item("Column18") = mdicMembrshipData.Item(CInt(dtRow.Item("employeeunkid")))
            '    Else
            '        rpt_Row.Item("Column18") = ""
            '    End If


            '    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            'Next

            'For Each dtRow As DataRow In dsSummary.Tables("DataTable").Rows
            '    rpt_Row = ds2.Tables("ArutiTable").NewRow

            '    rpt_Row.Item("Column1") = dtRow.Item("periodunkid")
            '    rpt_Row.Item("Column2") = dtRow.Item("period_name")
            '    rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("GrossSalary")), GUI.fmtCurrency)
            '    rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency)
            '    rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency)
            '    rpt_Row.Item("Column6") = dtRow.Item("HeadCount")
            '    rpt_Row.Item("Column10") = dtRow.Item("GName")

            '    ds2.Tables("ArutiTable").Rows.Add(rpt_Row)
            'Next

            'For Each dtRow As DataRow In dsFooter.Tables("DataTable").Rows
            '    rpt_Row = ds3.Tables("ArutiTable").NewRow

            '    rpt_Row.Item("Column1") = dtRow.Item("periodunkid")
            '    rpt_Row.Item("Column2") = dtRow.Item("period_name")
            '    rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("GrossSalary")), GUI.fmtCurrency)
            '    rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency)
            '    rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("NetSalary")), GUI.fmtCurrency)
            '    rpt_Row.Item("Column6") = dtRow.Item("HeadCount")
            '    rpt_Row.Item("Column10") = ""

            '    ds3.Tables("ArutiTable").Rows.Add(rpt_Row)

            '    If CInt(dtRow.Item("periodunkid")) = mintFromPeriodUnkId Then
            '        mintFirstPeriodHeadCount = CInt(dtRow.Item("HeadCount"))
            '    End If
            'Next

            objRpt = New ArutiReport.Designer.rptTerminationPackageReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            'If ConfigParameter._Object._IsShowPreparedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 15, "Prepared By :"))
            '    Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            'End If

            'If ConfigParameter._Object._IsShowCheckedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 16, "Checked By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            'End If

            'If ConfigParameter._Object._IsShowApprovedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 17, "Approved By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            'End If

            'If ConfigParameter._Object._IsShowReceivedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 18, "Received By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            'End If

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("Notice").SetDataSource(dsNotice)
            objRpt.Subreports("Bonus").SetDataSource(dsBonus)
            objRpt.Subreports("Leave").SetDataSource(dsLeave)
            objRpt.Subreports("CongePreavis").SetDataSource(dsCongePreavis)
            objRpt.Subreports("13thCheck").SetDataSource(ds13thCheck)
            objRpt.Subreports("Brut").SetDataSource(dsBrut)
            objRpt.Subreports("Deduction").SetDataSource(dsDeduction)
            objRpt.Subreports("Contribution").SetDataSource(dsContribution)


            Call ReportFunction.TextChange(objRpt, "txtSurname", Language.getMessage(mstrModuleName, 12, "Surname"))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 13, "Job Title"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeNumber", Language.getMessage(mstrModuleName, 14, "Employee Number"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 15, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtAnnualSalary", Language.getMessage(mstrModuleName, 16, "Annual Gross Salary"))
            Call ReportFunction.TextChange(objRpt, "txtMonthlySalary", Language.getMessage(mstrModuleName, 17, "Monthly Gross Salary"))
            Call ReportFunction.TextChange(objRpt, "txtNoChild", Language.getMessage(mstrModuleName, 18, "Number of Child"))
            Call ReportFunction.TextChange(objRpt, "txtNodependant", Language.getMessage(mstrModuleName, 19, "Number of Dependent"))
            Call ReportFunction.TextChange(objRpt, "txtSocialSecurityNo", Language.getMessage(mstrModuleName, 20, "Social Security Number"))
            Call ReportFunction.TextChange(objRpt, "txtTypeOfContract", Language.getMessage(mstrModuleName, 21, "Type of Contract"))
            Call ReportFunction.TextChange(objRpt, "txtHiringDate", Language.getMessage(mstrModuleName, 22, "Hiring Date"))
            Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 23, "Period"))
            Call ReportFunction.TextChange(objRpt, "txtWorkingDays", Language.getMessage(mstrModuleName, 24, "Number of working days"))
            Call ReportFunction.TextChange(objRpt, "txtNotivr", Language.getMessage(mstrModuleName, 25, "Notice"))
            Call ReportFunction.TextChange(objRpt, "txtNoticeDate", Language.getMessage(mstrModuleName, 26, "Notification of the Notice"))
            Call ReportFunction.TextChange(objRpt, "txtJourstop", Language.getMessage(mstrModuleName, 27, "Jours"))
            Call ReportFunction.TextChange(objRpt, "txtPrestation1", Language.getMessage(mstrModuleName, 28, "1. Prestation"))
            Call ReportFunction.TextChange(objRpt, "txtMontant26jours", Language.getMessage(mstrModuleName, 29, "Montant Mensuel de 26 Jours"))
            Call ReportFunction.TextChange(objRpt, "txtJours", Language.getMessage(mstrModuleName, 27, "Jours"))
            Call ReportFunction.TextChange(objRpt, "txtRate", Language.getMessage(mstrModuleName, 30, "Rate"))
            Call ReportFunction.TextChange(objRpt, "txtTotalCDF", Language.getMessage(mstrModuleName, 31, "Total CDF"))
            Call ReportFunction.TextChange(objRpt, "txtTotalUSD", Language.getMessage(mstrModuleName, 32, "Total USD"))
            Call ReportFunction.TextChange(objRpt, "txtSousTotal1", Language.getMessage(mstrModuleName, 33, "Sous-Total (1)"))
            Call ReportFunction.TextChange(objRpt, "txtApprover", Language.getMessage(mstrModuleName, 55, "Approuvé Par : "))
            Call ReportFunction.TextChange(objRpt, "txtApprover1", Language.getMessage(mstrModuleName, 55, "Approuvé Par : "))
            Call ReportFunction.TextChange(objRpt, "txtApprover2", Language.getMessage(mstrModuleName, 55, "Approuvé Par : "))
            Call ReportFunction.TextChange(objRpt, "txtHRDirector", Language.getMessage(mstrModuleName, 56, "Directeur des Ressources Humaines"))
            Call ReportFunction.TextChange(objRpt, "txtFinanceDirector", Language.getMessage(mstrModuleName, 57, "Directeur Financier"))
            Call ReportFunction.TextChange(objRpt, "txtNationalDirector", Language.getMessage(mstrModuleName, 58, "Directeur National"))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt, "txtCDFTotal", Format(decPrestationCDFTotal, GUI.fmtCurrency))
            'Call ReportFunction.TextChange(objRpt, "txtUSDTotal", Format(decPrestationUSDTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtCDFTotal", Format(decPrestationCDFTotal, strfmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtUSDTotal", Format(decPrestationUSDTotal, strfmtCurrency))
            'Sohail (21 Aug 2015) -- End


            Call ReportFunction.TextChange(objRpt.Subreports("Notice"), "txtNotice", Language.getMessage(mstrModuleName, 34, "2. Notice"))
            Call ReportFunction.TextChange(objRpt.Subreports("Notice"), "txtMontant26jours", Language.getMessage(mstrModuleName, 29, "Montant Mensuel de 26 Jours"))
            Call ReportFunction.TextChange(objRpt.Subreports("Notice"), "txtJours", Language.getMessage(mstrModuleName, 27, "Jours"))
            Call ReportFunction.TextChange(objRpt.Subreports("Notice"), "txtRate", Language.getMessage(mstrModuleName, 30, "Rate"))
            Call ReportFunction.TextChange(objRpt.Subreports("Notice"), "txtTotalCDF", Language.getMessage(mstrModuleName, 31, "Total CDF"))
            Call ReportFunction.TextChange(objRpt.Subreports("Notice"), "txtTotalUSD", Language.getMessage(mstrModuleName, 32, "Total USD"))
            Call ReportFunction.TextChange(objRpt.Subreports("Notice"), "txtSousTotal2", Language.getMessage(mstrModuleName, 35, "Sous-Total (2)"))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt.Subreports("Notice"), "txtCDFTotal", Format(decNoticeCDFTotal, GUI.fmtCurrency))
            'Call ReportFunction.TextChange(objRpt.Subreports("Notice"), "txtUSDTotal", Format(decNoticeUSDTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("Notice"), "txtCDFTotal", Format(decNoticeCDFTotal, strfmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("Notice"), "txtUSDTotal", Format(decNoticeUSDTotal, strfmtCurrency))
            'Sohail (21 Aug 2015) -- End

            Call ReportFunction.TextChange(objRpt.Subreports("Bonus"), "txtBonus", Language.getMessage(mstrModuleName, 36, "3. Bonus"))
            Call ReportFunction.TextChange(objRpt.Subreports("Bonus"), "txtMontant26jours", Language.getMessage(mstrModuleName, 29, "Montant Mensuel de 26 Jours"))
            Call ReportFunction.TextChange(objRpt.Subreports("Bonus"), "txtJours", Language.getMessage(mstrModuleName, 27, "Jours"))
            Call ReportFunction.TextChange(objRpt.Subreports("Bonus"), "txtRate", Language.getMessage(mstrModuleName, 30, "Rate"))
            Call ReportFunction.TextChange(objRpt.Subreports("Bonus"), "txtTotalCDF", Language.getMessage(mstrModuleName, 31, "Total CDF"))
            Call ReportFunction.TextChange(objRpt.Subreports("Bonus"), "txtTotalUSD", Language.getMessage(mstrModuleName, 32, "Total USD"))
            Call ReportFunction.TextChange(objRpt.Subreports("Bonus"), "txtSousTotal3", Language.getMessage(mstrModuleName, 37, "Sous-Total (3)"))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt.Subreports("Bonus"), "txtCDFTotal", Format(decBonusCDFTotal, GUI.fmtCurrency))
            'Call ReportFunction.TextChange(objRpt.Subreports("Bonus"), "txtUSDTotal", Format(decBonusUSDTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("Bonus"), "txtCDFTotal", Format(decBonusCDFTotal, strfmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("Bonus"), "txtUSDTotal", Format(decBonusUSDTotal, strfmtCurrency))
            'Sohail (21 Aug 2015) -- End

            Call ReportFunction.TextChange(objRpt.Subreports("Leave"), "txtLeavenotTaken", Language.getMessage(mstrModuleName, 38, "4. Leave non Taken"))
            Call ReportFunction.TextChange(objRpt.Subreports("Leave"), "txtMontant26jours", Language.getMessage(mstrModuleName, 29, "Montant Mensuel de 26 Jours"))
            Call ReportFunction.TextChange(objRpt.Subreports("Leave"), "txtJours", Language.getMessage(mstrModuleName, 27, "Jours"))
            Call ReportFunction.TextChange(objRpt.Subreports("Leave"), "txtRate", Language.getMessage(mstrModuleName, 30, "Rate"))
            Call ReportFunction.TextChange(objRpt.Subreports("Leave"), "txtTotalCDF", Language.getMessage(mstrModuleName, 31, "Total CDF"))
            Call ReportFunction.TextChange(objRpt.Subreports("Leave"), "txtTotalUSD", Language.getMessage(mstrModuleName, 32, "Total USD"))
            Call ReportFunction.TextChange(objRpt.Subreports("Leave"), "txtSousTotal4", Language.getMessage(mstrModuleName, 39, "Sous-Total (4)"))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt.Subreports("Leave"), "txtCDFTotal", Format(decLeaveCDFTotal, GUI.fmtCurrency))
            'Call ReportFunction.TextChange(objRpt.Subreports("Leave"), "txtUSDTotal", Format(decLeaveUSDTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("Leave"), "txtCDFTotal", Format(decLeaveCDFTotal, strfmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("Leave"), "txtUSDTotal", Format(decLeaveUSDTotal, strfmtCurrency))
            'Sohail (21 Aug 2015) -- End

            Call ReportFunction.TextChange(objRpt.Subreports("CongePreavis"), "txtCongePreavis", Language.getMessage(mstrModuleName, 40, "5. Conge Préavis"))
            Call ReportFunction.TextChange(objRpt.Subreports("CongePreavis"), "txtMontant26jours", Language.getMessage(mstrModuleName, 29, "Montant Mensuel de 26 Jours"))
            Call ReportFunction.TextChange(objRpt.Subreports("CongePreavis"), "txtJours", Language.getMessage(mstrModuleName, 27, "Jours"))
            Call ReportFunction.TextChange(objRpt.Subreports("CongePreavis"), "txtRate", Language.getMessage(mstrModuleName, 30, "Rate"))
            Call ReportFunction.TextChange(objRpt.Subreports("CongePreavis"), "txtTotalCDF", Language.getMessage(mstrModuleName, 31, "Total CDF"))
            Call ReportFunction.TextChange(objRpt.Subreports("CongePreavis"), "txtTotalUSD", Language.getMessage(mstrModuleName, 32, "Total USD"))
            Call ReportFunction.TextChange(objRpt.Subreports("CongePreavis"), "txtSousTotal5", Language.getMessage(mstrModuleName, 41, "Sous-Total (5)"))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt.Subreports("CongePreavis"), "txtCDFTotal", Format(decCongeCDFTotal, GUI.fmtCurrency))
            'Call ReportFunction.TextChange(objRpt.Subreports("CongePreavis"), "txtUSDTotal", Format(decCongeUSDTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("CongePreavis"), "txtCDFTotal", Format(decCongeCDFTotal, strfmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("CongePreavis"), "txtUSDTotal", Format(decCongeUSDTotal, strfmtCurrency))
            'Sohail (21 Aug 2015) -- End

            Call ReportFunction.TextChange(objRpt.Subreports("13thCheck"), "txt13thcheck", Language.getMessage(mstrModuleName, 42, "6. 13th Check (prorata temporis)"))
            Call ReportFunction.TextChange(objRpt.Subreports("13thCheck"), "txtMontant26jours", Language.getMessage(mstrModuleName, 29, "Montant Mensuel de 26 Jours"))
            Call ReportFunction.TextChange(objRpt.Subreports("13thCheck"), "txtJours", Language.getMessage(mstrModuleName, 27, "Jours"))
            Call ReportFunction.TextChange(objRpt.Subreports("13thCheck"), "txtRate", Language.getMessage(mstrModuleName, 30, "Rate"))
            Call ReportFunction.TextChange(objRpt.Subreports("13thCheck"), "txtTotalCDF", Language.getMessage(mstrModuleName, 31, "Total CDF"))
            Call ReportFunction.TextChange(objRpt.Subreports("13thCheck"), "txtTotalUSD", Language.getMessage(mstrModuleName, 32, "Total USD"))
            Call ReportFunction.TextChange(objRpt.Subreports("13thCheck"), "txtSousTotal6", Language.getMessage(mstrModuleName, 43, "Sous-Total (6)"))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt.Subreports("13thCheck"), "txtCDFTotal", Format(dec13thCheckCDFTotal, GUI.fmtCurrency))
            'Call ReportFunction.TextChange(objRpt.Subreports("13thCheck"), "txtUSDTotal", Format(dec13thCheckUSDTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("13thCheck"), "txtCDFTotal", Format(dec13thCheckCDFTotal, strfmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("13thCheck"), "txtUSDTotal", Format(dec13thCheckUSDTotal, strfmtCurrency))
            'Sohail (21 Aug 2015) -- End

            Call ReportFunction.TextChange(objRpt.Subreports("Brut"), "txtABrutTotal", Language.getMessage(mstrModuleName, 44, "A. Brut Total USD"))
            Call ReportFunction.TextChange(objRpt.Subreports("Brut"), "txtDescription", Language.getMessage(mstrModuleName, 45, "Description"))
            Call ReportFunction.TextChange(objRpt.Subreports("Brut"), "txtTotalCDF", Language.getMessage(mstrModuleName, 31, "Total CDF"))
            Call ReportFunction.TextChange(objRpt.Subreports("Brut"), "txtTotalUSD", Language.getMessage(mstrModuleName, 32, "Total USD"))
            Call ReportFunction.TextChange(objRpt.Subreports("Brut"), "txtTotalBrut", Language.getMessage(mstrModuleName, 46, "Total Brut"))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt.Subreports("Brut"), "txtCDFTotal", Format(decBrutCDFTotal, GUI.fmtCurrency))
            'Call ReportFunction.TextChange(objRpt.Subreports("Brut"), "txtUSDTotal", Format(decBrutUSDTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("Brut"), "txtCDFTotal", Format(decBrutCDFTotal, strfmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("Brut"), "txtUSDTotal", Format(decBrutUSDTotal, strfmtCurrency))
            'Sohail (21 Aug 2015) -- End

            Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtDeduction", Language.getMessage(mstrModuleName, 47, "B. Deduction"))
            Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtBaseTaxable", Language.getMessage(mstrModuleName, 48, "Base Taxable"))
            Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtBaseImposable", Language.getMessage(mstrModuleName, 49, "Base Imposable"))
            Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtDescription", Language.getMessage(mstrModuleName, 45, "Description"))
            Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtTotalCDF", Language.getMessage(mstrModuleName, 31, "Total CDF"))
            Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtTotalUSD", Language.getMessage(mstrModuleName, 32, "Total USD"))
            Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtTotalDeduction", Language.getMessage(mstrModuleName, 50, "Total Deduction"))
            Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtNetPay", Language.getMessage(mstrModuleName, 51, "Total Net à payer"))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtCDFTotal", Format(decDeductionCDFTotal, GUI.fmtCurrency))
            'Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtUSDTotal", Format(decDeductionUSDTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtCDFTotal", Format(decDeductionCDFTotal, strfmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("Deduction"), "txtUSDTotal", Format(decDeductionUSDTotal, strfmtCurrency))
            'Sohail (21 Aug 2015) -- End

            Call ReportFunction.TextChange(objRpt.Subreports("Contribution"), "txtMontant", Language.getMessage(mstrModuleName, 52, "C. Montant à payer à l'Etat"))
            Call ReportFunction.TextChange(objRpt.Subreports("Contribution"), "txtTotalCDF", Language.getMessage(mstrModuleName, 31, "Total CDF"))
            Call ReportFunction.TextChange(objRpt.Subreports("Contribution"), "txtTotalUSD", Language.getMessage(mstrModuleName, 32, "Total USD"))
            Call ReportFunction.TextChange(objRpt.Subreports("Contribution"), "txtTotal", Language.getMessage(mstrModuleName, 53, "Total"))
            Call ReportFunction.TextChange(objRpt.Subreports("Contribution"), "txtChargeTotale", Language.getMessage(mstrModuleName, 54, "D. Charge Totale"))

            Call ReportFunction.TextChange(objRpt, "txtFinalSettlement", Language.getMessage(mstrModuleName, 59, "Pour solde de tout compte"))
            Call ReportFunction.TextChange(objRpt, "txtNameValue", Language.getMessage(mstrModuleName, 60, "Nom  :") & mstrEmpName)
            Call ReportFunction.TextChange(objRpt, "txtFinalCountdown", Language.getMessage(mstrModuleName, 61, "Décompte Final de:"))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt, "txtFinalCDFAmt", Format(mdecNetPay, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtFinalCDFAmt", Format(mdecNetPay, strfmtCurrency))
            'Sohail (21 Aug 2015) -- End
            Call ReportFunction.TextChange(objRpt, "txtFinalCDF", Language.getMessage(mstrModuleName, 62, "CDF"))
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt, "txtFinalUSDAmt", Format(mdecNetPayUSD, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtFinalUSDAmt", Format(mdecNetPayUSD, strfmtCurrency))
            'Sohail (21 Aug 2015) -- End
            Call ReportFunction.TextChange(objRpt, "txtFinalUSD", Language.getMessage(mstrModuleName, 63, "USD"))
            Call ReportFunction.TextChange(objRpt, "txtFinalTransferBank", Language.getMessage(mstrModuleName, 64, "A transférer au compte:"))
            Call ReportFunction.TextChange(objRpt, "txtBankBranch", strBranch)
            Call ReportFunction.TextChange(objRpt, "txtAcccountNo", strAccountNo)
            Call ReportFunction.TextChange(objRpt, "txtExchangeRate", Language.getMessage(mstrModuleName, 65, "Exchange Rate :"))
            Call ReportFunction.TextChange(objRpt, "txtExchangeRateValue", mstrCurrency_Rate)


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            'If menExportAction = enExportAction.ExcelExtra Then
            '    mdtTableExcel = rpt_Data.Tables(0)

            '    Dim mintColumn As Integer = 0

            'mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 10, "Employee Code")
            'mdtTableExcel.Columns("Column1").SetOrdinal(0)
            'mintColumn += 1

            'mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 11, "Employee Name")
            'mdtTableExcel.Columns("Column2").SetOrdinal(1)
            'mintColumn += 1

            'mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 22, "Grade")
            'mdtTableExcel.Columns("Column3").SetOrdinal(2)
            'mintColumn += 1

            'mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 23, "Job")
            'mdtTableExcel.Columns("Column4").SetOrdinal(3)
            'mintColumn += 1

            'mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 24, "Last Increment Date")
            'mdtTableExcel.Columns("Column5").SetOrdinal(4)
            'mintColumn += 1

            'mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 12, "Current Scale")
            'mdtTableExcel.Columns("Column81").SetOrdinal(5)
            'mintColumn += 1

            'mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 13, "Increment Amount")
            'mdtTableExcel.Columns("Column82").SetOrdinal(6)
            'mintColumn += 1

            'mdtTableExcel.Columns("Column83").Caption = Language.getMessage(mstrModuleName, 14, "New Scale")
            'mdtTableExcel.Columns("Column83").SetOrdinal(7)
            'mintColumn += 1

            'mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 19, "Reason :")
            'mdtTableExcel.Columns("Column9").SetOrdinal(8)
            'mintColumn += 1

            'If mintViewIndex > 0 Then
            '    mdtTableExcel.Columns("Column10").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
            '    mdtTableExcel.Columns("Column10").SetOrdinal(9)
            '    mintColumn += 1
            'End If


            'For i = mintColumn To mdtTableExcel.Columns.Count - 1
            '    mdtTableExcel.Columns.RemoveAt(mintColumn)
            'Next

            'End If


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period :")
            Language.setMessage(mstrModuleName, 2, "Empoyee :")
            Language.setMessage(mstrModuleName, 3, "Appointed")
            Language.setMessage(mstrModuleName, 4, "Transfer In")
            Language.setMessage(mstrModuleName, 5, "Transfer Out")
            Language.setMessage(mstrModuleName, 6, "Reinstated")
            Language.setMessage(mstrModuleName, 7, "Exit")
            Language.setMessage(mstrModuleName, 9, "Order By :")
            Language.setMessage(mstrModuleName, 10, "Employee Code")
            Language.setMessage(mstrModuleName, 11, "Employee Name")
            Language.setMessage(mstrModuleName, 12, "Surname")
            Language.setMessage(mstrModuleName, 13, "Job Title")
            Language.setMessage(mstrModuleName, 14, "Employee Number")
            Language.setMessage(mstrModuleName, 15, "Department")
            Language.setMessage(mstrModuleName, 16, "Annual Gross Salary")
            Language.setMessage(mstrModuleName, 17, "Monthly Gross Salary")
            Language.setMessage(mstrModuleName, 18, "Number of Child")
            Language.setMessage(mstrModuleName, 19, "Number of Dependent")
            Language.setMessage(mstrModuleName, 20, "Social Security Number")
            Language.setMessage(mstrModuleName, 21, "Type of Contract")
            Language.setMessage(mstrModuleName, 22, "Hiring Date")
            Language.setMessage(mstrModuleName, 23, "Period")
            Language.setMessage(mstrModuleName, 24, "Number of working days")
            Language.setMessage(mstrModuleName, 25, "Notice")
            Language.setMessage(mstrModuleName, 26, "Notification of the Notice")
            Language.setMessage(mstrModuleName, 27, "Jours")
            Language.setMessage(mstrModuleName, 28, "1. Prestation")
            Language.setMessage(mstrModuleName, 29, "Montant Mensuel de 26 Jours")
            Language.setMessage(mstrModuleName, 30, "Rate")
            Language.setMessage(mstrModuleName, 31, "Total CDF")
            Language.setMessage(mstrModuleName, 32, "Total USD")
            Language.setMessage(mstrModuleName, 33, "Sous-Total (1)")
            Language.setMessage(mstrModuleName, 34, "2. Notice")
            Language.setMessage(mstrModuleName, 35, "Sous-Total (2)")
            Language.setMessage(mstrModuleName, 36, "3. Bonus")
            Language.setMessage(mstrModuleName, 37, "Sous-Total (3)")
            Language.setMessage(mstrModuleName, 38, "4. Leave non Taken")
            Language.setMessage(mstrModuleName, 39, "Sous-Total (4)")
            Language.setMessage(mstrModuleName, 40, "5. Conge Préavis")
            Language.setMessage(mstrModuleName, 41, "Sous-Total (5)")
            Language.setMessage(mstrModuleName, 42, "6. 13th Check (prorata temporis)")
            Language.setMessage(mstrModuleName, 43, "Sous-Total (6)")
            Language.setMessage(mstrModuleName, 44, "A. Brut Total USD")
            Language.setMessage(mstrModuleName, 45, "Description")
            Language.setMessage(mstrModuleName, 46, "Total Brut")
            Language.setMessage(mstrModuleName, 47, "B. Deduction")
            Language.setMessage(mstrModuleName, 48, "Base Taxable")
            Language.setMessage(mstrModuleName, 49, "Base Imposable")
            Language.setMessage(mstrModuleName, 50, "Total Deduction")
            Language.setMessage(mstrModuleName, 51, "Total Net à payer")
            Language.setMessage(mstrModuleName, 52, "C. Montant à payer à l'Etat")
            Language.setMessage(mstrModuleName, 53, "Total")
            Language.setMessage(mstrModuleName, 54, "D. Charge Totale")
            Language.setMessage(mstrModuleName, 55, "Approuvé Par :")
            Language.setMessage(mstrModuleName, 56, "Directeur des Ressources Humaines")
            Language.setMessage(mstrModuleName, 57, "Directeur Financier")
            Language.setMessage(mstrModuleName, 58, "Directeur National")
            Language.setMessage(mstrModuleName, 59, "Pour solde de tout compte")
            Language.setMessage(mstrModuleName, 60, "Nom  :")
            Language.setMessage(mstrModuleName, 61, "Décompte Final de:")
            Language.setMessage(mstrModuleName, 62, "CDF")
            Language.setMessage(mstrModuleName, 63, "USD")
            Language.setMessage(mstrModuleName, 64, "A transférer au compte:")
            Language.setMessage(mstrModuleName, 65, "Exchange Rate :")
            Language.setMessage(mstrModuleName, 66, "Grand Total :")
            Language.setMessage(mstrModuleName, 67, "IPR Brut")
            Language.setMessage(mstrModuleName, 68, "IPR Net")
            Language.setMessage(mstrModuleName, 69, "No dependants pour reduction Tax")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
