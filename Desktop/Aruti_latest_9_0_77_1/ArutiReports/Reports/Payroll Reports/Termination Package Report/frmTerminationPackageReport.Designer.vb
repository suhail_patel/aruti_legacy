﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTerminationPackageReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTerminationPackageReport))
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboINSS35 = New System.Windows.Forms.ComboBox
        Me.lblINSS35 = New System.Windows.Forms.Label
        Me.cboOtherDeductions = New System.Windows.Forms.ComboBox
        Me.lblOtherDeductions = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtSearchBonusHeads = New eZee.TextBox.AlphanumericTextBox
        Me.lblBonusHeads = New System.Windows.Forms.Label
        Me.dgvBonusHead = New System.Windows.Forms.DataGridView
        Me.objcolhIsCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objcolhTranheadUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhTranHeadCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhTranHeadName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboRateOfNotice = New System.Windows.Forms.ComboBox
        Me.lblRateOfNotice = New System.Windows.Forms.Label
        Me.cboFamilyAllowance = New System.Windows.Forms.ComboBox
        Me.lblFamilyAllowance = New System.Windows.Forms.Label
        Me.cboIPRNet = New System.Windows.Forms.ComboBox
        Me.lblIPRNet = New System.Windows.Forms.Label
        Me.cboAdvanceRepayment = New System.Windows.Forms.ComboBox
        Me.lblAdvanceRepayment = New System.Windows.Forms.Label
        Me.cboLoanRepayment = New System.Windows.Forms.ComboBox
        Me.lblLoanRepayment = New System.Windows.Forms.Label
        Me.cboLogement = New System.Windows.Forms.ComboBox
        Me.lblLogement = New System.Windows.Forms.Label
        Me.cboDependentHead = New System.Windows.Forms.ComboBox
        Me.lblDependentHead = New System.Windows.Forms.Label
        Me.cboChildHead = New System.Windows.Forms.ComboBox
        Me.lblExRate = New System.Windows.Forms.Label
        Me.lblChildHead = New System.Windows.Forms.Label
        Me.dtpNoticeDate = New System.Windows.Forms.DateTimePicker
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.lblNoticeDate = New System.Windows.Forms.Label
        Me.cboTerminationFactor = New System.Windows.Forms.ComboBox
        Me.lblTerminationFactor = New System.Windows.Forms.Label
        Me.lblDeductionHead = New System.Windows.Forms.Label
        Me.lblContribution = New System.Windows.Forms.Label
        Me.objchkallDeducation = New System.Windows.Forms.CheckBox
        Me.lvDeductionHead = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhDCheckAll = New System.Windows.Forms.ColumnHeader
        Me.colhDCode = New System.Windows.Forms.ColumnHeader
        Me.colhDName = New System.Windows.Forms.ColumnHeader
        Me.objallchkContributaion = New System.Windows.Forms.CheckBox
        Me.lvContributaionHead = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCCheckAll = New System.Windows.Forms.ColumnHeader
        Me.colhCCode = New System.Windows.Forms.ColumnHeader
        Me.colhCName = New System.Windows.Forms.ColumnHeader
        Me.cboNetPayhead = New System.Windows.Forms.ComboBox
        Me.lblNetPayhead = New System.Windows.Forms.Label
        Me.cboBaseImposablehead = New System.Windows.Forms.ComboBox
        Me.lblBaseImposableHead = New System.Windows.Forms.Label
        Me.cboBaseTaxablehead = New System.Windows.Forms.ComboBox
        Me.lblBaseTaxablehead = New System.Windows.Forms.Label
        Me.CboCheckHead = New System.Windows.Forms.ComboBox
        Me.lblCheckHead = New System.Windows.Forms.Label
        Me.cboNotificationAllowance = New System.Windows.Forms.ComboBox
        Me.lblNotificationAllowance = New System.Windows.Forms.Label
        Me.lbldaysinPeriod = New System.Windows.Forms.Label
        Me.cboDaysInPeriod = New System.Windows.Forms.ComboBox
        Me.cboCongePreavisHead = New System.Windows.Forms.ComboBox
        Me.lblCongePreavisHead = New System.Windows.Forms.Label
        Me.lblSalaryHead = New System.Windows.Forms.Label
        Me.cboSalaryHead = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblHourHead = New System.Windows.Forms.Label
        Me.lnHourAmtMapping = New eZee.Common.eZeeLine
        Me.lnkDeleteHourAmtMapping = New System.Windows.Forms.LinkLabel
        Me.lvHourAmtMapping = New System.Windows.Forms.ListView
        Me.objcolhSrNo = New System.Windows.Forms.ColumnHeader
        Me.colhHour = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.lnkAddHourAmtMapping = New System.Windows.Forms.LinkLabel
        Me.cboOT1Amount = New System.Windows.Forms.ComboBox
        Me.cboOT1Hours = New System.Windows.Forms.ComboBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbSortBy.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvBonusHead, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 591)
        Me.NavPanel.Size = New System.Drawing.Size(1038, 55)
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(548, 514)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 61
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(441, 63)
        Me.gbSortBy.TabIndex = 1
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbSortBy.Visible = False
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(87, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(346, 21)
        Me.txtOrderBy.TabIndex = 0
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(483, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(28, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(80, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EZeeCollapsibleContainer1.AutoScroll = True
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboINSS35)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblINSS35)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboOtherDeductions)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblOtherDeductions)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.Panel1)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.btnSaveSelection)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboRateOfNotice)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblRateOfNotice)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboFamilyAllowance)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblFamilyAllowance)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboIPRNet)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblIPRNet)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboAdvanceRepayment)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblAdvanceRepayment)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboLoanRepayment)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblLoanRepayment)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboLogement)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblLogement)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboDependentHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblDependentHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboChildHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblExRate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.gbSortBy)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblChildHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.dtpNoticeDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCurrency)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCurrency)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblNoticeDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboTerminationFactor)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblTerminationFactor)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblDeductionHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblContribution)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objchkallDeducation)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lvDeductionHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objallchkContributaion)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lvContributaionHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboNetPayhead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblNetPayhead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboBaseImposablehead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblBaseImposableHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboBaseTaxablehead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblBaseTaxablehead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.CboCheckHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCheckHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboNotificationAllowance)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblNotificationAllowance)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lbldaysinPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboDaysInPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboCongePreavisHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCongePreavisHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblSalaryHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboSalaryHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.Label1)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblHourHead)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnHourAmtMapping)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkDeleteHourAmtMapping)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lvHourAmtMapping)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkAddHourAmtMapping)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboOT1Amount)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboOT1Hours)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblLeaveType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboLeaveType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkAnalysisBy)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmployee)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(12, 66)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(1014, 519)
        Me.EZeeCollapsibleContainer1.TabIndex = 0
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Filter Criteria"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboINSS35
        '
        Me.cboINSS35.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboINSS35.DropDownWidth = 200
        Me.cboINSS35.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboINSS35.FormattingEnabled = True
        Me.cboINSS35.Location = New System.Drawing.Point(386, 329)
        Me.cboINSS35.Name = "cboINSS35"
        Me.cboINSS35.Size = New System.Drawing.Size(114, 21)
        Me.cboINSS35.TabIndex = 22
        '
        'lblINSS35
        '
        Me.lblINSS35.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblINSS35.Location = New System.Drawing.Point(266, 333)
        Me.lblINSS35.Name = "lblINSS35"
        Me.lblINSS35.Size = New System.Drawing.Size(115, 13)
        Me.lblINSS35.TabIndex = 330
        Me.lblINSS35.Text = "INSS 3.5"
        Me.lblINSS35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOtherDeductions
        '
        Me.cboOtherDeductions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboOtherDeductions.DropDownWidth = 200
        Me.cboOtherDeductions.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherDeductions.FormattingEnabled = True
        Me.cboOtherDeductions.Location = New System.Drawing.Point(385, 302)
        Me.cboOtherDeductions.Name = "cboOtherDeductions"
        Me.cboOtherDeductions.Size = New System.Drawing.Size(114, 21)
        Me.cboOtherDeductions.TabIndex = 20
        '
        'lblOtherDeductions
        '
        Me.lblOtherDeductions.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherDeductions.Location = New System.Drawing.Point(266, 306)
        Me.lblOtherDeductions.Name = "lblOtherDeductions"
        Me.lblOtherDeductions.Size = New System.Drawing.Size(115, 13)
        Me.lblOtherDeductions.TabIndex = 327
        Me.lblOtherDeductions.Text = "Other Deductions"
        Me.lblOtherDeductions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtSearchBonusHeads)
        Me.Panel1.Controls.Add(Me.lblBonusHeads)
        Me.Panel1.Controls.Add(Me.dgvBonusHead)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(548, 328)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(433, 145)
        Me.Panel1.TabIndex = 324
        '
        'txtSearchBonusHeads
        '
        Me.txtSearchBonusHeads.Flags = 0
        Me.txtSearchBonusHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchBonusHeads.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearchBonusHeads.Location = New System.Drawing.Point(128, 3)
        Me.txtSearchBonusHeads.Name = "txtSearchBonusHeads"
        Me.txtSearchBonusHeads.Size = New System.Drawing.Size(302, 21)
        Me.txtSearchBonusHeads.TabIndex = 326
        '
        'lblBonusHeads
        '
        Me.lblBonusHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBonusHeads.Location = New System.Drawing.Point(8, 8)
        Me.lblBonusHeads.Name = "lblBonusHeads"
        Me.lblBonusHeads.Size = New System.Drawing.Size(119, 13)
        Me.lblBonusHeads.TabIndex = 325
        Me.lblBonusHeads.Text = "Bonus Heads"
        Me.lblBonusHeads.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvBonusHead
        '
        Me.dgvBonusHead.AllowUserToAddRows = False
        Me.dgvBonusHead.AllowUserToDeleteRows = False
        Me.dgvBonusHead.BackgroundColor = System.Drawing.Color.White
        Me.dgvBonusHead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBonusHead.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhIsCheck, Me.objcolhTranheadUnkId, Me.colhTranHeadCode, Me.colhTranHeadName})
        Me.dgvBonusHead.Location = New System.Drawing.Point(3, 27)
        Me.dgvBonusHead.Name = "dgvBonusHead"
        Me.dgvBonusHead.RowHeadersVisible = False
        Me.dgvBonusHead.Size = New System.Drawing.Size(427, 115)
        Me.dgvBonusHead.TabIndex = 324
        '
        'objcolhIsCheck
        '
        Me.objcolhIsCheck.HeaderText = ""
        Me.objcolhIsCheck.Name = "objcolhIsCheck"
        Me.objcolhIsCheck.Width = 25
        '
        'objcolhTranheadUnkId
        '
        Me.objcolhTranheadUnkId.HeaderText = "Tran. Head Unk Id"
        Me.objcolhTranheadUnkId.Name = "objcolhTranheadUnkId"
        Me.objcolhTranheadUnkId.ReadOnly = True
        Me.objcolhTranheadUnkId.Visible = False
        '
        'colhTranHeadCode
        '
        Me.colhTranHeadCode.HeaderText = "Head Code"
        Me.colhTranHeadCode.Name = "colhTranHeadCode"
        Me.colhTranHeadCode.ReadOnly = True
        Me.colhTranHeadCode.Width = 85
        '
        'colhTranHeadName
        '
        Me.colhTranHeadName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhTranHeadName.HeaderText = "Tran. Head Name"
        Me.colhTranHeadName.Name = "colhTranHeadName"
        Me.colhTranHeadName.ReadOnly = True
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(548, 479)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 1
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'cboRateOfNotice
        '
        Me.cboRateOfNotice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboRateOfNotice.DropDownWidth = 200
        Me.cboRateOfNotice.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRateOfNotice.FormattingEnabled = True
        Me.cboRateOfNotice.Location = New System.Drawing.Point(386, 275)
        Me.cboRateOfNotice.Name = "cboRateOfNotice"
        Me.cboRateOfNotice.Size = New System.Drawing.Size(113, 21)
        Me.cboRateOfNotice.TabIndex = 18
        '
        'lblRateOfNotice
        '
        Me.lblRateOfNotice.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRateOfNotice.Location = New System.Drawing.Point(266, 279)
        Me.lblRateOfNotice.Name = "lblRateOfNotice"
        Me.lblRateOfNotice.Size = New System.Drawing.Size(115, 13)
        Me.lblRateOfNotice.TabIndex = 321
        Me.lblRateOfNotice.Text = "Rate Of Notice"
        Me.lblRateOfNotice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFamilyAllowance
        '
        Me.cboFamilyAllowance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboFamilyAllowance.DropDownWidth = 200
        Me.cboFamilyAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFamilyAllowance.FormattingEnabled = True
        Me.cboFamilyAllowance.Location = New System.Drawing.Point(133, 302)
        Me.cboFamilyAllowance.Name = "cboFamilyAllowance"
        Me.cboFamilyAllowance.Size = New System.Drawing.Size(126, 21)
        Me.cboFamilyAllowance.TabIndex = 19
        '
        'lblFamilyAllowance
        '
        Me.lblFamilyAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFamilyAllowance.Location = New System.Drawing.Point(8, 306)
        Me.lblFamilyAllowance.Name = "lblFamilyAllowance"
        Me.lblFamilyAllowance.Size = New System.Drawing.Size(119, 13)
        Me.lblFamilyAllowance.TabIndex = 318
        Me.lblFamilyAllowance.Text = "Family Allowance"
        Me.lblFamilyAllowance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIPRNet
        '
        Me.cboIPRNet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboIPRNet.DropDownWidth = 200
        Me.cboIPRNet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIPRNet.FormattingEnabled = True
        Me.cboIPRNet.Location = New System.Drawing.Point(133, 329)
        Me.cboIPRNet.Name = "cboIPRNet"
        Me.cboIPRNet.Size = New System.Drawing.Size(126, 21)
        Me.cboIPRNet.TabIndex = 21
        '
        'lblIPRNet
        '
        Me.lblIPRNet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIPRNet.Location = New System.Drawing.Point(8, 333)
        Me.lblIPRNet.Name = "lblIPRNet"
        Me.lblIPRNet.Size = New System.Drawing.Size(115, 13)
        Me.lblIPRNet.TabIndex = 316
        Me.lblIPRNet.Text = "IPR Net"
        Me.lblIPRNet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAdvanceRepayment
        '
        Me.cboAdvanceRepayment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboAdvanceRepayment.DropDownWidth = 200
        Me.cboAdvanceRepayment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAdvanceRepayment.FormattingEnabled = True
        Me.cboAdvanceRepayment.Location = New System.Drawing.Point(133, 275)
        Me.cboAdvanceRepayment.Name = "cboAdvanceRepayment"
        Me.cboAdvanceRepayment.Size = New System.Drawing.Size(126, 21)
        Me.cboAdvanceRepayment.TabIndex = 17
        '
        'lblAdvanceRepayment
        '
        Me.lblAdvanceRepayment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdvanceRepayment.Location = New System.Drawing.Point(8, 279)
        Me.lblAdvanceRepayment.Name = "lblAdvanceRepayment"
        Me.lblAdvanceRepayment.Size = New System.Drawing.Size(119, 13)
        Me.lblAdvanceRepayment.TabIndex = 315
        Me.lblAdvanceRepayment.Text = "Advance Repayment"
        Me.lblAdvanceRepayment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanRepayment
        '
        Me.cboLoanRepayment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboLoanRepayment.DropDownWidth = 200
        Me.cboLoanRepayment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanRepayment.FormattingEnabled = True
        Me.cboLoanRepayment.Location = New System.Drawing.Point(385, 248)
        Me.cboLoanRepayment.Name = "cboLoanRepayment"
        Me.cboLoanRepayment.Size = New System.Drawing.Size(114, 21)
        Me.cboLoanRepayment.TabIndex = 16
        '
        'lblLoanRepayment
        '
        Me.lblLoanRepayment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanRepayment.Location = New System.Drawing.Point(266, 252)
        Me.lblLoanRepayment.Name = "lblLoanRepayment"
        Me.lblLoanRepayment.Size = New System.Drawing.Size(115, 13)
        Me.lblLoanRepayment.TabIndex = 312
        Me.lblLoanRepayment.Text = "Loan Repayment"
        Me.lblLoanRepayment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLogement
        '
        Me.cboLogement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboLogement.DropDownWidth = 200
        Me.cboLogement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLogement.FormattingEnabled = True
        Me.cboLogement.Location = New System.Drawing.Point(133, 248)
        Me.cboLogement.Name = "cboLogement"
        Me.cboLogement.Size = New System.Drawing.Size(126, 21)
        Me.cboLogement.TabIndex = 15
        '
        'lblLogement
        '
        Me.lblLogement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLogement.Location = New System.Drawing.Point(8, 252)
        Me.lblLogement.Name = "lblLogement"
        Me.lblLogement.Size = New System.Drawing.Size(119, 13)
        Me.lblLogement.TabIndex = 311
        Me.lblLogement.Text = "Logement"
        Me.lblLogement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDependentHead
        '
        Me.cboDependentHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboDependentHead.DropDownWidth = 200
        Me.cboDependentHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDependentHead.FormattingEnabled = True
        Me.cboDependentHead.Location = New System.Drawing.Point(386, 221)
        Me.cboDependentHead.Name = "cboDependentHead"
        Me.cboDependentHead.Size = New System.Drawing.Size(114, 21)
        Me.cboDependentHead.TabIndex = 14
        '
        'lblDependentHead
        '
        Me.lblDependentHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDependentHead.Location = New System.Drawing.Point(266, 225)
        Me.lblDependentHead.Name = "lblDependentHead"
        Me.lblDependentHead.Size = New System.Drawing.Size(115, 13)
        Me.lblDependentHead.TabIndex = 307
        Me.lblDependentHead.Text = "No. of Dependants"
        Me.lblDependentHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboChildHead
        '
        Me.cboChildHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboChildHead.DropDownWidth = 200
        Me.cboChildHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChildHead.FormattingEnabled = True
        Me.cboChildHead.Location = New System.Drawing.Point(134, 221)
        Me.cboChildHead.Name = "cboChildHead"
        Me.cboChildHead.Size = New System.Drawing.Size(126, 21)
        Me.cboChildHead.TabIndex = 13
        '
        'lblExRate
        '
        Me.lblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExRate.Location = New System.Drawing.Point(683, 488)
        Me.lblExRate.Name = "lblExRate"
        Me.lblExRate.Size = New System.Drawing.Size(115, 13)
        Me.lblExRate.TabIndex = 259
        Me.lblExRate.Text = "Ex. Rate"
        Me.lblExRate.Visible = False
        '
        'lblChildHead
        '
        Me.lblChildHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChildHead.Location = New System.Drawing.Point(9, 225)
        Me.lblChildHead.Name = "lblChildHead"
        Me.lblChildHead.Size = New System.Drawing.Size(119, 13)
        Me.lblChildHead.TabIndex = 306
        Me.lblChildHead.Text = "No. of Children"
        Me.lblChildHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpNoticeDate
        '
        Me.dtpNoticeDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpNoticeDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpNoticeDate.Location = New System.Drawing.Point(133, 356)
        Me.dtpNoticeDate.Name = "dtpNoticeDate"
        Me.dtpNoticeDate.Size = New System.Drawing.Size(113, 21)
        Me.dtpNoticeDate.TabIndex = 23
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(386, 32)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(114, 21)
        Me.cboCurrency.TabIndex = 1
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(266, 36)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(119, 13)
        Me.lblCurrency.TabIndex = 257
        Me.lblCurrency.Text = "Currency"
        '
        'lblNoticeDate
        '
        Me.lblNoticeDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoticeDate.Location = New System.Drawing.Point(8, 360)
        Me.lblNoticeDate.Name = "lblNoticeDate"
        Me.lblNoticeDate.Size = New System.Drawing.Size(115, 13)
        Me.lblNoticeDate.TabIndex = 303
        Me.lblNoticeDate.Text = "Notice Date"
        Me.lblNoticeDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTerminationFactor
        '
        Me.cboTerminationFactor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboTerminationFactor.DropDownWidth = 200
        Me.cboTerminationFactor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTerminationFactor.FormattingEnabled = True
        Me.cboTerminationFactor.Location = New System.Drawing.Point(386, 194)
        Me.cboTerminationFactor.Name = "cboTerminationFactor"
        Me.cboTerminationFactor.Size = New System.Drawing.Size(114, 21)
        Me.cboTerminationFactor.TabIndex = 12
        '
        'lblTerminationFactor
        '
        Me.lblTerminationFactor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTerminationFactor.Location = New System.Drawing.Point(266, 198)
        Me.lblTerminationFactor.Name = "lblTerminationFactor"
        Me.lblTerminationFactor.Size = New System.Drawing.Size(115, 13)
        Me.lblTerminationFactor.TabIndex = 301
        Me.lblTerminationFactor.Text = "Termination Factor"
        Me.lblTerminationFactor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDeductionHead
        '
        Me.lblDeductionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeductionHead.Location = New System.Drawing.Point(548, 36)
        Me.lblDeductionHead.Name = "lblDeductionHead"
        Me.lblDeductionHead.Size = New System.Drawing.Size(119, 13)
        Me.lblDeductionHead.TabIndex = 299
        Me.lblDeductionHead.Text = "Deduction Head"
        Me.lblDeductionHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblContribution
        '
        Me.lblContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContribution.Location = New System.Drawing.Point(545, 194)
        Me.lblContribution.Name = "lblContribution"
        Me.lblContribution.Size = New System.Drawing.Size(119, 13)
        Me.lblContribution.TabIndex = 294
        Me.lblContribution.Text = "Contribution Head"
        Me.lblContribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkallDeducation
        '
        Me.objchkallDeducation.AutoSize = True
        Me.objchkallDeducation.Location = New System.Drawing.Point(554, 57)
        Me.objchkallDeducation.Name = "objchkallDeducation"
        Me.objchkallDeducation.Size = New System.Drawing.Size(15, 14)
        Me.objchkallDeducation.TabIndex = 298
        Me.objchkallDeducation.UseVisualStyleBackColor = True
        '
        'lvDeductionHead
        '
        Me.lvDeductionHead.BackColorOnChecked = False
        Me.lvDeductionHead.CheckBoxes = True
        Me.lvDeductionHead.ColumnHeaders = Nothing
        Me.lvDeductionHead.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhDCheckAll, Me.colhDCode, Me.colhDName})
        Me.lvDeductionHead.CompulsoryColumns = ""
        Me.lvDeductionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDeductionHead.FullRowSelect = True
        Me.lvDeductionHead.GridLines = True
        Me.lvDeductionHead.GroupingColumn = Nothing
        Me.lvDeductionHead.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvDeductionHead.HideSelection = False
        Me.lvDeductionHead.Location = New System.Drawing.Point(548, 52)
        Me.lvDeductionHead.MinColumnWidth = 50
        Me.lvDeductionHead.MultiSelect = False
        Me.lvDeductionHead.Name = "lvDeductionHead"
        Me.lvDeductionHead.OptionalColumns = ""
        Me.lvDeductionHead.ShowMoreItem = False
        Me.lvDeductionHead.ShowSaveItem = False
        Me.lvDeductionHead.ShowSelectAll = True
        Me.lvDeductionHead.ShowSizeAllColumnsToFit = True
        Me.lvDeductionHead.Size = New System.Drawing.Size(433, 136)
        Me.lvDeductionHead.Sortable = True
        Me.lvDeductionHead.TabIndex = 297
        Me.lvDeductionHead.UseCompatibleStateImageBehavior = False
        Me.lvDeductionHead.View = System.Windows.Forms.View.Details
        '
        'objcolhDCheckAll
        '
        Me.objcolhDCheckAll.Tag = "objcolhDCheckAll"
        Me.objcolhDCheckAll.Text = ""
        Me.objcolhDCheckAll.Width = 25
        '
        'colhDCode
        '
        Me.colhDCode.Tag = "colhDCode"
        Me.colhDCode.Text = "Code"
        Me.colhDCode.Width = 129
        '
        'colhDName
        '
        Me.colhDName.Tag = "colhDName"
        Me.colhDName.Text = "Name"
        Me.colhDName.Width = 273
        '
        'objallchkContributaion
        '
        Me.objallchkContributaion.AutoSize = True
        Me.objallchkContributaion.Location = New System.Drawing.Point(554, 216)
        Me.objallchkContributaion.Name = "objallchkContributaion"
        Me.objallchkContributaion.Size = New System.Drawing.Size(15, 14)
        Me.objallchkContributaion.TabIndex = 296
        Me.objallchkContributaion.UseVisualStyleBackColor = True
        '
        'lvContributaionHead
        '
        Me.lvContributaionHead.BackColorOnChecked = False
        Me.lvContributaionHead.CheckBoxes = True
        Me.lvContributaionHead.ColumnHeaders = Nothing
        Me.lvContributaionHead.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCCheckAll, Me.colhCCode, Me.colhCName})
        Me.lvContributaionHead.CompulsoryColumns = ""
        Me.lvContributaionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvContributaionHead.FullRowSelect = True
        Me.lvContributaionHead.GridLines = True
        Me.lvContributaionHead.GroupingColumn = Nothing
        Me.lvContributaionHead.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvContributaionHead.HideSelection = False
        Me.lvContributaionHead.Location = New System.Drawing.Point(548, 210)
        Me.lvContributaionHead.MinColumnWidth = 50
        Me.lvContributaionHead.MultiSelect = False
        Me.lvContributaionHead.Name = "lvContributaionHead"
        Me.lvContributaionHead.OptionalColumns = ""
        Me.lvContributaionHead.ShowMoreItem = False
        Me.lvContributaionHead.ShowSaveItem = False
        Me.lvContributaionHead.ShowSelectAll = True
        Me.lvContributaionHead.ShowSizeAllColumnsToFit = True
        Me.lvContributaionHead.Size = New System.Drawing.Size(433, 112)
        Me.lvContributaionHead.Sortable = True
        Me.lvContributaionHead.TabIndex = 295
        Me.lvContributaionHead.UseCompatibleStateImageBehavior = False
        Me.lvContributaionHead.View = System.Windows.Forms.View.Details
        '
        'objcolhCCheckAll
        '
        Me.objcolhCCheckAll.Tag = "objcolhCCheckAll"
        Me.objcolhCCheckAll.Text = ""
        Me.objcolhCCheckAll.Width = 25
        '
        'colhCCode
        '
        Me.colhCCode.Tag = "colhCCode"
        Me.colhCCode.Text = "Code"
        Me.colhCCode.Width = 129
        '
        'colhCName
        '
        Me.colhCName.Tag = "colhCName"
        Me.colhCName.Text = "Name"
        Me.colhCName.Width = 273
        '
        'cboNetPayhead
        '
        Me.cboNetPayhead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboNetPayhead.DropDownWidth = 200
        Me.cboNetPayhead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNetPayhead.FormattingEnabled = True
        Me.cboNetPayhead.Location = New System.Drawing.Point(134, 194)
        Me.cboNetPayhead.Name = "cboNetPayhead"
        Me.cboNetPayhead.Size = New System.Drawing.Size(126, 21)
        Me.cboNetPayhead.TabIndex = 11
        '
        'lblNetPayhead
        '
        Me.lblNetPayhead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetPayhead.Location = New System.Drawing.Point(9, 197)
        Me.lblNetPayhead.Name = "lblNetPayhead"
        Me.lblNetPayhead.Size = New System.Drawing.Size(119, 13)
        Me.lblNetPayhead.TabIndex = 286
        Me.lblNetPayhead.Text = "Net Pay head"
        Me.lblNetPayhead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBaseImposablehead
        '
        Me.cboBaseImposablehead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboBaseImposablehead.DropDownWidth = 200
        Me.cboBaseImposablehead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBaseImposablehead.FormattingEnabled = True
        Me.cboBaseImposablehead.Location = New System.Drawing.Point(386, 167)
        Me.cboBaseImposablehead.Name = "cboBaseImposablehead"
        Me.cboBaseImposablehead.Size = New System.Drawing.Size(114, 21)
        Me.cboBaseImposablehead.TabIndex = 10
        '
        'lblBaseImposableHead
        '
        Me.lblBaseImposableHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBaseImposableHead.Location = New System.Drawing.Point(266, 171)
        Me.lblBaseImposableHead.Name = "lblBaseImposableHead"
        Me.lblBaseImposableHead.Size = New System.Drawing.Size(115, 13)
        Me.lblBaseImposableHead.TabIndex = 283
        Me.lblBaseImposableHead.Text = "Base Imposable Head"
        Me.lblBaseImposableHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBaseTaxablehead
        '
        Me.cboBaseTaxablehead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboBaseTaxablehead.DropDownWidth = 200
        Me.cboBaseTaxablehead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBaseTaxablehead.FormattingEnabled = True
        Me.cboBaseTaxablehead.Location = New System.Drawing.Point(134, 167)
        Me.cboBaseTaxablehead.Name = "cboBaseTaxablehead"
        Me.cboBaseTaxablehead.Size = New System.Drawing.Size(126, 21)
        Me.cboBaseTaxablehead.TabIndex = 9
        '
        'lblBaseTaxablehead
        '
        Me.lblBaseTaxablehead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBaseTaxablehead.Location = New System.Drawing.Point(9, 170)
        Me.lblBaseTaxablehead.Name = "lblBaseTaxablehead"
        Me.lblBaseTaxablehead.Size = New System.Drawing.Size(119, 13)
        Me.lblBaseTaxablehead.TabIndex = 282
        Me.lblBaseTaxablehead.Text = "Base Taxable head"
        Me.lblBaseTaxablehead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CboCheckHead
        '
        Me.CboCheckHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.CboCheckHead.DropDownWidth = 200
        Me.CboCheckHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboCheckHead.FormattingEnabled = True
        Me.CboCheckHead.Location = New System.Drawing.Point(386, 140)
        Me.CboCheckHead.Name = "CboCheckHead"
        Me.CboCheckHead.Size = New System.Drawing.Size(114, 21)
        Me.CboCheckHead.TabIndex = 8
        '
        'lblCheckHead
        '
        Me.lblCheckHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCheckHead.Location = New System.Drawing.Point(266, 144)
        Me.lblCheckHead.Name = "lblCheckHead"
        Me.lblCheckHead.Size = New System.Drawing.Size(115, 13)
        Me.lblCheckHead.TabIndex = 278
        Me.lblCheckHead.Text = "13Th Check Head"
        Me.lblCheckHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNotificationAllowance
        '
        Me.cboNotificationAllowance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboNotificationAllowance.DropDownWidth = 200
        Me.cboNotificationAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNotificationAllowance.FormattingEnabled = True
        Me.cboNotificationAllowance.Location = New System.Drawing.Point(386, 113)
        Me.cboNotificationAllowance.Name = "cboNotificationAllowance"
        Me.cboNotificationAllowance.Size = New System.Drawing.Size(114, 21)
        Me.cboNotificationAllowance.TabIndex = 6
        '
        'lblNotificationAllowance
        '
        Me.lblNotificationAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNotificationAllowance.Location = New System.Drawing.Point(266, 117)
        Me.lblNotificationAllowance.Name = "lblNotificationAllowance"
        Me.lblNotificationAllowance.Size = New System.Drawing.Size(115, 13)
        Me.lblNotificationAllowance.TabIndex = 276
        Me.lblNotificationAllowance.Text = "Notification Allowance"
        Me.lblNotificationAllowance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbldaysinPeriod
        '
        Me.lbldaysinPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldaysinPeriod.Location = New System.Drawing.Point(266, 90)
        Me.lbldaysinPeriod.Name = "lbldaysinPeriod"
        Me.lbldaysinPeriod.Size = New System.Drawing.Size(115, 13)
        Me.lbldaysinPeriod.TabIndex = 275
        Me.lbldaysinPeriod.Text = "Days In Period"
        Me.lbldaysinPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDaysInPeriod
        '
        Me.cboDaysInPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboDaysInPeriod.DropDownWidth = 200
        Me.cboDaysInPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDaysInPeriod.FormattingEnabled = True
        Me.cboDaysInPeriod.Location = New System.Drawing.Point(386, 86)
        Me.cboDaysInPeriod.Name = "cboDaysInPeriod"
        Me.cboDaysInPeriod.Size = New System.Drawing.Size(114, 21)
        Me.cboDaysInPeriod.TabIndex = 4
        '
        'cboCongePreavisHead
        '
        Me.cboCongePreavisHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboCongePreavisHead.DropDownWidth = 200
        Me.cboCongePreavisHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCongePreavisHead.FormattingEnabled = True
        Me.cboCongePreavisHead.Location = New System.Drawing.Point(134, 140)
        Me.cboCongePreavisHead.Name = "cboCongePreavisHead"
        Me.cboCongePreavisHead.Size = New System.Drawing.Size(126, 21)
        Me.cboCongePreavisHead.TabIndex = 7
        '
        'lblCongePreavisHead
        '
        Me.lblCongePreavisHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCongePreavisHead.Location = New System.Drawing.Point(9, 144)
        Me.lblCongePreavisHead.Name = "lblCongePreavisHead"
        Me.lblCongePreavisHead.Size = New System.Drawing.Size(119, 13)
        Me.lblCongePreavisHead.TabIndex = 273
        Me.lblCongePreavisHead.Text = "Conge Preavis Head"
        Me.lblCongePreavisHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSalaryHead
        '
        Me.lblSalaryHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalaryHead.Location = New System.Drawing.Point(9, 117)
        Me.lblSalaryHead.Name = "lblSalaryHead"
        Me.lblSalaryHead.Size = New System.Drawing.Size(119, 13)
        Me.lblSalaryHead.TabIndex = 271
        Me.lblSalaryHead.Text = "Salary Head"
        Me.lblSalaryHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSalaryHead
        '
        Me.cboSalaryHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboSalaryHead.DropDownWidth = 200
        Me.cboSalaryHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSalaryHead.FormattingEnabled = True
        Me.cboSalaryHead.Location = New System.Drawing.Point(134, 113)
        Me.cboSalaryHead.Name = "cboSalaryHead"
        Me.cboSalaryHead.Size = New System.Drawing.Size(128, 21)
        Me.cboSalaryHead.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(266, 405)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 13)
        Me.Label1.TabIndex = 268
        Me.Label1.Text = "Amount Head"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHourHead
        '
        Me.lblHourHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHourHead.Location = New System.Drawing.Point(9, 405)
        Me.lblHourHead.Name = "lblHourHead"
        Me.lblHourHead.Size = New System.Drawing.Size(119, 13)
        Me.lblHourHead.TabIndex = 267
        Me.lblHourHead.Text = "Hours Head"
        Me.lblHourHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnHourAmtMapping
        '
        Me.lnHourAmtMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnHourAmtMapping.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnHourAmtMapping.Location = New System.Drawing.Point(7, 382)
        Me.lnHourAmtMapping.Name = "lnHourAmtMapping"
        Me.lnHourAmtMapping.Size = New System.Drawing.Size(457, 15)
        Me.lnHourAmtMapping.TabIndex = 266
        Me.lnHourAmtMapping.Text = "Hours and Amount Mapping"
        Me.lnHourAmtMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkDeleteHourAmtMapping
        '
        Me.lnkDeleteHourAmtMapping.Location = New System.Drawing.Point(468, 469)
        Me.lnkDeleteHourAmtMapping.Name = "lnkDeleteHourAmtMapping"
        Me.lnkDeleteHourAmtMapping.Size = New System.Drawing.Size(59, 13)
        Me.lnkDeleteHourAmtMapping.TabIndex = 265
        Me.lnkDeleteHourAmtMapping.TabStop = True
        Me.lnkDeleteHourAmtMapping.Text = "Delete"
        '
        'lvHourAmtMapping
        '
        Me.lvHourAmtMapping.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhSrNo, Me.colhHour, Me.colhAmount})
        Me.lvHourAmtMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvHourAmtMapping.FullRowSelect = True
        Me.lvHourAmtMapping.GridLines = True
        Me.lvHourAmtMapping.HideSelection = False
        Me.lvHourAmtMapping.Location = New System.Drawing.Point(7, 427)
        Me.lvHourAmtMapping.Name = "lvHourAmtMapping"
        Me.lvHourAmtMapping.Size = New System.Drawing.Size(455, 114)
        Me.lvHourAmtMapping.TabIndex = 263
        Me.lvHourAmtMapping.UseCompatibleStateImageBehavior = False
        Me.lvHourAmtMapping.View = System.Windows.Forms.View.Details
        '
        'objcolhSrNo
        '
        Me.objcolhSrNo.Tag = "objcolhSrNo"
        Me.objcolhSrNo.Text = ""
        Me.objcolhSrNo.Width = 0
        '
        'colhHour
        '
        Me.colhHour.Tag = "colhHour"
        Me.colhHour.Text = "Hour Head"
        Me.colhHour.Width = 190
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Amount Head"
        Me.colhAmount.Width = 190
        '
        'lnkAddHourAmtMapping
        '
        Me.lnkAddHourAmtMapping.Location = New System.Drawing.Point(468, 451)
        Me.lnkAddHourAmtMapping.Name = "lnkAddHourAmtMapping"
        Me.lnkAddHourAmtMapping.Size = New System.Drawing.Size(59, 13)
        Me.lnkAddHourAmtMapping.TabIndex = 264
        Me.lnkAddHourAmtMapping.TabStop = True
        Me.lnkAddHourAmtMapping.Text = "Add"
        '
        'cboOT1Amount
        '
        Me.cboOT1Amount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboOT1Amount.DropDownWidth = 200
        Me.cboOT1Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT1Amount.FormattingEnabled = True
        Me.cboOT1Amount.Location = New System.Drawing.Point(386, 401)
        Me.cboOT1Amount.Name = "cboOT1Amount"
        Me.cboOT1Amount.Size = New System.Drawing.Size(114, 21)
        Me.cboOT1Amount.TabIndex = 17
        '
        'cboOT1Hours
        '
        Me.cboOT1Hours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboOT1Hours.DropDownWidth = 200
        Me.cboOT1Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT1Hours.FormattingEnabled = True
        Me.cboOT1Hours.Location = New System.Drawing.Point(134, 401)
        Me.cboOT1Hours.Name = "cboOT1Hours"
        Me.cboOT1Hours.Size = New System.Drawing.Size(126, 21)
        Me.cboOT1Hours.TabIndex = 24
        '
        'lblLeaveType
        '
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(9, 90)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(119, 13)
        Me.lblLeaveType.TabIndex = 255
        Me.lblLeaveType.Text = "Leave Type"
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboLeaveType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(134, 86)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(128, 21)
        Me.cboLeaveType.TabIndex = 3
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(134, 32)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(9, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(119, 13)
        Me.lblPeriod.TabIndex = 253
        Me.lblPeriod.Text = "Period"
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(904, 5)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 0
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkAnalysisBy.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(506, 59)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 3
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 150
        Me.cboEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(133, 59)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(367, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(9, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(119, 13)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Emp. Name"
        '
        'frmTerminationPackageReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1038, 646)
        Me.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTerminationPackageReport"
        Me.Text = "frmTerminationPackageReport"
        Me.Controls.SetChildIndex(Me.EZeeCollapsibleContainer1, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvBonusHead, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveType As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblExRate As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblHourHead As System.Windows.Forms.Label
    Friend WithEvents lnHourAmtMapping As eZee.Common.eZeeLine
    Friend WithEvents lnkDeleteHourAmtMapping As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAddHourAmtMapping As System.Windows.Forms.LinkLabel
    Friend WithEvents lvHourAmtMapping As System.Windows.Forms.ListView
    Friend WithEvents objcolhSrNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhHour As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboOT1Amount As System.Windows.Forms.ComboBox
    Friend WithEvents cboOT1Hours As System.Windows.Forms.ComboBox
    Friend WithEvents lblSalaryHead As System.Windows.Forms.Label
    Friend WithEvents cboSalaryHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblCongePreavisHead As System.Windows.Forms.Label
    Friend WithEvents cboCongePreavisHead As System.Windows.Forms.ComboBox
    Friend WithEvents lbldaysinPeriod As System.Windows.Forms.Label
    Friend WithEvents cboDaysInPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboNotificationAllowance As System.Windows.Forms.ComboBox
    Friend WithEvents lblNotificationAllowance As System.Windows.Forms.Label
    Friend WithEvents CboCheckHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblCheckHead As System.Windows.Forms.Label
    Friend WithEvents cboBaseImposablehead As System.Windows.Forms.ComboBox
    Friend WithEvents lblBaseImposableHead As System.Windows.Forms.Label
    Friend WithEvents cboBaseTaxablehead As System.Windows.Forms.ComboBox
    Friend WithEvents lblBaseTaxablehead As System.Windows.Forms.Label
    Friend WithEvents cboNetPayhead As System.Windows.Forms.ComboBox
    Friend WithEvents lblNetPayhead As System.Windows.Forms.Label
    Friend WithEvents lblDeductionHead As System.Windows.Forms.Label
    Friend WithEvents lblContribution As System.Windows.Forms.Label
    Friend WithEvents objchkallDeducation As System.Windows.Forms.CheckBox
    Friend WithEvents lvDeductionHead As eZee.Common.eZeeListView
    Friend WithEvents objcolhDCheckAll As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objallchkContributaion As System.Windows.Forms.CheckBox
    Friend WithEvents lvContributaionHead As eZee.Common.eZeeListView
    Friend WithEvents objcolhCCheckAll As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCName As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents cboTerminationFactor As System.Windows.Forms.ComboBox
    Friend WithEvents lblTerminationFactor As System.Windows.Forms.Label
    Friend WithEvents lblNoticeDate As System.Windows.Forms.Label
    Friend WithEvents dtpNoticeDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboDependentHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblDependentHead As System.Windows.Forms.Label
    Friend WithEvents cboChildHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblChildHead As System.Windows.Forms.Label
    Friend WithEvents cboLoanRepayment As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanRepayment As System.Windows.Forms.Label
    Friend WithEvents cboLogement As System.Windows.Forms.ComboBox
    Friend WithEvents lblLogement As System.Windows.Forms.Label
    Friend WithEvents cboFamilyAllowance As System.Windows.Forms.ComboBox
    Friend WithEvents lblFamilyAllowance As System.Windows.Forms.Label
    Friend WithEvents cboIPRNet As System.Windows.Forms.ComboBox
    Friend WithEvents lblIPRNet As System.Windows.Forms.Label
    Friend WithEvents cboAdvanceRepayment As System.Windows.Forms.ComboBox
    Friend WithEvents lblAdvanceRepayment As System.Windows.Forms.Label
    Friend WithEvents cboRateOfNotice As System.Windows.Forms.ComboBox
    Friend WithEvents lblRateOfNotice As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvBonusHead As System.Windows.Forms.DataGridView
    Friend WithEvents objcolhIsCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objcolhTranheadUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhTranHeadCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhTranHeadName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblBonusHeads As System.Windows.Forms.Label
    Friend WithEvents txtSearchBonusHeads As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboOtherDeductions As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherDeductions As System.Windows.Forms.Label
    Friend WithEvents cboINSS35 As System.Windows.Forms.ComboBox
    Friend WithEvents lblINSS35 As System.Windows.Forms.Label
End Class
