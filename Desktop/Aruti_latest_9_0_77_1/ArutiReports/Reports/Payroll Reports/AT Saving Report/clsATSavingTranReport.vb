'************************************************************************************************************************************
'Class Name : clsATSavingTranReport.vb
'Purpose    :
'Date       :20/04/2011
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>

Public Class clsATSavingTranReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsATSavingTranReport"
    Private mstrReportId As String = enArutiReport.ATSavingTranReport
    Dim objDataOperation As clsDataOperation


#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Property Variables "

    Private mstrEmployeeName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mintAuditTypeId As Integer = 0
    Private mstrAuditTypeName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mintSchemeId As Integer = 0
    Private mstrSchemeName As String = String.Empty
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = String.Empty
    Private mintATUserId As Integer = 0
    Private mstrATUserName As String = String.Empty
    Private mdtAuditDateFrom As Date = Nothing
    Private mdtAuditDateTo As Date = Nothing
    Private mdblContributionFrom As Decimal = 0
    Private mdblContributionTo As Decimal = 0
    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIncludeInactiveEmp As Boolean = True
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    'S.SANDEEP [ 17 AUG 2011 ] -- END 

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (20 Apr 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _AuditTypeId() As Integer
        Set(ByVal value As Integer)
            mintAuditTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AuditTypeName() As String
        Set(ByVal value As String)
            mstrAuditTypeName = value
        End Set
    End Property

    Public WriteOnly Property _SchemeId() As Integer
        Set(ByVal value As Integer)
            mintSchemeId = value
        End Set
    End Property

    Public WriteOnly Property _SchemeName() As String
        Set(ByVal value As String)
            mstrSchemeName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _ATUserId() As Integer
        Set(ByVal value As Integer)
            mintATUserId = value
        End Set
    End Property

    Public WriteOnly Property _ATUserName() As String
        Set(ByVal value As String)
            mstrATUserName = value
        End Set
    End Property

    Public WriteOnly Property _AuditDateFrom() As Date
        Set(ByVal value As Date)
            mdtAuditDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _AuditDateTo() As Date
        Set(ByVal value As Date)
            mdtAuditDateTo = value
        End Set
    End Property

    Public WriteOnly Property _ContributionFrom() As Double
        Set(ByVal value As Double)
            mdblContributionFrom = value
        End Set
    End Property

    Public WriteOnly Property _ContributionTo() As Double
        Set(ByVal value As Double)
            mdblContributionTo = value
        End Set
    End Property

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch_Name() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property
    'S.SANDEEP [ 17 AUG 2011 ] -- END

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (20 Apr 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mintAuditTypeId = 0
            mstrAuditTypeName = ""
            mintSchemeId = 0
            mstrSchemeName = ""
            mintStatusId = 0
            mstrStatusName = ""
            mintATUserId = 0
            mstrATUserName = ""
            mdblContributionFrom = 0
            mdblContributionTo = 0
            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIncludeInactiveEmp = True
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            mintBranchId = -1
            mstrBranchName = ""
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@Redemption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Redemption"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))
            objDataOperation.AddParameter("@ADD", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 29, "Add"))
            objDataOperation.AddParameter("@EDIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 30, "Edit"))
            objDataOperation.AddParameter("@DELETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "Delete"))

            If mdtAuditDateFrom <> Nothing AndAlso mdtAuditDateTo <> Nothing Then
                objDataOperation.AddParameter("@AuditDateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditDateFrom))
                objDataOperation.AddParameter("@AuditDateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditDateTo))

                Me._FilterQuery &= " AND convert(varchar(10),atsvsaving_tran.auditdatetime,112) between @AuditDateFrom AND @AuditDateTo "

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Audit Date From :") & "" & mdtAuditDateFrom.Date & " " & _
                                   Language.getMessage(mstrModuleName, 33, "To") & " " & mdtAuditDateTo.Date & " "
            End If


            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND atsvsaving_tran.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 34, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintPeriodId > 0 Then
                objDataOperation.AddParameter("@PeriodunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                Me._FilterQuery &= " AND payperiodunkid = @PeriodunkId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, "Period :") & " " & mstrPeriodName & " "
            End If

            If mintAuditTypeId > 0 Then
                objDataOperation.AddParameter("@AuditTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditTypeId)
                Me._FilterQuery &= " AND atsvsaving_tran.audittype = @AuditTypeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 36, "Audit Type :") & " " & mstrAuditTypeName & " "
            End If

            If mintSchemeId > 0 Then
                objDataOperation.AddParameter("@SchemeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSchemeId)
                Me._FilterQuery &= " AND atsvsaving_tran.savingschemeunkid = @SchemeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 37, "Saving Scheme :") & " " & mstrSchemeName & " "
            End If

            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND savingstatus = @StatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 38, "Status :") & " " & mstrStatusName & " "
            End If

            If mintATUserId > 0 Then
                objDataOperation.AddParameter("@ATUserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintATUserId)
                Me._FilterQuery &= " AND atsvsaving_tran.audituserunkid = @ATUserId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 39, "Audit User :") & " " & mstrATUserName & " "
            End If


            If mdblContributionFrom > 0 Then
                objDataOperation.AddParameter("@ContributionFrom", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblContributionFrom)
                Me._FilterQuery &= " AND ISNULL(atsvsaving_contribution_tran.contribution,0.00) >= @ContributionFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 40, "Contribution From :") & " " & mdblContributionFrom & " "
            End If

            If mdblContributionTo > 0 Then
                objDataOperation.AddParameter("@mdblContributionTo", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblContributionTo)
                Me._FilterQuery &= " AND ISNULL(atsvsaving_contribution_tran.contribution,0.00) <= @mdblContributionTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 41, "Contribution To :") & " " & mdblContributionTo & " "
            End If

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 43, "Branch :") & " " & mstrBranchName & " "
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (20 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If
            'Sohail (20 Apr 2012) -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 42, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY atsvsaving_tran.employeeunkid,audittype, " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Try
            objConfig._Companyunkid = xCompanyUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIncludeInactiveEmp, True, objConfig._CurrencyFormat)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("auditdate", Language.getMessage(mstrModuleName, 1, "Audit Date")))
            iColumn_DetailReport.Add(New IColumn("employee", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("audittype", Language.getMessage(mstrModuleName, 3, "Audit Type")))
            iColumn_DetailReport.Add(New IColumn("username", Language.getMessage(mstrModuleName, 4, "Audit User")))
            iColumn_DetailReport.Add(New IColumn("contribution", Language.getMessage(mstrModuleName, 5, "Contribution")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal xDatabaseName As String _
                                           , ByVal xUserUnkid As Integer _
                                           , ByVal xYearUnkid As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal xPeriodStart As Date _
                                           , ByVal xPeriodEnd As Date _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                           , ByVal blnApplyUserAccessFilter As Boolean _
                                           , ByVal strfmtCurrency As String _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strfmtCurrency]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation



            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.

            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'StrQ = " SELECT voucherNo,hremployee_master.employeecode," & _
            '          " ISNULL(firstname,'') + ' ' + ISNULL(surname,'') 'employee', " & _
            '          " cfcommon_period_tran.period_name, " & _
            '          " CASE WHEN savingstatus = 1 THEN @InProgress " & _
            '          "          WHEN savingstatus = 2 THEN @OnHold" & _
            '          "          WHEN savingstatus = 3 THEN  @Redemption " & _
            '          "          WHEN savingstatus = 4 THEN @Completed END [status] , " & _
            '          " savingschemename AS [scheme], " & _
            '          " contribution , " & _
            '          " total_contribution , " & _
            '          " interest_rate,interest_amount,balance_amount,broughtforward, " & _
            '          " CASE WHEN audittype = 1 THEN @ADD " & _
            '          "         WHEN audittype = 2 THEN  @EDIT " & _
            '          "         WHEN audittype = 3 THEN @DELETE " & _
            '          " END audittype, " & _
            '          " hrmsConfiguration..cfuser_master.username , " & _
            '          " CONVERT(VARCHAR(10), auditdatetime,112) auditdate, " & _
            '          " CONVERT(VARCHAR(10), auditdatetime,108) audittime, " & _
            '          " ip, machine_name " & _
            '          " FROM atsvsaving_tran  " & _
            '          " JOIN hremployee_master ON hremployee_master.employeeunkid = atsvsaving_tran.employeeunkid " & _
            '          " JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = atsvsaving_tran.payperiodunkid " & _
            '          " JOIN svsavingscheme_master ON svsavingscheme_master.savingschemeunkid = atsvsaving_tran.savingschemeunkid " & _
            '          " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atsvsaving_tran.audituserunkid " & _
            '           "WHERE 1 = 1 "

            'StrQ = "SELECT " & _
            '        "	  voucherNo " & _
            '        "	 ,hremployee_master.employeecode " & _
            '        "	 ,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') employee " & _
            '        "	 ,cfcommon_period_tran.period_name " & _
            '        "	 ,CASE WHEN savingstatus = 1 THEN @InProgress " & _
            '          "          WHEN savingstatus = 2 THEN @OnHold" & _
            '          "          WHEN savingstatus = 3 THEN  @Redemption " & _
            '        "		   WHEN savingstatus = 4 THEN @Completed END status " & _
            '        "	,savingschemename AS scheme " & _
            '        "	,contribution " & _
            '        "	,total_contribution " & _
            '        "	,interest_rate " & _
            '        "	,interest_amount " & _
            '        "	,balance_amount " & _
            '        "	,broughtforward " & _
            '        "	,CASE WHEN audittype = 1 THEN @ADD " & _
            '          "         WHEN audittype = 2 THEN  @EDIT " & _
            '        "		  WHEN audittype = 3 THEN @DELETE END audittype " & _
            '        "	,hrmsConfiguration..cfuser_master.username " & _
            '        "	,CONVERT(VARCHAR(10), auditdatetime, 112) auditdate " & _
            '        "	,CONVERT(VARCHAR(10), auditdatetime, 108) audittime " & _
            '        "	,ip " & _
            '        "	,machine_name " & _
            '          " FROM atsvsaving_tran  " & _
            '          " JOIN hremployee_master ON hremployee_master.employeeunkid = atsvsaving_tran.employeeunkid " & _
            '          " JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = atsvsaving_tran.payperiodunkid " & _
            '          " JOIN svsavingscheme_master ON svsavingscheme_master.savingschemeunkid = atsvsaving_tran.savingschemeunkid " & _
            '          " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atsvsaving_tran.audituserunkid " & _
            '           "WHERE 1 = 1 "

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            'Pinkal (27-Feb-2013) -- End

            'If mblnIncludeInactiveEmp = False Then
            '    'Sohail (20 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'StrQ &= " AND hremployee_master.isactive = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
            '    'Sohail (20 Apr 2012) -- End
            'End If

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            'If mintBranchId > 0 Then
            '    StrQ &= " AND hremployee_master.stationunkid = " & mintBranchId
            'End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (28 Jun 2011) -- Start
            'Issue : According to prvilege that lower level user should not see superior level employees.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            'Sohail (20 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            'StrQ &= UserAccessLevel._AccessLevelFilterString
            'Sohail (20 Apr 2012) -- End
            'End If
            'Sohail (28 Jun 2011) -- End
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 


            StrQ = "SELECT DISTINCT " & _
                         " atsvsaving_tran.savingtranunkid " & _
                         ", atsvsaving_tran.voucherno " & _
                         ", atsvsaving_tran.effectivedate " & _
                         ", cfcommon_period_tran.periodunkid " & _
                         ", cfcommon_period_tran.period_name " & _
                         ", atsvsaving_tran.employeeunkid " & _
                         ", ISNULL(hremployee_master.employeecode, '') employeecode " & _
                         ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') employee " & _
                         ", atsvsaving_tran.savingschemeunkid " & _
                         ", svsavingscheme_master.savingschemename AS scheme " & _
                         ", atsvsaving_tran.savingstatus " & _
                         ",CASE WHEN svsaving_status_tran.statusunkid = 1 THEN @InProgress " & _
                         "          WHEN svsaving_status_tran.statusunkid = 2 THEN @OnHold" & _
                         "          WHEN svsaving_status_tran.statusunkid = 3 THEN  @Redemption " & _
                         "		   WHEN svsaving_status_tran.statusunkid = 4 THEN @Completed END status " & _
                         ", atsvsaving_tran.stopdate " & _
                         ", atsvsaving_tran.balance_amount " & _
                         ", atsvsaving_tran.broughtforward " & _
                         ", ISNULL(atsvsaving_tran.bf_amount, 0.00) AS bf_amount " & _
                         ", ISNULL(atsvsaving_tran.bf_balance, 0.00) AS bf_balance " & _
                         ", CASE WHEN atsvsaving_tran.audittype = 1 THEN @ADD " & _
                         "         WHEN atsvsaving_tran.audittype = 2 THEN  @EDIT " & _
                         "		  WHEN atsvsaving_tran.audittype = 3 THEN @DELETE END audittype " & _
                         ", hrmsConfiguration..cfuser_master.username " & _
                         ", CONVERT(VARCHAR(10), atsvsaving_tran.auditdatetime, 112) auditdate " & _
                         ", CONVERT(VARCHAR(10), atsvsaving_tran.auditdatetime, 108) audittime " & _
                         ", atsvsaving_tran.ip " & _
                         ", atsvsaving_tran.machine_name " & _
                         ", ISNULL(contributionperiod.period_name,'') contribution_Period " & _
                         ", ISNULL(atsvsaving_contribution_tran.contribution,0.00) AS contribution " & _
                         ", ISNULL(Interestperiod.period_name,'') AS interest_Period " & _
                         ", atsvsaving_interest_rate_tran.effectivedate " & _
                         ", ISNULL(atsvsaving_interest_rate_tran.interest_rate,0.00) AS interest_rate " & _
                    " FROM atsvsaving_tran " & _
                    " JOIN svsaving_status_tran ON svsaving_status_tran.savingtranunkid = atsvsaving_tran.savingtranunkid " & _
                    " LEFT JOIN svsavingscheme_master ON atsvsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
                    " LEFT JOIN hremployee_master ON atsvsaving_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    " LEFT JOIN cfcommon_period_tran ON atsvsaving_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                    " AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.modulerefid =  " & enModuleReference.Payroll & _
                    " LEFT JOIN cfcommon_period_tran AS statusperiod ON statusperiod.periodunkid = svsaving_status_tran.periodunkid " & _
                    " AND statusperiod.isactive = 1 AND statusperiod.modulerefid = " & enModuleReference.Payroll & _
                    " LEFT JOIN atsvsaving_contribution_tran ON atsvsaving_tran.savingtranunkid = atsvsaving_contribution_tran.savingtranunkid " & _
                    " LEFT JOIN cfcommon_period_tran AS contributionperiod ON contributionperiod.periodunkid = atsvsaving_contribution_tran.periodunkid " & _
                    " AND contributionperiod.isactive = 1 AND contributionperiod.modulerefid = " & enModuleReference.Payroll & _
                    " LEFT JOIN atsvsaving_interest_rate_tran ON atsvsaving_tran.savingtranunkid = atsvsaving_interest_rate_tran.savingtranunkid " & _
                    " LEFT JOIN cfcommon_period_tran AS Interestperiod ON Interestperiod.periodunkid = atsvsaving_interest_rate_tran.periodunkid " & _
                    " AND Interestperiod.isactive = 1 AND Interestperiod.modulerefid = " & enModuleReference.Payroll & _
                    " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atsvsaving_tran.audituserunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                     "( " & _
                     "    SELECT " & _
                     "         stationunkid " & _
                     "        ,deptgroupunkid " & _
                     "        ,departmentunkid " & _
                     "        ,sectiongroupunkid " & _
                     "        ,sectionunkid " & _
                     "        ,unitgroupunkid " & _
                     "        ,unitunkid " & _
                     "        ,teamunkid " & _
                     "        ,classgroupunkid " & _
                     "        ,classunkid " & _
                     "        ,employeeunkid " & _
                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                     "    FROM hremployee_transfer_tran " & _
                     "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                     ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= " WHERE 1 = 1 "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            'Sohail (21 Aug 2015) -- End

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.stationunkid = " & mintBranchId
                StrQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If




            'Pinkal (12 Jan 2015) -- End



            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then

                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column2") = dtRow.Item("employee")
                rpt_Rows.Item("Column3") = dtRow.Item("voucherNo")
                rpt_Rows.Item("Column4") = dtRow.Item("period_name")
                rpt_Rows.Item("Column5") = dtRow.Item("status")
                rpt_Rows.Item("Column6") = dtRow.Item("scheme")

                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.
                'rpt_Rows.Item("Column7") = Format(CDec(dtRow.Item("contribution")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column8") = Format(CDec(dtRow.Item("total_contribution")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column9") = Format(CDec(dtRow.Item("interest_rate")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column10") = Format(CDec(dtRow.Item("interest_amount")), GUI.fmtCurrency)
                rpt_Rows.Item("Column7") = dtRow.Item("contribution_Period").ToString()
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column8") = Format(CDec(dtRow.Item("contribution")), GUI.fmtCurrency)
                rpt_Rows.Item("Column8") = Format(CDec(dtRow.Item("contribution")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Rows.Item("Column9") = CDate(dtRow.Item("effectivedate")).ToShortDateString
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column10") = Format(CDec(dtRow.Item("interest_rate")), GUI.fmtCurrency)
                rpt_Rows.Item("Column10") = Format(CDec(dtRow.Item("interest_rate")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                'Pinkal (12 Jan 2015) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column11") = Format(CDec(dtRow.Item("balance_amount")), GUI.fmtCurrency)
                rpt_Rows.Item("Column11") = Format(CDec(dtRow.Item("balance_amount")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Rows.Item("Column13") = dtRow.Item("audittype")
                rpt_Rows.Item("Column14") = dtRow.Item("username")
                rpt_Rows.Item("Column15") = eZeeDate.convertDate(dtRow.Item("auditdate").ToString).ToShortDateString & vbCrLf & dtRow.Item("audittime").ToString
                rpt_Rows.Item("Column16") = dtRow.Item("ip")
                rpt_Rows.Item("Column17") = dtRow.Item("machine_name")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptATSavingTran

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription")

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 6, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 7, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 8, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 9, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 10, "Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 11, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtVoucherNo", Language.getMessage(mstrModuleName, 12, "Voucher No"))
            Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 13, "Period"))
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 14, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtScheme", Language.getMessage(mstrModuleName, 15, "Scheme"))

            'Pinkal (12 Jan 2015) -- Start
            'Enhancement - CHANGE IN SAVINGS MODULE.
            'Call ReportFunction.TextChange(objRpt, "txtContribution", Language.getMessage(mstrModuleName, 16, "Contribution"))
            'Call ReportFunction.TextChange(objRpt, "txttotalContribution", Language.getMessage(mstrModuleName, 17, "Total Contribution"))
            'Call ReportFunction.TextChange(objRpt, "txtInterestRate", Language.getMessage(mstrModuleName, 18, "Interest Rate"))
            'Call ReportFunction.TextChange(objRpt, "txtInterestAmt", Language.getMessage(mstrModuleName, 19, "Interest Amt"))
            Call ReportFunction.TextChange(objRpt, "txtContribution", Language.getMessage(mstrModuleName, 29, "Contribution Period"))
            Call ReportFunction.TextChange(objRpt, "txttotalContribution", Language.getMessage(mstrModuleName, 16, "Contribution"))
            Call ReportFunction.TextChange(objRpt, "txtInterestRate", Language.getMessage(mstrModuleName, 30, "Int. Effective Date"))
            Call ReportFunction.TextChange(objRpt, "txtInterestAmt", Language.getMessage(mstrModuleName, 18, "Interest Rate"))
            'Pinkal (12 Jan 2015) -- End


            Call ReportFunction.TextChange(objRpt, "txtBalance", Language.getMessage(mstrModuleName, 20, "Balance"))
            Call ReportFunction.TextChange(objRpt, "txtaudittype", Language.getMessage(mstrModuleName, 21, "Audit Type"))
            Call ReportFunction.TextChange(objRpt, "txtUsrName", Language.getMessage(mstrModuleName, 22, "User"))
            Call ReportFunction.TextChange(objRpt, "txtAuditDate", Language.getMessage(mstrModuleName, 23, "Audit Date"))
            Call ReportFunction.TextChange(objRpt, "txtIp", Language.getMessage(mstrModuleName, 24, "IP"))
            Call ReportFunction.TextChange(objRpt, "txtMName", Language.getMessage(mstrModuleName, 25, "Machine"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 26, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 27, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 28, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Audit Date")
            Language.setMessage(mstrModuleName, 2, "Employee Name")
            Language.setMessage(mstrModuleName, 3, "Audit Type")
            Language.setMessage(mstrModuleName, 4, "Audit User")
            Language.setMessage(mstrModuleName, 5, "Contribution")
            Language.setMessage(mstrModuleName, 6, "Prepared By :")
            Language.setMessage(mstrModuleName, 7, "Checked By :")
            Language.setMessage(mstrModuleName, 8, "Approved By :")
            Language.setMessage(mstrModuleName, 9, "Received By :")
            Language.setMessage(mstrModuleName, 10, "Code :")
            Language.setMessage(mstrModuleName, 11, "Employee :")
            Language.setMessage(mstrModuleName, 12, "Voucher No")
            Language.setMessage(mstrModuleName, 13, "Period")
            Language.setMessage(mstrModuleName, 14, "Status")
            Language.setMessage(mstrModuleName, 15, "Scheme")
            Language.setMessage(mstrModuleName, 16, "Contribution")
            Language.setMessage(mstrModuleName, 17, "Total Contribution")
            Language.setMessage(mstrModuleName, 18, "Interest Rate")
            Language.setMessage(mstrModuleName, 19, "Interest Amt")
            Language.setMessage(mstrModuleName, 20, "Balance")
            Language.setMessage(mstrModuleName, 21, "Audit Type")
            Language.setMessage(mstrModuleName, 22, "User")
            Language.setMessage(mstrModuleName, 23, "Audit Date")
            Language.setMessage(mstrModuleName, 24, "IP")
            Language.setMessage(mstrModuleName, 25, "Machine")
            Language.setMessage(mstrModuleName, 26, "Printed By :")
            Language.setMessage(mstrModuleName, 27, "Printed Date :")
            Language.setMessage(mstrModuleName, 28, "Page :")
            Language.setMessage(mstrModuleName, 29, "Add")
            Language.setMessage(mstrModuleName, 30, "Edit")
            Language.setMessage(mstrModuleName, 31, "Delete")
            Language.setMessage(mstrModuleName, 32, "Audit Date From :")
            Language.setMessage(mstrModuleName, 33, "To")
            Language.setMessage(mstrModuleName, 34, "Employee :")
            Language.setMessage(mstrModuleName, 35, "Period :")
            Language.setMessage(mstrModuleName, 36, "Audit Type :")
            Language.setMessage(mstrModuleName, 37, "Saving Scheme :")
            Language.setMessage(mstrModuleName, 38, "Status :")
            Language.setMessage(mstrModuleName, 39, "Audit User :")
            Language.setMessage(mstrModuleName, 40, "Contribution From :")
            Language.setMessage(mstrModuleName, 41, "Contribution To :")
            Language.setMessage(mstrModuleName, 42, "Order By :")
            Language.setMessage(mstrModuleName, 43, "Branch :")
            Language.setMessage("clsMasterData", 96, "In Progress")
            Language.setMessage("clsMasterData", 97, "On Hold")
            Language.setMessage("clsMasterData", 98, "Redemption")
            Language.setMessage("clsMasterData", 100, "Completed")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
