'************************************************************************************************************************************
'Class Name : clsBankSummaryReport.vb
'Purpose    :
'Date       :10/12/2010
'Written By :Vimal M. Gohil
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Vimal M. Gohil
''' </summary>
Public Class clsBankSummaryReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsBankSummaryReport"
    Private mstrReportId As String = enArutiReport.BankSummary
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = String.Empty

    'Vimal (30 Nov 2010) -- Start 
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = ""
    'Vimal (30 Nov 2010) -- End

    Private mintBankId As Integer = 0
    Private mstrBankName As String = String.Empty

    Private mintBranchId As Integer = 0
    Private mstrBranchName As String = String.Empty

    Private mstrAmountFrom As String = String.Empty
    Private mstrAmountTo As String = String.Empty

    Private mstrOrderByQuery As String = ""

    'Sohail (16 Dec 2011) -- Start
    Private mintCurrencyId As Integer = 0
    Private mstrCurrencyName As String = ""
    Private mblnIsMultipleCurrency As Boolean = False
    'Sohail (16 Dec 2011) -- End


    'Anjan [14 Mar 2014] -- Start
    'ENHANCEMENT : Requested by Rutta
    Private mblnShowCountry As Boolean = False
    'Anjan [14 Mar 2014 ] -- End
    Private mblnShowAddress As Boolean = False 'Sohail (24 Mar 2014)

#End Region

#Region " Properties "
    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _BankId() As Integer
        Set(ByVal value As Integer)
            mintBankId = value
        End Set
    End Property

    Public WriteOnly Property _BankName() As String
        Set(ByVal value As String)
            mstrBankName = value
        End Set
    End Property

    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _BranchName() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property

    Public WriteOnly Property _AmountFrom() As String
        Set(ByVal value As String)
            mstrAmountFrom = value
        End Set
    End Property

    Public WriteOnly Property _AmountTo() As String
        Set(ByVal value As String)
            mstrAmountTo = value
        End Set
    End Property

    'Vimal (30 Nov 2010) -- Start 
    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property
    'Vimal (30 Nov 2010) -- End

    'Sohail (16 Dec 2011) -- Start
    Public WriteOnly Property _CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyName() As String
        Set(ByVal value As String)
            mstrCurrencyName = value
        End Set
    End Property
    'Sohail (16 Dec 2011) -- End


    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    'Anjan [14 Mar 2014] -- Start
    'ENHANCEMENT : Requested by Rutta
    Public WriteOnly Property _ShowCountry() As Boolean
        Set(ByVal value As Boolean)
            mblnShowCountry = value
        End Set
    End Property
    'Anjan [14 Mar 2014 ] -- End

    'Sohail (24 Mar 2014) -- Start
    'ENHANCEMENT - Show Address on Bank Summary report.
    Public WriteOnly Property _ShowAddress() As Boolean
        Set(ByVal value As Boolean)
            mblnShowAddress = value
        End Set
    End Property
    'Sohail (24 Mar 2014) -- End

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""

            mintBankId = 0
            mstrBankName = ""

            mintBranchId = 0
            mstrBranchName = ""

            mstrAmountFrom = ""
            mstrAmountTo = ""

            'Vimal (30 Nov 2010) -- Start 
            mintPeriodId = 0
            mstrPeriodName = ""
            'Vimal (30 Nov 2010) -- End

            mstrOrderByQuery = ""

            'Sohail (16 Dec 2011) -- Start
            mintCurrencyId = 0
            mstrCurrencyName = ""
            'Sohail (16 Dec 2011) -- End

            'Anjan [14 Mar 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            mblnShowCountry = False
            'Anjan [14 Mar 2014 ] -- End
            mblnShowAddress = False 'Sohail (24 Mar 2014)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""


        Try
            'Vimal (30 Nov 2010) -- Start 
            If mintPeriodId > 0 Then
                objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                Me._FilterQuery &= " AND PeriodId = @PeriodId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, " Period : ") & " " & mstrPeriodName & " "
            End If
            'Vimal (30 Nov 2010) -- End


            If mintBankId > 0 Then
                objDataOperation.AddParameter("@BankId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBankId)
                Me._FilterQuery &= " AND BankId = @BankId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, " Bank : ") & " " & mstrBankName & " "
            End If

            If mintBranchId > 0 Then
                objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
                Me._FilterQuery &= " AND BranchId = @BranchId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, " Branch : ") & " " & mstrBranchName & " "
            End If

            If mstrAmountFrom <> "" AndAlso mstrAmountTo <> "" Then
                objDataOperation.AddParameter("@AmountFrom", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrAmountFrom))
                objDataOperation.AddParameter("@AmountTo", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrAmountTo))
                Me._FilterQuery &= " AND  Amount BETWEEN @AmountFrom And @AmountTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, " Amount From : ") & " " & mstrAmountFrom & " " & _
                                   Language.getMessage(mstrModuleName, 17, " To : ") & " " & mstrAmountTo & " "
            End If

            'Sohail (16 Dec 2011) -- Start
            If mintCurrencyId > 0 Then
                objDataOperation.AddParameter("@Currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyId)
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= " AND paidcurrencyid = @Currencyunkid "
                Me._FilterQuery &= " AND BankSummary.countryunkid = @Currencyunkid "
                'Sohail (03 Sep 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, "Paid Currency : ") & mstrCurrencyName & " "
            End If
            'Sohail (16 Dec 2011) -- End

            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= " ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Try
            Company._Object._Companyunkid = xCompanyUnkid
            ConfigParameter._Object._Companyunkid = xCompanyUnkid
            User._Object._Userunkid = xUserUnkid

            objConfig._Companyunkid = xCompanyUnkid

            objRpt = Generate_DetailReport(objConfig._RoundOff_Type, objConfig._CurrencyFormat)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
            objConfig = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property
    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("SrNo", Language.getMessage(mstrModuleName, 2, "No")))
            iColumn_DetailReport.Add(New IColumn("PeriodName", Language.getMessage(mstrModuleName, 3, "Period Name")))
            iColumn_DetailReport.Add(New IColumn("BankName", Language.getMessage(mstrModuleName, 4, "Bank")))
            iColumn_DetailReport.Add(New IColumn("BranchName", Language.getMessage(mstrModuleName, 5, "Branch Name")))
            iColumn_DetailReport.Add(New IColumn("Address", Language.getMessage(mstrModuleName, 6, "Address")))
            iColumn_DetailReport.Add(New IColumn("Country", Language.getMessage(mstrModuleName, 7, "Country")))
            iColumn_DetailReport.Add(New IColumn("Amount", Language.getMessage(mstrModuleName, 8, "Amount Paid")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    Private Function Generate_DetailReport(ByVal dblRoundOff_Type As Double, ByVal strFmtCurrency As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [dblRoundOff_Type, strFmtCurrency]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Vimal (30 Nov 2010) -- Start 
            'Issue: Insert Bank Address, Country
            'StrQ = "SELECT  * " & _
            '        "FROM    ( SELECT ROW_NUMBER()  OVER (ORDER BY  PeriodName) As SrNo " & _
            '               ",PeriodName " & _
            '               ",PeriodId " & _
            '               ",BranchId " & _
            '               ",BankId " & _
            '               ",BankName " & _
            '               ",BranchName " & _
            '               ",SUM(Amount) AS Amount " & _
            '        "FROM " & _
            '        "(SELECT  cfcommon_period_tran.period_name AS PeriodName " & _
            '               ", cfcommon_period_tran.periodunkid AS PeriodId " & _
            '               ", hrmsConfiguration..cfbankbranch_master.branchunkid AS BranchId " & _
            '               ", hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankId " & _
            '               ", hrmsConfiguration..cfpayrollgroup_master.groupname AS BankName " & _
            '               ", hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
            '               ", prempsalary_tran.amount AS Amount " & _
            '        "FROM prempsalary_tran " & _
            '        "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid " & _
            '        "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
            '        "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
            '        "JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
            '        "JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '        "WHERE ISNULL(prempsalary_tran.isvoid,0) = 0 " & _
            '              "AND ISNULL(premployee_bank_tran.isvoid,0) = 0 " & _
            '              "AND hrmsConfiguration..cfbankbranch_master.isactive = 1 " & _
            '              "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1 " & _
            '              "AND ISNULL(prpayment_tran.isvoid,0) = 0 " & _
            '              "AND cfcommon_period_tran.isactive = 1 " & _
            '        ") AS BankSummary " & _
            '        "GROUP BY " & _
            '                    "PeriodName " & _
            '                  ", PeriodId " & _
            '                  ", BranchId " & _
            '                  ", BankId " & _
            '                  ", BankName " & _
            '                  ", BranchName " & _
            '        ") AS BankSummary " & _
            '        "WHERE   1 = 1 "

            'Sohail (16 Dec 2011) -- Start
            'TRA - Multi currency Payment List, for Bank Payment and Cash Payment List.
            'StrQ = "SELECT  SrNo " & _
            '                 ", PeriodName " & _
            '                 ", PeriodId " & _
            '                 ", BranchId " & _
            '                 ", BankId " & _
            '                 ", BankName " & _
            '                 ", BranchName " & _
            '                 ", Address " & _
            '                 ", Country " & _
            '                 ", Amount " & _
            '        "FROM ( SELECT ROW_NUMBER() OVER ( ORDER BY PeriodName ) AS SrNo " & _
            '                          ", PeriodName " & _
            '                          ", PeriodId " & _
            '                          ", BranchId " & _
            '                          ", BankId " & _
            '                          ", BankName " & _
            '                          ", BranchName " & _
            '                          ", Address " & _
            '                          ", Country " & _
            '                          ", SUM(Amount) AS Amount " & _
            '                  "FROM ( SELECT cfcommon_period_tran.period_name AS PeriodName " & _
            '                                      ", cfcommon_period_tran.periodunkid AS PeriodId " & _
            '                                      ", hrmsConfiguration..cfbankbranch_master.branchunkid AS BranchId " & _
            '                                      ", hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankId " & _
            '                                      ", hrmsConfiguration..cfpayrollgroup_master.groupname AS BankName " & _
            '                                      ", hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
            '                                      ", ISNULL(hrmsConfiguration..cfbankbranch_master.address1,'') + ' ' + ISNULL(hrmsConfiguration..cfbankbranch_master.address2,'') + ' ' + ISNULL(hrmsConfiguration..cfcity_master.name,'') + ' ' + ISNULL(hrmsConfiguration..cfstate_master.name,'') + ' ' + ISNULL(hrmsConfiguration..cfzipcode_master.zipcode_no,'') AS Address " & _
            '                                      ", ISNULL(hrmsConfiguration..cfcountry_master.country_name,' ') AS Country " & _
            '                                      ", prempsalary_tran.amount AS Amount " & _
            '                              "FROM prempsalary_tran " & _
            '                                        "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid " & _
            '                                        "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
            '                                        "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
            '                                        "JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
            '                                        "JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                                        "LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfbankbranch_master.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '                                        "LEFT JOIN hrmsConfiguration..cfstate_master ON hrmsConfiguration..cfbankbranch_master.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
            '                                        "LEFT JOIN hrmsConfiguration..cfzipcode_master ON hrmsConfiguration..cfbankbranch_master.pincodeunkid = hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
            '                                        "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfbankbranch_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
            '                              "WHERE ISNULL(prempsalary_tran.isvoid, 0) = 0 " & _
            '                                        "AND ISNULL(premployee_bank_tran.isvoid, 0) = 0 " & _
            '                                        "AND hrmsConfiguration..cfbankbranch_master.isactive = 1 " & _
            '                                        "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1 " & _
            '                                        "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
            '                                        "AND cfcommon_period_tran.isactive = 1 " & _
            '                            ") AS BankSummary " & _
            '                  "GROUP BY  PeriodName " & _
            '                          ", PeriodId " & _
            '                          ", BranchId " & _
            '                          ", BankId " & _
            '                          ", BankName " & _
            '                          ", BranchName " & _
            '                          ", Address " & _
            '                          ", Country " & _
            '                ") AS BankSummary " & _
            '        "WHERE   1 = 1 "
            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'StrQ = "SELECT  SrNo " & _
            '                 ", PeriodName " & _
            '                 ", PeriodId " & _
            '                 ", BranchId " & _
            '                 ", BankId " & _
            '                 ", BankName " & _
            '                 ", BranchName " & _
            '                 ", Address " & _
            '                 ", Country " & _
            '                 ", Amount " & _
            '              ", paidcurrencyid " & _
            '              ", cfexchange_rate.currency_name " & _
            '              ", cfexchange_rate.currency_sign " & _
            '        "FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY end_date ) AS SrNo " & _
            '                          ", PeriodName " & _
            '                          ", PeriodId " & _
            '                          ", BranchId " & _
            '                          ", BankId " & _
            '                          ", BankName " & _
            '                          ", BranchName " & _
            '                          ", Address " & _
            '                          ", Country " & _
            '                          ", end_date " & _
            '                          ", paidcurrencyid " & _
            '                          ", SUM(expaidamt) AS Amount " & _
            '                  "FROM ( SELECT cfcommon_period_tran.period_name AS PeriodName " & _
            '                                      ", cfcommon_period_tran.periodunkid AS PeriodId " & _
            '                                      ", hrmsConfiguration..cfbankbranch_master.branchunkid AS BranchId " & _
            '                                      ", hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankId " & _
            '                                      ", hrmsConfiguration..cfpayrollgroup_master.groupname AS BankName " & _
            '                                      ", hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
            '                                      ", ISNULL(hrmsConfiguration..cfbankbranch_master.address1, '') + ' ' + ISNULL(hrmsConfiguration..cfbankbranch_master.address2, '') + ' ' " & _
            '                                        "+ ISNULL(hrmsConfiguration..cfcity_master.name, '') + ' ' + ISNULL(hrmsConfiguration..cfstate_master.name, '') + ' ' " & _
            '                                        "+ ISNULL(hrmsConfiguration..cfzipcode_master.zipcode_no, '') AS Address " & _
            '                                      ", ISNULL(hrmsConfiguration..cfcountry_master.country_name,' ') AS Country " & _
            '                                      ", end_date " & _
            '                                      ", prempsalary_tran.expaidamt " & _
            '                                      ", prempsalary_tran.paidcurrencyid " & _
            '                              "FROM prempsalary_tran " & _
            '                                        "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid " & _
            '                                        "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
            '                                        "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
            '                                        "JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
            '                                        "JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                                        "LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfbankbranch_master.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '                                        "LEFT JOIN hrmsConfiguration..cfstate_master ON hrmsConfiguration..cfbankbranch_master.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
            '                                        "LEFT JOIN hrmsConfiguration..cfzipcode_master ON hrmsConfiguration..cfbankbranch_master.pincodeunkid = hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
            '                                        "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfbankbranch_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
            '                              "WHERE ISNULL(prempsalary_tran.isvoid, 0) = 0 " & _
            '                                        "AND ISNULL(premployee_bank_tran.isvoid, 0) = 0 " & _
            '                                        "AND hrmsConfiguration..cfbankbranch_master.isactive = 1 " & _
            '                                        "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1 " & _
            '                                        "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
            '                                        "AND cfcommon_period_tran.isactive = 1 " & _
            '                            ") AS BankSummary " & _
            '                  "GROUP BY  PeriodName " & _
            '                          ", PeriodId " & _
            '                          ", BranchId " & _
            '                          ", BankId " & _
            '                          ", BankName " & _
            '                          ", BranchName " & _
            '                          ", Address " & _
            '                          ", Country " & _
            '                          ", end_date " & _
            '                          ", paidcurrencyid " & _
            '                ") AS BankSummary " & _
            '                "LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = BankSummary.paidcurrencyid " & _
            '        "WHERE   1 = 1 "
            StrQ = "SELECT  SrNo " & _
                             ", PeriodName " & _
                             ", PeriodId " & _
                             ", BranchId " & _
                             ", BankId " & _
                             ", BankName " & _
                             ", BranchName " & _
                             ", Address " & _
                             ", Country " & _
                             ", Amount " & _
                             ", currency_name " & _
                             ", currency_sign " & _
                             ", countryunkid " & _
                             ", EmpCount " & _
                    "FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY end_date ) AS SrNo " & _
                                      ", PeriodName " & _
                                      ", PeriodId " & _
                                      ", BranchId " & _
                                      ", BankId " & _
                                      ", BankName " & _
                                      ", BranchName " & _
                                      ", Address " & _
                                      ", Country " & _
                                      ", end_date " & _
                                      ", paidcurrencyid " & _
                                      ", SUM(expaidamt) AS Amount " & _
                                      ", countryunkid " & _
                                      ", currency_name " & _
                                      ", currency_sign " & _
                                      ", COUNT(*) AS EmpCount " & _
                              "FROM ( SELECT cfcommon_period_tran.period_name AS PeriodName " & _
                                                  ", cfcommon_period_tran.periodunkid AS PeriodId " & _
                                                  ", hrmsConfiguration..cfbankbranch_master.branchunkid AS BranchId " & _
                                                  ", hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankId " & _
                                                  ", hrmsConfiguration..cfpayrollgroup_master.groupname AS BankName " & _
                                                  ", hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
                                                  ", ISNULL(hrmsConfiguration..cfbankbranch_master.address1, '') + ' ' + ISNULL(hrmsConfiguration..cfbankbranch_master.address2, '') + ' ' " & _
                                                    "+ ISNULL(hrmsConfiguration..cfcity_master.name, '') + ' ' + ISNULL(hrmsConfiguration..cfstate_master.name, '') + ' ' " & _
                                                    "+ ISNULL(hrmsConfiguration..cfzipcode_master.zipcode_no, '') AS Address " & _
                                                  ", ISNULL(hrmsConfiguration..cfcountry_master.country_name,' ') AS Country " & _
                                                  ", end_date " & _
                                                  ", prempsalary_tran.expaidamt " & _
                                                  ", prempsalary_tran.paidcurrencyid " & _
                                                  ", prpayment_tran.countryunkid " & _
                                                  ", cfexchange_rate.currency_name " & _
                                                  ", cfexchange_rate.currency_sign " & _
                                          "FROM prempsalary_tran " & _
                                                    "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid " & _
                                                    "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                                                    "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                                                    "JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                                    "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                                    "JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                                    "JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = prpayment_tran.paidcurrencyid " & _
                                                    "LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfbankbranch_master.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
                                                    "LEFT JOIN hrmsConfiguration..cfstate_master ON hrmsConfiguration..cfbankbranch_master.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
                                                    "LEFT JOIN hrmsConfiguration..cfzipcode_master ON hrmsConfiguration..cfbankbranch_master.pincodeunkid = hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
                                                    "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfbankbranch_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                                          "WHERE ISNULL(prempsalary_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(premployee_bank_tran.isvoid, 0) = 0 " & _
                                                    "AND hrmsConfiguration..cfbankbranch_master.isactive = 1 " & _
                                                    "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1 " & _
                                                    "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                    "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                                    "AND cfcommon_period_tran.isactive = 1 " & _
                                        ") AS BankSummary " & _
                              "GROUP BY  PeriodName " & _
                                      ", PeriodId " & _
                                      ", BranchId " & _
                                      ", BankId " & _
                                      ", BankName " & _
                                      ", BranchName " & _
                                      ", Address " & _
                                      ", Country " & _
                                      ", end_date " & _
                                      ", paidcurrencyid " & _
                                      ", countryunkid " & _
                                      ", currency_name " & _
                                      ", currency_sign " & _
                            ") AS BankSummary " & _
                    "WHERE   1 = 1 "
            'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]
            'Sohail (27 Feb 2013) - [EmpCount]
            'Sohail (03 Sep 2012) -- End
            'Sohail (16 Dec 2011) -- End
            'Vimal (30 Nov 2010) -- End


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport



            'Pinkal (05-Jul-2011) -- Start
            'ISSUE : DECIMAL CHANGE IN REPORTS

            Dim mintPeriodId As Integer = -1
            Dim mdblGroupTotal As Decimal = 0
            Dim mblGrandTotal As Decimal = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("PeriodId")

                If mintPeriodId <> CInt(dtRow.Item("PeriodId")) Then
                    mintPeriodId = CInt(dtRow.Item("PeriodId"))
                    mdblGroupTotal = 0
                End If


                'Anjan (28 May 2012)-Start
                'ENHANCEMENT : BBL COMMENTS on Rutta Request
                'mdblGroupTotal += CDec(dtRow.Item("Amount"))
                'mblGrandTotal += CDec(dtRow.Item("expaidamt"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'mdblGroupTotal += CDec(Format(Rounding.BRound(CDec(dtRow.Item("Amount")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency))
                'mblGrandTotal += CDec(Format(Rounding.BRound(CDec(dtRow.Item("Amount")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency))
                'Sohail (01 Nov 2017) -- Start
                'Issue - 70.1 - SUPPORT 0001438 - SUMATRA - Bank payment list report and bank payment list summary report are not matching with the payroll summary report.
                'mdblGroupTotal += CDec(Format(Rounding.BRound(CDec(dtRow.Item("Amount")), dblRoundOff_Type), strFmtCurrency))
                'mblGrandTotal += CDec(Format(Rounding.BRound(CDec(dtRow.Item("Amount")), dblRoundOff_Type), strFmtCurrency))
                mdblGroupTotal += Rounding.BRound(CDec(dtRow.Item("Amount")), dblRoundOff_Type)
                mblGrandTotal += Rounding.BRound(CDec(dtRow.Item("Amount")), dblRoundOff_Type)
                'Sohail (01 Nov 2017) -- End
                'Sohail (21 Aug 2015) -- End
                'Anjan (28 May 2012)-End 



                rpt_Rows.Item("Column2") = dtRow.Item("PeriodName")
                rpt_Rows.Item("Column3") = dtRow.Item("BankName")
                rpt_Rows.Item("Column4") = dtRow.Item("BranchName")

                'Anjan (28 May 2012)-Start
                'ENHANCEMENT : BBL COMMENTS on Rutta Request
                'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column5") = Format(Rounding.BRound(CDec(dtRow.Item("Amount")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency)
                rpt_Rows.Item("Column5") = Format(Rounding.BRound(CDec(dtRow.Item("Amount")), dblRoundOff_Type), strFmtCurrency)
                'Sohail (21 Aug 2015) -- End
                'Anjan (28 May 2012)-End 


                rpt_Rows.Item("Column6") = dtRow.Item("SrNo")
                rpt_Rows.Item("Column7") = dtRow.Item("Address")
                rpt_Rows.Item("Column8") = dtRow.Item("Country")

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column9") = Format(mdblGroupTotal, GUI.fmtCurrency)
                'rpt_Rows.Item("Column10") = Format(mblGrandTotal, GUI.fmtCurrency)
                rpt_Rows.Item("Column9") = Format(mdblGroupTotal, strFmtCurrency)
                rpt_Rows.Item("Column10") = Format(mblGrandTotal, strFmtCurrency)
                'Sohail (21 Aug 2015) -- End

                rpt_Rows.Item("Column81") = CInt(dtRow.Item("EmpCount")) 'Sohail (27 Feb 2013)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptBankSummary

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 21, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 22, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 23, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 24, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            'Sandeep [ 10 FEB 2011 ] -- End 

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtNo", Language.getMessage(mstrModuleName, 2, "No"))
            Call ReportFunction.TextChange(objRpt, "txtPeriodName", Language.getMessage(mstrModuleName, 19, "Period Name :"))
            Call ReportFunction.TextChange(objRpt, "txtBank", Language.getMessage(mstrModuleName, 4, "Bank"))
            Call ReportFunction.TextChange(objRpt, "txtBranchName", Language.getMessage(mstrModuleName, 5, "Branch Name"))
            Call ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 6, "Address"))
            Call ReportFunction.TextChange(objRpt, "txtCountry", Language.getMessage(mstrModuleName, 7, "Country"))
            Call ReportFunction.TextChange(objRpt, "txtAmountPaid", Language.getMessage(mstrModuleName, 8, "Amount Paid"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 26, "Emp. Count")) 'Sohail (27 Feb 2013)


            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 9, "Group Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 10, "Grand Total :"))


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 11, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 12, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 13, "Page :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyname", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            ''Vimal (30 Nov 2010) -- Start 
            'Call ReportFunction.SetRptDecimal(objRpt, "grpCol51")
            'Call ReportFunction.SetRptDecimal(objRpt, "totCol51")
            ''Vimal (30 Nov 2010) -- End

            'Pinkal (05-Jul-2011) -- End


            'Anjan [14 Mar 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            If mblnShowCountry = False Then
                Call ReportFunction.EnableSuppress(objRpt, "txtCountry", True)

                Call ReportFunction.EnableSuppress(objRpt, "Column81", True)
            End If
            'Anjan [14 Mar 2014 ] -- End

            'Sohail (24 Mar 2014) -- Start
            'ENHANCEMENT - Show Address on Bank Summary report.
            If mblnShowAddress = False Then
                Call ReportFunction.EnableSuppress(objRpt, "txtAddress", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column71", True)
            End If
            'Sohail (24 Mar 2014) -- End



            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

    ' Messages
    '1
    '2, "No"
    '3, "Period Name"
    '4, "Bank"
    '5, "Branch Name"
    '6, "Address"
    '7, "Country"
    '8, "Amount Paid"
    '9, "Group Total :"
    '10, "Grand Total :"
    '11, "Printed By :"
    '12, "Printed Date :"
    '13, "Page :"
    '14, " Bank : "
    '15, " Branch : "
    '16, " Amount From : "
    '17, " To : "
    '18, " Order By : "
    '19, "Period Name :"
    '20, " Period : "

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "No")
            Language.setMessage(mstrModuleName, 3, "Period Name")
            Language.setMessage(mstrModuleName, 4, "Bank")
            Language.setMessage(mstrModuleName, 5, "Branch Name")
            Language.setMessage(mstrModuleName, 6, "Address")
            Language.setMessage(mstrModuleName, 7, "Country")
            Language.setMessage(mstrModuleName, 8, "Amount Paid")
            Language.setMessage(mstrModuleName, 9, "Group Total :")
            Language.setMessage(mstrModuleName, 10, "Grand Total :")
            Language.setMessage(mstrModuleName, 11, "Printed By :")
            Language.setMessage(mstrModuleName, 12, "Printed Date :")
            Language.setMessage(mstrModuleName, 13, "Page :")
            Language.setMessage(mstrModuleName, 14, " Bank :")
            Language.setMessage(mstrModuleName, 15, " Branch :")
            Language.setMessage(mstrModuleName, 16, " Amount From :")
            Language.setMessage(mstrModuleName, 17, " To :")
            Language.setMessage(mstrModuleName, 18, " Order By :")
            Language.setMessage(mstrModuleName, 19, "Period Name :")
            Language.setMessage(mstrModuleName, 20, " Period :")
            Language.setMessage(mstrModuleName, 21, "Prepared By :")
            Language.setMessage(mstrModuleName, 22, "Checked By :")
            Language.setMessage(mstrModuleName, 23, "Approved By :")
            Language.setMessage(mstrModuleName, 24, "Received By :")
            Language.setMessage(mstrModuleName, 25, "Paid Currency :")
            Language.setMessage(mstrModuleName, 26, "Emp. Count")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class




