'************************************************************************************************************************************
'Class Name : clsOverDeduction_NetPay_Report.vb
'Purpose    :
'Date       : 04/04/2012
'Written By : Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsOverDeduction_NetPay_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsOverDeduction_NetPay_Report"
    Private mstrReportId As String = enArutiReport.OverDeduction_NetPay_Report   '77
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Property Variables "

    Private mstrEmployeeName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mstrFilter As String = ""
    Private mstrSorting As String = ""

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _Employee_Name() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _Employee_Id() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _Period_Name() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _Period_Id() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrEmployeeName = ""
            mintEmployeeId = 0
            mstrPeriodName = ""
            mintPeriodId = 0
            mstrFilter = ""
            mstrSorting = ""

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeId > 0 Then
                mstrFilter &= "AND employeeunkid = '" & mintEmployeeId & "' "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintPeriodId > 0 Then
                mstrFilter &= "AND payperiodunkid = '" & mintPeriodId & "' "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Period :") & " " & mstrPeriodName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Order By :") & " " & Me.OrderByDisplay & " "
                mstrSorting &= Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Dim objUser As New clsUserAddEdit

        Try
            objConfig._Companyunkid = xCompanyUnkid
            objUser._Userunkid = xUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, True, True, False, objUser._Username, objConfig._CurrencyFormat)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
            objConfig = Nothing
            objUser = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Reports Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Public Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeename", Language.getMessage(mstrModuleName, 4, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("payperiodunkid", Language.getMessage(mstrModuleName, 16, "Period")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal xDatabaseName As String _
                                         , ByVal xUserUnkid As Integer _
                                         , ByVal intYearID As Integer _
                                         , ByVal xCompanyUnkid As Integer _
                                         , ByVal xPeriodStart As DateTime _
                                         , ByVal xPeriodEnd As DateTime _
                                         , ByVal xUserModeSetting As String _
                                         , ByVal xOnlyApproved As Boolean _
                                         , ByVal blnIncludeInActiveEmployee As Boolean _
                                         , ByVal blnApplyUserAccessFilter As Boolean _
                                         , ByVal blnExcludeTermEmp_PayProcess As Boolean _
                                         , ByVal strUserName As String _
                                         , ByVal strFmtCurrency As String _
                                         ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, intYearID, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, blnIncludeInActiveEmployee, blnApplyUserAccessFilter, blnExcludeTermEmp_PayProcess, strUserName, strFmtCurrency]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport = Nothing
        Try
            Dim objBalance As New clsTnALeaveTran


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length <= 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objBalance.GetList("List", mintEmployeeId, , , , , , , , , , , , mintPeriodId, "prtnaleave_tran.processdate")
                dsList = objBalance.GetList(xDatabaseName, xUserUnkid, intYearID, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, blnIncludeInActiveEmployee, blnApplyUserAccessFilter, "List", mintEmployeeId, mintPeriodId, "prtnaleave_tran.processdate", "", blnExcludeTermEmp_PayProcess)
                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objBalance.GetList("List", mintEmployeeId, , , , , , , , , , , , mintPeriodId, "prtnaleave_tran.processdate", True, mstrAdvance_Filter.Trim)
                dsList = objBalance.GetList(xDatabaseName, xUserUnkid, intYearID, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, blnIncludeInActiveEmployee, blnApplyUserAccessFilter, "List", mintEmployeeId, mintPeriodId, "prtnaleave_tran.processdate", mstrAdvance_Filter.Trim, blnExcludeTermEmp_PayProcess)
                'Sohail (21 Aug 2015) -- End
            End If
            'Pinkal (27-Feb-2013) -- End


            Call FilterTitleAndFilterQuery()
            If dsList.Tables("List").Rows.Count > 0 Then
                rpt_Data = New ArutiReport.Designer.dsArutiReport
                Dim dFilter As DataTable : Dim dTable As DataTable
                Dim dicIsPeriodAdded As New Dictionary(Of Integer, Integer)
                dFilter = New DataView(dsList.Tables("List"), "(total_amount+openingbalance) < 0 ", mstrSorting, DataViewRowState.CurrentRows).ToTable
                For Each dtRow As DataRow In dFilter.Rows
                    If dicIsPeriodAdded.ContainsKey(CInt(dtRow.Item("payperiodunkid"))) = True Then Continue For

                    dicIsPeriodAdded.Add(CInt(dtRow.Item("payperiodunkid")), CInt(dtRow.Item("payperiodunkid")))

                    dTable = New DataView(dFilter, "payperiodunkid = '" & CInt(dtRow.Item("payperiodunkid")) & "'", mstrSorting, DataViewRowState.CurrentRows).ToTable
                    For Each dRow As DataRow In dTable.Rows
                        Dim rpt_Rows As DataRow
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                        rpt_Rows.Item("Column1") = dRow.Item("employeecode")
                        rpt_Rows.Item("Column2") = dRow.Item("employeename")
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Rows.Item("Column3") = Format(CDec(dRow.Item("total_amount")), GUI.fmtCurrency)
                        rpt_Rows.Item("Column3") = Format(CDec(dRow.Item("total_amount")), strFmtCurrency)
                        'Sohail (21 Aug 2015) -- End
                        rpt_Rows.Item("Column4") = dRow.Item("PeriodName")
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Rows.Item("Column5") = Format(CDec(dTable.Compute("SUM(total_amount)", "payperiodunkid = '" & CInt(dtRow.Item("payperiodunkid")) & "'")), GUI.fmtCurrency)
                        'rpt_Rows.Item("Column6") = Format(CDec(dFilter.Compute("SUM(total_amount)", "")), GUI.fmtCurrency)
                        rpt_Rows.Item("Column5") = Format(CDec(dTable.Compute("SUM(total_amount)", "payperiodunkid = '" & CInt(dtRow.Item("payperiodunkid")) & "'")), strFmtCurrency)
                        rpt_Rows.Item("Column6") = Format(CDec(dFilter.Compute("SUM(total_amount)", "")), strFmtCurrency)
                        'Sohail (21 Aug 2015) -- End
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                Next
            End If

            objRpt = New ArutiReport.Designer.rptNetPay_OverDeduction_Report

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 5, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUserName)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 6, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 7, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 8, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 9, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 4, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 10, "Amount"))
            Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 2, "Period :"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 11, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 12, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 15, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Period :")
            Language.setMessage(mstrModuleName, 3, "Order By :")
            Language.setMessage(mstrModuleName, 4, "Employee Name")
            Language.setMessage(mstrModuleName, 5, "Prepared By :")
            Language.setMessage(mstrModuleName, 6, "Checked By :")
            Language.setMessage(mstrModuleName, 7, "Approved By :")
            Language.setMessage(mstrModuleName, 8, "Received By :")
            Language.setMessage(mstrModuleName, 9, "Employee Code")
            Language.setMessage(mstrModuleName, 10, "Amount")
            Language.setMessage(mstrModuleName, 11, "Sub Total :")
            Language.setMessage(mstrModuleName, 12, "Grand Total :")
            Language.setMessage(mstrModuleName, 13, "Printed By :")
            Language.setMessage(mstrModuleName, 14, "Printed Date :")
            Language.setMessage(mstrModuleName, 15, "Page :")
            Language.setMessage(mstrModuleName, 16, "Period")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
