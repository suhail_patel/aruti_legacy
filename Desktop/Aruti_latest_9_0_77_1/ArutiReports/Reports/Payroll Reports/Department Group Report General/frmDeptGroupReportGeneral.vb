#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmDeptGroupReportGeneral

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDeptGroupReportGeneral"
    Private objCCDepartmentGrpGeneral As clsDeptGroupReportGeneral

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty

    Private mstrBranchIDs As String = String.Empty
    Private mstrBranchNAMEs As String = String.Empty

    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = 0
    Private mstrCurrency_Rate As String = String.Empty

    Private mstrFromDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0

    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Constructor "

    Public Sub New()
        objCCDepartmentGrpGeneral = New clsDeptGroupReportGeneral(User._Object._Languageunkid,Company._Object._Companyunkid)
        objCCDepartmentGrpGeneral.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Branch = 1

    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objperiod As New clscommom_period_Tran
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objExRate As New clsExchangeRate
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet

        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, False)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'Anjan [10 June 2015] -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With
            objperiod = Nothing

            dsList = objDeptGrp.getComboList("List", True)
            With cboDepartment
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objExRate.getComboList("ExRate", True)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                End If
            End If

            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objperiod = Nothing
            objDeptGrp = Nothing
            objExRate = Nothing
            objMaster = Nothing
            dsList = Nothing
        End Try
    End Sub

    'Private Sub FillList()
    '    Dim objBranch As New clsStation
    '    Dim dsList As DataSet
    '    Dim lvItem As ListViewItem
    '    Try

    '        dsList = objBranch.GetList("List", False)

    '        For Each dsRow As DataRow In dsList.Tables("List").Rows
    '            lvItem = New ListViewItem

    '            lvItem.Text = ""
    '            lvItem.Tag = CInt(dsRow.Item("stationunkid"))

    '            If mstrBranchIDs.Trim <> "" Then
    '                If mstrBranchIDs.Split(",").Contains(dsRow.Item("stationunkid").ToString) = True Then
    '                    lvItem.Checked = True
    '                End If
    '            End If

    '            lvItem.SubItems.Add(dsRow.Item("code").ToString)

    '            lvItem.SubItems.Add(dsRow.Item("name").ToString)

    '            RemoveHandler lvBranch.ItemChecked, AddressOf lvBranch_ItemChecked
    '            lvBranch.Items.Add(lvItem)
    '            AddHandler lvBranch.ItemChecked, AddressOf lvBranch_ItemChecked
    '        Next


    '        If lvBranch.Items.Count > 6 Then
    '            colhName.Width = 200 - 18
    '        Else
    '            colhName.Width = 200
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
    '    Finally
    '        objBranch = Nothing
    '        dsList = Nothing
    '    End Try
    'End Sub

    Private Sub ResetValue()
        Try

            'objCCDepartmentWise.setDefaultOrderBy(0)
            'txtOrderBy.Text = objCCDepartmentWise.OrderByDisplay

            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod

            cboDepartment.SelectedValue = 0
            chkInactiveemp.Checked = False

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            chkIgnorezeroHead.Checked = True

            lblExRate.Text = ""
            mdecEx_Rate = 1
            mstrCurr_Sign = String.Empty
            mstrCurrency_Rate = ""

            mstrAdvanceFilter = ""

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.CostCenter_DepartmentWise_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Branch
                            mstrBranchIDs = dsRow.Item("transactionheadid").ToString

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    'Private Sub CheckAll(ByVal blnCheck As Boolean)
    '    Try
    '        For Each lvItem As ListViewItem In lvBranch.Items
    '            lvItem.Checked = blnCheck
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
    '    End Try
    'End Sub

    Private Function SetFilter() As Boolean
        Try
            'Nilay (18-Mar-2015) -- Start
            'Issue : Aga Khan wanted to export reports on local computer and not on cloud server.
            'If ConfigParameter._Object._ExportReportPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            '    Return False
            'Nilay (18-Mar-2015) -- End

            If cboPeriod.SelectedValue <= 0 Then 'Nilay (18-Mar-2015) -- ElseIf cboPeriod.SelectedValue <= 0
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is compulsory infomation. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Return False
                'ElseIf lvBranch.CheckedItems.Count <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one Branch from the list."), enMsgBoxStyle.Information)
                '    lvBranch.Focus()
                '    Return False
            End If

            'Dim lstID As List(Of String) = (From p In lvBranch.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToList
            'mstrBranchIDs = String.Join(",", lstID.ToArray)
            'Dim lstNAME As List(Of String) = (From p In lvBranch.CheckedItems.Cast(Of ListViewItem)() Select (p.SubItems(colhName.Index).Text)).ToList
            'mstrBranchNAMEs = String.Join(",", lstNAME.ToArray)


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmclsDeptGroupReportBranchWise_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCCDepartmentGrpGeneral = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmclsDeptGroupReportBranchWise_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmclsDeptGroupReportBranchWise_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            chkIgnorezeroHead.Checked = True
            Call FillCombo()
            Call ResetValue()
            'Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmclsDeptGroupReportBranchWise_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmclsDeptGroupReportBranchWise_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            ElseIf e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmclsDeptGroupReportBranchWise_KeyDown", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Try

            objCCDepartmentGrpGeneral.SetDefaultValue()


            'objCCDepartmentGrpGeneral._BranchIDs = mstrBranchIDs
            'objCCDepartmentGrpGeneral._BranchNAMEs = mstrBranchNAMEs

            objCCDepartmentGrpGeneral._EmployeeId = cboEmployee.SelectedValue
            objCCDepartmentGrpGeneral._EmployeeName = cboEmployee.Text

            objCCDepartmentGrpGeneral._PeriodId = cboPeriod.SelectedValue
            objCCDepartmentGrpGeneral._PeriodName = cboPeriod.Text

            objCCDepartmentGrpGeneral._IsActive = chkInactiveemp.Checked

            objCCDepartmentGrpGeneral._DepartmentId = cboDepartment.SelectedValue
            objCCDepartmentGrpGeneral._DepartmentName = cboDepartment.Text

            objCCDepartmentGrpGeneral._ViewByIds = mstrStringIds
            objCCDepartmentGrpGeneral._ViewIndex = mintViewIdx
            objCCDepartmentGrpGeneral._ViewByName = mstrStringName
            objCCDepartmentGrpGeneral._Analysis_Fields = mstrAnalysis_Fields
            objCCDepartmentGrpGeneral._Analysis_Join = mstrAnalysis_Join
            objCCDepartmentGrpGeneral._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCCDepartmentGrpGeneral._Report_GroupName = mstrReport_GroupName

            objCCDepartmentGrpGeneral._IgnoreZeroHeads = chkIgnorezeroHead.Checked

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objCCDepartmentGrpGeneral._PeriodStartDate = objPeriod._Start_Date

            objCCDepartmentGrpGeneral._PeriodEndDate = objPeriod._End_Date

            objCCDepartmentGrpGeneral._Ex_Rate = mdecEx_Rate
            objCCDepartmentGrpGeneral._Currency_Sign = mstrCurr_Sign
            objCCDepartmentGrpGeneral._Currency_Rate = mstrCurrency_Rate

            objCCDepartmentGrpGeneral._FromDatabaseName = mstrFromDatabaseName

            objCCDepartmentGrpGeneral._Advance_Filter = mstrAdvanceFilter

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objCCDepartmentGrpGeneral.Generate_DetailReport()
            objCCDepartmentGrpGeneral.Generate_DetailReport(mstrFromDatabaseName, User._Object._Userunkid, objPeriod._Yearunkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To 1
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.DepartmentGrp_Branchwise_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Branch
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = mstrBranchIDs

                        intUnkid = objUserDefRMode.isExist(enArutiReport.DepartmentGrp_Branchwise_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCCReportDepartmentWise.SetMessages()
            objfrm._Other_ModuleNames = "clsDeptGroupReportBranchWise"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Other Control's Events "
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                            , cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    'Sohail (21 Aug 2015) -- End
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = dsList.Tables("ExRate").Rows(0)("currency_sign")
                        lblExRate.Text = Language.getMessage(mstrModuleName, 5, "Exchange Rate :") & " " & CDbl(mdecEx_Rate) & " "
                        mstrCurrency_Rate = Format(CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1")), GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2")).ToString & " " & mstrCurr_Sign & " " 'Sohail (16 Mar 2013)
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                    mstrCurrency_Rate = ""
                End If
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrFromDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrFromDatabaseName = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try

    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objCCDepartmentGrpGeneral.setOrderBy(0)
            txtOrderBy.Text = objCCDepartmentGrpGeneral.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '#Region " List View Events "
    '    Private Sub lvBranch_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '        Try
    '            RemoveHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
    '            If lvBranch.CheckedItems.Count <= 0 Then
    '                objchkAllPeriod.CheckState = CheckState.Unchecked
    '            ElseIf lvBranch.CheckedItems.Count < lvBranch.Items.Count Then
    '                objchkAllPeriod.CheckState = CheckState.Indeterminate
    '            ElseIf lvBranch.CheckedItems.Count = lvBranch.Items.Count Then
    '                objchkAllPeriod.CheckState = CheckState.Checked
    '            End If
    '            AddHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lvBranch_ItemChecked", mstrModuleName)
    '        End Try
    '    End Sub
    '#End Region

    '#Region " Checkbox Events "
    '    Private Sub objchkAllPeriod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        Try
    '            Call CheckAll(CBool(objchkAllPeriod.Checked))
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "objchkAllPeriod_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub
    '#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lblExRate.Text = Language._Object.getCaption(Me.lblExRate.Name, Me.lblExRate.Text)
			Me.chkIgnorezeroHead.Text = Language._Object.getCaption(Me.chkIgnorezeroHead.Name, Me.chkIgnorezeroHead.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths.")
			Language.setMessage(mstrModuleName, 2, "Period is compulsory infomation. Please select period to continue.")
			Language.setMessage(mstrModuleName, 3, "Sorry, No exchange rate is defined for this currency for the period selected.")
			Language.setMessage(mstrModuleName, 5, "Exchange Rate :")
			Language.setMessage(mstrModuleName, 6, "Selection Saved Successfully")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
