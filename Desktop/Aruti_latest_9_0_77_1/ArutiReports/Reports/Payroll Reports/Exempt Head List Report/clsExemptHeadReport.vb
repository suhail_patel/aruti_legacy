'************************************************************************************************************************************
'Class Name : clsExemptHeadReport.vb
'Purpose    :
'Date       : 04/30/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsExemptHeadReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsExemptHeadReport"
    Private mstrReportId As String = enArutiReport.Exempt_Head_Report   '55
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_ExemptEListReport()
        Call Create_HeadWiseReport()
    End Sub

#End Region

#Region " Property Variables "

    Private mstrEmployeeName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrTranHeadName As String = String.Empty
    Private mintTranHeadId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mstrReportType_Namne As String = String.Empty
    Private mintReportType_Id As Integer = 0


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    'S.SANDEEP [ 17 AUG 2011 ] -- END 

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (20 Apr 2012) -- End


    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _Employee_Name() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _Employee_Id() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _TranHead_Name() As String
        Set(ByVal value As String)
            mstrTranHeadName = value
        End Set
    End Property

    Public WriteOnly Property _TranHead_Id() As Integer
        Set(ByVal value As Integer)
            mintTranHeadId = value
        End Set
    End Property

    Public WriteOnly Property _Period_Name() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _Period_Id() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _Report_Type_Name() As String
        Set(ByVal value As String)
            mstrReportType_Namne = value
        End Set
    End Property

    Public WriteOnly Property _Report_Type_Id() As String
        Set(ByVal value As String)
            mintReportType_Id = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch_Name() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property
    'S.SANDEEP [ 17 AUG 2011 ] -- END

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (20 Apr 2012) -- End


    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrEmployeeName = ""
            mintEmployeeId = 0
            mstrTranHeadName = ""
            mintTranHeadId = 0
            mstrPeriodName = ""
            mintPeriodId = 0
            mstrReportType_Namne = ""
            mintReportType_Id = 0


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            mintBranchId = -1
            mstrBranchName = ""
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            Select Case mintReportType_Id
                Case 0
                    objDataOperation.AddParameter("@NA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Not Assigned"))
                    objDataOperation.AddParameter("@Ex", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Excempted"))
                    objDataOperation.AddParameter("@NR", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Non Recurrent"))
            End Select

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND EmpId = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintTranHeadId > 0 Then
                objDataOperation.AddParameter("@TrnHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadId)
                Select Case mintReportType_Id
                    Case 1
                        Me._FilterQuery &= " AND THeadId = @TrnHeadId "
                End Select
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Transaction Head :") & " " & mstrTranHeadName & " "
            End If

            If mintPeriodId > 0 Then
                objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                Select Case mintReportType_Id
                    Case 1
                        Me._FilterQuery &= " AND PId = @PeriodId "
                End Select
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Period :") & " " & mstrPeriodName & " "
            End If

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Branch :") & " " & mstrBranchName & " "
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (20 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If
            'Sohail (20 Apr 2012) -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    Select Case pintReportType
        '        Case 0
        '            objRpt = Generate_HeadWiseReport()
        '        Case 1
        '            objRpt = Generate_ExemptListReport()
        '    End Select

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objUser As New clsUserAddEdit

        Try
            objUser._Userunkid = xUserUnkid

            Select Case pintReportType
                Case 0
                    objRpt = Generate_HeadWiseReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objUser._Username)
                Case 1
                    objRpt = Generate_ExemptListReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objUser._Username)
            End Select

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        Finally
            objUser = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            Select Case intReportType
                Case 0
                    OrderByDisplay = iColumn_HeadWise.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_HeadWise.ColumnItem(0).Name
                Case 1
                    OrderByDisplay = iColumn_ExemptReport.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_ExemptReport.ColumnItem(0).Name
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Select Case intReportType
                Case 0
                    Call OrderByExecute(iColumn_HeadWise)
                Case 1
                    Call OrderByExecute(iColumn_ExemptReport)
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Get_ReportTypeList(Optional ByVal StrList As String = "List") As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 0 AS Id,@HeadWise AS Name " & _
                   "UNION SELECT 1 AS Id, @EmployeeWise AS Name "

            objDataOperation.AddParameter("@HeadWise", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "Transaction Head Wise"))
            objDataOperation.AddParameter("@EmployeeWise", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Employee Wise"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_ReportTypeList; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_ExemptReport As New IColumnCollection
    Dim iColumn_HeadWise As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_ExemptReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_ExemptReport = value
        End Set
    End Property

    Public Property Field_HeadWise() As IColumnCollection
        Get
            Return iColumn_HeadWise
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_HeadWise = value
        End Set
    End Property

    Private Sub Create_ExemptEListReport()
        Try
            iColumn_ExemptReport.Clear()
            iColumn_ExemptReport.Add(New IColumn("EName", Language.getMessage(mstrModuleName, 8, "Employee")))
            iColumn_ExemptReport.Add(New IColumn("TName", Language.getMessage(mstrModuleName, 6, "Head Name")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub Create_HeadWiseReport()
        Try
            iColumn_HeadWise.Clear()
            iColumn_HeadWise.Add(New IColumn("ECode", Language.getMessage(mstrModuleName, 7, "Employee Code")))
            iColumn_HeadWise.Add(New IColumn("EName", Language.getMessage(mstrModuleName, 8, "Employee")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_ExemptListReport(ByVal xDatabaseName As String _
                                               , ByVal xUserUnkid As Integer _
                                               , ByVal xYearUnkid As Integer _
                                               , ByVal xCompanyUnkid As Integer _
                                               , ByVal xPeriodStart As Date _
                                               , ByVal xPeriodEnd As Date _
                                               , ByVal xUserModeSetting As String _
                                               , ByVal xOnlyApproved As Boolean _
                                               , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                               , ByVal blnApplyUserAccessFilter As Boolean _
                                               , ByVal strUserName As String _
                                               ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strUserName]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                    "     TCode AS TCode " & _
                    "    ,TName AS TName " & _
                    "    ,ECode AS ECode " & _
                    "    ,EName AS EName " & _
                    "    ,PCode AS PCode " & _
                    "    ,PName AS PName " & _
                    "FROM " & _
                    "( " & _
                    "     SELECT " & _
                    "         ISNULL(prtranhead_master.trnheadcode,'') AS TCode " & _
                    "        ,ISNULL(prtranhead_master.trnheadname,'') AS TName " & _
                    "        ,ISNULL(hremployee_master.employeecode,'') AS ECode "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            ',ISNULL(hremployee_master.firstname,'')+''+ISNULL(hremployee_master.othername,'')+''+ISNULL(hremployee_master.surname,'') AS EName "
            If mblnFirstNamethenSurname = False Then
                StrQ &= "        ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
            Else
                StrQ &= "        ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
            End If

            StrQ &= "        ,ISNULL(cfcommon_period_tran.period_code,'') AS PCode " & _
                    "        ,ISNULL(cfcommon_period_tran.period_name,'') AS PName " & _
                    "        ,prtranhead_master.tranheadunkid AS THeadId " & _
                    "        ,hremployee_master.employeeunkid AS EmpId " & _
                    "        ,cfcommon_period_tran.periodunkid AS PId " & _
                    "    FROM prtranhead_master " & _
                    "        JOIN premployee_exemption_tran ON prtranhead_master.tranheadunkid = premployee_exemption_tran.tranheadunkid " & _
                    "        JOIN cfcommon_period_tran ON premployee_exemption_tran.periodunkid = cfcommon_period_tran.periodunkid AND modulerefid = " & enModuleReference.Payroll & "  " & _
                    "        JOIN hremployee_master ON premployee_exemption_tran.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,hremployee_transfer_tran.employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "    WHERE premployee_exemption_tran.isvoid = 0 " & _
                    "        AND premployee_exemption_tran.periodunkid = @PeriodId  "



            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    'Sohail (20 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'StrQ &= "   AND hremployee_master.isactive = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
            '    'Sohail (20 Apr 2012) -- End
            'End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.stationunkid = " & mintBranchId
                StrQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (28 Jun 2011) -- Start
            'Issue : According to prvilege that lower level user should not see superior level employees.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    'Sohail (20 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            '    'Sohail (20 Apr 2012) -- End
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (28 Jun 2011) -- End
            StrQ &= " ) AS ExHead " & _
                    "WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("ECode")
                rpt_Rows.Item("Column2") = dtRow.Item("EName")
                rpt_Rows.Item("Column3") = dtRow.Item("TCode")
                rpt_Rows.Item("Column4") = dtRow.Item("TName")
                rpt_Rows.Item("Column5") = dtRow.Item("PCode")
                rpt_Rows.Item("Column6") = dtRow.Item("PName")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptExEmployeeWiseReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUserName)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtTCode", Language.getMessage(mstrModuleName, 5, "Head Code"))
            Call ReportFunction.TextChange(objRpt, "txtTName", Language.getMessage(mstrModuleName, 6, "Head Name"))
            Call ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 7, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 8, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtPCode", Language.getMessage(mstrModuleName, 9, "Period Code"))
            Call ReportFunction.TextChange(objRpt, "txtPName", Language.getMessage(mstrModuleName, 10, "Period Name"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 11, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 12, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 13, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            objRpt.SetDataSource(rpt_Data)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_ExemptListReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Private Function Generate_HeadWiseReport(ByVal xDatabaseName As String _
                                             , ByVal xUserUnkid As Integer _
                                             , ByVal xYearUnkid As Integer _
                                             , ByVal xCompanyUnkid As Integer _
                                             , ByVal xPeriodStart As Date _
                                             , ByVal xPeriodEnd As Date _
                                             , ByVal xUserModeSetting As String _
                                             , ByVal xOnlyApproved As Boolean _
                                             , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                             , ByVal blnApplyUserAccessFilter As Boolean _
                                             , ByVal strUserName As String _
                                             ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                        " ECode AS ECode " & _
                        ",EName AS EName " & _
                        ",GrpId AS GrpId " & _
                        ",GrpName AS GrpName " & _
                    "FROM " & _
                    "( " & _
                        "SELECT " & _
                             "ISNULL(hremployee_master.employeecode,'') AS ECode "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            ',ISNULL(hremployee_master.firstname,'')+''+ISNULL(hremployee_master.othername,'')+''+ISNULL(hremployee_master.surname,'') AS EName "
            If mblnFirstNamethenSurname = False Then
                StrQ &= "        ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
            Else
                StrQ &= "        ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
            End If

            StrQ &= ",1 AS GrpId " & _
                            ",@NA AS GrpName " & _
                            ",hremployee_master.employeeunkid AS EmpId " & _
                        "FROM hremployee_master "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "WHERE hremployee_master.employeeunkid NOT IN( SELECT DISTINCT employeeunkid FROM prearningdeduction_master WHERE tranheadunkid = @TrnHeadId AND prearningdeduction_master.isvoid = 0) "


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    'Sohail (20 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'StrQ &= " AND hremployee_master.isactive = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
            '    'Sohail (20 Apr 2012) -- End
            'End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.stationunkid = " & mintBranchId
                StrQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (28 Jun 2011) -- Start
            'Issue : According to prvilege that lower level user should not see superior level employees.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    'Sohail (20 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            '    'Sohail (20 Apr 2012) -- End
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (28 Jun 2011) -- End
            StrQ &= "UNION ALL " & _
                        "SELECT " & _
                             "ISNULL(hremployee_master.employeecode,'') AS ECode "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            ',ISNULL(hremployee_master.firstname,'')+''+ISNULL(hremployee_master.othername,'')+''+ISNULL(hremployee_master.surname,'') AS EName "
            If mblnFirstNamethenSurname = False Then
                StrQ &= "        ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
            Else
                StrQ &= "        ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
            End If

            StrQ &= ",Ex.GrpId AS GrpId " & _
                            ",Ex.GrpName AS GrpName " & _
                            ",hremployee_master.employeeunkid AS EmpId " & _
                        "FROM hremployee_master " & _
                        "JOIN " & _
                        "( " & _
                            "SELECT " & _
                                 "ISNULL(prearningdeduction_master.employeeunkid,-1) AS EmpId " & _
                                ",2 AS GrpId " & _
                                ",@Ex AS GrpName " & _
                            "FROM premployee_exemption_tran " & _
                                "JOIN prearningdeduction_master ON premployee_exemption_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
                                "AND premployee_exemption_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
                            "WHERE premployee_exemption_tran.isvoid = 0 " & _
                                "AND prearningdeduction_master.isvoid = 0 " & _
                                "AND premployee_exemption_tran.periodunkid = @PeriodId " & _
                                "AND premployee_exemption_tran.tranheadunkid=@TrnHeadId " & _
                        ") AS Ex ON Ex.EmpId = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "WHERE  1=1"


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    'Sohail (20 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'StrQ &= " AND  hremployee_master.isactive = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
            '    'Sohail (20 Apr 2012) -- End
            'End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.stationunkid = " & mintBranchId
                StrQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (28 Jun 2011) -- Start
            'Issue : According to prvilege that lower level user should not see superior level employees.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    'Sohail (20 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            '    'Sohail (20 Apr 2012) -- End
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (28 Jun 2011) -- End
            'StrQ &= "UNION ALL " & _
            '            "SELECT " & _
            '                 "DISTINCT " & _
            '                 "vwPayroll.employeeunkid " & _
            '                ",vwPayroll.employeename " & _
            '                ",3 AS GrpId " & _
            '                ",@NR AS GrpName " & _
            '                ",vwPayroll.employeeunkid AS EmpId " & _
            '            "FROM vwPayroll " & _
            '                "JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                "JOIN prearningdeduction_master ON vwPayroll.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
            '            "WHERE vwPayroll.tranheadunkid = @TrnHeadId " & _
            '                "AND payperiodunkid = @PeriodId AND vwPayroll.trnheadtype_id > 0 " & _
            '                "AND prearningdeduction_master.amount = 0 AND vwPayroll.amount = 0 " & _
            '                "AND isrecurrent = 0 AND prearningdeduction_master.isvoid = 0 AND prtranhead_master.isvoid = 0 " & _
            '        ") AS HWise " & _
            '        "WHERE 1 = 1 "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'StrQ &= "UNION ALL " & _
            '            "SELECT " & _
            '                 "DISTINCT " & _
            '               "vwPayroll.employeecode " & _
            '                ",vwPayroll.employeename " & _
            '                ",3 AS GrpId " & _
            '                ",@NR AS GrpName " & _
            '                ",vwPayroll.employeeunkid AS EmpId " & _
            '            "FROM vwPayroll " & _
            '                "JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                "JOIN prearningdeduction_master ON vwPayroll.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
            '                "JOIN hremployee_master ON hremployee_master.employeeunkid = vwPayroll.employeeunkid " & _
            '            "WHERE vwPayroll.tranheadunkid = @TrnHeadId " & _
            '                "AND payperiodunkid = @PeriodId AND vwPayroll.trnheadtype_id > 0 " & _
            '                "AND prearningdeduction_master.amount = 0 AND vwPayroll.amount = 0 " & _
            '                "AND isrecurrent = 0 AND prearningdeduction_master.isvoid = 0 AND prtranhead_master.isvoid = 0 "
            'StrQ &= "UNION ALL " & _
            '            "SELECT " & _
            '                 "DISTINCT " & _
            '               "vwPayroll.employeecode " & _
            '                ",vwPayroll.employeename " & _
            '                ",3 AS GrpId " & _
            '                ",@NR AS GrpName " & _
            '                ",vwPayroll.employeeunkid AS EmpId " & _
            '            "FROM vwPayroll " & _
            '                "JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                "JOIN prearningdeduction_master ON vwPayroll.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
            '                "JOIN hremployee_master ON hremployee_master.employeeunkid = vwPayroll.employeeunkid " & _
            '            "WHERE vwPayroll.tranheadunkid = @TrnHeadId " & _
            '                "AND payperiodunkid = @PeriodId AND vwPayroll.trnheadtype_id > 0 " & _
            '                "AND prearningdeduction_master.amount = 0 AND vwPayroll.amount = 0 " & _
            '                "AND isrecurrent = 0 AND prearningdeduction_master.isvoid = 0 AND prtranhead_master.isvoid = 0 "
            StrQ &= "UNION ALL " & _
                        "SELECT " & _
                             "DISTINCT " & _
                                "hremployee_master.employeecode  " & _
                              ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename  " & _
                            ",3 AS GrpId " & _
                            ",@NR AS GrpName " & _
                              ", prpayrollprocess_tran.employeeunkid AS EmpId " & _
                        "FROM    prpayrollprocess_tran " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                "JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
                                "JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "    WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                "AND prtnaleave_tran.isvoid = 0 " & _
                                "AND prtranhead_master.tranheadunkid = @TrnHeadId " & _
                                "AND payperiodunkid = @PeriodId " & _
                                "AND prtranhead_master.trnheadtype_id > 0 " & _
                                "AND prearningdeduction_master.amount = 0 " & _
                                "AND prpayrollprocess_tran.amount = 0 " & _
                                "AND isrecurrent = 0 " & _
                                "AND prearningdeduction_master.isvoid = 0 " & _
                                "AND prtranhead_master.isvoid = 0 "
            'Sohail (21 Aug 2015) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End

            'Sohail (28 Jun 2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    'Sohail (20 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'StrQ &= " AND  vwPayroll.isactive = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
            '    'Sohail (20 Apr 2012) -- End
            'End If
            'Sohail (21 Aug 2015) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND vwPayroll.BranchId = " & mintBranchId
                StrQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Issue : According to prvilege that lower level user should not see superior level employees.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    'Sohail (20 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'StrQ &= " AND vwPayroll.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            '    'Sohail (20 Apr 2012) -- End
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (28 Jun 2011) -- End
            StrQ &= ") AS HWise " & _
                    "WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("ECode")
                rpt_Row.Item("Column2") = dtRow.Item("EName")
                rpt_Row.Item("Column3") = dtRow.Item("GrpId")
                rpt_Row.Item("Column4") = dtRow.Item("GrpName")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptExHeadWiseReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUserName)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 7, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 8, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtView", Language.getMessage(mstrModuleName, 21, "View By :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 11, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 12, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 13, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            objRpt.SetDataSource(rpt_Data)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_HeadWiseReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By :")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Head Code")
            Language.setMessage(mstrModuleName, 6, "Head Name")
            Language.setMessage(mstrModuleName, 7, "Employee Code")
            Language.setMessage(mstrModuleName, 8, "Employee")
            Language.setMessage(mstrModuleName, 9, "Period Code")
            Language.setMessage(mstrModuleName, 10, "Period Name")
            Language.setMessage(mstrModuleName, 11, "Printed By :")
            Language.setMessage(mstrModuleName, 12, "Printed Date :")
            Language.setMessage(mstrModuleName, 13, "Page :")
            Language.setMessage(mstrModuleName, 14, "Employee :")
            Language.setMessage(mstrModuleName, 15, "Transaction Head :")
            Language.setMessage(mstrModuleName, 16, "Period :")
            Language.setMessage(mstrModuleName, 17, "Order By :")
            Language.setMessage(mstrModuleName, 18, "Not Assigned")
            Language.setMessage(mstrModuleName, 19, "Excempted")
            Language.setMessage(mstrModuleName, 20, "Non Recurrent")
            Language.setMessage(mstrModuleName, 21, "View By :")
            Language.setMessage(mstrModuleName, 22, "Branch :")
            Language.setMessage(mstrModuleName, 23, "Transaction Head Wise")
            Language.setMessage(mstrModuleName, 24, "Employee Wise")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
