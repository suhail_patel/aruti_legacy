'Class Name : clsEmployeeCostBudgetPlanVsActualExpenditure.vb
'Purpose    :
'Date       :02/08/2017
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsMonthlyEmployeeBudgetVarianceAnalysis
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsMonthlyEmployeeBudgetVarianceAnalysisReport"
    Private mstrReportId As String = enArutiReport.Monthly_Employee_Budget_Variance_Analysis_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    'Sohail (13 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
    'Private mintBudgetId As Integer = -1
    'Private mstrBudgetName As String = String.Empty
    'Private mintPeriodId As Integer = -1
    'Private mstrPeriodName As String = String.Empty
    Private mstrBudgetCodesIDs As String = String.Empty
    Private mstrBudgetCodesPeriodIDs As String = String.Empty
    Private mstrBudgetCodesPeriodNames As String = String.Empty
    Private mstrBudgetIDs As String = String.Empty
    Private mstrBudgetNames As String = String.Empty
    Private mintBudgetViewByID As Integer = 0
    Private mintAllocationByID As Integer = 0
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    'Sohail (13 Oct 2017) -- End

    'Varsha Rana (24-Aug-2017) -- Start
    'Enhancement -Payroll - Monthly Employee budget variance analysis Report
    Private mintTranHeadId As Integer = -1
    Private mstrTranHeadName As String = String.Empty
    ' Varsha Rana (24-Aug-2017) -- End


    Private mdtTableExcel As DataTable
    Private mstrMessage As String = ""

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_TableName As String = ""
    Private mstrAnalysis_CodeField As String = ""
    'Sohail (18 Aug 2017) -- Start
    'Enhancement - 69.1 - Analysis by option on Monthly Employee Budget Variance Analysis report type in Monthly Employee Budget Variance Analysis Report if budget allocation is on employee.
    Private m_intViewIndex As Integer = -1
    Private m_strViewByIds As String = ""
    Private m_strViewByName As String = ""
    Private m_strAnalysis_Fields As String = ""
    Private m_strAnalysis_Join As String = ""
    Private m_strAnalysis_OrderBy As String = ""
    Private m_strReport_GroupName As String = ""
    'Sohail (18 Aug 2017) -- End
#End Region

#Region " Properties "

    'Sohail (13 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
    'Public WriteOnly Property _BudgetId() As Integer
    '    Set(ByVal value As Integer)
    '        mintBudgetId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _BudgetName() As String
    '    Set(ByVal value As String)
    '        mstrBudgetName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _PeriodId() As Integer
    '    Set(ByVal value As Integer)
    '        mintPeriodId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _PeriodName() As String
    '    Set(ByVal value As String)
    '        mstrPeriodName = value
    '    End Set
    'End Property
    Public WriteOnly Property _BudgetCodesIDs() As String
        Set(ByVal value As String)
            mstrBudgetCodesIDs = value
        End Set
    End Property

    Public WriteOnly Property _BudgetCodesPeriodIDs() As String
        Set(ByVal value As String)
            mstrBudgetCodesPeriodIDs = value
        End Set
    End Property

    Public WriteOnly Property _BudgetCodesPeriodNames() As String
        Set(ByVal value As String)
            mstrBudgetCodesPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _BudgetIDs() As String
        Set(ByVal value As String)
            mstrBudgetIDs = value
        End Set
    End Property

    Public WriteOnly Property _BudgetNames() As String
        Set(ByVal value As String)
            mstrBudgetNames = value
        End Set
    End Property

    Public WriteOnly Property _BudgetViewByID() As Integer
        Set(ByVal value As Integer)
            mintBudgetViewByID = value
        End Set
    End Property

    Public WriteOnly Property _AllocationByID() As Integer
        Set(ByVal value As Integer)
            mintAllocationByID = value
        End Set
    End Property

    Public WriteOnly Property _Period_StartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStart = value
        End Set
    End Property

    Public WriteOnly Property _Period_EndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEnd = value
        End Set
    End Property
    'Sohail (13 Oct 2017) -- End

    'Varsha Rana (24-Aug-2017) -- Start
    'Enhancement -Payroll - Monthly Employee budget variance analysis Report
    Public WriteOnly Property _TranHeadId() As Integer
        Set(ByVal value As Integer)
            mintTranHeadId = value
        End Set
    End Property

    Public WriteOnly Property _TranHeadName() As String
        Set(ByVal value As String)
            mstrTranHeadName = value
        End Set
    End Property
    ' Varsha Rana (24-Aug-2017) -- End

  

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    'Sohail (18 Aug 2017) -- Start
    'Enhancement - 69.1 - Analysis by option on Monthly Employee Budget Variance Analysis report type in Monthly Employee Budget Variance Analysis Report if budget allocation is on employee.
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            m_intViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            m_strViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            m_strViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            m_strAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            m_strAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            m_strAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            m_strReport_GroupName = value
        End Set
    End Property
    'Sohail (18 Aug 2017) -- End

#End Region

#Region " Enum "
    Public Enum enReportType

        'Pinkal (09-Oct-2017) -- Start
        'Enhancement - Working on Budget Report Changes for CCBRT.
        'Monthly_Employee_Budget_Variance_Analysis = 1
        'Salary_Cost_Breakdown_by_Cost_Center = 2
        Monthly_Employee_Budget_Variance_Analysis_ByProject = 1
        Salary_Cost_Breakdown_by_Cost_Center_ByProject = 2
        Monthly_Employee_Budget_Variance_Analysis_ByActivity = 3
        Salary_Cost_Breakdown_by_Cost_Center_ByActivity = 4
        'Pinkal (09-Oct-2017) -- End

        
    End Enum
#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'mintBudgetId = -1
            'mstrBudgetName = ""
            'mintPeriodId = -1
            'mstrPeriodName = ""
            mstrBudgetCodesIDs = ""
            mstrBudgetCodesPeriodIDs = ""
            mstrBudgetCodesPeriodNames = ""
            mstrBudgetIDs = ""
            mstrBudgetNames = ""
            mintBudgetViewByID = 0
            mintAllocationByID = 0
            'Sohail (13 Oct 2017) -- End
            mstrMessage = ""


            'Varsha Rana (24-Aug-2017) -- Start
            'Enhancement -Payroll - Monthly Employee budget variance analysis Report
            mintTranHeadId = -1
            mstrTranHeadName = ""
            ' Varsha Rana (24-Aug-2017) -- End

          
            'mstrAdvance_Filter = ""



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetId)
            'objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            'Sohail (13 Oct 2017) -- End
            Me._FilterTitle &= ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            'OrderByDisplay = iColumn_DetailReport.ColumnItem(1).DisplayName
            'OrderByQuery = iColumn_DetailReport.ColumnItem(1).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 7, "Employee Code")))
            'If mblnFirstNamethenSurname = True Then
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ')", Language.getMessage(mstrModuleName, 8, "Employee Name")))
            'Else
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '')+ ' ' + ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 9, "Employee Name")))
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Export_Report(ByVal strReportType As String _
                                  , ByVal xUserUnkid As Integer _
                                  , ByVal xCompanyUnkid As Integer _
                                  , ByVal xUserModeSetting As String _
                                  , ByVal xOnlyApproved As Boolean _
                                  , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                  , ByVal blnApplyUserAccessFilter As Boolean _
                                  , ByVal strfmtCurrency As String _
                                  , ByVal xExportReportPath As String _
                                  , ByVal xOpenAfterExport As Boolean _
                                  , ByVal xDatabaseName As String _
                                  , ByVal xYearUnkid As Integer _
                                  )

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As DataSet
        Dim objBudget As New clsBudget_MasterNew
        Dim objBudget_Tran As New clsBudget_TranNew
        Dim objBudgetCodes As New clsBudgetcodes_master
        'Sohail (13 Oct 2017) -- Start
        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
        Dim objBudgetCodes_Tran As New clsBudgetcodes_Tran
        'Sohail (13 Oct 2017) -- End
        Dim objFundProjectCode As New clsFundProjectCode
        Dim objActivity As New clsfundactivity_Tran
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objPeriod As New clscommom_period_Tran
        'Dim mstrPreviousPeriodDBName As String 'Sohail (13 Oct 2017)

        Dim mdicFund As New Dictionary(Of Integer, Decimal)
        Dim dsAllHeads As DataSet = Nothing
        Dim mintViewById As Integer
        'Sohail (13 Oct 2017) -- Start
        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
        'Dim mintPresentationModeId As Integer 
        'Dim mdtBudgetDate As Date
        'Dim mstrPeriodIdList As String = ""
        'Sohail (13 Oct 2017) -- End
        Dim mdicHeadMapping As New Dictionary(Of Integer, Integer)
        Dim mstrAllocationTranUnkIDs As String = ""
        Dim mstrEmployeeIDs As String = ""
        Dim dtPeriodStart As Date
        Dim dtPeriodEnd As Date
        Dim objTnALeave As New clsTnALeaveTran

        Try

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'mstrPeriodIdList = objBudgetPeriod.GetPeriodIDs(mintBudgetId)
            'mdicHeadMapping = objBudgetHeadMapping.GetHeadMapping(mintBudgetId)
            'mstrAllocationTranUnkIDs = objBudget_Tran.GetAllocationTranUnkIDs(mintBudgetId)

            'objBudget._Budgetunkid = mintBudgetId
            'mintViewIdx = objBudget._Allocationbyid
            'mintViewById = objBudget._Viewbyid
            'mintPresentationModeId = objBudget._Presentationmodeid
            'mdtBudgetDate = objBudget._Budget_date.Date
            'mstrPreviousPeriodDBName = objPeriod.GetDatabaseName(objBudget._PreviousPeriodyearunkid, xCompanyUnkid)
            'objPeriod._Periodunkid(xDatabaseName) = mintPeriodId
            'dtPeriodStart = objPeriod._Start_Date
            'dtPeriodEnd = objPeriod._End_Date
            dtPeriodStart = mdtPeriodStart
            dtPeriodEnd = mdtPeriodEnd
            mintViewById = mintBudgetViewByID
            mintViewIdx = mintAllocationByID
            'Sohail (13 Oct 2017) -- End

            If CInt(mintViewById) = enBudgetViewBy.Allocation Then
                mstrAllocationTranUnkIDs = objBudgetCodes_Tran.getEmployeeIds(mstrBudgetCodesIDs) 'Sohail (13 Oct 2017)
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, mdtBudgetDate, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, dtPeriodEnd, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                'Sohail (13 Oct 2017) -- End
                If mintViewIdx = 0 Then mintViewIdx = -1
            ElseIf CInt(mintViewById) = enBudgetViewBy.Employee Then
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'mstrEmployeeIDs = objBudget_Tran.getEmployeeIds(mintBudgetId)
                mstrEmployeeIDs = objBudgetCodes_Tran.getEmployeeIds(mstrBudgetCodesIDs)
                'Sohail (13 Oct 2017) -- End
                frmEmpSelection.GetAnalysisByDetails("frmEmpSelection", mstrEmployeeIDs, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                mintViewIdx = 0
                mstrAllocationTranUnkIDs = mstrEmployeeIDs
            End If

            'Dim strTranHeadIDList As String = String.Join(",", (From p In mdicHeadMapping Select (p.Key.ToString)).ToArray) 'Sohail (13 Oct 2017)
            dsAllHeads = Nothing
            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'dsList = objFundProjectCode.GetList("FundProjectCode")
            dsList = objFundProjectCode.GetList("FundProjectCode", , True)
            'Sohail (13 Oct 2017) -- End
            mdicFund = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Total = CDec(p.Item("currentceilingbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)

            Dim dicFundCode As Dictionary(Of Integer, String) = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Code = p.Item("fundprojectcode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)

            dsList = objActivity.GetComboList("Activity", True)
            Dim d_row() As DataRow = dsList.Tables(0).Select("fundactivityunkid = 0")
            If d_row.Length > 0 Then
                d_row(0).Item("activitycode") = "" 'Sohail (29 Mar 2017)
                d_row(0).Item("activityname") = ""
                dsList.Tables(0).AcceptChanges()
            End If
            Dim dicActivityCode As Dictionary(Of Integer, String) = (From p In dsList.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activitycode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'dsList = objBudgetCodes.GetDataGridList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, mintBudgetId, mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, CInt(objBudget._Whotoincludeid), "Budget", mintPeriodId, True, "", dsAllHeads, True, mstrAnalysis_CodeField)
            dsList = objBudgetCodes.GetBudgetAllocation(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrBudgetCodesIDs, mintBudgetViewByID, enBudgetPresentation.TransactionWise, 0, "Budget", True, "", dsAllHeads, True, mstrAnalysis_CodeField)
            'Sohail (13 Oct 2017) -- End

            Dim strExpression As String = ""
            For Each pair In mdicFund
                strExpression &= " + [|_" & pair.Key.ToString & "]"
                dsList.Tables(0).Columns.Add("A||_" & pair.Key.ToString, System.Type.GetType("System.String")).DefaultValue = ""
            Next
            If strExpression.Trim <> "" Then
                dsList.Tables(0).Columns.Add("colhTotal", System.Type.GetType("System.Decimal"), strExpression.Substring(2)).DefaultValue = 0
            End If
            dsList.Tables(0).Columns.Add("colhDiff", System.Type.GetType("System.Decimal"), "budgetamount - colhTotal").DefaultValue = 0

            mdtTableExcel = dsList.Tables(0)

            Dim lst_Row As List(Of DataRow) = (From p In mdtTableExcel Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("colhTotal")) <> 100) Select (p)).ToList
            If lst_Row.Count > 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Total Percentage should be 100 for all transactions.")
                Return False
            End If

            'Sohail (24 Aug 2017) -- Start
            'Enhancement - 69.1 - Personnel head mapping for actual budget column on Monthly Employee Budget Analysis Report.
            'Dim dsBalance As DataSet = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, mintPeriodId, "")
            Dim dsBalance As DataSet = Nothing
            If mintTranHeadId > 0 Then

                'Sohail (08 Nov 2017) -- Start
                'Issue - 70.1 - Incorrect syntax near the keyword 'ON'.
                'StrQ = "SELECT  prtnaleave_tran.tnaleavetranunkid  " & _
                '          ", prtnaleave_tran.payperiodunkid " & _
                '          ", prtnaleave_tran.employeeunkid " & _
                '          ", prpayrollprocess_tran.payrollprocesstranunkid " & _
                '          ", prpayrollprocess_tran.tranheadunkid " & _
                '          ", ISNULL(prpayrollprocess_tran.amount, 0) AS total_amount " & _
                '    "FROM    prtnaleave_tran " & _
                '            "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '    "WHERE   prtnaleave_tran.isvoid = 0 " & _
                '            "AND prpayrollprocess_tran.isvoid = 0 " & _
                '            "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid " & _
                '            "AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") "
                ''Sohail (13 Oct 2017) - [AND prtnaleave_tran.payperiodunkid = @payperiodunkid] = [AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ")]
                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , False, xDatabaseName)
                If blnApplyUserAccessFilter = True Then
                    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
                End If
                Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, xDatabaseName)

                StrQ = "SELECT  prtnaleave_tran.tnaleavetranunkid  " & _
                          ", prtnaleave_tran.payperiodunkid " & _
                          ", prtnaleave_tran.employeeunkid " & _
                          ", prpayrollprocess_tran.payrollprocesstranunkid " & _
                          ", prpayrollprocess_tran.tranheadunkid " & _
                          ", ISNULL(prpayrollprocess_tran.amount, 0) AS total_amount " & _
                          ", ADF.* " & _
                    "FROM    prtnaleave_tran " & _
                            "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE   prtnaleave_tran.isvoid = 0 " & _
                            "AND prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid " & _
                            "AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If

                'If mstrAdvanceFilter.Length > 0 Then
                '    StrQ &= " AND " & mstrAdvanceFilter
                'End If
                'Sohail (08 Nov 2017) -- End

                objDataOperation = New clsDataOperation

                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                'Sohail (13 Oct 2017) -- End
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadId)

                dsBalance = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

            Else
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, mintPeriodId, "")
                Dim sf As New List(Of clsSQLFilterCollection)
                sf.Add(New clsSQLFilterCollection(" AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") ", "", SqlDbType.Int, eZeeDataType.INT_SIZE, 0))
                'Sohail (22 Oct 2021) -- Start
                'NMB Issue :  : Performance issue on process payroll.
                'dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, 0, "", , , sf)
                dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, 0, "", " 1 = 1 ", , sf)
                'Sohail (22 Oct 2021) -- End
                'Sohail (13 Oct 2017) -- End
            End If
            'Sohail (24 Aug 2017) -- End

            'Sohail (18 Aug 2017) -- Start
            'Enhancement - 69.1 - Analysis by option on Monthly Employee Budget Variance Analysis report type in Monthly Employee Budget Variance Analysis Report if budget allocation is on employee.
            Dim dsEmp As DataSet = Nothing
            If m_intViewIndex > 0 Then
                Dim objEmp As New clsEmployee_Master
                dsEmp = objEmp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, "Emp", False, -1, False, "", False, True, True)
                'dsEmp.Tables(0).Columns("employeeunkid").ColumnName = "allocationtranunkid"
                'mdtTableExcel.PrimaryKey = New DataColumn() {mdtTableExcel.Columns("allocationtranunkid"), mdtTableExcel.Columns("id")}
                'dsEmp.Tables(0).PrimaryKey = New DataColumn() {dsEmp.Tables(0).Columns("allocationtranunkid")}
                'mdtTableExcel.Merge(dsEmp.Tables(0))
                'dsEmp.Tables(0).Merge(mdtTableExcel)
            End If
            mdtTableExcel.Columns.Add("G_Id", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTableExcel.Columns.Add("G_Name", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (18 Aug 2017) -- End

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            Dim dicBudgetPeriod As New Dictionary(Of Integer, String)
            For Each strID As String In mstrBudgetIDs.Split(",")
                If dicBudgetPeriod.ContainsKey(CInt(strID)) = False Then
                    dicBudgetPeriod.Add(CInt(strID), objBudgetPeriod.GetPeriodIDs(CInt(strID)))
                End If
            Next
            Dim mdtExcel As DataTable = mdtTableExcel.Clone
            Dim d_r As DataRow = Nothing
            Dim strPrevKey As String = ""
            Dim strCurrKey As String = ""
            Dim intPrevPeriodId As Integer = 0
            Dim intCurrPeriodId As Integer = 0
            'Sohail (13 Oct 2017) -- End

            Dim decTotal As Decimal = 0
            For Each dsRow As DataRow In mdtTableExcel.Rows

                dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")), strfmtCurrency)
                For Each pair In dicFundCode
                    decTotal = 0

                    If CDec(dsRow.Item("|_" & pair.Key.ToString)) > 0 Then
                        Dim intAllocUnkId As Integer = CInt(dsRow.Item("allocationtranunkid"))
                        'Sohail (13 Oct 2017) -- Start
                        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                        'Dim decNetPay As Decimal = (From p In dsBalance.Tables(0) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                        Dim intPeriodID As Integer = CInt(dsRow.Item("periodunkid"))
                        Dim decNetPay As Decimal = (From p In dsBalance.Tables(0) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso CInt(p.Item("payperiodunkid")) = intPeriodID AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                        'Sohail (13 Oct 2017) -- End
                        decTotal += decNetPay * CDec(dsRow.Item("|_" & pair.Key.ToString)) / 100
                    End If

                    'dsRow.Item("|_" & pair.Key.ToString) = Format(CDec(dsRow.Item("|_" & pair.Key.ToString)), strfmtCurrency)
                    dsRow.Item("|_" & pair.Key.ToString) = Format(decTotal, strfmtCurrency)

                    If dicActivityCode.ContainsKey(CInt(dsRow.Item("||_" & pair.Key.ToString))) = True Then
                        dsRow.Item("A||_" & pair.Key.ToString) = dicActivityCode.Item(CInt(dsRow.Item("||_" & pair.Key.ToString)))
                    Else
                        dsRow.Item("A||_" & pair.Key.ToString) = ""
                    End If
                Next
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")) / mstrPeriodIdList.Split(",").ToArray.Count, strfmtCurrency)
                If dicBudgetPeriod.ContainsKey(CInt(dsRow.Item("budgetunkid"))) = True Then
                    dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")) / dicBudgetPeriod.Item(CInt(dsRow.Item("budgetunkid"))).Split(",").ToArray.Count, strfmtCurrency)
                Else
                    dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")), strfmtCurrency)
                End If
                'Sohail (13 Oct 2017) -- End
                'dsRow.Item("colhTotal") = Format(CDec(dsRow.Item("colhTotal")), strfmtCurrency)
                'dsRow.Item("colhDiff") = Format(CDec(dsRow.Item("colhDiff")), strfmtCurrency)

                'Sohail (18 Aug 2017) -- Start
                'Enhancement - 69.1 - Analysis by option on Monthly Employee Budget Variance Analysis report type in Monthly Employee Budget Variance Analysis Report if budget allocation is on employee.
                If m_intViewIndex > 0 Then
                    Dim dr() As DataRow = dsEmp.Tables(0).Select("employeeunkid = " & CInt(dsRow.Item("allocationtranunkid")) & " ")
                    dsRow.Item("G_Id") = 0
                    dsRow.Item("G_Name") = ""
                    If dr.Length > 0 Then
                        dsRow.Item("G_Id") = CInt(dr(0)(m_strAnalysis_OrderBy.Split(CChar("."))(1).ToString().Trim()))

                        Select Case m_intViewIndex
                            Case enAnalysisReport.Branch
                                dsRow.Item("G_Name") = dr(0)("station").ToString

                            Case enAnalysisReport.Department
                                dsRow.Item("G_Name") = dr(0)("DeptName").ToString

                            Case enAnalysisReport.Section
                                dsRow.Item("G_Name") = dr(0)("Section").ToString

                            Case enAnalysisReport.Unit
                                dsRow.Item("G_Name") = dr(0)("Unit").ToString

                            Case enAnalysisReport.Job
                                dsRow.Item("G_Name") = dr(0)("Job_Name").ToString

                            Case enAnalysisReport.CostCenter
                                dsRow.Item("G_Name") = dr(0)("CostCenter").ToString

                            Case enAnalysisReport.SectionGroup
                                dsRow.Item("G_Name") = dr(0)("SectionGroup").ToString

                            Case enAnalysisReport.UnitGroup
                                dsRow.Item("G_Name") = dr(0)("UnitGroup").ToString

                            Case enAnalysisReport.Team
                                dsRow.Item("G_Name") = dr(0)("Team").ToString

                            Case enAnalysisReport.JobGroup
                                dsRow.Item("G_Name") = dr(0)("JobGroup").ToString

                            Case enAnalysisReport.ClassGroup
                                dsRow.Item("G_Name") = dr(0)("ClassGroup").ToString

                            Case enAnalysisReport.Classs
                                dsRow.Item("G_Name") = dr(0)("Class").ToString

                            Case enAnalysisReport.DepartmentGroup
                                dsRow.Item("G_Name") = dr(0)("DeptGroup").ToString

                            Case enAnalysisReport.GradeGroup
                                dsRow.Item("G_Name") = dr(0)("GradeGroup").ToString

                            Case enAnalysisReport.Grade
                                dsRow.Item("G_Name") = dr(0)("Grade").ToString

                            Case enAnalysisReport.GradeLevel
                                dsRow.Item("G_Name") = dr(0)("GradeLevel").ToString

                        End Select

                    End If
                End If
                'Sohail (18 Aug 2017) -- End

                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                strCurrKey = dsRow.Item("allocationtranunkid").ToString() & "_" & dsRow.Item("jobunkid").ToString()
                intCurrPeriodId = CInt(dsRow.Item("periodunkid"))

                If strPrevKey <> strCurrKey Then
                    d_r = mdtExcel.NewRow
                    d_r.ItemArray = dsRow.ItemArray

                    mdtExcel.Rows.Add(d_r)
                Else
                    d_r = mdtExcel.Rows(mdtExcel.Rows.Count - 1)

                    For Each pair In dicFundCode
                        d_r.Item("|_" & pair.Key.ToString) += CDec(dsRow.Item("|_" & pair.Key.ToString))
                        If dsRow.Item("A||_" & pair.Key.ToString).ToString.Trim.Length > 0 Then
                            d_r.Item("A||_" & pair.Key.ToString) += ", " & dsRow.Item("A||_" & pair.Key.ToString)
                        End If
                    Next

                    If intPrevPeriodId <> intCurrPeriodId Then
                        d_r.Item("budgetamount") += CDec(dsRow.Item("budgetamount"))
                    End If

                    mdtExcel.AcceptChanges()
                End If
                strPrevKey = strCurrKey
                intPrevPeriodId = intCurrPeriodId
                'Sohail (13 Oct 2017) -- End

            Next
            mdtTableExcel.AcceptChanges()

            'Sohail (18 Aug 2017) -- Start
            'Enhancement - 69.1 - Analysis by option on Monthly Employee Budget Variance Analysis report type in Monthly Employee Budget Variance Analysis Report if budget allocation is on employee.
            'Sohail (06 Sep 2017) -- Start
            'Issue - 69.1 - All departments were coming on report even if only one department selected in Analysis by option.
            'mdtTableExcel = New DataView(mdtTableExcel, "allocationtranunkid > 0 ", "G_Name, G_Id", DataViewRowState.CurrentRows).ToTable
            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'If m_intViewIndex > 0 Then
            '    mdtTableExcel = New DataView(mdtTableExcel, "allocationtranunkid > 0 AND G_Id IN (" & m_strViewByIds & ") ", "G_Name, G_Id", DataViewRowState.CurrentRows).ToTable
            'Else
            '    mdtTableExcel = New DataView(mdtTableExcel, "allocationtranunkid > 0 ", "G_Name, G_Id", DataViewRowState.CurrentRows).ToTable
            'End If
            If m_intViewIndex > 0 Then
                mdtTableExcel = New DataView(mdtExcel, "allocationtranunkid > 0 AND G_Id IN (" & m_strViewByIds & ") ", "G_Name, G_Id", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTableExcel = New DataView(mdtExcel, "allocationtranunkid > 0 ", "G_Name, G_Id", DataViewRowState.CurrentRows).ToTable
            End If
            'Sohail (13 Oct 2017) -- End
            'Sohail (06 Sep 2017) -- End
            'Sohail (18 Aug 2017) -- End

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell


            Dim mintColumn As Integer = 0
            mdtTableExcel.Columns("GCode").Caption = Language.getMessage(mstrModuleName, 2, "Code") 'mstrReport_GroupName.Replace(" :", "")
            mdtTableExcel.Columns("GCode").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName.Replace(" :", "")
            mdtTableExcel.Columns("GName").SetOrdinal(mintColumn)
            mintColumn += 1

            'If CInt(mintViewById) = enBudgetViewBy.Employee Then
            '    mdtTableExcel.Columns("EmpJobTitle").Caption = Language.getMessage(mstrModuleName, 2, "Job Title")
            '    mdtTableExcel.Columns("EmpJobTitle").SetOrdinal(mintColumn)
            '    mintColumn += 1
            'End If

            mdtTableExcel.Columns("budgetamount").Caption = Language.getMessage(mstrModuleName, 3, "Budget Amount")
            mdtTableExcel.Columns("budgetamount").SetOrdinal(mintColumn)
            mintColumn += 1

            If mdtTableExcel.Columns.Contains("colhTotal") = True Then
                mdtTableExcel.Columns("colhTotal").Caption = Language.getMessage(mstrModuleName, 4, "Actual Amount")
                mdtTableExcel.Columns("colhTotal").SetOrdinal(mintColumn)
                mintColumn += 1
            End If

            If mdtTableExcel.Columns.Contains("colhDiff") = True Then
                mdtTableExcel.Columns("colhDiff").Caption = Language.getMessage(mstrModuleName, 5, "Difference")
                mdtTableExcel.Columns("colhDiff").SetOrdinal(mintColumn)
                mintColumn += 1
            End If

            For Each pair In dicFundCode
                mdtTableExcel.Columns("|_" & pair.Key.ToString).Caption = pair.Value.ToString
                mdtTableExcel.Columns("|_" & pair.Key.ToString).SetOrdinal(mintColumn)
                mintColumn += 1

                'mdtTableExcel.Columns("A||_" & pair.Key.ToString).Caption = Language.getMessage(mstrModuleName, 5, "Activity Code")
                'mdtTableExcel.Columns("A||_" & pair.Key.ToString).SetOrdinal(mintColumn)
                'mintColumn += 1
            Next

            'Sohail (18 Aug 2017) -- Start
            'Enhancement - 69.1 - Analysis by option on Monthly Employee Budget Variance Analysis report type in Monthly Employee Budget Variance Analysis Report if budget allocation is on employee.
            If m_intViewIndex > 0 Then
                mdtTableExcel.Columns("G_Name").Caption = m_strReport_GroupName.Replace(":", "")
                mdtTableExcel.Columns("G_Name").SetOrdinal(mintColumn)
                mintColumn += 1

                Dim arrGrp() As String = {"G_Name"}
                strarrGroupColumns = arrGrp
            End If

            'Sohail (18 Aug 2017) -- End


            For i = mintColumn To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(mintColumn)
            Next


            row = New WorksheetRow()
            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Budget Details") & " : " & mstrBudgetName & " ", "s9w")
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Budget Details") & " : " & mstrBudgetNames & " ", "s9w")
            'Sohail (13 Oct 2017) -- End
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "PD Details") & " : " & mstrPeriodName, "s9w")
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "PD Details") & " : " & mstrBudgetCodesPeriodNames, "s9w")
            'Sohail (13 Oct 2017) -- End
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "HeaderStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell("", "HeaderStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Actual Vs Budget"), "HeaderStyle")
            wcell.MergeAcross = 2
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Donor Actual Cost Allocation"), "HeaderStyle")
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1 - 5
            row.Cells.Add(wcell)

            rowsArrayHeader.Add(row)

            'SET EXCEL CELL WIDTH
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            'Pinkal (09-Oct-2017) -- Start
            'Enhancement - Working on Budget Report Changes for CCBRT.
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", True, rowsArrayHeader)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, strReportType, "", " ", Nothing, "", True, rowsArrayHeader)
            'Pinkal (09-Oct-2017) -- End



            Return True

        Catch ex As Exception
            exForce = New Exception(ex.Message & "; Procedure Name: Export_Report; Module Name: " & mstrModuleName)
            Throw exForce
            Return False
        End Try
    End Function

    Public Function Export_Salary_Cost_Breakdown_by_Cost_Center_Report(ByVal strReportType As String _
                                  , ByVal xUserUnkid As Integer _
                                  , ByVal xCompanyUnkid As Integer _
                                  , ByVal xUserModeSetting As String _
                                  , ByVal xOnlyApproved As Boolean _
                                  , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                  , ByVal blnApplyUserAccessFilter As Boolean _
                                  , ByVal strfmtCurrency As String _
                                  , ByVal xExportReportPath As String _
                                  , ByVal xOpenAfterExport As Boolean _
                                  , ByVal xDatabaseName As String _
                                  , ByVal xYearUnkid As Integer _
                                  )

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As DataSet = Nothing
        Dim objBudget As New clsBudget_MasterNew
        Dim objBudget_Tran As New clsBudget_TranNew
        Dim objBudgetCodes As New clsBudgetcodes_master
        'Sohail (13 Oct 2017) -- Start
        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
        Dim objBudgetCodes_Tran As New clsBudgetcodes_Tran
        'Sohail (13 Oct 2017) -- End
        Dim objFundProjectCode As New clsFundProjectCode
        Dim objActivity As New clsfundactivity_Tran
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objPeriod As New clscommom_period_Tran
        'Dim mstrPreviousPeriodDBName As String 'Sohail (13 Oct 2017)

        Dim mdicFund As New Dictionary(Of Integer, Decimal)
        Dim dsAllHeads As DataSet = Nothing
        Dim mintViewById As Integer
        'Sohail (13 Oct 2017) -- Start
        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
        'Dim mintPresentationModeId As Integer
        'Dim mdtBudgetDate As Date
        'Dim mstrPeriodIdList As String = ""
        'Sohail (13 Oct 2017) -- End
        Dim mdicHeadMapping As New Dictionary(Of Integer, Integer)
        Dim mstrAllocationTranUnkIDs As String = ""
        Dim mstrEmployeeIDs As String = ""
        Dim dtPeriodStart As Date
        Dim dtPeriodEnd As Date
        Dim objTnALeave As New clsTnALeaveTran

        Try

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'mstrPeriodIdList = objBudgetPeriod.GetPeriodIDs(mintBudgetId)
            'mdicHeadMapping = objBudgetHeadMapping.GetHeadMapping(mintBudgetId)
            'mstrAllocationTranUnkIDs = objBudget_Tran.GetAllocationTranUnkIDs(mintBudgetId)

            'objBudget._Budgetunkid = mintBudgetId
            'mintViewIdx = objBudget._Allocationbyid
            'mintViewById = objBudget._Viewbyid
            'mintPresentationModeId = objBudget._Presentationmodeid
            'mdtBudgetDate = objBudget._Budget_date.Date
            'mstrPreviousPeriodDBName = objPeriod.GetDatabaseName(objBudget._PreviousPeriodyearunkid, xCompanyUnkid)
            'objPeriod._Periodunkid(xDatabaseName) = mintPeriodId
            'dtPeriodStart = objPeriod._Start_Date
            'dtPeriodEnd = objPeriod._End_Date
            dtPeriodStart = mdtPeriodStart
            dtPeriodEnd = mdtPeriodEnd
            mintViewById = mintBudgetViewByID
            mintViewIdx = mintAllocationByID
            'Sohail (13 Oct 2017) -- End

            Dim dsEmp As DataSet = Nothing
            mstrAnalysis_OrderBy = "hremployee_master.employeeunkid"
            If CInt(mintViewById) = enBudgetViewBy.Allocation Then
                'frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, mdtBudgetDate, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                'If mintViewIdx = 0 Then mintViewIdx = -1

                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'mstrAllocationTranUnkIDs = objBudget_Tran.GetAllocationTranUnkIDs(mintBudgetId)
                mstrAllocationTranUnkIDs = objBudgetCodes_Tran.getEmployeeIds(mstrBudgetCodesIDs)
                'Sohail (13 Oct 2017) -- End
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", objBudget._Allocationbyid, mstrAllocationTranUnkIDs, objBudget._Budget_date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, dtPeriodEnd, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                'Sohail (13 Oct 2017) -- End

                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'dsList = objBudgetCodes.GetEmployeeActivityPercentage(mintPeriodId, mintBudgetId, 0, 0, mstrAllocationTranUnkIDs, 1)
                Dim sf As New List(Of clsSQLFilterCollection)
                sf.Add(New clsSQLFilterCollection(" AND bgbudgetcodesfundsource_tran.periodunkid IN (" & mstrBudgetCodesPeriodIDs & ") ", "", SqlDbType.Int, eZeeDataType.INT_SIZE, 0))
                dsList = objBudgetCodes.GetEmployeeActivityPercentage(0, 0, 0, 0, mstrAllocationTranUnkIDs, 1, mstrBudgetCodesIDs, sf)
                'Sohail (13 Oct 2017) -- End

            ElseIf CInt(mintViewById) = enBudgetViewBy.Employee Then
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'mstrEmployeeIDs = objBudget_Tran.getEmployeeIds(mintBudgetId)
                mstrEmployeeIDs = objBudgetCodes_Tran.getEmployeeIds(mstrBudgetCodesIDs)
                'Sohail (13 Oct 2017) -- End
                'frmEmpSelection.GetAnalysisByDetails("frmEmpSelection", mstrEmployeeIDs, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                'mintViewIdx = 0
                'mstrAllocationTranUnkIDs = mstrEmployeeIDs
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'dsList = objBudgetCodes.GetEmployeeActivityPercentage(mintPeriodId, mintBudgetId, 0, 0, mstrEmployeeIDs, 1)
                Dim sf As New List(Of clsSQLFilterCollection)
                sf.Add(New clsSQLFilterCollection(" AND bgbudgetcodesfundsource_tran.periodunkid IN (" & mstrBudgetCodesPeriodIDs & ") ", "", SqlDbType.Int, eZeeDataType.INT_SIZE, 0))
                dsList = objBudgetCodes.GetEmployeeActivityPercentage(0, 0, 0, 0, mstrEmployeeIDs, 1, mstrBudgetCodesIDs, sf)
                'Sohail (13 Oct 2017) -- End
            End If

            Dim dtTable As New DataTable
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim objEmp As New clsEmployee_Master
                'Dim objCC As New clscostcenter_master
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'Dim dsAct As DataSet = objFundProjectCode.GetList("ProjectCode")
                Dim dsAct As DataSet = objFundProjectCode.GetList("ProjectCode", , True)
                'Sohail (13 Oct 2017) -- End
                Dim mdicProjectCode As New Dictionary(Of Integer, String)

                mdicProjectCode = (From p In dsAct.Tables("ProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Code = p.Item("fundprojectcode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)

                dsEmp = objEmp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, "Emp", , , , "")


                dtTable.Columns.Add("CCGroupunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                dtTable.Columns.Add("Cost Center Group", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("CCenterunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                dtTable.Columns.Add("Cost Center", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Cost Center Code", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("No of Employees", System.Type.GetType("System.Int64")).DefaultValue = 0
                dtTable.Columns.Add("Personnel", System.Type.GetType("System.Decimal")).DefaultValue = 0
                For Each pair In mdicProjectCode
                    dtTable.Columns.Add(pair.Value, System.Type.GetType("System.Decimal")).DefaultValue = 0
                Next


                'Sohail (24 Aug 2017) -- Start
                'Enhancement - 69.1 - Personnel head mapping for actual budget column on Monthly Employee Budget Analysis Report.
                'Dim dsBalance As DataSet = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, mintPeriodId, "")
                Dim dsBalance As DataSet = Nothing
                If mintTranHeadId > 0 Then

                    'Sohail (08 Nov 2017) -- Start
                    'Issue - 70.1 - Incorrect syntax near the keyword 'ON'.
                    'StrQ = "SELECT  prtnaleave_tran.tnaleavetranunkid  " & _
                    '          ", prtnaleave_tran.payperiodunkid " & _
                    '          ", prtnaleave_tran.employeeunkid " & _
                    '          ", prpayrollprocess_tran.payrollprocesstranunkid " & _
                    '          ", prpayrollprocess_tran.tranheadunkid " & _
                    '          ", ISNULL(prpayrollprocess_tran.amount, 0) AS total_amount " & _
                    '    "FROM    prtnaleave_tran " & _
                    '            "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    '    "WHERE   prtnaleave_tran.isvoid = 0 " & _
                    '            "AND prpayrollprocess_tran.isvoid = 0 " & _
                    '            "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid " & _
                    '            "AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") "
                    ''Sohail (13 Oct 2017) - [AND prtnaleave_tran.payperiodunkid = @payperiodunkid] = [AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ")]
                    Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                    xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , False, xDatabaseName)
                    If blnApplyUserAccessFilter = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
                    End If
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, xDatabaseName)

                    StrQ = "SELECT  prtnaleave_tran.tnaleavetranunkid  " & _
                              ", prtnaleave_tran.payperiodunkid " & _
                              ", prtnaleave_tran.employeeunkid " & _
                              ", prpayrollprocess_tran.payrollprocesstranunkid " & _
                              ", prpayrollprocess_tran.tranheadunkid " & _
                              ", ISNULL(prpayrollprocess_tran.amount, 0) AS total_amount " & _
                              ", ADF.* " & _
                        "FROM    prtnaleave_tran " & _
                                "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If

                    If blnApplyUserAccessFilter = True Then
                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry
                        End If
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    StrQ &= "WHERE   prtnaleave_tran.isvoid = 0 " & _
                                "AND prpayrollprocess_tran.isvoid = 0 " & _
                                "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid " & _
                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") "

                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If

                    'If mstrAdvanceFilter.Length > 0 Then
                    '    StrQ &= " AND " & mstrAdvanceFilter
                    'End If
                    'Sohail (08 Nov 2017) -- End

                    objDataOperation = New clsDataOperation

                    'Sohail (13 Oct 2017) -- Start
                    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                    'objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                    'Sohail (13 Oct 2017) -- End
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadId)

                    dsBalance = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    End If

                Else
                    'Sohail (13 Oct 2017) -- Start
                    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                    'dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, mintPeriodId, "")
                    Dim sf As New List(Of clsSQLFilterCollection)
                    sf.Add(New clsSQLFilterCollection(" AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") ", "", SqlDbType.Int, eZeeDataType.INT_SIZE, 0))
                    'Sohail (22 Oct 2021) -- Start
                    'NMB Issue :  : Performance issue on process payroll.
                    'dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, 0, "", , , sf)
                    dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, 0, "", " 1 = 1 ", , sf)
                    'Sohail (22 Oct 2021) -- End
                    'Sohail (13 Oct 2017) -- End
                End If
                'Sohail (24 Aug 2017) -- End

                'dsEmp.Tables(0).PrimaryKey = New DataColumn() {dsEmp.Tables(0).Columns("employeeunkid")}
                'Dim c As New System.Data.ForeignKeyConstraint(dsEmp.Tables(0).Columns("employeeunkid"), dsEmp.Tables(0).Columns("costcenterunkid"))
                'dsEmp.Tables(0).Constraints.Add("", dsEmp.Tables(0).Columns("costcenterunkid"), False)
                'Dim d As DataRow = dsCC.Tables(0).NewRow
                'd.Item("costcenterunkid") = 0
                'd.Item("costcentercode") = "N/A"
                'd.Item("costcentername") = "N/A"
                'd.Item("costcentergroupmasterunkid") = -1
                'dsCC.Tables(0).Rows.InsertAt(d, 0)
                'dsCC.Tables(0).PrimaryKey = New DataColumn() {dsCC.Tables(0).Columns("costcenterunkid")}
                'dsEmp.Tables(0).Merge(dsCC.Tables(0))
                'Dim r As New DataRelation("r", dsEmp.Tables(0).Columns("employeeunkid"), dsCC.Tables(0).Columns("costcenterunkid"), False)

                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'dsBalance.Tables(0).PrimaryKey = New DataColumn() {dsBalance.Tables(0).Columns("employeeunkid")}
                'dsBalance.Tables(0).PrimaryKey = New DataColumn() {dsBalance.Tables(0).Columns("tnaleavetranunkid")}
                'dsBalance.Tables(0).PrimaryKey = New DataColumn() {dsBalance.Tables(0).Columns("employeeunkid"), dsBalance.Tables(0).Columns("payperiodunkid")}
                Dim dsTemp As New DataSet
                dsTemp.Tables.Add(New DataView(dsEmp.Tables(0), "employeecode IS NOT NULL", "costcentergroup, costcentergroupmasterunkid, costcenter, costcenterunkid", DataViewRowState.CurrentRows).ToTable)
                dsTemp.Tables.Add(dsBalance.Tables(0).Copy)
                Dim r As New DataRelation("r", dsTemp.Tables(0).Columns("employeeunkid"), dsTemp.Tables(1).Columns("employeeunkid"))
                dsTemp.Relations.Add(r)
                'dsEmp.Tables(0).PrimaryKey = New DataColumn() {dsEmp.Tables(0).Columns("employeeunkid")}
                'Sohail (13 Oct 2017) -- End
                'dsBalance.Tables(0).Merge(dsEmp.Tables(0))

                'Sohail (18 Aug 2017) -- Start
                'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
                'Dim dtTemp As DataTable = New DataView(dsBalance.Tables(0), "", "costcentergroup, costcentergroupmasterunkid, costcentercode, costcenterunkid", DataViewRowState.CurrentRows).ToTable
                'Sohail (24 Aug 2017) -- Start
                'Enhancement - 69.1 - Personnel head mapping for actual budget column on Monthly Employee Budget Analysis Report.
                'Dim dtTemp As DataTable = New DataView(dsBalance.Tables(0), "", "costcentergroup, costcentergroupmasterunkid, costcenter, costcenterunkid", DataViewRowState.CurrentRows).ToTable
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'Dim dtTemp As DataTable = New DataView(dsBalance.Tables(0), "employeecode IS NOT NULL", "costcentergroup, costcentergroupmasterunkid, costcenter, costcenterunkid", DataViewRowState.CurrentRows).ToTable
                'Sohail (13 Oct 2017) -- End
                'Sohail (24 Aug 2017) -- End
                'Sohail (18 Aug 2017) -- End

                Dim intPrevCCGrpId As Integer = -2
                Dim intPrevCCId As Integer = -2
                Dim intEmpCount As Integer = 0
                Dim decAmt As Decimal = 0
                Dim dr As DataRow = Nothing
                Dim intProjectCodeId As Integer
                Dim decTotal As Decimal = 0
                For Each dtRow As DataRow In dsTemp.Tables(0).Rows

                    'Sohail (13 Oct 2017) -- Start
                    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                    Dim intEmp As Integer = 0
                    If dtRow.GetChildRows("r").Length <= 0 Then Continue For

                    For Each chRow As DataRow In dtRow.GetChildRows("r")
                        intEmp += 1
                        'Sohail (13 Oct 2017) -- End

                        If intPrevCCGrpId <> CInt(dtRow.Item("costcentergroupmasterunkid")) OrElse intPrevCCId <> CInt(dtRow.Item("costcenterunkid")) Then
                            intEmpCount = 1
                            'Sohail (13 Oct 2017) -- Start
                            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                            'decAmt = CDec(dtRow.Item("total_amount"))
                            decAmt = CDec(chRow.Item("total_amount"))
                            'Sohail (13 Oct 2017) -- End
                            decTotal = 0

                            dr = dtTable.NewRow
                            If dtRow.Item("costcentergroup").ToString.Trim = "" Then
                                dr.Item("Cost Center Group") = Language.getMessage(mstrModuleName, 111, "N/A")
                            Else
                                dr.Item("Cost Center Group") = dtRow.Item("costcentergroup")
                            End If

                            dr.Item("CCGroupunkid") = dtRow.Item("costcentergroupmasterunkid")
                            dr.Item("CCenterunkid") = dtRow.Item("costcenterunkid")
                            If CInt(dtRow.Item("costcenterunkid")) <= 0 Then
                                dr.Item("Cost Center") = Language.getMessage(mstrModuleName, 222, "N/A")
                                dr.Item("Cost Center Code") = Language.getMessage(mstrModuleName, 333, "N/A")
                            Else
                                dr.Item("Cost Center") = dtRow.Item("costcenter")
                                dr.Item("Cost Center Code") = dtRow.Item("costcentercode")
                            End If

                            dr.Item("No of Employees") = intEmpCount
                            dr.Item("Personnel") = Format(decAmt, strfmtCurrency)

                            For Each pair In mdicProjectCode
                                intProjectCodeId = pair.Key
                                Dim intAllcId As Integer = CInt(dtRow.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim))
                                Dim intPeriodID As Integer = CInt(chRow.Item("payperiodunkid")) 'Sohail (13 Oct 2017)
                                decTotal = CDec(dr.Item(pair.Value))

                                'Sohail (13 Oct 2017) -- Start
                                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                                'Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundprojectcodeunkid")) = intProjectCodeId AndAlso CInt(p.Item("Employeeunkid")) = intAllcId AndAlso CDec(p.Item("percentage")) > 0 AndAlso CInt(p.Item("fundactivityunkid"))) Select (p)).ToList
                                Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundprojectcodeunkid")) = intProjectCodeId AndAlso CInt(p.Item("Employeeunkid")) = intAllcId AndAlso CInt(p.Item("periodunkid")) = intPeriodID AndAlso CDec(p.Item("percentage")) > 0 AndAlso CInt(p.Item("fundactivityunkid")) > 0) Select (p)).ToList
                                'Sohail (13 Oct 2017) -- End
                                If lstRow.Count > 0 Then
                                    For Each dRow As DataRow In lstRow
                                        Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                        'Sohail (13 Oct 2017) -- Start
                                        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                                        'Dim decNetPay As Decimal = (From p In dtTemp Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                                        Dim decNetPay As Decimal = (From p In dsTemp.Tables(1) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso CInt(p.Item("payperiodunkid")) = intPeriodID AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                                        'Sohail (13 Oct 2017) -- End
                                        decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                                    Next
                                End If

                                dr.Item(pair.Value) = Format(decTotal, strfmtCurrency)
                            Next

                            dtTable.Rows.Add(dr)
                        Else
                            'Sohail (13 Oct 2017) -- Start
                            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                            'intEmpCount += 1
                            If intEmp = 1 Then intEmpCount += 1
                            'Sohail (13 Oct 2017) -- End
                            decAmt += CDec(chRow.Item("total_amount"))

                            dr = dtTable.Rows(dtTable.Rows.Count - 1)
                            dr.Item("No of Employees") = intEmpCount
                            dr.Item("Personnel") = Format(decAmt, strfmtCurrency)
                            For Each pair In mdicProjectCode
                                intProjectCodeId = pair.Key
                                Dim intAllcId As Integer = CInt(dtRow.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim))
                                Dim intPeriodID As Integer = CInt(chRow.Item("payperiodunkid")) 'Sohail (13 Oct 2017)
                                decTotal = CDec(dr.Item(pair.Value))

                                'Sohail (13 Oct 2017) -- Start
                                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                                'Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundprojectcodeunkid")) = intProjectCodeId AndAlso CInt(p.Item("Employeeunkid")) = intAllcId AndAlso CDec(p.Item("percentage")) > 0 AndAlso CInt(p.Item("fundactivityunkid")) > 0) Select (p)).ToList
                                Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundprojectcodeunkid")) = intProjectCodeId AndAlso CInt(p.Item("Employeeunkid")) = intAllcId AndAlso CInt(p.Item("periodunkid")) = intPeriodID AndAlso CDec(p.Item("percentage")) > 0 AndAlso CInt(p.Item("fundactivityunkid")) > 0) Select (p)).ToList
                                'Sohail (13 Oct 2017) -- End
                                If lstRow.Count > 0 Then
                                    For Each dRow As DataRow In lstRow
                                        Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                        'Sohail (13 Oct 2017) -- Start
                                        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                                        'Dim decNetPay As Decimal = (From p In dtTemp Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                                        Dim decNetPay As Decimal = (From p In dsTemp.Tables(1) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso CInt(p.Item("payperiodunkid")) = intPeriodID AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                                        'Sohail (13 Oct 2017) -- End
                                        decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                                    Next
                                End If

                                dr.Item(pair.Value) = Format(decTotal, strfmtCurrency)
                            Next

                        End If

                        intPrevCCGrpId = CInt(dtRow.Item("costcentergroupmasterunkid"))
                        intPrevCCId = CInt(dtRow.Item("costcenterunkid"))

                        'Sohail (13 Oct 2017) -- Start
                        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                    Next
                    'Sohail (13 Oct 2017) -- End

                Next
                dtTable.AcceptChanges()
                'Dim intActivityId As Integer

                '    Dim decActBal As Decimal = 0

                '    For Each pair In mdicActivityCode
                '        intActivityId = pair.Key
                '        decTotal = 0
                '        decActBal = 0

                '        'If mdicActivity.ContainsKey(intActivityId) = True Then
                '        '    decActBal = mdicActivity.Item(intActivityId)
                '        'End If

                '        Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundactivityunkid")) = intActivityId AndAlso CDec(p.Item("percentage")) > 0) Select (p)).ToList
                '        If lstRow.Count > 0 Then
                '            For Each dRow As DataRow In lstRow
                '                Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                '                Dim decNetPay As Decimal = (From p In dsBalance.Tables(0) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("balanceamount")) = False) Select (CDec(p.Item("balanceamount")))).Sum
                '                decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                '            Next

                '            If decTotal > decActBal Then
                '                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Actual Salary Payment will be exceeding to the Activity Current Balance.") & vbCrLf & vbCrLf & mdicActivityCode.Item(pair.Key).ToString & " Actual Salary : " & Format(decTotal, GUI.fmtCurrency) & vbCrLf & mdicActivityCode.Item(pair.Key).ToString & " Current Balance : " & Format(pair.Value, GUI.fmtCurrency))
                '                'Exit For
                '                'Dim dr As DataRow = dtTable.NewRow
                '                'dr.Item("Activity Code") = pair.Value
                '                'dr.Item("Current Balance") = Format(decActBal, GUI.fmtCurrency)
                '                'dr.Item("Actual Salary") = Format(decTotal, GUI.fmtCurrency)
                '                'dtTable.Rows.Add(dr)
                '            End If
                '        End If
                '    Next
            End If


            'Dim strTranHeadIDList As String = String.Join(",", (From p In mdicHeadMapping Select (p.Key.ToString)).ToArray)
            'dsAllHeads = Nothing
            'dsList = objFundProjectCode.GetList("FundProjectCode")
            'mdicFund = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Total = CDec(p.Item("currentceilingbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
            'Dim dicFundCode As Dictionary(Of Integer, String) = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Code = p.Item("fundprojectcode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
            'dsList = objActivity.GetComboList("Activity", True)
            'Dim d_row() As DataRow = dsList.Tables(0).Select("fundactivityunkid = 0")
            'If d_row.Length > 0 Then
            '    d_row(0).Item("activitycode") = "" 'Sohail (29 Mar 2017)
            '    d_row(0).Item("activityname") = ""
            '    dsList.Tables(0).AcceptChanges()
            'End If
            'Dim dicActivityCode As Dictionary(Of Integer, String) = (From p In dsList.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activitycode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)

            'dsList = objBudgetCodes.GetDataGridList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtBudgetDate, mdtBudgetDate, xUserModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, mintBudgetId, mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, CInt(objBudget._Whotoincludeid), "Budget", mintPeriodId, True, "", dsAllHeads, True, mstrAnalysis_CodeField)

            'Dim strExpression As String = ""
            'For Each pair In mdicFund
            '    strExpression &= " + [|_" & pair.Key.ToString & "]"
            '    dsList.Tables(0).Columns.Add("A||_" & pair.Key.ToString, System.Type.GetType("System.String")).DefaultValue = ""
            'Next
            'If strExpression.Trim <> "" Then
            '    dsList.Tables(0).Columns.Add("colhTotal", System.Type.GetType("System.Decimal"), strExpression.Substring(2)).DefaultValue = 0
            'End If
            'dsList.Tables(0).Columns.Add("colhDiff", System.Type.GetType("System.Decimal"), "budgetamount - colhTotal").DefaultValue = 0

            'mdtTableExcel = dsList.Tables(0)

            'Dim lst_Row As List(Of DataRow) = (From p In mdtTableExcel Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("colhTotal")) <> 100) Select (p)).ToList
            'If lst_Row.Count > 0 Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Total Percentage should be 100 for all transactions.")
            '    Return False
            'End If



            'Dim decTotal As Decimal = 0
            'For Each dsRow As DataRow In mdtTableExcel.Rows


            '    dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")), strfmtCurrency)
            '    For Each pair In dicFundCode
            '        decTotal = 0

            '        If CDec(dsRow.Item("|_" & pair.Key.ToString)) > 0 Then
            '            Dim intAllocUnkId As Integer = CInt(dsRow.Item("allocationtranunkid"))
            '            Dim decNetPay As Decimal = (From p In dsBalance.Tables(0) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
            '            decTotal += decNetPay * CDec(dsRow.Item("|_" & pair.Key.ToString)) / 100
            '        End If

            '        'dsRow.Item("|_" & pair.Key.ToString) = Format(CDec(dsRow.Item("|_" & pair.Key.ToString)), strfmtCurrency)
            '        dsRow.Item("|_" & pair.Key.ToString) = Format(decTotal, strfmtCurrency)

            '        If dicActivityCode.ContainsKey(CInt(dsRow.Item("||_" & pair.Key.ToString))) = True Then
            '            dsRow.Item("A||_" & pair.Key.ToString) = dicActivityCode.Item(CInt(dsRow.Item("||_" & pair.Key.ToString)))
            '        Else
            '            dsRow.Item("A||_" & pair.Key.ToString) = ""
            '        End If
            '    Next
            '    dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")) / mstrPeriodIdList.Split(",").ToArray.Count, strfmtCurrency)
            '    'dsRow.Item("colhTotal") = Format(CDec(dsRow.Item("colhTotal")), strfmtCurrency)
            '    'dsRow.Item("colhDiff") = Format(CDec(dsRow.Item("colhDiff")), strfmtCurrency)
            'Next
            'mdtTableExcel.AcceptChanges()

            Dim rowsArrayHeader As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell


            'Dim mintColumn As Integer = 0
            'mdtTableExcel.Columns("GCode").Caption = Language.getMessage(mstrModuleName, 2, "Code") 'mstrReport_GroupName.Replace(" :", "")
            'mdtTableExcel.Columns("GCode").SetOrdinal(mintColumn)
            'mintColumn += 1

            'mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName.Replace(" :", "")
            'mdtTableExcel.Columns("GName").SetOrdinal(mintColumn)
            'mintColumn += 1

            ''If CInt(mintViewById) = enBudgetViewBy.Employee Then
            ''    mdtTableExcel.Columns("EmpJobTitle").Caption = Language.getMessage(mstrModuleName, 2, "Job Title")
            ''    mdtTableExcel.Columns("EmpJobTitle").SetOrdinal(mintColumn)
            ''    mintColumn += 1
            ''End If

            'mdtTableExcel.Columns("budgetamount").Caption = Language.getMessage(mstrModuleName, 3, "Budget Amount")
            'mdtTableExcel.Columns("budgetamount").SetOrdinal(mintColumn)
            'mintColumn += 1

            'If mdtTableExcel.Columns.Contains("colhTotal") = True Then
            '    mdtTableExcel.Columns("colhTotal").Caption = Language.getMessage(mstrModuleName, 4, "Actual Amount")
            '    mdtTableExcel.Columns("colhTotal").SetOrdinal(mintColumn)
            '    mintColumn += 1
            'End If

            'If mdtTableExcel.Columns.Contains("colhDiff") = True Then
            '    mdtTableExcel.Columns("colhDiff").Caption = Language.getMessage(mstrModuleName, 5, "Difference")
            '    mdtTableExcel.Columns("colhDiff").SetOrdinal(mintColumn)
            '    mintColumn += 1
            'End If

            'For Each pair In dicFundCode
            '    mdtTableExcel.Columns("|_" & pair.Key.ToString).Caption = pair.Value.ToString
            '    mdtTableExcel.Columns("|_" & pair.Key.ToString).SetOrdinal(mintColumn)
            '    mintColumn += 1

            '    'mdtTableExcel.Columns("A||_" & pair.Key.ToString).Caption = Language.getMessage(mstrModuleName, 5, "Activity Code")
            '    'mdtTableExcel.Columns("A||_" & pair.Key.ToString).SetOrdinal(mintColumn)
            '    'mintColumn += 1
            'Next


            'For i = mintColumn To mdtTableExcel.Columns.Count - 1
            '    mdtTableExcel.Columns.RemoveAt(mintColumn)
            'Next
            'Sohail (18 Aug 2017) -- Start
            'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
            dtTable.Columns("Cost Center").ExtendedProperties.Add("style", "s8b")
            'Sohail (18 Aug 2017) -- End

            dtTable.Columns.Remove("CCGroupunkid")
            dtTable.Columns.Remove("CCenterunkid")

            row = New WorksheetRow()
            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Period") & " : " & mstrPeriodName & " ", "s9w")
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Period") & " : " & mstrBudgetCodesPeriodNames & " ", "s9w")
            'Sohail (13 Oct 2017) -- End
            wcell.MergeAcross = dtTable.Columns.Count - 1
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Salary Cost Breakdown By Cost Center"), "HeaderStyle")
            wcell.MergeAcross = 4
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Salary Cost Breakdown by Donor"), "HeaderStyle")
            wcell.MergeAcross = dtTable.Columns.Count - 1 - 5
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            'row = New WorksheetRow()
            'wcell = New WorksheetCell("", "HeaderStyle")
            'row.Cells.Add(wcell)

            'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Actual Vs Budget"), "HeaderStyle")
            'wcell.MergeAcross = 2
            'row.Cells.Add(wcell)

            'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Donor Actual Cost Allocation"), "HeaderStyle")
            'wcell.MergeAcross = mdtTableExcel.Columns.Count - 1 - 5
            'row.Cells.Add(wcell)

            'rowsArrayHeader.Add(row)

            'SET EXCEL CELL WIDTH
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dtTable.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                'Sohail (18 Aug 2017) -- Start
                'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
                'intArrayColumnWidth(i) = 125
                If i <= 1 Then
                    intArrayColumnWidth(i) = 175
                Else
                    intArrayColumnWidth(i) = 125
                End If
                'Sohail (18 Aug 2017) -- End
            Next
            'SET EXCEL CELL WIDTH

            Dim strGTotal As String = Language.getMessage(mstrModuleName, 12, "Total")
            Dim strSubTotal As String = Language.getMessage(mstrModuleName, 13, "Total")
            'Sohail (18 Aug 2017) -- Start
            'Enhancement - 69.1 - One group in vertical grouping in advance excel for Salary Cost Breakdown by Cost Center report.
            'Dim strarrGroupColumns As String() = {"Cost Center Group", "Cost Center"}
            Dim strarrGroupColumns As String() = {"Cost Center Group"}
            'Sohail (18 Aug 2017) -- End

            'Pinkal (09-Oct-2017) -- Start
            'Enhancement - Working on Budget Report Changes for CCBRT.
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenAfterExport, dtTable, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", Nothing, strGTotal, True, rowsArrayHeader, Nothing, Nothing, , , True)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenAfterExport, dtTable, intArrayColumnWidth, True, True, True, strarrGroupColumns, strReportType, "", "", Nothing, strGTotal, True, rowsArrayHeader, Nothing, Nothing, , , True)
            'Pinkal (09-Oct-2017) -- End

            Return True

        Catch ex As Exception
            exForce = New Exception(ex.Message & "; Procedure Name: Export_Salary_Cost_Breakdown_by_Cost_Center_Report; Module Name: " & mstrModuleName)
            Throw exForce
            Return False
        End Try
    End Function


    'Pinkal (09-Oct-2017) -- Start
    'Enhancement - Working on Budget Report Changes for CCBRT.
    Public Function Export_Monthly_Employee_Budget_Variance_Analysis_ByActivtiy_Report(ByVal strReportType As String _
                                , ByVal xUserUnkid As Integer _
                                , ByVal xCompanyUnkid As Integer _
                                , ByVal xUserModeSetting As String _
                                , ByVal xOnlyApproved As Boolean _
                                , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                , ByVal blnApplyUserAccessFilter As Boolean _
                                , ByVal strfmtCurrency As String _
                                , ByVal xExportReportPath As String _
                                , ByVal xOpenAfterExport As Boolean _
                                , ByVal xDatabaseName As String _
                                , ByVal xYearUnkid As Integer _
                                )

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As DataSet
        Dim objBudget As New clsBudget_MasterNew
        Dim objBudget_Tran As New clsBudget_TranNew
        Dim objBudgetCodes As New clsBudgetcodes_master
        'Sohail (13 Oct 2017) -- Start
        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
        Dim objBudgetCodes_Tran As New clsBudgetcodes_Tran
        'Sohail (13 Oct 2017) -- End
        Dim objFundProjectCode As New clsFundProjectCode
        Dim objActivity As New clsfundactivity_Tran
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objPeriod As New clscommom_period_Tran
        'Dim mstrPreviousPeriodDBName As String 'Sohail (13 Oct 2017)

        Dim mdicFund As New Dictionary(Of Integer, Decimal)
        Dim dsAllHeads As DataSet = Nothing
        Dim mintViewById As Integer
        'Sohail (13 Oct 2017) -- Start
        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
        'Dim mintPresentationModeId As Integer
        'Dim mdtBudgetDate As Date
        'Sohail (13 Oct 2017) -- End
        Dim mstrPeriodIdList As String = ""
        Dim mdicHeadMapping As New Dictionary(Of Integer, Integer)
        Dim mstrAllocationTranUnkIDs As String = ""
        Dim mstrEmployeeIDs As String = ""
        Dim dtPeriodStart As Date
        Dim dtPeriodEnd As Date
        Dim objTnALeave As New clsTnALeaveTran

        Try
            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'mstrPeriodIdList = objBudgetPeriod.GetPeriodIDs(mintBudgetId)
            'mdicHeadMapping = objBudgetHeadMapping.GetHeadMapping(mintBudgetId)
            'mstrAllocationTranUnkIDs = objBudget_Tran.GetAllocationTranUnkIDs(mintBudgetId)

            'objBudget._Budgetunkid = mintBudgetId
            'mintViewIdx = objBudget._Allocationbyid
            'mintViewById = objBudget._Viewbyid
            'mintPresentationModeId = objBudget._Presentationmodeid
            'mdtBudgetDate = objBudget._Budget_date.Date
            'mstrPreviousPeriodDBName = objPeriod.GetDatabaseName(objBudget._PreviousPeriodyearunkid, xCompanyUnkid)
            'objPeriod._Periodunkid(xDatabaseName) = mintPeriodId
            'dtPeriodStart = objPeriod._Start_Date
            'dtPeriodEnd = objPeriod._End_Date
            dtPeriodStart = mdtPeriodStart
            dtPeriodEnd = mdtPeriodEnd
            mintViewById = mintBudgetViewByID
            mintViewIdx = mintAllocationByID
            'Sohail (13 Oct 2017) -- End

            If CInt(mintViewById) = enBudgetViewBy.Allocation Then
                mstrAllocationTranUnkIDs = objBudgetCodes_Tran.getEmployeeIds(mstrBudgetCodesIDs) 'Sohail (13 Oct 2017)
                frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, dtPeriodEnd, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                If mintViewIdx = 0 Then mintViewIdx = -1
            ElseIf CInt(mintViewById) = enBudgetViewBy.Employee Then
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'mstrEmployeeIDs = objBudget_Tran.getEmployeeIds(mintBudgetId)
                mstrEmployeeIDs = objBudgetCodes_Tran.getEmployeeIds(mstrBudgetCodesIDs)
                'Sohail (13 Oct 2017) -- End
                frmEmpSelection.GetAnalysisByDetails("frmEmpSelection", mstrEmployeeIDs, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                mintViewIdx = 0
                mstrAllocationTranUnkIDs = mstrEmployeeIDs
            End If

            'Dim strTranHeadIDList As String = String.Join(",", (From p In mdicHeadMapping Select (p.Key.ToString)).ToArray) 'Sohail (13 Oct 2017)
            dsAllHeads = Nothing
            dsList = objFundProjectCode.GetList("FundProjectCode", , True)
            mdicFund = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Total = CDec(p.Item("currentceilingbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
            Dim dicFundCode As Dictionary(Of Integer, String) = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Code = p.Item("fundprojectcode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
            dsList = objActivity.GetComboList("Activity", True, , True)
            Dim d_row() As DataRow = dsList.Tables(0).Select("fundactivityunkid = 0")
            If d_row.Length > 0 Then
                d_row(0).Item("activitycode") = ""
                d_row(0).Item("activityname") = ""
                dsList.Tables(0).AcceptChanges()
            End If
            Dim dicActivityCode As Dictionary(Of Integer, String) = (From p In dsList.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activitycode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'dsList = objBudgetCodes.GetDataGridList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, mintBudgetId, mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, CInt(objBudget._Whotoincludeid), "Budget", mintPeriodId, True, "", dsAllHeads, True, mstrAnalysis_CodeField)
            dsList = objBudgetCodes.GetBudgetAllocation(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrBudgetCodesIDs, mintBudgetViewByID, enBudgetPresentation.TransactionWise, 0, "Budget", True, "", dsAllHeads, True, mstrAnalysis_CodeField)
            'Sohail (13 Oct 2017) -- End

            Dim strExpression As String = ""
            For Each pair In mdicFund
                strExpression &= " + [|_" & pair.Key.ToString & "]"
                dsList.Tables(0).Columns.Add("A||_" & pair.Key.ToString, System.Type.GetType("System.String")).DefaultValue = ""
            Next

            If strExpression.Trim <> "" Then
                dsList.Tables(0).Columns.Add("colhTotal", System.Type.GetType("System.Decimal"), strExpression.Substring(2)).DefaultValue = 0
            End If
            dsList.Tables(0).Columns.Add("colhDiff", System.Type.GetType("System.Decimal"), "budgetamount - colhTotal").DefaultValue = 0

            mdtTableExcel = dsList.Tables(0)
            mdtTableExcel.Columns.Add("G_Id", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTableExcel.Columns.Add("G_Name", System.Type.GetType("System.String")).DefaultValue = ""


            Dim lst_Row As List(Of DataRow) = (From p In mdtTableExcel Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("colhTotal")) <> 100) Select (p)).ToList
            If lst_Row.Count > 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Total Percentage should be 100 for all transactions.")
                Return False
            End If


            Dim dsBalance As DataSet = Nothing
            If mintTranHeadId > 0 Then

                'Sohail (08 Nov 2017) -- Start
                'Issue - 70.1 - Incorrect syntax near the keyword 'ON'.
                'StrQ = "SELECT  prtnaleave_tran.tnaleavetranunkid  " & _
                '          ", prtnaleave_tran.payperiodunkid " & _
                '          ", prtnaleave_tran.employeeunkid " & _
                '          ", prpayrollprocess_tran.payrollprocesstranunkid " & _
                '          ", prpayrollprocess_tran.tranheadunkid " & _
                '          ", ISNULL(prpayrollprocess_tran.amount, 0) AS total_amount " & _
                '    "FROM    prtnaleave_tran " & _
                '            "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '    "WHERE   prtnaleave_tran.isvoid = 0 " & _
                '             "AND prpayrollprocess_tran.isvoid = 0 " & _
                '             "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid " & _
                '             "AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") "
                ''Sohail (13 Oct 2017) - [AND prtnaleave_tran.payperiodunkid = @payperiodunkid] = [AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ")]
                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , False, xDatabaseName)
                If blnApplyUserAccessFilter = True Then
                    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
                End If
                Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, xDatabaseName)

                StrQ = "SELECT  prtnaleave_tran.tnaleavetranunkid  " & _
                          ", prtnaleave_tran.payperiodunkid " & _
                          ", prtnaleave_tran.employeeunkid " & _
                          ", prpayrollprocess_tran.payrollprocesstranunkid " & _
                          ", prpayrollprocess_tran.tranheadunkid " & _
                          ", ISNULL(prpayrollprocess_tran.amount, 0) AS total_amount " & _
                          ", ADF.* " & _
                    "FROM    prtnaleave_tran " & _
                            "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE   prtnaleave_tran.isvoid = 0 " & _
                            "AND prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid " & _
                            "AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If

                'If mstrAdvanceFilter.Length > 0 Then
                '    StrQ &= " AND " & mstrAdvanceFilter
                'End If
                'Sohail (08 Nov 2017) -- End

                objDataOperation = New clsDataOperation

                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                'Sohail (13 Oct 2017) -- End
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadId)

                dsBalance = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

            Else
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, mintPeriodId, "")
                Dim sf As New List(Of clsSQLFilterCollection)
                sf.Add(New clsSQLFilterCollection(" AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") ", "", SqlDbType.Int, eZeeDataType.INT_SIZE, 0))
                'Sohail (22 Oct 2021) -- Start
                'NMB Issue :  : Performance issue on process payroll.
                'dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, 0, "", , , sf)
                dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, 0, "", " 1 = 1 ", , sf)
                'Sohail (22 Oct 2021) -- End
                'Sohail (13 Oct 2017) -- End
            End If

            Dim dsEmp As DataSet = Nothing
            If m_intViewIndex > 0 Then
                Dim objEmp As New clsEmployee_Master
                dsEmp = objEmp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, "Emp", False, -1, False, "", False, True, True)
            End If

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            Dim dicBudgetPeriod As New Dictionary(Of Integer, String)
            For Each strID As String In mstrBudgetIDs.Split(",")
                If dicBudgetPeriod.ContainsKey(CInt(strID)) = False Then
                    dicBudgetPeriod.Add(CInt(strID), objBudgetPeriod.GetPeriodIDs(CInt(strID)))
                End If
            Next
            Dim mdtExcel As DataTable = mdtTableExcel.Clone
            Dim d_r As DataRow = Nothing
            Dim strPrevKey As String = ""
            Dim strCurrKey As String = ""
            'Sohail (13 Oct 2017) -- End

            Dim decTotal As Decimal = 0
            Dim intEmpID As Integer = 0
            Dim intProjectCodeId As Integer = 0
            For Each dsRow As DataRow In mdtTableExcel.Rows
                dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")), strfmtCurrency)
                For Each pair In dicFundCode
                    decTotal = 0
                    If CDec(dsRow.Item("|_" & pair.Key.ToString)) > 0 Then
                        Dim intAllocUnkId As Integer = CInt(dsRow.Item("allocationtranunkid"))
                        Dim intPeriodID As Integer = CInt(dsRow.Item("periodunkid"))
                        Dim decNetPay As Decimal = (From p In dsBalance.Tables(0) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso CInt(p.Item("payperiodunkid")) = intPeriodID AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                        decTotal += decNetPay * CDec(dsRow.Item("|_" & pair.Key.ToString)) / 100

                        If dicActivityCode.ContainsKey(CInt(dsRow.Item("||_" & pair.Key.ToString))) = True Then
                            dsRow.Item("A||_" & pair.Key.ToString) = dicActivityCode.Item(CInt(dsRow.Item("||_" & pair.Key.ToString)))
                        Else
                            dsRow.Item("A||_" & pair.Key.ToString) = ""
                        End If
                    End If
                    dsRow.Item("|_" & pair.Key.ToString) = Format(decTotal, strfmtCurrency)
                Next

                If dicBudgetPeriod.ContainsKey(CInt(dsRow.Item("budgetunkid"))) = True Then
                    dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")) / dicBudgetPeriod.Item(CInt(dsRow.Item("budgetunkid"))).Split(",").ToArray.Count, strfmtCurrency)
                Else
                    dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")), strfmtCurrency)
                End If

                If m_intViewIndex > 0 Then
                    Dim dr() As DataRow = dsEmp.Tables(0).Select("employeeunkid = " & CInt(dsRow.Item("allocationtranunkid")) & " ")
                    dsRow.Item("G_Id") = 0
                    dsRow.Item("G_Name") = ""
                    If dr.Length > 0 Then
                        dsRow.Item("G_Id") = CInt(dr(0)(m_strAnalysis_OrderBy.Split(CChar("."))(1).ToString().Trim()))

                        Select Case m_intViewIndex
                            Case enAnalysisReport.Branch
                                dsRow.Item("G_Name") = dr(0)("station").ToString

                            Case enAnalysisReport.Department
                                dsRow.Item("G_Name") = dr(0)("DeptName").ToString

                            Case enAnalysisReport.Section
                                dsRow.Item("G_Name") = dr(0)("Section").ToString

                            Case enAnalysisReport.Unit
                                dsRow.Item("G_Name") = dr(0)("Unit").ToString

                            Case enAnalysisReport.Job
                                dsRow.Item("G_Name") = dr(0)("Job_Name").ToString

                            Case enAnalysisReport.CostCenter
                                dsRow.Item("G_Name") = dr(0)("CostCenter").ToString

                            Case enAnalysisReport.SectionGroup
                                dsRow.Item("G_Name") = dr(0)("SectionGroup").ToString

                            Case enAnalysisReport.UnitGroup
                                dsRow.Item("G_Name") = dr(0)("UnitGroup").ToString

                            Case enAnalysisReport.Team
                                dsRow.Item("G_Name") = dr(0)("Team").ToString

                            Case enAnalysisReport.JobGroup
                                dsRow.Item("G_Name") = dr(0)("JobGroup").ToString

                            Case enAnalysisReport.ClassGroup
                                dsRow.Item("G_Name") = dr(0)("ClassGroup").ToString

                            Case enAnalysisReport.Classs
                                dsRow.Item("G_Name") = dr(0)("Class").ToString

                            Case enAnalysisReport.DepartmentGroup
                                dsRow.Item("G_Name") = dr(0)("DeptGroup").ToString

                            Case enAnalysisReport.GradeGroup
                                dsRow.Item("G_Name") = dr(0)("GradeGroup").ToString

                            Case enAnalysisReport.Grade
                                dsRow.Item("G_Name") = dr(0)("Grade").ToString

                            Case enAnalysisReport.GradeLevel
                                dsRow.Item("G_Name") = dr(0)("GradeLevel").ToString

                        End Select

                    End If
                End If

                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                strCurrKey = dsRow.Item("allocationtranunkid").ToString() & "_" & dsRow.Item("jobunkid").ToString()

                If strPrevKey <> strCurrKey Then
                    d_r = mdtExcel.NewRow
                    d_r.ItemArray = dsRow.ItemArray

                    mdtExcel.Rows.Add(d_r)
                Else
                    d_r = mdtExcel.Rows(mdtExcel.Rows.Count - 1)

                    For Each pair In dicFundCode
                        d_r.Item("|_" & pair.Key.ToString) += CDec(dsRow.Item("|_" & pair.Key.ToString))
                        If dsRow.Item("A||_" & pair.Key.ToString).ToString.Trim.Length > 0 AndAlso d_r.Item("A||_" & pair.Key.ToString).ToString.Contains(dsRow.Item("A||_" & pair.Key.ToString).ToString) = False Then
                            d_r.Item("A||_" & pair.Key.ToString) += ", " & dsRow.Item("A||_" & pair.Key.ToString)
                        End If
                    Next

                    d_r.Item("budgetamount") += CDec(dsRow.Item("budgetamount"))

                    mdtExcel.AcceptChanges()
                End If
                strPrevKey = strCurrKey
                'Sohail (13 Oct 2017) -- End

            Next
            mdtTableExcel.AcceptChanges()

            If m_intViewIndex > 0 Then
                mdtTableExcel = New DataView(mdtExcel, "allocationtranunkid > 0 AND G_Id IN (" & m_strViewByIds & ") ", "G_Name, G_Id", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTableExcel = New DataView(mdtExcel, "allocationtranunkid > 0 ", "G_Name, G_Id", DataViewRowState.CurrentRows).ToTable
            End If

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell


            Dim mintColumn As Integer = 0
            mdtTableExcel.Columns("GCode").Caption = Language.getMessage(mstrModuleName, 2, "Code")
            mdtTableExcel.Columns("GCode").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName.Replace(" :", "")
            mdtTableExcel.Columns("GName").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("budgetamount").Caption = Language.getMessage(mstrModuleName, 3, "Budget Amount")
            mdtTableExcel.Columns("budgetamount").SetOrdinal(mintColumn)
            mintColumn += 1

            If mdtTableExcel.Columns.Contains("colhTotal") = True Then
                mdtTableExcel.Columns("colhTotal").Caption = Language.getMessage(mstrModuleName, 4, "Actual Amount")
                mdtTableExcel.Columns("colhTotal").SetOrdinal(mintColumn)
                mintColumn += 1
            End If

            If mdtTableExcel.Columns.Contains("colhDiff") = True Then
                mdtTableExcel.Columns("colhDiff").Caption = Language.getMessage(mstrModuleName, 5, "Difference")
                mdtTableExcel.Columns("colhDiff").SetOrdinal(mintColumn)
                mintColumn += 1
            End If

            For Each pair In dicFundCode
                mdtTableExcel.Columns("A||_" & pair.Key.ToString).Caption = Language.getMessage(mstrModuleName, 5, "Activity Code")
                mdtTableExcel.Columns("A||_" & pair.Key.ToString).SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("|_" & pair.Key.ToString).Caption = Language.getMessage(mstrModuleName, 5, "Amount")
                mdtTableExcel.Columns("|_" & pair.Key.ToString).SetOrdinal(mintColumn)
                mintColumn += 1

            Next


            If m_intViewIndex > 0 Then
                mdtTableExcel.Columns("G_Name").Caption = m_strReport_GroupName.Replace(":", "")
                mdtTableExcel.Columns("G_Name").SetOrdinal(mintColumn)
                mintColumn += 1

                Dim arrGrp() As String = {"G_Name"}
                strarrGroupColumns = arrGrp
            End If


            For i = mintColumn To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(mintColumn)
            Next


            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Budget Details") & " : " & mstrBudgetNames & " ", "s9w")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "PD Details") & " : " & mstrBudgetCodesPeriodNames, "s9w")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "HeaderStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell("", "HeaderStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Actual Vs Budget"), "HeaderStyle")
            wcell.MergeAcross = 2
            row.Cells.Add(wcell)


            For Each pair In dicFundCode
                wcell = New WorksheetCell(pair.Value.ToString, "HeaderStyle")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
            Next

            If m_intViewIndex > 0 Then
                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)
            End If

            rowsArrayHeader.Add(row)

            'SET EXCEL CELL WIDTH
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, strReportType, "", " ", Nothing, "", True, rowsArrayHeader)

            Return True

        Catch ex As Exception
            exForce = New Exception(ex.Message & "; Procedure Name: Export_Monthly_Employee_Budget_Variance_Analysis_ByActivtiy_Report; Module Name: " & mstrModuleName)
            Throw exForce
            Return False
        End Try
    End Function

    Public Function Export_Salary_Cost_Breakdown_by_Cost_Center_ByActivity_Report(ByVal strReportType As String _
                              , ByVal xUserUnkid As Integer _
                              , ByVal xCompanyUnkid As Integer _
                              , ByVal xUserModeSetting As String _
                              , ByVal xOnlyApproved As Boolean _
                              , ByVal xIncludeIn_ActiveEmployee As Boolean _
                              , ByVal blnApplyUserAccessFilter As Boolean _
                              , ByVal strfmtCurrency As String _
                              , ByVal xExportReportPath As String _
                              , ByVal xOpenAfterExport As Boolean _
                              , ByVal xDatabaseName As String _
                              , ByVal xYearUnkid As Integer _
                              )

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As DataSet = Nothing
        Dim objBudget As New clsBudget_MasterNew
        Dim objBudget_Tran As New clsBudget_TranNew
        Dim objBudgetCodes As New clsBudgetcodes_master
        'Sohail (13 Oct 2017) -- Start
        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
        Dim objBudgetCodes_Tran As New clsBudgetcodes_Tran
        'Sohail (13 Oct 2017) -- End
        Dim objFundProjectCode As New clsFundProjectCode
        Dim objActivity As New clsfundactivity_Tran
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objPeriod As New clscommom_period_Tran
        'Dim mstrPreviousPeriodDBName As String 'Sohail (13 Oct 2017)

        Dim mdicFund As New Dictionary(Of Integer, Decimal)
        Dim dsAllHeads As DataSet = Nothing
        Dim mintViewById As Integer
        'Sohail (13 Oct 2017) -- Start
        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
        'Dim mintPresentationModeId As Integer
        'Dim mdtBudgetDate As Date
        'Sohail (13 Oct 2017) -- End
        Dim mstrPeriodIdList As String = ""
        Dim mdicHeadMapping As New Dictionary(Of Integer, Integer)
        Dim mstrAllocationTranUnkIDs As String = ""
        Dim mstrEmployeeIDs As String = ""
        Dim dtPeriodStart As Date
        Dim dtPeriodEnd As Date
        Dim objTnALeave As New clsTnALeaveTran
        Dim dicPrjActivityCount As New Dictionary(Of String, Integer)

        Try

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'mstrPeriodIdList = objBudgetPeriod.GetPeriodIDs(mintBudgetId)
            'mdicHeadMapping = objBudgetHeadMapping.GetHeadMapping(mintBudgetId)
            'mstrAllocationTranUnkIDs = objBudget_Tran.GetAllocationTranUnkIDs(mintBudgetId)

            'objBudget._Budgetunkid = mintBudgetId
            'mintViewIdx = objBudget._Allocationbyid
            'mintViewById = objBudget._Viewbyid
            'mintPresentationModeId = objBudget._Presentationmodeid
            'mdtBudgetDate = objBudget._Budget_date.Date
            'mstrPreviousPeriodDBName = objPeriod.GetDatabaseName(objBudget._PreviousPeriodyearunkid, xCompanyUnkid)
            'objPeriod._Periodunkid(xDatabaseName) = mintPeriodId
            'dtPeriodStart = objPeriod._Start_Date
            'dtPeriodEnd = objPeriod._End_Date
            dtPeriodStart = mdtPeriodStart
            dtPeriodEnd = mdtPeriodEnd
            mintViewById = mintBudgetViewByID
            mintViewIdx = mintAllocationByID
            'Sohail (13 Oct 2017) -- End

            Dim dsEmp As DataSet = Nothing
            mstrAnalysis_OrderBy = "hremployee_master.employeeunkid"
            If CInt(mintViewById) = enBudgetViewBy.Allocation Then
                mstrAllocationTranUnkIDs = objBudgetCodes_Tran.getEmployeeIds(mstrBudgetCodesIDs)
                frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, dtPeriodEnd, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)

                Dim sf As New List(Of clsSQLFilterCollection)
                sf.Add(New clsSQLFilterCollection(" AND bgbudgetcodesfundsource_tran.periodunkid IN (" & mstrBudgetCodesPeriodIDs & ") ", "", SqlDbType.Int, eZeeDataType.INT_SIZE, 0))
                dsList = objBudgetCodes.GetEmployeeActivityPercentage(0, 0, 0, 0, mstrAllocationTranUnkIDs, 1, mstrBudgetCodesIDs, sf)

            ElseIf CInt(mintViewById) = enBudgetViewBy.Employee Then
                mstrEmployeeIDs = objBudgetCodes_Tran.getEmployeeIds(mstrBudgetCodesIDs)

                Dim sf As New List(Of clsSQLFilterCollection)
                sf.Add(New clsSQLFilterCollection(" AND bgbudgetcodesfundsource_tran.periodunkid IN (" & mstrBudgetCodesPeriodIDs & ") ", "", SqlDbType.Int, eZeeDataType.INT_SIZE, 0))
                dsList = objBudgetCodes.GetEmployeeActivityPercentage(0, 0, 0, 0, mstrEmployeeIDs, 1, mstrBudgetCodesIDs, sf)
            End If

            Dim dtTable As New DataTable
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim objEmp As New clsEmployee_Master
                Dim dsAct As DataSet = objFundProjectCode.GetList("ProjectCode", , True)
                Dim mdicProjectCode As New Dictionary(Of Integer, String)
                mdicProjectCode = (From p In dsAct.Tables("ProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Code = p.Item("fundprojectcode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)

                dsEmp = objEmp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, "Emp", , , , "")

                dtTable.Columns.Add("CCGroupunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                dtTable.Columns.Add("Cost Center Group", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("CCenterunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                dtTable.Columns.Add("Cost Center", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Cost Center Code", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("No of Employees", System.Type.GetType("System.Int64")).DefaultValue = 0
                dtTable.Columns.Add("Personnel", System.Type.GetType("System.Decimal")).DefaultValue = 0

                Dim dicActivityCode As New Dictionary(Of Integer, String)

                For Each pair In mdicProjectCode
                    Dim dsTmpActivity As DataSet = objActivity.GetComboList("Activity", False, pair.Key, True)
                    dicPrjActivityCount.Add(pair.Value, dsTmpActivity.Tables(0).Rows.Count - 1)
                    For Each dtRow As DataRow In dsTmpActivity.Tables(0).Rows
                        dtTable.Columns.Add("A||" & dtRow("fundactivityunkid").ToString, System.Type.GetType("System.Decimal")).DefaultValue = 0
                        dtTable.Columns("A||" & dtRow("fundactivityunkid").ToString).Caption = dtRow("activitycode").ToString
                        dicActivityCode.Add(CInt(dtRow("fundactivityunkid")), dtRow("activitycode").ToString())
                    Next
                Next
                Dim dsActivity As DataSet = objActivity.GetList("Activity", "", True)

                Dim dsBalance As DataSet = Nothing
                If mintTranHeadId > 0 Then

                    'Sohail (08 Nov 2017) -- Start
                    'Issue - 70.1 - Incorrect syntax near the keyword 'ON'.
                    'StrQ = "SELECT  prtnaleave_tran.tnaleavetranunkid  " & _
                    '          ", prtnaleave_tran.payperiodunkid " & _
                    '          ", prtnaleave_tran.employeeunkid " & _
                    '          ", prpayrollprocess_tran.payrollprocesstranunkid " & _
                    '          ", prpayrollprocess_tran.tranheadunkid " & _
                    '          ", ISNULL(prpayrollprocess_tran.amount, 0) AS total_amount " & _
                    '    "FROM    prtnaleave_tran " & _
                    '            "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    '    "WHERE   prtnaleave_tran.isvoid = 0 " & _
                    '             "AND prpayrollprocess_tran.isvoid = 0 " & _
                    '             "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid " & _
                    '            "AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") "
                    ''Sohail (13 Oct 2017) - [AND prtnaleave_tran.payperiodunkid = @payperiodunkid] = [AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ")]
                    Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                    xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , False, xDatabaseName)
                    If blnApplyUserAccessFilter = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
                    End If
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, xDatabaseName)

                    StrQ = "SELECT  prtnaleave_tran.tnaleavetranunkid  " & _
                              ", prtnaleave_tran.payperiodunkid " & _
                              ", prtnaleave_tran.employeeunkid " & _
                              ", prpayrollprocess_tran.payrollprocesstranunkid " & _
                              ", prpayrollprocess_tran.tranheadunkid " & _
                              ", ISNULL(prpayrollprocess_tran.amount, 0) AS total_amount " & _
                              ", ADF.* " & _
                        "FROM    prtnaleave_tran " & _
                                "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If

                    If blnApplyUserAccessFilter = True Then
                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry
                        End If
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    StrQ &= "WHERE   prtnaleave_tran.isvoid = 0 " & _
                                "AND prpayrollprocess_tran.isvoid = 0 " & _
                                "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid " & _
                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") "

                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If

                    'If mstrAdvanceFilter.Length > 0 Then
                    '    StrQ &= " AND " & mstrAdvanceFilter
                    'End If
                    'Sohail (08 Nov 2017) -- End

                    objDataOperation = New clsDataOperation

                    'objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadId)

                    dsBalance = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    End If

                Else
                    Dim sf As New List(Of clsSQLFilterCollection)
                    sf.Add(New clsSQLFilterCollection(" AND prtnaleave_tran.payperiodunkid IN (" & mstrBudgetCodesPeriodIDs & ") ", "", SqlDbType.Int, eZeeDataType.INT_SIZE, 0))
                    'Sohail (22 Oct 2021) -- Start
                    'NMB Issue :  : Performance issue on process payroll.
                    'dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, 0, "", , , sf)
                    dsBalance = objTnALeave.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, True, "list", 0, 0, "", " 1 = 1 ", , sf)
                    'Sohail (22 Oct 2021) -- End
                End If

                Dim dsTemp As New DataSet
                dsTemp.Tables.Add(New DataView(dsEmp.Tables(0), "employeecode IS NOT NULL", "costcentergroup, costcentergroupmasterunkid, costcenter, costcenterunkid", DataViewRowState.CurrentRows).ToTable)
                dsTemp.Tables.Add(dsBalance.Tables(0).Copy)
                Dim r As New DataRelation("r", dsTemp.Tables(0).Columns("employeeunkid"), dsTemp.Tables(1).Columns("employeeunkid"))
                dsTemp.Relations.Add(r)

                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'Dim dtTemp As DataTable = New DataView(dsBalance.Tables(0), "employeecode IS NOT NULL", "costcentergroup, costcentergroupmasterunkid, costcenter, costcenterunkid", DataViewRowState.CurrentRows).ToTable
                'Sohail (13 Oct 2017) -- End

                Dim intPrevCCGrpId As Integer = -2
                Dim intPrevCCId As Integer = -2
                Dim intEmpCount As Integer = 0
                Dim decAmt As Decimal = 0
                Dim dr As DataRow = Nothing
                Dim intProjectCodeId As Integer
                Dim decTotal As Decimal = 0
                For Each dtRow As DataRow In dsTemp.Tables(0).Rows

                    'Sohail (13 Oct 2017) -- Start
                    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                    Dim intEmp As Integer = 0
                    If dtRow.GetChildRows("r").Length <= 0 Then Continue For

                    For Each chRow As DataRow In dtRow.GetChildRows("r")
                        intEmp += 1
                        'Sohail (13 Oct 2017) -- End

                        If intPrevCCGrpId <> CInt(dtRow.Item("costcentergroupmasterunkid")) OrElse intPrevCCId <> CInt(dtRow.Item("costcenterunkid")) Then
                            intEmpCount = 1
                            'Sohail (13 Oct 2017) -- Start
                            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                            'decAmt = CDec(dtRow.Item("total_amount"))
                            decAmt = CDec(chRow.Item("total_amount"))
                            'Sohail (13 Oct 2017) -- End
                            decTotal = 0

                            dr = dtTable.NewRow
                            If dtRow.Item("costcentergroup").ToString.Trim = "" Then
                                dr.Item("Cost Center Group") = Language.getMessage(mstrModuleName, 111, "N/A")
                            Else
                                dr.Item("Cost Center Group") = dtRow.Item("costcentergroup")
                            End If

                            dr.Item("CCGroupunkid") = dtRow.Item("costcentergroupmasterunkid")
                            dr.Item("CCenterunkid") = dtRow.Item("costcenterunkid")
                            If CInt(dtRow.Item("costcenterunkid")) <= 0 Then
                                dr.Item("Cost Center") = Language.getMessage(mstrModuleName, 222, "N/A")
                                dr.Item("Cost Center Code") = Language.getMessage(mstrModuleName, 333, "N/A")
                            Else
                                dr.Item("Cost Center") = dtRow.Item("costcenter")
                                dr.Item("Cost Center Code") = dtRow.Item("costcentercode")
                            End If

                            dr.Item("No of Employees") = intEmpCount
                            dr.Item("Personnel") = Format(decAmt, strfmtCurrency)

                            'For Each pair In mdicProjectCode
                            '    intProjectCodeId = pair.Key
                            '    Dim intAllcId As Integer = CInt(dtRow.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim))
                            '    Dim intPeriodID As Integer = CInt(chRow.Item("payperiodunkid")) 'Sohail (13 Oct 2017)
                            '    For Each Apair In dicActivityCode
                            '        If Apair.Key <= 0 Then Continue For
                            '        Dim mintActivityId As Integer = Apair.Key
                            '        decTotal = dr.Item("A||" & mintActivityId.ToString())
                            '        Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundprojectcodeunkid")) = intProjectCodeId AndAlso CInt(p.Item("Employeeunkid")) = intAllcId AndAlso CInt(p.Item("periodunkid")) = intPeriodID AndAlso CDec(p.Item("percentage")) > 0 AndAlso CInt(p.Item("fundactivityunkid")) > 0 AndAlso CInt(p.Item("fundactivityunkid")) = mintActivityId) Select (p)).ToList
                            '        If lstRow.Count > 0 Then
                            '            For Each dRow As DataRow In lstRow
                            '                Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                            '                Dim decNetPay As Decimal = (From p In dsTemp.Tables(1) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso CInt(p.Item("payperiodunkid")) = intPeriodID AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                            '                decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                            '            Next
                            '            dr.Item("A||" & mintActivityId.ToString()) = Format(decTotal, strfmtCurrency)
                            '        End If
                            '    Next
                            'Next
                            For Each d_Row As DataRow In dsActivity.Tables(0).Rows
                                intProjectCodeId = CInt(d_Row.Item("fundprojectcodeunkid"))
                                Dim intAllcId As Integer = CInt(dtRow.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim))
                                Dim intPeriodID As Integer = CInt(chRow.Item("payperiodunkid")) 'Sohail (13 Oct 2017)
                                Dim mintActivityId As Integer = CInt(d_Row.Item("fundactivityunkid"))
                                'For Each Apair In dicActivityCode
                                If mintActivityId <= 0 Then Continue For

                                decTotal = dr.Item("A||" & mintActivityId.ToString())
                                Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundprojectcodeunkid")) = intProjectCodeId AndAlso CInt(p.Item("Employeeunkid")) = intAllcId AndAlso CInt(p.Item("periodunkid")) = intPeriodID AndAlso CDec(p.Item("percentage")) > 0 AndAlso CInt(p.Item("fundactivityunkid")) > 0 AndAlso CInt(p.Item("fundactivityunkid")) = mintActivityId) Select (p)).ToList
                                If lstRow.Count > 0 Then
                                    For Each dRow As DataRow In lstRow
                                        Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                        Dim decNetPay As Decimal = (From p In dsTemp.Tables(1) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso CInt(p.Item("payperiodunkid")) = intPeriodID AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                                        decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                                    Next
                                    dr.Item("A||" & mintActivityId.ToString()) = Format(decTotal, strfmtCurrency)
                                End If
                                'Next
                            Next

                            dtTable.Rows.Add(dr)
                        Else
                            'Sohail (13 Oct 2017) -- Start
                            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                            'intEmpCount += 1
                            If intEmp = 1 Then intEmpCount += 1
                            'Sohail (13 Oct 2017) -- End
                            decAmt += CDec(chRow.Item("total_amount"))

                            dr = dtTable.Rows(dtTable.Rows.Count - 1)
                            dr.Item("No of Employees") = intEmpCount
                            dr.Item("Personnel") = Format(decAmt, strfmtCurrency)

                            'For Each pair In mdicProjectCode
                            '    intProjectCodeId = pair.Key
                            '    Dim intAllcId As Integer = CInt(dtRow.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim))
                            '    Dim intPeriodID As Integer = CInt(chRow.Item("payperiodunkid")) 'Sohail (13 Oct 2017)
                            '    For Each Apair In dicActivityCode
                            '        If Apair.Key <= 0 Then Continue For
                            '        Dim mintActivityId As Integer = Apair.Key
                            '        decTotal = dr.Item("A||" & mintActivityId.ToString())
                            '        Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundprojectcodeunkid")) = intProjectCodeId AndAlso CInt(p.Item("Employeeunkid")) = intAllcId AndAlso CInt(p.Item("periodunkid")) = intPeriodID AndAlso CDec(p.Item("percentage")) > 0 AndAlso CInt(p.Item("fundactivityunkid")) > 0 AndAlso CInt(p.Item("fundactivityunkid")) = mintActivityId) Select (p)).ToList
                            '        If lstRow.Count > 0 Then
                            '            For Each dRow As DataRow In lstRow
                            '                Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                            '                Dim decNetPay As Decimal = (From p In dsTemp.Tables(1) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso CInt(p.Item("payperiodunkid")) = intPeriodID AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                            '                decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                            '            Next
                            '            dr.Item("A||" & mintActivityId.ToString()) = Format(decTotal, strfmtCurrency)
                            '        End If
                            '    Next
                            'Next
                            For Each d_Row As DataRow In dsActivity.Tables(0).Rows
                                intProjectCodeId = CInt(d_Row.Item("fundprojectcodeunkid"))
                                Dim intAllcId As Integer = CInt(dtRow.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim))
                                Dim intPeriodID As Integer = CInt(chRow.Item("payperiodunkid")) 'Sohail (13 Oct 2017)
                                Dim mintActivityId As Integer = CInt(d_Row.Item("fundactivityunkid"))
                                'For Each Apair In dicActivityCode
                                If mintActivityId <= 0 Then Continue For

                                decTotal = dr.Item("A||" & mintActivityId.ToString())
                                Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundprojectcodeunkid")) = intProjectCodeId AndAlso CInt(p.Item("Employeeunkid")) = intAllcId AndAlso CInt(p.Item("periodunkid")) = intPeriodID AndAlso CDec(p.Item("percentage")) > 0 AndAlso CInt(p.Item("fundactivityunkid")) > 0 AndAlso CInt(p.Item("fundactivityunkid")) = mintActivityId) Select (p)).ToList
                                If lstRow.Count > 0 Then
                                    For Each dRow As DataRow In lstRow
                                        Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                        Dim decNetPay As Decimal = (From p In dsTemp.Tables(1) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso CInt(p.Item("payperiodunkid")) = intPeriodID AndAlso IsDBNull(p.Item("total_amount")) = False) Select (CDec(p.Item("total_amount")))).Sum
                                        decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                                    Next
                                    dr.Item("A||" & mintActivityId.ToString()) = Format(decTotal, strfmtCurrency)
                                End If
                                'Next
                            Next

                        End If
                        intPrevCCGrpId = CInt(dtRow.Item("costcentergroupmasterunkid"))
                        intPrevCCId = CInt(dtRow.Item("costcenterunkid"))

                        'Sohail (13 Oct 2017) -- Start
                        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                    Next
                    'Sohail (13 Oct 2017) -- End

                Next

                dtTable.AcceptChanges()
            End If


            Dim rowsArrayHeader As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            dtTable.Columns("Cost Center").ExtendedProperties.Add("style", "s8b")

            dtTable.Columns.Remove("CCGroupunkid")
            dtTable.Columns.Remove("CCenterunkid")

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Period") & " : " & mstrBudgetCodesPeriodNames & " ", "s9w")
            wcell.MergeAcross = dtTable.Columns.Count - 1
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Salary Cost Breakdown By Cost Center"), "HeaderStyle")
            wcell.MergeAcross = 4
            row.Cells.Add(wcell)

            For Each pair In dicPrjActivityCount
                wcell = New WorksheetCell(pair.Key.ToString, "HeaderStyle")
                wcell.MergeAcross = pair.Value
                row.Cells.Add(wcell)
            Next

            rowsArrayHeader.Add(row)


            'SET EXCEL CELL WIDTH
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dtTable.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i <= 1 Then
                    intArrayColumnWidth(i) = 175
                Else
                    intArrayColumnWidth(i) = 125
                End If
            Next
            'SET EXCEL CELL WIDTH

            Dim strGTotal As String = Language.getMessage(mstrModuleName, 12, "Total")
            Dim strSubTotal As String = Language.getMessage(mstrModuleName, 13, "Total")
            Dim strarrGroupColumns As String() = {"Cost Center Group"}

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenAfterExport, dtTable, intArrayColumnWidth, True, True, True, strarrGroupColumns, strReportType, "", "", Nothing, strGTotal, True, rowsArrayHeader, Nothing, Nothing, , , True)

            Return True

        Catch ex As Exception
            exForce = New Exception(ex.Message & "; Procedure Name: Export_Salary_Cost_Breakdown_by_Cost_Center_ByActivity_Report; Module Name: " & mstrModuleName)
            Throw exForce
            Return False
        End Try
    End Function
    'Pinkal (09-Oct-2017) -- End


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Total Percentage should be 100 for all transactions.")
            Language.setMessage(mstrModuleName, 2, "Code")
            Language.setMessage(mstrModuleName, 3, "Budget Amount")
            Language.setMessage(mstrModuleName, 4, "Actual Amount")
            Language.setMessage(mstrModuleName, 5, "Difference")
            Language.setMessage(mstrModuleName, 6, "Budget Details")
            Language.setMessage(mstrModuleName, 7, "PD Details")
            Language.setMessage(mstrModuleName, 8, "Actual Vs Budget")
            Language.setMessage(mstrModuleName, 9, "Donor Actual Cost Allocation")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
