'************************************************************************************************************************************
'Class Name : clsLvForm_ApprovalStatus_Report.vb
'Purpose    :
'Date       : 26/04/2014
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsLvForm_ApprovalStatus_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLvForm_ApprovalStatus_Report"
    Private mstrReportId As String = enArutiReport.Leave_Form_Approval_Status_Report  '150
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLeaveId As Integer = 0
    Private mstrLeaveName As String = ""
    Private mintLevelId As Integer = 0
    Private mstrLevelName As String = ""
    Private mintStatusId As Integer = 0
    Private mstrStatus As String = ""
    Private mintLeaveFormId As Integer = 0
    Private mstrLeaveForm As String = ""
    Private mstrOrderByQuery As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    'Pinkal (27-Nov-2014) -- Start
    'Enhancement - PUT CHECKBOX IGNORE SAME LEVEL IF APPROVED / REJECT / RESCHEDULE IN LEAVE APPROVED STATUS REPORT FOR TRA
    Private mblnIgnoreSameLevelIfApproved As Boolean = True
    'Pinkal (27-Nov-2014) -- End

    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Pinkal (06-Jan-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId() As Integer
        Set(ByVal value As Integer)
            mintLeaveId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveName() As String
        Set(ByVal value As String)
            mstrLeaveName = value
        End Set
    End Property

    Public WriteOnly Property _LevelId() As Integer
        Set(ByVal value As Integer)
            mintLevelId = value
        End Set
    End Property

    Public WriteOnly Property _LeveleName() As String
        Set(ByVal value As String)
            mstrLevelName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatus = value
        End Set
    End Property

    Public WriteOnly Property _LeaveFormId() As Integer
        Set(ByVal value As Integer)
            mintLeaveFormId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveForm() As String
        Set(ByVal value As String)
            mstrLeaveForm = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property


    'Pinkal (27-Nov-2014) -- Start
    'Enhancement - PUT CHECKBOX IGNORE SAME LEVEL IF APPROVED / REJECT / RESCHEDULE IN LEAVE APPROVED STATUS REPORT FOR TRA

    Public WriteOnly Property _IgnoreSameLevelIfApproved() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnoreSameLevelIfApproved = value
        End Set
    End Property

    'Pinkal (27-Nov-2014) -- End


    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    'Pinkal (06-Jan-2016) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintLeaveId = 0
            mstrLeaveName = ""
            mintLevelId = 0
            mstrLevelName = ""
            mintStatusId = 0
            mstrStatus = ""
            mintLeaveFormId = 0
            mstrLeaveForm = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""

            'Pinkal (27-Nov-2014) -- Start
            'Enhancement - PUT CHECKBOX IGNORE SAME LEVEL IF APPROVED / REJECT / RESCHEDULE IN LEAVE APPROVED STATUS REPORT FOR TRA
            mblnIgnoreSameLevelIfApproved = True
            'Pinkal (27-Nov-2014) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            If mdtFromDate <> Nothing Then
                Me._FilterQuery &= " AND Convert(Char(8),lvleaveform.startdate,112) >= @startdate "
                objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "From Date: ") & " " & mdtFromDate.Date & " "
            End If

            If mdtToDate <> Nothing Then
                Me._FilterQuery &= " AND Convert(Char(8),lvleaveform.returndate,112) <= @enddate "
                objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "To Date: ") & " " & mdtToDate.Date & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND lvleaveform.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mintLeaveId > 0 Then
                Me._FilterQuery &= " AND lvleaveform.leavetypeunkid = @leavetypeunkid "
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Leave Type: ") & " " & mstrLeaveName & " "
            End If

            If mintLevelId > 0 Then
                Me._FilterQuery &= " AND lvapproverlevel_master.levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Level: ") & " " & mstrLevelName & " "
            End If

            If mintStatusId > 0 Then
                Me._FilterQuery &= " AND lvleaveform.statusunkid = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, " Status: ") & " " & mstrStatus & " "
            End If

            If mintLeaveFormId > 0 Then
                Me._FilterQuery &= " AND lvleaveform.formunkid = @formunkid "
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveFormId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, " Leave Form: ") & " " & mstrLeaveForm & " "
            End If


            'Pinkal (27-Nov-2014) -- Start
            'Enhancement - PUT CHECKBOX IGNORE SAME LEVEL IF APPROVED / REJECT / RESCHEDULE IN LEAVE APPROVED STATUS REPORT FOR TRA

            If mintViewIndex > 0 Then
                If Me.OrderByQuery <> "" Then
                    mstrOrderByQuery &= "ORDER BY lvleaveform.formno,lvapproverlevel_master.priority," & mstrAnalysis_OrderBy_GName & ", " & Me.OrderByQuery
                End If
            Else
                If Me.OrderByQuery <> "" Then
                    mstrOrderByQuery &= "  ORDER BY lvleaveform.formno,lvapproverlevel_master.priority, " & Me.OrderByQuery
                End If
            End If

            'Pinkal (27-Nov-2014) -- End

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, " Order By : ") & " " & Me.OrderByDisplay

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(formno, '')", Language.getMessage(mstrModuleName, 1, "Form No")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(lvleavetype_master.leavename, '')", Language.getMessage(mstrModuleName, 2, "Leave Type")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 3, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", Language.getMessage(mstrModuleName, 4, "Employee")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(appr.employeecode, '')", Language.getMessage(mstrModuleName, 6, "Approver Code")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(appr.firstname, '') + ' ' + ISNULL(appr.othername, '') + ' ' + ISNULL(appr.surname, '')", Language.getMessage(mstrModuleName, 7, "Approver")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(lvapproverlevel_master.levelname, '')", Language.getMessage(mstrModuleName, 8, "Level")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '    Public Sub Generate_DetailReport()
    '        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '        Dim StrQ As String = ""
    '        Dim StrQFilter As String = ""
    '        Dim dsList As New DataSet
    '        Try

    '            If mintCompanyUnkid <= 0 Then
    '                mintCompanyUnkid = Company._Object._Companyunkid
    '            End If

    '            Company._Object._Companyunkid = mintCompanyUnkid
    '            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

    '            If mintUserUnkid <= 0 Then
    '                mintUserUnkid = User._Object._Userunkid
    '            End If

    '            User._Object._Userunkid = mintUserUnkid


    '            objDataOperation = New clsDataOperation

    '            objDataOperation.ClearParameters()

    '            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
    '            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
    '            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
    '            objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
    '            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
    '            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 277, "Issued"))


    '            StrQ = "SELECT SPACE(5) + ISNULL(lvleavetype_master.leavename, '') AS leavetype " & _
    '                      ", ISNULL(formno, '') AS formno " & _
    '                      ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
    '                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee " & _
    '                      ", ISNULL(jb.job_name, '') AS jobtitle " & _
    '                      ", ISNULL(appr.employeecode, '') AS approvercode " & _
    '                      ", ISNULL(appr.firstname, '') + ' ' + ISNULL(appr.othername, '') + ' ' + ISNULL(appr.surname, '') AS approver "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", '' AS GName "
    '            End If

    '            StrQ &= ", ISNULL(lvapproverlevel_master.levelname, '') AS level " & _
    '                      ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.startdate),'') AS startdate " & _
    '                      ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.returndate),'') AS enddate " & _
    '                      ", ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid  " & _
    '                      "  AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND ISNULL(lvleaveform.returndate,lvleaveform.startdate) AND ISNULL(lvleaveday_fraction.approverunkid,0) <=0),0.00) days " & _
    '                      ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.approve_stdate),'') AS app_startdate " & _
    '                      ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.approve_eddate),'') AS app_enddate " & _
    '                      ", ISNULL(lvleaveform.approve_days, 0.00) AS approve_days "

    '            'Pinkal (28-Oct-2014) -- Start
    '            'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.

    '            'StrQ &= ", CASE WHEN lvleaveform.statusunkid = 1 THEN @Approve " & _
    '            '                 "WHEN lvleaveform.statusunkid = 2 THEN @Pending " & _
    '            '                 "WHEN lvleaveform.statusunkid = 3 THEN @Reject " & _
    '            '                 "WHEN lvleaveform.statusunkid = 4 THEN @ReSchedule " & _
    '            '                 "WHEN lvleaveform.statusunkid = 6 THEN @Cancel " & _
    '            '                 "WHEN lvleaveform.statusunkid = 7 THEN @Issued " & _
    '            '         " END AS status "

    '            StrQ &= ", CASE WHEN lvpendingleave_tran.statusunkid = 1 THEN @Approve " & _
    '                            "WHEN lvpendingleave_tran.statusunkid = 2 THEN @Pending " & _
    '                            "WHEN lvpendingleave_tran.statusunkid = 3 THEN @Reject " & _
    '                            "WHEN lvpendingleave_tran.statusunkid = 4 THEN @ReSchedule " & _
    '                            "WHEN lvpendingleave_tran.statusunkid = 6 THEN @Cancel " & _
    '                            "WHEN lvpendingleave_tran.statusunkid = 7 THEN @Issued " & _
    '                    " END AS status "

    '            'Pinkal (28-Oct-2014) -- End

    '            StrQ &= " ,lvleaveform.formunkid " & _
    '                     ", lvleaveform.employeeunkid " & _
    '                     ", lvleaveform.leavetypeunkid " & _
    '                     ", ISNULL(lvapproverlevel_master.levelunkid,-1) AS  levelunkid " & _
    '                     ", lvleaveform.statusunkid " & _
    '                     ", ISNULL(lvapproverlevel_master.priority,-1) AS priority " & _
    '                     ", lvpendingleave_tran.statusunkid AS ApproverStatusID " & _
    '                     " FROM lvleaveform " & _
    '                     " LEFT JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
    '                     " LEFT JOIN hrjob_master jb ON hremployee_master.jobunkid = jb.jobunkid " & _
    '                     " LEFT JOIN lvleavetype_master ON dbo.lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleavetype_master.isactive = 1 " & _
    '                     " JOIN lvpendingleave_tran ON lvleaveform.employeeunkid = lvpendingleave_tran.employeeunkid AND lvpendingleave_tran.formunkid = lvleaveform.formunkid AND lvpendingleave_tran.isvoid = 0 " & _
    '                     " LEFT JOIN lvleaveapprover_master ON lvpendingleave_tran.approverunkid = lvleaveapprover_master.leaveapproverunkid AND lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid " & _
    '                     " AND lvleaveapprover_master.isvoid = 0 " & _
    '                     " LEFT JOIN hremployee_master appr ON lvleaveapprover_master.leaveapproverunkid = appr.employeeunkid " & _
    '                     "  LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 "


    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If


    '            StrQ &= " WHERE  lvleaveform.isvoid = 0 "

    '            If mstrAdvance_Filter.Trim.Length > 0 Then
    '                StrQ &= " AND " & mstrAdvance_Filter
    '            End If


    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'End If

    '            If mstrUserAccessFilter.Trim.Length <= 0 Then
    '                mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '            End If

    '            If mstrUserAccessFilter.Trim.Length > 0 Then
    '                StrQ &= mstrUserAccessFilter
    '            End If

    '            Call FilterTitleAndFilterQuery()

    '            StrQ &= Me._FilterQuery

    '            StrQ &= mstrOrderByQuery

    '            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

    '            Dim strarrGroupColumns As String() = Nothing
    '            Dim rowsArrayHeader As New ArrayList
    '            Dim rowsArrayFooter As New ArrayList
    '            Dim row As WorksheetRow
    '            Dim wcell As WorksheetCell

    '            Dim mdtTableExcel As DataTable = dsList.Tables(0)

    '            'Pinkal (27-Nov-2014) -- Start
    '            'Enhancement - PUT CHECKBOX IGNORE SAME LEVEL IF APPROVED / REJECT / RESCHEDULE IN LEAVE APPROVED STATUS REPORT FOR TRA.

    '            If mblnIgnoreSameLevelIfApproved Then
    'ReGenerate:
    '                Dim mintPrioriry As Integer = 0
    '                For i As Integer = 0 To mdtTableExcel.Rows.Count - 1

    '                    If mintPrioriry <> CInt(mdtTableExcel.Rows(i)("priority")) Then
    '                        mintPrioriry = CInt(mdtTableExcel.Rows(i)("priority"))
    '                        Dim drRow() As DataRow = dsList.Tables(0).Select("formunkid = " & mdtTableExcel.Rows(i)("formunkid") & " AND ApproverStatusID <> 2 AND  priority  = " & mintPrioriry)
    '                        If drRow.Length > 0 Then
    '                            Dim drPending() As DataRow = mdtTableExcel.Select("formunkid = " & mdtTableExcel.Rows(i)("formunkid") & " AND ApproverStatusID = 2 AND priority  = " & mintPrioriry)
    '                            If drPending.Length > 0 Then
    '                                For Each dRow As DataRow In drPending
    '                                    mdtTableExcel.Rows.Remove(dRow)
    '                                    GoTo ReGenerate
    '                                Next
    '                                mdtTableExcel.AcceptChanges()
    '                            End If
    '                        End If
    '                    End If
    '                Next
    '            End If

    '            'Pinkal (27-Nov-2014) -- End


    '            If mdtTableExcel.Columns.Contains("formunkid") Then
    '                mdtTableExcel.Columns.Remove("formunkid")
    '            End If

    '            If mdtTableExcel.Columns.Contains("employeeunkid") Then
    '                mdtTableExcel.Columns.Remove("employeeunkid")
    '            End If

    '            If mdtTableExcel.Columns.Contains("leavetypeunkid") Then
    '                mdtTableExcel.Columns.Remove("leavetypeunkid")
    '            End If

    '            If mdtTableExcel.Columns.Contains("levelunkid") Then
    '                mdtTableExcel.Columns.Remove("levelunkid")
    '            End If

    '            If mdtTableExcel.Columns.Contains("statusunkid") Then
    '                mdtTableExcel.Columns.Remove("statusunkid")
    '            End If


    '            'Pinkal (27-Nov-2014) -- Start
    '            'Enhancement - PUT CHECKBOX IGNORE SAME LEVEL IF APPROVED / REJECT / RESCHEDULE IN LEAVE APPROVED STATUS REPORT FOR TRA
    '            If mdtTableExcel.Columns.Contains("priority") Then
    '                mdtTableExcel.Columns.Remove("priority")
    '            End If

    '            If mdtTableExcel.Columns.Contains("ApproverStatusID") Then
    '                mdtTableExcel.Columns.Remove("ApproverStatusID")
    '            End If
    '            'Pinkal (27-Nov-2014) -- End


    '            If mintViewIndex > 0 Then
    '                If mdtTableExcel.Columns.Contains("Id") Then
    '                    mdtTableExcel.Columns.Remove("Id")
    '                End If
    '                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
    '                Dim strGrpCols As String() = {"GName", "formno"}
    '                strarrGroupColumns = strGrpCols
    '            Else
    '                If mdtTableExcel.Columns.Contains("GName") Then
    '                    mdtTableExcel.Columns.Remove("GName")
    '                End If
    '                Dim strGrpCols As String() = {"formno"}
    '                strarrGroupColumns = strGrpCols
    '            End If

    '            row = New WorksheetRow()

    '            If Me._FilterTitle.ToString.Length > 0 Then
    '                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
    '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
    '                row.Cells.Add(wcell)
    '            End If
    '            rowsArrayHeader.Add(row)

    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayHeader.Add(row)


    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '            '--------------------

    '            If ConfigParameter._Object._IsShowPreparedBy = True Then
    '                row = New WorksheetRow()
    '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
    '                wcell.MergeAcross = 4
    '                row.Cells.Add(wcell)

    '                wcell = New WorksheetCell("", "s10bw")
    '                row.Cells.Add(wcell)
    '                rowsArrayFooter.Add(row)

    '                row = New WorksheetRow()
    '                wcell = New WorksheetCell("", "s10bw")
    '                row.Cells.Add(wcell)
    '                rowsArrayFooter.Add(row)
    '            End If


    '            If ConfigParameter._Object._IsShowCheckedBy = True Then
    '                row = New WorksheetRow()
    '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Checked By :"), "s8bw")
    '                wcell.MergeAcross = 4
    '                row.Cells.Add(wcell)

    '                wcell = New WorksheetCell("", "s10bw")
    '                row.Cells.Add(wcell)
    '                rowsArrayFooter.Add(row)

    '                row = New WorksheetRow()
    '                wcell = New WorksheetCell("", "s10bw")
    '                row.Cells.Add(wcell)
    '                rowsArrayFooter.Add(row)
    '            End If


    '            If ConfigParameter._Object._IsShowApprovedBy = True Then
    '                row = New WorksheetRow()
    '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "Approved By"), "s8bw")
    '                wcell.MergeAcross = 4
    '                row.Cells.Add(wcell)

    '                wcell = New WorksheetCell("", "s10bw")
    '                row.Cells.Add(wcell)
    '                rowsArrayFooter.Add(row)

    '                row = New WorksheetRow()
    '                wcell = New WorksheetCell("", "s10bw")
    '                row.Cells.Add(wcell)
    '                rowsArrayFooter.Add(row)
    '            End If

    '            If ConfigParameter._Object._IsShowReceivedBy = True Then
    '                row = New WorksheetRow()
    '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "Received By :"), "s8bw")
    '                wcell.MergeAcross = 4
    '                row.Cells.Add(wcell)
    '                rowsArrayFooter.Add(row)
    '            End If


    '            '--------------------



    '            For i As Integer = 0 To mdtTableExcel.Rows.Count - 1
    '                If mdtTableExcel.Rows(i)("startdate").ToString.Length > 0 Then
    '                    mdtTableExcel.Rows(i)("startdate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("startdate")), DateFormat.ShortDate)
    '                End If
    '                If mdtTableExcel.Rows(i)("enddate").ToString.Length > 0 Then
    '                    mdtTableExcel.Rows(i)("enddate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("enddate")), DateFormat.ShortDate)
    '                End If
    '                If mdtTableExcel.Rows(i)("app_startdate").ToString.Length > 0 Then
    '                    mdtTableExcel.Rows(i)("app_startdate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("app_startdate")), DateFormat.ShortDate)
    '                End If
    '                If mdtTableExcel.Rows(i)("app_enddate").ToString.Length > 0 Then
    '                    mdtTableExcel.Rows(i)("app_enddate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("app_enddate")), DateFormat.ShortDate)
    '                End If
    '            Next


    '            'SET EXCEL CELL WIDTH  
    '            Dim intArrayColumnWidth As Integer() = Nothing
    '            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
    '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
    '                intArrayColumnWidth(i) = 125
    '            Next
    '            'SET EXCEL CELL WIDTH


    '            mdtTableExcel.Columns("formno").Caption = Language.getMessage(mstrModuleName, 1, "Form No")
    '            mdtTableExcel.Columns("leavetype").Caption = Language.getMessage(mstrModuleName, 2, "Leave Type")
    '            mdtTableExcel.Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 3, "Employee Code")
    '            mdtTableExcel.Columns("employee").Caption = Language.getMessage(mstrModuleName, 4, "Employee")
    '            mdtTableExcel.Columns("jobtitle").Caption = Language.getMessage(mstrModuleName, 5, "Job Title")
    '            mdtTableExcel.Columns("approvercode").Caption = Language.getMessage(mstrModuleName, 6, "Approver Code")
    '            mdtTableExcel.Columns("approver").Caption = Language.getMessage(mstrModuleName, 7, "Approver")
    '            mdtTableExcel.Columns("level").Caption = Language.getMessage(mstrModuleName, 8, "Level")
    '            mdtTableExcel.Columns("startdate").Caption = Language.getMessage(mstrModuleName, 9, "Start Date")
    '            mdtTableExcel.Columns("enddate").Caption = Language.getMessage(mstrModuleName, 10, "End Date")
    '            mdtTableExcel.Columns("days").Caption = Language.getMessage(mstrModuleName, 27, "Applied Days")
    '            mdtTableExcel.Columns("app_startdate").Caption = Language.getMessage(mstrModuleName, 11, "Approved Start Date")
    '            mdtTableExcel.Columns("app_enddate").Caption = Language.getMessage(mstrModuleName, 12, "Approved End Date")
    '            mdtTableExcel.Columns("approve_days").Caption = Language.getMessage(mstrModuleName, 13, "Approved Days")
    '            mdtTableExcel.Columns("status").Caption = Language.getMessage(mstrModuleName, 26, "Status")

    '            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        End Try
    '    End Sub

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            objDataOperation = New clsDataOperation

            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrConditionQry As String

            StrInnerQry = "" : StrConditionQry = ""

            Dim objLeaveApprover As New clsleaveapprover_master
            dsCompany = objLeaveApprover.GetExternalApproverList(objDataOperation, "List")
            objLeaveApprover = Nothing

            'Pinkal (01-Mar-2016) -- End

            objDataOperation.ClearParameters()


            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)

            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 277, "Issued"))
            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))
            'Pinkal (11-Sep-2020) -- End




            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'StrQ = "SELECT SPACE(5) + ISNULL(lvleavetype_master.leavename, '') AS leavetype " & _
            '          ", ISNULL(formno, '') AS formno " & _
            '          ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
            '          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee " & _
            '          ", ISNULL(jb.job_name, '') AS jobtitle " & _
            '          ", ISNULL(appr.employeecode, '') AS approvercode " & _
            '          ", ISNULL(appr.firstname, '') + ' ' + ISNULL(appr.othername, '') + ' ' + ISNULL(appr.surname, '') AS approver "

            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", '' AS GName "
            'End If

            'StrQ &= ", ISNULL(lvapproverlevel_master.levelname, '') AS level " & _
            '          ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.startdate),'') AS startdate " & _
            '          ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.returndate),'') AS enddate " & _
            '          ", ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid  " & _
            '          "  AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND ISNULL(lvleaveform.returndate,lvleaveform.startdate) AND ISNULL(lvleaveday_fraction.approverunkid,0) <=0),0.00) days " & _
            '          ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.approve_stdate),'') AS app_startdate " & _
            '          ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.approve_eddate),'') AS app_enddate " & _
            '          ", ISNULL(lvleaveform.approve_days, 0.00) AS approve_days "

            'StrQ &= ", CASE WHEN lvpendingleave_tran.statusunkid = 1 THEN @Approve " & _
            '                "WHEN lvpendingleave_tran.statusunkid = 2 THEN @Pending " & _
            '                "WHEN lvpendingleave_tran.statusunkid = 3 THEN @Reject " & _
            '                "WHEN lvpendingleave_tran.statusunkid = 4 THEN @ReSchedule " & _
            '                "WHEN lvpendingleave_tran.statusunkid = 6 THEN @Cancel " & _
            '                "WHEN lvpendingleave_tran.statusunkid = 7 THEN @Issued " & _
            '        " END AS status "


            'StrQ &= " ,lvleaveform.formunkid " & _
            '         ", lvleaveform.employeeunkid " & _
            '         ", lvleaveform.leavetypeunkid " & _
            '         ", ISNULL(lvapproverlevel_master.levelunkid,-1) AS  levelunkid " & _
            '         ", lvleaveform.statusunkid " & _
            '         ", ISNULL(lvapproverlevel_master.priority,-1) AS priority " & _
            '         ", lvpendingleave_tran.statusunkid AS ApproverStatusID " & _
            '         " FROM lvleaveform " & _
            '         " LEFT JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
            '         " LEFT JOIN " & _
            '         " ( " & _
            '         "    SELECT " & _
            '         "         jobunkid " & _
            '         "        ,employeeunkid " & _
            '         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '         "    FROM hremployee_categorization_tran " & _
            '         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(strEmployeeAsOnDate) & "' " & _
            '         " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
            '         " LEFT JOIN hrjob_master jb ON jb.jobunkid = Jobs.jobunkid " & _
            '         " LEFT JOIN lvleavetype_master ON dbo.lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleavetype_master.isactive = 1 " & _
            '         " JOIN lvpendingleave_tran ON lvleaveform.employeeunkid = lvpendingleave_tran.employeeunkid AND lvpendingleave_tran.formunkid = lvleaveform.formunkid AND lvpendingleave_tran.isvoid = 0 " & _
            '         " LEFT JOIN lvleaveapprover_master ON lvpendingleave_tran.approverunkid = lvleaveapprover_master.leaveapproverunkid AND lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid " & _
            '         " AND lvleaveapprover_master.isvoid = 0 " & _
            '         " LEFT JOIN hremployee_master appr ON lvleaveapprover_master.leaveapproverunkid = appr.employeeunkid " & _
            '         "  LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 "


            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If


            'StrQ &= " WHERE  lvleaveform.isvoid = 0 "

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If


            StrInnerQry = "SELECT SPACE(5) + ISNULL(lvleavetype_master.leavename, '') AS leavetype " & _
                      ", ISNULL(formno, '') AS formno " & _
                      ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee " & _
                      ", ISNULL(jb.job_name, '') AS jobtitle " & _
                      ",#APPR_CODE#  AS approvercode " & _
                      ",#APPR_NAME#  AS approver "

            If mintViewIndex > 0 Then
                StrInnerQry &= mstrAnalysis_Fields
            Else
                StrInnerQry &= ", '' AS GName "
            End If
            'S.SANDEEP [15 NOV 2016] -- START
            'StrInnerQry &= ", ISNULL(lvapproverlevel_master.levelname, '') AS level " & _
            '          ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.startdate),'') AS startdate " & _
            '          ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.returndate),'') AS enddate " & _
            '          ", ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid  " & _
            '          "  AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND ISNULL(lvleaveform.returndate,lvleaveform.startdate) AND ISNULL(lvleaveday_fraction.approverunkid,0) <=0),0.00) days " & _
            '          ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.approve_stdate),'') AS app_startdate " & _
            '          ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.approve_eddate),'') AS app_enddate " & _
            '          ", ISNULL(lvleaveform.approve_days, 0.00) AS approve_days " & _
            '         ", CASE WHEN lvpendingleave_tran.statusunkid = 1 THEN @Approve " & _
            '                "WHEN lvpendingleave_tran.statusunkid = 2 THEN @Pending " & _
            '                "WHEN lvpendingleave_tran.statusunkid = 3 THEN @Reject " & _
            '                "WHEN lvpendingleave_tran.statusunkid = 4 THEN @ReSchedule " & _
            '                "WHEN lvpendingleave_tran.statusunkid = 6 THEN @Cancel " & _
            '                "WHEN lvpendingleave_tran.statusunkid = 7 THEN @Issued " & _
            '         " END AS status " & _
            '         " ,lvleaveform.formunkid " & _
            '         ", lvleaveform.employeeunkid " & _
            '         ", lvleaveform.leavetypeunkid " & _
            '         ", ISNULL(lvapproverlevel_master.levelunkid,-1) AS  levelunkid " & _
            '         ", lvleaveform.statusunkid " & _
            '         ", ISNULL(lvapproverlevel_master.priority,-1) AS priority " & _
            '         ", lvpendingleave_tran.statusunkid AS ApproverStatusID " & _
            '         " FROM lvleaveform " & _
            '         " LEFT JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
            '         " LEFT JOIN " & _
            '         " ( " & _
            '         "    SELECT " & _
            '         "         jobunkid " & _
            '         "        ,employeeunkid " & _
            '         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '         "    FROM #DBName#hremployee_categorization_tran " & _
            '         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
            '         " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
            '         " LEFT JOIN hrjob_master jb ON jb.jobunkid = Jobs.jobunkid " & _
            '         " LEFT JOIN lvleavetype_master ON dbo.lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleavetype_master.isactive = 1 " & _
            '         " JOIN lvpendingleave_tran ON lvleaveform.employeeunkid = lvpendingleave_tran.employeeunkid AND lvpendingleave_tran.formunkid = lvleaveform.formunkid AND lvpendingleave_tran.isvoid = 0 " & _
            '         " LEFT JOIN lvleaveapprover_master ON lvpendingleave_tran.approverunkid = lvleaveapprover_master.leaveapproverunkid AND lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid " & _
            '         " AND lvleaveapprover_master.isvoid = 0 " & _
            '         " #EMP_JOIN# " & _
            '         "  LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 "

            StrInnerQry &= ", ISNULL(lvapproverlevel_master.levelname, '') AS level " & _
                      ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.startdate),'') AS startdate " & _
                      ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.returndate),'') AS enddate " & _
                      ", ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid  " & _
                      "  AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND ISNULL(lvleaveform.returndate,lvleaveform.startdate) AND ISNULL(lvleaveday_fraction.approverunkid,0) <=0),0.00) days " & _
                      ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.approve_stdate),'') AS app_startdate " & _
                      ", ISNULL(CONVERT(VARCHAR(max),lvleaveform.approve_eddate),'') AS app_enddate " & _
                      ", ISNULL(lvleaveform.approve_days, 0.00) AS approve_days " & _
                     ", CASE WHEN lvpendingleave_tran.statusunkid = 1 THEN @Approve " & _
                            "WHEN lvpendingleave_tran.statusunkid = 2 THEN @Pending " & _
                            "WHEN lvpendingleave_tran.statusunkid = 3 THEN @Reject " & _
                            "WHEN lvpendingleave_tran.statusunkid = 4 THEN @ReSchedule " & _
                            "WHEN lvpendingleave_tran.statusunkid = 6 THEN @Cancel " & _
                            "WHEN lvpendingleave_tran.statusunkid = 7 THEN @Issued " & _
                     " END AS status " & _
                     " ,lvleaveform.formunkid " & _
                     ", lvleaveform.employeeunkid " & _
                     ", lvleaveform.leavetypeunkid " & _
                     ", ISNULL(lvapproverlevel_master.levelunkid,-1) AS  levelunkid " & _
                     ", lvleaveform.statusunkid " & _
                     ", ISNULL(lvapproverlevel_master.priority,-1) AS priority " & _
                     ", lvpendingleave_tran.statusunkid AS ApproverStatusID " & _
                      ", SUM(1)OVER(PARTITION BY lvleaveform.formunkid,lvpendingleave_tran.statusunkid) AS tc " & _
                      ", SUM(1) OVER (PARTITION BY lvleaveform.formunkid,lvapproverlevel_master.priority) AS tp " & _
                      ", CAST(1 AS INT) AS visibilityid " & _
                     " FROM lvleaveform " & _
                      " JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid "


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrInnerQry &= xAdvanceJoinQry
            End If

            StrInnerQry &= " LEFT JOIN " & _
                     " ( " & _
                     "    SELECT " & _
                     "         jobunkid " & _
                     "        ,employeeunkid " & _
                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                     "    FROM hremployee_categorization_tran " & _
                     "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                     " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                     " LEFT JOIN hrjob_master jb ON jb.jobunkid = Jobs.jobunkid " & _
                     " LEFT JOIN lvleavetype_master ON dbo.lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleavetype_master.isactive = 1 " & _
                     " JOIN lvpendingleave_tran ON lvleaveform.employeeunkid = lvpendingleave_tran.employeeunkid AND lvpendingleave_tran.formunkid = lvleaveform.formunkid AND lvpendingleave_tran.isvoid = 0 " & _
                     " LEFT JOIN lvleaveapprover_master ON lvpendingleave_tran.approverunkid = lvleaveapprover_master.leaveapproverunkid AND lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid " & _
                     " AND lvleaveapprover_master.isvoid = 0 " & _
                     " #EMP_JOIN# " & _
                     "  LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 "

            'Pinkal (11-Sep-2020) -- End

            'S.SANDEEP [15 NOV 2016] -- END


            'Pinkal (23-Nov-2017) -- Start
            'Enhancement - Windsor Golf Hotel - issue # 0001614: Wrong employee designation getting picked on leave reports
            '  "    FROM #DBName#hremployee_categorization_tran " & _
            'Pinkal (23-Nov-2017) -- End

            If mintViewIndex > 0 Then
                StrInnerQry &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrInnerQry &= xUACQry
            End If


            StrConditionQry &= " #WHERE_CONDITION# AND  lvleaveform.isvoid = 0 AND lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isexternalapprover = #ExAppr# " & _
                   " AND lvleaveapprover_master.isactive = 1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrConditionQry &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrConditionQry &= " AND " & xUACFiltrQry
            End If

            Call FilterTitleAndFilterQuery()

            StrConditionQry &= Me._FilterQuery

            StrQ = StrInnerQry
            StrQ &= StrConditionQry

            StrQ = StrQ.Replace("#APPR_CODE#", "ISNULL(appr.employeecode, '') ")
            StrQ = StrQ.Replace("#APPR_NAME#", "ISNULL(appr.firstname, '') + ' ' + ISNULL(appr.othername, '') + ' ' + ISNULL(appr.surname, '') ")
            StrQ = StrQ.Replace("#ExAppr#", "0")
            StrQ = StrQ.Replace("#DBName#", "")
            StrQ = StrQ.Replace("#EMP_JOIN#", "JOIN hremployee_master appr ON lvleaveapprover_master.leaveapproverunkid = appr.employeeunkid ")
            StrQ = StrQ.Replace("#WHERE_CONDITION#", " WHERE 1 = 1 ")

            StrQ &= mstrOrderByQuery
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry
                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPR_CODE#", " '' ")
                    StrQ = StrQ.Replace("#APPR_NAME#", " ISNULL(UEmp.username,'') ")
                    StrQ = StrQ.Replace("#EMP_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvleaveapprover_master.leaveapproverunkid ")

                    StrQ &= " ORDER BY lvleaveform.formno,lvapproverlevel_master.priority "
                Else
                    StrQ = StrQ.Replace("#APPR_CODE#", " CASE WHEN ISNULL(appr.employeecode, '') = '' THEN '' ELSE ISNULL(appr.employeecode, '') END ")
                    StrQ = StrQ.Replace("#APPR_NAME#", " CASE WHEN ISNULL(appr.firstname, '') + ' ' + ISNULL(appr.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(appr.firstname, '') + ' ' + ISNULL(appr.surname, '') END ")
                    StrQ = StrQ.Replace("#EMP_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvleaveapprover_master.leaveapproverunkid " & _
                                                      " JOIN #DBName#hremployee_master appr ON UEmp.employeeunkid = appr.employeeunkid ")

                    StrQ &= mstrOrderByQuery
                End If


                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DBName#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DBName#", "")
                End If

                StrQ = StrQ.Replace("#ExAppr#", "1")
                StrQ = StrQ.Replace("#WHERE_CONDITION#", " WHERE UEmp.companyunkid = " & dr("companyunkid") & " ")

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next

            Dim dView = New DataView(dsList.Tables(0).Copy, "", "formno,priority,ApproverStatusID", DataViewRowState.CurrentRows)
            dsList.Tables(0).Clear()
            dsList.Tables(0).Merge(dView.ToTable)



            'Pinkal (01-Mar-2016) -- End


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dsList.Tables(0).Clone

            'Pinkal (27-Nov-2014) -- Start
            'Enhancement - PUT CHECKBOX IGNORE SAME LEVEL IF APPROVED / REJECT / RESCHEDULE IN LEAVE APPROVED STATUS REPORT FOR TRA.
            '            If mblnIgnoreSameLevelIfApproved Then
            'ReGenerate:
            '                Dim mintPrioriry As Integer = 0
            '                For i As Integer = 0 To mdtTableExcel.Rows.Count - 1
            '                    If mintPrioriry <> CInt(mdtTableExcel.Rows(i)("priority")) Then
            '                        mintPrioriry = CInt(mdtTableExcel.Rows(i)("priority"))
            '                        Dim drRow() As DataRow = dsList.Tables(0).Select("formunkid = " & mdtTableExcel.Rows(i)("formunkid") & " AND ApproverStatusID <> 2 AND  priority  = " & mintPrioriry)
            '                        If drRow.Length > 0 Then
            '                            Dim drPending() As DataRow = mdtTableExcel.Select("formunkid = " & mdtTableExcel.Rows(i)("formunkid") & " AND ApproverStatusID = 2 AND priority  = " & mintPrioriry)
            '                            If drPending.Length > 0 Then
            '                                For Each dRow As DataRow In drPending
            '                                    mdtTableExcel.Rows.Remove(dRow)
            '                                    GoTo ReGenerate
            '                                Next
            '                                mdtTableExcel.AcceptChanges()
            '                            End If
            '                        End If
            '                    End If
            '                Next
            'End If

            'Dim d = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("RCount") <> 1 And x.Field(Of Integer)("statusunkid") <> 3).Where(Function(x) x.Field(Of Integer)("statusunkid") <> 4)
            'If d.Count > 0 Then
            '    mdtTableExcel = d.CopyToDataTable()
            'End If

            'Dim dr1 = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("RCount") = 1 And x.Field(Of Int64)("rno") = 1)
            'If dr1.Count > 0 Then
            '    mdtTableExcel.Merge(dr1.CopyToDataTable())
            'End If
            'mdtTableExcel = New DataView(mdtTableExcel, "", "formno,priority", DataViewRowState.CurrentRows).ToTable

            'tc

            Dim dt As DataTable = mdtTableExcel.Clone()
            Dim strValue As String = String.Empty
            Dim drTemp() As DataRow = Nothing
            drTemp = dsList.Tables(0).Select("statusunkid <> 2 AND (ApproverStatusID <> 2 OR tp = 1)")
            If drTemp.Length > 0 Then
                mdtTableExcel = drTemp.CopyToDataTable()
            End If
            strValue = String.Join(",", mdtTableExcel.AsEnumerable().Select(Function(x) x.Field(Of Integer)("formunkid").ToString).Distinct().ToArray)
            If strValue.Trim.Length > 0 Then
                drTemp = dsList.Tables(0).Select("formunkid NOT IN (" & strValue & ")")
                If drTemp.Length > 0 Then
                    dt = drTemp.CopyToDataTable()
                End If
            Else
                dt = dsList.Tables(0).Copy
            End If
            strValue = String.Join(",", dt.AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("formunkid")) _
                                       .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("ApproverStatusID")) _
                                       .Distinct().Count()}).Where(Function(y) y.barCount = 1).Select(Function(a) a.barid.ToString).ToArray())
            If strValue.Trim.Length > 0 Then
                mdtTableExcel.Merge(dsList.Tables(0).Select("formunkid IN(" & strValue & ")").CopyToDataTable(), True)
            End If
            strValue = String.Join(",", dt.AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("formunkid")) _
                                       .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("ApproverStatusID")) _
                                       .Distinct().Count()}).Where(Function(y) y.barCount > 1).Select(Function(a) a.barid.ToString).ToArray())
            If strValue.Trim.Length > 0 Then
                dt = dsList.Tables(0).Select("formunkid IN(" & strValue & ")").CopyToDataTable()
            End If

            If dt.Rows.Count > 0 Then
                Dim r = dt.AsEnumerable().GroupBy(Function(v) New With {Key .no = v.Field(Of Integer)("formunkid"), Key .value = v.Field(Of Integer)("priority"), Key .st = v.Field(Of Integer)("ApproverStatusID")})
                Dim x = r.SelectMany(Function(g) g)
                dt = x.CopyToDataTable()

ReGenerate:     Dim mintPrioriry As Integer = 0
                For i As Integer = 0 To dt.Rows.Count - 1
                    If mintPrioriry <> CInt(dt.Rows(i)("priority")) Then
                        mintPrioriry = CInt(dt.Rows(i)("priority"))
                        Dim drRow() As DataRow = dt.Select("formunkid = " & dt.Rows(i)("formunkid") & " AND ApproverStatusID <> 2 AND  priority  = " & mintPrioriry)
                        If drRow.Length > 0 Then
                            Dim drPending() As DataRow = dt.Select("formunkid = " & dt.Rows(i)("formunkid") & " AND ApproverStatusID = 2 AND priority  = " & mintPrioriry)
                            If drPending.Length > 0 Then
                                For Each dRow As DataRow In drPending
                                    dt.Rows.Remove(dRow)
                                    GoTo ReGenerate
                                Next
                                dt.AcceptChanges()
                            End If
                        End If
                    End If
                Next
            End If
            mdtTableExcel.Merge(dt, True)

            'strValue = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("formunkid")) _
            '                           .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("tc")) _
            '                           .Distinct().Count()}).Where(Function(y) y.barCount > 1).Select(Function(a) a.barid.ToString).ToArray())

            'dt = dsList.Tables(0).Select("formunkid IN(" & strValue & ")").CopyToDataTable()
            'If dt.Rows.Count > 0 Then
            '    'strValue = String.Join(",", dt.AsEnumerable().GroupBy(Function(v) New With {Key .no = v.Field(Of Integer)("formunkid"), Key .value = v.Field(Of Integer)("priority"), Key .st = v.Field(Of Integer)("ApproverStatusID"), Key .rn = v.Field(Of Int64)("rno")}).Where(Function(z) z.Key.st <> 2).Select(Function(x) x.Key.rn.ToString).ToArray)
            '    'mdtTableExcel = dsList.Tables(0).Select("rno IN(" & strValue & ")").CopyToDataTable()

            '    Dim r = dt.AsEnumerable().GroupBy(Function(v) New With {Key .no = v.Field(Of Integer)("formunkid"), Key .value = v.Field(Of Integer)("priority"), Key .st = v.Field(Of Integer)("ApproverStatusID")})
            '    Dim x = r.SelectMany(Function(g) g)
            '    mdtTableExcel = x.CopyToDataTable()
            '    mdtTableExcel = New DataView(mdtTableExcel, "(ApproverStatusID <> 2 OR tp = 1)", "priority", DataViewRowState.CurrentRows).ToTable

            '    'Dim r = From a In dt Group Join b In dt On a.Field(Of Integer)("formunkid") Equals b.Field(Of Integer)("formunkid") And a.Field(Of Integer)("priority") Equals b.Field(Of Integer)("priority") Into grp = Group _
            '    '         Select grp.Distinct.First().Field(Of Int64)("rno") 'Select a.Field(Of Int64)("rno")
            '    'Dim str As String() = r.Select(Function(x) x.ToString).Distinct.ToArray
            '    'strValue = String.Join(",", str)
            '    'dt = dsList.Tables(0).Select("rno IN(" & strValue & ")").CopyToDataTable()
            '    'Dim dtr = dt.AsEnumerable().Where(Function(x) x.Field(Of Integer)("ApproverStatusID") <> 2)
            '    'mdtTableExcel = dtr.CopyToDataTable
            '    'dt = dsList.Tables(0).Select("rno NOT IN(" & strValue & ")").CopyToDataTable()
            '    'mdtTableExcel.Merge(dt, True)
            'End If

            'strValue = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("formunkid")) _
            '                            .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("tc")) _
            '                            .Distinct().Count()}).Where(Function(y) y.barCount = 1).Select(Function(a) a.barid.ToString).ToArray())

            'If strValue.Trim.Length > 0 Then
            '    dt = dsList.Tables(0).Select("formunkid IN(" & strValue & ")").CopyToDataTable()
            '    mdtTableExcel.Merge(dt, True)
            'End If

            'Pinkal (26-Jun-2020) -- Start
            'Problem in Report- Solving Report Grouping problem in Leave Form Status Report.
            If mintViewIndex > 0 Then
                mdtTableExcel = New DataView(mdtTableExcel, "", "GName,formno,priority", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTableExcel = New DataView(mdtTableExcel, "", "formno,priority", DataViewRowState.CurrentRows).ToTable
            End If
            'Pinkal (26-Jun-2020) -- End


            If mdtTableExcel.Columns.Contains("tp") Then
                mdtTableExcel.Columns.Remove("tp")
            End If

            If mdtTableExcel.Columns.Contains("tc") Then
                mdtTableExcel.Columns.Remove("tc")
            End If

            If mdtTableExcel.Columns.Contains("visibilityid") Then
                mdtTableExcel.Columns.Remove("visibilityid")
            End If

            If mdtTableExcel.Columns.Contains("formunkid") Then
                mdtTableExcel.Columns.Remove("formunkid")
            End If

            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            If mdtTableExcel.Columns.Contains("leavetypeunkid") Then
                mdtTableExcel.Columns.Remove("leavetypeunkid")
            End If

            If mdtTableExcel.Columns.Contains("levelunkid") Then
                mdtTableExcel.Columns.Remove("levelunkid")
            End If

            If mdtTableExcel.Columns.Contains("statusunkid") Then
                mdtTableExcel.Columns.Remove("statusunkid")
            End If


            'Pinkal (27-Nov-2014) -- Start
            'Enhancement - PUT CHECKBOX IGNORE SAME LEVEL IF APPROVED / REJECT / RESCHEDULE IN LEAVE APPROVED STATUS REPORT FOR TRA
            If mdtTableExcel.Columns.Contains("priority") Then
                mdtTableExcel.Columns.Remove("priority")
            End If

            If mdtTableExcel.Columns.Contains("ApproverStatusID") Then
                mdtTableExcel.Columns.Remove("ApproverStatusID")
            End If
            'Pinkal (27-Nov-2014) -- End


            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName", "formno"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
                Dim strGrpCols As String() = {"formno"}
                strarrGroupColumns = strGrpCols
            End If


            'Pinkal (23-Nov-2017) -- Start
            'Enhancement - Windsor Golf Hotel - issue # 0001614: Wrong employee designation getting picked on leave reports
            Dim columnNames As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, columnNames)
            'Pinkal (23-Nov-2017) -- End

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing
                'Pinkal (24-Aug-2015) -- End

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            '--------------------



            For i As Integer = 0 To mdtTableExcel.Rows.Count - 1
                If mdtTableExcel.Rows(i)("startdate").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("startdate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("startdate")), DateFormat.ShortDate)
                End If
                If mdtTableExcel.Rows(i)("enddate").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("enddate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("enddate")), DateFormat.ShortDate)
                End If
                If mdtTableExcel.Rows(i)("app_startdate").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("app_startdate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("app_startdate")), DateFormat.ShortDate)
                End If
                If mdtTableExcel.Rows(i)("app_enddate").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("app_enddate") = FormatDateTime(CDate(mdtTableExcel.Rows(i)("app_enddate")), DateFormat.ShortDate)
                End If
            Next


            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            mdtTableExcel.Columns("formno").Caption = Language.getMessage(mstrModuleName, 1, "Form No")
            mdtTableExcel.Columns("leavetype").Caption = Language.getMessage(mstrModuleName, 2, "Leave Type")
            mdtTableExcel.Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 3, "Employee Code")
            mdtTableExcel.Columns("employee").Caption = Language.getMessage(mstrModuleName, 4, "Employee")
            mdtTableExcel.Columns("jobtitle").Caption = Language.getMessage(mstrModuleName, 5, "Job Title")
            mdtTableExcel.Columns("approvercode").Caption = Language.getMessage(mstrModuleName, 6, "Approver Code")
            mdtTableExcel.Columns("approver").Caption = Language.getMessage(mstrModuleName, 7, "Approver")
            mdtTableExcel.Columns("level").Caption = Language.getMessage(mstrModuleName, 8, "Level")
            mdtTableExcel.Columns("startdate").Caption = Language.getMessage(mstrModuleName, 9, "Start Date")
            mdtTableExcel.Columns("enddate").Caption = Language.getMessage(mstrModuleName, 10, "End Date")
            mdtTableExcel.Columns("days").Caption = Language.getMessage(mstrModuleName, 27, "Applied Days")
            mdtTableExcel.Columns("app_startdate").Caption = Language.getMessage(mstrModuleName, 11, "Approved Start Date")
            mdtTableExcel.Columns("app_enddate").Caption = Language.getMessage(mstrModuleName, 12, "Approved End Date")
            mdtTableExcel.Columns("approve_days").Caption = Language.getMessage(mstrModuleName, 13, "Approved Days")
            mdtTableExcel.Columns("status").Caption = Language.getMessage(mstrModuleName, 26, "Status")

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Form No")
            Language.setMessage(mstrModuleName, 2, "Leave Type")
            Language.setMessage(mstrModuleName, 3, "Employee Code")
            Language.setMessage(mstrModuleName, 4, "Employee")
            Language.setMessage(mstrModuleName, 5, "Job Title")
            Language.setMessage(mstrModuleName, 6, "Approver Code")
            Language.setMessage(mstrModuleName, 7, "Approver")
            Language.setMessage(mstrModuleName, 8, "Level")
            Language.setMessage(mstrModuleName, 9, "Start Date")
            Language.setMessage(mstrModuleName, 10, "End Date")
            Language.setMessage(mstrModuleName, 11, "Approved Start Date")
            Language.setMessage(mstrModuleName, 12, "Approved End Date")
            Language.setMessage(mstrModuleName, 13, "Approved Days")
            Language.setMessage(mstrModuleName, 14, "From Date:")
            Language.setMessage(mstrModuleName, 15, "To Date:")
            Language.setMessage(mstrModuleName, 16, "Employee:")
            Language.setMessage(mstrModuleName, 17, "Leave Type:")
            Language.setMessage(mstrModuleName, 18, "Level:")
            Language.setMessage(mstrModuleName, 19, " Status:")
            Language.setMessage(mstrModuleName, 20, " Leave Form:")
            Language.setMessage(mstrModuleName, 21, " Order By :")
            Language.setMessage(mstrModuleName, 22, "Prepared By :")
            Language.setMessage(mstrModuleName, 23, "Checked By :")
            Language.setMessage(mstrModuleName, 24, "Approved By")
            Language.setMessage(mstrModuleName, 25, "Received By :")
            Language.setMessage(mstrModuleName, 26, "Status")
            Language.setMessage(mstrModuleName, 27, "Applied Days")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 113, "Re-Scheduled")
            Language.setMessage("clsMasterData", 115, "Cancelled")
            Language.setMessage("clsMasterData", 277, "Issued")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
