#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
#End Region


Public Class frmEmployeeLeaveForm

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmEmployeeLeaveForm"
    Private objLeaveForm As clsEmployeeLeaveForm
#End Region

#Region "Constructor"
    Public Sub New()
        objLeaveForm = New clsEmployeeLeaveForm(User._Object._Languageunkid,Company._Object._Companyunkid)
        objLeaveForm.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As DataSet

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsList = objEmployee.GetEmployeeList("Employee", True, False)
            'Else
            '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END



            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsList.Tables(0)

            Dim objLeaveType As New clsleavetype_master
            dsList = Nothing
            dsList = objLeaveType.getListForCombo("List", True)
            cboLeaveType.ValueMember = "leavetypeunkid"
            cboLeaveType.DisplayMember = "name"
            cboLeaveType.DataSource = dsList.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedIndex = 0
            cboLeaveType.SelectedIndex = 0

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 
            cboLeaveForm.SelectedIndex = 0
            'Pinkal (06-Mar-2014) -- End


            'Pinkal (19-Jun-2014) -- Start
            'Enhancement : TRA Changes Leave Enhancement
            chkIncludeDependandList.Checked = False
            'Pinkal (19-Jun-2014) -- End


            'Pinkal (02-May-2017) -- Start
            'Enhancement - Working on Leave Enhancement for HJF.
            chkDonotShowExpense.Checked = True
            'Pinkal (02-May-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objLeaveForm.SetDefaultValue()

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 
            'objLeaveForm._LeaveFormNo = txtLeaveForm.Text.Trim
            objLeaveForm._LeaveFormId = CInt(cboLeaveForm.SelectedValue)
            'Pinkal (06-Mar-2014) -- End

            objLeaveForm._EmployeeId = cboEmployee.SelectedValue
            objLeaveForm._EmployeeName = cboEmployee.Text
            objLeaveForm._LeaveTypeId = cboLeaveType.SelectedValue
            objLeaveForm._LeaveTypeName = cboLeaveType.Text


            'Pinkal (24-May-2014) -- Start
            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
            objLeaveForm._YearId = FinancialYear._Object._YearUnkid
            objLeaveForm._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objLeaveForm._Fin_StartDate = FinancialYear._Object._Database_Start_Date.Date
            objLeaveForm._Fin_Enddate = FinancialYear._Object._Database_End_Date.Date
            'Pinkal (24-May-2014) -- End


            'Pinkal (19-Jun-2014) -- Start
            'Enhancement : TRA Changes Leave Enhancement
            objLeaveForm._IncludeDependents = chkIncludeDependandList.Checked
            'Pinkal (19-Jun-2014) -- End


            'Pinkal (25-Jul-2015) -- Start
            'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 
            objLeaveForm._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
            objLeaveForm._DBEnddate = FinancialYear._Object._Database_End_Date.Date
            'Pinkal (25-Jul-2015) -- End


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            objLeaveForm._LeaveAccrueTenureSetting = ConfigParameter._Object._LeaveAccrueTenureSetting
            objLeaveForm._LeaveAccrueDaysAfterEachMonth = ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth
            'Pinkal (18-Nov-2016) -- End



            'Pinkal (02-May-2017) -- Start
            'Enhancement - Working on Leave Enhancement for HJF.
            objLeaveForm._DoNotShowExpesneIfNoExpense = chkDonotShowExpense.Checked
            'Pinkal (02-May-2017) -- End


            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            objLeaveForm._PaymentApprovalwithLeaveApproval = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
            'Pinkal (13-Jun-2019) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes  Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 

    Public Function Validation() As Boolean
        Dim mblnFlag As Boolean = False
        Try
            If CInt(cboLeaveType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboLeaveType.Focus()
                Return False

            ElseIf CInt(cboLeaveForm.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Leave Form is compulsory information.Please Select Leave Form."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboLeaveForm.Focus()
                Return False

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (06-Mar-2014) -- End

#End Region

#Region "Form's Events"

    Private Sub frmEmployeeLeaveForm_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objLeaveForm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveForm_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objLeaveForm._ReportName
            Me._Message = objLeaveForm._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveForm_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveForm_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmployeeLeaveForm_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveForm_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveForm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveForm_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub frmEmployeeLeaveForm_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try


            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes  Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 

            'If txtLeaveForm.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Form No cannot be blank. Leave Form No is required information."), enMsgBoxStyle.Information)
            '    txtLeaveForm.Select()
            '    Exit Sub
            'End If

            If Validation() = False Then Exit Sub

            'Pinkal (06-Mar-2014) -- End

            If SetFilter() = False Then Exit Sub


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objLeaveForm.generateReport(0, e.Type, enExportAction.None)
            objLeaveForm.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                              , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                              , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                              , True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, 0)
            'Pinkal (24-Aug-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveForm_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveForm_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes  Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 

            If Validation() = False Then Exit Sub

            'Pinkal (06-Mar-2014) -- End

            If SetFilter() = False Then Exit Sub


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objLeaveForm.generateReport(0, enPrintAction.None, e.Type)
            objLeaveForm.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                           , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                           , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                           , True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, 0)
            'Pinkal (24-Aug-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveForm_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveForm_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveForm_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveForm_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveForm_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeaveType.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboLeaveType.DataSource
            frm.ValueMember = cboLeaveType.ValueMember
            frm.DisplayMember = cboLeaveType.DisplayMember
            If frm.DisplayDialog Then
                cboLeaveType.SelectedValue = frm.SelectedValue
                cboLeaveType.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmEmployeeLeaveForm_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeLeaveForm.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeLeaveForm"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEmployeeLeaveForm_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 

    Private Sub objbtnSearchLeaveForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeaveForm.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboLeaveForm.DataSource
            frm.ValueMember = cboLeaveForm.ValueMember
            frm.DisplayMember = cboLeaveForm.DisplayMember
            If frm.DisplayDialog Then
                cboLeaveForm.SelectedValue = frm.SelectedValue
                cboLeaveForm.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeaveForm_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (06-Mar-2014) -- End


#End Region


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 

#Region "Combobox Event"

    Private Sub cboLeaveType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            Dim objForm As New clsleaveform
            Dim dsList As DataSet = Nothing

            'Pinkal (24-May-2014) -- Start
            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
            'dsList = objForm.getListForCombo(CInt(cboLeaveType.SelectedValue), "", CInt(cboEmployee.SelectedValue), True, True)
            dsList = objForm.getListForCurrentYearLeaveForm(FinancialYear._Object._YearUnkid, CInt(cboLeaveType.SelectedValue), "", CInt(cboEmployee.SelectedValue), True, True)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "yearunkid <=0 or yearunkid = " & FinancialYear._Object._YearUnkid, "", DataViewRowState.CurrentRows).ToTable
            'Pinkal (24-May-2014) -- End
            cboLeaveForm.ValueMember = "formunkid"
            cboLeaveForm.DisplayMember = "name"
            cboLeaveForm.DataSource = dtTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLeaveType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (06-Mar-2014) -- End




    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblLeaveForm.Text = Language._Object.getCaption(Me.lblLeaveForm.Name, Me.lblLeaveForm.Text)
            Me.chkIncludeDependandList.Text = Language._Object.getCaption(Me.chkIncludeDependandList.Name, Me.chkIncludeDependandList.Text)
            Me.chkDonotShowExpense.Text = Language._Object.getCaption(Me.chkDonotShowExpense.Name, Me.chkDonotShowExpense.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type.")
			Language.setMessage(mstrModuleName, 3, "Leave Form is compulsory information.Please Select Leave Form.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
