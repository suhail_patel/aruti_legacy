
Imports eZeeCommonLib
Imports Aruti.Data
Imports System


Public Class clsEmployeeLeaveForm
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeLeaveForm"
    Private mstrReportId As String = enArutiReport.EmployeeLeaveForm
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintLeaveFormID As Integer = 0
    Private mstrLeaveFormNo As String = ""
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLeaveTypeId As Integer = 0
    Private mstrLeaveTypeName As String = ""
    Private mstrOrderByQuery As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mintLeaveBalanceSetting As Integer = 0
    Private mintYearId As Integer = 0
    Private mdtFinStartDate As DateTime = Nothing
    Private mdtFinEndDate As DateTime = Nothing
    Private mblnIncludedependents As Boolean = False
    Dim mdtEndDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
    Private mblnShowExpense As Boolean = True
    Private mdtdbStartDate As DateTime = Nothing
    Private mdtdbEndDate As DateTime = Nothing

    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
    Private mintLeaveAccrueTenureSetting As Integer = enLeaveAccrueTenureSetting.Yearly
    Private mintLeaveAccrueDaysAfterEachMonth As Integer = 0
    'Pinkal (18-Nov-2016) -- End


    'Pinkal (02-May-2017) -- Start
    'Enhancement - Working on Leave Enhancement for HJF.
    Private mblnDonotShowExpenseifNoExepense As Boolean = True
    'Pinkal (02-May-2017) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveFormId() As Integer
        Set(ByVal value As Integer)
            mintLeaveFormID = value
        End Set
    End Property

    Public WriteOnly Property _LeaveFormNo() As String
        Set(ByVal value As String)
            mstrLeaveFormNo = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeId() As Integer
        Set(ByVal value As Integer)
            mintLeaveTypeId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeName() As String
        Set(ByVal value As String)
            mstrLeaveTypeName = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    Public WriteOnly Property _Fin_StartDate() As Date
        Set(ByVal value As Date)
            mdtFinStartDate = value
        End Set
    End Property

    Public WriteOnly Property _Fin_Enddate() As Date
        Set(ByVal value As Date)
            mdtFinEndDate = value
        End Set
    End Property

    Public WriteOnly Property _IncludeDependents() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludedependents = value
        End Set
    End Property

    Public WriteOnly Property _PaymentApprovalwithLeaveApproval() As Boolean
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

    Public WriteOnly Property _ShowLeaveExpense() As Boolean
        Set(ByVal value As Boolean)
            mblnShowExpense = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DBStartdate
    ''' Modify By: Pinkal 
    ''' </summary>
    ''' 
    Public Property _DBStartdate() As DateTime
        Get
            Return mdtdbStartDate
        End Get
        Set(ByVal value As DateTime)
            mdtdbStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DBEnddate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DBEnddate() As DateTime
        Get
            Return mdtdbEndDate
        End Get
        Set(ByVal value As DateTime)
            mdtdbEndDate = value
        End Set
    End Property

    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

    Public WriteOnly Property _LeaveAccrueTenureSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueTenureSetting = value
        End Set
    End Property

    Public WriteOnly Property _LeaveAccrueDaysAfterEachMonth() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueDaysAfterEachMonth = value
        End Set
    End Property

    'Pinkal (18-Nov-2016) -- End


    'Pinkal (02-May-2017) -- Start
    'Enhancement - Working on Leave Enhancement for HJF.

    Public WriteOnly Property _DoNotShowExpesneIfNoExpense() As Boolean
        Set(ByVal value As Boolean)
            mblnDonotShowExpenseifNoExepense = value
        End Set
    End Property

    'Pinkal (02-May-2017) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            mintLeaveFormID = 0
            mstrLeaveFormNo = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintLeaveTypeId = 0
            mstrLeaveTypeName = ""
            mstrOrderByQuery = ""
            mintYearId = 0
            mintLeaveBalanceSetting = 0
            mdtFinEndDate = Nothing
            mdtFinEndDate = Nothing
            mblnIncludedependents = False

            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            mblnShowExpense = True
            'Pinkal (13-Jul-2015) -- End


            'Pinkal (02-May-2017) -- Start
            'Enhancement - Working on Leave Enhancement for HJF.
            mblnDonotShowExpenseifNoExepense = True
            'Pinkal (02-May-2017) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND lvleaveform.employeeunkid  = @EmployeeId "
            End If

            If mintLeaveTypeId > 0 Then
                objDataOperation.AddParameter("@LeaveTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveTypeId)
                Me._FilterQuery &= " AND  lvleaveform.leavetypeunkid = @LeaveTypeId "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        Try

            '    'Pinkal (01-Feb-2014) -- Start
            '    'Enhancement : TRA Changes

            '    If mintCompanyUnkid <= 0 Then
            '        mintCompanyUnkid = Company._Object._Companyunkid
            '    End If

            '    Company._Object._Companyunkid = mintCompanyUnkid
            '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            '    If mintUserUnkid <= 0 Then
            '        mintUserUnkid = User._Object._Userunkid
            '    End If

            '    User._Object._Userunkid = mintUserUnkid

            '    'Pinkal (01-Feb-2014) -- End


            '    objRpt = Generate_DetailReport()

            '    'Pinkal (01-Feb-2014) -- Start
            '    'Enhancement : TRA Changes
            '    Rpt = objRpt
            '    'Pinkal (01-Feb-2014) -- End

            '    If Not IsNothing(objRpt) Then
            '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            '    End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                               , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date _
                                                               , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String _
                                                               , ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer _
                                                               , Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                               , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid

            'Pinkal (07-Jul-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.
            Dim mintCountryId As Integer = Company._Object._Countryunkid
            'Pinkal (07-Jul-2016) -- End

            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            'Pinkal (07-Jul-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.
            If mintCountryId <> 190 Then
                objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, True, xUserModeSetting)
            ElseIf mintCountryId = 190 Then 'Sieraa Leone
                objRpt = Generate_SLLeaveFormDetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, True, xUserModeSetting)
            End If
            'Pinkal (07-Jul-2016) -- End

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '        Dim StrQ As String = ""
    '        Dim dsList As New DataSet
    '        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '        Dim rpt_LeaveApprovers As ArutiReport.Designer.dsArutiReport
    '        Dim rpt_LeaveExpense As ArutiReport.Designer.dsArutiReport

    '        'Pinkal (19-Jun-2014) -- Start
    '        'Enhancement : TRA Changes Leave Enhancement
    '        Dim rpt_Dependents As ArutiReport.Designer.dsArutiReport
    '        'Pinkal (19-Jun-2014) -- End


    '        Try
    '            objDataOperation = New clsDataOperation


    '            'Pinkal (15-Jul-2013) -- Start
    '            'Enhancement : TRA Changes


    '            'StrQ = " SELECT  lvleaveform.employeeunkid " & _
    '            '           ",lvleaveform.formunkid " & _
    '            '           ",lvleaveform.leavetypeunkid " & _
    '            '           ",employeecode " & _
    '            '           ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employeename " & _
    '            '           ",hrjob_master.job_name AS jobtitle " & _
    '            '           ",hrgrade_master.name AS grade " & _
    '            '           ",hrdepartment_master.name AS department " & _
    '            '           " ,hremployee_master.appointeddate " & _
    '            '           ",hremployee_master.confirmation_date " & _
    '            '           ",lvleaveform.formno " & _
    '            '           ",lvleavetype_master.leavename " & _
    '            '           ",lvleaveform.applydate " & _
    '            '           ",lvleaveform.startdate " & _
    '            '           ",lvleaveform.returndate " & _
    '            '           ",lvleaveform.addressonleave " & _
    '            '           ",lvleaveform.remark " & _
    '            '           ",ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND lvleaveform.returndate),0.00) days " & _
    '            '           " FROM lvleaveform " & _
    '            '           " JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
    '            '           " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '            '           " JOIN hrgrade_master ON hremployee_master.gradeunkid = hrgrade_master.gradeunkid " & _
    '            '           " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '            '           " JOIN lvleavetype_master ON lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
    '            '           " WHERE lvleaveform.isvoid = 0 AND lvleaveform.formno = '" & mstrLeaveFormNo & "'"


    'StrQ = " SELECT  lvleaveform.employeeunkid " & _
    '           ",lvleaveform.formunkid " & _
    '           ",lvleaveform.leavetypeunkid " & _
    '           ",employeecode " & _
    '           ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employeename " & _
    '           ",hrjob_master.job_name AS jobtitle " & _
    '           ",hrgrade_master.name AS grade " & _
    '           ",hrdepartment_master.name AS department " & _
    '           " ,hremployee_master.appointeddate " & _
    '           ",hremployee_master.confirmation_date " & _
    '           ",lvleaveform.formno " & _
    '           ",lvleavetype_master.leavename " & _
    '           ",lvleaveform.applydate " & _
    '           ",lvleaveform.startdate " & _
    '           ",lvleaveform.returndate " & _
    '                      ", lvleaveform.approve_stdate " & _
    '                      ", lvleaveform.approve_eddate " & _
    '           ",lvleaveform.addressonleave " & _
    '           ",lvleaveform.remark " & _
    '                      ",ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND lvleaveform.returndate AND lvleaveday_fraction.approverunkid <=0),0.00) days " & _
    '                      ", ISNULL(lvleaveform.approve_days,0) AS 	approve_days	  " & _
    '           " FROM lvleaveform " & _
    '           " JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
    '           " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '           " JOIN hrgrade_master ON hremployee_master.gradeunkid = hrgrade_master.gradeunkid " & _
    '           " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '           " JOIN lvleavetype_master ON lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
    '                       " WHERE lvleaveform.isvoid = 0 "


    '            'Pinkal (06-Mar-2014) -- Start
    '            'Enhancement : TRA Changes  Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 

    '            'AND lvleaveform.formno = '" & mstrLeaveFormNo & "'"
    '            StrQ &= " AND lvleaveform.formunkid = " & mintLeaveFormID

    '            'Pinkal (06-Mar-2014) -- End

    '            'Pinkal (15-Jul-2013) -- End


    '            Call FilterTitleAndFilterQuery()

    '            StrQ &= Me._FilterQuery

    '            StrQ &= mstrOrderByQuery

    '            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '            End If

    '            rpt_Data = New ArutiReport.Designer.dsArutiReport
    '            rpt_LeaveApprovers = New ArutiReport.Designer.dsArutiReport
    '            rpt_LeaveExpense = New ArutiReport.Designer.dsArutiReport


    '            'Pinkal (19-Jun-2014) -- Start
    '            'Enhancement : TRA Changes Leave Enhancement
    '            rpt_Dependents = New ArutiReport.Designer.dsArutiReport
    '            'Pinkal (19-Jun-2014) -- End


    '            Dim objLeaveBal As New clsleavebalance_tran
    '            Dim objLeaveForm As New clsleaveform
    '            Dim objMaster As New clsMasterData

    '            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '                Dim rpt_Rows As DataRow
    '                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

    '                rpt_Rows.Item("Column1") = dtRow.Item("formno")
    '                rpt_Rows.Item("Column2") = dtRow.Item("employeename")
    '                rpt_Rows.Item("Column3") = dtRow.Item("employeecode")
    '                rpt_Rows.Item("Column4") = dtRow.Item("jobtitle")
    '                rpt_Rows.Item("Column5") = dtRow.Item("grade")
    '                rpt_Rows.Item("Column6") = dtRow.Item("department")

    '                If Not IsDBNull(dtRow.Item("appointeddate")) Then
    '                    rpt_Rows.Item("Column7") = CDate(dtRow.Item("appointeddate")).ToShortDateString
    '                Else
    '                    rpt_Rows.Item("Column7") = ""
    '                End If

    '                If Not IsDBNull(dtRow.Item("confirmation_date")) Then
    '                    rpt_Rows.Item("Column8") = CDate(dtRow.Item("confirmation_date")).ToShortDateString
    '                Else
    '                    rpt_Rows.Item("Column8") = ""
    '                End If

    '                rpt_Rows.Item("Column9") = dtRow.Item("leavename")

    '                Dim dList As DataSet = objLeaveForm.GetEmployeeLastIssuedLeave(CInt(dtRow.Item("formunkid")), CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("leavetypeunkid")))

    '                If dList IsNot Nothing And dList.Tables(0).Rows.Count > 0 Then
    '                    rpt_Rows.Item("Column14") = CDate(dList.Tables(0).Rows(0)("startdate")).ToShortDateString & Language.getMessage(mstrModuleName, 40, " To ") & CDate(dList.Tables(0).Rows(0)("enddate")).ToShortDateString
    '                End If


    '                'Pinkal (31-Jan-2014) -- Start
    '                'Enhancement : Oman Changes
    '                If mintLeaveTypeId <= 0 Then mintLeaveTypeId = CInt(dtRow.Item("leavetypeunkid"))
    '                'Pinkal (31-Jan-2014) -- End


    '                'Pinkal (01-Feb-2014) -- Start
    '                'Enhancement : TRA Changes
    '                If mintEmployeeId <= 0 Then mintEmployeeId = CInt(dtRow.Item("employeeunkid"))
    '                'Pinkal (01-Feb-2014) -- End



    '                'Pinkal (04-Mar-2013) -- Start
    '                'Enhancement : TRA Changes
    '                Dim dsBalance As DataSet = Nothing


    '                'Pinkal (24-May-2014) -- Start
    '                'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]

    '                'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                '    dsBalance = objLeaveBal.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")))
    '                'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                '    dsBalance = objLeaveBal.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")), Nothing, Nothing, True, True)
    '                'End If

    '                If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                    dsBalance = objLeaveBal.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")), mdtFinStartDate.Date, mdtFinEndDate.Date, , , mintLeaveBalanceSetting, , mintYearId)
    '                ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    'Pinkal (25-Jul-2015) -- Start
    '                    'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 
    '                    objLeaveBal._DBStartdate = mdtdbStartDate
    '                    objLeaveBal._DBEnddate = mdtdbEndDate
    '                    'Pinkal (25-Jul-2015) -- End
    '                    dsBalance = objLeaveBal.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")), Nothing, Nothing, True, True, mintLeaveBalanceSetting, , mintYearId)
    '                End If

    '                'Pinkal (24-May-2014) -- End

    '                'Pinkal (04-Mar-2013) -- End

    '                If dsBalance IsNot Nothing And dsBalance.Tables(0).Rows.Count > 0 Then
    '                    rpt_Rows.Item("Column10") = CDec(dsBalance.Tables(0).Rows(0)("LstyearAccrue_amount")).ToString("#0.00")
    '                    rpt_Rows.Item("Column11") = CDec(dsBalance.Tables(0).Rows(0)("Accrue_amount")).ToString("#0.00")
    '                    rpt_Rows.Item("Column12") = CDec(dsBalance.Tables(0).Rows(0)("LstyearIssue_amount")).ToString("#0.00")
    '                    rpt_Rows.Item("Column13") = CDec(dsBalance.Tables(0).Rows(0)("Issue_amount")).ToString("#0.00")
    '                    rpt_Rows.Item("Column15") = CDec(dsBalance.Tables(0).Rows(0)("Balance")).ToString("#0.00")

    '                    'Pinkal (30-Apr-2013) -- Start
    '                    'Enhancement : TRA Changes
    '                    rpt_Rows.Item("Column25") = CDec(dsBalance.Tables(0).Rows(0)("LeaveBF")).ToString("#0.00")
    '                    'Pinkal (30-Apr-2013) -- End


    '                End If

    '                If Not IsDBNull(dtRow.Item("applydate")) Then
    '                    rpt_Rows.Item("Column16") = CDate(dtRow.Item("applydate")).ToShortDateString
    '                End If

    '                If Not IsDBNull(dtRow.Item("startdate")) Then
    '                    rpt_Rows.Item("Column17") = CDate(dtRow.Item("startdate")).ToShortDateString
    '                End If

    '                If Not IsDBNull(dtRow.Item("returndate")) Then
    '                    rpt_Rows.Item("Column18") = CDate(dtRow.Item("returndate")).ToShortDateString

    '                    'Pinkal (21-Jul-2014) -- Start
    '                    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    '                    mdtEndDate = CDate(dtRow.Item("returndate")).ToShortDateString
    '                    'Pinkal (21-Jul-2014) -- End

    '                End If

    '                rpt_Rows.Item("Column19") = dtRow.Item("addressonleave")
    '                rpt_Rows.Item("Column20") = dtRow.Item("remark")


    '                'Pinkal (28-MAY-2012) -- Start
    '                'Enhancement : TRA Changes

    '                Dim dsScale As DataSet = objMaster.Get_Current_Scale("List", CInt(dtRow.Item("employeeunkid")), ConfigParameter._Object._CurrentDateAndTime.Date)

    '                If dsScale IsNot Nothing AndAlso dsScale.Tables(0).Rows.Count > 0 Then
    '                    rpt_Rows.Item("Column21") = Format(CDec(dsScale.Tables(0).Rows(0)("newscale")), GUI.fmtCurrency)
    '                Else
    '                    rpt_Rows.Item("Column21") = Format(0, GUI.fmtCurrency)
    '                End If

    '                'Pinkal (28-MAY-2012) -- End


    '                'Pinkal (15-Jun-2012) -- Start
    '                'Enhancement : TRA Changes
    '                rpt_Rows.Item("Column22") = CDec(dtRow.Item("days")).ToString("#0.00")
    '                'Pinkal (15-Jun-2012) -- End



    '                'Pinkal (04-Mar-2013) -- Start
    '                'Enhancement : TRA Changes


    '                'Pinkal (24-May-2014) -- Start
    '                'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
    '                'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    'Pinkal (24-May-2014) -- End
    '                    Dim dsLeaveBal As DataSet = objLeaveBal.GetList("LeaveBalance", True, False, CInt(dtRow.Item("employeeunkid")), True, True)
    '                    Dim dtLeaveBal As DataTable = New DataView(dsLeaveBal.Tables(0), "leavetypeunkid = " & CInt(dtRow.Item("leavetypeunkid")), "", DataViewRowState.CurrentRows).ToTable

    '                    If dtLeaveBal.Rows.Count > 0 Then
    '                        rpt_Rows.Item("Column23") = CDate(dtLeaveBal.Rows(0)("startdate")).ToShortDateString
    '                        rpt_Rows.Item("Column24") = CDate(dtLeaveBal.Rows(0)("enddate")).ToShortDateString
    '                    Else
    '                        rpt_Rows.Item("Column23") = ""
    '                        rpt_Rows.Item("Column24") = ""
    '                    End If

    '                End If
    '                'Pinkal (04-Mar-2013) -- End



    '                'Pinkal (15-Jul-2013) -- Start
    '                'Enhancement : TRA Changes

    '                If Not IsDBNull(dtRow.Item("approve_stdate")) Then
    '                    rpt_Rows.Item("Column26") = CDate(dtRow.Item("approve_stdate")).ToShortDateString
    '                End If

    '                If Not IsDBNull(dtRow.Item("approve_eddate")) Then
    '                    rpt_Rows.Item("Column27") = CDate(dtRow.Item("approve_eddate")).ToShortDateString
    '                End If

    '                If Not IsDBNull(dtRow.Item("approve_days")) Then
    '                    rpt_Rows.Item("Column28") = CDec(dtRow.Item("approve_days")).ToString("#0.00")
    '                End If

    '                'Pinkal (15-Jul-2013) -- End



    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '            Next



    '            'Pinkal (15-Jul-2013) -- Start
    '            'Enhancement : TRA Changes

    '            'StrQ = " SELECT  lvleaveapprover_master.approverunkid  " & _
    '            '          ",lvapproverlevel_master.levelunkid " & _
    '            '          ",lvapproverlevel_master.levelname " & _
    '            '          ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') approvername " & _
    '            '          ",hrjob_master.job_name AS approvertitle " & _
    '            '          ",lvpendingleave_tran.approvaldate " & _
    '            '          ",CASE WHEN lvpendingleave_tran.statusunkid = 1 THEN  @Approve " & _
    '            '          "          WHEN lvpendingleave_tran.statusunkid = 2 THEN  @Pending " & _
    '            '          "          WHEN lvpendingleave_tran.statusunkid = 3 THEN  @Reject " & _
    '            '          "          WHEN lvpendingleave_tran.statusunkid = 4 THEN  @ReSchedule  " & _
    '            '          "          WHEN lvpendingleave_tran.statusunkid = 6 THEN  @Cancel " & _
    '            '          "          WHEN lvpendingleave_tran.statusunkid = 7 THEN @Issued " & _
    '            '          " END status	 " & _
    '            '          ",lvpendingleave_tran.remark " & _
    '            '          " FROM lvpendingleave_tran " & _
    '            '          " JOIN lvleaveform ON lvpendingleave_tran.formunkid = lvleaveform.formunkid AND lvleaveform.isvoid = 0 AND lvleaveform.formno = '" & mstrLeaveFormNo & "'" & _
    '            '          " JOIN lvleaveapprover_master ON lvpendingleave_tran.approvertranunkid = lvleaveapprover_master.approverunkid " & _
    '            '          " JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid = lvapproverlevel_master.levelunkid " & _
    '            '          " JOIN hremployee_master ON lvleaveapprover_master.leaveapproverunkid = hremployee_master.employeeunkid " & _
    '            '          " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '            '          " WHERE lvpendingleave_tran.isvoid = 0 "

    '            StrQ = " SELECT  lvleaveapprover_master.approverunkid  " & _
    '                      ",lvapproverlevel_master.levelunkid " & _
    '                      ",lvapproverlevel_master.levelname " & _
    '                    ",lvapproverlevel_master.priority " & _
    '                      ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') approvername " & _
    '                      ",hrjob_master.job_name AS approvertitle " & _
    '                      ",lvpendingleave_tran.approvaldate " & _
    '                    ",lvpendingleave_tran.startdate " & _
    '                    ",lvpendingleave_tran.enddate " & _
    '                    ",ISNULL(lvpendingleave_tran.dayfraction,0) AS dayfraction " & _
    '                    ",lvpendingleave_tran.statusunkid " & _
    '                      ",CASE WHEN lvpendingleave_tran.statusunkid = 1 THEN  @Approve " & _
    '                      "          WHEN lvpendingleave_tran.statusunkid = 2 THEN  @Pending " & _
    '                      "          WHEN lvpendingleave_tran.statusunkid = 3 THEN  @Reject " & _
    '                      "          WHEN lvpendingleave_tran.statusunkid = 4 THEN  @ReSchedule  " & _
    '                      "          WHEN lvpendingleave_tran.statusunkid = 6 THEN  @Cancel " & _
    '                      "          WHEN lvpendingleave_tran.statusunkid = 7 THEN @Issued " & _
    '                      " END status	 " & _
    '                      ",lvpendingleave_tran.remark " & _
    '                      " FROM lvpendingleave_tran " & _
    '                      " JOIN lvleaveform ON lvpendingleave_tran.formunkid = lvleaveform.formunkid AND lvleaveform.isvoid = 0 AND lvleaveform.formunkid = '" & mintLeaveFormID & "'" & _
    '                      " JOIN lvleaveapprover_master ON lvpendingleave_tran.approvertranunkid = lvleaveapprover_master.approverunkid " & _
    '                      " JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid = lvapproverlevel_master.levelunkid " & _
    '                      " JOIN hremployee_master ON lvleaveapprover_master.leaveapproverunkid = hremployee_master.employeeunkid " & _
    '                      " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '                      " WHERE lvpendingleave_tran.isvoid = 0 "

    '            'Pinkal (15-Jul-2013) -- End

    '            If mintEmployeeId > 0 Then
    '                StrQ &= " AND lvpendingleave_tran.employeeunkid = " & mintEmployeeId
    '            End If

    '            If mintLeaveTypeId > 0 Then
    '                StrQ &= " AND lvleaveform.leavetypeunkid = " & mintLeaveTypeId
    '            End If


    '            StrQ &= " ORDER BY lvapproverlevel_master.priority "

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
    '            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
    '            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
    '            objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
    '            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
    '            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))
    '            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '            End If

    '            Dim mintPriorty As Integer = -1


    '            'Pinkal (15-Jul-2013) -- Start
    '            'Enhancement : TRA Changes

    '            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
    'Recalculate:
    '                Dim mintPrioriry As Integer = 0
    '                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

    '                    If mintPrioriry <> CInt(dsList.Tables(0).Rows(i)("priority")) Then
    '                        mintPrioriry = CInt(dsList.Tables(0).Rows(i)("priority"))
    '                        Dim drRow() As DataRow = dsList.Tables(0).Select("statusunkid <> 2 AND priority  = " & mintPrioriry)
    '                        If drRow.Length > 0 Then
    '                            Dim drPending() As DataRow = dsList.Tables(0).Select("statusunkid = 2 AND priority  = " & mintPrioriry)
    '                            If drPending.Length > 0 Then
    '                                For Each dRow As DataRow In drPending
    '                                    dsList.Tables(0).Rows.Remove(dRow)
    '                                    GoTo Recalculate
    '                                Next
    '                                dsList.AcceptChanges()
    '                            End If
    '                        End If


    '                    End If

    '                Next
    '            End If

    '            'Pinkal (15-Jul-2013) -- End




    '            For Each dtRow As DataRow In dsList.Tables(0).Rows
    '                Dim rpt_Row As DataRow
    '                rpt_Row = rpt_LeaveApprovers.Tables("ArutiTable").NewRow
    '                rpt_Row.Item("Column1") = dtRow.Item("levelname")
    '                rpt_Row.Item("Column2") = dtRow.Item("approvername")
    '                rpt_Row.Item("Column3") = dtRow.Item("approvertitle")

    '                If Not IsDBNull(dtRow.Item("approvaldate")) Then
    '                    rpt_Row.Item("Column4") = CDate(dtRow.Item("approvaldate")).ToShortDateString
    '                Else
    '                    rpt_Row.Item("Column4") = ""
    '                End If

    '                rpt_Row.Item("Column5") = dtRow.Item("status")
    '                rpt_Row.Item("Column6") = dtRow.Item("remark")


    '                'Pinkal (15-Jul-2013) -- Start
    '                'Enhancement : TRA Changes

    '                If Not IsDBNull(dtRow.Item("startdate")) Then
    '                    rpt_Row.Item("Column7") = CDate(dtRow.Item("startdate")).ToShortDateString
    '                Else
    '                    rpt_Row.Item("Column7") = ""
    '                End If

    '                If Not IsDBNull(dtRow.Item("enddate")) Then
    '                    rpt_Row.Item("Column8") = CDate(dtRow.Item("enddate")).ToShortDateString
    '                Else
    '                    rpt_Row.Item("Column8") = ""
    '                End If

    '                If Not IsDBNull(dtRow.Item("dayfraction")) Then
    '                    rpt_Row.Item("Column9") = CDec((dtRow.Item("dayfraction"))).ToString("#0.00")
    '                End If

    '                'Pinkal (15-Jul-2013) -- End


    '                rpt_LeaveApprovers.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            Next


    '            StrQ = " SELECT ISNULL(description,'') description " & _
    '                       " ,qty " & _
    '                       " ,unitcost " & _
    '                       " ,amount  " & _
    '                       " FROM lvleaveexpense " & _
    '                       " JOIN lvleaveform ON lvleaveexpense.formunkid = lvleaveform.formunkid " & _
    '                       " WHERE lvleaveexpense.formunkid IN " & _
    '                       " ( " & _
    '                            " SELECT formunkid FROM lvleaveform " & _
    '                        "     WHERE formunkid = '" & mintLeaveFormID & "'"

    '            If mintEmployeeId > 0 Then
    '                StrQ &= " AND lvleaveform.employeeunkid = " & mintEmployeeId
    '            End If

    '            If mintLeaveTypeId > 0 Then
    '                StrQ &= " AND lvleaveform.leavetypeunkid = " & mintLeaveTypeId
    '            End If

    '            StrQ &= " ) AND lvleaveexpense.isvoid=0 "

    '            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '            End If


    '            'Pinkal (07-May-2015) -- Start
    '            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.

    '            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then

    '                StrQ = " SELECT statusunkid FROM cmclaim_request_master WHERE  expensetypeid = " & enExpenseType.EXP_LEAVE & " AND referenceunkid = @formunkid AND isvoid = 0 "
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveFormID)
    '                dsList = objDataOperation.ExecQuery(StrQ, "List")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '                End If

    '                If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then

    '                    If CInt(dsList.Tables(0).Rows(0)("statusunkid")) = 2 OrElse CInt(dsList.Tables(0).Rows(0)("statusunkid")) = 3 Then  'Pending/Reject

    '                        StrQ = "SELECT cmexpense_master.name AS description " & _
    '                                   ", ISNULL(cmclaim_request_tran.quantity,0) AS Qty " & _
    '                                   ", ISNULL(cmclaim_request_tran.unitprice,0.00) AS unitcost " & _
    '                                   ", ISNULL(cmclaim_request_tran.amount,0.00) as amount " & _
    '                                   " FROM cmclaim_request_tran " & _
    '                                   " LEFT JOIN cmclaim_request_master ON cmclaim_request_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
    '                                   " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid AND cmexpense_master.expensetypeid = " & enExpenseType.EXP_LEAVE & _
    '                                   " WHERE cmclaim_request_master.expensetypeid = " & enExpenseType.EXP_LEAVE & " And cmclaim_request_master.referenceunkid = @formunkid AND cmclaim_request_tran.isvoid = 0 "

    '                    ElseIf CInt(dsList.Tables(0).Rows(0)("statusunkid")) = 1 Then  'Approve
    '                        Dim mstrApproverIDs As String = ""
    '                        Dim drRow() As DataRow = Nothing
    '                        Dim intMaxPriority As Integer = -1

    '                        If mblnPaymentApprovalwithLeaveApproval Then
    '                            Dim objLeaveApprover As New clsleaveapprover_master
    '                            Dim dtTable As DataTable = objLeaveApprover.GetEmployeeApprover(-1, mintEmployeeId)
    '                            objLeaveApprover = Nothing
    '                            intMaxPriority = dtTable.Compute("Max(priority)", "1=1")
    '                            drRow = dtTable.Select("priority = " & intMaxPriority)

    '                            If drRow.Length > 0 Then
    '                                For i As Integer = 0 To drRow.Length - 1
    '                                    mstrApproverIDs &= drRow(i)("approverunkid").ToString() & ","
    '                                Next
    '                                If mstrApproverIDs.Trim.Length > 0 Then
    '                                    mstrApproverIDs = mstrApproverIDs.Trim.Substring(0, mstrApproverIDs.Trim.Length - 1)
    '                                End If
    '                            End If

    '                        Else
    '                            Dim objClaimApprover As New clsExpenseApprover_Master
    '                            Dim dsApproverList As DataSet = objClaimApprover.GetEmployeeApprovers(enExpenseType.EXP_LEAVE, mintEmployeeId, "List")
    '                            objClaimApprover = Nothing
    '                            intMaxPriority = dsApproverList.Tables(0).Compute("Max(crpriority)", "1=1")
    '                            drRow = dsApproverList.Tables(0).Select("crpriority = " & intMaxPriority)

    '                            If drRow.Length > 0 Then
    '                                For i As Integer = 0 To drRow.Length - 1
    '                                    mstrApproverIDs = drRow(i)("crapproverunkid").ToString() & ","
    '                                Next
    '                                If mstrApproverIDs.Trim.Length > 0 Then
    '                                    mstrApproverIDs = mstrApproverIDs.Trim.Substring(0, mstrApproverIDs.Trim.Length - 1)
    '                                End If
    '                            End If
    '                        End If

    '                        StrQ = "SELECT cmexpense_master.name AS description " & _
    '                                  ", ISNULL(cmclaim_approval_tran.quantity,0) AS Qty " & _
    '                                  ", ISNULL(cmclaim_approval_tran.unitprice,0.00) AS unitcost " & _
    '                                  ", ISNULL(cmclaim_approval_tran.amount,0.00) as amount " & _
    '                                  " FROM cmclaim_approval_tran " & _
    '                                  " LEFT JOIN cmclaim_request_master ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 " & _
    '                                  " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmexpense_master.expensetypeid = " & enExpenseType.EXP_LEAVE & _
    '                                  " WHERE cmclaim_request_master.expensetypeid = " & enExpenseType.EXP_LEAVE & " AND cmclaim_request_master.referenceunkid = @formunkid " & _
    '                                  " AND cmclaim_approval_tran.crapproverunkid IN (" & mstrApproverIDs & ")  AND cmclaim_approval_tran.statusunkid = 1 AND cmclaim_approval_tran.isvoid = 0"

    '                    End If

    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveFormID)
    '                    dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '                    If objDataOperation.ErrorMessage <> "" Then
    '                        Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '                    End If

    '                End If

    '            End If

    '            'Pinkal (07-May-2015) -- End




    '            Dim mdclTotalAmount As Decimal = 0
    '            For Each dtRow As DataRow In dsList.Tables(0).Rows
    '                Dim rpt_Row As DataRow
    '                rpt_Row = rpt_LeaveExpense.Tables("ArutiTable").NewRow
    '                rpt_Row.Item("Column1") = dtRow.Item("description")
    '                rpt_Row.Item("Column2") = Format(CDec(dtRow.Item("qty")), GUI.fmtCurrency)
    '                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("unitcost")), GUI.fmtCurrency)
    '                rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency)
    '                mdclTotalAmount += CDec(dtRow.Item("amount"))
    '                rpt_LeaveExpense.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            Next



    '            'Pinkal (19-Jun-2014) -- Start
    '            'Enhancement : TRA Changes Leave Enhancement
    '            Dim objDependents As New clsDependants_Beneficiary_tran

    '            'Pinkal (21-Jul-2014) -- Start
    '            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    '            'Dim dsDependents As DataSet = objDependents.GetQualifiedDepedant(mintEmployeeId)
    '            Dim dsDependents As DataSet = objDependents.GetQualifiedDepedant(mintEmployeeId, False, True, mdtEndDate)
    '            'Pinkal (21-Jul-2014) -- End


    '            For Each dtRow As DataRow In dsDependents.Tables(0).Rows
    '                Dim rpt_Row As DataRow
    '                rpt_Row = rpt_Dependents.Tables("ArutiTable").NewRow
    '                rpt_Row.Item("Column1") = dtRow.Item("dependants")
    '                rpt_Row.Item("Column2") = dtRow.Item("gender")
    '                rpt_Row.Item("Column3") = dtRow.Item("age")
    '                rpt_Row.Item("Column4") = dtRow.Item("Months")
    '                rpt_Row.Item("Column5") = dtRow.Item("relation")
    '                rpt_Dependents.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            Next

    '            'Pinkal (19-Jun-2014) -- End



    '            objRpt = New ArutiReport.Designer.rptEmployeeLeaveForm


    '            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '            Dim arrImageRow As DataRow = Nothing
    '            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '            ReportFunction.Logo_Display(objRpt, _
    '                                        ConfigParameter._Object._IsDisplayLogo, _
    '                                        ConfigParameter._Object._ShowLogoRightSide, _
    '                                        "arutiLogo1", _
    '                                        "arutiLogo2", _
    '                                        arrImageRow, _
    '                                        "txtCompanyName", _
    '                                        "txtReportName", _
    '                                        "", _
    '                                        ConfigParameter._Object._GetLeftMargin, _
    '                                        ConfigParameter._Object._GetRightMargin)

    '            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '                rpt_Data.Tables("ArutiTable").Rows.Add("")
    '            End If

    '            objRpt.SetDataSource(rpt_Data)

    '            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)

    '            If Company._Object._Address1.Trim.Length > 0 Then
    '                Call ReportFunction.TextChange(objRpt, "txtAddress1", Company._Object._Address1)
    '            Else
    '                Call ReportFunction.EnableSuppress(objRpt, "txtAddress1", True)
    '            End If

    '            Dim mstrAddress As String = String.Empty

    '            If Company._Object._Address2.ToString.Trim <> "" Then
    '                mstrAddress = Company._Object._Address2.ToString.Trim
    '            End If

    '            If Company._Object._Post_Code_No.ToString.Trim <> "" Then
    '                mstrAddress &= " , " & Company._Object._Post_Code_No.ToString.Trim
    '            End If

    '            If Company._Object._City_Name.ToString.Trim <> "" Then
    '                mstrAddress &= " , " & Company._Object._City_Name.ToString.Trim
    '            End If

    '            If Company._Object._State_Name.ToString.Trim <> "" Then
    '                mstrAddress &= " , " & Company._Object._State_Name.ToString.Trim
    '            End If

    '            If Company._Object._Country_Name.ToString.Trim <> "" Then
    '                mstrAddress &= " , " & Company._Object._Country_Name.ToString.Trim
    '            End If


    '            If mstrAddress.Trim.Length > 0 Then
    '                Call ReportFunction.TextChange(objRpt, "txtAddress2", mstrAddress)
    '            Else
    '                Call ReportFunction.EnableSuppress(objRpt, "txtAddress2", True)
    '            End If


    '            Call ReportFunction.TextChange(objRpt, "txtLeaveApplicationNo", Language.getMessage(mstrModuleName, 1, "LEAVE APPLICATION NUMBER :"))
    '            Call ReportFunction.TextChange(objRpt, "txtEmployeeParticulars", Language.getMessage(mstrModuleName, 2, "EMPLOYEE PARTICULARS"))
    '            Call ReportFunction.TextChange(objRpt, "txtLeaveRecords", Language.getMessage(mstrModuleName, 3, "LEAVE RECORDS"))
    '            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 4, "Name :"))
    '            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 5, "Emp Code :"))
    '            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 6, "Job Title :"))
    '            Call ReportFunction.TextChange(objRpt, "txtGrade", Language.getMessage(mstrModuleName, 7, "Grade :"))
    '            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 9, "Dept :"))
    '            Call ReportFunction.TextChange(objRpt, "txtAppDate", Language.getMessage(mstrModuleName, 10, "Appt. Date :"))
    '            Call ReportFunction.TextChange(objRpt, "txtConfirmDate", Language.getMessage(mstrModuleName, 11, "Confirm. Date:"))
    '            Call ReportFunction.TextChange(objRpt, "txtLeaveType", Language.getMessage(mstrModuleName, 12, "Leave Type :"))
    '            Call ReportFunction.TextChange(objRpt, "txtAccrueLstYr", Language.getMessage(mstrModuleName, 13, "Accrued UpTo Last Year :"))
    '            Call ReportFunction.TextChange(objRpt, "txtAccrueToDate", Language.getMessage(mstrModuleName, 14, "Accrued ToDate :"))
    '            Call ReportFunction.TextChange(objRpt, "txtIssueLstYr", Language.getMessage(mstrModuleName, 15, "Issued UpTo Last Year :"))
    '            Call ReportFunction.TextChange(objRpt, "txtIssueToDate", Language.getMessage(mstrModuleName, 16, "Total Issued ToDate :"))
    '            Call ReportFunction.TextChange(objRpt, "txtLastLeaveDates", Language.getMessage(mstrModuleName, 17, "Last Leave Dates :"))
    '            Call ReportFunction.TextChange(objRpt, "txtLeaveBalance", Language.getMessage(mstrModuleName, 18, "Leave Balance :"))
    '            Call ReportFunction.TextChange(objRpt, "txtLeaveApplicationDetail", Language.getMessage(mstrModuleName, 19, "LEAVE APPLICATION DETAILS"))
    '            Call ReportFunction.TextChange(objRpt, "txtApplyDate", Language.getMessage(mstrModuleName, 20, "Apply Date :"))
    '            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 21, "Start Date :"))
    '            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 22, "End Date :"))
    '            Call ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 23, "Address and Tel# While on Leave :"))
    '            Call ReportFunction.TextChange(objRpt, "txtComments", Language.getMessage(mstrModuleName, 24, "Remarks/Comments :"))



    '            'Pinkal (30-Apr-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Call ReportFunction.TextChange(objRpt, "txtLeaveBF", Language.getMessage(mstrModuleName, 49, "Leave B/F :"))
    '            'Pinkal (30-Apr-2013) -- End



    '            'Pinkal (31-Jan-2014) -- Start
    '            'Enhancement : Oman Changes
    '            If ConfigParameter._Object._ShowBasicSalaryOnLeaveFormReport Then
    '                Call ReportFunction.TextChange(objRpt, "txtBasicSal", Language.getMessage(mstrModuleName, 46, "Basic Salary :"))
    '            Else
    '                Call ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "Column211", True)
    '            End If
    '            'Pinkal (31-Jan-2014) -- End






    '            'Pinkal (24-Aug-2012) -- Start
    '            'Enhancement : TRA Changes

    '            If ConfigParameter._Object._IsShowApprovedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 41, "APPROVED BY :"))
    '                Call ReportFunction.TextChange(objRpt, "lblHrManager", Language.getMessage(mstrModuleName, 42, "HR. MANAGER"))
    '                Call ReportFunction.TextChange(objRpt, "lblHOD", Language.getMessage(mstrModuleName, 43, "HEAD OF DEPARTMENT"))
    '                Call ReportFunction.TextChange(objRpt, "lblMd", Language.getMessage(mstrModuleName, 44, "MANAGING DIRECTOR"))
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
    '            End If

    '            'Pinkal (24-Aug-2012) -- End



    '            'Pinkal (15-Jun-2012) -- Start
    '            'Enhancement : TRA Changes
    '            Call ReportFunction.TextChange(objRpt, "txtDaysRequsted", Language.getMessage(mstrModuleName, 45, "Days Requested : "))
    '            'Pinkal (15-Jun-2012) -- End



    '            'Pinkal (04-Mar-2013) -- Start
    '            'Enhancement : TRA Changes


    '            'Pinkal (24-May-2014) -- Start
    '            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
    '            'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                'Pinkal (24-May-2014) -- End
    '                Call ReportFunction.EnableSuppress(objRpt, "txtELCStart", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "txtELCEnd", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "Column231", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "Column241", True)
    '            Else
    '                Call ReportFunction.TextChange(objRpt, "txtELCStart", Language.getMessage(mstrModuleName, 47, "ELC Start Date :"))
    '                Call ReportFunction.TextChange(objRpt, "txtELCEnd", Language.getMessage(mstrModuleName, 48, "ELC End Date :"))
    '            End If

    '            'Pinkal (04-Mar-2013) -- End



    '            'Pinkal (15-Jul-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Call ReportFunction.TextChange(objRpt, "txtApprovedStDate", Language.getMessage(mstrModuleName, 50, "Approved Start Date : "))
    '            Call ReportFunction.TextChange(objRpt, "txtApprovedEdDate", Language.getMessage(mstrModuleName, 51, "Approved End Date : "))
    '            Call ReportFunction.TextChange(objRpt, "txtApprovedDays", Language.getMessage(mstrModuleName, 52, "Approved Days : "))
    '            'Pinkal (15-Jul-2013) -- End


    '            'Pinkal (24-Aug-2013) -- Start
    '            'Enhancement : TRA Changes

    '            Dim dsLvBal As DataSet = Nothing
    '            Dim dvLeaveBal As DataTable = Nothing
    '            Dim mdtAsonDate As Date = Nothing

    '            'Pinkal (24-May-2014) -- Start
    '            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
    '            'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                'Pinkal (24-May-2014) -- End
    '                dsLvBal = objLeaveBal.GetList("LeaveBalance", True, False, mintEmployeeId, False, False)
    '                dvLeaveBal = New DataView(dsLvBal.Tables(0), "leavetypeunkid = " & mintLeaveTypeId, "", DataViewRowState.CurrentRows).ToTable
    '                If dvLeaveBal.Rows.Count > 0 Then
    '                    If Not dvLeaveBal.Rows(0)("enddate") Is DBNull.Value Then

    '                        'Pinkal (24-May-2014) -- Start
    '                        'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]

    '                        'If CDate(dvLeaveBal.Rows(0)("enddate")).Date >= FinancialYear._Object._Database_End_Date.Date Then
    '                        '    mdtAsonDate = FinancialYear._Object._Database_End_Date.Date.ToShortDateString
    '                        'ElseIf CDate(dvLeaveBal.Rows(0)("enddate")).Date < FinancialYear._Object._Database_End_Date.Date Then
    '                        '    mdtAsonDate = CDate(dvLeaveBal.Rows(0)("enddate")).ToShortDateString
    '                        'End If

    '                        If CDate(dvLeaveBal.Rows(0)("enddate")).Date >= mdtFinEndDate.Date Then
    '                            mdtAsonDate = mdtFinEndDate.Date.ToShortDateString
    '                        ElseIf CDate(dvLeaveBal.Rows(0)("enddate")).Date < mdtFinEndDate.Date Then
    '                            mdtAsonDate = CDate(dvLeaveBal.Rows(0)("enddate")).ToShortDateString
    '                        End If

    '                        'Pinkal (24-May-2014) -- End


    '                    Else

    '                        'Pinkal (24-May-2014) -- Start
    '                        'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
    '                        'mdtAsonDate = FinancialYear._Object._Database_End_Date.ToShortDateString
    '                        mdtAsonDate = mdtFinEndDate.Date.ToShortDateString
    '                        'Pinkal (24-May-2014) -- End


    '                    End If
    '                End If


    '                'Pinkal (24-May-2014) -- Start
    '                'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
    '                'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                'Pinkal (24-May-2014) -- End
    '                dsLvBal = objLeaveBal.GetList("LeaveBalance", True, False, mintEmployeeId, True, True)
    '                dvLeaveBal = New DataView(dsLvBal.Tables(0), "leavetypeunkid = " & mintLeaveTypeId, "", DataViewRowState.CurrentRows).ToTable
    '                If dvLeaveBal.Rows.Count > 0 Then
    '                    If Not dvLeaveBal.Rows(0)("enddate") Is DBNull.Value Then
    '                        mdtAsonDate = CDate(dvLeaveBal.Rows(0)("enddate")).ToShortDateString
    '                    End If
    '                End If
    '            End If

    '            Call ReportFunction.TextChange(objRpt, "txtLeaveBaltilldate", Language.getMessage(mstrModuleName, 56, " Till") & Space(5) & mdtAsonDate)

    '            'Pinkal (24-Aug-2013) -- End



    '            objRpt.Subreports("rptLeaveApproverDetailReport").SetDataSource(rpt_LeaveApprovers)
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtLeaveApprovals", Language.getMessage(mstrModuleName, 25, "LEAVE APPROVALS"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtLevelName", Language.getMessage(mstrModuleName, 26, "Level Name"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtApproverName", Language.getMessage(mstrModuleName, 27, "Approver Name"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtApproverTitle", Language.getMessage(mstrModuleName, 28, "Approver Title"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtApprovalDate", Language.getMessage(mstrModuleName, 29, "Approval Date"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtApprovalStatus", Language.getMessage(mstrModuleName, 30, "Approval Status"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtRemarks", Language.getMessage(mstrModuleName, 31, "Remarks/Comments"))


    '            'Pinkal (15-Jul-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtStartDate", Language.getMessage(mstrModuleName, 53, "Start Date"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtEndDate", Language.getMessage(mstrModuleName, 54, "End Date"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtDay", Language.getMessage(mstrModuleName, 55, "Days"))
    '            'Pinkal (15-Jul-2013) -- End



    '            objRpt.Subreports("rptEmployeeLeaveExpense").SetDataSource(rpt_LeaveExpense)

    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txttotalAmount", Format(mdclTotalAmount, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtLeaveApprovals", Language.getMessage(mstrModuleName, 32, "APPROVED LEAVE EXPENSES AND TRAVEL ASSISTANCE"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtdescription", Language.getMessage(mstrModuleName, 33, "Description"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtQty", Language.getMessage(mstrModuleName, 34, "Qty"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtUnitRate", Language.getMessage(mstrModuleName, 35, "Unit Rate"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtAmount", Language.getMessage(mstrModuleName, 36, "Amount"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtTotal", Language.getMessage(mstrModuleName, 37, "Total :"))




    '            'Pinkal (13-Jul-2015) -- Start
    '            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
    '            ReportFunction.EnableSuppressSection(objRpt, "DetailSection1", Not mblnShowExpense)
    '            'Pinkal (13-Jul-2015) -- End




    '            'Pinkal (19-Jun-2014) -- Start
    '            'Enhancement : TRA Changes Leave Enhancement
    '            If mblnIncludedependents Then
    '                objRpt.Subreports("rptEmployeeDepedents").SetDataSource(rpt_Dependents)
    '                Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeDepedents"), "txtName", Language.getMessage(mstrModuleName, 57, "Name"))
    '                Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeDepedents"), "txtGender", Language.getMessage(mstrModuleName, 58, "Gender"))
    '                Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeDepedents"), "txtAge", Language.getMessage(mstrModuleName, 59, "Age"))
    '                Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeDepedents"), "txtMonths", Language.getMessage(mstrModuleName, 60, "Month"))
    '                Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeDepedents"), "txtRelations", Language.getMessage(mstrModuleName, 61, "Relation"))
    '            Else

    '                ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", True)
    '            End If

    '            'Pinkal (19-Jun-2014) -- End



    '            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 38, "Printed By :"))
    '            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 39, "Printed Date :"))


    '            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)

    '            If Company._Object._Address1.Trim.Length <= 0 AndAlso mstrAddress.Trim.Length <= 0 Then
    '                objRpt.ReportDefinition.ReportObjects("txtReportName").Top = objRpt.ReportDefinition.ReportObjects("txtAddress1").Top
    '            End If

    '            Return objRpt
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '            Return Nothing
    '        End Try
    '    End Function


    Private Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_LeaveApprovers As ArutiReport.Designer.dsArutiReport
        Dim rpt_LeaveExpense As ArutiReport.Designer.dsArutiReport
        Dim rpt_Dependents As ArutiReport.Designer.dsArutiReport


        Try
            objDataOperation = New clsDataOperation


            StrQ = " SELECT  lvleaveform.employeeunkid " & _
                       ",lvleaveform.formunkid " & _
                       ",lvleaveform.leavetypeunkid " & _
                       ",employeecode " & _
                       ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employeename " & _
                       ",hrjob_master.job_name AS jobtitle " & _
                       ",hrgrade_master.name AS grade " & _
                       ",hrdepartment_master.name AS department " & _
                       " ,hremployee_master.appointeddate " & _
                       ",hremployee_master.confirmation_date " & _
                       ",lvleaveform.formno " & _
                       ",lvleavetype_master.leavename " & _
                       ",lvleaveform.applydate " & _
                       ",lvleaveform.startdate " & _
                       ",lvleaveform.returndate " & _
                      ", lvleaveform.approve_stdate " & _
                      ", lvleaveform.approve_eddate " & _
                       ",lvleaveform.addressonleave " & _
                       ",lvleaveform.remark " & _
                      ",ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND lvleaveform.returndate AND lvleaveday_fraction.approverunkid <=0),0.00) days " & _
                      ", ISNULL(lvleaveform.approve_days,0) AS 	approve_days	  " & _
                       " FROM lvleaveform " & _
                       " JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
                       " JOIN " & _
                         " ( " & _
                         "         SELECT " & _
                         "         jobunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "    FROM hremployee_categorization_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                         " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                        " JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                        " JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "        gradeunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                        "    FROM prsalaryincrement_tran " & _
                        "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                        ") AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                        " JOIN hrgrade_master ON hrgrade_master.gradeunkid = Grds.gradeunkid " & _
                        " JOIN " & _
                                " ( " & _
                                "    SELECT " & _
                                "         departmentunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                                " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                       " JOIN hrdepartment_master ON  hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                       " JOIN lvleavetype_master ON lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "         date1 " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "    FROM hremployee_dates_tran " & _
                       "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                       " )AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 " & _
                       " WHERE lvleaveform.isvoid = 0 "

            'Pinkal (06-Jan-2016) -- Start 'Enhancement - Working on Changes in SS for Leave Module.[AND hremployee_dates_tran.employeeunkid IN (" & mintEmployeeId & ")]

            StrQ &= " AND lvleaveform.formunkid = " & mintLeaveFormID


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            rpt_LeaveApprovers = New ArutiReport.Designer.dsArutiReport
            rpt_LeaveExpense = New ArutiReport.Designer.dsArutiReport
            rpt_Dependents = New ArutiReport.Designer.dsArutiReport

            Dim objLeaveBal As New clsleavebalance_tran
            Dim objLeaveForm As New clsleaveform
            Dim objMaster As New clsMasterData

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("formno")
                rpt_Rows.Item("Column2") = dtRow.Item("employeename")
                rpt_Rows.Item("Column3") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column4") = dtRow.Item("jobtitle")
                rpt_Rows.Item("Column5") = dtRow.Item("grade")
                rpt_Rows.Item("Column6") = dtRow.Item("department")

                If Not IsDBNull(dtRow.Item("appointeddate")) Then
                    rpt_Rows.Item("Column7") = CDate(dtRow.Item("appointeddate")).ToShortDateString
                Else
                    rpt_Rows.Item("Column7") = ""
                End If

                If Not IsDBNull(dtRow.Item("confirmation_date")) Then
                    rpt_Rows.Item("Column8") = CDate(dtRow.Item("confirmation_date")).ToShortDateString
                Else
                    rpt_Rows.Item("Column8") = ""
                End If

                rpt_Rows.Item("Column9") = dtRow.Item("leavename")

                Dim dList As DataSet = objLeaveForm.GetEmployeeLastIssuedLeave(CInt(dtRow.Item("formunkid")), CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("leavetypeunkid")))

                If dList IsNot Nothing And dList.Tables(0).Rows.Count > 0 Then
                    rpt_Rows.Item("Column14") = CDate(dList.Tables(0).Rows(0)("startdate")).ToShortDateString & Language.getMessage(mstrModuleName, 40, " To ") & CDate(dList.Tables(0).Rows(0)("enddate")).ToShortDateString
                End If

                If mintLeaveTypeId <= 0 Then mintLeaveTypeId = CInt(dtRow.Item("leavetypeunkid"))
                If mintEmployeeId <= 0 Then mintEmployeeId = CInt(dtRow.Item("employeeunkid"))
                Dim dsBalance As DataSet = Nothing


                'Pinkal (24-May-2014) -- Start
                'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]

                'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '    dsBalance = objLeaveBal.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")))
                'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                '    dsBalance = objLeaveBal.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")), Nothing, Nothing, True, True)
                'End If

                If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                    'Pinkal (28-Jan-2019) -- Start
                    'Bug - Due to Change In Get Balance Info Method report was showing 0 for all columns due to Pacra changes did by sohail for Leave Details.
                    'dsBalance = objLeaveBal.GetLeaveBalanceInfo(mdtEmployeeAsonDate.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtFinStartDate.Date, mdtFinEndDate.Date, False, False, mintLeaveBalanceSetting, 0, xYearUnkid)
                    dsBalance = objLeaveBal.GetLeaveBalanceInfo(mdtEmployeeAsonDate.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtFinStartDate.Date, mdtFinEndDate.Date, False, False, mintLeaveBalanceSetting, -1, xYearUnkid)
                    'Pinkal (28-Jan-2019) -- End

                ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    objLeaveBal._DBStartdate = mdtdbStartDate
                    objLeaveBal._DBEnddate = mdtdbEndDate
                    dsBalance = objLeaveBal.GetLeaveBalanceInfo(mdtEmployeeAsonDate.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True, mintLeaveBalanceSetting, 0, xYearUnkid)

                End If

                If dsBalance IsNot Nothing And dsBalance.Tables(0).Rows.Count > 0 Then
                    rpt_Rows.Item("Column10") = CDec(dsBalance.Tables(0).Rows(0)("LstyearAccrue_amount")).ToString("#0.00")
                    rpt_Rows.Item("Column11") = CDec(dsBalance.Tables(0).Rows(0)("Accrue_amount")).ToString("#0.00")
                    rpt_Rows.Item("Column12") = CDec(dsBalance.Tables(0).Rows(0)("LstyearIssue_amount")).ToString("#0.00")
                    rpt_Rows.Item("Column13") = CDec(dsBalance.Tables(0).Rows(0)("Issue_amount")).ToString("#0.00")
                    rpt_Rows.Item("Column15") = CDec(dsBalance.Tables(0).Rows(0)("Balance")).ToString("#0.00")
                    rpt_Rows.Item("Column25") = CDec(dsBalance.Tables(0).Rows(0)("LeaveBF")).ToString("#0.00")
                    rpt_Rows.Item("Column13") = CDec(CDec(dsBalance.Tables(0).Rows(0)("Issue_amount")) + CDec(dsBalance.Tables(0).Rows(0)("LeaveEncashment"))).ToString("#0.00")
                    rpt_Rows.Item("Column29") = CDec(dsBalance.Tables(0).Rows(0)("LeaveAdjustment")).ToString("#0.00")
                    rpt_Rows.Item("Column30") = CDec(CDec(dsBalance.Tables(0).Rows(0)("Accrue_amount")) + CDec(dsBalance.Tables(0).Rows(0)("LeaveAdjustment")) + CDec(dsBalance.Tables(0).Rows(0)("LeaveBF")) - (CDec(dsBalance.Tables(0).Rows(0)("Issue_amount")) + CDec(dsBalance.Tables(0).Rows(0)("LeaveEncashment")))).ToString("#0.00")
                End If

                If Not IsDBNull(dtRow.Item("applydate")) Then
                    rpt_Rows.Item("Column16") = CDate(dtRow.Item("applydate")).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("startdate")) Then
                    rpt_Rows.Item("Column17") = CDate(dtRow.Item("startdate")).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("returndate")) Then
                    rpt_Rows.Item("Column18") = CDate(dtRow.Item("returndate")).ToShortDateString
                    mdtEndDate = CDate(dtRow.Item("returndate")).ToShortDateString
                End If

                rpt_Rows.Item("Column19") = dtRow.Item("addressonleave")
                rpt_Rows.Item("Column20") = dtRow.Item("remark")

                Dim dsScale As DataSet = objMaster.Get_Current_Scale("List", CInt(dtRow.Item("employeeunkid")), mdtEmployeeAsonDate.Date)


                If dsScale IsNot Nothing AndAlso dsScale.Tables(0).Rows.Count > 0 Then
                    rpt_Rows.Item("Column21") = Format(CDec(dsScale.Tables(0).Rows(0)("newscale")), GUI.fmtCurrency)
                Else
                    rpt_Rows.Item("Column21") = Format(0, GUI.fmtCurrency)
                End If

                rpt_Rows.Item("Column22") = CDec(dtRow.Item("days")).ToString("#0.00")

                If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    Dim dsLeaveBal As DataSet = objLeaveBal.GetList("LeaveBalance", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                                             , eZeeDate.convertDate(mdtEmployeeAsonDate), xUserModeSetting, xOnlyApproved _
                                                                                             , False, True, True, False, CInt(dtRow.Item("employeeunkid")), True, True, False, "")
                    Dim dtLeaveBal As DataTable = New DataView(dsLeaveBal.Tables(0), "leavetypeunkid = " & CInt(dtRow.Item("leavetypeunkid")), "", DataViewRowState.CurrentRows).ToTable

                    If dtLeaveBal.Rows.Count > 0 Then
                        rpt_Rows.Item("Column23") = CDate(dtLeaveBal.Rows(0)("startdate")).ToShortDateString
                        rpt_Rows.Item("Column24") = CDate(dtLeaveBal.Rows(0)("enddate")).ToShortDateString
                    Else
                        rpt_Rows.Item("Column23") = ""
                        rpt_Rows.Item("Column24") = ""
                    End If

                End If

                If Not IsDBNull(dtRow.Item("approve_stdate")) Then
                    rpt_Rows.Item("Column26") = CDate(dtRow.Item("approve_stdate")).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("approve_eddate")) Then
                    rpt_Rows.Item("Column27") = CDate(dtRow.Item("approve_eddate")).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("approve_days")) Then
                    rpt_Rows.Item("Column28") = CDec(dtRow.Item("approve_days")).ToString("#0.00")
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next


            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrCommJoinQry, StrConditionQry As String

            StrInnerQry = "" : StrCommJoinQry = "" : StrConditionQry = ""

            StrQ = "SELECT DISTINCT " & _
                   "   ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "  ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "FROM lvpendingleave_tran " & _
                   "    JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   "WHERE lvpendingleave_tran.isvoid = 0 AND lvleaveapprover_master.isexternalapprover = 1 AND lvpendingleave_tran.formunkid = '" & mintLeaveFormID & "' "

            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrInnerQry = "SELECT lvleaveapprover_master.approverunkid  " & _
                      ",lvapproverlevel_master.levelunkid " & _
                      ",lvapproverlevel_master.levelname " & _
                    ",lvapproverlevel_master.priority " & _
                          "    ,#APPVR_NAME# approvername " & _
                          "    ,#APPR_JOB_NAME# AS approvertitle " & _
                      ",lvpendingleave_tran.approvaldate " & _
                    ",lvpendingleave_tran.startdate " & _
                    ",lvpendingleave_tran.enddate " & _
                    ",ISNULL(lvpendingleave_tran.dayfraction,0) AS dayfraction " & _
                    ",lvpendingleave_tran.statusunkid " & _
                      ",CASE WHEN lvpendingleave_tran.statusunkid = 1 THEN  @Approve " & _
                      "          WHEN lvpendingleave_tran.statusunkid = 2 THEN  @Pending " & _
                      "          WHEN lvpendingleave_tran.statusunkid = 3 THEN  @Reject " & _
                      "          WHEN lvpendingleave_tran.statusunkid = 4 THEN  @ReSchedule  " & _
                      "          WHEN lvpendingleave_tran.statusunkid = 6 THEN  @Cancel " & _
                      "          WHEN lvpendingleave_tran.statusunkid = 7 THEN @Issued " & _
                      " END status	 " & _
                      ",lvpendingleave_tran.remark " & _
                      " FROM lvpendingleave_tran " & _
                      " JOIN lvleaveform ON lvpendingleave_tran.formunkid = lvleaveform.formunkid AND lvleaveform.isvoid = 0 AND lvleaveform.formunkid = '" & mintLeaveFormID & "'" & _
                      " JOIN lvleaveapprover_master ON lvpendingleave_tran.approvertranunkid = lvleaveapprover_master.approverunkid " & _
                      " JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid = lvapproverlevel_master.levelunkid " & _
                          " #COMM_JOIN# "

            StrConditionQry = " WHERE lvpendingleave_tran.isvoid = 0 AND lvleaveapprover_master.isexternalapprover = #ExApprId# "

            If mintEmployeeId > 0 Then
                StrConditionQry &= " AND lvpendingleave_tran.employeeunkid = " & mintEmployeeId
            End If

            If mintLeaveTypeId > 0 Then
                StrConditionQry &= " AND lvleaveform.leavetypeunkid = " & mintLeaveTypeId
            End If

            StrQ = StrInnerQry
            StrQ &= StrConditionQry
            StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")
            StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON lvleaveapprover_master.leaveapproverunkid = hremployee_master.employeeunkid " & _
                                                                     " JOIN " & _
                                                                     " ( " & _
                                                                     "         SELECT " & _
                                                                     "         jobunkid " & _
                                                                     "        ,employeeunkid " & _
                                                                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                                     "    FROM #DB_Name#hremployee_categorization_tran " & _
                                                                     "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                                                                     " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                                                     " JOIN #DB_Name#hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid ")


            'Pinkal (06-May-2019) --  'Defect [0003794] - Worked Leave Approver old Job Title still appearing on Leave form instead of new job title [TFDA].

            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#ExApprId#", "0")
            StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
            StrQ &= " ORDER BY lvapproverlevel_master.priority "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry

                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPR_JOB_NAME#", "'' ")
                    StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(UEmp.username,'') ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvpendingleave_tran.approverunkid ")
                Else
                    StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
                    StrQ = StrQ.Replace("#APPVR_NAME#", " CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvpendingleave_tran.approverunkid " & _
                                                       " JOIN #DB_Name#hremployee_master ON UEmp.employeeunkid = hremployee_master.employeeunkid " & _
                                                       " JOIN " & _
                                                       " ( " & _
                                                       "         SELECT " & _
                                                       "         jobunkid " & _
                                                       "        ,employeeunkid " & _
                                                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                       "    FROM #DB_Name#hremployee_categorization_tran " & _
                                                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                                                       " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                                       " JOIN #DB_Name#hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid ")

                    'Pinkal (06-May-2019) --  'Defect [0003794] - Worked Leave Approver old Job Title still appearing on Leave form instead of new job title [TFDA].

                End If


                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DB_Name#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DB_Name#", "")
                End If

                StrQ = StrQ.Replace("#ExApprId#", "1")
                StrQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next



            Dim mintPriorty As Integer = -1

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
Recalculate:
                Dim mintPrioriry As Integer = 0
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If mintPrioriry <> CInt(dsList.Tables(0).Rows(i)("priority")) Then
                        mintPrioriry = CInt(dsList.Tables(0).Rows(i)("priority"))
                        Dim drRow() As DataRow = dsList.Tables(0).Select("statusunkid <> 2 AND priority  = " & mintPrioriry)
                        If drRow.Length > 0 Then
                            Dim drPending() As DataRow = dsList.Tables(0).Select("statusunkid = 2 AND priority  = " & mintPrioriry)
                            If drPending.Length > 0 Then
                                For Each dRow As DataRow In drPending
                                    dsList.Tables(0).Rows.Remove(dRow)
                                    GoTo Recalculate
                                Next
                                dsList.AcceptChanges()
                            End If
                        End If


                    End If

                Next
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_LeaveApprovers.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("levelname")
                rpt_Row.Item("Column2") = dtRow.Item("approvername")
                rpt_Row.Item("Column3") = dtRow.Item("approvertitle")

                If Not IsDBNull(dtRow.Item("approvaldate")) Then
                    rpt_Row.Item("Column4") = CDate(dtRow.Item("approvaldate")).ToShortDateString
                Else
                    rpt_Row.Item("Column4") = ""
                End If
                rpt_Row.Item("Column5") = dtRow.Item("status")
                rpt_Row.Item("Column6") = dtRow.Item("remark")
                If Not IsDBNull(dtRow.Item("startdate")) Then
                    rpt_Row.Item("Column7") = CDate(dtRow.Item("startdate")).ToShortDateString
                Else
                    rpt_Row.Item("Column7") = ""
                End If

                If Not IsDBNull(dtRow.Item("enddate")) Then
                    rpt_Row.Item("Column8") = CDate(dtRow.Item("enddate")).ToShortDateString
                Else
                    rpt_Row.Item("Column8") = ""
                End If

                If Not IsDBNull(dtRow.Item("dayfraction")) Then
                    rpt_Row.Item("Column9") = CDec((dtRow.Item("dayfraction"))).ToString("#0.00")
                End If
                rpt_LeaveApprovers.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            dsList = Nothing

            StrQ = " SELECT ISNULL(description,'') description " & _
                       " ,qty " & _
                       " ,unitcost " & _
                       " ,amount  " & _
                    ",'' AS sector " & _
                    ",'' as expense_remark	" & _
                       " FROM lvleaveexpense " & _
                       " JOIN lvleaveform ON lvleaveexpense.formunkid = lvleaveform.formunkid " & _
                       " WHERE lvleaveexpense.formunkid IN " & _
                       " ( " & _
                            " SELECT formunkid FROM lvleaveform " & _
                        "     WHERE formunkid = '" & mintLeaveFormID & "'"


            If mintEmployeeId > 0 Then
                StrQ &= " AND lvleaveform.employeeunkid = " & mintEmployeeId
            End If

            If mintLeaveTypeId > 0 Then
                StrQ &= " AND lvleaveform.leavetypeunkid = " & mintLeaveTypeId
            End If

            StrQ &= " ) AND lvleaveexpense.isvoid=0 "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then

                StrQ = " SELECT statusunkid FROM cmclaim_request_master WHERE  expensetypeid = " & enExpenseType.EXP_LEAVE & " AND referenceunkid = @formunkid AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveFormID)
                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then

                    If CInt(dsList.Tables(0).Rows(0)("statusunkid")) = 2 OrElse CInt(dsList.Tables(0).Rows(0)("statusunkid")) = 3 Then  'Pending/Reject

                        dsList = Nothing

                        StrQ = "SELECT cmexpense_master.name AS description " & _
                                   ", ISNULL(cmclaim_request_tran.quantity,0) AS Qty " & _
                                   ", ISNULL(cmclaim_request_tran.unitprice,0.00) AS unitcost " & _
                                   ", ISNULL(cmclaim_request_tran.amount,0.00) as amount " & _
                                   ", ISNULL(cfcommon_master.name, '' ) AS sector " & _
                                   ", ISNULL(cmclaim_request_tran.expense_remark, '' ) AS expense_remark " & _
                                   " FROM cmclaim_request_tran " & _
                                   " LEFT JOIN cmclaim_request_master ON cmclaim_request_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                                   " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid AND cmexpense_master.expensetypeid = " & enExpenseType.EXP_LEAVE & _
                                   " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_request_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                                   " WHERE cmclaim_request_master.expensetypeid = " & enExpenseType.EXP_LEAVE & " And cmclaim_request_master.referenceunkid = @formunkid AND cmclaim_request_tran.isvoid = 0 "


                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveFormID)
                        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                        If objDataOperation.ErrorMessage <> "" Then
                            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                        End If

                    ElseIf CInt(dsList.Tables(0).Rows(0)("statusunkid")) = 1 Then  'Approve
                        Dim mstrApproverIDs As String = ""
                        Dim drRow() As DataRow = Nothing
                        Dim intMaxPriority As Integer = -1

                        If mblnPaymentApprovalwithLeaveApproval Then


                            Dim objPending As New clspendingleave_Tran
                            Dim dtTable As DataTable = objPending.GetEmployeeApproverListWithPriority(mintEmployeeId, mintLeaveFormID, Nothing).Tables(0)
                            objPending = Nothing

                            'Pinkal (13-Jun-2019) -- Start
                            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[Due to Setting change at NMB from simultaneous to parallel not able to display expense] 
                            'intMaxPriority = dtTable.Compute("Max(priority)", "1=1")
                            intMaxPriority = IIf(IsDBNull(dtTable.Compute("Max(priority)", "1=1")), -1, dtTable.Compute("Max(priority)", "1=1"))
                            'Pinkal (13-Jun-2019) -- End


                            drRow = dtTable.Select("priority = " & intMaxPriority)

                            If drRow.Length > 0 Then
                                For i As Integer = 0 To drRow.Length - 1
                                    mstrApproverIDs &= drRow(i)("approverunkid").ToString() & ","
                                Next
                                If mstrApproverIDs.Trim.Length > 0 Then
                                    mstrApproverIDs = mstrApproverIDs.Trim.Substring(0, mstrApproverIDs.Trim.Length - 1)
                                End If
                            End If

                        Else

                            Dim objClaimApprover As New clsclaim_request_approval_tran
                            Dim dsApproverList As DataSet = objClaimApprover.GetEmployeeClaimApproverListWithPriority(mintEmployeeId, mintLeaveFormID, Nothing)
                            objClaimApprover = Nothing


                            'Pinkal (13-Jun-2019) -- Start
                            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[Due to Setting change at NMB from simultaneous to parallel not able to display expense] 
                            'intMaxPriority = dsApproverList.Tables(0).Compute("Max(crpriority)", "1=1")
                            intMaxPriority = IIf(IsDBNull(dsApproverList.Tables(0).Compute("Max(crpriority)", "1=1")), -1, dsApproverList.Tables(0).Compute("Max(crpriority)", "1=1"))
                            'Pinkal (13-Jun-2019) -- End

                            drRow = dsApproverList.Tables(0).Select("crpriority = " & intMaxPriority)

                            If drRow.Length > 0 Then
                                For i As Integer = 0 To drRow.Length - 1
                                    'Pinkal (18-Dec-2020) -- Start
                                    'Bug  -  Bug Resolved for not displaying expense details in Employee Leave Form Report.
                                    'mstrApproverIDs = drRow(i)("crapproverunkid").ToString() & ","
                                    mstrApproverIDs &= drRow(i)("crapproverunkid").ToString() & ","
                                    'Pinkal (18-Dec-2020) -- End
                                Next
                                If mstrApproverIDs.Trim.Length > 0 Then
                                    mstrApproverIDs = mstrApproverIDs.Trim.Substring(0, mstrApproverIDs.Trim.Length - 1)
                                End If
                            End If

                        End If

                        dsList = Nothing


                        'Pinkal (15-Apr-2019) -- Start
                        'Enhancement - Working on Leave & CR Changes for NMB.[Due to Auto Approve/Reject from Arutireflex]

                        'StrQ = "SELECT cmexpense_master.name AS description " & _
                        '          ", ISNULL(cmclaim_approval_tran.quantity,0) AS Qty " & _
                        '          ", ISNULL(cmclaim_approval_tran.unitprice,0.00) AS unitcost " & _
                        '          ", ISNULL(cmclaim_approval_tran.amount,0.00) as amount " & _
                        '          ", ISNULL(cfcommon_master.name, '' ) AS sector " & _
                        '          ", ISNULL(cmclaim_approval_tran.expense_remark, '' ) AS expense_remark " & _
                        '          " FROM cmclaim_approval_tran " & _
                        '          " LEFT JOIN cmclaim_request_master ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 " & _
                        '          " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmexpense_master.expensetypeid = " & enExpenseType.EXP_LEAVE & _
                        '          " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_approval_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                        '          " WHERE cmclaim_request_master.expensetypeid = " & enExpenseType.EXP_LEAVE & " AND cmclaim_request_master.referenceunkid = @formunkid " & _
                        '          " AND cmclaim_approval_tran.crapproverunkid IN (" & mstrApproverIDs & ")  AND cmclaim_approval_tran.statusunkid = 1 AND cmclaim_approval_tran.isvoid = 0"

                        StrQ = "SELECT DISTINCT cmexpense_master.name AS description " & _
                                  ", ISNULL(cmclaim_approval_tran.quantity,0) AS Qty " & _
                                  ", ISNULL(cmclaim_approval_tran.unitprice,0.00) AS unitcost " & _
                                  ", ISNULL(cmclaim_approval_tran.amount,0.00) as amount " & _
                                  ", ISNULL(cfcommon_master.name, '' ) AS sector " & _
                                  ", ISNULL(cmclaim_approval_tran.expense_remark, '' ) AS expense_remark " & _
                                  " FROM cmclaim_approval_tran " & _
                                  " LEFT JOIN cmclaim_request_master ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 " & _
                                  " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmexpense_master.expensetypeid = " & enExpenseType.EXP_LEAVE & _
                                  " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_approval_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                                  " WHERE cmclaim_request_master.expensetypeid = " & enExpenseType.EXP_LEAVE & " AND cmclaim_request_master.referenceunkid = @formunkid " & _
                                  " AND cmclaim_approval_tran.statusunkid = 1 AND cmclaim_approval_tran.isvoid = 0"

                        'Pinkal (13-Jun-2019) -- Start
                        'Enhancement - NMB FINAL LEAVE UAT CHANGES.[Due to Setting change at NMB from simultaneous to parallel not able to display expense] 
                        If mstrApproverIDs.Trim.Length > 0 Then
                            StrQ &= " AND cmclaim_approval_tran.crapproverunkid IN (" & mstrApproverIDs & ")"
                        End If
                        'Pinkal (13-Jun-2019) -- End


                        'Pinkal (15-Apr-2019) -- End


                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveFormID)
                        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                        If objDataOperation.ErrorMessage <> "" Then
                            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                        End If

                    ElseIf CInt(dsList.Tables(0).Rows(0)("statusunkid")) = 6 Then  'Cancel
                        dsList = Nothing

                    End If

                End If

            End If

            Dim mdclTotalAmount As Decimal = 0

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_LeaveExpense.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column1") = dtRow.Item("description")
                    rpt_Row.Item("Column2") = Format(CDec(dtRow.Item("qty")), GUI.fmtCurrency)
                    rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("unitcost")), GUI.fmtCurrency)
                    rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency)
                    rpt_Row.Item("Column5") = dtRow.Item("sector").ToString()
                    rpt_Row.Item("Column6") = dtRow.Item("expense_remark").ToString()

                    mdclTotalAmount += CDec(dtRow.Item("amount"))
                    rpt_LeaveExpense.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

            End If

            Dim objDependents As New clsDependants_Beneficiary_tran
            Dim dsDependents As DataSet = objDependents.GetQualifiedDepedant(mintEmployeeId, False, True, mdtEndDate)

            For Each dtRow As DataRow In dsDependents.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Dependents.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("dependants")
                rpt_Row.Item("Column2") = dtRow.Item("gender")
                rpt_Row.Item("Column3") = dtRow.Item("age")
                rpt_Row.Item("Column4") = dtRow.Item("Months")
                rpt_Row.Item("Column5") = dtRow.Item("relation")
                rpt_Dependents.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            objRpt = New ArutiReport.Designer.rptEmployeeLeaveForm

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)

            If Company._Object._Address1.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtAddress1", Company._Object._Address1)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtAddress1", True)
            End If

            Dim mstrAddress As String = String.Empty

            If Company._Object._Address2.ToString.Trim <> "" Then
                mstrAddress = Company._Object._Address2.ToString.Trim
            End If

            If Company._Object._Post_Code_No.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._Post_Code_No.ToString.Trim
            End If

            If Company._Object._City_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._City_Name.ToString.Trim
            End If

            If Company._Object._State_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._State_Name.ToString.Trim
            End If

            If Company._Object._Country_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._Country_Name.ToString.Trim
            End If


            If mstrAddress.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtAddress2", mstrAddress)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtAddress2", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtLeaveApplicationNo", Language.getMessage(mstrModuleName, 1, "LEAVE APPLICATION NUMBER :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeParticulars", Language.getMessage(mstrModuleName, 2, "EMPLOYEE PARTICULARS"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveRecords", Language.getMessage(mstrModuleName, 3, "LEAVE RECORDS"))
            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 4, "Name :"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 5, "Emp Code :"))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 6, "Job Title :"))
            Call ReportFunction.TextChange(objRpt, "txtGrade", Language.getMessage(mstrModuleName, 7, "Grade :"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 9, "Dept :"))
            Call ReportFunction.TextChange(objRpt, "txtAppDate", Language.getMessage(mstrModuleName, 10, "Appt. Date :"))
            Call ReportFunction.TextChange(objRpt, "txtConfirmDate", Language.getMessage(mstrModuleName, 11, "Confirm. Date:"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveType", Language.getMessage(mstrModuleName, 12, "Leave Type :"))
            Call ReportFunction.TextChange(objRpt, "txtAccrueLstYr", Language.getMessage(mstrModuleName, 13, "Accrued UpTo Last Year :"))
            Call ReportFunction.TextChange(objRpt, "txtAccrueToDate", Language.getMessage(mstrModuleName, 14, "Accrued ToDate :"))
            Call ReportFunction.TextChange(objRpt, "txtIssueLstYr", Language.getMessage(mstrModuleName, 15, "Issued UpTo Last Year :"))
            Call ReportFunction.TextChange(objRpt, "txtIssueToDate", Language.getMessage(mstrModuleName, 16, "Total Issued ToDate :"))
            Call ReportFunction.TextChange(objRpt, "txtLastLeaveDates", Language.getMessage(mstrModuleName, 17, "Last Leave Dates :"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveBalance", Language.getMessage(mstrModuleName, 18, "Leave Balance :"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveApplicationDetail", Language.getMessage(mstrModuleName, 19, "LEAVE APPLICATION DETAILS"))
            Call ReportFunction.TextChange(objRpt, "txtApplyDate", Language.getMessage(mstrModuleName, 20, "Apply Date :"))
            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 21, "Start Date :"))
            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 22, "End Date :"))
            Call ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 23, "Address and Tel# While on Leave :"))
            Call ReportFunction.TextChange(objRpt, "txtComments", Language.getMessage(mstrModuleName, 24, "Remarks/Comments :"))
            Call ReportFunction.TextChange(objRpt, "txtTotalAdjustment", Language.getMessage(mstrModuleName, 64, "Total Adjustment :"))

            Call ReportFunction.TextChange(objRpt, "txtLeaveBF", Language.getMessage(mstrModuleName, 49, "Leave B/F :"))
            If ConfigParameter._Object._ShowBasicSalaryOnLeaveFormReport Then
                Call ReportFunction.TextChange(objRpt, "txtBasicSal", Language.getMessage(mstrModuleName, 46, "Basic Salary :"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column211", True)
            End If
            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 41, "APPROVED BY :"))
                Call ReportFunction.TextChange(objRpt, "lblHrManager", Language.getMessage(mstrModuleName, 42, "HR. MANAGER"))
                Call ReportFunction.TextChange(objRpt, "lblHOD", Language.getMessage(mstrModuleName, 43, "HEAD OF DEPARTMENT"))
                Call ReportFunction.TextChange(objRpt, "lblMd", Language.getMessage(mstrModuleName, 44, "MANAGING DIRECTOR"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If
            Call ReportFunction.TextChange(objRpt, "txtDaysRequsted", Language.getMessage(mstrModuleName, 45, "Days Requested : "))

            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                Call ReportFunction.EnableSuppress(objRpt, "txtELCStart", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtELCEnd", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column231", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column241", True)
            Else
                Call ReportFunction.TextChange(objRpt, "txtELCStart", Language.getMessage(mstrModuleName, 47, "ELC Start Date :"))
                Call ReportFunction.TextChange(objRpt, "txtELCEnd", Language.getMessage(mstrModuleName, 48, "ELC End Date :"))
            End If
            Call ReportFunction.TextChange(objRpt, "txtApprovedStDate", Language.getMessage(mstrModuleName, 50, "Approved Start Date : "))
            Call ReportFunction.TextChange(objRpt, "txtApprovedEdDate", Language.getMessage(mstrModuleName, 51, "Approved End Date : "))
            Call ReportFunction.TextChange(objRpt, "txtApprovedDays", Language.getMessage(mstrModuleName, 52, "Approved Days : "))
            Dim dsLvBal As DataSet = Nothing
            Dim dvLeaveBal As DataTable = Nothing
            Dim mdtAsonDate As Date = Nothing

            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                dsLvBal = objLeaveBal.GetList("LeaveBalance", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                            , eZeeDate.convertDate(mdtEmployeeAsonDate), xUserModeSetting, xOnlyApproved, False _
                                            , True, False, False, mintEmployeeId, False, False, False, "", Nothing)
                dvLeaveBal = New DataView(dsLvBal.Tables(0), "leavetypeunkid = " & mintLeaveTypeId, "", DataViewRowState.CurrentRows).ToTable
                If dvLeaveBal.Rows.Count > 0 Then
                    If Not dvLeaveBal.Rows(0)("enddate") Is DBNull.Value Then

                        If CDate(dvLeaveBal.Rows(0)("enddate")).Date >= mdtFinEndDate.Date Then
                            mdtAsonDate = mdtFinEndDate.Date.ToShortDateString
                        ElseIf CDate(dvLeaveBal.Rows(0)("enddate")).Date < mdtFinEndDate.Date Then
                            mdtAsonDate = CDate(dvLeaveBal.Rows(0)("enddate")).ToShortDateString
                        End If
                    Else
                        mdtAsonDate = mdtFinEndDate.Date.ToShortDateString
                    End If
                End If

            ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                dsLvBal = objLeaveBal.GetList("LeaveBalance", xDatabaseName, xUserModeSetting, xYearUnkid, xCompanyUnkid _
                                                            , eZeeDate.convertDate(mdtEmployeeAsonDate), xUserModeSetting, xOnlyApproved _
                                                          , False, True, False, False, mintEmployeeId, True, True, False, "", Nothing)

                dvLeaveBal = New DataView(dsLvBal.Tables(0), "leavetypeunkid = " & mintLeaveTypeId, "", DataViewRowState.CurrentRows).ToTable
                If dvLeaveBal.Rows.Count > 0 Then
                    If Not dvLeaveBal.Rows(0)("enddate") Is DBNull.Value Then
                        mdtAsonDate = CDate(dvLeaveBal.Rows(0)("enddate")).ToShortDateString
                    End If
                End If
            End If

            Call ReportFunction.TextChange(objRpt, "txtLeaveBaltilldate", Language.getMessage(mstrModuleName, 56, " Till") & Space(5) & mdtAsonDate)
            Call ReportFunction.TextChange(objRpt, "txtAsonDateLeaveBal", Language.getMessage(mstrModuleName, 103, "Leave Balance As on Date :"))

            objRpt.Subreports("rptLeaveApproverDetailReport").SetDataSource(rpt_LeaveApprovers)
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtLeaveApprovals", Language.getMessage(mstrModuleName, 25, "LEAVE APPROVALS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtLevelName", Language.getMessage(mstrModuleName, 26, "Level Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtApproverName", Language.getMessage(mstrModuleName, 27, "Approver Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtApproverTitle", Language.getMessage(mstrModuleName, 28, "Approver Title"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtApprovalDate", Language.getMessage(mstrModuleName, 29, "Approval Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtApprovalStatus", Language.getMessage(mstrModuleName, 30, "Approval Status"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtRemarks", Language.getMessage(mstrModuleName, 31, "Remarks/Comments"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtStartDate", Language.getMessage(mstrModuleName, 53, "Start Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtEndDate", Language.getMessage(mstrModuleName, 54, "End Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveApproverDetailReport"), "txtDay", Language.getMessage(mstrModuleName, 55, "Days"))

            objRpt.Subreports("rptEmployeeLeaveExpense").SetDataSource(rpt_LeaveExpense)
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txttotalAmount", Format(mdclTotalAmount, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtLeaveApprovals", Language.getMessage(mstrModuleName, 32, "APPROVED LEAVE EXPENSES AND TRAVEL ASSISTANCE"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtdescription", Language.getMessage(mstrModuleName, 33, "Description"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtQty", Language.getMessage(mstrModuleName, 34, "Qty"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtUnitRate", Language.getMessage(mstrModuleName, 35, "Unit Rate"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtAmount", Language.getMessage(mstrModuleName, 36, "Amount"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtTotal", Language.getMessage(mstrModuleName, 37, "Total :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtSector", Language.getMessage(mstrModuleName, 62, "Sector / Route"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveExpense"), "txtExpenseRemark", Language.getMessage(mstrModuleName, 63, "Expense Remark"))

            If rpt_LeaveExpense.Tables("ArutiTable").Rows.Count <= 0 Then
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection1", mblnDonotShowExpenseifNoExepense)
            End If

            If mblnIncludedependents Then
                objRpt.Subreports("rptEmployeeDepedents").SetDataSource(rpt_Dependents)
                Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeDepedents"), "txtName", Language.getMessage(mstrModuleName, 57, "Name"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeDepedents"), "txtGender", Language.getMessage(mstrModuleName, 58, "Gender"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeDepedents"), "txtAge", Language.getMessage(mstrModuleName, 59, "Age"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeDepedents"), "txtMonths", Language.getMessage(mstrModuleName, 60, "Month"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeDepedents"), "txtRelations", Language.getMessage(mstrModuleName, 61, "Relation"))
            Else

                ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", True)
            End If

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 38, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 39, "Printed Date :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)

            If Company._Object._Address1.Trim.Length <= 0 AndAlso mstrAddress.Trim.Length <= 0 Then
                objRpt.ReportDefinition.ReportObjects("txtReportName").Top = objRpt.ReportDefinition.ReportObjects("txtAddress1").Top
            End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Public Sub Send_ELeaveForm(ByVal StrFileName As String, ByVal StrFilePath As String)
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim strReportExportFile As String = ""
    '    Try
    '        objRpt = Generate_DetailReport()
    '        If Not IsNothing(objRpt) Then
    '            Dim oStream As New IO.MemoryStream
    '            oStream = objRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat)
    '            Dim fs As New System.IO.FileStream(StrFilePath & "\" & StrFileName.Replace(" ", "_") & ".pdf", IO.FileMode.Create, IO.FileAccess.Write)
    '            Dim data As Byte() = oStream.ToArray()
    '            fs.Write(data, 0, data.Length)
    '            fs.Close()
    '            fs.Dispose()
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Send_ELeaveForm; Module Name: " & mstrModuleName)
    '    Finally
    '        objRpt.Dispose()
    '    End Try
    'End Sub

    Public Sub Send_ELeaveForm(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String, ByVal StrFileName As String, ByVal StrFilePath As String)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objRpt = Generate_DetailReport()


            'Pinkal (10-Mar-2017) -- Start
            'Enhancement - Bug Solved For AKF Printing Wrong Company Name & User.
            mintCompanyUnkid = xCompanyUnkid
            Company._Object._Companyunkid = mintCompanyUnkid
            mintUserUnkid = xUserUnkid
            User._Object._Userunkid = mintUserUnkid
            'Pinkal (10-Mar-2017) -- End

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate, xOnlyApproved, xUserModeSetting)

            'Pinkal (24-Aug-2015) -- End

            If Not IsNothing(objRpt) Then
                Dim oStream As New IO.MemoryStream
                oStream = objRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat)
                Dim fs As New System.IO.FileStream(StrFilePath & "\" & StrFileName.Replace(" ", "_") & ".pdf", IO.FileMode.Create, IO.FileAccess.Write)
                Dim data As Byte() = oStream.ToArray()
                fs.Write(data, 0, data.Length)
                fs.Close()
                fs.Dispose()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_ELeaveForm; Module Name: " & mstrModuleName)
        Finally
            objRpt.Dispose()
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- End

    'Pinkal (07-Jul-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.

    Private Function Generate_SLLeaveFormDetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_Level1LeaveApprovers As ArutiReport.Designer.dsArutiReport
        Dim rpt_Level2LeaveApprovers As ArutiReport.Designer.dsArutiReport


        Try


            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = xCompanyUnkid

            objDataOperation.ClearParameters()

            StrQ = " SELECT  lvleaveform.employeeunkid " & _
                       ",lvleaveform.formunkid " & _
                       ",lvleaveform.leavetypeunkid " & _
                       ",employeecode " & _
                       ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employeename " & _
                       ",hrjob_master.job_name AS jobtitle " & _
                       ",hrgrade_master.name AS grade " & _
                       ",hrgradelevel_master.name AS gradelevel " & _
                       ",hrdepartment_master.name AS department " & _
                       ",hremployee_master.appointeddate " & _
                       ",hremployee_master.confirmation_date " & _
                       ",DATEDIFF(YEAR,Convert(char(8), hremployee_master.birthdate,112),Convert(char(8),GetDate(),112)) As Age " & _
                       ",ISNULL(hremployee_master.email,'') AS Email " & _
                       ",ISNULL(hremployee_master.present_mobile,'') AS present_mobile " & _
                       ",lvleaveform.formno " & _
                       ",lvleavetype_master.leavename " & _
                       ",lvleaveform.applydate " & _
                       ",lvleaveform.startdate " & _
                       ",lvleaveform.returndate " & _
                      ", lvleaveform.approve_stdate " & _
                      ", lvleaveform.approve_eddate " & _
                      ",lvleaveform.addressonleave " & _
                      ",lvleaveform.remark " & _
                      ",ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND lvleaveform.returndate AND lvleaveday_fraction.approverunkid <=0),0.00) days " & _
                      ", ISNULL(lvleaveform.approve_days,0) AS 	approve_days	  " & _
                      " FROM lvleaveform " & _
                      " JOIN hremployee_master ON lvleaveform.employeeunkid = hremployee_master.employeeunkid " & _
                      " JOIN " & _
                      " ( " & _
                      "         SELECT " & _
                      "         jobunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_categorization_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                      " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                      " JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                      " JOIN " & _
                      "( " & _
                      "    SELECT " & _
                      "         gradeunkid " & _
                      "         ,gradelevelunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                      "    FROM prsalaryincrement_tran " & _
                      "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                      ") AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                      " JOIN hrgrade_master ON hrgrade_master.gradeunkid = Grds.gradeunkid " & _
                      " JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = Grds.gradelevelunkid " & _
                      " JOIN " & _
                              " ( " & _
                                "    SELECT " & _
                                "         departmentunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                                " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                       " JOIN hrdepartment_master ON  hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                       " JOIN lvleavetype_master ON lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "         date1 " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "    FROM hremployee_dates_tran " & _
                       "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                       " )AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 " & _
                       " WHERE lvleaveform.isvoid = 0 "

            StrQ &= " AND lvleaveform.formunkid = " & mintLeaveFormID


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            rpt_Level1LeaveApprovers = New ArutiReport.Designer.dsArutiReport
            rpt_Level2LeaveApprovers = New ArutiReport.Designer.dsArutiReport

            Dim objLeaveBal As New clsleavebalance_tran
            Dim objLeaveForm As New clsleaveform
            Dim objMaster As New clsMasterData

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("formno")
                rpt_Rows.Item("Column2") = dtRow.Item("employeename")
                rpt_Rows.Item("Column3") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column4") = dtRow.Item("jobtitle")
                rpt_Rows.Item("Column5") = dtRow.Item("grade")
                rpt_Rows.Item("Column29") = dtRow.Item("gradelevel")
                rpt_Rows.Item("Column6") = dtRow.Item("department")

                If Not IsDBNull(dtRow.Item("appointeddate")) Then
                    rpt_Rows.Item("Column7") = CDate(dtRow.Item("appointeddate")).ToShortDateString
                Else
                    rpt_Rows.Item("Column7") = ""
                End If

                If Not IsDBNull(dtRow.Item("confirmation_date")) Then
                    rpt_Rows.Item("Column8") = CDate(dtRow.Item("confirmation_date")).ToShortDateString
                Else
                    rpt_Rows.Item("Column8") = ""
                End If

                rpt_Rows.Item("Column9") = dtRow.Item("leavename")
                rpt_Rows.Item("Column10") = dtRow.Item("Age").ToString()
                rpt_Rows.Item("Column11") = dtRow.Item("Email").ToString()
                rpt_Rows.Item("Column12") = dtRow.Item("present_mobile").ToString()

                If mintLeaveTypeId <= 0 Then mintLeaveTypeId = CInt(dtRow.Item("leavetypeunkid"))
                If mintEmployeeId <= 0 Then mintEmployeeId = CInt(dtRow.Item("employeeunkid"))
                Dim dsBalance As DataSet = Nothing

                If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    'Pinkal (28-Jan-2019) -- Start
                    'Bug - Due to Change In Get Balance Info Method report was showing 0 for all columns due to Pacra changes did by sohail for Leave Details.
                    'dsBalance = objLeaveBal.GetLeaveBalanceInfo(mdtEmployeeAsonDate.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtFinStartDate.Date, mdtFinEndDate.Date, False, False, mintLeaveBalanceSetting, 0, xYearUnkid)
                    dsBalance = objLeaveBal.GetLeaveBalanceInfo(mdtEmployeeAsonDate.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, mdtFinStartDate.Date, mdtFinEndDate.Date, False, False, mintLeaveBalanceSetting, -1, xYearUnkid)
                    'Pinkal (28-Jan-2019) -- End


                ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    objLeaveBal._DBStartdate = mdtdbStartDate
                    objLeaveBal._DBEnddate = mdtdbEndDate

                    'Pinkal (18-Nov-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                    'dsBalance = objLeaveBal.GetLeaveBalanceInfo(mdtEmployeeAsonDate.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")), Nothing, Nothing, True, True, mintLeaveBalanceSetting, 0, xYearUnkid)
                    dsBalance = objLeaveBal.GetLeaveBalanceInfo(mdtEmployeeAsonDate.Date, CInt(dtRow.Item("leavetypeunkid")), CInt(dtRow.Item("employeeunkid")), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True, mintLeaveBalanceSetting, 0, xYearUnkid)
                    'Pinkal (18-Nov-2016) -- End

                End If

                If dsBalance IsNot Nothing And dsBalance.Tables(0).Rows.Count > 0 Then
                    rpt_Rows.Item("Column13") = CDec(dsBalance.Tables(0).Rows(0)("LstyearAccrue_amount")).ToString("#0.00")
                    rpt_Rows.Item("Column14") = CDec(dsBalance.Tables(0).Rows(0)("Accrue_amount")).ToString("#0.00")
                    rpt_Rows.Item("Column15") = CDec(dsBalance.Tables(0).Rows(0)("LstyearIssue_amount")).ToString("#0.00")
                    rpt_Rows.Item("Column16") = CDec(dsBalance.Tables(0).Rows(0)("Issue_amount")).ToString("#0.00")
                    rpt_Rows.Item("Column17") = Format(CDec(dsBalance.Tables(0).Rows(0)("Balance")) + CDec(dtRow.Item("approve_days")), "#0.00")  'Leave Balance Before Issue current Leave Form
                    rpt_Rows.Item("Column18") = CDec(dsBalance.Tables(0).Rows(0)("LeaveBF")).ToString("#0.00")
                End If

                If Not IsDBNull(dtRow.Item("applydate")) Then
                    rpt_Rows.Item("Column19") = CDate(dtRow.Item("applydate")).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("startdate")) Then
                    rpt_Rows.Item("Column20") = CDate(dtRow.Item("startdate")).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("returndate")) Then
                    rpt_Rows.Item("Column21") = CDate(dtRow.Item("returndate")).ToShortDateString
                    mdtEndDate = CDate(dtRow.Item("returndate")).ToShortDateString
                End If

                rpt_Rows.Item("Column22") = dtRow.Item("addressonleave")
                rpt_Rows.Item("Column23") = dtRow.Item("remark")
                Dim dsScale As DataSet = objMaster.Get_Current_Scale("List", CInt(dtRow.Item("employeeunkid")), mdtEmployeeAsonDate.Date)

                If dsScale IsNot Nothing AndAlso dsScale.Tables(0).Rows.Count > 0 Then
                    rpt_Rows.Item("Column24") = objConfig._Currency & " " & Format(CDec(dsScale.Tables(0).Rows(0)("newscale")), GUI.fmtCurrency)
                Else
                    rpt_Rows.Item("Column24") = objConfig._Currency & " " & Format(0, GUI.fmtCurrency)
                End If

                rpt_Rows.Item("Column25") = CDec(dtRow.Item("days")).ToString("#0.00")


                If Not IsDBNull(dtRow.Item("approve_stdate")) Then
                    rpt_Rows.Item("Column26") = CDate(dtRow.Item("approve_stdate")).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("approve_eddate")) Then
                    rpt_Rows.Item("Column27") = CDate(dtRow.Item("approve_eddate")).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("approve_days")) Then
                    rpt_Rows.Item("Column28") = CDec(dtRow.Item("approve_days")).ToString("#0.00")
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next


            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrCommJoinQry, StrConditionQry As String

            StrInnerQry = "" : StrCommJoinQry = "" : StrConditionQry = ""

            StrQ = "SELECT DISTINCT " & _
                   "   ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "  ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "FROM lvpendingleave_tran " & _
                   "    JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   "WHERE lvpendingleave_tran.isvoid = 0 AND lvleaveapprover_master.isexternalapprover = 1 AND lvpendingleave_tran.formunkid = '" & mintLeaveFormID & "' "

            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrInnerQry = "SELECT lvleaveapprover_master.approverunkid  " & _
                      ",lvapproverlevel_master.levelunkid " & _
                      ",lvapproverlevel_master.levelname " & _
                    ",lvapproverlevel_master.priority " & _
                          "    ,#APPVR_NAME# approvername " & _
                          "    ,#APPR_JOB_NAME# AS approvertitle " & _
                      ",lvpendingleave_tran.approvaldate " & _
                    ",lvpendingleave_tran.startdate " & _
                    ",lvpendingleave_tran.enddate " & _
                    ",ISNULL(lvpendingleave_tran.dayfraction,0) AS dayfraction " & _
                    ",lvpendingleave_tran.statusunkid " & _
                      ",CASE WHEN lvpendingleave_tran.statusunkid = 1 THEN  @Approve " & _
                      "          WHEN lvpendingleave_tran.statusunkid = 2 THEN  @Pending " & _
                      "          WHEN lvpendingleave_tran.statusunkid = 3 THEN  @Reject " & _
                      "          WHEN lvpendingleave_tran.statusunkid = 4 THEN  @ReSchedule  " & _
                      "          WHEN lvpendingleave_tran.statusunkid = 6 THEN  @Cancel " & _
                      "          WHEN lvpendingleave_tran.statusunkid = 7 THEN @Issued " & _
                      " END status	 " & _
                      ", CASE WHEN lvpendingleave_tran.statusunkid = 1 THEN  lvpendingleave_tran.remark ELSE '' END AS Approval_Remark " & _
                      ", CASE WHEN lvpendingleave_tran.statusunkid = 3 OR lvpendingleave_tran.statusunkid = 4 THEN  lvpendingleave_tran.remark ELSE '' END AS Reject_Remark " & _
                      " FROM lvpendingleave_tran " & _
                      " JOIN lvleaveform ON lvpendingleave_tran.formunkid = lvleaveform.formunkid AND lvleaveform.isvoid = 0 AND lvleaveform.formunkid = '" & mintLeaveFormID & "'" & _
                      " JOIN lvleaveapprover_master ON lvpendingleave_tran.approvertranunkid = lvleaveapprover_master.approverunkid " & _
                      " JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid = lvapproverlevel_master.levelunkid " & _
                          " #COMM_JOIN# "

            StrConditionQry = " WHERE lvpendingleave_tran.isvoid = 0 AND lvleaveapprover_master.isexternalapprover = #ExApprId# "

            If mintEmployeeId > 0 Then
                StrConditionQry &= " AND lvpendingleave_tran.employeeunkid = " & mintEmployeeId
            End If

            If mintLeaveTypeId > 0 Then
                StrConditionQry &= " AND lvleaveform.leavetypeunkid = " & mintLeaveTypeId
            End If


            StrQ = StrInnerQry
            StrQ &= StrConditionQry
            StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")
            StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON lvleaveapprover_master.leaveapproverunkid = hremployee_master.employeeunkid " & _
                                               " JOIN #DB_Name#hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid ")
            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#ExApprId#", "0")
            StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
            StrQ &= " ORDER BY lvapproverlevel_master.priority "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry

                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPR_JOB_NAME#", "'' ")
                    StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(UEmp.username,'') ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvpendingleave_tran.approverunkid ")
                Else
                    StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
                    StrQ = StrQ.Replace("#APPVR_NAME#", " CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvpendingleave_tran.approverunkid " & _
                                                       " JOIN #DB_Name#hremployee_master ON UEmp.employeeunkid = hremployee_master.employeeunkid " & _
                                                       " JOIN #DB_Name#hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid ")
                End If


                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DB_Name#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DB_Name#", "")
                End If

                StrQ = StrQ.Replace("#ExApprId#", "1")
                StrQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next


            Dim mintPriorty As Integer = -1

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
Recalculate:
                Dim mintPrioriry As Integer = 0
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If mintPrioriry <> CInt(dsList.Tables(0).Rows(i)("priority")) Then
                        mintPrioriry = CInt(dsList.Tables(0).Rows(i)("priority"))
                        Dim drRow() As DataRow = dsList.Tables(0).Select("statusunkid <> 2 AND priority  = " & mintPrioriry)
                        If drRow.Length > 0 Then
                            Dim drPending() As DataRow = dsList.Tables(0).Select("statusunkid = 2 AND priority  = " & mintPrioriry)
                            If drPending.Length > 0 Then
                                For Each dRow As DataRow In drPending
                                    dsList.Tables(0).Rows.Remove(dRow)
                                    GoTo Recalculate
                                Next
                                dsList.AcceptChanges()
                            End If
                        End If


                    End If

                Next
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Level1LeaveApprovers.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("levelname")
                rpt_Row.Item("Column2") = dtRow.Item("approvername")
                rpt_Row.Item("Column3") = dtRow.Item("approvertitle")

                If Not IsDBNull(dtRow.Item("approvaldate")) Then
                    rpt_Row.Item("Column4") = CDate(dtRow.Item("approvaldate")).ToShortDateString
                Else
                    rpt_Row.Item("Column4") = ""
                End If
                rpt_Row.Item("Column5") = dtRow.Item("status")
                rpt_Row.Item("Column6") = dtRow.Item("Approval_Remark").ToString()
                If Not IsDBNull(dtRow.Item("startdate")) Then
                    rpt_Row.Item("Column7") = CDate(dtRow.Item("startdate")).ToShortDateString
                Else
                    rpt_Row.Item("Column7") = ""
                End If

                If Not IsDBNull(dtRow.Item("enddate")) Then
                    rpt_Row.Item("Column8") = CDate(dtRow.Item("enddate")).ToShortDateString
                Else
                    rpt_Row.Item("Column8") = ""
                End If

                If Not IsDBNull(dtRow.Item("dayfraction")) Then
                    rpt_Row.Item("Column9") = CDec((dtRow.Item("dayfraction"))).ToString("#0.00")
                End If

                rpt_Row.Item("Column10") = dtRow.Item("Reject_Remark").ToString()

                rpt_Row.Item("Column11") = dtRow.Item("priority").ToString()
                rpt_Row.Item("Column12") = dtRow.Item("statusunkid").ToString()

                rpt_Level1LeaveApprovers.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            If rpt_Level1LeaveApprovers IsNot Nothing Then
                Dim dtLevel1 As DataTable = Nothing
                rpt_Level2LeaveApprovers = rpt_Level1LeaveApprovers.Copy
                dtLevel1 = New DataView(rpt_Level1LeaveApprovers.Tables(0), "Column11 = MIN(Column11) AND (Column12 =1 OR Column12 = 7)", "", DataViewRowState.CurrentRows).ToTable
                If dtLevel1 IsNot Nothing AndAlso dtLevel1.Rows.Count <= 0 Then
                    dtLevel1 = New DataView(rpt_Level1LeaveApprovers.Tables(0), "Column11 = MIN(Column11) AND Column12 =2", "", DataViewRowState.CurrentRows).ToTable
                End If
                rpt_Level1LeaveApprovers.Tables.RemoveAt(0)
                rpt_Level1LeaveApprovers.Tables.Add(dtLevel1.Copy)
                dtLevel1.Clear()
                dtLevel1 = Nothing

                Dim dtLevel2 As DataTable = Nothing
                dtLevel2 = New DataView(rpt_Level2LeaveApprovers.Tables(0), "Column11 = MAX(Column11) AND (Column12 <>  2 AND Column12 <> 6)", "", DataViewRowState.CurrentRows).ToTable
                If dtLevel2 IsNot Nothing AndAlso dtLevel2.Rows.Count <= 0 Then
                    dtLevel2 = New DataView(rpt_Level2LeaveApprovers.Tables(0), "Column11 = MAX(Column11) AND Column12 =2", "", DataViewRowState.CurrentRows).ToTable
                End If
                rpt_Level2LeaveApprovers.Tables.RemoveAt(0)
                rpt_Level2LeaveApprovers.Tables.Add(dtLevel2.Copy)
                dtLevel2.Clear()
                dtLevel2 = Nothing
            End If

            dsList = Nothing

            objConfig = Nothing

            objRpt = New ArutiReport.Designer.rptSLEmpLeaveForm

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        False, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)

            'If Company._Object._Address1.Trim.Length > 0 Then
            '    Call ReportFunction.TextChange(objRpt, "txtAddress1", Company._Object._Address1)
            'Else
            '    Call ReportFunction.EnableSuppress(objRpt, "txtAddress1", True)
            'End If

            'Dim mstrAddress As String = String.Empty

            'If Company._Object._Address2.ToString.Trim <> "" Then
            '    mstrAddress = Company._Object._Address2.ToString.Trim
            'End If

            'If Company._Object._Post_Code_No.ToString.Trim <> "" Then
            '    mstrAddress &= " , " & Company._Object._Post_Code_No.ToString.Trim
            'End If

            'If Company._Object._City_Name.ToString.Trim <> "" Then
            '    mstrAddress &= " , " & Company._Object._City_Name.ToString.Trim
            'End If

            'If Company._Object._State_Name.ToString.Trim <> "" Then
            '    mstrAddress &= " , " & Company._Object._State_Name.ToString.Trim
            'End If

            'If Company._Object._Country_Name.ToString.Trim <> "" Then
            '    mstrAddress &= " , " & Company._Object._Country_Name.ToString.Trim
            'End If


            'If mstrAddress.Trim.Length > 0 Then
            '    Call ReportFunction.TextChange(objRpt, "txtAddress2", mstrAddress)
            'Else
            '    Call ReportFunction.EnableSuppress(objRpt, "txtAddress2", True)
            'End If


            Call ReportFunction.TextChange(objRpt, "txtAppForVacationLeave", Language.getMessage(mstrModuleName, 104, "APPLICATION FOR VACATION LEAVE"))
            Call ReportFunction.TextChange(objRpt, "txtStaffChanges", Language.getMessage(mstrModuleName, 65, "Form to be completed in Triplicate and forwarded through the Head of Department to the Director-General, HRMO who will keep the duplicate and returned the original and triplicate to the officer’s Department. If approved, original to be attached to Amendment Form for staff changes."))
            Call ReportFunction.TextChange(objRpt, "txtOfficers", Language.getMessage(mstrModuleName, 66, "To be completed by Officers below Grade 11"))
            Call ReportFunction.TextChange(objRpt, "txtNameofOfficers", Language.getMessage(mstrModuleName, 67, "Name of Officer"))
            Call ReportFunction.TextChange(objRpt, "txtAge", Language.getMessage(mstrModuleName, 68, "Age"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 69, "Ministry/Department/Agency"))
            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 70, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtAppointment", Language.getMessage(mstrModuleName, 71, "Appointment and present rank"))
            Call ReportFunction.TextChange(objRpt, "txtPresentSalary", Language.getMessage(mstrModuleName, 72, "Present Salary"))
            Call ReportFunction.TextChange(objRpt, "txtWhether", Language.getMessage(mstrModuleName, 73, "Whether (a) confirmed, or (b) on contract or (c) on probation (N.B.  In case of (b) and (c) date of termination of contract or probationary period is to be given)."))
            Call ReportFunction.TextChange(objRpt, "txtassumption", Language.getMessage(mstrModuleName, 74, "Expected date of assumption or resumption of duty"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveDue", Language.getMessage(mstrModuleName, 75, "Amount of leave due on present tour"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveClaimed", Language.getMessage(mstrModuleName, 76, "Amount of deferred leave claimed in respect of previous tours"))
            Call ReportFunction.TextChange(objRpt, "txtAuthority", Language.getMessage(mstrModuleName, 77, "(Quote authority for deferring leave)"))
            Call ReportFunction.TextChange(objRpt, "txtLeavePeriod", Language.getMessage(mstrModuleName, 78, "Period of leave now applied for"))
            Call ReportFunction.TextChange(objRpt, "txtProceedingLeave", Language.getMessage(mstrModuleName, 79, "Proposed date of proceeding on leave"))
            Call ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 80, "Address on leave"))
            Call ReportFunction.TextChange(objRpt, "txtMobile", Language.getMessage(mstrModuleName, 81, "Mobile"))
            Call ReportFunction.TextChange(objRpt, "txtEmail", Language.getMessage(mstrModuleName, 82, "Email"))
            Call ReportFunction.TextChange(objRpt, "txtSignatureApplicant", Language.getMessage(mstrModuleName, 83, "Signature of Applicant"))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 84, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 100, "Days."))
            Call ReportFunction.TextChange(objRpt, "txtSalaryGrade", Language.getMessage(mstrModuleName, 101, "Salary Grade"))
            Call ReportFunction.TextChange(objRpt, "txtIncrementDate", Language.getMessage(mstrModuleName, 102, "Incremental Date"))


            objRpt.Subreports("rptLevel1ApproverDetails").SetDataSource(rpt_Level1LeaveApprovers)
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel1ApproverDetails"), "txtHOD", Language.getMessage(mstrModuleName, 85, "To be completed by Head of Department"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel1ApproverDetails"), "txtRecommended", Language.getMessage(mstrModuleName, 86, "Recommended"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel1ApproverDetails"), "txtLeaveGranted", Language.getMessage(mstrModuleName, 87, "(Please state period of leave to be granted)"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel1ApproverDetails"), "txtPropose", Language.getMessage(mstrModuleName, 88, "I propose/do not propose to recommend the officer named below to act in the post while the substantive holder is on leave."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel1ApproverDetails"), "txtMr", Language.getMessage(mstrModuleName, 89, "Mr./Mrs./Miss"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel1ApproverDetails"), "txtServingContract", Language.getMessage(mstrModuleName, 90, "This officer is serving on Contract For"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel1ApproverDetails"), "txtTours", Language.getMessage(mstrModuleName, 91, "tour(s)."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel1ApproverDetails"), "txtExpires", Language.getMessage(mstrModuleName, 92, "Which expires on"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel1ApproverDetails"), "txtApprovalDate", Language.getMessage(mstrModuleName, 84, "Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel1ApproverDetails"), "txtsignatureHOD", Language.getMessage(mstrModuleName, 93, "Signature of Permanent Secretary / Head of Department"))


            objRpt.Subreports("rptLevel2ApproverDetails").SetDataSource(rpt_Level2LeaveApprovers)
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel2ApproverDetails"), "txtHRMOffice", Language.getMessage(mstrModuleName, 94, "Human Resource Management Office"))
            'Call ReportFunction.TextChange(objRpt.Subreports("rptLevel2ApproverDetails"), "txtApprovedNotApproved", Language.getMessage(mstrModuleName, 95, "Approved/Not approved (select)."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel2ApproverDetails"), "txtName", Language.getMessage(mstrModuleName, 96, "Name:"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel2ApproverDetails"), "txtSignature", Language.getMessage(mstrModuleName, 97, "Signature:"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel2ApproverDetails"), "txtHRMDate", Language.getMessage(mstrModuleName, 84, "Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel2ApproverDetails"), "txtDGHRMO", Language.getMessage(mstrModuleName, 98, "D.G (HRMO)"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLevel2ApproverDetails"), "txtReasonIfNotApproved", Language.getMessage(mstrModuleName, 99, "Reason/s if not approved"))


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 38, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 39, "Printed Date :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)

            'If Company._Object._Address1.Trim.Length <= 0 AndAlso mstrAddress.Trim.Length <= 0 Then
            '    objRpt.ReportDefinition.ReportObjects("txtReportName"). = objRpt.ReportDefinition.ReportObjects("txtAddress1").
            'End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_SLLeaveFormDetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (07-Jul-2016) -- End


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 113, "Re-Scheduled")
            Language.setMessage("clsMasterData", 115, "Cancelled")
            Language.setMessage("clsMasterData", 276, "Issued")
            Language.setMessage(mstrModuleName, 1, "LEAVE APPLICATION NUMBER :")
            Language.setMessage(mstrModuleName, 2, "EMPLOYEE PARTICULARS")
            Language.setMessage(mstrModuleName, 3, "LEAVE RECORDS")
            Language.setMessage(mstrModuleName, 4, "Name :")
            Language.setMessage(mstrModuleName, 5, "Emp Code :")
            Language.setMessage(mstrModuleName, 6, "Job Title :")
            Language.setMessage(mstrModuleName, 7, "Grade :")
            Language.setMessage(mstrModuleName, 9, "Dept :")
            Language.setMessage(mstrModuleName, 10, "Appt. Date :")
            Language.setMessage(mstrModuleName, 11, "Confirm. Date:")
            Language.setMessage(mstrModuleName, 12, "Leave Type :")
            Language.setMessage(mstrModuleName, 13, "Accrued UpTo Last Year :")
            Language.setMessage(mstrModuleName, 14, "Accrued ToDate :")
            Language.setMessage(mstrModuleName, 15, "Issued UpTo Last Year :")
            Language.setMessage(mstrModuleName, 16, "Total Issued ToDate :")
            Language.setMessage(mstrModuleName, 17, "Last Leave Dates :")
            Language.setMessage(mstrModuleName, 18, "Leave Balance :")
            Language.setMessage(mstrModuleName, 19, "LEAVE APPLICATION DETAILS")
            Language.setMessage(mstrModuleName, 20, "Apply Date :")
            Language.setMessage(mstrModuleName, 21, "Start Date :")
            Language.setMessage(mstrModuleName, 22, "End Date :")
            Language.setMessage(mstrModuleName, 23, "Address and Tel# While on Leave :")
            Language.setMessage(mstrModuleName, 24, "Remarks/Comments :")
            Language.setMessage(mstrModuleName, 25, "LEAVE APPROVALS")
            Language.setMessage(mstrModuleName, 26, "Level Name")
            Language.setMessage(mstrModuleName, 27, "Approver Name")
            Language.setMessage(mstrModuleName, 28, "Approver Title")
            Language.setMessage(mstrModuleName, 29, "Approval Date")
            Language.setMessage(mstrModuleName, 30, "Approval Status")
            Language.setMessage(mstrModuleName, 31, "Remarks/Comments")
            Language.setMessage(mstrModuleName, 32, "APPROVED LEAVE EXPENSES AND TRAVEL ASSISTANCE")
            Language.setMessage(mstrModuleName, 33, "Description")
            Language.setMessage(mstrModuleName, 34, "Qty")
            Language.setMessage(mstrModuleName, 35, "Unit Rate")
            Language.setMessage(mstrModuleName, 36, "Amount")
            Language.setMessage(mstrModuleName, 37, "Total :")
            Language.setMessage(mstrModuleName, 38, "Printed By :")
            Language.setMessage(mstrModuleName, 39, "Printed Date :")
            Language.setMessage(mstrModuleName, 40, " To")
            Language.setMessage(mstrModuleName, 41, "APPROVED BY :")
            Language.setMessage(mstrModuleName, 42, "HR. MANAGER")
            Language.setMessage(mstrModuleName, 43, "HEAD OF DEPARTMENT")
            Language.setMessage(mstrModuleName, 44, "MANAGING DIRECTOR")
            Language.setMessage(mstrModuleName, 45, "Days Requested :")
            Language.setMessage(mstrModuleName, 46, "Basic Salary :")
            Language.setMessage(mstrModuleName, 47, "ELC Start Date :")
            Language.setMessage(mstrModuleName, 48, "ELC End Date :")
            Language.setMessage(mstrModuleName, 49, "Leave B/F :")
            Language.setMessage(mstrModuleName, 50, "Approved Start Date :")
            Language.setMessage(mstrModuleName, 51, "Approved End Date :")
            Language.setMessage(mstrModuleName, 52, "Approved Days :")
            Language.setMessage(mstrModuleName, 53, "Start Date")
            Language.setMessage(mstrModuleName, 54, "End Date")
            Language.setMessage(mstrModuleName, 55, "Days")
            Language.setMessage(mstrModuleName, 56, " Till")
            Language.setMessage(mstrModuleName, 57, "Name")
            Language.setMessage(mstrModuleName, 58, "Gender")
            Language.setMessage(mstrModuleName, 59, "Age")
            Language.setMessage(mstrModuleName, 60, "Month")
            Language.setMessage(mstrModuleName, 61, "Relation")
            Language.setMessage(mstrModuleName, 62, "Sector / Route")
            Language.setMessage(mstrModuleName, 63, "Expense Remark")
            Language.setMessage(mstrModuleName, 64, "Total Adjustment :")
            Language.setMessage(mstrModuleName, 65, "Form to be completed in Triplicate and forwarded through the Head of Department to the Director-General, HRMO who will keep the duplicate and returned the original and triplicate to the officer’s Department. If approved, original to be attached to Amendment Form for staff changes.")
            Language.setMessage(mstrModuleName, 66, "To be completed by Officers below Grade 11")
            Language.setMessage(mstrModuleName, 67, "Name of Officer")
            Language.setMessage(mstrModuleName, 68, "Age")
            Language.setMessage(mstrModuleName, 69, "Ministry/Department/Agency")
            Language.setMessage(mstrModuleName, 70, "Code")
            Language.setMessage(mstrModuleName, 71, "Appointment and present rank")
            Language.setMessage(mstrModuleName, 72, "Present Salary")
            Language.setMessage(mstrModuleName, 73, "Whether (a) confirmed, or (b) on contract or (c) on probation (N.B.  In case of (b) and (c) date of termination of contract or probationary period is to be given).")
            Language.setMessage(mstrModuleName, 74, "Expected date of assumption or resumption of duty")
            Language.setMessage(mstrModuleName, 75, "Amount of leave due on present tour")
            Language.setMessage(mstrModuleName, 76, "Amount of deferred leave claimed in respect of previous tours")
            Language.setMessage(mstrModuleName, 77, "(Quote authority for deferring leave)")
            Language.setMessage(mstrModuleName, 78, "Period of leave now applied for")
            Language.setMessage(mstrModuleName, 79, "Proposed date of proceeding on leave")
            Language.setMessage(mstrModuleName, 80, "Address on leave")
            Language.setMessage(mstrModuleName, 81, "Mobile")
            Language.setMessage(mstrModuleName, 82, "Email")
            Language.setMessage(mstrModuleName, 83, "Signature of Applicant")
            Language.setMessage(mstrModuleName, 84, "Date")
            Language.setMessage(mstrModuleName, 85, "To be completed by Head of Department")
            Language.setMessage(mstrModuleName, 86, "Recommended")
            Language.setMessage(mstrModuleName, 87, "(Please state period of leave to be granted)")
            Language.setMessage(mstrModuleName, 88, "I propose/do not propose to recommend the officer named below to act in the post while the substantive holder is on leave.")
            Language.setMessage(mstrModuleName, 89, "Mr./Mrs./Miss")
            Language.setMessage(mstrModuleName, 90, "This officer is serving on Contract For")
            Language.setMessage(mstrModuleName, 91, "tour(s).")
            Language.setMessage(mstrModuleName, 92, "Which expires on")
            Language.setMessage(mstrModuleName, 93, "Signature of Permanent Secretary / Head of Department")
            Language.setMessage(mstrModuleName, 94, "Human Resource Management Office")
            Language.setMessage(mstrModuleName, 96, "Name:")
            Language.setMessage(mstrModuleName, 97, "Signature:")
            Language.setMessage(mstrModuleName, 98, "D.G (HRMO)")
            Language.setMessage(mstrModuleName, 99, "Reason/s if not approved")
            Language.setMessage(mstrModuleName, 100, "Days.")
            Language.setMessage(mstrModuleName, 101, "Salary Grade")
            Language.setMessage(mstrModuleName, 102, "Incremental Date")
            Language.setMessage(mstrModuleName, 103, "Leave Balance As on Date :")
            Language.setMessage(mstrModuleName, 104, "APPLICATION FOR VACATION LEAVE")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
