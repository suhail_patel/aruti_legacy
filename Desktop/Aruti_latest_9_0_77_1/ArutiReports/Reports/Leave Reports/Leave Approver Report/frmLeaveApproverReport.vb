'************************************************************************************************************************************
'Class Name : frmLeaveApproverReport.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************


#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmLeaveApproverReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmLeaveApproverReport"
    Private objLeaveApprover As clsLeaveApproverReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Constructor "

    Public Sub New()
        objLeaveApprover = New clsLeaveApproverReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objLeaveApprover.SetDefaultValue()
        InitializeComponent()
        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objapprover As New clsleaveapprover_master
            Dim objLevel As New clsapproverlevel_master
            Dim objLeaveType As New clsleavetype_master
            Dim dsList As New DataSet


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'dsList = objapprover.GetList("Approver", True)
            dsList = objapprover.GetList("Approver", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                    , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                    , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, False, -1, Nothing, "", "")
            'Pinkal (24-Aug-2015) -- End


            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("leaveapproverunkid") = 0
            drRow("name") = Language.getMessage(mstrModuleName, 1, "Select")
            'S.SANDEEP [30 JAN 2016] -- START
            drRow("isexternalapprover") = False
            'S.SANDEEP [30 JAN 2016] -- END
            dsList.Tables(0).Rows.InsertAt(drRow, 0)

            'S.SANDEEP [30 JAN 2016] -- START
            'Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "leaveapproverunkid", "name")
            Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")
            'S.SANDEEP [30 JAN 2016] -- END


            With cboApprover
                .ValueMember = "leaveapproverunkid"
                .DisplayMember = "name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
            objapprover = Nothing

            dsList = objLevel.getListForCombo("Level", True)
            With cboLevel
                .ValueMember = "levelunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Level")
                .SelectedValue = 0
            End With
            objLevel = Nothing

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            dsList = objLeaveType.getListForCombo("LeaveType", True)
            With cboLeaveType
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("LeaveType")
                .SelectedValue = 0
            End With
            objLeaveType = Nothing

            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 3, "Leave Approver Report"))

            'Pinkal (25-Apr-2014) -- Start
            'Enhancement : TRA Changes
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 5, "Employee List with Their Leave Approver Report "))
            'Pinkal (25-Apr-2014) -- End

            If ConfigParameter._Object._IsLeaveApprover_ForLeaveType Then
                cboReportType.Items.Add(Language.getMessage(mstrModuleName, 4, "Employee Wise Leave Approver Report"))
            End If
            cboReportType.SelectedIndex = 0

            dsList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objLeaveApprover.SetDefaultValue()
            objLeaveApprover._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objLeaveApprover._ReportName = cboReportType.Text
            objLeaveApprover._ApproverID = CInt(cboApprover.SelectedValue)
            objLeaveApprover._ApproverName = cboApprover.Text
            objLeaveApprover._LevelID = CInt(cboLevel.SelectedValue)
            objLeaveApprover._LevelName = cboLevel.Text
            objLeaveApprover._EmployeeID = CInt(cboEmployee.SelectedValue)
            objLeaveApprover._EmployeeName = cboEmployee.Text
            objLeaveApprover._LeaveTypeID = CInt(cboLeaveType.SelectedValue)
            objLeaveApprover._LeaveTypeName = cboLeaveType.Text
            objLeaveApprover._ViewByIds = mstrStringIds
            objLeaveApprover._ViewIndex = mintViewIdx
            objLeaveApprover._ViewByName = mstrStringName
            objLeaveApprover._Analysis_Fields = mstrAnalysis_Fields
            objLeaveApprover._Analysis_Join = mstrAnalysis_Join
            objLeaveApprover._Analysis_OrderBy = mstrAnalysis_OrderBy
            objLeaveApprover._Report_GroupName = mstrReport_GroupName
            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objLeaveApprover._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objLeaveApprover.setDefaultOrderBy(0)
            txtOrderBy.Text = objLeaveApprover.OrderByDisplay
            cboApprover.SelectedIndex = 0
            cboLevel.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboLeaveType.SelectedIndex = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmLeaveApproverReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objLeaveApprover = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveApproverReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveApproverReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Me._Title = objLeaveApprover._ReportName
            Me._Message = objLeaveApprover._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveApproverReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveApproverReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmLeaveApproverReport_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnSearchApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApprover.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboApprover
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboLevel
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeaveType.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboLeaveType
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeaveType_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveApproverReport_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If CInt(cboReportType.SelectedIndex) <= 0 Then

                If CInt(cboApprover.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Leave Approver is mandatory information.Please Select Leave Approver."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    cboApprover.Select()
                    Exit Sub
                End If

                'Pinkal (25-Apr-2014) -- Start
                'Enhancement : TRA Changes AS PER DENNIS REQUIREMENT GIVEN BY TRA

                'ElseIf CInt(cboReportType.SelectedIndex) > 0 Then

            ElseIf CInt(cboReportType.SelectedIndex) > 1 Then

                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employee is mandatory information.Please Select Employee."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    cboEmployee.Select()
                    Exit Sub
                End If

                'Pinkal (25-Apr-2014) -- End

            End If

            If SetFilter() = False Then Exit Sub


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objLeaveApprover.generateReport(0, e.Type, enExportAction.None)
            objLeaveApprover.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                   , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                   , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                                   , 0, e.Type, enExportAction.None, -1)
            'Pinkal (24-Aug-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveApproverReport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveApproverReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If CInt(cboReportType.SelectedIndex) <= 0 Then

            If CInt(cboApprover.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Leave Approver is mandatory information.Please Select Leave Approver."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboApprover.Select()
                Exit Sub
            End If

                'Pinkal (25-Apr-2014) -- Start
                'Enhancement : TRA Changes AS PER DENNIS REQUIREMENT GIVEN BY TRA

                'ElseIf CInt(cboReportType.SelectedIndex) > 0 Then

            ElseIf CInt(cboReportType.SelectedIndex) > 1 Then

                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employee is mandatory information.Please Select Employee."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    cboEmployee.Select()
                    Exit Sub
                End If

            End If

            If SetFilter() = False Then Exit Sub

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objLeaveApprover.generateReport(0, enPrintAction.None, e.Type)
            objLeaveApprover.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                   , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                   , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                                   , 0, enPrintAction.None, e.Type, -1)
            'Pinkal (24-Aug-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveApproverReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveApproverReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveApproverReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveApproverReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveApproverReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLeaveApproverReport.SetMessages()
            objfrm._Other_ModuleNames = "clsLeaveApproverReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region "Control"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try

            'Pinkal (25-Apr-2014) -- Start
            'Enhancement : TRA Changes
            'objLeaveApprover.setOrderBy(0)
            objLeaveApprover.setOrderBy(CInt(cboReportType.SelectedIndex))
            'Pinkal (25-Apr-2014) -- End
            txtOrderBy.Text = objLeaveApprover.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "LinkLabel Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "ComboBox Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try


            'Pinkal (25-Apr-2014) -- Start
            'Enhancement : TRA Changes

            If cboReportType.SelectedIndex <= 1 Then
                'If ConfigParameter._Object._IsLeaveApprover_ForLeaveType Then
                '    cboEmployee.SelectedIndex = 0
                '    cboEmployee.Enabled = False
                '    objbtnSearchEmployee.Enabled = False
                'End If
                cboApprover.SelectedIndex = 0
                cboApprover.Enabled = True
                objbtnSearchApprover.Enabled = True
                cboLevel.SelectedIndex = 0
                cboLevel.Enabled = True
                objbtnSearchLevel.Enabled = True
                gbSortBy.Visible = True
                cboLeaveType.Enabled = False
                objbtnSearchLeaveType.Enabled = False

            Else
                cboLeaveType.Enabled = True
                objbtnSearchLeaveType.Enabled = True
                cboEmployee.SelectedIndex = 0
                cboEmployee.Enabled = True
                objbtnSearchEmployee.Enabled = True
                gbSortBy.Visible = False
            End If


            If CInt(cboReportType.SelectedIndex = 1) Then
                lnkAnalysisBy.Visible = True
            Else
                lnkAnalysisBy.Visible = False
            End If

            'S.SANDEEP [30 JAN 2016] -- START
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            'S.SANDEEP [30 JAN 2016] -- END

            'Pinkal (25-Apr-2014) -- End

            cboLeaveType.SelectedIndex = 0
            objLeaveApprover.setDefaultOrderBy(CInt(cboReportType.SelectedIndex))
            txtOrderBy.Text = objLeaveApprover.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
			Me.LblLevel.Text = Language._Object.getCaption(Me.LblLevel.Name, Me.LblLevel.Text)
			Me.LblLeaveType.Text = Language._Object.getCaption(Me.LblLeaveType.Name, Me.LblLeaveType.Text)
			Me.LblReportType.Text = Language._Object.getCaption(Me.LblReportType.Name, Me.LblReportType.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Leave Approver is mandatory information.Please Select Leave Approver.")
			Language.setMessage(mstrModuleName, 3, "Leave Approver Report")
			Language.setMessage(mstrModuleName, 4, "Employee Wise Leave Approver Report")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

   
End Class
