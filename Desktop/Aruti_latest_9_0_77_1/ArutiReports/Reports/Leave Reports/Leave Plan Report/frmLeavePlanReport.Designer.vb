﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeavePlanReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchJob = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.LblEmployee = New System.Windows.Forms.Label
        Me.LblJob = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchLeaveType = New eZee.Common.eZeeGradientButton
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblLeaveTodate = New System.Windows.Forms.Label
        Me.dtpLeaveToDate = New System.Windows.Forms.DateTimePicker
        Me.lblLeaveFromDate = New System.Windows.Forms.Label
        Me.dtpLeaveFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblLeaveName = New System.Windows.Forms.Label
        Me.cboLeave = New System.Windows.Forms.ComboBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 512)
        Me.NavPanel.Size = New System.Drawing.Size(767, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAnalysisBy)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchJob)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.LblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.LblJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveTodate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpLeaveToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpLeaveFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveName)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeave)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(383, 142)
        Me.gbFilterCriteria.TabIndex = 16
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(287, 4)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 91
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchJob
        '
        Me.objbtnSearchJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJob.BorderSelected = False
        Me.objbtnSearchJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJob.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJob.Location = New System.Drawing.Point(355, 88)
        Me.objbtnSearchJob.Name = "objbtnSearchJob"
        Me.objbtnSearchJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJob.TabIndex = 208
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(355, 62)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 205
        '
        'LblEmployee
        '
        Me.LblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.LblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployee.Location = New System.Drawing.Point(8, 65)
        Me.LblEmployee.Name = "LblEmployee"
        Me.LblEmployee.Size = New System.Drawing.Size(74, 13)
        Me.LblEmployee.TabIndex = 206
        Me.LblEmployee.Text = "Employee"
        '
        'LblJob
        '
        Me.LblJob.BackColor = System.Drawing.Color.Transparent
        Me.LblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblJob.Location = New System.Drawing.Point(8, 91)
        Me.LblJob.Name = "LblJob"
        Me.LblJob.Size = New System.Drawing.Size(74, 13)
        Me.LblJob.TabIndex = 209
        Me.LblJob.Text = "Job"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(88, 61)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(261, 21)
        Me.cboEmployee.TabIndex = 204
        '
        'objbtnSearchLeaveType
        '
        Me.objbtnSearchLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeaveType.BorderSelected = False
        Me.objbtnSearchLeaveType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeaveType.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeaveType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeaveType.Location = New System.Drawing.Point(355, 114)
        Me.objbtnSearchLeaveType.Name = "objbtnSearchLeaveType"
        Me.objbtnSearchLeaveType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeaveType.TabIndex = 104
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 120
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(88, 87)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(261, 21)
        Me.cboJob.TabIndex = 207
        '
        'lblLeaveTodate
        '
        Me.lblLeaveTodate.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveTodate.Location = New System.Drawing.Point(202, 38)
        Me.lblLeaveTodate.Name = "lblLeaveTodate"
        Me.lblLeaveTodate.Size = New System.Drawing.Size(33, 13)
        Me.lblLeaveTodate.TabIndex = 194
        Me.lblLeaveTodate.Text = "To"
        '
        'dtpLeaveToDate
        '
        Me.dtpLeaveToDate.Checked = False
        Me.dtpLeaveToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpLeaveToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpLeaveToDate.Location = New System.Drawing.Point(252, 34)
        Me.dtpLeaveToDate.Name = "dtpLeaveToDate"
        Me.dtpLeaveToDate.Size = New System.Drawing.Size(97, 21)
        Me.dtpLeaveToDate.TabIndex = 4
        '
        'lblLeaveFromDate
        '
        Me.lblLeaveFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveFromDate.Location = New System.Drawing.Point(8, 38)
        Me.lblLeaveFromDate.Name = "lblLeaveFromDate"
        Me.lblLeaveFromDate.Size = New System.Drawing.Size(74, 13)
        Me.lblLeaveFromDate.TabIndex = 192
        Me.lblLeaveFromDate.Text = "Leave Date"
        '
        'dtpLeaveFromDate
        '
        Me.dtpLeaveFromDate.Checked = False
        Me.dtpLeaveFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpLeaveFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpLeaveFromDate.Location = New System.Drawing.Point(88, 34)
        Me.dtpLeaveFromDate.Name = "dtpLeaveFromDate"
        Me.dtpLeaveFromDate.Size = New System.Drawing.Size(97, 21)
        Me.dtpLeaveFromDate.TabIndex = 3
        '
        'lblLeaveName
        '
        Me.lblLeaveName.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveName.Location = New System.Drawing.Point(8, 117)
        Me.lblLeaveName.Name = "lblLeaveName"
        Me.lblLeaveName.Size = New System.Drawing.Size(74, 13)
        Me.lblLeaveName.TabIndex = 190
        Me.lblLeaveName.Text = "Leave "
        '
        'cboLeave
        '
        Me.cboLeave.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeave.DropDownWidth = 120
        Me.cboLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeave.FormattingEnabled = True
        Me.cboLeave.Location = New System.Drawing.Point(88, 113)
        Me.cboLeave.Name = "cboLeave"
        Me.cboLeave.Size = New System.Drawing.Size(261, 21)
        Me.cboLeave.TabIndex = 5
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 214)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(383, 63)
        Me.gbSortBy.TabIndex = 17
        Me.gbSortBy.TabStop = True
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(336, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 9
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(68, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(76, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(254, 21)
        Me.txtOrderBy.TabIndex = 8
        '
        'frmLeavePlanReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 567)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.KeyPreview = True
        Me.Name = "frmLeavePlanReport"
        Me.Text = "frmLeaveIssue_AT_Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblLeaveName As System.Windows.Forms.Label
    Public WithEvents cboLeave As System.Windows.Forms.ComboBox
    Public WithEvents lblLeaveTodate As System.Windows.Forms.Label
    Public WithEvents dtpLeaveToDate As System.Windows.Forms.DateTimePicker
    Public WithEvents lblLeaveFromDate As System.Windows.Forms.Label
    Public WithEvents dtpLeaveFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSearchLeaveType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchJob As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Private WithEvents LblEmployee As System.Windows.Forms.Label
    Private WithEvents LblJob As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Public WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
End Class
