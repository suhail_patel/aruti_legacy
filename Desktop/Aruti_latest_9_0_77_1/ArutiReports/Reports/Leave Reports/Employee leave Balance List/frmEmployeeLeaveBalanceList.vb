#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
#End Region

Public Class frmEmployeeLeaveBalanceList

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmEmployeeLeaveBalanceSummaryReport"
    Private objEmpLeaveBalanceSummary As clsEmployeeLeaveBalanceListReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    'Pinkal (06-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mintLeaveBalanceSetting As Integer = -1
    'Pinkal (06-Feb-2013) -- End

    'Pinkal (24-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (24-Feb-2013) -- End


#End Region

#Region "Constructor"
    Public Sub New()
        objEmpLeaveBalanceSummary = New clsEmployeeLeaveBalanceListReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpLeaveBalanceSummary.SetDefaultValue()
        InitializeComponent()

        'Pinkal (24-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (24-Feb-2013) -- End

    End Sub
#End Region

#Region "Private Function"
    Public Sub FillCombo()
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    cboEmployee.DataSource = (New clsEmployee_Master).GetEmployeeList("Employee", True, False).Tables(0)
            'Else
            '    cboEmployee.DataSource = (New clsEmployee_Master).GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)).Tables(0)
            'End If

            cboEmployee.DataSource = (New clsEmployee_Master).GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True).Tables(0)
            'S.SANDEEP [04 JUN 2015] -- END


            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Leave Type Wise "))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Employee Wise "))

            'Pinkal (06-May-2013) -- Start
            'Enhancement : TRA Changes
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 4, "Employee Without Leave Accrue"))
            'Pinkal (06-May-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub FillList()
        Try
            Dim objLeaveType As New clsleavetype_master

            lvLeaveType.Items.Clear()
            Dim dsList As DataSet = objLeaveType.getListForCombo("Leave", , 1)
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dsList.Tables("Leave").Rows
                lvItem = New ListViewItem
                lvItem.Text = dtRow.Item("name").ToString
                lvItem.Tag = CInt(dtRow.Item("leavetypeunkid").ToString)

                lvLeaveType.Items.Add(lvItem)
            Next
            lvLeaveType.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtDate.Value = DateTime.Now
            cboEmployee.SelectedValue = 0
            cboReportType.SelectedIndex = 0


            For i As Integer = 0 To lvLeaveType.Items.Count - 1
                If lvLeaveType.Items(i).Checked = False Then
                    lvLeaveType.Items(i).Checked = True
                End If
            Next

            objEmpLeaveBalanceSummary.setDefaultOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objEmpLeaveBalanceSummary.OrderByDisplay
            chkInactiveemp.Checked = False
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1

            'Pinkal (24-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (24-Feb-2013) -- End


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Pinkal (24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objEmpLeaveBalanceSummary.SetDefaultValue()

            objEmpLeaveBalanceSummary._EmployeeId = cboEmployee.SelectedValue
            objEmpLeaveBalanceSummary._EmployeeName = cboEmployee.Text

            objEmpLeaveBalanceSummary._ReportId = cboReportType.SelectedIndex
            objEmpLeaveBalanceSummary._ReportTypeName = cboReportType.Text

            objEmpLeaveBalanceSummary._Date = dtDate.Value
            objEmpLeaveBalanceSummary._IncludeInactiveEmp = chkInactiveemp.Checked
            objEmpLeaveBalanceSummary._ViewByIds = mstrStringIds
            objEmpLeaveBalanceSummary._ViewIndex = mintViewIdx
            objEmpLeaveBalanceSummary._ViewByName = mstrStringName
            objEmpLeaveBalanceSummary._Analysis_Fields = mstrAnalysis_Fields
            objEmpLeaveBalanceSummary._Analysis_Join = mstrAnalysis_Join
            objEmpLeaveBalanceSummary._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpLeaveBalanceSummary._Report_GroupName = mstrReport_GroupName


            'Pinkal (06-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objEmpLeaveBalanceSummary._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            'Pinkal (06-Feb-2013) -- End

            'Pinkal (24-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objEmpLeaveBalanceSummary._Advance_Filter = mstrAdvanceFilter
            'Pinkal (24-Feb-2013) -- End


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            objEmpLeaveBalanceSummary._IncludeCloseELC = chkIncludeCloseELC.Checked
            'Pinkal (21-Jul-2014) -- End

            'Pinkal (25-Jul-2015) -- Start
            'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 
            objEmpLeaveBalanceSummary._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
            objEmpLeaveBalanceSummary._DBEnddate = FinancialYear._Object._Database_End_Date.Date
            'Pinkal (25-Jul-2015) -- End


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            objEmpLeaveBalanceSummary._LeaveAccrueTenureSetting = ConfigParameter._Object._LeaveAccrueTenureSetting
            objEmpLeaveBalanceSummary._LeaveAccrueDaysAfterEachMonth = ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth
            'Pinkal (18-Nov-2016) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmEmployeeLeaveBalanceSummaryReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpLeaveBalanceSummary = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveBalanceSummaryReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveBalanceSummaryReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Me._Title = objEmpLeaveBalanceSummary._ReportName
            Me._Message = objEmpLeaveBalanceSummary._ReportDesc

            Call FillCombo()
            Call FillList()
            Call ResetValue()


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '    chkIncludeCloseELC.Visible = True
            '    chkIncludeCloseELC.Checked = False
            'Else
            '    chkIncludeCloseELC.Visible = False
            '    chkIncludeCloseELC.Checked = False
            'End If
            'Pinkal (21-Jul-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveBalanceSummaryReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveBalanceSummaryReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmployeeLeaveBalanceSummaryReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveBalanceSummaryReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveBalanceSummaryReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveBalanceSummaryReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons"
    Private Sub frmEmployeeLeaveBalanceSummaryReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Dim strLeaveId As String = ""

        Try
            If lvLeaveType.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select atleast one Leave Type."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If lvLeaveType.CheckedItems.Count > 0 Then
                For i As Integer = 0 To lvLeaveType.CheckedItems.Count - 1
                    strLeaveId &= "" & lvLeaveType.CheckedItems(i).Tag.ToString & ","
                Next
                strLeaveId = strLeaveId.Substring(0, strLeaveId.Length - 1)
            End If

            objEmpLeaveBalanceSummary._LeaveTypeIds = strLeaveId

            If SetFilter() = False Then Exit Sub


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITIONcboReportType.SelectedIndex

            'If cboReportType.SelectedIndex <= 1 Then
            '    objEmpLeaveBalanceSummary.generateReport(0, e.Type, enExportAction.None)
            'ElseIf cboReportType.SelectedIndex > 1 Then
            '    objEmpLeaveBalanceSummary.generateReport(cboReportType.SelectedIndex, e.Type, enExportAction.None)
            'End If

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            'If cboReportType.SelectedIndex <= 1 Then
            '    objEmpLeaveBalanceSummary.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                                User._Object._Userunkid, _
            '                                                FinancialYear._Object._YearUnkid, _
            '                                                Company._Object._Companyunkid, _
            '                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                                ConfigParameter._Object._ExportReportPath, _
            '                                                ConfigParameter._Object._OpenAfterExport, _
            '                                                cboReportType.SelectedIndex, e.Type, enExportAction.None)
            'ElseIf cboReportType.SelectedIndex > 1 Then
            '    objEmpLeaveBalanceSummary.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                                User._Object._Userunkid, _
            '                                                FinancialYear._Object._YearUnkid, _
            '                                                Company._Object._Companyunkid, _
            '                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                                ConfigParameter._Object._ExportReportPath, _
            '                                                ConfigParameter._Object._OpenAfterExport, _
            '                                                cboReportType.SelectedIndex, e.Type, enExportAction.None)
            'End If
            objEmpLeaveBalanceSummary.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                            User._Object._Userunkid, _
                                                            FinancialYear._Object._YearUnkid, _
                                                            Company._Object._Companyunkid, _
                                                            dtDate.Value.Date, _
                                                            dtDate.Value.Date, _
                                                            ConfigParameter._Object._UserAccessModeSetting, True, _
                                                            ConfigParameter._Object._ExportReportPath, _
                                                            ConfigParameter._Object._OpenAfterExport, _
                                                            cboReportType.SelectedIndex, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Shani(11-Feb-2016) -- End

            'Pinkal (24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveBalanceSummaryReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveBalanceSummaryReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Dim strLeaveId As String = ""
        Try
            If SetFilter() = False Then Exit Sub


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

            ' objEmpLeaveBalanceSummary.generateReport(0, enPrintAction.None, e.Type)

            If lvLeaveType.CheckedItems.Count > 0 Then
                For i As Integer = 0 To lvLeaveType.CheckedItems.Count - 1
                    strLeaveId &= "" & lvLeaveType.CheckedItems(i).Tag.ToString & ","
                Next
                strLeaveId = strLeaveId.Substring(0, strLeaveId.Length - 1)
            End If

            objEmpLeaveBalanceSummary._LeaveTypeIds = strLeaveId

            If cboReportType.SelectedIndex <= 1 Then
                objEmpLeaveBalanceSummary.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                            , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                                                            , cboReportType.SelectedIndex, enPrintAction.None, e.Type)
            ElseIf cboReportType.SelectedIndex > 1 Then
                objEmpLeaveBalanceSummary.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                            , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                                                            , cboReportType.SelectedIndex, enPrintAction.None, e.Type)
            End If

            'Pinkal (24-Aug-2015) -- End

            'Pinkal (24-Aug-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveBalanceSummaryReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveBalanceSummaryReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveBalanceSummaryReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveBalanceSummaryReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveBalanceSummaryReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveBalanceSummaryReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeLeaveBalanceListReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeLeaveBalanceListReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEmployeeLeaveBalanceSummaryReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (24-Feb-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (24-Feb-2013) -- End


    'Pinkal (06-May-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (06-May-2013) -- End


#End Region

#Region "Controls"
    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmpLeaveBalanceSummary.setOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objEmpLeaveBalanceSummary.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            objEmpLeaveBalanceSummary.setDefaultOrderBy(CInt(cboReportType.SelectedIndex))
            txtOrderBy.Text = objEmpLeaveBalanceSummary.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (25-Apr-2014) -- Start
    'Enhancement : TRA Changes

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            For i As Integer = 0 To lvLeaveType.Items.Count - 1
                RemoveHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked
                lvLeaveType.Items(i).Checked = chkSelectAll.Checked
                AddHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Listview Event"

    Private Sub lvLeaveType_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvLeaveType.ItemChecked
        Try
            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            If lvLeaveType.CheckedItems.Count <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked

            ElseIf lvLeaveType.CheckedItems.Count < lvLeaveType.Items.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate

            ElseIf lvLeaveType.CheckedItems.Count = lvLeaveType.Items.Count Then
                chkSelectAll.CheckState = CheckState.Checked

            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLeaveType_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (25-Apr-2014) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
			Me.ColumnHeader2.Text = Language._Object.getCaption(CStr(Me.ColumnHeader2.Tag), Me.ColumnHeader2.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Leave Type Wise")
			Language.setMessage(mstrModuleName, 2, "Employee Wise")
			Language.setMessage(mstrModuleName, 3, "Please Select atleast one Leave Type.")
			Language.setMessage(mstrModuleName, 4, "Employee Without Leave Accrue")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
