'************************************************************************************************************************************
'Class Name : clsMonthlyTaxIncomeReport.vb
'Purpose    :
'Date       :28/08/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsMonthlyTaxIncomeReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsMonthlyTaxIncomeReport"
    Private mstrReportId As String = enArutiReport.Monthly_Tax_Income_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = String.Empty
    'Sohail (06 Jan 2015) -- Start
    'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private marrDatabaseName As New ArrayList
    Private mstrCurrDatabaseName As String = FinancialYear._Object._DatabaseName
    'Sohail (06 Jan 2015) -- End

    Private mintDBBNSMembershipUnkID As Integer = 0
    Private mstrDBBNSMembershipName As String = String.Empty

    Private mintOPSMembershipUnkID As Integer = 0
    Private mstrOPSMembershipName As String = String.Empty

    Private mintPPSMembershipUnkID As Integer = 0
    Private mstrPPSMembershipName As String = String.Empty

    Private mintTINNoMembershipUnkID As Integer = 0
    Private mstrTINNoMembershipName As String = String.Empty

    Private mintPAYEHeadUnkID As Integer = 0
    Private mstrPAYEHeadName As String = String.Empty

    Private mintOtherEarningTranId As Integer = 0
    Private mstrOtherEarningTranName As String = String.Empty

    Private mblnIncludeInactiveEmp As Boolean = True

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty

    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mDicDataBaseYearId As Dictionary(Of String, Integer)
    Private mDicDataBaseEndDate As Dictionary(Of String, String)
    'S.SANDEEP [04 JUN 2015] -- END

    'Sohail (03 Aug 2019) -- Start
    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
    Private mdtEmpAsOnDate As Date
    'Sohail (03 Aug 2019) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    'Sohail (06 Jan 2015) -- Start
    'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _CurrDatabaseName() As String
        Set(ByVal value As String)
            mstrCurrDatabaseName = value
        End Set
    End Property
    'Sohail (06 Jan 2015) -- End

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _DBBNSMembershipId() As Integer
        Set(ByVal value As Integer)
            mintDBBNSMembershipUnkID = value
        End Set
    End Property

    Public WriteOnly Property _DBBNSMembershipName() As String
        Set(ByVal value As String)
            mstrDBBNSMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _OPSMembershipId() As Integer
        Set(ByVal value As Integer)
            mintOPSMembershipUnkID = value
        End Set
    End Property

    Public WriteOnly Property _OPSMembershipName() As String
        Set(ByVal value As String)
            mstrOPSMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _PPSMembershipId() As Integer
        Set(ByVal value As Integer)
            mintPPSMembershipUnkID = value
        End Set
    End Property

    Public WriteOnly Property _PPSMembershipName() As String
        Set(ByVal value As String)
            mstrPPSMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _TINNoMembershipId() As Integer
        Set(ByVal value As Integer)
            mintTINNoMembershipUnkID = value
        End Set
    End Property

    Public WriteOnly Property _TINNoMembershipName() As String
        Set(ByVal value As String)
            mstrTINNoMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _PAYEHeadId() As Integer
        Set(ByVal value As Integer)
            mintPAYEHeadUnkID = value
        End Set
    End Property

    Public WriteOnly Property _PAYEHeadName() As String
        Set(ByVal value As String)
            mstrPAYEHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranName() As String
        Set(ByVal value As String)
            mstrOtherEarningTranName = value
        End Set
    End Property



    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property


    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public WriteOnly Property _DicDataBaseYearId() As Dictionary(Of String, Integer)
        Set(ByVal value As Dictionary(Of String, Integer))
            mDicDataBaseYearId = value
        End Set
    End Property

    Public WriteOnly Property _DicDataBaseEndDate() As Dictionary(Of String, String)
        Set(ByVal value As Dictionary(Of String, String))
            mDicDataBaseEndDate = value
        End Set
    End Property
    'S.SANDEEP [04 JUN 2015] -- END

    'Sohail (03 Aug 2019) -- Start
    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
    Public WriteOnly Property _EmpAsOnDate() As Date
        Set(ByVal value As Date)
            mdtEmpAsOnDate = value
        End Set
    End Property
    'Sohail (03 Aug 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = ""
            'Sohail (06 Jan 2015) -- Start
            'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
            mstrPeriodIds = ""
            mstrPeriodNames = ""
            'Sohail (06 Jan 2015) -- End
            mintEmpId = 0
            mstrEmpName = ""
            mintDBBNSMembershipUnkID = 0
            mintOPSMembershipUnkID = 0
            mintPPSMembershipUnkID = 0
            mintTINNoMembershipUnkID = 0
            mintPAYEHeadUnkID = 0
            mintOtherEarningTranId = 0


            mblnIncludeInactiveEmp = True


            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""


            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            mDicDataBaseYearId = Nothing
            mDicDataBaseEndDate = Nothing
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'Sohail (06 Jan 2015) -- Start
            'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
            'objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            'Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Period :") & " " & mstrPeriodName & " "
            'Sohail (06 Jan 2015) -- End

            objDataOperation.AddParameter("@membershipDBBNSid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDBBNSMembershipUnkID)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "D.B.B.N.S.S.S. :") & " " & mstrDBBNSMembershipName & " "

            objDataOperation.AddParameter("@payheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPAYEHeadUnkID)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Tax Paid :") & " " & mstrPAYEHeadName & " "

            'If mintOPSMembershipUnkID > 0 Then
            objDataOperation.AddParameter("@membershipOPSid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOPSMembershipUnkID)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "O.P.S. :") & " " & mstrOPSMembershipName & " "
            'End If

            'If mintPPSMembershipUnkID > 0 Then
            objDataOperation.AddParameter("@membershipPPSid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPPSMembershipUnkID)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "P.P.S. :") & " " & mstrPPSMembershipName & " "
            'End If

            'If mintTINNoMembershipUnkID > 0 Then
            objDataOperation.AddParameter("@membershipTINNOid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTINNoMembershipUnkID)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "TIN No. :") & " " & mstrTINNoMembershipName & " "
            'End If

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Employee :") & " " & mstrEmpName & " "
            End If


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Order By : ") & " " & Me.OrderByDisplay
            End If
            If mintViewIndex > 0 Then
                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                'Me._FilterQuery &= " ORDER BY GName, TableDBBNSSS.end_date, TableDBBNSSS.EmpName "
                Me._FilterQuery &= " ORDER BY GName, TableDBBNSSS.EmpName "
                'Sohail (06 Jan 2015) -- End
            Else
                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                'Me._FilterQuery &= " ORDER BY TableDBBNSSS.end_date, TableDBBNSSS.EmpName "
                Me._FilterQuery &= " ORDER BY TableDBBNSSS.EmpName "
                'Sohail (06 Jan 2015) -- End
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("EmpCode", Language.getMessage(mstrModuleName, 29, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 30, "Employee Name")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function Generate_DetailReport(ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal dtPeriodEnd As Date, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean, _
                                          ByVal intBaseCurrencyId As Integer) As Boolean
        'Public Function Generate_DetailReport() As Boolean
        'S.SANDEEP [04 JUN 2015] -- END
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim dtPeriod As DataTable = Nothing
        'Sohail (06 Jan 2015) -- Start
        'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
        Dim StrInnerQ As String = ""
        Dim strDatabaseName As String = ""
        'Sohail (06 Jan 2015) -- End
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            'S.SANDEEP [04 JUN 2015] -- END


            '[mstrCurrDatabaseName] CHANGED TO [strDatabaseName] IN QUERY. DUE TO ACCESS ISSUE FOR EMPLOYEE GIVEN IN LAST YEAR CAN BE DIFFERENT FROM CURRENT YEAR DATABASE.


            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT   TableDBBNSSS.EmpId " & _
                              ", TableDBBNSSS.EmpCode AS EmpCode " & _
                              ", TableDBBNSSS.EmpName " & _
                              ", TableDBBNSSS.JOB_Title " & _
                              ", ISNULL(TableTINNO.Membership_No, '') AS TINNO " & _
                              ", SUM(ISNULL(BasicSal, 0)) AS BasicSal " & _
                              ", SUM(ISNULL(TableTaxble.Amount, 0) - ISNULL(BasicSal, 0)) AS TaxableEarning " & _
                              ", SUM(ISNULL(TableTaxble.Amount, 0)) AS TOTAL_EMOLUMENTS " & _
                              ", TableDBBNSSS.MembershipUnkId AS MemId " & _
                              ", TableDBBNSSS.Membership_No AS MembershipNo " & _
                              ", TableDBBNSSS.Membership_Name AS MembershipName " & _
                              ", SUM(ISNULL(TableDBBNSSS.Amount, 0)) AS DBBNSSS " & _
                              ", SUM(ISNULL(TableOPS.Amount, 0)) AS OPS " & _
                              ", SUM(ISNULL(TablePPS.Amount, 0)) AS PPS " & _
                              ", SUM(ISNULL(TableTaxRelief.Amount, 0)) AS TaxRelief " & _
                              ", SUM(ISNULL(TableTaxble.Amount, 0) - ISNULL(TableDBBNSSS.Amount, 0) - ISNULL(TableOPS.Amount, 0) - ISNULL(TablePPS.Amount, 0)) AS TaxableIncome " & _
                              ", SUM(ISNULL(TableTaxPaid.Amount, 0)) AS TaxPaid " & _
                              ", SUM(ISNULL(TableTaxPaid.Amount, 0) - ISNULL(TableTaxRelief.Amount, 0)) AS PAYETAX " & _
                              ", SUM(ISNULL(TableGross.GrossPay, 0)) AS GrossPay " & _
                              ", TableDBBNSSS.Id " & _
                              ", TableDBBNSSS.GName " & _
                        "FROM    ( "
                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                '"SELECT  TableDBBNSSS.Periodid  " & _
                '              ", TableDBBNSSS.PeriodName " & _
                '              ", TableDBBNSSS.end_date " & _
                '                ", TableDBBNSSS.StDate AS StDate " & _
                'Sohail (06 Jan 2015) -- End

                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If
                    'Sohail (06 Jan 2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END


                    StrInnerQ &= "     SELECT    ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                                          ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                                          ", CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date, '01-Jan-1900'), 112) AS end_date " & _
                                          ", hremployee_master.employeeunkid AS Empid " & _
                                          ", hremployee_master.employeecode AS EmpCode " & _
                                          ", ISNULL(hrjob_master.job_name, '') AS JOB_Title " & _
                                          ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                                          ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                                          ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                                          ", hrmembership_master.membershipname AS Membership_Name " & _
                                          ", cfcommon_period_tran.start_date AS StDate "

                    If mblnFirstNamethenSurname = True Then
                        StrInnerQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                    Else
                        StrInnerQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '')+ ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
                    End If

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrInnerQ &= "          FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                    '                            "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                            "LEFT JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                    '                                                             "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                                                             "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
                    '                            "JOIN " & strDatabaseName & "..hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    '                                                    "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                    '                            "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    '                            "JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _                '                            
                    '                            "LEFT JOIN " & strDatabaseName & "..hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid "

                    StrInnerQ &= "          FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "LEFT JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                 "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                 "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
                                                "JOIN " & strDatabaseName & "..hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                        "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                                                "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                "JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "LEFT JOIN " & _
                                                "( " & _
                                                "    SELECT " & _
                                                "         jobunkid " & _
                                                "        ,jobgroupunkid " & _
                                                "        ,employeeunkid " & _
                                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                "    FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
                                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & mDicDataBaseEndDate(strDatabaseName).ToString & "' " & _
                                                ") AS Jobs ON Jobs.employeeunkid = " & strDatabaseName & "..hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                                "LEFT JOIN " & strDatabaseName & "..hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid "
                    'S.SANDEEP [04 JUN 2015] -- END


                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "          WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                            "AND hrmembership_master.membershipunkid = @membershipDBBNSid " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "
                    'Sohail (06 Jan 2015)- [AND prtnaleave_tran.payperiodunkid = @periodunkid]

                    If mintEmpId > 0 Then
                        StrInnerQ &= "                AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (06 Jan 2015) -- Start
                    'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""
                'Sohail (06 Jan 2015) -- End

                StrQ &= "        ) AS TableDBBNSSS " & _
                            "LEFT JOIN ( "

                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If
                    'Sohail (06 Jan 2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid  " & _
                                                  ", hremployee_master.employeeunkid AS Empid " & _
                                                  ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                                                  ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                                                  ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                                                  ", hrmembership_master.membershipname AS Membership_Name "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "                    FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "LEFT JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                      "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                      "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
                                                        "JOIN " & strDatabaseName & "..hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND hrmembership_master.membershipunkid = @membershipOPSid " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "
                    'Sohail (06 Jan 2015) - [AND prtnaleave_tran.payperiodunkid = @periodunkid]

                    If mintEmpId > 0 Then
                        StrInnerQ &= "                        AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''Pinkal (15-Oct-2014) -- Start
                    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    'End If
                    ''Pinkal (15-Oct-2014) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END


                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (06 Jan 2015) -- Start
                    'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""
                'Sohail (06 Jan 2015) -- End

                StrQ &= "                  ) AS TableOPS ON TableDBBNSSS.Periodid = TableOPS.Periodid " & _
                                                           "AND TableDBBNSSS.Empid = TableOPS.Empid " & _
                            "LEFT JOIN ( "

                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If
                    'Sohail (06 Jan 2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid  " & _
                                                  ", hremployee_master.employeeunkid AS Empid " & _
                                                  ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                                                  ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                                                  ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                                                  ", hrmembership_master.membershipname AS Membership_Name "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "                    FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "LEFT JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                      "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                      "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
                                                        "JOIN " & strDatabaseName & "..hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND hrmembership_master.membershipunkid = @membershipPPSid " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "
                    'Sohail (06 Jan 2015) - [AND prtnaleave_tran.payperiodunkid = @periodunkid]

                    If mintEmpId > 0 Then
                        StrInnerQ &= "                AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''Pinkal (15-Oct-2014) -- Start
                    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    'End If
                    ''Pinkal (15-Oct-2014) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END


                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (06 Jan 2015) -- Start
                    'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""
                'Sohail (06 Jan 2015) -- End

                StrQ &= "                  ) AS TablePPS ON TableDBBNSSS.Periodid = TablePPS.Periodid " & _
                                                           "AND TableDBBNSSS.Empid = TablePPS.Empid " & _
                                "LEFT JOIN ( "

                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If
                    'Sohail (06 Jan 2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  hremployee_master.employeeunkid AS Empid  " & _
                                                  ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                                                  ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                                                  ", hrmembership_master.membershipname AS Membership_Name "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "                    FROM    " & strDatabaseName & "..hremployee_master " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
                                                        "JOIN " & strDatabaseName & "..hrmembership_master ON hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "                    WHERE   hrmembership_master.membershipunkid = @membershipTINNOid "

                    If mintEmpId > 0 Then
                        StrInnerQ &= "                AND hremployee_master.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''Pinkal (15-Oct-2014) -- Start
                    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    'End If
                    ''Pinkal (15-Oct-2014) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END


                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (06 Jan 2015) -- Start
                    'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""
                'Sohail (06 Jan 2015) -- End

                StrQ &= "                  ) AS TableTINNO ON TableDBBNSSS.Empid = TableTINNO.Empid " & _
                                "LEFT JOIN ( "

                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If
                    'Sohail (06 Jan 2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid  " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                  ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "                    FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                                   "AND prpayrollprocess_tran.tranheadunkid > 0 "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.istaxable = 1 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "
                    'Sohail (06 Jan 2015) - [AND prtnaleave_tran.payperiodunkid = @periodunkid]

                    If mintEmpId > 0 Then
                        StrInnerQ &= "                AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''Pinkal (15-Oct-2014) -- Start
                    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    'End If
                    ''Pinkal (15-Oct-2014) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END


                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "                    GROUP BY "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields.Substring(2).Replace("AS Id", "").Replace("AS GName", "") & ","
                    End If

                    StrInnerQ &= "                            prtnaleave_tran.payperiodunkid  " & _
                                                      ", prpayrollprocess_tran.employeeunkid "

                    'Sohail (06 Jan 2015) -- Start
                    'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""
                'Sohail (06 Jan 2015) -- End

                StrQ &= ") AS TableTaxble ON TableDBBNSSS.Periodid = TableTaxble.Periodid " & _
                                                              "AND TableDBBNSSS.Empid = TableTaxble.Empid " & _
                                    "LEFT JOIN ( "

                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If
                    'Sohail (06 Jan 2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid  " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                  ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "                    FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                                   "AND prpayrollprocess_tran.tranheadunkid > 0 "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.istaxrelief = 1 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "
                    'Sohail (06 Jan 2015) - [AND prtnaleave_tran.payperiodunkid = @periodunkid]

                    If mintEmpId > 0 Then
                        StrInnerQ &= "                AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''Pinkal (15-Oct-2014) -- Start
                    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    'End If
                    ''Pinkal (15-Oct-2014) -- End


                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "                    GROUP BY "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields.Substring(2).Replace("AS Id", "").Replace("AS GName", "") & ","
                    End If

                    StrInnerQ &= "                            prtnaleave_tran.payperiodunkid  " & _
                                                      ", prpayrollprocess_tran.employeeunkid "

                    'Sohail (06 Jan 2015) -- Start
                    'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""
                'Sohail (06 Jan 2015) -- End

                StrQ &= ") AS TableTaxRelief ON TableDBBNSSS.Periodid = TableTaxRelief.Periodid " & _
                                                                 "AND TableDBBNSSS.Empid = TableTaxRelief.Empid " & _
                                    "LEFT JOIN ( "

                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If
                    'Sohail (06 Jan 2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid  " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                  ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "                    FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                                   "AND prpayrollprocess_tran.tranheadunkid > 0 "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @payheadid " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "
                    'Sohail (06 Jan 2015) - [AND prtnaleave_tran.payperiodunkid = @periodunkid]

                    If mintEmpId > 0 Then
                        StrInnerQ &= "                AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''Pinkal (15-Oct-2014) -- Start
                    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    'End If
                    ''Pinkal (15-Oct-2014) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END


                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "                    GROUP BY "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields.Substring(2).Replace("AS Id", "").Replace("AS GName", "") & ","
                    End If

                    StrInnerQ &= "                            prtnaleave_tran.payperiodunkid  " & _
                                                      ", prpayrollprocess_tran.employeeunkid "

                    'Sohail (06 Jan 2015) -- Start
                    'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""
                'Sohail (06 Jan 2015) -- End

                StrQ &= ") AS TableTaxPaid ON TableDBBNSSS.Periodid = TableTaxPaid.Periodid " & _
                                                               "AND TableDBBNSSS.Empid = TableTaxPaid.Empid " & _
                                    "LEFT JOIN ( " & _
                                    "SELECT  Gross.Periodid  " & _
                                                  ", Gross.Empid " & _
                                                  ", SUM(Gross.Amount) AS GrossPay " & _
                                                "FROM    ( "
                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If
                    'Sohail (06 Jan 2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT    prtnaleave_tran.payperiodunkid AS Periodid  " & _
                                                              ", hremployee_master.employeeunkid AS Empid " & _
                                                              ", CAST(ISNULL(prpayrollprocess_tran.amount , 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "
                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "                              FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                                    "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                    "LEFT JOIN " & strDatabaseName & "..practivity_master ON practivity_master.activityunkid = prpayrollprocess_tran.activityunkid "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "                              WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                                "AND ( prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ) "
                    'Sohail (06 Jan 2015) - [AND prtnaleave_tran.payperiodunkid = @periodunkid]

                    If mintEmpId > 0 Then
                        StrInnerQ &= "                                    AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''Pinkal (15-Oct-2014) -- Start
                    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    'End If
                    ''Pinkal (15-Oct-2014) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END


                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (06 Jan 2015) -- Start
                    'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""
                'Sohail (06 Jan 2015) -- End

                StrQ &= "                            ) AS Gross " & _
                                            "GROUP BY Gross.Periodid  " & _
                                                  ", Gross.Empid " & _
                                          ") AS TableGross ON TableDBBNSSS.EmpId = TableGross.Empid " & _
                                                             "AND TableDBBNSSS.Periodid = TableGross.Periodid " & _
                                "LEFT JOIN ( "

                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If
                    'Sohail (06 Jan 2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  prtnaleave_tran.payperiodunkid AS Periodid  " & _
                                                  ", hremployee_master.employeeunkid AS Empid " & _
                                                  ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "                    FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "
                    'Sohail (06 Jan 2015) - [AND prtnaleave_tran.payperiodunkid = @periodunkid]

                    If mintEmpId > 0 Then
                        StrInnerQ &= "                AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                        StrInnerQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                    Else
                        StrInnerQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''Pinkal (15-Oct-2014) -- Start
                    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    'End If
                    ''Pinkal (15-Oct-2014) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END


                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "                    GROUP BY "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields.Substring(2).Replace("AS Id", "").Replace("AS GName", "") & ","
                    End If

                    StrInnerQ &= "                            prtnaleave_tran.payperiodunkid  " & _
                                                      ", hremployee_master.employeeunkid "

                    'Sohail (06 Jan 2015) -- Start
                    'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""

                StrQ &= ") AS BAS ON BAS.Empid = TableDBBNSSS.EmpId " & _
                                                      "AND TableDBBNSSS.Periodid = BAS.Periodid "

                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
                StrQ &= " GROUP BY TableDBBNSSS.EmpId  " & _
                                  ", TableDBBNSSS.EmpCode " & _
                                  ", TableDBBNSSS.EmpName " & _
                                  ", TableDBBNSSS.JOB_Title " & _
                                  ", ISNULL(TableTINNO.Membership_No, '') " & _
                                  ", TableDBBNSSS.MembershipUnkId " & _
                                  ", TableDBBNSSS.Membership_No " & _
                                  ", TableDBBNSSS.Membership_Name " & _
                                  ", TableDBBNSSS.Id " & _
                                  ", TableDBBNSSS.GName "
                'Sohail (06 Jan 2015) -- End


            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then  ' FOR DIFFERENT CURRENCY 




            End If


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            mdtTableExcel = dsList.Tables(0)
            Dim intColIdx As Integer = -1

            mdtTableExcel.Columns("EmpCode").Caption = Language.getMessage(mstrModuleName, 9, "EMPLOYEE ID")
            intColIdx += 1
            mdtTableExcel.Columns("EmpCode").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("EmpName").Caption = Language.getMessage(mstrModuleName, 10, "NAME")
            intColIdx += 1
            mdtTableExcel.Columns("EmpName").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("JOB_Title").Caption = Language.getMessage(mstrModuleName, 11, "JOB TITLE")
            intColIdx += 1
            mdtTableExcel.Columns("JOB_Title").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TINNO").Caption = Language.getMessage(mstrModuleName, 12, "TIN No.")
            intColIdx += 1
            mdtTableExcel.Columns("TINNO").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("BasicSal").Caption = Language.getMessage(mstrModuleName, 13, "BASIC SALARY")
            intColIdx += 1
            mdtTableExcel.Columns("BasicSal").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TaxableEarning").Caption = Language.getMessage(mstrModuleName, 14, "TAXABLE EARNINGS")
            intColIdx += 1
            mdtTableExcel.Columns("TaxableEarning").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TOTAL_EMOLUMENTS").Caption = Language.getMessage(mstrModuleName, 15, "TOTAL EMOLUMENTS")
            intColIdx += 1
            mdtTableExcel.Columns("TOTAL_EMOLUMENTS").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("MembershipNo").Caption = Language.getMessage(mstrModuleName, 16, "EMPLOYEE SSF")
            intColIdx += 1
            mdtTableExcel.Columns("MembershipNo").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("DBBNSSS").Caption = Language.getMessage(mstrModuleName, 17, "EMPLOYEE D.B.B.N.S.S.S")
            intColIdx += 1
            mdtTableExcel.Columns("DBBNSSS").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("OPS").Caption = Language.getMessage(mstrModuleName, 18, "EMPLOYEE O.P.S")
            intColIdx += 1
            mdtTableExcel.Columns("OPS").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("PPS").Caption = Language.getMessage(mstrModuleName, 19, "EMPLOYEE P.P.S")
            intColIdx += 1
            mdtTableExcel.Columns("PPS").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TaxRelief").Caption = Language.getMessage(mstrModuleName, 20, "TAX RELIEF")
            intColIdx += 1
            mdtTableExcel.Columns("TaxRelief").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TaxableIncome").Caption = Language.getMessage(mstrModuleName, 21, "TAXABLE INCOME")
            intColIdx += 1
            mdtTableExcel.Columns("TaxableIncome").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TaxPaid").Caption = Language.getMessage(mstrModuleName, 22, "EMPLOYEE INC. TAX")
            intColIdx += 1
            mdtTableExcel.Columns("TaxPaid").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("PAYETAX").Caption = Language.getMessage(mstrModuleName, 23, "PAYE TAX")
            intColIdx += 1
            mdtTableExcel.Columns("PAYETAX").SetOrdinal(intColIdx)


            Dim strarrGroupColumns As String() = Nothing
            Dim strSubTotal As String = ""
            Dim strGTotal As String = Language.getMessage(mstrModuleName, 24, "Grand Total :")
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell = Nothing

            If mintViewIndex > 0 Then
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                intColIdx += 1
                mdtTableExcel.Columns("GName").SetOrdinal(intColIdx)

                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
                strSubTotal = Language.getMessage(mstrModuleName, 25, "Sub Total :")
            End If

            For i = intColIdx + 1 To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(intColIdx + 1)
            Next

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)
            '--------------------

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "I.R.S. Number:  100923"), "s9wc")
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)
            '--------------------

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)
            '--------------------

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            'Sohail (06 Jan 2015) -- Start
            'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
            'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 27, "Pay Period :") & mstrPeriodName, "s9wc")
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 27, "Pay Period :") & mstrPeriodNames, "s9wc")
            'Sohail (06 Jan 2015) -- End
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)
            '--------------------

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)
            '--------------------

            row = New WorksheetRow()
            For i As Integer = 61 To 75
                If i >= 65 AndAlso i <= 74 Then
                    wcell = New WorksheetCell(Chr(i), "HeaderStyle")
                Else
                    wcell = New WorksheetCell("", "HeaderStyle")
                End If
                row.Cells.Add(wcell)
            Next
            If mintViewIndex > 0 Then
                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)
            '--------------------


            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            intArrayColumnWidth(0) = 125
            intArrayColumnWidth(1) = 200
            intArrayColumnWidth(2) = 200
            For i As Integer = 3 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next



            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Language.getMessage(mstrModuleName, 28, "Ghana INCOME TAX REPORT"), , " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, Nothing)

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "D.B.B.N.S.S.S. :")
            Language.setMessage(mstrModuleName, 3, "Tax Paid :")
            Language.setMessage(mstrModuleName, 4, "O.P.S. :")
            Language.setMessage(mstrModuleName, 5, "P.P.S. :")
            Language.setMessage(mstrModuleName, 6, "TIN No. :")
            Language.setMessage(mstrModuleName, 7, "Employee :")
            Language.setMessage(mstrModuleName, 8, "Order By :")
            Language.setMessage(mstrModuleName, 9, "EMPLOYEE ID")
            Language.setMessage(mstrModuleName, 10, "NAME")
            Language.setMessage(mstrModuleName, 11, "JOB TITLE")
            Language.setMessage(mstrModuleName, 12, "TIN No.")
            Language.setMessage(mstrModuleName, 13, "BASIC SALARY")
            Language.setMessage(mstrModuleName, 14, "TAXABLE EARNINGS")
            Language.setMessage(mstrModuleName, 15, "TOTAL EMOLUMENTS")
            Language.setMessage(mstrModuleName, 16, "EMPLOYEE SSF")
            Language.setMessage(mstrModuleName, 17, "EMPLOYEE D.B.B.N.S.S.S")
            Language.setMessage(mstrModuleName, 18, "EMPLOYEE O.P.S")
            Language.setMessage(mstrModuleName, 19, "EMPLOYEE P.P.S")
            Language.setMessage(mstrModuleName, 20, "TAX RELIEF")
            Language.setMessage(mstrModuleName, 21, "TAXABLE INCOME")
            Language.setMessage(mstrModuleName, 22, "EMPLOYEE INC. TAX")
            Language.setMessage(mstrModuleName, 23, "PAYE TAX")
            Language.setMessage(mstrModuleName, 24, "Grand Total :")
            Language.setMessage(mstrModuleName, 25, "Sub Total :")
            Language.setMessage(mstrModuleName, 26, "I.R.S. Number:  100923")
            Language.setMessage(mstrModuleName, 27, "Pay Period :")
            Language.setMessage(mstrModuleName, 28, "Ghana INCOME TAX REPORT")
            Language.setMessage(mstrModuleName, 29, "Employee Code")
            Language.setMessage(mstrModuleName, 30, "Employee Name")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
