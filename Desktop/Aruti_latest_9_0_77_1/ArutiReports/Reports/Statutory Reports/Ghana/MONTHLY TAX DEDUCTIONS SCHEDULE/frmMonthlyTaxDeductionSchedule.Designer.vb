﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMonthlyTaxDeductionSchedule
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMonthlyTaxDeductionSchedule))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblToPeriod = New System.Windows.Forms.Label
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblVehicleElement = New System.Windows.Forms.Label
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.cboVehicleElement = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lnOtherInformation = New eZee.Common.eZeeLine
        Me.lblTINNo = New System.Windows.Forms.Label
        Me.cboTINNo = New System.Windows.Forms.ComboBox
        Me.cboPensionsThirdTier = New System.Windows.Forms.ComboBox
        Me.LblCurrencyRate = New System.Windows.Forms.Label
        Me.cboAccomodationElement = New System.Windows.Forms.ComboBox
        Me.lblAccomodationElement = New System.Windows.Forms.Label
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.cboPensionsSSF = New System.Windows.Forms.ComboBox
        Me.lblPensionsSSF = New System.Windows.Forms.Label
        Me.lblPensionsThirdTier = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.gbBasicSalaryOtherEarning = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.objbtnSearchOtherEarning = New eZee.Common.eZeeGradientButton
        Me.cboOtherEarning = New System.Windows.Forms.ComboBox
        Me.lblOtherEarning = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSearchAccomodationElement = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchVehicleElement = New eZee.Common.eZeeGradientButton
        Me.gbMandatoryInfo.SuspendLayout()
        Me.gbBasicSalaryOtherEarning.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(705, 60)
        Me.eZeeHeader.TabIndex = 7
        Me.eZeeHeader.Title = "Monthly Tax Deducations Schedule Report"
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchVehicleElement)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchAccomodationElement)
        Me.gbMandatoryInfo.Controls.Add(Me.lblToPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.btnSaveSelection)
        Me.gbMandatoryInfo.Controls.Add(Me.lblVehicleElement)
        Me.gbMandatoryInfo.Controls.Add(Me.cboToPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboVehicleElement)
        Me.gbMandatoryInfo.Controls.Add(Me.cboEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.lblEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.lnOtherInformation)
        Me.gbMandatoryInfo.Controls.Add(Me.lblTINNo)
        Me.gbMandatoryInfo.Controls.Add(Me.cboTINNo)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPensionsThirdTier)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrencyRate)
        Me.gbMandatoryInfo.Controls.Add(Me.cboAccomodationElement)
        Me.gbMandatoryInfo.Controls.Add(Me.lblAccomodationElement)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkAnalysisBy)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPensionsSSF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPensionsSSF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPensionsThirdTier)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(578, 233)
        Me.gbMandatoryInfo.TabIndex = 8
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblToPeriod
        '
        Me.lblToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToPeriod.Location = New System.Drawing.Point(310, 36)
        Me.lblToPeriod.Name = "lblToPeriod"
        Me.lblToPeriod.Size = New System.Drawing.Size(101, 15)
        Me.lblToPeriod.TabIndex = 106
        Me.lblToPeriod.Text = "To Period"
        Me.lblToPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(151, 187)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 8
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'lblVehicleElement
        '
        Me.lblVehicleElement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVehicleElement.Location = New System.Drawing.Point(310, 90)
        Me.lblVehicleElement.Name = "lblVehicleElement"
        Me.lblVehicleElement.Size = New System.Drawing.Size(101, 15)
        Me.lblVehicleElement.TabIndex = 103
        Me.lblVehicleElement.Text = "Vehicle Element"
        Me.lblVehicleElement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToPeriod
        '
        Me.cboToPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToPeriod.DropDownWidth = 230
        Me.cboToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(417, 33)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboToPeriod.TabIndex = 1
        '
        'cboVehicleElement
        '
        Me.cboVehicleElement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVehicleElement.DropDownWidth = 180
        Me.cboVehicleElement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVehicleElement.FormattingEnabled = True
        Me.cboVehicleElement.Location = New System.Drawing.Point(417, 87)
        Me.cboVehicleElement.Name = "cboVehicleElement"
        Me.cboVehicleElement.Size = New System.Drawing.Size(126, 21)
        Me.cboVehicleElement.TabIndex = 5
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(151, 157)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(392, 21)
        Me.cboEmployee.TabIndex = 7
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(549, 157)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 100
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 159)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(137, 15)
        Me.lblEmployee.TabIndex = 99
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnOtherInformation
        '
        Me.lnOtherInformation.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnOtherInformation.Location = New System.Drawing.Point(8, 138)
        Me.lnOtherInformation.Name = "lnOtherInformation"
        Me.lnOtherInformation.Size = New System.Drawing.Size(562, 15)
        Me.lnOtherInformation.TabIndex = 97
        Me.lnOtherInformation.Text = "Other Information"
        '
        'lblTINNo
        '
        Me.lblTINNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTINNo.Location = New System.Drawing.Point(8, 115)
        Me.lblTINNo.Name = "lblTINNo"
        Me.lblTINNo.Size = New System.Drawing.Size(137, 15)
        Me.lblTINNo.TabIndex = 95
        Me.lblTINNo.Text = "TIN No."
        Me.lblTINNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTINNo
        '
        Me.cboTINNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTINNo.DropDownWidth = 230
        Me.cboTINNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTINNo.FormattingEnabled = True
        Me.cboTINNo.Location = New System.Drawing.Point(151, 114)
        Me.cboTINNo.Name = "cboTINNo"
        Me.cboTINNo.Size = New System.Drawing.Size(126, 21)
        Me.cboTINNo.TabIndex = 4
        '
        'cboPensionsThirdTier
        '
        Me.cboPensionsThirdTier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPensionsThirdTier.DropDownWidth = 230
        Me.cboPensionsThirdTier.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPensionsThirdTier.FormattingEnabled = True
        Me.cboPensionsThirdTier.Location = New System.Drawing.Point(417, 60)
        Me.cboPensionsThirdTier.Name = "cboPensionsThirdTier"
        Me.cboPensionsThirdTier.Size = New System.Drawing.Size(126, 21)
        Me.cboPensionsThirdTier.TabIndex = 3
        '
        'LblCurrencyRate
        '
        Me.LblCurrencyRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrencyRate.Location = New System.Drawing.Point(342, 89)
        Me.LblCurrencyRate.Name = "LblCurrencyRate"
        Me.LblCurrencyRate.Size = New System.Drawing.Size(101, 15)
        Me.LblCurrencyRate.TabIndex = 78
        Me.LblCurrencyRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAccomodationElement
        '
        Me.cboAccomodationElement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccomodationElement.DropDownWidth = 230
        Me.cboAccomodationElement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccomodationElement.FormattingEnabled = True
        Me.cboAccomodationElement.Location = New System.Drawing.Point(151, 87)
        Me.cboAccomodationElement.Name = "cboAccomodationElement"
        Me.cboAccomodationElement.Size = New System.Drawing.Size(126, 21)
        Me.cboAccomodationElement.TabIndex = 6
        '
        'lblAccomodationElement
        '
        Me.lblAccomodationElement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccomodationElement.Location = New System.Drawing.Point(8, 90)
        Me.lblAccomodationElement.Name = "lblAccomodationElement"
        Me.lblAccomodationElement.Size = New System.Drawing.Size(137, 15)
        Me.lblAccomodationElement.TabIndex = 77
        Me.lblAccomodationElement.Text = "Accomodation Element"
        Me.lblAccomodationElement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(390, 4)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(131, 17)
        Me.lnkAnalysisBy.TabIndex = 74
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboPensionsSSF
        '
        Me.cboPensionsSSF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPensionsSSF.DropDownWidth = 230
        Me.cboPensionsSSF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPensionsSSF.FormattingEnabled = True
        Me.cboPensionsSSF.Location = New System.Drawing.Point(151, 60)
        Me.cboPensionsSSF.Name = "cboPensionsSSF"
        Me.cboPensionsSSF.Size = New System.Drawing.Size(126, 21)
        Me.cboPensionsSSF.TabIndex = 2
        '
        'lblPensionsSSF
        '
        Me.lblPensionsSSF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPensionsSSF.Location = New System.Drawing.Point(8, 63)
        Me.lblPensionsSSF.Name = "lblPensionsSSF"
        Me.lblPensionsSSF.Size = New System.Drawing.Size(137, 15)
        Me.lblPensionsSSF.TabIndex = 68
        Me.lblPensionsSSF.Text = "Pensions SSF"
        Me.lblPensionsSSF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPensionsThirdTier
        '
        Me.lblPensionsThirdTier.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPensionsThirdTier.Location = New System.Drawing.Point(310, 64)
        Me.lblPensionsThirdTier.Name = "lblPensionsThirdTier"
        Me.lblPensionsThirdTier.Size = New System.Drawing.Size(101, 15)
        Me.lblPensionsThirdTier.TabIndex = 55
        Me.lblPensionsThirdTier.Text = "Pensions Third Tier"
        Me.lblPensionsThirdTier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(137, 15)
        Me.lblPeriod.TabIndex = 57
        Me.lblPeriod.Text = "From Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(151, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'gbBasicSalaryOtherEarning
        '
        Me.gbBasicSalaryOtherEarning.BorderColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.Checked = False
        Me.gbBasicSalaryOtherEarning.CollapseAllExceptThis = False
        Me.gbBasicSalaryOtherEarning.CollapsedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapseOnLoad = False
        Me.gbBasicSalaryOtherEarning.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbBasicSalaryOtherEarning.ExpandedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicSalaryOtherEarning.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicSalaryOtherEarning.HeaderHeight = 25
        Me.gbBasicSalaryOtherEarning.HeaderMessage = ""
        Me.gbBasicSalaryOtherEarning.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBasicSalaryOtherEarning.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.HeightOnCollapse = 0
        Me.gbBasicSalaryOtherEarning.LeftTextSpace = 0
        Me.gbBasicSalaryOtherEarning.Location = New System.Drawing.Point(12, 305)
        Me.gbBasicSalaryOtherEarning.Name = "gbBasicSalaryOtherEarning"
        Me.gbBasicSalaryOtherEarning.OpenHeight = 300
        Me.gbBasicSalaryOtherEarning.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicSalaryOtherEarning.ShowBorder = True
        Me.gbBasicSalaryOtherEarning.ShowCheckBox = True
        Me.gbBasicSalaryOtherEarning.ShowCollapseButton = False
        Me.gbBasicSalaryOtherEarning.ShowDefaultBorderColor = True
        Me.gbBasicSalaryOtherEarning.ShowDownButton = False
        Me.gbBasicSalaryOtherEarning.ShowHeader = True
        Me.gbBasicSalaryOtherEarning.Size = New System.Drawing.Size(578, 61)
        Me.gbBasicSalaryOtherEarning.TabIndex = 88
        Me.gbBasicSalaryOtherEarning.Temp = 0
        Me.gbBasicSalaryOtherEarning.Text = "Basic Salary As Other Earning"
        Me.gbBasicSalaryOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.objbtnSearchOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.cboOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblOtherEarning)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(573, 33)
        Me.pnlAdvanceTaken.TabIndex = 259
        '
        'objbtnSearchOtherEarning
        '
        Me.objbtnSearchOtherEarning.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOtherEarning.BorderSelected = False
        Me.objbtnSearchOtherEarning.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOtherEarning.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOtherEarning.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOtherEarning.Location = New System.Drawing.Point(289, 6)
        Me.objbtnSearchOtherEarning.Name = "objbtnSearchOtherEarning"
        Me.objbtnSearchOtherEarning.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOtherEarning.TabIndex = 57
        '
        'cboOtherEarning
        '
        Me.cboOtherEarning.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEarning.FormattingEnabled = True
        Me.cboOtherEarning.Location = New System.Drawing.Point(97, 6)
        Me.cboOtherEarning.Name = "cboOtherEarning"
        Me.cboOtherEarning.Size = New System.Drawing.Size(186, 21)
        Me.cboOtherEarning.TabIndex = 0
        '
        'lblOtherEarning
        '
        Me.lblOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherEarning.Location = New System.Drawing.Point(8, 8)
        Me.lblOtherEarning.Name = "lblOtherEarning"
        Me.lblOtherEarning.Size = New System.Drawing.Size(83, 16)
        Me.lblOtherEarning.TabIndex = 0
        Me.lblOtherEarning.Text = "Other Earning"
        Me.lblOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 506)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(705, 55)
        Me.EZeeFooter1.TabIndex = 89
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(412, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(508, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(604, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'objbtnSearchAccomodationElement
        '
        Me.objbtnSearchAccomodationElement.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAccomodationElement.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAccomodationElement.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAccomodationElement.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAccomodationElement.BorderSelected = False
        Me.objbtnSearchAccomodationElement.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAccomodationElement.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAccomodationElement.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAccomodationElement.Location = New System.Drawing.Point(283, 87)
        Me.objbtnSearchAccomodationElement.Name = "objbtnSearchAccomodationElement"
        Me.objbtnSearchAccomodationElement.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAccomodationElement.TabIndex = 108
        '
        'objbtnSearchVehicleElement
        '
        Me.objbtnSearchVehicleElement.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVehicleElement.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVehicleElement.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVehicleElement.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVehicleElement.BorderSelected = False
        Me.objbtnSearchVehicleElement.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVehicleElement.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVehicleElement.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVehicleElement.Location = New System.Drawing.Point(549, 87)
        Me.objbtnSearchVehicleElement.Name = "objbtnSearchVehicleElement"
        Me.objbtnSearchVehicleElement.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVehicleElement.TabIndex = 109
        '
        'frmMonthlyTaxDeductionSchedule
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(705, 561)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.gbBasicSalaryOtherEarning)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmMonthlyTaxDeductionSchedule"
        Me.Text = "frmMonthlyTaxDeductionSchedule"
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbBasicSalaryOtherEarning.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblToPeriod As System.Windows.Forms.Label
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents lblVehicleElement As System.Windows.Forms.Label
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboVehicleElement As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lnOtherInformation As eZee.Common.eZeeLine
    Friend WithEvents lblTINNo As System.Windows.Forms.Label
    Friend WithEvents cboTINNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboPensionsThirdTier As System.Windows.Forms.ComboBox
    Friend WithEvents LblCurrencyRate As System.Windows.Forms.Label
    Friend WithEvents cboAccomodationElement As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccomodationElement As System.Windows.Forms.Label
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents cboPensionsSSF As System.Windows.Forms.ComboBox
    Friend WithEvents lblPensionsSSF As System.Windows.Forms.Label
    Friend WithEvents lblPensionsThirdTier As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents gbBasicSalaryOtherEarning As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchOtherEarning As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOtherEarning As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherEarning As System.Windows.Forms.Label
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchVehicleElement As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchAccomodationElement As eZee.Common.eZeeGradientButton
End Class
