'************************************************************************************************************************************
'Class Name :clsMauritius_PAYE_Report.vb
'Purpose    :
'Date       :13-Oct-2017
'Written By :Sandeep J Sharma
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

#End Region

Public Class clsMauritius_PAYE_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsMauritius_PAYE_Report"
    Private mstrReportId As String = enArutiReport.Mauritius_PAYE_Report    '197
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub
#End Region

#Region " Private variables "

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty
    Private mintPAYEHeadUnkID As Integer = 0
    Private mstrPAYEHeadName As String = String.Empty
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = ""
    Private mintEmolumentHeadId As Integer = 0
    Private mstrEmolumentHeadName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _PAYEHeadId() As Integer
        Set(ByVal value As Integer)
            mintPAYEHeadUnkID = value
        End Set
    End Property

    Public WriteOnly Property _PAYEHeadName() As String
        Set(ByVal value As String)
            mstrPAYEHeadName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _EmolumentHeadId() As Integer
        Set(ByVal value As Integer)
            mintEmolumentHeadId = value
        End Set
    End Property

    Public WriteOnly Property _EmolumentHeadName() As String
        Set(ByVal value As String)
            mstrEmolumentHeadName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrReport_GroupName = String.Empty
            mintPAYEHeadUnkID = 0
            mstrPAYEHeadName = String.Empty
            mintMembershipId = 0
            mstrMembershipName = ""
            mintEmolumentHeadId = 0
            mstrEmolumentHeadName = String.Empty
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Public Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                          ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal dtPeriodStartDate As Date, _
                                          ByVal dtPeriodEnd As Date, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean, _
                                          ByVal intBaseCurrencyId As Integer, _
                                          ByVal strExportPath As String, _
                                          ByVal strFmtCurrency As String) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            objDataOperation = New clsDataOperation
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'S.SANDEEP [23-OCT-2017] -- START
            'StrQ = "select " & _
            '       "     membershipno as EmployeeID " & _
            '       "    ,hremployee_master.surname as surname " & _
            '       "    ,hremployee_master.firstname + ' ' + hremployee_master.othername as ename " & _
            '       "    ,cast(emolument.amount as decimal(36," & decDecimalPlaces & ")) as eamount " & _
            '       "    ,cast(paye.amount as decimal(36," & decDecimalPlaces & ")) as pamount " & _
            '       "from prpayrollprocess_tran " & _
            '       "    join hremployee_master on hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
            '       "    join prtnaleave_tran on prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '       "    left join hremployee_meminfo_tran on prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
            '       "        and hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid and isnull(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
            '       "    left join hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid and hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid "
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'StrQ &= "    join " & _
            '        "    ( " & _
            '        "        select " & _
            '        "             hremployee_master.employeeunkid " & _
            '        "            ,amount " & _
            '        "        from prpayrollprocess_tran " & _
            '        "            join hremployee_master on hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
            '        "            join prtnaleave_tran on prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'StrQ &= "        where prpayrollprocess_tran.isvoid = 0 and prtnaleave_tran.isvoid = 0 " & _
            '        "            and payperiodunkid = @payperiodunkid AND prpayrollprocess_tran.tranheadunkid = @emolumentid " & _
            '        "    ) as emolument on emolument.employeeunkid = hremployee_master.employeeunkid " & _
            '        "    join " & _
            '        "    ( " & _
            '        "        select " & _
            '        "             hremployee_master.employeeunkid " & _
            '        "            ,amount " & _
            '        "        from prpayrollprocess_tran " & _
            '        "            join hremployee_master on hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
            '        "            join prtnaleave_tran on prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'StrQ &= "        where prpayrollprocess_tran.isvoid = 0 and prtnaleave_tran.isvoid = 0 " & _
            '        "            and payperiodunkid = @payperiodunkid AND prpayrollprocess_tran.tranheadunkid = @payeid " & _
            '        "    ) as paye on paye.employeeunkid = hremployee_master.employeeunkid " & _
            '        "where prpayrollprocess_tran.isvoid = 0 and prtnaleave_tran.isvoid = 0 " & _
            '        "and payperiodunkid = @payperiodunkid AND hrmembership_master.membershipunkid = @membershipunkid "

            StrQ = "select " & _
                   "     isnull(tinno.EmployeeID,'') as EmployeeID " & _
                   "    ,hremployee_master.surname as surname " & _
                   "    ,hremployee_master.firstname + ' ' + hremployee_master.othername as ename " & _
                   "    ,cast(emolument.amount as decimal(36," & decDecimalPlaces & ")) as eamount " & _
                   "    ,cast(prpayrollprocess_tran.amount as decimal(36," & decDecimalPlaces & ")) as pamount " & _
                   "from prpayrollprocess_tran " & _
                   "    join hremployee_master on hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            StrQ &= "   join prtnaleave_tran on prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    "   left join " & _
                    "    ( " & _
                    "        select " & _
                    "             hremployee_master.employeeunkid " & _
                    "            ,amount " & _
                    "        from prpayrollprocess_tran " & _
                    "           join hremployee_master on hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            StrQ &= "           join prtnaleave_tran on prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    "       where prpayrollprocess_tran.isvoid = 0 and prtnaleave_tran.isvoid = 0 " & _
                    "       and payperiodunkid = @payperiodunkid and prpayrollprocess_tran.tranheadunkid = @emolumentid " & _
                    "    ) as emolument on emolument.employeeunkid = hremployee_master.employeeunkid " & _
                    "   left join " & _
                    "    ( " & _
                    "       select distinct " & _
                    "            membershipno as EmployeeID " & _
                    "           ,hremployee_master.employeeunkid " & _
                    "       from hremployee_master " & _
                    "           left join hremployee_meminfo_tran on hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                    "           left join hrmembership_master on hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            StrQ &= "       where hrmembership_master.membershipunkid = @membershipunkid and hremployee_meminfo_tran.isactive = 1 " & _
                    "           and hrmembership_master.isactive = 1 and isdeleted = 0 " & _
                    "   ) as tinno on tinno.employeeunkid = hremployee_master.employeeunkid " & _
                    "where prpayrollprocess_tran.isvoid = 0 and prtnaleave_tran.isvoid = 0 " & _
                    "and payperiodunkid = @payperiodunkid and prpayrollprocess_tran.tranheadunkid = @payeid "
            'S.SANDEEP [23-OCT-2017] -- END

            objDataOperation.AddParameter("@payeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPAYEHeadUnkID)
            objDataOperation.AddParameter("@emolumentid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmolumentHeadId)
            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim sb As New StringBuilder()
            sb.Append("MNS,PAYE,V1.0" & vbCrLf)
            sb.Append(Language.getMessage(mstrModuleName, 100, "Employer Registration Number").Replace(",", "") & "," & _
                      Language.getMessage(mstrModuleName, 101, "Employer Business Registration Number").Replace(",", "") & "," & _
                      Language.getMessage(mstrModuleName, 102, "Employer Name").Replace(",", "") & "," & _
                      Language.getMessage(mstrModuleName, 103, "Tax Period").Replace(",", "") & "," & _
                      Language.getMessage(mstrModuleName, 104, "Telephone Number").Replace(",", "") & "," & _
                      Language.getMessage(mstrModuleName, 105, "Mobile Number").Replace(",", "") & "," & _
                      Language.getMessage(mstrModuleName, 106, "Name of Declarant").Replace(",", "") & "," & _
                      Language.getMessage(mstrModuleName, 107, "Email Address").Replace(",", "") & vbCrLf)
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = intCompanyUnkid
            sb.Append(objCompany._Tinno.ToString().Replace(",", "") & "," & _
                      objCompany._Company_Reg_No.ToString().Replace(",", "") & "," & _
                      objCompany._Name.Replace(",", "") & "," & _
                      dtPeriodEnd.Year.ToString().Substring(2, 2) & dtPeriodEnd.Month.ToString("0#") & "," & _
                      objCompany._Phone1.Replace(",", "") & "," & _
                      objCompany._Phone2.Replace(",", "") & "," & _
                      objCompany._Name.Replace(",", "") & "," & _
                      objCompany._Email.Replace(",", "") & vbCrLf)
            objCompany = Nothing

            sb.Append(Language.getMessage(mstrModuleName, 108, "Employee ID").Replace(",", "") & "," & _
                      Language.getMessage(mstrModuleName, 109, "Surname of Employee").Replace(",", "") & "," & _
                      Language.getMessage(mstrModuleName, 110, "Other Names of Employee").Replace(",", "") & "," & _
                      mstrEmolumentHeadName.Replace(",", "") & "," & _
                      Language.getMessage(mstrModuleName, 111, "PAYE Amount").Replace(",", "") & vbCrLf)

            For Each row As DataRow In dsList.Tables("DataTable").Rows
                sb.Append(row("EmployeeID").ToString.Replace(",", "") & "," & _
                          row("surname").ToString.Replace(",", "") & "," & _
                          row("ename").ToString.Replace(",", "") & "," & _
                          Format(CDec(row("eamount")), strFmtCurrency).ToString.Replace(",", "") & "," & _
                          Format(CDec(row("pamount")), strFmtCurrency).ToString.Replace(",", "") & vbCrLf)
            Next

            If sb.ToString.Length > 0 Then
                Try
                    System.IO.File.WriteAllText(strExportPath, sb.ToString)
                    blnFlag = True
                Catch ex As Exception
                End Try
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Employer Registration Number")
            Language.setMessage(mstrModuleName, 101, "Employer Business Registration Number")
            Language.setMessage(mstrModuleName, 102, "Employer Name")
            Language.setMessage(mstrModuleName, 103, "Tax Period")
            Language.setMessage(mstrModuleName, 104, "Telephone Number")
            Language.setMessage(mstrModuleName, 105, "Mobile Number")
            Language.setMessage(mstrModuleName, 106, "Name of Declarant")
            Language.setMessage(mstrModuleName, 107, "Email Address")
            Language.setMessage(mstrModuleName, 108, "Employee ID")
            Language.setMessage(mstrModuleName, 109, "Surname of Employee")
            Language.setMessage(mstrModuleName, 110, "Other Names of Employee")
            Language.setMessage(mstrModuleName, 111, "PAYE Amount")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
