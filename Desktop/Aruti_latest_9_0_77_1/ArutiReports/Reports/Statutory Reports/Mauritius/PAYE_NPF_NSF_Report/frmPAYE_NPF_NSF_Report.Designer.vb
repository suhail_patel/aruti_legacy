﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPAYE_NPF_NSF_Report
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPAYE_NPF_NSF_Report))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboNSFHead = New System.Windows.Forms.ComboBox
        Me.lblNSFHead = New System.Windows.Forms.Label
        Me.cboNPFHead = New System.Windows.Forms.ComboBox
        Me.lblNPFHead = New System.Windows.Forms.Label
        Me.cboWageBillNSFHead = New System.Windows.Forms.ComboBox
        Me.lblWageBillNSFHead = New System.Windows.Forms.Label
        Me.cboWageBillNPFHead = New System.Windows.Forms.ComboBox
        Me.lblWageBillNPFHead = New System.Windows.Forms.Label
        Me.cboEmolumentHead = New System.Windows.Forms.ComboBox
        Me.lblEmolumentHead = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPAYE = New System.Windows.Forms.ComboBox
        Me.lblPayeHead = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblMembership = New System.Windows.Forms.Label
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbBasicSalaryOtherEarning = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.cboOtherEarning = New System.Windows.Forms.ComboBox
        Me.lblOtherEarning = New System.Windows.Forms.Label
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.SaveDialog = New System.Windows.Forms.SaveFileDialog
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbBasicSalaryOtherEarning.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 528)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(724, 55)
        Me.EZeeFooter1.TabIndex = 3
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(431, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(527, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(623, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(724, 60)
        Me.eZeeHeader.TabIndex = 4
        Me.eZeeHeader.Title = "PAYE NPF NSF Report"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbFilterCriteria.AutoScroll = True
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboNSFHead)
        Me.gbFilterCriteria.Controls.Add(Me.lblNSFHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboNPFHead)
        Me.gbFilterCriteria.Controls.Add(Me.lblNPFHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboWageBillNSFHead)
        Me.gbFilterCriteria.Controls.Add(Me.lblWageBillNSFHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboWageBillNPFHead)
        Me.gbFilterCriteria.Controls.Add(Me.lblWageBillNPFHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmolumentHead)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmolumentHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPAYE)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayeHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.lblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.gbBasicSalaryOtherEarning)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(407, 456)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNSFHead
        '
        Me.cboNSFHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNSFHead.FormattingEnabled = True
        Me.cboNSFHead.Location = New System.Drawing.Point(127, 196)
        Me.cboNSFHead.Name = "cboNSFHead"
        Me.cboNSFHead.Size = New System.Drawing.Size(239, 21)
        Me.cboNSFHead.TabIndex = 6
        '
        'lblNSFHead
        '
        Me.lblNSFHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNSFHead.Location = New System.Drawing.Point(8, 198)
        Me.lblNSFHead.Name = "lblNSFHead"
        Me.lblNSFHead.Size = New System.Drawing.Size(113, 17)
        Me.lblNSFHead.TabIndex = 122
        Me.lblNSFHead.Text = "NSF Head"
        Me.lblNSFHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNPFHead
        '
        Me.cboNPFHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNPFHead.FormattingEnabled = True
        Me.cboNPFHead.Location = New System.Drawing.Point(127, 169)
        Me.cboNPFHead.Name = "cboNPFHead"
        Me.cboNPFHead.Size = New System.Drawing.Size(239, 21)
        Me.cboNPFHead.TabIndex = 5
        '
        'lblNPFHead
        '
        Me.lblNPFHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNPFHead.Location = New System.Drawing.Point(8, 171)
        Me.lblNPFHead.Name = "lblNPFHead"
        Me.lblNPFHead.Size = New System.Drawing.Size(113, 17)
        Me.lblNPFHead.TabIndex = 120
        Me.lblNPFHead.Text = "NPF Head"
        Me.lblNPFHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboWageBillNSFHead
        '
        Me.cboWageBillNSFHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWageBillNSFHead.FormattingEnabled = True
        Me.cboWageBillNSFHead.Location = New System.Drawing.Point(127, 142)
        Me.cboWageBillNSFHead.Name = "cboWageBillNSFHead"
        Me.cboWageBillNSFHead.Size = New System.Drawing.Size(239, 21)
        Me.cboWageBillNSFHead.TabIndex = 4
        '
        'lblWageBillNSFHead
        '
        Me.lblWageBillNSFHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWageBillNSFHead.Location = New System.Drawing.Point(8, 144)
        Me.lblWageBillNSFHead.Name = "lblWageBillNSFHead"
        Me.lblWageBillNSFHead.Size = New System.Drawing.Size(113, 17)
        Me.lblWageBillNSFHead.TabIndex = 118
        Me.lblWageBillNSFHead.Text = "Wage Bill NSF Head"
        Me.lblWageBillNSFHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboWageBillNPFHead
        '
        Me.cboWageBillNPFHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWageBillNPFHead.FormattingEnabled = True
        Me.cboWageBillNPFHead.Location = New System.Drawing.Point(127, 115)
        Me.cboWageBillNPFHead.Name = "cboWageBillNPFHead"
        Me.cboWageBillNPFHead.Size = New System.Drawing.Size(239, 21)
        Me.cboWageBillNPFHead.TabIndex = 3
        '
        'lblWageBillNPFHead
        '
        Me.lblWageBillNPFHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWageBillNPFHead.Location = New System.Drawing.Point(8, 117)
        Me.lblWageBillNPFHead.Name = "lblWageBillNPFHead"
        Me.lblWageBillNPFHead.Size = New System.Drawing.Size(113, 17)
        Me.lblWageBillNPFHead.TabIndex = 116
        Me.lblWageBillNPFHead.Text = "Wage Bill NPF Head"
        Me.lblWageBillNPFHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmolumentHead
        '
        Me.cboEmolumentHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmolumentHead.FormattingEnabled = True
        Me.cboEmolumentHead.Location = New System.Drawing.Point(127, 223)
        Me.cboEmolumentHead.Name = "cboEmolumentHead"
        Me.cboEmolumentHead.Size = New System.Drawing.Size(239, 21)
        Me.cboEmolumentHead.TabIndex = 7
        '
        'lblEmolumentHead
        '
        Me.lblEmolumentHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmolumentHead.Location = New System.Drawing.Point(8, 225)
        Me.lblEmolumentHead.Name = "lblEmolumentHead"
        Me.lblEmolumentHead.Size = New System.Drawing.Size(113, 17)
        Me.lblEmolumentHead.TabIndex = 113
        Me.lblEmolumentHead.Text = "Emolument Head"
        Me.lblEmolumentHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(127, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(239, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(113, 17)
        Me.lblPeriod.TabIndex = 108
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPAYE
        '
        Me.cboPAYE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPAYE.FormattingEnabled = True
        Me.cboPAYE.Location = New System.Drawing.Point(127, 250)
        Me.cboPAYE.Name = "cboPAYE"
        Me.cboPAYE.Size = New System.Drawing.Size(239, 21)
        Me.cboPAYE.TabIndex = 8
        '
        'lblPayeHead
        '
        Me.lblPayeHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayeHead.Location = New System.Drawing.Point(8, 252)
        Me.lblPayeHead.Name = "lblPayeHead"
        Me.lblPayeHead.Size = New System.Drawing.Size(113, 17)
        Me.lblPayeHead.TabIndex = 111
        Me.lblPayeHead.Text = "PAYE"
        Me.lblPayeHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 180
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(127, 88)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(239, 21)
        Me.cboMembership.TabIndex = 2
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(8, 91)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(113, 17)
        Me.lblMembership.TabIndex = 107
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(127, 277)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 9
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'gbBasicSalaryOtherEarning
        '
        Me.gbBasicSalaryOtherEarning.BorderColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.Checked = False
        Me.gbBasicSalaryOtherEarning.CollapseAllExceptThis = False
        Me.gbBasicSalaryOtherEarning.CollapsedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapseOnLoad = False
        Me.gbBasicSalaryOtherEarning.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbBasicSalaryOtherEarning.ExpandedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicSalaryOtherEarning.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicSalaryOtherEarning.HeaderHeight = 25
        Me.gbBasicSalaryOtherEarning.HeaderMessage = ""
        Me.gbBasicSalaryOtherEarning.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBasicSalaryOtherEarning.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.HeightOnCollapse = 0
        Me.gbBasicSalaryOtherEarning.LeftTextSpace = 0
        Me.gbBasicSalaryOtherEarning.Location = New System.Drawing.Point(11, 328)
        Me.gbBasicSalaryOtherEarning.Name = "gbBasicSalaryOtherEarning"
        Me.gbBasicSalaryOtherEarning.OpenHeight = 300
        Me.gbBasicSalaryOtherEarning.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicSalaryOtherEarning.ShowBorder = True
        Me.gbBasicSalaryOtherEarning.ShowCheckBox = True
        Me.gbBasicSalaryOtherEarning.ShowCollapseButton = False
        Me.gbBasicSalaryOtherEarning.ShowDefaultBorderColor = True
        Me.gbBasicSalaryOtherEarning.ShowDownButton = False
        Me.gbBasicSalaryOtherEarning.ShowHeader = True
        Me.gbBasicSalaryOtherEarning.Size = New System.Drawing.Size(355, 61)
        Me.gbBasicSalaryOtherEarning.TabIndex = 10
        Me.gbBasicSalaryOtherEarning.Temp = 0
        Me.gbBasicSalaryOtherEarning.Text = "Basic Salary As Other Earning"
        Me.gbBasicSalaryOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.cboOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblOtherEarning)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(350, 33)
        Me.pnlAdvanceTaken.TabIndex = 259
        '
        'cboOtherEarning
        '
        Me.cboOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEarning.FormattingEnabled = True
        Me.cboOtherEarning.Location = New System.Drawing.Point(97, 6)
        Me.cboOtherEarning.Name = "cboOtherEarning"
        Me.cboOtherEarning.Size = New System.Drawing.Size(186, 21)
        Me.cboOtherEarning.TabIndex = 0
        '
        'lblOtherEarning
        '
        Me.lblOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherEarning.Location = New System.Drawing.Point(8, 8)
        Me.lblOtherEarning.Name = "lblOtherEarning"
        Me.lblOtherEarning.Size = New System.Drawing.Size(83, 16)
        Me.lblOtherEarning.TabIndex = 0
        Me.lblOtherEarning.Text = "Other Earning"
        Me.lblOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(279, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 78
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEmployee
        '
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(127, 61)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(239, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 64)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(113, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmPAYE_NPF_NSF_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 583)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPAYE_NPF_NSF_Report"
        Me.ShowInTaskbar = False
        Me.Text = "iTax Form B Report"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbBasicSalaryOtherEarning.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbBasicSalaryOtherEarning As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents cboOtherEarning As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherEarning As System.Windows.Forms.Label
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents SaveDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents cboEmolumentHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmolumentHead As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPAYE As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayeHead As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboNSFHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblNSFHead As System.Windows.Forms.Label
    Friend WithEvents cboNPFHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblNPFHead As System.Windows.Forms.Label
    Friend WithEvents cboWageBillNSFHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblWageBillNSFHead As System.Windows.Forms.Label
    Friend WithEvents cboWageBillNPFHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblWageBillNPFHead As System.Windows.Forms.Label
End Class
