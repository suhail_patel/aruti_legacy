'************************************************************************************************************************************
'Class Name : clsITaxFormBReport.vb
'Purpose    :
'Date       :29/03/2018
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsPAYE_NPF_NSF_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPAYE_NPF_NSF_Report"
    Private mstrReportId As String = enArutiReport.PAYE_NPF_NSF_Report
    Private menReport As enArutiReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "
    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintWageBill_NPFTranId As Integer = 0
    Private mstrWageBill_NPFTranName As String = String.Empty
    Private mintWageBill_NSFTranId As Integer = 0
    Private mstrWageBill_NSFTranName As String = String.Empty
    Private mintNPFTranId As Integer = 0
    Private mstrNPFTranName As String = String.Empty
    Private mintNSFTranId As Integer = 0
    Private mstrNSFTranName As String = String.Empty
    Private mintEmolumentsTranId As Integer = 0
    Private mstrEmolumentsTranName As String = String.Empty
    Private mintPAYETranId As Integer = 0
    Private mstrPAYETranName As String = String.Empty
    Private mintOtherEarningTranId As Integer = 0

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""

   

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _WageBill_NPFTranId() As Integer
        Set(ByVal value As Integer)
            mintWageBill_NPFTranId = value
        End Set
    End Property

    Public WriteOnly Property _WageBill_NPFTranName() As String
        Set(ByVal value As String)
            mstrWageBill_NPFTranName = value
        End Set
    End Property

    Public WriteOnly Property _WageBill_NSFTranId() As Integer
        Set(ByVal value As Integer)
            mintWageBill_NSFTranId = value
        End Set
    End Property

    Public WriteOnly Property _WageBill_NSFTranName() As String
        Set(ByVal value As String)
            mstrWageBill_NSFTranName = value
        End Set
    End Property

    Public WriteOnly Property _NPFTranId() As Integer
        Set(ByVal value As Integer)
            mintNPFTranId = value
        End Set
    End Property

    Public WriteOnly Property _NPFTranName() As String
        Set(ByVal value As String)
            mstrNPFTranName = value
        End Set
    End Property

    Public WriteOnly Property _NSFTranId() As Integer
        Set(ByVal value As Integer)
            mintNSFTranId = value
        End Set
    End Property

    Public WriteOnly Property _NSFTranName() As String
        Set(ByVal value As String)
            mstrNSFTranName = value
        End Set
    End Property

    Public WriteOnly Property _EmolumentsTranId() As Integer
        Set(ByVal value As Integer)
            mintEmolumentsTranId = value
        End Set
    End Property

    Public WriteOnly Property _EmolumentsTranName() As String
        Set(ByVal value As String)
            mstrEmolumentsTranName = value
        End Set
    End Property

    Public WriteOnly Property _PAYETranId() As Integer
        Set(ByVal value As Integer)
            mintPAYETranId = value
        End Set
    End Property

    Public WriteOnly Property _PAYETranName() As String
        Set(ByVal value As String)
            mstrPAYETranName = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property



    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GroupName = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrPeriodIds = ""
            mstrPeriodNames = ""
            mintMembershipId = 0
            mstrMembershipName = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintWageBill_NPFTranId = 0
            mstrWageBill_NPFTranName = ""
            mintWageBill_NSFTranId = 0
            mstrWageBill_NSFTranName = ""
            mintNPFTranId = 0
            mstrNPFTranName = ""
            mintNSFTranId = 0
            mstrNSFTranName = ""
            mintEmolumentsTranId = 0
            mstrEmolumentsTranName = ""
            mintPAYETranId = 0
            mstrPAYETranName = ""
            mintOtherEarningTranId = 0

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GroupName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@wagebill_npf_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWageBill_NPFTranId)
            objDataOperation.AddParameter("@wagebill_nsf_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWageBill_NSFTranId)
            objDataOperation.AddParameter("@npf_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNPFTranId)
            objDataOperation.AddParameter("@nsf_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNSFTranId)
            objDataOperation.AddParameter("@emoluments_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmolumentsTranId)
            objDataOperation.AddParameter("@paye_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPAYETranId)

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND empid = @EmpId "
            End If

            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Try

        Catch ex As Exception

        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 1, "Employee Code")))

            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ')", Language.getMessage(mstrModuleName, 2, "Employee Name")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer, _
                                           ByVal strExportPath As String, _
                                           ByVal strfmtCurrency As String) As Boolean

        Dim dsList As DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        'Dim StrInnerQ As String = String.Empty
        Dim objCompany As New clsCompany_Master With {._Companyunkid = intCompanyUnkid}
        Dim strBuilder As New StringBuilder
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            StrQ = "SELECT  basicpay  " & _
                          ", wagebill_npf " & _
                          ", wagebill_nsf " & _
                          ", npf " & _
                          ", nsf " & _
                          ", emoluments " & _
                          ", paye " & _
                          ", empid AS empid " & _
                          ", hremployee_master.employeecode " & _
                          ", hremployee_master.surname " & _
                          ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') AS othername " & _
                          ", ISNULL(M.membershipno,'') AS MembershipNo " & _
                          ", ISNULL(hremployee_master.present_address1, '') + ' ' + ISNULL(hremployee_master.present_address2, '') AS ADDRESS " & _
                          ", ISNULL(hremployee_master.paytypeunkid,0) AS paytypeunkid " & _
                          ", ptype.name AS PayType " & _
                          ", hremployee_master.paypointunkid " & _
                          ", ISNULL(prpaypoint_master.paypointname,'') AS paypointname " & _
                          ", ISNULL(hrmsConfiguration..cfcity_master.name, '') AS city " & _
                          ", CC.costcentercode AS CCCode " & _
                          ", CC.costcentername AS CCName " & _
                          ", Id " & _
                          ", GName " & _
                    "FROM    ( SELECT    SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay  " & _
                                      ", SUM(ISNULL(PAYE.wagebill_npf, 0)) AS wagebill_npf " & _
                                      ", SUM(ISNULL(PAYE.wagebill_nsf, 0)) AS wagebill_nsf " & _
                                      ", SUM(ISNULL(PAYE.npf, 0)) AS npf " & _
                                      ", SUM(ISNULL(PAYE.nsf, 0)) AS nsf " & _
                                      ", SUM(ISNULL(PAYE.emoluments, 0)) AS emoluments " & _
                                      ", SUM(ISNULL(PAYE.paye, 0)) AS paye " & _
                                      ", paye.empid AS empid " & _
                                      ", Id " & _
                                      ", GName " & _
                              "FROM      ( SELECT    CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary  " & _
                                                  ", 0 AS wagebill_npf " & _
                                                  ", 0 AS wagebill_nsf " & _
                                                  ", 0 AS npf " & _
                                                  ", 0 AS nsf " & _
                                                  ", 0 AS emoluments " & _
                                                  ", 0 AS paye " & _
                                                  ", vwPayroll.employeeunkid AS empid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "
            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND vwPayroll.tranheadunkid = @OtherEarningTranId  " & _
                        "AND vwPayroll.GroupID = " & enViewPayroll_Group.Earning & " "
            Else
                StrQ &= "AND vwPayroll.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & " AND (vwPayroll.typeof_id = " & CInt(enTypeOf.Salary) & "  OR vwPayroll.isbasicsalaryasotherearning = 1)  "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS wagebill_npf " & _
                                                  ", 0 AS wagebill_nsf " & _
                                                  ", 0 AS npf " & _
                                                  ", 0 AS nsf " & _
                                                  ", 0 AS emoluments " & _
                                                  ", 0 AS paye " & _
                                                  ", vwPayroll.employeeunkid AS empid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "
            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @wagebill_npf_id " & _
                                                    "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintEmployeeId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            End If

            If mintWageBill_NPFTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS wagebill_npf " & _
                                                  ", CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS wagebill_nsf " & _
                                                  ", 0 AS npf " & _
                                                  ", 0 AS nsf " & _
                                                  ", 0 AS emoluments " & _
                                                  ", 0 AS paye " & _
                                                  ", vwPayroll.employeeunkid AS empid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @wagebill_nsf_id " & _
                                                    "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintEmployeeId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            End If

            If mintWageBill_NSFTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS wagebill_npf " & _
                                                  ", 0 AS wagebill_nsf " & _
                                                  ", CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS npf " & _
                                                  ", 0 AS nsf " & _
                                                  ", 0 AS emoluments " & _
                                                  ", 0 AS paye " & _
                                                  ", vwPayroll.employeeunkid AS empid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @npf_id " & _
                                                    "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintEmployeeId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            End If

            If mintNPFTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If


            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS wagebill_npf " & _
                                                  ", 0 AS wagebill_nsf " & _
                                                  ", 0 AS npf " & _
                                                  ", CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS nsf " & _
                                                  ", 0 AS emoluments " & _
                                                  ", 0 AS paye " & _
                                                  ", vwPayroll.employeeunkid AS empid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @nsf_id " & _
                                                    "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintEmployeeId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            End If

            If mintNSFTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= "UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS wagebill_npf " & _
                                                  ", 0 AS wagebill_nsf " & _
                                                  ", 0 AS npf " & _
                                                  ", 0 AS nsf " & _
                                                  ", CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS emoluments " & _
                                                  ", 0 AS paye " & _
                                                  ", vwPayroll.employeeunkid AS empid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @emoluments_id " & _
                                                    "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintEmployeeId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            End If

            If mintEmolumentsTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= "UNION ALL " & _
                                         "SELECT    0 AS basicsalary  " & _
                                                 ", 0 AS wagebill_npf " & _
                                                 ", 0 AS wagebill_nsf " & _
                                                 ", 0 AS npf " & _
                                                 ", 0 AS nsf " & _
                                                 ", 0 AS emoluments " & _
                                                 ", CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS paye " & _
                                                 ", vwPayroll.employeeunkid AS empid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @paye_id " & _
                                                    "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintEmployeeId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            End If

            If mintEmolumentsTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= "                    ) AS PAYE " & _
                              "WHERE     1 = 1 " & _
                              "GROUP BY  empid  " & _
                                      ", Id " & _
                                      ", GName " & _
                            ") AS A " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
                            "LEFT JOIN cfcommon_master as ptype ON ptype.masterunkid = hremployee_master.paytypeunkid AND ptype.mastertype = " & clsCommon_Master.enCommonMaster.PAY_TYPE & " " & _
                            "LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = hremployee_master.present_post_townunkid " & _
                            "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid " & _
                            "LEFT JOIN prpaypoint_master ON prpaypoint_master.paypointunkid = hremployee_master.paypointunkid " & _
                            "LEFT JOIN ( SELECT  employeeunkid " & _
                                              ", membershipno = ISNULL(STUFF(( SELECT    '; ' + membershipno " & _
                                                                              "FROM      hremployee_master AS b "
            If xUACQry.Trim.Length > 0 Then
                StrQ &= System.Text.RegularExpressions.Regex.Replace(xUACQry, "\bhremployee_master\b", "b")
            End If

            StrQ &= "LEFT JOIN hremployee_meminfo_tran ON b.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                                                                              "WHERE     membershipno IS NOT NULL " & _
                                                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                                                        "AND b.employeeunkid = a.employeeunkid "

            If mintMembershipId > 0 Then
                StrQ &= "AND membershipunkid = @membershipunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & System.Text.RegularExpressions.Regex.Replace(xUACFiltrQry, "\bhremployee_master\b", "b")
            End If

            StrQ &= "                                                        FOR " & _
                                                                              "XML PATH('')), 1, 2, ''), '') " & _
                                       "FROM    hremployee_master a " & _
                                      "GROUP BY employeeunkid " & _
                                    ") AS M ON M.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= "WHERE   1 = 1 "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery


            If mintViewIndex > 0 Then
                StrQ &= " ORDER BY GNAME, hremployee_master.firstname + ISNULL(hremployee_master.othername, '') + hremployee_master.surname "
            Else
                StrQ &= " ORDER BY hremployee_master.firstname + ISNULL(hremployee_master.othername, '') + hremployee_master.surname "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strBuilder.Append(Language.getMessage(mstrModuleName, 3, "MRA").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 4, "PACO").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 5, "V1.0").Replace(",", ""))
            strBuilder.Append(vbCrLf)

            strBuilder.Append(Language.getMessage(mstrModuleName, 6, "Employer Registration Number").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 7, "Employer Business Registration Number").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 8, "Employer Name").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 9, "Tax Period").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 10, "Telephone Number").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 11, "Mobile Number").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 12, "Name of Declarant").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 13, "Email Address").Replace(",", ""))
            strBuilder.Append(vbCrLf)

            strBuilder.Append(objCompany._Tinno.Replace(",", ""))
            strBuilder.Append("," & objCompany._Company_Reg_No.Replace(",", ""))
            strBuilder.Append("," & objCompany._Name.Replace(",", ""))
            strBuilder.Append("," & Format(dtPeriodEnd, "yyMM"))
            strBuilder.Append("," & objCompany._Phone1.Replace(",", ""))
            strBuilder.Append("," & objCompany._Phone2.Replace(",", ""))
            strBuilder.Append("," & objCompany._Name.Replace(",", ""))
            strBuilder.Append("," & objCompany._Email.Replace(",", ""))
            strBuilder.Append(vbCrLf)

            strBuilder.Append(Language.getMessage(mstrModuleName, 14, "Employee ID").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 15, "Surname of Employee").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 16, "Other Names of Employee").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 17, "Contribution Code").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 18, "Pay Code").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 19, "Frequency").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 20, "Wage Bill (MUR)").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 21, "Wage Bill on which NPF is calculated (If Employer has opted to pay above threshold) (MUR)").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 22, "Wage Bill on which NSF is calculated (If Employee has opted to pay above threshold) (MUR)").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 23, "National Pension Fund Amount (NPF) (MUR)").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 24, "National Solidarity Fund Amount (NSF) (MUR)").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 25, "LEVY Applicable? (Y/N)").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 26, "Emoluments excluding travelling and end of year bonus").Replace(",", ""))
            strBuilder.Append("," & Language.getMessage(mstrModuleName, 27, "PAYE Amount (MUR)").Replace(",", ""))
            strBuilder.Append(vbCrLf)



            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                strBuilder.Append(dtRow.Item("MembershipNo").ToString) 'Column A
                strBuilder.Append("," & dtRow.Item("surname").ToString & "") 'Column B
                strBuilder.Append("," & dtRow.Item("othername").ToString & "") 'Column C
                strBuilder.Append("," & Language.getMessage(mstrModuleName, 28, "C").Replace(",", ""))
                strBuilder.Append("," & Language.getMessage(mstrModuleName, 29, "M").Replace(",", ""))
                strBuilder.Append("," & Language.getMessage(mstrModuleName, 30, "1").Replace(",", ""))
                strBuilder.Append("," & Format(dtRow.Item("basicpay"), strfmtCurrency).Replace(",", "") & "")
                strBuilder.Append("," & Format(dtRow.Item("wagebill_npf"), strfmtCurrency).Replace(",", "") & "")
                strBuilder.Append("," & Format(dtRow.Item("wagebill_nsf"), strfmtCurrency).Replace(",", "") & "")
                strBuilder.Append("," & Format(dtRow.Item("npf"), strfmtCurrency).Replace(",", "") & "")
                strBuilder.Append("," & Format(dtRow.Item("nsf"), strfmtCurrency).Replace(",", "") & "")
                strBuilder.Append("," & Language.getMessage(mstrModuleName, 31, "Y").Replace(",", ""))
                strBuilder.Append("," & Format(dtRow.Item("emoluments"), strfmtCurrency).Replace(",", "") & "")
                strBuilder.Append("," & Format(dtRow.Item("paye"), strfmtCurrency).Replace(",", "") & "")

                strBuilder.Append(vbCrLf)
            Next


            If SaveTextFile(strExportPath, strBuilder) Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_PayrollReport; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function SaveTextFile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try

            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code")
            Language.setMessage(mstrModuleName, 2, "Employee Name")
            Language.setMessage(mstrModuleName, 3, "MRA")
            Language.setMessage(mstrModuleName, 4, "PACO")
            Language.setMessage(mstrModuleName, 5, "V1.0")
            Language.setMessage(mstrModuleName, 6, "Employer Registration Number")
            Language.setMessage(mstrModuleName, 7, "Employer Business Registration Number")
            Language.setMessage(mstrModuleName, 8, "Employer Name")
            Language.setMessage(mstrModuleName, 9, "Tax Period")
            Language.setMessage(mstrModuleName, 10, "Telephone Number")
            Language.setMessage(mstrModuleName, 11, "Mobile Number")
            Language.setMessage(mstrModuleName, 12, "Name of Declarant")
            Language.setMessage(mstrModuleName, 13, "Email Address")
            Language.setMessage(mstrModuleName, 14, "Employee ID")
            Language.setMessage(mstrModuleName, 15, "Surname of Employee")
            Language.setMessage(mstrModuleName, 16, "Other Names of Employee")
            Language.setMessage(mstrModuleName, 17, "Contribution Code")
            Language.setMessage(mstrModuleName, 18, "Pay Code")
            Language.setMessage(mstrModuleName, 19, "Frequency")
            Language.setMessage(mstrModuleName, 20, "Wage Bill (MUR)")
            Language.setMessage(mstrModuleName, 21, "Wage Bill on which NPF is calculated (If Employer has opted to pay above threshold) (MUR)")
            Language.setMessage(mstrModuleName, 22, "Wage Bill on which NSF is calculated (If Employee has opted to pay above threshold) (MUR)")
            Language.setMessage(mstrModuleName, 23, "National Pension Fund Amount (NPF) (MUR)")
            Language.setMessage(mstrModuleName, 24, "National Solidarity Fund Amount (NSF) (MUR)")
            Language.setMessage(mstrModuleName, 25, "LEVY Applicable? (Y/N)")
            Language.setMessage(mstrModuleName, 26, "Emoluments excluding travelling and end of year bonus")
            Language.setMessage(mstrModuleName, 27, "PAYE Amount (MUR)")
            Language.setMessage(mstrModuleName, 28, "C")
            Language.setMessage(mstrModuleName, 29, "M")
            Language.setMessage(mstrModuleName, 30, "1")
            Language.setMessage(mstrModuleName, 31, "Y")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
