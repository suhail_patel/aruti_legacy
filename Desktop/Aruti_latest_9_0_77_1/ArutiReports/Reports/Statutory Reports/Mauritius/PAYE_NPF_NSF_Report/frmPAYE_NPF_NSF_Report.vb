'************************************************************************************************************************************
'Class Name : frmPAYE_NPF_NSF_Report.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPAYE_NPF_NSF_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPAYE_NPF_NSF_Report"
    Private objPAYENPF As clsPAYE_NPF_NSF_Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    Private mdtPeriodStartDate As DateTime = Nothing
    Private mdtPeriodEndDate As DateTime = Nothing
    Private mintFirstOpenPeriod As Integer = 0

    Private mstrSearchText As String = ""
#End Region

#Region " Contructor "

    Public Sub New()

        objPAYENPF = New clsPAYE_NPF_NSF_Report(User._Object._Languageunkid,Company._Object._Companyunkid)

        objPAYENPF.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Membership = 1
        WageBillNPF = 2
        WageBillNSF = 3
        NPF = 4
        NSF = 5
        Emolument = 6
        PAYE = 7
        Other_Earning = 8
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objperiod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objMembership As New clsmembership_master
        Dim objMaster As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.OPEN)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, 0)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Taxes)
            With cboPAYE
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
                Call SetDefaultSearchText(cboPAYE)
            End With

            dsCombos = objMembership.getListForCombo("Membership", True, , 0)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", True)
            With cboWageBillNPFHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboWageBillNPFHead)
            End With

            With cboWageBillNSFHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .SelectedValue = 0
                Call SetDefaultSearchText(cboWageBillNSFHead)
            End With

            With cboNPFHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .SelectedValue = 0
                Call SetDefaultSearchText(cboNPFHead)
            End With

            With cboNSFHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .SelectedValue = 0
                Call SetDefaultSearchText(cboNSFHead)
            End With

            With cboEmolumentHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .SelectedValue = 0
                Call SetDefaultSearchText(cboEmolumentHead)
            End With

            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboOtherEarning)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillEmployee()
        Dim objEmp As New clsEmployee_Master
        Dim dsCombos As DataSet
        Try
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, "Emp", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
                Call SetDefaultSearchText(cboEmployee)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        Finally
            objEmp = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboEmployee.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboWageBillNPFHead.SelectedValue = 0
            cboWageBillNSFHead.SelectedValue = 0
            cboNPFHead.SelectedValue = 0
            cboNSFHead.SelectedValue = 0
            cboEmolumentHead.SelectedValue = 0
            cboPAYE.SelectedValue = 0
            cboOtherEarning.SelectedValue = 0
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport

            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GroupName = ""

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.PAYE_NPF_NSF_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                        Case enHeadTypeId.WageBillNPF
                            cboWageBillNPFHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.WageBillNSF
                            cboWageBillNSFHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.NPF
                            cboNPFHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.NSF
                            cboNSFHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Emolument
                            cboEmolumentHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.PAYE
                            cboPAYE.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                        Case enHeadTypeId.Other_Earning
                            cboOtherEarning.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            If CInt(dsRow.Item("transactionheadid")) > 0 Then
                                gbBasicSalaryOtherEarning.Checked = True
                            Else
                                gbBasicSalaryOtherEarning.Checked = False
                            End If

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objPAYENPF.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select period to continue, Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select membership to continue, Membership is mandatory information."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            ElseIf CInt(cboNPFHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select NPF head to continue, NPF head is mandatory information."), enMsgBoxStyle.Information)
                cboNPFHead.Focus()
                Return False
            ElseIf CInt(cboNSFHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select NSF head to continue, NSF head is mandatory information."), enMsgBoxStyle.Information)
                cboNSFHead.Focus()
                Return False
            ElseIf CInt(cboEmolumentHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select emolument head to continue, Emolument head is mandatory information."), enMsgBoxStyle.Information)
                cboEmolumentHead.Focus()
                Return False
            ElseIf CInt(cboPAYE.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select paye to continue, PAYE is mandatory information."), enMsgBoxStyle.Information)
                cboPAYE.Focus()
                Return False
            End If

            objPAYENPF._PeriodIds = cboPeriod.SelectedValue.ToString
            objPAYENPF._PeriodNames = cboPeriod.Text
            objPAYENPF._EmployeeId = CInt(cboEmployee.SelectedValue)
            objPAYENPF._EmployeeName = cboEmployee.Text
            objPAYENPF._MembershipId = CInt(cboMembership.SelectedValue)
            objPAYENPF._MembershipName = cboMembership.Text
            objPAYENPF._WageBill_NPFTranId = CInt(cboWageBillNPFHead.SelectedValue)
            objPAYENPF._WageBill_NPFTranName = cboWageBillNPFHead.Text
            objPAYENPF._WageBill_NSFTranId = CInt(cboWageBillNSFHead.SelectedValue)
            objPAYENPF._WageBill_NSFTranName = cboWageBillNSFHead.Text
            objPAYENPF._NPFTranId = CInt(cboNPFHead.SelectedValue)
            objPAYENPF._NPFTranName = cboNPFHead.Text
            objPAYENPF._NSFTranId = CInt(cboNSFHead.SelectedValue)
            objPAYENPF._NSFTranName = cboNSFHead.Text
            objPAYENPF._EmolumentsTranId = CInt(cboEmolumentHead.SelectedValue)
            objPAYENPF._EmolumentsTranName = cboEmolumentHead.Text
            objPAYENPF._PAYETranId = CInt(cboPAYE.SelectedValue)
            objPAYENPF._PAYETranName = cboPAYE.Text

            objPAYENPF._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)

            objPAYENPF._ViewByIds = mstrStringIds
            objPAYENPF._ViewIndex = mintViewIdx
            objPAYENPF._ViewByName = mstrStringName
            objPAYENPF._Analysis_Fields = mstrAnalysis_Fields
            objPAYENPF._Analysis_Join = mstrAnalysis_Join
            objPAYENPF._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPAYENPF._Report_GroupName = mstrReport_GroupName

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objPAYENPF._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(mdtPeriodEndDate))
            'Sohail (03 Aug 2019) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try

    End Function

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 7, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefauSetRegularFontltSearchText", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPAYE_NPF_NSF_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPAYENPF = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPAYE_NPF_NSF_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPAYE_NPF_NSF_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            ElseIf e.Control AndAlso e.KeyCode = Keys.E Then
                Call btnExport.PerformClick()
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPAYE_NPF_NSF_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPAYE_NPF_NSF_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me.eZeeHeader.Title = objPAYENPF._ReportName

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPAYE_NPF_NSF_Report_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Dim svDialog As New FolderBrowserDialog
            If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim stringFileName As String = "PAYE_" & cboPeriod.Text & "_" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & Format(Now, "hhmmss") & ".csv"
                Dim stringFilePath As String = svDialog.SelectedPath & "\" & stringFileName
                If objPAYENPF.Generate_DetailReport(strDatabaseName:=FinancialYear._Object._DatabaseName, intUserUnkid:=User._Object._Userunkid, intYearUnkid:=FinancialYear._Object._YearUnkid, intCompanyUnkid:=Company._Object._Companyunkid, dtPeriodStart:=mdtPeriodStartDate, dtPeriodEnd:=mdtPeriodEndDate, strUserModeSetting:=ConfigParameter._Object._UserAccessModeSetting, blnOnlyApproved:=True, intBaseCurrencyId:=ConfigParameter._Object._Base_CurrencyId, strExportPath:=stringFilePath, strfmtCurrency:=GUI.fmtCurrency) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Problem in exporting report."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Report Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPAYE_NPF_NSF_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsPAYE_NPF_NSF_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.PAYE_NPF_NSF_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_NPF_NSF_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.WageBillNPF
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboWageBillNPFHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_NPF_NSF_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.WageBillNSF
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboWageBillNSFHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_NPF_NSF_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.NPF
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboNPFHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_NPF_NSF_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.NSF
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboNSFHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_NPF_NSF_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Emolument
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmolumentHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_NPF_NSF_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.PAYE
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboPAYE.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_NPF_NSF_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Other_Earning
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOtherEarning.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_NPF_NSF_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
            Else
                mdtPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            Call FillEmployee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress, _
                                                                                                                cboWageBillNPFHead.KeyPress, cboWageBillNSFHead.KeyPress, _
                                                                                                                cboNPFHead.KeyPress, cboNSFHead.KeyPress, _
                                                                                                                cboEmolumentHead.KeyPress, cboPAYE.KeyPress, cboOtherEarning.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboEmployee.Name Then
                        .CodeMember = "employeecode"
                    Else
                        .CodeMember = "code"
                    End If

                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, _
                                                                                                      cboWageBillNPFHead.SelectedIndexChanged, cboWageBillNSFHead.SelectedIndexChanged, _
                                                                                                      cboNPFHead.SelectedIndexChanged, cboNSFHead.SelectedIndexChanged, _
                                                                                                      cboEmolumentHead.SelectedIndexChanged, cboPAYE.SelectedIndexChanged, cboOtherEarning.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus, _
                                                                                          cboWageBillNPFHead.GotFocus, cboWageBillNSFHead.GotFocus, _
                                                                                          cboNPFHead.GotFocus, cboNSFHead.GotFocus, _
                                                                                          cboEmolumentHead.GotFocus, cboPAYE.GotFocus, cboOtherEarning.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave, _
                                                                                       cboWageBillNPFHead.Leave, cboWageBillNSFHead.Leave, _
                                                                                       cboNPFHead.Leave, cboNSFHead.Leave, _
                                                                                       cboEmolumentHead.Leave, cboPAYE.Leave, cboOtherEarning.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNPFHead_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboWageBillNPFHead.Validating, cboWageBillNSFHead.Validating, _
                                                                                                                        cboNPFHead.Validating, cboNSFHead.Validating, _
                                                                                                                        cboEmolumentHead.Validating
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteria.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboEmployee.Name AndAlso t.Name <> cboMembership.Name AndAlso t.Name <> cboPAYE.Name AndAlso t.Name <> cboPeriod.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Validating", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub gbFilterCriteria_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles gbFilterCriteria.Scroll
        Try
            gbFilterCriteria.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbFilterCriteria_Scroll", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GroupName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblEmolumentHead.Text = Language._Object.getCaption(Me.lblEmolumentHead.Name, Me.lblEmolumentHead.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblPayeHead.Text = Language._Object.getCaption(Me.lblPayeHead.Name, Me.lblPayeHead.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblNSFHead.Text = Language._Object.getCaption(Me.lblNSFHead.Name, Me.lblNSFHead.Text)
			Me.lblNPFHead.Text = Language._Object.getCaption(Me.lblNPFHead.Name, Me.lblNPFHead.Text)
			Me.lblWageBillNSFHead.Text = Language._Object.getCaption(Me.lblWageBillNSFHead.Name, Me.lblWageBillNSFHead.Text)
			Me.lblWageBillNPFHead.Text = Language._Object.getCaption(Me.lblWageBillNPFHead.Name, Me.lblWageBillNPFHead.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select period to continue, Period is mandatory information.")
			Language.setMessage(mstrModuleName, 2, "Please select membership to continue, Membership is mandatory information.")
			Language.setMessage(mstrModuleName, 3, "Please select NPF head to continue, NPF head is mandatory information.")
			Language.setMessage(mstrModuleName, 4, "Please select NSF head to continue, NSF head is mandatory information.")
			Language.setMessage(mstrModuleName, 5, "Please select emolument head to continue, Emolument head is mandatory information.")
			Language.setMessage(mstrModuleName, 6, "Please select paye to continue, PAYE is mandatory information.")
			Language.setMessage(mstrModuleName, 7, "Type to Search")
			Language.setMessage(mstrModuleName, 8, "Problem in exporting report.")
			Language.setMessage(mstrModuleName, 9, "Report Exported Successfully.")
			Language.setMessage(mstrModuleName, 10, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 11, "Sorry, This transaction head is already mapped.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>


End Class
