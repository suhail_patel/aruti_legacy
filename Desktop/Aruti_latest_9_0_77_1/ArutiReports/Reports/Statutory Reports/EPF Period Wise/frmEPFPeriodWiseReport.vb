'************************************************************************************************************************************
'Class Name : frmEPFPeriodWiseReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEPFPeriodWiseReport
    Private ReadOnly mstrModuleName As String = "frmEPFPeriodWiseReport"
    Private objMS_NSSF As clsEPFPeriodWiseReport
    Private dsPeriods As New DataSet

#Region " Private Variables "

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintPaidCurrencyId As Integer = 0

    'Sohail (19 Mar 2015) -- Start
    'Enhancement - Providing closed year to date period on EPF Period Wise Report.
    Private mintFirstOpenPeriod As Integer = 0
    'Sohail (19 Mar 2015) -- End

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mDicDataBaseYearId As Dictionary(Of String, Integer)
    Private mDicDataBaseEndDate As Dictionary(Of String, String)
    'S.SANDEEP [04 JUN 2015] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        objMS_NSSF = New clsEPFPeriodWiseReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objperiod As New clscommom_period_Tran
        Dim objMember As New clsmembership_master
        Dim objEmpContribution As New clsTransactionHead
        Dim objMaster As New clsMasterData 'Sohail (19 Mar 2015)
        Try
            dsCombos = objMember.getListForCombo("Membership", True, , 1)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (19 Mar 2015) -- Start
            'Enhancement - Providing closed year to date period on EPF Period Wise Report.
            'dsPeriods = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsPeriods = objperiod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsPeriods = objperiod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            'Sohail (19 Mar 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsPeriods.Tables(0).Copy
                'Sohail (19 Mar 2015) -- Start
                'Enhancement - Providing closed year to date period on EPF Period Wise Report.
                '.SelectedValue = 0
                .SelectedValue = mintFirstOpenPeriod
                'Sohail (19 Mar 2015) -- End
            End With
            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsPeriods.Tables(0).Copy
                'Sohail (19 Mar 2015) -- Start
                'Enhancement - Providing closed year to date period on EPF Period Wise Report.
                '.SelectedValue = 0
                .SelectedValue = mintFirstOpenPeriod
                'Sohail (19 Mar 2015) -- End
            End With

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", True)
            cboCurrency.ValueMember = "countryunkid"
            cboCurrency.DisplayMember = "currency_sign"
            cboCurrency.DataSource = dsCombos.Tables("Currency")

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End

            'Sohail (16 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objperiod = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Public Sub FillEmpList()
        Dim objEmployee As New clsEmployee_Master
        Try
            lvEmployee.Items.Clear()

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Dim dsList As DataSet = objEmployee.GetEmployeeList("Employee", False)
            Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, True, "Employee", False)
            'S.SANDEEP [04 JUN 2015] -- END

            Dim dtTable As DataTable = New DataView(dsList.Tables("Employee"), "", "employeename", DataViewRowState.CurrentRows).ToTable
            Dim lvItem As ListViewItem
            RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = dtRow.Item("employeename").ToString
                lvItem.Tag = CInt(dtRow.Item("employeeunkid").ToString)
                lvEmployee.Items.Add(lvItem)
            Next
            lvEmployee.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmpList", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvEmployee.Items
                RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboMembership.SelectedValue = 0
            cboCurrency.SelectedValue = 0
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            'Sohail (19 Mar 2015) -- Start
            'Enhancement - Providing closed year to date period on EPF Period Wise Report.
            'cboPeriod.SelectedValue = 0
            'cboToPeriod.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboToPeriod.SelectedValue = mintFirstOpenPeriod
            'Sohail (19 Mar 2015) -- End
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            cboOtherEarning.SelectedValue = 0
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            mDicDataBaseYearId = Nothing
            mDicDataBaseEndDate = Nothing
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        'Sohail (19 Mar 2015) -- Start
        'Enhancement - Providing closed year to date period on EPF Period Wise Report.
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim arrDatabaseName As New ArrayList
        Dim arrAllDatabaseName As New ArrayList
        Dim intPrevYearID As Integer = 0
        'Sohail (19 Mar 2015) -- End
        Try
            objMS_NSSF.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please Select From Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 And CInt(cboToPeriod.SelectedValue) > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Please Select From Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Function
            End If
            If CInt(cboPeriod.SelectedValue) > 0 And CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Please Select To Period."), enMsgBoxStyle.Information)
                cboToPeriod.Select()
                Exit Function
            End If

            If cboToPeriod.SelectedIndex < cboPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, To Period cannot be less than From Period"), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Function
            End If

            If CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            End If

            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If

            If lvEmployee.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Please check atleast one employee to continue."), enMsgBoxStyle.Information)
                lvEmployee.Focus()
                Return False
            End If

            Dim strPeriodIDs As String = String.Empty
            Dim i As Integer = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            mDicDataBaseYearId = New Dictionary(Of String, Integer)
            mDicDataBaseEndDate = New Dictionary(Of String, String)
            'S.SANDEEP [04 JUN 2015] -- END

            For Each dsRow As DataRow In dsPeriods.Tables(0).Rows
                If i >= cboPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strPeriodIDs &= "," & dsRow.Item("periodunkid").ToString
                    'Sohail (19 Mar 2015) -- Start
                    'Enhancement - Providing closed year to date period on EPF Period Wise Report.
                    If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                        dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
                        If dsList.Tables("Database").Rows.Count > 0 Then
                            arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                        End If
                    End If
                    intPrevYearID = CInt(dsRow.Item("yearunkid"))
                    'Sohail (19 Mar 2015) -- End
                End If
                i += 1
            Next
            If strPeriodIDs.Trim <> "" Then strPeriodIDs = Mid(strPeriodIDs, 2)
            'Sohail (19 Mar 2015) -- Start
            'Enhancement - Providing closed year to date period on EPF Period Wise Report.
            If arrDatabaseName.Count <= 0 Then Return False
            Dim dsDB As DataSet = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", 0)
            Dim a() As String = (From p In dsDB.Tables(0) Select (p.Item("database_name").ToString)).ToArray
            arrAllDatabaseName.AddRange(a)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            mDicDataBaseYearId = dsDB.Tables(0).AsEnumerable().ToDictionary(Of String, Integer)(Function(x) x.Field(Of String)("database_name"), Function(y) y.Field(Of Integer)("yearunkid"))
            mDicDataBaseEndDate = dsDB.Tables(0).AsEnumerable().ToDictionary(Of String, String)(Function(x) x.Field(Of String)("database_name"), Function(x) x.Field(Of String)("end_date"))

            objMS_NSSF._DicDataBaseEndDate = mDicDataBaseEndDate
            objMS_NSSF._DicDataBaseYearId = mDicDataBaseYearId
            'S.SANDEEP [04 JUN 2015] -- END


            'Sohail (19 Mar 2015) -- End

            If (mintBaseCurrId = mintPaidCurrencyId) Or mintPaidCurrencyId <= 0 Then
                mintPaidCurrencyId = mintBaseCurrId
            End If
            'objMS_NSSF._CountryId = CInt(cboCurrency.SelectedValue)
            'objMS_NSSF._BaseCurrencyId = mintBaseCurrId
            'objMS_NSSF._PaidCurrencyId = mintPaidCurrencyId
            'If mdecBaseExRate > 0 AndAlso mdecPaidExRate Then
            '    objMS_NSSF._ConversionRate = mdecPaidExRate / mdecBaseExRate
            'End If
            'objMS_NSSF._ExchangeRate = LblCurrencyRate.Text

            'Nilay (13-Oct-2016) -- Start
            'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
            'objMS_NSSF._ShowBasicSalary = ConfigParameter._Object._ShowBasicSalaryOnStatutoryReport
            Dim objMember As New clsmembership_master
            objMember._Membershipunkid = CInt(cboMembership.SelectedValue)
            objMS_NSSF._ShowBasicSalary = objMember._IsShowBasicSalary
            'Nilay (13-Oct-2016) -- End

            If gbBasicSalaryOtherEarning.Checked = True Then
                objMS_NSSF._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objMS_NSSF._OtherEarningTranId = 0
            End If
            objMS_NSSF._PeriodIds = strPeriodIDs
            'Sohail (19 Mar 2015) -- Start
            'Enhancement - Providing closed year to date period on EPF Period Wise Report.
            objMS_NSSF._Arr_DatabaseName = arrDatabaseName
            objMS_NSSF._Arr_AllDatabaseName = arrAllDatabaseName
            objMS_NSSF._TranDatabaseName = FinancialYear._Object._DatabaseName
            objMS_NSSF._UserAccessLevelFilterString = UserAccessLevel._AccessLevelFilterString
            'Sohail (19 Mar 2015) -- End
            objMS_NSSF._MembershipId = cboMembership.SelectedValue
            objMS_NSSF._MembershipName = cboMembership.Text
            objMS_NSSF._ViewByIds = mstrStringIds
            objMS_NSSF._ViewIndex = mintViewIdx
            objMS_NSSF._ViewByName = mstrStringName
            objMS_NSSF._Analysis_Fields = mstrAnalysis_Fields
            objMS_NSSF._Analysis_Join = mstrAnalysis_Join
            objMS_NSSF._Report_GroupName = mstrReport_GroupName
            objMS_NSSF._FromPeriodId = cboPeriod.SelectedValue
            objMS_NSSF._FromPeriodName = cboPeriod.Text
            objMS_NSSF._ToPeriodId = cboToPeriod.SelectedValue
            objMS_NSSF._ToPeriodName = cboToPeriod.Text
            Dim strEmployeeId As String = String.Empty
            For Each lvItem As ListViewItem In lvEmployee.CheckedItems
                strEmployeeId &= "" & lvItem.Tag.ToString & ","
            Next
            strEmployeeId = strEmployeeId.Substring(0, strEmployeeId.Length - 1)
            objMS_NSSF._EmployeeIds = strEmployeeId

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmEPFPeriodWiseReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objMS_NSSF = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEPFPeriodWiseReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEPFPeriodWiseReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Me._Title = objMS_NSSF._ReportName
            Me._Message = objMS_NSSF._ReportDesc
            Call FillCombo()
            Call FillEmpList()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEPFPeriodWiseReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEPFPeriodWiseReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEPFPeriodWiseReport_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEPFPeriodWiseReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEPFPeriodWiseReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEPFPeriodWiseReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEPFPeriodWiseReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub frmEPFPeriodWiseReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objMS_NSSF.generateReport(0, e.Type, enExportAction.None)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMS_NSSF.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                             User._Object._Userunkid, _
            '                             FinancialYear._Object._YearUnkid, _
            '                             Company._Object._Companyunkid, _
            '                             Nothing, Nothing, ConfigParameter._Object._UserAccessModeSetting, _
            '                             True, ConfigParameter._Object._ExportReportPath, _
            '                             ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            objMS_NSSF.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEPFPeriodWiseReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEPFPeriodWiseReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objMS_NSSF.generateReport(0, enPrintAction.None, e.Type)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objMS_NSSF.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEPFPeriodWiseReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEPFPeriodWiseReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEPFPeriodWiseReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEPFPeriodWiseReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEPFPeriodWiseReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Controls "

    Private Sub chkSelectallEmployeeList_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectallEmployeeList.CheckedChanged
        Try
            Call CheckAllEmployee(chkSelectallEmployeeList.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectallEmployeeList_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvEmployee_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployee.ItemChecked
        Try
            RemoveHandler chkSelectallEmployeeList.CheckedChanged, AddressOf chkSelectallEmployeeList_CheckedChanged
            If lvEmployee.CheckedItems.Count <= 0 Then
                chkSelectallEmployeeList.CheckState = CheckState.Unchecked
            ElseIf lvEmployee.CheckedItems.Count < lvEmployee.Items.Count Then
                chkSelectallEmployeeList.CheckState = CheckState.Indeterminate
            ElseIf lvEmployee.CheckedItems.Count = lvEmployee.Items.Count Then
                chkSelectallEmployeeList.CheckState = CheckState.Checked
            End If
            AddHandler chkSelectallEmployeeList.CheckedChanged, AddressOf chkSelectallEmployeeList_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmployee_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            lvEmployee.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvEmployee.FindItemWithText(txtSearchEmp.Text, False, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvEmployee.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'frm.CodeMember = "trnheadcode"
            frm.CodeMember = "code"
            'Sohail (16 Mar 2016) -- End
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.LblTo.Text = Language._Object.getCaption(Me.LblTo.Name, Me.LblTo.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
			Me.lblSearchEmp.Text = Language._Object.getCaption(Me.lblSearchEmp.Name, Me.lblSearchEmp.Text)
			Me.chkSelectallEmployeeList.Text = Language._Object.getCaption(Me.chkSelectallEmployeeList.Name, Me.chkSelectallEmployeeList.Text)
			Me.ColumnHeader2.Text = Language._Object.getCaption(CStr(Me.ColumnHeader2.Tag), Me.ColumnHeader2.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Please Select From Period.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Please Select From Period.")
			Language.setMessage(mstrModuleName, 3, "Sorry, Please Select To Period.")
			Language.setMessage(mstrModuleName, 4, "Sorry, To Period cannot be less than From Period")
			Language.setMessage(mstrModuleName, 5, "Membership is mandatory information. Please select Membership to continue.")
			Language.setMessage(mstrModuleName, 6, "Sorry, Please select transaction head.")
			Language.setMessage(mstrModuleName, 7, "Sorry, Please check atleast one employee to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
