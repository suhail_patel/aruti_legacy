'************************************************************************************************************************************
'Class Name : frmPPFContribution.vb
'Purpose    : 
'Written By : Anjan
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPPFContribution

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPPFContribution"
    Private objContribution As clsPPFContribution
    Private eReport As enArutiReport

    'Sohail (14 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (14 Mar 2013) -- End

    'Pinkal (02-May-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintPaidCurrencyId As Integer = 0
    'Pinkal (02-May-2013) -- End

#End Region

#Region " Contructor "

    Public Sub New(ByVal menReport As enArutiReport, ByVal intLangId As Integer, ByVal intCompanyId As Integer)

        eReport = menReport
        'Anjan (20 Mar 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        objContribution = New clsPPFContribution(menReport, intLangId, intCompanyId)
        'Anjan (20 Mar 2012)-End 

        objContribution.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objMember As New clsmembership_master
        Dim objMaster As New clsMasterData
        Dim dsCombos As New DataSet
        Dim objperiod As New clscommom_period_Tran
        Dim objEmpContribution As New clsTransactionHead
        Try
            'dsCombos = objEmp.GetEmployeeList("Emp", True, True)
            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombos.Tables(0)
            '    .SelectedValue = 0
            'End With


            dsCombos = objMember.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'dsCombos = objMaster.getComboListForHeadType("HeadType")
            'With cboEmpHeadType
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombos.Tables(0)
            '    .SelectedValue = 0
            'End With

            'With cboCoHeadType
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombos.Tables(0).Copy
            '    .SelectedValue = 0
            'End With
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("EmpContribution", True, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Employee_Statutory_Contributions)
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "EmpContribution", True, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Employee_Statutory_Contributions)
            'Sohail (21 Aug 2015) -- End
            With cboEmpHeadType
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("EmpContribution")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("EmployerContribution", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "EmployerContribution", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            'Sohail (21 Aug 2015) -- End
            With cboCoHeadType
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("EmployerContribution")
                .SelectedValue = 0
            End With

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End

            'Sohail (16 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
            End With
            'Sohail (28 Aug 2013) -- End

            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", True)
            cboCurrency.ValueMember = "countryunkid"
            cboCurrency.DisplayMember = "currency_sign"
            cboCurrency.DataSource = dsCombos.Tables("Currency")

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If

            'Pinkal (02-May-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objEmp = Nothing
            dsCombos = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboCoHeadType.SelectedValue = 0
            cboEmpHeadType.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboOtherEarning.SelectedValue = 0 'Sohail (28 Aug 2013)
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport 'Sohail (07 Sep 2013)
            objContribution.setDefaultOrderBy(0)
            txtOrderBy.Text = objContribution.OrderByDisplay

            'Sohail (14 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            'Sohail (14 Mar 2013) -- End

            'cboCTypeOf.SelectedValue = 0
            'cboETypeOf.SelectedValue = 0
            'cboMembership.SelectedValue = 0
            

            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes
            cboCurrency.SelectedValue = 0
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            'Pinkal (02-May-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            Call objContribution.SetDefaultValue()

            'If CInt(cboMembership.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
            '    cboMembership.Focus()
            '    Return False
            'End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboEmpHeadType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee Contribution is mandatory information. Please select Employee Contribution to continue."), enMsgBoxStyle.Information)
                cboEmpHeadType.Focus()
                Return False
            End If

            'If CInt(cboETypeOf.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Contribution Type is mandatory information. Please select Employee Contribution Type to continue."), enMsgBoxStyle.Information)
            '    cboETypeOf.Focus()
            '    Return False
            'End If

            If CInt(cboCoHeadType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employer Contribution is mandatory information. Please select Employer Contribution to continue."), enMsgBoxStyle.Information)
                cboCoHeadType.Focus()
                Return False
            End If

            'If CInt(cboCTypeOf.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employer Contribution Type is mandatory information. Please select Employer Contribution Type to continue."), enMsgBoxStyle.Information)
            '    cboCTypeOf.Focus()
            '    Return False
            'End If

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If
            'Sohail (28 Aug 2013) -- End

            If CInt(cboMembership.SelectedValue) > 0 Then
                objContribution._MembershipId = cboMembership.SelectedValue
                objContribution._MembershipName = cboMembership.Text
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objContribution._PeriodId = cboPeriod.SelectedValue
                objContribution._PeriodName = cboPeriod.Text
            End If

            If CInt(cboEmpHeadType.SelectedValue) > 0 Then
                objContribution._EmpHeadTypeId = cboEmpHeadType.SelectedValue
            End If

            'If CInt(cboETypeOf.SelectedValue) > 0 Then
            '    objContribution._EmpTypeOfId = cboETypeOf.SelectedValue
            'End If

            If CInt(cboCoHeadType.SelectedValue) > 0 Then
                objContribution._EmprHeadTypeId = cboCoHeadType.SelectedValue
            End If

            'If CInt(cboCTypeOf.SelectedValue) > 0 Then
            '    objContribution._EmprTypeOfId = cboCTypeOf.SelectedValue
            'End If

            'S.SANDEEP [ 28 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Nilay (13-Oct-2016) -- Start
            'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
            'objContribution._ShowBasicSalary = ConfigParameter._Object._ShowBasicSalaryOnStatutoryReport
            objContribution._ShowBasicSalary = CBool(chkShowBasicSalary.Checked)
            'Nilay (13-Oct-2016) -- End

            'S.SANDEEP [ 28 FEB 2013 ] -- END

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If gbBasicSalaryOtherEarning.Checked = True Then
                objContribution._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objContribution._OtherEarningTranId = 0
            End If
            'Sohail (28 Aug 2013) -- End

            'Sohail (14 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            objContribution._ViewByIds = mstrStringIds
            objContribution._ViewIndex = mintViewIdx
            objContribution._ViewByName = mstrStringName
            objContribution._Analysis_Fields = mstrAnalysis_Fields
            objContribution._Analysis_Join = mstrAnalysis_Join
            objContribution._Report_GroupName = mstrReport_GroupName
            'Sohail (14 Mar 2013) -- End


            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes

            If (mintBaseCurrId = mintPaidCurrencyId) Or mintPaidCurrencyId <= 0 Then
                mintPaidCurrencyId = mintBaseCurrId
            End If

            objContribution._CountryId = CInt(cboCurrency.SelectedValue)
            objContribution._BaseCurrencyId = mintBaseCurrId
            objContribution._PaidCurrencyId = mintPaidCurrencyId

            If mdecBaseExRate > 0 AndAlso mdecPaidExRate Then
                objContribution._ConversionRate = mdecPaidExRate / mdecBaseExRate
            End If

            objContribution._ExchangeRate = LblCurrencyRate.Text

            'Pinkal (02-May-2013) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter()", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmPPFContributionReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objContribution = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPPFContributionReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPPFContributionReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = objContribution._ReportName
            Me._Message = objContribution._ReportDesc

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Call FillCombo()
            Call ResetValue()

            'Sohail (14 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            If eReport = enArutiReport.PSPFContribution AndAlso ConfigParameter._Object._PSPFIntegration = enPSPFIntegration.EFT_PSPF Then
                lnkEFTPSPF.Visible = True
            Else
                lnkEFTPSPF.Visible = False
            End If
            'Sohail (14 Mar 2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPPFContributionReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objContribution.generateReport(0, e.Type, enExportAction.None)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objContribution.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  ConfigParameter._Object._UserAccessModeSetting, _
            '                                  True, ConfigParameter._Object._ExportReportPath, _
            '                                  ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            objContribution._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objContribution.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              objPeriod._Start_Date, _
                                              objPeriod._End_Date, _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, ConfigParameter._Object._ExportReportPath, _
                                              ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objContribution.generateReport(0, enPrintAction.None, e.Type)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objContribution.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  ConfigParameter._Object._UserAccessModeSetting, _
            '                                  True, ConfigParameter._Object._ExportReportPath, _
            '                                  ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            objContribution._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objContribution.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              objPeriod._Start_Date, _
                                              objPeriod._End_Date, _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, ConfigParameter._Object._ExportReportPath, _
                                              ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

   

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPPFContribution.SetMessages()

            Select Case eReport
                Case enArutiReport.PPFContribution
                    objfrm._Other_ModuleNames = "clsPPFContribution"
                Case enArutiReport.PSPFContribution
                    objfrm._Other_ModuleNames = "clsPSPFContribution"
                Case enArutiReport.GEPFContribution
                    objfrm._Other_ModuleNames = "clsGEPFContribution"

            End Select


            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objContribution.setOrderBy(0)
            txtOrderBy.Text = objContribution.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub cboCoHeadType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCoHeadType.SelectedIndexChanged
    '    Try
    '        If CInt(cboCoHeadType.SelectedValue) > 0 Then
    '            Dim objMaster As New clsMasterData
    '            Dim dsList As New DataSet
    '            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboCoHeadType.SelectedValue))
    '            With cboCTypeOf
    '                .ValueMember = "Id"
    '                .DisplayMember = "Name"
    '                .DataSource = dsList.Tables(0)
    '                .SelectedValue = 0
    '            End With
    '            objMaster = Nothing
    '            dsList = Nothing
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboCoHeadType_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboEmpHeadType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmpHeadType.SelectedIndexChanged
    '    Try
    '        If CInt(cboEmpHeadType.SelectedValue) > 0 Then
    '            Dim objMaster As New clsMasterData
    '            Dim dsList As New DataSet
    '            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboEmpHeadType.SelectedValue))
    '            With cboETypeOf
    '                .ValueMember = "Id"
    '                .DisplayMember = "Name"
    '                .DataSource = dsList.Tables(0)
    '                .SelectedValue = 0
    '            End With
    '            objMaster = Nothing
    '            dsList = Nothing
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboEmpHeadType_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Sohail (28 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (28 Aug 2013) -- End

#End Region

    'Sohail (14 Mar 2013) -- Start
    'TRA - ENHANCEMENT
#Region "LinkLabel Event"
    Private Sub lnkEFTPSPF_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFTPSPF.LinkClicked
        Try
            If Not SetFilter() Then Exit Sub

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objContribution.GenerateEFT_PSPF_DetailReport() = True Then
            If objContribution.GenerateEFT_PSPF_DetailReport(FinancialYear._Object._DatabaseName, _
                                                             User._Object._Userunkid, _
                                                             FinancialYear._Object._YearUnkid, _
                                                             Company._Object._Companyunkid, _
                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                             ConfigParameter._Object._UserAccessModeSetting, _
                                                             True, ConfigParameter._Object._ExportReportPath, _
                                                             ConfigParameter._Object._OpenAfterExport, ConfigParameter._Object._Base_CurrencyId) = True Then
                'S.SANDEEP [04 JUN 2015] -- END
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFTPSPF_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region
    'Sohail (14 Mar 2013) -- End

#Region "ComboBox Event"

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboCurrency.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                Dim dsList As DataSet = Nothing
                dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate")).ToTable

                If dtTable.Rows.Count > 0 Then
                    mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                    mintPaidCurrencyId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    LblCurrencyRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                Else
                    mdecBaseExRate = 0
                    mdecPaidExRate = 0
                    mintPaidCurrencyId = 0
                    LblCurrencyRate.Text = ""
                End If
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
                mintPaidCurrencyId = 0
                LblCurrencyRate.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmpContriHeadType.Text = Language._Object.getCaption(Me.lblEmpContriHeadType.Name, Me.lblEmpContriHeadType.Text)
			Me.lblCompanyHeadType.Text = Language._Object.getCaption(Me.lblCompanyHeadType.Name, Me.lblCompanyHeadType.Text)
			Me.lblEmpTypeOf.Text = Language._Object.getCaption(Me.lblEmpTypeOf.Name, Me.lblEmpTypeOf.Text)
			Me.lblCTypeOf.Text = Language._Object.getCaption(Me.lblCTypeOf.Name, Me.lblCTypeOf.Text)
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lblSortBy.Text = Language._Object.getCaption(Me.lblSortBy.Name, Me.lblSortBy.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lnkEFTPSPF.Text = Language._Object.getCaption(Me.lnkEFTPSPF.Name, Me.lnkEFTPSPF.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 2, "Employee Contribution is mandatory information. Please select Employee Contribution to continue.")
			Language.setMessage(mstrModuleName, 3, "Employer Contribution is mandatory information. Please select Employer Contribution to continue.")
			Language.setMessage(mstrModuleName, 4, "Please select transaction head.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
