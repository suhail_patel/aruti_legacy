﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPE_FormsReports
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPE_FormsReports))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblRType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.LblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblDateAsOn = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.lbltoDate = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.objchkA_All = New System.Windows.Forms.CheckBox
        Me.lvAllocation = New System.Windows.Forms.ListView
        Me.colhAllocations = New System.Windows.Forms.ColumnHeader
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.gbAllocationInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocSaveInfo = New System.Windows.Forms.LinkLabel
        Me.gbHeadSelection = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchTransHead = New eZee.Common.eZeeGradientButton
        Me.lnkTransHeadSelection = New System.Windows.Forms.LinkLabel
        Me.cboHealthInsurance = New System.Windows.Forms.ComboBox
        Me.lblHealthInsurance = New System.Windows.Forms.Label
        Me.gbMembershipSelection = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblMemberships = New System.Windows.Forms.Label
        Me.lnkMembershipSelection = New System.Windows.Forms.LinkLabel
        Me.objchkM_All = New System.Windows.Forms.CheckBox
        Me.lvMembership = New System.Windows.Forms.ListView
        Me.colhMembership = New System.Windows.Forms.ColumnHeader
        Me.objcolhEHeadId = New System.Windows.Forms.ColumnHeader
        Me.objcolhCHeadId = New System.Windows.Forms.ColumnHeader
        Me.gbIncPromotion = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchPromotion = New eZee.Common.eZeeGradientButton
        Me.cboPromotion = New System.Windows.Forms.ComboBox
        Me.lblPromtion = New System.Windows.Forms.Label
        Me.objbtnSearchIncrement = New eZee.Common.eZeeGradientButton
        Me.lnkSalarySelection = New System.Windows.Forms.LinkLabel
        Me.cboIncrement = New System.Windows.Forms.ComboBox
        Me.lblAnnIncrement = New System.Windows.Forms.Label
        Me.EZeeFooter1.SuspendLayout()
        Me.gbMandatoryInfo.SuspendLayout()
        Me.gbAllocationInfo.SuspendLayout()
        Me.gbHeadSelection.SuspendLayout()
        Me.gbMembershipSelection.SuspendLayout()
        Me.gbIncPromotion.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(807, 60)
        Me.eZeeHeader.TabIndex = 22
        Me.eZeeHeader.Title = ""
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 478)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(807, 55)
        Me.EZeeFooter1.TabIndex = 23
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(411, 13)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 6
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        Me.objbtnAdvanceFilter.Visible = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(514, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(610, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(706, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.lblRType)
        Me.gbMandatoryInfo.Controls.Add(Me.cboReportType)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.LblEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.cboEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.lblDateAsOn)
        Me.gbMandatoryInfo.Controls.Add(Me.dtpFromDate)
        Me.gbMandatoryInfo.Controls.Add(Me.lbltoDate)
        Me.gbMandatoryInfo.Controls.Add(Me.dtpToDate)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(386, 120)
        Me.gbMandatoryInfo.TabIndex = 24
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Filter Criteria"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(288, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 19
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkSetAnalysis.Visible = False
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(7, 64)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(95, 15)
        Me.lblPeriod.TabIndex = 98
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 145
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(108, 61)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(238, 21)
        Me.cboPeriod.TabIndex = 97
        '
        'lblRType
        '
        Me.lblRType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRType.Location = New System.Drawing.Point(7, 37)
        Me.lblRType.Name = "lblRType"
        Me.lblRType.Size = New System.Drawing.Size(95, 15)
        Me.lblRType.TabIndex = 95
        Me.lblRType.Text = "Report Type"
        Me.lblRType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 400
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(108, 34)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(238, 21)
        Me.cboReportType.TabIndex = 94
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(352, 88)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 60
        '
        'LblEmployee
        '
        Me.LblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployee.Location = New System.Drawing.Point(7, 91)
        Me.LblEmployee.Name = "LblEmployee"
        Me.LblEmployee.Size = New System.Drawing.Size(95, 15)
        Me.LblEmployee.TabIndex = 60
        Me.LblEmployee.Text = "Employee"
        Me.LblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(108, 88)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(238, 21)
        Me.cboEmployee.TabIndex = 59
        '
        'lblDateAsOn
        '
        Me.lblDateAsOn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateAsOn.Location = New System.Drawing.Point(7, 64)
        Me.lblDateAsOn.Name = "lblDateAsOn"
        Me.lblDateAsOn.Size = New System.Drawing.Size(95, 15)
        Me.lblDateAsOn.TabIndex = 74
        Me.lblDateAsOn.Text = "From Date"
        Me.lblDateAsOn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(108, 61)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpFromDate.TabIndex = 75
        '
        'lbltoDate
        '
        Me.lbltoDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltoDate.Location = New System.Drawing.Point(209, 64)
        Me.lbltoDate.Name = "lbltoDate"
        Me.lbltoDate.Size = New System.Drawing.Size(35, 15)
        Me.lbltoDate.TabIndex = 76
        Me.lbltoDate.Text = "To"
        Me.lbltoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(251, 61)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpToDate.TabIndex = 77
        '
        'objchkA_All
        '
        Me.objchkA_All.AutoSize = True
        Me.objchkA_All.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkA_All.Location = New System.Drawing.Point(318, 65)
        Me.objchkA_All.Name = "objchkA_All"
        Me.objchkA_All.Size = New System.Drawing.Size(15, 14)
        Me.objchkA_All.TabIndex = 64
        Me.objchkA_All.UseVisualStyleBackColor = True
        '
        'lvAllocation
        '
        Me.lvAllocation.CheckBoxes = True
        Me.lvAllocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhAllocations})
        Me.lvAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAllocation.FullRowSelect = True
        Me.lvAllocation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAllocation.HideSelection = False
        Me.lvAllocation.Location = New System.Drawing.Point(88, 59)
        Me.lvAllocation.Name = "lvAllocation"
        Me.lvAllocation.Size = New System.Drawing.Size(275, 246)
        Me.lvAllocation.TabIndex = 63
        Me.lvAllocation.UseCompatibleStateImageBehavior = False
        Me.lvAllocation.View = System.Windows.Forms.View.Details
        '
        'colhAllocations
        '
        Me.colhAllocations.Tag = "colhAllocations"
        Me.colhAllocations.Text = ""
        Me.colhAllocations.Width = 249
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(7, 35)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(75, 15)
        Me.lblAllocation.TabIndex = 62
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(88, 32)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(275, 21)
        Me.cboAllocations.TabIndex = 61
        '
        'gbAllocationInfo
        '
        Me.gbAllocationInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAllocationInfo.Checked = False
        Me.gbAllocationInfo.CollapseAllExceptThis = False
        Me.gbAllocationInfo.CollapsedHoverImage = Nothing
        Me.gbAllocationInfo.CollapsedNormalImage = Nothing
        Me.gbAllocationInfo.CollapsedPressedImage = Nothing
        Me.gbAllocationInfo.CollapseOnLoad = False
        Me.gbAllocationInfo.Controls.Add(Me.lnkAllocSaveInfo)
        Me.gbAllocationInfo.Controls.Add(Me.objchkA_All)
        Me.gbAllocationInfo.Controls.Add(Me.cboAllocations)
        Me.gbAllocationInfo.Controls.Add(Me.lblAllocation)
        Me.gbAllocationInfo.Controls.Add(Me.lvAllocation)
        Me.gbAllocationInfo.ExpandedHoverImage = Nothing
        Me.gbAllocationInfo.ExpandedNormalImage = Nothing
        Me.gbAllocationInfo.ExpandedPressedImage = Nothing
        Me.gbAllocationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAllocationInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAllocationInfo.HeaderHeight = 25
        Me.gbAllocationInfo.HeaderMessage = ""
        Me.gbAllocationInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAllocationInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAllocationInfo.HeightOnCollapse = 0
        Me.gbAllocationInfo.LeftTextSpace = 0
        Me.gbAllocationInfo.Location = New System.Drawing.Point(404, 66)
        Me.gbAllocationInfo.Name = "gbAllocationInfo"
        Me.gbAllocationInfo.OpenHeight = 300
        Me.gbAllocationInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAllocationInfo.ShowBorder = True
        Me.gbAllocationInfo.ShowCheckBox = False
        Me.gbAllocationInfo.ShowCollapseButton = False
        Me.gbAllocationInfo.ShowDefaultBorderColor = True
        Me.gbAllocationInfo.ShowDownButton = False
        Me.gbAllocationInfo.ShowHeader = True
        Me.gbAllocationInfo.Size = New System.Drawing.Size(375, 313)
        Me.gbAllocationInfo.TabIndex = 65
        Me.gbAllocationInfo.Temp = 0
        Me.gbAllocationInfo.Text = "Allocation"
        Me.gbAllocationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocSaveInfo
        '
        Me.lnkAllocSaveInfo.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocSaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocSaveInfo.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocSaveInfo.Location = New System.Drawing.Point(277, 4)
        Me.lnkAllocSaveInfo.Name = "lnkAllocSaveInfo"
        Me.lnkAllocSaveInfo.Size = New System.Drawing.Size(94, 17)
        Me.lnkAllocSaveInfo.TabIndex = 100
        Me.lnkAllocSaveInfo.TabStop = True
        Me.lnkAllocSaveInfo.Text = "Save Selection"
        Me.lnkAllocSaveInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gbHeadSelection
        '
        Me.gbHeadSelection.BorderColor = System.Drawing.Color.Black
        Me.gbHeadSelection.Checked = False
        Me.gbHeadSelection.CollapseAllExceptThis = False
        Me.gbHeadSelection.CollapsedHoverImage = Nothing
        Me.gbHeadSelection.CollapsedNormalImage = Nothing
        Me.gbHeadSelection.CollapsedPressedImage = Nothing
        Me.gbHeadSelection.CollapseOnLoad = False
        Me.gbHeadSelection.Controls.Add(Me.objbtnSearchTransHead)
        Me.gbHeadSelection.Controls.Add(Me.lnkTransHeadSelection)
        Me.gbHeadSelection.Controls.Add(Me.cboHealthInsurance)
        Me.gbHeadSelection.Controls.Add(Me.lblHealthInsurance)
        Me.gbHeadSelection.ExpandedHoverImage = Nothing
        Me.gbHeadSelection.ExpandedNormalImage = Nothing
        Me.gbHeadSelection.ExpandedPressedImage = Nothing
        Me.gbHeadSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHeadSelection.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbHeadSelection.HeaderHeight = 25
        Me.gbHeadSelection.HeaderMessage = ""
        Me.gbHeadSelection.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbHeadSelection.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbHeadSelection.HeightOnCollapse = 0
        Me.gbHeadSelection.LeftTextSpace = 0
        Me.gbHeadSelection.Location = New System.Drawing.Point(12, 192)
        Me.gbHeadSelection.Name = "gbHeadSelection"
        Me.gbHeadSelection.OpenHeight = 300
        Me.gbHeadSelection.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbHeadSelection.ShowBorder = True
        Me.gbHeadSelection.ShowCheckBox = False
        Me.gbHeadSelection.ShowCollapseButton = False
        Me.gbHeadSelection.ShowDefaultBorderColor = True
        Me.gbHeadSelection.ShowDownButton = False
        Me.gbHeadSelection.ShowHeader = True
        Me.gbHeadSelection.Size = New System.Drawing.Size(386, 64)
        Me.gbHeadSelection.TabIndex = 66
        Me.gbHeadSelection.Temp = 0
        Me.gbHeadSelection.Text = "Trans. Head Selection"
        Me.gbHeadSelection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchTransHead
        '
        Me.objbtnSearchTransHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTransHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTransHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTransHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTransHead.BorderSelected = False
        Me.objbtnSearchTransHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTransHead.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTransHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTransHead.Location = New System.Drawing.Point(352, 32)
        Me.objbtnSearchTransHead.Name = "objbtnSearchTransHead"
        Me.objbtnSearchTransHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTransHead.TabIndex = 102
        '
        'lnkTransHeadSelection
        '
        Me.lnkTransHeadSelection.BackColor = System.Drawing.Color.Transparent
        Me.lnkTransHeadSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkTransHeadSelection.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkTransHeadSelection.Location = New System.Drawing.Point(288, 4)
        Me.lnkTransHeadSelection.Name = "lnkTransHeadSelection"
        Me.lnkTransHeadSelection.Size = New System.Drawing.Size(94, 17)
        Me.lnkTransHeadSelection.TabIndex = 100
        Me.lnkTransHeadSelection.TabStop = True
        Me.lnkTransHeadSelection.Text = "Save Selection"
        Me.lnkTransHeadSelection.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboHealthInsurance
        '
        Me.cboHealthInsurance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHealthInsurance.DropDownWidth = 350
        Me.cboHealthInsurance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHealthInsurance.FormattingEnabled = True
        Me.cboHealthInsurance.Location = New System.Drawing.Point(108, 32)
        Me.cboHealthInsurance.Name = "cboHealthInsurance"
        Me.cboHealthInsurance.Size = New System.Drawing.Size(238, 21)
        Me.cboHealthInsurance.TabIndex = 61
        '
        'lblHealthInsurance
        '
        Me.lblHealthInsurance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHealthInsurance.Location = New System.Drawing.Point(7, 35)
        Me.lblHealthInsurance.Name = "lblHealthInsurance"
        Me.lblHealthInsurance.Size = New System.Drawing.Size(95, 15)
        Me.lblHealthInsurance.TabIndex = 62
        Me.lblHealthInsurance.Text = "Health Insurance"
        Me.lblHealthInsurance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbMembershipSelection
        '
        Me.gbMembershipSelection.BorderColor = System.Drawing.Color.Black
        Me.gbMembershipSelection.Checked = False
        Me.gbMembershipSelection.CollapseAllExceptThis = False
        Me.gbMembershipSelection.CollapsedHoverImage = Nothing
        Me.gbMembershipSelection.CollapsedNormalImage = Nothing
        Me.gbMembershipSelection.CollapsedPressedImage = Nothing
        Me.gbMembershipSelection.CollapseOnLoad = False
        Me.gbMembershipSelection.Controls.Add(Me.lblMemberships)
        Me.gbMembershipSelection.Controls.Add(Me.lnkMembershipSelection)
        Me.gbMembershipSelection.Controls.Add(Me.objchkM_All)
        Me.gbMembershipSelection.Controls.Add(Me.lvMembership)
        Me.gbMembershipSelection.ExpandedHoverImage = Nothing
        Me.gbMembershipSelection.ExpandedNormalImage = Nothing
        Me.gbMembershipSelection.ExpandedPressedImage = Nothing
        Me.gbMembershipSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMembershipSelection.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMembershipSelection.HeaderHeight = 25
        Me.gbMembershipSelection.HeaderMessage = ""
        Me.gbMembershipSelection.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMembershipSelection.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMembershipSelection.HeightOnCollapse = 0
        Me.gbMembershipSelection.LeftTextSpace = 0
        Me.gbMembershipSelection.Location = New System.Drawing.Point(12, 262)
        Me.gbMembershipSelection.Name = "gbMembershipSelection"
        Me.gbMembershipSelection.OpenHeight = 300
        Me.gbMembershipSelection.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMembershipSelection.ShowBorder = True
        Me.gbMembershipSelection.ShowCheckBox = False
        Me.gbMembershipSelection.ShowCollapseButton = False
        Me.gbMembershipSelection.ShowDefaultBorderColor = True
        Me.gbMembershipSelection.ShowDownButton = False
        Me.gbMembershipSelection.ShowHeader = True
        Me.gbMembershipSelection.Size = New System.Drawing.Size(386, 213)
        Me.gbMembershipSelection.TabIndex = 67
        Me.gbMembershipSelection.Temp = 0
        Me.gbMembershipSelection.Text = "Membership Selection"
        Me.gbMembershipSelection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMemberships
        '
        Me.lblMemberships.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMemberships.Location = New System.Drawing.Point(7, 34)
        Me.lblMemberships.Name = "lblMemberships"
        Me.lblMemberships.Size = New System.Drawing.Size(95, 15)
        Me.lblMemberships.TabIndex = 102
        Me.lblMemberships.Text = "Memberships"
        Me.lblMemberships.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkMembershipSelection
        '
        Me.lnkMembershipSelection.BackColor = System.Drawing.Color.Transparent
        Me.lnkMembershipSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkMembershipSelection.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkMembershipSelection.Location = New System.Drawing.Point(288, 4)
        Me.lnkMembershipSelection.Name = "lnkMembershipSelection"
        Me.lnkMembershipSelection.Size = New System.Drawing.Size(94, 17)
        Me.lnkMembershipSelection.TabIndex = 100
        Me.lnkMembershipSelection.TabStop = True
        Me.lnkMembershipSelection.Text = "Save Selection"
        Me.lnkMembershipSelection.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objchkM_All
        '
        Me.objchkM_All.AutoSize = True
        Me.objchkM_All.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkM_All.Location = New System.Drawing.Point(319, 37)
        Me.objchkM_All.Name = "objchkM_All"
        Me.objchkM_All.Size = New System.Drawing.Size(15, 14)
        Me.objchkM_All.TabIndex = 64
        Me.objchkM_All.UseVisualStyleBackColor = True
        '
        'lvMembership
        '
        Me.lvMembership.CheckBoxes = True
        Me.lvMembership.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhMembership, Me.objcolhEHeadId, Me.objcolhCHeadId})
        Me.lvMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvMembership.FullRowSelect = True
        Me.lvMembership.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvMembership.HideSelection = False
        Me.lvMembership.Location = New System.Drawing.Point(108, 32)
        Me.lvMembership.Name = "lvMembership"
        Me.lvMembership.Size = New System.Drawing.Size(238, 175)
        Me.lvMembership.TabIndex = 63
        Me.lvMembership.UseCompatibleStateImageBehavior = False
        Me.lvMembership.View = System.Windows.Forms.View.Details
        '
        'colhMembership
        '
        Me.colhMembership.Tag = "colhMembership"
        Me.colhMembership.Text = "Membership"
        Me.colhMembership.Width = 229
        '
        'objcolhEHeadId
        '
        Me.objcolhEHeadId.Tag = "objcolhEHeadId"
        Me.objcolhEHeadId.Width = 0
        '
        'objcolhCHeadId
        '
        Me.objcolhCHeadId.Tag = "objcolhCHeadId"
        Me.objcolhCHeadId.Width = 0
        '
        'gbIncPromotion
        '
        Me.gbIncPromotion.BorderColor = System.Drawing.Color.Black
        Me.gbIncPromotion.Checked = False
        Me.gbIncPromotion.CollapseAllExceptThis = False
        Me.gbIncPromotion.CollapsedHoverImage = Nothing
        Me.gbIncPromotion.CollapsedNormalImage = Nothing
        Me.gbIncPromotion.CollapsedPressedImage = Nothing
        Me.gbIncPromotion.CollapseOnLoad = False
        Me.gbIncPromotion.Controls.Add(Me.objbtnSearchPromotion)
        Me.gbIncPromotion.Controls.Add(Me.cboPromotion)
        Me.gbIncPromotion.Controls.Add(Me.lblPromtion)
        Me.gbIncPromotion.Controls.Add(Me.objbtnSearchIncrement)
        Me.gbIncPromotion.Controls.Add(Me.lnkSalarySelection)
        Me.gbIncPromotion.Controls.Add(Me.cboIncrement)
        Me.gbIncPromotion.Controls.Add(Me.lblAnnIncrement)
        Me.gbIncPromotion.ExpandedHoverImage = Nothing
        Me.gbIncPromotion.ExpandedNormalImage = Nothing
        Me.gbIncPromotion.ExpandedPressedImage = Nothing
        Me.gbIncPromotion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbIncPromotion.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbIncPromotion.HeaderHeight = 25
        Me.gbIncPromotion.HeaderMessage = ""
        Me.gbIncPromotion.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbIncPromotion.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbIncPromotion.HeightOnCollapse = 0
        Me.gbIncPromotion.LeftTextSpace = 0
        Me.gbIncPromotion.Location = New System.Drawing.Point(404, 385)
        Me.gbIncPromotion.Name = "gbIncPromotion"
        Me.gbIncPromotion.OpenHeight = 300
        Me.gbIncPromotion.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbIncPromotion.ShowBorder = True
        Me.gbIncPromotion.ShowCheckBox = False
        Me.gbIncPromotion.ShowCollapseButton = False
        Me.gbIncPromotion.ShowDefaultBorderColor = True
        Me.gbIncPromotion.ShowDownButton = False
        Me.gbIncPromotion.ShowHeader = True
        Me.gbIncPromotion.Size = New System.Drawing.Size(375, 90)
        Me.gbIncPromotion.TabIndex = 68
        Me.gbIncPromotion.Temp = 0
        Me.gbIncPromotion.Text = "Increment && Promotion Selection"
        Me.gbIncPromotion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchPromotion
        '
        Me.objbtnSearchPromotion.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPromotion.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPromotion.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPromotion.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPromotion.BorderSelected = False
        Me.objbtnSearchPromotion.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPromotion.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPromotion.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPromotion.Location = New System.Drawing.Point(342, 60)
        Me.objbtnSearchPromotion.Name = "objbtnSearchPromotion"
        Me.objbtnSearchPromotion.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPromotion.TabIndex = 106
        '
        'cboPromotion
        '
        Me.cboPromotion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPromotion.DropDownWidth = 350
        Me.cboPromotion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPromotion.FormattingEnabled = True
        Me.cboPromotion.Location = New System.Drawing.Point(88, 60)
        Me.cboPromotion.Name = "cboPromotion"
        Me.cboPromotion.Size = New System.Drawing.Size(248, 21)
        Me.cboPromotion.TabIndex = 104
        '
        'lblPromtion
        '
        Me.lblPromtion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPromtion.Location = New System.Drawing.Point(7, 63)
        Me.lblPromtion.Name = "lblPromtion"
        Me.lblPromtion.Size = New System.Drawing.Size(75, 15)
        Me.lblPromtion.TabIndex = 105
        Me.lblPromtion.Text = "Promotion"
        Me.lblPromtion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchIncrement
        '
        Me.objbtnSearchIncrement.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchIncrement.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchIncrement.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchIncrement.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchIncrement.BorderSelected = False
        Me.objbtnSearchIncrement.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchIncrement.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchIncrement.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchIncrement.Location = New System.Drawing.Point(342, 33)
        Me.objbtnSearchIncrement.Name = "objbtnSearchIncrement"
        Me.objbtnSearchIncrement.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchIncrement.TabIndex = 102
        '
        'lnkSalarySelection
        '
        Me.lnkSalarySelection.BackColor = System.Drawing.Color.Transparent
        Me.lnkSalarySelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSalarySelection.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSalarySelection.Location = New System.Drawing.Point(277, 4)
        Me.lnkSalarySelection.Name = "lnkSalarySelection"
        Me.lnkSalarySelection.Size = New System.Drawing.Size(94, 17)
        Me.lnkSalarySelection.TabIndex = 100
        Me.lnkSalarySelection.TabStop = True
        Me.lnkSalarySelection.Text = "Save Selection"
        Me.lnkSalarySelection.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboIncrement
        '
        Me.cboIncrement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncrement.DropDownWidth = 350
        Me.cboIncrement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIncrement.FormattingEnabled = True
        Me.cboIncrement.Location = New System.Drawing.Point(88, 33)
        Me.cboIncrement.Name = "cboIncrement"
        Me.cboIncrement.Size = New System.Drawing.Size(248, 21)
        Me.cboIncrement.TabIndex = 61
        '
        'lblAnnIncrement
        '
        Me.lblAnnIncrement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnnIncrement.Location = New System.Drawing.Point(7, 36)
        Me.lblAnnIncrement.Name = "lblAnnIncrement"
        Me.lblAnnIncrement.Size = New System.Drawing.Size(75, 15)
        Me.lblAnnIncrement.TabIndex = 62
        Me.lblAnnIncrement.Text = "Increment"
        Me.lblAnnIncrement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmPE_FormsReports
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(807, 533)
        Me.Controls.Add(Me.gbIncPromotion)
        Me.Controls.Add(Me.gbMembershipSelection)
        Me.Controls.Add(Me.gbHeadSelection)
        Me.Controls.Add(Me.gbAllocationInfo)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPE_FormsReports"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "PE Form(s)"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbAllocationInfo.ResumeLayout(False)
        Me.gbAllocationInfo.PerformLayout()
        Me.gbHeadSelection.ResumeLayout(False)
        Me.gbMembershipSelection.ResumeLayout(False)
        Me.gbMembershipSelection.PerformLayout()
        Me.gbIncPromotion.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblRType As System.Windows.Forms.Label
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents LblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objchkA_All As System.Windows.Forms.CheckBox
    Friend WithEvents lvAllocation As System.Windows.Forms.ListView
    Friend WithEvents colhAllocations As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents gbAllocationInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkAllocSaveInfo As System.Windows.Forms.LinkLabel
    Friend WithEvents gbHeadSelection As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkTransHeadSelection As System.Windows.Forms.LinkLabel
    Friend WithEvents cboHealthInsurance As System.Windows.Forms.ComboBox
    Friend WithEvents lblHealthInsurance As System.Windows.Forms.Label
    Friend WithEvents gbMembershipSelection As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblMemberships As System.Windows.Forms.Label
    Friend WithEvents lnkMembershipSelection As System.Windows.Forms.LinkLabel
    Friend WithEvents objchkM_All As System.Windows.Forms.CheckBox
    Friend WithEvents lvMembership As System.Windows.Forms.ListView
    Friend WithEvents colhMembership As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchTransHead As eZee.Common.eZeeGradientButton
    Friend WithEvents gbIncPromotion As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchPromotion As eZee.Common.eZeeGradientButton
    Friend WithEvents cboPromotion As System.Windows.Forms.ComboBox
    Friend WithEvents lblPromtion As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchIncrement As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkSalarySelection As System.Windows.Forms.LinkLabel
    Friend WithEvents cboIncrement As System.Windows.Forms.ComboBox
    Friend WithEvents lblAnnIncrement As System.Windows.Forms.Label
    Friend WithEvents objcolhEHeadId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCHeadId As System.Windows.Forms.ColumnHeader
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateAsOn As System.Windows.Forms.Label
    Friend WithEvents lbltoDate As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
End Class
