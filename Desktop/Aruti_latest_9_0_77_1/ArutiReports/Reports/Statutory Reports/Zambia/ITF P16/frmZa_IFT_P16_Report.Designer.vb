﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmZa_IFT_P16_Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmZa_IFT_P16_Report))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblSkillDevelopmentLevy = New System.Windows.Forms.Label
        Me.cboSkillDevelopmentLevy = New System.Windows.Forms.ComboBox
        Me.lnkETaxRemittanceExport = New System.Windows.Forms.LinkLabel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkHeadSelectAll = New System.Windows.Forms.CheckBox
        Me.dgTranHead = New System.Windows.Forms.DataGridView
        Me.objdgHeadcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhTranheadunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhTranHeadCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhTranHeadName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchHead = New System.Windows.Forms.TextBox
        Me.lblDeduction = New System.Windows.Forms.Label
        Me.chkShowIdentity = New System.Windows.Forms.CheckBox
        Me.cboIdentity = New System.Windows.Forms.ComboBox
        Me.lblIdentity = New System.Windows.Forms.Label
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblMembership = New System.Windows.Forms.Label
        Me.lblTaxAdjusted = New System.Windows.Forms.Label
        Me.cboTaxAdjusted = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblSlabEffectivePeriod = New System.Windows.Forms.Label
        Me.cboSlabEffectivePeriod = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblPAYESlabTranHead = New System.Windows.Forms.Label
        Me.cboTranHead = New System.Windows.Forms.ComboBox
        Me.SaveDialog = New System.Windows.Forms.SaveFileDialog
        Me.lblGrossEmolument = New System.Windows.Forms.Label
        Me.cboGrossEmolument = New System.Windows.Forms.ComboBox
        Me.lblChargeableEmolument = New System.Windows.Forms.Label
        Me.cboChargeableEmolument = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgTranHead, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 501)
        Me.NavPanel.Size = New System.Drawing.Size(679, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblChargeableEmolument)
        Me.gbFilterCriteria.Controls.Add(Me.cboChargeableEmolument)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrossEmolument)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrossEmolument)
        Me.gbFilterCriteria.Controls.Add(Me.lblSkillDevelopmentLevy)
        Me.gbFilterCriteria.Controls.Add(Me.cboSkillDevelopmentLevy)
        Me.gbFilterCriteria.Controls.Add(Me.lnkETaxRemittanceExport)
        Me.gbFilterCriteria.Controls.Add(Me.Panel1)
        Me.gbFilterCriteria.Controls.Add(Me.lblDeduction)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowIdentity)
        Me.gbFilterCriteria.Controls.Add(Me.cboIdentity)
        Me.gbFilterCriteria.Controls.Add(Me.lblIdentity)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.lblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.lblTaxAdjusted)
        Me.gbFilterCriteria.Controls.Add(Me.cboTaxAdjusted)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblSlabEffectivePeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboSlabEffectivePeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.lblPAYESlabTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboTranHead)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(656, 392)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSkillDevelopmentLevy
        '
        Me.lblSkillDevelopmentLevy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSkillDevelopmentLevy.Location = New System.Drawing.Point(9, 171)
        Me.lblSkillDevelopmentLevy.Name = "lblSkillDevelopmentLevy"
        Me.lblSkillDevelopmentLevy.Size = New System.Drawing.Size(104, 15)
        Me.lblSkillDevelopmentLevy.TabIndex = 250
        Me.lblSkillDevelopmentLevy.Text = "Skill Dev. Levy"
        Me.lblSkillDevelopmentLevy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSkillDevelopmentLevy
        '
        Me.cboSkillDevelopmentLevy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSkillDevelopmentLevy.FormattingEnabled = True
        Me.cboSkillDevelopmentLevy.Location = New System.Drawing.Point(119, 169)
        Me.cboSkillDevelopmentLevy.Name = "cboSkillDevelopmentLevy"
        Me.cboSkillDevelopmentLevy.Size = New System.Drawing.Size(198, 21)
        Me.cboSkillDevelopmentLevy.TabIndex = 5
        '
        'lnkETaxRemittanceExport
        '
        Me.lnkETaxRemittanceExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkETaxRemittanceExport.Location = New System.Drawing.Point(116, 344)
        Me.lnkETaxRemittanceExport.Name = "lnkETaxRemittanceExport"
        Me.lnkETaxRemittanceExport.Size = New System.Drawing.Size(201, 13)
        Me.lnkETaxRemittanceExport.TabIndex = 247
        Me.lnkETaxRemittanceExport.TabStop = True
        Me.lnkETaxRemittanceExport.Text = "e-Tax Remittance Export..."
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkHeadSelectAll)
        Me.Panel1.Controls.Add(Me.dgTranHead)
        Me.Panel1.Controls.Add(Me.txtSearchHead)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(447, 34)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(198, 215)
        Me.Panel1.TabIndex = 245
        '
        'objchkHeadSelectAll
        '
        Me.objchkHeadSelectAll.AutoSize = True
        Me.objchkHeadSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkHeadSelectAll.Name = "objchkHeadSelectAll"
        Me.objchkHeadSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkHeadSelectAll.TabIndex = 18
        Me.objchkHeadSelectAll.UseVisualStyleBackColor = True
        '
        'dgTranHead
        '
        Me.dgTranHead.AllowUserToAddRows = False
        Me.dgTranHead.AllowUserToDeleteRows = False
        Me.dgTranHead.AllowUserToResizeRows = False
        Me.dgTranHead.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgTranHead.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgTranHead.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgTranHead.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgTranHead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgTranHead.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgHeadcolhCheck, Me.objdgcolhTranheadunkid, Me.dgColhTranHeadCode, Me.dgColhTranHeadName})
        Me.dgTranHead.Location = New System.Drawing.Point(0, 28)
        Me.dgTranHead.Name = "dgTranHead"
        Me.dgTranHead.RowHeadersVisible = False
        Me.dgTranHead.RowHeadersWidth = 5
        Me.dgTranHead.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgTranHead.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgTranHead.Size = New System.Drawing.Size(195, 184)
        Me.dgTranHead.TabIndex = 286
        '
        'objdgHeadcolhCheck
        '
        Me.objdgHeadcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgHeadcolhCheck.HeaderText = ""
        Me.objdgHeadcolhCheck.Name = "objdgHeadcolhCheck"
        Me.objdgHeadcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgHeadcolhCheck.Width = 25
        '
        'objdgcolhTranheadunkid
        '
        Me.objdgcolhTranheadunkid.HeaderText = "tranheadunkid"
        Me.objdgcolhTranheadunkid.Name = "objdgcolhTranheadunkid"
        Me.objdgcolhTranheadunkid.ReadOnly = True
        Me.objdgcolhTranheadunkid.Visible = False
        '
        'dgColhTranHeadCode
        '
        Me.dgColhTranHeadCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhTranHeadCode.HeaderText = "Code"
        Me.dgColhTranHeadCode.Name = "dgColhTranHeadCode"
        Me.dgColhTranHeadCode.ReadOnly = True
        Me.dgColhTranHeadCode.Width = 50
        '
        'dgColhTranHeadName
        '
        Me.dgColhTranHeadName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhTranHeadName.HeaderText = "Head Name"
        Me.dgColhTranHeadName.Name = "dgColhTranHeadName"
        Me.dgColhTranHeadName.ReadOnly = True
        '
        'txtSearchHead
        '
        Me.txtSearchHead.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchHead.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchHead.Location = New System.Drawing.Point(0, 3)
        Me.txtSearchHead.Name = "txtSearchHead"
        Me.txtSearchHead.Size = New System.Drawing.Size(195, 21)
        Me.txtSearchHead.TabIndex = 12
        '
        'lblDeduction
        '
        Me.lblDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeduction.Location = New System.Drawing.Point(340, 38)
        Me.lblDeduction.Name = "lblDeduction"
        Me.lblDeduction.Size = New System.Drawing.Size(101, 15)
        Me.lblDeduction.TabIndex = 244
        Me.lblDeduction.Text = "Deduction"
        Me.lblDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowIdentity
        '
        Me.chkShowIdentity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowIdentity.Location = New System.Drawing.Point(119, 277)
        Me.chkShowIdentity.Name = "chkShowIdentity"
        Me.chkShowIdentity.Size = New System.Drawing.Size(198, 17)
        Me.chkShowIdentity.TabIndex = 9
        Me.chkShowIdentity.Text = "Show Identity"
        Me.chkShowIdentity.UseVisualStyleBackColor = True
        '
        'cboIdentity
        '
        Me.cboIdentity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIdentity.Enabled = False
        Me.cboIdentity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIdentity.FormattingEnabled = True
        Me.cboIdentity.Location = New System.Drawing.Point(119, 250)
        Me.cboIdentity.Name = "cboIdentity"
        Me.cboIdentity.Size = New System.Drawing.Size(198, 21)
        Me.cboIdentity.TabIndex = 8
        '
        'lblIdentity
        '
        Me.lblIdentity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdentity.Location = New System.Drawing.Point(9, 252)
        Me.lblIdentity.Name = "lblIdentity"
        Me.lblIdentity.Size = New System.Drawing.Size(104, 15)
        Me.lblIdentity.TabIndex = 242
        Me.lblIdentity.Text = "Identity"
        Me.lblIdentity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(119, 300)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 10
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(119, 142)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(198, 21)
        Me.cboMembership.TabIndex = 4
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(9, 144)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(104, 15)
        Me.lblMembership.TabIndex = 95
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTaxAdjusted
        '
        Me.lblTaxAdjusted.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxAdjusted.Location = New System.Drawing.Point(9, 117)
        Me.lblTaxAdjusted.Name = "lblTaxAdjusted"
        Me.lblTaxAdjusted.Size = New System.Drawing.Size(104, 15)
        Me.lblTaxAdjusted.TabIndex = 93
        Me.lblTaxAdjusted.Text = "Tax Adjusted"
        Me.lblTaxAdjusted.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTaxAdjusted
        '
        Me.cboTaxAdjusted.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTaxAdjusted.FormattingEnabled = True
        Me.cboTaxAdjusted.Location = New System.Drawing.Point(119, 115)
        Me.cboTaxAdjusted.Name = "cboTaxAdjusted"
        Me.cboTaxAdjusted.Size = New System.Drawing.Size(198, 21)
        Me.cboTaxAdjusted.TabIndex = 3
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(9, 90)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(104, 15)
        Me.lblPeriod.TabIndex = 90
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(119, 88)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(198, 21)
        Me.cboPeriod.TabIndex = 2
        '
        'lblSlabEffectivePeriod
        '
        Me.lblSlabEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSlabEffectivePeriod.Location = New System.Drawing.Point(9, 63)
        Me.lblSlabEffectivePeriod.Name = "lblSlabEffectivePeriod"
        Me.lblSlabEffectivePeriod.Size = New System.Drawing.Size(104, 15)
        Me.lblSlabEffectivePeriod.TabIndex = 88
        Me.lblSlabEffectivePeriod.Text = "EffectivePeriod"
        Me.lblSlabEffectivePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSlabEffectivePeriod
        '
        Me.cboSlabEffectivePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSlabEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSlabEffectivePeriod.FormattingEnabled = True
        Me.cboSlabEffectivePeriod.Location = New System.Drawing.Point(119, 61)
        Me.cboSlabEffectivePeriod.Name = "cboSlabEffectivePeriod"
        Me.cboSlabEffectivePeriod.Size = New System.Drawing.Size(198, 21)
        Me.cboSlabEffectivePeriod.TabIndex = 1
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(547, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 85
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPAYESlabTranHead
        '
        Me.lblPAYESlabTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPAYESlabTranHead.Location = New System.Drawing.Point(9, 36)
        Me.lblPAYESlabTranHead.Name = "lblPAYESlabTranHead"
        Me.lblPAYESlabTranHead.Size = New System.Drawing.Size(104, 15)
        Me.lblPAYESlabTranHead.TabIndex = 1
        Me.lblPAYESlabTranHead.Text = "P.A.Y.E."
        Me.lblPAYESlabTranHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTranHead
        '
        Me.cboTranHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead.FormattingEnabled = True
        Me.cboTranHead.Location = New System.Drawing.Point(119, 34)
        Me.cboTranHead.Name = "cboTranHead"
        Me.cboTranHead.Size = New System.Drawing.Size(198, 21)
        Me.cboTranHead.TabIndex = 0
        '
        'lblGrossEmolument
        '
        Me.lblGrossEmolument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrossEmolument.Location = New System.Drawing.Point(9, 198)
        Me.lblGrossEmolument.Name = "lblGrossEmolument"
        Me.lblGrossEmolument.Size = New System.Drawing.Size(104, 15)
        Me.lblGrossEmolument.TabIndex = 253
        Me.lblGrossEmolument.Text = "Gross Emoluments"
        Me.lblGrossEmolument.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrossEmolument
        '
        Me.cboGrossEmolument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrossEmolument.FormattingEnabled = True
        Me.cboGrossEmolument.Location = New System.Drawing.Point(119, 196)
        Me.cboGrossEmolument.Name = "cboGrossEmolument"
        Me.cboGrossEmolument.Size = New System.Drawing.Size(198, 21)
        Me.cboGrossEmolument.TabIndex = 6
        '
        'lblChargeableEmolument
        '
        Me.lblChargeableEmolument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChargeableEmolument.Location = New System.Drawing.Point(9, 225)
        Me.lblChargeableEmolument.Name = "lblChargeableEmolument"
        Me.lblChargeableEmolument.Size = New System.Drawing.Size(104, 15)
        Me.lblChargeableEmolument.TabIndex = 255
        Me.lblChargeableEmolument.Text = "Chargeable Emoluments"
        Me.lblChargeableEmolument.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboChargeableEmolument
        '
        Me.cboChargeableEmolument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChargeableEmolument.FormattingEnabled = True
        Me.cboChargeableEmolument.Location = New System.Drawing.Point(119, 223)
        Me.cboChargeableEmolument.Name = "cboChargeableEmolument"
        Me.cboChargeableEmolument.Size = New System.Drawing.Size(198, 21)
        Me.cboChargeableEmolument.TabIndex = 7
        '
        'frmZa_IFT_P16_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(679, 556)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmZa_IFT_P16_Report"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmZa_IFT_P16_Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgTranHead, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblPAYESlabTranHead As System.Windows.Forms.Label
    Friend WithEvents cboTranHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblSlabEffectivePeriod As System.Windows.Forms.Label
    Friend WithEvents cboSlabEffectivePeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblTaxAdjusted As System.Windows.Forms.Label
    Friend WithEvents cboTaxAdjusted As System.Windows.Forms.ComboBox
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents cboIdentity As System.Windows.Forms.ComboBox
    Friend WithEvents lblIdentity As System.Windows.Forms.Label
    Friend WithEvents chkShowIdentity As System.Windows.Forms.CheckBox
    Friend WithEvents lblDeduction As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkHeadSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgTranHead As System.Windows.Forms.DataGridView
    Private WithEvents txtSearchHead As System.Windows.Forms.TextBox
    Friend WithEvents objdgHeadcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhTranheadunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhTranHeadCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhTranHeadName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkETaxRemittanceExport As System.Windows.Forms.LinkLabel
    Friend WithEvents SaveDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents lblSkillDevelopmentLevy As System.Windows.Forms.Label
    Friend WithEvents cboSkillDevelopmentLevy As System.Windows.Forms.ComboBox
    Friend WithEvents lblChargeableEmolument As System.Windows.Forms.Label
    Friend WithEvents cboChargeableEmolument As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrossEmolument As System.Windows.Forms.Label
    Friend WithEvents cboGrossEmolument As System.Windows.Forms.ComboBox
End Class
