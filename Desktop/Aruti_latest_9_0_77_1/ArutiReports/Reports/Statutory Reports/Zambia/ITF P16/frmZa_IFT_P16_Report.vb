'************************************************************************************************************************************
'Class Name : frmZa_IFT_P16_Report.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmZa_IFT_P16_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmZa_IFT_P16_Report"
    Private objIFT_P16 As clsZa_ITF_P16_Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    'Sohail (14 May 2019) -- Start
    'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
    Private mdtPeriodStartDate As DateTime = Nothing
    'Sohail (14 May 2019) -- End
    Private mdtPeriodEndDate As DateTime = Nothing
    'Sohail (19 Sep 2017) -- Start
    'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
    Private mstrSearchText As String = ""
    'Sohail (19 Sep 2017) -- End
    'Sohail (14 Apr 2018) -- Start
    'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
    Private dvTranHead As DataView
    Private mstrSearchHeadText As String = ""
    'Sohail (14 Apr 2018) -- End

#End Region

#Region " Contructor "

    Public Sub New()
        objIFT_P16 = New clsZa_ITF_P16_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objIFT_P16.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

    'Sohail (19 Sep 2017) -- Start
    'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
#Region " Private Enum "
    Private Enum enHeadTypeId
        PAYE = 1
        TaxAdjusted = 2
        Membership = 3
        'Sohail (09 Nov 2017) -- Start
        'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
        ShowIdentity = 4
        Identity = 5
        'Sohail (09 Nov 2017) -- End        
        'Sohail (14 Apr 2018) -- Start
        'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
        DeductionHeads = 6
        'Sohail (14 Apr 2018) -- End
        'Sohail (27 Jul 2019) -- Start
        'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
        SkillDevHead = 7
        'Sohail (27 Jul 2019) -- End
        'Sohail (17 Dec 2021) -- Start
        'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
        GrossEmolument = 8
        ChargeableEmolument = 9
        'Sohail (17 Dec 2021) -- End
    End Enum
#End Region
    'Sohail (19 Sep 2017) -- End

#Region " Private Function "

    Private Sub FillCombo()
        Dim objperiod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        'Sohail (19 Sep 2017) -- Start
        'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
        Dim objMember As New clsmembership_master
        'Sohail (19 Sep 2017) -- End
        'Sohail (09 Nov 2017) -- Start
        'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
        Dim objCMaster As New clsCommon_Master
        'Sohail (09 Nov 2017) -- End
        Dim dsCombos As New DataSet
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("Taxes", True, enTranHeadType.EmployeesStatutoryDeductions, enCalcType.AsComputedOnWithINEXCESSOFTaxSlab, enTypeOf.Taxes)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Taxes", True, enTranHeadType.EmployeesStatutoryDeductions, enCalcType.AsComputedOnWithINEXCESSOFTaxSlab, enTypeOf.Taxes)
            'Sohail (21 Aug 2015) -- End
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0)
            With cboSlabEffectivePeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Slab")
                .SelectedValue = 0
            End With

            'Sohail (19 Sep 2017) -- Start
            'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , enTypeOf.Informational, , )
            With cboTaxAdjusted
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboTaxAdjusted)

            'Sohail (17 Dec 2021) -- Start
            'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
            With cboGrossEmolument
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboGrossEmolument)

            With cboChargeableEmolument
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboChargeableEmolument)
            'Sohail (17 Dec 2021) -- End

            'Sohail (27 Jul 2019) -- Start
            'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True)
            With cboSkillDevelopmentLevy
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboSkillDevelopmentLevy)
            'Sohail (27 Jul 2019) -- End

            dsCombos = objMember.getListForCombo("Membership", True, , 1)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (19 Sep 2017) -- End

            'Sohail (09 Nov 2017) -- Start
            'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboIdentity
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            'Sohail (09 Nov 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            'Sohail (09 Nov 2017) -- Start
            'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
            objperiod = Nothing
            objTranHead = Nothing
            objExcessSlan = Nothing
            objMember = Nothing
            objCMaster = Nothing
            'Sohail (09 Nov 2017) -- End
        End Try
    End Sub

    'Sohail (14 Apr 2018) -- Start
    'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
    Private Sub FillList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            dgTranHead.DataSource = Nothing
            RemoveHandler txtSearchHead.TextChanged, AddressOf txtSearchHead_TextChanged
            Call SetDefaultSearchHeadText()
            AddHandler txtSearchHead.TextChanged, AddressOf txtSearchHead_TextChanged

            dsList = objTranHead.GetHeadListTypeWise(enTranHeadType.EmployeesStatutoryDeductions, enTypeOf.Employee_Statutory_Contributions)

            Dim dtTable As DataTable = New DataView(dsList.Tables(0)).ToTable
            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvTranHead = dtTable.DefaultView
            dgTranHead.AutoGenerateColumns = False
            objdgHeadcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhTranheadunkid.DataPropertyName = "Id"
            dgColhTranHeadCode.DataPropertyName = "code"
            dgColhTranHeadName.DataPropertyName = "name"

            dgTranHead.DataSource = dvTranHead
            dvTranHead.Sort = "IsChecked DESC, name "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchHeadText()
        Try
            mstrSearchHeadText = Language.getMessage(mstrModuleName, 8, "Type to Search")
            With txtSearchHead
                .ForeColor = Color.Gray
                .Text = mstrSearchHeadText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchPeriodText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkHeadSelectAll.CheckedChanged, AddressOf objchkHeadSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgTranHead.CurrentRow.Cells(objdgHeadcolhCheck.Index).Value)

            Dim intCount As Integer = dvTranHead.Table.Select("IsChecked = 1").Length

            If intCount <= 0 Then
                objchkHeadSelectAll.CheckState = CheckState.Unchecked
            ElseIf intCount < dgTranHead.Rows.Count Then
                objchkHeadSelectAll.CheckState = CheckState.Indeterminate
            ElseIf intCount = dgTranHead.Rows.Count Then
                objchkHeadSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkHeadSelectAll.CheckedChanged, AddressOf objchkHeadSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub CheckAllHead(ByVal blnCheckAll As Boolean)
        Try
            If dvTranHead IsNot Nothing Then
                For Each dr As DataRowView In dvTranHead
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvTranHead.ToTable.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllHead", mstrModuleName)
        End Try
    End Sub

    'Sohail (14 Apr 2018) -- End

    Private Sub ResetValue()
        Try
            cboTranHead.SelectedValue = 0
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""
            cboSlabEffectivePeriod.SelectedValue = 0
            'Sohail (19 Sep 2017) -- Start
            'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
            cboTaxAdjusted.SelectedValue = 0
            cboMembership.SelectedValue = 0
            'Sohail (09 Nov 2017) -- Start
            'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
            cboIdentity.SelectedValue = 0
            'Sohail (09 Nov 2017) -- End
            'Sohail (27 Jul 2019) -- Start
            'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
            cboSkillDevelopmentLevy.SelectedValue = 0
            'Sohail (27 Jul 2019) -- End
            'Sohail (17 Dec 2021) -- Start
            'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
            cboGrossEmolument.SelectedValue = 0
            cboChargeableEmolument.SelectedValue = 0
            'Sohail (17 Dec 2021) -- End

            Call GetValue()
            'Sohail (19 Sep 2017) -- End

            'Sohail (14 Apr 2018) -- Start
            'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
            Call SetDefaultSearchHeadText()
            'Sohail (14 Apr 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objIFT_P16.SetDefaultValue()
            If CInt(cboTranHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Slab Transaction Head is mandatory information. Please select Slab Transaction Head."), enMsgBoxStyle.Information)
                cboTranHead.Focus()
                Return False
            End If

            If CInt(cboSlabEffectivePeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Effective Period is mandatory information. Please select Effective Period."), enMsgBoxStyle.Information)
                cboSlabEffectivePeriod.Focus()
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period"), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
                'Sohail (17 Dec 2021) -- Start
                'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
            ElseIf CInt(cboGrossEmolument.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Gross Emolument Head is mandatory information. Please select Gross Emolument Head."), enMsgBoxStyle.Information)
                cboGrossEmolument.Focus()
                Return False
            ElseIf CInt(cboChargeableEmolument.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Chargeable Emolument Head is mandatory information. Please select Chargeable Emolument Head."), enMsgBoxStyle.Information)
                cboChargeableEmolument.Focus()
                Return False
                'Sohail (17 Dec 2021) -- End
                'Sohail (19 Sep 2017) -- Start
                'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
                'Sohail (14 May 2019) -- Start
                'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes. (Make Tax Adjusted selection optional).
                'ElseIf CInt(cboTaxAdjusted.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Tax Adjusted Head is mandatory information. Please select Tax Adjusted Head."), enMsgBoxStyle.Information)
                '    cboTaxAdjusted.Focus()
                '    Return False
                'Sohail (14 May 2019) -- End

                'Sohail (06 Oct 2018) -- Start
                'Enhancement - PACRA - Ref. No.  226 : Make Membership selection optional on PAYE Monthly Return ITF P16 Report in 75.1.
                'ElseIf CInt(cboMembership.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Membership is mandatory information. Please select Membership."), enMsgBoxStyle.Information)
                '    cboMembership.Focus()
                '    Return False
                'Sohail (06 Oct 2018) -- End
                'Sohail (19 Sep 2017) -- End
                'Sohail (09 Nov 2017) -- Start
                'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
            ElseIf chkShowIdentity.Checked = True AndAlso CInt(cboIdentity.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Identity is mandatory information. Please select Identity."), enMsgBoxStyle.Information)
                cboIdentity.Focus()
                Return False
                'Sohail (09 Nov 2017) -- End
            End If

            objIFT_P16._Analysis_Fields = mstrAnalysis_Fields
            objIFT_P16._Analysis_Join = mstrAnalysis_Join
            objIFT_P16._Analysis_OrderBy = mstrAnalysis_OrderBy_GName
            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
            objIFT_P16._StartPeriodDate = mdtPeriodStartDate
            'Sohail (14 May 2019) -- End
            objIFT_P16._EndPeriodDate = mdtPeriodEndDate
            objIFT_P16._SlabTranId = cboTranHead.SelectedValue
            objIFT_P16._ViewByIds = mstrStringIds
            objIFT_P16._ViewIndex = mintViewIdx
            objIFT_P16._ViewByName = mstrStringName
            objIFT_P16._PeriodName = cboPeriod.Text
            objIFT_P16._PeriodUnkid = cboPeriod.SelectedValue
            'Sohail (19 Sep 2017) -- Start
            'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
            objIFT_P16._TaxAdjustedHeadUnkid = CInt(cboTaxAdjusted.SelectedValue)
            objIFT_P16._TaxAdjustedHeadName = cboTaxAdjusted.Text
            objIFT_P16._MembershipUnkid = CInt(cboMembership.SelectedValue)
            objIFT_P16._MembershipName = cboMembership.Text
            'Sohail (19 Sep 2017) -- End
            'Sohail (09 Nov 2017) -- Start
            'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
            objIFT_P16._IdentityUnkid = CInt(cboIdentity.SelectedValue)
            objIFT_P16._IdentityName = cboIdentity.Text
            objIFT_P16._ShowIdentity = chkShowIdentity.Checked
            'Sohail (09 Nov 2017) -- End

            'Sohail (27 Jul 2019) -- Start
            'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
            objIFT_P16._SkillDevHeadUnkid = CInt(cboSkillDevelopmentLevy.SelectedValue)
            objIFT_P16._SkillDevHeadName = cboSkillDevelopmentLevy.Text
            'Sohail (27 Jul 2019) -- End

            'Sohail (17 Dec 2021) -- Start
            'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
            objIFT_P16._GrossEmolumentHeadId = CInt(cboGrossEmolument.SelectedValue)
            objIFT_P16._GrossEmolumentHeadName = cboGrossEmolument.Text

            objIFT_P16._ChargeableEmolumentHeadId = CInt(cboChargeableEmolument.SelectedValue)
            objIFT_P16._ChargeableEmolumentHeadName = cboChargeableEmolument.Text
            'Sohail (17 Dec 2021) -- End

            'Sohail (14 Apr 2018) -- Start
            'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
            Dim strIDs As String = String.Join(",", (From p In dvTranHead.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("id").ToString)).ToArray)
            If strIDs.Trim.Length <= 0 Then
                strIDs = " 0 "
            End If
            objIFT_P16._DeductionHeadIDs = strIDs
            objIFT_P16._DeductionHeadNames = String.Join(",", (From p In dvTranHead.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("Name").ToString)).ToArray)
            'Sohail (14 Apr 2018) -- End

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objIFT_P16._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(mdtPeriodEndDate))
            'Sohail (03 Aug 2019) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    'Sohail (19 Sep 2017) -- Start
    'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Za_ITF_P16_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.PAYE
                            cboTranHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.TaxAdjusted
                            cboTaxAdjusted.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                            If CInt(dsRow.Item("transactionheadid")) > 0 Then
                                With cboTaxAdjusted
                                    .ForeColor = Color.Black
                                    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                                End With
                            End If

                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                            'Sohail (09 Nov 2017) -- Start
                            'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
                        Case enHeadTypeId.ShowIdentity
                            chkShowIdentity.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Identity
                            cboIdentity.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (09 Nov 2017) -- End

                            'Sohail (14 Apr 2018) -- Start
                            'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
                        Case CInt(enHeadTypeId.DeductionHeads)
                            Dim arrHead As String()
                            arrHead = dsRow("transactionheadid").ToString().Split(",")
                            Dim lstHead As List(Of DataRow) = (From p In dvTranHead.Table Where (arrHead.Contains(p.Item("id").ToString)) Select (p)).ToList
                            For Each dtRow As DataRow In lstHead
                                dtRow.Item("IsChecked") = True
                            Next
                            dvTranHead.Table.AcceptChanges()
                            If dgTranHead.RowCount > 0 Then dgTranHead.CurrentCell = dgTranHead.Rows(0).Cells(0)
                            'Sohail (14 Apr 2018) -- End

                            'Sohail (27 Jul 2019) -- Start
                            'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
                        Case enHeadTypeId.SkillDevHead
                            cboSkillDevelopmentLevy.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (27 Jul 2019) -- End

                            'Sohail (17 Dec 2021) -- Start
                            'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
                        Case enHeadTypeId.GrossEmolument
                            cboGrossEmolument.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.ChargeableEmolument
                            cboChargeableEmolument.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (17 Dec 2021) -- End

                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 8, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (19 Sep 2017) -- End

    'Sohail (27 Jul 2019) -- Start
    'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRegularFont", mstrModuleName)
        End Try
    End Sub
    'Sohail (27 Jul 2019) -- End

#End Region

#Region " Forms "

    Private Sub frmZa_IFT_P16_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objIFT_P16 = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmZa_IFT_P16_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmZa_IFT_P16_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objIFT_P16._ReportName
            Me._Message = objIFT_P16._ReportDesc

            Call FillCombo()
            'Sohail (14 Apr 2018) -- Start
            'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
            Call FillList()
            'Sohail (14 Apr 2018) -- End
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmZa_IFT_P16_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objIFT_P16.generateReport(0, e.Type, enExportAction.None)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objIFT_P16.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                             User._Object._Userunkid, _
            '                             FinancialYear._Object._YearUnkid, _
            '                             Company._Object._Companyunkid, _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             ConfigParameter._Object._UserAccessModeSetting, _
            '                             True, ConfigParameter._Object._ExportReportPath, _
            '                             ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            objIFT_P16.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         mdtPeriodStartDate, _
                                         mdtPeriodEndDate, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objIFT_P16.generateReport(0, enPrintAction.None, e.Type)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objIFT_P16.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                             User._Object._Userunkid, _
            '                             FinancialYear._Object._YearUnkid, _
            '                             Company._Object._Companyunkid, _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             ConfigParameter._Object._UserAccessModeSetting, _
            '                             True, ConfigParameter._Object._ExportReportPath, _
            '                             ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            objIFT_P16.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         mdtPeriodStartDate, _
                                         mdtPeriodEndDate, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsZa_ITF_P16_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsZa_ITF_P16_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Sohail (19 Sep 2017) -- Start
    'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()
                Dim intUnkid As Integer = -1
                objUserDefRMode._Reportunkid = enArutiReport.Za_ITF_P16_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.PAYE
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboTranHead.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Za_ITF_P16_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.TaxAdjusted
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboTaxAdjusted.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Za_ITF_P16_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboMembership.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Za_ITF_P16_Report, 0, 0, intHeadType)

                        'Sohail (09 Nov 2017) -- Start
                        'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
                    Case enHeadTypeId.ShowIdentity
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkShowIdentity.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Za_ITF_P16_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Identity
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboIdentity.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Za_ITF_P16_Report, 0, 0, intHeadType)
                        'Sohail (09 Nov 2017) -- End

                        'Sohail (14 Apr 2018) -- Start
                        'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
                    Case CInt(enHeadTypeId.DeductionHeads)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = String.Join(",", (From p In dvTranHead.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("id").ToString)).ToArray)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Za_ITF_P16_Report, 0, 0, intHeadType)
                        'Sohail (14 Apr 2018) -- End

                        'Sohail (27 Jul 2019) -- Start
                        'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
                    Case CInt(enHeadTypeId.SkillDevHead)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboSkillDevelopmentLevy.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Za_ITF_P16_Report, 0, 0, intHeadType)
                        'Sohail (27 Jul 2019) -- End

                        'Sohail (17 Dec 2021) -- Start
                        'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
                    Case CInt(enHeadTypeId.GrossEmolument)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboGrossEmolument.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Za_ITF_P16_Report, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.ChargeableEmolument)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboChargeableEmolument.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Za_ITF_P16_Report, 0, 0, intHeadType)
                        'Sohail (17 Dec 2021) -- End

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
            Next
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (19 Sep 2017) -- End

    'Sohail (14 May 2019) -- Start
    'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
    Private Sub lnkETaxRemittanceExport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkETaxRemittanceExport.LinkClicked
        Try
            If Not SetFilter() Then Exit Sub

            objIFT_P16._IsExportToCSV = True

            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0

            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objIFT_P16.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                             User._Object._Userunkid, _
            '                             FinancialYear._Object._YearUnkid, _
            '                             Company._Object._Companyunkid, _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             ConfigParameter._Object._UserAccessModeSetting, _
            '                             True, SaveDialog.FileName, _
            '                             ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            objIFT_P16.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         mdtPeriodStartDate, _
                                         mdtPeriodEndDate, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, SaveDialog.FileName, _
                                         ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Data exported successfully!"), enMsgBoxStyle.Information)

            If ConfigParameter._Object._OpenAfterExport = True Then
                Process.Start(SaveDialog.FileName)
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "lnkETaxRemittanceExport_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (14 May 2019) -- End

#End Region

#Region " Controls "

    Private Sub cboSlabEffectivePeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSlabEffectivePeriod.SelectedIndexChanged
        Dim objperiod As New clscommom_period_Tran
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Dim dtTable As DataTable
        Dim strNextSlabFilter As String = ""

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            'Sohail (21 Aug 2015) -- End
            If CInt(cboSlabEffectivePeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objperiod._Periodunkid = CInt(cboSlabEffectivePeriod.SelectedValue)
                objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboSlabEffectivePeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End


                Dim ds As DataSet = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0)
                Dim dt As DataTable = New DataView(ds.Tables("Slab"), "end_date > '" & eZeeDate.convertDate(objperiod._End_Date) & "' ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Rows.Count > 0 Then
                    strNextSlabFilter = " AND end_date < '" & dt.Rows(0).Item("end_date").ToString & "' "
                End If

                dtTable = New DataView(dsCombos.Tables("Period"), "end_date >= '" & eZeeDate.convertDate(objperiod._End_Date) & "' " & strNextSlabFilter & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombos.Tables("Period")).ToTable
            End If
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSlabEffectivePeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objperiod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objperiod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                'Sohail (14 May 2019) -- Start
                'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
                mdtPeriodStartDate = objperiod._Start_Date
                'Sohail (14 May 2019) -- End
                mdtPeriodEndDate = objperiod._End_Date
                objperiod = Nothing
            Else
                'Sohail (14 May 2019) -- Start
                'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
                mdtPeriodStartDate = Nothing
                'Sohail (14 May 2019) -- End
                mdtPeriodEndDate = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (19 Sep 2017) -- Start
    'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
    Private Sub cboTaxAdjusted_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboTaxAdjusted.KeyPress, cboSkillDevelopmentLevy.KeyPress, cboGrossEmolument.KeyPress, cboChargeableEmolument.KeyPress
        'Sohail (17 Dec 2021) - [cboGrossEmolument, cboChargeableEmolument]
        'Sohail (27 Jul 2019) - [cboSkillDevelopmentLevy]

        'Sohail (27 Jul 2019) -- Start
        'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
        Dim cbo As ComboBox = CType(sender, ComboBox)
        'Sohail (27 Jul 2019) -- End

        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    'Sohail (27 Jul 2019) -- Start
                    'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
                    '.ValueMember = cboTaxAdjusted.ValueMember
                    '.DisplayMember = cboTaxAdjusted.DisplayMember
                    '.DataSource = CType(cboTaxAdjusted.DataSource, DataTable)
                    '.CodeMember = "code"
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = "code"
                    'Sohail (27 Jul 2019) -- End
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    'Sohail (27 Jul 2019) -- Start
                    'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
                    'cboTaxAdjusted.SelectedValue = frm.SelectedValue
                    cbo.SelectedValue = frm.SelectedValue
                    'Sohail (27 Jul 2019) -- End
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    'Sohail (27 Jul 2019) -- Start
                    'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
                    'cboTaxAdjusted.Text = ""
                    cbo.Text = ""
                    'Sohail (27 Jul 2019) -- End
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTaxAdjusted_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTaxAdjusted_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTaxAdjusted.SelectedIndexChanged, cboSkillDevelopmentLevy.SelectedIndexChanged, cboGrossEmolument.SelectedIndexChanged, cboChargeableEmolument.SelectedIndexChanged
        'Sohail (17 Dec 2021) - [cboGrossEmolument, cboChargeableEmolument]
        'Sohail (27 Jul 2019) - [cboSkillDevelopmentLevy]

        'Sohail (27 Jul 2019) -- Start
        'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
        Dim cbo As ComboBox = CType(sender, ComboBox)
        'Sohail (27 Jul 2019) -- End

        Try
            'Sohail (27 Jul 2019) -- Start
            'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
            'If CInt(cboTaxAdjusted.SelectedValue) < 0 Then Call SetDefaultSearchText(cboTaxAdjusted)
            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If
            'Sohail (27 Jul 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTaxAdjusted_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTaxAdjusted_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTaxAdjusted.GotFocus, cboSkillDevelopmentLevy.GotFocus, cboGrossEmolument.GotFocus, cboChargeableEmolument.GotFocus
        'Sohail (17 Dec 2021) - [cboGrossEmolument, cboChargeableEmolument]
        'Sohail (27 Jul 2019) - [cboSkillDevelopmentLevy]

        'Sohail (27 Jul 2019) -- Start
        'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
        Dim cbo As ComboBox = CType(sender, ComboBox)
        'Sohail (27 Jul 2019) -- End

        Try
            'Sohail (27 Jul 2019) -- Start
            'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
            'With cboTaxAdjusted
            With cbo
                'Sohail (27 Jul 2019) -- End
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTaxAdjusted_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTaxAdjusted_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTaxAdjusted.Leave, cboSkillDevelopmentLevy.Leave, cboGrossEmolument.Leave, cboChargeableEmolument.Leave
        'Sohail (17 Dec 2021) - [cboGrossEmolument, cboChargeableEmolument]
        'Sohail (27 Jul 2019) - [cboSkillDevelopmentLevy]

        'Sohail (27 Jul 2019) -- Start
        'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
        Dim cbo As ComboBox = CType(sender, ComboBox)
        'Sohail (27 Jul 2019) -- End

        Try
            'Sohail (27 Jul 2019) -- Start
            'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
            'If CInt(cboTaxAdjusted.SelectedValue) <= 0 Then
            '    Call SetDefaultSearchText(cboTaxAdjusted)
            'End If
            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
            'Sohail (27 Jul 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTaxAdjusted_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (19 Sep 2017) -- End

    'Sohail (09 Nov 2017) -- Start
    'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
    Private Sub chkShowIdentity_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowIdentity.CheckedChanged
        Try
            If chkShowIdentity.Checked = True Then
                cboIdentity.Enabled = True
            Else
                cboIdentity.SelectedValue = 0
                cboIdentity.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowIdentity_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (09 Nov 2017) -- End

    'Sohail (14 Apr 2018) -- Start
    'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
    Private Sub objchkHeadSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkHeadSelectAll.CheckedChanged
        Try
            Call CheckAllHead(objchkHeadSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkHeadSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchHead_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchHead.GotFocus
        Try
            With txtSearchHead
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchHeadText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchHead_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchHead_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchHead.Leave
        Try
            If txtSearchHead.Text.Trim = "" Then
                Call SetDefaultSearchHeadText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchHead_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchHead_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchHead.TextChanged
        Try
            If txtSearchHead.Text.Trim = mstrSearchHeadText Then Exit Sub
            If dvTranHead IsNot Nothing Then
                dvTranHead.RowFilter = "code LIKE '%" & txtSearchHead.Text.Replace("'", "''") & "%'  OR name LIKE '%" & txtSearchHead.Text.Replace("'", "''") & "%'"
                dgTranHead.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchHead_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (14 Apr 2018) -- End

#End Region



   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.lblPAYESlabTranHead.Text = Language._Object.getCaption(Me.lblPAYESlabTranHead.Name, Me.lblPAYESlabTranHead.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblSlabEffectivePeriod.Text = Language._Object.getCaption(Me.lblSlabEffectivePeriod.Name, Me.lblSlabEffectivePeriod.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblTaxAdjusted.Text = Language._Object.getCaption(Me.lblTaxAdjusted.Name, Me.lblTaxAdjusted.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblIdentity.Text = Language._Object.getCaption(Me.lblIdentity.Name, Me.lblIdentity.Text)
			Me.chkShowIdentity.Text = Language._Object.getCaption(Me.chkShowIdentity.Name, Me.chkShowIdentity.Text)
			Me.lblDeduction.Text = Language._Object.getCaption(Me.lblDeduction.Name, Me.lblDeduction.Text)
			Me.dgColhTranHeadCode.HeaderText = Language._Object.getCaption(Me.dgColhTranHeadCode.Name, Me.dgColhTranHeadCode.HeaderText)
			Me.dgColhTranHeadName.HeaderText = Language._Object.getCaption(Me.dgColhTranHeadName.Name, Me.dgColhTranHeadName.HeaderText)
			Me.lnkETaxRemittanceExport.Text = Language._Object.getCaption(Me.lnkETaxRemittanceExport.Name, Me.lnkETaxRemittanceExport.Text)
			Me.lblSkillDevelopmentLevy.Text = Language._Object.getCaption(Me.lblSkillDevelopmentLevy.Name, Me.lblSkillDevelopmentLevy.Text)
			Me.lblChargeableEmolument.Text = Language._Object.getCaption(Me.lblChargeableEmolument.Name, Me.lblChargeableEmolument.Text)
			Me.lblGrossEmolument.Text = Language._Object.getCaption(Me.lblGrossEmolument.Name, Me.lblGrossEmolument.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Slab Transaction Head is mandatory information. Please select Slab Transaction Head.")
			Language.setMessage(mstrModuleName, 2, "Effective Period is mandatory information. Please select Effective Period.")
			Language.setMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period")
			Language.setMessage(mstrModuleName, 4, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 7, "Identity is mandatory information. Please select Identity.")
			Language.setMessage(mstrModuleName, 8, "Type to Search")
			Language.setMessage(mstrModuleName, 9, "Data exported successfully!")
			Language.setMessage(mstrModuleName, 10, "Gross Emolument Head is mandatory information. Please select Gross Emolument Head.")
			Language.setMessage(mstrModuleName, 11, "Chargeable Emolument Head is mandatory information. Please select Chargeable Emolument Head.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
