'************************************************************************************************************************************
'Class Name : frmRemittancForm.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmRemittancForm

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmRemittancForm"
    Private objRemittanceForm As clsRemittanceForm
    Private mstrEndPeriodName As String = String.Empty
    Private mdtStartPeriodDate As DateTime
    Private mdtEndPeriodDate As DateTime
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""

#End Region

#Region " Contructor "

    Public Sub New()
        objRemittanceForm = New clsRemittanceForm(User._Object._Languageunkid,Company._Object._Companyunkid)
        objRemittanceForm.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objperiod As New clscommom_period_Tran
        Dim objMembership As New clsmembership_master
        Dim objEmpContribution As New clsTransactionHead
        Dim dsCombos As New DataSet
        Try

            dsCombos = objMembership.getListForCombo("List", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("EmpContribution", True, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Employee_Statutory_Contributions)
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "EmpContribution", True, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Employee_Statutory_Contributions)
            'Sohail (21 Aug 2015) -- End
            With cboEmpHeadType
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("EmpContribution")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("EmployerContribution", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "EmployerContribution", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            'Sohail (21 Aug 2015) -- End
            With cboCoHeadType
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("EmployerContribution")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillPeriod()
        Try
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Dim dsList As DataSet = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List")
            Dim dsList As DataSet = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List")
            'Sohail (21 Aug 2015) -- End

            Dim lvItem As ListViewItem
            lvPeriod.Items.Clear()

            For Each drRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.Tag = drRow("periodunkid")
                lvItem.SubItems.Add(drRow("name").ToString())
                'Sohail (03 Aug 2019) -- Start
                'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                lvItem.SubItems.Add(drRow.Item("start_Date").ToString)
                lvItem.SubItems(objcolhPeriodStart.Index).Tag = drRow.Item("end_Date").ToString
                'Sohail (03 Aug 2019) -- End
                lvPeriod.Items.Add(lvItem)
            Next

            If lvPeriod.Items.Count > 16 Then
                colhPeriodName.Width = 185 - 18
            Else
                colhPeriodName.Width = 185
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPeriod", mstrModuleName)
        End Try
    End Sub

    Private Sub DoOperation(ByVal blnOperation As Boolean)
        Try
            For Each lvItem As ListViewItem In lvPeriod.Items
                lvItem.Checked = blnOperation
            Next
            lvPeriod.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        End Try
    End Sub

    Private Sub GetCommaSeparatedTags(ByRef mstrIds As String)
        Dim intCnt As Integer = 1
        Try
            For Each lvItem As ListViewItem In lvPeriod.CheckedItems
                mstrIds &= "," & lvItem.Tag.ToString
                If intCnt = lvPeriod.CheckedItems.Count Then
                    mstrEndPeriodName = lvItem.SubItems(colhPeriodName.Index).Text
                End If
                intCnt += 1
            Next
            mstrIds = Mid(mstrIds, 2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCommaSeparatedTags", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboMembership.SelectedValue = 0

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objRemittanceForm.SetDefaultValue()

            If cboMembership.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Membership is mandatory information. Please select membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            End If

            If cboEmpHeadType.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Contribution is mandatory information. Please select Employee Contribution to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            End If

            If cboCoHeadType.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employer Contribution is mandatory information. Please select Employee Contribution to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            End If

            If lvPeriod.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check atleast one Period to view report."), enMsgBoxStyle.Information)
                lvPeriod.Focus()
                Return False
            End If

            objRemittanceForm._MembershipId = cboMembership.SelectedValue
            Dim strPreriodIds As String = ""
            Call GetCommaSeparatedTags(strPreriodIds)
            objRemittanceForm._PeriodIds = strPreriodIds
            objRemittanceForm._ViewByIds = mstrStringIds
            objRemittanceForm._ViewIndex = mintViewIdx
            objRemittanceForm._ViewBy = mstrStringName
            objRemittanceForm._Analysis_Fields = mstrAnalysis_Fields
            objRemittanceForm._Analysis_Join = mstrAnalysis_Join
            objRemittanceForm._EmpHeadTypeId = cboEmpHeadType.SelectedValue
            objRemittanceForm._EmprHeadTypeId = cboCoHeadType.SelectedValue

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmRemittancForm_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objRemittanceForm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRemittancForm_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmRemittancForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'ISSUE : THIS LINK WAS KEPT BUT IT'S NOT USED IN CLASS FOR SETTING ANALYSIS FOR QUERY, SO LINK IS SET FOR NOT VISIBLE DISCUSSED WITH PINKAL
            lnkSetAnalysis.Visible = False
            'S.SANDEEP [04 JUN 2015] -- END

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Me._Title = objRemittanceForm._ReportName
            Me._Message = objRemittanceForm._ReportDesc

            Call FillCombo()
            Call FillPeriod()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRemittancForm_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objRemittanceForm.generateReport(0, e.Type, enExportAction.None)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objRemittanceForm.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                    User._Object._Userunkid, _
            '                                    FinancialYear._Object._YearUnkid, _
            '                                    Company._Object._Companyunkid, _
            '                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                    ConfigParameter._Object._UserAccessModeSetting, _
            '                                    True, ConfigParameter._Object._ExportReportPath, _
            '                                    ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            objRemittanceForm._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, (From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max)

            objRemittanceForm.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate((From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Text.ToString)).Min), _
                                                eZeeDate.convertDate((From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                True, ConfigParameter._Object._ExportReportPath, _
                                                ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objRemittanceForm.generateReport(0, enPrintAction.None, e.Type)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objRemittanceForm.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                    User._Object._Userunkid, _
            '                                    FinancialYear._Object._YearUnkid, _
            '                                    Company._Object._Companyunkid, _
            '                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                    ConfigParameter._Object._UserAccessModeSetting, _
            '                                    True, ConfigParameter._Object._ExportReportPath, _
            '                                    ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            objRemittanceForm._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, (From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max)

            objRemittanceForm.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate((From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Text.ToString)).Min), _
                                                eZeeDate.convertDate((From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                True, ConfigParameter._Object._ExportReportPath, _
                                                ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsRemittanceForm.SetMessages()
            objfrm._Other_ModuleNames = "clsRemittanceForm"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub objchkAllPeriod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllPeriod.CheckedChanged
        Try
            Call DoOperation(CBool(objchkAllPeriod.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllPeriod_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriod.ItemChecked
        Try
            RemoveHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
            If lvPeriod.CheckedItems.Count <= 0 Then
                objchkAllPeriod.CheckState = CheckState.Unchecked
            ElseIf lvPeriod.CheckedItems.Count < lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Indeterminate
            ElseIf lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            'mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.LblMembership.Text = Language._Object.getCaption(Me.LblMembership.Name, Me.LblMembership.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub

    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Housing Head is compulsory information.Please select Housing Head to continue.")
            Language.setMessage(mstrModuleName, 2, "Slab Tax Head is compulsory information.Please select Slab Tax Head to continue.")
            Language.setMessage(mstrModuleName, 3, "Please check atleast one Period to view report.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>
End Class
