﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAnnualPayeReturn
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objchkAllPeriod = New System.Windows.Forms.CheckBox
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboTemparory = New System.Windows.Forms.ComboBox
        Me.cboPermanent = New System.Windows.Forms.ComboBox
        Me.lvPeriod = New eZee.Common.eZeeListView(Me.components)
        Me.objColhPCheck = New System.Windows.Forms.ColumnHeader
        Me.colhPeriodName = New System.Windows.Forms.ColumnHeader
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblSlabEffectivePeriod = New System.Windows.Forms.Label
        Me.cboSlabEffectivePeriod = New System.Windows.Forms.ComboBox
        Me.lblSlabTranHead = New System.Windows.Forms.Label
        Me.cboTranHead = New System.Windows.Forms.ComboBox
        Me.objcolhPeriodStart = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 453)
        Me.NavPanel.Size = New System.Drawing.Size(724, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objchkAllPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.gbFilterCriteria.Controls.Add(Me.lvPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.lblSlabEffectivePeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboSlabEffectivePeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblSlabTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboTranHead)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(357, 319)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkAllPeriod
        '
        Me.objchkAllPeriod.AutoSize = True
        Me.objchkAllPeriod.Location = New System.Drawing.Point(126, 185)
        Me.objchkAllPeriod.Name = "objchkAllPeriod"
        Me.objchkAllPeriod.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllPeriod.TabIndex = 89
        Me.objchkAllPeriod.UseVisualStyleBackColor = True
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.Label2)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.Label1)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboTemparory)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboPermanent)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(10, 84)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(337, 88)
        Me.EZeeCollapsibleContainer1.TabIndex = 94
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Employement Type"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(4, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 15)
        Me.Label2.TabIndex = 94
        Me.Label2.Text = "Permanent"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 15)
        Me.Label1.TabIndex = 92
        Me.Label1.Text = "Temparory"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTemparory
        '
        Me.cboTemparory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTemparory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTemparory.FormattingEnabled = True
        Me.cboTemparory.Location = New System.Drawing.Point(107, 57)
        Me.cboTemparory.Name = "cboTemparory"
        Me.cboTemparory.Size = New System.Drawing.Size(225, 21)
        Me.cboTemparory.TabIndex = 91
        '
        'cboPermanent
        '
        Me.cboPermanent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPermanent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPermanent.FormattingEnabled = True
        Me.cboPermanent.Location = New System.Drawing.Point(107, 30)
        Me.cboPermanent.Name = "cboPermanent"
        Me.cboPermanent.Size = New System.Drawing.Size(225, 21)
        Me.cboPermanent.TabIndex = 2
        '
        'lvPeriod
        '
        Me.lvPeriod.BackColorOnChecked = False
        Me.lvPeriod.CheckBoxes = True
        Me.lvPeriod.ColumnHeaders = Nothing
        Me.lvPeriod.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhPCheck, Me.colhPeriodName, Me.objcolhPeriodStart})
        Me.lvPeriod.CompulsoryColumns = ""
        Me.lvPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPeriod.FullRowSelect = True
        Me.lvPeriod.GridLines = True
        Me.lvPeriod.GroupingColumn = Nothing
        Me.lvPeriod.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPeriod.HideSelection = False
        Me.lvPeriod.Location = New System.Drawing.Point(118, 180)
        Me.lvPeriod.MinColumnWidth = 50
        Me.lvPeriod.MultiSelect = False
        Me.lvPeriod.Name = "lvPeriod"
        Me.lvPeriod.OptionalColumns = ""
        Me.lvPeriod.ShowMoreItem = False
        Me.lvPeriod.ShowSaveItem = False
        Me.lvPeriod.ShowSelectAll = True
        Me.lvPeriod.ShowSizeAllColumnsToFit = True
        Me.lvPeriod.Size = New System.Drawing.Size(229, 126)
        Me.lvPeriod.Sortable = True
        Me.lvPeriod.TabIndex = 87
        Me.lvPeriod.UseCompatibleStateImageBehavior = False
        Me.lvPeriod.View = System.Windows.Forms.View.Details
        '
        'objColhPCheck
        '
        Me.objColhPCheck.Tag = "objColhPCheck"
        Me.objColhPCheck.Text = ""
        Me.objColhPCheck.Width = 25
        '
        'colhPeriodName
        '
        Me.colhPeriodName.Tag = "colhPeriodName"
        Me.colhPeriodName.Text = "Periods"
        Me.colhPeriodName.Width = 200
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 184)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(104, 15)
        Me.lblPeriod.TabIndex = 88
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(249, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 85
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSlabEffectivePeriod
        '
        Me.lblSlabEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSlabEffectivePeriod.Location = New System.Drawing.Point(8, 59)
        Me.lblSlabEffectivePeriod.Name = "lblSlabEffectivePeriod"
        Me.lblSlabEffectivePeriod.Size = New System.Drawing.Size(104, 15)
        Me.lblSlabEffectivePeriod.TabIndex = 83
        Me.lblSlabEffectivePeriod.Text = "EffectivePeriod"
        Me.lblSlabEffectivePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSlabEffectivePeriod
        '
        Me.cboSlabEffectivePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSlabEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSlabEffectivePeriod.FormattingEnabled = True
        Me.cboSlabEffectivePeriod.Location = New System.Drawing.Point(118, 57)
        Me.cboSlabEffectivePeriod.Name = "cboSlabEffectivePeriod"
        Me.cboSlabEffectivePeriod.Size = New System.Drawing.Size(229, 21)
        Me.cboSlabEffectivePeriod.TabIndex = 2
        '
        'lblSlabTranHead
        '
        Me.lblSlabTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSlabTranHead.Location = New System.Drawing.Point(8, 32)
        Me.lblSlabTranHead.Name = "lblSlabTranHead"
        Me.lblSlabTranHead.Size = New System.Drawing.Size(104, 15)
        Me.lblSlabTranHead.TabIndex = 1
        Me.lblSlabTranHead.Text = "Slab Trans. Head"
        Me.lblSlabTranHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTranHead
        '
        Me.cboTranHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead.FormattingEnabled = True
        Me.cboTranHead.Location = New System.Drawing.Point(118, 30)
        Me.cboTranHead.Name = "cboTranHead"
        Me.cboTranHead.Size = New System.Drawing.Size(229, 21)
        Me.cboTranHead.TabIndex = 1
        '
        'objcolhPeriodStart
        '
        Me.objcolhPeriodStart.Tag = "objcolhPeriodStart"
        Me.objcolhPeriodStart.Width = 0
        '
        'frmAnnualPayeReturn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 508)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAnnualPayeReturn"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblSlabTranHead As System.Windows.Forms.Label
    Friend WithEvents cboTranHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblSlabEffectivePeriod As System.Windows.Forms.Label
    Friend WithEvents cboSlabEffectivePeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents objchkAllPeriod As System.Windows.Forms.CheckBox
    Friend WithEvents lvPeriod As eZee.Common.eZeeListView
    Friend WithEvents objColhPCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriodName As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboTemparory As System.Windows.Forms.ComboBox
    Friend WithEvents cboPermanent As System.Windows.Forms.ComboBox
    Friend WithEvents objcolhPeriodStart As System.Windows.Forms.ColumnHeader
End Class
