#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmSkillsAndDevelopmentLevyMonthlyReturn

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmSkillsAndDevelopmentLevyMonthlyReturn"
    Private objSkillsAndDevelopmentLevyMonthlyReturn As clsSkillsAndDevelopmentLevyMonthlyReturn
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private eReport As enArutiReport
    Private mintFirstOpenPeriod As Integer = 0

    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintPaidCurrencyId As Integer = 0

#End Region

#Region " Contructor "
    Public Sub New(ByVal menReport As enArutiReport)

        eReport = menReport

        objSkillsAndDevelopmentLevyMonthlyReturn = New clsSkillsAndDevelopmentLevyMonthlyReturn(menReport, User._Object._Languageunkid, Company._Object._Companyunkid)

        If Company._Object._Countryunkid = 112 Then
            _Show_ExcelExtra_Menu = True
            _Show_ExcelDataOnly_Menu = True
        Else
            _Show_ExcelExtra_Menu = False
        End If

        objSkillsAndDevelopmentLevyMonthlyReturn.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Currency = 1
        SDL_AMOUNT = 2
        SDL_PAYABLE = 3
    End Enum
#End Region

#Region " Private Function "
    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmpContribution As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim objperiod As New clscommom_period_Tran

        Try

            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)

            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , , , )
            'Hemant (07 Sep 2020) -- Start
            'Enhancement : Comment By Allan on Jira - We need to show all transaction heads on SDL Amount field regardless of the calculation type.
            'Dim strSystemGeneratedHead As String = " " & enCalcType.NET_PAY & ", " & enCalcType.TOTAL_EARNING & ", " & enCalcType.TOTAL_DEDUCTION & ", " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & ", " & enCalcType.TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_CASH_BENEFIT_TOTAL & ""
            'Dim dt As DataTable = New DataView(dsCombos.Tables("Head"), "calctype_id NOT IN (" & strSystemGeneratedHead & ")", "", DataViewRowState.CurrentRows).ToTable
            Dim dt As DataTable = New DataView(dsCombos.Tables("Head"), "", "", DataViewRowState.CurrentRows).ToTable
            'Hemant (07 Sep 2020) -- End            

            With cboSDLAmount
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dt
                .SelectedValue = 0
            End With

            With cboSDLPayable
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dt.Copy()
                .SelectedValue = 0
            End With

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", True)
            cboCurrency.ValueMember = "countryunkid"
            cboCurrency.DisplayMember = "currency_sign"
            cboCurrency.DataSource = dsCombos.Tables("Currency")

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objperiod = Nothing
            objEmpContribution = Nothing
            objMaster = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try

            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboSDLAmount.SelectedValue = 0
            cboSDLPayable.SelectedValue = 0
            cboCurrency.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            Call objSkillsAndDevelopmentLevyMonthlyReturn.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            End If


            objSkillsAndDevelopmentLevyMonthlyReturn._SDLAmountId = CInt(cboSDLAmount.SelectedValue)
            objSkillsAndDevelopmentLevyMonthlyReturn._SDLPayableId = CInt(cboSDLPayable.SelectedValue)

            If (mintBaseCurrId = mintPaidCurrencyId) Or mintPaidCurrencyId <= 0 Then
                mintPaidCurrencyId = mintBaseCurrId
            End If

            objSkillsAndDevelopmentLevyMonthlyReturn._CountryId = CInt(cboCurrency.SelectedValue)
            objSkillsAndDevelopmentLevyMonthlyReturn._BaseCurrencyId = mintBaseCurrId
            objSkillsAndDevelopmentLevyMonthlyReturn._PaidCurrencyId = mintPaidCurrencyId

            If mdecBaseExRate > 0 AndAlso mdecPaidExRate Then
                objSkillsAndDevelopmentLevyMonthlyReturn._ConversionRate = mdecPaidExRate / mdecBaseExRate
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objSkillsAndDevelopmentLevyMonthlyReturn._PeriodId = cboPeriod.SelectedValue
                objSkillsAndDevelopmentLevyMonthlyReturn._PeriodName = cboPeriod.Text
                objSkillsAndDevelopmentLevyMonthlyReturn._PeriodCode = CType(cboPeriod.SelectedItem, DataRowView).Item("code").ToString
            End If

            objSkillsAndDevelopmentLevyMonthlyReturn._ViewByIds = mstrStringIds
            objSkillsAndDevelopmentLevyMonthlyReturn._ViewIndex = mintViewIdx
            objSkillsAndDevelopmentLevyMonthlyReturn._ViewByName = mstrStringName
            objSkillsAndDevelopmentLevyMonthlyReturn._Analysis_Fields = mstrAnalysis_Fields
            objSkillsAndDevelopmentLevyMonthlyReturn._Analysis_Join = mstrAnalysis_Join
            objSkillsAndDevelopmentLevyMonthlyReturn._Report_GroupName = mstrReport_GroupName

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter()", mstrModuleName)
        End Try
    End Function

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Skills_And_Development_Levy_Monthly_Return)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Currency
                            cboCurrency.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                        Case enHeadTypeId.SDL_AMOUNT
                            cboSDLAmount.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.SDL_PAYABLE
                            cboSDLPayable.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                        
                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
#End Region

#Region " Forms "
    Private Sub frmSkillsAndDevelopmentLevyMonthlyReturn_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSkillsAndDevelopmentLevyMonthlyReturn = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmSkillsAndDevelopmentLevyMonthlyReturn_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSkillsAndDevelopmentLevyMonthlyReturn_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Me._Title = objSkillsAndDevelopmentLevyMonthlyReturn._ReportName
            Me._Message = objSkillsAndDevelopmentLevyMonthlyReturn._ReportDesc

            Call OtherSettings()

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSkillsAndDevelopmentLevyMonthlyReturn_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "
    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub
            objSkillsAndDevelopmentLevyMonthlyReturn.generateReportNew(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       ConfigParameter._Object._UserAccessModeSetting, _
                                       True, ConfigParameter._Object._ExportReportPath, _
                                       ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            objSkillsAndDevelopmentLevyMonthlyReturn.generateReportNew(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       ConfigParameter._Object._UserAccessModeSetting, _
                                       True, ConfigParameter._Object._ExportReportPath, _
                                       ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSkillsAndDevelopmentLevyMonthlyReturn.SetMessages()
            objfrm._Other_ModuleNames = "clsSkillsAndDevelopmentLevyMonthlyReturn"

            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To 13
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.Skills_And_Development_Levy_Monthly_Return
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Currency
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCurrency.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.SDL_AMOUNT
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboSDLAmount.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.SDL_PAYABLE
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboSDLPayable.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"
    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'S.SANDEEP |28-JUL-2021| -- START
    Private Sub lnkExportDataxlsx_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkExportDataxlsx.LinkClicked
        Try
            If Not SetFilter() Then Exit Sub
            Dim ifile As String = "SDLtemplate.xlsm"
            Dim ipath As String = My.Computer.FileSystem.SpecialDirectories.Temp
            IO.File.WriteAllBytes(IO.Path.Combine(ipath, ifile), My.Resources.SDLtemplate)
            If IO.File.Exists(IO.Path.Combine(ipath, ifile)) Then

                Dim strExportPath As String = ""
                If System.IO.Directory.Exists(ConfigParameter._Object._ExportReportPath) = False Then
                    Dim dig As New System.Windows.Forms.FolderBrowserDialog
                    dig.Description = "Select Folder Where to export report."

                    If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                        strExportPath = dig.SelectedPath
                    Else
                        Exit Sub
                    End If
                Else
                    strExportPath = ConfigParameter._Object._ExportReportPath
                End If

                objSkillsAndDevelopmentLevyMonthlyReturn.generateReportNew(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                       ConfigParameter._Object._UserAccessModeSetting, _
                                       True, strExportPath, _
                                       ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.ExcelXLSM, ConfigParameter._Object._Base_CurrencyId)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkExportDataxlsx_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |28-JUL-2021| -- END

#End Region

#Region "ComboBox Event"
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Try
            objlblExRate.Text = ""
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            If CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                Dim ds As DataSet = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                If ds.Tables("ExRate").Rows.Count > 0 Then
                    mdecBaseExRate = CDec(ds.Tables("ExRate").Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(ds.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                    mintPaidCurrencyId = CInt(ds.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
                    objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & ds.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboCurrency.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                Dim dsList As DataSet = Nothing
                dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate")).ToTable

                If dtTable.Rows.Count > 0 Then
                    mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                    mintPaidCurrencyId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                Else
                    mdecBaseExRate = 0
                    mdecPaidExRate = 0
                    mintPaidCurrencyId = 0
                    objlblExRate.Text = ""
                End If
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
                mintPaidCurrencyId = 0
                objlblExRate.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

            Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)
			Me.lblSDLAmount.Text = Language._Object.getCaption(Me.lblSDLAmount.Name, Me.lblSDLAmount.Text)
			Me.lblSDLPayable.Text = Language._Object.getCaption(Me.lblSDLPayable.Name, Me.lblSDLPayable.Text)
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.chkShowBasicSal.Text = Language._Object.getCaption(Me.chkShowBasicSal.Name, Me.chkShowBasicSal.Text)
			Me.lblCompanyHeadType.Text = Language._Object.getCaption(Me.lblCompanyHeadType.Name, Me.lblCompanyHeadType.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblEmpContriHeadType.Text = Language._Object.getCaption(Me.lblEmpContriHeadType.Name, Me.lblEmpContriHeadType.Text)
			Me.lblPeriod1.Text = Language._Object.getCaption(Me.lblPeriod1.Name, Me.lblPeriod1.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

    
End Class
