﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSkillsAndDevelopmentLevyMonthlyReturn
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSkillsAndDevelopmentLevyMonthlyReturn))
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.LblCurrency = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblSDLAmount = New System.Windows.Forms.Label
        Me.cboSDLAmount = New System.Windows.Forms.ComboBox
        Me.lblSDLPayable = New System.Windows.Forms.Label
        Me.cboSDLPayable = New System.Windows.Forms.ComboBox
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblExRate = New System.Windows.Forms.Label
        Me.cboEmpHeadType = New System.Windows.Forms.ComboBox
        Me.chkShowBasicSal = New System.Windows.Forms.CheckBox
        Me.lblCompanyHeadType = New System.Windows.Forms.Label
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblEmpContriHeadType = New System.Windows.Forms.Label
        Me.cboCoHeadType = New System.Windows.Forms.ComboBox
        Me.lblPeriod1 = New System.Windows.Forms.Label
        Me.cboPeriod1 = New System.Windows.Forms.ComboBox
        Me.lnkExportDataxlsx = New System.Windows.Forms.LinkLabel
        Me.gbMandatoryInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 279)
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(9, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(79, 15)
        Me.lblPeriod.TabIndex = 109
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 180
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(107, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboPeriod.TabIndex = 110
        '
        'LblCurrency
        '
        Me.LblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrency.Location = New System.Drawing.Point(9, 63)
        Me.LblCurrency.Name = "LblCurrency"
        Me.LblCurrency.Size = New System.Drawing.Size(94, 15)
        Me.LblCurrency.TabIndex = 108
        Me.LblCurrency.Text = "Currency"
        Me.LblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 180
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(107, 61)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(126, 21)
        Me.cboCurrency.TabIndex = 107
        '
        'lblSDLAmount
        '
        Me.lblSDLAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSDLAmount.Location = New System.Drawing.Point(9, 91)
        Me.lblSDLAmount.Name = "lblSDLAmount"
        Me.lblSDLAmount.Size = New System.Drawing.Size(94, 15)
        Me.lblSDLAmount.TabIndex = 116
        Me.lblSDLAmount.Text = "SDL Amount"
        Me.lblSDLAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSDLAmount
        '
        Me.cboSDLAmount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSDLAmount.DropDownWidth = 180
        Me.cboSDLAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSDLAmount.FormattingEnabled = True
        Me.cboSDLAmount.Location = New System.Drawing.Point(107, 88)
        Me.cboSDLAmount.Name = "cboSDLAmount"
        Me.cboSDLAmount.Size = New System.Drawing.Size(126, 21)
        Me.cboSDLAmount.TabIndex = 115
        '
        'lblSDLPayable
        '
        Me.lblSDLPayable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSDLPayable.Location = New System.Drawing.Point(260, 91)
        Me.lblSDLPayable.Name = "lblSDLPayable"
        Me.lblSDLPayable.Size = New System.Drawing.Size(94, 15)
        Me.lblSDLPayable.TabIndex = 118
        Me.lblSDLPayable.Text = "SDL Payable"
        Me.lblSDLPayable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSDLPayable
        '
        Me.cboSDLPayable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSDLPayable.DropDownWidth = 180
        Me.cboSDLPayable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSDLPayable.FormattingEnabled = True
        Me.cboSDLPayable.Location = New System.Drawing.Point(360, 88)
        Me.cboSDLPayable.Name = "cboSDLPayable"
        Me.cboSDLPayable.Size = New System.Drawing.Size(118, 21)
        Me.cboSDLPayable.TabIndex = 117
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.lnkExportDataxlsx)
        Me.gbMandatoryInfo.Controls.Add(Me.btnSaveSelection)
        Me.gbMandatoryInfo.Controls.Add(Me.lblSDLAmount)
        Me.gbMandatoryInfo.Controls.Add(Me.lblSDLPayable)
        Me.gbMandatoryInfo.Controls.Add(Me.cboSDLAmount)
        Me.gbMandatoryInfo.Controls.Add(Me.cboSDLPayable)
        Me.gbMandatoryInfo.Controls.Add(Me.objlblExRate)
        Me.gbMandatoryInfo.Controls.Add(Me.cboEmpHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.chkShowBasicSal)
        Me.gbMandatoryInfo.Controls.Add(Me.lblCompanyHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.lblEmpContriHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCoHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod1)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod1)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(10, 70)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(488, 172)
        Me.gbMandatoryInfo.TabIndex = 119
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(107, 131)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 120
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'objlblExRate
        '
        Me.objlblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExRate.Location = New System.Drawing.Point(235, 65)
        Me.objlblExRate.Name = "objlblExRate"
        Me.objlblExRate.Size = New System.Drawing.Size(207, 15)
        Me.objlblExRate.TabIndex = 79
        Me.objlblExRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmpHeadType
        '
        Me.cboEmpHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpHeadType.DropDownWidth = 180
        Me.cboEmpHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpHeadType.FormattingEnabled = True
        Me.cboEmpHeadType.Location = New System.Drawing.Point(107, 339)
        Me.cboEmpHeadType.Name = "cboEmpHeadType"
        Me.cboEmpHeadType.Size = New System.Drawing.Size(118, 21)
        Me.cboEmpHeadType.TabIndex = 59
        Me.cboEmpHeadType.Visible = False
        '
        'chkShowBasicSal
        '
        Me.chkShowBasicSal.Checked = True
        Me.chkShowBasicSal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowBasicSal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowBasicSal.Location = New System.Drawing.Point(107, 343)
        Me.chkShowBasicSal.Name = "chkShowBasicSal"
        Me.chkShowBasicSal.Size = New System.Drawing.Size(205, 17)
        Me.chkShowBasicSal.TabIndex = 73
        Me.chkShowBasicSal.Text = "Show Basic Salary"
        Me.chkShowBasicSal.UseVisualStyleBackColor = True
        Me.chkShowBasicSal.Visible = False
        '
        'lblCompanyHeadType
        '
        Me.lblCompanyHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyHeadType.Location = New System.Drawing.Point(231, 343)
        Me.lblCompanyHeadType.Name = "lblCompanyHeadType"
        Me.lblCompanyHeadType.Size = New System.Drawing.Size(87, 15)
        Me.lblCompanyHeadType.TabIndex = 62
        Me.lblCompanyHeadType.Text = "Co. Contribution"
        Me.lblCompanyHeadType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCompanyHeadType.Visible = False
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(382, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 69
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEmpContriHeadType
        '
        Me.lblEmpContriHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpContriHeadType.Location = New System.Drawing.Point(7, 342)
        Me.lblEmpContriHeadType.Name = "lblEmpContriHeadType"
        Me.lblEmpContriHeadType.Size = New System.Drawing.Size(94, 15)
        Me.lblEmpContriHeadType.TabIndex = 60
        Me.lblEmpContriHeadType.Text = "Emp. Contribution"
        Me.lblEmpContriHeadType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEmpContriHeadType.Visible = False
        '
        'cboCoHeadType
        '
        Me.cboCoHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCoHeadType.DropDownWidth = 180
        Me.cboCoHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCoHeadType.FormattingEnabled = True
        Me.cboCoHeadType.Location = New System.Drawing.Point(324, 340)
        Me.cboCoHeadType.Name = "cboCoHeadType"
        Me.cboCoHeadType.Size = New System.Drawing.Size(118, 21)
        Me.cboCoHeadType.TabIndex = 61
        Me.cboCoHeadType.Visible = False
        '
        'lblPeriod1
        '
        Me.lblPeriod1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod1.Location = New System.Drawing.Point(7, 368)
        Me.lblPeriod1.Name = "lblPeriod1"
        Me.lblPeriod1.Size = New System.Drawing.Size(94, 15)
        Me.lblPeriod1.TabIndex = 57
        Me.lblPeriod1.Text = "Period"
        Me.lblPeriod1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPeriod1.Visible = False
        '
        'cboPeriod1
        '
        Me.cboPeriod1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod1.DropDownWidth = 180
        Me.cboPeriod1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod1.FormattingEnabled = True
        Me.cboPeriod1.Location = New System.Drawing.Point(107, 366)
        Me.cboPeriod1.Name = "cboPeriod1"
        Me.cboPeriod1.Size = New System.Drawing.Size(118, 21)
        Me.cboPeriod1.TabIndex = 58
        Me.cboPeriod1.Visible = False
        '
        'lnkExportDataxlsx
        '
        Me.lnkExportDataxlsx.BackColor = System.Drawing.Color.Transparent
        Me.lnkExportDataxlsx.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkExportDataxlsx.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkExportDataxlsx.Location = New System.Drawing.Point(270, 138)
        Me.lnkExportDataxlsx.Name = "lnkExportDataxlsx"
        Me.lnkExportDataxlsx.Size = New System.Drawing.Size(208, 17)
        Me.lnkExportDataxlsx.TabIndex = 122
        Me.lnkExportDataxlsx.TabStop = True
        Me.lnkExportDataxlsx.Text = "Export in xslm format (Data Only)"
        Me.lnkExportDataxlsx.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmSkillsAndDevelopmentLevyMonthlyReturn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 334)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Name = "frmSkillsAndDevelopmentLevyMonthlyReturn"
        Me.Text = "frmSkillsAndDevelopmentLevyMonthlyReturn"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbMandatoryInfo, 0)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents LblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblSDLAmount As System.Windows.Forms.Label
    Friend WithEvents cboSDLAmount As System.Windows.Forms.ComboBox
    Friend WithEvents lblSDLPayable As System.Windows.Forms.Label
    Friend WithEvents cboSDLPayable As System.Windows.Forms.ComboBox
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblExRate As System.Windows.Forms.Label
    Friend WithEvents cboEmpHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowBasicSal As System.Windows.Forms.CheckBox
    Friend WithEvents lblCompanyHeadType As System.Windows.Forms.Label
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblEmpContriHeadType As System.Windows.Forms.Label
    Friend WithEvents cboCoHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod1 As System.Windows.Forms.Label
    Friend WithEvents cboPeriod1 As System.Windows.Forms.ComboBox
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents lnkExportDataxlsx As System.Windows.Forms.LinkLabel
End Class
