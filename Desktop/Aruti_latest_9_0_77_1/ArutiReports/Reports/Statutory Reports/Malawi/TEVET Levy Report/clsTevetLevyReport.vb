'************************************************************************************************************************************
'Class Name : clsTevetLevyReport.vb
'Purpose    :
'Date       :04/05/2018
'Written By :Gajanan
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class clsTevetLevyReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTevetLevyReport"
    Private mstrReportId As String = enArutiReport.TEVET_LEVY_REPORT
    Dim objDataOperation As clsDataOperation


#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintEmployeeID As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mstrPeriodIDs As String = String.Empty
    Private mstrFinancialYear = String.Empty
    Private mdecPercentageLevy As Decimal = 0

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty

    Private mstrPeriodNames As String = String.Empty
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    Private mstrPaymentMode As String = String.Empty
    Private mdecPercentage As Decimal = 1

    Private mstrFmtCurrency As String = GUI.fmtCurrency
    Private mdtDatabaseEndDate As Date = FinancialYear._Object._Database_End_Date
    Private mblnApplyUserAccessFilter As Boolean = True

    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIDs() As String
        Set(ByVal value As String)
            mstrPeriodIDs = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriodEndDate = value
        End Set
    End Property
    Public WriteOnly Property _FinancialYear() As String
        Set(ByVal value As String)
            mstrFinancialYear = value
        End Set
    End Property

    Public WriteOnly Property _FmtCurrency() As String
        Set(ByVal value As String)
            mstrFmtCurrency = value
        End Set
    End Property

    Public WriteOnly Property _PaymentMode() As String
        Set(ByVal value As String)
            mstrPaymentMode = value
        End Set
    End Property

    Public WriteOnly Property _Percentage() As Decimal
        Set(ByVal value As Decimal)
            mdecPercentage = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtDatabaseEndDate = value
        End Set
    End Property

    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrReport_GroupName = String.Empty

            mintEmployeeID = 0
            mstrEmployeeName = ""
            mstrPeriodIDs = ""
            mstrPeriodNames = ""
            mstrFmtCurrency = ""

            mstrPaymentMode = ""
            mdecPercentage = 1

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeID > 0 Then
                objDataOperation.AddParameter("@EmployeeID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeID)
            End If
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId

            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            'Gajanan (11 May 2018) -- Start
            'Malawi Issue Fix -: Change In Query OF employee Head Count ,Add Condition For Get Only Active Employee On Selected Period In statutory report for Malawi - TEVET Levy Report.
            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, True, False, strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtPeriodEndDate.Date, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)


            StrQ = "SELECT  cfcommon_period_tran.periodunkid " & _
                         ", cfcommon_period_tran.period_code " & _
                         ", cfcommon_period_tran.period_name " & _
                         ", cfcommon_period_tran.start_date " & _
                         ", cfcommon_period_tran.end_date " & _
                         ", ISNULL(SUM(prpayrollprocess_tran.amount), 0) AS TotalEarning " & _
                   "FROM " & strDatabaseName & "..prpayrollprocess_tran " & _
                         "LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                         "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                         "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                         "AND prtnaleave_tran.isvoid = 0 " & _
                         "AND cfcommon_period_tran.isactive = 1 " & _
                         "AND cfcommon_period_tran.modulerefid = 1 " & _
                         "AND payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                         "AND prpayrollprocess_tran.add_deduct = " & CInt(enAddDeduct.Add) & " "

            If mintEmployeeID > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmployeeID "
            End If

            StrQ &= "GROUP BY cfcommon_period_tran.periodunkid " & _
                    ", cfcommon_period_tran.period_code " & _
                    ", cfcommon_period_tran.period_name " & _
                    ", cfcommon_period_tran.start_date " & _
                    ", cfcommon_period_tran.end_date " & _
                    " ORDER BY cfcommon_period_tran.end_date "

            Call FilterTitleAndFilterQuery()

            'StrQ &= Me._FilterQuery


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objMaster As New clsMasterData
            Dim dsCombo As DataSet = objMaster.getGenderList("List")
            Dim dicGender As Dictionary(Of Integer, String) = (From p In dsCombo.Tables(0) Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            StrQ = "SELECT " & _
                        "hremployee_master.gender " & _
                      ", COUNT(hremployee_master.employeeunkid) AS Total "

            StrQ &= ", CASE hremployee_master.gender "
            For Each pair In dicGender
                StrQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            StrQ &= " ELSE '' END AS gender_name "

            StrQ &= "FROM " & strDatabaseName & "..hremployee_master "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If mblnApplyUserAccessFilter = True AndAlso xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "WHERE hremployee_master.isapproved = 1 "

            If mblnApplyUserAccessFilter = True AndAlso xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mintEmployeeID > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @EmployeeID "
            End If

            If xDataFilterQry.Trim.Length > 0 Then
                StrQ &= " " & xDataFilterQry
            End If
            'Gajanan (11 May 2018) -- End
            StrQ &= "GROUP BY hremployee_master.gender "

            Dim dsGender As DataSet = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing

            Dim TotalGrossEmoluments As Decimal = 0
            Dim PercentageLevy As Decimal = 0


            For Each dRow As DataRow In dsList.Tables(0).Rows
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dRow.Item("periodunkid")
                rpt_Row.Item("Column2") = dRow.Item("period_code")
                rpt_Row.Item("Column3") = dRow.Item("period_name")
                rpt_Row.Item("Column4") = dRow.Item("start_date")
                rpt_Row.Item("Column5") = dRow.Item("end_date")
                rpt_Row.Item("Column6") = Format(CDec(dRow.Item("TotalEarning")), mstrFmtCurrency)

                TotalGrossEmoluments += CDec(dRow.Item("TotalEarning"))

                rpt_Row.Item("Column7") = Format(TotalGrossEmoluments, mstrFmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next
            mdecPercentageLevy = Format(CDec(((TotalGrossEmoluments * mdecPercentage) / 100)), mstrFmtCurrency)


            Dim intMale As Integer = 0
            Dim intFemale As Integer = 0
            Dim intTotal As Integer = 0
            For Each dRow As DataRow In dsGender.Tables(0).Rows

                If CInt(dRow.Item("gender")) = 1 Then
                    intMale = CInt(dRow.Item("Total"))
                ElseIf CInt(dRow.Item("gender")) = 2 Then
                    intFemale = CInt(dRow.Item("Total"))
                End If

                intTotal += CInt(dRow.Item("Total"))
            Next


            objRpt = New ArutiReport.Designer.rptTevetLevyReport

            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = intCompanyUnkid
            Dim strAdd3 As String = ""
            If objCompany._City_Name.Trim.Length > 0 Then
                strAdd3 += objCompany._City_Name
            End If
            If objCompany._State_Name.Trim.Length > 0 Then
                strAdd3 += " " + objCompany._State_Name
            End If
            If objCompany._Country_Name.Trim.Length > 0 Then
                strAdd3 += " " + objCompany._Country_Name
            End If
            If objCompany._Post_Code_No.Trim.Length > 0 Then
                strAdd3 += " " + objCompany._Post_Code_No
            End If
            strAdd3 = Trim(strAdd3)


            Dim dsPerm As New DataSet : Dim dsTemp As New DataSet


            ReportFunction.TextChange(objRpt, "txtEmpName", objCompany._Name)
            ReportFunction.TextChange(objRpt, "txtEmpPostalAddress", objCompany._Address1)
            ReportFunction.TextChange(objRpt, "txtEmpPhyAddress", objCompany._Address2)
            ReportFunction.TextChange(objRpt, "txtRegion", objCompany._State_Name)
            ReportFunction.TextChange(objRpt, "txtDistrict", objCompany._District)
            ReportFunction.TextChange(objRpt, "txtPhoneNumber", objCompany._Phone1)
            ReportFunction.TextChange(objRpt, "txtFaxNumber", objCompany._Fax)
            ReportFunction.TextChange(objRpt, "txtEmail", objCompany._Email)
            ReportFunction.TextChange(objRpt, "txtContactPerson", objCompany._Reg1_Value)
            ReportFunction.TextChange(objRpt, "txtLevyCalculationYear", mstrFinancialYear.ToString())
            ReportFunction.TextChange(objRpt, "txtLevyCalculationYear2", mstrFinancialYear.ToString())
            ReportFunction.TextChange(objRpt, "txtEmployerCode", objCompany._Vatno)
            ReportFunction.TextChange(objRpt, "txtPercentage", mdecPercentage)
            ReportFunction.TextChange(objRpt, "txtPercentageLevy", mdecPercentageLevy.ToString(mstrFmtCurrency))
            ReportFunction.TextChange(objRpt, "txtPaymentMode", mstrPaymentMode)
            ReportFunction.TextChange(objRpt, "txtMale", intMale)
            ReportFunction.TextChange(objRpt, "txtFemale", intFemale)
            ReportFunction.TextChange(objRpt, "txtTotalEmployee", intTotal)

            ReportFunction.TextChange(objRpt, "lblTitle", Language.getMessage(mstrModuleName, 1, "Employer's Data Form"))
            ReportFunction.TextChange(objRpt, "lblNote", Language.getMessage(mstrModuleName, 2, "NOTE: Submit Your Levy Calculated In Step 2 Below Using The Provided Employer's Data Form. A Separate Invoice Is Not Produced"))
            ReportFunction.TextChange(objRpt, "lblLevyCalculationYear", Language.getMessage(mstrModuleName, 3, "Levy Calculation For"))
            ReportFunction.TextChange(objRpt, "lblEmployerCode", Language.getMessage(mstrModuleName, 4, "Teveta Employer Code (ID)"))
            ReportFunction.TextChange(objRpt, "lblSection1", Language.getMessage(mstrModuleName, 5, "Section 1: Basic Information - Please Provide the required Information In the blank spaces below to enable TEVETA update its records about your organization"))
            ReportFunction.TextChange(objRpt, "lblsr1", Language.getMessage(mstrModuleName, 6, "1"))
            ReportFunction.TextChange(objRpt, "lblEmpName", Language.getMessage(mstrModuleName, 7, "Employer's Name"))
            ReportFunction.TextChange(objRpt, "lblsr2", Language.getMessage(mstrModuleName, 8, "2"))
            ReportFunction.TextChange(objRpt, "lblEmpPostalAddress", Language.getMessage(mstrModuleName, 9, "Employer's Postal Address"))
            ReportFunction.TextChange(objRpt, "lblsr3", Language.getMessage(mstrModuleName, 10, "3"))
            ReportFunction.TextChange(objRpt, "lblEmpPhyAddress", Language.getMessage(mstrModuleName, 11, "Employer's Physical Address"))
            ReportFunction.TextChange(objRpt, "lblsr4", Language.getMessage(mstrModuleName, 12, "4"))
            ReportFunction.TextChange(objRpt, "lblRegion", Language.getMessage(mstrModuleName, 13, "Region"))
            ReportFunction.TextChange(objRpt, "lblsr5", Language.getMessage(mstrModuleName, 14, "5"))
            ReportFunction.TextChange(objRpt, "lblDistrict", Language.getMessage(mstrModuleName, 15, "District"))
            ReportFunction.TextChange(objRpt, "lblsr6", Language.getMessage(mstrModuleName, 16, "6"))
            ReportFunction.TextChange(objRpt, "lblPhoneNumber", Language.getMessage(mstrModuleName, 17, "Phone Number"))
            ReportFunction.TextChange(objRpt, "lblsr7", Language.getMessage(mstrModuleName, 18, "7"))
            ReportFunction.TextChange(objRpt, "lblFaxNumber", Language.getMessage(mstrModuleName, 19, "Fax Number"))
            ReportFunction.TextChange(objRpt, "lblsr8", Language.getMessage(mstrModuleName, 20, "8"))
            ReportFunction.TextChange(objRpt, "lblEmail", Language.getMessage(mstrModuleName, 21, "E-mail"))
            ReportFunction.TextChange(objRpt, "lblsr9", Language.getMessage(mstrModuleName, 22, "9"))
            ReportFunction.TextChange(objRpt, "lblContactPerson", Language.getMessage(mstrModuleName, 23, "Contact Person"))
            ReportFunction.TextChange(objRpt, "lblsr10", Language.getMessage(mstrModuleName, 24, "10"))
            ReportFunction.TextChange(objRpt, "lblNoOfEmp", Language.getMessage(mstrModuleName, 25, "Number Of Employee"))
            ReportFunction.TextChange(objRpt, "lblMale", Language.getMessage(mstrModuleName, 26, "Male"))
            ReportFunction.TextChange(objRpt, "lblFemale", Language.getMessage(mstrModuleName, 27, "Female"))
            ReportFunction.TextChange(objRpt, "lblTotalEmployee", Language.getMessage(mstrModuleName, 28, "Total"))
            ReportFunction.TextChange(objRpt, "lblSection2", Language.getMessage(mstrModuleName, 29, "Section 2: Payroll Information - Please provide details of gross emluments for the year"))
            ReportFunction.TextChange(objRpt, "lblMonthOf", Language.getMessage(mstrModuleName, 30, "Month Of The "))
            ReportFunction.TextChange(objRpt, "lblFnYear", Language.getMessage(mstrModuleName, 31, "Financial Year"))
            ReportFunction.TextChange(objRpt, "lblGrossEmoluments", Language.getMessage(mstrModuleName, 32, "Gross Emoluments"))
            ReportFunction.TextChange(objRpt, "lblTotal", Language.getMessage(mstrModuleName, 33, "Total"))
            ReportFunction.TextChange(objRpt, "lblSection3", Language.getMessage(mstrModuleName, 34, "Section 3: Calculation Of TEVET Levy"))
            ReportFunction.TextChange(objRpt, "lblstep1", Language.getMessage(mstrModuleName, 35, "Step 1: Show The Basis Of Calculating TEVET Levy"))
            ReportFunction.TextChange(objRpt, "lblTransferGross", Language.getMessage(mstrModuleName, 36, "Transfer The Gross Emoluments Tabulated In Section 2 Above"))
            ReportFunction.TextChange(objRpt, "lblstep2", Language.getMessage(mstrModuleName, 37, "Step 2: Calculate The TEVET Levy Amount"))
            ReportFunction.TextChange(objRpt, "lbltake", Language.getMessage(mstrModuleName, 38, "Take"))
            ReportFunction.TextChange(objRpt, "lblPercentageOfTotal", Language.getMessage(mstrModuleName, 39, "% Of Total In Step 1 Above = Levy For Current Year"))
            ReportFunction.TextChange(objRpt, "lblstep3", Language.getMessage(mstrModuleName, 40, "Step 3: Please Select You Preferred Mode Of Amount"))

            objRpt.SetDataSource(rpt_Data)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function


#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employer's Data Form")
            Language.setMessage(mstrModuleName, 2, "NOTE: Submit Your Levy Calculated In Step 2 Below Using The Provided Employer's Data Form. A Separate Invoice Is Not Produced")
            Language.setMessage(mstrModuleName, 3, "Levy Calculation For")
            Language.setMessage(mstrModuleName, 4, "Teveta Employer Code (ID)")
            Language.setMessage(mstrModuleName, 5, "Section 1: Basic Information - Please Provide the required Information In the blank spaces below to enable TEVETA update its records about your organization")
            Language.setMessage(mstrModuleName, 6, "1")
            Language.setMessage(mstrModuleName, 7, "Employer's Name")
            Language.setMessage(mstrModuleName, 8, "2")
            Language.setMessage(mstrModuleName, 9, "Employer's Postal Address")
            Language.setMessage(mstrModuleName, 10, "3")
            Language.setMessage(mstrModuleName, 11, "Employer's Physical Address")
            Language.setMessage(mstrModuleName, 12, "4")
            Language.setMessage(mstrModuleName, 13, "Region")
            Language.setMessage(mstrModuleName, 14, "5")
            Language.setMessage(mstrModuleName, 15, "District")
            Language.setMessage(mstrModuleName, 16, "6")
            Language.setMessage(mstrModuleName, 17, "Phone Number")
            Language.setMessage(mstrModuleName, 18, "7")
            Language.setMessage(mstrModuleName, 19, "Fax Number")
            Language.setMessage(mstrModuleName, 20, "8")
            Language.setMessage(mstrModuleName, 21, "E-mail")
            Language.setMessage(mstrModuleName, 22, "9")
            Language.setMessage(mstrModuleName, 23, "Contact Person")
            Language.setMessage(mstrModuleName, 24, "10")
            Language.setMessage(mstrModuleName, 25, "Number Of Employee")
            Language.setMessage(mstrModuleName, 26, "Male")
            Language.setMessage(mstrModuleName, 27, "Female")
            Language.setMessage(mstrModuleName, 28, "Total")
            Language.setMessage(mstrModuleName, 29, "Section 2: Payroll Information - Please provide details of gross emluments for the year")
            Language.setMessage(mstrModuleName, 30, "Month Of The")
            Language.setMessage(mstrModuleName, 31, "Financial Year")
            Language.setMessage(mstrModuleName, 32, "Gross Emoluments")
            Language.setMessage(mstrModuleName, 33, "Total")
            Language.setMessage(mstrModuleName, 34, "Section 3: Calculation Of TEVET Levy")
            Language.setMessage(mstrModuleName, 35, "Step 1: Show The Basis Of Calculating TEVET Levy")
            Language.setMessage(mstrModuleName, 36, "Transfer The Gross Emoluments Tabulated In Section 2 Above")
            Language.setMessage(mstrModuleName, 37, "Step 2: Calculate The TEVET Levy Amount")
            Language.setMessage(mstrModuleName, 38, "Take")
            Language.setMessage(mstrModuleName, 39, "% Of Total In Step 1 Above = Levy For Current Year")
            Language.setMessage(mstrModuleName, 40, "Step 3: Please Select You Preferred Mode Of Amount")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
