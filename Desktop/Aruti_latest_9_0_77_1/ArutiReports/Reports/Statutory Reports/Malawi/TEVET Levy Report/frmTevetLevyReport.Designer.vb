﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTevetLevyReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTevetLevyReport))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblPaymentMode = New System.Windows.Forms.Label
        Me.cboPaymentMode = New System.Windows.Forms.ComboBox
        Me.txtPercentage = New eZee.TextBox.NumericTextBox
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.lblFYear = New System.Windows.Forms.Label
        Me.cboFYear = New System.Windows.Forms.ComboBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.objchkPeriodSelectAll = New System.Windows.Forms.CheckBox
        Me.dgPeriod = New System.Windows.Forms.DataGridView
        Me.objdgperiodcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhPeriodunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhPeriodCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPeriodStart = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPeriodEnd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchPeriod = New System.Windows.Forms.TextBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgPeriod, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 379)
        Me.NavPanel.Size = New System.Drawing.Size(679, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.lblPaymentMode)
        Me.gbFilterCriteria.Controls.Add(Me.cboPaymentMode)
        Me.gbFilterCriteria.Controls.Add(Me.txtPercentage)
        Me.gbFilterCriteria.Controls.Add(Me.lblPercentage)
        Me.gbFilterCriteria.Controls.Add(Me.lblFYear)
        Me.gbFilterCriteria.Controls.Add(Me.cboFYear)
        Me.gbFilterCriteria.Controls.Add(Me.Panel2)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(11, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(656, 303)
        Me.gbFilterCriteria.TabIndex = 2
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(548, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 269
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkSetAnalysis.Visible = False
        '
        'lblPaymentMode
        '
        Me.lblPaymentMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentMode.Location = New System.Drawing.Point(9, 121)
        Me.lblPaymentMode.Name = "lblPaymentMode"
        Me.lblPaymentMode.Size = New System.Drawing.Size(114, 15)
        Me.lblPaymentMode.TabIndex = 268
        Me.lblPaymentMode.Text = "Payment Mode"
        Me.lblPaymentMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPaymentMode
        '
        Me.cboPaymentMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaymentMode.FormattingEnabled = True
        Me.cboPaymentMode.Location = New System.Drawing.Point(129, 115)
        Me.cboPaymentMode.Name = "cboPaymentMode"
        Me.cboPaymentMode.Size = New System.Drawing.Size(188, 21)
        Me.cboPaymentMode.TabIndex = 267
        '
        'txtPercentage
        '
        Me.txtPercentage.AllowNegative = True
        Me.txtPercentage.Decimal = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtPercentage.DigitsInGroup = 0
        Me.txtPercentage.Flags = 0
        Me.txtPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercentage.Location = New System.Drawing.Point(128, 88)
        Me.txtPercentage.MaxDecimalPlaces = 2
        Me.txtPercentage.MaxWholeDigits = 6
        Me.txtPercentage.Name = "txtPercentage"
        Me.txtPercentage.Prefix = ""
        Me.txtPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtPercentage.Size = New System.Drawing.Size(188, 21)
        Me.txtPercentage.TabIndex = 5
        Me.txtPercentage.Text = "1"
        Me.txtPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPercentage
        '
        Me.lblPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentage.Location = New System.Drawing.Point(8, 94)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(114, 15)
        Me.lblPercentage.TabIndex = 265
        Me.lblPercentage.Text = "Percentage "
        Me.lblPercentage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFYear
        '
        Me.lblFYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFYear.Location = New System.Drawing.Point(8, 40)
        Me.lblFYear.Name = "lblFYear"
        Me.lblFYear.Size = New System.Drawing.Size(97, 15)
        Me.lblFYear.TabIndex = 263
        Me.lblFYear.Text = "Financial Year"
        Me.lblFYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFYear
        '
        Me.cboFYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFYear.FormattingEnabled = True
        Me.cboFYear.Location = New System.Drawing.Point(128, 34)
        Me.cboFYear.Name = "cboFYear"
        Me.cboFYear.Size = New System.Drawing.Size(189, 21)
        Me.cboFYear.TabIndex = 12
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.objchkPeriodSelectAll)
        Me.Panel2.Controls.Add(Me.dgPeriod)
        Me.Panel2.Controls.Add(Me.txtSearchPeriod)
        Me.Panel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(440, 34)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(202, 258)
        Me.Panel2.TabIndex = 13
        '
        'objchkPeriodSelectAll
        '
        Me.objchkPeriodSelectAll.AutoSize = True
        Me.objchkPeriodSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkPeriodSelectAll.Name = "objchkPeriodSelectAll"
        Me.objchkPeriodSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkPeriodSelectAll.TabIndex = 18
        Me.objchkPeriodSelectAll.UseVisualStyleBackColor = True
        '
        'dgPeriod
        '
        Me.dgPeriod.AllowUserToAddRows = False
        Me.dgPeriod.AllowUserToDeleteRows = False
        Me.dgPeriod.AllowUserToResizeRows = False
        Me.dgPeriod.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgPeriod.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgPeriod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgPeriod.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgPeriod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgPeriod.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgperiodcolhCheck, Me.objdgcolhPeriodunkid, Me.dgColhPeriodCode, Me.dgColhPeriod, Me.dgcolhPeriodStart, Me.dgcolhPeriodEnd})
        Me.dgPeriod.Location = New System.Drawing.Point(0, 28)
        Me.dgPeriod.Name = "dgPeriod"
        Me.dgPeriod.RowHeadersVisible = False
        Me.dgPeriod.RowHeadersWidth = 5
        Me.dgPeriod.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgPeriod.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPeriod.Size = New System.Drawing.Size(199, 227)
        Me.dgPeriod.TabIndex = 286
        '
        'objdgperiodcolhCheck
        '
        Me.objdgperiodcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgperiodcolhCheck.HeaderText = ""
        Me.objdgperiodcolhCheck.Name = "objdgperiodcolhCheck"
        Me.objdgperiodcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgperiodcolhCheck.Width = 25
        '
        'objdgcolhPeriodunkid
        '
        Me.objdgcolhPeriodunkid.HeaderText = "periodunkid"
        Me.objdgcolhPeriodunkid.Name = "objdgcolhPeriodunkid"
        Me.objdgcolhPeriodunkid.ReadOnly = True
        Me.objdgcolhPeriodunkid.Visible = False
        '
        'dgColhPeriodCode
        '
        Me.dgColhPeriodCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhPeriodCode.HeaderText = "Code"
        Me.dgColhPeriodCode.Name = "dgColhPeriodCode"
        Me.dgColhPeriodCode.ReadOnly = True
        Me.dgColhPeriodCode.Width = 70
        '
        'dgColhPeriod
        '
        Me.dgColhPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhPeriod.HeaderText = "Period Name"
        Me.dgColhPeriod.Name = "dgColhPeriod"
        Me.dgColhPeriod.ReadOnly = True
        '
        'dgcolhPeriodStart
        '
        Me.dgcolhPeriodStart.HeaderText = "Period Start"
        Me.dgcolhPeriodStart.Name = "dgcolhPeriodStart"
        Me.dgcolhPeriodStart.ReadOnly = True
        Me.dgcolhPeriodStart.Visible = False
        '
        'dgcolhPeriodEnd
        '
        Me.dgcolhPeriodEnd.HeaderText = "Period End"
        Me.dgcolhPeriodEnd.Name = "dgcolhPeriodEnd"
        Me.dgcolhPeriodEnd.ReadOnly = True
        Me.dgcolhPeriodEnd.Visible = False
        '
        'txtSearchPeriod
        '
        Me.txtSearchPeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchPeriod.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchPeriod.Location = New System.Drawing.Point(0, 3)
        Me.txtSearchPeriod.Name = "txtSearchPeriod"
        Me.txtSearchPeriod.Size = New System.Drawing.Size(199, 21)
        Me.txtSearchPeriod.TabIndex = 12
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(337, 34)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(97, 15)
        Me.lblPeriod.TabIndex = 244
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(129, 142)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 14
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 67)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(114, 15)
        Me.lblEmployee.TabIndex = 88
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(128, 61)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(188, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'frmTevetLevyReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(679, 434)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmTevetLevyReport"
        Me.Text = "Tevet Levy Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgPeriod, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblFYear As System.Windows.Forms.Label
    Friend WithEvents cboFYear As System.Windows.Forms.ComboBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents objchkPeriodSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgPeriod As System.Windows.Forms.DataGridView
    Friend WithEvents objdgperiodcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhPeriodunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhPeriodCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPeriodStart As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPeriodEnd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Public WithEvents txtPercentage As eZee.TextBox.NumericTextBox
    Private WithEvents txtSearchPeriod As System.Windows.Forms.TextBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblPaymentMode As System.Windows.Forms.Label
    Friend WithEvents cboPaymentMode As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
End Class
