'************************************************************************************************************************************
'Class Name : clsIncomeTaxDivisionP12Report.vb
'Purpose    :
'Date       :05/06/2015
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsIncomeTaxDivisionP12Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsIncomeTaxDivisionP12Report"
    Private mstrReportId As String = enArutiReport.INCOME_TAX_DIVISION_P12_REPORT

    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    'For Report
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintHousingTranId As Integer = 0
    Private mstrDeductionHeadIds As String = ""
    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private mstrYearName As String = String.Empty
    Private mintPeriodCount As Integer = -1
    Private mintPayeHeadId As Integer = 0
    Private mstrPayeHeadName As String = ""
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = ""
    Private mintOtherEarningTranId As Integer = 0
    Private mblnShowPayrollNo As Boolean = False
    Private mblnShowEmployeeTINNo As Boolean = False

    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    Private mblnIncludeInactiveEmp As Boolean = True

    'For Analysis By
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _DeductionHeadIds() As String
        Set(ByVal value As String)
            mstrDeductionHeadIds = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadId() As Integer
        Set(ByVal value As Integer)
            mintPayeHeadId = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadName() As String
        Set(ByVal value As String)
            mstrPayeHeadName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _YearName() As String
        Set(ByVal value As String)
            mstrYearName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ShowPayrollNo() As Boolean
        Set(ByVal value As Boolean)
            mblnShowPayrollNo = value
        End Set
    End Property

    Public WriteOnly Property _ShowEmployeeTINNo() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmployeeTINNo = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintEmpId = 0
            mstrEmpName = ""
            mintHousingTranId = 0
            mstrDeductionHeadIds = ""
            mstrPeriodIds = ""
            mintPayeHeadId = 0
            mstrPayeHeadName = ""
            mintMembershipId = 0
            mstrMembershipName = ""
            mintOtherEarningTranId = 0
            mblnShowPayrollNo = False
            mblnShowEmployeeTINNo = False

            mblnIncludeInactiveEmp = True


            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@payetranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayeHeadId)

            If mintMembershipId > 0 Then
                Me._FilterTitle = "Membership : " & mstrMembershipName

                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            End If

            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try

        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBaseCurrencySign As String

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                dtPeriodStart = mdtPeriodStartDate : dtPeriodEnd = mdtPeriodEndDate
            End If

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = "" : xDateJoinQry = "" : xDateFilterQry = ""
            'Sohail (10 Feb 2021) -- Start
            'Blantyre Water Board - Enhancement : OLD-296 : Total Number Employed Field should pick count for processed employees only.  Not ALL active employees.
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , True, strDatabaseName)
            'Sohail (10 Feb 2021) -- End
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details:.
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = intCompanyUnkid

            StrQ = "SELECT hremployee_master.employeeunkid " & _
                    ", hremployee_master.employeecode " & _
                    ", hremployee_master.gender "

            If objConfig._FirstNamethenSurname = True Then
                StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS empname "
            Else
                StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS empname "
            End If

            StrQ &= "INTO #TableEmp " & _
                     "FROM hremployee_master "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE 1 = 1 "

            If mintEmpId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'Sohail (11 Dec 2020) -- End

            StrQ &= "SELECT  basicpay AS basicpay  " & _
                          ", allowancebenefit AS allowancebenefit " & _
                          ", noncashbenefit AS noncashbenefit " & _
                          ", basicpay + noncashbenefit + ( allowancebenefit ) AS Grosspay " & _
                          ", deduction AS deduction " & _
                          ", ( basicpay + noncashbenefit + ( allowancebenefit ) ) - deduction AS taxapayable " & _
                          ", taxpaid " & _
                          ", empid AS empid " & _
                          ", #TableEmp.employeecode AS empcode " & _
                          ", #TableEmp.gender "

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'If ConfigParameter._Object._FirstNamethenSurname = True Then
            '    StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS empname "
            'Else
            '    StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS empname "
            'End If
            StrQ &= ", #TableEmp.empname "
            'Sohail (11 Dec 2020) -- End

            StrQ &= "FROM    ( SELECT    SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay  " & _
                                      ", SUM(ISNULL(PAYE.allowance, 0)) AS allowancebenefit " & _
                                      ", SUM(ISNULL(PAYE.noncashbenefit, 0)) AS noncashbenefit " & _
                                      ", SUM(ISNULL(PAYE.deduction, 0)) AS deduction " & _
                                      ", SUM(ISNULL(PAYE.taxpaid, 0)) AS taxpaid " & _
                                      ", paye.empid AS empid " & _
                              "FROM      ( SELECT    CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS deduction " & _
                                                  ", 0 AS taxpaid " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            'Sohail (11 Dec 2020) - [JOIN hremployee_master] = [JOIN #TableEmp]

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END
            'Sohail (11 Dec 2020) -- End

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
            Else
                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
            End If

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'If mintEmpId > 0 Then
            '    StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            'End If
            'Sohail (11 Dec 2020) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END


            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS allowance " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS deduction " & _
                                                  ", 0 AS taxpaid " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            'Sohail (11 Dec 2020) - [JOIN hremployee_master] = [JOIN #TableEmp]

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END
            'Sohail (11 Dec 2020) -- End

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                                    "AND prtranhead_master.istaxable = 1 " & _
                                                    "AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND NOT ( prtranhead_master.isnoncashbenefit = 1 ) "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND NOT ( prtranhead_master.tranheadunkid = @OtherEarningTranId AND istaxable = 1 ) "
            Else
                StrQ &= "AND NOT ( prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) "
            End If

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'If mintEmpId > 0 Then
            '    StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            'End If
            'Sohail (11 Dec 2020) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END


            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS allowance " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS noncashbenefit " & _
                                                  ", 0 AS deduction " & _
                                                  ", 0 AS taxpaid " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            'Sohail (11 Dec 2020) - [JOIN hremployee_master] = [JOIN #TableEmp]

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END
            'Sohail (11 Dec 2020) -- End

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.isnoncashbenefit = 1 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS deduction " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            'Sohail (11 Dec 2020) - [JOIN hremployee_master] = [JOIN #TableEmp]

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END
            'Sohail (11 Dec 2020) -- End

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid IN (" & mstrDeductionHeadIds & ") " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'If mintEmpId > 0 Then
            '    StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            'End If
            ''Sohail (11 Dec 2020) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END


            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS deduction " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxpaid " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            'Sohail (11 Dec 2020) - [JOIN hremployee_master] = [JOIN #TableEmp]

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END
            'Sohail (11 Dec 2020) -- End

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @payetranheadunkid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'If mintEmpId > 0 Then
            '    StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            'End If
            'Sohail (11 Dec 2020) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END


            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                    ) AS PAYE " & _
                              "WHERE     1 = 1 " & _
                              "GROUP BY  empid " & _
                              "HAVING    SUM(ISNULL(PAYE.taxpaid, 0)) <> 0 " & _
                            ") AS A " & _
                            "LEFT JOIN #TableEmp ON #TableEmp.employeeunkid = A.empid "
            'Sohail (11 Dec 2020) - [JOIN hremployee_master] = [JOIN #TableEmp]

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'If ConfigParameter._Object._FirstNamethenSurname = True Then
            '    StrQ &= " ORDER BY  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "
            'Else
            '    StrQ &= " ORDER BY  ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  "
            'End If

            'If mintEmpId > 0 Then
            '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            'End If
            StrQ &= " ORDER BY #TableEmp.empname "

            StrQ &= " DROP TABLE #TableEmp "
            'Sohail (11 Dec 2020) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "SELECT gender, COUNT(hremployee_master.employeeunkid) AS EmpCount " & _
                    "FROM hremployee_master "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= "WHERE hremployee_master.isapproved = 1 " & _
            '            " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'S.SANDEEP [04 JUN 2015] -- END

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            StrQ &= " WHERE 1 = 1 "

            If mintEmpId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END


            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "GROUP BY gender "

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))

            Dim dsActiveEmp As DataSet = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'StrQ = "SELECT 1 AS SrNo, 0 AS FromAmount, 499 AS ToAmount, '0  -  499' AS Slab " & _
            '        "UNION ALL " & _
            '        "SELECT 2 AS SrNo, 500 AS FromAmount, 2999 AS ToAmount, '500  -  2999' AS Slab " & _
            '        "UNION ALL " & _
            '        "SELECT 3 AS SrNo, 3000 AS FromAmount, 5999 AS ToAmount, '3000  -  5999' AS Slab " & _
            '        "UNION ALL " & _
            '        "SELECT 4 AS SrNo, 6000 AS FromAmount, 14999 AS ToAmount, '6000  -  14999' AS Slab " & _
            '        "UNION ALL " & _
            '        "SELECT 5 AS SrNo, 15000 AS FromAmount, 999999999999999 AS ToAmount, 'Over 15000' AS Slab "
            StrQ = "SELECT 1 AS SrNo, 0 AS FromAmount, 13000 AS ToAmount, '0  -  13,000' AS Slab " & _
                    "UNION ALL " & _
                    "SELECT 2 AS SrNo, 13001 AS FromAmount, 50000 AS ToAmount, '13,001  -  50,000' AS Slab " & _
                    "UNION ALL " & _
                    "SELECT 3 AS SrNo, 50001 AS FromAmount, 100000 AS ToAmount, '50,001  -  100,000' AS Slab " & _
                    "UNION ALL " & _
                    "SELECT 4 AS SrNo, 100001 AS FromAmount, 999999999999999 AS ToAmount, 'Over 100,000' AS Slab "
            'Sohail (11 Dec 2020) -- End

            Dim dsSlab As DataSet = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim decTotalTaxPaid As Decimal = 0
            Dim intTotalEmpMaleCount As Integer = 0
            Dim intTotalEmpFemaleCount As Integer = 0
            Dim intTotalPAYEMaleCount As Integer = 0
            Dim intTotalPAYEFemaleCount As Integer = 0
            Dim decTotalGrossAmountMale As Decimal = 0
            Dim decTotalGrossAmountFemale As Decimal = 0
            Dim decTotalTaxableAmountMale As Decimal = 0
            Dim decTotalTaxableAmountFemale As Decimal = 0

            If dsList.Tables(0).Rows.Count > 0 Then
                decTotalTaxPaid = (From p In dsList.Tables(0) Select (CDec(p.Item("taxpaid")))).Sum()
                intTotalPAYEMaleCount = (From p In dsList.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 1) Select (p)).Count()
                intTotalPAYEFemaleCount = (From p In dsList.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 2) Select (p)).Count()

                decTotalGrossAmountMale = (From p In dsList.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 1) Select (CDec(p.Item("Grosspay")))).Sum()
                decTotalGrossAmountFemale = (From p In dsList.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 2) Select (CDec(p.Item("Grosspay")))).Sum()

                decTotalGrossAmountMale = (From p In dsList.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 1) Select (CDec(p.Item("Grosspay")))).Sum()
                decTotalGrossAmountFemale = (From p In dsList.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 2) Select (CDec(p.Item("Grosspay")))).Sum()

                decTotalTaxableAmountMale = (From p In dsList.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 1) Select (CDec(p.Item("taxapayable")))).Sum()
                decTotalTaxableAmountFemale = (From p In dsList.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 2) Select (CDec(p.Item("taxapayable")))).Sum()
            End If

            If dsActiveEmp.Tables(0).Rows.Count > 0 Then
                intTotalEmpMaleCount = (From p In dsActiveEmp.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 1) Select (CInt(p.Item("EmpCount")))).Sum()
                intTotalEmpFemaleCount = (From p In dsActiveEmp.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 2) Select (CInt(p.Item("EmpCount")))).Sum()
            End If

            Dim rpt_Row As DataRow = Nothing

            Dim decFrom As Decimal = 0
            Dim decTo As Decimal = 0
            For Each dsRow As DataRow In dsSlab.Tables(0).Rows
                decFrom = CDec(dsRow.Item("FromAmount"))
                decTo = CDec(dsRow.Item("ToAmount"))

                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column2") = dsRow.Item("Slab").ToString
                rpt_Row.Item("Column3") = (From p In dsList.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 1 AndAlso CDec(p.Item("taxapayable")) >= decFrom AndAlso CDec(p.Item("taxapayable")) <= decTo) Select (p)).Count()
                rpt_Row.Item("Column4") = (From p In dsList.Tables(0) Where (p.Item("gender").ToString.Trim <> "" AndAlso CInt(p.Item("gender")) = 2 AndAlso CDec(p.Item("taxapayable")) >= decFrom AndAlso CDec(p.Item("taxapayable")) <= decTo) Select (p)).Count()

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptIncomeTaxDivision_P12

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "txtMalawiRevenue", Language.getMessage(mstrModuleName, 1, "MALAWI REVENUE AUTHORITY"))
            ReportFunction.TextChange(objRpt, "txtIncomeTaxDiv", Language.getMessage(mstrModuleName, 2, "INCOME TAX DIVISON"))
            ReportFunction.TextChange(objRpt, "txtP12", Language.getMessage(mstrModuleName, 3, "P. 12"))
            ReportFunction.TextChange(objRpt, "txtA", Language.getMessage(mstrModuleName, 4, "A."))
            ReportFunction.TextChange(objRpt, "txtTaxRemitted", Language.getMessage(mstrModuleName, 5, "AMOUNT OF TAX REMITTED"))
            ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 6, "P.A.Y.E.  DEDUCTION FOR THE MONTH OF"))
            ReportFunction.TextChange(objRpt, "txtMonth", mstrPeriodNames)
            ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 7, "YEAR"))
            ReportFunction.TextChange(objRpt, "txtYear", mstrYearName)
            ReportFunction.TextChange(objRpt, "lblRefNumber", Language.getMessage(mstrModuleName, 8, "P.A.Y.E. Reference Number"))
            ReportFunction.TextChange(objRpt, "lblTPIN", Language.getMessage(mstrModuleName, 9, "TPIN"))
            ReportFunction.TextChange(objRpt, "txtPINNum", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt, "lblNameAddress", Language.getMessage(mstrModuleName, 10, "Employer's Name and Address"))
            ReportFunction.TextChange(objRpt, "txtName", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "txtAddress1", Company._Object._Address1 & " " & Company._Object._Address2 & " " & Company._Object._City_Name)
            ReportFunction.TextChange(objRpt, "txtAddress2", Company._Object._State_Name & " " & Company._Object._Post_Code_No & " " & Company._Object._Country_Name)
            ReportFunction.TextChange(objRpt, "txtTaxPaidAmount", Format(decTotalTaxPaid, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtB", Language.getMessage(mstrModuleName, 11, "B."))
            ReportFunction.TextChange(objRpt, "txtEconomicActivity", Language.getMessage(mstrModuleName, 12, "ECONOMIC ACTIVITY"))
            ReportFunction.TextChange(objRpt, "lblActivity", Language.getMessage(mstrModuleName, 13, "Nature of Activity:"))
            ReportFunction.TextChange(objRpt, "txtActivity", "")
            ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 14, "(Describe in full your major activity)"))
            'Sohail (11 Dec 2020) -- Start
            'Blantyre Water Board Enhancement # OLD-203 : - Amend section E slab of the P12 report with the given details: .
            'ReportFunction.TextChange(objRpt, "txtOfficeUse", Language.getMessage(mstrModuleName, 15, "Nature of Activity:"))
            'ReportFunction.TextChange(objRpt, "lblActivity", Language.getMessage(mstrModuleName, 16, "For office use"))
            ReportFunction.TextChange(objRpt, "txtOfficeUse", Language.getMessage(mstrModuleName, 16, "For office use"))
            'Sohail (11 Dec 2020) -- End
            ReportFunction.TextChange(objRpt, "txtC", Language.getMessage(mstrModuleName, 17, "C."))
            ReportFunction.TextChange(objRpt, "txtOutputValue", Language.getMessage(mstrModuleName, 18, "VALUE OF TOTAL OUTPUT"))
            ReportFunction.TextChange(objRpt, "lblGrossSales", Language.getMessage(mstrModuleName, 19, "Value of Gross Sales:"))
            ReportFunction.TextChange(objRpt, "txtK", Language.getMessage(mstrModuleName, 20, "K"))
            ReportFunction.TextChange(objRpt, "txtGrossSales", "")
            ReportFunction.TextChange(objRpt, "txtD", Language.getMessage(mstrModuleName, 21, "D."))
            ReportFunction.TextChange(objRpt, "txtDTitle", Language.getMessage(mstrModuleName, 22, "TOTAL PERSONS ENGAGED AND GROSS EMOLUMENTS ON ALL EMPLOYEES"))
            ReportFunction.TextChange(objRpt, "txtDMales", Language.getMessage(mstrModuleName, 23, "Males"))
            ReportFunction.TextChange(objRpt, "txtDFemales", Language.getMessage(mstrModuleName, 24, "Females"))
            ReportFunction.TextChange(objRpt, "lblDNumberMale", Language.getMessage(mstrModuleName, 25, "Number"))
            ReportFunction.TextChange(objRpt, "lblDNumberFemale", Language.getMessage(mstrModuleName, 26, "Number"))
            ReportFunction.TextChange(objRpt, "lblDa", Language.getMessage(mstrModuleName, 27, "(a)"))
            ReportFunction.TextChange(objRpt, "lbltoalNumEmp", Language.getMessage(mstrModuleName, 28, "Total Number Employed"))
            ReportFunction.TextChange(objRpt, "txtMActiveEmpCount", intTotalEmpMaleCount)
            ReportFunction.TextChange(objRpt, "txtFActiveEmpCount", intTotalEmpFemaleCount)
            ReportFunction.TextChange(objRpt, "lblDb", Language.getMessage(mstrModuleName, 29, "(b)"))
            ReportFunction.TextChange(objRpt, "lblNumOnPAYE", Language.getMessage(mstrModuleName, 30, "Number on P.A.Y.E."))
            ReportFunction.TextChange(objRpt, "txtMEmpPAYE", intTotalPAYEMaleCount)
            ReportFunction.TextChange(objRpt, "txtFEmpPAYE", intTotalPAYEFemaleCount)
            ReportFunction.TextChange(objRpt, "lblDKwachaMale", Language.getMessage(mstrModuleName, 31, "Kwacha"))
            ReportFunction.TextChange(objRpt, "lblDKwachaFemale", Language.getMessage(mstrModuleName, 32, "Kwacha"))
            ReportFunction.TextChange(objRpt, "lblDc", Language.getMessage(mstrModuleName, 33, "(c)"))
            ReportFunction.TextChange(objRpt, "lblGrossEmoluments", Language.getMessage(mstrModuleName, 34, "Gross Emoluments (for ""a"" above)"))
            ReportFunction.TextChange(objRpt, "txtMGrossPay", Format(decTotalGrossAmountMale, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtFGrossPay", Format(decTotalGrossAmountFemale, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "lblDd", Language.getMessage(mstrModuleName, 35, "(d)"))
            ReportFunction.TextChange(objRpt, "lblGEmolumentsPAYE", Language.getMessage(mstrModuleName, 36, "Gross Emoluments on P.A.Y.E. (for ""b"" above)"))
            ReportFunction.TextChange(objRpt, "txtMTaxablePay", Format(decTotalTaxableAmountMale, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtFTaxablePay", Format(decTotalTaxableAmountFemale, GUI.fmtCurrency)) '
            ReportFunction.TextChange(objRpt, "txtE", Language.getMessage(mstrModuleName, 37, "E."))
            ReportFunction.TextChange(objRpt, "txtETitle", Language.getMessage(mstrModuleName, 38, "MONTHLY TOTAL NUMBER OF EMPLOYEE BY INCOME CATEGORY"))
            ReportFunction.TextChange(objRpt, "txtEMales", Language.getMessage(mstrModuleName, 39, "Males"))
            ReportFunction.TextChange(objRpt, "txtEFemales", Language.getMessage(mstrModuleName, 40, "Females"))
            ReportFunction.TextChange(objRpt, "txtEkwacha", Language.getMessage(mstrModuleName, 41, "Kwacha"))
            ReportFunction.TextChange(objRpt, "lblENumberMale", Language.getMessage(mstrModuleName, 42, "Number"))
            ReportFunction.TextChange(objRpt, "lblENumberFemale", Language.getMessage(mstrModuleName, 43, "Number"))
            ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 44, "Date:"))
            ReportFunction.TextChange(objRpt, "txtDate", "")
            ReportFunction.TextChange(objRpt, "lblSignature", Language.getMessage(mstrModuleName, 45, "Signature of Employer's Representative"))
            ReportFunction.TextChange(objRpt, "txtSignature", "")

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "MALAWI REVENUE AUTHORITY")
            Language.setMessage(mstrModuleName, 2, "INCOME TAX DIVISON")
            Language.setMessage(mstrModuleName, 3, "P. 12")
            Language.setMessage(mstrModuleName, 4, "A.")
            Language.setMessage(mstrModuleName, 5, "AMOUNT OF TAX REMITTED")
            Language.setMessage(mstrModuleName, 6, "P.A.Y.E.  DEDUCTION FOR THE MONTH OF")
            Language.setMessage(mstrModuleName, 7, "YEAR")
            Language.setMessage(mstrModuleName, 8, "P.A.Y.E. Reference Number")
            Language.setMessage(mstrModuleName, 9, "TPIN")
            Language.setMessage(mstrModuleName, 10, "Employer's Name and Address")
            Language.setMessage(mstrModuleName, 11, "B.")
            Language.setMessage(mstrModuleName, 12, "ECONOMIC ACTIVITY")
            Language.setMessage(mstrModuleName, 13, "Nature of Activity:")
            Language.setMessage(mstrModuleName, 14, "(Describe in full your major activity)")
            Language.setMessage(mstrModuleName, 15, "Nature of Activity:")
            Language.setMessage(mstrModuleName, 16, "For office use")
            Language.setMessage(mstrModuleName, 17, "C.")
            Language.setMessage(mstrModuleName, 18, "VALUE OF TOTAL OUTPUT")
            Language.setMessage(mstrModuleName, 19, "Value of Gross Sales:")
            Language.setMessage(mstrModuleName, 20, "K")
            Language.setMessage(mstrModuleName, 21, "D.")
            Language.setMessage(mstrModuleName, 22, "TOTAL PERSONS ENGAGED AND GROSS EMOLUMENTS ON ALL EMPLOYEES")
            Language.setMessage(mstrModuleName, 23, "Males")
            Language.setMessage(mstrModuleName, 24, "Females")
            Language.setMessage(mstrModuleName, 25, "Number")
            Language.setMessage(mstrModuleName, 26, "Number")
            Language.setMessage(mstrModuleName, 27, "(a)")
            Language.setMessage(mstrModuleName, 28, "Total Number Employed")
            Language.setMessage(mstrModuleName, 29, "(b)")
            Language.setMessage(mstrModuleName, 30, "Number on P.A.Y.E.")
            Language.setMessage(mstrModuleName, 31, "Kwacha")
            Language.setMessage(mstrModuleName, 32, "Kwacha")
            Language.setMessage(mstrModuleName, 33, "(c)")
            Language.setMessage(mstrModuleName, 34, "Gross Emoluments (for ""a"" above)")
            Language.setMessage(mstrModuleName, 35, "(d)")
            Language.setMessage(mstrModuleName, 36, "Gross Emoluments on P.A.Y.E. (for ""b"" above)")
            Language.setMessage(mstrModuleName, 37, "E.")
            Language.setMessage(mstrModuleName, 38, "MONTHLY TOTAL NUMBER OF EMPLOYEE BY INCOME CATEGORY")
            Language.setMessage(mstrModuleName, 39, "Males")
            Language.setMessage(mstrModuleName, 40, "Females")
            Language.setMessage(mstrModuleName, 41, "Kwacha")
            Language.setMessage(mstrModuleName, 42, "Number")
            Language.setMessage(mstrModuleName, 43, "Number")
            Language.setMessage(mstrModuleName, 44, "Date:")
            Language.setMessage(mstrModuleName, 45, "Signature of Employer's Representative")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
