﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmITaxFormBReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmITaxFormBReport))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIgnoreZero = New System.Windows.Forms.CheckBox
        Me.lblColumnAI = New System.Windows.Forms.Label
        Me.cboColumnAI = New System.Windows.Forms.ComboBox
        Me.lblColumnAG = New System.Windows.Forms.Label
        Me.cboColumnAG = New System.Windows.Forms.ComboBox
        Me.lblColumnN = New System.Windows.Forms.Label
        Me.cboColumnN = New System.Windows.Forms.ComboBox
        Me.lblColumnAA = New System.Windows.Forms.Label
        Me.cboColumnAA = New System.Windows.Forms.ComboBox
        Me.lblDisabilityMembership = New System.Windows.Forms.Label
        Me.cboDisabilityMembership = New System.Windows.Forms.ComboBox
        Me.lblColumnS = New System.Windows.Forms.Label
        Me.cboColumnS = New System.Windows.Forms.ComboBox
        Me.lblColumnAB = New System.Windows.Forms.Label
        Me.cboColumnAB = New System.Windows.Forms.ComboBox
        Me.lblEmployerRentedHouse = New System.Windows.Forms.Label
        Me.cboEmployerRentedHouse = New System.Windows.Forms.ComboBox
        Me.lblEmployerOwnedHouse = New System.Windows.Forms.Label
        Me.cboEmployerOwnedHouse = New System.Windows.Forms.ComboBox
        Me.lblEmployerRentedHouseValue = New System.Windows.Forms.Label
        Me.cboEmployerRentedHouseValue = New System.Windows.Forms.ComboBox
        Me.lblEmployerOwnedHouseValue = New System.Windows.Forms.Label
        Me.cboEmployerOwnedHouseValue = New System.Windows.Forms.ComboBox
        Me.lblNonResident = New System.Windows.Forms.Label
        Me.cboNonResident = New System.Windows.Forms.ComboBox
        Me.lblResident = New System.Windows.Forms.Label
        Me.cboResident = New System.Windows.Forms.ComboBox
        Me.lblMonthlyRelief = New System.Windows.Forms.Label
        Me.cboMonthlyRelief = New System.Windows.Forms.ComboBox
        Me.lblActualContribution = New System.Windows.Forms.Label
        Me.cboActualContribution = New System.Windows.Forms.ComboBox
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblLumpSum = New System.Windows.Forms.Label
        Me.cboLumpSum = New System.Windows.Forms.ComboBox
        Me.lblDirectorFee = New System.Windows.Forms.Label
        Me.cboDirectorFee = New System.Windows.Forms.ComboBox
        Me.lblOverTimeAllow = New System.Windows.Forms.Label
        Me.cboOverTimeAllow = New System.Windows.Forms.ComboBox
        Me.lblLeavePay = New System.Windows.Forms.Label
        Me.cboLeavePay = New System.Windows.Forms.ComboBox
        Me.lblTransAllow = New System.Windows.Forms.Label
        Me.cboTransAllow = New System.Windows.Forms.ComboBox
        Me.gbBasicSalaryOtherEarning = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.objbtnSearchOtherEarning = New eZee.Common.eZeeGradientButton
        Me.cboOtherEarning = New System.Windows.Forms.ComboBox
        Me.lblOtherEarning = New System.Windows.Forms.Label
        Me.cboSlabEffectivePeriod = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objchkAllPeriod = New System.Windows.Forms.CheckBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lvPeriod = New eZee.Common.eZeeListView(Me.components)
        Me.objColhPCheck = New System.Windows.Forms.ColumnHeader
        Me.colhPeriodName = New System.Windows.Forms.ColumnHeader
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblHouseAllow = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboHouseAllow = New System.Windows.Forms.ComboBox
        Me.SaveDialog = New System.Windows.Forms.SaveFileDialog
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbBasicSalaryOtherEarning.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 528)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(724, 55)
        Me.EZeeFooter1.TabIndex = 3
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(431, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(527, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(623, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(724, 60)
        Me.eZeeHeader.TabIndex = 4
        Me.eZeeHeader.Title = "iTax Form B Report"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbFilterCriteria.AutoScroll = True
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkIgnoreZero)
        Me.gbFilterCriteria.Controls.Add(Me.lblColumnAI)
        Me.gbFilterCriteria.Controls.Add(Me.cboColumnAI)
        Me.gbFilterCriteria.Controls.Add(Me.lblColumnAG)
        Me.gbFilterCriteria.Controls.Add(Me.cboColumnAG)
        Me.gbFilterCriteria.Controls.Add(Me.lblColumnN)
        Me.gbFilterCriteria.Controls.Add(Me.cboColumnN)
        Me.gbFilterCriteria.Controls.Add(Me.lblColumnAA)
        Me.gbFilterCriteria.Controls.Add(Me.cboColumnAA)
        Me.gbFilterCriteria.Controls.Add(Me.lblDisabilityMembership)
        Me.gbFilterCriteria.Controls.Add(Me.cboDisabilityMembership)
        Me.gbFilterCriteria.Controls.Add(Me.lblColumnS)
        Me.gbFilterCriteria.Controls.Add(Me.cboColumnS)
        Me.gbFilterCriteria.Controls.Add(Me.lblColumnAB)
        Me.gbFilterCriteria.Controls.Add(Me.cboColumnAB)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployerRentedHouse)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployerRentedHouse)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployerOwnedHouse)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployerOwnedHouse)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployerRentedHouseValue)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployerRentedHouseValue)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployerOwnedHouseValue)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployerOwnedHouseValue)
        Me.gbFilterCriteria.Controls.Add(Me.lblNonResident)
        Me.gbFilterCriteria.Controls.Add(Me.cboNonResident)
        Me.gbFilterCriteria.Controls.Add(Me.lblResident)
        Me.gbFilterCriteria.Controls.Add(Me.cboResident)
        Me.gbFilterCriteria.Controls.Add(Me.lblMonthlyRelief)
        Me.gbFilterCriteria.Controls.Add(Me.cboMonthlyRelief)
        Me.gbFilterCriteria.Controls.Add(Me.lblActualContribution)
        Me.gbFilterCriteria.Controls.Add(Me.cboActualContribution)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.lblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.lblLumpSum)
        Me.gbFilterCriteria.Controls.Add(Me.cboLumpSum)
        Me.gbFilterCriteria.Controls.Add(Me.lblDirectorFee)
        Me.gbFilterCriteria.Controls.Add(Me.cboDirectorFee)
        Me.gbFilterCriteria.Controls.Add(Me.lblOverTimeAllow)
        Me.gbFilterCriteria.Controls.Add(Me.cboOverTimeAllow)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeavePay)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeavePay)
        Me.gbFilterCriteria.Controls.Add(Me.lblTransAllow)
        Me.gbFilterCriteria.Controls.Add(Me.cboTransAllow)
        Me.gbFilterCriteria.Controls.Add(Me.gbBasicSalaryOtherEarning)
        Me.gbFilterCriteria.Controls.Add(Me.cboSlabEffectivePeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.objchkAllPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lvPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblHouseAllow)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboHouseAllow)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(700, 456)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIgnoreZero
        '
        Me.chkIgnoreZero.Checked = True
        Me.chkIgnoreZero.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIgnoreZero.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnoreZero.Location = New System.Drawing.Point(127, 481)
        Me.chkIgnoreZero.Name = "chkIgnoreZero"
        Me.chkIgnoreZero.Size = New System.Drawing.Size(161, 17)
        Me.chkIgnoreZero.TabIndex = 140
        Me.chkIgnoreZero.Text = "Ignore Zero Value"
        Me.chkIgnoreZero.UseVisualStyleBackColor = True
        '
        'lblColumnAI
        '
        Me.lblColumnAI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColumnAI.Location = New System.Drawing.Point(345, 449)
        Me.lblColumnAI.Name = "lblColumnAI"
        Me.lblColumnAI.Size = New System.Drawing.Size(112, 15)
        Me.lblColumnAI.TabIndex = 138
        Me.lblColumnAI.Text = "PAYE"
        Me.lblColumnAI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboColumnAI
        '
        Me.cboColumnAI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboColumnAI.DropDownWidth = 180
        Me.cboColumnAI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboColumnAI.FormattingEnabled = True
        Me.cboColumnAI.Location = New System.Drawing.Point(465, 446)
        Me.cboColumnAI.Name = "cboColumnAI"
        Me.cboColumnAI.Size = New System.Drawing.Size(213, 21)
        Me.cboColumnAI.TabIndex = 137
        '
        'lblColumnAG
        '
        Me.lblColumnAG.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColumnAG.Location = New System.Drawing.Point(7, 451)
        Me.lblColumnAG.Name = "lblColumnAG"
        Me.lblColumnAG.Size = New System.Drawing.Size(112, 15)
        Me.lblColumnAG.TabIndex = 135
        Me.lblColumnAG.Text = "Insurance Relief"
        Me.lblColumnAG.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboColumnAG
        '
        Me.cboColumnAG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboColumnAG.DropDownWidth = 180
        Me.cboColumnAG.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboColumnAG.FormattingEnabled = True
        Me.cboColumnAG.Location = New System.Drawing.Point(127, 448)
        Me.cboColumnAG.Name = "cboColumnAG"
        Me.cboColumnAG.Size = New System.Drawing.Size(213, 21)
        Me.cboColumnAG.TabIndex = 21
        '
        'lblColumnN
        '
        Me.lblColumnN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColumnN.Location = New System.Drawing.Point(8, 397)
        Me.lblColumnN.Name = "lblColumnN"
        Me.lblColumnN.Size = New System.Drawing.Size(112, 15)
        Me.lblColumnN.TabIndex = 133
        Me.lblColumnN.Text = "Car Benefit"
        Me.lblColumnN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboColumnN
        '
        Me.cboColumnN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboColumnN.DropDownWidth = 180
        Me.cboColumnN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboColumnN.FormattingEnabled = True
        Me.cboColumnN.Location = New System.Drawing.Point(127, 394)
        Me.cboColumnN.Name = "cboColumnN"
        Me.cboColumnN.Size = New System.Drawing.Size(213, 21)
        Me.cboColumnN.TabIndex = 17
        '
        'lblColumnAA
        '
        Me.lblColumnAA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColumnAA.Location = New System.Drawing.Point(10, 424)
        Me.lblColumnAA.Name = "lblColumnAA"
        Me.lblColumnAA.Size = New System.Drawing.Size(112, 15)
        Me.lblColumnAA.TabIndex = 131
        Me.lblColumnAA.Text = "Mortgage Relief"
        Me.lblColumnAA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboColumnAA
        '
        Me.cboColumnAA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboColumnAA.DropDownWidth = 180
        Me.cboColumnAA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboColumnAA.FormattingEnabled = True
        Me.cboColumnAA.Location = New System.Drawing.Point(127, 421)
        Me.cboColumnAA.Name = "cboColumnAA"
        Me.cboColumnAA.Size = New System.Drawing.Size(213, 21)
        Me.cboColumnAA.TabIndex = 19
        '
        'lblDisabilityMembership
        '
        Me.lblDisabilityMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisabilityMembership.Location = New System.Drawing.Point(348, 279)
        Me.lblDisabilityMembership.Name = "lblDisabilityMembership"
        Me.lblDisabilityMembership.Size = New System.Drawing.Size(112, 15)
        Me.lblDisabilityMembership.TabIndex = 128
        Me.lblDisabilityMembership.Text = "Disability Membership"
        Me.lblDisabilityMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDisabilityMembership
        '
        Me.cboDisabilityMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisabilityMembership.DropDownWidth = 180
        Me.cboDisabilityMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisabilityMembership.FormattingEnabled = True
        Me.cboDisabilityMembership.Location = New System.Drawing.Point(465, 276)
        Me.cboDisabilityMembership.Name = "cboDisabilityMembership"
        Me.cboDisabilityMembership.Size = New System.Drawing.Size(213, 21)
        Me.cboDisabilityMembership.TabIndex = 10
        '
        'lblColumnS
        '
        Me.lblColumnS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColumnS.Location = New System.Drawing.Point(345, 395)
        Me.lblColumnS.Name = "lblColumnS"
        Me.lblColumnS.Size = New System.Drawing.Size(112, 15)
        Me.lblColumnS.TabIndex = 125
        Me.lblColumnS.Text = "House Rent Value"
        Me.lblColumnS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboColumnS
        '
        Me.cboColumnS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboColumnS.DropDownWidth = 180
        Me.cboColumnS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboColumnS.FormattingEnabled = True
        Me.cboColumnS.Location = New System.Drawing.Point(465, 392)
        Me.cboColumnS.Name = "cboColumnS"
        Me.cboColumnS.Size = New System.Drawing.Size(213, 21)
        Me.cboColumnS.TabIndex = 18
        '
        'lblColumnAB
        '
        Me.lblColumnAB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColumnAB.Location = New System.Drawing.Point(347, 422)
        Me.lblColumnAB.Name = "lblColumnAB"
        Me.lblColumnAB.Size = New System.Drawing.Size(112, 15)
        Me.lblColumnAB.TabIndex = 123
        Me.lblColumnAB.Text = "Home ownership saving"
        Me.lblColumnAB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboColumnAB
        '
        Me.cboColumnAB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboColumnAB.DropDownWidth = 180
        Me.cboColumnAB.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboColumnAB.FormattingEnabled = True
        Me.cboColumnAB.Location = New System.Drawing.Point(465, 419)
        Me.cboColumnAB.Name = "cboColumnAB"
        Me.cboColumnAB.Size = New System.Drawing.Size(213, 21)
        Me.cboColumnAB.TabIndex = 20
        '
        'lblEmployerRentedHouse
        '
        Me.lblEmployerRentedHouse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployerRentedHouse.Location = New System.Drawing.Point(347, 364)
        Me.lblEmployerRentedHouse.Name = "lblEmployerRentedHouse"
        Me.lblEmployerRentedHouse.Size = New System.Drawing.Size(112, 27)
        Me.lblEmployerRentedHouse.TabIndex = 120
        Me.lblEmployerRentedHouse.Text = "Employer's Rented House"
        Me.lblEmployerRentedHouse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployerRentedHouse
        '
        Me.cboEmployerRentedHouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployerRentedHouse.DropDownWidth = 180
        Me.cboEmployerRentedHouse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployerRentedHouse.FormattingEnabled = True
        Me.cboEmployerRentedHouse.Location = New System.Drawing.Point(465, 365)
        Me.cboEmployerRentedHouse.Name = "cboEmployerRentedHouse"
        Me.cboEmployerRentedHouse.Size = New System.Drawing.Size(213, 21)
        Me.cboEmployerRentedHouse.TabIndex = 16
        '
        'lblEmployerOwnedHouse
        '
        Me.lblEmployerOwnedHouse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployerOwnedHouse.Location = New System.Drawing.Point(8, 364)
        Me.lblEmployerOwnedHouse.Name = "lblEmployerOwnedHouse"
        Me.lblEmployerOwnedHouse.Size = New System.Drawing.Size(112, 27)
        Me.lblEmployerOwnedHouse.TabIndex = 118
        Me.lblEmployerOwnedHouse.Text = "Employer's Owned House"
        Me.lblEmployerOwnedHouse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployerOwnedHouse
        '
        Me.cboEmployerOwnedHouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployerOwnedHouse.DropDownWidth = 180
        Me.cboEmployerOwnedHouse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployerOwnedHouse.FormattingEnabled = True
        Me.cboEmployerOwnedHouse.Location = New System.Drawing.Point(127, 365)
        Me.cboEmployerOwnedHouse.Name = "cboEmployerOwnedHouse"
        Me.cboEmployerOwnedHouse.Size = New System.Drawing.Size(213, 21)
        Me.cboEmployerOwnedHouse.TabIndex = 15
        '
        'lblEmployerRentedHouseValue
        '
        Me.lblEmployerRentedHouseValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployerRentedHouseValue.Location = New System.Drawing.Point(347, 330)
        Me.lblEmployerRentedHouseValue.Name = "lblEmployerRentedHouseValue"
        Me.lblEmployerRentedHouseValue.Size = New System.Drawing.Size(112, 27)
        Me.lblEmployerRentedHouseValue.TabIndex = 116
        Me.lblEmployerRentedHouseValue.Text = "Employer's Rented House Value"
        Me.lblEmployerRentedHouseValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployerRentedHouseValue
        '
        Me.cboEmployerRentedHouseValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployerRentedHouseValue.DropDownWidth = 180
        Me.cboEmployerRentedHouseValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployerRentedHouseValue.FormattingEnabled = True
        Me.cboEmployerRentedHouseValue.Location = New System.Drawing.Point(465, 333)
        Me.cboEmployerRentedHouseValue.Name = "cboEmployerRentedHouseValue"
        Me.cboEmployerRentedHouseValue.Size = New System.Drawing.Size(213, 21)
        Me.cboEmployerRentedHouseValue.TabIndex = 14
        '
        'lblEmployerOwnedHouseValue
        '
        Me.lblEmployerOwnedHouseValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployerOwnedHouseValue.Location = New System.Drawing.Point(8, 330)
        Me.lblEmployerOwnedHouseValue.Name = "lblEmployerOwnedHouseValue"
        Me.lblEmployerOwnedHouseValue.Size = New System.Drawing.Size(112, 27)
        Me.lblEmployerOwnedHouseValue.TabIndex = 114
        Me.lblEmployerOwnedHouseValue.Text = "Employer's Owned House Value"
        Me.lblEmployerOwnedHouseValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployerOwnedHouseValue
        '
        Me.cboEmployerOwnedHouseValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployerOwnedHouseValue.DropDownWidth = 180
        Me.cboEmployerOwnedHouseValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployerOwnedHouseValue.FormattingEnabled = True
        Me.cboEmployerOwnedHouseValue.Location = New System.Drawing.Point(127, 333)
        Me.cboEmployerOwnedHouseValue.Name = "cboEmployerOwnedHouseValue"
        Me.cboEmployerOwnedHouseValue.Size = New System.Drawing.Size(213, 21)
        Me.cboEmployerOwnedHouseValue.TabIndex = 13
        '
        'lblNonResident
        '
        Me.lblNonResident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNonResident.Location = New System.Drawing.Point(348, 306)
        Me.lblNonResident.Name = "lblNonResident"
        Me.lblNonResident.Size = New System.Drawing.Size(112, 15)
        Me.lblNonResident.TabIndex = 111
        Me.lblNonResident.Text = "Non Resident"
        Me.lblNonResident.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNonResident
        '
        Me.cboNonResident.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNonResident.DropDownWidth = 180
        Me.cboNonResident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNonResident.FormattingEnabled = True
        Me.cboNonResident.Location = New System.Drawing.Point(465, 303)
        Me.cboNonResident.Name = "cboNonResident"
        Me.cboNonResident.Size = New System.Drawing.Size(213, 21)
        Me.cboNonResident.TabIndex = 12
        '
        'lblResident
        '
        Me.lblResident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResident.Location = New System.Drawing.Point(8, 306)
        Me.lblResident.Name = "lblResident"
        Me.lblResident.Size = New System.Drawing.Size(112, 15)
        Me.lblResident.TabIndex = 109
        Me.lblResident.Text = "Resident"
        Me.lblResident.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResident
        '
        Me.cboResident.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResident.DropDownWidth = 180
        Me.cboResident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResident.FormattingEnabled = True
        Me.cboResident.Location = New System.Drawing.Point(127, 303)
        Me.cboResident.Name = "cboResident"
        Me.cboResident.Size = New System.Drawing.Size(213, 21)
        Me.cboResident.TabIndex = 11
        '
        'lblMonthlyRelief
        '
        Me.lblMonthlyRelief.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonthlyRelief.Location = New System.Drawing.Point(8, 279)
        Me.lblMonthlyRelief.Name = "lblMonthlyRelief"
        Me.lblMonthlyRelief.Size = New System.Drawing.Size(112, 15)
        Me.lblMonthlyRelief.TabIndex = 106
        Me.lblMonthlyRelief.Text = "Monthly Relief"
        Me.lblMonthlyRelief.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMonthlyRelief
        '
        Me.cboMonthlyRelief.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMonthlyRelief.DropDownWidth = 180
        Me.cboMonthlyRelief.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMonthlyRelief.FormattingEnabled = True
        Me.cboMonthlyRelief.Location = New System.Drawing.Point(127, 276)
        Me.cboMonthlyRelief.Name = "cboMonthlyRelief"
        Me.cboMonthlyRelief.Size = New System.Drawing.Size(213, 21)
        Me.cboMonthlyRelief.TabIndex = 9
        '
        'lblActualContribution
        '
        Me.lblActualContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualContribution.Location = New System.Drawing.Point(8, 252)
        Me.lblActualContribution.Name = "lblActualContribution"
        Me.lblActualContribution.Size = New System.Drawing.Size(112, 15)
        Me.lblActualContribution.TabIndex = 104
        Me.lblActualContribution.Text = "Actual Contribution"
        Me.lblActualContribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboActualContribution
        '
        Me.cboActualContribution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActualContribution.DropDownWidth = 180
        Me.cboActualContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActualContribution.FormattingEnabled = True
        Me.cboActualContribution.Location = New System.Drawing.Point(127, 249)
        Me.cboActualContribution.Name = "cboActualContribution"
        Me.cboActualContribution.Size = New System.Drawing.Size(213, 21)
        Me.cboActualContribution.TabIndex = 8
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(465, 473)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 22
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(8, 225)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(112, 15)
        Me.lblMembership.TabIndex = 97
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 180
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(127, 222)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(213, 21)
        Me.cboMembership.TabIndex = 7
        '
        'lblLumpSum
        '
        Me.lblLumpSum.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLumpSum.Location = New System.Drawing.Point(8, 198)
        Me.lblLumpSum.Name = "lblLumpSum"
        Me.lblLumpSum.Size = New System.Drawing.Size(112, 15)
        Me.lblLumpSum.TabIndex = 95
        Me.lblLumpSum.Text = "Lump Sum"
        Me.lblLumpSum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLumpSum
        '
        Me.cboLumpSum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLumpSum.DropDownWidth = 180
        Me.cboLumpSum.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLumpSum.FormattingEnabled = True
        Me.cboLumpSum.Location = New System.Drawing.Point(127, 195)
        Me.cboLumpSum.Name = "cboLumpSum"
        Me.cboLumpSum.Size = New System.Drawing.Size(213, 21)
        Me.cboLumpSum.TabIndex = 6
        '
        'lblDirectorFee
        '
        Me.lblDirectorFee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDirectorFee.Location = New System.Drawing.Point(8, 171)
        Me.lblDirectorFee.Name = "lblDirectorFee"
        Me.lblDirectorFee.Size = New System.Drawing.Size(112, 15)
        Me.lblDirectorFee.TabIndex = 93
        Me.lblDirectorFee.Text = "Director's Fee"
        Me.lblDirectorFee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDirectorFee
        '
        Me.cboDirectorFee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDirectorFee.DropDownWidth = 180
        Me.cboDirectorFee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDirectorFee.FormattingEnabled = True
        Me.cboDirectorFee.Location = New System.Drawing.Point(127, 168)
        Me.cboDirectorFee.Name = "cboDirectorFee"
        Me.cboDirectorFee.Size = New System.Drawing.Size(213, 21)
        Me.cboDirectorFee.TabIndex = 5
        '
        'lblOverTimeAllow
        '
        Me.lblOverTimeAllow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOverTimeAllow.Location = New System.Drawing.Point(8, 144)
        Me.lblOverTimeAllow.Name = "lblOverTimeAllow"
        Me.lblOverTimeAllow.Size = New System.Drawing.Size(112, 15)
        Me.lblOverTimeAllow.TabIndex = 91
        Me.lblOverTimeAllow.Text = "Over Time Allowance"
        Me.lblOverTimeAllow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOverTimeAllow
        '
        Me.cboOverTimeAllow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOverTimeAllow.DropDownWidth = 180
        Me.cboOverTimeAllow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOverTimeAllow.FormattingEnabled = True
        Me.cboOverTimeAllow.Location = New System.Drawing.Point(127, 141)
        Me.cboOverTimeAllow.Name = "cboOverTimeAllow"
        Me.cboOverTimeAllow.Size = New System.Drawing.Size(213, 21)
        Me.cboOverTimeAllow.TabIndex = 4
        '
        'lblLeavePay
        '
        Me.lblLeavePay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeavePay.Location = New System.Drawing.Point(8, 117)
        Me.lblLeavePay.Name = "lblLeavePay"
        Me.lblLeavePay.Size = New System.Drawing.Size(112, 15)
        Me.lblLeavePay.TabIndex = 89
        Me.lblLeavePay.Text = "Leave Pay"
        Me.lblLeavePay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLeavePay
        '
        Me.cboLeavePay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeavePay.DropDownWidth = 180
        Me.cboLeavePay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeavePay.FormattingEnabled = True
        Me.cboLeavePay.Location = New System.Drawing.Point(127, 114)
        Me.cboLeavePay.Name = "cboLeavePay"
        Me.cboLeavePay.Size = New System.Drawing.Size(213, 21)
        Me.cboLeavePay.TabIndex = 3
        '
        'lblTransAllow
        '
        Me.lblTransAllow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransAllow.Location = New System.Drawing.Point(8, 90)
        Me.lblTransAllow.Name = "lblTransAllow"
        Me.lblTransAllow.Size = New System.Drawing.Size(112, 15)
        Me.lblTransAllow.TabIndex = 87
        Me.lblTransAllow.Text = "Transport Allowance"
        Me.lblTransAllow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTransAllow
        '
        Me.cboTransAllow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransAllow.DropDownWidth = 180
        Me.cboTransAllow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransAllow.FormattingEnabled = True
        Me.cboTransAllow.Location = New System.Drawing.Point(127, 87)
        Me.cboTransAllow.Name = "cboTransAllow"
        Me.cboTransAllow.Size = New System.Drawing.Size(213, 21)
        Me.cboTransAllow.TabIndex = 2
        '
        'gbBasicSalaryOtherEarning
        '
        Me.gbBasicSalaryOtherEarning.BorderColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.Checked = False
        Me.gbBasicSalaryOtherEarning.CollapseAllExceptThis = False
        Me.gbBasicSalaryOtherEarning.CollapsedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapseOnLoad = False
        Me.gbBasicSalaryOtherEarning.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbBasicSalaryOtherEarning.ExpandedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicSalaryOtherEarning.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicSalaryOtherEarning.HeaderHeight = 25
        Me.gbBasicSalaryOtherEarning.HeaderMessage = ""
        Me.gbBasicSalaryOtherEarning.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBasicSalaryOtherEarning.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.HeightOnCollapse = 0
        Me.gbBasicSalaryOtherEarning.LeftTextSpace = 0
        Me.gbBasicSalaryOtherEarning.Location = New System.Drawing.Point(11, 509)
        Me.gbBasicSalaryOtherEarning.Name = "gbBasicSalaryOtherEarning"
        Me.gbBasicSalaryOtherEarning.OpenHeight = 300
        Me.gbBasicSalaryOtherEarning.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicSalaryOtherEarning.ShowBorder = True
        Me.gbBasicSalaryOtherEarning.ShowCheckBox = True
        Me.gbBasicSalaryOtherEarning.ShowCollapseButton = False
        Me.gbBasicSalaryOtherEarning.ShowDefaultBorderColor = True
        Me.gbBasicSalaryOtherEarning.ShowDownButton = False
        Me.gbBasicSalaryOtherEarning.ShowHeader = True
        Me.gbBasicSalaryOtherEarning.Size = New System.Drawing.Size(667, 61)
        Me.gbBasicSalaryOtherEarning.TabIndex = 23
        Me.gbBasicSalaryOtherEarning.Temp = 0
        Me.gbBasicSalaryOtherEarning.Text = "Basic Salary As Other Earning"
        Me.gbBasicSalaryOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.objbtnSearchOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.cboOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblOtherEarning)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(662, 33)
        Me.pnlAdvanceTaken.TabIndex = 259
        '
        'objbtnSearchOtherEarning
        '
        Me.objbtnSearchOtherEarning.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOtherEarning.BorderSelected = False
        Me.objbtnSearchOtherEarning.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOtherEarning.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOtherEarning.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOtherEarning.Location = New System.Drawing.Point(289, 6)
        Me.objbtnSearchOtherEarning.Name = "objbtnSearchOtherEarning"
        Me.objbtnSearchOtherEarning.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOtherEarning.TabIndex = 1
        '
        'cboOtherEarning
        '
        Me.cboOtherEarning.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEarning.FormattingEnabled = True
        Me.cboOtherEarning.Location = New System.Drawing.Point(97, 6)
        Me.cboOtherEarning.Name = "cboOtherEarning"
        Me.cboOtherEarning.Size = New System.Drawing.Size(186, 21)
        Me.cboOtherEarning.TabIndex = 0
        '
        'lblOtherEarning
        '
        Me.lblOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherEarning.Location = New System.Drawing.Point(8, 8)
        Me.lblOtherEarning.Name = "lblOtherEarning"
        Me.lblOtherEarning.Size = New System.Drawing.Size(83, 16)
        Me.lblOtherEarning.TabIndex = 0
        Me.lblOtherEarning.Text = "Other Earning"
        Me.lblOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSlabEffectivePeriod
        '
        Me.cboSlabEffectivePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSlabEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSlabEffectivePeriod.FormattingEnabled = True
        Me.cboSlabEffectivePeriod.Location = New System.Drawing.Point(402, 60)
        Me.cboSlabEffectivePeriod.Name = "cboSlabEffectivePeriod"
        Me.cboSlabEffectivePeriod.Size = New System.Drawing.Size(277, 21)
        Me.cboSlabEffectivePeriod.TabIndex = 8
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(477, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 78
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objchkAllPeriod
        '
        Me.objchkAllPeriod.AutoSize = True
        Me.objchkAllPeriod.Location = New System.Drawing.Point(408, 93)
        Me.objchkAllPeriod.Name = "objchkAllPeriod"
        Me.objchkAllPeriod.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllPeriod.TabIndex = 4
        Me.objchkAllPeriod.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(399, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(103, 15)
        Me.lblPeriod.TabIndex = 76
        Me.lblPeriod.Text = "Effective Periods"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvPeriod
        '
        Me.lvPeriod.BackColorOnChecked = False
        Me.lvPeriod.CheckBoxes = True
        Me.lvPeriod.ColumnHeaders = Nothing
        Me.lvPeriod.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhPCheck, Me.colhPeriodName})
        Me.lvPeriod.CompulsoryColumns = ""
        Me.lvPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPeriod.FullRowSelect = True
        Me.lvPeriod.GridLines = True
        Me.lvPeriod.GroupingColumn = Nothing
        Me.lvPeriod.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPeriod.HideSelection = False
        Me.lvPeriod.Location = New System.Drawing.Point(402, 87)
        Me.lvPeriod.MinColumnWidth = 50
        Me.lvPeriod.MultiSelect = False
        Me.lvPeriod.Name = "lvPeriod"
        Me.lvPeriod.OptionalColumns = ""
        Me.lvPeriod.ShowMoreItem = False
        Me.lvPeriod.ShowSaveItem = False
        Me.lvPeriod.ShowSelectAll = True
        Me.lvPeriod.ShowSizeAllColumnsToFit = True
        Me.lvPeriod.Size = New System.Drawing.Size(277, 183)
        Me.lvPeriod.Sortable = True
        Me.lvPeriod.TabIndex = 9
        Me.lvPeriod.UseCompatibleStateImageBehavior = False
        Me.lvPeriod.View = System.Windows.Forms.View.Details
        '
        'objColhPCheck
        '
        Me.objColhPCheck.Tag = "objColhPCheck"
        Me.objColhPCheck.Text = ""
        Me.objColhPCheck.Width = 25
        '
        'colhPeriodName
        '
        Me.colhPeriodName.Tag = "colhPeriodName"
        Me.colhPeriodName.Text = "Periods"
        Me.colhPeriodName.Width = 205
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(127, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(213, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(346, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 56
        '
        'lblHouseAllow
        '
        Me.lblHouseAllow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHouseAllow.Location = New System.Drawing.Point(8, 63)
        Me.lblHouseAllow.Name = "lblHouseAllow"
        Me.lblHouseAllow.Size = New System.Drawing.Size(112, 15)
        Me.lblHouseAllow.TabIndex = 58
        Me.lblHouseAllow.Text = "House Allowance"
        Me.lblHouseAllow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(112, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboHouseAllow
        '
        Me.cboHouseAllow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHouseAllow.DropDownWidth = 180
        Me.cboHouseAllow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHouseAllow.FormattingEnabled = True
        Me.cboHouseAllow.Location = New System.Drawing.Point(127, 60)
        Me.cboHouseAllow.Name = "cboHouseAllow"
        Me.cboHouseAllow.Size = New System.Drawing.Size(213, 21)
        Me.cboHouseAllow.TabIndex = 1
        '
        'frmITaxFormBReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 583)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmITaxFormBReport"
        Me.ShowInTaskbar = False
        Me.Text = "iTax Form B Report"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbBasicSalaryOtherEarning.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbBasicSalaryOtherEarning As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchOtherEarning As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOtherEarning As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherEarning As System.Windows.Forms.Label
    Friend WithEvents cboSlabEffectivePeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents objchkAllPeriod As System.Windows.Forms.CheckBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lvPeriod As eZee.Common.eZeeListView
    Friend WithEvents objColhPCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriodName As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblHouseAllow As System.Windows.Forms.Label
    Friend WithEvents cboHouseAllow As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblLumpSum As System.Windows.Forms.Label
    Friend WithEvents cboLumpSum As System.Windows.Forms.ComboBox
    Friend WithEvents lblDirectorFee As System.Windows.Forms.Label
    Friend WithEvents cboDirectorFee As System.Windows.Forms.ComboBox
    Friend WithEvents lblOverTimeAllow As System.Windows.Forms.Label
    Friend WithEvents cboOverTimeAllow As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeavePay As System.Windows.Forms.Label
    Friend WithEvents cboLeavePay As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransAllow As System.Windows.Forms.Label
    Friend WithEvents cboTransAllow As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents SaveDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents lblActualContribution As System.Windows.Forms.Label
    Friend WithEvents cboActualContribution As System.Windows.Forms.ComboBox
    Friend WithEvents lblMonthlyRelief As System.Windows.Forms.Label
    Friend WithEvents cboMonthlyRelief As System.Windows.Forms.ComboBox
    Friend WithEvents lblResident As System.Windows.Forms.Label
    Friend WithEvents cboResident As System.Windows.Forms.ComboBox
    Friend WithEvents lblNonResident As System.Windows.Forms.Label
    Friend WithEvents cboNonResident As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployerOwnedHouseValue As System.Windows.Forms.Label
    Friend WithEvents cboEmployerOwnedHouseValue As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployerRentedHouseValue As System.Windows.Forms.Label
    Friend WithEvents cboEmployerRentedHouseValue As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployerRentedHouse As System.Windows.Forms.Label
    Friend WithEvents cboEmployerRentedHouse As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployerOwnedHouse As System.Windows.Forms.Label
    Friend WithEvents cboEmployerOwnedHouse As System.Windows.Forms.ComboBox
    Friend WithEvents lblColumnAB As System.Windows.Forms.Label
    Friend WithEvents cboColumnAB As System.Windows.Forms.ComboBox
    Friend WithEvents lblColumnS As System.Windows.Forms.Label
    Friend WithEvents cboColumnS As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisabilityMembership As System.Windows.Forms.Label
    Friend WithEvents cboDisabilityMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblColumnAG As System.Windows.Forms.Label
    Friend WithEvents cboColumnAG As System.Windows.Forms.ComboBox
    Friend WithEvents lblColumnN As System.Windows.Forms.Label
    Friend WithEvents cboColumnN As System.Windows.Forms.ComboBox
    Friend WithEvents lblColumnAA As System.Windows.Forms.Label
    Friend WithEvents cboColumnAA As System.Windows.Forms.ComboBox
    Friend WithEvents lblColumnAI As System.Windows.Forms.Label
    Friend WithEvents cboColumnAI As System.Windows.Forms.ComboBox
    Friend WithEvents chkIgnoreZero As System.Windows.Forms.CheckBox
End Class
