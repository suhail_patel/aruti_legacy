'************************************************************************************************************************************
'Class Name : clsITaxFormBReport.vb
'Purpose    :
'Date       :26/07/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsITaxFormBReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsITaxFormBReport"
    'Nilay (11 Apr 2017) -- Start
    'Enhancements: New statutory report iTax Form C for CCK
    'Private mstrReportId As String = enArutiReport.ITax_Form_B_Report
    Private mstrReportId As String = ""
    Private menReport As enArutiReport
    'Nilay (11 Apr 2017) -- End
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal enReport As enArutiReport, ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        'Nilay (11 Apr 2017) -- Start
        'Enhancements: New statutory report iTax Form C for CCK
        mstrReportId = enReport
        menReport = enReport
        'Nilay (11 Apr 2017) -- End

        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "
    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintColumnFTranId As Integer = 0
    Private mintColumnGTranId As Integer = 0
    Private mintColumnHTranId As Integer = 0
    Private mintColumnITranId As Integer = 0
    Private mintColumnJTranId As Integer = 0
    Private mintColumnKTranId As Integer = 0
    Private mintOtherEarningTranId As Integer = 0
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = String.Empty
    'Sohail (11 Sep 2015) -- Start
    'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
    Private mintColumnYTranId As Integer = 0
    Private mintColumnAFTranId As Integer = 0
    'Sohail (11 Sep 2015) -- End

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""

    'Nilay (17 Feb 2017) -- Start
    'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
    Private mintResidentPayTypeID As Integer = 0
    Private mintNonResidentPayTypeID As Integer = 0
    Private mintEmployerOwnedHousePayPointID As Integer = 0
    Private mintEmployerRentedHousePayPointID As Integer = 0
    Private mintEmployerOwnedHouseTranId As Integer = 0
    Private mintEmployerRentedHouseTranId As Integer = 0
    Private mintColumnSTranId As Integer = 0
    Private mintColumnABTranId As Integer = 0
    'Nilay (17 Feb 2017) -- End
    'Sohail (02 Feb 2018) -- Start
    'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
    Private mintColumnNTranId As Integer = 0
    Private mintColumnAATranId As Integer = 0
    Private mintColumnAGTranId As Integer = 0
    'Sohail (02 Feb 2018) -- End
    'Sohail (08 Mar 2018) -- Start
    'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
    Private mintColumnAITranId As Integer = 0
    'Sohail (08 Mar 2018) -- End
    'Sohail (03 Apr 2017) -- Start
    'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
    Private mintDisabilityMembershipId As Integer = 0
    Private mstrDisabilityMembershipName As String = String.Empty
    'Sohail (03 Apr 2017) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ColumnFTranId() As Integer
        Set(ByVal value As Integer)
            mintColumnFTranId = value
        End Set
    End Property

    Public WriteOnly Property _ColumnGTranId() As Integer
        Set(ByVal value As Integer)
            mintColumnGTranId = value
        End Set
    End Property

    Public WriteOnly Property _ColumnHTranId() As Integer
        Set(ByVal value As Integer)
            mintColumnHTranId = value
        End Set
    End Property

    Public WriteOnly Property _ColumnITranId() As Integer
        Set(ByVal value As Integer)
            mintColumnITranId = value
        End Set
    End Property

    Public WriteOnly Property _ColumnJTranId() As Integer
        Set(ByVal value As Integer)
            mintColumnJTranId = value
        End Set
    End Property

    Public WriteOnly Property _ColumnKTranId() As Integer
        Set(ByVal value As Integer)
            mintColumnKTranId = value
        End Set
    End Property


    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property

    'Sohail (11 Sep 2015) -- Start
    'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
    Public WriteOnly Property _ColumnYTranId() As Integer
        Set(ByVal value As Integer)
            mintColumnYTranId = value
        End Set
    End Property

    Public WriteOnly Property _ColumnAFTranId() As Integer
        Set(ByVal value As Integer)
            mintColumnAFTranId = value
        End Set
    End Property
    'Sohail (11 Sep 2015) -- End



    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GroupName = value
        End Set
    End Property

    'Nilay (17 Feb 2017) -- Start
    'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
    Public WriteOnly Property _ResidentPayTypeID() As Integer
        Set(ByVal value As Integer)
            mintResidentPayTypeID = value
        End Set
    End Property

    Public WriteOnly Property _NonResidentPayTypeID() As Integer
        Set(ByVal value As Integer)
            mintNonResidentPayTypeID = value
        End Set
    End Property

    Public WriteOnly Property _ColumnSTranId() As Integer
        Set(ByVal value As Integer)
            mintColumnSTranId = value
        End Set
    End Property

    Public WriteOnly Property _ColumnABTranId() As Integer
        Set(ByVal value As Integer)
            mintColumnABTranId = value
        End Set
    End Property

    'Sohail (02 Feb 2018) -- Start
    'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
    Public WriteOnly Property _ColumnNTranId() As Integer
        Set(ByVal value As Integer)
            mintColumnNTranId = value
        End Set
    End Property

    Public WriteOnly Property _ColumnAATranId() As Integer
        Set(ByVal value As Integer)
            mintColumnAATranId = value
        End Set
    End Property

    Public WriteOnly Property _ColumnAGTranId() As Integer
        Set(ByVal value As Integer)
            mintColumnAGTranId = value
        End Set
    End Property
    'Sohail (02 Feb 2018) -- End

    'Sohail (08 Mar 2018) -- Start
    'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
    Public WriteOnly Property _ColumnAITranId() As Integer
        Set(ByVal value As Integer)
            mintColumnAITranId = value
        End Set
    End Property
    'Sohail (08 Mar 2018) -- End

    Public WriteOnly Property _EmployerOwnedHousePayPointID() As Integer
        Set(ByVal value As Integer)
            mintEmployerOwnedHousePayPointID = value
        End Set
    End Property

    Public WriteOnly Property _EmployerRentedHousePayPointID() As Integer
        Set(ByVal value As Integer)
            mintEmployerRentedHousePayPointID = value
        End Set
    End Property

    Public WriteOnly Property _EmployerOwnedHouseTranId() As Integer
        Set(ByVal value As Integer)
            mintEmployerOwnedHouseTranId = value
        End Set
    End Property

    Public WriteOnly Property _EmployerRentedHouseTranId() As Integer
        Set(ByVal value As Integer)
            mintEmployerRentedHouseTranId = value
        End Set
    End Property
    'Nilay (17 Feb 2017) -- End

    'Sohail (03 Apr 2017) -- Start
    'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
    Public WriteOnly Property _DisabilityMembershipId() As Integer
        Set(ByVal value As Integer)
            mintDisabilityMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _DisabilityMembershipName() As String
        Set(ByVal value As String)
            mstrDisabilityMembershipName = value
        End Set
    End Property
    'Sohail (03 Apr 2017) -- End

    'Hemant (17 Nov 2020) -- Start
    'Enhancement # OLD-204 : 1) By default, Ignore Zero Values on Itax Form B report. Employees with zero values all through (or Unprocessed employees)  should NOT appear on report at all. 
    Private mblnIgnoreZeroValue As Boolean = False
    Public WriteOnly Property _IgnoreZeroValue() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnoreZeroValue = value
        End Set
    End Property

    Private mblnIncluderInactiveEmp As Boolean = False
    Public WriteOnly Property _IncluderInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncluderInactiveEmp = value
        End Set
    End Property
    'Hemant (17 Nov 2020) -- End

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrPeriodIds = ""
            mstrPeriodNames = ""
            mintOtherEarningTranId = 0
            mintMembershipId = 0
            mstrMembershipName = ""
            'Sohail (11 Sep 2015) -- Start
            'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
            mintColumnFTranId = 0
            mintColumnGTranId = 0
            mintColumnHTranId = 0
            mintColumnITranId = 0
            mintColumnJTranId = 0
            mintColumnKTranId = 0
            mintColumnYTranId = 0
            mintColumnAFTranId = 0
            'Sohail (11 Sep 2015) -- End
            'Sohail (03 Apr 2017) -- Start
            'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
            mintDisabilityMembershipId = 0
            mstrDisabilityMembershipName = ""
            'Sohail (03 Apr 2017) -- End

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GroupName = ""

            'Nilay (17 Feb 2017) -- Start
            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
            mintColumnSTranId = 0
            mintColumnABTranId = 0
            mintResidentPayTypeID = 0
            mintNonResidentPayTypeID = 0
            mintEmployerOwnedHousePayPointID = 0
            mintEmployerRentedHousePayPointID = 0
            mintEmployerOwnedHouseTranId = 0
            mintEmployerRentedHouseTranId = 0
            'Nilay (17 Feb 2017) -- End
            'Sohail (02 Feb 2018) -- Start
            'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
            mintColumnNTranId = 0
            mintColumnAATranId = 0
            mintColumnAGTranId = 0
            'Sohail (02 Feb 2018) -- End
            'Sohail (08 Mar 2018) -- Start
            'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
            mintColumnAITranId = 0
            'Sohail (08 Mar 2018) -- End

            'Hemant (17 Nov 2020) -- Start
            'Enhancement # OLD-204 : 1) By default, Ignore Zero Values on Itax Form B report. Employees with zero values all through (or Unprocessed employees)  should NOT appear on report at all. 
            mblnIgnoreZeroValue = False
            'Hemant (17 Nov 2020) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@column_f_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnFTranId)
            objDataOperation.AddParameter("@column_g_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnGTranId)
            objDataOperation.AddParameter("@column_h_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnHTranId)
            objDataOperation.AddParameter("@column_i_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnITranId)
            objDataOperation.AddParameter("@column_j_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnJTranId)
            objDataOperation.AddParameter("@column_k_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnKTranId)
            'Nilay (17 Feb 2017) -- Start
            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
            objDataOperation.AddParameter("@column_s_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnSTranId)
            objDataOperation.AddParameter("@column_ownedhouse_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployerOwnedHouseTranId)
            objDataOperation.AddParameter("@column_rentedhouse_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployerRentedHouseTranId)
            objDataOperation.AddParameter("@column_ab_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnABTranId)
            'Nilay (17 Feb 2017) -- End

            'Sohail (02 Feb 2018) -- Start
            'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
            objDataOperation.AddParameter("@column_n_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnNTranId)
            objDataOperation.AddParameter("@column_aa_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnAATranId)
            objDataOperation.AddParameter("@column_ag_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnAGTranId)
            'Sohail (02 Feb 2018) -- End
            'Sohail (08 Mar 2018) -- Start
            'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
            objDataOperation.AddParameter("@column_ai_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnAITranId)
            'Sohail (08 Mar 2018) -- End

            'Sohail (11 Sep 2015) -- Start
            'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
            objDataOperation.AddParameter("@column_y_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnYTranId)
            objDataOperation.AddParameter("@column_af_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnAFTranId)
            'Sohail (11 Sep 2015) -- End

            'Sohail (03 Apr 2017) -- Start
            'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
            objDataOperation.AddParameter("@disabilitymembershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisabilityMembershipId)
            'Sohail (03 Apr 2017) -- End

            If mintEmployeeId > 0 Then
                'Hemant (07 Dec 2020) -- Start
                'Issue #AH-1841: Active Employee Missing on Itax Form B report 
                'objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                'Me._FilterQuery &= " AND empid = @EmpId "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                'Hemant (07 Dec 2020) -- End
            End If

            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 1, "Employee Code")))

            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ')", Language.getMessage(mstrModuleName, 2, "Employee Name")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function Generate_PayrollReport(ByVal strExportPath As String, _
                                           ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer, _
                                           ByVal dtPeriodStart As Date) As Boolean
        'Public Function Generate_PayrollReport(ByVal strExportPath As String) As Boolean
        'Hemant (07 Dec 2020) -- [dtPeriodStart]
        'S.SANDEEP [04 JUN 2015] -- END
        Dim dsList As DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Dim StrInnerQ As String = String.Empty
        Dim objActivity As New clsActivity_Master
        Dim strBuilder As New StringBuilder
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            'Hemant (17 Nov 2020) -- Start
            'Enhancement # OLD-204 : 2) Employees terminated & excluded from Payroll for that month should NOT appear on this report.
            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            'Hemant (07 Dec 2020) -- Start
            'Issue #AH-1841: Active Employee Missing on Itax Form B report
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodEnd, dtPeriodEnd, , , strDatabaseName)
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , True, strDatabaseName)
            'Hemant (07 Dec 2020) -- End

            'Hemant (17 Nov 2020) -- End
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            StrQ = "SELECT  hremployee_master.employeeunkid " & _
                          ", hremployee_master.employeecode " & _
                          ", REPLACE(ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' +  ISNULL(hremployee_master.surname, ''), '  ',' ') AS employeename "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "INTO #TableEmp " & _
                   "FROM    hremployee_master "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If
            'Sohail (08 Oct 2020) -- End

            'Hemant (17 Nov 2020) -- Start
            'Enhancement # OLD-204 : 2) Employees terminated & excluded from Payroll for that month should NOT appear on this report.
            If mblnIncluderInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If
            'Hemant (07 Dec 2020) -- Start
            'Issue #AH-1841: Active Employee Missing on Itax Form B report
            'StrQ &= " AND ISNULL(TRM.IsExPayroll, 0) = 0 "
            'Hemant (07 Dec 2020) -- End
            'Hemant (17 Nov 2020) -- End

            StrQ &= "SELECT  basicpay  " & _
                          ", column_f " & _
                          ", column_g " & _
                          ", column_h " & _
                          ", column_i " & _
                          ", column_j " & _
                          ", column_k " & _
                          ", column_s " & _
                          ", column_ownedhouse " & _
                          ", column_rentedhouse " & _
                          ", column_y " & _
                          ", column_ab " & _
                          ", column_af " & _
                          ", column_n " & _
                          ", column_aa " & _
                          ", column_ag " & _
                          ", column_ai " & _
                          ", grosspay " & _
                          ", grosspay - basicpay - column_f - column_g - column_h - column_i - column_j - column_k AS otherearnings " & _
                          ", noncashbenefit " & _
                          ", taxpayable " & _
                          ", relief " & _
                          ", taxpayable - relief AS taxdue " & _
                          ", empid AS empid " & _
                          ", A.employeecode " & _
                          ", employeename " & _
                          ", ISNULL(M.membershipno,'') AS MembershipNo " & _
                          ", ISNULL(hremployee_master.present_address1, '') + ' ' + ISNULL(hremployee_master.present_address2, '') AS ADDRESS " & _
                          ", ISNULL(hremployee_master.paytypeunkid,0) AS paytypeunkid " & _
                          ", ptype.name AS PayType " & _
                          ", hremployee_master.paypointunkid " & _
                          ", ISNULL(prpaypoint_master.paypointname,'') AS paypointname " & _
                          ", ISNULL(hrmsConfiguration..cfcity_master.name, '') AS city " & _
                          ", CC.costcentercode AS CCCode " & _
                          ", CC.costcentername AS CCName " & _
                          ", column_x " & _
                          ", Id " & _
                          ", GName " & _
                          ", ISNULL(D.membershipno,'') AS DisabilityMembershipNo " & _
                    "FROM    ( SELECT    SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay  " & _
                                      ", SUM(ISNULL(PAYE.column_f, 0)) AS column_f " & _
                                      ", SUM(ISNULL(PAYE.column_g, 0)) AS column_g " & _
                                      ", SUM(ISNULL(PAYE.column_h, 0)) AS column_h " & _
                                      ", SUM(ISNULL(PAYE.column_i, 0)) AS column_i " & _
                                      ", SUM(ISNULL(PAYE.column_j, 0)) AS column_j " & _
                                      ", SUM(ISNULL(PAYE.column_k, 0)) AS column_k " & _
                                      ", SUM(ISNULL(PAYE.column_s, 0)) AS column_s " & _
                                      ", SUM(ISNULL(PAYE.column_ownedhouse, 0)) AS column_ownedhouse " & _
                                      ", SUM(ISNULL(PAYE.column_rentedhouse, 0)) AS column_rentedhouse " & _
                                      ", SUM(ISNULL(PAYE.column_y, 0)) AS column_y " & _
                                      ", SUM(ISNULL(PAYE.column_ab, 0)) AS column_ab " & _
                                      ", SUM(ISNULL(PAYE.column_af, 0)) AS column_af " & _
                                      ", SUM(ISNULL(PAYE.column_n, 0)) AS column_n " & _
                                      ", SUM(ISNULL(PAYE.column_aa, 0)) AS column_aa " & _
                                      ", SUM(ISNULL(PAYE.column_ag, 0)) AS column_ag " & _
                                      ", SUM(ISNULL(PAYE.column_ai, 0)) AS column_ai " & _
                                      ", SUM(ISNULL(PAYE.noncashbenefit, 0)) AS noncashbenefit " & _
                                      ", SUM(ISNULL(PAYE.taxpayable, 0)) AS taxpayable " & _
                                      ", SUM(ISNULL(PAYE.relief, 0)) AS relief " & _
                                      ", SUM(ISNULL(PAYE.grosspay, 0)) AS grosspay " & _
                                      ", SUM(ISNULL(PAYE.column_x, 0)) AS column_x " & _
                                      ", paye.empid AS empid " & _
                                      ", employeecode " & _
                                      ", employeename " & _
                                      ", Id " & _
                                      ", GName " & _
                              "FROM      ( SELECT    CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 AS column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]
            'Nilay (17 Feb 2017) -- [paytypeunkid, paypointunkid, paypointname]
            'Sohail (11 Sep 2015) - [column_y, column_af]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "
            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
            '    StrQ &= "AND vwPayroll.tranheadunkid = @OtherEarningTranId  " & _
            '            "AND vwPayroll.GroupID = " & enViewPayroll_Group.Earning & " "
            'Else
            '    'Sohail (22 Mar 2018) -- Start
            '    'CCK Issue - Support Issue Id # 002128 : Wrong basic salary getting captured on itax form B for some employees in 70.2.
            '    'StrQ &= "AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  "
            '    StrQ &= "AND vwPayroll.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & " AND (vwPayroll.typeof_id = " & CInt(enTypeOf.Salary) & "  OR vwPayroll.isbasicsalaryasotherearning = 1)  "
            '    'Sohail (22 Mar 2018) -- End
            'End If

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
            Else
                StrQ &= "AND prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & " AND (prtranhead_master.typeof_id = " & CInt(enTypeOf.Salary) & "  OR prtranhead_master.isbasicsalaryasotherearning = 1)  "
            End If
            'Sohail (08 Oct 2020) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END


            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]
            'Sohail (11 Sep 2015) - [column_y, column_af]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "
            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @column_f_id " & _
            '                                        "AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            'If mintColumnFTranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_f_id " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintColumnFTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]
            'Sohail (11 Sep 2015) - [column_y, column_af]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @column_g_id " & _
            '                                        "AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            'If mintColumnGTranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_g_id " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintColumnGTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]
            'Sohail (11 Sep 2015) - [column_y, column_af]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @column_h_id " & _
            '                                        "AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            'If mintColumnHTranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_h_id " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintColumnHTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]
            'Sohail (11 Sep 2015) - [column_y, column_af]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @column_i_id " & _
            '                                        "AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            'If mintColumnITranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_i_id " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintColumnITranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]
            'Sohail (11 Sep 2015) - [column_y, column_af]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @column_j_id " & _
            '                                        "AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            'If mintColumnJTranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_j_id " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintColumnJTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]
            'Sohail (11 Sep 2015) - [column_y, column_af]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @column_k_id " & _
            '                                        "AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            'If mintColumnKTranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_k_id " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintColumnKTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (08 Oct 2020) -- End

            'Nilay (17 Feb 2017) -- Start
            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @column_s_id " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            'If mintColumnSTranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_s_id " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintColumnSTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @column_ownedhouse_id " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            'If mintEmployerOwnedHouseTranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_ownedhouse_id " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintEmployerOwnedHouseTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @column_rentedhouse_id " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            'If mintEmployerRentedHouseTranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''Nilay (17 Feb 2017) -- End
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_rentedhouse_id " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintEmployerRentedHouseTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (08 Oct 2020) -- End

            'Sohail (11 Sep 2015) -- Start
            'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
            StrQ &= "                      UNION ALL " & _
                                         "SELECT    0 AS basicsalary  " & _
                                                 ", 0 AS column_f " & _
                                                 ", 0 AS column_g " & _
                                                 ", 0 AS column_h " & _
                                                 ", 0 AS column_i " & _
                                                 ", 0 AS column_j " & _
                                                 ", 0 AS column_k " & _
                                                 ", 0 AS column_s " & _
                                                 ", 0 AS column_ownedhouse " & _
                                                 ", 0 AS column_rentedhouse " & _
                                                 ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_y " & _
                                                 ", 0 AS column_ab " & _
                                                 ", 0 AS column_af " & _
                                                 ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                 ", 0 AS noncashbenefit " & _
                                                 ", 0 AS taxpayable " & _
                                                 ", 0 AS relief " & _
                                                 ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                 ", #TableEmp.employeeunkid AS empid " & _
                                                 ", #TableEmp.employeecode " & _
                                                 ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      prpayrollprocess_tran " & _
            '                                        "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '                                        "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                                        "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
            '                                        "AND prtnaleave_tran.isvoid = 0 " & _
            '                                        "AND prtranhead_master.isvoid = 0 " & _
            '                                        "AND prtranhead_master.tranheadunkid > 0 " & _
            '                                        "AND prtranhead_master.tranheadunkid = @column_y_id " & _
            '                                        "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
            'End If

            'If mintColumnYTranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If


            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_y_id " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintColumnYTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (08 Oct 2020) -- End

            'Nilay (17 Feb 2017) -- Start
            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
            StrQ &= "                      UNION ALL " & _
                                         "SELECT    0 AS basicsalary  " & _
                                                 ", 0 AS column_f " & _
                                                 ", 0 AS column_g " & _
                                                 ", 0 AS column_h " & _
                                                 ", 0 AS column_i " & _
                                                 ", 0 AS column_j " & _
                                                 ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                 ", 0 AS column_y " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @column_ab_id " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            'If mintColumnABTranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''Nilay (17 Feb 2017) -- End
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_ab_id " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintColumnABTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                         "SELECT    0 AS basicsalary  " & _
                                                 ", 0 AS column_f " & _
                                                 ", 0 AS column_g " & _
                                                 ", 0 AS column_h " & _
                                                 ", 0 AS column_i " & _
                                                 ", 0 AS column_j " & _
                                                 ", 0 AS column_k " & _
                                                 ", 0 AS column_s " & _
                                                 ", 0 AS column_ownedhouse " & _
                                                 ", 0 AS column_rentedhouse " & _
                                                 ", 0 AS column_y " & _
                                                 ", 0 AS column_ab " & _
                                                 ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_af " & _
                                                 ", 0 AS column_n " & _
                                                 ", 0 AS column_aa " & _
                                                 ", 0 AS column_ag " & _
                                                 ", 0 AS column_ai " & _
                                                 ", 0 AS noncashbenefit " & _
                                                 ", 0 AS taxpayable " & _
                                                 ", 0 AS relief " & _
                                                 ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                 ", #TableEmp.employeeunkid AS empid " & _
                                                 ", #TableEmp.employeecode " & _
                                                 ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_af_id " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
            'End If
            'Sohail (08 Oct 2020) -- End

            If mintColumnAFTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'Sohail (08 Oct 2020) -- End

            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (11 Sep 2015) -- End

            'Sohail (02 Feb 2018) -- Start
            'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
            StrQ &= "                      UNION ALL " & _
                                         "SELECT    0 AS basicsalary  " & _
                                                 ", 0 AS column_f " & _
                                                 ", 0 AS column_g " & _
                                                 ", 0 AS column_h " & _
                                                 ", 0 AS column_i " & _
                                                 ", 0 AS column_j " & _
                                                 ", 0 AS column_k " & _
                                                 ", 0 AS column_s " & _
                                                 ", 0 AS column_ownedhouse " & _
                                                 ", 0 AS column_rentedhouse " & _
                                                 ", 0 AS column_y " & _
                                                 ", 0 AS column_ab " & _
                                                 ", 0 AS column_af " & _
                                                 ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_n " & _
                                                 ", 0 AS column_aa " & _
                                                 ", 0 AS column_ag " & _
                                                 ", 0 AS column_ai " & _
                                                 ", 0 AS noncashbenefit " & _
                                                 ", 0 AS taxpayable " & _
                                                 ", 0 AS relief " & _
                                                 ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                 ", #TableEmp.employeeunkid AS empid " & _
                                                 ", #TableEmp.employeecode " & _
                                                 ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_n_id " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
            'End If
            'Sohail (08 Oct 2020) -- End

            If mintColumnNTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                         "SELECT    0 AS basicsalary  " & _
                                                 ", 0 AS column_f " & _
                                                 ", 0 AS column_g " & _
                                                 ", 0 AS column_h " & _
                                                 ", 0 AS column_i " & _
                                                 ", 0 AS column_j " & _
                                                 ", 0 AS column_k " & _
                                                 ", 0 AS column_s " & _
                                                 ", 0 AS column_ownedhouse " & _
                                                 ", 0 AS column_rentedhouse " & _
                                                 ", 0 AS column_y " & _
                                                 ", 0 AS column_ab " & _
                                                 ", 0 AS column_af " & _
                                                 ", 0 AS column_n " & _
                                                 ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_aa " & _
                                                 ", 0 AS column_ag " & _
                                                 ", 0 AS column_ai " & _
                                                 ", 0 AS noncashbenefit " & _
                                                 ", 0 AS taxpayable " & _
                                                 ", 0 AS relief " & _
                                                 ", 0 AS grosspay " & _
                                                 ", 0 as column_x " & _
                                                 ", #TableEmp.employeeunkid AS empid " & _
                                                 ", #TableEmp.employeecode " & _
                                                 ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_aa_id " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
            'End If
            'Sohail (08 Oct 2020) -- End

            If mintColumnAATranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                         "SELECT    0 AS basicsalary  " & _
                                                 ", 0 AS column_f " & _
                                                 ", 0 AS column_g " & _
                                                 ", 0 AS column_h " & _
                                                 ", 0 AS column_i " & _
                                                 ", 0 AS column_j " & _
                                                 ", 0 AS column_k " & _
                                                 ", 0 AS column_s " & _
                                                 ", 0 AS column_ownedhouse " & _
                                                 ", 0 AS column_rentedhouse " & _
                                                 ", 0 AS column_y " & _
                                                 ", 0 AS column_ab " & _
                                                 ", 0 AS column_af " & _
                                                 ", 0 AS column_n " & _
                                                 ", 0 AS column_aa " & _
                                                 ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_ag " & _
                                                 ", 0 AS column_ai " & _
                                                 ", 0 AS noncashbenefit " & _
                                                 ", 0 AS taxpayable " & _
                                                 ", 0 AS relief " & _
                                                 ", 0 AS grosspay " & _
                                                 ", 0 as column_x " & _
                                                 ", #TableEmp.employeeunkid AS empid " & _
                                                 ", #TableEmp.employeecode " & _
                                                 ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_ag_id " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
            'End If
            'Sohail (08 Oct 2020) -- End

            If mintColumnAGTranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'Sohail (08 Oct 2020) -- End
            'Sohail (02 Feb 2018) -- End

            'Sohail (08 Mar 2018) -- Start
            'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
            StrQ &= "                      UNION ALL " & _
                                         "SELECT    0 AS basicsalary  " & _
                                                 ", 0 AS column_f " & _
                                                 ", 0 AS column_g " & _
                                                 ", 0 AS column_h " & _
                                                 ", 0 AS column_i " & _
                                                 ", 0 AS column_j " & _
                                                 ", 0 AS column_k " & _
                                                 ", 0 AS column_s " & _
                                                 ", 0 AS column_ownedhouse " & _
                                                 ", 0 AS column_rentedhouse " & _
                                                 ", 0 AS column_y " & _
                                                 ", 0 AS column_ab " & _
                                                 ", 0 AS column_af " & _
                                                 ", 0 AS column_n " & _
                                                 ", 0 AS column_aa " & _
                                                 ", 0 AS column_ag " & _
                                                 ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_ai " & _
                                                 ", 0 AS noncashbenefit " & _
                                                 ", 0 AS taxpayable " & _
                                                 ", 0 AS relief " & _
                                                 ", 0 AS grosspay " & _
                                                 ", 0 as column_x " & _
                                                 ", #TableEmp.employeeunkid AS empid " & _
                                                 ", #TableEmp.employeecode " & _
                                                 ", #TableEmp.employeename "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @column_ai_id " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
            'End If
            'Sohail (08 Oct 2020) -- End

            'Sohail (20 Mar 2018) -- Start
            'Support Issue id # 0002118 : Column AI on itax form B picking 0 instead of the actual PAYE SP(70.2)
            'If mintColumnNTranId <= 0 Then
            '    StrQ &= " AND 1 = 2 "
            'End If
            If mintColumnAITranId <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (20 Mar 2018) -- End

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'Sohail (08 Oct 2020) -- End
            'Sohail (08 Mar 2018) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]
            'Sohail (11 Sep 2015) - [column_y, column_af]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            '                                        "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid "

            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     vwPayroll.GroupID IN ( " & enViewPayroll_Group.Informational & " ) " & _
            '                                        "AND prtranhead_master.isnoncashbenefit = 1 " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.Informational & " ) " & _
                                                    "AND prtranhead_master.isnoncashbenefit = 1 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]
            'Sohail (11 Sep 2015) - [column_y, column_af]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     vwPayroll.typeof_id = " & enTypeOf.Taxes & " " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                     "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                     "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                     "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.typeof_id = " & enTypeOf.Taxes & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS relief " & _
                                                  ", 0 AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]
            'Sohail (11 Sep 2015) - [column_y, column_af]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            '                                        "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid "

            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     vwPayroll.GroupID IN ( " & enViewPayroll_Group.Earning & ", " & enViewPayroll_Group.Deduction & ", " & enViewPayroll_Group.EmployerContribution & ", " & enViewPayroll_Group.Informational & " ) " & _
            '                                        "AND prtranhead_master.istaxrelief = 1 " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployersStatutoryContributions & ", " & enTranHeadType.Informational & " ) " & _
                                                    "AND prtranhead_master.istaxrelief = 1 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary  " & _
                                                  ", 0 AS column_f " & _
                                                  ", 0 AS column_g " & _
                                                  ", 0 AS column_h " & _
                                                  ", 0 AS column_i " & _
                                                  ", 0 AS column_j " & _
                                                  ", 0 AS column_k " & _
                                                  ", 0 AS column_s " & _
                                                  ", 0 AS column_ownedhouse " & _
                                                  ", 0 AS column_rentedhouse " & _
                                                  ", 0 AS column_y " & _
                                                  ", 0 AS column_ab " & _
                                                  ", 0 AS column_af " & _
                                                  ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS relief " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS grosspay " & _
                                                  ", 0 as column_x " & _
                                                  ", #TableEmp.employeeunkid AS empid " & _
                                                  ", #TableEmp.employeecode " & _
                                                  ", #TableEmp.employeename "
            'Sohail (08 Mar 2018) - [column_ai]
            'Sohail (02 Feb 2018) - [column_n, column_aa, column_ag]
            'Nilay (17 Feb 2017) -- [column_s, column_ownedhouse, column_rentedhouse, column_ab]
            'Sohail (11 Sep 2015) - [column_y, column_af]

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "                      FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'StrQ &= "                      WHERE     vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                                        "AND vwPayroll.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND vwPayroll.employeeunkid = @EmpId "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.add_deduct = 1 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "
            'Sohail (08 Oct 2020) -- End

            'Gajanan [10-July-2020] -- Start
            StrQ &= "                      UNION ALL " & _
                                         "SELECT    0 AS basicsalary  " & _
                                                 ", 0 AS column_f " & _
                                                 ", 0 AS column_g " & _
                                                 ", 0 AS column_h " & _
                                                 ", 0 AS column_i " & _
                                                 ", 0 AS column_j " & _
                                                 ", 0 AS column_k " & _
                                                 ", 0 AS column_s " & _
                                                 ", 0 AS column_ownedhouse " & _
                                                 ", 0 AS column_rentedhouse " & _
                                                 ", 0 AS column_y " & _
                                                 ", 0 AS column_ab " & _
                                                 ", 0 AS column_af " & _
                                                 ", 0 AS column_n " & _
                                                  ", 0 AS column_aa " & _
                                                  ", 0 AS column_ag " & _
                                                  ", 0 AS column_ai " & _
                                                 ", 0 AS noncashbenefit " & _
                                                 ", 0 AS taxpayable " & _
                                                 ", 0 AS relief " & _
                                                 ", 0 AS grosspay " & _
                                                 ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS column_x " & _
                                                 ", #TableEmp.employeeunkid AS empid " & _
                                                 ", #TableEmp.employeecode " & _
                                                 ", #TableEmp.employeename "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableEmp.Id, #TableEmp.GName "
            'Sohail (08 Oct 2020) -- End

            StrQ &= "                      FROM      prpayrollprocess_tran " & _
                                                    "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid  = #TableEmp.employeeunkid " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'Sohail (08 Oct 2020) -- End

            StrQ &= " WHERE prpayrollprocess_tran.isvoid = 0 " & _
                    "AND prtnaleave_tran.isvoid = 0 " & _
                    "AND prtranhead_master.isvoid = 0 " & _
                    "AND prtranhead_master.tranheadunkid > 0 " & _
                    "AND prtranhead_master.calctype_id IN ( " & enCalcType.AsComputedOnWithINEXCESSOFTaxSlab & "  ) " & _
                    "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            'If mintEmployeeId > 0 Then
            '    StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'Sohail (08 Oct 2020) -- End

            'Gajanan [10-July-2020] -- End

            StrQ &= "                    ) AS PAYE " & _
                              "WHERE     1 = 1 " & _
                              "GROUP BY  empid  " & _
                                      ", employeecode " & _
                                      ", employeename " & _
                                      ", Id " & _
                                      ", GName " & _
                            ") AS A " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
                            "LEFT JOIN cfcommon_master as ptype ON ptype.masterunkid = hremployee_master.paytypeunkid AND ptype.mastertype = " & clsCommon_Master.enCommonMaster.PAY_TYPE & " " & _
                            "LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = hremployee_master.present_post_townunkid " & _
                            "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid "

            'Nilay (17 Feb 2017) -- Start
            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
            StrQ &= "LEFT JOIN prpaypoint_master ON prpaypoint_master.paypointunkid = hremployee_master.paypointunkid "
            'Nilay (17 Feb 2017) -- End

            StrQ &= "LEFT JOIN ( SELECT  employeeunkid " & _
                                              ", membershipno = ISNULL(STUFF(( SELECT    '; ' + membershipno " & _
                                                                              "FROM      hremployee_master AS b "
            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                StrQ &= System.Text.RegularExpressions.Regex.Replace(xUACQry, "\bhremployee_master\b", "b")
            End If
            'S.SANDEEP [15 NOV 2016] -- END
            StrQ &= "LEFT JOIN hremployee_meminfo_tran ON b.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                                                                              "WHERE     membershipno IS NOT NULL " & _
                                                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                                                        "AND b.employeeunkid = a.employeeunkid "

            If mintMembershipId > 0 Then
                StrQ &= "AND membershipunkid = @membershipunkid "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & System.Text.RegularExpressions.Regex.Replace(xUACFiltrQry, "\bhremployee_master\b", "b")
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "                                                        FOR " & _
                    "                 XML PATH('')), 1, 2, ''), '') " & _
                    "                 FROM    hremployee_master a " & _
                    "                 GROUP BY employeeunkid " & _
                    "                 ) AS M ON M.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (03 Apr 2017) -- Start
            'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
            StrQ &= "LEFT JOIN ( SELECT  employeeunkid " & _
                                      ", membershipno = ISNULL(STUFF(( SELECT    '; ' + membershipno " & _
                                                                              "FROM      hremployee_master AS b "
            If xUACQry.Trim.Length > 0 Then
                StrQ &= System.Text.RegularExpressions.Regex.Replace(xUACQry, "\bhremployee_master\b", "b")
            End If

            StrQ &= "LEFT JOIN hremployee_meminfo_tran ON b.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                                                                              "WHERE     membershipno IS NOT NULL " & _
                                                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                                                        "AND b.employeeunkid = a.employeeunkid " & _
                                                                                        "AND membershipunkid = @disabilitymembershipunkid "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & System.Text.RegularExpressions.Regex.Replace(xUACFiltrQry, "\bhremployee_master\b", "b")
            End If

            StrQ &= "                                                        FOR " & _
                                                                              "XML PATH('')), 1, 2, ''), '') " & _
                                       "FROM    hremployee_master a " & _
                                      "GROUP BY employeeunkid " & _
                                    ") AS D ON D.employeeunkid = hremployee_master.employeeunkid "
            'Sohail (03 Apr 2017) -- End

            StrQ &= "WHERE   1 = 1 "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery


            If mintViewIndex > 0 Then
                StrQ &= " ORDER BY GNAME, employeename"
            Else
                StrQ &= " ORDER BY employeename "
            End If

            'Sohail (08 Oct 2020) -- Start
            'Sheer Logic Performance Issue # OLD-181 : - Itax Form B Report does not Export.
            StrQ &= " DROP TABLE #TableEmp "
            'Sohail (08 Oct 2020) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Hemant (17 Nov 2020) -- Start
            'Enhancement # OLD-204 : 1) By default, Ignore Zero Values on Itax Form B report. Employees with zero values all through (or Unprocessed employees)  should NOT appear on report at all. 
            If mblnIgnoreZeroValue Then
                Dim dtIgnoreZero As DataTable
                dtIgnoreZero = IgnoreZeroHead(dsList.Tables(0))
                dsList.Tables.RemoveAt(0)
                dsList.Tables.Add(dtIgnoreZero)
            End If
            'Hemant (17 Nov 2020) -- End

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                strBuilder.Append(dtRow.Item("MembershipNo").ToString) 'Column A
                strBuilder.Append("," & dtRow.Item("employeename").ToString & "") 'Column B
                'Sohail (03 Apr 2017) -- Start
                'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
                'strBuilder.Append("," & dtRow.Item("PayType").ToString & "") 'Column C

                'Nilay (11 Apr 2017) -- Start
                'Enhancements: New statutory report iTax Form C for CCK
                'strBuilder.Append("," & dtRow.Item("DisabilityMembershipNo").ToString & "") 'Column C
                strBuilder.Append("," & dtRow.Item("PayType").ToString & "") 'Column C
                'Nilay (11 Apr 2017) -- End

                'Sohail (03 Apr 2017) -- End

                'Nilay (17 Feb 2017) -- Start
                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
                'strBuilder.Append(",Primary Employee") 'Column D
                'Gajanan [1-July-2020] -- Start
                'If CInt(dtRow.Item("paytypeunkid")) = mintResidentPayTypeID Then
                '    strBuilder.Append(",Primary Employee") 'Column D
                'ElseIf CInt(dtRow.Item("paytypeunkid")) = mintNonResidentPayTypeID Then
                '    strBuilder.Append(",Secondary Employee") 'Column D
                'Else
                '    strBuilder.Append(",") 'Column D
                'End If
                If CDec(dtRow.Item("column_x")) > 0 Then
                    strBuilder.Append(",Primary Employee") 'Column D
                ElseIf CDec(dtRow.Item("column_x")) <= 0 Then
                    strBuilder.Append(",Secondary Employee") 'Column D
                Else
                    strBuilder.Append(",") 'Column D
                End If
                'Gajanan [1-July-2020] -- End
                'Nilay (17 Feb 2017) -- End

                'Nilay (11 Apr 2017) -- Start
                'Enhancements: New statutory report iTax Form C for CCK
                If menReport = enArutiReport.ITax_Form_C_Report Then
                    strBuilder.Append("," & dtRow.Item("DisabilityMembershipNo").ToString & "") 'Column E
                End If
                'NOTE: When iTax C report exported then remaining following columns will be start from F and continue
                'Nilay (11 Apr 2017) -- End
                strBuilder.Append("," & Format(dtRow.Item("basicpay"), GUI.fmtCurrency).Replace(",", "") & "") 'Column E
                strBuilder.Append("," & Format(dtRow.Item("column_f"), GUI.fmtCurrency).Replace(",", "") & "") 'Column F
                strBuilder.Append("," & Format(dtRow.Item("column_g"), GUI.fmtCurrency).Replace(",", "") & "") 'Column G
                strBuilder.Append("," & Format(dtRow.Item("column_h"), GUI.fmtCurrency).Replace(",", "") & "") 'Column H
                strBuilder.Append("," & Format(dtRow.Item("column_i"), GUI.fmtCurrency).Replace(",", "") & "") 'Column I
                strBuilder.Append("," & Format(dtRow.Item("column_j"), GUI.fmtCurrency).Replace(",", "") & "") 'Column J
                strBuilder.Append("," & Format(dtRow.Item("column_k"), GUI.fmtCurrency).Replace(",", "") & "") 'Column K
                strBuilder.Append("," & Format(dtRow.Item("grosspay") - dtRow.Item("basicpay") - dtRow.Item("column_f") - dtRow.Item("column_G") - dtRow.Item("column_h") - dtRow.Item("column_i") - dtRow.Item("column_j") - dtRow.Item("column_k"), GUI.fmtCurrency).Replace(",", "") & "") 'Column L
                strBuilder.Append(",") 'Column M
                'Sohail (11 Sep 2015) -- Start
                'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
                'strBuilder.Append(",") 'Column N
                'Sohail (02 Feb 2018) -- Start
                'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
                'strBuilder.Append(",0") 'Column N
                strBuilder.Append("," & Format(dtRow.Item("column_n"), GUI.fmtCurrency).Replace(",", "") & "") 'Column N
                'Sohail (02 Feb 2018) -- End
                'Sohail (11 Sep 2015) -- End
                strBuilder.Append("," & Format(dtRow.Item("noncashbenefit"), GUI.fmtCurrency).Replace(",", "") & "") 'Column O
                strBuilder.Append(",") 'Column P
                'Sohail (08 Mar 2018) -- Start
                'CCK Enhancement - Ref. # 177 : If S is having value then Q column should be blank on I Tax Form B and C report in 70.1.
                'strBuilder.Append(",0") 'Column Q
                If CDec(dtRow.Item("column_s")) <> 0 Then
                    strBuilder.Append(",") 'Column Q
                Else
                    strBuilder.Append(",0") 'Column Q
                End If
                'Sohail (08 Mar 2018) -- End
                'Nilay (17 Feb 2017) -- Start
                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
                'strBuilder.Append(",Benefit not given") 'Column R
                'strBuilder.Append(",") 'Column S
                'strBuilder.Append(",") 'Column T
                strBuilder.Append("," & dtRow.Item("paypointname").ToString & "") 'Column R

                'Sohail (08 Mar 2018) -- Start
                'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
                'strBuilder.Append("," & Format(dtRow.Item("column_s"), GUI.fmtCurrency).Replace(",", "") & "") 'Column S
                If CDec(dtRow.Item("column_s")) <> 0 Then
                    strBuilder.Append("," & Format(dtRow.Item("column_s"), GUI.fmtCurrency).Replace(",", "") & "") 'Column S
                Else
                    strBuilder.Append(",") 'Column S
                End If
                'Sohail (08 Mar 2018) -- End

                'Sohail (08 Mar 2018) -- Start
                'CCK Enhancement - Ref. # 177 : In Column T, it should be null on I Tax Form B and C report in 70.1.
                'If CInt(dtRow.Item("paypointunkid")) = mintEmployerOwnedHousePayPointID Then
                '    strBuilder.Append("," & Format(dtRow.Item("column_ownedhouse"), GUI.fmtCurrency).Replace(",", "") & "") 'Column T
                'ElseIf CInt(dtRow.Item("paypointunkid")) = mintEmployerRentedHousePayPointID Then
                '    strBuilder.Append("," & Format(dtRow.Item("column_rentedhouse"), GUI.fmtCurrency).Replace(",", "") & "") 'Column T
                'Else
                '    strBuilder.Append(",") 'Column T
                'End If
                strBuilder.Append(",") 'Column T
                'Sohail (08 Mar 2018) -- End


                'Nilay (17 Feb 2017) -- End
                'Sohail (08 Mar 2018) -- Start
                'CCK Enhancement - Ref. # 177 : U will be dependent on S, if S has a value, U should be 0, else if S is blank, corresponding U should be blank as well on I Tax Form B and C report in 70.1.
                'strBuilder.Append(",") 'Column U
                If CDec(dtRow.Item("column_s")) <> 0 Then
                    strBuilder.Append(",0") 'Column U
                Else
                    strBuilder.Append(",") 'Column U
                End If
                'Sohail (08 Mar 2018) -- End
                strBuilder.Append(",") 'Column V
                strBuilder.Append(",") 'Column W
                strBuilder.Append(",") 'Column X
                'Sohail (11 Sep 2015) -- Start
                'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
                'strBuilder.Append(",") 'Column Y
                strBuilder.Append("," & Format(dtRow.Item("column_y"), GUI.fmtCurrency).Replace(",", "") & "") 'Column Y
                'Sohail (11 Sep 2015) -- End
                strBuilder.Append(",") 'Column Z
                'Sohail (11 Sep 2015) -- Start
                'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
                'strBuilder.Append(",") 'Column AA
                'strBuilder.Append(",") 'Column AB
                'Sohail (02 Feb 2018) -- Start
                'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
                'strBuilder.Append(",0") 'Column AA
                strBuilder.Append("," & Format(dtRow.Item("column_aa"), GUI.fmtCurrency).Replace(",", "") & "") 'Column AA
                'Sohail (02 Feb 2018) -- End

                'Nilay (17 Feb 2017) -- Start
                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
                'strBuilder.Append(",0") 'Column AB
                'Sohail (10 Feb 2021) -- Start
                'FFK Enhancement : OLD-302 : Column AB to be blank if the amount is zero.
                'strBuilder.Append("," & Format(dtRow.Item("column_ab"), GUI.fmtCurrency).Replace(",", "") & "") 'Column AB
                If CDec(dtRow.Item("column_ab")) = 0 Then
                    strBuilder.Append(",") 'Column AB
                Else
                strBuilder.Append("," & Format(dtRow.Item("column_ab"), GUI.fmtCurrency).Replace(",", "") & "") 'Column AB
                End If
                'Sohail (10 Feb 2021) -- End
                'Nilay (17 Feb 2017) -- End

                'Sohail (11 Sep 2015) -- End
                strBuilder.Append(",") 'Column AC
                strBuilder.Append(",") 'Column AD
                strBuilder.Append(",") 'Column AE
                'Sohail (08 Mar 2018) -- Start
                'CCK Enhancement - Ref. # 177 : On itax form C, add another blank column before column AG (before AF as Form C has one Extra column E) on I Tax Form B and C report in 70.1.
                If menReport = enArutiReport.ITax_Form_C_Report Then
                    strBuilder.Append(",")
                End If
                'Sohail (08 Mar 2018) -- End
                'Sohail (11 Sep 2015) -- Start
                'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
                'strBuilder.Append(",") 'Column AF
                strBuilder.Append("," & Format(dtRow.Item("column_af"), GUI.fmtCurrency).Replace(",", "") & "") 'Column AF
                'Sohail (11 Sep 2015) -- End
                'Sohail (02 Feb 2018) -- Start
                'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
                'strBuilder.Append(",0") 'Column AG
                strBuilder.Append("," & Format(dtRow.Item("column_ag"), GUI.fmtCurrency).Replace(",", "") & "") 'Column AG
                'Sohail (02 Feb 2018) -- End
                strBuilder.Append(",") 'Column AH
                'Sohail (08 Mar 2018) -- Start
                'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
                'strBuilder.Append("," & Format(dtRow.Item("taxdue"), GUI.fmtCurrency).Replace(",", "") & "") 'Column AI
                strBuilder.Append("," & Format(dtRow.Item("column_ai"), GUI.fmtCurrency).Replace(",", "") & "") 'Column AI
                'Sohail (08 Mar 2018) -- End

                strBuilder.Append(vbCrLf)
            Next


            If SaveTextFile(strExportPath, strBuilder) Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_PayrollReport; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function SaveTextFile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try

            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    Private Function IgnoreZeroHead(ByVal objDataReader As DataTable) As DataTable
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Dim intColCount As Integer = objDataReader.Columns.Count
        Dim intRowCount As Integer = objDataReader.Rows.Count
        Try

            Dim intZeroColumn As Integer = 0
            Dim arrZeroColIndx As New ArrayList

            'Report Column Caption

            Dim decCols() As String = (From p In objDataReader.Columns.Cast(Of DataColumn)() Where (p.DataType Is Type.GetType("System.Decimal")) Select (p.ColumnName.ToString & " = 0 AND ")).ToArray
            If decCols.Length > 0 Then
                Dim s As String = String.Join("", decCols)
                Dim r() As DataRow = objDataReader.Select(s.Substring(0, s.Length - 4))
                For Each dr As DataRow In r
                    objDataReader.Rows.Remove(dr)
                Next
            End If

            Return objDataReader

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IgnoreZeroHead", mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code")
            Language.setMessage(mstrModuleName, 2, "Employee Name")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
