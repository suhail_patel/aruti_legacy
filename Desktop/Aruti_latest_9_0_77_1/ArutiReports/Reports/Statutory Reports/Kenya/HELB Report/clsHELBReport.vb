'************************************************************************************************************************************
'Class Name : clsHELBReport.vb
'Purpose    :
'Date       :19/08/2013
'Written By : Pinkal Jariwala.
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala.
''' </summary>
Public Class clsHELBReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsHELBReport"
    Private mstrReportId As String = enArutiReport.HELB_REPORT
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty
    Private mintNonPayrollMembershipId As Integer = -1
    Private mstrNonPayrollMembershipName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mbIncludeInactiveEmp As Boolean = True
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0
    Private mstrAdvance_Filter As String = ""
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    Private mstrAddress As String
    'Sohail (10 Mar 2016) -- Start
    'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
    Private mintModeID As Integer = 0
    Private mstrModeName As String = ""
    Private mintLoanID As Integer = 0
    Private mstrLoanName As String = ""
    'Sohail (10 Mar 2016) -- End

    'Nilay (18-Oct-2016) -- Start
    'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
    Private mintSavingSchemeID As Integer = 0
    'Nilay (18-Oct-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _NonPayrollMembershipId() As Integer
        Set(ByVal value As Integer)
            mintNonPayrollMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _NonPayrollMembershipName() As String
        Set(ByVal value As String)
            mstrNonPayrollMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mbIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    'Sohail (10 Mar 2016) -- Start
    'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
    Public WriteOnly Property _ModeID() As Integer
        Set(ByVal value As Integer)
            mintModeID = value
        End Set
    End Property

    Public WriteOnly Property _ModeName() As String
        Set(ByVal value As String)
            mstrModeName = value
        End Set
    End Property

    Public WriteOnly Property _LoanID() As Integer
        Set(ByVal value As Integer)
            mintLoanID = value
        End Set
    End Property

    Public WriteOnly Property _LoanName() As String
        Set(ByVal value As String)
            mstrLoanName = value
        End Set
    End Property
    'Sohail (10 Mar 2016) -- End

    'Nilay (18-Oct-2016) -- Start
    'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
    Public WriteOnly Property _SavingSchemeID() As Integer
        Set(ByVal value As Integer)
            mintSavingSchemeID = value
        End Set
    End Property
    'Nilay (18-Oct-2016) -- End

    'Sohail (03 Aug 2021) -- Start
    'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
    Private mstrSavingSchemeName As String = String.Empty
    Public WriteOnly Property _SavingSchemeName() As String
        Set(ByVal value As String)
            mstrSavingSchemeName = value
        End Set
    End Property

    Private mintDeductionID As Integer = 0
    Public WriteOnly Property _DeductionID() As Integer
        Set(ByVal value As Integer)
            mintDeductionID = value
        End Set
    End Property

    Private mstrDeductionName As String = String.Empty
    Public WriteOnly Property _DeductionName() As String
        Set(ByVal value As String)
            mstrDeductionName = value
        End Set
    End Property
    'Sohail (03 Aug 2021) -- End

#End Region

    'Sohail (10 Mar 2016) -- Start
    'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
#Region " Enum "
    Public Enum enMode
        MEMBERSHIP = 1
        LOAN = 2
        SAVING = 3 'Nilay (18-Oct-2016)
        Deduction = 4 'Sohail (03 Aug 2021)
    End Enum
#End Region
    'Sohail (10 Mar 2016) -- End

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintMembershipId = 0
            mstrMembershipName = ""
            mintNonPayrollMembershipId = 0
            mstrNonPayrollMembershipName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mbIncludeInactiveEmp = True
            mintReportId = -1
            mstrReportTypeName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""
            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""
            'Sohail (10 Mar 2016) -- Start
            'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
            mintModeID = enMode.MEMBERSHIP
            mstrModeName = ""
            mintLoanID = 0
            mstrLoanName = ""
            'Sohail (10 Mar 2016) -- End

            'Nilay (18-Oct-2016) -- Start
            'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
            mintSavingSchemeID = 0
            'Nilay (18-Oct-2016) -- End
            'Sohail (03 Aug 2021) -- Start
            'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
            mstrSavingSchemeName = ""
            mintDeductionID = 0
            mstrDeductionName = ""
            'Sohail (03 Aug 2021) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    objRpt = Generate_DetailReport()


        '    If Not IsNothing(objRpt) Then
        '        Dim intArrayColumnWidth As Integer() = {60, 250, 110, 110, 110, 110}
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim row As ExcelWriter.WorksheetRow
        '        Dim wcell As WorksheetCell
        '        If mdtTableExcel IsNot Nothing Then

        '            If Company._Object._Countryunkid = 112 Then 'Kenya
        '                mstrReportTypeName = Me._ReportName

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "HELB MONTHLY REMITTANCE SCHEDULE"), "s8bc")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8")
        '                wcell.MergeAcross = 1
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "(TO BE DULY COMPLETED WITH EVERY CHEQUE/DEPOSIT SLIP/EFT SUBMITTED TO HELB)"), "s8bc")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8")
        '                wcell.MergeAcross = 1
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "*Please email the remittance form to: remittance@helb.co.ke"), "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8")
        '                wcell.MergeAcross = 1
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "*Deadline for remittance is 15th of the following month, late remittance will attract 5% penalty per month outstanding."), "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8")
        '                wcell.MergeAcross = 1
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "*No payment shall be accepted without remittance schedule"), "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8")
        '                wcell.MergeAcross = 1
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "EMPLOYER INFO"), "s8vc")
        '                wcell.MergeDown = 7
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "HELB EMPLOYER CODE NUMBER"), "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Company._Object._Reg2_Value, "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "NAME OF EMPLOYER"), "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Company._Object._Name, "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "POSTAL ADDRESS AND CODE"), "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Company._Object._Address2 & " " & Company._Object._Post_Code_No, "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "PHYSICAL ADDRESS"), "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(mstrAddress, "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "E-MAIL ADDRESS"), "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Company._Object._Email, "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "TELEPHONE NUMBER"), "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Company._Object._Phone1, "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "MOBILE NUMBER"), "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Company._Object._Phone2, "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------


        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "PAYMENT IN FOR"), "s8vc")
        '                wcell.MergeDown = 3
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "PAYROLL MONTH"), "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(mstrPeriodName, "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "CHEQUE NUMBER/TYPE OF PAYMENT"), "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "EFT"), "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "DATE OF PAYMENT/EFT/DEPOSIT"), "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "HELB RECEIVING BANK"), "s8")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8")
        '                wcell.MergeAcross = 2
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8")
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "EMPLOYEES DETAILS"), "s8")
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '                row.Cells.Add(wcell)

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '            End If

        '        End If

        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, False, True, True, Nothing, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, Nothing)

        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            'Sohail (10 Mar 2016) -- Start
            'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
            If mintModeID = enMode.LOAN Then
                objRpt = Generate_DetailReportForLoan(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
                'Nilay (18-Oct-2016) -- Start
                'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
            ElseIf mintModeID = enMode.SAVING Then
                objRpt = Generate_DetailReportForSaving(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                        xBaseCurrencyId)
                'Nilay (18-Oct-2016) -- End

                'Sohail (03 Aug 2021) -- Start
                'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
            ElseIf mintModeID = enMode.Deduction Then
                objRpt = Generate_DetailReportForDeduction(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
                'Sohail (03 Aug 2021) -- End

            Else
                objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
            End If
            'Sohail (10 Mar 2016) -- End

            If Not IsNothing(objRpt) Then
                Dim intArrayColumnWidth As Integer() = {60, 250, 110, 110, 110, 110}
                Dim rowsArrayHeader As New ArrayList
                Dim row As ExcelWriter.WorksheetRow
                Dim wcell As WorksheetCell
                    Dim objCompany As New clsCompany_Master
                    objCompany._Companyunkid = xCompanyUnkid
                'S.SANDEEP |18-JUN-2021| -- START
                'ISSUE/ENHANCEMENT : Kenya Reports Changes (Statutory)
                If menExportAction <> enExportAction.ExcelCSV Then
                    If mdtTableExcel IsNot Nothing Then

                    If objCompany._Countryunkid = 112 Then 'Kenya
                        mstrReportTypeName = Me._ReportName

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "HELB MONTHLY REMITTANCE SCHEDULE"), "s8bc")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "(TO BE DULY COMPLETED WITH EVERY CHEQUE/DEPOSIT SLIP/EFT SUBMITTED TO HELB)"), "s8bc")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "*Please email the remittance form to: remittance@helb.co.ke"), "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "*Deadline for remittance is 15th of the following month, late remittance will attract 5% penalty per month outstanding."), "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "*No payment shall be accepted without remittance schedule"), "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "EMPLOYER INFO"), "s8vc")
                        wcell.MergeDown = 7
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "HELB EMPLOYER CODE NUMBER"), "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Reg2_Value, "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "NAME OF EMPLOYER"), "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Name, "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "POSTAL ADDRESS AND CODE"), "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Address2 & " " & Company._Object._Post_Code_No, "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "PHYSICAL ADDRESS"), "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(mstrAddress, "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "E-MAIL ADDRESS"), "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Email, "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "TELEPHONE NUMBER"), "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Phone1, "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "MOBILE NUMBER"), "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Phone2, "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------


                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "PAYMENT IN FOR"), "s8vc")
                        wcell.MergeDown = 3
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "PAYROLL MONTH"), "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(mstrPeriodName, "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "CHEQUE NUMBER/TYPE OF PAYMENT"), "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "EFT"), "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "DATE OF PAYMENT/EFT/DEPOSIT"), "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "HELB RECEIVING BANK"), "s8")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8")
                        wcell.MergeAcross = 2
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8")
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "EMPLOYEES DETAILS"), "s8")
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        row.Cells.Add(wcell)

                        rowsArrayHeader.Add(row)
                        '--------------------

                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    objCompany = Nothing
                    'S.SANDEEP [04 JUN 2015] -- END

                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, False, True, True, Nothing, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, Nothing)

                Else
                    If mdtTableExcel IsNot Nothing Then
                        Dim xTable As DataTable = mdtTableExcel.Copy
                        Dim strBuilder As New System.Text.StringBuilder
                        xTable.Columns.RemoveAt(0)
                        For Each iCol As DataColumn In xTable.Columns
                            xTable.Columns(iCol.Ordinal).ColumnName = iCol.Caption.Trim().Replace(" ", "_")
                        Next
                        xTable.Columns(Language.getMessage(mstrModuleName, 6, "ID NUMBER").Replace(" ", "_")).SetOrdinal(0)
                        strBuilder.Append(String.Join(",", xTable.Columns.Cast(Of DataColumn).AsEnumerable().Select(Function(x) x.ColumnName).ToArray()))
                        strBuilder.Append(vbCrLf)
                        For Each iRow As DataRow In xTable.Rows
                            strBuilder.Append(iRow.Item(Language.getMessage(mstrModuleName, 6, "ID NUMBER").Replace(" ", "_")).ToString)
                            strBuilder.Append("," & iRow.Item(Language.getMessage(mstrModuleName, 5, "NAMES").Replace(" ", "_")).ToString)
                            strBuilder.Append("," & iRow.Item(Language.getMessage(mstrModuleName, 7, "STAFF NUMBER").Replace(" ", "_")).ToString)
                            strBuilder.Append("," & CStr(iRow.Item(Language.getMessage(mstrModuleName, 8, "AMOUNT").Replace(" ", "_"))).Replace(",", ""))
                            strBuilder.Append(vbCrLf)
                        Next
                        If Me._ReportName.Contains("/") = True Then
                            Me._ReportName = Me._ReportName.Replace("/", "_")
                        End If

                        If Me._ReportName.Contains("\") Then
                            Me._ReportName = Me._ReportName.Replace("\", "_")
                        End If

                        If Me._ReportName.Contains(":") Then
                            Me._ReportName = Me._ReportName.Replace(":", "_")
                        End If
                        Dim strExportFileName As String = ""
                        strExportFileName = Me._ReportName.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
                        xExportReportPath = xExportReportPath & "\" & strExportFileName & ".csv"
                        Dim fsFile As New IO.FileStream(xExportReportPath, IO.FileMode.Create, IO.FileAccess.Write)
                        Dim strWriter As New IO.StreamWriter(fsFile)
                        With strWriter
                            .BaseStream.Seek(0, IO.SeekOrigin.End)
                            .Write(strBuilder)
                            .Close()
                        End With
                    End If
                    If IO.File.Exists(xExportReportPath) Then
                        If xOpenReportAfterExport Then
                            Process.Start(xExportReportPath)
                        End If
                    End If
                End If
                'S.SANDEEP |18-JUN-2021| -- END
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Sohail (10 Mar 2016) -- Start
    'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblAddSelect = True Then
                strQ = "SELECT 0 AS id, ' ' +  @name AS name UNION "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "Select"))
            End If

            'Nilay (18-Oct-2016) -- Start
            'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
            'strQ &= "SELECT 1 AS id, @Membership AS name " & _
            '       "UNION SELECT 2 AS id, @Loan AS name "
            strQ &= "SELECT " & enMode.MEMBERSHIP & " AS id, @Membership AS name " & _
                    "UNION SELECT " & enMode.LOAN & " AS id, @Loan AS name " & _
                    "UNION SELECT " & enMode.SAVING & " AS id, @Saving AS name " & _
                    "UNION SELECT " & enMode.Deduction & " AS id, @Deduction AS name "
            'Sohail (03 Aug 2021) - [Deduction]
            ''Nilay (18-Oct-2016) -- End

            objDataOperation.AddParameter("@Membership", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 28, "Membership"))
            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 29, "Loan"))
            'Nilay (18-Oct-2016) -- Start
            'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
            objDataOperation.AddParameter("@Saving", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 30, "Saving"))
            'Nilay (18-Oct-2016) -- End
            'Sohail (03 Aug 2021) -- Start
            'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
            objDataOperation.AddParameter("@Deduction", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "Deduction"))
            'Sohail (03 Aug 2021) -- End

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function
    'Sohail (10 Mar 2016) -- End

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                             ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", ISNULL(CAmount,0) AS employercontribution " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)+ISNULL(CAmount,0) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                             ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "       LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                 ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                           "FROM    prpayrollprocess_tran " & _
                                                   "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                   "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                   "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                   "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                   "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                   WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                           "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                           "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                           "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "           ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                        "WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                        "AND hrmembership_master.membershipunkid = @MemId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If


                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                If mintNonPayrollMembershipId > 0 Then

                    StrQ &= "UNION ALL " & _
                                "SELECT hremployee_master.employeecode AS employeecode " & _
                                     ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                                      ",'' AS MemshipNo " & _
                                      ", 0 AS empcontribution " & _
                                      ", 0 AS employercontribution " & _
                                      ", 0 AS Amount " & _
                                      ", '' AS PeriodName " & _
                                      ", hremployee_master.employeeunkid AS EmpId " & _
                                      ", '' AS MemshipCategory " & _
                                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                                      ", '' AS Remarks " & _
                                      ", 1 AS NonPayrollMembership  " & _
                                      "FROM    hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "

                    StrQ &= mstrAnalysis_Join

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrQ &= UserAccessLevel._AccessLevelFilterString
                    'End If

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If

                    'S.SANDEEP [04 JUN 2015] -- END

                End If

                StrQ &= "ORDER BY employeecode "

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  ISNULL(CAmount,0) AS employercontribution " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) +  ISNULL(CAmount,0)) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                              ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                               "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                "AND prpayment_tran.countryunkid = " & mintCountryId & " "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "   LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                       "FROM    prpayrollprocess_tran " & _
                                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "               WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                               "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                               "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                               "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "              ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                    "AND hrmembership_master.membershipunkid = @MemId " & _
                                    "AND prpayment_tran.periodunkid = @PeriodId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAmount,0) AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)+  ISNULL(CAmount,0)) AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                             "FROM    prpayrollprocess_tran " & _
                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                     "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                     "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                     "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                     "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                     "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                            "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                            "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                            "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "            ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                         "WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                         "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                         "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                         "AND hrmembership_master.membershipunkid = @MemId " & _
                                         "AND prpayment_tran.periodunkid = @PeriodId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAmount,0) AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) +   ISNULL(CAmount,0))  AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                        "FROM    prpayrollprocess_tran " & _
                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "LEFT JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                    "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                                    "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "              ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                          " WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                          " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                          " AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                          " AND hrmembership_master.membershipunkid = @MemId " & _
                                          " AND prpayment_tran.periodunkid IS NULL "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                If mintNonPayrollMembershipId > 0 Then

                    StrQ &= "UNION ALL " & _
                                "SELECT hremployee_master.employeecode AS employeecode " & _
                                     ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                                      ",'' AS MemshipNo " & _
                                      ", 0 AS empcontribution " & _
                                      ", 0 AS employercontribution " & _
                                      ", 0 AS Amount " & _
                                      ", '' AS PeriodName " & _
                                      ", hremployee_master.employeeunkid AS EmpId " & _
                                      ", '' AS MemshipCategory " & _
                                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                                      ", '' AS Remarks " & _
                                      ", 1 AS NonPayrollMembership  " & _
                                      "FROM    hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "

                    StrQ &= mstrAnalysis_Join

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrQ &= UserAccessLevel._AccessLevelFilterString
                    'End If

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If

                    'S.SANDEEP [04 JUN 2015] -- END

                End If

                StrQ &= "ORDER BY employeecode "

            End If

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdtData As DataTable = New DataView(dsList.Tables(0), "", "employeecode, Amount DESC", DataViewRowState.CurrentRows).ToTable

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim iCnt As Integer = 0
            Dim StrECode As String = String.Empty
            Dim StrPrevECode As String = String.Empty
            Dim rpt_Row As DataRow = Nothing
            Dim decColumn81Total As Decimal


            For Each dtRow As DataRow In mdtData.Rows
                StrECode = dtRow.Item("employeecode").ToString.Trim

                If StrPrevECode <> StrECode Then
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                    If CBool(dtRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                        rpt_Row.Item("Column5") = dtRow.Item("OtherMembership")
                    Else
                        rpt_Row.Item("Column5") = ""
                    End If

                    iCnt += 1
                    rpt_Row.Item("Column6") = iCnt

                    rpt_Row.Item("Column11") = Language.getMessage(mstrModuleName, 1, "EMPLOYER INFO")
                    rpt_Row.Item("Column12") = Language.getMessage(mstrModuleName, 2, "PAYMENT IN FOR")
                Else
                    rpt_Row = rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)
                    rpt_Row.Item("Column5") = dtRow.Item("OtherMembership")
                    rpt_Row.AcceptChanges()

                    StrPrevECode = StrECode

                    Continue For
                End If

                rpt_Row.Item("Column1") = dtRow.Item("employeecode").ToString
                rpt_Row.Item("Column2") = dtRow.Item("EName")

                If CBool(dtRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                    rpt_Row.Item("Column3") = ""
                Else
                    rpt_Row.Item("Column3") = dtRow.Item("MemshipNo")
                End If

                rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)


                decColumn81Total = decColumn81Total + CDec(dtRow.Item("Amount"))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                StrPrevECode = StrECode
            Next

            mstrAddress = Company._Object._Address1
            mstrAddress &= " " & Company._Object._City_Name & " " & Company._Object._State_Name

            objRpt = New ArutiReport.Designer.rptKenyaHELB_Report
            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblEmployerInfo", Language.getMessage(mstrModuleName, 1, "EMPLOYER INFO"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentInFor", Language.getMessage(mstrModuleName, 31, "PAYMENT INFOR")) 'Nilay (18-Oct-2016)
            Call ReportFunction.TextChange(objRpt, "txtEmpDetails", Language.getMessage(mstrModuleName, 3, "EMPLOYEES DETAILS"))
            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 4, "S/No."))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 5, "NAMES"))
            Call ReportFunction.TextChange(objRpt, "txtIDNo", Language.getMessage(mstrModuleName, 6, "ID NUMBER"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 7, "STAFF NUMBER"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 8, "AMOUNT"))

            Call ReportFunction.TextChange(objRpt, "txtTotalAmount", Format(decColumn81Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 9, "TOTAL"))

            Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 10, "HELB MONTHLY REMITTANCE SCHEDULE"))
            Call ReportFunction.TextChange(objRpt, "txtSubmitted", Language.getMessage(mstrModuleName, 11, "(TO BE DULY COMPLETED WITH EVERY CHEQUE/DEPOSIT SLIP/EFT SUBMITTED TO HELB)"))
            Call ReportFunction.TextChange(objRpt, "txtEmailTo", Language.getMessage(mstrModuleName, 12, "*Please email the remittance form to: remittance@helb.co.ke "))
            Call ReportFunction.TextChange(objRpt, "txtDeadline", Language.getMessage(mstrModuleName, 13, "*Deadline for remittance is 15th of the following month, late remittance will attract 5% penalty per month outstanding."))
            Call ReportFunction.TextChange(objRpt, "txtPaymentAccepted", Language.getMessage(mstrModuleName, 14, "*No payment shall be accepted without remittance schedule"))
            Call ReportFunction.TextChange(objRpt, "lblEmpCodeNo", Language.getMessage(mstrModuleName, 15, "HELB EMPLOYER CODE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 16, "NAME OF EMPLOYER"))
            Call ReportFunction.TextChange(objRpt, "lblPostalCode", Language.getMessage(mstrModuleName, 17, "POSTAL ADDRESS AND CODE"))
            Call ReportFunction.TextChange(objRpt, "lblPhysicalAddress", Language.getMessage(mstrModuleName, 18, "PHYSICAL ADDRESS"))
            Call ReportFunction.TextChange(objRpt, "lblEmail", Language.getMessage(mstrModuleName, 19, "E-MAIL ADDRESS"))
            Call ReportFunction.TextChange(objRpt, "lblPhoneNo", Language.getMessage(mstrModuleName, 20, "TELEPHONE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblMobileNo", Language.getMessage(mstrModuleName, 21, "MOBILE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 22, "PAYROLL MONTH"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentType", Language.getMessage(mstrModuleName, 23, "CHEQUE NUMBER/TYPE OF PAYMENT"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentDate", Language.getMessage(mstrModuleName, 24, "DATE OF PAYMENT/EFT/DEPOSIT"))
            Call ReportFunction.TextChange(objRpt, "lblBank", Language.getMessage(mstrModuleName, 25, "HELB RECEIVING BANK"))

            Call ReportFunction.TextChange(objRpt, "txtEmpCodeNo", Company._Object._Reg2_Value)
            Call ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt, "txtPostalCode", Company._Object._Address2 & " " & Company._Object._Post_Code_No)
            Call ReportFunction.TextChange(objRpt, "txtPhysicalAddress", mstrAddress)
            Call ReportFunction.TextChange(objRpt, "txtEmail", Company._Object._Email)
            Call ReportFunction.TextChange(objRpt, "txtPhoneNo", Company._Object._Phone1)
            Call ReportFunction.TextChange(objRpt, "txtMobileNo", Company._Object._Phone2)
            Call ReportFunction.TextChange(objRpt, "txtMonth", mstrPeriodName)
            Call ReportFunction.TextChange(objRpt, "txtPaymentType", Language.getMessage(mstrModuleName, 26, "EFT"))
            Call ReportFunction.TextChange(objRpt, "txtPaymentDate", "")
            Call ReportFunction.TextChange(objRpt, "txtBank", "")





            Dim intColIndex As Integer = -1
            If menExportAction = enExportAction.ExcelExtra Or menExportAction = enExportAction.ExcelCSV Then
                mdtTableExcel = rpt_Data.Tables(0)

                mdtTableExcel.Columns("Column6").Caption = Language.getMessage(mstrModuleName, 4, "S/No.")
                intColIndex += 1
                mdtTableExcel.Columns("Column6").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 5, "NAMES")
                intColIndex += 1
                mdtTableExcel.Columns("Column2").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 6, "ID NUMBER")
                intColIndex += 1
                mdtTableExcel.Columns("Column3").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 7, "STAFF NUMBER")
                intColIndex += 1
                mdtTableExcel.Columns("Column1").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 8, "AMOUNT")
                intColIndex += 1
                mdtTableExcel.Columns("Column81").SetOrdinal(intColIndex)


                For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
                Next

            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Sohail (10 Mar 2016) -- Start
    'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
    Private Function Generate_DetailReportForLoan(ByVal strDatabaseName As String, _
                                                  ByVal intUserUnkid As Integer, _
                                                  ByVal intYearUnkid As Integer, _
                                                  ByVal intCompanyUnkid As Integer, _
                                                  ByVal dtPeriodEnd As Date, _
                                                  ByVal strUserModeSetting As String, _
                                                  ByVal blnOnlyApproved As Boolean, _
                                                  ByVal intBaseCurrencyId As Integer _
                                                  ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                             ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", 0 AS employercontribution " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                             ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                               "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                'StrQ &= "       LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                '                                 ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                '                           "FROM    prpayrollprocess_tran " & _
                '                                   "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                   "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                '                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                '                                   "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                '                                   "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '                                   "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                'StrQ &= mstrAnalysis_Join

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'StrQ &= "                   WHERE prpayrollprocess_tran.isvoid = 0 " & _
                '                           "AND prtnaleave_tran.isvoid = 0 " & _
                '                           "AND lnloan_advance_tran.isvoid = 0 " & _
                '                           "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                '                           "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                '                           "AND lnloan_advance_tran.loanschemeunkid = @loanschemeunkid "

                'If xUACFiltrQry.Trim.Length > 0 Then
                '    StrQ &= " AND " & xUACFiltrQry
                'End If

                StrQ &= "          /* ) AS C ON C.EmpId = hremployee_master.employeeunkid*/ " & _
                                        "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND lnloan_advance_tran.isvoid = 0 " & _
                                        "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                        "AND lnloan_advance_tran.loanschemeunkid = @loanschemeunkid "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'If mintNonPayrollMembershipId > 0 Then

                '    StrQ &= "UNION ALL " & _
                '                "SELECT hremployee_master.employeecode AS employeecode " & _
                '                     ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                '                      ",'' AS MemshipNo " & _
                '                      ", 0 AS empcontribution " & _
                '                      ", 0 AS employercontribution " & _
                '                      ", 0 AS Amount " & _
                '                      ", '' AS PeriodName " & _
                '                      ", hremployee_master.employeeunkid AS EmpId " & _
                '                      ", '' AS MemshipCategory " & _
                '                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                '                      ", '' AS Remarks " & _
                '                      ", 1 AS NonPayrollMembership  " & _
                '                      "FROM    hremployee_master " & _
                '                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                '                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "

                '    StrQ &= mstrAnalysis_Join

                '    If xUACQry.Trim.Length > 0 Then
                '        StrQ &= xUACQry
                '    End If

                '    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                '    If mstrAdvance_Filter.Trim.Length > 0 Then
                '        StrQ &= " AND " & mstrAdvance_Filter
                '    End If

                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrQ &= " AND " & xUACFiltrQry
                '    End If

                'End If

                StrQ &= "ORDER BY employeecode "

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", 0 AS employercontribution " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                              ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                               "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                               "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                "AND prpayment_tran.countryunkid = " & mintCountryId & " "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                'StrQ &= "   LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                '                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                '                       "FROM    prpayrollprocess_tran " & _
                '                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                '                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                '                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                '                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                '                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                'StrQ &= mstrAnalysis_Join

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'StrQ &= "               WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                '                               "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                '                               "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                '                               "AND hrmembership_master.membershipunkid = @MemId "

                'If xUACFiltrQry.Trim.Length > 0 Then
                '    StrQ &= " AND " & xUACFiltrQry
                'End If

                StrQ &= "            /*  ) AS C ON C.EmpId = hremployee_master.employeeunkid*/ " & _
                                    "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND lnloan_advance_tran.isvoid = 0 " & _
                                    "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                    "AND prpayment_tran.periodunkid = @PeriodId " & _
                                    "AND lnloan_advance_tran.loanschemeunkid = @loanschemeunkid "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", 0 AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)) AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                             "FROM    prpayrollprocess_tran " & _
                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                                "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                'StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                '                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                '                            "FROM    prpayrollprocess_tran " & _
                '                                     "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                     "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                '                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                '                                     "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                '                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                '                                     "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '                                     "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                'StrQ &= mstrAnalysis_Join

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                '                            "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                '                            "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                '                            "AND hrmembership_master.membershipunkid = @MemId "

                'If xUACFiltrQry.Trim.Length > 0 Then
                '    StrQ &= " AND " & xUACFiltrQry
                'End If

                StrQ &= "          /*  ) AS C ON C.EmpId = hremployee_master.employeeunkid*/ " & _
                                        "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND lnloan_advance_tran.isvoid = 0 " & _
                                        "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                        "AND prpayment_tran.periodunkid = @PeriodId " & _
                                        "AND lnloan_advance_tran.loanschemeunkid = @loanschemeunkid "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", 0 AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)) AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                        "FROM    prpayrollprocess_tran " & _
                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                                "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "LEFT JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                'StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                '                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                '                            "FROM    prpayrollprocess_tran " & _
                '                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                    "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                '                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                '                                    "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                '                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                '                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '                                    "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                'StrQ &= mstrAnalysis_Join

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                '                                    "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                '                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                '                                    "AND hrmembership_master.membershipunkid = @MemId "

                'If xUACFiltrQry.Trim.Length > 0 Then
                '    StrQ &= " AND " & xUACFiltrQry
                'End If

                StrQ &= "             /* ) AS C ON C.EmpId = hremployee_master.employeeunkid*/ " & _
                                          "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                                            "AND prtnaleave_tran.isvoid = 0 " & _
                                            "AND lnloan_advance_tran.isvoid = 0 " & _
                                            "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                            "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                            "AND lnloan_advance_tran.loanschemeunkid = @loanschemeunkid " & _
                                            " AND prpayment_tran.periodunkid IS NULL "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'If mintNonPayrollMembershipId > 0 Then

                '    StrQ &= "UNION ALL " & _
                '                "SELECT hremployee_master.employeecode AS employeecode " & _
                '                     ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                '                      ",'' AS MemshipNo " & _
                '                      ", 0 AS empcontribution " & _
                '                      ", 0 AS employercontribution " & _
                '                      ", 0 AS Amount " & _
                '                      ", '' AS PeriodName " & _
                '                      ", hremployee_master.employeeunkid AS EmpId " & _
                '                      ", '' AS MemshipCategory " & _
                '                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                '                      ", '' AS Remarks " & _
                '                      ", 1 AS NonPayrollMembership  " & _
                '                      "FROM    hremployee_master " & _
                '                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                '                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "

                '    StrQ &= mstrAnalysis_Join

                '    If xUACQry.Trim.Length > 0 Then
                '        StrQ &= xUACQry
                '    End If

                '    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                '    If mstrAdvance_Filter.Trim.Length > 0 Then
                '        StrQ &= " AND " & mstrAdvance_Filter
                '    End If

                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrQ &= " AND " & xUACFiltrQry
                '    End If

                'End If

                StrQ &= "ORDER BY employeecode "

            End If

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNonPayrollMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanID)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdtData As DataTable = New DataView(dsList.Tables(0), "", "employeecode, Amount DESC", DataViewRowState.CurrentRows).ToTable

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim iCnt As Integer = 0
            Dim StrECode As String = String.Empty
            Dim StrPrevECode As String = String.Empty
            Dim rpt_Row As DataRow = Nothing
            Dim decColumn81Total As Decimal


            For Each dtRow As DataRow In mdtData.Rows
                StrECode = dtRow.Item("employeecode").ToString.Trim

                If StrPrevECode <> StrECode Then
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                    If CBool(dtRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                        rpt_Row.Item("Column5") = dtRow.Item("OtherMembership")
                    Else
                        rpt_Row.Item("Column5") = ""
                    End If

                    iCnt += 1
                    rpt_Row.Item("Column6") = iCnt

                    rpt_Row.Item("Column11") = Language.getMessage(mstrModuleName, 1, "EMPLOYER INFO")
                    rpt_Row.Item("Column12") = Language.getMessage(mstrModuleName, 2, "PAYMENT IN FOR")
                Else
                    rpt_Row = rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)
                    rpt_Row.Item("Column5") = dtRow.Item("OtherMembership")
                    rpt_Row.AcceptChanges()

                    StrPrevECode = StrECode

                    Continue For
                End If

                rpt_Row.Item("Column1") = dtRow.Item("employeecode").ToString
                rpt_Row.Item("Column2") = dtRow.Item("EName")

                If CBool(dtRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                    rpt_Row.Item("Column3") = ""
                Else
                    rpt_Row.Item("Column3") = dtRow.Item("MemshipNo")
                End If

                rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)


                decColumn81Total = decColumn81Total + CDec(dtRow.Item("Amount"))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                StrPrevECode = StrECode
            Next

            mstrAddress = Company._Object._Address1
            mstrAddress &= " " & Company._Object._City_Name & " " & Company._Object._State_Name

            objRpt = New ArutiReport.Designer.rptKenyaHELB_Report
            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblEmployerInfo", Language.getMessage(mstrModuleName, 1, "EMPLOYER INFO"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentInFor", Language.getMessage(mstrModuleName, 2, "PAYMENT IN FOR"))
            Call ReportFunction.TextChange(objRpt, "txtEmpDetails", Language.getMessage(mstrModuleName, 3, "EMPLOYEES DETAILS"))
            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 4, "S/No."))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 5, "NAMES"))
            Call ReportFunction.TextChange(objRpt, "txtIDNo", Language.getMessage(mstrModuleName, 6, "ID NUMBER"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 7, "STAFF NUMBER"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 8, "AMOUNT"))

            Call ReportFunction.TextChange(objRpt, "txtTotalAmount", Format(decColumn81Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 9, "TOTAL"))

            Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 10, "HELB MONTHLY REMITTANCE SCHEDULE"))
            Call ReportFunction.TextChange(objRpt, "txtSubmitted", Language.getMessage(mstrModuleName, 11, "(TO BE DULY COMPLETED WITH EVERY CHEQUE/DEPOSIT SLIP/EFT SUBMITTED TO HELB)"))
            Call ReportFunction.TextChange(objRpt, "txtEmailTo", Language.getMessage(mstrModuleName, 12, "*Please email the remittance form to: remittance@helb.co.ke "))
            Call ReportFunction.TextChange(objRpt, "txtDeadline", Language.getMessage(mstrModuleName, 13, "*Deadline for remittance is 15th of the following month, late remittance will attract 5% penalty per month outstanding."))
            Call ReportFunction.TextChange(objRpt, "txtPaymentAccepted", Language.getMessage(mstrModuleName, 14, "*No payment shall be accepted without remittance schedule"))
            Call ReportFunction.TextChange(objRpt, "lblEmpCodeNo", Language.getMessage(mstrModuleName, 15, "HELB EMPLOYER CODE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 16, "NAME OF EMPLOYER"))
            Call ReportFunction.TextChange(objRpt, "lblPostalCode", Language.getMessage(mstrModuleName, 17, "POSTAL ADDRESS AND CODE"))
            Call ReportFunction.TextChange(objRpt, "lblPhysicalAddress", Language.getMessage(mstrModuleName, 18, "PHYSICAL ADDRESS"))
            Call ReportFunction.TextChange(objRpt, "lblEmail", Language.getMessage(mstrModuleName, 19, "E-MAIL ADDRESS"))
            Call ReportFunction.TextChange(objRpt, "lblPhoneNo", Language.getMessage(mstrModuleName, 20, "TELEPHONE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblMobileNo", Language.getMessage(mstrModuleName, 21, "MOBILE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 22, "PAYROLL MONTH"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentType", Language.getMessage(mstrModuleName, 23, "CHEQUE NUMBER/TYPE OF PAYMENT"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentDate", Language.getMessage(mstrModuleName, 24, "DATE OF PAYMENT/EFT/DEPOSIT"))
            Call ReportFunction.TextChange(objRpt, "lblBank", Language.getMessage(mstrModuleName, 25, "HELB RECEIVING BANK"))

            Call ReportFunction.TextChange(objRpt, "txtEmpCodeNo", Company._Object._Reg2_Value)
            Call ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt, "txtPostalCode", Company._Object._Address2 & " " & Company._Object._Post_Code_No)
            Call ReportFunction.TextChange(objRpt, "txtPhysicalAddress", mstrAddress)
            Call ReportFunction.TextChange(objRpt, "txtEmail", Company._Object._Email)
            Call ReportFunction.TextChange(objRpt, "txtPhoneNo", Company._Object._Phone1)
            Call ReportFunction.TextChange(objRpt, "txtMobileNo", Company._Object._Phone2)
            Call ReportFunction.TextChange(objRpt, "txtMonth", mstrPeriodName)
            Call ReportFunction.TextChange(objRpt, "txtPaymentType", Language.getMessage(mstrModuleName, 26, "EFT"))
            Call ReportFunction.TextChange(objRpt, "txtPaymentDate", "")
            Call ReportFunction.TextChange(objRpt, "txtBank", "")





            Dim intColIndex As Integer = -1
            If menExportAction = enExportAction.ExcelExtra Or menExportAction = enExportAction.ExcelCSV Then
                mdtTableExcel = rpt_Data.Tables(0)

                mdtTableExcel.Columns("Column6").Caption = Language.getMessage(mstrModuleName, 4, "S/No.")
                intColIndex += 1
                mdtTableExcel.Columns("Column6").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 5, "NAMES")
                intColIndex += 1
                mdtTableExcel.Columns("Column2").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 6, "ID NUMBER")
                intColIndex += 1
                mdtTableExcel.Columns("Column3").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 7, "STAFF NUMBER")
                intColIndex += 1
                mdtTableExcel.Columns("Column1").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 8, "AMOUNT")
                intColIndex += 1
                mdtTableExcel.Columns("Column81").SetOrdinal(intColIndex)


                For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
                Next

            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReportForLoan; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sohail (10 Mar 2016) -- End

    'Nilay (18-Oct-2016) -- Start
    'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
    Private Function Generate_DetailReportForSaving(ByVal strDatabaseName As String, _
                                                    ByVal intUserUnkid As Integer, _
                                                    ByVal intYearUnkid As Integer, _
                                                    ByVal intCompanyUnkid As Integer, _
                                                    ByVal dtPeriodEnd As Date, _
                                                    ByVal strUserModeSetting As String, _
                                                    ByVal blnOnlyApproved As Boolean, _
                                                    ByVal intBaseCurrencyId As Integer _
                                                    ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                             ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", 0 AS employercontribution " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                             ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN svsaving_tran ON svsaving_tran.savingtranunkid = prpayrollprocess_tran.savingtranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If


                StrQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                            " AND prtnaleave_tran.isvoid = 0 " & _
                            " AND svsaving_tran.isvoid = 0 " & _
                            " AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                            " AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                            " AND svsaving_tran.savingschemeunkid = @savingschemeunkid "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "ORDER BY employeecode "

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", 0 AS employercontribution " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                              ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN svsaving_tran ON svsaving_tran.savingtranunkid = prpayrollprocess_tran.savingtranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                               "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                "AND prpayment_tran.countryunkid = " & mintCountryId & " "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                            " AND prtnaleave_tran.isvoid = 0 " & _
                            " AND svsaving_tran.isvoid = 0 " & _
                            " AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                            " AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                            " AND prpayment_tran.periodunkid = @PeriodId " & _
                            " AND svsaving_tran.savingschemeunkid = @savingschemeunkid "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", 0 AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)) AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                             "FROM    prpayrollprocess_tran " & _
                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN svsaving_tran ON svsaving_tran.savingtranunkid = prpayrollprocess_tran.savingtranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                            " AND prtnaleave_tran.isvoid = 0 " & _
                            " AND svsaving_tran.isvoid = 0 " & _
                            " AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                            " AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                            " AND prpayment_tran.periodunkid = @PeriodId " & _
                            " AND svsaving_tran.savingschemeunkid = @savingschemeunkid "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", 0 AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)) AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                        "FROM    prpayrollprocess_tran " & _
                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN svsaving_tran ON svsaving_tran.savingtranunkid = prpayrollprocess_tran.savingtranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "LEFT JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                            " AND prtnaleave_tran.isvoid = 0 " & _
                            " AND svsaving_tran.isvoid = 0 " & _
                            " AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                            " AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                            " AND svsaving_tran.savingschemeunkid = @savingschemeunkid " & _
                            " AND prpayment_tran.periodunkid IS NULL "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "ORDER BY employeecode "

            End If

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNonPayrollMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingSchemeID)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdtData As DataTable = New DataView(dsList.Tables(0), "", "employeecode, Amount DESC", DataViewRowState.CurrentRows).ToTable

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim iCnt As Integer = 0
            Dim StrECode As String = String.Empty
            Dim StrPrevECode As String = String.Empty
            Dim rpt_Row As DataRow = Nothing
            Dim decColumn81Total As Decimal


            For Each dtRow As DataRow In mdtData.Rows
                StrECode = dtRow.Item("employeecode").ToString.Trim

                If StrPrevECode <> StrECode Then
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                    If CBool(dtRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                        rpt_Row.Item("Column5") = dtRow.Item("OtherMembership")
                    Else
                        rpt_Row.Item("Column5") = ""
                    End If

                    iCnt += 1
                    rpt_Row.Item("Column6") = iCnt

                    rpt_Row.Item("Column11") = Language.getMessage(mstrModuleName, 1, "EMPLOYER INFO")
                    rpt_Row.Item("Column12") = Language.getMessage(mstrModuleName, 2, "PAYMENT IN FOR")
                Else
                    rpt_Row = rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)
                    rpt_Row.Item("Column5") = dtRow.Item("OtherMembership")
                    rpt_Row.AcceptChanges()

                    StrPrevECode = StrECode

                    Continue For
                End If

                rpt_Row.Item("Column1") = dtRow.Item("employeecode").ToString
                rpt_Row.Item("Column2") = dtRow.Item("EName")

                If CBool(dtRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                    rpt_Row.Item("Column3") = ""
                Else
                    rpt_Row.Item("Column3") = dtRow.Item("MemshipNo")
                End If

                rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)


                decColumn81Total = decColumn81Total + CDec(dtRow.Item("Amount"))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                StrPrevECode = StrECode
            Next

            mstrAddress = Company._Object._Address1
            mstrAddress &= " " & Company._Object._City_Name & " " & Company._Object._State_Name

            objRpt = New ArutiReport.Designer.rptKenyaHELB_Report
            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblEmployerInfo", Language.getMessage(mstrModuleName, 1, "EMPLOYER INFO"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentInFor", Language.getMessage(mstrModuleName, 2, "PAYMENT IN FOR"))
            Call ReportFunction.TextChange(objRpt, "txtEmpDetails", Language.getMessage(mstrModuleName, 3, "EMPLOYEES DETAILS"))
            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 4, "S/No."))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 5, "NAMES"))
            Call ReportFunction.TextChange(objRpt, "txtIDNo", Language.getMessage(mstrModuleName, 6, "ID NUMBER"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 7, "STAFF NUMBER"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 8, "AMOUNT"))

            Call ReportFunction.TextChange(objRpt, "txtTotalAmount", Format(decColumn81Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 9, "TOTAL"))

            Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 10, "HELB MONTHLY REMITTANCE SCHEDULE"))
            Call ReportFunction.TextChange(objRpt, "txtSubmitted", Language.getMessage(mstrModuleName, 11, "(TO BE DULY COMPLETED WITH EVERY CHEQUE/DEPOSIT SLIP/EFT SUBMITTED TO HELB)"))
            Call ReportFunction.TextChange(objRpt, "txtEmailTo", Language.getMessage(mstrModuleName, 12, "*Please email the remittance form to: remittance@helb.co.ke "))
            Call ReportFunction.TextChange(objRpt, "txtDeadline", Language.getMessage(mstrModuleName, 13, "*Deadline for remittance is 15th of the following month, late remittance will attract 5% penalty per month outstanding."))
            Call ReportFunction.TextChange(objRpt, "txtPaymentAccepted", Language.getMessage(mstrModuleName, 14, "*No payment shall be accepted without remittance schedule"))
            Call ReportFunction.TextChange(objRpt, "lblEmpCodeNo", Language.getMessage(mstrModuleName, 15, "HELB EMPLOYER CODE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 16, "NAME OF EMPLOYER"))
            Call ReportFunction.TextChange(objRpt, "lblPostalCode", Language.getMessage(mstrModuleName, 17, "POSTAL ADDRESS AND CODE"))
            Call ReportFunction.TextChange(objRpt, "lblPhysicalAddress", Language.getMessage(mstrModuleName, 18, "PHYSICAL ADDRESS"))
            Call ReportFunction.TextChange(objRpt, "lblEmail", Language.getMessage(mstrModuleName, 19, "E-MAIL ADDRESS"))
            Call ReportFunction.TextChange(objRpt, "lblPhoneNo", Language.getMessage(mstrModuleName, 20, "TELEPHONE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblMobileNo", Language.getMessage(mstrModuleName, 21, "MOBILE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 22, "PAYROLL MONTH"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentType", Language.getMessage(mstrModuleName, 23, "CHEQUE NUMBER/TYPE OF PAYMENT"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentDate", Language.getMessage(mstrModuleName, 24, "DATE OF PAYMENT/EFT/DEPOSIT"))
            Call ReportFunction.TextChange(objRpt, "lblBank", Language.getMessage(mstrModuleName, 25, "HELB RECEIVING BANK"))

            Call ReportFunction.TextChange(objRpt, "txtEmpCodeNo", Company._Object._Reg2_Value)
            Call ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt, "txtPostalCode", Company._Object._Address2 & " " & Company._Object._Post_Code_No)
            Call ReportFunction.TextChange(objRpt, "txtPhysicalAddress", mstrAddress)
            Call ReportFunction.TextChange(objRpt, "txtEmail", Company._Object._Email)
            Call ReportFunction.TextChange(objRpt, "txtPhoneNo", Company._Object._Phone1)
            Call ReportFunction.TextChange(objRpt, "txtMobileNo", Company._Object._Phone2)
            Call ReportFunction.TextChange(objRpt, "txtMonth", mstrPeriodName)
            Call ReportFunction.TextChange(objRpt, "txtPaymentType", Language.getMessage(mstrModuleName, 26, "EFT"))
            Call ReportFunction.TextChange(objRpt, "txtPaymentDate", "")
            Call ReportFunction.TextChange(objRpt, "txtBank", "")





            Dim intColIndex As Integer = -1
            If menExportAction = enExportAction.ExcelExtra Or menExportAction = enExportAction.ExcelCSV Then
                mdtTableExcel = rpt_Data.Tables(0)

                mdtTableExcel.Columns("Column6").Caption = Language.getMessage(mstrModuleName, 4, "S/No.")
                intColIndex += 1
                mdtTableExcel.Columns("Column6").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 5, "NAMES")
                intColIndex += 1
                mdtTableExcel.Columns("Column2").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 6, "ID NUMBER")
                intColIndex += 1
                mdtTableExcel.Columns("Column3").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 7, "STAFF NUMBER")
                intColIndex += 1
                mdtTableExcel.Columns("Column1").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 8, "AMOUNT")
                intColIndex += 1
                mdtTableExcel.Columns("Column81").SetOrdinal(intColIndex)


                For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
                Next

            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReportForSaving; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Nilay (18-Oct-2016) -- End

    'Sohail (03 Aug 2021) -- Start
    'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
    Private Function Generate_DetailReportForDeduction(ByVal strDatabaseName As String, _
                                                      ByVal intUserUnkid As Integer, _
                                                      ByVal intYearUnkid As Integer, _
                                                      ByVal intCompanyUnkid As Integer, _
                                                      ByVal dtPeriodEnd As Date, _
                                                      ByVal strUserModeSetting As String, _
                                                      ByVal blnOnlyApproved As Boolean, _
                                                      ByVal intBaseCurrencyId As Integer _
                                                      ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                             ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", 0 AS employercontribution " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                             ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

               

                StrQ &= "                WHERE prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                        "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'If mintNonPayrollMembershipId > 0 Then

                '    StrQ &= "UNION ALL " & _
                '                "SELECT hremployee_master.employeecode AS employeecode " & _
                '                     ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                '                      ",'' AS MemshipNo " & _
                '                      ", 0 AS empcontribution " & _
                '                      ", 0 AS employercontribution " & _
                '                      ", 0 AS Amount " & _
                '                      ", '' AS PeriodName " & _
                '                      ", hremployee_master.employeeunkid AS EmpId " & _
                '                      ", '' AS MemshipCategory " & _
                '                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                '                      ", '' AS Remarks " & _
                '                      ", 1 AS NonPayrollMembership  " & _
                '                      "FROM    hremployee_master " & _
                '                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                '                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "

                '    StrQ &= mstrAnalysis_Join

                '    If xUACQry.Trim.Length > 0 Then
                '        StrQ &= xUACQry
                '    End If

                '    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                '    If mstrAdvance_Filter.Trim.Length > 0 Then
                '        StrQ &= " AND " & mstrAdvance_Filter
                '    End If

                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrQ &= " AND " & xUACFiltrQry
                '    End If

                'End If

                StrQ &= "ORDER BY employeecode "

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", 0 AS employercontribution " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                              ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                               "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                "AND prpayment_tran.countryunkid = " & mintCountryId & " "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If


                StrQ &= "            WHERE prpayrollprocess_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                    "AND prpayment_tran.periodunkid = @PeriodId " & _
                                    "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", 0 AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)) AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                             "FROM    prpayrollprocess_tran " & _
                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                'StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                '                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                '                            "FROM    prpayrollprocess_tran " & _
                '                                     "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                     "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                '                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                '                                     "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                '                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                '                                     "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '                                     "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                'StrQ &= mstrAnalysis_Join

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                '                            "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                '                            "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                '                            "AND hrmembership_master.membershipunkid = @MemId "

                'If xUACFiltrQry.Trim.Length > 0 Then
                '    StrQ &= " AND " & xUACFiltrQry
                'End If

                StrQ &= "                WHERE prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                        "AND prpayment_tran.periodunkid = @PeriodId " & _
                                        "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", 0 AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)) AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                        "FROM    prpayrollprocess_tran " & _
                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                                "AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "LEFT JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                    WHERE prpayrollprocess_tran.isvoid = 0 " & _
                                            "AND prtnaleave_tran.isvoid = 0 " & _
                                            "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                            "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid " & _
                                            " AND prpayment_tran.periodunkid IS NULL "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'If mintNonPayrollMembershipId > 0 Then

                '    StrQ &= "UNION ALL " & _
                '                "SELECT hremployee_master.employeecode AS employeecode " & _
                '                     ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                '                      ",'' AS MemshipNo " & _
                '                      ", 0 AS empcontribution " & _
                '                      ", 0 AS employercontribution " & _
                '                      ", 0 AS Amount " & _
                '                      ", '' AS PeriodName " & _
                '                      ", hremployee_master.employeeunkid AS EmpId " & _
                '                      ", '' AS MemshipCategory " & _
                '                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                '                      ", '' AS Remarks " & _
                '                      ", 1 AS NonPayrollMembership  " & _
                '                      "FROM    hremployee_master " & _
                '                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                '                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "

                '    StrQ &= mstrAnalysis_Join

                '    If xUACQry.Trim.Length > 0 Then
                '        StrQ &= xUACQry
                '    End If

                '    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                '    If mstrAdvance_Filter.Trim.Length > 0 Then
                '        StrQ &= " AND " & mstrAdvance_Filter
                '    End If

                '    If xUACFiltrQry.Trim.Length > 0 Then
                '        StrQ &= " AND " & xUACFiltrQry
                '    End If

                'End If

                StrQ &= "ORDER BY employeecode "

            End If

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNonPayrollMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionID)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdtData As DataTable = New DataView(dsList.Tables(0), "", "employeecode, Amount DESC", DataViewRowState.CurrentRows).ToTable

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim iCnt As Integer = 0
            Dim StrECode As String = String.Empty
            Dim StrPrevECode As String = String.Empty
            Dim rpt_Row As DataRow = Nothing
            Dim decColumn81Total As Decimal


            For Each dtRow As DataRow In mdtData.Rows
                StrECode = dtRow.Item("employeecode").ToString.Trim

                If StrPrevECode <> StrECode Then
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                    If CBool(dtRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                        rpt_Row.Item("Column5") = dtRow.Item("OtherMembership")
                    Else
                        rpt_Row.Item("Column5") = ""
                    End If

                    iCnt += 1
                    rpt_Row.Item("Column6") = iCnt

                    rpt_Row.Item("Column11") = Language.getMessage(mstrModuleName, 1, "EMPLOYER INFO")
                    rpt_Row.Item("Column12") = Language.getMessage(mstrModuleName, 2, "PAYMENT IN FOR")
                Else
                    rpt_Row = rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)
                    rpt_Row.Item("Column5") = dtRow.Item("OtherMembership")
                    rpt_Row.AcceptChanges()

                    StrPrevECode = StrECode

                    Continue For
                End If

                rpt_Row.Item("Column1") = dtRow.Item("employeecode").ToString
                rpt_Row.Item("Column2") = dtRow.Item("EName")

                If CBool(dtRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                    rpt_Row.Item("Column3") = ""
                Else
                    rpt_Row.Item("Column3") = dtRow.Item("MemshipNo")
                End If

                rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)


                decColumn81Total = decColumn81Total + CDec(dtRow.Item("Amount"))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                StrPrevECode = StrECode
            Next

            mstrAddress = Company._Object._Address1
            mstrAddress &= " " & Company._Object._City_Name & " " & Company._Object._State_Name

            objRpt = New ArutiReport.Designer.rptKenyaHELB_Report
            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblEmployerInfo", Language.getMessage(mstrModuleName, 1, "EMPLOYER INFO"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentInFor", Language.getMessage(mstrModuleName, 2, "PAYMENT IN FOR"))
            Call ReportFunction.TextChange(objRpt, "txtEmpDetails", Language.getMessage(mstrModuleName, 3, "EMPLOYEES DETAILS"))
            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 4, "S/No."))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 5, "NAMES"))
            Call ReportFunction.TextChange(objRpt, "txtIDNo", Language.getMessage(mstrModuleName, 6, "ID NUMBER"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 7, "STAFF NUMBER"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 8, "AMOUNT"))

            Call ReportFunction.TextChange(objRpt, "txtTotalAmount", Format(decColumn81Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 9, "TOTAL"))

            Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 10, "HELB MONTHLY REMITTANCE SCHEDULE"))
            Call ReportFunction.TextChange(objRpt, "txtSubmitted", Language.getMessage(mstrModuleName, 11, "(TO BE DULY COMPLETED WITH EVERY CHEQUE/DEPOSIT SLIP/EFT SUBMITTED TO HELB)"))
            Call ReportFunction.TextChange(objRpt, "txtEmailTo", Language.getMessage(mstrModuleName, 12, "*Please email the remittance form to: remittance@helb.co.ke "))
            Call ReportFunction.TextChange(objRpt, "txtDeadline", Language.getMessage(mstrModuleName, 13, "*Deadline for remittance is 15th of the following month, late remittance will attract 5% penalty per month outstanding."))
            Call ReportFunction.TextChange(objRpt, "txtPaymentAccepted", Language.getMessage(mstrModuleName, 14, "*No payment shall be accepted without remittance schedule"))
            Call ReportFunction.TextChange(objRpt, "lblEmpCodeNo", Language.getMessage(mstrModuleName, 15, "HELB EMPLOYER CODE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 16, "NAME OF EMPLOYER"))
            Call ReportFunction.TextChange(objRpt, "lblPostalCode", Language.getMessage(mstrModuleName, 17, "POSTAL ADDRESS AND CODE"))
            Call ReportFunction.TextChange(objRpt, "lblPhysicalAddress", Language.getMessage(mstrModuleName, 18, "PHYSICAL ADDRESS"))
            Call ReportFunction.TextChange(objRpt, "lblEmail", Language.getMessage(mstrModuleName, 19, "E-MAIL ADDRESS"))
            Call ReportFunction.TextChange(objRpt, "lblPhoneNo", Language.getMessage(mstrModuleName, 20, "TELEPHONE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblMobileNo", Language.getMessage(mstrModuleName, 21, "MOBILE NUMBER"))
            Call ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 22, "PAYROLL MONTH"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentType", Language.getMessage(mstrModuleName, 23, "CHEQUE NUMBER/TYPE OF PAYMENT"))
            Call ReportFunction.TextChange(objRpt, "lblPaymentDate", Language.getMessage(mstrModuleName, 24, "DATE OF PAYMENT/EFT/DEPOSIT"))
            Call ReportFunction.TextChange(objRpt, "lblBank", Language.getMessage(mstrModuleName, 25, "HELB RECEIVING BANK"))

            Call ReportFunction.TextChange(objRpt, "txtEmpCodeNo", Company._Object._Reg2_Value)
            Call ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt, "txtPostalCode", Company._Object._Address2 & " " & Company._Object._Post_Code_No)
            Call ReportFunction.TextChange(objRpt, "txtPhysicalAddress", mstrAddress)
            Call ReportFunction.TextChange(objRpt, "txtEmail", Company._Object._Email)
            Call ReportFunction.TextChange(objRpt, "txtPhoneNo", Company._Object._Phone1)
            Call ReportFunction.TextChange(objRpt, "txtMobileNo", Company._Object._Phone2)
            Call ReportFunction.TextChange(objRpt, "txtMonth", mstrPeriodName)
            Call ReportFunction.TextChange(objRpt, "txtPaymentType", Language.getMessage(mstrModuleName, 26, "EFT"))
            Call ReportFunction.TextChange(objRpt, "txtPaymentDate", "")
            Call ReportFunction.TextChange(objRpt, "txtBank", "")





            Dim intColIndex As Integer = -1
            If menExportAction = enExportAction.ExcelExtra Or menExportAction = enExportAction.ExcelCSV Then
                mdtTableExcel = rpt_Data.Tables(0)

                mdtTableExcel.Columns("Column6").Caption = Language.getMessage(mstrModuleName, 4, "S/No.")
                intColIndex += 1
                mdtTableExcel.Columns("Column6").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 5, "NAMES")
                intColIndex += 1
                mdtTableExcel.Columns("Column2").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 6, "ID NUMBER")
                intColIndex += 1
                mdtTableExcel.Columns("Column3").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 7, "STAFF NUMBER")
                intColIndex += 1
                mdtTableExcel.Columns("Column1").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 8, "AMOUNT")
                intColIndex += 1
                mdtTableExcel.Columns("Column81").SetOrdinal(intColIndex)


                For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
                Next

            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReportForDeduction; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sohail (03 Aug 2021) -- End

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "EMPLOYER INFO")
			Language.setMessage(mstrModuleName, 2, "PAYMENT IN FOR")
            Language.setMessage(mstrModuleName, 3, "EMPLOYEES DETAILS")
            Language.setMessage(mstrModuleName, 4, "S/No.")
            Language.setMessage(mstrModuleName, 5, "NAMES")
            Language.setMessage(mstrModuleName, 6, "ID NUMBER")
            Language.setMessage(mstrModuleName, 7, "STAFF NUMBER")
            Language.setMessage(mstrModuleName, 8, "AMOUNT")
            Language.setMessage(mstrModuleName, 9, "TOTAL")
            Language.setMessage(mstrModuleName, 10, "HELB MONTHLY REMITTANCE SCHEDULE")
            Language.setMessage(mstrModuleName, 11, "(TO BE DULY COMPLETED WITH EVERY CHEQUE/DEPOSIT SLIP/EFT SUBMITTED TO HELB)")
            Language.setMessage(mstrModuleName, 12, "*Please email the remittance form to: remittance@helb.co.ke")
            Language.setMessage(mstrModuleName, 13, "*Deadline for remittance is 15th of the following month, late remittance will attract 5% penalty per month outstanding.")
            Language.setMessage(mstrModuleName, 14, "*No payment shall be accepted without remittance schedule")
            Language.setMessage(mstrModuleName, 15, "HELB EMPLOYER CODE NUMBER")
            Language.setMessage(mstrModuleName, 16, "NAME OF EMPLOYER")
            Language.setMessage(mstrModuleName, 17, "POSTAL ADDRESS AND CODE")
            Language.setMessage(mstrModuleName, 18, "PHYSICAL ADDRESS")
            Language.setMessage(mstrModuleName, 19, "E-MAIL ADDRESS")
            Language.setMessage(mstrModuleName, 20, "TELEPHONE NUMBER")
            Language.setMessage(mstrModuleName, 21, "MOBILE NUMBER")
            Language.setMessage(mstrModuleName, 22, "PAYROLL MONTH")
            Language.setMessage(mstrModuleName, 23, "CHEQUE NUMBER/TYPE OF PAYMENT")
            Language.setMessage(mstrModuleName, 24, "DATE OF PAYMENT/EFT/DEPOSIT")
            Language.setMessage(mstrModuleName, 25, "HELB RECEIVING BANK")
            Language.setMessage(mstrModuleName, 26, "EFT")
            Language.setMessage(mstrModuleName, 27, "Select")
            Language.setMessage(mstrModuleName, 28, "Membership")
            Language.setMessage(mstrModuleName, 29, "Loan")
            Language.setMessage(mstrModuleName, 30, "Saving")
            Language.setMessage(mstrModuleName, 31, "PAYMENT INFOR")
			Language.setMessage(mstrModuleName, 32, "Deduction")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
