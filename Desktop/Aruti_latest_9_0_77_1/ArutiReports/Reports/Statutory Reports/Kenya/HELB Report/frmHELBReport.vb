'************************************************************************************************************************************
'Class Name : frmHELBReport.vb
'Purpose    : 
'Written By : Sohail.
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmHELBReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmHELBReport"
    Private objHELB As clsHELBReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private eReport As enArutiReport
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Contructor "

    Public Sub New()
        _Show_ExcelExtra_Menu = True
        objHELB = New clsHELBReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objHELB.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMember As New clsmembership_master
        Dim dsCombos As New DataSet
        Dim objperiod As New clscommom_period_Tran
        Dim objEmpContribution As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim objLoan As New clsLoan_Scheme 'Sohail (10 Mar 2016)
        Dim objSavingScheme As New clsSavingScheme 'Nilay (18-Oct-2016)
        Try
            dsCombos = objMember.getListForCombo("Membership", True, , 1)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objMember.getListForCombo("Membership", True, , 2)
            With cboIncludeMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Membership")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", True)
            cboCurrency.ValueMember = "countryunkid"
            cboCurrency.DisplayMember = "currency_sign"
            cboCurrency.DataSource = dsCombos.Tables("Currency")

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If

            'Sohail (10 Mar 2016) -- Start
            'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
            dsCombos = objHELB.getListForCombo("Mode", False)
            With cboMode
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Mode")
                .SelectedValue = CInt(clsHELBReport.enMode.MEMBERSHIP)
            End With

            dsCombos = objLoan.getComboList(True, "Loan")
            With cboLoan
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Loan")
                .SelectedValue = 0
            End With
            'Sohail (10 Mar 2016) -- End

            'Nilay (18-Oct-2016) -- Start
            'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
            dsCombos = objSavingScheme.getComboList(True, "Saving")
            With cboSavingScheme
                .ValueMember = "savingschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Saving")
                .SelectedValue = 0
            End With
            'Nilay (18-Oct-2016) -- End

            'Sohail (03 Aug 2021) -- Start
            'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.DeductionForEmployee)
            With cboDeduction
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (03 Aug 2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objperiod = Nothing
            objEmpContribution = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboIncludeMembership.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            cboCurrency.SelectedValue = 0
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            mstrAdvanceFilter = ""

            'S.SANDEEP |18-JUN-2021| -- START
            'ISSUE/ENHANCEMENT : Kenya Reports Changes (Statutory)
            GetValue()
            'S.SANDEEP |18-JUN-2021| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            Call objHELB.SetDefaultValue()

            'Sohail (10 Mar 2016) -- Start
            'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
            'If CInt(cboMembership.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
            '    cboMembership.Focus()
            '    Exit Function
            'End If
            'Sohail (10 Mar 2016) -- End

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            End If

            'Sohail (10 Mar 2016) -- Start
            'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
            If CInt(cboMode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Mode is mandatory information. Please select Mode to continue."), enMsgBoxStyle.Information)
                cboMode.Focus()
                Return False
            ElseIf CInt(cboMode.SelectedValue) = clsHELBReport.enMode.MEMBERSHIP AndAlso CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            ElseIf CInt(cboMode.SelectedValue) = clsHELBReport.enMode.LOAN AndAlso CInt(cboIncludeMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboIncludeMembership.Focus()
                Return False
            ElseIf CInt(cboMode.SelectedValue) = clsHELBReport.enMode.LOAN AndAlso CInt(cboLoan.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Loan Scheme is mandatory information. Please select Loan Scheme to continue."), enMsgBoxStyle.Information)
                cboLoan.Focus()
                Return False
                'Nilay (18-Oct-2016) -- Start
                'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
            ElseIf CInt(cboMode.SelectedValue) = clsHELBReport.enMode.SAVING AndAlso CInt(cboIncludeMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboIncludeMembership.Focus()
                Return False
            ElseIf CInt(cboMode.SelectedValue) = clsHELBReport.enMode.SAVING AndAlso CInt(cboSavingScheme.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Saving Scheme is mandatory information. Please select Saving Scheme to continue."), enMsgBoxStyle.Information)
                cboSavingScheme.Focus()
                Return False
                'Nilay (18-Oct-2016) -- End
                'Sohail (03 Aug 2021) -- Start
                'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
            ElseIf CInt(cboMode.SelectedValue) = clsHELBReport.enMode.Deduction AndAlso CInt(cboIncludeMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboIncludeMembership.Focus()
                Return False
            ElseIf CInt(cboMode.SelectedValue) = clsHELBReport.enMode.Deduction AndAlso CInt(cboDeduction.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Deduction transaction head is mandatory information. Please select deduction transaction head to continue."), enMsgBoxStyle.Information)
                cboSavingScheme.Focus()
                Return False
                'Sohail (03 Aug 2021) -- End
            End If
            'Sohail (10 Mar 2016) -- End

            If CInt(cboMembership.SelectedValue) > 0 Then
                objHELB._MembershipId = cboMembership.SelectedValue
                objHELB._MembershipName = cboMembership.Text
            End If

            If CInt(cboIncludeMembership.SelectedValue) > 0 Then
                objHELB._NonPayrollMembershipId = cboIncludeMembership.SelectedValue
                objHELB._NonPayrollMembershipName = cboIncludeMembership.Text
            Else
                objHELB._NonPayrollMembershipName = Language.getMessage(mstrModuleName, 3, "ID No.")
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objHELB._PeriodId = cboPeriod.SelectedValue
                objHELB._PeriodName = cboPeriod.Text
            End If

            'Sohail (10 Mar 2016) -- Start
            'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
            objHELB._ModeID = CInt(cboMode.SelectedValue)
            objHELB._ModeName = cboMode.Text
            objHELB._LoanID = CInt(cboLoan.SelectedValue)
            objHELB._LoanName = cboLoan.Text
            'Sohail (10 Mar 2016) -- End

            'Nilay (18-Oct-2016) -- Start
            'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
            objHELB._SavingSchemeID = CInt(cboSavingScheme.SelectedValue)
            'Nilay (18-Oct-2016) -- End

            'Sohail (03 Aug 2021) -- Start
            'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
            objHELB._SavingSchemeName = cboSavingScheme.Text
            objHELB._DeductionID = CInt(cboDeduction.SelectedValue)
            objHELB._DeductionName = cboDeduction.Text
            'Sohail (03 Aug 2021) -- End

            objHELB._ViewByIds = mstrStringIds
            objHELB._ViewIndex = mintViewIdx
            objHELB._ViewByName = mstrStringName
            objHELB._Analysis_Fields = mstrAnalysis_Fields
            objHELB._Analysis_Join = mstrAnalysis_Join
            objHELB._Report_GroupName = mstrReport_GroupName

            If (mintBaseCurrId = mintPaidCurrencyId) Or mintPaidCurrencyId <= 0 Then
                mintPaidCurrencyId = mintBaseCurrId
            End If

            objHELB._CountryId = CInt(cboCurrency.SelectedValue)
            objHELB._BaseCurrencyId = mintBaseCurrId
            objHELB._PaidCurrencyId = mintPaidCurrencyId

            If mdecBaseExRate > 0 AndAlso mdecPaidExRate Then
                objHELB._ConversionRate = mdecPaidExRate / mdecBaseExRate
            End If

            objHELB._ExchangeRate = LblCurrencyRate.Text

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            objHELB._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    'S.SANDEEP |18-JUN-2021| -- START
    'ISSUE/ENHANCEMENT : Kenya Reports Changes (Statutory)
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.HELB_REPORT)
            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))
                        Case enHeadTypeId.Currency
                            cboCurrency.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                        Case enHeadTypeId.Mode
                            cboMode.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                        Case enHeadTypeId.Loan
                            cboLoan.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                        Case enHeadTypeId.Saving
                            cboSavingScheme.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                        Case enHeadTypeId.IncludeMembership
                            cboIncludeMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (03 Aug 2021) -- Start
                            'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
                        Case enHeadTypeId.Deduction
                            cboDeduction.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (03 Aug 2021) -- End
                    End Select
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |18-JUN-2021| -- END

#End Region

#Region " Forms "

    Private Sub frmHELBReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objHELB = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmHELBReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHELBReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = objHELB._ReportName
            Me._Message = objHELB._ReportDesc
            Call OtherSettings()
            'Sohail (10 Mar 2016) -- Start
            'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
            lblIncludeMembership.Location = lblMembership.Location
            lblIncludeMembership.Size = lblMembership.Size
            cboIncludeMembership.Location = cboMembership.Location
            cboIncludeMembership.Size = cboMembership.Size
            'Sohail (10 Mar 2016) -- End

            'Nilay (18-Oct-2016) -- Start
            'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
            lblSavingScheme.Location = lblLoan.Location
            lblSavingScheme.Size = lblLoan.Size
            cboSavingScheme.Location = cboLoan.Location
            cboSavingScheme.Size = cboLoan.Size
            'Nilay (18-Oct-2016) -- End

            'Sohail (03 Aug 2021) -- Start
            'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
            lblDeduction.Location = lblLoan.Location
            lblDeduction.Size = lblLoan.Size
            cboDeduction.Location = cboLoan.Location
            cboDeduction.Size = cboLoan.Size
            'Sohail (03 Aug 2021) -- End

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHELBReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then

                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

    'S.SANDEEP |18-JUN-2021| -- START
    'ISSUE/ENHANCEMENT : Kenya Reports Changes (Statutory)
#Region " Enum "
    Private Enum enHeadTypeId
        Currency = 1
        Mode = 2
        Membership = 3
        Loan = 4
        Saving = 5
        IncludeMembership = 6
        Deduction = 7 'Sohail (03 Aug 2021)
    End Enum
#End Region
    'S.SANDEEP |18-JUN-2021| -- END

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objHELB.generateReport(0, e.Type, enExportAction.None)
            objHELB.generateReportNew(FinancialYear._Object._DatabaseName, _
                                      User._Object._Userunkid, _
                                      FinancialYear._Object._YearUnkid, _
                                      Company._Object._Companyunkid, _
                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                      ConfigParameter._Object._UserAccessModeSetting, _
                                      True, ConfigParameter._Object._ExportReportPath, _
                                      ConfigParameter._Object._OpenAfterExport, 0, e.Type, _
                                      enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objHELB.generateReport(0, enPrintAction.None, e.Type)
            objHELB.generateReportNew(FinancialYear._Object._DatabaseName, _
                                      User._Object._Userunkid, _
                                      FinancialYear._Object._YearUnkid, _
                                      Company._Object._Companyunkid, _
                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                      ConfigParameter._Object._UserAccessModeSetting, _
                                      True, ConfigParameter._Object._ExportReportPath, _
                                      ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, _
                                      e.Type, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsHELBReport.SetMessages()
            objfrm._Other_ModuleNames = "clsHELBReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'S.SANDEEP |18-JUN-2021| -- START
    'ISSUE/ENHANCEMENT : Kenya Reports Changes (Statutory)
    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try
            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()
                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.HELB_REPORT
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0
                Select Case intHeadType
                    Case enHeadTypeId.Currency
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCurrency.SelectedValue.ToString
                    Case enHeadTypeId.Mode
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMode.SelectedValue.ToString
                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString
                    Case enHeadTypeId.Loan
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboLoan.SelectedValue.ToString
                    Case enHeadTypeId.Saving
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboSavingScheme.SelectedValue.ToString
                    Case enHeadTypeId.IncludeMembership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboIncludeMembership.SelectedValue.ToString
                        'Sohail (03 Aug 2021) -- Start
                        'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
                    Case enHeadTypeId.Deduction
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboDeduction.SelectedValue.ToString
                        'Sohail (03 Aug 2021) -- End
                End Select
                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
            Next
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Selection Saved Successfully"), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |18-JUN-2021| -- END

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'S.SANDEEP |18-JUN-2021| -- START
    'ISSUE/ENHANCEMENT : Kenya Reports Changes (Statutory)
    Private Sub lnkExportDataxlsx_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkExportDataxlsx.LinkClicked        
        Try
            If Not SetFilter() Then Exit Sub

            Dim strExportPath As String = ""
            If System.IO.Directory.Exists(ConfigParameter._Object._ExportReportPath) = False Then
                Dim dig As New System.Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    strExportPath = dig.SelectedPath
                Else
                    Exit Sub
                End If
            Else
                strExportPath = ConfigParameter._Object._ExportReportPath
            End If

            objHELB.generateReportNew(FinancialYear._Object._DatabaseName, _
                                      User._Object._Userunkid, _
                                      FinancialYear._Object._YearUnkid, _
                                      Company._Object._Companyunkid, _
                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                      ConfigParameter._Object._UserAccessModeSetting, _
                                      True, strExportPath, _
                                      ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, _
                                      enExportAction.ExcelCSV, ConfigParameter._Object._Base_CurrencyId)

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Report successfully exported."), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkExportDataxlsx_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |18-JUN-2021| -- END


#End Region

#Region "ComboBox Event"

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboCurrency.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                Dim dsList As DataSet = Nothing
                dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate")).ToTable

                If dtTable.Rows.Count > 0 Then
                    mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                    mintPaidCurrencyId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    LblCurrencyRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                Else
                    mdecBaseExRate = 0
                    mdecPaidExRate = 0
                    mintPaidCurrencyId = 0
                    LblCurrencyRate.Text = ""
                End If
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
                mintPaidCurrencyId = 0
                LblCurrencyRate.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (10 Mar 2016) -- Start
    'Enhancement - Allow HELB Report to generate with Loan Scheme and Non Payroll Membership for KBC.
    Private Sub cboMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Try
            'Sohail (03 Aug 2021) -- Start
            'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
            lblMembership.Visible = False
            cboMembership.SelectedValue = 0
            cboMembership.Visible = False

            lblIncludeMembership.Visible = False
            cboIncludeMembership.SelectedValue = 0
            cboIncludeMembership.Visible = False

            lblLoan.Visible = False
            cboLoan.SelectedValue = 0
            cboLoan.Visible = False

            lblSavingScheme.Visible = False
            cboSavingScheme.Visible = False
            cboSavingScheme.SelectedValue = 0

            lblDeduction.Visible = False
            cboDeduction.Visible = False
            cboDeduction.SelectedValue = 0
            'Sohail (03 Aug 2021) -- End

            If CInt(cboMode.SelectedValue) = clsHELBReport.enMode.LOAN Then
                lblMembership.Visible = False
                cboMembership.SelectedValue = 0
                cboMembership.Visible = False

                lblIncludeMembership.Visible = True
                cboIncludeMembership.Visible = True
                lblLoan.Visible = True
                cboLoan.Visible = True
                'Nilay (18-Oct-2016) -- Start
                'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
                lblSavingScheme.Visible = False
                cboSavingScheme.Visible = False
                cboSavingScheme.SelectedValue = 0

            ElseIf CInt(cboMode.SelectedValue) = clsHELBReport.enMode.SAVING Then
                lblMembership.Visible = False
                cboMembership.SelectedValue = 0
                cboMembership.Visible = False

                lblIncludeMembership.Visible = True
                cboIncludeMembership.Visible = True

                lblLoan.Visible = False
                cboLoan.SelectedValue = 0
                cboLoan.Visible = False

                lblSavingScheme.Visible = True
                cboSavingScheme.Visible = True
                'Nilay (18-Oct-2016) -- End

                'Sohail (03 Aug 2021) -- Start
                'Kenya - Helb Report Enhancement : OLD - 432 : Open up Mode to also include Deductions from employee in addition to loan and saving.
            ElseIf CInt(cboMode.SelectedValue) = clsHELBReport.enMode.Deduction Then
                lblIncludeMembership.Visible = True
                cboIncludeMembership.Visible = True

                lblDeduction.Visible = True
                cboDeduction.Visible = True
                'Sohail (03 Aug 2021) -- End

            Else
                lblIncludeMembership.Visible = False
                cboIncludeMembership.SelectedValue = 0
                cboIncludeMembership.Visible = False
                lblLoan.Visible = False
                cboLoan.SelectedValue = 0
                cboLoan.Visible = False

                lblMembership.Visible = True
                cboMembership.Visible = True
                'Nilay (18-Oct-2016) -- Start
                'Enhancement - Allow HELB Report to generate with Savings Scheme and Non Payroll Membership for HJF
                lblSavingScheme.Visible = False
                cboSavingScheme.Visible = False
                cboSavingScheme.SelectedValue = 0
                'Nilay (18-Oct-2016) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (10 Mar 2016) -- End

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lblIncludeMembership.Text = Language._Object.getCaption(Me.lblIncludeMembership.Name, Me.lblIncludeMembership.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblMode.Text = Language._Object.getCaption(Me.lblMode.Name, Me.lblMode.Text)
			Me.lblLoan.Text = Language._Object.getCaption(Me.lblLoan.Name, Me.lblLoan.Text)
			Me.lblSavingScheme.Text = Language._Object.getCaption(Me.lblSavingScheme.Name, Me.lblSavingScheme.Text)
			Me.lnkExportDataxlsx.Text = Language._Object.getCaption(Me.lnkExportDataxlsx.Name, Me.lnkExportDataxlsx.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblDeduction.Text = Language._Object.getCaption(Me.lblDeduction.Name, Me.lblDeduction.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue.")
			Language.setMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 3, "ID No.")
			Language.setMessage(mstrModuleName, 4, "Mode is mandatory information. Please select Mode to continue.")
			Language.setMessage(mstrModuleName, 5, "Loan Scheme is mandatory information. Please select Loan Scheme to continue.")
			Language.setMessage(mstrModuleName, 6, "Report successfully exported.")
			Language.setMessage(mstrModuleName, 7, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 8, "Saving Scheme is mandatory information. Please select Saving Scheme to continue.")
			Language.setMessage(mstrModuleName, 9, "Deduction transaction head is mandatory information. Please select deduction transaction head to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
