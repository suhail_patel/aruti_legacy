﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHELBReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHELBReport))
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkExportDataxlsx = New System.Windows.Forms.LinkLabel
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboSavingScheme = New System.Windows.Forms.ComboBox
        Me.lblSavingScheme = New System.Windows.Forms.Label
        Me.lblLoan = New System.Windows.Forms.Label
        Me.cboLoan = New System.Windows.Forms.ComboBox
        Me.lblMode = New System.Windows.Forms.Label
        Me.cboMode = New System.Windows.Forms.ComboBox
        Me.lblIncludeMembership = New System.Windows.Forms.Label
        Me.cboIncludeMembership = New System.Windows.Forms.ComboBox
        Me.LblCurrencyRate = New System.Windows.Forms.Label
        Me.LblCurrency = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboDeduction = New System.Windows.Forms.ComboBox
        Me.lblDeduction = New System.Windows.Forms.Label
        Me.gbMandatoryInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.lblDeduction)
        Me.gbMandatoryInfo.Controls.Add(Me.cboDeduction)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkExportDataxlsx)
        Me.gbMandatoryInfo.Controls.Add(Me.btnSaveSelection)
        Me.gbMandatoryInfo.Controls.Add(Me.cboSavingScheme)
        Me.gbMandatoryInfo.Controls.Add(Me.lblSavingScheme)
        Me.gbMandatoryInfo.Controls.Add(Me.lblLoan)
        Me.gbMandatoryInfo.Controls.Add(Me.cboLoan)
        Me.gbMandatoryInfo.Controls.Add(Me.lblMode)
        Me.gbMandatoryInfo.Controls.Add(Me.cboMode)
        Me.gbMandatoryInfo.Controls.Add(Me.lblIncludeMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboIncludeMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrencyRate)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.lblMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(455, 209)
        Me.gbMandatoryInfo.TabIndex = 69
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkExportDataxlsx
        '
        Me.lnkExportDataxlsx.BackColor = System.Drawing.Color.Transparent
        Me.lnkExportDataxlsx.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkExportDataxlsx.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkExportDataxlsx.Location = New System.Drawing.Point(240, 176)
        Me.lnkExportDataxlsx.Name = "lnkExportDataxlsx"
        Me.lnkExportDataxlsx.Size = New System.Drawing.Size(208, 17)
        Me.lnkExportDataxlsx.TabIndex = 114
        Me.lnkExportDataxlsx.TabStop = True
        Me.lnkExportDataxlsx.Text = "Export in csv format (Data Only)"
        Me.lnkExportDataxlsx.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(123, 169)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 113
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'cboSavingScheme
        '
        Me.cboSavingScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSavingScheme.DropDownWidth = 230
        Me.cboSavingScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSavingScheme.FormattingEnabled = True
        Me.cboSavingScheme.Location = New System.Drawing.Point(123, 142)
        Me.cboSavingScheme.Name = "cboSavingScheme"
        Me.cboSavingScheme.Size = New System.Drawing.Size(34, 21)
        Me.cboSavingScheme.TabIndex = 95
        Me.cboSavingScheme.Visible = False
        '
        'lblSavingScheme
        '
        Me.lblSavingScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSavingScheme.Location = New System.Drawing.Point(255, 144)
        Me.lblSavingScheme.Name = "lblSavingScheme"
        Me.lblSavingScheme.Size = New System.Drawing.Size(42, 17)
        Me.lblSavingScheme.TabIndex = 94
        Me.lblSavingScheme.Text = "Saving Scheme"
        Me.lblSavingScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSavingScheme.Visible = False
        '
        'lblLoan
        '
        Me.lblLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoan.Location = New System.Drawing.Point(9, 144)
        Me.lblLoan.Name = "lblLoan"
        Me.lblLoan.Size = New System.Drawing.Size(108, 15)
        Me.lblLoan.TabIndex = 91
        Me.lblLoan.Text = "Loan"
        Me.lblLoan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoan
        '
        Me.cboLoan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoan.DropDownWidth = 180
        Me.cboLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoan.FormattingEnabled = True
        Me.cboLoan.Location = New System.Drawing.Point(123, 142)
        Me.cboLoan.Name = "cboLoan"
        Me.cboLoan.Size = New System.Drawing.Size(126, 21)
        Me.cboLoan.TabIndex = 92
        '
        'lblMode
        '
        Me.lblMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMode.Location = New System.Drawing.Point(9, 90)
        Me.lblMode.Name = "lblMode"
        Me.lblMode.Size = New System.Drawing.Size(108, 15)
        Me.lblMode.TabIndex = 88
        Me.lblMode.Text = "Mode"
        Me.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMode
        '
        Me.cboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMode.DropDownWidth = 180
        Me.cboMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMode.FormattingEnabled = True
        Me.cboMode.Location = New System.Drawing.Point(123, 88)
        Me.cboMode.Name = "cboMode"
        Me.cboMode.Size = New System.Drawing.Size(126, 21)
        Me.cboMode.TabIndex = 89
        '
        'lblIncludeMembership
        '
        Me.lblIncludeMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncludeMembership.Location = New System.Drawing.Point(255, 115)
        Me.lblIncludeMembership.Name = "lblIncludeMembership"
        Me.lblIncludeMembership.Size = New System.Drawing.Size(42, 17)
        Me.lblIncludeMembership.TabIndex = 86
        Me.lblIncludeMembership.Text = "Include Membership"
        Me.lblIncludeMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIncludeMembership
        '
        Me.cboIncludeMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncludeMembership.DropDownWidth = 230
        Me.cboIncludeMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIncludeMembership.FormattingEnabled = True
        Me.cboIncludeMembership.Location = New System.Drawing.Point(123, 115)
        Me.cboIncludeMembership.Name = "cboIncludeMembership"
        Me.cboIncludeMembership.Size = New System.Drawing.Size(34, 21)
        Me.cboIncludeMembership.TabIndex = 85
        '
        'LblCurrencyRate
        '
        Me.LblCurrencyRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrencyRate.Location = New System.Drawing.Point(260, 64)
        Me.LblCurrencyRate.Name = "LblCurrencyRate"
        Me.LblCurrencyRate.Size = New System.Drawing.Size(188, 15)
        Me.LblCurrencyRate.TabIndex = 79
        Me.LblCurrencyRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCurrency
        '
        Me.LblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrency.Location = New System.Drawing.Point(9, 62)
        Me.LblCurrency.Name = "LblCurrency"
        Me.LblCurrency.Size = New System.Drawing.Size(108, 15)
        Me.LblCurrency.TabIndex = 75
        Me.LblCurrency.Text = "Currency"
        Me.LblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 180
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(123, 61)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(126, 21)
        Me.cboCurrency.TabIndex = 76
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(357, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 69
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(9, 117)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(108, 15)
        Me.lblMembership.TabIndex = 64
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 180
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(123, 115)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(126, 21)
        Me.cboMembership.TabIndex = 65
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(9, 35)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(108, 15)
        Me.lblPeriod.TabIndex = 57
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 180
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(123, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboPeriod.TabIndex = 58
        '
        'cboDeduction
        '
        Me.cboDeduction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeduction.DropDownWidth = 230
        Me.cboDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeduction.FormattingEnabled = True
        Me.cboDeduction.Location = New System.Drawing.Point(163, 142)
        Me.cboDeduction.Name = "cboDeduction"
        Me.cboDeduction.Size = New System.Drawing.Size(34, 21)
        Me.cboDeduction.TabIndex = 116
        Me.cboDeduction.Visible = False
        '
        'lblDeduction
        '
        Me.lblDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeduction.Location = New System.Drawing.Point(303, 144)
        Me.lblDeduction.Name = "lblDeduction"
        Me.lblDeduction.Size = New System.Drawing.Size(42, 17)
        Me.lblDeduction.TabIndex = 117
        Me.lblDeduction.Text = "Deduction"
        Me.lblDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblDeduction.Visible = False
        '
        'frmHELBReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 452)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmHELBReport"
        Me.Text = "frmHELBReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbMandatoryInfo, 0)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblIncludeMembership As System.Windows.Forms.Label
    Friend WithEvents cboIncludeMembership As System.Windows.Forms.ComboBox
    Friend WithEvents LblCurrencyRate As System.Windows.Forms.Label
    Friend WithEvents LblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblMode As System.Windows.Forms.Label
    Friend WithEvents cboMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoan As System.Windows.Forms.Label
    Friend WithEvents cboLoan As System.Windows.Forms.ComboBox
    Friend WithEvents cboSavingScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblSavingScheme As System.Windows.Forms.Label
    Friend WithEvents lnkExportDataxlsx As System.Windows.Forms.LinkLabel
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents cboDeduction As System.Windows.Forms.ComboBox
    Friend WithEvents lblDeduction As System.Windows.Forms.Label
End Class
