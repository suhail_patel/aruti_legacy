﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmP9AReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmP9AReport))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnKeywordsBS = New eZee.Common.eZeeGradientButton
        Me.lblBasicSalaryFormula = New System.Windows.Forms.Label
        Me.txtBasicSalaryFormula = New System.Windows.Forms.TextBox
        Me.objbtnSearchTaxableIcome = New eZee.Common.eZeeGradientButton
        Me.cboTaxableIcome = New System.Windows.Forms.ComboBox
        Me.lblTaxableIcome = New System.Windows.Forms.Label
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchInsuranceRelief = New eZee.Common.eZeeGradientButton
        Me.cboInsuranceRelief = New System.Windows.Forms.ComboBox
        Me.lblInsuranceRelief = New System.Windows.Forms.Label
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.objbtnSearchQuarterValue = New eZee.Common.eZeeGradientButton
        Me.objelLineOptional = New eZee.Common.eZeeLine
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSearchPersonalRelief = New eZee.Common.eZeeGradientButton
        Me.cboPersonalRelief = New System.Windows.Forms.ComboBox
        Me.lblPersonalRelief = New System.Windows.Forms.Label
        Me.objbtnSearchTaxCharged = New eZee.Common.eZeeGradientButton
        Me.cboTaxCharged = New System.Windows.Forms.ComboBox
        Me.lblTaxCharged = New System.Windows.Forms.Label
        Me.objbtnSearchAmountOfInterest = New eZee.Common.eZeeGradientButton
        Me.cboAmountOfInterest = New System.Windows.Forms.ComboBox
        Me.lblAmountOfInterest = New System.Windows.Forms.Label
        Me.objbtnSearchE3 = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchE2 = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchE1 = New eZee.Common.eZeeGradientButton
        Me.cboE3 = New System.Windows.Forms.ComboBox
        Me.lblE3 = New System.Windows.Forms.Label
        Me.cboE2 = New System.Windows.Forms.ComboBox
        Me.lblE2 = New System.Windows.Forms.Label
        Me.cboE1 = New System.Windows.Forms.ComboBox
        Me.lblE1 = New System.Windows.Forms.Label
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.lblToPeriod = New System.Windows.Forms.Label
        Me.cboFromPeriod = New System.Windows.Forms.ComboBox
        Me.lblFromPeriod = New System.Windows.Forms.Label
        Me.lblQuarterValue = New System.Windows.Forms.Label
        Me.cboQuarterValue = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnKeywordsTC = New eZee.Common.eZeeGradientButton
        Me.lblTaxChargedFormula = New System.Windows.Forms.Label
        Me.txtTaxChargedFormula = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 466)
        Me.NavPanel.Size = New System.Drawing.Size(791, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnKeywordsTC)
        Me.gbFilterCriteria.Controls.Add(Me.lblTaxChargedFormula)
        Me.gbFilterCriteria.Controls.Add(Me.txtTaxChargedFormula)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnKeywordsBS)
        Me.gbFilterCriteria.Controls.Add(Me.lblBasicSalaryFormula)
        Me.gbFilterCriteria.Controls.Add(Me.txtBasicSalaryFormula)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTaxableIcome)
        Me.gbFilterCriteria.Controls.Add(Me.cboTaxableIcome)
        Me.gbFilterCriteria.Controls.Add(Me.lblTaxableIcome)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchInsuranceRelief)
        Me.gbFilterCriteria.Controls.Add(Me.cboInsuranceRelief)
        Me.gbFilterCriteria.Controls.Add(Me.lblInsuranceRelief)
        Me.gbFilterCriteria.Controls.Add(Me.lblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchQuarterValue)
        Me.gbFilterCriteria.Controls.Add(Me.objelLineOptional)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPersonalRelief)
        Me.gbFilterCriteria.Controls.Add(Me.cboPersonalRelief)
        Me.gbFilterCriteria.Controls.Add(Me.lblPersonalRelief)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTaxCharged)
        Me.gbFilterCriteria.Controls.Add(Me.cboTaxCharged)
        Me.gbFilterCriteria.Controls.Add(Me.lblTaxCharged)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAmountOfInterest)
        Me.gbFilterCriteria.Controls.Add(Me.cboAmountOfInterest)
        Me.gbFilterCriteria.Controls.Add(Me.lblAmountOfInterest)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchE3)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchE2)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchE1)
        Me.gbFilterCriteria.Controls.Add(Me.cboE3)
        Me.gbFilterCriteria.Controls.Add(Me.lblE3)
        Me.gbFilterCriteria.Controls.Add(Me.cboE2)
        Me.gbFilterCriteria.Controls.Add(Me.lblE2)
        Me.gbFilterCriteria.Controls.Add(Me.cboE1)
        Me.gbFilterCriteria.Controls.Add(Me.lblE1)
        Me.gbFilterCriteria.Controls.Add(Me.cboToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboFromPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblQuarterValue)
        Me.gbFilterCriteria.Controls.Add(Me.cboQuarterValue)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(643, 343)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnKeywordsBS
        '
        Me.objbtnKeywordsBS.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsBS.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsBS.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsBS.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsBS.BorderSelected = False
        Me.objbtnKeywordsBS.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsBS.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsBS.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsBS.Location = New System.Drawing.Point(608, 195)
        Me.objbtnKeywordsBS.Name = "objbtnKeywordsBS"
        Me.objbtnKeywordsBS.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsBS.TabIndex = 163
        '
        'lblBasicSalaryFormula
        '
        Me.lblBasicSalaryFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBasicSalaryFormula.Location = New System.Drawing.Point(8, 197)
        Me.lblBasicSalaryFormula.Name = "lblBasicSalaryFormula"
        Me.lblBasicSalaryFormula.Size = New System.Drawing.Size(107, 15)
        Me.lblBasicSalaryFormula.TabIndex = 79
        Me.lblBasicSalaryFormula.Text = "Basic Salary Formula"
        Me.lblBasicSalaryFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBasicSalaryFormula
        '
        Me.txtBasicSalaryFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBasicSalaryFormula.Location = New System.Drawing.Point(121, 195)
        Me.txtBasicSalaryFormula.Name = "txtBasicSalaryFormula"
        Me.txtBasicSalaryFormula.Size = New System.Drawing.Size(482, 21)
        Me.txtBasicSalaryFormula.TabIndex = 78
        '
        'objbtnSearchTaxableIcome
        '
        Me.objbtnSearchTaxableIcome.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxableIcome.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxableIcome.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxableIcome.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTaxableIcome.BorderSelected = False
        Me.objbtnSearchTaxableIcome.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTaxableIcome.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTaxableIcome.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTaxableIcome.Location = New System.Drawing.Point(609, 141)
        Me.objbtnSearchTaxableIcome.Name = "objbtnSearchTaxableIcome"
        Me.objbtnSearchTaxableIcome.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTaxableIcome.TabIndex = 76
        '
        'cboTaxableIcome
        '
        Me.cboTaxableIcome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTaxableIcome.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTaxableIcome.FormattingEnabled = True
        Me.cboTaxableIcome.Location = New System.Drawing.Point(436, 141)
        Me.cboTaxableIcome.Name = "cboTaxableIcome"
        Me.cboTaxableIcome.Size = New System.Drawing.Size(167, 21)
        Me.cboTaxableIcome.TabIndex = 75
        '
        'lblTaxableIcome
        '
        Me.lblTaxableIcome.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxableIcome.Location = New System.Drawing.Point(323, 143)
        Me.lblTaxableIcome.Name = "lblTaxableIcome"
        Me.lblTaxableIcome.Size = New System.Drawing.Size(107, 15)
        Me.lblTaxableIcome.TabIndex = 74
        Me.lblTaxableIcome.Text = "Taxable Icome"
        Me.lblTaxableIcome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(535, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 72
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchInsuranceRelief
        '
        Me.objbtnSearchInsuranceRelief.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchInsuranceRelief.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchInsuranceRelief.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchInsuranceRelief.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchInsuranceRelief.BorderSelected = False
        Me.objbtnSearchInsuranceRelief.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchInsuranceRelief.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchInsuranceRelief.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchInsuranceRelief.Location = New System.Drawing.Point(294, 168)
        Me.objbtnSearchInsuranceRelief.Name = "objbtnSearchInsuranceRelief"
        Me.objbtnSearchInsuranceRelief.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchInsuranceRelief.TabIndex = 35
        '
        'cboInsuranceRelief
        '
        Me.cboInsuranceRelief.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInsuranceRelief.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInsuranceRelief.FormattingEnabled = True
        Me.cboInsuranceRelief.Location = New System.Drawing.Point(121, 168)
        Me.cboInsuranceRelief.Name = "cboInsuranceRelief"
        Me.cboInsuranceRelief.Size = New System.Drawing.Size(167, 21)
        Me.cboInsuranceRelief.TabIndex = 34
        '
        'lblInsuranceRelief
        '
        Me.lblInsuranceRelief.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInsuranceRelief.Location = New System.Drawing.Point(8, 170)
        Me.lblInsuranceRelief.Name = "lblInsuranceRelief"
        Me.lblInsuranceRelief.Size = New System.Drawing.Size(107, 15)
        Me.lblInsuranceRelief.TabIndex = 33
        Me.lblInsuranceRelief.Text = "Insurance Relief"
        Me.lblInsuranceRelief.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(323, 172)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(108, 15)
        Me.lblMembership.TabIndex = 25
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 180
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(436, 168)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(167, 21)
        Me.cboMembership.TabIndex = 26
        '
        'objbtnSearchQuarterValue
        '
        Me.objbtnSearchQuarterValue.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchQuarterValue.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchQuarterValue.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchQuarterValue.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchQuarterValue.BorderSelected = False
        Me.objbtnSearchQuarterValue.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchQuarterValue.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchQuarterValue.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchQuarterValue.Location = New System.Drawing.Point(294, 60)
        Me.objbtnSearchQuarterValue.Name = "objbtnSearchQuarterValue"
        Me.objbtnSearchQuarterValue.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchQuarterValue.TabIndex = 6
        '
        'objelLineOptional
        '
        Me.objelLineOptional.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLineOptional.Location = New System.Drawing.Point(8, 291)
        Me.objelLineOptional.Name = "objelLineOptional"
        Me.objelLineOptional.Size = New System.Drawing.Size(595, 13)
        Me.objelLineOptional.TabIndex = 28
        Me.objelLineOptional.Text = "Optional Details"
        Me.objelLineOptional.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(121, 249)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 27
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'objbtnSearchPersonalRelief
        '
        Me.objbtnSearchPersonalRelief.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPersonalRelief.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPersonalRelief.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPersonalRelief.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPersonalRelief.BorderSelected = False
        Me.objbtnSearchPersonalRelief.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPersonalRelief.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPersonalRelief.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPersonalRelief.Location = New System.Drawing.Point(294, 141)
        Me.objbtnSearchPersonalRelief.Name = "objbtnSearchPersonalRelief"
        Me.objbtnSearchPersonalRelief.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPersonalRelief.TabIndex = 24
        '
        'cboPersonalRelief
        '
        Me.cboPersonalRelief.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPersonalRelief.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPersonalRelief.FormattingEnabled = True
        Me.cboPersonalRelief.Location = New System.Drawing.Point(121, 141)
        Me.cboPersonalRelief.Name = "cboPersonalRelief"
        Me.cboPersonalRelief.Size = New System.Drawing.Size(167, 21)
        Me.cboPersonalRelief.TabIndex = 23
        '
        'lblPersonalRelief
        '
        Me.lblPersonalRelief.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalRelief.Location = New System.Drawing.Point(8, 143)
        Me.lblPersonalRelief.Name = "lblPersonalRelief"
        Me.lblPersonalRelief.Size = New System.Drawing.Size(107, 15)
        Me.lblPersonalRelief.TabIndex = 22
        Me.lblPersonalRelief.Text = "Personal Relief"
        Me.lblPersonalRelief.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchTaxCharged
        '
        Me.objbtnSearchTaxCharged.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxCharged.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxCharged.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxCharged.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTaxCharged.BorderSelected = False
        Me.objbtnSearchTaxCharged.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTaxCharged.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTaxCharged.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTaxCharged.Location = New System.Drawing.Point(609, 114)
        Me.objbtnSearchTaxCharged.Name = "objbtnSearchTaxCharged"
        Me.objbtnSearchTaxCharged.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTaxCharged.TabIndex = 21
        '
        'cboTaxCharged
        '
        Me.cboTaxCharged.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTaxCharged.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTaxCharged.FormattingEnabled = True
        Me.cboTaxCharged.Location = New System.Drawing.Point(436, 114)
        Me.cboTaxCharged.Name = "cboTaxCharged"
        Me.cboTaxCharged.Size = New System.Drawing.Size(167, 21)
        Me.cboTaxCharged.TabIndex = 20
        '
        'lblTaxCharged
        '
        Me.lblTaxCharged.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxCharged.Location = New System.Drawing.Point(323, 116)
        Me.lblTaxCharged.Name = "lblTaxCharged"
        Me.lblTaxCharged.Size = New System.Drawing.Size(107, 15)
        Me.lblTaxCharged.TabIndex = 19
        Me.lblTaxCharged.Text = "Tax Charged"
        Me.lblTaxCharged.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchAmountOfInterest
        '
        Me.objbtnSearchAmountOfInterest.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAmountOfInterest.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAmountOfInterest.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAmountOfInterest.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAmountOfInterest.BorderSelected = False
        Me.objbtnSearchAmountOfInterest.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAmountOfInterest.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAmountOfInterest.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAmountOfInterest.Location = New System.Drawing.Point(294, 114)
        Me.objbtnSearchAmountOfInterest.Name = "objbtnSearchAmountOfInterest"
        Me.objbtnSearchAmountOfInterest.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAmountOfInterest.TabIndex = 18
        '
        'cboAmountOfInterest
        '
        Me.cboAmountOfInterest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAmountOfInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAmountOfInterest.FormattingEnabled = True
        Me.cboAmountOfInterest.Location = New System.Drawing.Point(121, 114)
        Me.cboAmountOfInterest.Name = "cboAmountOfInterest"
        Me.cboAmountOfInterest.Size = New System.Drawing.Size(167, 21)
        Me.cboAmountOfInterest.TabIndex = 17
        '
        'lblAmountOfInterest
        '
        Me.lblAmountOfInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmountOfInterest.Location = New System.Drawing.Point(8, 116)
        Me.lblAmountOfInterest.Name = "lblAmountOfInterest"
        Me.lblAmountOfInterest.Size = New System.Drawing.Size(107, 15)
        Me.lblAmountOfInterest.TabIndex = 16
        Me.lblAmountOfInterest.Text = "Amount Of Interest"
        Me.lblAmountOfInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchE3
        '
        Me.objbtnSearchE3.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchE3.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchE3.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchE3.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchE3.BorderSelected = False
        Me.objbtnSearchE3.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchE3.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchE3.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchE3.Location = New System.Drawing.Point(609, 87)
        Me.objbtnSearchE3.Name = "objbtnSearchE3"
        Me.objbtnSearchE3.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchE3.TabIndex = 15
        '
        'objbtnSearchE2
        '
        Me.objbtnSearchE2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchE2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchE2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchE2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchE2.BorderSelected = False
        Me.objbtnSearchE2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchE2.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchE2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchE2.Location = New System.Drawing.Point(294, 87)
        Me.objbtnSearchE2.Name = "objbtnSearchE2"
        Me.objbtnSearchE2.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchE2.TabIndex = 12
        '
        'objbtnSearchE1
        '
        Me.objbtnSearchE1.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchE1.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchE1.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchE1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchE1.BorderSelected = False
        Me.objbtnSearchE1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchE1.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchE1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchE1.Location = New System.Drawing.Point(609, 60)
        Me.objbtnSearchE1.Name = "objbtnSearchE1"
        Me.objbtnSearchE1.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchE1.TabIndex = 9
        '
        'cboE3
        '
        Me.cboE3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboE3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboE3.FormattingEnabled = True
        Me.cboE3.Location = New System.Drawing.Point(436, 87)
        Me.cboE3.Name = "cboE3"
        Me.cboE3.Size = New System.Drawing.Size(167, 21)
        Me.cboE3.TabIndex = 14
        '
        'lblE3
        '
        Me.lblE3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblE3.Location = New System.Drawing.Point(323, 89)
        Me.lblE3.Name = "lblE3"
        Me.lblE3.Size = New System.Drawing.Size(107, 15)
        Me.lblE3.TabIndex = 13
        Me.lblE3.Text = "E3"
        Me.lblE3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboE2
        '
        Me.cboE2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboE2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboE2.FormattingEnabled = True
        Me.cboE2.Location = New System.Drawing.Point(121, 87)
        Me.cboE2.Name = "cboE2"
        Me.cboE2.Size = New System.Drawing.Size(167, 21)
        Me.cboE2.TabIndex = 11
        '
        'lblE2
        '
        Me.lblE2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblE2.Location = New System.Drawing.Point(8, 89)
        Me.lblE2.Name = "lblE2"
        Me.lblE2.Size = New System.Drawing.Size(107, 15)
        Me.lblE2.TabIndex = 10
        Me.lblE2.Text = "E2"
        Me.lblE2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboE1
        '
        Me.cboE1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboE1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboE1.FormattingEnabled = True
        Me.cboE1.Location = New System.Drawing.Point(436, 60)
        Me.cboE1.Name = "cboE1"
        Me.cboE1.Size = New System.Drawing.Size(167, 21)
        Me.cboE1.TabIndex = 8
        '
        'lblE1
        '
        Me.lblE1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblE1.Location = New System.Drawing.Point(323, 62)
        Me.lblE1.Name = "lblE1"
        Me.lblE1.Size = New System.Drawing.Size(107, 15)
        Me.lblE1.TabIndex = 7
        Me.lblE1.Text = "E1"
        Me.lblE1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToPeriod
        '
        Me.cboToPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(436, 33)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(167, 21)
        Me.cboToPeriod.TabIndex = 3
        '
        'lblToPeriod
        '
        Me.lblToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToPeriod.Location = New System.Drawing.Point(323, 36)
        Me.lblToPeriod.Name = "lblToPeriod"
        Me.lblToPeriod.Size = New System.Drawing.Size(107, 15)
        Me.lblToPeriod.TabIndex = 2
        Me.lblToPeriod.Text = "To Period"
        Me.lblToPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFromPeriod
        '
        Me.cboFromPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromPeriod.FormattingEnabled = True
        Me.cboFromPeriod.Location = New System.Drawing.Point(121, 33)
        Me.cboFromPeriod.Name = "cboFromPeriod"
        Me.cboFromPeriod.Size = New System.Drawing.Size(167, 21)
        Me.cboFromPeriod.TabIndex = 1
        '
        'lblFromPeriod
        '
        Me.lblFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblFromPeriod.Name = "lblFromPeriod"
        Me.lblFromPeriod.Size = New System.Drawing.Size(107, 15)
        Me.lblFromPeriod.TabIndex = 0
        Me.lblFromPeriod.Text = "From Period"
        Me.lblFromPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblQuarterValue
        '
        Me.lblQuarterValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQuarterValue.Location = New System.Drawing.Point(8, 62)
        Me.lblQuarterValue.Name = "lblQuarterValue"
        Me.lblQuarterValue.Size = New System.Drawing.Size(107, 15)
        Me.lblQuarterValue.TabIndex = 4
        Me.lblQuarterValue.Text = "Quarter Value"
        Me.lblQuarterValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboQuarterValue
        '
        Me.cboQuarterValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQuarterValue.DropDownWidth = 180
        Me.cboQuarterValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQuarterValue.FormattingEnabled = True
        Me.cboQuarterValue.Location = New System.Drawing.Point(121, 60)
        Me.cboQuarterValue.Name = "cboQuarterValue"
        Me.cboQuarterValue.Size = New System.Drawing.Size(167, 21)
        Me.cboQuarterValue.TabIndex = 5
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(294, 307)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 31
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(121, 307)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(167, 21)
        Me.cboEmployee.TabIndex = 30
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 274)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(107, 15)
        Me.lblEmployee.TabIndex = 29
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnKeywordsTC
        '
        Me.objbtnKeywordsTC.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsTC.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsTC.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsTC.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsTC.BorderSelected = False
        Me.objbtnKeywordsTC.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsTC.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsTC.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsTC.Location = New System.Drawing.Point(608, 222)
        Me.objbtnKeywordsTC.Name = "objbtnKeywordsTC"
        Me.objbtnKeywordsTC.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsTC.TabIndex = 167
        '
        'lblTaxChargedFormula
        '
        Me.lblTaxChargedFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxChargedFormula.Location = New System.Drawing.Point(8, 224)
        Me.lblTaxChargedFormula.Name = "lblTaxChargedFormula"
        Me.lblTaxChargedFormula.Size = New System.Drawing.Size(110, 15)
        Me.lblTaxChargedFormula.TabIndex = 166
        Me.lblTaxChargedFormula.Text = "Tax Charged Formula"
        Me.lblTaxChargedFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTaxChargedFormula
        '
        Me.txtTaxChargedFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTaxChargedFormula.Location = New System.Drawing.Point(121, 222)
        Me.txtTaxChargedFormula.Name = "txtTaxChargedFormula"
        Me.txtTaxChargedFormula.Size = New System.Drawing.Size(482, 21)
        Me.txtTaxChargedFormula.TabIndex = 165
        '
        'frmP9AReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(791, 521)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmP9AReport"
        Me.Text = "frmP9AReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblToPeriod As System.Windows.Forms.Label
    Friend WithEvents cboFromPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblFromPeriod As System.Windows.Forms.Label
    Friend WithEvents lblQuarterValue As System.Windows.Forms.Label
    Friend WithEvents cboQuarterValue As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboE3 As System.Windows.Forms.ComboBox
    Friend WithEvents lblE3 As System.Windows.Forms.Label
    Friend WithEvents cboE2 As System.Windows.Forms.ComboBox
    Friend WithEvents lblE2 As System.Windows.Forms.Label
    Friend WithEvents cboE1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblE1 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchE3 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchE2 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchE1 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchAmountOfInterest As eZee.Common.eZeeGradientButton
    Friend WithEvents cboAmountOfInterest As System.Windows.Forms.ComboBox
    Friend WithEvents lblAmountOfInterest As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTaxCharged As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTaxCharged As System.Windows.Forms.ComboBox
    Friend WithEvents lblTaxCharged As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchPersonalRelief As eZee.Common.eZeeGradientButton
    Friend WithEvents cboPersonalRelief As System.Windows.Forms.ComboBox
    Friend WithEvents lblPersonalRelief As System.Windows.Forms.Label
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents objelLineOptional As eZee.Common.eZeeLine
    Friend WithEvents objbtnSearchQuarterValue As eZee.Common.eZeeGradientButton
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchInsuranceRelief As eZee.Common.eZeeGradientButton
    Friend WithEvents cboInsuranceRelief As System.Windows.Forms.ComboBox
    Friend WithEvents lblInsuranceRelief As System.Windows.Forms.Label
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchTaxableIcome As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTaxableIcome As System.Windows.Forms.ComboBox
    Friend WithEvents lblTaxableIcome As System.Windows.Forms.Label
    Friend WithEvents lblBasicSalaryFormula As System.Windows.Forms.Label
    Friend WithEvents txtBasicSalaryFormula As System.Windows.Forms.TextBox
    Friend WithEvents objbtnKeywordsBS As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnKeywordsTC As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTaxChargedFormula As System.Windows.Forms.Label
    Friend WithEvents txtTaxChargedFormula As System.Windows.Forms.TextBox
End Class
