﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmP10AP10DReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmP10AP10DReport))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTaxOn = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchFringeBenefit = New eZee.Common.eZeeGradientButton
        Me.cboTaxOn = New System.Windows.Forms.ComboBox
        Me.lblTaxOn = New System.Windows.Forms.Label
        Me.cboFringeBenefit = New System.Windows.Forms.ComboBox
        Me.lblFringeBenefit = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboSlabEffectivePeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblPAYE = New System.Windows.Forms.Label
        Me.cboPAYE = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objchkAllPeriod = New System.Windows.Forms.CheckBox
        Me.lvPeriod = New eZee.Common.eZeeListView(Me.components)
        Me.objColhPCheck = New System.Windows.Forms.ColumnHeader
        Me.colhPeriodName = New System.Windows.Forms.ColumnHeader
        Me.colhYear = New System.Windows.Forms.ColumnHeader
        Me.objcolhStartDate = New System.Windows.Forms.ColumnHeader
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbBasicSalaryOtherEarning = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.objbtnSearchOtherEarning = New eZee.Common.eZeeGradientButton
        Me.cboOtherEarning = New System.Windows.Forms.ComboBox
        Me.lblOtherEarning = New System.Windows.Forms.Label
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbBasicSalaryOtherEarning.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 563)
        Me.NavPanel.Size = New System.Drawing.Size(688, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTaxOn)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchFringeBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.cboTaxOn)
        Me.gbFilterCriteria.Controls.Add(Me.lblTaxOn)
        Me.gbFilterCriteria.Controls.Add(Me.cboFringeBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.lblFringeBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.lblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.cboSlabEffectivePeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPAYE)
        Me.gbFilterCriteria.Controls.Add(Me.cboPAYE)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.objchkAllPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lvPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(658, 300)
        Me.gbFilterCriteria.TabIndex = 86
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReportType
        '
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(79, 15)
        Me.lblReportType.TabIndex = 95
        Me.lblReportType.Text = "Report Type"
        Me.lblReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 180
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(93, 33)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(203, 21)
        Me.cboReportType.TabIndex = 94
        '
        'objbtnSearchTaxOn
        '
        Me.objbtnSearchTaxOn.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxOn.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxOn.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxOn.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTaxOn.BorderSelected = False
        Me.objbtnSearchTaxOn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTaxOn.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTaxOn.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTaxOn.Location = New System.Drawing.Point(302, 168)
        Me.objbtnSearchTaxOn.Name = "objbtnSearchTaxOn"
        Me.objbtnSearchTaxOn.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTaxOn.TabIndex = 93
        '
        'objbtnSearchFringeBenefit
        '
        Me.objbtnSearchFringeBenefit.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFringeBenefit.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFringeBenefit.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFringeBenefit.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFringeBenefit.BorderSelected = False
        Me.objbtnSearchFringeBenefit.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFringeBenefit.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFringeBenefit.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFringeBenefit.Location = New System.Drawing.Point(302, 141)
        Me.objbtnSearchFringeBenefit.Name = "objbtnSearchFringeBenefit"
        Me.objbtnSearchFringeBenefit.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFringeBenefit.TabIndex = 92
        '
        'cboTaxOn
        '
        Me.cboTaxOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTaxOn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTaxOn.FormattingEnabled = True
        Me.cboTaxOn.Location = New System.Drawing.Point(93, 168)
        Me.cboTaxOn.Name = "cboTaxOn"
        Me.cboTaxOn.Size = New System.Drawing.Size(203, 21)
        Me.cboTaxOn.TabIndex = 90
        '
        'lblTaxOn
        '
        Me.lblTaxOn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxOn.Location = New System.Drawing.Point(8, 171)
        Me.lblTaxOn.Name = "lblTaxOn"
        Me.lblTaxOn.Size = New System.Drawing.Size(79, 56)
        Me.lblTaxOn.TabIndex = 89
        Me.lblTaxOn.Text = "Tax On LUMPSUMS/AUDIT/PENALTY/INTERST"
        Me.lblTaxOn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFringeBenefit
        '
        Me.cboFringeBenefit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFringeBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFringeBenefit.FormattingEnabled = True
        Me.cboFringeBenefit.Location = New System.Drawing.Point(93, 141)
        Me.cboFringeBenefit.Name = "cboFringeBenefit"
        Me.cboFringeBenefit.Size = New System.Drawing.Size(203, 21)
        Me.cboFringeBenefit.TabIndex = 88
        '
        'lblFringeBenefit
        '
        Me.lblFringeBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFringeBenefit.Location = New System.Drawing.Point(8, 144)
        Me.lblFringeBenefit.Name = "lblFringeBenefit"
        Me.lblFringeBenefit.Size = New System.Drawing.Size(79, 15)
        Me.lblFringeBenefit.TabIndex = 87
        Me.lblFringeBenefit.Text = "Fringe Benefit"
        Me.lblFringeBenefit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(93, 114)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(203, 21)
        Me.cboMembership.TabIndex = 85
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(8, 117)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(79, 15)
        Me.lblMembership.TabIndex = 84
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSlabEffectivePeriod
        '
        Me.cboSlabEffectivePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSlabEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSlabEffectivePeriod.FormattingEnabled = True
        Me.cboSlabEffectivePeriod.Location = New System.Drawing.Point(446, 33)
        Me.cboSlabEffectivePeriod.Name = "cboSlabEffectivePeriod"
        Me.cboSlabEffectivePeriod.Size = New System.Drawing.Size(195, 21)
        Me.cboSlabEffectivePeriod.TabIndex = 83
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(337, 37)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(103, 15)
        Me.lblPeriod.TabIndex = 82
        Me.lblPeriod.Text = "Effective Periods"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPAYE
        '
        Me.lblPAYE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPAYE.Location = New System.Drawing.Point(8, 63)
        Me.lblPAYE.Name = "lblPAYE"
        Me.lblPAYE.Size = New System.Drawing.Size(79, 15)
        Me.lblPAYE.TabIndex = 81
        Me.lblPAYE.Text = "P. A. Y. E."
        Me.lblPAYE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPAYE
        '
        Me.cboPAYE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPAYE.DropDownWidth = 180
        Me.cboPAYE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPAYE.FormattingEnabled = True
        Me.cboPAYE.Location = New System.Drawing.Point(93, 60)
        Me.cboPAYE.Name = "cboPAYE"
        Me.cboPAYE.Size = New System.Drawing.Size(203, 21)
        Me.cboPAYE.TabIndex = 80
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(553, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 78
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objchkAllPeriod
        '
        Me.objchkAllPeriod.AutoSize = True
        Me.objchkAllPeriod.Location = New System.Drawing.Point(414, 67)
        Me.objchkAllPeriod.Name = "objchkAllPeriod"
        Me.objchkAllPeriod.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllPeriod.TabIndex = 4
        Me.objchkAllPeriod.UseVisualStyleBackColor = True
        '
        'lvPeriod
        '
        Me.lvPeriod.BackColorOnChecked = False
        Me.lvPeriod.CheckBoxes = True
        Me.lvPeriod.ColumnHeaders = Nothing
        Me.lvPeriod.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhPCheck, Me.colhPeriodName, Me.colhYear, Me.objcolhStartDate})
        Me.lvPeriod.CompulsoryColumns = ""
        Me.lvPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPeriod.FullRowSelect = True
        Me.lvPeriod.GridLines = True
        Me.lvPeriod.GroupingColumn = Nothing
        Me.lvPeriod.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPeriod.HideSelection = False
        Me.lvPeriod.Location = New System.Drawing.Point(407, 63)
        Me.lvPeriod.MinColumnWidth = 50
        Me.lvPeriod.MultiSelect = False
        Me.lvPeriod.Name = "lvPeriod"
        Me.lvPeriod.OptionalColumns = ""
        Me.lvPeriod.ShowMoreItem = False
        Me.lvPeriod.ShowSaveItem = False
        Me.lvPeriod.ShowSelectAll = True
        Me.lvPeriod.ShowSizeAllColumnsToFit = True
        Me.lvPeriod.Size = New System.Drawing.Size(234, 229)
        Me.lvPeriod.Sortable = True
        Me.lvPeriod.TabIndex = 75
        Me.lvPeriod.UseCompatibleStateImageBehavior = False
        Me.lvPeriod.View = System.Windows.Forms.View.Details
        '
        'objColhPCheck
        '
        Me.objColhPCheck.Tag = "objColhPCheck"
        Me.objColhPCheck.Text = ""
        Me.objColhPCheck.Width = 25
        '
        'colhPeriodName
        '
        Me.colhPeriodName.Tag = "colhPeriodName"
        Me.colhPeriodName.Text = "Periods"
        Me.colhPeriodName.Width = 205
        '
        'colhYear
        '
        Me.colhYear.Text = ""
        Me.colhYear.Width = 0
        '
        'objcolhStartDate
        '
        Me.objcolhStartDate.Width = 0
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(302, 87)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 56
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(93, 87)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(203, 21)
        Me.cboEmployee.TabIndex = 55
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 90)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(79, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbBasicSalaryOtherEarning
        '
        Me.gbBasicSalaryOtherEarning.BorderColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.Checked = False
        Me.gbBasicSalaryOtherEarning.CollapseAllExceptThis = False
        Me.gbBasicSalaryOtherEarning.CollapsedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapseOnLoad = False
        Me.gbBasicSalaryOtherEarning.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbBasicSalaryOtherEarning.ExpandedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicSalaryOtherEarning.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicSalaryOtherEarning.HeaderHeight = 25
        Me.gbBasicSalaryOtherEarning.HeaderMessage = ""
        Me.gbBasicSalaryOtherEarning.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBasicSalaryOtherEarning.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.HeightOnCollapse = 0
        Me.gbBasicSalaryOtherEarning.LeftTextSpace = 0
        Me.gbBasicSalaryOtherEarning.Location = New System.Drawing.Point(12, 372)
        Me.gbBasicSalaryOtherEarning.Name = "gbBasicSalaryOtherEarning"
        Me.gbBasicSalaryOtherEarning.OpenHeight = 300
        Me.gbBasicSalaryOtherEarning.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicSalaryOtherEarning.ShowBorder = True
        Me.gbBasicSalaryOtherEarning.ShowCheckBox = True
        Me.gbBasicSalaryOtherEarning.ShowCollapseButton = False
        Me.gbBasicSalaryOtherEarning.ShowDefaultBorderColor = True
        Me.gbBasicSalaryOtherEarning.ShowDownButton = False
        Me.gbBasicSalaryOtherEarning.ShowHeader = True
        Me.gbBasicSalaryOtherEarning.Size = New System.Drawing.Size(658, 61)
        Me.gbBasicSalaryOtherEarning.TabIndex = 87
        Me.gbBasicSalaryOtherEarning.Temp = 0
        Me.gbBasicSalaryOtherEarning.Text = "Basic Salary As Other Earning"
        Me.gbBasicSalaryOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.objbtnSearchOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.cboOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblOtherEarning)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(318, 33)
        Me.pnlAdvanceTaken.TabIndex = 259
        '
        'objbtnSearchOtherEarning
        '
        Me.objbtnSearchOtherEarning.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOtherEarning.BorderSelected = False
        Me.objbtnSearchOtherEarning.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOtherEarning.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOtherEarning.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOtherEarning.Location = New System.Drawing.Point(289, 3)
        Me.objbtnSearchOtherEarning.Name = "objbtnSearchOtherEarning"
        Me.objbtnSearchOtherEarning.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOtherEarning.TabIndex = 57
        '
        'cboOtherEarning
        '
        Me.cboOtherEarning.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEarning.FormattingEnabled = True
        Me.cboOtherEarning.Location = New System.Drawing.Point(97, 3)
        Me.cboOtherEarning.Name = "cboOtherEarning"
        Me.cboOtherEarning.Size = New System.Drawing.Size(186, 21)
        Me.cboOtherEarning.TabIndex = 56
        '
        'lblOtherEarning
        '
        Me.lblOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherEarning.Location = New System.Drawing.Point(8, 8)
        Me.lblOtherEarning.Name = "lblOtherEarning"
        Me.lblOtherEarning.Size = New System.Drawing.Size(83, 16)
        Me.lblOtherEarning.TabIndex = 0
        Me.lblOtherEarning.Text = "Other Earning"
        Me.lblOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(93, 239)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 97
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'frmP10AP10DReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(688, 618)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbBasicSalaryOtherEarning)
        Me.Name = "frmP10AP10DReport"
        Me.Text = "frmP10DReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbBasicSalaryOtherEarning, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbBasicSalaryOtherEarning.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboSlabEffectivePeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblPAYE As System.Windows.Forms.Label
    Friend WithEvents cboPAYE As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents objchkAllPeriod As System.Windows.Forms.CheckBox
    Friend WithEvents lvPeriod As eZee.Common.eZeeListView
    Friend WithEvents objColhPCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriodName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents gbBasicSalaryOtherEarning As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchOtherEarning As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOtherEarning As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherEarning As System.Windows.Forms.Label
    Friend WithEvents cboTaxOn As System.Windows.Forms.ComboBox
    Friend WithEvents lblTaxOn As System.Windows.Forms.Label
    Friend WithEvents cboFringeBenefit As System.Windows.Forms.ComboBox
    Friend WithEvents lblFringeBenefit As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTaxOn As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchFringeBenefit As eZee.Common.eZeeGradientButton
    Friend WithEvents colhYear As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblReportType As System.Windows.Forms.Label
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents objcolhStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
End Class
