'************************************************************************************************************************************
'Class Name : clsP11Report.vb
'Purpose    :
'Date       :12/08/2013
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsP11Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsP11Report"
    Private mstrReportId As String = enArutiReport.PAYE_P11_Report

    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    'For Report
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private mintPayeHeadId As Integer = 0
    Private mstrPayeHeadName As String = ""

    Private mblnIncludeInactiveEmp As Boolean = True

    'For Analysis By
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadId() As Integer
        Set(ByVal value As Integer)
            mintPayeHeadId = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadName() As String
        Set(ByVal value As String)
            mstrPayeHeadName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintEmpId = 0
            mstrEmpName = ""
            mstrPeriodIds = ""
            mintPayeHeadId = 0
            mstrPayeHeadName = ""

            mblnIncludeInactiveEmp = True


            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@payetranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayeHeadId)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try

        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBaseCurrencySign As String
        Dim decTotalPaye As Decimal = 0

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ = "SELECT    SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS PAYE " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @payetranheadunkid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END



            StrQ &= "                               HAVING    SUM(ISNULL(prpayrollprocess_tran.amount, 0)) <> 0 "



            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery


            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                decTotalPaye = CDec(dsList.Tables(0).Rows(0).Item("PAYE"))
            End If

            StrQ = "SELECT 0 AS id, 'Shs. 1000/=' AS name, 1000 AS denomination, 0.00 AS Amount " & _
                    "UNION SELECT 1 AS id, '" & Language.getMessage(mstrModuleName, 1, "Shs.") & " 500/=' AS name,  500 AS denomination, 0.00 AS Amount " & _
                    "UNION SELECT 2 AS id, '" & Language.getMessage(mstrModuleName, 1, "Shs.") & " 200/=' AS name, 200 AS denomination, 0.00 AS Amount " & _
                    "UNION SELECT 3 AS id, '" & Language.getMessage(mstrModuleName, 1, "Shs.") & " 100/=' AS name, 100 AS denomination, 0.00 AS Amount " & _
                    "UNION SELECT 4 AS id, '" & Language.getMessage(mstrModuleName, 1, "Shs.") & " 50/=' AS name, 50 AS denomination, 0.00 AS Amount " & _
                    "UNION SELECT 5 AS id, '" & Language.getMessage(mstrModuleName, 1, "Shs.") & " 20/=' AS name, 20 AS denomination, 0.00 AS Amount " & _
                    "UNION SELECT 6 AS id, '" & Language.getMessage(mstrModuleName, 1, "Shs.") & " 10/=' AS name, 10 AS denomination, 0.00 AS Amount " & _
                    "UNION SELECT 7 AS id, '" & Language.getMessage(mstrModuleName, 1, "Shs.") & " 5/=' AS name, 5 AS denomination, 0.00 AS Amount " & _
                    "ORDER BY id "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim decOtherCoins As Decimal = decTotalPaye
            For Each dsRow As DataRow In dsList.Tables(0).Rows
                If decOtherCoins >= CDec(dsRow.Item("denomination")) Then
                    dsRow.Item("Amount") = (CDec(dsRow.Item("denomination")) * Math.Floor(decOtherCoins / CDec(dsRow.Item("denomination"))))
                    decOtherCoins -= CDec(dsRow.Item("Amount"))
                End If
            Next


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = ""
                rpt_Row.Item("Column2") = dtRow.Item("name")
                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                rpt_Row.Item("Column4") = ""
                rpt_Row.Item("Column5") = ""


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptKY_P11

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 2, "ISO 9001:2008 CERTIFIED"))
            ReportFunction.TextChange(objRpt, "lblCaption2", Language.getMessage(mstrModuleName, 3, "DOMESTIC TAXES DEPARTMENT"))
            ReportFunction.TextChange(objRpt, "lblAppendix", Language.getMessage(mstrModuleName, 4, "APPENDIX 6"))
            ReportFunction.TextChange(objRpt, "lblCaption3", Language.getMessage(mstrModuleName, 5, "P.A.Y.E/FRING BENEFIT TAX CREDIT SLIP"))
            ReportFunction.TextChange(objRpt, "lblSrNo", Language.getMessage(mstrModuleName, 6, "SERIAL NO"))
            ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 7, ""))
            ReportFunction.TextChange(objRpt, "lblPayrollMonth", Language.getMessage(mstrModuleName, 8, "Pay Roll Month"))
            ReportFunction.TextChange(objRpt, "txtMonth", mstrPeriodNames.ToString)
            ReportFunction.TextChange(objRpt, "lblEmployerPin", Language.getMessage(mstrModuleName, 9, "EMPLOYER'S PIN"))
            ReportFunction.TextChange(objRpt, "txtEmplrPIN", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt, "lblEmplrName", Language.getMessage(mstrModuleName, 10, "Employer's Name"))
            ReportFunction.TextChange(objRpt, "txtEmplrName", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "lblTo", Language.getMessage(mstrModuleName, 12, "To"))
            ReportFunction.TextChange(objRpt, "txtBankName", Language.getMessage(mstrModuleName, 13, ""))
            ReportFunction.TextChange(objRpt, "lblBank", Language.getMessage(mstrModuleName, 14, "Bank"))
            ReportFunction.TextChange(objRpt, "lblText1", Language.getMessage(mstrModuleName, 15, "Please pay the Central Bank Of Kenya, for the credit of the " & Chr(10) & "Paymaster-General" & Chr(10) & ", PAYE account "))
            ReportFunction.TextChange(objRpt, "lblAccNo", Language.getMessage(mstrModuleName, 16, "No. 05-010-0056"))
            ReportFunction.TextChange(objRpt, "lblCurrency", strBaseCurrencySign)
            ReportFunction.TextChange(objRpt, "txtAmount", Format(decTotalPaye, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "lblInwords", Language.getMessage(mstrModuleName, 17, "(In words) Kenya Shillings"))
            If decTotalPaye > 0 Then
                Dim objDashboard As New clsDashboard_Class
                Dim strNumToWords As String = objDashboard.NumToWord(CInt(decTotalPaye)).ToString
                Dim sword As String = decTotalPaye.ToString.Substring(InStr(decTotalPaye.ToString, "."))
                If CInt(sword) > 0 Then
                    strNumToWords += " AND " & sword & " / 1" & New String("0", sword.Length)
                End If
                ReportFunction.TextChange(objRpt, "txtAmtWord1", strNumToWords)
                ReportFunction.TextChange(objRpt, "txtAmtWord2", "")
            Else
                ReportFunction.TextChange(objRpt, "txtAmtWord1", "")
                ReportFunction.TextChange(objRpt, "txtAmtWord2", "")
            End If
            ReportFunction.TextChange(objRpt, "lblNatureOfPayment", Language.getMessage(mstrModuleName, 18, "NATURE OF PAYMENT"))
            ReportFunction.TextChange(objRpt, "txtNOP1", Language.getMessage(mstrModuleName, 19, ""))
            ReportFunction.TextChange(objRpt, "txtNOP3", Language.getMessage(mstrModuleName, 20, ""))
            ReportFunction.TextChange(objRpt, "lblNoteCoins", Language.getMessage(mstrModuleName, 21, "NOTES/COINS"))
            ReportFunction.TextChange(objRpt, "lblPayeTax", Language.getMessage(mstrModuleName, 22, "1. PAYE TAX KSHS"))
            ReportFunction.TextChange(objRpt, "lblPenalty", Language.getMessage(mstrModuleName, 23, "2. PAYE INTEREST/PENALTY KSHS."))
            ReportFunction.TextChange(objRpt, "lblOthers", Language.getMessage(mstrModuleName, 24, "OTHERS(WCPS. ETC) KSHS"))
            ReportFunction.TextChange(objRpt, "txtBankStamp", Language.getMessage(mstrModuleName, 25, "Bank Stamp & Tellers Initials"))
            ReportFunction.TextChange(objRpt, "lblOtherCoins", Language.getMessage(mstrModuleName, 26, "OTHER COINS"))
            ReportFunction.TextChange(objRpt, "txtOtherCoins", Format(decOtherCoins, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "lblChequeNo", Language.getMessage(mstrModuleName, 27, "CHEQUE NO."))
            ReportFunction.TextChange(objRpt, "txtChequeNo1", Language.getMessage(mstrModuleName, 28, ""))
            ReportFunction.TextChange(objRpt, "txtChequeNo2", Language.getMessage(mstrModuleName, 29, ""))
            ReportFunction.TextChange(objRpt, "lblTotal", Language.getMessage(mstrModuleName, 30, "TOTAL CREDITS"))
            ReportFunction.TextChange(objRpt, "txtTotCol3", Format(decTotalPaye, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtTotCol4", Format(0, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtTotCol5", Format(0, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "lblOfficer", Language.getMessage(mstrModuleName, 31, "To Officer in Charge"))
            ReportFunction.TextChange(objRpt, "lblDomesticTax", Language.getMessage(mstrModuleName, 32, "Domestic Tax Office"))
            ReportFunction.TextChange(objRpt, "lblA", Language.getMessage(mstrModuleName, 33, "(A)"))
            ReportFunction.TextChange(objRpt, "lblPayeCertificate", Language.getMessage(mstrModuleName, 34, "PAYING-IN CERTIFICATE"))
            ReportFunction.TextChange(objRpt, "lblText2", Language.getMessage(mstrModuleName, 35, "We/I certifiy that the above represent :-"))
            ReportFunction.TextChange(objRpt, "lblText3", Language.getMessage(mstrModuleName, 36, "The full amount of the tax which was required to be deducted for the month shown above and includes any amounts brought forward from the previous months  for which the cumulative total was less than 100/="))
            ReportFunction.TextChange(objRpt, "lblB", Language.getMessage(mstrModuleName, 37, "(B)"))
            ReportFunction.TextChange(objRpt, "lblNilCertificate", Language.getMessage(mstrModuleName, 38, "NIL CERTIFICATE"))
            ReportFunction.TextChange(objRpt, "lblText4", Language.getMessage(mstrModuleName, 39, "We/I certifiy that for the month shown above the amount of PAYE/FRINGE BENEFIT TAX required to be deducted was Nil, or less than shs. 100/= which sum is being carried forward."))
            ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 40, "EMPLOYER'S NAME"))
            ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "lblPostalAddress", Language.getMessage(mstrModuleName, 41, "Postal Address:"))
            ReportFunction.TextChange(objRpt, "txtAddress", Company._Object._Address1 & " " & Company._Object._Address2 & " " & Company._Object._City_Name & " " & Company._Object._State_Name & " " & Company._Object._Post_Code_No)
            ReportFunction.TextChange(objRpt, "lblTelephone", Language.getMessage(mstrModuleName, 42, "TELEPHONE"))
            ReportFunction.TextChange(objRpt, "txtTelephone", Company._Object._Phone1)
            ReportFunction.TextChange(objRpt, "lblPayeOfficer", Language.getMessage(mstrModuleName, 43, "NAME OF PAYING OFFICER"))
            ReportFunction.TextChange(objRpt, "txtPayeOfficer", Language.getMessage(mstrModuleName, 44, ""))
            ReportFunction.TextChange(objRpt, "lblSignature", Language.getMessage(mstrModuleName, 45, "SIGNATURE"))
            ReportFunction.TextChange(objRpt, "txtSignature", Language.getMessage(mstrModuleName, 46, ""))
            ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 47, "DATE"))
            ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 48, ""))
            ReportFunction.TextChange(objRpt, "lblBankNote", Language.getMessage(mstrModuleName, 49, "NOTE TO ACCEPTING BANK"))
            ReportFunction.TextChange(objRpt, "lblOriginal", Language.getMessage(mstrModuleName, 50, "Original"))
            ReportFunction.TextChange(objRpt, "lblBankNote1", Language.getMessage(mstrModuleName, 51, "Domestic Tax Department"))
            ReportFunction.TextChange(objRpt, "lblDuplicate", Language.getMessage(mstrModuleName, 52, "Duplicate"))
            ReportFunction.TextChange(objRpt, "lblBankNote2", Language.getMessage(mstrModuleName, 53, "Remitting Bank"))
            ReportFunction.TextChange(objRpt, "lblTriplicate", Language.getMessage(mstrModuleName, 54, "Triplicate"))
            ReportFunction.TextChange(objRpt, "lblBankNote3", Language.getMessage(mstrModuleName, 55, "To remain in book as employer's receipt"))
            ReportFunction.TextChange(objRpt, "lblAnalysis", Language.getMessage(mstrModuleName, 56, "ANALYSIS OF TOTAL CREDITS AND LIABLE EMPLOYEES"))
            ReportFunction.TextChange(objRpt, "lblText5", Language.getMessage(mstrModuleName, 57, "To be completed by Employer"))
            ReportFunction.TextChange(objRpt, "lblTotalPAYE", Language.getMessage(mstrModuleName, 58, "TOTAL PAYE/INT/PEN./FRINGE BENEFIT TAX PAID"))
            ReportFunction.TextChange(objRpt, "lblCurrency1", strBaseCurrencySign)
            ReportFunction.TextChange(objRpt, "txtAmount1", Format(decTotalPaye, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "lblAuditTax", Language.getMessage(mstrModuleName, 59, "PAYE AUDIT TAX"))
            ReportFunction.TextChange(objRpt, "lblCurrency2", strBaseCurrencySign)
            ReportFunction.TextChange(objRpt, "txtAmount2", Format(0, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "lblTotalWCPS", Language.getMessage(mstrModuleName, 60, "TOTAL W C P S"))
            ReportFunction.TextChange(objRpt, "lblCurrency3", strBaseCurrencySign)
            ReportFunction.TextChange(objRpt, "txtAmount3", Format(0, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "lblTotalCredit", Language.getMessage(mstrModuleName, 61, "TOTAL CREDIT"))
            ReportFunction.TextChange(objRpt, "lblCurrency4", strBaseCurrencySign)
            ReportFunction.TextChange(objRpt, "txtAmount4", Format(decTotalPaye, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "lblText6", Language.getMessage(mstrModuleName, 62, "Liable employees are employees from whose monthly emoluments the total tax paid is deducted."))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Shs.")
            Language.setMessage(mstrModuleName, 2, "ISO 9001:2008 CERTIFIED")
            Language.setMessage(mstrModuleName, 3, "DOMESTIC TAXES DEPARTMENT")
            Language.setMessage(mstrModuleName, 4, "APPENDIX 6")
            Language.setMessage(mstrModuleName, 5, "P.A.Y.E/FRING BENEFIT TAX CREDIT SLIP")
            Language.setMessage(mstrModuleName, 6, "SERIAL NO")
            Language.setMessage(mstrModuleName, 7, "")
            Language.setMessage(mstrModuleName, 8, "Pay Roll Month")
            Language.setMessage(mstrModuleName, 9, "EMPLOYER'S PIN")
            Language.setMessage(mstrModuleName, 10, "Employer's Name")
            Language.setMessage(mstrModuleName, 11, "")
            Language.setMessage(mstrModuleName, 12, "To")
            Language.setMessage(mstrModuleName, 13, "")
            Language.setMessage(mstrModuleName, 14, "Bank")
            Language.setMessage(mstrModuleName, 15, "Please pay the Central Bank Of Kenya, for the credit of the " & Chr(10) & "Paymaster-General" & Chr(10) & ", PAYE account")
            Language.setMessage(mstrModuleName, 16, "No. 05-010-0056")
            Language.setMessage(mstrModuleName, 17, "(In words) Kenya Shillings")
            Language.setMessage(mstrModuleName, 18, "NATURE OF PAYMENT")
            Language.setMessage(mstrModuleName, 19, "")
            Language.setMessage(mstrModuleName, 20, "")
            Language.setMessage(mstrModuleName, 21, "NOTES/COINS")
            Language.setMessage(mstrModuleName, 22, "1. PAYE TAX KSHS")
            Language.setMessage(mstrModuleName, 23, "2. PAYE INTEREST/PENALTY KSHS.")
            Language.setMessage(mstrModuleName, 24, "OTHERS(WCPS. ETC) KSHS")
            Language.setMessage(mstrModuleName, 25, "Bank Stamp & Tellers Initials")
            Language.setMessage(mstrModuleName, 26, "OTHER COINS")
            Language.setMessage(mstrModuleName, 27, "CHEQUE NO.")
            Language.setMessage(mstrModuleName, 28, "")
            Language.setMessage(mstrModuleName, 29, "")
            Language.setMessage(mstrModuleName, 30, "TOTAL CREDITS")
            Language.setMessage(mstrModuleName, 31, "To Officer in Charge")
            Language.setMessage(mstrModuleName, 32, "Domestic Tax Office")
            Language.setMessage(mstrModuleName, 33, "(A)")
            Language.setMessage(mstrModuleName, 34, "PAYING-IN CERTIFICATE")
            Language.setMessage(mstrModuleName, 35, "We/I certifiy that the above represent :-")
            Language.setMessage(mstrModuleName, 36, "The full amount of the tax which was required to be deducted for the month shown above and includes any amounts brought forward from the previous months  for which the cumulative total was less than 100/=")
            Language.setMessage(mstrModuleName, 37, "(B)")
            Language.setMessage(mstrModuleName, 38, "NIL CERTIFICATE")
            Language.setMessage(mstrModuleName, 39, "We/I certifiy that for the month shown above the amount of PAYE/FRINGE BENEFIT TAX required to be deducted was Nil, or less than shs. 100/= which sum is being carried forward.")
            Language.setMessage(mstrModuleName, 40, "EMPLOYER'S NAME")
            Language.setMessage(mstrModuleName, 41, "Postal Address:")
            Language.setMessage(mstrModuleName, 42, "TELEPHONE")
            Language.setMessage(mstrModuleName, 43, "NAME OF PAYING OFFICER")
            Language.setMessage(mstrModuleName, 44, "")
            Language.setMessage(mstrModuleName, 45, "SIGNATURE")
            Language.setMessage(mstrModuleName, 46, "")
            Language.setMessage(mstrModuleName, 47, "DATE")
            Language.setMessage(mstrModuleName, 48, "")
            Language.setMessage(mstrModuleName, 49, "NOTE TO ACCEPTING BANK")
            Language.setMessage(mstrModuleName, 50, "Original")
            Language.setMessage(mstrModuleName, 51, "Domestic Tax Department")
            Language.setMessage(mstrModuleName, 52, "Duplicate")
            Language.setMessage(mstrModuleName, 53, "Remitting Bank")
            Language.setMessage(mstrModuleName, 54, "Triplicate")
            Language.setMessage(mstrModuleName, 55, "To remain in book as employer's receipt")
            Language.setMessage(mstrModuleName, 56, "ANALYSIS OF TOTAL CREDITS AND LIABLE EMPLOYEES")
            Language.setMessage(mstrModuleName, 57, "To be completed by Employer")
            Language.setMessage(mstrModuleName, 58, "TOTAL PAYE/INT/PEN./FRINGE BENEFIT TAX PAID")
            Language.setMessage(mstrModuleName, 59, "PAYE AUDIT TAX")
            Language.setMessage(mstrModuleName, 60, "TOTAL W C P S")
            Language.setMessage(mstrModuleName, 61, "TOTAL CREDIT")
            Language.setMessage(mstrModuleName, 62, "Liable employees are employees from whose monthly emoluments the total tax paid is deducted.")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
