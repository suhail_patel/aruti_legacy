﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQuarterlyWages
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmQuarterlyWages))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.LblNbreHrsJrs = New System.Windows.Forms.Label
        Me.txtTauxCNSS = New eZee.TextBox.NumericTextBox
        Me.LblTauxCNSS = New System.Windows.Forms.Label
        Me.LblCotisationsnettes = New System.Windows.Forms.Label
        Me.LblRémunérationtotale = New System.Windows.Forms.Label
        Me.LblMontantdeduction = New System.Windows.Forms.Label
        Me.LblSalaireDeplafonne = New System.Windows.Forms.Label
        Me.LblSalaireplafonne = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvPeriod = New System.Windows.Forms.DataGridView
        Me.objcolhPeriodunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColhPeriodCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPeriodName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPeriodCustomCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPeriodStart = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPeriodEnd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkShowLogo = New System.Windows.Forms.CheckBox
        Me.lblYearQuarter = New System.Windows.Forms.Label
        Me.cboYearQuarter = New System.Windows.Forms.ComboBox
        Me.LblCurrencyRate = New System.Windows.Forms.Label
        Me.LblCurrency = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.txtMontantdeduction = New System.Windows.Forms.TextBox
        Me.txtRémunérationtotale = New System.Windows.Forms.TextBox
        Me.txtCotisationsnettes = New System.Windows.Forms.TextBox
        Me.txtSalaireplafonne = New System.Windows.Forms.TextBox
        Me.txtSalaireDeplafonne = New System.Windows.Forms.TextBox
        Me.txtNbreHrsJrs = New System.Windows.Forms.TextBox
        Me.objbtnNbreHrsJrs = New eZee.Common.eZeeGradientButton
        Me.objbtnSalaireDeplafonne = New eZee.Common.eZeeGradientButton
        Me.objbtnSalaireplafonne = New eZee.Common.eZeeGradientButton
        Me.objbtnMontantDeduction = New eZee.Common.eZeeGradientButton
        Me.objbtnRémunérationtotale = New eZee.Common.eZeeGradientButton
        Me.objbtntxtCotisationsnettes = New eZee.Common.eZeeGradientButton
        Me.EZeeFooter1.SuspendLayout()
        Me.gbMandatoryInfo.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvPeriod, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(645, 60)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = ""
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(249, 13)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 6
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        Me.objbtnAdvanceFilter.Visible = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(352, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(448, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(544, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 478)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(645, 55)
        Me.EZeeFooter1.TabIndex = 70
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.objbtntxtCotisationsnettes)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnRémunérationtotale)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnMontantDeduction)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSalaireplafonne)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSalaireDeplafonne)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnNbreHrsJrs)
        Me.gbMandatoryInfo.Controls.Add(Me.txtNbreHrsJrs)
        Me.gbMandatoryInfo.Controls.Add(Me.txtSalaireDeplafonne)
        Me.gbMandatoryInfo.Controls.Add(Me.txtSalaireplafonne)
        Me.gbMandatoryInfo.Controls.Add(Me.txtCotisationsnettes)
        Me.gbMandatoryInfo.Controls.Add(Me.txtRémunérationtotale)
        Me.gbMandatoryInfo.Controls.Add(Me.txtMontantdeduction)
        Me.gbMandatoryInfo.Controls.Add(Me.LblNbreHrsJrs)
        Me.gbMandatoryInfo.Controls.Add(Me.txtTauxCNSS)
        Me.gbMandatoryInfo.Controls.Add(Me.LblTauxCNSS)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCotisationsnettes)
        Me.gbMandatoryInfo.Controls.Add(Me.LblRémunérationtotale)
        Me.gbMandatoryInfo.Controls.Add(Me.LblMontantdeduction)
        Me.gbMandatoryInfo.Controls.Add(Me.LblSalaireDeplafonne)
        Me.gbMandatoryInfo.Controls.Add(Me.LblSalaireplafonne)
        Me.gbMandatoryInfo.Controls.Add(Me.Panel1)
        Me.gbMandatoryInfo.Controls.Add(Me.btnSaveSelection)
        Me.gbMandatoryInfo.Controls.Add(Me.chkShowLogo)
        Me.gbMandatoryInfo.Controls.Add(Me.lblYearQuarter)
        Me.gbMandatoryInfo.Controls.Add(Me.cboYearQuarter)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrencyRate)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.lblMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboMembership)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(622, 357)
        Me.gbMandatoryInfo.TabIndex = 1
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblNbreHrsJrs
        '
        Me.LblNbreHrsJrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNbreHrsJrs.Location = New System.Drawing.Point(9, 231)
        Me.LblNbreHrsJrs.Name = "LblNbreHrsJrs"
        Me.LblNbreHrsJrs.Size = New System.Drawing.Size(169, 18)
        Me.LblNbreHrsJrs.TabIndex = 179
        Me.LblNbreHrsJrs.Text = "Nbre Hrs/Jrs"
        Me.LblNbreHrsJrs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTauxCNSS
        '
        Me.txtTauxCNSS.AllowNegative = True
        Me.txtTauxCNSS.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTauxCNSS.DigitsInGroup = 0
        Me.txtTauxCNSS.Flags = 0
        Me.txtTauxCNSS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTauxCNSS.Location = New System.Drawing.Point(184, 258)
        Me.txtTauxCNSS.MaxDecimalPlaces = 4
        Me.txtTauxCNSS.MaxWholeDigits = 9
        Me.txtTauxCNSS.Name = "txtTauxCNSS"
        Me.txtTauxCNSS.Prefix = ""
        Me.txtTauxCNSS.RangeMax = 1.7976931348623157E+308
        Me.txtTauxCNSS.RangeMin = -1.7976931348623157E+308
        Me.txtTauxCNSS.Size = New System.Drawing.Size(166, 21)
        Me.txtTauxCNSS.TabIndex = 5
        Me.txtTauxCNSS.Text = "0"
        Me.txtTauxCNSS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblTauxCNSS
        '
        Me.LblTauxCNSS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTauxCNSS.Location = New System.Drawing.Point(9, 259)
        Me.LblTauxCNSS.Name = "LblTauxCNSS"
        Me.LblTauxCNSS.Size = New System.Drawing.Size(169, 18)
        Me.LblTauxCNSS.TabIndex = 177
        Me.LblTauxCNSS.Text = "Taux CNSS( c )  Taux CNAMGS %"
        Me.LblTauxCNSS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCotisationsnettes
        '
        Me.LblCotisationsnettes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCotisationsnettes.Location = New System.Drawing.Point(9, 151)
        Me.LblCotisationsnettes.Name = "LblCotisationsnettes"
        Me.LblCotisationsnettes.Size = New System.Drawing.Size(169, 20)
        Me.LblCotisationsnettes.TabIndex = 175
        Me.LblCotisationsnettes.Text = "Cotisations nettes dues CNAMGS"
        Me.LblCotisationsnettes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblRémunérationtotale
        '
        Me.LblRémunérationtotale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRémunérationtotale.Location = New System.Drawing.Point(9, 120)
        Me.LblRémunérationtotale.Name = "LblRémunérationtotale"
        Me.LblRémunérationtotale.Size = New System.Drawing.Size(169, 28)
        Me.LblRémunérationtotale.TabIndex = 173
        Me.LblRémunérationtotale.Text = "Rémunération totale plafonnée CNAMGS"
        Me.LblRémunérationtotale.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMontantdeduction
        '
        Me.LblMontantdeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMontantdeduction.Location = New System.Drawing.Point(9, 88)
        Me.LblMontantdeduction.Name = "LblMontantdeduction"
        Me.LblMontantdeduction.Size = New System.Drawing.Size(169, 28)
        Me.LblMontantdeduction.TabIndex = 171
        Me.LblMontantdeduction.Text = "Montant déduction Allocations Familiales"
        Me.LblMontantdeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblSalaireDeplafonne
        '
        Me.LblSalaireDeplafonne.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSalaireDeplafonne.Location = New System.Drawing.Point(9, 204)
        Me.LblSalaireDeplafonne.Name = "LblSalaireDeplafonne"
        Me.LblSalaireDeplafonne.Size = New System.Drawing.Size(169, 18)
        Me.LblSalaireDeplafonne.TabIndex = 123
        Me.LblSalaireDeplafonne.Text = "Salaire Deplafonne"
        Me.LblSalaireDeplafonne.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblSalaireplafonne
        '
        Me.LblSalaireplafonne.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSalaireplafonne.Location = New System.Drawing.Point(9, 178)
        Me.LblSalaireplafonne.Name = "LblSalaireplafonne"
        Me.LblSalaireplafonne.Size = New System.Drawing.Size(169, 17)
        Me.LblSalaireplafonne.TabIndex = 121
        Me.LblSalaireplafonne.Text = "Salaire Plafonne (f)"
        Me.LblSalaireplafonne.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvPeriod)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(381, 33)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(232, 158)
        Me.Panel1.TabIndex = 72
        '
        'dgvPeriod
        '
        Me.dgvPeriod.AllowUserToAddRows = False
        Me.dgvPeriod.AllowUserToDeleteRows = False
        Me.dgvPeriod.AllowUserToResizeRows = False
        Me.dgvPeriod.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvPeriod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvPeriod.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvPeriod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvPeriod.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhPeriodunkid, Me.ColhPeriodCode, Me.colhPeriodName, Me.objcolhPeriodCustomCode, Me.objcolhPeriodStart, Me.objcolhPeriodEnd})
        Me.dgvPeriod.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPeriod.Location = New System.Drawing.Point(0, 0)
        Me.dgvPeriod.Name = "dgvPeriod"
        Me.dgvPeriod.ReadOnly = True
        Me.dgvPeriod.RowHeadersVisible = False
        Me.dgvPeriod.RowHeadersWidth = 5
        Me.dgvPeriod.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvPeriod.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPeriod.Size = New System.Drawing.Size(232, 158)
        Me.dgvPeriod.TabIndex = 287
        '
        'objcolhPeriodunkid
        '
        Me.objcolhPeriodunkid.HeaderText = "periodunkid"
        Me.objcolhPeriodunkid.Name = "objcolhPeriodunkid"
        Me.objcolhPeriodunkid.ReadOnly = True
        Me.objcolhPeriodunkid.Visible = False
        '
        'ColhPeriodCode
        '
        Me.ColhPeriodCode.HeaderText = "Code"
        Me.ColhPeriodCode.Name = "ColhPeriodCode"
        Me.ColhPeriodCode.ReadOnly = True
        Me.ColhPeriodCode.Width = 50
        '
        'colhPeriodName
        '
        Me.colhPeriodName.HeaderText = "Period Name"
        Me.colhPeriodName.Name = "colhPeriodName"
        Me.colhPeriodName.ReadOnly = True
        Me.colhPeriodName.Width = 160
        '
        'objcolhPeriodCustomCode
        '
        Me.objcolhPeriodCustomCode.HeaderText = "Custom Code"
        Me.objcolhPeriodCustomCode.Name = "objcolhPeriodCustomCode"
        Me.objcolhPeriodCustomCode.ReadOnly = True
        Me.objcolhPeriodCustomCode.Visible = False
        '
        'objcolhPeriodStart
        '
        Me.objcolhPeriodStart.HeaderText = "Period Start"
        Me.objcolhPeriodStart.Name = "objcolhPeriodStart"
        Me.objcolhPeriodStart.ReadOnly = True
        Me.objcolhPeriodStart.Visible = False
        '
        'objcolhPeriodEnd
        '
        Me.objcolhPeriodEnd.HeaderText = "Period End"
        Me.objcolhPeriodEnd.Name = "objcolhPeriodEnd"
        Me.objcolhPeriodEnd.ReadOnly = True
        Me.objcolhPeriodEnd.Visible = False
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(239, 317)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 10
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'chkShowLogo
        '
        Me.chkShowLogo.Checked = True
        Me.chkShowLogo.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowLogo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLogo.Location = New System.Drawing.Point(184, 288)
        Me.chkShowLogo.Name = "chkShowLogo"
        Me.chkShowLogo.Size = New System.Drawing.Size(166, 17)
        Me.chkShowLogo.TabIndex = 9
        Me.chkShowLogo.Text = "Show Logo"
        Me.chkShowLogo.UseVisualStyleBackColor = True
        '
        'lblYearQuarter
        '
        Me.lblYearQuarter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYearQuarter.Location = New System.Drawing.Point(9, 62)
        Me.lblYearQuarter.Name = "lblYearQuarter"
        Me.lblYearQuarter.Size = New System.Drawing.Size(108, 17)
        Me.lblYearQuarter.TabIndex = 86
        Me.lblYearQuarter.Text = "Year Quarter"
        Me.lblYearQuarter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYearQuarter
        '
        Me.cboYearQuarter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYearQuarter.DropDownWidth = 230
        Me.cboYearQuarter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYearQuarter.FormattingEnabled = True
        Me.cboYearQuarter.Location = New System.Drawing.Point(184, 60)
        Me.cboYearQuarter.Name = "cboYearQuarter"
        Me.cboYearQuarter.Size = New System.Drawing.Size(166, 21)
        Me.cboYearQuarter.TabIndex = 1
        '
        'LblCurrencyRate
        '
        Me.LblCurrencyRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrencyRate.Location = New System.Drawing.Point(404, 332)
        Me.LblCurrencyRate.Name = "LblCurrencyRate"
        Me.LblCurrencyRate.Size = New System.Drawing.Size(188, 15)
        Me.LblCurrencyRate.TabIndex = 79
        Me.LblCurrencyRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCurrency
        '
        Me.LblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrency.Location = New System.Drawing.Point(368, 305)
        Me.LblCurrency.Name = "LblCurrency"
        Me.LblCurrency.Size = New System.Drawing.Size(94, 15)
        Me.LblCurrency.TabIndex = 75
        Me.LblCurrency.Text = "Currency"
        Me.LblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LblCurrency.Visible = False
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 180
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(466, 302)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(126, 21)
        Me.cboCurrency.TabIndex = 76
        Me.cboCurrency.Visible = False
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(357, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 69
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkSetAnalysis.Visible = False
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(9, 35)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(108, 15)
        Me.lblMembership.TabIndex = 64
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 180
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(184, 33)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(166, 21)
        Me.cboMembership.TabIndex = 0
        '
        'txtMontantdeduction
        '
        Me.txtMontantdeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMontantdeduction.Location = New System.Drawing.Point(184, 88)
        Me.txtMontantdeduction.Name = "txtMontantdeduction"
        Me.txtMontantdeduction.Size = New System.Drawing.Size(166, 21)
        Me.txtMontantdeduction.TabIndex = 181
        '
        'txtRémunérationtotale
        '
        Me.txtRémunérationtotale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRémunérationtotale.Location = New System.Drawing.Point(184, 120)
        Me.txtRémunérationtotale.Name = "txtRémunérationtotale"
        Me.txtRémunérationtotale.Size = New System.Drawing.Size(166, 21)
        Me.txtRémunérationtotale.TabIndex = 182
        '
        'txtCotisationsnettes
        '
        Me.txtCotisationsnettes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCotisationsnettes.Location = New System.Drawing.Point(184, 151)
        Me.txtCotisationsnettes.Name = "txtCotisationsnettes"
        Me.txtCotisationsnettes.Size = New System.Drawing.Size(166, 21)
        Me.txtCotisationsnettes.TabIndex = 183
        '
        'txtSalaireplafonne
        '
        Me.txtSalaireplafonne.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalaireplafonne.Location = New System.Drawing.Point(184, 176)
        Me.txtSalaireplafonne.Name = "txtSalaireplafonne"
        Me.txtSalaireplafonne.Size = New System.Drawing.Size(166, 21)
        Me.txtSalaireplafonne.TabIndex = 184
        '
        'txtSalaireDeplafonne
        '
        Me.txtSalaireDeplafonne.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalaireDeplafonne.Location = New System.Drawing.Point(184, 203)
        Me.txtSalaireDeplafonne.Name = "txtSalaireDeplafonne"
        Me.txtSalaireDeplafonne.Size = New System.Drawing.Size(166, 21)
        Me.txtSalaireDeplafonne.TabIndex = 185
        '
        'txtNbreHrsJrs
        '
        Me.txtNbreHrsJrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNbreHrsJrs.Location = New System.Drawing.Point(184, 230)
        Me.txtNbreHrsJrs.Name = "txtNbreHrsJrs"
        Me.txtNbreHrsJrs.Size = New System.Drawing.Size(166, 21)
        Me.txtNbreHrsJrs.TabIndex = 186
        '
        'objbtnNbreHrsJrs
        '
        Me.objbtnNbreHrsJrs.BackColor = System.Drawing.Color.Transparent
        Me.objbtnNbreHrsJrs.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnNbreHrsJrs.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnNbreHrsJrs.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnNbreHrsJrs.BorderSelected = False
        Me.objbtnNbreHrsJrs.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnNbreHrsJrs.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnNbreHrsJrs.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnNbreHrsJrs.Location = New System.Drawing.Point(354, 230)
        Me.objbtnNbreHrsJrs.Name = "objbtnNbreHrsJrs"
        Me.objbtnNbreHrsJrs.Size = New System.Drawing.Size(21, 21)
        Me.objbtnNbreHrsJrs.TabIndex = 187
        '
        'objbtnSalaireDeplafonne
        '
        Me.objbtnSalaireDeplafonne.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSalaireDeplafonne.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSalaireDeplafonne.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSalaireDeplafonne.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSalaireDeplafonne.BorderSelected = False
        Me.objbtnSalaireDeplafonne.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSalaireDeplafonne.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnSalaireDeplafonne.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSalaireDeplafonne.Location = New System.Drawing.Point(354, 203)
        Me.objbtnSalaireDeplafonne.Name = "objbtnSalaireDeplafonne"
        Me.objbtnSalaireDeplafonne.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSalaireDeplafonne.TabIndex = 188
        '
        'objbtnSalaireplafonne
        '
        Me.objbtnSalaireplafonne.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSalaireplafonne.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSalaireplafonne.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSalaireplafonne.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSalaireplafonne.BorderSelected = False
        Me.objbtnSalaireplafonne.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSalaireplafonne.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnSalaireplafonne.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSalaireplafonne.Location = New System.Drawing.Point(354, 176)
        Me.objbtnSalaireplafonne.Name = "objbtnSalaireplafonne"
        Me.objbtnSalaireplafonne.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSalaireplafonne.TabIndex = 189
        '
        'objbtnMontantDeduction
        '
        Me.objbtnMontantDeduction.BackColor = System.Drawing.Color.Transparent
        Me.objbtnMontantDeduction.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnMontantDeduction.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnMontantDeduction.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnMontantDeduction.BorderSelected = False
        Me.objbtnMontantDeduction.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnMontantDeduction.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnMontantDeduction.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnMontantDeduction.Location = New System.Drawing.Point(354, 88)
        Me.objbtnMontantDeduction.Name = "objbtnMontantDeduction"
        Me.objbtnMontantDeduction.Size = New System.Drawing.Size(21, 21)
        Me.objbtnMontantDeduction.TabIndex = 190
        '
        'objbtnRémunérationtotale
        '
        Me.objbtnRémunérationtotale.BackColor = System.Drawing.Color.Transparent
        Me.objbtnRémunérationtotale.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnRémunérationtotale.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnRémunérationtotale.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnRémunérationtotale.BorderSelected = False
        Me.objbtnRémunérationtotale.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnRémunérationtotale.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnRémunérationtotale.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnRémunérationtotale.Location = New System.Drawing.Point(354, 120)
        Me.objbtnRémunérationtotale.Name = "objbtnRémunérationtotale"
        Me.objbtnRémunérationtotale.Size = New System.Drawing.Size(21, 21)
        Me.objbtnRémunérationtotale.TabIndex = 191
        '
        'objbtntxtCotisationsnettes
        '
        Me.objbtntxtCotisationsnettes.BackColor = System.Drawing.Color.Transparent
        Me.objbtntxtCotisationsnettes.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtntxtCotisationsnettes.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtntxtCotisationsnettes.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtntxtCotisationsnettes.BorderSelected = False
        Me.objbtntxtCotisationsnettes.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtntxtCotisationsnettes.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtntxtCotisationsnettes.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtntxtCotisationsnettes.Location = New System.Drawing.Point(354, 151)
        Me.objbtntxtCotisationsnettes.Name = "objbtntxtCotisationsnettes"
        Me.objbtntxtCotisationsnettes.Size = New System.Drawing.Size(21, 21)
        Me.objbtntxtCotisationsnettes.TabIndex = 192
        '
        'frmQuarterlyWages
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(645, 533)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmQuarterlyWages"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "DECLARATION TRIMESTRIELLE DES SALAIRES"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbMandatoryInfo.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvPeriod, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents chkShowLogo As System.Windows.Forms.CheckBox
    Friend WithEvents lblYearQuarter As System.Windows.Forms.Label
    Friend WithEvents cboYearQuarter As System.Windows.Forms.ComboBox
    Friend WithEvents LblCurrencyRate As System.Windows.Forms.Label
    Friend WithEvents LblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents dgvPeriod As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LblSalaireplafonne As System.Windows.Forms.Label
    Friend WithEvents LblSalaireDeplafonne As System.Windows.Forms.Label
    Friend WithEvents objcolhPeriodunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColhPeriodCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPeriodName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPeriodCustomCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPeriodStart As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPeriodEnd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LblMontantdeduction As System.Windows.Forms.Label
    Friend WithEvents LblRémunérationtotale As System.Windows.Forms.Label
    Friend WithEvents LblCotisationsnettes As System.Windows.Forms.Label
    Public WithEvents txtTauxCNSS As eZee.TextBox.NumericTextBox
    Friend WithEvents LblTauxCNSS As System.Windows.Forms.Label
    Friend WithEvents LblNbreHrsJrs As System.Windows.Forms.Label
    Friend WithEvents txtSalaireplafonne As System.Windows.Forms.TextBox
    Friend WithEvents txtCotisationsnettes As System.Windows.Forms.TextBox
    Friend WithEvents txtRémunérationtotale As System.Windows.Forms.TextBox
    Friend WithEvents txtMontantdeduction As System.Windows.Forms.TextBox
    Friend WithEvents txtNbreHrsJrs As System.Windows.Forms.TextBox
    Friend WithEvents txtSalaireDeplafonne As System.Windows.Forms.TextBox
    Friend WithEvents objbtntxtCotisationsnettes As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnRémunérationtotale As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnMontantDeduction As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSalaireplafonne As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSalaireDeplafonne As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnNbreHrsJrs As eZee.Common.eZeeGradientButton
End Class
