﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region


Public Class frmID20ETATReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmID20ETATReport"
    Private objRTCNAMGS As clsID20ETATReport
    Private mintFirstPeriodId As Integer = 0
    Private mintLastPeriodId As Integer = 0

    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintPaidCurrencyId As Integer = 0

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""

#End Region

#Region " Constructor "

    Public Sub New()
        objRTCNAMGS = New clsID20ETATReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objRTCNAMGS.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Public Enum enHeadTypeId
        SalaireBrutInferieur = 1
        SalaireBrutSuperieur = 2
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombos As DataSet
        Dim mdtPeriod As DataTable
        Try
            mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)

            mdtPeriod = New DataView(dsCombos.Tables("Period"), "periodunkid = 0 OR (start_date >= '" & eZeeDate.convertDate(CDate("01/Jan/" & Year(CDate(FinancialYear._Object._Database_End_Date).AddYears(-2)))) & "' AND end_date <= '" & eZeeDate.convertDate(CDate("31/Dec/" & Year(CDate(FinancialYear._Object._Database_End_Date).AddYears(-1)))) & "') ", "", DataViewRowState.CurrentRows).ToTable

            If mdtPeriod.Rows.Count > 1 Then
                If CInt(mdtPeriod.Rows(1).Item("periodunkid")) > 0 Then mintFirstPeriodId = CInt(mdtPeriod.Rows(1).Item("periodunkid"))

                mintFirstPeriodId = (From p In mdtPeriod Where (Year(eZeeDate.convertDate(p.Item("start_date").ToString)) = Year(CDate(FinancialYear._Object._Database_End_Date).AddYears(-1))) Select (CInt(p.Item("periodunkid")))).FirstOrDefault

                mintLastPeriodId = CInt(mdtPeriod.Rows(mdtPeriod.Rows.Count - 1).Item("periodunkid"))
            Else
                mintFirstPeriodId = 0
            End If
            If mintLastPeriodId <= 0 Then mintLastPeriodId = mintFirstPeriodId

            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstPeriodId
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = mintLastPeriodId
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try

            txtSalaireBrutInferieurFormula.Text = ""
            txtSalaireBrutSuperieurFormula.Text = ""

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            cboCurrency.SelectedValue = 0
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            mstrAdvanceFilter = ""


            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.ID20_ETAT_DE_LA_MASSE_SALARIALE_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.SalaireBrutInferieur
                            txtSalaireBrutInferieurFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.SalaireBrutSuperieur
                            txtSalaireBrutSuperieurFormula.Text = dsRow.Item("transactionheadid").ToString

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim intPrevYearID As Integer = 0
        Dim mdicYearDBName As New Dictionary(Of Integer, String)
        Try
            objRTCNAMGS.SetDefaultValue()

            If CInt(cboFromPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "From period is mandatory information. Please select From period to continue."), enMsgBoxStyle.Information)
                cboFromPeriod.Focus()
                Exit Function
            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "To period is mandatory information. Please select To period to continue."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Function
            ElseIf CInt(cboFromPeriod.SelectedIndex) > CInt(cboToPeriod.SelectedIndex) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Function
            ElseIf txtSalaireBrutInferieurFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please enter Salaire Brut Inferieur Formula."), enMsgBoxStyle.Information)
                txtSalaireBrutInferieurFormula.Focus()
                Exit Function
            ElseIf txtSalaireBrutSuperieurFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please enter Salaire Brut Superieur Formula."), enMsgBoxStyle.Information)
                txtSalaireBrutSuperieurFormula.Focus()
                Exit Function
            End If



            With objRTCNAMGS
                ._FromPeriodId = CInt(cboFromPeriod.SelectedValue)
                ._FromPeriodName = cboFromPeriod.Text

                ._ToPeriodId = CInt(cboToPeriod.SelectedValue)
                ._ToPeriodName = cboToPeriod.Text

                ._PeriodStart = eZeeDate.convertDate(CType(cboFromPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                ._PeriodEnd = eZeeDate.convertDate(CType(cboToPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
                ._FirstPeriodYearName = eZeeDate.convertDate(CType(cboFromPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Year.ToString

                ._SalaireBrutInferieurFormula = txtSalaireBrutInferieurFormula.Text.Trim
                ._SalaireBrutSuperieurFormula = txtSalaireBrutSuperieurFormula.Text.Trim

                Dim i As Integer = 0
                Dim strPreriodIds As String = cboFromPeriod.SelectedValue.ToString
                Dim mstrPeriodsName As String = cboFromPeriod.Text
                dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid")))
                If dsList.Tables("Database").Rows.Count > 0 Then
                    intPrevYearID = CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid"))
                    If mdicYearDBName.ContainsKey(intPrevYearID) = False Then
                        mdicYearDBName.Add(intPrevYearID, dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                    End If
                End If

                For Each dsRow As DataRow In CType(cboFromPeriod.DataSource, DataTable).Rows
                    If i > cboFromPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                        strPreriodIds &= ", " & dsRow.Item("periodunkid").ToString
                        mstrPeriodsName &= ", " & dsRow.Item("name").ToString
                        If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
                            If dsList.Tables("Database").Rows.Count > 0 Then
                                If mdicYearDBName.ContainsKey(CInt(dsRow.Item("yearunkid"))) = False Then
                                    mdicYearDBName.Add(CInt(dsRow.Item("yearunkid")), dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                                End If
                            End If
                        End If
                        intPrevYearID = CInt(dsRow.Item("yearunkid"))
                    End If
                    i += 1
                Next
                ._dic_YearDBName = mdicYearDBName

                ._PeriodIDs = strPreriodIds
                ._PeriodNames = mstrPeriodsName


                ._ViewByIds = mstrStringIds
                ._ViewIndex = mintViewIdx
                ._ViewByName = mstrStringName
                ._Analysis_Fields = mstrAnalysis_Fields
                ._Analysis_Join = mstrAnalysis_Join
                ._Report_GroupName = mstrReport_GroupName

                If (mintBaseCurrId = mintPaidCurrencyId) Or mintPaidCurrencyId <= 0 Then
                    mintPaidCurrencyId = mintBaseCurrId
                End If

                ._CountryId = CInt(cboCurrency.SelectedValue)
                ._BaseCurrencyId = mintBaseCurrId
                ._PaidCurrencyId = mintPaidCurrencyId
                ._CurrencyName = cboCurrency.Text.Trim

                If mdecBaseExRate > 0 AndAlso mdecPaidExRate Then
                    ._ConversionRate = mdecPaidExRate / mdecBaseExRate
                End If

                ._ExchangeRate = LblCurrencyRate.Text


            End With




            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Event(s) "

    Private Sub frmRapportTrimestrielCNAMGS_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objRTCNAMGS = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRapportTrimestrielCNAMGS_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmRapportTrimestrielCNAMGS_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            eZeeHeader.Title = objRTCNAMGS._ReportName
            eZeeHeader.Message = objRTCNAMGS._ReportDesc

            OtherSettings()
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRapportTrimestrielCNAMGS_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objRTCNAMGS._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objRTCNAMGS._PeriodEnd))

            objRTCNAMGS.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             objRTCNAMGS._PeriodStart, _
                                             objRTCNAMGS._PeriodEnd, _
                                             ConfigParameter._Object._UserAccessModeSetting, True, FinancialYear._Object._Database_Start_Date, ConfigParameter._Object._Base_CurrencyId, GUI.fmtCurrency, "", ConfigParameter._Object._OpenAfterExport, False)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Sub

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.ID20_ETAT_DE_LA_MASSE_SALARIALE_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.SalaireBrutInferieur
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtSalaireBrutInferieurFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID20_ETAT_DE_LA_MASSE_SALARIALE_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.SalaireBrutSuperieur
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtSalaireBrutSuperieurFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID20_ETAT_DE_LA_MASSE_SALARIALE_Report, 0, 0, intHeadType)


                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsID20ETATReport.SetMessages()
            objfrm._Other_ModuleNames = "clsID20ETATReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnKeywords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnKeywordsSBI.Click, objbtnKeywordsSBS.Click
        Dim frm As New frmRemark
        Dim strB As New StringBuilder
        Try
            frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 7, "Available Keywords")
            frm.Text = frm.objgbRemarks.Text
            Dim strRemarks As String = ""
            strB.Length = 0

            strB.AppendLine("#headcode#      (e.g. #010#)")

            strRemarks = strB.ToString

            frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, eZee.Common.eZeeGradientButton).Name & "_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "


#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lblSalaireBrutInferieurFormula.Text = Language._Object.getCaption(Me.lblSalaireBrutInferieurFormula.Name, Me.lblSalaireBrutInferieurFormula.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.Name, Me.lblFromPeriod.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblSalaireBrutSuperieurFormula.Text = Language._Object.getCaption(Me.lblSalaireBrutSuperieurFormula.Name, Me.lblSalaireBrutSuperieurFormula.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "From period is mandatory information. Please select From period to continue.")
			Language.setMessage(mstrModuleName, 2, "To period is mandatory information. Please select To period to continue.")
			Language.setMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period.")
			Language.setMessage(mstrModuleName, 4, "Please enter Salaire Brut Inferieur Formula.")
			Language.setMessage(mstrModuleName, 5, "Please enter Salaire Brut Superieur Formula.")
			Language.setMessage(mstrModuleName, 6, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 7, "Available Keywords")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class