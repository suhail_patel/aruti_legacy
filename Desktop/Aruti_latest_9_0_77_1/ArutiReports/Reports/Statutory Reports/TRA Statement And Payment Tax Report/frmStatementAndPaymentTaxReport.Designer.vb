﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStatementAndPaymentTaxReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStatementAndPaymentTaxReport))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkExportDataxlsx = New System.Windows.Forms.LinkLabel
        Me.chkInactiveEmp = New System.Windows.Forms.CheckBox
        Me.EZeeGradientButton1 = New eZee.Common.eZeeGradientButton
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.cboAllocation = New System.Windows.Forms.ComboBox
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.objchkAllDeductionHead = New System.Windows.Forms.CheckBox
        Me.lvDeductionHead = New eZee.Common.eZeeListView(Me.components)
        Me.objColhPCheck = New System.Windows.Forms.ColumnHeader
        Me.colhHeadName = New System.Windows.Forms.ColumnHeader
        Me.objbtnSearchDirector = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchSecondary = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchPrimary = New eZee.Common.eZeeGradientButton
        Me.lblDeductionhead = New System.Windows.Forms.Label
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.lblbasicpayHead = New System.Windows.Forms.Label
        Me.BasicPay = New eZee.Common.eZeeGradientButton
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cbobasicpay = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.objbtnSearchPayable = New eZee.Common.eZeeGradientButton
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboPayable = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTaxable = New eZee.Common.eZeeGradientButton
        Me.lblTransectionHead = New System.Windows.Forms.Label
        Me.cboTaxableHead = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboDirectorFee = New System.Windows.Forms.ComboBox
        Me.lblSecondary = New System.Windows.Forms.Label
        Me.cboSecondary = New System.Windows.Forms.ComboBox
        Me.lblPrimary = New System.Windows.Forms.Label
        Me.cboPrimary = New System.Windows.Forms.ComboBox
        Me.objbtnSearchIdentity = New eZee.Common.eZeeGradientButton
        Me.lblidentity = New System.Windows.Forms.Label
        Me.cboIdType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchMembership = New eZee.Common.eZeeGradientButton
        Me.LblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lnkSetAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.chkShowEmployeeCode = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 407)
        Me.NavPanel.Size = New System.Drawing.Size(800, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowEmployeeCode)
        Me.gbFilterCriteria.Controls.Add(Me.lnkExportDataxlsx)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveEmp)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeGradientButton1)
        Me.gbFilterCriteria.Controls.Add(Me.lblAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.objchkAllDeductionHead)
        Me.gbFilterCriteria.Controls.Add(Me.lvDeductionHead)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchDirector)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchSecondary)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPrimary)
        Me.gbFilterCriteria.Controls.Add(Me.lblDeductionhead)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblbasicpayHead)
        Me.gbFilterCriteria.Controls.Add(Me.BasicPay)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cbobasicpay)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPayable)
        Me.gbFilterCriteria.Controls.Add(Me.Label3)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayable)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTaxable)
        Me.gbFilterCriteria.Controls.Add(Me.lblTransectionHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboTaxableHead)
        Me.gbFilterCriteria.Controls.Add(Me.Label2)
        Me.gbFilterCriteria.Controls.Add(Me.cboDirectorFee)
        Me.gbFilterCriteria.Controls.Add(Me.lblSecondary)
        Me.gbFilterCriteria.Controls.Add(Me.cboSecondary)
        Me.gbFilterCriteria.Controls.Add(Me.lblPrimary)
        Me.gbFilterCriteria.Controls.Add(Me.cboPrimary)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchIdentity)
        Me.gbFilterCriteria.Controls.Add(Me.lblidentity)
        Me.gbFilterCriteria.Controls.Add(Me.cboIdType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchMembership)
        Me.gbFilterCriteria.Controls.Add(Me.LblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 67)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(776, 334)
        Me.gbFilterCriteria.TabIndex = 2
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkExportDataxlsx
        '
        Me.lnkExportDataxlsx.BackColor = System.Drawing.Color.Transparent
        Me.lnkExportDataxlsx.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkExportDataxlsx.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkExportDataxlsx.Location = New System.Drawing.Point(435, 285)
        Me.lnkExportDataxlsx.Name = "lnkExportDataxlsx"
        Me.lnkExportDataxlsx.Size = New System.Drawing.Size(208, 17)
        Me.lnkExportDataxlsx.TabIndex = 137
        Me.lnkExportDataxlsx.TabStop = True
        Me.lnkExportDataxlsx.Text = "Export in xslm format (Data Only)"
        Me.lnkExportDataxlsx.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkInactiveEmp
        '
        Me.chkInactiveEmp.AutoSize = True
        Me.chkInactiveEmp.Location = New System.Drawing.Point(584, 36)
        Me.chkInactiveEmp.Name = "chkInactiveEmp"
        Me.chkInactiveEmp.Size = New System.Drawing.Size(176, 17)
        Me.chkInactiveEmp.TabIndex = 135
        Me.chkInactiveEmp.Text = "Include Inactive Employee"
        Me.chkInactiveEmp.UseVisualStyleBackColor = True
        '
        'EZeeGradientButton1
        '
        Me.EZeeGradientButton1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BackColor1 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BackColor2 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EZeeGradientButton1.BorderSelected = False
        Me.EZeeGradientButton1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.EZeeGradientButton1.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.EZeeGradientButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.EZeeGradientButton1.Location = New System.Drawing.Point(373, 142)
        Me.EZeeGradientButton1.Name = "EZeeGradientButton1"
        Me.EZeeGradientButton1.Size = New System.Drawing.Size(28, 21)
        Me.EZeeGradientButton1.TabIndex = 133
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(6, 144)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(137, 15)
        Me.lblAllocation.TabIndex = 132
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocation
        '
        Me.cboAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation.DropDownWidth = 230
        Me.cboAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation.FormattingEnabled = True
        Me.cboAllocation.Location = New System.Drawing.Point(149, 142)
        Me.cboAllocation.Name = "cboAllocation"
        Me.cboAllocation.Size = New System.Drawing.Size(220, 21)
        Me.cboAllocation.TabIndex = 131
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(649, 280)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 129
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'objchkAllDeductionHead
        '
        Me.objchkAllDeductionHead.AutoSize = True
        Me.objchkAllDeductionHead.Location = New System.Drawing.Point(432, 69)
        Me.objchkAllDeductionHead.Name = "objchkAllDeductionHead"
        Me.objchkAllDeductionHead.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllDeductionHead.TabIndex = 127
        Me.objchkAllDeductionHead.UseVisualStyleBackColor = True
        '
        'lvDeductionHead
        '
        Me.lvDeductionHead.BackColorOnChecked = False
        Me.lvDeductionHead.CheckBoxes = True
        Me.lvDeductionHead.ColumnHeaders = Nothing
        Me.lvDeductionHead.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhPCheck, Me.colhHeadName})
        Me.lvDeductionHead.CompulsoryColumns = ""
        Me.lvDeductionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDeductionHead.FullRowSelect = True
        Me.lvDeductionHead.GridLines = True
        Me.lvDeductionHead.GroupingColumn = Nothing
        Me.lvDeductionHead.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvDeductionHead.HideSelection = False
        Me.lvDeductionHead.Location = New System.Drawing.Point(425, 61)
        Me.lvDeductionHead.MinColumnWidth = 50
        Me.lvDeductionHead.MultiSelect = False
        Me.lvDeductionHead.Name = "lvDeductionHead"
        Me.lvDeductionHead.OptionalColumns = ""
        Me.lvDeductionHead.ShowMoreItem = False
        Me.lvDeductionHead.ShowSaveItem = False
        Me.lvDeductionHead.ShowSelectAll = True
        Me.lvDeductionHead.ShowSizeAllColumnsToFit = True
        Me.lvDeductionHead.Size = New System.Drawing.Size(335, 183)
        Me.lvDeductionHead.Sortable = True
        Me.lvDeductionHead.TabIndex = 126
        Me.lvDeductionHead.UseCompatibleStateImageBehavior = False
        Me.lvDeductionHead.View = System.Windows.Forms.View.Details
        '
        'objColhPCheck
        '
        Me.objColhPCheck.Tag = "objColhPCheck"
        Me.objColhPCheck.Text = ""
        Me.objColhPCheck.Width = 25
        '
        'colhHeadName
        '
        Me.colhHeadName.Tag = "colhPeriodName"
        Me.colhHeadName.Text = "Heads"
        Me.colhHeadName.Width = 205
        '
        'objbtnSearchDirector
        '
        Me.objbtnSearchDirector.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDirector.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDirector.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDirector.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDirector.BorderSelected = False
        Me.objbtnSearchDirector.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDirector.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDirector.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDirector.Location = New System.Drawing.Point(373, 223)
        Me.objbtnSearchDirector.Name = "objbtnSearchDirector"
        Me.objbtnSearchDirector.Size = New System.Drawing.Size(28, 21)
        Me.objbtnSearchDirector.TabIndex = 124
        '
        'objbtnSearchSecondary
        '
        Me.objbtnSearchSecondary.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSecondary.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecondary.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecondary.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSecondary.BorderSelected = False
        Me.objbtnSearchSecondary.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSecondary.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSecondary.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSecondary.Location = New System.Drawing.Point(373, 198)
        Me.objbtnSearchSecondary.Name = "objbtnSearchSecondary"
        Me.objbtnSearchSecondary.Size = New System.Drawing.Size(28, 21)
        Me.objbtnSearchSecondary.TabIndex = 123
        '
        'objbtnSearchPrimary
        '
        Me.objbtnSearchPrimary.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPrimary.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPrimary.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPrimary.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPrimary.BorderSelected = False
        Me.objbtnSearchPrimary.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPrimary.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPrimary.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPrimary.Location = New System.Drawing.Point(373, 169)
        Me.objbtnSearchPrimary.Name = "objbtnSearchPrimary"
        Me.objbtnSearchPrimary.Size = New System.Drawing.Size(28, 21)
        Me.objbtnSearchPrimary.TabIndex = 122
        '
        'lblDeductionhead
        '
        Me.lblDeductionhead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeductionhead.Location = New System.Drawing.Point(422, 34)
        Me.lblDeductionhead.Name = "lblDeductionhead"
        Me.lblDeductionhead.Size = New System.Drawing.Size(137, 15)
        Me.lblDeductionhead.TabIndex = 119
        Me.lblDeductionhead.Text = "Deduction Head"
        Me.lblDeductionhead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(373, 61)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(28, 21)
        Me.objbtnSearchPeriod.TabIndex = 113
        '
        'lblbasicpayHead
        '
        Me.lblbasicpayHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblbasicpayHead.Location = New System.Drawing.Point(423, 254)
        Me.lblbasicpayHead.Name = "lblbasicpayHead"
        Me.lblbasicpayHead.Size = New System.Drawing.Size(87, 15)
        Me.lblbasicpayHead.TabIndex = 116
        Me.lblbasicpayHead.Text = "Basic Pay Head"
        Me.lblbasicpayHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BasicPay
        '
        Me.BasicPay.BackColor = System.Drawing.Color.Transparent
        Me.BasicPay.BackColor1 = System.Drawing.Color.Transparent
        Me.BasicPay.BackColor2 = System.Drawing.Color.Transparent
        Me.BasicPay.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.BasicPay.BorderSelected = False
        Me.BasicPay.DialogResult = System.Windows.Forms.DialogResult.None
        Me.BasicPay.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.BasicPay.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BasicPay.Location = New System.Drawing.Point(740, 250)
        Me.BasicPay.Name = "BasicPay"
        Me.BasicPay.Size = New System.Drawing.Size(28, 21)
        Me.BasicPay.TabIndex = 117
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(149, 61)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(220, 21)
        Me.cboPeriod.TabIndex = 112
        '
        'cbobasicpay
        '
        Me.cbobasicpay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbobasicpay.DropDownWidth = 230
        Me.cbobasicpay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbobasicpay.FormattingEnabled = True
        Me.cbobasicpay.Location = New System.Drawing.Point(514, 250)
        Me.cbobasicpay.Name = "cbobasicpay"
        Me.cbobasicpay.Size = New System.Drawing.Size(220, 21)
        Me.cbobasicpay.TabIndex = 115
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(6, 64)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(107, 15)
        Me.lblPeriod.TabIndex = 111
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchPayable
        '
        Me.objbtnSearchPayable.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPayable.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPayable.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPayable.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPayable.BorderSelected = False
        Me.objbtnSearchPayable.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPayable.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPayable.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPayable.Location = New System.Drawing.Point(374, 277)
        Me.objbtnSearchPayable.Name = "objbtnSearchPayable"
        Me.objbtnSearchPayable.Size = New System.Drawing.Size(28, 21)
        Me.objbtnSearchPayable.TabIndex = 109
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 279)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(137, 15)
        Me.Label3.TabIndex = 108
        Me.Label3.Text = "Transection Payable"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayable
        '
        Me.cboPayable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayable.DropDownWidth = 230
        Me.cboPayable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayable.FormattingEnabled = True
        Me.cboPayable.Location = New System.Drawing.Point(150, 277)
        Me.cboPayable.Name = "cboPayable"
        Me.cboPayable.Size = New System.Drawing.Size(220, 21)
        Me.cboPayable.TabIndex = 107
        '
        'objbtnSearchTaxable
        '
        Me.objbtnSearchTaxable.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxable.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxable.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTaxable.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTaxable.BorderSelected = False
        Me.objbtnSearchTaxable.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTaxable.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTaxable.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTaxable.Location = New System.Drawing.Point(374, 250)
        Me.objbtnSearchTaxable.Name = "objbtnSearchTaxable"
        Me.objbtnSearchTaxable.Size = New System.Drawing.Size(28, 21)
        Me.objbtnSearchTaxable.TabIndex = 106
        '
        'lblTransectionHead
        '
        Me.lblTransectionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransectionHead.Location = New System.Drawing.Point(7, 252)
        Me.lblTransectionHead.Name = "lblTransectionHead"
        Me.lblTransectionHead.Size = New System.Drawing.Size(137, 15)
        Me.lblTransectionHead.TabIndex = 105
        Me.lblTransectionHead.Text = "Taxable Amount Head"
        Me.lblTransectionHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTaxableHead
        '
        Me.cboTaxableHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTaxableHead.DropDownWidth = 230
        Me.cboTaxableHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTaxableHead.FormattingEnabled = True
        Me.cboTaxableHead.Location = New System.Drawing.Point(150, 250)
        Me.cboTaxableHead.Name = "cboTaxableHead"
        Me.cboTaxableHead.Size = New System.Drawing.Size(220, 21)
        Me.cboTaxableHead.TabIndex = 104
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 225)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(137, 15)
        Me.Label2.TabIndex = 103
        Me.Label2.Text = "Director Fees"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDirectorFee
        '
        Me.cboDirectorFee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDirectorFee.DropDownWidth = 230
        Me.cboDirectorFee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDirectorFee.FormattingEnabled = True
        Me.cboDirectorFee.Location = New System.Drawing.Point(149, 223)
        Me.cboDirectorFee.Name = "cboDirectorFee"
        Me.cboDirectorFee.Size = New System.Drawing.Size(220, 21)
        Me.cboDirectorFee.TabIndex = 102
        '
        'lblSecondary
        '
        Me.lblSecondary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSecondary.Location = New System.Drawing.Point(6, 198)
        Me.lblSecondary.Name = "lblSecondary"
        Me.lblSecondary.Size = New System.Drawing.Size(137, 15)
        Me.lblSecondary.TabIndex = 101
        Me.lblSecondary.Text = "Secondry Employeement"
        Me.lblSecondary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSecondary
        '
        Me.cboSecondary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSecondary.DropDownWidth = 230
        Me.cboSecondary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSecondary.FormattingEnabled = True
        Me.cboSecondary.Location = New System.Drawing.Point(149, 196)
        Me.cboSecondary.Name = "cboSecondary"
        Me.cboSecondary.Size = New System.Drawing.Size(220, 21)
        Me.cboSecondary.TabIndex = 100
        '
        'lblPrimary
        '
        Me.lblPrimary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrimary.Location = New System.Drawing.Point(6, 171)
        Me.lblPrimary.Name = "lblPrimary"
        Me.lblPrimary.Size = New System.Drawing.Size(137, 15)
        Me.lblPrimary.TabIndex = 95
        Me.lblPrimary.Text = "Primary Employeement"
        Me.lblPrimary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPrimary
        '
        Me.cboPrimary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPrimary.DropDownWidth = 230
        Me.cboPrimary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPrimary.FormattingEnabled = True
        Me.cboPrimary.Location = New System.Drawing.Point(149, 169)
        Me.cboPrimary.Name = "cboPrimary"
        Me.cboPrimary.Size = New System.Drawing.Size(220, 21)
        Me.cboPrimary.TabIndex = 94
        '
        'objbtnSearchIdentity
        '
        Me.objbtnSearchIdentity.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchIdentity.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchIdentity.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchIdentity.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchIdentity.BorderSelected = False
        Me.objbtnSearchIdentity.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchIdentity.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchIdentity.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchIdentity.Location = New System.Drawing.Point(373, 115)
        Me.objbtnSearchIdentity.Name = "objbtnSearchIdentity"
        Me.objbtnSearchIdentity.Size = New System.Drawing.Size(28, 21)
        Me.objbtnSearchIdentity.TabIndex = 93
        '
        'lblidentity
        '
        Me.lblidentity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblidentity.Location = New System.Drawing.Point(6, 117)
        Me.lblidentity.Name = "lblidentity"
        Me.lblidentity.Size = New System.Drawing.Size(107, 15)
        Me.lblidentity.TabIndex = 92
        Me.lblidentity.Text = "Idenity"
        Me.lblidentity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIdType
        '
        Me.cboIdType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIdType.DropDownWidth = 230
        Me.cboIdType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIdType.FormattingEnabled = True
        Me.cboIdType.Location = New System.Drawing.Point(149, 115)
        Me.cboIdType.Name = "cboIdType"
        Me.cboIdType.Size = New System.Drawing.Size(220, 21)
        Me.cboIdType.TabIndex = 91
        '
        'objbtnSearchMembership
        '
        Me.objbtnSearchMembership.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMembership.BorderSelected = False
        Me.objbtnSearchMembership.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMembership.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMembership.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMembership.Location = New System.Drawing.Point(373, 88)
        Me.objbtnSearchMembership.Name = "objbtnSearchMembership"
        Me.objbtnSearchMembership.Size = New System.Drawing.Size(28, 21)
        Me.objbtnSearchMembership.TabIndex = 90
        '
        'LblMembership
        '
        Me.LblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMembership.Location = New System.Drawing.Point(6, 90)
        Me.LblMembership.Name = "LblMembership"
        Me.LblMembership.Size = New System.Drawing.Size(107, 15)
        Me.LblMembership.TabIndex = 89
        Me.LblMembership.Text = "Membership"
        Me.LblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 230
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(149, 88)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(220, 21)
        Me.cboMembership.TabIndex = 88
        '
        'lnkSetAllocation
        '
        Me.lnkSetAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAllocation.Location = New System.Drawing.Point(677, 4)
        Me.lnkSetAllocation.Name = "lnkSetAllocation"
        Me.lnkSetAllocation.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAllocation.TabIndex = 78
        Me.lnkSetAllocation.TabStop = True
        Me.lnkSetAllocation.Text = "Allocation"
        Me.lnkSetAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(373, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(28, 21)
        Me.objbtnSearchEmployee.TabIndex = 56
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(149, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(220, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(6, 37)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(107, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowEmployeeCode
        '
        Me.chkShowEmployeeCode.Checked = True
        Me.chkShowEmployeeCode.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmployeeCode.Location = New System.Drawing.Point(149, 305)
        Me.chkShowEmployeeCode.Name = "chkShowEmployeeCode"
        Me.chkShowEmployeeCode.Size = New System.Drawing.Size(221, 17)
        Me.chkShowEmployeeCode.TabIndex = 139
        Me.chkShowEmployeeCode.Text = "Show Employee Code On Report"
        Me.chkShowEmployeeCode.UseVisualStyleBackColor = True
        '
        'frmStatementAndPaymentTaxReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 462)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmStatementAndPaymentTaxReport"
        Me.Text = "frmStatementAndPaymentTaxReport"
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkSetAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchMembership As eZee.Common.eZeeGradientButton
    Friend WithEvents LblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchIdentity As eZee.Common.eZeeGradientButton
    Friend WithEvents lblidentity As System.Windows.Forms.Label
    Friend WithEvents cboIdType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPrimary As System.Windows.Forms.Label
    Friend WithEvents cboPrimary As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboDirectorFee As System.Windows.Forms.ComboBox
    Friend WithEvents lblSecondary As System.Windows.Forms.Label
    Friend WithEvents cboSecondary As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransectionHead As System.Windows.Forms.Label
    Friend WithEvents cboTaxableHead As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchTaxable As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchPayable As eZee.Common.eZeeGradientButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboPayable As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblDeductionhead As System.Windows.Forms.Label
    Friend WithEvents BasicPay As eZee.Common.eZeeGradientButton
    Friend WithEvents lblbasicpayHead As System.Windows.Forms.Label
    Friend WithEvents cbobasicpay As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchDirector As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchSecondary As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchPrimary As eZee.Common.eZeeGradientButton
    Friend WithEvents lvDeductionHead As eZee.Common.eZeeListView
    Friend WithEvents objColhPCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhHeadName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAllDeductionHead As System.Windows.Forms.CheckBox
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeGradientButton1 As eZee.Common.eZeeGradientButton
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents chkInactiveEmp As System.Windows.Forms.CheckBox
    Friend WithEvents lnkExportDataxlsx As System.Windows.Forms.LinkLabel
    Friend WithEvents chkShowEmployeeCode As System.Windows.Forms.CheckBox
End Class
