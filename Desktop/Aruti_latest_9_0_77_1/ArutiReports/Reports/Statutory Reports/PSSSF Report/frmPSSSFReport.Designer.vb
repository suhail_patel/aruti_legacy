﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPSSSFReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboIdentity = New System.Windows.Forms.ComboBox
        Me.lblVOLHead = New System.Windows.Forms.Label
        Me.cboVOLHead = New System.Windows.Forms.ComboBox
        Me.lblIdentity = New System.Windows.Forms.Label
        Me.objlblExRate = New System.Windows.Forms.Label
        Me.LblCurrency = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.cboEmpHeadType = New System.Windows.Forms.ComboBox
        Me.chkShowBasicSal = New System.Windows.Forms.CheckBox
        Me.lblCompanyHeadType = New System.Windows.Forms.Label
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblEmpContriHeadType = New System.Windows.Forms.Label
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboCoHeadType = New System.Windows.Forms.ComboBox
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblPeriod1 = New System.Windows.Forms.Label
        Me.cboPeriod1 = New System.Windows.Forms.ComboBox
        Me.gbMandatoryInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 236)
        Me.NavPanel.Size = New System.Drawing.Size(732, 55)
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboIdentity)
        Me.gbMandatoryInfo.Controls.Add(Me.lblVOLHead)
        Me.gbMandatoryInfo.Controls.Add(Me.cboVOLHead)
        Me.gbMandatoryInfo.Controls.Add(Me.lblIdentity)
        Me.gbMandatoryInfo.Controls.Add(Me.objlblExRate)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.cboEmpHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.chkShowBasicSal)
        Me.gbMandatoryInfo.Controls.Add(Me.lblCompanyHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.lblEmpContriHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.lblMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCoHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.cboMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod1)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod1)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(455, 127)
        Me.gbMandatoryInfo.TabIndex = 6
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(232, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(79, 15)
        Me.lblPeriod.TabIndex = 105
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 180
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(316, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboPeriod.TabIndex = 106
        '
        'cboIdentity
        '
        Me.cboIdentity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIdentity.DropDownWidth = 180
        Me.cboIdentity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIdentity.FormattingEnabled = True
        Me.cboIdentity.Location = New System.Drawing.Point(109, 90)
        Me.cboIdentity.Name = "cboIdentity"
        Me.cboIdentity.Size = New System.Drawing.Size(118, 21)
        Me.cboIdentity.TabIndex = 103
        '
        'lblVOLHead
        '
        Me.lblVOLHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVOLHead.Location = New System.Drawing.Point(233, 92)
        Me.lblVOLHead.Name = "lblVOLHead"
        Me.lblVOLHead.Size = New System.Drawing.Size(87, 15)
        Me.lblVOLHead.TabIndex = 88
        Me.lblVOLHead.Text = "VOL Head"
        Me.lblVOLHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboVOLHead
        '
        Me.cboVOLHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVOLHead.DropDownWidth = 180
        Me.cboVOLHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVOLHead.FormattingEnabled = True
        Me.cboVOLHead.Location = New System.Drawing.Point(320, 89)
        Me.cboVOLHead.Name = "cboVOLHead"
        Me.cboVOLHead.Size = New System.Drawing.Size(120, 21)
        Me.cboVOLHead.TabIndex = 4
        '
        'lblIdentity
        '
        Me.lblIdentity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdentity.Location = New System.Drawing.Point(9, 85)
        Me.lblIdentity.Name = "lblIdentity"
        Me.lblIdentity.Size = New System.Drawing.Size(94, 26)
        Me.lblIdentity.TabIndex = 86
        Me.lblIdentity.Text = "Identity"
        Me.lblIdentity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblExRate
        '
        Me.objlblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExRate.Location = New System.Drawing.Point(235, 63)
        Me.objlblExRate.Name = "objlblExRate"
        Me.objlblExRate.Size = New System.Drawing.Size(207, 15)
        Me.objlblExRate.TabIndex = 79
        Me.objlblExRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCurrency
        '
        Me.LblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrency.Location = New System.Drawing.Point(9, 62)
        Me.LblCurrency.Name = "LblCurrency"
        Me.LblCurrency.Size = New System.Drawing.Size(94, 15)
        Me.LblCurrency.TabIndex = 75
        Me.LblCurrency.Text = "Currency"
        Me.LblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 180
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(109, 60)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(118, 21)
        Me.cboCurrency.TabIndex = 1
        '
        'cboEmpHeadType
        '
        Me.cboEmpHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpHeadType.DropDownWidth = 180
        Me.cboEmpHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpHeadType.FormattingEnabled = True
        Me.cboEmpHeadType.Location = New System.Drawing.Point(107, 339)
        Me.cboEmpHeadType.Name = "cboEmpHeadType"
        Me.cboEmpHeadType.Size = New System.Drawing.Size(118, 21)
        Me.cboEmpHeadType.TabIndex = 59
        Me.cboEmpHeadType.Visible = False
        '
        'chkShowBasicSal
        '
        Me.chkShowBasicSal.Checked = True
        Me.chkShowBasicSal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowBasicSal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowBasicSal.Location = New System.Drawing.Point(107, 343)
        Me.chkShowBasicSal.Name = "chkShowBasicSal"
        Me.chkShowBasicSal.Size = New System.Drawing.Size(205, 17)
        Me.chkShowBasicSal.TabIndex = 73
        Me.chkShowBasicSal.Text = "Show Basic Salary"
        Me.chkShowBasicSal.UseVisualStyleBackColor = True
        Me.chkShowBasicSal.Visible = False
        '
        'lblCompanyHeadType
        '
        Me.lblCompanyHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyHeadType.Location = New System.Drawing.Point(231, 343)
        Me.lblCompanyHeadType.Name = "lblCompanyHeadType"
        Me.lblCompanyHeadType.Size = New System.Drawing.Size(87, 15)
        Me.lblCompanyHeadType.TabIndex = 62
        Me.lblCompanyHeadType.Text = "Co. Contribution"
        Me.lblCompanyHeadType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCompanyHeadType.Visible = False
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(357, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 69
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEmpContriHeadType
        '
        Me.lblEmpContriHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpContriHeadType.Location = New System.Drawing.Point(7, 342)
        Me.lblEmpContriHeadType.Name = "lblEmpContriHeadType"
        Me.lblEmpContriHeadType.Size = New System.Drawing.Size(94, 15)
        Me.lblEmpContriHeadType.TabIndex = 60
        Me.lblEmpContriHeadType.Text = "Emp. Contribution"
        Me.lblEmpContriHeadType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEmpContriHeadType.Visible = False
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(9, 36)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(94, 15)
        Me.lblMembership.TabIndex = 64
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCoHeadType
        '
        Me.cboCoHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCoHeadType.DropDownWidth = 180
        Me.cboCoHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCoHeadType.FormattingEnabled = True
        Me.cboCoHeadType.Location = New System.Drawing.Point(324, 340)
        Me.cboCoHeadType.Name = "cboCoHeadType"
        Me.cboCoHeadType.Size = New System.Drawing.Size(118, 21)
        Me.cboCoHeadType.TabIndex = 61
        Me.cboCoHeadType.Visible = False
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 180
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(109, 33)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(118, 21)
        Me.cboMembership.TabIndex = 0
        '
        'lblPeriod1
        '
        Me.lblPeriod1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod1.Location = New System.Drawing.Point(7, 368)
        Me.lblPeriod1.Name = "lblPeriod1"
        Me.lblPeriod1.Size = New System.Drawing.Size(94, 15)
        Me.lblPeriod1.TabIndex = 57
        Me.lblPeriod1.Text = "Period"
        Me.lblPeriod1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPeriod1.Visible = False
        '
        'cboPeriod1
        '
        Me.cboPeriod1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod1.DropDownWidth = 180
        Me.cboPeriod1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod1.FormattingEnabled = True
        Me.cboPeriod1.Location = New System.Drawing.Point(107, 366)
        Me.cboPeriod1.Name = "cboPeriod1"
        Me.cboPeriod1.Size = New System.Drawing.Size(118, 21)
        Me.cboPeriod1.TabIndex = 58
        Me.cboPeriod1.Visible = False
        '
        'frmPSSSFReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(732, 291)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPSSSFReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmPSSSFReport"
        Me.Controls.SetChildIndex(Me.gbMandatoryInfo, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblVOLHead As System.Windows.Forms.Label
    Friend WithEvents cboVOLHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblIdentity As System.Windows.Forms.Label
    Friend WithEvents objlblExRate As System.Windows.Forms.Label
    Friend WithEvents LblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmpHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowBasicSal As System.Windows.Forms.CheckBox
    Friend WithEvents lblCompanyHeadType As System.Windows.Forms.Label
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblEmpContriHeadType As System.Windows.Forms.Label
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboCoHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod1 As System.Windows.Forms.Label
    Friend WithEvents cboPeriod1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboIdentity As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
End Class
