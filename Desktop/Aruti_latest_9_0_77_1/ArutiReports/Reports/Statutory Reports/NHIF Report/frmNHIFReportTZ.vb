'************************************************************************************************************************************
'Class Name   : clsNHIFReportTZ.vb
'Form Name    : frmNHIFReportTZ.vb
'Purpose      : For Tanzania
'Written By   : Rana Varsha.
'Created Date : 30-Dec-2017
'Modified     : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmNHIFReportTZ

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmNHIFReportTZ"
    Private objNHIF As clsNHIFReportTZ
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private eReport As enArutiReport
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Contructor "

    Public Sub New()
        _Show_ExcelExtra_Menu = True
        objNHIF = New clsNHIFReportTZ(User._Object._Languageunkid, Company._Object._Companyunkid)
        objNHIF.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

    'Sohail (17 Oct 2020) -- Start
    'FFK Enhancement # OLD-67 : - Give option to Save User-Selected Settings when Generating Statutory Reports NSSF and NHIF Reports for Kenyan Companies - FFK.
#Region " Private Enum "
    Private Enum enHeadTypeId
        Membership = 1
        Currency = 2
        Include_Membership = 3
        ShowRemark = 4
        ShowRepoNameExcelAdvance = 5
        Ignore_Zero = 6
    End Enum
#End Region
    'Sohail (17 Oct 2020) -- End

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMember As New clsmembership_master
        Dim dsCombos As New DataSet
        Dim objperiod As New clscommom_period_Tran
        Dim objEmpContribution As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Try
            dsCombos = objMember.getListForCombo("Membership", True, , 1)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objMember.getListForCombo("Membership", True, , 2)
            With cboIncludeMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Membership")
                .SelectedValue = 0
            End With

            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)

            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", True)
            cboCurrency.ValueMember = "countryunkid"
            cboCurrency.DisplayMember = "currency_sign"
            cboCurrency.DataSource = dsCombos.Tables("Currency")

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objperiod = Nothing
            objEmpContribution = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboIncludeMembership.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            cboCurrency.SelectedValue = 0
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            mstrAdvanceFilter = ""

            chkShowRemark.Checked = False

            chkShowReportNameOnReport.Checked = False

            Call GetValue() 'Sohail (17 Oct 2020)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    'Sohail (17 Oct 2020) -- Start
    'FFK Enhancement # OLD-67 : - Give option to Save User-Selected Settings when Generating Statutory Reports NSSF and NHIF Reports for Kenyan Companies - FFK.
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.NHIF_Report_TZ)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Currency
                            cboCurrency.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Include_Membership
                            cboIncludeMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.ShowRemark
                            chkShowRemark.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.ShowRepoNameExcelAdvance
                            chkShowReportNameOnReport.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Ignore_Zero
                            chkIgnoreZero.Checked = CBool(dsRow.Item("transactionheadid"))

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (17 Oct 2020) -- End

    Private Function SetFilter() As Boolean
        Try
            Call objNHIF.SetDefaultValue()

            If CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Exit Function
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            End If

            If CInt(cboMembership.SelectedValue) > 0 Then
                objNHIF._MembershipId = cboMembership.SelectedValue
                objNHIF._MembershipName = cboMembership.Text
            End If

            If CInt(cboIncludeMembership.SelectedValue) > 0 Then
                objNHIF._NonPayrollMembershipId = cboIncludeMembership.SelectedValue
                objNHIF._NonPayrollMembershipName = cboIncludeMembership.Text
            Else
                objNHIF._NonPayrollMembershipName = Language.getMessage(mstrModuleName, 3, "ID No.")
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objNHIF._PeriodId = cboPeriod.SelectedValue
                objNHIF._PeriodName = cboPeriod.Text
                objNHIF._PeriodCode = CType(cboPeriod.SelectedItem, DataRowView).Item("code").ToString
            End If

            objNHIF._ViewByIds = mstrStringIds
            objNHIF._ViewIndex = mintViewIdx
            objNHIF._ViewByName = mstrStringName
            objNHIF._Analysis_Fields = mstrAnalysis_Fields
            objNHIF._Analysis_Join = mstrAnalysis_Join
            objNHIF._Report_GroupName = mstrReport_GroupName

            If (mintBaseCurrId = mintPaidCurrencyId) Or mintPaidCurrencyId <= 0 Then
                mintPaidCurrencyId = mintBaseCurrId
            End If

            objNHIF._CountryId = CInt(cboCurrency.SelectedValue)
            objNHIF._BaseCurrencyId = mintBaseCurrId
            objNHIF._PaidCurrencyId = mintPaidCurrencyId
            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3875 - 76.1 - Put link for NHIF EFT and it should generate report without report header of report name address and company name and add "EmployerNo" column..
            objNHIF._CurrencyName = cboCurrency.Text.Trim
            'Sohail (10 Jul 2019) -- End

            If mdecBaseExRate > 0 AndAlso mdecPaidExRate Then
                objNHIF._ConversionRate = mdecPaidExRate / mdecBaseExRate
            End If

            objNHIF._ExchangeRate = LblCurrencyRate.Text

            objNHIF._ShowRemark = chkShowRemark.Checked
            objNHIF._IgnoreZero = chkIgnoreZero.Checked

            objNHIF._ShowReportNameOnReport = chkShowReportNameOnReport.Checked
            objNHIF._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname

            'Hemant (18 Mar 2019) -- Start
            'ISSUE#3600-UONGOZI INSTITUTE: NHIF is not showing basic salary even when you tick "Show basic Salary" in membership master.
            Dim objMember As New clsmembership_master
            objMember._Membershipunkid = CInt(cboMembership.SelectedValue)
            objNHIF._ShowBasicSalary = objMember._IsShowBasicSalary
            'Hemant (18 Mar 2019) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmNHIFReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objNHIF = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPPFContributionReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmNHIFReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = objNHIF._ReportName
            Me._Message = objNHIF._ReportDesc
            Call OtherSettings()
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPPFContributionReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then

                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objNHIF.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                          User._Object._Userunkid, _
            '                          FinancialYear._Object._YearUnkid, _
            '                          Company._Object._Companyunkid, _
            '                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                          ConfigParameter._Object._UserAccessModeSetting, _
            '                          True, ConfigParameter._Object._ExportReportPath, _
            '                          ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            objNHIF._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objNHIF.generateReportNew(FinancialYear._Object._DatabaseName, _
                                      User._Object._Userunkid, _
                                      FinancialYear._Object._YearUnkid, _
                                      Company._Object._Companyunkid, _
                                      objPeriod._Start_Date, _
                                      objPeriod._End_Date, _
                                      ConfigParameter._Object._UserAccessModeSetting, _
                                      True, ConfigParameter._Object._ExportReportPath, _
                                      ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3875 - 76.1 - Put link for NHIF EFT and it should generate report without report header of report name address and company name and add "EmployerNo" column.          
            'objNHIF.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                          User._Object._Userunkid, _
            '                          FinancialYear._Object._YearUnkid, _
            '                          Company._Object._Companyunkid, _
            '                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                          ConfigParameter._Object._UserAccessModeSetting, _
            '                          True, ConfigParameter._Object._ExportReportPath, _
            '                          ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            objNHIF._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date)) 'Sohail (03 Aug 2019)

            objNHIF.generateReportNew(FinancialYear._Object._DatabaseName, _
                                      User._Object._Userunkid, _
                                      FinancialYear._Object._YearUnkid, _
                                      Company._Object._Companyunkid, _
                                     objPeriod._Start_Date, _
                                     objPeriod._End_Date, _
                                      ConfigParameter._Object._UserAccessModeSetting, _
                                      True, ConfigParameter._Object._ExportReportPath, _
                                      ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (10 Jul 2019) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsNHIFReportTZ.SetMessages()
            objfrm._Other_ModuleNames = "clsNHIFReportTZ"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Sohail (17 Oct 2020) -- Start
    'FFK Enhancement # OLD-67 : - Give option to Save User-Selected Settings when Generating Statutory Reports NSSF and NHIF Reports for Kenyan Companies - FFK.
    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.NHIF_Report_TZ
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NHIF_Report_TZ, 0, 0, intHeadType)

                    Case enHeadTypeId.Currency
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCurrency.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NHIF_Report_TZ, 0, 0, intHeadType)

                    Case enHeadTypeId.Include_Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboIncludeMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NHIF_Report_TZ, 0, 0, intHeadType)

                    Case enHeadTypeId.ShowRemark
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = chkShowRemark.Checked.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NHIF_Report_TZ, 0, 0, intHeadType)

                    Case enHeadTypeId.ShowRepoNameExcelAdvance
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = chkShowReportNameOnReport.Checked.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NHIF_Report_TZ, 0, 0, intHeadType)

                    Case enHeadTypeId.Ignore_Zero
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = chkIgnoreZero.Checked.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NHIF_Report_TZ, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (17 Oct 2020) -- End

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'Sohail (10 Jul 2019) -- Start
    'Good Neighbours Enhancement - Support Issue Id # 3875 - 76.1 - Put link for NHIF EFT and it should generate report without report header of report name address and company name and add "EmployerNo" column..
    Private Sub lnkEFT_NHIF_Export_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFT_NHIF_Export.LinkClicked
        Try
            If SetFilter() = False Then Exit Try

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            If objNHIF.Generate_EFT_NHIF_Report(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport) = True Then

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_NHIF_Export_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (10 Jul 2019) -- End

#End Region

#Region "ComboBox Event"

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboCurrency.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                Dim dsList As DataSet = Nothing
                dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate")).ToTable

                If dtTable.Rows.Count > 0 Then
                    mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                    mintPaidCurrencyId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    LblCurrencyRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                Else
                    mdecBaseExRate = 0
                    mdecPaidExRate = 0
                    mintPaidCurrencyId = 0
                    LblCurrencyRate.Text = ""
                End If
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
                mintPaidCurrencyId = 0
                LblCurrencyRate.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.lblIncludeMembership.Text = Language._Object.getCaption(Me.lblIncludeMembership.Name, Me.lblIncludeMembership.Text)
			Me.chkIgnoreZero.Text = Language._Object.getCaption(Me.chkIgnoreZero.Name, Me.chkIgnoreZero.Text)
			Me.chkShowReportNameOnReport.Text = Language._Object.getCaption(Me.chkShowReportNameOnReport.Name, Me.chkShowReportNameOnReport.Text)
			Me.chkShowRemark.Text = Language._Object.getCaption(Me.chkShowRemark.Name, Me.chkShowRemark.Text)
			Me.lnkEFT_NHIF_Export.Text = Language._Object.getCaption(Me.lnkEFT_NHIF_Export.Name, Me.lnkEFT_NHIF_Export.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue.")
			Language.setMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 3, "ID No.")
			Language.setMessage(mstrModuleName, 4, "Selection Saved Successfully")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
