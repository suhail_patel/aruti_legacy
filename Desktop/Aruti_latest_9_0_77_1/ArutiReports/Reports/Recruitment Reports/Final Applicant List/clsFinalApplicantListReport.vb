'************************************************************************************************************************************
'Class Name : clsFinalApplicantListReport.vb
'Purpose    :
'Date       :12/09/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsFinalApplicantListReport

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsFinalApplicantListReport"
    Private mintApplicantUnkid As Integer = -1
    Private mstrApplicantName As String = String.Empty
    Private mintVacancyUnkid As Integer = -1
    Private mstrVacancyName As String = ""
    Private mstrApplicantIds As String = String.Empty
    Private mstrApplicantNames As String = String.Empty
    Private mstrFilterCriteria As String = String.Empty
    Private mstrFilterValue As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _ApplicantUnkid() As Integer
        Set(ByVal value As Integer)
            mintApplicantUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantName() As String
        Set(ByVal value As String)
            mstrApplicantName = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantIds() As String
        Set(ByVal value As String)
            mstrApplicantIds = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantNames() As String
        Set(ByVal value As String)
            mstrApplicantNames = value
        End Set
    End Property

    Public WriteOnly Property _VacancyUnkid() As Integer
        Set(ByVal value As Integer)
            mintVacancyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _VacancyName() As String
        Set(ByVal value As String)
            mstrVacancyName = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Private Sub Apply_Filter()
        Try
            'If mintApplicantUnkid > 0 Then
            '    mstrFilterValue = "applicantunkid  = '" & mintApplicantUnkid & "'"
            '    mstrFilterCriteria &= Language.getMessage(mstrModuleName, 1, "Applicant(s) : ") & " " & mstrApplicantName & " "
            'End If

            If mintVacancyUnkid > 0 Then
                If mstrFilterValue.Length > 0 Then
                    mstrFilterValue &= " AND vacancyunkid = '" & mintVacancyUnkid & "'"
                Else
                    mstrFilterValue = "vacancyunkid  = '" & mintVacancyUnkid & "'"
                End If
                mstrFilterCriteria &= Language.getMessage(mstrModuleName, 2, "Vacancy : ") & " " & mstrVacancyName & " "
            End If

            If mstrApplicantIds.Trim.Length > 0 Then
                If mstrFilterValue.Length > 0 Then
                    mstrFilterValue &= " AND applicantunkid IN (" & mstrApplicantIds & ")"
                Else
                    mstrFilterValue &= "applicantunkid IN (" & mstrApplicantIds & ")"
                End If
                'mstrFilterCriteria &= Language.getMessage(mstrModuleName, 1, "Applicant(s) : ") & " " & mstrApplicantNames & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Apply_Filter; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Public Sub Print_List(ByVal dsList As DataSet)
    Public Sub Print_List(ByVal dsList As DataSet, _
                          ByVal xUserUnkid As Integer, _
                          ByVal xCompanyUnkid As Integer, _
                          ByVal xNoofPagestoPrint As Integer)
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim dtTable As DataTable
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objUser As New clsUserAddEdit
        Dim objCompany As New clsCompany_Master
        'Shani(24-Aug-2015) -- End
        Try
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objUser._Userunkid = xUserUnkid
            objCompany._Companyunkid = xCompanyUnkid
            'Shani(24-Aug-2015) -- End

            If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Call Apply_Filter()
                If mstrFilterValue.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables(0), mstrFilterValue, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                End If

                rpt_Data = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dtTable.Rows
                    Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                    rpt_Row.Item("Column1") = dtRow.Item("applicantname")
                    rpt_Row.Item("Column2") = dtRow.Item("Address")
                    rpt_Row.Item("Column3") = dtRow.Item("mobile")
                    rpt_Row.Item("Column4") = dtRow.Item("email")

                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

                objRpt = New ArutiReport.Designer.rptFinalApplicantList

                objRpt.SetDataSource(rpt_Data)

                Dim arrImageRow As DataRow = Nothing
                arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

                ReportFunction.Logo_Display(objRpt, _
                                            True, _
                                            ConfigParameter._Object._ShowLogoRightSide, _
                                            "arutiLogo1", _
                                            "arutiLogo2", _
                                            arrImageRow, _
                                            "txtCompanyName", _
                                            "txtReportName", _
                                            "txtFilterDescription")

                rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

                If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                    rpt_Data.Tables("ArutiTable").Rows.Add("")
                End If


                Call ReportFunction.TextChange(objRpt, "txtApplicantName", Language.getMessage(mstrModuleName, 3, "Applicant"))
                Call ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 4, "Address"))
                Call ReportFunction.TextChange(objRpt, "txtContact", Language.getMessage(mstrModuleName, 5, "Contact No."))
                Call ReportFunction.TextChange(objRpt, "txtEmail", Language.getMessage(mstrModuleName, 6, "Email Address"))

                Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 7, "Printed By :"))
                Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 8, "Printed Date :"))
                Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 9, "Page :"))

                Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 10, "Final Applicant(s) List"))
                Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Now.Date)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'Call ReportFunction.TextChange(objRpt, "txtPrintedBy", User._Object._Username)

                'Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)
                Call ReportFunction.TextChange(objRpt, "txtPrintedBy", objUser._Username)

                Call ReportFunction.TextChange(objRpt, "txtCompanyName", objCompany._Name)
                'Shani(24-Aug-2015) -- End

                Call ReportFunction.TextChange(objRpt, "txtFilterDescription", mstrFilterCriteria)

                Dim prd As New System.Drawing.Printing.PrintDocument
                objRpt.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
                objRpt.PrintOptions.PaperSize = prd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objRpt.PrintToPrinter(ConfigParameter._Object._NoofPagestoPrint, False, 0, 0)
                objRpt.PrintToPrinter(xNoofPagestoPrint, False, 0, 0)
                'Shani(24-Aug-2015) -- End


            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Print_List; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Applicant(s) :")
            Language.setMessage(mstrModuleName, 2, "Vacancy :")
            Language.setMessage(mstrModuleName, 3, "Applicant")
            Language.setMessage(mstrModuleName, 4, "Address")
            Language.setMessage(mstrModuleName, 5, "Contact No.")
            Language.setMessage(mstrModuleName, 6, "Email Address")
            Language.setMessage(mstrModuleName, 7, "Printed By :")
            Language.setMessage(mstrModuleName, 8, "Printed Date :")
            Language.setMessage(mstrModuleName, 9, "Page :")
            Language.setMessage(mstrModuleName, 10, "Final Applicant(s) List")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
