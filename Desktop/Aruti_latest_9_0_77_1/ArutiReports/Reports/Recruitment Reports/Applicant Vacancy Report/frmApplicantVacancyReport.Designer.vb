﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApplicantVacancyReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.LalblVacancy = New System.Windows.Forms.Label
        Me.objbtnSearchApplicant = New eZee.Common.eZeeGradientButton
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.lblVacancyType = New System.Windows.Forms.Label
        Me.cboApplicant = New System.Windows.Forms.ComboBox
        Me.lblApplicant = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 457)
        Me.NavPanel.Size = New System.Drawing.Size(767, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.LalblVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchApplicant)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.cboApplicant)
        Me.gbFilterCriteria.Controls.Add(Me.lblApplicant)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(401, 121)
        Me.gbFilterCriteria.TabIndex = 3
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(365, 87)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 29
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 450
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.IntegralHeight = False
        Me.cboVacancy.Location = New System.Drawing.Point(97, 87)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(262, 21)
        Me.cboVacancy.TabIndex = 6
        '
        'LalblVacancy
        '
        Me.LalblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LalblVacancy.Location = New System.Drawing.Point(8, 89)
        Me.LalblVacancy.Name = "LalblVacancy"
        Me.LalblVacancy.Size = New System.Drawing.Size(84, 15)
        Me.LalblVacancy.TabIndex = 5
        Me.LalblVacancy.Text = "Vacancy"
        Me.LalblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchApplicant
        '
        Me.objbtnSearchApplicant.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchApplicant.BorderSelected = False
        Me.objbtnSearchApplicant.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchApplicant.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchApplicant.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchApplicant.Location = New System.Drawing.Point(365, 33)
        Me.objbtnSearchApplicant.Name = "objbtnSearchApplicant"
        Me.objbtnSearchApplicant.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchApplicant.TabIndex = 2
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(97, 60)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(262, 21)
        Me.cboVacancyType.TabIndex = 4
        '
        'lblVacancyType
        '
        Me.lblVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyType.Location = New System.Drawing.Point(8, 63)
        Me.lblVacancyType.Name = "lblVacancyType"
        Me.lblVacancyType.Size = New System.Drawing.Size(84, 15)
        Me.lblVacancyType.TabIndex = 3
        Me.lblVacancyType.Text = "Vacancy Type"
        Me.lblVacancyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApplicant
        '
        Me.cboApplicant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApplicant.DropDownWidth = 450
        Me.cboApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApplicant.FormattingEnabled = True
        Me.cboApplicant.Location = New System.Drawing.Point(97, 33)
        Me.cboApplicant.Name = "cboApplicant"
        Me.cboApplicant.Size = New System.Drawing.Size(262, 21)
        Me.cboApplicant.TabIndex = 1
        '
        'lblApplicant
        '
        Me.lblApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicant.Location = New System.Drawing.Point(8, 36)
        Me.lblApplicant.Name = "lblApplicant"
        Me.lblApplicant.Size = New System.Drawing.Size(84, 15)
        Me.lblApplicant.TabIndex = 0
        Me.lblApplicant.Text = "Applicant"
        Me.lblApplicant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmApplicantVacancyReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 512)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApplicantVacancyReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmApplicantVacancyReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents LalblVacancy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchApplicant As eZee.Common.eZeeGradientButton
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancyType As System.Windows.Forms.Label
    Friend WithEvents cboApplicant As System.Windows.Forms.ComboBox
    Friend WithEvents lblApplicant As System.Windows.Forms.Label
End Class
