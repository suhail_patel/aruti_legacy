'************************************************************************************************************************************
'Class Name : frmStaffRequisitionFormReport.vb
'Purpose    : 
'Written By : Sandeep J Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmStaffRequisitionFormReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmStaffRequisitionFormReport"
    Private objRequisitionForm As clsStaffRequisitionFormReport
    Private mdtRequisitionForm As DataTable
    Private mdtAllocation As DataTable
    Private dvFormNo As DataView
    Private dvAlloc As DataView
    'Hemant (01 Nov 2021) -- Start
    'ENHANCEMENT : OLD-515 - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
    Private mintStaffRequisitionTranunkid As Integer = 0
    'Hemant (01 Nov 2021) -- End
#End Region

#Region "Properties"

    'Hemant (01 Nov 2021) -- Start
    'ENHANCEMENT : OLD-515 - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
    Public WriteOnly Property _StaffRequisitionTranunkid() As Integer
        Set(ByVal value As Integer)
            mintStaffRequisitionTranunkid = value
        End Set
    End Property
    'Hemant (01 Nov 2021) -- End

#End Region

#Region " Contructor "

    Public Sub New()
        objRequisitionForm = New clsStaffRequisitionFormReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objRequisitionForm.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objJob As New clsJobs
        Dim objRequisition As New clsStaffrequisition_Tran
        Dim dsList As New DataSet
        Try
            dsList = objMaster.GetEAllocation_Notification("List")
            Dim dr As DataRow = dsList.Tables(0).NewRow()
            dr("id") = 0 : dr("name") = Language.getMessage(mstrModuleName, 1, "Select")
            dsList.Tables(0).Rows.InsertAt(dr, 0)
            With cboRequistionBy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objMaster.GetListForStaffRequisitionStatus(True, "Status")
            With cboRequisitionType
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Status")
                .SelectedValue = 0
            End With

            dsList = objJob.getComboList("Job", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Job")
                .SelectedValue = 0
            End With
            'Sohail (18 Aug 2021) -- Start
            'NMB Enhancement : OLD-431 : Show staff requisition form report on MSS. Staff Requisition Form No. list should be specific to the job selected. .
            'dsList = objRequisition.GetList("List", "", "")
            'mdtRequisitionForm = dsList.Tables("List").Copy
            'Dim dcol As New DataColumn
            'With dcol
            '    .ColumnName = "ischeck"
            '    .DataType = GetType(System.Boolean)
            '    .DefaultValue = False
            'End With
            'mdtRequisitionForm.Columns.Add(dcol)
            'dgvFormNo.AutoGenerateColumns = False
            'dvFormNo = mdtRequisitionForm.DefaultView
            'objdgcolhFCheck.DataPropertyName = "ischeck"
            'dgcolhFormNo.DataPropertyName = "formno"
            'objdgcolhFormId.DataPropertyName = "staffrequisitiontranunkid"
            'dgvFormNo.DataSource = dvFormNo
            'Sohail (18 Aug 2021) -- End

            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            dsList = objMaster.getApprovalStatus("Job", True, True, True, True, True, True, True)
            With cboStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Job")
                .SelectedValue = 0
            End With
            'Hemant (01 Nov 2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objMaster = Nothing : objJob = Nothing : objRequisition = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboJob.SelectedValue = 0
            cboRequisitionType.SelectedValue = 0
            cboRequistionBy.SelectedValue = 0
            dtpWStartDateFrom.Checked = False
            dtpWStartDateTo.Checked = False
            nudPositionFrom.Value = 0
            nudPositionTo.Value = 0
            objchkFormNo.Checked = False
            txtSearchFormNo.Text = ""
            txtSearchAllocation.Text = ""
            objchkAllocation.Checked = False
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            cboStatus.SelectedValue = 0
            'Hemant (01 Nov 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objRequisitionForm.SetDefaultValue()
            mdtRequisitionForm.AcceptChanges()
            If mdtAllocation IsNot Nothing Then mdtAllocation.AcceptChanges()
            If mdtRequisitionForm.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Please check atleast one staff requisition form number in order to generate report."), enMsgBoxStyle.Information)
                dgvFormNo.Focus()
                Return False
            End If

            If mdtAllocation IsNot Nothing AndAlso mdtAllocation.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count > 0 Then
                objRequisitionForm._AllocationIds = String.Join(",", mdtAllocation.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)(objdgcolhAllocId.DataPropertyName).ToString()).ToArray())
                objRequisitionForm._AllocationNames = String.Join(",", mdtAllocation.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of String)(objdgcolhAllocation.DataPropertyName).ToString()).ToArray())
            End If
            objRequisitionForm._JobId = CInt(cboJob.SelectedValue)
            objRequisitionForm._JobName = cboJob.Text
            objRequisitionForm._PositionFrom = nudPositionFrom.Value
            objRequisitionForm._PositionTo = nudPositionTo.Value
            objRequisitionForm._RequisitionById = CInt(cboRequistionBy.SelectedValue)
            objRequisitionForm._RequisitionByName = cboRequistionBy.Text
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            objRequisitionForm._StatusId = CInt(cboStatus.SelectedValue)
            objRequisitionForm._StatusName = cboStatus.Text
            'Hemant (01 Nov 2021) -- End
            If mdtRequisitionForm.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count > 0 Then
                objRequisitionForm._RequisitionFormIds = String.Join(",", mdtRequisitionForm.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)(objdgcolhFormId.DataPropertyName).ToString()).ToArray())
                objRequisitionForm._RequisitionFormNos = String.Join(",", mdtRequisitionForm.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of String)(dgcolhFormNo.DataPropertyName).ToString()).ToArray())
            End If
            objRequisitionForm._RequisitionTypeId = CInt(cboRequisitionType.SelectedValue)
            objRequisitionForm._RequisitionTypeName = cboRequisitionType.Text
            If dtpWStartDateFrom.Checked = True Then
                objRequisitionForm._WorkFromDate = dtpWStartDateFrom.Value.Date
            End If
            If dtpWStartDateTo.Checked = True Then
                objRequisitionForm._WorkToDate = dtpWStartDateTo.Value.Date
            End If
            'Sohail (14 Sep 2021) -- Start
            'NMB Issue :  : Staff requisition report showing wrong signature (printed by).
            If User._Object._Firstname.Trim <> "" Then
                objRequisitionForm._User_Name = User._Object._Firstname & " " & User._Object._Lastname
            Else
                objRequisitionForm._User_Name = User._Object._Username
            End If
            'Sohail (14 Sep 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case cboRequistionBy.SelectedValue
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
                Case enAllocation.COST_CENTER
                    Dim objCC As New clscostcenter_master
                    dList = objCC.GetList("List")
                    Call Fill_List(dList.Tables(0), "costcenterunkid", "costcentername")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            Dim dcol As New DataColumn
            With dcol
                .ColumnName = "ischeck"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            dTable.Columns.Add(dcol)
            mdtAllocation = dTable.Copy()
            dvAlloc = mdtAllocation.DefaultView
            dgvAllocation.AutoGenerateColumns = False
            objdgcolhACheck.DataPropertyName = "ischeck"
            objdgcolhAllocation.DataPropertyName = StrDisColName
            objdgcolhAllocId.DataPropertyName = StrIdColName
            dgvAllocation.DataSource = dvAlloc
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    'Sohail (18 Aug 2021) -- Start
    'NMB Enhancement : OLD-431 : Show staff requisition form report on MSS. Staff Requisition Form No. list should be specific to the job selected. .
    Private Sub Fill_FormNo()
        Dim objRequisition As New clsStaffrequisition_Tran
        Dim dsList As DataSet
        Dim strFilter As String = ""
        Try
            If CInt(cboRequisitionType.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_tran.staffrequisitiontypeid = " & CInt(cboRequisitionType.SelectedValue) & " "
            End If

            If CInt(cboRequistionBy.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_tran.staffrequisitionbyid = " & CInt(cboRequistionBy.SelectedValue) & " "
            End If

            If CInt(cboJob.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_tran.jobunkid = " & CInt(cboJob.SelectedValue) & " "
            End If

            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            If CInt(cboStatus.SelectedValue) > 0 Then
                strFilter &= "AND rcstaffrequisition_tran.form_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If
            'Hemant (01 Nov 2021) -- End

            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(4)

            dsList = objRequisition.GetList("List", strFilter, "")
            mdtRequisitionForm = dsList.Tables("List").Copy
            Dim dcol As New DataColumn
            With dcol
                .ColumnName = "ischeck"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtRequisitionForm.Columns.Add(dcol)
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-515 - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
            If mintStaffRequisitionTranunkid > 0 Then
                For Each drRow As DataRow In mdtRequisitionForm.Select("staffrequisitiontranunkid =" & mintStaffRequisitionTranunkid)
                    drRow("ischeck") = True
                Next
                mdtRequisitionForm.AcceptChanges()
            End If
            'Hemant (01 Nov 2021) -- End
            dgvFormNo.AutoGenerateColumns = False
            dvFormNo = mdtRequisitionForm.DefaultView
            objdgcolhFCheck.DataPropertyName = "ischeck"
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-504 - Staff requisition form report should display job name against the staff requisition form no. Also provide a status filter on this report.
            'dgcolhFormNo.DataPropertyName = "formno"
            dgcolhFormNo.DataPropertyName = "FormNoWithJobTitle"
            'Hemant (01 Nov 2021) -- End
            objdgcolhFormId.DataPropertyName = "staffrequisitiontranunkid"
            dgvFormNo.DataSource = dvFormNo

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_FormNo", mstrModuleName)
        Finally
            objRequisition = Nothing
        End Try
    End Sub
    'Sohail (18 Aug 2021) -- End

#End Region

#Region " Form Event(s) "

    Private Sub frmStaffRequisitionFormReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objRequisitionForm = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmStaffRequisitionFormReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStaffRequisitionFormReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Me._Title = objRequisitionForm._ReportName
            Me._Message = objRequisitionForm._ReportDesc
            Call FillCombo()
            Call ResetValue()
            'Hemant (01 Nov 2021) -- Start
            'ENHANCEMENT : OLD-515 - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
            mintStaffRequisitionTranunkid = 0
            'Hemant (01 Nov 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffRequisitionFormReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStaffRequisitionFormReport.SetMessages()
            objfrm._Other_ModuleNames = "clsStaffRequisitionFormReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button Event(s) "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub
            objRequisitionForm.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            objRequisitionForm.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJob.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboJob.DataSource
            frm.ValueMember = cboJob.ValueMember
            frm.DisplayMember = cboJob.DisplayMember
            If frm.DisplayDialog Then
                cboJob.SelectedValue = frm.SelectedValue
                cboJob.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJob_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboRequistionBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRequistionBy.SelectedIndexChanged
        Try
            If CInt(cboRequistionBy.SelectedValue) > 0 Then
                Call Fill_Data()
                tlbAllocation.Enabled = True
                objdgcolhAllocation.HeaderText = cboRequistionBy.Text
            Else
                objdgcolhAllocation.HeaderText = ""
                txtSearchAllocation.Text = ""
                objchkAllocation.Checked = False
                dgvAllocation.DataSource = Nothing
                tlbAllocation.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboRequistionBy_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (18 Aug 2021) -- Start
    'NMB Enhancement : OLD-431 : Show staff requisition form report on MSS. Staff Requisition Form No. list should be specific to the job selected. .
    Private Sub cboJob_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRequisitionType.SelectedIndexChanged, cboRequistionBy.SelectedIndexChanged, cboJob.SelectedIndexChanged, _
                                                                                                        cboStatus.SelectedIndexChanged

        'Hemant (01 Nov 2021) -- [cboStatus.SelectedIndexChanged]
        Try
            Call Fill_FormNo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJob_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Aug 2021) -- End

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearchFormNo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchFormNo.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvFormNo.Rows.Count > 0 Then
                        If dgvFormNo.SelectedRows(0).Index = dgvFormNo.Rows(dgvFormNo.RowCount - 1).Index Then Exit Sub
                        dgvFormNo.Rows(dgvFormNo.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvFormNo.Rows.Count > 0 Then
                        If dgvFormNo.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvFormNo.Rows(dgvFormNo.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchFormNo_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchFormNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchFormNo.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchFormNo.Text.Trim.Length > 0 Then
                strSearch = dgcolhFormNo.DataPropertyName & " LIKE '%" & txtSearchFormNo.Text & "%'"
            End If
            dvFormNo.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchFormNo_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchAllocation_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchAllocation.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAllocation.Rows.Count > 0 Then
                        If dgvAllocation.SelectedRows(0).Index = dgvAllocation.Rows(dgvAllocation.RowCount - 1).Index Then Exit Sub
                        dgvAllocation.Rows(dgvAllocation.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAllocation.Rows.Count > 0 Then
                        If dgvAllocation.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAllocation.Rows(dgvAllocation.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchAllocation_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchAllocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchAllocation.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchAllocation.Text.Trim.Length > 0 Then
                strSearch = objdgcolhAllocation.DataPropertyName & " LIKE '%" & txtSearchAllocation.Text & "%'"
            End If
            dvAlloc.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchAllocation_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGridView Event(s) "

    Private Sub dgvFormNo_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFormNo.CellContentClick
        Try
            RemoveHandler objchkFormNo.CheckedChanged, AddressOf objchkFormNo_CheckedChanged
            If e.ColumnIndex = objdgcolhFCheck.Index Then

                If Me.dgvFormNo.IsCurrentCellDirty Then
                    Me.dgvFormNo.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dvFormNo.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dvFormNo.ToTable.Rows.Count = drRow.Length Then
                        objchkFormNo.CheckState = CheckState.Checked
                    Else
                        objchkFormNo.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkFormNo.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkFormNo.CheckedChanged, AddressOf objchkFormNo_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvFormNo_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAllocation_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAllocation.CellContentClick
        Try
            RemoveHandler objchkAllocation.CheckedChanged, AddressOf objchkAllocation_CheckedChanged
            If e.ColumnIndex = objdgcolhACheck.Index Then

                If Me.dgvAllocation.IsCurrentCellDirty Then
                    Me.dgvAllocation.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dvAlloc.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dvAlloc.ToTable.Rows.Count = drRow.Length Then
                        objchkAllocation.CheckState = CheckState.Checked
                    Else
                        objchkAllocation.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAllocation.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkAllocation.CheckedChanged, AddressOf objchkAllocation_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAllocation_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkFormNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkFormNo.CheckedChanged
        Try
            RemoveHandler dgvFormNo.CellContentClick, AddressOf dgvFormNo_CellContentClick
            For Each dr As DataRowView In dvFormNo
                dr.Item("ischeck") = CBool(objchkFormNo.CheckState)
            Next
            dgvFormNo.Refresh()
            AddHandler dgvFormNo.CellContentClick, AddressOf dgvFormNo_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkFormNo_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAllocation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllocation.CheckedChanged
        Try
            RemoveHandler dgvAllocation.CellContentClick, AddressOf dgvAllocation_CellContentClick
            For Each dr As DataRowView In dvAlloc
                dr.Item("ischeck") = CBool(objchkAllocation.CheckState)
            Next
            dgvAllocation.Refresh()
            AddHandler dgvAllocation.CellContentClick, AddressOf dgvAllocation_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllocation_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblWorkStartDateTo.Text = Language._Object.getCaption(Me.lblWorkStartDateTo.Name, Me.lblWorkStartDateTo.Text)
            Me.lblWorkStartDateFrom.Text = Language._Object.getCaption(Me.lblWorkStartDateFrom.Name, Me.lblWorkStartDateFrom.Text)
            Me.lblPositionTo.Text = Language._Object.getCaption(Me.lblPositionTo.Name, Me.lblPositionTo.Text)
            Me.lblPositionFrom.Text = Language._Object.getCaption(Me.lblPositionFrom.Name, Me.lblPositionFrom.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblRequisitionBy.Text = Language._Object.getCaption(Me.lblRequisitionBy.Name, Me.lblRequisitionBy.Text)
            Me.lblRequisitiontype.Text = Language._Object.getCaption(Me.lblRequisitiontype.Name, Me.lblRequisitiontype.Text)
            Me.dgcolhFormNo.HeaderText = Language._Object.getCaption(Me.dgcolhFormNo.Name, Me.dgcolhFormNo.HeaderText)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

  
End Class
