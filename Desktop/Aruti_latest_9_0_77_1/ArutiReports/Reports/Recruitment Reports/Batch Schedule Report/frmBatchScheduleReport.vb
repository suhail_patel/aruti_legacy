'************************************************************************************************************************************
'Class Name : frmBatchScheduleReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmBatchScheduleReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmBatchScheduleReport"
    Private objBtachReport As clsBatchScheduleReport

#End Region

#Region " Contructor "

    Public Sub New()
        objBtachReport = New clsBatchScheduleReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objBtachReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCMaster As New clsCommon_Master
        Dim objVMaster As New clsVacancy
        Try
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "List")
            With cboInterviewType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "List")
            With cboResultGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With


            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'dsCombo = objVMaster.getComboList(True, "List")

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objVMaster.getComboList(True, "List", , , , , True)
            dsCombo = objVMaster.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , , , , True)
            'Shani(24-Aug-2015) -- End

            'Anjan (02 Mar 2012)-End


            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objCMaster = Nothing
            objVMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboInterviewType.SelectedValue = 0
            cboResultGroup.SelectedValue = 0
            cboVacancy.SelectedValue = 0
            txtBatchName.Text = ""
            txtInterviewerName.Text = ""
            txtLocation.Text = ""
            objBtachReport.setDefaultOrderBy(0)
            txtOrderBy.Text = objBtachReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objBtachReport.SetDefaultValue()

            objBtachReport._BatchName = txtBatchName.Text.Trim
            objBtachReport._InterviewerName = txtInterviewerName.Text.Trim

            objBtachReport._InterviewTypeId = cboInterviewType.SelectedValue
            objBtachReport._InterviewTypeName = cboInterviewType.Text

            objBtachReport._LocationName = txtLocation.Text.Trim

            objBtachReport._ResultGroupId = cboResultGroup.SelectedValue
            objBtachReport._ResultGroupName = cboResultGroup.Text

            objBtachReport._VacancyId = cboVacancy.SelectedValue
            objBtachReport._VacancyName = cboVacancy.Text

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmBatchScheduleReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBtachReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmBatchScheduleReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBatchScheduleReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objBtachReport._ReportName
            Me._Message = objBtachReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatchScheduleReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'objBtachReport.generateReport(0, e.Type, enExportAction.None)
            objBtachReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, _
                                             0, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'objBtachReport.generateReport(0, enPrintAction.None, e.Type)
            objBtachReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, _
                                             0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objBtachReport.setOrderBy(0)
            txtOrderBy.Text = objBtachReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBatchScheduleReport.SetMessages()
            objfrm._Other_ModuleNames = "clsBatchScheduleReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblInterviewType.Text = Language._Object.getCaption(Me.lblInterviewType.Name, Me.lblInterviewType.Text)
			Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblResult.Text = Language._Object.getCaption(Me.lblResult.Name, Me.lblResult.Text)
			Me.lblLocation.Text = Language._Object.getCaption(Me.lblLocation.Name, Me.lblLocation.Text)
			Me.lblBatchName.Text = Language._Object.getCaption(Me.lblBatchName.Name, Me.lblBatchName.Text)
			Me.lblInterviewer.Text = Language._Object.getCaption(Me.lblInterviewer.Name, Me.lblInterviewer.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
