﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBatchScheduleReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblLocation = New System.Windows.Forms.Label
        Me.lblBatchName = New System.Windows.Forms.Label
        Me.lblInterviewer = New System.Windows.Forms.Label
        Me.txtLocation = New System.Windows.Forms.TextBox
        Me.txtBatchName = New System.Windows.Forms.TextBox
        Me.txtInterviewerName = New System.Windows.Forms.TextBox
        Me.lblResult = New System.Windows.Forms.Label
        Me.cboResultGroup = New System.Windows.Forms.ComboBox
        Me.lblInterviewType = New System.Windows.Forms.Label
        Me.lblVacancy = New System.Windows.Forms.Label
        Me.cboInterviewType = New System.Windows.Forms.ComboBox
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 502)
        Me.NavPanel.Size = New System.Drawing.Size(765, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblLocation)
        Me.gbFilterCriteria.Controls.Add(Me.lblBatchName)
        Me.gbFilterCriteria.Controls.Add(Me.lblInterviewer)
        Me.gbFilterCriteria.Controls.Add(Me.txtLocation)
        Me.gbFilterCriteria.Controls.Add(Me.txtBatchName)
        Me.gbFilterCriteria.Controls.Add(Me.txtInterviewerName)
        Me.gbFilterCriteria.Controls.Add(Me.lblResult)
        Me.gbFilterCriteria.Controls.Add(Me.cboResultGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblInterviewType)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.cboInterviewType)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancy)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(443, 118)
        Me.gbFilterCriteria.TabIndex = 3
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLocation
        '
        Me.lblLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocation.Location = New System.Drawing.Point(232, 90)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(69, 16)
        Me.lblLocation.TabIndex = 103
        Me.lblLocation.Text = "Location"
        Me.lblLocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBatchName
        '
        Me.lblBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchName.Location = New System.Drawing.Point(232, 62)
        Me.lblBatchName.Name = "lblBatchName"
        Me.lblBatchName.Size = New System.Drawing.Size(69, 16)
        Me.lblBatchName.TabIndex = 102
        Me.lblBatchName.Text = "Batch Name"
        Me.lblBatchName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInterviewer
        '
        Me.lblInterviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewer.Location = New System.Drawing.Point(8, 91)
        Me.lblInterviewer.Name = "lblInterviewer"
        Me.lblInterviewer.Size = New System.Drawing.Size(83, 15)
        Me.lblInterviewer.TabIndex = 101
        Me.lblInterviewer.Text = "Interviewer"
        Me.lblInterviewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLocation
        '
        Me.txtLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocation.Location = New System.Drawing.Point(306, 87)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(129, 21)
        Me.txtLocation.TabIndex = 100
        '
        'txtBatchName
        '
        Me.txtBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchName.Location = New System.Drawing.Point(306, 60)
        Me.txtBatchName.Name = "txtBatchName"
        Me.txtBatchName.Size = New System.Drawing.Size(129, 21)
        Me.txtBatchName.TabIndex = 99
        '
        'txtInterviewerName
        '
        Me.txtInterviewerName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterviewerName.Location = New System.Drawing.Point(97, 88)
        Me.txtInterviewerName.Name = "txtInterviewerName"
        Me.txtInterviewerName.Size = New System.Drawing.Size(129, 21)
        Me.txtInterviewerName.TabIndex = 98
        '
        'lblResult
        '
        Me.lblResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResult.Location = New System.Drawing.Point(232, 36)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(69, 15)
        Me.lblResult.TabIndex = 97
        Me.lblResult.Text = "Result Grp."
        Me.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResultGroup
        '
        Me.cboResultGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultGroup.DropDownWidth = 360
        Me.cboResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultGroup.FormattingEnabled = True
        Me.cboResultGroup.Location = New System.Drawing.Point(307, 33)
        Me.cboResultGroup.Name = "cboResultGroup"
        Me.cboResultGroup.Size = New System.Drawing.Size(128, 21)
        Me.cboResultGroup.TabIndex = 96
        '
        'lblInterviewType
        '
        Me.lblInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewType.Location = New System.Drawing.Point(8, 63)
        Me.lblInterviewType.Name = "lblInterviewType"
        Me.lblInterviewType.Size = New System.Drawing.Size(83, 15)
        Me.lblInterviewType.TabIndex = 94
        Me.lblInterviewType.Text = "Interview Type"
        Me.lblInterviewType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVacancy
        '
        Me.lblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancy.Location = New System.Drawing.Point(8, 36)
        Me.lblVacancy.Name = "lblVacancy"
        Me.lblVacancy.Size = New System.Drawing.Size(83, 15)
        Me.lblVacancy.TabIndex = 93
        Me.lblVacancy.Text = "Vacancy"
        Me.lblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInterviewType
        '
        Me.cboInterviewType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterviewType.DropDownWidth = 360
        Me.cboInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterviewType.FormattingEnabled = True
        Me.cboInterviewType.Location = New System.Drawing.Point(97, 60)
        Me.cboInterviewType.Name = "cboInterviewType"
        Me.cboInterviewType.Size = New System.Drawing.Size(129, 21)
        Me.cboInterviewType.TabIndex = 92
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 360
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(97, 33)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(129, 21)
        Me.cboVacancy.TabIndex = 91
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 190)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(443, 63)
        Me.gbSortBy.TabIndex = 20
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(414, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(83, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(97, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(311, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'frmBatchScheduleReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(765, 557)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmBatchScheduleReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblInterviewType As System.Windows.Forms.Label
    Friend WithEvents lblVacancy As System.Windows.Forms.Label
    Friend WithEvents cboInterviewType As System.Windows.Forms.ComboBox
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents cboResultGroup As System.Windows.Forms.ComboBox
    Friend WithEvents txtBatchName As System.Windows.Forms.TextBox
    Friend WithEvents txtInterviewerName As System.Windows.Forms.TextBox
    Friend WithEvents txtLocation As System.Windows.Forms.TextBox
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents lblBatchName As System.Windows.Forms.Label
    Friend WithEvents lblInterviewer As System.Windows.Forms.Label
End Class
