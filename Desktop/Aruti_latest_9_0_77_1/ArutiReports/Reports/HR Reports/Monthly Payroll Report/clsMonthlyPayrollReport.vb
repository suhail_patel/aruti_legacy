'************************************************************************************************************************************
'Class Name : clsMonthlyPayrollReport.vb
'Purpose    :
'Date       :28/02/2017
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

Public Class clsMonthlyPayrollReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsMonthlyPayrollReport"
    Private mstrReportId As String = enArutiReport.EmployeeMonthlyPayrollReport '189
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = String.Empty
    Private mintExitReasonId As Integer = 0
    Private mstrExitReasonName As String = String.Empty
    Private mdicCheckedLeaveTypeIds As Dictionary(Of Integer, String) = Nothing
    Private mdicCheckedTransHeadIds As Dictionary(Of Integer, String) = Nothing
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True
    Private mblnSkipAbsentFromTnA As Boolean = False
    Private mblnIncludeLeavePendingApprovedForms As Boolean = False
    Private mblnIncludeSystemRetirementMonthlyPayrollReport As Boolean = False
    Private mintCategorizationReasonId As Integer = 0
    Private mstrCategorizationReasonName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _ExitReasonId() As Integer
        Set(ByVal value As Integer)
            mintExitReasonId = value
        End Set
    End Property

    Public WriteOnly Property _ExitReasonName() As String
        Set(ByVal value As String)
            mstrExitReasonName = value
        End Set
    End Property

    Public WriteOnly Property _CheckedLeaveTypeIds() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicCheckedLeaveTypeIds = value
        End Set
    End Property

    Public WriteOnly Property _CheckedTransHeadIds() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicCheckedTransHeadIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    Public WriteOnly Property _SkipAbsentFromTnA() As Boolean
        Set(ByVal value As Boolean)
            mblnSkipAbsentFromTnA = value
        End Set
    End Property

    Public WriteOnly Property _IncludeLeavePendingApprovedForms() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeLeavePendingApprovedForms = value
        End Set
    End Property

    Public WriteOnly Property _IncludeSystemRetirementMonthlyPayrollReport() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeSystemRetirementMonthlyPayrollReport = value
        End Set
    End Property

    Public WriteOnly Property _CategorizationReasonId() As Integer
        Set(ByVal value As Integer)
            mintCategorizationReasonId = value
        End Set
    End Property

    Public WriteOnly Property _CategorizationReasonName() As String
        Set(ByVal value As String)
            mstrCategorizationReasonName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = String.Empty
            mintMembershipId = 0
            mstrMembershipName = String.Empty
            mintExitReasonId = 0
            mstrExitReasonName = String.Empty
            mdicCheckedLeaveTypeIds = Nothing
            mdicCheckedTransHeadIds = Nothing
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = String.Empty
            mblnIncludeAccessFilterQry = True
            mblnSkipAbsentFromTnA = False
            mblnIncludeLeavePendingApprovedForms = False
            mblnIncludeSystemRetirementMonthlyPayrollReport = False
            mintCategorizationReasonId = 0
            mstrCategorizationReasonName = String.Empty
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@sdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDate))
            objDataOperation.AddParameter("@edate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDate))

            'If mintPeriodId > 0 Then
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 100, "Period :") & " " & mstrPeriodName & " "
            'End If

            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 101, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintReportTypeId = 1 Then 'NEW STAFF
                If mintMembershipId > 0 Then
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 102, "Membership :") & " " & mstrMembershipName & " "
                End If
            ElseIf mintReportTypeId = 2 Then    'Exit Staff
                If mintExitReasonId > 0 Then
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 103, "Exit Reason :") & " " & mstrExitReasonName & " "
                End If
            ElseIf mintReportTypeId = 4 Then    'Recategorize
                If mintCategorizationReasonId > 0 Then
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 415, "Categorization Reason :") & " " & mstrCategorizationReasonName & " "
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Function New_Staff_Report(ByVal strDatabaseName As String, _
                                      ByVal intUserUnkid As Integer, _
                                      ByVal intYearUnkid As Integer, _
                                      ByVal intCompanyUnkid As Integer, _
                                      ByVal strUserModeSetting As String, _
                                      ByVal blnOnlyApproved As Boolean, _
                                      ByVal srtAmountFormat As String, _
                                      ByRef StrGroupColName As String) As DataTable
        Dim StrQ As String = String.Empty
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtStartDate, mdtEndDate, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEndDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEndDate, strDatabaseName)

            '///////////// GENERATING DATATABLE //////////////////// --- START
            dtTable = New DataTable("NS")
            Dim dCol As DataColumn = Nothing

            With dtTable
                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col1"
                    .DataType = GetType(System.Int32)
                    .Caption = Language.getMessage(mstrModuleName, 200, "S/N")
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col2"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 201, "Full Name")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col3"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 202, "Job Title")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col4"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 203, "Program/Department")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col5"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 204, "Date Of Joining")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col6"
                    .DataType = GetType(System.Decimal)
                    .Caption = Language.getMessage(mstrModuleName, 205, "Basic Salary")
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col7"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 206, "Type Of Membership")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col8"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 207, "Contract Expiry Date")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col9"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 208, "Employment Type")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            dCol = New DataColumn
                            With dCol
                                .ColumnName = "Head_" & iKey.ToString
                                .DataType = GetType(System.Decimal)
                                .Caption = mdicCheckedTransHeadIds(iKey)
                                .DefaultValue = 0
                            End With
                            .Columns.Add(dCol)
                        Next
                    End If
                End If

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col10"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 209, "Comment")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col11"
                    .DataType = GetType(System.String)
                    .Caption = ""
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)
            End With
            '///////////// GENERATING DATATABLE //////////////////// --- END

            StrQ = "SELECT " & _
                   "     ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS fullname " & _
                   "    ,ISNULL(jm.job_name,'') AS designation " & _
                   "    ,ISNULL(dp.name,'') AS department " & _
                   "    ,CONVERT(NVARCHAR(8),hremployee_master.appointeddate,112) AS doj " & _
                   "    ,ISNULL(S.newscale,0) AS basicsalary " & _
                   "    ,ISNULL(mt.mtype,'') AS membershiptype " & _
                   "    ,ISNULL(E.eoc,'') AS contract_expiry " & _
                   "    ,'' AS comments " & _
                   "    ,ISNULL(ety.name,'') AS employment_type " & _
                   "    ,@nh AS staff_type " & _
                   "    ,1 AS staff_type_id "
            If mdicCheckedTransHeadIds IsNot Nothing Then
                If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                        StrQ &= "   ,ISNULL(A" & iKey.ToString & ".amount,0) AS Head_" & iKey.ToString & " "
                    Next
                End If
            End If
            StrQ &= "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "LEFT JOIN cfcommon_master AS ety ON ety.masterunkid = hremployee_master.employmenttypeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         CONVERT(NVARCHAR(8),hremployee_dates_tran.date1,112) AS eoc " & _
                    "        ,hremployee_dates_tran.employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
                    "    FROM hremployee_dates_tran " & _
                    "    WHERE hremployee_dates_tran.isvoid = 0 AND hremployee_dates_tran.datetypeunkid = 4 " & _
                    "    AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @edate " & _
                    ") AS E ON E.employeeunkid = hremployee_master.employeeunkid AND E.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         hremployee_categorization_tran.employeeunkid " & _
                    "        ,hremployee_categorization_tran.jobunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE hremployee_categorization_tran.isvoid = 0 " & _
                    "    AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= @edate " & _
                    ") AS R ON R.employeeunkid = hremployee_master.employeeunkid AND R.rno = 1 " & _
                    "LEFT JOIN hrjob_master as jm ON jm.jobunkid = R.jobunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         hremployee_transfer_tran.employeeunkid " & _
                    "        ,hremployee_transfer_tran.departmentunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE hremployee_transfer_tran.isvoid = 0 " & _
                    "    AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= @edate " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.rno = 1 " & _
                    "LEFT JOIN hrdepartment_master as dp ON dp.departmentunkid = T.departmentunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         prsalaryincrement_tran.employeeunkid " & _
                    "        ,prsalaryincrement_tran.newscale " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC, prsalaryincrement_tran.salaryincrementtranunkid DESC) AS rno " & _
                    "    FROM prsalaryincrement_tran " & _
                    "    WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                    "    AND CONVERT(NVARCHAR(8),prsalaryincrement_tran.incrementdate,112) <= @edate " & _
                    ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         hremployee_meminfo_tran.employeeunkid " & _
                    "        ,(SELECT ISNULL(STUFF((SELECT ' | ' + hrmembership_master.membershipname + ' - ' + et.membershipno " & _
                    "                               FROM hremployee_meminfo_tran AS et " & _
                    "                                    JOIN hrmembership_master ON et.membershipunkid = hrmembership_master.membershipunkid " & _
                    "                               WHERE et.isactive = 1 AND et.isdeleted = 0 AND et.employeeunkid = hremployee_meminfo_tran.employeeunkid "
            If mintMembershipId > 0 Then
                StrQ &= " AND et.membershipunkid = @membershipunkid "
            End If

            StrQ &= "    ORDER BY et.employeeunkid FOR XML PATH('')),1,2,''),'')) AS mtype " & _
                    "    FROM hremployee_meminfo_tran " & _
                    "WHERE hremployee_meminfo_tran.isactive = 1 AND hremployee_meminfo_tran.isdeleted = 0 "

            If mintMembershipId > 0 Then
                StrQ &= " AND hremployee_meminfo_tran.membershipunkid = @membershipunkid "
            End If

            StrQ &= "GROUP BY hremployee_meminfo_tran.employeeunkid " & _
                    ") AS mt ON mt.employeeunkid = hremployee_master.employeeunkid "

            If mdicCheckedTransHeadIds IsNot Nothing Then
                If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                        StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "   SELECT " & _
                                "        prpayrollprocess_tran.employeeunkid " & _
                                "       ,prpayrollprocess_tran.tranheadunkid " & _
                                "       ,prpayrollprocess_tran.amount " & _
                                "   FROM prpayrollprocess_tran " & _
                                "       JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "   WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                                "       AND prpayrollprocess_tran.tranheadunkid = '" & iKey.ToString & "' AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                                ") AS A" & iKey.ToString & " ON A" & iKey.ToString & ".employeeunkid = hremployee_master.employeeunkid "
                    Next
                End If
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= " WHERE hremployee_master.employeeunkid NOT IN " & _
                    "( " & _
                         "SELECT A.employeeunkid " & _
                         "FROM " & _
                         "( " & _
                              "SELECT " & _
                                    "hremployee_rehire_tran.employeeunkid " & _
                                   ",hremployee_rehire_tran.reinstatment_date " & _
                                   ",hremployee_rehire_tran.effectivedate " & _
                                   ",ROW_NUMBER()OVER(PARTITION BY hremployee_rehire_tran.employeeunkid ORDER BY hremployee_rehire_tran.effectivedate DESC) AS rno " & _
                              "FROM hremployee_rehire_tran " & _
                              "WHERE hremployee_rehire_tran.isvoid = 0 " & _
                              "AND CONVERT(NVARCHAR(8),hremployee_rehire_tran.effectivedate,112) <= @edate " & _
                         ") AS A WHERE A.rno = 1 AND CONVERT(NVARCHAR(8),A.reinstatment_date,112) BETWEEN @sdate AND @edate " & _
                    ") AND CONVERT(NVARCHAR(8),hremployee_master.appointeddate,112) BETWEEN @sdate AND @edate "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            StrQ &= " UNION "

            StrQ &= "SELECT " & _
                    "     ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS fullname " & _
                    "    ,ISNULL(jm.job_name,'') AS designation " & _
                    "    ,ISNULL(dp.name,'') AS department " & _
                    "    ,CONVERT(NVARCHAR(8),hremployee_master.appointeddate,112) AS doj " & _
                    "    ,ISNULL(S.newscale,0) AS basicsalary " & _
                    "    ,ISNULL(mt.mtype,'') AS membershiptype " & _
                    "    ,ISNULL(E.eoc,'') AS contract_expiry " & _
                    "    ,'' AS comments " & _
                    "    ,ISNULL(ety.name,'') AS employment_type " & _
                    "    ,@rh AS staff_type " & _
                    "    ,2 AS staff_type_id "
            If mdicCheckedTransHeadIds IsNot Nothing Then
                If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                        StrQ &= "   ,ISNULL(A" & iKey.ToString & ".amount,0) AS Head_" & iKey.ToString & " "
                    Next
                End If
            End If
            StrQ &= "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "JOIN " & _
                    "( " & _
                    "   SELECT A.employeeunkid,A.reinstatment_date " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            hremployee_rehire_tran.employeeunkid " & _
                    "           ,hremployee_rehire_tran.reinstatment_date " & _
                    "           ,hremployee_rehire_tran.effectivedate " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_rehire_tran.employeeunkid ORDER BY hremployee_rehire_tran.effectivedate DESC) AS rno " & _
                    "       FROM hremployee_rehire_tran " & _
                    "       WHERE hremployee_rehire_tran.isvoid = 0 " & _
                    "       AND CONVERT(NVARCHAR(8),hremployee_rehire_tran.effectivedate,112) <= @edate " & _
                    "   ) AS A WHERE A.rno = 1 AND CONVERT(NVARCHAR(8),A.reinstatment_date,112) BETWEEN @sdate AND @edate " & _
                    ") AS rht ON rht.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN cfcommon_master AS ety ON ety.masterunkid = hremployee_master.employmenttypeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         CONVERT(NVARCHAR(8),hremployee_dates_tran.date1,112) AS eoc " & _
                    "        ,hremployee_dates_tran.employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
                    "    FROM hremployee_dates_tran " & _
                    "    WHERE hremployee_dates_tran.isvoid = 0 AND hremployee_dates_tran.datetypeunkid = 4 " & _
                    "    AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @edate " & _
                    ") AS E ON E.employeeunkid = hremployee_master.employeeunkid AND E.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         hremployee_categorization_tran.employeeunkid " & _
                    "        ,hremployee_categorization_tran.jobunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE hremployee_categorization_tran.isvoid = 0 " & _
                    "    AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= @edate " & _
                    ") AS R ON R.employeeunkid = hremployee_master.employeeunkid AND R.rno = 1 " & _
                    "LEFT JOIN hrjob_master as jm ON jm.jobunkid = R.jobunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         hremployee_transfer_tran.employeeunkid " & _
                    "        ,hremployee_transfer_tran.departmentunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE hremployee_transfer_tran.isvoid = 0 " & _
                    "    AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= @edate " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.rno = 1 " & _
                    "LEFT JOIN hrdepartment_master as dp ON dp.departmentunkid = T.departmentunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         prsalaryincrement_tran.employeeunkid " & _
                    "        ,prsalaryincrement_tran.newscale " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC, prsalaryincrement_tran.salaryincrementtranunkid DESC) AS rno " & _
                    "    FROM prsalaryincrement_tran " & _
                    "    WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                    "    AND CONVERT(NVARCHAR(8),prsalaryincrement_tran.incrementdate,112) <= @edate " & _
                    ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         hremployee_meminfo_tran.employeeunkid " & _
                    "        ,(SELECT ISNULL(STUFF((SELECT ' | ' + hrmembership_master.membershipname + ' - ' + et.membershipno " & _
                    "                               FROM hremployee_meminfo_tran AS et " & _
                    "                                    JOIN hrmembership_master ON et.membershipunkid = hrmembership_master.membershipunkid " & _
                    "                               WHERE et.isactive = 1 AND et.isdeleted = 0 AND et.employeeunkid = hremployee_meminfo_tran.employeeunkid "
            If mintMembershipId > 0 Then
                StrQ &= " AND et.membershipunkid = @membershipunkid "
            End If

            StrQ &= "    ORDER BY et.employeeunkid FOR XML PATH('')),1,2,''),'')) AS mtype " & _
                    "    FROM hremployee_meminfo_tran " & _
                    "WHERE hremployee_meminfo_tran.isactive = 1 AND hremployee_meminfo_tran.isdeleted = 0 "

            If mintMembershipId > 0 Then
                StrQ &= " AND hremployee_meminfo_tran.membershipunkid = @membershipunkid "
            End If

            StrQ &= "GROUP BY hremployee_meminfo_tran.employeeunkid " & _
                    ") AS mt ON mt.employeeunkid = hremployee_master.employeeunkid "

            If mdicCheckedTransHeadIds IsNot Nothing Then
                If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                        StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "   SELECT " & _
                                "        prpayrollprocess_tran.employeeunkid " & _
                                "       ,prpayrollprocess_tran.tranheadunkid " & _
                                "       ,prpayrollprocess_tran.amount " & _
                                "   FROM prpayrollprocess_tran " & _
                                "       JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "   WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                                "       AND prpayrollprocess_tran.tranheadunkid = '" & iKey.ToString & "' AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                                ") AS A" & iKey.ToString & " ON A" & iKey.ToString & ".employeeunkid = hremployee_master.employeeunkid "
                    Next
                End If
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= " WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)

            objDataOperation.AddParameter("@nh", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 210, "New Staff"))
            objDataOperation.AddParameter("@rh", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 211, "Re-hired Staff"))

            Call FilterTitleAndFilterQuery()

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dview As DataView = dsList.Tables(0).DefaultView
            dview.Sort = "staff_type_id"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dview.ToTable.Copy)

            Dim intSrn As Integer = 0 : Dim intGrpId As Integer = 0
            For Each row As DataRow In dsList.Tables(0).Rows
                Dim drow As DataRow = dtTable.NewRow
                If intGrpId <> row.Item("staff_type_id") Then
                    intSrn = 1
                    intGrpId = row.Item("staff_type_id")
                End If

                drow.Item("Col1") = intSrn.ToString
                drow.Item("Col2") = row.Item("fullname")
                drow.Item("Col3") = row.Item("designation")
                drow.Item("Col4") = row.Item("department")
                drow.Item("Col5") = eZeeDate.convertDate(row.Item("doj").ToString).ToShortDateString
                drow.Item("Col6") = Format(CDec(row.Item("basicsalary")), srtAmountFormat)
                drow.Item("Col7") = row.Item("membershiptype")
                If row.Item("contract_expiry").ToString.Trim.Length > 0 Then
                    drow.Item("Col8") = eZeeDate.convertDate(row.Item("contract_expiry").ToString).ToShortDateString
                Else
                    drow.Item("Col8") = ""
                End If
                drow.Item("Col9") = row.Item("employment_type")
                drow.Item("Col10") = row.Item("comments")
                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            drow.Item("Head_" & iKey.ToString) = row.Item("Head_" & iKey.ToString)
                        Next
                    End If
                End If
                drow.Item("Col11") = row.Item("staff_type")
                dtTable.Rows.Add(drow)
                intSrn = intSrn + 1
            Next

            StrGroupColName = "Col11"

            Return dtTable
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New_Staff_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function Exit_Staff_Report(ByVal strDatabaseName As String, _
                                       ByVal intUserUnkid As Integer, _
                                       ByVal intYearUnkid As Integer, _
                                       ByVal intCompanyUnkid As Integer, _
                                       ByVal strUserModeSetting As String, _
                                       ByVal blnOnlyApproved As Boolean, _
                                       ByRef StrGroupColName As String) As DataTable
        Dim StrQ As String = String.Empty
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Try
            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEndDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEndDate, strDatabaseName)

            '///////////// GENERATING DATATABLE //////////////////// --- START
            dtTable = New DataTable("NS")
            Dim dCol As DataColumn = Nothing

            With dtTable
                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col1"
                    .DataType = GetType(System.Int32)
                    .Caption = Language.getMessage(mstrModuleName, 200, "S/N")
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col2"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 201, "Full Name")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col3"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 202, "Job Title")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col4"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 203, "Program/Department")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col5"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 406, "Last Working Date")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col6"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 407, "Last Month In Payroll")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col7"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 408, "Leaving Date")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col8"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 409, "EOC Date")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col9"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 410, "Retirement Date")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            dCol = New DataColumn
                            With dCol
                                .ColumnName = "Head_" & iKey.ToString
                                .DataType = GetType(System.Decimal)
                                .Caption = mdicCheckedTransHeadIds(iKey)
                                .DefaultValue = 0
                            End With
                            .Columns.Add(dCol)
                        Next
                    End If
                End If

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col10"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 209, "Comment")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col11"   'SUB GROUP
                    .DataType = GetType(System.String)
                    .Caption = ""
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col12"   'Main Group
                    .DataType = GetType(System.String)
                    .Caption = ""
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)
            End With
            '///////////// GENERATING DATATABLE //////////////////// --- END

            StrQ = "SELECT " & _
                   "     ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS fullname " & _
                   "    ,ISNULL(jm.job_name,'') AS designation " & _
                   "    ,ISNULL(dp.name,'') AS department " & _
                   "    ,tefdate " & _
                   "    ,CONVERT(NVARCHAR(8),TE.termination_from_date,112) AS lastmonthinpayroll " & _
                   "    ,ISNULL(TER.name,@NA) as reason " & _
                   "    ,'' AS leavingdate " & _
                   "    ,CONVERT(NVARCHAR(8),TE.emplend_date,112) AS eocdate " & _
                   "    ,CONVERT(NVARCHAR(8),RE.retirement_date,112) as retire " & _
                   "    ,TE.isexclude_payroll " & _
                   "    ,'' AS comments " & _
                   "    ,1 as sortid " & _
                   "    ,@nh as modes "
            If mdicCheckedTransHeadIds IsNot Nothing Then
                If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                        StrQ &= "   ,ISNULL(A" & iKey.ToString & ".amount,0) AS Head_" & iKey.ToString & " "
                    Next
                End If
            End If
            StrQ &= "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "LEFT JOIN cfcommon_master AS ety ON ety.masterunkid = hremployee_master.employmenttypeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         CONVERT(NVARCHAR(8),hremployee_dates_tran.date1,112) AS eoc " & _
                   "        ,hremployee_dates_tran.employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
                   "    FROM hremployee_dates_tran " & _
                   "    WHERE hremployee_dates_tran.isvoid = 0 AND hremployee_dates_tran.datetypeunkid = 4 " & _
                   "    AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @edate " & _
                   ") AS E ON E.employeeunkid = hremployee_master.employeeunkid AND E.rno = 1 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hremployee_categorization_tran.employeeunkid " & _
                   "        ,hremployee_categorization_tran.jobunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                   "    FROM hremployee_categorization_tran " & _
                   "    WHERE hremployee_categorization_tran.isvoid = 0 " & _
                   "    AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= @edate " & _
                   ") AS R ON R.employeeunkid = hremployee_master.employeeunkid AND R.rno = 1 " & _
                   "LEFT JOIN hrjob_master as jm ON jm.jobunkid = R.jobunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hremployee_transfer_tran.employeeunkid " & _
                   "        ,hremployee_transfer_tran.departmentunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                   "    FROM hremployee_transfer_tran " & _
                   "    WHERE hremployee_transfer_tran.isvoid = 0 " & _
                   "    AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= @edate " & _
                   ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.rno = 1 " & _
                   "LEFT JOIN hrdepartment_master as dp ON dp.departmentunkid = T.departmentunkid " & _
                   "JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         employeeunkid " & _
                   "        ,date1 AS emplend_date " & _
                   "        ,date2 AS termination_from_date " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        ,CONVERT(CHAR(8),effectivedate,112) as tefdate " & _
                   "        ,actionreasonunkid " & _
                   "        ,hremployee_dates_tran.isexclude_payroll " & _
                   "    FROM hremployee_dates_tran " & _
                   "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' " & _
                   "    AND CONVERT(CHAR(8),effectivedate,112) <= @edate " & _
                   ")TE ON TE.employeeunkid = hremployee_master.employeeunkid AND TE.rno = 1 AND TE.termination_from_date IS NOT NULL " & _
                   "AND TE.emplend_date IS NOT NULL " & _
                   "LEFT JOIN cfcommon_master AS TER ON TER.masterunkid = TE.actionreasonunkid AND TER.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         employeeunkid " & _
                   "        ,date1 AS retirement_date " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        ,actionreasonunkid " & _
                   "    FROM hremployee_dates_tran " & _
                   "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' " & _
                   "    AND CONVERT(CHAR(8),effectivedate,112) <= @edate " & _
                   ")RE ON RE.employeeunkid = hremployee_master.employeeunkid AND RE.rno = 1 AND RE.retirement_date IS NOT NULL "

            If mdicCheckedTransHeadIds IsNot Nothing Then
                If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                        StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "   SELECT " & _
                                "        prpayrollprocess_tran.employeeunkid " & _
                                "       ,prpayrollprocess_tran.tranheadunkid " & _
                                "       ,prpayrollprocess_tran.amount " & _
                                "   FROM prpayrollprocess_tran " & _
                                "       JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "   WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                                "       AND prpayrollprocess_tran.tranheadunkid = '" & iKey.ToString & "' AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                                ") AS A" & iKey.ToString & " ON A" & iKey.ToString & ".employeeunkid = hremployee_master.employeeunkid "
                    Next
                End If
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= " WHERE 1 = 1 " & _
                    "   AND ((@sdate BETWEEN CONVERT(CHAR(8), TE.emplend_date, 112) AND CONVERT(CHAR(8), TE.termination_from_date, 112) " & _
                    "   OR @edate BETWEEN CONVERT(CHAR(8), TE.emplend_date, 112) AND CONVERT(CHAR(8), TE.termination_from_date, 112)) " & _
                    "   OR (CONVERT(CHAR(8), TE.emplend_date, 112) BETWEEN @sdate AND @edate " & _
                    "   AND CONVERT(CHAR(8), TE.termination_from_date, 112) " & _
                    "   BETWEEN @sdate AND @edate)) "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If mintExitReasonId > 0 Then
                StrQ &= " AND TER.masterunkid = @masterunkid "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If mblnIncludeSystemRetirementMonthlyPayrollReport Then

                StrQ &= " UNION " & _
                        "    SELECT " & _
                        "        ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS fullname " & _
                        "       ,ISNULL(jm.job_name,'') AS designation " & _
                        "       ,ISNULL(dp.name,'') AS department " & _
                        "       ,tefdate " & _
                        "       ,ISNULL(CONVERT(NVARCHAR(8),RE.retirement_date,112),'') AS lastmonthinpayroll " & _
                        "       ,ISNULL(TRE.name,'N/A') as reason " & _
                        "       ,'' AS leavingdate " & _
                        "       ,ISNULL(CONVERT(NVARCHAR(8),TE.emplend_date,112),'') AS eocdate " & _
                        "       ,ISNULL(CONVERT(NVARCHAR(8),RE.retirement_date,112),'') as retire " & _
                        "       ,ISNULL(TE.isexclude_payroll,0) AS isexclude_payroll " & _
                        "       ,'' AS comments " & _
                        "       ,2 as sortid " & _
                        "       ,@rh as modes "
                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            StrQ &= "   ,ISNULL(A" & iKey.ToString & ".amount,0) AS Head_" & iKey.ToString & " "
                        Next
                    End If
                End If
                StrQ &= "    FROM hremployee_master "
                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'S.SANDEEP |28-OCT-2021| -- END
                StrQ &= "       LEFT JOIN cfcommon_master AS ety ON ety.masterunkid = hremployee_master.employmenttypeunkid " & _
                        "       LEFT JOIN " & _
                        "       ( " & _
                        "           SELECT " & _
                        "                CONVERT(NVARCHAR(8),hremployee_dates_tran.date1,112) AS eoc " & _
                        "               ,hremployee_dates_tran.employeeunkid " & _
                        "               ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
                        "           FROM hremployee_dates_tran " & _
                        "           WHERE hremployee_dates_tran.isvoid = 0 AND hremployee_dates_tran.datetypeunkid = 4 " & _
                        "           AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @edate " & _
                        "       ) AS E ON E.employeeunkid = hremployee_master.employeeunkid AND E.rno = 1 " & _
                        "       LEFT JOIN " & _
                        "       ( " & _
                        "           SELECT " & _
                        "                hremployee_categorization_tran.employeeunkid " & _
                        "               ,hremployee_categorization_tran.jobunkid " & _
                        "               ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                        "           FROM hremployee_categorization_tran " & _
                        "           WHERE hremployee_categorization_tran.isvoid = 0 " & _
                        "           AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= @edate " & _
                        "       ) AS R ON R.employeeunkid = hremployee_master.employeeunkid AND R.rno = 1 " & _
                        "       LEFT JOIN hrjob_master as jm ON jm.jobunkid = R.jobunkid " & _
                        "       LEFT JOIN " & _
                        "       ( " & _
                        "           SELECT " & _
                        "                hremployee_transfer_tran.employeeunkid " & _
                        "               ,hremployee_transfer_tran.departmentunkid " & _
                        "               ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                        "           FROM hremployee_transfer_tran " & _
                        "           WHERE hremployee_transfer_tran.isvoid = 0 " & _
                        "           AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= @edate " & _
                        "       ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.rno = 1 " & _
                        "       LEFT JOIN hrdepartment_master as dp ON dp.departmentunkid = T.departmentunkid " & _
                        "       LEFT JOIN " & _
                        "       ( " & _
                        "           SELECT " & _
                        "                employeeunkid " & _
                        "               ,date1 AS emplend_date " & _
                        "               ,date2 AS termination_from_date " & _
                        "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "               ,actionreasonunkid " & _
                        "               ,hremployee_dates_tran.isexclude_payroll " & _
                        "           FROM hremployee_dates_tran " & _
                        "           WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' " & _
                        "           AND CONVERT(CHAR(8),effectivedate,112) <= @edate " & _
                        "       )TE ON TE.employeeunkid = hremployee_master.employeeunkid AND TE.rno = 1 AND TE.termination_from_date IS NOT NULL AND TE.emplend_date IS NOT NULL " & _
                        "       JOIN " & _
                        "       ( " & _
                        "           SELECT " & _
                        "                employeeunkid " & _
                        "               ,date1 AS retirement_date " & _
                        "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "               ,actionreasonunkid " & _
                        "               ,CONVERT(CHAR(8),effectivedate,112) as tefdate " & _
                        "           FROM hremployee_dates_tran " & _
                        "           WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' " & _
                        "           AND CONVERT(CHAR(8),effectivedate,112) <= @edate " & _
                        "       )RE ON RE.employeeunkid = hremployee_master.employeeunkid AND RE.rno = 1 AND RE.retirement_date IS NOT NULL " & _
                        "       LEFT JOIN cfcommon_master AS TRE ON TRE.masterunkid = TE.actionreasonunkid AND TRE.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' "

                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            StrQ &= "    LEFT JOIN " & _
                                    "    ( " & _
                                    "       SELECT " & _
                                    "            prpayrollprocess_tran.employeeunkid " & _
                                    "           ,prpayrollprocess_tran.tranheadunkid " & _
                                    "           ,prpayrollprocess_tran.amount " & _
                                    "       FROM prpayrollprocess_tran " & _
                                    "           JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                    "       WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                                    "           AND prpayrollprocess_tran.tranheadunkid = '" & iKey.ToString & "' AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                                    "    ) AS A" & iKey.ToString & " ON A" & iKey.ToString & ".employeeunkid = hremployee_master.employeeunkid "
                        Next
                    End If
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'S.SANDEEP |28-OCT-2021| -- END
                

                StrQ &= " WHERE TE.emplend_date IS NULL AND TE.termination_from_date IS NULL AND CONVERT(NVARCHAR(8),RE.retirement_date,112) BETWEEN @sdate AND @edate "


                If mintEmployeeId > 0 Then
                    StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                End If

                If mintExitReasonId > 0 Then
                    StrQ &= " AND TRE.masterunkid = @masterunkid "
                End If

                If mstrAdvanceFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvanceFilter
                End If

            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExitReasonId)

            objDataOperation.AddParameter("@nh", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 411, "Termination"))
            objDataOperation.AddParameter("@rh", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 412, "Retirement By System"))
            objDataOperation.AddParameter("@NA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 413, "N/A"))

            Call FilterTitleAndFilterQuery()

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dview As DataView = dsList.Tables(0).DefaultView
            dview.Sort = "sortid,reason"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dview.ToTable.Copy)

            Dim intSrn As Integer = 0 : Dim strGrp As String = "" : Dim strPGrp As String = ""
            For Each row As DataRow In dsList.Tables(0).Rows
                Dim drow As DataRow = dtTable.NewRow
                If strPGrp <> row.Item("modes") Then
                    strGrp = ""
                    strPGrp = row.Item("modes")
                End If
                If strGrp <> row.Item("reason") Then
                    intSrn = 1
                    strGrp = row.Item("reason")
                End If
                drow.Item("Col1") = intSrn.ToString
                drow.Item("Col2") = row.Item("fullname")
                drow.Item("Col3") = row.Item("designation")
                drow.Item("Col4") = row.Item("department")
                If row.Item("tefdate").ToString.Trim.Length > 0 Then
                    drow.Item("Col5") = eZeeDate.convertDate(row.Item("tefdate").ToString).ToShortDateString
                Else
                    drow.Item("Col5") = ""
                End If
                If row.Item("lastmonthinpayroll").ToString.Trim.Length > 0 Then
                    drow.Item("Col6") = eZeeDate.convertDate(row.Item("lastmonthinpayroll").ToString).ToShortDateString
                Else
                    drow.Item("Col6") = ""
                End If
                drow.Item("Col7") = row.Item("leavingdate")

                If row.Item("eocdate").ToString.Trim.Length > 0 Then
                    drow.Item("Col8") = eZeeDate.convertDate(row.Item("eocdate").ToString).ToShortDateString
                Else
                    drow.Item("Col8") = ""
                End If

                If row.Item("retire").ToString.Trim.Length > 0 Then
                    drow.Item("Col9") = eZeeDate.convertDate(row.Item("retire").ToString).ToShortDateString
                Else
                    drow.Item("Col9") = ""
                End If
                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            drow.Item("Head_" & iKey.ToString) = row.Item("Head_" & iKey.ToString)
                        Next
                    End If
                End If
                drow.Item("Col10") = row.Item("comments")
                drow.Item("Col11") = row.Item("reason")
                drow.Item("Col12") = row.Item("modes")

                dtTable.Rows.Add(drow)
                intSrn = intSrn + 1
            Next

            If mblnIncludeSystemRetirementMonthlyPayrollReport Then
                StrGroupColName = "Col12,Col11"
            Else
                StrGroupColName = "Col11"
                dtTable.Columns.Remove("Col12")
            End If

            dtTable.Columns.Remove("Col7")

            Return dtTable

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Exit_Staff_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function Staff_On_Leave_Report(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal srtAmountFormat As String, _
                                           ByRef StrGroupColName As String) As DataTable

        Dim StrQ As String = String.Empty
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtStartDate, mdtEndDate, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEndDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEndDate, strDatabaseName)

            '///////////// GENERATING DATATABLE //////////////////// --- START
            dtTable = New DataTable("NS")
            Dim dCol As DataColumn = Nothing

            With dtTable
                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col1"
                    .DataType = GetType(System.Int32)
                    .Caption = Language.getMessage(mstrModuleName, 200, "S/N")
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col2"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 201, "Full Name")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col3"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 202, "Job Title")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)


                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col4"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 203, "Program/Department")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col5"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 301, "Start Date")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col6"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 302, "End Date")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col7"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 303, "No Of Days")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col8"
                    .DataType = GetType(System.Decimal)
                    .Caption = Language.getMessage(mstrModuleName, 205, "Basic Salary")
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col9"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 209, "Comment")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            dCol = New DataColumn
                            With dCol
                                .ColumnName = "Head_" & iKey.ToString
                                .DataType = GetType(System.Decimal)
                                .Caption = mdicCheckedTransHeadIds(iKey)
                                .DefaultValue = 0
                            End With
                            .Columns.Add(dCol)
                        Next
                    End If
                End If

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col10"
                    .DataType = GetType(System.String)
                    .Caption = ""
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)
            End With
            '///////////// GENERATING DATATABLE //////////////////// --- END

            If mblnSkipAbsentFromTnA = False Then
                StrQ = "SELECT " & _
                   "     ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS fullname " & _
                   "    ,ISNULL(jm.job_name,'') AS designation " & _
                   "    ,ISNULL(dp.name,'') AS department " & _
                   "    ,@sdate AS stdate " & _
                   "    ,@edate AS eddate " & _
                   "    ,@GrpName AS leavename " & _
                   "    ,up.ndays " & _
                   "    ,ISNULL(S.newscale,0) as bsalary " & _
                   "    ,'' AS comments " & _
                   "    ,up.sortid "
                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            StrQ &= "   ,ISNULL(A" & iKey.ToString & ".amount,0) AS Head_" & iKey.ToString & " "
                        Next
                    End If
                End If
                StrQ &= "FROM hremployee_master "
                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If
                'S.SANDEEP |28-OCT-2021| -- END
                StrQ &= "JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         tnalogin_summary.employeeunkid " & _
                        "        ,COUNT(1) AS ndays " & _
                        "        ,1 AS sortid " & _
                        "    FROM tnalogin_summary " & _
                        "    WHERE tnalogin_summary.isunpaidleave = 1 AND tnalogin_summary.isabsentprocess = 1 " & _
                        "    AND CONVERT(NVARCHAR(8), tnalogin_summary.login_date, 112) BETWEEN @sdate AND @edate " & _
                        "    GROUP BY tnalogin_summary.employeeunkid " & _
                        ") AS up ON up.employeeunkid = hremployee_master.employeeunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         hremployee_categorization_tran.employeeunkid " & _
                        "        ,hremployee_categorization_tran.jobunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                        "    FROM hremployee_categorization_tran " & _
                        "    WHERE hremployee_categorization_tran.isvoid = 0 " & _
                        "        AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= @edate " & _
                        ") AS R ON R.employeeunkid = hremployee_master.employeeunkid AND R.rno = 1 " & _
                        "LEFT JOIN hrjob_master as jm ON jm.jobunkid = R.jobunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         hremployee_transfer_tran.employeeunkid " & _
                        "        ,hremployee_transfer_tran.departmentunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                        "    FROM hremployee_transfer_tran " & _
                        "    WHERE hremployee_transfer_tran.isvoid = 0 " & _
                        "    AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= @edate " & _
                        ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.rno = 1 " & _
                        "LEFT JOIN hrdepartment_master as dp ON dp.departmentunkid = T.departmentunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         prsalaryincrement_tran.employeeunkid " & _
                        "        ,prsalaryincrement_tran.newscale " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC, prsalaryincrement_tran.salaryincrementtranunkid DESC) AS rno " & _
                        "    FROM prsalaryincrement_tran " & _
                        "    WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                        "    AND CONVERT(NVARCHAR(8),prsalaryincrement_tran.incrementdate,112) <= @edate " & _
                        ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.rno = 1 "
                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            StrQ &= "LEFT JOIN " & _
                                    "( " & _
                                    "   SELECT " & _
                                    "        prpayrollprocess_tran.employeeunkid " & _
                                    "       ,prpayrollprocess_tran.tranheadunkid " & _
                                    "       ,prpayrollprocess_tran.amount " & _
                                    "   FROM prpayrollprocess_tran " & _
                                    "       JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                    "   WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                                    "       AND prpayrollprocess_tran.tranheadunkid = '" & iKey.ToString & "' AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                                    ") AS A" & iKey.ToString & " ON A" & iKey.ToString & ".employeeunkid = hremployee_master.employeeunkid "
                        Next
                    End If
                End If

                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry
                'End If

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If


                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP |28-OCT-2021| -- END
                

                StrQ &= " WHERE 1 = 1 "

                If mintEmployeeId > 0 Then
                    StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                End If

                If mstrAdvanceFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvanceFilter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If

                StrQ &= "UNION ALL "

            End If

            StrQ &= "SELECT " & _
                    "    ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS fullname " & _
                    "   ,ISNULL(jm.job_name,'') AS designation " & _
                    "   ,ISNULL(dp.name,'') AS department " & _
                    "   ,lv.stdate " & _
                    "   ,lv.eddate " & _
                    "   ,@sfo +' '+lv.leavename AS leavename " & _
                    "   ,lv.ndays " & _
                    "   ,ISNULL(S.newscale,0) as bsalary " & _
                    "   ,'' AS comments " & _
                    "   ,lv.sortid "
            If mdicCheckedTransHeadIds IsNot Nothing Then
                If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                        StrQ &= "   ,ISNULL(A" & iKey.ToString & ".amount,0) AS Head_" & iKey.ToString & " "
                    Next
                End If
            End If
            StrQ &= "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            lf.employeeunkid " & _
                    "           ,lvleavetype_master.leavename " & _
                    "           ,ISNULL(CONVERT(CHAR(8), ISNULL(lf.approve_stdate, lf.startdate), 112), '') AS stdate " & _
                    "           ,ISNULL(CONVERT(CHAR(8), ISNULL(lf.approve_eddate, lf.returndate), 112), '') AS eddate " & _
                    "           ,lf.statusunkid " & _
                    "           ,CASE WHEN lf.statusunkid = 2 THEN df.dfy " & _
                    "                 WHEN lf.statusunkid = 1 THEN lf.approve_days " & _
                    "                 WHEN lf.statusunkid = 7 THEN lt.idfy " & _
                    "            END as ndays " & _
                    "           ,2 AS sortid " & _
                    "       FROM lvleaveform AS lf " & _
                    "       LEFT JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "                lvleaveday_fraction.formunkid " & _
                    "               ,SUM(lvleaveday_fraction.dayfraction) AS dfy " & _
                    "           FROM lvleaveday_fraction WHERE lvleaveday_fraction.isvoid = 0 " & _
                    "           AND lvleaveday_fraction.approverunkid <= -1 " & _
                    "           GROUP BY lvleaveday_fraction.formunkid " & _
                    "       ) AS df ON df.formunkid = lf.formunkid " & _
                    "       LEFT JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "                lvleaveIssue_master.formunkid " & _
                    "               ,SUM(lvleaveIssue_tran.dayfraction) AS idfy " & _
                    "           FROM lvleaveIssue_master " & _
                    "               JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                    "           WHERE lvleaveIssue_master.isvoid = 0 AND lvleaveIssue_tran.isvoid = 0 " & _
                    "           GROUP BY lvleaveIssue_master.formunkid " & _
                    "       ) AS lt ON lt.formunkid = lf.formunkid " & _
                    "JOIN lvleavetype_master ON lf.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                    "WHERE lf.isvoid = 0 " & _
                    "   AND ((@sdate BETWEEN CONVERT(CHAR(8), ISNULL(lf.approve_stdate, lf.startdate), 112) AND CONVERT(CHAR(8), ISNULL(lf.approve_eddate, lf.returndate), 112) " & _
                    "   OR @edate BETWEEN CONVERT(CHAR(8), ISNULL(lf.approve_stdate, lf.startdate), 112) AND CONVERT(CHAR(8), ISNULL(lf.approve_eddate, lf.returndate), 112)) " & _
                    "   OR (CONVERT(CHAR(8), ISNULL(lf.approve_stdate, lf.startdate), 112) BETWEEN @sdate AND @edate " & _
                    "   AND CONVERT(CHAR(8), ISNULL(lf.approve_eddate, lf.returndate), 112) " & _
                    "   BETWEEN @sdate AND @edate)) "

            If mblnIncludeLeavePendingApprovedForms Then
                StrQ &= "AND lf.statusunkid IN (1,2,7) "
            Else
                StrQ &= "AND lf.statusunkid IN (7) "
            End If

            If mdicCheckedLeaveTypeIds IsNot Nothing AndAlso mdicCheckedLeaveTypeIds.Keys.Count > 0 Then
                StrQ &= " AND lvleavetype_master.leavetypeunkid IN (" & String.Join(",", mdicCheckedLeaveTypeIds.Select(Function(x) x.Key.ToString).ToArray) & ") "
            End If

            StrQ &= ") AS lv ON lv.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        CONVERT(NVARCHAR(8),hremployee_dates_tran.date1,112) AS eoc " & _
                    "       ,hremployee_dates_tran.employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
                    "   FROM hremployee_dates_tran " & _
                    "   WHERE hremployee_dates_tran.isvoid = 0 AND hremployee_dates_tran.datetypeunkid = 4 " & _
                    "       AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @edate " & _
                    ") AS E ON E.employeeunkid = hremployee_master.employeeunkid AND E.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hremployee_categorization_tran.employeeunkid " & _
                    "       ,hremployee_categorization_tran.jobunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                    "   FROM hremployee_categorization_tran " & _
                    "   WHERE hremployee_categorization_tran.isvoid = 0 " & _
                    "   AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= @edate " & _
                    ") AS R ON R.employeeunkid = hremployee_master.employeeunkid AND R.rno = 1 " & _
                    "LEFT JOIN hrjob_master as jm ON jm.jobunkid = R.jobunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hremployee_transfer_tran.employeeunkid " & _
                    "       ,hremployee_transfer_tran.departmentunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                    "   AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= @edate " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.rno = 1 " & _
                    "LEFT JOIN hrdepartment_master as dp ON dp.departmentunkid = T.departmentunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        prsalaryincrement_tran.employeeunkid " & _
                    "       ,prsalaryincrement_tran.newscale " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC, prsalaryincrement_tran.salaryincrementtranunkid DESC) AS rno " & _
                    "   FROM prsalaryincrement_tran " & _
                    "   WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                    "       AND CONVERT(NVARCHAR(8),prsalaryincrement_tran.incrementdate,112) <= @edate " & _
                    ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.rno = 1 "

            If mdicCheckedTransHeadIds IsNot Nothing Then
                If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                        StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "   SELECT " & _
                                "        prpayrollprocess_tran.employeeunkid " & _
                                "       ,prpayrollprocess_tran.tranheadunkid " & _
                                "       ,prpayrollprocess_tran.amount " & _
                                "   FROM prpayrollprocess_tran " & _
                                "       JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "   WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                                "       AND prpayrollprocess_tran.tranheadunkid = '" & iKey.ToString & "' AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                                ") AS A" & iKey.ToString & " ON A" & iKey.ToString & ".employeeunkid = hremployee_master.employeeunkid "
                    Next
                End If
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= " WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            Call FilterTitleAndFilterQuery()

            objDataOperation.AddParameter("@GrpName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 405, "Staff On Unpaid Leave/Absent Without Permission"))
            objDataOperation.AddParameter("@sfo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 304, "Staff On"))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dview As DataView = dsList.Tables(0).DefaultView
            dview.Sort = "sortid ASC,leavename"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dview.ToTable.Copy)

            Dim intSrn As Integer = 0 : Dim strGrpId As String = ""
            For Each row As DataRow In dsList.Tables(0).Rows
                Dim drow As DataRow = dtTable.NewRow
                If strGrpId <> row.Item("leavename") Then
                    intSrn = 1
                    strGrpId = row.Item("leavename")
                End If

                drow.Item("Col1") = intSrn.ToString
                drow.Item("Col2") = row.Item("fullname")
                drow.Item("Col3") = row.Item("designation")
                drow.Item("Col4") = row.Item("department")
                drow.Item("Col5") = eZeeDate.convertDate(row.Item("stdate").ToString).ToShortDateString
                drow.Item("Col6") = eZeeDate.convertDate(row.Item("eddate").ToString).ToShortDateString
                drow.Item("Col7") = CDbl(row.Item("ndays"))
                drow.Item("Col8") = Format(CDec(row.Item("bsalary")), srtAmountFormat)
                drow.Item("Col9") = row.Item("comments")
                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            drow.Item("Head_" & iKey.ToString) = row.Item("Head_" & iKey.ToString)
                        Next
                    End If
                End If
                drow.Item("Col10") = row.Item("leavename")
                dtTable.Rows.Add(drow)
                intSrn = intSrn + 1
            Next

            StrGroupColName = "Col10"

            Return dtTable
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Staff_On_Leave_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function Staff_Promotion_Recategorization_Report(ByVal strDatabaseName As String, _
                                                             ByVal intUserUnkid As Integer, _
                                                             ByVal intYearUnkid As Integer, _
                                                             ByVal intCompanyUnkid As Integer, _
                                                             ByVal strUserModeSetting As String, _
                                                             ByVal blnOnlyApproved As Boolean, _
                                                             ByVal strCurrencyFormat As String _
                                                             ) As DataTable
        Dim StrQ As String = String.Empty
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtStartDate, mdtEndDate, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEndDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEndDate, strDatabaseName)

            '///////////// GENERATING DATATABLE //////////////////// --- START
            dtTable = New DataTable("NS")
            Dim dCol As DataColumn = Nothing

            With dtTable
                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col1"
                    .DataType = GetType(System.Int32)
                    .Caption = Language.getMessage(mstrModuleName, 200, "S/N")
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col2"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 201, "Full Name")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col3"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 402, "From Position")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col4"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 403, "To Position")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col5"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 404, "W.E.F")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)


                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col7"
                    .DataType = GetType(System.String)
                    .Caption = Language.getMessage(mstrModuleName, 414, "Categorization Reason")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Col6"
                    .DataType = GetType(System.Decimal)
                    .Caption = Language.getMessage(mstrModuleName, 205, "Basic Salary")
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            dCol = New DataColumn
                            With dCol
                                .ColumnName = "Head_" & iKey.ToString
                                .DataType = GetType(System.Decimal)
                                .Caption = mdicCheckedTransHeadIds(iKey)
                                .DefaultValue = 0
                            End With
                            .Columns.Add(dCol)
                        Next
                    End If
                End If
            End With
            '///////////// GENERATING DATATABLE //////////////////// --- END

            StrQ = "DECLARE @promotion as table (empid int, tjobid int, tdate nvarchar(8),changereasonunkid int) " & _
                   "INSERT INTO @promotion (empid,tjobid,tdate,changereasonunkid) " & _
                   "SELECT " & _
                   "     A.employeeunkid " & _
                   "    ,A.tojobid " & _
                   "    ,A.tdate " & _
                   "    ,ISNULL(A.changereasonunkid,0) AS changereasonunkid " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         ct.employeeunkid " & _
                   "        ,ct.jobunkid as tojobid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY ct.employeeunkid ORDER BY ct.effectivedate DESC) as rno " & _
                   "        ,CONVERT(NVARCHAR(8),ct.effectivedate,112) as tdate " & _
                   "        ,ct.changereasonunkid " & _
                   "    FROM hremployee_categorization_tran AS ct " & _
                   "    WHERE ct.isvoid = 0 " & _
                   "        AND CONVERT(NVARCHAR(8),ct.effectivedate,112) <= @edate " & _
                   ") AS A WHERE A.rno = 1 AND tdate BETWEEN @sdate AND @edate " & _
                   "SELECT " & _
                   "     ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS fullname " & _
                   "    ,ISNULL(fjm.job_name,'') AS fposition " & _
                   "    ,ISNULL(tjm.job_name,'') AS tposition " & _
                   "    ,[@promotion].tdate AS efdate " & _
                   "    ,ISNULL(S.newscale,0) AS bsalary " & _
                   "    ,ISNULL(ctr.name,@NA) AS creason "
            If mdicCheckedTransHeadIds IsNot Nothing Then
                If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                        StrQ &= "   ,ISNULL(A" & iKey.ToString & ".amount,0) AS Head_" & iKey.ToString & " "
                    Next
                End If
            End If
            StrQ &= "FROM @promotion " & _
                    "    JOIN hremployee_master ON hremployee_master.employeeunkid = [@promotion].empid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "    LEFT JOIN cfcommon_master AS ctr ON ctr.masterunkid = [@promotion].changereasonunkid AND ctr.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             prsalaryincrement_tran.employeeunkid " & _
                    "            ,prsalaryincrement_tran.newscale " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC, prsalaryincrement_tran.salaryincrementtranunkid DESC) AS rno " & _
                    "        FROM prsalaryincrement_tran " & _
                    "        WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                    "        AND CONVERT(NVARCHAR(8),prsalaryincrement_tran.incrementdate,112) <= @edate " & _
                    "    ) AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.rno = 1 " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             ot.employeeunkid " & _
                    "            ,ot.jobunkid as fjobid " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY ot.employeeunkid ORDER BY ot.effectivedate DESC) as rno " & _
                    "            ,CONVERT(NVARCHAR(8),ot.effectivedate,112) as tdate " & _
                    "        FROM hremployee_categorization_tran as ot " & _
                    "            JOIN @promotion ON [@promotion].empid = ot.employeeunkid " & _
                    "        WHERE ot.isvoid = 0 AND CONVERT(NVARCHAR(8),ot.effectivedate,112) < [@promotion].tdate " & _
                    "    ) AS pr ON pr.employeeunkid = [@promotion].empid AND pr.rno = 1 " & _
                    "    LEFT JOIN hrjob_master as tjm ON tjm.jobunkid = [@promotion].tjobid AND tjm.isactive = 1 " & _
                    "    LEFT JOIN hrjob_master as fjm ON fjm.jobunkid = pr.fjobid and fjm.isactive = 1 "
            If mdicCheckedTransHeadIds IsNot Nothing Then
                If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                        StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "   SELECT " & _
                                "        prpayrollprocess_tran.employeeunkid " & _
                                "       ,prpayrollprocess_tran.tranheadunkid " & _
                                "       ,prpayrollprocess_tran.amount " & _
                                "   FROM prpayrollprocess_tran " & _
                                "       JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "   WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                                "       AND prpayrollprocess_tran.tranheadunkid = '" & iKey.ToString & "' AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                                ") AS A" & iKey.ToString & " ON A" & iKey.ToString & ".employeeunkid = hremployee_master.employeeunkid "
                    Next
                End If
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If


            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If mintCategorizationReasonId > 0 Then
                StrQ &= " AND [@promotion].changereasonunkid = @changereasonunkid "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            'ISNULL(fjm.job_name,@NE) AS fposition
            'objDataOperation.AddParameter("@NE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 401, "Not Found"))

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategorizationReasonId)
            objDataOperation.AddParameter("@NA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 413, "N/A"))

            Call FilterTitleAndFilterQuery()

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim intSrn As Integer = 1
            For Each row As DataRow In dsList.Tables(0).Rows
                Dim drow As DataRow = dtTable.NewRow

                drow.Item("Col1") = intSrn.ToString
                drow.Item("Col2") = row.Item("fullname")
                drow.Item("Col3") = row.Item("fposition")
                drow.Item("Col4") = row.Item("tposition")
                drow.Item("Col5") = eZeeDate.convertDate(row.Item("efdate").ToString).ToShortDateString
                drow.Item("Col6") = Format(CDec(row.Item("bsalary")), strCurrencyFormat)
                drow.Item("Col7") = row.Item("creason")
                If mdicCheckedTransHeadIds IsNot Nothing Then
                    If mdicCheckedTransHeadIds.Keys.Count > 0 Then
                        For Each iKey As Integer In mdicCheckedTransHeadIds.Keys
                            drow.Item("Head_" & iKey.ToString) = row.Item("Head_" & iKey.ToString)
                        Next
                    End If
                End If
                dtTable.Rows.Add(drow)
                intSrn = intSrn + 1
            Next

            Return dtTable
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Staff_Promotion_Recategorization_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Report Generation "

    Public Sub Export_MonthlyPayroll_Report(ByVal strDatabaseName As String, _
                                            ByVal intUserUnkid As Integer, _
                                            ByVal intYearUnkid As Integer, _
                                            ByVal intCompanyUnkid As Integer, _
                                            ByVal dtPeriodStart As Date, _
                                            ByVal dtPeriodEnd As Date, _
                                            ByVal strUserModeSetting As String, _
                                            ByVal blnOnlyApproved As Boolean, _
                                            ByVal xOpenReportAfterExport As Boolean, _
                                            ByVal xExportPath As String, ByVal strCurrencyFormat As String)
        Dim mdtFinalTable As DataTable = Nothing
        Try
            objDataOperation = New clsDataOperation
            Dim StrGroupColName As String = String.Empty
            Select Case mintReportTypeId

                Case 1  'NEW STAFF
                    mdtFinalTable = New_Staff_Report(strDatabaseName, _
                                                     intUserUnkid, _
                                                     intYearUnkid, _
                                                     intCompanyUnkid, _
                                                     strUserModeSetting, _
                                                     blnOnlyApproved, strCurrencyFormat, StrGroupColName)
                Case 2  'EXIT STAFF
                    mdtFinalTable = Exit_Staff_Report(strDatabaseName, _
                                                      intUserUnkid, _
                                                      intYearUnkid, _
                                                      intCompanyUnkid, _
                                                      strUserModeSetting, _
                                                      blnOnlyApproved, StrGroupColName)
                Case 3  'STAFF ON LEAVE
                    mdtFinalTable = Staff_On_Leave_Report(strDatabaseName, _
                                                          intUserUnkid, _
                                                          intYearUnkid, _
                                                          intCompanyUnkid, _
                                                          strUserModeSetting, _
                                                          blnOnlyApproved, strCurrencyFormat, StrGroupColName)
                Case 4  'STAFF PROMOTION & RECATEGORIZATION
                    mdtFinalTable = Staff_Promotion_Recategorization_Report(strDatabaseName, _
                                                                            intUserUnkid, _
                                                                            intYearUnkid, _
                                                                            intCompanyUnkid, _
                                                                            strUserModeSetting, _
                                                                            blnOnlyApproved, strCurrencyFormat)
            End Select

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtFinalTable.Columns.Count - 1)
            For j As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(j) = 125
            Next

            Dim strarrGroupColumns As String() = Nothing
            Dim strGrpCols As String() = Nothing
            If StrGroupColName.Trim.Length > 0 Then
                If StrGroupColName.Trim.Length > 0 Then
                    If StrGroupColName.Contains(",") Then
                        strGrpCols = StrGroupColName.Split(",")
                    Else
                        strGrpCols = New String() {StrGroupColName}
                    End If
                End If
                strarrGroupColumns = strGrpCols
            End If

            Dim strReportName As String = Me._ReportName & " - [ " & mstrReportTypeName & " ]"
            Dim strExtraReportTitle As String = mstrPeriodName
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportPath, xOpenReportAfterExport, mdtFinalTable, intArrayColumnWidth, True, True, False, strarrGroupColumns, strReportName, strExtraReportTitle, Me._FilterTitle, Nothing, "", False, Nothing, Nothing, Nothing)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 101, "Employee :")
            Language.setMessage(mstrModuleName, 102, "Membership :")
            Language.setMessage(mstrModuleName, 103, "Exit Reason :")
            Language.setMessage(mstrModuleName, 200, "S/N")
            Language.setMessage(mstrModuleName, 201, "Full Name")
            Language.setMessage(mstrModuleName, 202, "Job Title")
            Language.setMessage(mstrModuleName, 203, "Program/Department")
            Language.setMessage(mstrModuleName, 204, "Date Of Joining")
            Language.setMessage(mstrModuleName, 205, "Basic Salary")
            Language.setMessage(mstrModuleName, 206, "Type Of Membership")
            Language.setMessage(mstrModuleName, 207, "Contract Expiry Date")
            Language.setMessage(mstrModuleName, 208, "Employment Type")
            Language.setMessage(mstrModuleName, 209, "Comment")
            Language.setMessage(mstrModuleName, 210, "New Staff")
            Language.setMessage(mstrModuleName, 211, "Re-hired Staff")
            Language.setMessage(mstrModuleName, 301, "Start Date")
            Language.setMessage(mstrModuleName, 302, "End Date")
            Language.setMessage(mstrModuleName, 303, "No Of Days")
            Language.setMessage(mstrModuleName, 304, "Staff On")
            Language.setMessage(mstrModuleName, 402, "From Position")
            Language.setMessage(mstrModuleName, 403, "To Position")
            Language.setMessage(mstrModuleName, 404, "W.E.F")
            Language.setMessage(mstrModuleName, 405, "Staff On Unpaid Leave/Absent Without Permission")
            Language.setMessage(mstrModuleName, 406, "Last Working Date")
            Language.setMessage(mstrModuleName, 407, "Last Month In Payroll")
            Language.setMessage(mstrModuleName, 408, "Leaving Date")
            Language.setMessage(mstrModuleName, 409, "EOC Date")
            Language.setMessage(mstrModuleName, 410, "Retirement Date")
            Language.setMessage(mstrModuleName, 411, "Termination")
            Language.setMessage(mstrModuleName, 412, "Retirement By System")
            Language.setMessage(mstrModuleName, 413, "N/A")
            Language.setMessage(mstrModuleName, 414, "Categorization Reason")
            Language.setMessage(mstrModuleName, 415, "Categorization Reason :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
