﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMovementReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.lvAllocation = New System.Windows.Forms.ListView
        Me.colhAllocations = New System.Windows.Forms.ColumnHeader
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblDateTo = New System.Windows.Forms.Label
        Me.lblDateFrom = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblReportType = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.gbMandatoryInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 482)
        Me.NavPanel.Size = New System.Drawing.Size(686, 55)
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.chkInactiveemp)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.txtSearch)
        Me.gbMandatoryInfo.Controls.Add(Me.objchkAll)
        Me.gbMandatoryInfo.Controls.Add(Me.lvAllocation)
        Me.gbMandatoryInfo.Controls.Add(Me.lblAllocation)
        Me.gbMandatoryInfo.Controls.Add(Me.cboAllocations)
        Me.gbMandatoryInfo.Controls.Add(Me.lblEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.cboEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.lblDateTo)
        Me.gbMandatoryInfo.Controls.Add(Me.lblDateFrom)
        Me.gbMandatoryInfo.Controls.Add(Me.dtpToDate)
        Me.gbMandatoryInfo.Controls.Add(Me.lblReportType)
        Me.gbMandatoryInfo.Controls.Add(Me.dtpFromDate)
        Me.gbMandatoryInfo.Controls.Add(Me.cboReportType)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(394, 356)
        Me.gbMandatoryInfo.TabIndex = 18
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(87, 330)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(269, 16)
        Me.chkInactiveemp.TabIndex = 94
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(362, 88)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 92
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(87, 142)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(269, 21)
        Me.txtSearch.TabIndex = 91
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAll.Location = New System.Drawing.Point(331, 174)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 90
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'lvAllocation
        '
        Me.lvAllocation.CheckBoxes = True
        Me.lvAllocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhAllocations})
        Me.lvAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAllocation.FullRowSelect = True
        Me.lvAllocation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAllocation.HideSelection = False
        Me.lvAllocation.Location = New System.Drawing.Point(87, 169)
        Me.lvAllocation.Name = "lvAllocation"
        Me.lvAllocation.Size = New System.Drawing.Size(269, 155)
        Me.lvAllocation.TabIndex = 89
        Me.lvAllocation.UseCompatibleStateImageBehavior = False
        Me.lvAllocation.View = System.Windows.Forms.View.Details
        '
        'colhAllocations
        '
        Me.colhAllocations.Tag = "colhAllocations"
        Me.colhAllocations.Text = ""
        Me.colhAllocations.Width = 260
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(8, 118)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(73, 16)
        Me.lblAllocation.TabIndex = 88
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(87, 115)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(269, 21)
        Me.cboAllocations.TabIndex = 87
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 90)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(73, 16)
        Me.lblEmployee.TabIndex = 86
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 145
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(87, 88)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(269, 21)
        Me.cboEmployee.TabIndex = 85
        '
        'lblDateTo
        '
        Me.lblDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateTo.Location = New System.Drawing.Point(193, 63)
        Me.lblDateTo.Name = "lblDateTo"
        Me.lblDateTo.Size = New System.Drawing.Size(51, 16)
        Me.lblDateTo.TabIndex = 84
        Me.lblDateTo.Text = "Date To"
        Me.lblDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDateFrom
        '
        Me.lblDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateFrom.Location = New System.Drawing.Point(8, 63)
        Me.lblDateFrom.Name = "lblDateFrom"
        Me.lblDateFrom.Size = New System.Drawing.Size(73, 16)
        Me.lblDateFrom.TabIndex = 67
        Me.lblDateFrom.Text = "Date From"
        Me.lblDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(256, 61)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(100, 21)
        Me.dtpToDate.TabIndex = 83
        '
        'lblReportType
        '
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(73, 16)
        Me.lblReportType.TabIndex = 60
        Me.lblReportType.Text = "Report Type"
        Me.lblReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(87, 61)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(100, 21)
        Me.dtpFromDate.TabIndex = 68
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 145
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(87, 34)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(269, 21)
        Me.cboReportType.TabIndex = 59
        '
        'frmMovementReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(686, 537)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmMovementReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Movement Report"
        Me.Controls.SetChildIndex(Me.gbMandatoryInfo, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbMandatoryInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblReportType As System.Windows.Forms.Label
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateTo As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvAllocation As System.Windows.Forms.ListView
    Friend WithEvents colhAllocations As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
End Class
