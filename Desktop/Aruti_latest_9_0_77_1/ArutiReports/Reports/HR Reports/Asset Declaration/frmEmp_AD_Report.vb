'************************************************************************************************************************************
'Class Name : frmEmp_AD_Report.vb
'Purpose    : 
'Written By : Sandeep Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmp_AD_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmp_AD_Report"
    Private objGenADRpt As clsEmp_AD_Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
        objGenADRpt = New clsEmp_AD_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objGenADRpt.SetDefaultValue()
        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        _Show_AdvanceFilter = True
        'S.SANDEEP [ 13 FEB 2013 ] -- END
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Employees With Non Official Assets Declarations"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Employees Without Assets Declarations"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Employees Who Declarared Their Assets"))
                .SelectedIndex = 0
            End With

            dsList = objMData.GetAD_Parameter_List(False, "List")
            With cboAppointment
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 1
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboAppointment.SelectedValue = 1
            cboEmployee.SelectedValue = 0
            cboReportType.SelectedIndex = 0
            dtpAsOnDate.Value = ConfigParameter._Object._CurrentDateAndTime

            dtpDate1.Value = dtpAsOnDate.Value
            dtpDate2.Value = dtpAsOnDate.Value

            dtpDate1.Checked = False
            dtpDate2.Checked = False

            txtDAmountFrom.Text = ""
            txtDAmountTo.Text = ""
            txtLAmountFrom.Text = ""
            txtLAmountTo.Text = ""

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            objGenADRpt.setDefaultOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objGenADRpt.OrderByDisplay

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objGenADRpt.SetDefaultValue()

            If cboAppointment.SelectedValue = enAD_Report_Parameter.APP_DATE_FROM Then
                If dtpDate1.Checked = True AndAlso dtpDate2.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date."), enMsgBoxStyle.Information)
                    dtpDate2.Focus()
                    Return False
                ElseIf dtpDate1.Checked = False AndAlso dtpDate2.Checked = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date."), enMsgBoxStyle.Information)
                    dtpDate1.Focus()
                    Return False
                ElseIf dtpDate1.Checked = True AndAlso dtpDate2.Checked = True Then
                    If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Appointment To Date cannot be less then Appointment From Date."), enMsgBoxStyle.Information)
                        dtpDate2.Focus()
                        Return False
                    End If
                End If
            End If

            objGenADRpt._AppointmentTypeId = cboAppointment.SelectedValue
            objGenADRpt._AppointmentTypeName = cboAppointment.Text
            objGenADRpt._AsOnDate = dtpAsOnDate.Value.Date
            objGenADRpt._EmployeeId = cboEmployee.SelectedValue
            objGenADRpt._EmployeeName = cboEmployee.Text
            objGenADRpt._IncluderInactiveEmp = chkInactiveemp.Checked
            If txtDAmountFrom.Text <> "" AndAlso txtDAmountTo.Text <> "" Then
                objGenADRpt._DAmtFrom = txtDAmountFrom.Decimal
                objGenADRpt._DAmtTo = txtDAmountTo.Decimal
            End If

            If txtLAmountFrom.Text <> "" AndAlso txtLAmountTo.Text <> "" Then
                objGenADRpt._LAmtFrom = txtLAmountFrom.Decimal
                objGenADRpt._LAmtTo = txtLAmountTo.Decimal
            End If

            If dtpDate1.Checked = True Then
                objGenADRpt._Date1 = dtpDate1.Value.Date
            End If

            If dtpDate2.Visible = True Then
                If dtpDate2.Checked = True Then
                    objGenADRpt._Date2 = dtpDate2.Value.Date
                End If
            End If
            objGenADRpt._ReportTypeId = cboReportType.SelectedIndex
            objGenADRpt._ReportTypeName = cboReportType.Text
            objGenADRpt._ShowSalary = chkShowEmpSal.Checked
            objGenADRpt._ViewByIds = mstrStringIds
            objGenADRpt._ViewIndex = mintViewIdx
            objGenADRpt._ViewByName = mstrStringName
            objGenADRpt._Analysis_Fields = mstrAnalysis_Fields
            objGenADRpt._Analysis_Join = mstrAnalysis_Join
            objGenADRpt._Analysis_OrderBy = mstrAnalysis_OrderBy
            objGenADRpt._Report_GroupName = mstrReport_GroupName

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objGenADRpt._Advance_Filter = mstrAdvanceFilter
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms Events "

    Private Sub frmEmp_AD_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objGenADRpt = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmp_AD_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmp_AD_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Me._Title = objGenADRpt._ReportName
            Me._Message = objGenADRpt._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmp_AD_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmp_AD_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsEmp_AD_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Evemts "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objGenADRpt.generateReport(cboReportType.SelectedIndex, e.Type, enExportAction.None)
            objGenADRpt.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._ExportReportPath, _
                                          ConfigParameter._Object._OpenAfterExport, _
                                          cboReportType.SelectedIndex, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objGenADRpt.generateReport(cboReportType.SelectedIndex, enPrintAction.None, e.Type)
            objGenADRpt.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._ExportReportPath, _
                                          ConfigParameter._Object._OpenAfterExport, _
                                          cboReportType.SelectedIndex, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objGenADRpt.setOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objGenADRpt.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Controls Events "

    Private Sub cboAppointment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAppointment.SelectedIndexChanged
        Try
            objlblCaption.Text = cboAppointment.Text
            Select Case CInt(cboAppointment.SelectedValue)
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If dtpDate2.Visible = False Then dtpDate2.Visible = True
                    If lblTo.Visible = False Then lblTo.Visible = True
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    dtpDate2.Visible = False : lblTo.Visible = False
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    dtpDate2.Visible = False : lblTo.Visible = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAppointment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            txtDAmountFrom.Text = "" : txtDAmountTo.Text = ""
            txtLAmountFrom.Text = "" : txtLAmountTo.Text = ""
            Select Case cboReportType.SelectedIndex
                Case 0, 2
                    If txtDAmountFrom.Enabled = False Then txtDAmountFrom.Enabled = True
                    If txtDAmountTo.Enabled = False Then txtDAmountTo.Enabled = True

                    If txtLAmountFrom.Enabled = False Then txtLAmountFrom.Enabled = True
                    If txtLAmountTo.Enabled = False Then txtLAmountTo.Enabled = True
                Case 1
                    txtDAmountFrom.Enabled = False : txtDAmountTo.Enabled = False
                    txtLAmountFrom.Enabled = False : txtLAmountTo.Enabled = False
            End Select

            objGenADRpt.setDefaultOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objGenADRpt.OrderByDisplay

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblAsOnDate.Text = Language._Object.getCaption(Me.lblAsOnDate.Name, Me.lblAsOnDate.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
			Me.lblAppointment.Text = Language._Object.getCaption(Me.lblAppointment.Name, Me.lblAppointment.Text)
			Me.lblDecAmt.Text = Language._Object.getCaption(Me.lblDecAmt.Name, Me.lblDecAmt.Text)
			Me.lblLAmtTo.Text = Language._Object.getCaption(Me.lblLAmtTo.Name, Me.lblLAmtTo.Text)
			Me.lblDAmtTo.Text = Language._Object.getCaption(Me.lblDAmtTo.Name, Me.lblDAmtTo.Text)
			Me.lblLiability.Text = Language._Object.getCaption(Me.lblLiability.Name, Me.lblLiability.Text)
			Me.chkShowEmpSal.Text = Language._Object.getCaption(Me.chkShowEmpSal.Name, Me.chkShowEmpSal.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employees With Non Official Assets Declarations")
			Language.setMessage(mstrModuleName, 2, "Employees Without Assets Declarations")
			Language.setMessage(mstrModuleName, 3, "Employees Who Declarared Their Assets")
			Language.setMessage(mstrModuleName, 4, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date.")
			Language.setMessage(mstrModuleName, 5, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date.")
			Language.setMessage(mstrModuleName, 6, "Sorry, Appointment To Date cannot be less then Appointment From Date.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
