﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmManningLevel_Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lblVarianceTo = New System.Windows.Forms.Label
        Me.lblAvailableTo = New System.Windows.Forms.Label
        Me.lblPlannedTo = New System.Windows.Forms.Label
        Me.lblVariance = New System.Windows.Forms.Label
        Me.lblAvailableCount = New System.Windows.Forms.Label
        Me.lblPlannedCount = New System.Windows.Forms.Label
        Me.txtVarianceTo = New eZee.TextBox.IntegerTextBox
        Me.txtAvailableTo = New eZee.TextBox.IntegerTextBox
        Me.txtVarianceFrom = New eZee.TextBox.IntegerTextBox
        Me.txtAvailableFrom = New eZee.TextBox.IntegerTextBox
        Me.txtPlannedTo = New eZee.TextBox.IntegerTextBox
        Me.txtPlannedFrom = New eZee.TextBox.IntegerTextBox
        Me.objbtnSearchJob = New eZee.Common.eZeeGradientButton
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.objbtnSearchJobGroup = New eZee.Common.eZeeGradientButton
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 485)
        Me.NavPanel.Size = New System.Drawing.Size(726, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lblVarianceTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblAvailableTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblPlannedTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblVariance)
        Me.gbFilterCriteria.Controls.Add(Me.lblAvailableCount)
        Me.gbFilterCriteria.Controls.Add(Me.lblPlannedCount)
        Me.gbFilterCriteria.Controls.Add(Me.txtVarianceTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtAvailableTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtVarianceFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtAvailableFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtPlannedTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtPlannedFrom)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.Label2)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.Label1)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(495, 169)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(11, 143)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(447, 16)
        Me.chkInactiveemp.TabIndex = 8
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lblVarianceTo
        '
        Me.lblVarianceTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVarianceTo.Location = New System.Drawing.Point(263, 119)
        Me.lblVarianceTo.Name = "lblVarianceTo"
        Me.lblVarianceTo.Size = New System.Drawing.Size(52, 15)
        Me.lblVarianceTo.TabIndex = 16
        Me.lblVarianceTo.Text = "To"
        Me.lblVarianceTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAvailableTo
        '
        Me.lblAvailableTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAvailableTo.Location = New System.Drawing.Point(263, 92)
        Me.lblAvailableTo.Name = "lblAvailableTo"
        Me.lblAvailableTo.Size = New System.Drawing.Size(52, 15)
        Me.lblAvailableTo.TabIndex = 12
        Me.lblAvailableTo.Text = "To"
        Me.lblAvailableTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlannedTo
        '
        Me.lblPlannedTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlannedTo.Location = New System.Drawing.Point(263, 65)
        Me.lblPlannedTo.Name = "lblPlannedTo"
        Me.lblPlannedTo.Size = New System.Drawing.Size(52, 15)
        Me.lblPlannedTo.TabIndex = 8
        Me.lblPlannedTo.Text = "To"
        Me.lblPlannedTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVariance
        '
        Me.lblVariance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVariance.Location = New System.Drawing.Point(8, 119)
        Me.lblVariance.Name = "lblVariance"
        Me.lblVariance.Size = New System.Drawing.Size(79, 15)
        Me.lblVariance.TabIndex = 14
        Me.lblVariance.Text = "Variance From"
        Me.lblVariance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAvailableCount
        '
        Me.lblAvailableCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAvailableCount.Location = New System.Drawing.Point(8, 92)
        Me.lblAvailableCount.Name = "lblAvailableCount"
        Me.lblAvailableCount.Size = New System.Drawing.Size(79, 15)
        Me.lblAvailableCount.TabIndex = 10
        Me.lblAvailableCount.Text = "Available From"
        Me.lblAvailableCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlannedCount
        '
        Me.lblPlannedCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlannedCount.Location = New System.Drawing.Point(8, 65)
        Me.lblPlannedCount.Name = "lblPlannedCount"
        Me.lblPlannedCount.Size = New System.Drawing.Size(79, 15)
        Me.lblPlannedCount.TabIndex = 6
        Me.lblPlannedCount.Text = "Planned From"
        Me.lblPlannedCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVarianceTo
        '
        Me.txtVarianceTo.AllowNegative = True
        Me.txtVarianceTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtVarianceTo.DigitsInGroup = 0
        Me.txtVarianceTo.Flags = 0
        Me.txtVarianceTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVarianceTo.Location = New System.Drawing.Point(321, 116)
        Me.txtVarianceTo.MaxDecimalPlaces = 0
        Me.txtVarianceTo.MaxWholeDigits = 9
        Me.txtVarianceTo.Name = "txtVarianceTo"
        Me.txtVarianceTo.Prefix = ""
        Me.txtVarianceTo.RangeMax = 2147483647
        Me.txtVarianceTo.RangeMin = -2147483648
        Me.txtVarianceTo.Size = New System.Drawing.Size(137, 21)
        Me.txtVarianceTo.TabIndex = 17
        Me.txtVarianceTo.Text = "0"
        Me.txtVarianceTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAvailableTo
        '
        Me.txtAvailableTo.AllowNegative = True
        Me.txtAvailableTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAvailableTo.DigitsInGroup = 0
        Me.txtAvailableTo.Flags = 0
        Me.txtAvailableTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAvailableTo.Location = New System.Drawing.Point(321, 89)
        Me.txtAvailableTo.MaxDecimalPlaces = 0
        Me.txtAvailableTo.MaxWholeDigits = 9
        Me.txtAvailableTo.Name = "txtAvailableTo"
        Me.txtAvailableTo.Prefix = ""
        Me.txtAvailableTo.RangeMax = 2147483647
        Me.txtAvailableTo.RangeMin = -2147483648
        Me.txtAvailableTo.Size = New System.Drawing.Size(137, 21)
        Me.txtAvailableTo.TabIndex = 13
        Me.txtAvailableTo.Text = "0"
        Me.txtAvailableTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVarianceFrom
        '
        Me.txtVarianceFrom.AllowNegative = True
        Me.txtVarianceFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtVarianceFrom.DigitsInGroup = 0
        Me.txtVarianceFrom.Flags = 0
        Me.txtVarianceFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVarianceFrom.Location = New System.Drawing.Point(93, 116)
        Me.txtVarianceFrom.MaxDecimalPlaces = 0
        Me.txtVarianceFrom.MaxWholeDigits = 9
        Me.txtVarianceFrom.Name = "txtVarianceFrom"
        Me.txtVarianceFrom.Prefix = ""
        Me.txtVarianceFrom.RangeMax = 2147483647
        Me.txtVarianceFrom.RangeMin = -2147483648
        Me.txtVarianceFrom.Size = New System.Drawing.Size(137, 21)
        Me.txtVarianceFrom.TabIndex = 15
        Me.txtVarianceFrom.Text = "0"
        Me.txtVarianceFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAvailableFrom
        '
        Me.txtAvailableFrom.AllowNegative = True
        Me.txtAvailableFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAvailableFrom.DigitsInGroup = 0
        Me.txtAvailableFrom.Flags = 0
        Me.txtAvailableFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAvailableFrom.Location = New System.Drawing.Point(93, 89)
        Me.txtAvailableFrom.MaxDecimalPlaces = 0
        Me.txtAvailableFrom.MaxWholeDigits = 9
        Me.txtAvailableFrom.Name = "txtAvailableFrom"
        Me.txtAvailableFrom.Prefix = ""
        Me.txtAvailableFrom.RangeMax = 2147483647
        Me.txtAvailableFrom.RangeMin = -2147483648
        Me.txtAvailableFrom.Size = New System.Drawing.Size(137, 21)
        Me.txtAvailableFrom.TabIndex = 11
        Me.txtAvailableFrom.Text = "0"
        Me.txtAvailableFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPlannedTo
        '
        Me.txtPlannedTo.AllowNegative = True
        Me.txtPlannedTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPlannedTo.DigitsInGroup = 0
        Me.txtPlannedTo.Flags = 0
        Me.txtPlannedTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlannedTo.Location = New System.Drawing.Point(321, 62)
        Me.txtPlannedTo.MaxDecimalPlaces = 0
        Me.txtPlannedTo.MaxWholeDigits = 9
        Me.txtPlannedTo.Name = "txtPlannedTo"
        Me.txtPlannedTo.Prefix = ""
        Me.txtPlannedTo.RangeMax = 2147483647
        Me.txtPlannedTo.RangeMin = -2147483648
        Me.txtPlannedTo.Size = New System.Drawing.Size(137, 21)
        Me.txtPlannedTo.TabIndex = 9
        Me.txtPlannedTo.Text = "0"
        Me.txtPlannedTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPlannedFrom
        '
        Me.txtPlannedFrom.AllowNegative = True
        Me.txtPlannedFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPlannedFrom.DigitsInGroup = 0
        Me.txtPlannedFrom.Flags = 0
        Me.txtPlannedFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlannedFrom.Location = New System.Drawing.Point(93, 62)
        Me.txtPlannedFrom.MaxDecimalPlaces = 0
        Me.txtPlannedFrom.MaxWholeDigits = 9
        Me.txtPlannedFrom.Name = "txtPlannedFrom"
        Me.txtPlannedFrom.Prefix = ""
        Me.txtPlannedFrom.RangeMax = 2147483647
        Me.txtPlannedFrom.RangeMin = -2147483648
        Me.txtPlannedFrom.Size = New System.Drawing.Size(137, 21)
        Me.txtPlannedFrom.TabIndex = 7
        Me.txtPlannedFrom.Text = "0"
        Me.txtPlannedFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnSearchJob
        '
        Me.objbtnSearchJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJob.BorderSelected = False
        Me.objbtnSearchJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJob.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJob.Location = New System.Drawing.Point(464, 35)
        Me.objbtnSearchJob.Name = "objbtnSearchJob"
        Me.objbtnSearchJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJob.TabIndex = 5
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 300
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(321, 35)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(137, 21)
        Me.cboJob.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(263, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Job"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchJobGroup
        '
        Me.objbtnSearchJobGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJobGroup.BorderSelected = False
        Me.objbtnSearchJobGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJobGroup.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJobGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJobGroup.Location = New System.Drawing.Point(236, 35)
        Me.objbtnSearchJobGroup.Name = "objbtnSearchJobGroup"
        Me.objbtnSearchJobGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJobGroup.TabIndex = 2
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobGroup.DropDownWidth = 300
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(93, 35)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(137, 21)
        Me.cboJobGroup.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Job Group"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 241)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(495, 63)
        Me.gbSortBy.TabIndex = 1
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(464, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(78, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(93, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(365, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'frmManningLevel_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(726, 540)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmManningLevel_Report"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Manning Level Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchJob As eZee.Common.eZeeGradientButton
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchJobGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents lblVariance As System.Windows.Forms.Label
    Friend WithEvents lblAvailableCount As System.Windows.Forms.Label
    Friend WithEvents lblPlannedCount As System.Windows.Forms.Label
    Friend WithEvents txtVarianceTo As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtAvailableTo As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtVarianceFrom As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtAvailableFrom As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtPlannedTo As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtPlannedFrom As eZee.TextBox.IntegerTextBox
    Friend WithEvents lblVarianceTo As System.Windows.Forms.Label
    Friend WithEvents lblAvailableTo As System.Windows.Forms.Label
    Friend WithEvents lblPlannedTo As System.Windows.Forms.Label
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
End Class
