'************************************************************************************************************************************
'Class Name : clsEmployeeDistributionReport.vb
'Purpose    :
'Date       :01/10/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsEmployeeDistributionReport
    Inherits IReportData

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeDistributionReport"
    'Pinkal (24-Jun-2011) -- End
    Private mstrReportId As String = enArutiReport.EmployeeDistributionReport '28
    Private objDataOperation As clsDataOperation
    Private dtFinalTable As DataTable
    Private dsEmpList As New DataSet
    Private dsCommList As New DataSet
    Private dtTempTable As DataTable
    Private dblColTot As Decimal()

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintMonthId As Integer = -1
    Private mstrMonthName As String = String.Empty
    Private mintYearId As Integer = 0
    Private mstrYearName As String = String.Empty
    Private mintDistributionById As Integer = -1
    Private mstrDistributionName As String = String.Empty
    Private mdtAsOnDate As DateTime = Nothing
    Private mintViewTypeId As Integer = 0

    'Sandeep [ 09 MARCH 2011 ] -- Start
    Dim StrFinalPath As String = String.Empty
    'Sandeep [ 09 MARCH 2011 ] -- End 


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Private dtFinal As DataTable = Nothing
    Private dclColTot As Decimal()
    'Pinkal (14-Aug-2012) -- End

    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrUserAccessFilter As String = String.Empty
    'Pinkal (12-Nov-2012) -- End

    Private mStrEmplType_Date As String = ""

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _MonthId() As Integer
        Set(ByVal value As Integer)
            mintMonthId = value
        End Set
    End Property

    Public WriteOnly Property _MonthName() As String
        Set(ByVal value As String)
            mstrMonthName = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    Public WriteOnly Property _YearName() As String
        Set(ByVal value As String)
            mstrYearName = value
        End Set
    End Property

    Public WriteOnly Property _DistributionId() As Integer
        Set(ByVal value As Integer)
            mintDistributionById = value
        End Set
    End Property

    Public WriteOnly Property _DistributionName() As String
        Set(ByVal value As String)
            mstrDistributionName = value
        End Set
    End Property

    Public WriteOnly Property _AsOnDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property

    Public WriteOnly Property _ViewTypeId() As Integer
        Set(ByVal value As Integer)
            mintViewTypeId = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Public ReadOnly Property _dtFinal() As DataTable
        Get
            Return dtFinal
        End Get
    End Property

    Public ReadOnly Property _dclColTot() As Decimal()
        Get
            Return dclColTot
        End Get
    End Property

    'Pinkal (14-Aug-2012) -- End


    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (12-Nov-2012) -- End

    Public WriteOnly Property _EmplType_Date() As String
        Set(ByVal value As String)
            mStrEmplType_Date = value
        End Set
    End Property

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintDistributionById = 0
            mstrDistributionName = String.Empty
            mintMonthId = 0
            mstrMonthName = String.Empty
            mintYearId = 0
            mstrYearName = String.Empty
            mdtAsOnDate = Nothing
            mintViewTypeId = 0



            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End
            mStrEmplType_Date = ""

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mdtAsOnDate <> Nothing Then
                objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            End If

            Select Case mintViewTypeId
                Case 1 'Date
                    'objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "As On Date :") & " " & mdtAsOnDate.Date & " "
                Case 2 'Month
                    objDataOperation.AddParameter("@MonthId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMonthId)
                    objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrYearName)
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Month :") & " " & mstrMonthName & " " & Language.getMessage(mstrModuleName, 18, "And") & " " & _
                                                      Language.getMessage(mstrModuleName, 19, "Year :") & " " & mstrYearName & " "

            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Sub CreateTable()
        Try
            dtFinalTable = New DataTable("Distribution")
            Dim dCol As DataColumn

            dCol = New DataColumn("no")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = Language.getMessage(mstrModuleName, 1, "No.")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("distribution")
            dCol.DataType = System.Type.GetType("System.String")
            Select Case mintDistributionById
                Case 1
                    dCol.Caption = Language.getMessage(mstrModuleName, 2, "Department Name")
                Case 2
                    dCol.Caption = Language.getMessage(mstrModuleName, 3, "Branch Name")
            End Select
            dCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("allstaff")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = Language.getMessage(mstrModuleName, 4, "Staff C/F")
            dCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("newstaff")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = Language.getMessage(mstrModuleName, 5, "New Staff")
            dCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dCol)

            'dCol = New DataColumn("transferin")
            'dCol.DataType = System.Type.GetType("System.Int32")
            'dCol.Caption = Language.getMessage(mstrModuleName, 6, "Transfers IN")
            'dCol.DefaultValue = 0
            'dtFinalTable.Columns.Add(dCol)

            'dCol = New DataColumn("transferout")
            'dCol.DataType = System.Type.GetType("System.Int32")
            'dCol.Caption = Language.getMessage(mstrModuleName, 7, "Transfers OUT")
            'dCol.DefaultValue = 0
            'dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("attotal")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = Language.getMessage(mstrModuleName, 22, "Total Staff")
            dCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("expired")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = Language.getMessage(mstrModuleName, 8, "Expired Contracts")
            dCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("staffexits")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Staff Exits")
            dCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dCol)

            '--------- SETTING DYNAMIC COLUMNS ----- START
            Dim objCMaster As New clsCommon_Master
            dsCommList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, False, "List")
            If dsCommList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsCommList.Tables("List").Rows
                    dCol = New DataColumn("Column" & dtRow.Item("masterunkid"))
                    dCol.DefaultValue = 0
                    dCol.Caption = dtRow.Item("name")
                    dtFinalTable.Columns.Add(dCol)
                Next
            End If
            objCMaster = Nothing
            '--------- SETTING DYNAMIC COLUMNS ----- END

            dCol = New DataColumn("male")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = Language.getMessage(mstrModuleName, 20, "Male")
            dCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("female")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = Language.getMessage(mstrModuleName, 10, "Female")
            dCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("NA")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = Language.getMessage(mstrModuleName, 110, "N/A")
            dCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("distribid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dCol)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub SetDepartmentValue(ByVal intDistributionId As Integer)
        Dim objObject As Object = Nothing
        Try
            Select Case intDistributionId
                Case 1  '---- Department
                    objObject = New clsDepartment
                    dsCommList = CType(objObject, clsDepartment).getComboList("List", False)
                Case 2  '---- Branch
                    objObject = New clsStation
                    dsCommList = CType(objObject, clsStation).getComboList("List", False)
            End Select
            Dim intCnt As Integer = 1

            If dsCommList.Tables("List").Rows.Count > 0 Then
                For Each dRow As DataRow In dsCommList.Tables("List").Rows
                    Dim dtFRow As DataRow = dtFinalTable.NewRow

                    dtFRow.Item("no") = intCnt.ToString
                    dtFRow.Item("distribution") = dRow.Item("name")

                    Select Case intDistributionId
                        Case 1
                            dtFRow.Item("distribid") = dRow.Item("departmentunkid")
                        Case 2
                            dtFRow.Item("distribid") = dRow.Item("stationunkid")
                    End Select

                    intCnt += 1
                    dtFinalTable.Rows.Add(dtFRow)
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDepartmentValue; Module Name: " & mstrModuleName)
        Finally
            objObject = Nothing
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Sub SetAllStaffValue(ByVal strDatabaseName As String, _
                                 ByVal intUserUnkid As Integer, _
                                 ByVal intYearUnkid As Integer, _
                                 ByVal intCompanyUnkid As Integer, _
                                 ByVal dtPeriodStart As Date, _
                                 ByVal dtPeriodEnd As Date, _
                                 ByVal strUserModeSetting As String, _
                                 ByVal blnOnlyApproved As Boolean)
        'Private Sub SetAllStaffValue()

        dtPeriodStart = mdtAsOnDate
        dtPeriodEnd = mdtAsOnDate

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
        If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT "
            'Select Case mintDistributionById
            '    Case 1 'Department
            '        StrQ &= " departmentunkid "
            '    Case 2 'Branch
            '        StrQ &= " stationunkid "
            'End Select

            'StrQ &= ",COUNT(employeeunkid) AS Staff " & _
            '        "FROM hremployee_master "

            'Select Case mintViewTypeId
            '    Case 1 'Date
            '        StrQ &= "WHERE CONVERT(CHAR(8),appointeddate,112) <= @Date " & _
            '                " AND CONVERT(CHAR(8),appointeddate,112) <> @Date "
            '    Case 2 'Month
            '        'StrQ &= "WHERE DATEPART(mm,appointeddate) < @MonthId " & _
            '        '             " AND DATEPART(yy,appointeddate) <= @YearId "
            '        StrQ &= "WHERE CONVERT(CHAR(8),appointeddate,112) <= @Date " & _
            '             " AND CONVERT(CHAR(8),appointeddate,112) <> @Date "
            'End Select
            ''" AND DATEPART(mm,appointeddate) <> @MonthId " & _

            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'If mblnIsActive = False Then
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ISSUE : TRA ENHANCEMENTS
            '    'StrQ &= " AND hremployee_master.isactive  = 1"
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "

            '    'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            'End If
            ''Pinkal (24-Jun-2011) -- End

            ''Pinkal (12-Nov-2012) -- Start
            ''Enhancement : TRA Changes

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            ''Pinkal (12-Nov-2012) -- End

            'Select Case mintDistributionById
            '    Case 1  'Department
            '        StrQ &= "GROUP BY departmentunkid " & _
            '                "ORDER BY departmentunkid "
            '    Case 2  'Branch
            '        StrQ &= "GROUP BY stationunkid " & _
            '                "ORDER BY stationunkid "
            'End Select


            StrQ = "SELECT "
            Select Case mintDistributionById
                Case 1 'Department
                    StrQ &= " AL.departmentunkid "
                Case 2 'Branch
                    StrQ &= " AL.stationunkid "
            End Select

            StrQ &= ",COUNT(hremployee_master.employeeunkid) AS Staff " & _
                    "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        departmentunkid " & _
                    "       ,stationunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ")AS AL ON AL.employeeunkid = hremployee_master.employeeunkid AND AL.rno = 1 "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            Select Case mintViewTypeId
                Case 1 'Date
                    StrQ &= "WHERE CONVERT(CHAR(8),appointeddate,112) <= @Date " & _
                            " AND CONVERT(CHAR(8),appointeddate,112) <> @Date "
                Case 2 'Month
                    StrQ &= "WHERE CONVERT(CHAR(8),appointeddate,112) <= @Date " & _
                         " AND CONVERT(CHAR(8),appointeddate,112) <> @Date "
            End Select

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            Select Case mintDistributionById
                Case 1  'Department
                    StrQ &= "GROUP BY AL.departmentunkid " & _
                            "ORDER BY AL.departmentunkid "
                Case 2  'Branch
                    StrQ &= "GROUP BY AL.stationunkid " & _
                            "ORDER BY AL.stationunkid "
            End Select
            'S.SANDEEP [04 JUN 2015] -- END

            Call FilterTitleAndFilterQuery()
            dsCommList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTemp() As DataRow = Nothing

            If dsCommList.Tables("DataTable").Rows.Count > 0 Then
                For Each dtFRow As DataRow In dtFinalTable.Rows
                    Select Case mintDistributionById
                        Case 1
                            dtTemp = dsCommList.Tables("DataTable").Select("departmentunkid = '" & dtFRow.Item("distribid") & "'")
                        Case 2
                            dtTemp = dsCommList.Tables("DataTable").Select("stationunkid = '" & dtFRow.Item("distribid") & "'")
                    End Select

                    If dtTemp.Length > 0 Then
                        dtFRow.Item("allstaff") = dtTemp(0)("Staff")
                        dtFinalTable.AcceptChanges()
                    End If

                Next
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetAllStaffValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Sub Set_StaffValue(ByVal intStaffTypeId As Integer, _
                               ByVal strDatabaseName As String, _
                               ByVal intUserUnkid As Integer, _
                               ByVal intYearUnkid As Integer, _
                               ByVal intCompanyUnkid As Integer, _
                               ByVal dtPeriodStart As Date, _
                               ByVal dtPeriodEnd As Date, _
                               ByVal strUserModeSetting As String, _
                               ByVal blnOnlyApproved As Boolean)
        'Private Sub Set_StaffValue(ByVal intStaffTypeId As Integer)

        Select Case mintViewTypeId
            Case 1
                dtPeriodStart = mdtAsOnDate
                dtPeriodEnd = mdtAsOnDate
            Case 2
                dtPeriodStart = mdtAsOnDate
                dtPeriodEnd = eZeeDate.convertDate(mStrEmplType_Date)
        End Select

        Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

        'S.SANDEEP [04 JUN 2015] -- END
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT "
            'Select Case mintDistributionById
            '    Case 1  'Department
            '        StrQ &= " departmentunkid "
            '    Case 2  'Branch
            '        StrQ &= " stationunkid "
            'End Select
            'StrQ &= ",COUNT(employeeunkid) AS Staff " & _
            '        "FROM hremployee_master "

            'Select Case mintViewTypeId
            '    Case 1 'Date
            '        Select Case intStaffTypeId
            '            Case 1  'New Staff
            '                StrQ &= "WHERE CONVERT(CHAR(8),appointeddate,112) = @Date "
            '            Case 2  'Expired Contracts
            '                StrQ &= "WHERE CONVERT(CHAR(8),empl_enddate,112) = @Date "
            '            Case 3  'Staff Exits
            '                StrQ &= " WHERE (CONVERT(CHAR(8),termination_from_date,112) = @Date OR CONVERT(CHAR(8),termination_to_date,112) = @Date ) " & _
            '                        " AND employeeunkid NOT IN (SELECT employeeunkid FROM hremployee_master WHERE CONVERT(CHAR(8),empl_enddate,112) = @Date)"
            '        End Select
            '    Case 2 'Month
            '        Select Case intStaffTypeId
            '            Case 1  'New Staff
            '                StrQ &= "WHERE DATEPART(mm,appointeddate) = @MonthId " & _
            '                        "	AND DATEPART(yy,appointeddate) = @YearId "
            '            Case 2  'Expired Contracts
            '                StrQ &= "WHERE DATEPART(mm,empl_enddate) = @MonthId " & _
            '                        "	AND DATEPART(yy,empl_enddate) = @YearId "
            '            Case 3  'Staff Exits
            '                'StrQ &= "WHERE DATEPART(mm,termination_from_date) = @MonthId " & _
            '                '        "	AND DATEPART(yy,termination_from_date) = @YearId "

            '                StrQ &= "WHERE (((DATEPART(mm, termination_from_date) = @MonthId AND DATEPART(yy,termination_from_date) = @YearId) AND employeeunkid NOT IN(SELECT employeeunkid FROM hremployee_master WHERE DATEPART(mm, empl_enddate) = @MonthId AND DATEPART(yy, empl_enddate) = @YearId)) " & _
            '                        " OR ((DATEPART(mm, termination_to_date) = @MonthId AND DATEPART(yy,termination_to_date) = @YearId) AND employeeunkid IN (SELECT employeeunkid FROM hremployee_master WHERE empl_enddate IS NULL AND termination_from_date IS NULL ))) "
            '        End Select

            'End Select



            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            ''If mblnIsActive = False Then
            ''    'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''    'ISSUE : TRA ENHANCEMENTS
            ''    'StrQ &= " AND hremployee_master.isactive = 1"
            ''    StrQ &= " AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) >= @startdate AND CONVERT(CHAR(8),appointeddate,112) <= @enddate "
            ''" AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            ''" AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            ''" AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "


            ''    'Anjan [ 03 Aug 2013 ] -- Start
            ''    'ENHANCEMENT : Recruitment TRA changes requested by Andrew

            ''    'Anjan [ 03 Aug 2013 ] -- End

            ''    'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            ''    'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            ''    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            ''    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            ''    'S.SANDEEP [ 12 MAY 2012 ] -- END
            ''End If

            ''Pinkal (24-Jun-2011) -- End

            ''Pinkal (12-Nov-2012) -- Start
            ''Enhancement : TRA Changes

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            ''Pinkal (12-Nov-2012) -- End

            'Select Case mintDistributionById
            '    Case 1  'Department
            '        StrQ &= "GROUP BY departmentunkid " & _
            '                "ORDER BY departmentunkid "
            '    Case 2  'Branch
            '        StrQ &= "GROUP BY stationunkid " & _
            '                "ORDER BY stationunkid "
            'End Select


            StrQ = "SELECT "
            Select Case mintDistributionById
                Case 1  'Department
                    StrQ &= " AL.departmentunkid "
                Case 2  'Branch
                    StrQ &= " AL.stationunkid "
            End Select
            StrQ &= ",COUNT(hremployee_master.employeeunkid) AS Staff " & _
                    "FROM hremployee_master " & _
                    "JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        departmentunkid " & _
                    "       ,stationunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ")AS AL ON AL.employeeunkid = hremployee_master.employeeunkid AND AL.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        date1 AS empl_enddate " & _
                    "       ,date2 AS termination_from_date " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "   FROM hremployee_dates_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   AND datetypeunkid = 4 " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        date1 AS termination_to_date " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "   FROM hremployee_dates_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' " & _
                    ") AS R ON R.employeeunkid = hremployee_master.employeeunkid "

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            Select Case mintViewTypeId
                Case 1 'Date
                    Select Case intStaffTypeId
                        Case 1  'New Staff
                            StrQ &= "WHERE CONVERT(CHAR(8),appointeddate,112) = @Date "
                        Case 2  'Expired Contracts
                            StrQ &= "WHERE CONVERT(CHAR(8),T.empl_enddate,112) = @Date "
                        Case 3  'Staff Exits
                            StrQ &= " WHERE (CONVERT(CHAR(8),T.termination_from_date,112) = @Date OR CONVERT(CHAR(8),R.termination_to_date,112) = @Date ) "
                    End Select
                Case 2 'Month
                    Select Case intStaffTypeId
                        Case 1  'New Staff
                            StrQ &= "WHERE DATEPART(mm,appointeddate) = @MonthId " & _
                                    "	AND DATEPART(yy,appointeddate) = @YearId "
                        Case 2  'Expired Contracts
                            StrQ &= "WHERE DATEPART(mm,T.empl_enddate) = @MonthId " & _
                                    "	AND DATEPART(yy,T.empl_enddate) = @YearId "
                        Case 3  'Staff Exits
                            'StrQ &= "WHERE DATEPART(mm,termination_from_date) = @MonthId " & _
                            '        "	AND DATEPART(yy,termination_from_date) = @YearId "

                            StrQ &= "WHERE (((DATEPART(mm, T.termination_from_date) = @MonthId AND DATEPART(yy,T.termination_from_date) = @YearId)) " & _
                                    " OR ((DATEPART(mm, R.termination_to_date) = @MonthId AND DATEPART(yy,R.termination_to_date) = @YearId))) "
                    End Select

            End Select


            Select Case mintDistributionById
                Case 1  'Department
                    StrQ &= "GROUP BY AL.departmentunkid " & _
                            "ORDER BY AL.departmentunkid "
                Case 2  'Branch
                    StrQ &= "GROUP BY AL.stationunkid " & _
                            "ORDER BY AL.stationunkid "
            End Select

            Call FilterTitleAndFilterQuery()

            dsCommList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Dim dtTemp() As DataRow = Nothing

            If dsCommList.Tables("DataTable").Rows.Count > 0 Then
                Select Case intStaffTypeId
                    Case 1  'New Staff
                        For Each dtFRow As DataRow In dtFinalTable.Rows
                            Select Case mintDistributionById
                                Case 1  'Department
                                    dtTemp = dsCommList.Tables("DataTable").Select("departmentunkid = '" & dtFRow.Item("distribid") & "'")
                                Case 2  'Branch
                                    dtTemp = dsCommList.Tables("DataTable").Select("stationunkid = '" & dtFRow.Item("distribid") & "'")
                            End Select

                            If dtTemp.Length > 0 Then
                                dtFRow.Item("newstaff") = dtTemp(0)("Staff")
                            End If

                        Next
                    Case 2  'Expired Contracts
                        For Each dtFRow As DataRow In dtFinalTable.Rows
                            Select Case mintDistributionById
                                Case 1  'Department
                                    dtTemp = dsCommList.Tables("DataTable").Select("departmentunkid = '" & dtFRow.Item("distribid") & "'")
                                Case 2  'Branch
                                    dtTemp = dsCommList.Tables("DataTable").Select("stationunkid = '" & dtFRow.Item("distribid") & "'")
                            End Select

                            If dtTemp.Length > 0 Then
                                dtFRow.Item("expired") = dtTemp(0)("Staff")
                            End If

                        Next
                    Case 3  'Staff Exits
                        For Each dtFRow As DataRow In dtFinalTable.Rows
                            Select Case mintDistributionById
                                Case 1  'Department
                                    dtTemp = dsCommList.Tables("DataTable").Select("departmentunkid = '" & dtFRow.Item("distribid") & "'")
                                Case 2  'Branch
                                    dtTemp = dsCommList.Tables("DataTable").Select("stationunkid = '" & dtFRow.Item("distribid") & "'")
                            End Select

                            If dtTemp.Length > 0 Then
                                dtFRow.Item("staffexits") = dtTemp(0)("Staff")
                            End If

                        Next
                End Select
                dtFinalTable.AcceptChanges()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Set_StaffValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Sub SetEmploymentType(ByVal strDatabaseName As String, _
                                  ByVal intUserUnkid As Integer, _
                                  ByVal intYearUnkid As Integer, _
                                  ByVal intCompanyUnkid As Integer, _
                                  ByVal dtPeriodStart As Date, _
                                  ByVal dtPeriodEnd As Date, _
                                  ByVal strUserModeSetting As String, _
                                  ByVal blnOnlyApproved As Boolean)
        'Private Sub SetEmploymentType()

        Select Case mintViewTypeId
            Case 1
                dtPeriodStart = mdtAsOnDate
                dtPeriodEnd = mdtAsOnDate
            Case 2
                dtPeriodStart = mdtAsOnDate
                dtPeriodEnd = eZeeDate.convertDate(mStrEmplType_Date)
        End Select

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
        If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT "
            'Select Case mintDistributionById
            '    Case 1  'Department
            '        StrQ &= "  departmentunkid "
            '    Case 2  'Branch
            '        StrQ &= "  stationunkid "
            'End Select
            'StrQ &= ",COUNT(employmenttypeunkid) EmplType " & _
            '        ",employmenttypeunkid " & _
            '        " FROM hremployee_master " & _
            '        " JOIN cfcommon_master ON cfcommon_master.masterunkid=hremployee_master.employmenttypeunkid and cfcommon_master.isactive = 1 and cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE


            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'Select Case mintViewTypeId
            '    Case 1 ' Date
            '        StrQ &= " WHERE CONVERT(CHAR(8),appointeddate,112) <= @Date "
            '    Case 2 ' Month
            '        StrQ &= "WHERE CONVERT(CHAR(8),appointeddate,112) <= '" & mStrEmplType_Date & "' "
            'End Select
            ''S.SANDEEP [ 12 MAY 2012 ] -- END


            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            'If mblnIsActive = False Then
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ISSUE : TRA ENHANCEMENTS
            '    'StrQ &= "WHERE hremployee_master.isactive = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "



            '    'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    Select Case mintViewTypeId
            '        Case 1
            '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            '        Case 2
            '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mStrEmplType_Date)
            '    End Select
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            'End If
            ''Pinkal (24-Jun-2011) -- End

            ''Pinkal (12-Nov-2012) -- Start
            ''Enhancement : TRA Changes

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            ''Pinkal (12-Nov-2012) -- End


            'Select Case mintDistributionById
            '    Case 1  'Department
            '        StrQ &= "GROUP BY departmentunkid,employmenttypeunkid " & _
            '                "ORDER BY departmentunkid "
            '    Case 2  'Branch
            '        StrQ &= "GROUP BY stationunkid,employmenttypeunkid " & _
            '                "ORDER BY stationunkid "
            'End Select


            StrQ = "SELECT "
            Select Case mintDistributionById
                Case 1  'Department
                    StrQ &= "  AL.departmentunkid "
                Case 2  'Branch
                    StrQ &= "  AL.stationunkid "
            End Select
            StrQ &= ",COUNT(employmenttypeunkid) EmplType " & _
                    ",employmenttypeunkid " & _
                    " FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid=hremployee_master.employmenttypeunkid and cfcommon_master.isactive = 1 and cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & _
                    "JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        employeeunkid " & _
                    "       ,stationunkid " & _
                    "       ,departmentunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS AL ON AL.employeeunkid = hremployee_master.employeeunkid AND AL.rno = 1 "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            Select Case mintViewTypeId
                Case 1 ' Date
                    StrQ &= " WHERE CONVERT(CHAR(8),appointeddate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "
                Case 2 ' Month
                    StrQ &= " WHERE CONVERT(CHAR(8),appointeddate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "
            End Select

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            Select Case mintDistributionById
                Case 1  'Department
                    StrQ &= "GROUP BY AL.departmentunkid,employmenttypeunkid " & _
                            "ORDER BY AL.departmentunkid "
                Case 2  'Branch
                    StrQ &= "GROUP BY AL.stationunkid,employmenttypeunkid " & _
                            "ORDER BY AL.stationunkid "
            End Select

            'S.SANDEEP [04 JUN 2015] -- END

            Call FilterTitleAndFilterQuery()

            dsCommList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTemp() As DataRow = Nothing

            If dsCommList.Tables("DataTable").Rows.Count > 0 Then
                For Each dtFRow As DataRow In dtFinalTable.Rows
                    Select Case mintDistributionById
                        Case 1
                            dtTemp = dsCommList.Tables("DataTable").Select("departmentunkid = '" & dtFRow.Item("distribid") & "'")
                        Case 2
                            dtTemp = dsCommList.Tables("DataTable").Select("stationunkid = '" & dtFRow.Item("distribid") & "'")
                    End Select

                    If dtTemp.Length > 0 Then
                        For i As Integer = 0 To dtTemp.Length - 1
                            dtFRow.Item("Column" & dtTemp(i)("employmenttypeunkid")) += dtTemp(i)("EmplType")
                        Next
                        dtFinalTable.AcceptChanges()
                    End If
                Next
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetEmploymentType; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Sub SetMaleFemaleValue(ByVal strDatabaseName As String, _
                                   ByVal intUserUnkid As Integer, _
                                   ByVal intYearUnkid As Integer, _
                                   ByVal intCompanyUnkid As Integer, _
                                   ByVal dtPeriodStart As Date, _
                                   ByVal dtPeriodEnd As Date, _
                                   ByVal strUserModeSetting As String, _
                                   ByVal blnOnlyApproved As Boolean)
        'Private Sub SetMaleFemaleValue()

        Select Case mintViewTypeId
            Case 1
                dtPeriodStart = mdtAsOnDate
                dtPeriodEnd = mdtAsOnDate
            Case 2
                dtPeriodStart = mdtAsOnDate
                dtPeriodEnd = eZeeDate.convertDate(mStrEmplType_Date)
        End Select

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
        If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT "
            'Select Case mintDistributionById
            '    Case 1  'Department
            '        StrQ &= "  departmentunkid "
            '    Case 2  'Branch
            '        StrQ &= "  stationunkid "
            'End Select
            'StrQ &= " ,SUM(CASE WHEN gender = 1 THEN 1 ELSE 0 END) AS Male " & _
            '        " ,SUM(CASE WHEN gender = 2 THEN 1 ELSE 0 END) AS Female " & _
            '        " ,SUM(CASE WHEN gender <= 0 THEN 1 ELSE 0 END) AS NA " & _
            '        "FROM hremployee_master "

            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'Select Case mintViewTypeId
            '    Case 1 ' Date
            '        StrQ &= " WHERE CONVERT(CHAR(8),appointeddate,112) <= @Date "
            '    Case 2 ' Month
            '        StrQ &= " WHERE CONVERT(CHAR(8),appointeddate,112) <= '" & mStrEmplType_Date & "' "
            'End Select
            ''S.SANDEEP [ 12 MAY 2012 ] -- END

            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'If mblnIsActive = False Then
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ISSUE : TRA ENHANCEMENTS
            '    'StrQ &= " WHERE hremployee_master.isactive = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "

            '    'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

            '    Select Case mintViewTypeId
            '        Case 1
            '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            '        Case 2
            '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mStrEmplType_Date)
            '    End Select

            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            'End If
            ''Pinkal (24-Jun-2011) -- End

            ''Pinkal (12-Nov-2012) -- Start
            ''Enhancement : TRA Changes

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            ''Pinkal (12-Nov-2012) -- End

            'Select Case mintDistributionById
            '    Case 1  'Department
            '        StrQ &= " GROUP BY departmentunkid "
            '    Case 2  'Branch
            '        StrQ &= " GROUP BY stationunkid "
            'End Select


            StrQ = "SELECT "
            Select Case mintDistributionById
                Case 1  'Department
                    StrQ &= "  AL.departmentunkid "
                Case 2  'Branch
                    StrQ &= "  AL.stationunkid "
            End Select
            StrQ &= " ,SUM(CASE WHEN gender = 1 THEN 1 ELSE 0 END) AS Male " & _
                    " ,SUM(CASE WHEN gender = 2 THEN 1 ELSE 0 END) AS Female " & _
                    " ,SUM(CASE WHEN gender <= 0 THEN 1 ELSE 0 END) AS NA " & _
                    "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        employeeunkid " & _
                    "       ,stationunkid " & _
                    "       ,departmentunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS AL ON AL.employeeunkid = hremployee_master.employeeunkid AND AL.rno = 1 "


            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            Select Case mintViewTypeId
                Case 1 ' Date
                    StrQ &= " WHERE CONVERT(CHAR(8),appointeddate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "
                Case 2 ' Month
                    StrQ &= " WHERE CONVERT(CHAR(8),appointeddate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "
            End Select

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            Select Case mintDistributionById
                Case 1  'Department
                    StrQ &= " GROUP BY AL.departmentunkid "
                Case 2  'Branch
                    StrQ &= " GROUP BY AL.stationunkid "
            End Select

            'S.SANDEEP [04 JUN 2015] -- END

            Call FilterTitleAndFilterQuery()

            dsCommList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTemp() As DataRow = Nothing

            If dsCommList.Tables("DataTable").Rows.Count > 0 Then
                For Each dtFRow As DataRow In dtFinalTable.Rows
                    Select Case mintDistributionById
                        Case 1
                            dtTemp = dsCommList.Tables("DataTable").Select("departmentunkid = '" & dtFRow.Item("distribid") & "'")
                        Case 2
                            dtTemp = dsCommList.Tables("DataTable").Select("stationunkid = '" & dtFRow.Item("distribid") & "'")
                    End Select

                    If dtTemp.Length > 0 Then
                        dtFRow.Item("male") = dtTemp(0)("Male") 'Male Count
                        dtFRow.Item("female") = dtTemp(0)("Female") 'Female Count
                        dtFRow.Item("NA") = dtTemp(0)("NA") 'NA Count
                        dtFinalTable.AcceptChanges()
                    End If

                Next
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetMaleFemaleValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub SetTransferIn_OutValue()
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try

            dtTempTable = New DataTable("InOut")
            dtTempTable.Columns.Add("deptId", System.Type.GetType("System.Int32"))
            dtTempTable.Columns.Add("In", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTempTable.Columns.Add("Out", System.Type.GetType("System.Int32")).DefaultValue = 0

            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                       "   DISTINCT "
            Select Case mintDistributionById
                Case 1 'Department
                    StrQ &= " departmentunkid "
                Case 2 'Branch
                    StrQ &= " stationunkid "
            End Select
            StrQ &= " ,employeeunkid " & _
                           " ,audittype " & _
                           "FROM athremployee_master " & _
                           "WHERE employeeunkid = @EmpId 	AND audittype = 1 "
            Select Case mintViewTypeId
                Case 1 'Date
                    StrQ &= "	AND CONVERT(CHAR(8),auditdatetime,112)= @Date "
                Case 2 'Month
                    StrQ &= "   AND DATEPART(mm,auditdatetime) = @MonthId " & _
                                   "	AND DATEPART(yy,appointeddate) = @YearId "
            End Select



            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            If mblnIsActive = False Then
                'S.SANDEEP [ 12 MAY 2012 ] -- START
                'ISSUE : TRA ENHANCEMENTS
                'StrQ &= " AND athremployee_master.isactive = 1 "
                StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
                        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
                        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
                        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
                'S.SANDEEP [ 12 MAY 2012 ] -- END
            End If
            'Pinkal (24-Jun-2011) -- End


            'Pinkal (12-Nov-2012) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "athremployee_master")
            'End If

            If mstrUserAccessFilter = "" Then
                If UserAccessLevel._AccessLevel.Length > 0 Then
                    StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "athremployee_master")
                End If
            Else
                StrQ &= mstrUserAccessFilter.Replace("hremployee_master", "athremployee_master")
            End If
            'Pinkal (12-Nov-2012) -- End



            StrQ &= " UNION " & _
                            "SELECT " & _
                            "   DISTINCT "
            Select Case mintDistributionById
                Case 1 'Department
                    StrQ &= " departmentunkid "
                Case 2 'Branch
                    StrQ &= " stationunkid "
            End Select
            StrQ &= " ,employeeunkid " & _
                           " ,audittype " & _
                           "FROM athremployee_master " & _
                           "WHERE employeeunkid = @EmpId 	AND audittype = 2 "
            Select Case mintViewTypeId
                Case 1 'Date
                    StrQ &= "	AND CONVERT(CHAR(8),auditdatetime,112)= @Date "
                Case 2 'Month
                    StrQ &= "   AND DATEPART(mm,auditdatetime) = @MonthId " & _
                                   "	AND DATEPART(yy,appointeddate) = @YearId "
            End Select

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            If mblnIsActive = False Then
                'S.SANDEEP [ 12 MAY 2012 ] -- START
                'ISSUE : TRA ENHANCEMENTS
                'StrQ &= " AND athremployee_master.isactive = 1 "
                StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
                        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
                        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
                        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
                'S.SANDEEP [ 12 MAY 2012 ] -- END
            End If
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES


            'Pinkal (12-Nov-2012) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "athremployee_master")
            'End If

            If mstrUserAccessFilter = "" Then
                If UserAccessLevel._AccessLevel.Length > 0 Then
                    StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "athremployee_master")
                End If
            Else
                StrQ &= mstrUserAccessFilter.Replace("hremployee_master", "athremployee_master")
            End If
            'Pinkal (12-Nov-2012) -- End






            'S.SANDEEP [ 12 MAY 2012 ] -- END

            Dim objEmp As New clsEmployee_Master
            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsEmpList = objEmp.GetEmployeeList("List", False, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If mblnIsActive Then
            '    dsEmpList = objEmp.GetEmployeeList("List", False, True)
            'Else
            '    dsEmpList = objEmp.GetEmployeeList("List", False, False)
            'End If

            dsEmpList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, mblnIsActive, "List", False)

            'S.SANDEEP [04 JUN 2015] -- END

            'Pinkal (24-Jun-2011) -- End


            If dsEmpList.Tables("List").Rows.Count > 0 Then
                For Each dERow As DataRow In dsEmpList.Tables("List").Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, dERow.Item("employeeunkid"))

                    'S.SANDEEP [ 12 MAY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If mblnIsActive = False Then
                        'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                        'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
                        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
                    End If
                    'S.SANDEEP [ 12 MAY 2012 ] -- END

                    Call FilterTitleAndFilterQuery()

                    dsCommList = objDataOperation.ExecQuery(StrQ, "DataTable")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    Dim intCnt As Integer = dsCommList.Tables("DataTable").Rows.Count
                    Dim dtRow As DataRow
                    Dim intOldVal As Integer = -1
                    Dim dtTemp() As DataRow = Nothing
                    If dsCommList.Tables("DataTable").Rows.Count > 1 Then
                        For i As Integer = 0 To intCnt - 1
                            If dsCommList.Tables(0).Rows(i)("audittype") = 1 Then
                                Select Case mintDistributionById
                                    Case 1 'Department
                                        intOldVal = dsCommList.Tables(0).Rows(i)("departmentunkid")
                                    Case 2 'Branch
                                        intOldVal = dsCommList.Tables(0).Rows(i)("stationunkid")
                                End Select
                                dtTemp = dtTempTable.Select("deptId ='" & intOldVal & "'")
                                If dtTemp.Length > 0 Then
                                    Dim intIdx As Integer = dtTempTable.Rows.IndexOf(dtTemp(0))
                                    dtTempTable.Rows(intIdx).Item("deptId") = dtTemp(0)("deptId")
                                    dtTempTable.Rows(intIdx).Item("In") = dtTemp(0)("In")
                                    dtTempTable.Rows(intIdx).Item("Out") = dtTemp(0)("Out")
                                    dtTempTable.AcceptChanges()
                                Else
                                    dtRow = dtTempTable.NewRow
                                    Select Case mintDistributionById
                                        Case 1 'Department
                                            dtRow.Item("deptId") = dsCommList.Tables(0).Rows(i)("departmentunkid")
                                        Case 2 'Branch
                                            dtRow.Item("deptId") = dsCommList.Tables(0).Rows(i)("stationunkid")
                                    End Select
                                    dtRow.Item("In") = 0
                                    dtRow.Item("Out") = 0
                                    dtTempTable.Rows.Add(dtRow)
                                End If
                            ElseIf dsCommList.Tables(0).Rows(i)("audittype") = 2 Then
                                Select Case mintDistributionById
                                    Case 1 'Department
                                        If intOldVal <> dsCommList.Tables(0).Rows(i)("departmentunkid") Then

                                            dtTemp = dtTempTable.Select("deptId ='" & intOldVal & "'")
                                            If dtTemp.Length > 0 Then
                                                Dim intIdx As Integer = dtTempTable.Rows.IndexOf(dtTemp(0))
                                                dtTempTable.Rows(intIdx).Item("Out") += 1
                                            End If
                                            Select Case mintDistributionById
                                                Case 1 'Department
                                                    intOldVal = dsCommList.Tables(0).Rows(i)("departmentunkid")
                                                Case 2 'Branch
                                                    intOldVal = dsCommList.Tables(0).Rows(i)("stationunkid")
                                            End Select
                                            dtTemp = dtTempTable.Select("deptId ='" & intOldVal & "'")
                                            If dtTemp.Length > 0 Then
                                                Dim intIdx As Integer = dtTempTable.Rows.IndexOf(dtTemp(0))
                                                dtTempTable.Rows(intIdx).Item("deptId") = dtTemp(0)("deptId")
                                                dtTempTable.Rows(intIdx).Item("In") = +1
                                                dtTempTable.Rows(intIdx).Item("Out") = dtTemp(0)("Out")
                                                dtTempTable.AcceptChanges()
                                            Else
                                                dtRow = dtTempTable.NewRow
                                                Select Case mintDistributionById
                                                    Case 1 'Department
                                                        dtRow.Item("deptId") = dsCommList.Tables(0).Rows(i)("departmentunkid")
                                                    Case 2 'Branch
                                                        dtRow.Item("deptId") = dsCommList.Tables(0).Rows(i)("stationunkid")
                                                End Select
                                                dtRow.Item("In") = +1
                                                dtRow.Item("Out") = 0
                                                dtTempTable.Rows.Add(dtRow)
                                            End If
                                        End If
                                    Case 2 'Branch
                                        If intOldVal <> dsCommList.Tables(0).Rows(i)("stationunkid") Then

                                            dtTemp = dtTempTable.Select("deptId ='" & intOldVal & "'")
                                            If dtTemp.Length > 0 Then
                                                Dim intIdx As Integer = dtTempTable.Rows.IndexOf(dtTemp(0))
                                                dtTempTable.Rows(intIdx).Item("Out") += 1
                                            End If
                                            intOldVal = dsCommList.Tables(0).Rows(i)("departmentunkid")
                                            dtTemp = dtTempTable.Select("deptId ='" & intOldVal & "'")
                                            If dtTemp.Length > 0 Then
                                                Dim intIdx As Integer = dtTempTable.Rows.IndexOf(dtTemp(0))
                                                dtTempTable.Rows(intIdx).Item("deptId") = dtTemp(0)("deptId")
                                                dtTempTable.Rows(intIdx).Item("In") = +1
                                                dtTempTable.Rows(intIdx).Item("Out") = dtTemp(0)("Out")
                                                dtTempTable.AcceptChanges()
                                            Else
                                                dtRow = dtTempTable.NewRow
                                                dtRow.Item("deptId") = dsCommList.Tables(0).Rows(i)("stationunkid")
                                                dtRow.Item("In") = +1
                                                dtRow.Item("Out") = 0
                                                dtTempTable.Rows.Add(dtRow)
                                            End If
                                        End If
                                End Select
                            End If
                        Next
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetTransferIn_OutValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub MakeMeargeIn_Out()
        Try
            'Call SetTransferIn_OutValue()
            Dim dtTemp() As DataRow = Nothing

            For Each dtFRow As DataRow In dtFinalTable.Rows
                'dtTemp = dtTempTable.Select("deptId = '" & dtFRow.Item("distribid") & "'")
                'If dtTemp.Length > 0 Then
                '    dtFRow.Item("transferin") = dtTemp(0)("In")
                '    dtFRow.Item("transferout") = dtTemp(0)("Out")
                'End If
                'dtFRow.Item("attotal") = (dtFRow.Item("allstaff") + dtFRow.Item("newstaff") + dtFRow.Item("transferin")) - dtFRow.Item("transferout")
                dtFRow.Item("attotal") = (dtFRow.Item("allstaff") + dtFRow.Item("newstaff"))
                dtFinalTable.AcceptChanges()
            Next

            dtFinalTable.Columns.Remove("distribid")

            Dim dblTotal As Decimal = 0
            Dim m As Integer = 2
            Dim n As Integer = 2

            dblColTot = New Decimal(dtFinalTable.Columns.Count) {}

            While m < dtFinalTable.Columns.Count
                For t As Integer = 0 To dtFinalTable.Rows.Count - 1
                    dblTotal += CDec(dtFinalTable.Rows(t)(m))
                Next
                dblColTot(n) = Format(CDec(dblTotal), GUI.fmtCurrency)
                dblTotal = 0
                m += 1
                n += 1
            End While


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: MakeMeargeIn_Out; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function GetDistributionBy(Optional ByVal strList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 1 AS Id,@Departmentwise As Name UNION " & _
                   "SELECT 2 AS Id,@Branchwise AS Name "

            objDataOperation.AddParameter("@Departmentwise", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Department Wise"))
            objDataOperation.AddParameter("@Branchwise", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Branch Wise"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDistributionBy; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            dsList.Dispose()
            StrQ = String.Empty
        End Try
    End Function

    Public Function GetYearList(Optional ByVal strList As String = "List", Optional ByVal intCompanyId As Integer = -1, Optional ByVal intCompanyYearId As Integer = -1) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   "  YEAR(start_date) AS Years " & _
                   "FROM hrmsConfiguration..cffinancial_year_tran " & _
                   "WHERE companyunkid = @CompanyId AND yearunkid = @YearUnkid " & _
                   "UNION " & _
                   "SELECT " & _
                   "  YEAR(end_date) AS Years " & _
                   "FROM hrmsConfiguration..cffinancial_year_tran " & _
                   "WHERE companyunkid = @CompanyId AND yearunkid = @YearUnkid "

            If intCompanyId > 0 Then
                objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            Else
                objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, Company._Object._Companyunkid)
            End If

            If intCompanyYearId > 0 Then
                objDataOperation.AddParameter("@YearUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyYearId)
            Else
                objDataOperation.AddParameter("@YearUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._YearUnkid)
            End If


            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetYearList; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function GetMonthList(Optional ByVal strList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dtMTable As New DataTable(strList)
        Dim dsList As New DataSet
        Try

            dtMTable.Columns.Add("id", System.Type.GetType("System.Int32"))
            dtMTable.Columns.Add("name", System.Type.GetType("System.String"))
            Dim dMRow As DataRow = Nothing

            For i As Integer = 0 To 12
                dMRow = dtMTable.NewRow

                If i = 0 Then
                    dMRow.Item("id") = 0
                    dMRow.Item("name") = Language.getMessage(mstrModuleName, 13, "Select")
                Else
                    dMRow.Item("id") = i
                    dMRow.Item("name") = MonthName(i)
                End If

                dtMTable.Rows.Add(dMRow)
            Next

            dsList.Tables.Add(dtMTable)

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMonthList; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try

            'HEADER PART
            strBuilder.Append(" <TITLE>" & Me._ReportName & " [ " & mstrDistributionName & " ]" & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If

                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 21, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='60%' colspan=15 align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 14, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='60%' colspan=15  align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & " [ " & mstrDistributionName & " ]" & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            'Report Column Caption
            For j As Integer = 0 To objDataReader.Columns.Count - 1
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next

            strBuilder.Append(" </TR> " & vbCrLf)

            'Data Part
            For i As Integer = 0 To objDataReader.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1

                    'Pinkal (24-Aug-2012) -- Start
                    'Enhancement : TRA Changes

                    If k <= 1 Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    Else
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If


                    'Pinkal (24-Aug-2012) -- End
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" <TR> " & vbCrLf)
            For k As Integer = 0 To objDataReader.Columns.Count - 1
                If k = 0 Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 23, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
                ElseIf k <= 1 Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & "" & "</B></FONT></TD>" & vbCrLf)
                Else
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Format(CDec(dblColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                End If
            Next
            strBuilder.Append(" </TR>  " & vbCrLf)

            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)


            'Sandeep [ 09 MARCH 2011 ] -- Start
            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If
            'Sandeep [ 09 MARCH 2011 ] -- End 

            'Anjan (14 Apr 2011)-Start
            'Issue : To remove "\" from path which comes at end in path.
            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If
            'Anjan (14 Apr 2011)-End


            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then

                'Sandeep [ 09 MARCH 2011 ] -- Start
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                'Sandeep [ 09 MARCH 2011 ] -- End 

                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Sub Export_Employee_Distribution(ByVal strDatabaseName As String, _
                                            ByVal intUserUnkid As Integer, _
                                            ByVal intYearUnkid As Integer, _
                                            ByVal intCompanyUnkid As Integer, _
                                            ByVal dtPeriodStart As Date, _
                                            ByVal dtPeriodEnd As Date, _
                                            ByVal strUserModeSetting As String, _
                                            ByVal blnOnlyApproved As Boolean, _
                                            ByVal isWeb As Boolean)
        'Public Sub Export_Employee_Distribution(Optional ByVal isWeb As Boolean = False)
        'S.SANDEEP [04 JUN 2015] -- END

        Try
            Call CreateTable()                                  '-------------- GENERATING TABLE STRUCTURE
            Call SetDepartmentValue(mintDistributionById)       '-------------- SETTING DEPARTMENT TO TABLE
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Call SetMaleFemaleValue()                           '-------------- SETTING MALE & FEMALE COUNT TO TABLE
            'Call SetEmploymentType()                            '-------------- SETTING EMPLOYMENT TYPE TO TABLE
            'Call Set_StaffValue(1)                              '-------------- SETTING NEW STAFF TO TABLE
            'Call Set_StaffValue(2)                              '-------------- SETTING EXPIRED CONTRACTS TO TABLE
            'Call Set_StaffValue(3)                              '-------------- SETTING STAFF EXITS TO TABLE
            'Call SetAllStaffValue()                             '-------------- SETTING ALL STAFF VALUE TO TABLE
            Call SetMaleFemaleValue(strDatabaseName, _
                                    intUserUnkid, _
                                    intYearUnkid, _
                                    intCompanyUnkid, _
                                    dtPeriodStart, _
                                    dtPeriodEnd, _
                                    strUserModeSetting, _
                                    blnOnlyApproved)             '-------------- SETTING MALE & FEMALE COUNT TO TABLE            
            Call SetEmploymentType(strDatabaseName, _
                                    intUserUnkid, _
                                    intYearUnkid, _
                                    intCompanyUnkid, _
                                    dtPeriodStart, _
                                    dtPeriodEnd, _
                                    strUserModeSetting, _
                                    blnOnlyApproved)            '-------------- SETTING EMPLOYMENT TYPE TO TABLE
            'S.SANDEEP [04 JUN 2015] -- END

            Call Set_StaffValue(1, _
                                strDatabaseName, _
                                intUserUnkid, _
                                intYearUnkid, _
                                intCompanyUnkid, _
                                dtPeriodStart, _
                                dtPeriodEnd, _
                                strUserModeSetting, _
                                blnOnlyApproved)                '-------------- SETTING NEW STAFF TO TABLE
            Call Set_StaffValue(2, _
                                strDatabaseName, _
                                intUserUnkid, _
                                intYearUnkid, _
                                intCompanyUnkid, _
                                dtPeriodStart, _
                                dtPeriodEnd, _
                                strUserModeSetting, _
                                blnOnlyApproved)                '-------------- SETTING EXPIRED CONTRACTS TO TABLE
            Call Set_StaffValue(3, _
                                strDatabaseName, _
                                intUserUnkid, _
                                intYearUnkid, _
                                intCompanyUnkid, _
                                dtPeriodStart, _
                                dtPeriodEnd, _
                                strUserModeSetting, _
                                blnOnlyApproved)                '-------------- SETTING STAFF EXITS TO TABLE

            Call SetAllStaffValue(strDatabaseName, _
                                  intUserUnkid, _
                                  intYearUnkid, _
                                  intCompanyUnkid, _
                                  dtPeriodStart, _
                                  dtPeriodEnd, _
                                  strUserModeSetting, _
                                  blnOnlyApproved)              '-------------- SETTING ALL STAFF VALUE TO TABLE
            Call MakeMeargeIn_Out()                             '-------------- SETTING TRANSFERS IN/OUT OF EMPLOYEE 


            If isWeb = False Then

                '------------- EXPORTING TABLE TO EXCEL SHEET
                If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtFinalTable) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
                    'Sandeep [ 09 MARCH 2011 ] -- Start
                    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
                    'Sandeep [ 09 MARCH 2011 ] -- End 
                End If

            Else

                dtFinal = dtFinalTable
                dclColTot = dblColTot

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Employee_Distribution; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "No.")
            Language.setMessage(mstrModuleName, 2, "Department Name")
            Language.setMessage(mstrModuleName, 3, "Branch Name")
            Language.setMessage(mstrModuleName, 4, "Staff C/F")
            Language.setMessage(mstrModuleName, 5, "New Staff")
            Language.setMessage(mstrModuleName, 8, "Expired Contracts")
            Language.setMessage(mstrModuleName, 9, "Staff Exits")
            Language.setMessage(mstrModuleName, 10, "Female")
            Language.setMessage(mstrModuleName, 11, "Department Wise")
            Language.setMessage(mstrModuleName, 12, "Branch Wise")
            Language.setMessage(mstrModuleName, 13, "Select")
            Language.setMessage(mstrModuleName, 14, "Date :")
            Language.setMessage(mstrModuleName, 15, "Report successfully exported to Report export path")
            Language.setMessage(mstrModuleName, 16, "As On Date :")
            Language.setMessage(mstrModuleName, 17, "Month :")
            Language.setMessage(mstrModuleName, 18, "And")
            Language.setMessage(mstrModuleName, 19, "Year :")
            Language.setMessage(mstrModuleName, 20, "Male")
            Language.setMessage(mstrModuleName, 21, "Prepared By :")
            Language.setMessage(mstrModuleName, 22, "Total Staff")
            Language.setMessage(mstrModuleName, 23, "Grand Total :")
            Language.setMessage(mstrModuleName, 110, "N/A")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
