'************************************************************************************************************************************
'Class Name :clsDiscipline_Status_Summary_Report.vb
'Purpose    :
'Date       :09 -Apr -2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsDiscipline_Status_Summary_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsDiscipline_Status_Summary_Report"
    Private mstrReportId As String = enArutiReport.Discipline_Status_Summary_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintDisciplineStatusId As Integer = 0
    Private mstrDisciplineStatus As String = String.Empty
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1

#End Region

#Region " Properties "

    Public WriteOnly Property _DisciplineStatusId() As Integer
        Set(ByVal value As Integer)
            mintDisciplineStatusId = value
        End Set
    End Property

    Public WriteOnly Property _DisciplineStatus() As String
        Set(ByVal value As String)
            mstrDisciplineStatus = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintDisciplineStatusId = 0
            mstrDisciplineStatus = ""
            Rpt = Nothing
            mintUserUnkid = -1
            mintCompanyUnkid = -1
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintDisciplineStatusId > 0 Then
                Me._FilterQuery = " AND hrdisciplinestatus_master.disciplinestatusunkid = " & mintDisciplineStatusId
                Me._FilterTitle = Language.getMessage(mstrModuleName, 1, "Discipline Status :") & " " & mstrDisciplineStatus & " "
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport()

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [ 01 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT " & _
            '            "name " & _
            '            ",COUNT(disciplinefileunkid) AS TotalCases " & _
            '            ",statuslevel " & _
            '        "FROM hrdisciplinestatus_master " & _
            '        "JOIN " & _
            '        "( " & _
            '            "SELECT " & _
            '                "disciplinefileunkid " & _
            '                ",disciplinestatusunkid " & _
            '                ",ROW_NUMBER() OVER(PARTITION BY disciplinefileunkid ORDER BY statusdate DESC) AS rno " & _
            '            "FROM hrdiscipline_status_tran " & _
            '        ")AS DIS ON dbo.hrdisciplinestatus_master.disciplinestatusunkid = DIS.disciplinestatusunkid " & _
            '        "WHERE isactive = 1 AND rno = 1 "

            'S.SANDEEP [25 JUL 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            'StrQ = "SELECT " & _
            '           "name " & _
            '           ",COUNT(disciplinefileunkid) AS TotalCases " & _
            '           ",statuslevel " & _
            '       "FROM hrdisciplinestatus_master " & _
            '       "JOIN " & _
            '       "( " & _
            '           "SELECT " & _
            '               " hrdiscipline_status_tran.disciplinefileunkid " & _
            '               ",hrdiscipline_status_tran.disciplinestatusunkid " & _
            '               ",ROW_NUMBER() OVER(PARTITION BY involved_employeeunkid ORDER BY statusdate DESC) AS rno " & _
            '           "FROM hrdiscipline_status_tran " & _
            '           " JOIN hrdiscipline_file ON hrdiscipline_status_tran.disciplinefileunkid = hrdiscipline_file.disciplinefileunkid " & _
            '       ")AS DIS ON dbo.hrdisciplinestatus_master.disciplinestatusunkid = DIS.disciplinestatusunkid " & _
            '       "WHERE isactive = 1 AND rno = 1 "

            StrQ = "SELECT " & _
                        "name " & _
                        ",COUNT(disciplinefileunkid) AS TotalCases " & _
                        ",statuslevel " & _
                    "FROM hrdisciplinestatus_master " & _
                    "JOIN " & _
                    "( " & _
                        "SELECT " & _
                            " hrdiscipline_status_tran.disciplinefileunkid " & _
                            ",hrdiscipline_status_tran.disciplinestatusunkid " & _
                            ",ROW_NUMBER() OVER(PARTITION BY involved_employeeunkid ORDER BY statusdate DESC) AS rno " & _
                        "FROM hrdiscipline_status_tran " & _
                   "        JOIN hrdiscipline_file_master ON hrdiscipline_status_tran.disciplinefileunkid = hrdiscipline_file_master.disciplinefileunkid " & _
                    ")AS DIS ON dbo.hrdisciplinestatus_master.disciplinestatusunkid = DIS.disciplinestatusunkid " & _
                    "WHERE isactive = 1 AND rno = 1 "
            'S.SANDEEP [25 JUL 2016] -- START



            'S.SANDEEP [ 01 FEB 2013 ] -- END
            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            StrQ &= "GROUP BY name,statuslevel " & _
                    "ORDER BY statuslevel ASC "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim iCnt As Integer = 1
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = iCnt.ToString
                rpt_Row.Item("Column2") = dtRow.Item("name")
                rpt_Row.Item("Column3") = dtRow.Item("TotalCases")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

                iCnt += 1
            Next

            objRpt = New ArutiReport.Designer.rptDiscipline_Status_Summary

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                         ConfigParameter._Object._GetLeftMargin, _
                                         ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 5, "SN."))
            Call ReportFunction.TextChange(objRpt, "txtDisciplineStatus", Language.getMessage(mstrModuleName, 6, "STATUS"))
            Call ReportFunction.TextChange(objRpt, "txtTotalCases", Language.getMessage(mstrModuleName, 7, "NO OF CASE"))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 8, "TOTAL"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 9, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 10, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 11, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Discipline Status :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By :")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "SN.")
            Language.setMessage(mstrModuleName, 6, "STATUS")
            Language.setMessage(mstrModuleName, 7, "NO OF CASE")
            Language.setMessage(mstrModuleName, 8, "TOTAL")
            Language.setMessage(mstrModuleName, 9, "Printed By :")
            Language.setMessage(mstrModuleName, 10, "Printed Date :")
            Language.setMessage(mstrModuleName, 11, "Page :")
            Language.setMessage(mstrModuleName, 12, "Prepared By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
