'************************************************************************************************************************************
'Class Name : clsEmployeeRelationReport.vb
'Purpose    :
'Date       :14/02/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsEmployeeRelationReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeRelationReport"
    Private mstrReportId As String = enArutiReport.EmployeeRelationReport   '34
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mstrFirstName As String = String.Empty
    Private mstrLastName As String = String.Empty
    Private mintRelationId As Integer = 0
    Private mstrRelationName As String = String.Empty
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty

    'Sandeep [ 12 MARCH 2011 ] -- Start
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'Sandeep [ 12 MARCH 2011 ] -- End 
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (16 May 2012) -- End

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End

    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrUserAccessFilter As String = String.Empty
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'S.SANDEEP [ 06 JUN 2014 ] -- START
    Private mblnShowDepedantsWithoutImage As Boolean = False
    Private mblnIsImageInDatabase As Boolean = False
    Private mstrShowDepedantsWithoutImageText As String = ""
    'S.SANDEEP [ 06 JUN 2014 ] -- END


    'Pinkal (16-AUG-2014) -- Start
    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
    Private mdtAsonDate As Date = Nothing
    Private mstrCondition As String = ""

    Private mstrIncludeInactiveDependants As String = ""
    Private mblnIncludeInactiveDependants As Boolean = False

    Private mstrShowOnlyExemptedDependents As String = ""
    Private mblnShowOnlyExemptedDependents As Boolean = False

    Private mstrShowOnlyInActiveDependants As String = ""
    Private mblnShowOnlyInActiveDependants As Boolean = False

    Private mstrAgeLimitWise As String = ""
    Private mblnAgeLimitWise As Boolean = False

    Private mstrExemptionWise As String = ""
    Private mblnExemptionWise As Boolean = False

    'Pinkal (16-AUG-2014) -- End

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End


    'S.SANDEEP [16-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 16
    Private mblnShowSubAndGrandTotalRelationWise As Boolean = False
    'S.SANDEEP [16-SEP-2017] -- END


#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _FirstName() As String
        Set(ByVal value As String)
            mstrFirstName = value
        End Set
    End Property

    Public WriteOnly Property _LastName() As String
        Set(ByVal value As String)
            mstrLastName = value
        End Set
    End Property

    Public WriteOnly Property _RelationId() As Integer
        Set(ByVal value As Integer)
            mintRelationId = value
        End Set
    End Property

    Public WriteOnly Property _RelationName() As String
        Set(ByVal value As String)
            mstrRelationName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    'Sandeep [ 12 MARCH 2011 ] -- Start
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property
    'Sandeep [ 12 MARCH 2011 ] -- End 

    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (16 May 2012) -- End

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End
    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'S.SANDEEP [ 06 JUN 2014 ] -- START
    Public WriteOnly Property _ShowDepedantsWithoutImage() As Boolean
        Set(ByVal value As Boolean)
            mblnShowDepedantsWithoutImage = value
        End Set
    End Property

    Public WriteOnly Property _IsImageInDatabase() As Boolean
        Set(ByVal value As Boolean)
            mblnIsImageInDatabase = value
        End Set
    End Property

    Public WriteOnly Property _ShowDepedantsWithoutImageText() As String
        Set(ByVal value As String)
            mstrShowDepedantsWithoutImageText = value
        End Set
    End Property
    'S.SANDEEP [ 06 JUN 2014 ] -- END


    'Pinkal (16-AUG-2014) -- Start
    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report

    Public WriteOnly Property _AsonDate() As Date
        Set(ByVal value As Date)
            mdtAsonDate = value
        End Set
    End Property

    Public WriteOnly Property _Condition() As String
        Set(ByVal value As String)
            mstrCondition = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveDependantsText() As String
        Set(ByVal value As String)
            mstrIncludeInactiveDependants = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveDependants()
        Set(ByVal value)
            mblnIncludeInactiveDependants = value
        End Set
    End Property

    Public WriteOnly Property _ShowOnlyExemptedDependentsText() As String
        Set(ByVal value As String)
            mstrShowOnlyExemptedDependents = value
        End Set
    End Property

    Public WriteOnly Property _ShowOnlyExemptedDependents()
        Set(ByVal value)
            mblnShowOnlyExemptedDependents = value
        End Set
    End Property

    Public WriteOnly Property _ShowOnlyInActiveDependantsText() As String
        Set(ByVal value As String)
            mstrShowOnlyInActiveDependants = value
        End Set
    End Property

    Public WriteOnly Property _ShowOnlyInactiveDependants()
        Set(ByVal value)
            mblnShowOnlyInActiveDependants = value
        End Set
    End Property

    Public WriteOnly Property _AgeLimitWiseText() As String
        Set(ByVal value As String)
            mstrAgeLimitWise = value
        End Set
    End Property

    Public WriteOnly Property _AgeLimitWise()
        Set(ByVal value)
            mblnAgeLimitWise = value
        End Set
    End Property

    Public WriteOnly Property _ExemptionWiseText() As String
        Set(ByVal value As String)
            mstrExemptionWise = value
        End Set
    End Property

    Public WriteOnly Property _ExemptionWise()
        Set(ByVal value)
            mblnExemptionWise = value
        End Set
    End Property

    'Pinkal (16-AUG-2014) -- End

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- E

    'S.SANDEEP [16-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 16
    Public WriteOnly Property _ShowSubAndGrandTotalRelationWise() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSubAndGrandTotalRelationWise = value
        End Set
    End Property
    'S.SANDEEP [16-SEP-2017] -- END
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrFirstName = ""
            mstrLastName = ""
            mintRelationId = 0
            mstrRelationName = ""
            mintReportTypeId = 0
            mstrReportTypeName = ""

            'Sandeep [ 12 MARCH 2011 ] -- Start
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            'Sandeep [ 12 MARCH 2011 ] -- End 
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (16 May 2012) -- End

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'S.SANDEEP [ 06 JUN 2014 ] -- START
            mblnShowDepedantsWithoutImage = False
            mblnIsImageInDatabase = False
            'S.SANDEEP [ 06 JUN 2014 ] -- END


            'Pinkal (16-AUG-2014) -- Start
            'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
            mdtAsonDate = Nothing
            mstrCondition = ""
            mstrIncludeInactiveDependants = ""
            mblnIncludeInactiveDependants = False
            mstrShowOnlyExemptedDependents = ""
            mblnShowOnlyExemptedDependents = False
            mstrShowOnlyInActiveDependants = ""
            mblnShowOnlyInActiveDependants = False
            mstrAgeLimitWise = ""
            mblnAgeLimitWise = False
            mstrExemptionWise = ""
            mblnExemptionWise = False

            'Pinkal (16-AUG-2014) -- End

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End

            'S.SANDEEP [16-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 16
            mblnShowSubAndGrandTotalRelationWise = False
            'S.SANDEEP [16-SEP-2017] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try


            'Sandeep [ 12 MARCH 2011 ] -- Start
            'If mintEmployeeId > 0 Then
            '    objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            '    Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Employee :") & " " & mstrEmployeeName & " "
            'End If

            'Select Case mintReportTypeId
            '    Case 0  'Dependants
            '        If mstrFirstName.Trim <> "" Then
            '            objDataOperation.AddParameter("@FirstName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrFirstName & "%")
            '            Me._FilterQuery &= " AND ISNULL(hrdependants_beneficiaries_tran.first_name,'') LIKE @FirstName "
            '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Firstname :") & " " & mstrFirstName & " "
            '        End If

            '        If mstrLastName.Trim <> "" Then
            '            objDataOperation.AddParameter("@LastName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrLastName & "%")
            '            Me._FilterQuery &= " AND ISNULL(hrdependants_beneficiaries_tran.last_name,'') LIKE @LastName "
            '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Lastname :") & " " & mstrLastName & " "
            '        End If
            '    Case 1  'Referee
            '        If mstrFirstName.Trim <> "" Then
            '            objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrFirstName & "%")
            '            Me._FilterQuery &= " AND ISNULL(hremployee_referee_tran.name,'') LIKE @Name "
            '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Name :") & " " & mstrFirstName & " "
            '        End If
            'End Select

            'If mintRelationId > 0 Then
            '    objDataOperation.AddParameter("@RelationId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationId)
            '    Me._FilterQuery &= " AND cfcommon_master.masterunkid = @RelationId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Relation :") & " " & mstrRelationName & " "
            'End If

            If mblIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Employee :") & " " & mstrEmployeeName & " "
            End If

            Select Case mintReportTypeId
                Case 0  'Dependants
                    If mstrFirstName.Trim <> "" Then
                        objDataOperation.AddParameter("@FirstName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrFirstName & "%")
                        Me._FilterQuery &= " AND ISNULL(hrdependants_beneficiaries_tran.first_name,'') LIKE @FirstName "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Firstname :") & " " & mstrFirstName & " "
                    End If

                    If mstrLastName.Trim <> "" Then
                        objDataOperation.AddParameter("@LastName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrLastName & "%")
                        Me._FilterQuery &= " AND ISNULL(hrdependants_beneficiaries_tran.last_name,'') LIKE @LastName "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Lastname :") & " " & mstrLastName & " "
                    End If

                    'S.SANDEEP [ 06 JUN 2014 ] -- START
                    If mblnShowDepedantsWithoutImage = True Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "View Type :") & " " & mstrShowDepedantsWithoutImageText & " "
                    End If
                    'S.SANDEEP [ 06 JUN 2014 ] -- END



                Case 1  'Referee
                    If mstrFirstName.Trim <> "" Then
                        objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrFirstName & "%")
                        'Sohail (16 May 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Me._FilterQuery &= " AND DRName LIKE @Name "
                        Me._FilterQuery &= " AND ISNULL(hremployee_referee_tran.name,'') LIKE @Name "
                        'Sohail (16 May 2012) -- End
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Name :") & " " & mstrFirstName & " "
                    End If
            End Select

            If mintRelationId > 0 Then
                objDataOperation.AddParameter("@RelationId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationId)
                'Sohail (16 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= " AND RelId = @RelationId "
                Me._FilterQuery &= " AND cfcommon_master.masterunkid = @RelationId "
                'Sohail (16 May 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Relation :") & " " & mstrRelationName & " "
            End If

            If mstrViewByName.Length > 0 Then
                'Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Analysis By :") & " " & mstrViewByName & " " 'Sohail (16 May 2012)
            End If
            'Sandeep [ 12 MARCH 2011 ] -- End 


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                Me._FilterQuery &= "ORDER BY hremployee_master.employeeunkid, " & Me.OrderByQuery
            Else
                Me._FilterQuery &= "ORDER BY hremployee_master.employeeunkid "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQueryForDependent(Optional ByVal blnFinal As Boolean = True)
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
            End If

            If mstrFirstName.Trim <> "" Then
                Me._FilterQuery &= " AND ISNULL(hrdependants_beneficiaries_tran.first_name,'') LIKE @FirstName "
            End If

            If mstrLastName.Trim <> "" Then
                Me._FilterQuery &= " AND ISNULL(hrdependants_beneficiaries_tran.last_name,'') LIKE @LastName "
            End If

            If mblnShowDepedantsWithoutImage = True Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "View Type :") & " " & mstrShowDepedantsWithoutImageText & " "
            End If

            If mintRelationId > 0 Then
                Me._FilterQuery &= " AND cfcommon_master.masterunkid = @RelationId "
            End If

            'S.SANDEEP [15-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : ERROR RELATED TO, AS ON DATE DECLARTIAON
            'objDataOperation.AddParameter("@asondate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsonDate))
            'S.SANDEEP [15-SEP-2017] -- END

            If blnFinal Then

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "As on Date :") & " " & mdtAsonDate.Date & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Condition :") & " " & mstrCondition & " "

                If mblIsActive = False Then
                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                End If

                If mintEmployeeId > 0 Then
                    objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Employee :") & " " & mstrEmployeeName & " "
                End If

                If mstrFirstName.Trim <> "" Then
                    objDataOperation.AddParameter("@FirstName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrFirstName & "%")
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Firstname :") & " " & mstrFirstName & " "
                End If

                If mstrLastName.Trim <> "" Then
                    objDataOperation.AddParameter("@LastName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrLastName & "%")
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Lastname :") & " " & mstrLastName & " "
                End If

                If mblnShowDepedantsWithoutImage = True Then
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "View Type :") & " " & mstrShowDepedantsWithoutImageText & " "
                End If

                If mintRelationId > 0 Then
                    objDataOperation.AddParameter("@RelationId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationId)
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Relation :") & " " & mstrRelationName & " "
                End If


                If mstrShowOnlyExemptedDependents.Trim.Length > 0 Then
                    Me._FilterTitle &= mstrShowOnlyExemptedDependents & " "
                End If

                If mstrIncludeInactiveDependants.Trim.Length > 0 Then
                    Me._FilterTitle &= mstrIncludeInactiveDependants & " "
                End If

                If mstrShowOnlyInActiveDependants.Trim.Length > 0 Then
                    Me._FilterTitle &= mstrShowOnlyInActiveDependants & " "
                End If

                If mstrAgeLimitWise.Trim.Length > 0 Then
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "InActive Dependent Option :") & " " & mstrAgeLimitWise & " "
                End If

                If mstrExemptionWise.Trim.Length > 0 Then
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "InActive Dependent Option :") & " " & mstrExemptionWise & " "
                End If

                If Me.OrderByQuery <> "" Then
                    Me._FilterTitle &= ""
                    Me._FilterQuery &= "ORDER BY hremployee_master.employeeunkid, " & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY hremployee_master.employeeunkid "
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQueryForDepedent; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If
        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()

        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If
            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, False)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        'Sandeep [ 12 MARCH 2011 ] -- Start
        Dim blnFlag As Boolean = False
        'Sandeep [ 12 MARCH 2011 ] -- End 

        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            ''S.SANDEEP [ 06 JUN 2014 ] -- START
            'Dim iFilterText As String = String.Empty
            'If mblnShowDepedantsWithoutImage = True Then
            '    If mblnIsImageInDatabase = True Then
            '        iFilterText = " AND hrdependants_beneficiaries_tran.dpndtbeneficetranunkid NOT IN " & _
            '                      "(SELECT dependantunkid FROM arutiimages..hrdependant_images WHERE isvoid = 0 "
            '        If mintEmployeeId > 0 Then
            '            iFilterText &= " AND companyunkid = '" & mintCompanyUnkid & "' AND employeeunkid = '" & mintEmployeeId & "' "
            '        End If
            '        iFilterText &= ")"
            '    Else
            '        iFilterText = " AND hrdependants_beneficiaries_tran.dpndtbeneficetranunkid NOT IN " & _
            '                      "(SELECT transactionid FROM hr_images_tran WHERE referenceid = '" & enImg_Email_RefId.Dependants_Beneficiaries & "' "
            '        If mintEmployeeId > 0 Then
            '            iFilterText &= " AND hr_images_tran.employeeunkid = '" & mintEmployeeId & "' "
            '        End If
            '        iFilterText &= ")"
            '    End If
            'End If
            ''S.SANDEEP [ 06 JUN 2014 ] -- END


            ''Sohail (16 May 2012) -- Start
            ''TRA - ENHANCEMENT
            ''Select Case mintReportTypeId
            ''    Case 0  'Dependant
            ''        'Sandeep [ 12 MARCH 2011 ] -- Start
            ''        'StrQ = "SELECT " & _
            ''        '         " ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            ''        '         ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
            ''        '         ",ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS DRName " & _
            ''        '         ",ISNULL(cfcommon_master.name,'') AS Relation " & _
            ''        '         ",ISNULL(hrdependants_beneficiaries_tran.address,'') AS Address " & _
            ''        '         ",CASE WHEN ISNULL(hrdependants_beneficiaries_tran.telephone_no,'') = '' THEN ISNULL(hrdependants_beneficiaries_tran.mobile_no,'') " & _
            ''        '         "  ELSE ISNULL(hrdependants_beneficiaries_tran.telephone_no,'') END AS Phone " & _
            ''        '      "FROM hrdependants_beneficiaries_tran " & _
            ''        '         "JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''        '         "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
            ''        '      "WHERE ISNULL(hrdependants_beneficiaries_tran.isvoid,0) = 0 AND ISNULL(hremployee_master.isactive,0) = 1 "
            ''        StrQ = "SELECT "
            ''        Select Case mintViewIndex
            ''            Case enAnalysisReport.Branch
            ''                StrQ &= " stationunkid , name AS GName "
            ''                blnFlag = True
            ''            Case enAnalysisReport.Department
            ''                StrQ &= " departmentunkid , name AS GName "
            ''                blnFlag = True
            ''            Case enAnalysisReport.Section
            ''                StrQ &= "  sectionunkid , name AS GName "
            ''                blnFlag = True
            ''            Case enAnalysisReport.Unit
            ''                StrQ &= "  unitunkid , name AS GName "
            ''                blnFlag = True
            ''            Case enAnalysisReport.Job
            ''                StrQ &= "  jobunkid , job_name AS GName "
            ''                blnFlag = True
            ''                'S.SANDEEP [ 06 SEP 2011 ] -- START
            ''            Case enAnalysisReport.CostCenter
            ''                StrQ &= " costcenterunkid  , costcentername AS GName "
            ''                blnFlag = True
            ''                'S.SANDEEP [ 06 SEP 2011 ] -- END 
            ''            Case Else
            ''                StrQ &= "  ISNULL(ECode,'') AS ECode " & _
            ''                              ", ISNULL(EName,'') AS EName " & _
            ''                              ", ISNULL(DRName,'') AS DRName " & _
            ''                              ", ISNULL(Relation,'') AS Relation " & _
            ''                              ", ISNULL(Address,'') AS Address " & _
            ''                              ", ISNULL(Phone,'') AS Phone " & _
            ''                              ",'' AS GName "
            ''                blnFlag = False
            ''        End Select

            ''        If blnFlag = True Then
            ''            StrQ &= " , ISNULL(ECode,'') AS ECode " & _
            ''                          ", ISNULL(EName,'') AS EName " & _
            ''                          ", ISNULL(DRName,'') AS DRName " & _
            ''                          ", ISNULL(Relation,'') AS Relation " & _
            ''                          ", ISNULL(Address,'') AS Address " & _
            ''                          ", ISNULL(Phone,'') AS Phone "
            ''            Select Case mintViewIndex
            ''                Case enAnalysisReport.Branch
            ''                    StrQ &= " FROM hrstation_master "
            ''                Case enAnalysisReport.Department
            ''                    StrQ &= " FROM hrdepartment_master "
            ''                Case enAnalysisReport.Section
            ''                    StrQ &= " FROM hrsection_master "
            ''                Case enAnalysisReport.Unit
            ''                    StrQ &= " FROM hrunit_master "
            ''                Case enAnalysisReport.Job
            ''                    StrQ &= " FROM hrjob_master "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- START
            ''                Case enAnalysisReport.CostCenter
            ''                    StrQ &= " FROM prcostcenter_master "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- END 
            ''            End Select
            ''            StrQ &= " LEFT JOIN " & _
            ''                " ( " & _
            ''                        "SELECT " & _
            ''         " ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            ''         ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
            ''         ",ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS DRName " & _
            ''         ",ISNULL(cfcommon_master.name,'') AS Relation " & _
            ''         ",ISNULL(hrdependants_beneficiaries_tran.address,'') AS Address " & _
            ''         ",CASE WHEN ISNULL(hrdependants_beneficiaries_tran.telephone_no,'') = '' THEN ISNULL(hrdependants_beneficiaries_tran.mobile_no,'') " & _
            ''         "  ELSE ISNULL(hrdependants_beneficiaries_tran.telephone_no,'') END AS Phone " & _
            ''                          ",hremployee_master.employeeunkid AS EmpId " & _
            ''                          ",ISNULL(hrdependants_beneficiaries_tran.first_name,'') AS FirstName " & _
            ''                          ",ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS LastName " & _
            ''                          ",cfcommon_master.masterunkid  AS RelId "
            ''            Select Case mintViewIndex
            ''                Case enAnalysisReport.Branch
            ''                    StrQ &= ", hremployee_master.stationunkid AS BranchId  "
            ''                Case enAnalysisReport.Department
            ''                    StrQ &= ", hremployee_master.departmentunkid AS DeptId "
            ''                Case enAnalysisReport.Section
            ''                    StrQ &= ", hremployee_master.sectionunkid AS SecId  "
            ''                Case enAnalysisReport.Unit
            ''                    StrQ &= ", hremployee_master.unitunkid AS UnitId  "
            ''                Case enAnalysisReport.Job
            ''                    StrQ &= ", hremployee_master.jobunkid AS JobId  "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- START
            ''                Case enAnalysisReport.CostCenter
            ''                    StrQ &= ", hremployee_master.costcenterunkid As CCId "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- END 
            ''            End Select


            ''            'Pinkal (24-Jun-2011) -- Start
            ''            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            ''            'StrQ &= "FROM hrdependants_beneficiaries_tran " & _
            ''            '              "JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''            '              "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
            ''            '           "WHERE ISNULL(hrdependants_beneficiaries_tran.isvoid,0) = 0 AND ISNULL(hremployee_master.isactive,0) = 1 "

            ''StrQ &= "FROM hrdependants_beneficiaries_tran " & _
            ''              "JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''              "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
            ''                     "WHERE ISNULL(hrdependants_beneficiaries_tran.isvoid,0) = 0 "

            ''            If mblIsActive = False Then
            ''                'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''                'ISSUE : TRA ENHANCEMENTS
            ''                'StrQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
            ''                StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            ''                'S.SANDEEP [ 12 MAY 2012 ] -- END
            ''            End If

            ''            'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''            'ENHANCEMENT : TRA CHANGES
            ''            If UserAccessLevel._AccessLevel.Length > 0 Then
            ''                StrQ &= UserAccessLevel._AccessLevelFilterString
            ''            End If
            ''            'S.SANDEEP [ 12 MAY 2012 ] -- END

            ''            'Pinkal (24-Jun-2011) -- End

            ''            Select Case mintViewIndex
            ''                Case enAnalysisReport.Branch
            ''                    StrQ &= " ) AS ERR ON hrstation_master.stationunkid = ERR.BranchId " & _
            ''                                  " WHERE hrstation_master.stationunkid IN ( " & mstrViewByIds & " ) "
            ''                Case enAnalysisReport.Department
            ''                    StrQ &= " ) AS ERR ON hrdepartment_master.departmentunkid = ERR.DeptId " & _
            ''                                  " WHERE hrdepartment_master.departmentunkid IN ( " & mstrViewByIds & " ) "
            ''                Case enAnalysisReport.Section
            ''                    StrQ &= " ) AS ERR ON hrsection_master.sectionunkid = ERR.SecId " & _
            ''                                  " WHERE hrsection_master.sectionunkid IN ( " & mstrViewByIds & " ) "
            ''                Case enAnalysisReport.Unit
            ''                    StrQ &= " ) AS ERR ON hrunit_master.unitunkid = ERR.UnitId " & _
            ''                                  " WHERE hrunit_master.unitunkid IN ( " & mstrViewByIds & " ) "
            ''                Case enAnalysisReport.Job
            ''                    StrQ &= " ) AS ERR ON hrjob_master.jobunkid = ERR.JobId " & _
            ''                                  " WHERE hrjob_master.jobunkid IN ( " & mstrViewByIds & " ) "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- START
            ''                Case enAnalysisReport.CostCenter
            ''                    StrQ &= " ) AS ERR ON prcostcenter_master.costcenterunkid = ERR.CCId " & _
            ''                                  " WHERE prcostcenter_master.costcenterunkid IN ( " & mstrViewByIds & " ) "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- END 
            ''            End Select
            ''        Else


            ''            'Pinkal (24-Jun-2011) -- Start
            ''            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            ''            'StrQ &= " FROM " & _
            ''            '             " ( " & _
            ''            '                      "SELECT " & _
            ''            '                      "     ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            ''            '                      ",    ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
            ''            '                      ",    ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS DRName " & _
            ''            '                      ",    ISNULL(cfcommon_master.name,'') AS Relation " & _
            ''            '                      ",    ISNULL(hrdependants_beneficiaries_tran.address,'') AS Address " & _
            ''            '                      ",    CASE WHEN ISNULL(hrdependants_beneficiaries_tran.telephone_no,'') = '' THEN ISNULL(hrdependants_beneficiaries_tran.mobile_no,'') " & _
            ''            '                      "         ELSE ISNULL(hrdependants_beneficiaries_tran.telephone_no,'') END AS Phone " & _
            ''            '                      ",    hremployee_master.employeeunkid AS EmpId " & _
            ''            '                      ",    ISNULL(hrdependants_beneficiaries_tran.first_name,'') AS FirstName " & _
            ''            '                      ",    ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS LastName " & _
            ''            '                      ",    cfcommon_master.masterunkid  AS RelId " & _
            ''            '   "FROM hrdependants_beneficiaries_tran " & _
            ''            '      "JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''            '      "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
            ''            '                      "WHERE ISNULL(hrdependants_beneficiaries_tran.isvoid,0) = 0 AND ISNULL(hremployee_master.isactive,0) = 1 ) AS ERR WHERE 1 = 1 "

            ''StrQ &= " FROM " & _
            ''             " ( " & _
            ''                      "SELECT " & _
            ''                      "     ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            ''                      ",    ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
            ''                      ",    ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS DRName " & _
            ''                      ",    ISNULL(cfcommon_master.name,'') AS Relation " & _
            ''                      ",    ISNULL(hrdependants_beneficiaries_tran.address,'') AS Address " & _
            ''                      ",    CASE WHEN ISNULL(hrdependants_beneficiaries_tran.telephone_no,'') = '' THEN ISNULL(hrdependants_beneficiaries_tran.mobile_no,'') " & _
            ''                      "         ELSE ISNULL(hrdependants_beneficiaries_tran.telephone_no,'') END AS Phone " & _
            ''                      ",    hremployee_master.employeeunkid AS EmpId " & _
            ''                      ",    ISNULL(hrdependants_beneficiaries_tran.first_name,'') AS FirstName " & _
            ''                      ",    ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS LastName " & _
            ''                      ",    cfcommon_master.masterunkid  AS RelId " & _
            ''   "FROM hrdependants_beneficiaries_tran " & _
            ''      "JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''      "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
            ''                                 "WHERE ISNULL(hrdependants_beneficiaries_tran.isvoid,0) = 0 "

            ''            If mblIsActive = False Then
            ''                'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''                'ISSUE : TRA ENHANCEMENTS
            ''                'StrQ &= " AND ISNULL(hremployee_master.isactive,0) = 1  "
            ''                StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            ''                'S.SANDEEP [ 12 MAY 2012 ] -- END
            ''            End If

            ''            'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''            'ENHANCEMENT : TRA CHANGES
            ''            If UserAccessLevel._AccessLevel.Length > 0 Then
            ''                StrQ &= UserAccessLevel._AccessLevelFilterString
            ''            End If
            ''            'S.SANDEEP [ 12 MAY 2012 ] -- END

            ''            StrQ &= " ) AS ERR WHERE 1 = 1 "

            ''            'Pinkal (24-Jun-2011) -- End

            ''        End If
            ''        'Sandeep [ 12 MARCH 2011 ] -- End 

            ''    Case 1  'Referee

            ''        'Sandeep [ 12 MARCH 2011 ] -- Start
            ''        'StrQ = "SELECT " & _
            ''        '                          " ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            ''        '                          ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
            ''        '                          ",ISNULL(hremployee_referee_tran.name,'') AS DRName " & _
            ''        '                          ",ISNULL(cfcommon_master.name,'') AS Relation " & _
            ''        '                          ",ISNULL(hremployee_referee_tran.address,'') AS Address " & _
            ''        '                          ",CASE WHEN ISNULL(hremployee_referee_tran.telephone_no,'') = '' THEN ISNULL(hremployee_referee_tran.mobile_no,'') " & _
            ''        '                          "  ELSE ISNULL(hremployee_referee_tran.telephone_no,'') END AS Phone " & _
            ''        '                       "FROM hremployee_referee_tran " & _
            ''        '                          "JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''        '                          "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
            ''        '                       "WHERE ISNULL(hremployee_referee_tran.isvoid,0) = 0 AND ISNULL(hremployee_master.isactive,0) = 1 "
            ''        StrQ = " SELECT "
            ''        Select Case mintViewIndex
            ''            Case enAnalysisReport.Branch
            ''                StrQ &= " stationunkid , name AS GName "
            ''                blnFlag = True
            ''            Case enAnalysisReport.Department
            ''                StrQ &= " departmentunkid , name AS GName "
            ''                blnFlag = True
            ''            Case enAnalysisReport.Section
            ''                StrQ &= "  sectionunkid , name AS GName "
            ''                blnFlag = True
            ''            Case enAnalysisReport.Unit
            ''                StrQ &= "  unitunkid , name AS GName "
            ''                blnFlag = True
            ''            Case enAnalysisReport.Job
            ''                StrQ &= "  jobunkid , job_name AS GName "
            ''                blnFlag = True
            ''                'S.SANDEEP [ 06 SEP 2011 ] -- START
            ''            Case enAnalysisReport.CostCenter
            ''                StrQ &= " costcenterunkid  , costcentername AS GName "
            ''                blnFlag = True
            ''                'S.SANDEEP [ 06 SEP 2011 ] -- END 
            ''            Case Else
            ''                StrQ &= " ECode AS ECode " & _
            ''                                ", EName AS EName " & _
            ''                                ", DRName AS DRName " & _
            ''                                ", Relation AS Relation " & _
            ''                                ", Address AS Address " & _
            ''                                ", Phone AS Phone " & _
            ''                                ",'' AS GName "
            ''                blnFlag = False
            ''        End Select

            ''        If blnFlag = True Then
            ''            StrQ &= " , ISNULL(ECode,'') AS ECode " & _
            ''                          ", ISNULL(EName,'') AS EName " & _
            ''                          ", ISNULL(DRName,'') AS DRName " & _
            ''                          ", ISNULL(Relation,'') AS Relation " & _
            ''                          ", ISNULL(Address,'') AS Address " & _
            ''                          ", ISNULL(Phone,'') AS Phone "
            ''            Select Case mintViewIndex
            ''                Case enAnalysisReport.Branch
            ''                    StrQ &= " FROM hrstation_master "
            ''                Case enAnalysisReport.Department
            ''                    StrQ &= " FROM hrdepartment_master "
            ''                Case enAnalysisReport.Section
            ''                    StrQ &= " FROM hrsection_master "
            ''                Case enAnalysisReport.Unit
            ''                    StrQ &= " FROM hrunit_master "
            ''                Case enAnalysisReport.Job
            ''                    StrQ &= " FROM hrjob_master "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- START
            ''                Case enAnalysisReport.CostCenter
            ''                    StrQ &= " FROM prcostcenter_master "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- END 
            ''            End Select
            ''            StrQ &= " LEFT JOIN " & _
            ''                " ( " & _
            ''                     "SELECT " & _
            ''                          " ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            ''                          ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
            ''                          ",ISNULL(hremployee_referee_tran.name,'') AS DRName " & _
            ''                          ",ISNULL(cfcommon_master.name,'') AS Relation " & _
            ''                          ",ISNULL(hremployee_referee_tran.address,'') AS Address " & _
            ''                          ",CASE WHEN ISNULL(hremployee_referee_tran.telephone_no,'') = '' THEN ISNULL(hremployee_referee_tran.mobile_no,'') " & _
            ''                          "  ELSE ISNULL(hremployee_referee_tran.telephone_no,'') END AS Phone " & _
            ''                        ",hremployee_master.employeeunkid AS EmpId " & _
            ''                        ",cfcommon_master.masterunkid  AS RelId "
            ''            Select Case mintViewIndex
            ''                Case enAnalysisReport.Branch
            ''                    StrQ &= ", hremployee_master.stationunkid AS BranchId  "
            ''                Case enAnalysisReport.Department
            ''                    StrQ &= ", hremployee_master.departmentunkid AS DeptId "
            ''                Case enAnalysisReport.Section
            ''                    StrQ &= ", hremployee_master.sectionunkid AS SecId  "
            ''                Case enAnalysisReport.Unit
            ''                    StrQ &= ", hremployee_master.unitunkid AS UnitId  "
            ''                Case enAnalysisReport.Job
            ''                    StrQ &= ", hremployee_master.jobunkid AS JobId  "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- START
            ''                Case enAnalysisReport.CostCenter
            ''                    StrQ &= ", hremployee_master.costcenterunkid As CCId "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- END 
            ''            End Select


            ''            'Pinkal (24-Jun-2011) -- Start
            ''            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            ''            'StrQ &= "FROM hremployee_referee_tran " & _
            ''            '            "JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''            '            "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
            ''            '       "WHERE ISNULL(hremployee_referee_tran.isvoid,0) = 0 AND ISNULL(hremployee_master.isactive,0) = 1 "


            ''StrQ &= "FROM hremployee_referee_tran " & _
            ''            "JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''            "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
            ''                       "WHERE ISNULL(hremployee_referee_tran.isvoid,0) = 0 "

            ''            If mblIsActive = False Then
            ''                'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''                'ISSUE : TRA ENHANCEMENTS
            ''                'StrQ &= " AND ISNULL(hremployee_master.isactive,0) = 1  "
            ''                StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            ''                'S.SANDEEP [ 12 MAY 2012 ] -- END
            ''            End If

            ''            'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''            'ENHANCEMENT : TRA CHANGES
            ''            If UserAccessLevel._AccessLevel.Length > 0 Then
            ''                StrQ &= UserAccessLevel._AccessLevelFilterString
            ''            End If
            ''            'S.SANDEEP [ 12 MAY 2012 ] -- END
            ''            'Pinkal (24-Jun-2011) -- End

            ''            Select Case mintViewIndex
            ''                Case enAnalysisReport.Branch
            ''                    StrQ &= " ) AS ERR ON hrstation_master.stationunkid = ERR.BranchId " & _
            ''                                  " WHERE hrstation_master.stationunkid IN ( " & mstrViewByIds & " ) "
            ''                Case enAnalysisReport.Department
            ''                    StrQ &= " ) AS ERR ON hrdepartment_master.departmentunkid = ERR.DeptId " & _
            ''                                  " WHERE hrdepartment_master.departmentunkid IN ( " & mstrViewByIds & " ) "
            ''                Case enAnalysisReport.Section
            ''                    StrQ &= " ) AS ERR ON hrsection_master.sectionunkid = ERR.SecId " & _
            ''                                  " WHERE hrsection_master.sectionunkid IN ( " & mstrViewByIds & " ) "
            ''                Case enAnalysisReport.Unit
            ''                    StrQ &= " ) AS ERR ON hrunit_master.unitunkid = ERR.UnitId " & _
            ''                                  " WHERE hrunit_master.unitunkid IN ( " & mstrViewByIds & " ) "
            ''                Case enAnalysisReport.Job
            ''                    StrQ &= " ) AS ERR ON hrjob_master.jobunkid = ERR.JobId " & _
            ''                                  " WHERE hrjob_master.jobunkid IN ( " & mstrViewByIds & " ) "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- START
            ''                Case enAnalysisReport.CostCenter
            ''                    StrQ &= " ) AS ERR ON prcostcenter_master.costcenterunkid = ERR.CCId " & _
            ''                                  " WHERE prcostcenter_master.costcenterunkid IN ( " & mstrViewByIds & " ) "
            ''                    'S.SANDEEP [ 06 SEP 2011 ] -- END 
            ''            End Select
            ''        Else


            ''            'Pinkal (24-Jun-2011) -- Start
            ''            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            ''            'StrQ &= " FROM " & _
            ''            '                " ( " & _
            ''            '                         "SELECT " & _
            ''            '      " ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            ''            '      ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
            ''            '      ",ISNULL(hremployee_referee_tran.name,'') AS DRName " & _
            ''            '      ",ISNULL(cfcommon_master.name,'') AS Relation " & _
            ''            '      ",ISNULL(hremployee_referee_tran.address,'') AS Address " & _
            ''            '      ",CASE WHEN ISNULL(hremployee_referee_tran.telephone_no,'') = '' THEN ISNULL(hremployee_referee_tran.mobile_no,'') " & _
            ''            '      "  ELSE ISNULL(hremployee_referee_tran.telephone_no,'') END AS Phone " & _
            ''            '                             ",hremployee_master.employeeunkid AS EmpId " & _
            ''            '                             ",cfcommon_master.masterunkid  AS RelId " & _
            ''            '   "FROM hremployee_referee_tran " & _
            ''            '      "JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''            '      "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
            ''            '                        "WHERE ISNULL(hremployee_referee_tran.isvoid,0) = 0 AND ISNULL(hremployee_master.isactive,0) = 1 ) AS ERR WHERE 1 = 1 "

            ''StrQ &= " FROM " & _
            ''                " ( " & _
            ''                         "SELECT " & _
            ''      " ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            ''      ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
            ''      ",ISNULL(hremployee_referee_tran.name,'') AS DRName " & _
            ''      ",ISNULL(cfcommon_master.name,'') AS Relation " & _
            ''      ",ISNULL(hremployee_referee_tran.address,'') AS Address " & _
            ''      ",CASE WHEN ISNULL(hremployee_referee_tran.telephone_no,'') = '' THEN ISNULL(hremployee_referee_tran.mobile_no,'') " & _
            ''      "  ELSE ISNULL(hremployee_referee_tran.telephone_no,'') END AS Phone " & _
            ''                             ",hremployee_master.employeeunkid AS EmpId " & _
            ''                             ",cfcommon_master.masterunkid  AS RelId " & _
            ''   "FROM hremployee_referee_tran " & _
            ''      "JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''      "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
            ''                                "WHERE ISNULL(hremployee_referee_tran.isvoid,0) = 0  "

            ''            If mblIsActive = False Then
            ''                'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''                'ISSUE : TRA ENHANCEMENTS
            ''                'StrQ &= " AND ISNULL(hremployee_master.isactive,0) = 1"
            ''                StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            ''                        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            ''                'S.SANDEEP [ 12 MAY 2012 ] -- END
            ''            End If

            ''            'S.SANDEEP [ 12 MAY 2012 ] -- START
            ''            'ENHANCEMENT : TRA CHANGES
            ''            If UserAccessLevel._AccessLevel.Length > 0 Then
            ''                StrQ &= UserAccessLevel._AccessLevelFilterString
            ''            End If
            ''            'S.SANDEEP [ 12 MAY 2012 ] -- END

            ''            StrQ &= " ) AS ERR WHERE 1 = 1 "

            ''            'Pinkal (24-Jun-2011) -- End

            ''        End If
            ''        'Sandeep [ 12 MARCH 2011 ] -- End 
            ''End Select
            'Select Case mintReportTypeId
            '    Case 0  'Dependant


            '        'Pinkal (16-AUG-2014) -- Start
            '        'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report

            '        If mblnShowOnlyExemptedDependents = False AndAlso (mblnAgeLimitWise = False OrElse mblnAgeLimitWise) AndAlso mblnExemptionWise = False Then

            '            StrQ = "SELECT  ISNULL(hremployee_master.employeecode, '') AS ECode "
            '            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '            'ENHANCEMENT : Requested by Rutta
            '            '"	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '            If mblnFirstNamethenSurname = False Then
            '                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EName "
            '            Else
            '                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EName "
            '            End If
            '            'S.SANDEEP [ 26 MAR 2014 ] -- END

            '            StrQ &= ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS DRName " & _
            '                             ", ISNULL(cfcommon_master.name, '') AS Relation " & _
            '                             ", ISNULL(hrdependants_beneficiaries_tran.address, '') AS Address " & _
            '                             ", CASE WHEN ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') = '' " & _
            '                                    "THEN ISNULL(hrdependants_beneficiaries_tran.mobile_no, '') " & _
            '                                    "ELSE ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') " & _
            '                               "END AS Phone " & _
            '                             ", hremployee_master.employeeunkid AS EmpId " & _
            '                                  ", hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS DpndtTranId " & _
            '                             ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') AS FirstName " & _
            '                             ", ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS LastName " & _
            '                             ", cfcommon_master.masterunkid AS RelId " & _
            '                             ", ISNULL(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate,112), '') AS birthdate "

            '            If mintViewIndex > 0 Then
            '                StrQ &= mstrAnalysis_Fields
            '            Else
            '                StrQ &= ", 0 AS Id, '' AS GName "
            '            End If

            '            StrQ &= "FROM    hrdependants_beneficiaries_tran " & _
            '                               "JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                               "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
            '                                                       "AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " "

            '            StrQ &= mstrAnalysis_Join

            '            StrQ &= "WHERE   ISNULL(hrdependants_beneficiaries_tran.isvoid, 0) = 0 "


            '            'Pinkal (16-AUG-2014) -- Start
            '            'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
            '            If mblnIncludeInactiveDependants = False AndAlso mblnShowOnlyInActiveDependants = False Then
            '                StrQ &= " AND ((YEAR(@asondate)-YEAR(hrdependants_beneficiaries_tran.birthdate)) <= ISNULL(cfcommon_master.dependant_limit,0)  or ISNULL(cfcommon_master.dependant_limit,0) = 0) "
            '            ElseIf mblnShowOnlyInActiveDependants AndAlso mblnAgeLimitWise AndAlso mblnIncludeInactiveDependants = False Then
            '                StrQ &= " AND ((YEAR(@asondate)-YEAR(hrdependants_beneficiaries_tran.birthdate)) > ISNULL(cfcommon_master.dependant_limit,0))  AND ISNULL(cfcommon_master.dependant_limit,0)  <>  0 "
            '            ElseIf mblnShowOnlyInActiveDependants AndAlso mblnAgeLimitWise AndAlso mblnIncludeInactiveDependants = False Then
            '                StrQ &= " AND ((YEAR(@asondate)-YEAR(hrdependants_beneficiaries_tran.birthdate)) > ISNULL(cfcommon_master.dependant_limit,0))  AND ISNULL(cfcommon_master.dependant_limit,0)  <>  0 "
            '            End If
            '            'Pinkal (16-AUG-2014) -- End


            '            'S.SANDEEP [ 06 JUN 2014 ] -- START
            '            If iFilterText.Trim.Length > 0 Then
            '                StrQ &= iFilterText
            '            End If
            '            'S.SANDEEP [ 06 JUN 2014 ] -- END

            '            If mstrAdvance_Filter.Trim.Length > 0 Then
            '                StrQ &= " AND " & mstrAdvance_Filter
            '            End If

            '            If mblIsActive = False Then
            '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            '            End If

            '            If mstrUserAccessFilter = "" Then
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    StrQ &= UserAccessLevel._AccessLevelFilterString
            '                End If
            '            Else
            '                StrQ &= mstrUserAccessFilter
            '            End If

            '            If mblnAgeLimitWise AndAlso (mblnIncludeInactiveDependants OrElse mblnShowOnlyInActiveDependants) Then
            '                FilterTitleAndFilterQueryForDependent(True)
            '            Else
            '                FilterTitleAndFilterQueryForDependent(False)
            '            End If


            '            StrQ &= Me._FilterQuery

            '            If mblnAgeLimitWise = False AndAlso mblnExemptionWise = False Then
            '                StrQ &= " UNION "
            '            End If


            '        End If

            '        If mblnAgeLimitWise = False AndAlso (mblnExemptionWise = False OrElse mblnExemptionWise) Then

            '            StrQ &= "  SELECT  ISNULL(hremployee_master.employeecode, '') AS ECode "

            '            If mblnFirstNamethenSurname = False Then
            '                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EName "
            '            Else
            '                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EName "
            '            End If

            '            StrQ &= ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS DRName " & _
            '                         ", ISNULL(cfcommon_master.name, '') AS Relation " & _
            '                         ", ISNULL(hrdependants_beneficiaries_tran.address, '') AS Address " & _
            '                         ", CASE WHEN ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') = '' " & _
            '                                " THEN ISNULL(hrdependants_beneficiaries_tran.mobile_no, '') " & _
            '                                " ELSE ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') " & _
            '                         " END AS Phone " & _
            '                         ", hremployee_master.employeeunkid AS EmpId " & _
            '                         ", hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS DpndtTranId " & _
            '                         ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') AS FirstName " & _
            '                         ", ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS LastName " & _
            '                         ", cfcommon_master.masterunkid AS RelId " & _
            '                         ", ISNULL(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate,112), '') AS birthdate "

            '            If mintViewIndex > 0 Then
            '                StrQ &= mstrAnalysis_Fields
            '            Else
            '                StrQ &= ", 0 AS Id, '' AS GName "
            '            End If

            '            StrQ &= " FROM  mddependant_exception " & _
            '                         " JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid  AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
            '                         " JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                         " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
            '                         " AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " "

            '            StrQ &= mstrAnalysis_Join

            '            If mblnShowOnlyInActiveDependants = False AndAlso mblnExemptionWise = False Then
            '                StrQ &= "WHERE ISNULL(mddependant_exception.isvoid,0) = 0 AND ISNULL(hrdependants_beneficiaries_tran.isvoid, 0) = 0 AND mddependant_exception.ismedical = 1 AND (mddependant_exception.mdstopdate IS NULL OR  Convert(Char(8),mddependant_exception.mdstopdate,112)  " & mstrCondition & "@asondate)"
            '            ElseIf mblnShowOnlyInActiveDependants AndAlso mblnExemptionWise Then
            '                StrQ &= "WHERE ISNULL(mddependant_exception.isvoid,0) = 0 AND ISNULL(hrdependants_beneficiaries_tran.isvoid, 0) = 0 AND mddependant_exception.ismedical = 1 AND  Convert(Char(8),mddependant_exception.mdstopdate,112)  " & mstrCondition & "@asondate"
            '            End If

            '            If iFilterText.Trim.Length > 0 Then
            '                StrQ &= iFilterText
            '            End If

            '            If mstrAdvance_Filter.Trim.Length > 0 Then
            '                StrQ &= " AND " & mstrAdvance_Filter
            '            End If

            '            If mblIsActive = False Then
            '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            '            End If

            '            If mstrUserAccessFilter = "" Then
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    StrQ &= UserAccessLevel._AccessLevelFilterString
            '                End If
            '            Else
            '                StrQ &= mstrUserAccessFilter
            '            End If

            '            FilterTitleAndFilterQueryForDependent(False)

            '            StrQ &= Me._FilterQuery

            '            StrQ &= " UNION  SELECT  ISNULL(hremployee_master.employeecode, '') AS ECode "

            '            If mblnFirstNamethenSurname = False Then
            '                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EName "
            '            Else
            '                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EName "
            '            End If

            '            StrQ &= ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS DRName " & _
            '                         ", ISNULL(cfcommon_master.name, '') AS Relation " & _
            '                         ", ISNULL(hrdependants_beneficiaries_tran.address, '') AS Address " & _
            '                         ", CASE WHEN ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') = '' " & _
            '                                " THEN ISNULL(hrdependants_beneficiaries_tran.mobile_no, '') " & _
            '                                " ELSE ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') " & _
            '                         " END AS Phone " & _
            '                         ", hremployee_master.employeeunkid AS EmpId " & _
            '                         ", hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS DpndtTranId " & _
            '                         ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') AS FirstName " & _
            '                         ", ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS LastName " & _
            '                         ", cfcommon_master.masterunkid AS RelId " & _
            '                         ", ISNULL(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate,112), '') AS birthdate "


            '            If mintViewIndex > 0 Then
            '                StrQ &= mstrAnalysis_Fields
            '            Else
            '                StrQ &= ", 0 AS Id, '' AS GName "
            '            End If

            '            StrQ &= " FROM  mddependant_exception " & _
            '                         " JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid  AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
            '                         " JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                         " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
            '                         " AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " "

            '            StrQ &= mstrAnalysis_Join

            '            If mblnShowOnlyInActiveDependants = False AndAlso mblnExemptionWise = False Then
            '                StrQ &= "WHERE ISNULL(mddependant_exception.isvoid,0) = 0 AND ISNULL(hrdependants_beneficiaries_tran.isvoid, 0) = 0 AND mddependant_exception.isleave = 1 AND (mddependant_exception.lvstopdate IS NULL OR  Convert(Char(8),mddependant_exception.lvstopdate,112)  " & mstrCondition & "@asondate)"
            '            ElseIf mblnShowOnlyInActiveDependants AndAlso mblnExemptionWise Then
            '                StrQ &= "WHERE ISNULL(mddependant_exception.isvoid,0) = 0 AND ISNULL(hrdependants_beneficiaries_tran.isvoid, 0) = 0 AND mddependant_exception.isleave = 1 AND  Convert(Char(8),mddependant_exception.lvstopdate,112)  " & mstrCondition & "@asondate"
            '            End If

            '            If iFilterText.Trim.Length > 0 Then
            '                StrQ &= iFilterText
            '            End If

            '            If mstrAdvance_Filter.Trim.Length > 0 Then
            '                StrQ &= " AND " & mstrAdvance_Filter
            '            End If

            '            If mblIsActive = False Then
            '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            '            End If

            '            If mstrUserAccessFilter = "" Then
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    StrQ &= UserAccessLevel._AccessLevelFilterString
            '                End If
            '            Else
            '                StrQ &= mstrUserAccessFilter
            '            End If

            '            FilterTitleAndFilterQueryForDependent(True)
            '            StrQ &= Me._FilterQuery
            '        End If

            '        'Pinkal (16-AUG-2014) -- End



            '    Case 1  'Referee
            '        StrQ = "SELECT  ISNULL(hremployee_master.employeecode, '') AS ECode "
            '        'S.SANDEEP [ 26 MAR 2014 ] -- START
            '        'ENHANCEMENT : Requested by Rutta
            '        '",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
            '        If mblnFirstNamethenSurname = False Then
            '            StrQ &= ",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
            '        Else
            '            StrQ &= ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
            '        End If

            '        StrQ &= ",ISNULL(hremployee_referee_tran.name,'') AS DRName " & _
            '                  ",ISNULL(cfcommon_master.name,'') AS Relation " & _
            '                  ",ISNULL(hremployee_referee_tran.address,'') AS Address " & _
            '                      ", CASE WHEN ISNULL(hremployee_referee_tran.telephone_no, '') = '' " & _
            '                             "THEN ISNULL(hremployee_referee_tran.mobile_no, '') " & _
            '                             "ELSE ISNULL(hremployee_referee_tran.telephone_no, '') " & _
            '                        "END AS Phone " & _
            '                                         ",hremployee_master.employeeunkid AS EmpId " & _
            '                      ", cfcommon_master.masterunkid AS RelId " & _
            '                      ", '' AS birthdate "
            '        'Sohail (08 Aug 2013) - [birthdate]

            '        If mintViewIndex > 0 Then
            '            StrQ &= mstrAnalysis_Fields
            '        Else
            '            StrQ &= ", 0 AS Id, '' AS GName "
            '        End If

            '        StrQ &= "FROM    hremployee_referee_tran " & _
            '                  "JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                        "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid " & _
            '                                                "AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " "

            '        StrQ &= mstrAnalysis_Join

            '        StrQ &= "WHERE   ISNULL(hremployee_referee_tran.isvoid, 0) = 0 "

            '        'S.SANDEEP [ 13 FEB 2013 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        If mstrAdvance_Filter.Trim.Length > 0 Then
            '            StrQ &= " AND " & mstrAdvance_Filter
            '        End If
            '        'S.SANDEEP [ 13 FEB 2013 ] -- END

            '        If mblIsActive = False Then
            '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                    " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            '        End If


            '        'If UserAccessLevel._AccessLevel.Length > 0 Then
            '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
            '        'End If
            '        'S.SANDEEP [ 12 NOV 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        'If UserAccessLevel._AccessLevel.Length > 0 Then
            '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
            '        'End If
            '        If mstrUserAccessFilter = "" Then
            '            If UserAccessLevel._AccessLevel.Length > 0 Then
            '                StrQ &= UserAccessLevel._AccessLevelFilterString
            '            End If
            '        Else
            '            StrQ &= mstrUserAccessFilter
            '        End If
            '        'S.SANDEEP [ 12 NOV 2012 ] -- END

            '        Call FilterTitleAndFilterQuery()

            '        StrQ &= Me._FilterQuery

            'End Select

            ''Sohail (16 May 2012) -- End

            Dim iFilterText As String = String.Empty

            If mblnShowDepedantsWithoutImage = True Then
                If mblnIsImageInDatabase = True Then
                    iFilterText = " AND hrdependants_beneficiaries_tran.dpndtbeneficetranunkid NOT IN " & _
                                  "(SELECT dependantunkid FROM arutiimages..hrdependant_images WHERE isvoid = 0 "
                    If mintEmployeeId > 0 Then
                        iFilterText &= " AND companyunkid = '" & mintCompanyUnkid & "' AND employeeunkid = '" & mintEmployeeId & "' "
                    End If
                    iFilterText &= ")"
                Else
                    iFilterText = " AND hrdependants_beneficiaries_tran.dpndtbeneficetranunkid NOT IN " & _
                                  "(SELECT transactionid FROM hr_images_tran WHERE referenceid = '" & enImg_Email_RefId.Dependants_Beneficiaries & "' "
                    If mintEmployeeId > 0 Then
                        iFilterText &= " AND hr_images_tran.employeeunkid = '" & mintEmployeeId & "' "
                    End If
                    iFilterText &= ")"
                End If
            End If

            Select Case mintReportTypeId

                Case 0  'Dependant

                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    StrQ = "SELECT * " & _
                               "INTO #TableDepn " & _
                               "FROM " & _
                               "( " & _
                                   "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                        ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                                   "FROM hrdependant_beneficiaries_status_tran "

                    If mintEmployeeId > 0 Then
                        StrQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
                    End If

                    StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

                    If mintEmployeeId > 0 Then
                        StrQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = " & mintEmployeeId & " "
                    End If

                    If mdtAsonDate <> Nothing Then
                        StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @asondate "
                    Else
                        'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
                    End If

                    StrQ &= ") AS A " & _
                            "WHERE 1 = 1 " & _
                            " AND A.ROWNO = 1 "
                    'Sohail (18 May 2019) -- End

                    If mblnShowOnlyExemptedDependents = False AndAlso (mblnAgeLimitWise = False OrElse mblnAgeLimitWise) AndAlso mblnExemptionWise = False Then

                        StrQ &= "SELECT  ISNULL(hremployee_master.employeecode, '') AS ECode "
                        If mblnFirstNamethenSurname = False Then
                            StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EName "
                        Else
                            StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EName "
                        End If

                        StrQ &= ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS DRName " & _
                                ", ISNULL(cfcommon_master.name, '') AS Relation " & _
                                ", ISNULL(hrdependants_beneficiaries_tran.address, '') AS Address " & _
                                ", CASE WHEN ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') = '' THEN ISNULL(hrdependants_beneficiaries_tran.mobile_no, '') " & _
                                "  ELSE ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') END AS Phone " & _
                                ", hremployee_master.employeeunkid AS EmpId " & _
                                ", hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS DpndtTranId " & _
                                ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') AS FirstName " & _
                                ", ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS LastName " & _
                                ", cfcommon_master.masterunkid AS RelId " & _
                                ", ISNULL(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate,112), '') AS birthdate "

                        'S.SANDEEP |09-APR-2019| -- START
                        StrQ &= " ,(CAST(CONVERT(CHAR(8),@asondate,112) AS INT)-CAST(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112) AS INT))/10000 AS AgeYears "
                        'S.SANDEEP |09-APR-2019| -- END

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Fields
                        Else
                            StrQ &= ", 0 AS Id, '' AS GName "
                        End If

                        StrQ &= "FROM hrdependants_beneficiaries_tran " & _
                                "   JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                                "   JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid "
                        If xAdvanceJoinQry.Trim.Length > 0 Then
                            StrQ &= xAdvanceJoinQry
                        End If

                        If xDateJoinQry.Trim.Length > 0 Then
                            StrQ &= xDateJoinQry
                        End If
                        StrQ &= "   JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " "
                        'Sohail (18 May 2019) - [JOIN #TableDepn]


                        'S.SANDEEP |28-OCT-2021| -- START
                        'ISSUE : REPORT OPTIMZATION
                        'If xDateJoinQry.Trim.Length > 0 Then
                        '    StrQ &= xDateJoinQry
                        'End If

                        'If xUACQry.Trim.Length > 0 Then
                        '    StrQ &= xUACQry
                        'End If

                        'If xAdvanceJoinQry.Trim.Length > 0 Then
                        '    StrQ &= xAdvanceJoinQry
                        'End If

                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry
                        End If
                        'S.SANDEEP |28-OCT-2021| -- END
                        

                        StrQ &= mstrAnalysis_Join

                        StrQ &= "WHERE ISNULL(hrdependants_beneficiaries_tran.isvoid, 0) = 0 "

                        If mblnIncludeInactiveDependants = False AndAlso mblnShowOnlyInActiveDependants = False Then
                            StrQ &= " AND ((YEAR(@asondate)-YEAR(hrdependants_beneficiaries_tran.birthdate)) <= ISNULL(cfcommon_master.dependant_limit,0)  or ISNULL(cfcommon_master.dependant_limit,0) = 0) " & _
                                    " AND #TableDepn.isactive = 1 "
                            'Sohail (18 May 2019) - [isactive]
                        ElseIf mblnShowOnlyInActiveDependants AndAlso mblnAgeLimitWise AndAlso mblnIncludeInactiveDependants = False Then
                            StrQ &= " AND ((YEAR(@asondate)-YEAR(hrdependants_beneficiaries_tran.birthdate)) > ISNULL(cfcommon_master.dependant_limit,0))  AND ISNULL(cfcommon_master.dependant_limit,0)  <>  0 " & _
                                    " AND #TableDepn.isactive = 0 "
                            'Sohail (18 May 2019) - [isactive]
                        ElseIf mblnShowOnlyInActiveDependants AndAlso mblnAgeLimitWise AndAlso mblnIncludeInactiveDependants = False Then
                            StrQ &= " AND ((YEAR(@asondate)-YEAR(hrdependants_beneficiaries_tran.birthdate)) > ISNULL(cfcommon_master.dependant_limit,0))  AND ISNULL(cfcommon_master.dependant_limit,0)  <>  0 " & _
                                    " AND #TableDepn.isactive = 0 "
                            'Sohail (18 May 2019) - [isactive]
                        End If

                        If iFilterText.Trim.Length > 0 Then
                            StrQ &= iFilterText
                        End If

                        If mstrAdvance_Filter.Trim.Length > 0 Then
                            StrQ &= " AND " & mstrAdvance_Filter
                        End If

                        If xUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " AND " & xUACFiltrQry & " "
                        End If

                        If mblIsActive = False Then
                            If xDateFilterQry.Trim.Length > 0 Then
                                StrQ &= xDateFilterQry & " "
                            End If
                        End If

                        If mblnAgeLimitWise AndAlso (mblnIncludeInactiveDependants OrElse mblnShowOnlyInActiveDependants) Then
                            FilterTitleAndFilterQueryForDependent(True)
                        Else
                            FilterTitleAndFilterQueryForDependent(False)
                        End If


                        StrQ &= Me._FilterQuery

                        If mblnAgeLimitWise = False AndAlso mblnExemptionWise = False Then
                            StrQ &= " UNION "
                        End If


                    End If

                    If mblnAgeLimitWise = False AndAlso (mblnExemptionWise = False OrElse mblnExemptionWise) Then

                        StrQ &= "  SELECT  ISNULL(hremployee_master.employeecode, '') AS ECode "

                        If mblnFirstNamethenSurname = False Then
                            StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EName "
                        Else
                            StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EName "
                        End If

                        StrQ &= ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS DRName " & _
                                ", ISNULL(cfcommon_master.name, '') AS Relation " & _
                                ", ISNULL(hrdependants_beneficiaries_tran.address, '') AS Address " & _
                                ", CASE WHEN ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') = '' THEN ISNULL(hrdependants_beneficiaries_tran.mobile_no, '') " & _
                                "  ELSE ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') END AS Phone " & _
                                ", hremployee_master.employeeunkid AS EmpId " & _
                                ", hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS DpndtTranId " & _
                                ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') AS FirstName " & _
                                ", ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS LastName " & _
                                ", cfcommon_master.masterunkid AS RelId " & _
                                ", ISNULL(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate,112), '') AS birthdate "

                        'S.SANDEEP |09-APR-2019| -- START
                        StrQ &= " ,(CAST(CONVERT(CHAR(8),@asondate,112) AS INT)-CAST(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112) AS INT))/10000 AS AgeYears "
                        'S.SANDEEP |09-APR-2019| -- END

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Fields
                        Else
                            StrQ &= ", 0 AS Id, '' AS GName "
                        End If

                        StrQ &= " FROM  mddependant_exception " & _
                                     " JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid  AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                                     " JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                                     " JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid "
                        If xAdvanceJoinQry.Trim.Length > 0 Then
                            StrQ &= xAdvanceJoinQry
                        End If

                        If xDateJoinQry.Trim.Length > 0 Then
                            StrQ &= xDateJoinQry
                        End If
                        StrQ &= " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
                                     " AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " "
                        'Sohail (18 May 2019) - [JOIN #TableDepn]

                        'S.SANDEEP |28-OCT-2021| -- START
                        'ISSUE : REPORT OPTIMZATION
                        'If xDateJoinQry.Trim.Length > 0 Then
                        '    StrQ &= xDateJoinQry
                        'End If

                        'If xUACQry.Trim.Length > 0 Then
                        '    StrQ &= xUACQry
                        'End If

                        'If xAdvanceJoinQry.Trim.Length > 0 Then
                        '    StrQ &= xAdvanceJoinQry
                        'End If
                        

                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry
                        End If
                        'S.SANDEEP |28-OCT-2021| -- END

                        StrQ &= mstrAnalysis_Join

                        If mblnShowOnlyInActiveDependants = False AndAlso mblnExemptionWise = False Then
                            StrQ &= "WHERE ISNULL(mddependant_exception.isvoid,0) = 0 AND ISNULL(hrdependants_beneficiaries_tran.isvoid, 0) = 0 AND mddependant_exception.ismedical = 1 AND (mddependant_exception.mdstopdate IS NULL OR  Convert(Char(8),mddependant_exception.mdstopdate,112)  " & mstrCondition & "@asondate)" & _
                                    " AND #TableDepn.isactive = 1 "
                            'Sohail (18 May 2019) - [isactive]
                        ElseIf mblnShowOnlyInActiveDependants AndAlso mblnExemptionWise Then
                            StrQ &= "WHERE ISNULL(mddependant_exception.isvoid,0) = 0 AND ISNULL(hrdependants_beneficiaries_tran.isvoid, 0) = 0 AND mddependant_exception.ismedical = 1 AND  Convert(Char(8),mddependant_exception.mdstopdate,112)  " & mstrCondition & "@asondate" & _
                                    " AND #TableDepn.isactive = 0 "
                            'Sohail (18 May 2019) - [isactive]
                        End If

                        If iFilterText.Trim.Length > 0 Then
                            StrQ &= iFilterText
                        End If

                        If mstrAdvance_Filter.Trim.Length > 0 Then
                            StrQ &= " AND " & mstrAdvance_Filter
                        End If

                        If xUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " AND " & xUACFiltrQry & " "
                        End If

                        If mblIsActive = False Then
                            If xDateFilterQry.Trim.Length > 0 Then
                                StrQ &= xDateFilterQry & " "
                            End If
                        End If

                        FilterTitleAndFilterQueryForDependent(False)

                        StrQ &= Me._FilterQuery

                        StrQ &= " UNION  SELECT  ISNULL(hremployee_master.employeecode, '') AS ECode "

                        If mblnFirstNamethenSurname = False Then
                            StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EName "
                        Else
                            StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EName "
                        End If

                        StrQ &= ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS DRName " & _
                                ", ISNULL(cfcommon_master.name, '') AS Relation " & _
                                ", ISNULL(hrdependants_beneficiaries_tran.address, '') AS Address " & _
                                ", CASE WHEN ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') = '' THEN ISNULL(hrdependants_beneficiaries_tran.mobile_no, '') " & _
                                "  ELSE ISNULL(hrdependants_beneficiaries_tran.telephone_no, '') END AS Phone " & _
                                ", hremployee_master.employeeunkid AS EmpId " & _
                                ", hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS DpndtTranId " & _
                                ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') AS FirstName " & _
                                ", ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS LastName " & _
                                ", cfcommon_master.masterunkid AS RelId " & _
                                ", ISNULL(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate,112), '') AS birthdate "

                        'S.SANDEEP |09-APR-2019| -- START
                        StrQ &= " ,(CAST(CONVERT(CHAR(8),@asondate,112) AS INT)-CAST(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112) AS INT))/10000 AS AgeYears "
                        'S.SANDEEP |09-APR-2019| -- END


                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Fields
                        Else
                            StrQ &= ", 0 AS Id, '' AS GName "
                        End If

                        StrQ &= " FROM  mddependant_exception " & _
                                "   JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid  AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                                "   JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                                "   JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid "
                        If xAdvanceJoinQry.Trim.Length > 0 Then
                            StrQ &= xAdvanceJoinQry
                        End If

                        If xDateJoinQry.Trim.Length > 0 Then
                            StrQ &= xDateJoinQry
                        End If
                        StrQ &= "   JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
                                " AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " "
                        'Sohail (18 May 2019) - [JOIN #TableDepn]


                        'S.SANDEEP |28-OCT-2021| -- START
                        'ISSUE : REPORT OPTIMZATION
                        'If xDateJoinQry.Trim.Length > 0 Then
                        '    StrQ &= xDateJoinQry
                        'End If

                        'If xUACQry.Trim.Length > 0 Then
                        '    StrQ &= xUACQry
                        'End If

                        'If xAdvanceJoinQry.Trim.Length > 0 Then
                        '    StrQ &= xAdvanceJoinQry
                        'End If

                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry
                        End If                    
                        'S.SANDEEP |28-OCT-2021| -- END
                        

                        StrQ &= mstrAnalysis_Join

                        If mblnShowOnlyInActiveDependants = False AndAlso mblnExemptionWise = False Then
                            StrQ &= "WHERE ISNULL(mddependant_exception.isvoid,0) = 0 AND ISNULL(hrdependants_beneficiaries_tran.isvoid, 0) = 0 AND mddependant_exception.isleave = 1 AND (mddependant_exception.lvstopdate IS NULL OR  Convert(Char(8),mddependant_exception.lvstopdate,112)  " & mstrCondition & "@asondate)" & _
                                    " AND #TableDepn.isactive = 1 "
                            'Sohail (18 May 2019) - [isactive]
                        ElseIf mblnShowOnlyInActiveDependants AndAlso mblnExemptionWise Then
                            StrQ &= "WHERE ISNULL(mddependant_exception.isvoid,0) = 0 AND ISNULL(hrdependants_beneficiaries_tran.isvoid, 0) = 0 AND mddependant_exception.isleave = 1 AND  Convert(Char(8),mddependant_exception.lvstopdate,112)  " & mstrCondition & "@asondate" & _
                                    " AND #TableDepn.isactive = 0 "
                            'Sohail (18 May 2019) - [isactive]
                        End If

                        If iFilterText.Trim.Length > 0 Then
                            StrQ &= iFilterText
                        End If

                        If mstrAdvance_Filter.Trim.Length > 0 Then
                            StrQ &= " AND " & mstrAdvance_Filter
                        End If

                        If xUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " AND " & xUACFiltrQry & " "
                        End If

                        If mblIsActive = False Then
                            If xDateFilterQry.Trim.Length > 0 Then
                                StrQ &= xDateFilterQry & " "
                            End If
                        End If

                        FilterTitleAndFilterQueryForDependent(True)
                        StrQ &= Me._FilterQuery
                    End If

                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    StrQ &= " DROP TABLE #TableDepn "
                    'Sohail (18 May 2019) -- End

                Case 1  'Referee


                    StrQ = "SELECT  ISNULL(hremployee_master.employeecode, '') AS ECode "
                    If mblnFirstNamethenSurname = False Then
                        StrQ &= ",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
                    Else
                        StrQ &= ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
                    End If

                    StrQ &= ",ISNULL(hremployee_referee_tran.name,'') AS DRName " & _
                            ",ISNULL(cfcommon_master.name,'') AS Relation " & _
                            ",ISNULL(hremployee_referee_tran.address,'') AS Address " & _
                            ",CASE WHEN ISNULL(hremployee_referee_tran.telephone_no, '') = '' THEN ISNULL(hremployee_referee_tran.mobile_no, '') " & _
                            " ELSE ISNULL(hremployee_referee_tran.telephone_no, '') END AS Phone " & _
                            ",hremployee_master.employeeunkid AS EmpId " & _
                            ",cfcommon_master.masterunkid AS RelId " & _
                            ",'' AS birthdate "

                    If mintViewIndex > 0 Then
                        StrQ &= mstrAnalysis_Fields
                    Else
                        StrQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrQ &= "FROM hremployee_referee_tran " & _
                            "   JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid "
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    StrQ &= "   JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid " & _
                            "AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If
                   

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If

                    
                    'S.SANDEEP |28-OCT-2021| -- END
                    

                    StrQ &= mstrAnalysis_Join

                    StrQ &= "WHERE   ISNULL(hremployee_referee_tran.isvoid, 0) = 0 "

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry & " "
                    End If

                    If mblIsActive = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQ &= xDateFilterQry & " "
                        End If
                    End If

                    Call FilterTitleAndFilterQuery()

                    StrQ &= Me._FilterQuery

            End Select

            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [15-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : ERROR RELATED TO, AS ON DATE DECLARTIAON
            objDataOperation.AddParameter("@asondate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsonDate))
            'S.SANDEEP [15-SEP-2017] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim strEmpName As String = String.Empty
            Dim intCnt As Integer = 1


            'Pinkal (16-AUG-2014) -- Start
            'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
            Dim objException As New clsdependant_exception
            'Pinkal (16-AUG-2014) -- End

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                If strEmpName <> dtRow.Item("EName") Then
                    intCnt = 1
                    strEmpName = dtRow.Item("EName")
                End If

                rpt_Rows.Item("Column1") = dtRow.Item("ECode")
                rpt_Rows.Item("Column2") = dtRow.Item("EName")
                rpt_Rows.Item("Column3") = dtRow.Item("DRName")
                rpt_Rows.Item("Column4") = dtRow.Item("Relation")
                If dtRow.Item("ECode").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column5") = intCnt.ToString & ". "
                Else
                    rpt_Rows.Item("Column5") = ""
                End If
                rpt_Rows.Item("Column6") = dtRow.Item("Address")
                rpt_Rows.Item("Column7") = dtRow.Item("Phone")

                'Sandeep [ 12 MARCH 2011 ] -- Start
                rpt_Rows.Item("Column8") = dtRow.Item("GName")
                'Sandeep [ 12 MARCH 2011 ] -- End 

                'Sohail (06 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                If dtRow.Item("birthdate").ToString.Trim <> "" Then
                    rpt_Rows.Item("Column9") = eZeeDate.convertDate(dtRow.Item("birthdate").ToString).ToShortDateString
                    'S.SANDEEP |09-APR-2019| -- START
                    'rpt_Rows.Item("Column10") = DateDiff(DateInterval.Year, eZeeDate.convertDate(dtRow.Item("birthdate").ToString), DateAndTime.Now.Date)
                    rpt_Rows.Item("Column10") = dtRow("AgeYears")
                    'S.SANDEEP |09-APR-2019| -- END
                Else
                    rpt_Rows.Item("Column9") = ""
                    rpt_Rows.Item("Column10") = ""
                End If
                'Sohail (06 Aug 2013) -- End

                'Pinkal (16-AUG-2014) -- Start
                'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
                If mintReportTypeId = 0 Then

                    rpt_Rows.Item("Column11") = CBool(objException.GetDependantExceptionModule(enModuleReference.Medical, CInt(dtRow("EmpId")), CInt(dtRow("DpndtTranId")), CInt(dtRow("RelId"))))
                    Dim mdtMedicalStopDate As DateTime = objException.GetDependantModuleWiseStopDate(enModuleReference.Medical, CInt(dtRow("EmpId")), CInt(dtRow("DpndtTranId")), CInt(dtRow("RelId")))
                    rpt_Rows.Item("Column12") = IIf(mdtMedicalStopDate <> Nothing, mdtMedicalStopDate.ToShortDateString, DBNull.Value)

                    rpt_Rows.Item("Column13") = CBool(objException.GetDependantExceptionModule(enModuleReference.Leave, CInt(dtRow("EmpId")), CInt(dtRow("DpndtTranId")), CInt(dtRow("RelId"))))
                    Dim mdtLeaveStopDate As DateTime = objException.GetDependantModuleWiseStopDate(enModuleReference.Leave, CInt(dtRow("EmpId")), CInt(dtRow("DpndtTranId")), CInt(dtRow("RelId")))
                    rpt_Rows.Item("Column14") = IIf(mdtLeaveStopDate <> Nothing, mdtLeaveStopDate.ToShortDateString, DBNull.Value)

                End If
                'Pinkal (16-AUG-2014) -- End

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                intCnt += 1
            Next


            'Pinkal (16-AUG-2014) -- Start
            'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report

            If mintReportTypeId = 0 Then
                objRpt = New ArutiReport.Designer.rptEmpActiveDependantsReport
            ElseIf mintReportTypeId = 1 Then
                objRpt = New ArutiReport.Designer.rptEmpRelationReport
            End If

            'Pinkal (16-AUG-2014) -- End



            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 18, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 19, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 20, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 21, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            'S.SANDEEP [16-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 16
            'Private  As Boolean = False
            If mintReportTypeId = 0 Then
                If mblnShowSubAndGrandTotalRelationWise = False Then
                    ReportFunction.TextChange(objRpt, "txtTotalEmployee", Language.getMessage(mstrModuleName, 29, "Total Employee"))
                    Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
                    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
                End If
            End If
            'S.SANDEEP [16-SEP-2017] -- END

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 1, "Code"))
            ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 2, "Employee :"))
            Select Case mintReportTypeId
                Case 0

                    'Pinkal (16-AUG-2014) -- Start
                    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
                    'ReportFunction.TextChange(objRpt, "txtDependant_txtRefreeName", Language.getMessage(mstrModuleName, 3, "Dependant Name"))
                    ReportFunction.TextChange(objRpt, "txtDependantName", Language.getMessage(mstrModuleName, 3, "Dependant Name"))
                    'Pinkal (16-AUG-2014) -- End

                Case 1
                    ReportFunction.TextChange(objRpt, "txtDependant_txtRefreeName", Language.getMessage(mstrModuleName, 4, "Referee Name"))
            End Select
            ReportFunction.TextChange(objRpt, "txtRelation", Language.getMessage(mstrModuleName, 5, "Relation"))
            ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 6, "Address"))
            ReportFunction.TextChange(objRpt, "txtTel_txtMobNo", Language.getMessage(mstrModuleName, 7, "Tel./Mobile No."))

            'Sohail (06 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            Select Case mintReportTypeId
                Case 0  'Dependant
                    ReportFunction.TextChange(objRpt, "txtBirthDate", Language.getMessage(mstrModuleName, 16, "Birth Date"))
                    ReportFunction.TextChange(objRpt, "txtAge", Language.getMessage(mstrModuleName, 17, "Age"))

                    'Pinkal (16-AUG-2014) -- Start
                    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report

                    ReportFunction.TextChange(objRpt, "txtIsMedical", Language.getMessage(mstrModuleName, 24, "Medical"))
                    ReportFunction.TextChange(objRpt, "txtMdStopDate", Language.getMessage(mstrModuleName, 25, "Stop Date"))
                    ReportFunction.TextChange(objRpt, "txtIsLeave", Language.getMessage(mstrModuleName, 27, "Leave"))
                    ReportFunction.TextChange(objRpt, "txtLVStopDate", Language.getMessage(mstrModuleName, 25, "Stop Date"))

                    'Pinkal (16-AUG-2014) -- End

                Case 1  'Referee
                    ReportFunction.TextChange(objRpt, "txtBirthDate", "")
                    ReportFunction.TextChange(objRpt, "txtAge", "")
            End Select
            'Sohail (06 Aug 2013) -- End


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 8, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 9, "Printed Date :"))


            'Pinkal (16-AUG-2014) -- Start
            'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
            If mintReportTypeId = 1 Then
                Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 10, "Page :"))
            End If
            'Pinkal (16-AUG-2014) -- End



            'Sandeep [ 12 MARCH 2011 ] -- Start
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 17, "Branch :"))
            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 18, "Department :"))
            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 19, "Section :"))
            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 20, "Unit :"))
            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 21, "Job :"))
            '        'S.SANDEEP [ 06 SEP 2011 ] -- START
            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 22, "Cost Center :"))
            '        'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    Case Else
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", "")
            'End Select
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'Sohail (16 May 2012) -- End
            'Sandeep [ 12 MARCH 2011 ] -- End 

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " - [ " & mstrReportTypeName & " ]")
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Code")
            Language.setMessage(mstrModuleName, 2, "Employee :")
            Language.setMessage(mstrModuleName, 3, "Dependant Name")
            Language.setMessage(mstrModuleName, 4, "Referee Name")
            Language.setMessage(mstrModuleName, 5, "Relation")
            Language.setMessage(mstrModuleName, 6, "Address")
            Language.setMessage(mstrModuleName, 7, "Tel./Mobile No.")
            Language.setMessage(mstrModuleName, 8, "Printed By :")
            Language.setMessage(mstrModuleName, 9, "Printed Date :")
            Language.setMessage(mstrModuleName, 10, "Page :")
            Language.setMessage(mstrModuleName, 11, "Employee :")
            Language.setMessage(mstrModuleName, 12, "Firstname :")
            Language.setMessage(mstrModuleName, 13, "Lastname :")
            Language.setMessage(mstrModuleName, 14, "Name :")
            Language.setMessage(mstrModuleName, 15, "Relation :")
            Language.setMessage(mstrModuleName, 16, "Birth Date")
            Language.setMessage(mstrModuleName, 17, "Age")
            Language.setMessage(mstrModuleName, 18, "Prepared By :")
            Language.setMessage(mstrModuleName, 19, "Checked By :")
            Language.setMessage(mstrModuleName, 20, "Approved By :")
            Language.setMessage(mstrModuleName, 21, "Received By :")
            Language.setMessage(mstrModuleName, 22, "View Type :")
            Language.setMessage(mstrModuleName, 23, "Condition :")
            Language.setMessage(mstrModuleName, 24, "Medical")
            Language.setMessage(mstrModuleName, 25, "Stop Date")
            Language.setMessage(mstrModuleName, 26, "As on Date :")
            Language.setMessage(mstrModuleName, 27, "Leave")
            Language.setMessage(mstrModuleName, 28, "InActive Dependent Option :")
            Language.setMessage(mstrModuleName, 29, "Total Employee")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
