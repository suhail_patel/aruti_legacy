Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsEmployeeIdentitiesReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeIdentitiesReport"
    Private mstrReportId As String = enArutiReport.EmployeeIdentityReport
    Dim objDataOperation As clsDataOperation

#Region "Constructor"
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region "Private Variables"

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintEmployeeID As Integer = 0
    Private mstrEmployee As String = String.Empty
    Private mdtExpiryDateFrom As DateTime = Nothing
    Private mdtExpiryDateTo As DateTime = Nothing
    Private mdtIssueDateFrom As DateTime = Nothing
    Private mdtIssueDateTo As DateTime = Nothing
    Private mstrOrderByQuery As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END


    'Pinkal (19-AUG-2014) -- Start
    'Enhancement - Added Include Inactive Employee
    Private mblIsActive As Boolean = True
    'Pinkal (19-AUG-2014) -- End

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End
#End Region

#Region "Properties"

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _Report_Type_Name() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property

    Public WriteOnly Property _Employee() As String
        Set(ByVal value As String)
            mstrEmployee = value
        End Set
    End Property

    Public WriteOnly Property _ExpiryDateFrom() As DateTime
        Set(ByVal value As DateTime)
            mdtExpiryDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ExpiryDateTo() As DateTime
        Set(ByVal value As DateTime)
            mdtExpiryDateTo = value
        End Set
    End Property

    Public WriteOnly Property _IssueDateFrom() As DateTime
        Set(ByVal value As DateTime)
            mdtIssueDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _IssueDateTo() As DateTime
        Set(ByVal value As DateTime)
            mdtIssueDateTo = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Pinkal (19-AUG-2014) -- Start
    'Enhancement - Added Include Inactive Employee

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblIsActive = value
        End Set
    End Property

    'Pinkal (19-AUG-2014) -- End
    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End
#End Region

#Region "Public Function & Procedures"

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            mintEmployeeID = 0
            mstrEmployee = ""
            mdtExpiryDateFrom = Nothing
            mdtExpiryDateTo = Nothing
            mdtIssueDateFrom = Nothing
            mdtIssueDateTo = Nothing
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""

            'Pinkal (19-AUG-2014) -- Start
            'Enhancement - Added Include Inactive Employee
            mblIsActive = False
            'Pinkal (19-AUG-2014) -- End
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Dim strFilter As String = ""
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try


            If mblIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If mintEmployeeID > 0 Then
                objDataOperation.AddParameter("@EmployeeID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeID)
                Me._FilterQuery &= " AND hremployee_idinfo_tran.employeeunkid = @EmployeeID "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Employee : ") & " " & mstrEmployee & " "
            End If

            If mdtExpiryDateFrom <> Nothing Then
                objDataOperation.AddParameter("@ExpiryDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtExpiryDateFrom))
                Me._FilterQuery &= " AND Convert(char(8),hremployee_idinfo_tran.expiry_date,112) >= @ExpiryDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Expiry Date From : ") & " " & mdtExpiryDateFrom.Date & " "
            End If

            If mdtExpiryDateTo <> Nothing Then
                objDataOperation.AddParameter("@ExpiryDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtExpiryDateTo))
                Me._FilterQuery &= " AND Convert(char(8),hremployee_idinfo_tran.expiry_date,112) <= @ExpiryDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Expiry Date To : ") & " " & mdtExpiryDateTo.Date & " "
            End If

            If mdtIssueDateFrom <> Nothing Then
                objDataOperation.AddParameter("@IssueDateFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtIssueDateFrom))
                Me._FilterQuery &= " AND Convert(char(8),hremployee_idinfo_tran.issue_date,112) >= @IssueDateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Issue Date From : ") & " " & mdtIssueDateFrom.Date & " "
            End If

            If mdtIssueDateTo <> Nothing Then
                objDataOperation.AddParameter("@IssueDateTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtIssueDateTo))
                Me._FilterQuery &= " AND Convert(char(8),hremployee_idinfo_tran.issue_date,112) <= @IssueDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Issue Date To : ") & " " & mdtIssueDateTo.Date & " "
            End If


            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= " ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_EmployeeIdentityDetailReport()
        '    Rpt = objRpt


        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objRpt = Generate_EmployeeIdentityDetailReport()
            objRpt = Generate_EmployeeIdentityDetailReport(xDatabaseName, _
                                                           xUserUnkid, _
                                                           xYearUnkid, _
                                                           xCompanyUnkid, _
                                                           xPeriodStart, _
                                                           xPeriodEnd, _
                                                           xUserModeSetting, _
                                                           xOnlyApproved)
            'Shani(24-Aug-2015) -- End
            Rpt = objRpt


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""

        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Report Generation"
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Public Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("hremployee_master.employeecode", Language.getMessage(mstrModuleName, 22, "Employee Code")))
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 23, "Employee")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'') + ' '+ ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 23, "Employee")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 23, "Employee")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            iColumn_DetailReport.Add(New IColumn("ISNULL(cfcommon_master.name,'')", Language.getMessage(mstrModuleName, 7, "Identity Type")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_idinfo_tran.serial_no,'')", Language.getMessage(mstrModuleName, 8, "Serial No")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_idinfo_tran.identity_no,'')", Language.getMessage(mstrModuleName, 9, "Identity No")))
            iColumn_DetailReport.Add(New IColumn("hremployee_idinfo_tran.issue_date", Language.getMessage(mstrModuleName, 11, "Issue Date")))
            iColumn_DetailReport.Add(New IColumn("hremployee_idinfo_tran.expiry_date", Language.getMessage(mstrModuleName, 12, "Expiry Date")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function Generate_EmployeeIdentityDetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Public Function Generate_EmployeeIdentityDetailReport(ByVal strDatabaseName As String, _
                                                          ByVal intUserUnkid As Integer, _
                                                          ByVal intYearUnkid As Integer, _
                                                          ByVal intCompanyUnkid As Integer, _
                                                          ByVal dtPeriodStart As Date, _
                                                          ByVal dtPeriodEnd As Date, _
                                                          ByVal strUserModeSetting As String, _
                                                          ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Try


            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            StrQ = "SELECT  ISNULL(hremployee_master.employeecode, '') AS Employeecode "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            '", ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee "
            End If

            StrQ &= ", ISNULL(cfcommon_master.name,'') AS [Identity Type] " & _
                   ", ISNULL(hremployee_idinfo_tran.serial_no,'') AS SerialNo " & _
                   ", ISNULL(hremployee_idinfo_tran.identity_no,'') AS IdentityNo " & _
                   ", ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
                   ", ISNULL(hremployee_idinfo_tran.issued_place,'') AS [Issued Place] " & _
                   ", hremployee_idinfo_tran.issue_date AS [Issued Date] " & _
                   ", hremployee_idinfo_tran.expiry_date AS [Expiry Date] "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM hremployee_idinfo_tran " & _
                       " LEFT JOIN hremployee_master ON hremployee_idinfo_tran.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If mblIsActive = False Then
                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.IDENTITY_TYPES & _
                       " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_idinfo_tran.countryunkid "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= mstrAnalysis_Join

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If mblIsActive = False Then
            '    If xDateJoinQry.Trim.Length > 0 Then
            '        StrQ &= xDateJoinQry
            '    End If
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            'Shani(24-Aug-2015) -- End

            StrQ &= " WHERE 1 = 1 "

            'Pinkal (19-AUG-2014) -- Start
            'Enhancement - Added Include Inactive Employee

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mblIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Shani(24-Aug-2015) -- End

            'Pinkal (19-AUG-2014) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("Employeecode")
                rpt_Row.Item("Column2") = dtRow.Item("Employee")
                rpt_Row.Item("Column3") = dtRow.Item("Identity Type")
                rpt_Row.Item("Column4") = dtRow.Item("SerialNo")
                rpt_Row.Item("Column5") = dtRow.Item("IdentityNo")
                rpt_Row.Item("Column6") = dtRow.Item("Country")
                rpt_Row.Item("Column7") = dtRow.Item("Issued Place")

                If Not IsDBNull(dtRow.Item("Issued Date")) Then
                    rpt_Row.Item("Column8") = CDate(dtRow.Item("Issued Date")).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("Expiry Date")) Then
                    rpt_Row.Item("Column9") = CDate(dtRow.Item("Expiry Date")).ToShortDateString
                End If

                rpt_Row.Item("Column10") = dtRow.Item("Id")
                rpt_Row.Item("Column11") = dtRow.Item("GName")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeIdentities

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 5, "Employee Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 6, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtIdentityType", Language.getMessage(mstrModuleName, 7, "Identity Type"))
            Call ReportFunction.TextChange(objRpt, "txtSerialNo", Language.getMessage(mstrModuleName, 8, "Serial No"))
            Call ReportFunction.TextChange(objRpt, "txtIdentityNo", Language.getMessage(mstrModuleName, 9, "Identity No"))
            Call ReportFunction.TextChange(objRpt, "txtCountry", Language.getMessage(mstrModuleName, 10, "Country"))
            Call ReportFunction.TextChange(objRpt, "txtIssuePlace", Language.getMessage(mstrModuleName, 24, "Place of Issue"))
            Call ReportFunction.TextChange(objRpt, "txtIssueDate", Language.getMessage(mstrModuleName, 11, "Issue Date"))
            Call ReportFunction.TextChange(objRpt, "txtExpiryDate", Language.getMessage(mstrModuleName, 12, "Expiry Date"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyname", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EmployeeIdentityDetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By :")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Employee Code :")
            Language.setMessage(mstrModuleName, 6, "Employee :")
            Language.setMessage(mstrModuleName, 7, "Identity Type")
            Language.setMessage(mstrModuleName, 8, "Serial No")
            Language.setMessage(mstrModuleName, 9, "Identity No")
            Language.setMessage(mstrModuleName, 10, "Country")
            Language.setMessage(mstrModuleName, 11, "Issue Date")
            Language.setMessage(mstrModuleName, 12, "Expiry Date")
            Language.setMessage(mstrModuleName, 13, "Printed By :")
            Language.setMessage(mstrModuleName, 14, "Printed Date :")
            Language.setMessage(mstrModuleName, 16, "Employee :")
            Language.setMessage(mstrModuleName, 17, "Expiry Date From :")
            Language.setMessage(mstrModuleName, 18, "Expiry Date To :")
            Language.setMessage(mstrModuleName, 19, "Issue Date From :")
            Language.setMessage(mstrModuleName, 20, "Issue Date To :")
            Language.setMessage(mstrModuleName, 21, "Order By :")
            Language.setMessage(mstrModuleName, 22, "Employee Code")
            Language.setMessage(mstrModuleName, 23, "Employee")
            Language.setMessage(mstrModuleName, 24, "Place of Issue")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
