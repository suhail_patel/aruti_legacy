﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeDetail_withED
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowEmpScale = New System.Windows.Forms.CheckBox
        Me.objbtnSearchMembership = New eZee.Common.eZeeGradientButton
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.LblMembership = New System.Windows.Forms.Label
        Me.objbtnsearchTranHead3 = New eZee.Common.eZeeGradientButton
        Me.cboTranHead3 = New System.Windows.Forms.ComboBox
        Me.LblTranHead3 = New System.Windows.Forms.Label
        Me.objbtnsearchTranHead2 = New eZee.Common.eZeeGradientButton
        Me.cboTranHead2 = New System.Windows.Forms.ComboBox
        Me.LblTranHead2 = New System.Windows.Forms.Label
        Me.objbtnsearchTranHead1 = New eZee.Common.eZeeGradientButton
        Me.cboTranHead1 = New System.Windows.Forms.ComboBox
        Me.LblTranHead1 = New System.Windows.Forms.Label
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.LblPeriod = New System.Windows.Forms.Label
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.objbtnSearchJob = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchDept = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmp = New eZee.Common.eZeeGradientButton
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 456)
        Me.NavPanel.Size = New System.Drawing.Size(612, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowEmpScale)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchMembership)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.LblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnsearchTranHead3)
        Me.gbFilterCriteria.Controls.Add(Me.cboTranHead3)
        Me.gbFilterCriteria.Controls.Add(Me.LblTranHead3)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnsearchTranHead2)
        Me.gbFilterCriteria.Controls.Add(Me.cboTranHead2)
        Me.gbFilterCriteria.Controls.Add(Me.LblTranHead2)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnsearchTranHead1)
        Me.gbFilterCriteria.Controls.Add(Me.cboTranHead1)
        Me.gbFilterCriteria.Controls.Add(Me.LblTranHead1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.LblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchJob)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchDept)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmp)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.lblJob)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(389, 292)
        Me.gbFilterCriteria.TabIndex = 17
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowEmpScale
        '
        Me.chkShowEmpScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmpScale.Location = New System.Drawing.Point(78, 267)
        Me.chkShowEmpScale.Name = "chkShowEmpScale"
        Me.chkShowEmpScale.Size = New System.Drawing.Size(291, 17)
        Me.chkShowEmpScale.TabIndex = 200
        Me.chkShowEmpScale.Text = "Show Employee Scale"
        Me.chkShowEmpScale.UseVisualStyleBackColor = True
        '
        'objbtnSearchMembership
        '
        Me.objbtnSearchMembership.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMembership.BorderSelected = False
        Me.objbtnSearchMembership.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMembership.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMembership.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMembership.Location = New System.Drawing.Point(359, 138)
        Me.objbtnSearchMembership.Name = "objbtnSearchMembership"
        Me.objbtnSearchMembership.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMembership.TabIndex = 198
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 300
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(78, 138)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(275, 21)
        Me.cboMembership.TabIndex = 197
        '
        'LblMembership
        '
        Me.LblMembership.BackColor = System.Drawing.Color.Transparent
        Me.LblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMembership.Location = New System.Drawing.Point(8, 142)
        Me.LblMembership.Name = "LblMembership"
        Me.LblMembership.Size = New System.Drawing.Size(68, 13)
        Me.LblMembership.TabIndex = 196
        Me.LblMembership.Text = "Membership"
        '
        'objbtnsearchTranHead3
        '
        Me.objbtnsearchTranHead3.BackColor = System.Drawing.Color.Transparent
        Me.objbtnsearchTranHead3.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnsearchTranHead3.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnsearchTranHead3.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnsearchTranHead3.BorderSelected = False
        Me.objbtnsearchTranHead3.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnsearchTranHead3.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnsearchTranHead3.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnsearchTranHead3.Location = New System.Drawing.Point(359, 220)
        Me.objbtnsearchTranHead3.Name = "objbtnsearchTranHead3"
        Me.objbtnsearchTranHead3.Size = New System.Drawing.Size(21, 21)
        Me.objbtnsearchTranHead3.TabIndex = 195
        '
        'cboTranHead3
        '
        Me.cboTranHead3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTranHead3.DropDownWidth = 300
        Me.cboTranHead3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead3.FormattingEnabled = True
        Me.cboTranHead3.Location = New System.Drawing.Point(78, 220)
        Me.cboTranHead3.Name = "cboTranHead3"
        Me.cboTranHead3.Size = New System.Drawing.Size(275, 21)
        Me.cboTranHead3.TabIndex = 194
        '
        'LblTranHead3
        '
        Me.LblTranHead3.BackColor = System.Drawing.Color.Transparent
        Me.LblTranHead3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTranHead3.Location = New System.Drawing.Point(8, 224)
        Me.LblTranHead3.Name = "LblTranHead3"
        Me.LblTranHead3.Size = New System.Drawing.Size(68, 13)
        Me.LblTranHead3.TabIndex = 193
        Me.LblTranHead3.Text = "Tran. Head3"
        '
        'objbtnsearchTranHead2
        '
        Me.objbtnsearchTranHead2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnsearchTranHead2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnsearchTranHead2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnsearchTranHead2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnsearchTranHead2.BorderSelected = False
        Me.objbtnsearchTranHead2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnsearchTranHead2.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnsearchTranHead2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnsearchTranHead2.Location = New System.Drawing.Point(359, 193)
        Me.objbtnsearchTranHead2.Name = "objbtnsearchTranHead2"
        Me.objbtnsearchTranHead2.Size = New System.Drawing.Size(21, 21)
        Me.objbtnsearchTranHead2.TabIndex = 192
        '
        'cboTranHead2
        '
        Me.cboTranHead2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTranHead2.DropDownWidth = 300
        Me.cboTranHead2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead2.FormattingEnabled = True
        Me.cboTranHead2.Location = New System.Drawing.Point(78, 193)
        Me.cboTranHead2.Name = "cboTranHead2"
        Me.cboTranHead2.Size = New System.Drawing.Size(275, 21)
        Me.cboTranHead2.TabIndex = 191
        '
        'LblTranHead2
        '
        Me.LblTranHead2.BackColor = System.Drawing.Color.Transparent
        Me.LblTranHead2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTranHead2.Location = New System.Drawing.Point(8, 197)
        Me.LblTranHead2.Name = "LblTranHead2"
        Me.LblTranHead2.Size = New System.Drawing.Size(68, 13)
        Me.LblTranHead2.TabIndex = 190
        Me.LblTranHead2.Text = "Tran. Head2"
        '
        'objbtnsearchTranHead1
        '
        Me.objbtnsearchTranHead1.BackColor = System.Drawing.Color.Transparent
        Me.objbtnsearchTranHead1.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnsearchTranHead1.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnsearchTranHead1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnsearchTranHead1.BorderSelected = False
        Me.objbtnsearchTranHead1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnsearchTranHead1.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnsearchTranHead1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnsearchTranHead1.Location = New System.Drawing.Point(359, 166)
        Me.objbtnsearchTranHead1.Name = "objbtnsearchTranHead1"
        Me.objbtnsearchTranHead1.Size = New System.Drawing.Size(21, 21)
        Me.objbtnsearchTranHead1.TabIndex = 189
        '
        'cboTranHead1
        '
        Me.cboTranHead1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTranHead1.DropDownWidth = 300
        Me.cboTranHead1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead1.FormattingEnabled = True
        Me.cboTranHead1.Location = New System.Drawing.Point(78, 166)
        Me.cboTranHead1.Name = "cboTranHead1"
        Me.cboTranHead1.Size = New System.Drawing.Size(275, 21)
        Me.cboTranHead1.TabIndex = 188
        '
        'LblTranHead1
        '
        Me.LblTranHead1.BackColor = System.Drawing.Color.Transparent
        Me.LblTranHead1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTranHead1.Location = New System.Drawing.Point(8, 170)
        Me.LblTranHead1.Name = "LblTranHead1"
        Me.LblTranHead1.Size = New System.Drawing.Size(68, 13)
        Me.LblTranHead1.TabIndex = 187
        Me.LblTranHead1.Text = "Tran. Head1"
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(359, 111)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 186
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 300
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(78, 111)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(275, 21)
        Me.cboPeriod.TabIndex = 185
        '
        'LblPeriod
        '
        Me.LblPeriod.BackColor = System.Drawing.Color.Transparent
        Me.LblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPeriod.Location = New System.Drawing.Point(8, 115)
        Me.LblPeriod.Name = "LblPeriod"
        Me.LblPeriod.Size = New System.Drawing.Size(68, 13)
        Me.LblPeriod.TabIndex = 184
        Me.LblPeriod.Text = "Period"
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(78, 247)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(291, 16)
        Me.chkInactiveemp.TabIndex = 182
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'objbtnSearchJob
        '
        Me.objbtnSearchJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJob.BorderSelected = False
        Me.objbtnSearchJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJob.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJob.Location = New System.Drawing.Point(359, 84)
        Me.objbtnSearchJob.Name = "objbtnSearchJob"
        Me.objbtnSearchJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJob.TabIndex = 180
        '
        'objbtnSearchDept
        '
        Me.objbtnSearchDept.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDept.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDept.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDept.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDept.BorderSelected = False
        Me.objbtnSearchDept.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDept.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDept.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDept.Location = New System.Drawing.Point(359, 57)
        Me.objbtnSearchDept.Name = "objbtnSearchDept"
        Me.objbtnSearchDept.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDept.TabIndex = 179
        '
        'objbtnSearchEmp
        '
        Me.objbtnSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmp.BorderSelected = False
        Me.objbtnSearchEmp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmp.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmp.Location = New System.Drawing.Point(359, 30)
        Me.objbtnSearchEmp.Name = "objbtnSearchEmp"
        Me.objbtnSearchEmp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmp.TabIndex = 60
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 300
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(78, 84)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(275, 21)
        Me.cboJob.TabIndex = 177
        '
        'lblJob
        '
        Me.lblJob.BackColor = System.Drawing.Color.Transparent
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(8, 88)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(68, 13)
        Me.lblJob.TabIndex = 176
        Me.lblJob.Text = "Job"
        '
        'lblDepartment
        '
        Me.lblDepartment.BackColor = System.Drawing.Color.Transparent
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 61)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(68, 13)
        Me.lblDepartment.TabIndex = 175
        Me.lblDepartment.Text = "Department"
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.DropDownWidth = 300
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(78, 57)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(275, 21)
        Me.cboDepartment.TabIndex = 174
        '
        'lblEmployee
        '
        Me.lblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 34)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(68, 13)
        Me.lblEmployee.TabIndex = 167
        Me.lblEmployee.Text = "Employee"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(78, 30)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(275, 21)
        Me.cboEmployee.TabIndex = 168
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 364)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(389, 63)
        Me.gbSortBy.TabIndex = 18
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(359, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(68, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(78, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(275, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'frmEmployeeDetail_withED
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(612, 511)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbSortBy)
        Me.Name = "frmEmployeeDetail_withED"
        Me.Text = "frmEmployeeDetail_BBL"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchJob As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchDept As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmp As eZee.Common.eZeeGradientButton
    Public WithEvents cboJob As System.Windows.Forms.ComboBox
    Private WithEvents lblJob As System.Windows.Forms.Label
    Private WithEvents lblDepartment As System.Windows.Forms.Label
    Public WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Private WithEvents lblEmployee As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Public WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Private WithEvents LblPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnsearchTranHead3 As eZee.Common.eZeeGradientButton
    Public WithEvents cboTranHead3 As System.Windows.Forms.ComboBox
    Private WithEvents LblTranHead3 As System.Windows.Forms.Label
    Friend WithEvents objbtnsearchTranHead2 As eZee.Common.eZeeGradientButton
    Public WithEvents cboTranHead2 As System.Windows.Forms.ComboBox
    Private WithEvents LblTranHead2 As System.Windows.Forms.Label
    Friend WithEvents objbtnsearchTranHead1 As eZee.Common.eZeeGradientButton
    Public WithEvents cboTranHead1 As System.Windows.Forms.ComboBox
    Private WithEvents LblTranHead1 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchMembership As eZee.Common.eZeeGradientButton
    Public WithEvents cboMembership As System.Windows.Forms.ComboBox
    Private WithEvents LblMembership As System.Windows.Forms.Label
    Friend WithEvents chkShowEmpScale As System.Windows.Forms.CheckBox
End Class
