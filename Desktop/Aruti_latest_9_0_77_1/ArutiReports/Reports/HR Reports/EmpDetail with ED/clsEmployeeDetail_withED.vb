Imports Aruti.Data
Imports eZeeCommonLib

Public Class clsEmployeeDetail_withED
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeDetail_withED"
    Private mstrReportId As String = enArutiReport.EmployeeDetail_WithED
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintEmployeeId As Integer
    Private mstrEmployeeName As String = String.Empty
    Private mintDepartmentId As Integer
    Private mstrDepartmentName As String = String.Empty
    Private mintJobId As Integer
    Private mstrJobName As String = String.Empty
    Private mintPeriodId As Integer
    Private mstrPeriod As String = String.Empty
    Private mintMembershipId As Integer
    Private mstrMemberShip As String = String.Empty
    Private mintTranHead1 As Integer
    Private mstrTranHead1 As String = String.Empty
    Private mintTranHead2 As Integer
    Private mstrTranHead2 As String = String.Empty
    Private mintTranHead3 As Integer
    Private mstrTranHead3 As String = String.Empty
    Private mblnIncludeInactiveEmp As Boolean = False

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'Pinkal (24-Apr-2013) -- Start
    'Enhancement : TRA Changes
    Private mblnShowEmpScale As Boolean = False
    'Pinkal (24-Apr-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(02-Dec-2015) -- Start
    'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
    Private mdicMembership As Dictionary(Of Integer, String) = Nothing
    Private mdtPeriodStartDt As Date = Nothing
    Private mdtPeriodEndDt As Date = Nothing
    Private mdtTableExcel As DataTable = Nothing
    Private mblnExport As Boolean = False
    Private mstrReportTypeName As String = String.Empty
    'Shani(02-Dec-2015) -- End

    'Shani(21-Dec-2015) -- Start
    'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
    Private mintViewIndex As Integer = -1
    'Private mstrViewByIds As String = String.Empty
    'Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Shani(21-Dec-2015) -- End

#End Region

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentId() As Integer
        Set(ByVal value As Integer)
            mintDepartmentId = value
        End Set
    End Property

    Public WriteOnly Property _Department() As String
        Set(ByVal value As String)
            mstrDepartmentName = value
        End Set
    End Property

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _Job() As String
        Set(ByVal value As String)
            mstrJobName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _Period() As String
        Set(ByVal value As String)
            mstrPeriod = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _Membership() As String
        Set(ByVal value As String)
            mstrMemberShip = value
        End Set
    End Property

    Public WriteOnly Property _TranheadId1() As Integer
        Set(ByVal value As Integer)
            mintTranHead1 = value
        End Set
    End Property

    Public WriteOnly Property _TranHead1() As String
        Set(ByVal value As String)
            mstrTranHead1 = value
        End Set
    End Property

    Public WriteOnly Property _TranheadId2() As Integer
        Set(ByVal value As Integer)
            mintTranHead2 = value
        End Set
    End Property

    Public WriteOnly Property _TranHead2() As String
        Set(ByVal value As String)
            mstrTranHead2 = value
        End Set
    End Property

    Public WriteOnly Property _TranheadId3() As Integer
        Set(ByVal value As Integer)
            mintTranHead3 = value
        End Set
    End Property

    Public WriteOnly Property _TranHead3() As String
        Set(ByVal value As String)
            mstrTranHead3 = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'Pinkal (24-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _ShowEmployeeScale() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmpScale = value
        End Set
    End Property

    'Pinkal (24-Apr-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END


    'Shani(02-Dec-2015) -- Start
    'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
    Public WriteOnly Property _MembershipDic() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicMembership = value
        End Set
    End Property

    Public WriteOnly Property _Period_Start_Date() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDt = value
        End Set
    End Property

    Public WriteOnly Property _Period_End_Date() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDt = value
        End Set
    End Property

    Public WriteOnly Property _IsExport() As Boolean
        Set(ByVal value As Boolean)
            mblnExport = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property
    'Shani(02-Dec-2015) -- End

    'Shani(21-Dec-2015) -- Start
    'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    'Public WriteOnly Property _ViewByIds() As String
    '    Set(ByVal value As String)
    '        mstrViewByIds = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ViewByName() As String
    '    Set(ByVal value As String)
    '        mstrViewByName = value
    '    End Set
    'End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Shani(21-Dec-2015) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintDepartmentId = 0
            mstrDepartmentName = ""
            mintJobId = 0
            mstrJobName = ""
            mintPeriodId = 0
            mstrPeriod = ""
            mintMembershipId = 0
            mstrMemberShip = ""
            mintTranHead1 = 0
            mstrTranHead1 = ""
            mintTranHead2 = 0
            mstrTranHead2 = ""
            mintTranHead3 = 0
            mstrTranHead3 = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            mblnShowEmpScale = False
            'Pinkal (24-Apr-2013) -- End

            'Shani(21-Dec-2015) -- Start
            'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
            'Shani(21-Dec-2015) -- Start
            'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
            mintViewIndex = -1
            'mstrViewByIds = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Shani(21-Dec-2015) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "Period : ") & " " & mstrPeriod & " "
            End If

            If mintMembershipId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Membership : ") & " " & mstrMemberShip & " "
            End If

            If mintTranHead1 > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "Transaction Head 1 : ") & " " & mstrTranHead1 & " "
            End If

            If mintTranHead2 > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "Transaction Head 2 : ") & " " & mstrTranHead2 & " "
            End If

            If mintTranHead3 > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Transaction Head 3 : ") & " " & mstrTranHead3 & " "
            End If
            'Shani(24-Aug-2015) -- End

            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkId = @employeeunkId "
                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintJobId > 0 Then
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Me._FilterQuery &= " AND hremployee_master.jobunkid = @jobunkid "
                Me._FilterQuery &= " AND hrjob_master.jobunkid = @jobunkid "
                'Shani(24-Aug-2015) -- End

                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Job : ") & " " & mstrJobName & " "
            End If

            If mintDepartmentId > 0 Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Me._FilterQuery &= " AND hremployee_master.departmentunkid = @departmentunkid"
                'Anjan (28 Aug 2017) - Start
                'Me._FilterQuery &= " AND hrdepartment_master.departmentunkid = @departmentunkid"
                Me._FilterQuery &= " AND DEPT.departmentunkid = @departmentunkid"
                'Anjan (28 Aug 2017) - End
                'Shani(24-Aug-2015) -- End

                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Department : ") & " " & mstrDepartmentName & " "
            End If
            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            If mintPeriodId > 0 AndAlso mdtPeriodStartDt <> Nothing AndAlso mdtPeriodEndDt <> Nothing Then
                objDataOperation.AddParameter("@periodstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDt))
                objDataOperation.AddParameter("@periodenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDt))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "Period : ") & " " & mstrPeriod & " "
            End If
            'Shani(02-Dec-2015) -- End
            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objRpt = Generate_DetailReport()
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, pintReportType)
            'Shani(24-Aug-2015) -- End

            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'If Not IsNothing(objRpt) Then
            '    Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            'End If
            If mblnExport = True Then
                Dim intArrayColumnWidth As Integer() = Nothing

                ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    intArrayColumnWidth(i) = 125
                Next

                'Shani(21-Dec-2015) -- Start
                'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
                Dim strSubTotal As String = ""
                Dim strarrGroupColumns As String() = Nothing
                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns.Remove("ID")
                    mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                    Dim strGrpCols As String() = {"GName"}
                    strarrGroupColumns = strGrpCols
                    strSubTotal = Language.getMessage(mstrModuleName, 6, "Sub Total :")
                Else
                    mdtTableExcel.Columns.Remove("ID")
                    mdtTableExcel.Columns.Remove("GName")
                End If
                'Shani(21-Dec-2015) -- End

                Call ReportExecute(objRpt, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, , , , strarrGroupColumns, mstrReportTypeName)
            Else
                If Not IsNothing(objRpt) Then
                    Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
                End If
            End If
            'Shani(02-Dec-2015) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()

            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania


            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 14, "Employee Code")))
            ''S.SANDEEP [ 26 MAR 2014 ] -- START
            ''iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 15, "Employee Name")))
            'If mblnFirstNamethenSurname = False Then
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 15, "Employee Name")))
            'Else
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 15, "Employee Name")))
            'End If

            'iColumn_DetailReport.Add(New IColumn("hrdepartment_master.name", Language.getMessage(mstrModuleName, 16, "Department")))
            'iColumn_DetailReport.Add(New IColumn("hrjob_master.job_name", Language.getMessage(mstrModuleName, 17, "Job Title")))

            iColumn_DetailReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 15, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("Department", Language.getMessage(mstrModuleName, 16, "Department")))
            iColumn_DetailReport.Add(New IColumn("Job", Language.getMessage(mstrModuleName, 17, "Job Title")))
            'Shani(02-Dec-2015) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intReportType As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass

        'Shani(02-Dec-2015) -- [ByVal intReportType As Integer]

        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            objDataOperation.ClearParameters()

            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'StrQ = "SELECT  Employeecode "
            If mintMembershipId > 0 OrElse mdicMembership IsNot Nothing Then
                StrQ = "SELECT DISTINCT  Employeecode "
            Else
                StrQ = "SELECT  Employeecode "
            End If
            'Shani(02-Dec-2015) -- End

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') 'EmpName' " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS EmpName "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS EmpName "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'StrQ &= ",ISNULL(hrdepartment_master.name, '') 'Department' " & _
            '          ",ISNULL(hrjob_master.job_name, '') 'Job' " & _
            '          ",ISNULL(CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female END, '') AS 'Gender' " & _
            '          ",ISNULL(cfcommon_master.name, '') AS 'Marital Status' " & _
            '          ",ISNULL(hremployee_master.birthdate, '') 'DOB' " & _
            '          ",ISNULL(hremployee_master.appointeddate, '') 'DOJ' " & _
            '          ",ISNULL(hrmsConfiguration..cfcountry_master.country_name, '') AS 'Nationality' " & _
            '          ", CASE WHEN Len(ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname, '')) > 0 THEN " & _
            '          "           ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname, '') + '-' " & _
            '          " ELSE " & _
            '          "           ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname, '')  " & _
            '          " End " & _
            '          " + ISNULL(hrmsconfiguration..cfbankbranch_master.branchname, '') 'Bank' " & _
            '          ",ISNULL(premployee_bank_tran.accountno, '') 'AccountNo' " & _
            '          ",ISNULL(prsalaryincrement_tran.newscale, 0.00) AS 'Basic Salary' "
            StrQ &= ",ISNULL(DEPT.name, '') 'Department' " & _
                      ",ISNULL(hrjob_master.job_name, '') 'Job' " & _
                      ",ISNULL(CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female END, '') AS 'Gender' " & _
                      ",ISNULL(cfcommon_master.name, '') AS 'Marital Status' " & _
                      ",ISNULL(hremployee_master.birthdate, '') 'DOB' " & _
                      ",ISNULL(hremployee_master.appointeddate, '') 'DOJ' " & _
                      ",ISNULL(hrmsConfiguration..cfcountry_master.country_name, '') AS 'Nationality' " & _
                      ", CASE WHEN Len(ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname, '')) > 0 THEN " & _
                      "           ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname, '') + '-' " & _
                      " ELSE " & _
                      "           ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname, '')  " & _
                      " End " & _
                      " + ISNULL(hrmsconfiguration..cfbankbranch_master.branchname, '') 'Bank' " & _
                      ",ISNULL(premployee_bank_tran.accountno, '') 'AccountNo' " & _
                      ",ISNULL(Scale.newscale, 0.00) AS 'Basic Salary' "

            'Shani(21-Dec-2015) -- [hrdepartment_master.name -- > DEPT.name]

            'Shani(24-Aug-2015) -- End

            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'If mintMembershipId > 0 Then
            '    StrQ &= ",ISNULL(hremployee_meminfo_tran.membershipno, '') 'membership' "
            'Else
            '    StrQ &= ",'' AS 'membership' "
            'End If
            StrQ &= ",ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') AS JoinDate"
            If intReportType = 2 Then
                StrQ &= ",ISNULL(CONVERT(CHAR(8),EocDt.date2,112),'')  as terminationdate "
            End If

            If mdicMembership IsNot Nothing Then
                For Each dic As Integer In mdicMembership.Keys
                    StrQ &= ",ISNULL([" & mdicMembership(dic).Trim & "].membershipno, '') '" & mdicMembership(dic).Trim & "' "
                Next
            ElseIf mintMembershipId > 0 Then
                StrQ &= ",ISNULL(hremployee_meminfo_tran.membershipno, '') 'membership' "
            Else
                StrQ &= ",'' AS 'membership' "
            End If
            'Shani(02-Dec-2015) -- End

            If mintTranHead1 > 0 Then
                StrQ &= ",ISNULL(benfhead1.amount, 0.00) 'Amount1' "
            Else
                StrQ &= ",0.00 AS  'Amount1' "
            End If

            If mintTranHead2 > 0 Then
                StrQ &= ",ISNULL(benfhead2.amount, 0.00) 'Amount2' "
            Else
                StrQ &= ",0.00 AS  'Amount2' "
            End If

            If mintTranHead3 > 0 Then
                StrQ &= ",ISNULL(benfhead3.amount, 0.00) 'Amount3' "
            Else
                StrQ &= ",0.00 AS  'Amount3' "
            End If

            'Shani(21-Dec-2015) -- Start
            'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
            StrQ &= ",ISNULL(CONVERT(CHAR(8), EocDt.date1,112),'') AS 'EOC' "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            'Shani(21-Dec-2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= " FROM hremployee_master " & _
            '          " LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '          " LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '          " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.maritalstatusunkid  AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
            '          " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.nationalityunkid " & _
            '          " LEFT JOIN premployee_bank_tran ON hremployee_master.employeeunkid = premployee_bank_tran.employeeunkid " & _
            '          " LEFT JOIN hrmsconfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsconfiguration..cfbankbranch_master.branchunkid " & _
            '          " LEFT JOIN hrmsconfiguration..cfpayrollgroup_master ON hrmsconfiguration..cfbankbranch_master.bankgroupunkid = hrmsconfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id = " & enPayrollGroupType.Bank & _
            '          " LEFT JOIN prsalaryincrement_tran ON prsalaryincrement_tran.employeeunkid = hremployee_master.employeeunkid  AND prsalaryincrement_tran.periodunkid IN ( " & mintPeriodId & " ) "

            StrQ &= " FROM hremployee_master "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= " LEFT JOIN " & _
                        "( " & _
                            "SELECT " & _
                                " departmentunkid" & _
                                ",employeeunkid " & _
                                ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "FROM hremployee_transfer_tran " & _
                            "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1  " & _
                      " LEFT JOIN hrdepartment_master AS DEPT ON DEPT.departmentunkid=Alloc.departmentunkid" & _
                      " LEFT JOIN " & _
                        "( " & _
                            "SELECT " & _
                                "jobunkid" & _
                                ",employeeunkid " & _
                                ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "FROM hremployee_categorization_tran " & _
                            "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                      " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid" & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.maritalstatusunkid  AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
                      " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.nationalityunkid " & _
                      " LEFT JOIN premployee_bank_tran ON hremployee_master.employeeunkid = premployee_bank_tran.employeeunkid " & _
                      " LEFT JOIN hrmsconfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsconfiguration..cfbankbranch_master.branchunkid " & _
                      " LEFT JOIN hrmsconfiguration..cfpayrollgroup_master ON hrmsconfiguration..cfbankbranch_master.bankgroupunkid = hrmsconfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id = " & enPayrollGroupType.Bank & _
                      " Left Join " & _
                        "(" & _
                            "SELECT " & _
                                " newscale" & _
                                ",employeeunkid " & _
                                ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS rno " & _
                            "FROM prsalaryincrement_tran " & _
                            "WHERE prsalaryincrement_tran.isvoid = 0 " & _
                                "AND prsalaryincrement_tran.periodunkid IN (" & mintPeriodId & ") AND prsalaryincrement_tran.isapproved = 1 " & _
                        ") AS Scale ON Scale.employeeunkid = hremployee_master.employeeunkid AND Scale.rno = 1" & _
                      "LEFT JOIN " & _
                      "    ( " & _
                      "        SELECT " & _
                      "             date1 " & _
                      "            ,date2 " & _
                      "            ,employeeunkid " & _
                      "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "        FROM hremployee_dates_tran " & _
                      "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                      "    )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 "

            'Shani(21-Dec-2015) -- Start
            'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
            '" LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid=Alloc.departmentunkid" & _-- >" LEFT JOIN hrdepartment_master  AS DEPT ON DEPT.departmentunkid=Alloc.departmentunkid" & _
            'Shani(21-Dec-2015) -- End

            'Shani(24-Aug-2015) -- End

            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'If mintMembershipId > 0 Then
            '    StrQ &= " LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid AND membershipunkid = " & mintMembershipId
            'End If
            If mdicMembership IsNot Nothing Then
                For Each dic As Integer In mdicMembership.Keys
                    StrQ &= " LEFT JOIN hremployee_meminfo_tran  AS [" & mdicMembership(dic) & "] ON hremployee_master.employeeunkid = [" & mdicMembership(dic) & "].employeeunkid AND [" & mdicMembership(dic) & "].membershipunkid = " & dic & _
                            " AND [" & mdicMembership(dic) & "].isactive = 1 " & _
                            " AND [" & mdicMembership(dic) & "].isdeleted  = 0 "
                Next
            ElseIf mintMembershipId > 0 Then
                StrQ &= " LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid AND membershipunkid = " & mintMembershipId & _
                " AND hremployee_Meminfo_tran.isactive = 1 " & _
                                     " AND hremployee_meminfo_tran.isdeleted  = 0 "
                'Sohail (07 Dec 2015) - [AND hremployee_Meminfo_tran.isactive = 1 AND hremployee_meminfo_tran.isdeleted  = 0 ]
            End If
            'Shani(02-Dec-2015) -- End

            If mintTranHead1 > 0 Then
                StrQ &= " LEFT JOIN " & _
                           " ( " & _
                                  " SELECT ISNULL(amount,0.00) amount,prpayrollprocess_tran.employeeunkid " & _
                                  " FROM prpayrollprocess_tran " & _
                                  " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid AND payperiodunkid IN ( " & mintPeriodId & " ) " & _
                                  " AND prtnaleave_tran.employeeunkid = prpayrollprocess_tran.employeeunkid AND prtnaleave_tran.isvoid = 0 " & _
                                  " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prtranhead_master.tranheadunkid IN (" & mintTranHead1 & ")" & _
                                  " WHERE prpayrollprocess_tran.isvoid = 0 " & _
                             ") AS benfhead1   " & _
                              "ON benfhead1.employeeunkid = hremployee_master.employeeunkid  "
            End If


            If mintTranHead2 > 0 Then
                StrQ &= " LEFT JOIN " & _
                           " ( " & _
                                  " SELECT ISNULL(amount,0.00) amount,prpayrollprocess_tran.employeeunkid " & _
                                  " FROM prpayrollprocess_tran " & _
                                  " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid AND payperiodunkid IN ( " & mintPeriodId & " ) " & _
                                  " AND prtnaleave_tran.employeeunkid = prpayrollprocess_tran.employeeunkid AND prtnaleave_tran.isvoid = 0 " & _
                                  " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prtranhead_master.tranheadunkid IN (" & mintTranHead2 & ")" & _
                                  " WHERE prpayrollprocess_tran.isvoid = 0 " & _
                             ") AS benfhead2   " & _
                              "ON benfhead2.employeeunkid = hremployee_master.employeeunkid  "
            End If

            If mintTranHead3 > 0 Then
                StrQ &= " LEFT JOIN " & _
                           " ( " & _
                                  " SELECT ISNULL(amount,0.00) amount,prpayrollprocess_tran.employeeunkid " & _
                                  " FROM prpayrollprocess_tran " & _
                                  " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid AND payperiodunkid IN ( " & mintPeriodId & " ) " & _
                                  " AND prtnaleave_tran.employeeunkid = prpayrollprocess_tran.employeeunkid AND prtnaleave_tran.isvoid = 0 " & _
                                  " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prtranhead_master.tranheadunkid IN (" & mintTranHead3 & ")" & _
                                  " WHERE prpayrollprocess_tran.isvoid = 0 " & _
                             ") AS benfhead3   " & _
                              "ON benfhead3.employeeunkid = hremployee_master.employeeunkid  "
            End If
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            'Shani(24-Aug-2015) -- End

            'Shani(21-Dec-2015) -- Start
            'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
            StrQ &= mstrAnalysis_Join
            'Shani(21-Dec-2015) -- End

            StrQ &= " WHERE 1 = 1 "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            'End If

            ''S.SANDEEP [ 13 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            ''S.SANDEEP [ 13 FEB 2013 ] -- END

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'If mblnIncludeInactiveEmp = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry & " "
            '    End If
            'End If
            If intReportType = 0 Then
                If mblnIncludeInactiveEmp = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If
            ElseIf intReportType = 1 Then
                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) BETWEEN @periodstartdate AND @periodenddate "
            ElseIf intReportType = 2 Then
                StrQ &= " AND CONVERT(CHAR(8),EocDt.date2,112) BETWEEN @periodstartdate AND @periodenddate "
            End If
            'Shani(02-Dec-2015) -- End



            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery


            objDataOperation.AddParameter("@Male", SqlDbType.NChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            If mblnExport = True Then GoTo export
            'Shani(02-Dec-2015) -- End

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("Employeecode")
                rpt_Rows.Item("Column2") = dtRow.Item("EmpName")
                rpt_Rows.Item("Column3") = dtRow.Item("Department")
                rpt_Rows.Item("Column4") = dtRow.Item("Job")
                rpt_Rows.Item("Column5") = dtRow.Item("Gender")
                rpt_Rows.Item("Column6") = dtRow.Item("Marital Status")

                If IsDBNull(dtRow.Item("DOB")) Then
                    rpt_Rows.Item("Column7") = ""
                Else
                    rpt_Rows.Item("Column7") = CDate(dtRow.Item("DOB")).ToShortDateString
                End If

                If IsDBNull(dtRow.Item("DOJ")) Then
                    rpt_Rows.Item("Column8") = ""
                Else
                    rpt_Rows.Item("Column8") = CDate(dtRow.Item("DOJ")).ToShortDateString
                End If

                rpt_Rows.Item("Column9") = dtRow.Item("Nationality")
                rpt_Rows.Item("Column11") = dtRow.Item("Bank")
                rpt_Rows.Item("Column12") = dtRow.Item("AccountNo")
                rpt_Rows.Item("Column13") = dtRow.Item("membership")


                'Pinkal (24-Apr-2013) -- Start
                'Enhancement : TRA Changes

                If mblnShowEmpScale Then
                    rpt_Rows.Item("Column10") = Format(CDec(dtRow.Item("Basic Salary")), GUI.fmtCurrency)
                    rpt_Rows.Item("Column14") = Format(CDec(dtRow.Item("Amount1")), GUI.fmtCurrency)
                    rpt_Rows.Item("Column15") = Format(CDec(dtRow.Item("Amount2")), GUI.fmtCurrency)
                    rpt_Rows.Item("Column16") = Format(CDec(dtRow.Item("Amount3")), GUI.fmtCurrency)
                End If

                'Pinkal (24-Apr-2013) -- End

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next


            objRpt = New ArutiReport.Designer.rptEmployeeDetail_withED


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 5, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 6, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 7, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 8, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            If mblnShowEmpScale Then
                Call ReportFunction.TextChange(objRpt, "txtSalary", Language.getMessage(mstrModuleName, 23, "Salary"))
                Call ReportFunction.TextChange(objRpt, "txtThead1", mstrTranHead1)
                Call ReportFunction.TextChange(objRpt, "txtThead2", mstrTranHead2)
                Call ReportFunction.TextChange(objRpt, "txtThead3", mstrTranHead3)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtSalary", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtThead1", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtThead2", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtThead3", True)
            End If
            'Pinkal (24-Apr-2013) -- End


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 14, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 15, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 16, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 17, "Job Title"))
            Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 18, "Gender"))
            Call ReportFunction.TextChange(objRpt, "txtMaritalStatus", Language.getMessage(mstrModuleName, 19, "Marital St."))
            Call ReportFunction.TextChange(objRpt, "txtDOB", Language.getMessage(mstrModuleName, 20, "D.O.B"))
            Call ReportFunction.TextChange(objRpt, "txtDOJ", Language.getMessage(mstrModuleName, 21, "D.O.J"))
            Call ReportFunction.TextChange(objRpt, "txtNationality", Language.getMessage(mstrModuleName, 22, "Nationality"))

            Call ReportFunction.TextChange(objRpt, "txtBank", Language.getMessage(mstrModuleName, 24, "Bank"))
            Call ReportFunction.TextChange(objRpt, "txtAcNo", Language.getMessage(mstrModuleName, 25, "Account No"))
            Call ReportFunction.TextChange(objRpt, "txtMShip", mstrMemberShip)



            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 9, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 10, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
export:

            If mblnExport = True Then
                mdtTableExcel = dsList.Tables(0)
                mdtTableExcel.Columns.Add("SrNo", System.Type.GetType("System.String"))
                'mdtTableExcel.Columns("DOJ").DataType = System.Type.GetType("System.Object")
                'mdtTableExcel.Columns("terminationdate").DataType = System.Type.GetType("System.Object")

                For Each dRow As DataRow In mdtTableExcel.Rows
                    dRow.Item("SrNo") = (mdtTableExcel.Rows.IndexOf(dRow) + 1).ToString
                    If IsDBNull(dRow.Item("JoinDate")) Then
                        dRow.Item("JoinDate") = ""
                    Else
                        dRow.Item("JoinDate") = eZeeDate.convertDate(dRow.Item("JoinDate").ToString).ToShortDateString
                    End If
                    If intReportType = 2 Then
                        'Shani(21-Dec-2015) -- Start
                        'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
                        'If IsDBNull(dRow.Item("terminationdate")) Then
                        If IsDBNull(dRow.Item("terminationdate")) OrElse dRow.Item("terminationdate").ToString.Trim.Length <= 0 Then
                            'Shani(21-Dec-2015) -- End

                            dRow.Item("terminationdate") = ""
                        Else
                            dRow.Item("terminationdate") = eZeeDate.convertDate(dRow.Item("terminationdate").ToString).ToShortDateString
                        End If
                    End If
                    dRow.Item("Basic Salary") = Format(CDec(dRow.Item("Basic Salary")), GUI.fmtCurrency)
                    dRow.Item("Amount1") = Format(CDec(dRow.Item("Amount1")), GUI.fmtCurrency)
                    dRow.Item("Amount2") = Format(CDec(dRow.Item("Amount2")), GUI.fmtCurrency)
                    dRow.Item("Amount3") = Format(CDec(dRow.Item("Amount3")), GUI.fmtCurrency)

                    'Shani(21-Dec-2015) -- Start
                    'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
                    If dRow.Item("EOC").ToString.Trim.Length <= 0 Then
                        dRow.Item("EOC") = ""
                    Else
                        dRow.Item("EOC") = eZeeDate.convertDate(dRow.Item("EOC").ToString).ToShortDateString
                    End If
                    'Shani(21-Dec-2015) -- End

                    dRow.AcceptChanges()
                Next

                Dim mintColumn As Integer = 0

                mdtTableExcel.Columns("SrNo").Caption = Language.getMessage(mstrModuleName, 110, "Sr. No.")
                mdtTableExcel.Columns("SrNo").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Employeecode").Caption = Language.getMessage(mstrModuleName, 111, "EMPNo")
                mdtTableExcel.Columns("Employeecode").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("EmpName").Caption = Language.getMessage(mstrModuleName, 112, "EMP NAME")
                mdtTableExcel.Columns("EmpName").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Job").Caption = Language.getMessage(mstrModuleName, 113, "DESIGNATION")
                mdtTableExcel.Columns("Job").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("JoinDate").Caption = Language.getMessage(mstrModuleName, 114, "DATE OF JOINING")
                mdtTableExcel.Columns("JoinDate").SetOrdinal(mintColumn)
                mintColumn += 1

                'Shani(21-Dec-2015) -- Start
                'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
                mdtTableExcel.Columns("EOC").Caption = Language.getMessage(mstrModuleName, 119, "EOC Date")
                mdtTableExcel.Columns("EOC").SetOrdinal(mintColumn)
                mintColumn += 1
                'Shani(21-Dec-2015) -- End

                If intReportType = 2 Then
                    mdtTableExcel.Columns("terminationdate").Caption = Language.getMessage(mstrModuleName, 115, "Terminated date")
                    mdtTableExcel.Columns("terminationdate").SetOrdinal(mintColumn)
                    mintColumn += 1
                End If

                For Each dic As Integer In mdicMembership.Keys
                    mdtTableExcel.Columns("" & mdicMembership(dic) & "").Caption = mdicMembership(dic)
                    mdtTableExcel.Columns("" & mdicMembership(dic) & "").SetOrdinal(mintColumn)
                    mintColumn += 1
                Next

                mdtTableExcel.Columns("Bank").Caption = Language.getMessage(mstrModuleName, 116, "BANK NAME")
                mdtTableExcel.Columns("Bank").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("AccountNo").Caption = Language.getMessage(mstrModuleName, 117, "BANK A/C NO")
                mdtTableExcel.Columns("AccountNo").SetOrdinal(mintColumn)
                mintColumn += 1

                If mblnShowEmpScale = True Then
                    mdtTableExcel.Columns("Basic Salary").Caption = Language.getMessage(mstrModuleName, 118, "Basic Salary")
                    mdtTableExcel.Columns("Basic Salary").SetOrdinal(mintColumn)
                    mintColumn += 1
                End If

                mdtTableExcel.Columns("Amount1").Caption = mstrTranHead1
                mdtTableExcel.Columns("Amount1").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Amount2").Caption = mstrTranHead2
                mdtTableExcel.Columns("Amount2").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Amount3").Caption = mstrTranHead3
                mdtTableExcel.Columns("Amount3").SetOrdinal(mintColumn)
                mintColumn += 1

                'Shani(21-Dec-2015) -- Start
                'ENHANCEMENT : Add Analysis by Link And Add EOC Column In report
                mdtTableExcel.Columns("Id").Caption = "ID"
                mdtTableExcel.Columns("Id").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("GName").Caption = "GName"
                mdtTableExcel.Columns("GName").SetOrdinal(mintColumn)
                mintColumn += 1
                'Shani(21-Dec-2015) -- End

                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next
            End If
            'Shani(02-Dec-2015) -- End

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Job :")
            Language.setMessage(mstrModuleName, 3, "Department :")
            Language.setMessage(mstrModuleName, 4, " Order By :")
            Language.setMessage(mstrModuleName, 5, "Prepared By :")
            Language.setMessage(mstrModuleName, 6, "Checked By :")
            Language.setMessage(mstrModuleName, 7, "Approved By :")
            Language.setMessage(mstrModuleName, 8, "Received By :")
            Language.setMessage(mstrModuleName, 9, "Printed By :")
            Language.setMessage(mstrModuleName, 10, "Printed Date :")
            Language.setMessage(mstrModuleName, 12, "Male")
            Language.setMessage(mstrModuleName, 13, "Female")
            Language.setMessage(mstrModuleName, 14, "Employee Code")
            Language.setMessage(mstrModuleName, 15, "Employee Name")
            Language.setMessage(mstrModuleName, 16, "Department")
            Language.setMessage(mstrModuleName, 17, "Job Title")
            Language.setMessage(mstrModuleName, 18, "Gender")
            Language.setMessage(mstrModuleName, 19, "Marital St.")
            Language.setMessage(mstrModuleName, 20, "D.O.B")
            Language.setMessage(mstrModuleName, 21, "D.O.J")
            Language.setMessage(mstrModuleName, 22, "Nationality")
            Language.setMessage(mstrModuleName, 23, "Salary")
            Language.setMessage(mstrModuleName, 24, "Bank")
            Language.setMessage(mstrModuleName, 25, "Account No")
            Language.setMessage(mstrModuleName, 26, "Period :")
            Language.setMessage(mstrModuleName, 110, "Sr. No.")
            Language.setMessage(mstrModuleName, 111, "EMPNo")
            Language.setMessage(mstrModuleName, 112, "EMP NAME")
            Language.setMessage(mstrModuleName, 113, "DESIGNATION")
            Language.setMessage(mstrModuleName, 114, "DATE OF JOINING")
            Language.setMessage(mstrModuleName, 115, "Terminated date")
            Language.setMessage(mstrModuleName, 116, "BANK NAME")
            Language.setMessage(mstrModuleName, 117, "BANK A/C NO")
            Language.setMessage(mstrModuleName, 118, "Basic Salary")
            Language.setMessage(mstrModuleName, 119, "EOC Date")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
