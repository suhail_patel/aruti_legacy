'************************************************************************************************************************************
'Class Name : clsEmployeeRefereeReport.vb
'Purpose    :
'Date       :10/9/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsEmployeeRefereeReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeRefereeReport"
    Private mstrReportId As String = enArutiReport.Employee_Referee_Report  '22
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mstrEmployeeCode As String = String.Empty
    Private mintRefereeTypeId As Integer = -1
    Private mstrRefereeTypeName As String = String.Empty
    Private mstrRefereeName As String = String.Empty

    'Sandeep [ 12 MARCH 2011 ] -- Start
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'Sandeep [ 12 MARCH 2011 ] -- End 

    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (16 May 2012) -- End

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End
    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrUserAccessFilter As String = String.Empty
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _RefereeTypeId() As Integer
        Set(ByVal value As Integer)
            mintRefereeTypeId = value
        End Set
    End Property

    Public WriteOnly Property _RefereeTypeName() As String
        Set(ByVal value As String)
            mstrRefereeTypeName = value
        End Set
    End Property

    Public WriteOnly Property _RefereeName() As String
        Set(ByVal value As String)
            mstrRefereeName = value
        End Set
    End Property

    'Sandeep [ 12 MARCH 2011 ] -- Start
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property
    'Sandeep [ 12 MARCH 2011 ] -- End 

    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (16 May 2012) -- End

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End

    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrEmployeeCode = ""
            mintRefereeTypeId = 0
            mstrRefereeTypeName = ""
            mstrRefereeName = ""
            'Sandeep [ 12 MARCH 2011 ] -- Start
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            'Sandeep [ 12 MARCH 2011 ] -- End 

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (16 May 2012) -- End

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            'Sandeep [ 12 MARCH 2011 ] -- Start
            'If mintEmployeeId > 0 Then
            '    objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            '    Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & " "
            'End If

            'If mstrEmployeeCode.Trim <> "" Then
            '    objDataOperation.AddParameter("@EmployeeCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrEmployeeCode & "%")
            '    Me._FilterQuery &= " AND ISNULL(hremployee_master.employeecode,'') LIKE @EmployeeCode "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Code :") & " " & mstrEmployeeCode & " "
            'End If

            'If mintRefereeTypeId > 0 Then
            '    objDataOperation.AddParameter("@RefereeTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRefereeTypeId)
            '    Me._FilterQuery &= " AND cfcommon_master.masterunkid = @RefereeTypeId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Referee Type :") & " " & mstrRefereeTypeName & " "
            'End If

            'If mstrRefereeName.Trim <> "" Then
            '    objDataOperation.AddParameter("@RefereeName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrRefereeName & "%")
            '    Me._FilterQuery &= " AND ISNULL(hremployee_referee_tran.name,'') LIKE @RefereeName "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Referee Name :") & " " & mstrRefereeName & " "
            'End If

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                'Sohail (16 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= " AND EmpId = @EmployeeId "
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                'Sohail (16 May 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mstrEmployeeCode.Trim <> "" Then
                objDataOperation.AddParameter("@EmployeeCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrEmployeeCode & "%")
                'Sohail (16 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= " AND Code LIKE @EmployeeCode "
                Me._FilterQuery &= " AND ISNULL(hremployee_master.employeecode,'') LIKE @EmployeeCode "
                'Sohail (16 May 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Code :") & " " & mstrEmployeeCode & " "
            End If

            If mintRefereeTypeId > 0 Then
                objDataOperation.AddParameter("@RefereeTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRefereeTypeId)
                'Sohail (16 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= " AND RefTypeId = @RefereeTypeId "
                Me._FilterQuery &= " AND cfcommon_master.masterunkid = @RefereeTypeId "
                'Sohail (16 May 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Referee Type :") & " " & mstrRefereeTypeName & " "
            End If

            If mstrRefereeName.Trim <> "" Then
                objDataOperation.AddParameter("@RefereeName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrRefereeName & "%")
                'Sohail (16 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= " AND RefereeName LIKE @RefereeName "
                Me._FilterQuery &= " AND ISNULL(hremployee_referee_tran.name,'') LIKE @RefereeName "
                'Sohail (16 May 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Referee Name :") & " " & mstrRefereeName & " "
            End If

            If mstrViewByName.Length > 0 Then
                'Me._FilterTitle &= Language.getMessage(mstrModuleName, 34, "Analysis By :") & " " & mstrViewByName & " " 'Sohail (16 May 2012)
            End If

            'Sandeep [ 12 MARCH 2011 ] -- End 

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If
        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()

        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End


        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            'Pinkal (14-Aug-2012) -- Start
            'Enhancement : TRA Changes
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)

            Rpt = objRpt

            'Pinkal (14-Aug-2012) -- End


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'Sandeep [ 12 MARCH 2011 ] -- Start
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 6, "Code")))
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 7, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 7, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 7, "Employee Name")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            iColumn_DetailReport.Add(New IColumn("ISNULL(cfcommon_master.name,'')", Language.getMessage(mstrModuleName, 8, "Referee Type")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_referee_tran.name,'')", Language.getMessage(mstrModuleName, 9, "Referee Name")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_referee_tran.mobile_no,'')", Language.getMessage(mstrModuleName, 10, "Mobile")))

            'iColumn_DetailReport.Add(New IColumn("ISNULL(ECode,'')", Language.getMessage(mstrModuleName, 6, "Code")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(EmpName,'')", Language.getMessage(mstrModuleName, 7, "Employee Name")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(Refereetype,'')", Language.getMessage(mstrModuleName, 8, "Referee Type")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(RefereeName,'')", Language.getMessage(mstrModuleName, 9, "Referee Name")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(Mobile,'')", Language.getMessage(mstrModuleName, 10, "Mobile")))
            'Sandeep [ 12 MARCH 2011 ] -- End 

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        'Sandeep [ 12 MARCH 2011 ] -- Start
        Dim blnFlag As Boolean = False
        'Sandeep [ 12 MARCH 2011 ] -- End 
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END


            'Sandeep [ 12 MARCH 2011 ] -- Start
            'StrQ = "SELECT " & _
            '        "	 ISNULL(hremployee_master.employeecode,'') AS Code " & _
            '        "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '        "	,ISNULL(cfcommon_master.name,'') AS Refereetype " & _
            '        "	,ISNULL(hremployee_referee_tran.name,'') AS RefereeName " & _
            '        "	,ISNULL(hremployee_referee_tran.mobile_no,'') AS Mobile " & _
            '        "	,ISNULL(hremployee_referee_tran.email,'') AS Email " & _
            '        "	,ISNULL(hremployee_referee_tran.address,'') AS Address " & _
            '        "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
            '        "	,ISNULL(hrmsConfiguration..cfstate_master.name,'') AS State " & _
            '        "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
            '        "FROM hremployee_referee_tran " & _
            '        "	LEFT JOIN hrmsConfiguration..cfstate_master ON hremployee_referee_tran.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
            '        "	LEFT JOIN hrmsConfiguration..cfcity_master ON hremployee_referee_tran.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '        "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
            '        "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid " & _
            '        "	JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "WHERE ISNULL(hremployee_master.isactive,0) = 1 " & _
            '        "	AND ISNULL(hremployee_referee_tran.isvoid,0) = 0 "

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'StrQ = "SELECT "
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        StrQ &= " stationunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Department
            '        StrQ &= " departmentunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Section
            '        StrQ &= "  sectionunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Unit
            '        StrQ &= "  unitunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Job
            '        StrQ &= "  jobunkid , job_name AS GName "
            '        blnFlag = True
            '        'S.SANDEEP [ 06 SEP 2011 ] -- START
            '    Case enAnalysisReport.CostCenter
            '        StrQ &= " costcenterunkid  , costcentername AS GName "
            '        blnFlag = True
            '        'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    Case Else
            '        StrQ &= "  ISNULL(ECode,'') AS ECode " & _
            '                      ", ISNULL(EmpName,'') AS EmpName " & _
            '                      ", ISNULL(Refereetype,'') AS Refereetype " & _
            '                      ", ISNULL(RefereeName,'') AS RefereeName " & _
            '                      ", ISNULL(Mobile,'') AS Mobile " & _
            '                      ", ISNULL(Email,'') AS Email " & _
            '                      ", ISNULL(Address,'') AS Address " & _
            '                      ", ISNULL(Country,'') AS Country " & _
            '                      ", ISNULL(State,'') AS State " & _
            '                      ", ISNULL(City,'') AS City " & _
            '                      ",'' AS GName "
            '        blnFlag = False
            'End Select

            'If blnFlag = True Then
            '    StrQ &= ", ISNULL(ERS.ECode,'') AS ECode " & _
            '                  ", ISNULL(ERS.EmpName,'') AS EmpName " & _
            '                  ", ISNULL(ERS.Refereetype,'') AS Refereetype " & _
            '                  ", ISNULL(ERS.RefereeName,'') AS RefereeName " & _
            '                  ", ISNULL(ERS.Mobile,'') AS Mobile " & _
            '                  ", ISNULL(ERS.Email,'') AS Email " & _
            '                  ", ISNULL(ERS.Address,'') AS Address " & _
            '                  ", ISNULL(ERS.Country,'') AS Country " & _
            '                  ", ISNULL(ERS.State,'') AS State " & _
            '                  ", ISNULL(ERS.City,'') AS City "
            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '    StrQ &= " FROM hrstation_master "
            '        Case enAnalysisReport.Department
            '    StrQ &= " FROM hrdepartment_master "
            '        Case enAnalysisReport.Section
            '    StrQ &= " FROM hrsection_master "
            '        Case enAnalysisReport.Unit
            '    StrQ &= " FROM hrunit_master "
            '        Case enAnalysisReport.Job
            '    StrQ &= " FROM hrjob_master "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= " FROM prcostcenter_master "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    End Select
            '    StrQ &= " LEFT JOIN " & _
            '                 " ( " & _
            '                 " SELECT " & _
            '                 "	 ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            '                 "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '                 "	,ISNULL(cfcommon_master.name,'') AS Refereetype " & _
            '                 "	,ISNULL(hremployee_referee_tran.name,'') AS RefereeName " & _
            '                 "	,ISNULL(hremployee_referee_tran.mobile_no,'') AS Mobile " & _
            '                 "	,ISNULL(hremployee_referee_tran.email,'') AS Email " & _
            '                 "	,ISNULL(hremployee_referee_tran.address,'') AS Address " & _
            '                 "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
            '                 "	,ISNULL(hrmsConfiguration..cfstate_master.name,'') AS State " & _
            '                 "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
            '                 "  ,hremployee_master.employeeunkid AS EmpId " & _
            '                 "  ,cfcommon_master.masterunkid AS RefTypeId "
            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= ", hremployee_master.stationunkid AS BranchId  "
            '        Case enAnalysisReport.Department
            '            StrQ &= ", hremployee_master.departmentunkid AS DeptId "
            '        Case enAnalysisReport.Section
            '            StrQ &= ", hremployee_master.sectionunkid AS SecId  "
            '        Case enAnalysisReport.Unit
            '            StrQ &= ", hremployee_master.unitunkid AS UnitId  "
            '        Case enAnalysisReport.Job
            '            StrQ &= ", hremployee_master.jobunkid AS JobId  "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= ", hremployee_master.costcenterunkid As CCId "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END
            '    End Select


            '    'Pinkal (24-Jun-2011) -- Start
            '    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            '    'StrQ &= "FROM hremployee_referee_tran " & _
            '    '        "	LEFT JOIN hrmsConfiguration..cfstate_master ON hremployee_referee_tran.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
            '    '        "	LEFT JOIN hrmsConfiguration..cfcity_master ON hremployee_referee_tran.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '    '        "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
            '    '        "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid " & _
            '    '        "	JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '    '        "WHERE ISNULL(hremployee_master.isactive,0) = 1 " & _
            '    '        "	AND ISNULL(hremployee_referee_tran.isvoid,0) = 0 "

            'StrQ &= "FROM hremployee_referee_tran " & _
            '        "	LEFT JOIN hrmsConfiguration..cfstate_master ON hremployee_referee_tran.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
            '        "	LEFT JOIN hrmsConfiguration..cfcity_master ON hremployee_referee_tran.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '        "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
            '        "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid " & _
            '        "	JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '            "WHERE ISNULL(hremployee_referee_tran.isvoid,0) = 0 "

            '    If mblIsActive = False Then
            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ISSUE : TRA ENHANCEMENTS
            '        'StrQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
            '        StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END
            '    End If

            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END

            '    'Pinkal (24-Jun-2011) -- End

            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= " ) AS ERS ON hrstation_master.stationunkid = ERS.BranchId " & _
            '                          " WHERE hrstation_master.stationunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Department
            '            StrQ &= " ) AS ERS ON hrdepartment_master.departmentunkid = ERS.DeptId " & _
            '                          " WHERE hrdepartment_master.departmentunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Section
            '            StrQ &= " ) AS ERS ON hrsection_master.sectionunkid = ERS.SecId " & _
            '                          " WHERE hrsection_master.sectionunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Unit
            '            StrQ &= " ) AS ERS ON hrunit_master.unitunkid = ERS.UnitId " & _
            '                          " WHERE hrunit_master.unitunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Job
            '            StrQ &= " ) AS ERS ON hrjob_master.jobunkid = ERS.JobId " & _
            '                          " WHERE hrjob_master.jobunkid IN ( " & mstrViewByIds & " ) "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= " ) AS ERS ON prcostcenter_master.costcenterunkid = ERS.CCId " & _
            '                          " WHERE prcostcenter_master.costcenterunkid IN ( " & mstrViewByIds & " ) "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END
            '    End Select
            'Else
            '    'StrQ &= " FROM " & _
            '    '                 " ( " & _
            '    '                 " SELECT " & _
            '    '                 "	 ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            '    '    "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '    '    "	,ISNULL(cfcommon_master.name,'') AS Refereetype " & _
            '    '    "	,ISNULL(hremployee_referee_tran.name,'') AS RefereeName " & _
            '    '    "	,ISNULL(hremployee_referee_tran.mobile_no,'') AS Mobile " & _
            '    '    "	,ISNULL(hremployee_referee_tran.email,'') AS Email " & _
            '    '    "	,ISNULL(hremployee_referee_tran.address,'') AS Address " & _
            '    '    "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
            '    '    "	,ISNULL(hrmsConfiguration..cfstate_master.name,'') AS State " & _
            '    '    "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
            '    '                 "  ,hremployee_master.employeeunkid AS EmpId " & _
            '    '                 "  ,cfcommon_master.masterunkid AS RefTypeId " & _
            '    '    "FROM hremployee_referee_tran " & _
            '    '    "	LEFT JOIN hrmsConfiguration..cfstate_master ON hremployee_referee_tran.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
            '    '    "	LEFT JOIN hrmsConfiguration..cfcity_master ON hremployee_referee_tran.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '    '    "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
            '    '    "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid " & _
            '    '    "	JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '    '    "WHERE ISNULL(hremployee_master.isactive,0) = 1 " & _
            '    '                 "	AND ISNULL(hremployee_referee_tran.isvoid,0) = 0 ) AS ERS WHERE 1 = 1 "

            'StrQ &= " FROM " & _
            '                 " ( " & _
            '                 " SELECT " & _
            '                 "	 ISNULL(hremployee_master.employeecode,'') AS ECode " & _
            '    "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '    "	,ISNULL(cfcommon_master.name,'') AS Refereetype " & _
            '    "	,ISNULL(hremployee_referee_tran.name,'') AS RefereeName " & _
            '    "	,ISNULL(hremployee_referee_tran.mobile_no,'') AS Mobile " & _
            '    "	,ISNULL(hremployee_referee_tran.email,'') AS Email " & _
            '    "	,ISNULL(hremployee_referee_tran.address,'') AS Address " & _
            '    "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
            '    "	,ISNULL(hrmsConfiguration..cfstate_master.name,'') AS State " & _
            '    "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
            '                 "  ,hremployee_master.employeeunkid AS EmpId " & _
            '                 "  ,cfcommon_master.masterunkid AS RefTypeId " & _
            '    "FROM hremployee_referee_tran " & _
            '    "	LEFT JOIN hrmsConfiguration..cfstate_master ON hremployee_referee_tran.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
            '    "	LEFT JOIN hrmsConfiguration..cfcity_master ON hremployee_referee_tran.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '    "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
            '    "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid " & _
            '    "	JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "WHERE ISNULL(hremployee_referee_tran.isvoid,0) = 0  "

            '    If mblIsActive = False Then
            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ISSUE : TRA ENHANCEMENTS
            '        'StrQ &= " AND ISNULL(hremployee_master.isactive,0) = 1"
            '        StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END
            '    End If

            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''S.SANDEEP [ 12 MAY 2012 ] -- END

            '    StrQ &= " ) AS ERS WHERE 1 = 1 "

            'End If

            ''Sandeep [ 12 MARCH 2011 ] -- End 
            StrQ = "SELECT  ISNULL(hremployee_master.employeecode, '') AS ECode "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            '"	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= "	,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EmpName "
            Else
                StrQ &= "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName "
            End If

            StrQ &= "	,ISNULL(cfcommon_master.name,'') AS Refereetype " & _
                    "	,ISNULL(hremployee_referee_tran.name,'') AS RefereeName " & _
                    "	,ISNULL(hremployee_referee_tran.mobile_no,'') AS Mobile " & _
                    "	,ISNULL(hremployee_referee_tran.email,'') AS Email " & _
                    "	,ISNULL(hremployee_referee_tran.address,'') AS Address " & _
                    "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
                    "	,ISNULL(hrmsConfiguration..cfstate_master.name,'') AS State " & _
                    "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
                    "   ,hremployee_master.employeeunkid AS EmpId " & _
                    "   ,cfcommon_master.masterunkid AS RefTypeId "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hremployee_referee_tran " & _
                    "  LEFT JOIN hrmsConfiguration..cfstate_master ON hremployee_referee_tran.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
                    "  LEFT JOIN hrmsConfiguration..cfcity_master ON hremployee_referee_tran.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
                    "  LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_referee_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                    "  JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid " & _
                    "  JOIN hremployee_master ON hremployee_referee_tran.employeeunkid = hremployee_master.employeeunkid "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= mstrAnalysis_Join

            StrQ &= "WHERE   ISNULL(hremployee_referee_tran.isvoid, 0) = 0 "

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If mblIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- END


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            'Sohail (16 May 2012) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow


                'S.SANDEEP [ 23 SEP 2011 ] -- START
                'ENHANCEMENT : CODE OPTIMIZATION
                If dtRow.Item("ECode").ToString.Trim.Length <= 0 Then Continue For
                'S.SANDEEP [ 23 SEP 2011 ] -- END 


                'Sandeep [ 12 MARCH 2011 ] -- Start
                'rpt_Row.Item("Column1") = dtRow.Item("Code")
                rpt_Row.Item("Column1") = dtRow.Item("ECode")
                'Sandeep [ 12 MARCH 2011 ] -- End 
                rpt_Row.Item("Column2") = dtRow.Item("EmpName")
                rpt_Row.Item("Column3") = dtRow.Item("Refereetype")
                rpt_Row.Item("Column4") = dtRow.Item("RefereeName")
                rpt_Row.Item("Column5") = dtRow.Item("Mobile")
                rpt_Row.Item("Column6") = dtRow.Item("Email")
                rpt_Row.Item("Column7") = dtRow.Item("Address") & " " & dtRow.Item("Country") & " " & _
                                          dtRow.Item("State") & " " & dtRow.Item("City")

                'Sandeep [ 12 MARCH 2011 ] -- Start
                rpt_Row.Item("Column8") = dtRow.Item("GName")
                'Sandeep [ 12 MARCH 2011 ] -- End 

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeRefereeReport


            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 18, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End 


            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 6, "Code"))
            ReportFunction.TextChange(objRpt, "txtEmployeeNAme", Language.getMessage(mstrModuleName, 7, "Employee Name"))
            ReportFunction.TextChange(objRpt, "txtRefereeType", Language.getMessage(mstrModuleName, 8, "Referee Type"))
            ReportFunction.TextChange(objRpt, "txtRefereeName", Language.getMessage(mstrModuleName, 9, "Referee Name"))
            ReportFunction.TextChange(objRpt, "txtMobile", Language.getMessage(mstrModuleName, 10, "Mobile"))
            ReportFunction.TextChange(objRpt, "txtEmail", Language.getMessage(mstrModuleName, 11, "Email"))
            ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 12, "Postal Address"))
            ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 13, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 14, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 15, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 16, "Page :"))


            'Sandeep [ 12 MARCH 2011 ] -- Start
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 29, "Branch :"))
            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 30, "Department :"))
            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 31, "Section :"))
            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 32, "Unit :"))
            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 33, "Job :"))
            '        'S.SANDEEP [ 06 SEP 2011 ] -- START
            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 34, "Cost Center :"))
            '        'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    Case Else
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", "")
            'End Select
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'Sohail (16 May 2012) -- End
            'Sandeep [ 12 MARCH 2011 ] -- End 

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Code :")
            Language.setMessage(mstrModuleName, 3, "Referee Type :")
            Language.setMessage(mstrModuleName, 4, "Referee Name :")
            Language.setMessage(mstrModuleName, 5, "Order By :")
            Language.setMessage(mstrModuleName, 6, "Code")
            Language.setMessage(mstrModuleName, 7, "Employee Name")
            Language.setMessage(mstrModuleName, 8, "Referee Type")
            Language.setMessage(mstrModuleName, 9, "Referee Name")
            Language.setMessage(mstrModuleName, 10, "Mobile")
            Language.setMessage(mstrModuleName, 11, "Email")
            Language.setMessage(mstrModuleName, 12, "Postal Address")
            Language.setMessage(mstrModuleName, 13, "Grand Total :")
            Language.setMessage(mstrModuleName, 14, "Printed By :")
            Language.setMessage(mstrModuleName, 15, "Printed Date :")
            Language.setMessage(mstrModuleName, 16, "Page :")
            Language.setMessage(mstrModuleName, 17, "Prepared By :")
            Language.setMessage(mstrModuleName, 18, "Checked By :")
            Language.setMessage(mstrModuleName, 19, "Approved By :")
            Language.setMessage(mstrModuleName, 20, "Received By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
