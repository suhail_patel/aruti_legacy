'************************************************************************************************************************************
'Class Name : clsTrainingEnrollVCReport.vb
'Purpose    :
'Date       :23/02/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsTrainingEnrollVCReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTrainingEnrollVCReport"
    Private mstrReportId As String = enArutiReport.TrainingEnroll_Void_CancelReport   '37
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = String.Empty
    Private mintEnUserId As Integer = 0
    Private mstrEnUserName As String = String.Empty
    Private mintVoidUserId As Integer = 0
    Private mstrVoidUserName As String = String.Empty
    Private mintCancelUserId As Integer = 0
    Private mstrCancelUserName As String = String.Empty
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = String.Empty
    Private mintTrainingId As Integer = 0
    Private mstrTrainingName As String = String.Empty


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _EnUserId() As Integer
        Set(ByVal value As Integer)
            mintEnUserId = value
        End Set
    End Property

    Public WriteOnly Property _EnUserName() As String
        Set(ByVal value As String)
            mstrEnUserName = value
        End Set
    End Property

    Public WriteOnly Property _VoidUserId() As Integer
        Set(ByVal value As Integer)
            mintVoidUserId = value
        End Set
    End Property

    Public WriteOnly Property _VoidUserName() As String
        Set(ByVal value As String)
            mstrVoidUserName = value
        End Set
    End Property

    Public WriteOnly Property _CancelUserId() As Integer
        Set(ByVal value As Integer)
            mintCancelUserId = value
        End Set
    End Property

    Public WriteOnly Property _CancelUserName() As String
        Set(ByVal value As String)
            mstrCancelUserName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingId() As Integer
        Set(ByVal value As Integer)
            mintTrainingId = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsAcitve() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmpId = 0
            mstrEmpName = ""
            mintEnUserId = 0
            mstrEnUserName = ""
            mintVoidUserId = 0
            mstrVoidUserName = ""
            mintCancelUserId = 0
            mstrCancelUserName = ""
            mintStatusId = 0
            mstrStatusName = ""
            mintTrainingId = 0
            mstrTrainingName = ""


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            objDataOperation.AddParameter("@CStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Cancelled"))
            objDataOperation.AddParameter("@VStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Voided"))

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                Me._FilterQuery &= " AND EmpId = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Employee :") & " " & mstrEmpName & " "
            End If

            If mintEnUserId > 0 Then
                objDataOperation.AddParameter("@EnUserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEnUserId)
                Me._FilterQuery &= " AND EnUserId = @EnUserId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Enrolled By :") & " " & mstrEnUserName & " "
            End If

            If mintVoidUserId > 0 Then
                objDataOperation.AddParameter("@VoidUserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserId)
                Me._FilterQuery &= " AND VoidUserId = @VoidUserId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Void User :") & " " & mstrVoidUserName & " "
            End If

            If mintCancelUserId > 0 Then
                objDataOperation.AddParameter("@CancelUserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelUserId)
                Me._FilterQuery &= " AND CanUserId = @CancelUserId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Cancel User :") & " " & mstrCancelUserName & " "
            End If

            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND VCStatusId = @StatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Status :") & " " & mstrStatusName & " "
            End If

            If mintTrainingId > 0 Then
                objDataOperation.AddParameter("@TrainingId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingId)
                Me._FilterQuery &= " AND TrainingId = @TrainingId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Training :") & " " & mstrTrainingName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Get_Status(Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 0 AS Id, @Select As Name " & _
                   "UNION SELECT 1 AS Id, @CStatus AS Name " & _
                   "UNION SELECT 2 AS Id, @VStatus As Name " & _
                   "ORDER BY Id "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Select"))
            objDataOperation.AddParameter("@CStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Cancelled"))
            objDataOperation.AddParameter("@VStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Voided"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Status; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'StrQ = "SELECT " & _
            '          " Title AS Title " & _
            '          ",EmpName AS EmpName " & _
            '          ",EDate AS EDate " & _
            '          ",ETime AS ETime " & _
            '          ",EUser AS EUser " & _
            '          ",VCDate AS VCDate " & _
            '          ",VCTime AS VCTime " & _
            '          ",VCUser AS VCUser " & _
            '          ",VCReason AS VCReason " & _
            '          ",VCStatus AS VCStatus " & _
            '        "FROM " & _
            '        "( " & _
            '             "SELECT " & _
            '                  " hrtraining_scheduling.course_title AS Title " & _
            '                  ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '                  ",CONVERT(CHAR(8),enroll_date,112) AS EDate " & _
            '                  ",CONVERT(CHAR(8),enroll_date,108) AS ETime " & _
            '                  ",EnUser.username AS EUser " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.cancellationdate,112) " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.voiddatetime,112) " & _
            '                  " END AS VCDate " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.cancellationdate,108) " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.voiddatetime,108) " & _
            '                  " END AS VCTime " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN ISNULL(hrmsConfiguration..cfuser_master.username,'') " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN ISNULL(VoidUser.username,'') " & _
            '                  " END AS VCUser " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN hrtraining_enrollment_tran.cancellationreason " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN hrtraining_enrollment_tran.voidreason " & _
            '                  " END AS VCReason " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN @CStatus " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN @VStatus " & _
            '                  " END AS VCStatus " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN 1 " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN 2 " & _
            '                  " END AS VCStatusId " & _
            '                  ",EnUser.userunkid AS EnUserId " & _
            '                  ",VoidUser.userunkid AS VoidUserId " & _
            '                  ",hrmsConfiguration..cfuser_master.userunkid AS CanUserId " & _
            '                  ",hremployee_master.employeeunkid AS EmpId " & _
            '                  ",hrtraining_scheduling.trainingschedulingunkid AS TrainingId " & _
            '             "FROM hrtraining_enrollment_tran " & _
            '                  "LEFT JOIN hrmsConfiguration..cfuser_master AS VoidUser ON hrtraining_enrollment_tran.voiduserunkid = VoidUser.userunkid " & _
            '                  "LEFT JOIN hrmsConfiguration..cfuser_master ON hrtraining_enrollment_tran.cancellationuserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
            '                  "JOIN hrmsConfiguration..cfuser_master  AS EnUser ON hrtraining_enrollment_tran.userunkid = EnUser.userunkid " & _
            '                  "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                  "JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
            '             "WHERE (hrtraining_enrollment_tran.iscancel = 1 OR hrtraining_enrollment_tran.isvoid = 1) "

            StrQ = "SELECT " & _
                      " Title AS Title " & _
                      ",EmpName AS EmpName " & _
                      ",EDate AS EDate " & _
                      ",ETime AS ETime " & _
                      ",EUser AS EUser " & _
                      ",VCDate AS VCDate " & _
                      ",VCTime AS VCTime " & _
                      ",VCUser AS VCUser " & _
                      ",VCReason AS VCReason " & _
                      ",VCStatus AS VCStatus " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                              " cfcommon_master.name AS Title "
            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            'ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName "
            If mblnFirstNamethenSurname = False Then
                StrQ &= ",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EmpName "
            Else
                StrQ &= ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName "
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= ",CONVERT(CHAR(8),enroll_date,112) AS EDate " & _
            '                  ",CONVERT(CHAR(8),enroll_date,108) AS ETime " & _
            '                  ",EnUser.username AS EUser " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.cancellationdate,112) " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.voiddatetime,112) " & _
            '                  " END AS VCDate " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.cancellationdate,108) " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.voiddatetime,108) " & _
            '                  " END AS VCTime " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN ISNULL(hrmsConfiguration..cfuser_master.username,'') " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN ISNULL(VoidUser.username,'') " & _
            '                  " END AS VCUser " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN hrtraining_enrollment_tran.cancellationreason " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN hrtraining_enrollment_tran.voidreason " & _
            '                  " END AS VCReason " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN @CStatus " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN @VStatus " & _
            '                  " END AS VCStatus " & _
            '                  ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN 1 " & _
            '                              "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN 2 " & _
            '                  " END AS VCStatusId " & _
            '                  ",EnUser.userunkid AS EnUserId " & _
            '                  ",VoidUser.userunkid AS VoidUserId " & _
            '                  ",hrmsConfiguration..cfuser_master.userunkid AS CanUserId " & _
            '                  ",hremployee_master.employeeunkid AS EmpId " & _
            '                  ",hrtraining_scheduling.trainingschedulingunkid AS TrainingId " & _
            '             "FROM hrtraining_enrollment_tran " & _
            '                  "LEFT JOIN hrmsConfiguration..cfuser_master AS VoidUser ON hrtraining_enrollment_tran.voiduserunkid = VoidUser.userunkid " & _
            '                  "LEFT JOIN hrmsConfiguration..cfuser_master ON hrtraining_enrollment_tran.cancellationuserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
            '                  "JOIN hrmsConfiguration..cfuser_master  AS EnUser ON hrtraining_enrollment_tran.userunkid = EnUser.userunkid " & _
            '                  "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                  "JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
            '                  "JOIN cfcommon_master ON cfcommon_master.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) " & _
            '             "WHERE (hrtraining_enrollment_tran.iscancel = 1 OR hrtraining_enrollment_tran.isvoid = 1) "
            ''Anjan (10 Feb 2012)-End 



            'If mblnIsActive = False Then
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ISSUE : TRA ENHANCEMENTS
            '    'StrQ &= " AND hremployee_master.isactive  = 1"
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            'End If

            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''S.SANDEEP [ 12 MAY 2012 ] -- END

            StrQ &= ",CONVERT(CHAR(8),enroll_date,112) AS EDate " & _
                              ",CONVERT(CHAR(8),enroll_date,108) AS ETime " & _
                              ",EnUser.username AS EUser " & _
                              ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.cancellationdate,112) " & _
                                          "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.voiddatetime,112) " & _
                              " END AS VCDate " & _
                              ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.cancellationdate,108) " & _
                                          "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN CONVERT(CHAR(8),hrtraining_enrollment_tran.voiddatetime,108) " & _
                              " END AS VCTime " & _
                              ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN ISNULL(hrmsConfiguration..cfuser_master.username,'') " & _
                                          "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN ISNULL(VoidUser.username,'') " & _
                              " END AS VCUser " & _
                              ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN hrtraining_enrollment_tran.cancellationreason " & _
                                          "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN hrtraining_enrollment_tran.voidreason " & _
                              " END AS VCReason " & _
                              ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN @CStatus " & _
                                          "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN @VStatus " & _
                              " END AS VCStatus " & _
                              ",CASE WHEN hrtraining_enrollment_tran.iscancel = 1 THEN 1 " & _
                                          "WHEN hrtraining_enrollment_tran.isvoid = 1 THEN 2 " & _
                              " END AS VCStatusId " & _
                              ",EnUser.userunkid AS EnUserId " & _
                              ",VoidUser.userunkid AS VoidUserId " & _
                              ",hrmsConfiguration..cfuser_master.userunkid AS CanUserId " & _
                              ",hremployee_master.employeeunkid AS EmpId " & _
                              ",hrtraining_scheduling.trainingschedulingunkid AS TrainingId " & _
                         "FROM hrtraining_enrollment_tran " & _
                              "LEFT JOIN hrmsConfiguration..cfuser_master AS VoidUser ON hrtraining_enrollment_tran.voiduserunkid = VoidUser.userunkid " & _
                              "LEFT JOIN hrmsConfiguration..cfuser_master ON hrtraining_enrollment_tran.cancellationuserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                              "JOIN hrmsConfiguration..cfuser_master  AS EnUser ON hrtraining_enrollment_tran.userunkid = EnUser.userunkid " & _
                              "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                              "JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                              "JOIN cfcommon_master ON cfcommon_master.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE (hrtraining_enrollment_tran.iscancel = 1 OR hrtraining_enrollment_tran.isvoid = 1) "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Shani(24-Aug-2015) -- End

            StrQ &= " ) AS TEVC WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("Title")
                rpt_Rows.Item("Column2") = dtRow.Item("EmpName")
                rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("EDate").ToString).ToShortDateString & vbCrLf & dtRow.Item("ETime")
                rpt_Rows.Item("Column4") = dtRow.Item("EUser")
                rpt_Rows.Item("Column5") = dtRow.Item("VCUser")
                rpt_Rows.Item("Column6") = eZeeDate.convertDate(dtRow.Item("VCDate").ToString).ToShortDateString & vbCrLf & dtRow.Item("VCTime")
                rpt_Rows.Item("Column7") = dtRow.Item("VCStatus")
                rpt_Rows.Item("Column8") = dtRow.Item("VCReason")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptTrainingEnrollVCReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 21, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 22, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 23, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 24, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtTitle", Language.getMessage(mstrModuleName, 3, "Title"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 4, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtEnrollDateTime", Language.getMessage(mstrModuleName, 5, "Enroll Date & Time"))
            Call ReportFunction.TextChange(objRpt, "txtEnrollUser", Language.getMessage(mstrModuleName, 6, "Enrolled User"))
            Call ReportFunction.TextChange(objRpt, "txtVoidCancelUser", Language.getMessage(mstrModuleName, 7, "Void/Cancelled User"))
            Call ReportFunction.TextChange(objRpt, "txtVCDateTime", Language.getMessage(mstrModuleName, 8, "Void/Cancel Date & Time"))
            Call ReportFunction.TextChange(objRpt, "txtVCStatus", Language.getMessage(mstrModuleName, 9, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtReason", Language.getMessage(mstrModuleName, 10, "Reason"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 11, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 12, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 13, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Cancelled")
            Language.setMessage(mstrModuleName, 2, "Voided")
            Language.setMessage(mstrModuleName, 3, "Title")
            Language.setMessage(mstrModuleName, 4, "Employee")
            Language.setMessage(mstrModuleName, 5, "Enroll Date & Time")
            Language.setMessage(mstrModuleName, 6, "Enrolled User")
            Language.setMessage(mstrModuleName, 7, "Void/Cancelled User")
            Language.setMessage(mstrModuleName, 8, "Void/Cancel Date & Time")
            Language.setMessage(mstrModuleName, 9, "Status")
            Language.setMessage(mstrModuleName, 10, "Reason")
            Language.setMessage(mstrModuleName, 11, "Printed By :")
            Language.setMessage(mstrModuleName, 12, "Printed Date :")
            Language.setMessage(mstrModuleName, 13, "Page :")
            Language.setMessage(mstrModuleName, 14, "Employee :")
            Language.setMessage(mstrModuleName, 15, "Enrolled By :")
            Language.setMessage(mstrModuleName, 16, "Void User :")
            Language.setMessage(mstrModuleName, 17, "Cancel User :")
            Language.setMessage(mstrModuleName, 18, "Status :")
            Language.setMessage(mstrModuleName, 19, "Training :")
            Language.setMessage(mstrModuleName, 20, "Select")
            Language.setMessage(mstrModuleName, 21, "Prepared By :")
            Language.setMessage(mstrModuleName, 22, "Checked By :")
            Language.setMessage(mstrModuleName, 23, "Approved By :")
            Language.setMessage(mstrModuleName, 24, "Received By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
