﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplinaryPenaltyReport
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisciplinaryPenaltyReport))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIncludeInactiveEmployee = New System.Windows.Forms.CheckBox
        Me.objbtnSeachAction = New eZee.Common.eZeeGradientButton
        Me.lblDisciplineAction = New System.Windows.Forms.Label
        Me.cboDisciplineAction = New System.Windows.Forms.ComboBox
        Me.lblPegFrom = New System.Windows.Forms.Label
        Me.lblPegTo = New System.Windows.Forms.Label
        Me.dtpPToDate = New System.Windows.Forms.DateTimePicker
        Me.dtpPFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblPeffFrom = New System.Windows.Forms.Label
        Me.lblPeffTo = New System.Windows.Forms.Label
        Me.dtpEPToDate = New System.Windows.Forms.DateTimePicker
        Me.dtpEPFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblDateFrom = New System.Windows.Forms.Label
        Me.lblTo = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.chkDisplayAllocationBasedOnChargeDate = New System.Windows.Forms.CheckBox
        Me.cboOffenceCategory = New System.Windows.Forms.ComboBox
        Me.lblOffenceCategory = New System.Windows.Forms.Label
        Me.objbtnSearchOffence = New eZee.Common.eZeeGradientButton
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblDisciplineType = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchCategory = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboDisciplineType = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 431)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(768, 55)
        Me.EZeeFooter1.TabIndex = 93
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(475, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(571, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(667, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(768, 60)
        Me.eZeeHeader.TabIndex = 94
        Me.eZeeHeader.Title = ""
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeInactiveEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSeachAction)
        Me.gbFilterCriteria.Controls.Add(Me.lblDisciplineAction)
        Me.gbFilterCriteria.Controls.Add(Me.cboDisciplineAction)
        Me.gbFilterCriteria.Controls.Add(Me.lblPegFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblPegTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpPToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpPFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeffFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeffTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpEPToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpEPFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.chkDisplayAllocationBasedOnChargeDate)
        Me.gbFilterCriteria.Controls.Add(Me.cboOffenceCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblOffenceCategory)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchOffence)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.lblDisciplineType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboDisciplineType)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(507, 287)
        Me.gbFilterCriteria.TabIndex = 95
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeInactiveEmployee
        '
        Me.chkIncludeInactiveEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmployee.Location = New System.Drawing.Point(166, 258)
        Me.chkIncludeInactiveEmployee.Name = "chkIncludeInactiveEmployee"
        Me.chkIncludeInactiveEmployee.Size = New System.Drawing.Size(292, 17)
        Me.chkIncludeInactiveEmployee.TabIndex = 163
        Me.chkIncludeInactiveEmployee.Text = "Include Inactive Employee"
        Me.chkIncludeInactiveEmployee.UseVisualStyleBackColor = True
        '
        'objbtnSeachAction
        '
        Me.objbtnSeachAction.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSeachAction.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSeachAction.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSeachAction.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSeachAction.BorderSelected = False
        Me.objbtnSeachAction.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSeachAction.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSeachAction.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSeachAction.Location = New System.Drawing.Point(467, 121)
        Me.objbtnSeachAction.Name = "objbtnSeachAction"
        Me.objbtnSeachAction.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSeachAction.TabIndex = 161
        '
        'lblDisciplineAction
        '
        Me.lblDisciplineAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineAction.Location = New System.Drawing.Point(22, 124)
        Me.lblDisciplineAction.Name = "lblDisciplineAction"
        Me.lblDisciplineAction.Size = New System.Drawing.Size(138, 17)
        Me.lblDisciplineAction.TabIndex = 160
        Me.lblDisciplineAction.Text = "Penalty Given"
        '
        'cboDisciplineAction
        '
        Me.cboDisciplineAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineAction.DropDownWidth = 180
        Me.cboDisciplineAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineAction.FormattingEnabled = True
        Me.cboDisciplineAction.Location = New System.Drawing.Point(166, 121)
        Me.cboDisciplineAction.Name = "cboDisciplineAction"
        Me.cboDisciplineAction.Size = New System.Drawing.Size(292, 21)
        Me.cboDisciplineAction.TabIndex = 159
        '
        'lblPegFrom
        '
        Me.lblPegFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPegFrom.Location = New System.Drawing.Point(22, 96)
        Me.lblPegFrom.Name = "lblPegFrom"
        Me.lblPegFrom.Size = New System.Drawing.Size(138, 17)
        Me.lblPegFrom.TabIndex = 158
        Me.lblPegFrom.Text = "Penalty Expiry From"
        '
        'lblPegTo
        '
        Me.lblPegTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPegTo.Location = New System.Drawing.Point(296, 97)
        Me.lblPegTo.Name = "lblPegTo"
        Me.lblPegTo.Size = New System.Drawing.Size(37, 15)
        Me.lblPegTo.TabIndex = 157
        Me.lblPegTo.Text = "To"
        Me.lblPegTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpPToDate
        '
        Me.dtpPToDate.Checked = False
        Me.dtpPToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPToDate.Location = New System.Drawing.Point(339, 94)
        Me.dtpPToDate.Name = "dtpPToDate"
        Me.dtpPToDate.ShowCheckBox = True
        Me.dtpPToDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpPToDate.TabIndex = 155
        '
        'dtpPFromDate
        '
        Me.dtpPFromDate.Checked = False
        Me.dtpPFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPFromDate.Location = New System.Drawing.Point(166, 94)
        Me.dtpPFromDate.Name = "dtpPFromDate"
        Me.dtpPFromDate.ShowCheckBox = True
        Me.dtpPFromDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpPFromDate.TabIndex = 156
        '
        'lblPeffFrom
        '
        Me.lblPeffFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeffFrom.Location = New System.Drawing.Point(22, 69)
        Me.lblPeffFrom.Name = "lblPeffFrom"
        Me.lblPeffFrom.Size = New System.Drawing.Size(138, 17)
        Me.lblPeffFrom.TabIndex = 154
        Me.lblPeffFrom.Text = "Penalty Effective From"
        '
        'lblPeffTo
        '
        Me.lblPeffTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeffTo.Location = New System.Drawing.Point(296, 70)
        Me.lblPeffTo.Name = "lblPeffTo"
        Me.lblPeffTo.Size = New System.Drawing.Size(37, 15)
        Me.lblPeffTo.TabIndex = 153
        Me.lblPeffTo.Text = "To"
        Me.lblPeffTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEPToDate
        '
        Me.dtpEPToDate.Checked = False
        Me.dtpEPToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEPToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEPToDate.Location = New System.Drawing.Point(339, 67)
        Me.dtpEPToDate.Name = "dtpEPToDate"
        Me.dtpEPToDate.ShowCheckBox = True
        Me.dtpEPToDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpEPToDate.TabIndex = 151
        '
        'dtpEPFromDate
        '
        Me.dtpEPFromDate.Checked = False
        Me.dtpEPFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEPFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEPFromDate.Location = New System.Drawing.Point(166, 67)
        Me.dtpEPFromDate.Name = "dtpEPFromDate"
        Me.dtpEPFromDate.ShowCheckBox = True
        Me.dtpEPFromDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpEPFromDate.TabIndex = 152
        '
        'lblDateFrom
        '
        Me.lblDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateFrom.Location = New System.Drawing.Point(22, 42)
        Me.lblDateFrom.Name = "lblDateFrom"
        Me.lblDateFrom.Size = New System.Drawing.Size(138, 17)
        Me.lblDateFrom.TabIndex = 150
        Me.lblDateFrom.Text = "Charge Date"
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(296, 43)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(37, 15)
        Me.lblTo.TabIndex = 149
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(339, 40)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.ShowCheckBox = True
        Me.dtpToDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpToDate.TabIndex = 147
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(166, 40)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.ShowCheckBox = True
        Me.dtpFromDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpFromDate.TabIndex = 148
        '
        'chkDisplayAllocationBasedOnChargeDate
        '
        Me.chkDisplayAllocationBasedOnChargeDate.Checked = True
        Me.chkDisplayAllocationBasedOnChargeDate.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDisplayAllocationBasedOnChargeDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDisplayAllocationBasedOnChargeDate.Location = New System.Drawing.Point(166, 235)
        Me.chkDisplayAllocationBasedOnChargeDate.Name = "chkDisplayAllocationBasedOnChargeDate"
        Me.chkDisplayAllocationBasedOnChargeDate.Size = New System.Drawing.Size(292, 17)
        Me.chkDisplayAllocationBasedOnChargeDate.TabIndex = 145
        Me.chkDisplayAllocationBasedOnChargeDate.Text = "Display Employee Allocation Based on Charge Date"
        Me.chkDisplayAllocationBasedOnChargeDate.UseVisualStyleBackColor = True
        '
        'cboOffenceCategory
        '
        Me.cboOffenceCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOffenceCategory.DropDownWidth = 300
        Me.cboOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOffenceCategory.FormattingEnabled = True
        Me.cboOffenceCategory.Location = New System.Drawing.Point(166, 148)
        Me.cboOffenceCategory.Name = "cboOffenceCategory"
        Me.cboOffenceCategory.Size = New System.Drawing.Size(292, 21)
        Me.cboOffenceCategory.TabIndex = 139
        '
        'lblOffenceCategory
        '
        Me.lblOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOffenceCategory.Location = New System.Drawing.Point(22, 150)
        Me.lblOffenceCategory.Name = "lblOffenceCategory"
        Me.lblOffenceCategory.Size = New System.Drawing.Size(138, 17)
        Me.lblOffenceCategory.TabIndex = 138
        Me.lblOffenceCategory.Text = "Offence Category"
        '
        'objbtnSearchOffence
        '
        Me.objbtnSearchOffence.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOffence.BorderSelected = False
        Me.objbtnSearchOffence.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOffence.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOffence.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOffence.Location = New System.Drawing.Point(467, 175)
        Me.objbtnSearchOffence.Name = "objbtnSearchOffence"
        Me.objbtnSearchOffence.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOffence.TabIndex = 143
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(410, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 20
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDisciplineType
        '
        Me.lblDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineType.Location = New System.Drawing.Point(22, 177)
        Me.lblDisciplineType.Name = "lblDisciplineType"
        Me.lblDisciplineType.Size = New System.Drawing.Size(138, 17)
        Me.lblDisciplineType.TabIndex = 141
        Me.lblDisciplineType.Text = "Offence Description"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(467, 202)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 59
        '
        'objbtnSearchCategory
        '
        Me.objbtnSearchCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCategory.BorderSelected = False
        Me.objbtnSearchCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCategory.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCategory.Location = New System.Drawing.Point(467, 148)
        Me.objbtnSearchCategory.Name = "objbtnSearchCategory"
        Me.objbtnSearchCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCategory.TabIndex = 140
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(22, 204)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(138, 17)
        Me.lblEmployee.TabIndex = 57
        Me.lblEmployee.Text = "Employee"
        '
        'cboDisciplineType
        '
        Me.cboDisciplineType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineType.DropDownWidth = 1000
        Me.cboDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineType.FormattingEnabled = True
        Me.cboDisciplineType.Location = New System.Drawing.Point(166, 175)
        Me.cboDisciplineType.Name = "cboDisciplineType"
        Me.cboDisciplineType.Size = New System.Drawing.Size(292, 21)
        Me.cboDisciplineType.TabIndex = 142
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(166, 202)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(292, 21)
        Me.cboEmployee.TabIndex = 58
        '
        'frmDisciplinaryPenaltyReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(768, 486)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisciplinaryPenaltyReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Disciplinary Penalty Report"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkDisplayAllocationBasedOnChargeDate As System.Windows.Forms.CheckBox
    Friend WithEvents cboOffenceCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblOffenceCategory As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchOffence As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblDisciplineType As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboDisciplineType As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblPegFrom As System.Windows.Forms.Label
    Friend WithEvents lblPegTo As System.Windows.Forms.Label
    Friend WithEvents dtpPToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpPFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPeffFrom As System.Windows.Forms.Label
    Friend WithEvents lblPeffTo As System.Windows.Forms.Label
    Friend WithEvents dtpEPToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpEPFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateFrom As System.Windows.Forms.Label
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSeachAction As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDisciplineAction As System.Windows.Forms.Label
    Friend WithEvents cboDisciplineAction As System.Windows.Forms.ComboBox
    Friend WithEvents chkIncludeInactiveEmployee As System.Windows.Forms.CheckBox
End Class
