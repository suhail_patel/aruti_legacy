﻿Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmDisciplinaryPenaltyReport

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplinaryPenaltyReport"
    Private objDisciplinaryPenalty As clsDisciplinaryPenaltyReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Constructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        objDisciplinaryPenalty = New clsDisciplinaryPenaltyReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objDisciplinaryPenalty.SetDefaultValue()
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objCategory As New clsCommon_Master
        Dim objDisciplineAction As New clsAction_Reason
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        FinancialYear._Object._YearUnkid, _
                                                        Company._Object._Companyunkid, _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        ConfigParameter._Object._UserAccessModeSetting, _
                                                        True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objCategory.getComboList(clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY, True, "list")
            With cboOffenceCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objDisciplineAction.getComboList("Action", True, True)
            With cboDisciplineAction
                .ValueMember = "actionreasonunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            objEmployee = Nothing
            objCategory = Nothing
            objDisciplineAction = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboDisciplineType.SelectedValue = 0
            cboOffenceCategory.SelectedValue = 0
            cboDisciplineAction.SelectedValue = 0

            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime

            dtpEPFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpEPToDate.Value = ConfigParameter._Object._CurrentDateAndTime

            dtpPFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpPToDate.Value = ConfigParameter._Object._CurrentDateAndTime

            dtpFromDate.Checked = False
            dtpToDate.Checked = False

            dtpEPFromDate.Checked = False
            dtpEPToDate.Checked = False

            dtpPFromDate.Checked = False
            dtpToDate.Checked = False

            chkIncludeInactiveEmployee.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objDisciplinaryPenalty.SetDefaultValue()

            objDisciplinaryPenalty._EmployeeId = cboEmployee.SelectedValue
            objDisciplinaryPenalty._EmployeeName = cboEmployee.Text

            If dtpFromDate.Checked = True AndAlso dtpToDate.Checked = True Then
                objDisciplinaryPenalty._ChargeDateFrom = dtpFromDate.Value.Date
                objDisciplinaryPenalty._ChargeDateTo = dtpToDate.Value.Date
            End If

            If dtpEPFromDate.Checked = True AndAlso dtpEPToDate.Checked = True Then
                objDisciplinaryPenalty._PenEffDateFrom = dtpEPFromDate.Value.Date
                objDisciplinaryPenalty._PenEffDateTo = dtpEPToDate.Value.Date
            End If

            If dtpPFromDate.Checked = True AndAlso dtpPToDate.Checked = True Then
                objDisciplinaryPenalty._PenExpDateFrom = dtpPFromDate.Value.Date
                objDisciplinaryPenalty._PenExpDateTo = dtpPToDate.Value.Date
            End If

            objDisciplinaryPenalty._OffenceCategory = cboOffenceCategory.Text
            objDisciplinaryPenalty._OffenceCategoryId = CInt(cboOffenceCategory.SelectedValue)
            objDisciplinaryPenalty._OffenceDescrId = CInt(cboDisciplineType.SelectedValue)
            objDisciplinaryPenalty._OffenceDescription = cboDisciplineType.Text
            objDisciplinaryPenalty._ViewByIds = mstrStringIds
            objDisciplinaryPenalty._ViewIndex = mintViewIdx
            objDisciplinaryPenalty._ViewByName = mstrStringName
            objDisciplinaryPenalty._Analysis_Fields = mstrAnalysis_Fields
            objDisciplinaryPenalty._Analysis_Join = mstrAnalysis_Join
            objDisciplinaryPenalty._Analysis_OrderBy = mstrAnalysis_OrderBy
            objDisciplinaryPenalty._Report_GroupName = mstrReport_GroupName
            objDisciplinaryPenalty._ShowAllocationBasedOnChargeDate = chkDisplayAllocationBasedOnChargeDate.Checked
            objDisciplinaryPenalty._ShowAllocationBasedOnChargeDateString = chkDisplayAllocationBasedOnChargeDate.Text
            objDisciplinaryPenalty._ActionId = cboDisciplineAction.SelectedValue
            objDisciplinaryPenalty._ActionName = cboDisciplineAction.Text
            objDisciplinaryPenalty._IncludeInactiveEmployee = chkIncludeInactiveEmployee.Checked
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Private Sub frmDisciplinaryPenaltyReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objDisciplinaryPenalty = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplinaryPenaltyReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplinaryPenaltyReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            eZeeHeader.Title = objDisciplinaryPenalty._ReportName
            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplinaryPenaltyReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("TAB")
                    e.Handled = True
                    Exit Select

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objDisciplinaryPenalty.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDisciplinaryPenaltyReport.SetMessages()
            objfrm._Other_ModuleNames = "clsDisciplinaryPenaltyReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.ValueMember = cboEmployee.ValueMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchOffence_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOffence.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboDisciplineType.DataSource
            frm.DisplayMember = cboDisciplineType.DisplayMember
            frm.ValueMember = cboDisciplineType.ValueMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboDisciplineType.SelectedValue = frm.SelectedValue
                cboDisciplineType.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOffenceCategory.DataSource
            frm.DisplayMember = cboOffenceCategory.DisplayMember
            frm.ValueMember = cboOffenceCategory.ValueMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboOffenceCategory.SelectedValue = frm.SelectedValue
                cboOffenceCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSeachAction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSeachAction.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboDisciplineAction.DataSource
            frm.DisplayMember = cboDisciplineAction.DisplayMember
            frm.ValueMember = cboDisciplineAction.ValueMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboDisciplineAction.SelectedValue = frm.SelectedValue
                cboDisciplineAction.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSeachAction_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboOffenceCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOffenceCategory.SelectedIndexChanged
        Dim objOffence As New clsDisciplineType
        Dim dsList As New DataSet
        Try
            dsList = objOffence.getComboList("List", True, CInt(cboOffenceCategory.SelectedValue))
            With cboDisciplineType
                .ValueMember = "disciplinetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: cboOffenceCategory_SelectedIndexChanged; Module Name: " & mstrModuleName)
        Finally
            objOffence = Nothing : dsList.Dispose()
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog("EM")
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.chkDisplayAllocationBasedOnChargeDate.Text = Language._Object.getCaption(Me.chkDisplayAllocationBasedOnChargeDate.Name, Me.chkDisplayAllocationBasedOnChargeDate.Text)
            Me.lblOffenceCategory.Text = Language._Object.getCaption(Me.lblOffenceCategory.Name, Me.lblOffenceCategory.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.lblDisciplineType.Text = Language._Object.getCaption(Me.lblDisciplineType.Name, Me.lblDisciplineType.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPegFrom.Text = Language._Object.getCaption(Me.lblPegFrom.Name, Me.lblPegFrom.Text)
            Me.lblPegTo.Text = Language._Object.getCaption(Me.lblPegTo.Name, Me.lblPegTo.Text)
            Me.lblPeffFrom.Text = Language._Object.getCaption(Me.lblPeffFrom.Name, Me.lblPeffFrom.Text)
            Me.lblPeffTo.Text = Language._Object.getCaption(Me.lblPeffTo.Name, Me.lblPeffTo.Text)
            Me.lblDateFrom.Text = Language._Object.getCaption(Me.lblDateFrom.Name, Me.lblDateFrom.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblDisciplineAction.Text = Language._Object.getCaption(Me.lblDisciplineAction.Name, Me.lblDisciplineAction.Text)
            Me.chkIncludeInactiveEmployee.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmployee.Name, Me.chkIncludeInactiveEmployee.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class