'************************************************************************************************************************************
'Class Name : clsEmployeeSkills.vb
'Purpose    :
'Date       :7th Feb 2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsTrainingDetailReport

#Region " Private Variables "

    Private Const mstrModuleName = "clsTrainingDetailReport"
    Dim objDataOperation As clsDataOperation
    Dim objTrainingSchedule As clsTraining_Scheduling
    Dim objQMaster As clsCommon_Master
    Dim objRMaster As clsCommon_Master
    Dim objInstituteMaster As clsinstitute_master
    Dim objFYear As clsCompany_Master

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Try
            objTrainingSchedule = New clsTraining_Scheduling
            objQMaster = New clsCommon_Master
            objRMaster = New clsCommon_Master
            objInstituteMaster = New clsinstitute_master
            objFYear = New clsCompany_Master
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Constructor - New; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Public Function "

    Public Sub Generate_Training_Info(ByVal intTrainingId As Integer, ByVal eAction As enPrintAction, ByVal mblIsActive As Boolean)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim rpt_Resources As ArutiReport.Designer.dsArutiReport
        Dim rpt_Trainers As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            objDataOperation.AddParameter("@TrainingId", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingId)

            '===========================================================================================> TRAINING RESOURCES ====> START
            StrQ = "SELECT " & _
                      "	 ISNULL(cfcommon_master.name,'') AS Resources " & _
                      "	,hrtraining_resources_tran.resources_remark AS Remark " & _
                      "FROM hrtraining_resources_tran " & _
                      " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_resources_tran.resourceunkid AND cfcommon_master.mastertype = " & _
                                clsCommon_Master.enCommonMaster.TRAINING_RESOURCES & " " & _
                      "WHERE ISNULL(isvoid,0) = 0  " & _
                      " AND ISNULL(iscancel,0) = 0 " & _
                      " AND trainingschedulingunkid = @TrainingId "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Resources = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Resources.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("Resources")
                rpt_Row.Item("Column2") = dtRow.Item("Remark")

                rpt_Resources.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next
            '===========================================================================================> TRAINING RESOURCES ====> END

            '===========================================================================================> TRAINERS INFO. ====> START


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            'StrQ = "SELECT " & _
            '            "    CASE WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN " & _
            '            "                           ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
            '            "             WHEN hrtraining_trainers_tran.employeeunkid < 0 THEN " & _
            '            "                           ISNULL(hrtraining_trainers_tran.other_name,'') END AS EName " & _
            '            ",  @Level+' '+CAST(hrtraining_trainers_tran.trainer_level_id AS NVARCHAR(10)) AS TLevel " & _
            '            ",  CASE WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN " & _
            '            "                           ISNULL(hrdepartment_master.name,'') " & _
            '            "            WHEN hrtraining_trainers_tran.employeeunkid < 0 THEN " & _
            '            "                           ISNULL(hrtraining_trainers_tran.other_position,'') END Dept " & _
            '            ",  CASE WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN " & _
            '            "                           ISNULL(@CName,'') " & _
            '            "            WHEN hrtraining_trainers_tran.employeeunkid < 0 THEN " & _
            '            "                           ISNULL(hrtraining_trainers_tran.other_company,'') END TCName " & _
            '            ",  CASE WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN " & _
            '            "                           ISNULL(hremployee_master.present_tel_no,'') " & _
            '            "            WHEN hrtraining_trainers_tran.employeeunkid < 0 THEN " & _
            '            "                           ISNULL(hrtraining_trainers_tran.other_contactno,'') END TContact " & _
            '            "FROM hrtraining_trainers_tran " & _
            '            "	LEFT JOIN hremployee_master ON hrtraining_trainers_tran.employeeunkid = hremployee_master.employeeunkid AND isactive = 1 " & _
            '            "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '            "WHERE ISNULL(isvoid,0) = 0 AND ISNULL(iscancel,0) = 0 " & _
            '            "	AND trainingschedulingunkid = @TrainingId "


            StrQ = "SELECT " & _
                        "    CASE WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN "

            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            '" ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Trainees"
            If mblnFirstNamethenSurname = False Then
                StrQ &= "                           ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') "
            Else
                StrQ &= "                           ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') "
            End If
            'SANDEEP [ 26 MAR 2014 ] -- END

            StrQ &= "             WHEN hrtraining_trainers_tran.employeeunkid < 0 THEN " & _
                    "                           ISNULL(hrtraining_trainers_tran.other_name,'') END AS EName " & _
                    ",  @Level+' '+CAST(hrtraining_trainers_tran.trainer_level_id AS NVARCHAR(10)) AS TLevel " & _
                    ",  CASE WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN " & _
                    "                           ISNULL(hrdepartment_master.name,'') " & _
                    "            WHEN hrtraining_trainers_tran.employeeunkid < 0 THEN " & _
                    "                           ISNULL(hrtraining_trainers_tran.other_position,'') END Dept " & _
                    ",  CASE WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN " & _
                    "                           ISNULL(@CName,'') " & _
                    "            WHEN hrtraining_trainers_tran.employeeunkid < 0 THEN " & _
                    "                           ISNULL(hrtraining_trainers_tran.other_company,'') END TCName " & _
                    ",  CASE WHEN hrtraining_trainers_tran.employeeunkid > 0 THEN " & _
                    "                           ISNULL(hremployee_master.present_tel_no,'') " & _
                    "            WHEN hrtraining_trainers_tran.employeeunkid < 0 THEN " & _
                    "                           ISNULL(hrtraining_trainers_tran.other_contactno,'') END TContact " & _
                    "FROM hrtraining_trainers_tran " & _
                    "	LEFT JOIN hremployee_master ON hrtraining_trainers_tran.employeeunkid = hremployee_master.employeeunkid  " & _
                    "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    "WHERE ISNULL(isvoid,0) = 0 AND ISNULL(iscancel,0) = 0 " & _
                    "	AND trainingschedulingunkid = @TrainingId "

            If mblIsActive = False Then
                StrQ &= " AND  hremployee_master.isactive = 1"
            End If

            'Pinkal (24-Jun-2011) -- End

            objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Level"))
            objDataOperation.AddParameter("@CName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Company._Object._Name)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Trainers = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Trainers.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("EName")
                rpt_Row.Item("Column2") = dtRow.Item("TLevel")
                rpt_Row.Item("Column3") = dtRow.Item("Dept")
                rpt_Row.Item("Column4") = dtRow.Item("TCName")
                rpt_Row.Item("Column5") = dtRow.Item("TContact")

                rpt_Trainers.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next
            '===========================================================================================> TRAINERS INFO. ====> END

            objTrainingSchedule._Trainingschedulingunkid = intTrainingId
            objQMaster._Masterunkid = objTrainingSchedule._Qualificationgroupunkid
            objRMaster._Masterunkid = objTrainingSchedule._Resultgroupunkid
            objInstituteMaster._Instituteunkid = objTrainingSchedule._Traninginstituteunkid
            objFYear._YearUnkid = objTrainingSchedule._Yearunkid


            objRpt = New ArutiReport.Designer.rptTrainingDetailReport

            objRpt.Subreports("rptResources").SetDataSource(rpt_Resources)
            objRpt.Subreports("rptTrainers").SetDataSource(rpt_Trainers)

            Call ReportFunction.TextChange(objRpt, "txtCompany", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt, "txtAddress", Company._Object._Address1 & " " & Company._Object._Address2)
            Call ReportFunction.TextChange(objRpt, "lblTitle", Language.getMessage(mstrModuleName, 2, "Course Title :"))
            Call ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 3, "Training Year :"))
            Call ReportFunction.TextChange(objRpt, "lblQlalification", Language.getMessage(mstrModuleName, 4, "Qualification Group :"))
            Call ReportFunction.TextChange(objRpt, "lblTotalAmount", Language.getMessage(mstrModuleName, 5, "Total Amount"))
            Call ReportFunction.TextChange(objRpt, "lblTelNo", Language.getMessage(mstrModuleName, 6, "Tel. No."))
            Call ReportFunction.TextChange(objRpt, "lblStartDateTime", Language.getMessage(mstrModuleName, 7, "Start Date & Time :"))
            Call ReportFunction.TextChange(objRpt, "lblProvider", Language.getMessage(mstrModuleName, 8, "Training Provider :"))
            Call ReportFunction.TextChange(objRpt, "lblEndDateTime", Language.getMessage(mstrModuleName, 9, "End Date & Time :"))
            Call ReportFunction.TextChange(objRpt, "lblContactPerson", Language.getMessage(mstrModuleName, 10, "Contact Person :"))
            Call ReportFunction.TextChange(objRpt, "lblResultGroup", Language.getMessage(mstrModuleName, 11, "Result Group :"))
            Call ReportFunction.TextChange(objRpt, "lblVenue", Language.getMessage(mstrModuleName, 12, "Venue :"))
            Call ReportFunction.TextChange(objRpt, "lblEligibility", Language.getMessage(mstrModuleName, 13, "Eligibility :"))
            Call ReportFunction.TextChange(objRpt, "lblTrainingRemark", Language.getMessage(mstrModuleName, 14, "Remark :"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptResources"), "lblTrainingResources", Language.getMessage(mstrModuleName, 15, "Training Resources"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptResources"), "lblResources", Language.getMessage(mstrModuleName, 16, "Resources"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptResources"), "lblRemark", Language.getMessage(mstrModuleName, 17, "Resources Remark"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainers"), "lblTrainersInfo", Language.getMessage(mstrModuleName, 18, "Trainers Information"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainers"), "lblEmployeeName", Language.getMessage(mstrModuleName, 19, "Employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainers"), "lblLevel", Language.getMessage(mstrModuleName, 1, "Level"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainers"), "lblDepartment", Language.getMessage(mstrModuleName, 20, "Department"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainers"), "lblCompany", Language.getMessage(mstrModuleName, 21, "Company"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptTrainers"), "lblContactNo", Language.getMessage(mstrModuleName, 22, "Contact No."))
            Call ReportFunction.TextChange(objRpt, "lblTrainingDetailReport", Language.getMessage(mstrModuleName, 23, "Training Detail Information"))

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            'Call ReportFunction.TextChange(objRpt, "txtTitle", objTrainingSchedule._Course_Title)
            Dim objCMaster As New clsCommon_Master
            objCMaster._Masterunkid = objTrainingSchedule._Course_Title
            Call ReportFunction.TextChange(objRpt, "txtTitle", objCMaster._Name)
            objCMaster = Nothing
            'S.SANDEEP [08-FEB-2017] -- END

            Call ReportFunction.TextChange(objRpt, "txtYear", objFYear._FinancialYear_Name)
            Call ReportFunction.TextChange(objRpt, "txtQualification", objQMaster._Name)
            Dim dblTotAmount As Decimal = Format(CDec(objTrainingSchedule._Fees + objTrainingSchedule._Accomodation + objTrainingSchedule._Allowance + _
                                                               objTrainingSchedule._Exam + objTrainingSchedule._Material + objTrainingSchedule._Meals + objTrainingSchedule._Misc + _
                                                               objTrainingSchedule._Travel), GUI.fmtCurrency)
            Call ReportFunction.TextChange(objRpt, "txtTotalAmount", Format(dblTotAmount, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTelNo", objTrainingSchedule._Contact_No)
            Call ReportFunction.TextChange(objRpt, "txtStartDateTime", objTrainingSchedule._Start_Date.ToShortDateString & " " & objTrainingSchedule._Start_Time.ToShortTimeString)
            Call ReportFunction.TextChange(objRpt, "txtProvider", objInstituteMaster._Institute_Name)
            Call ReportFunction.TextChange(objRpt, "txtEndDateTime", objTrainingSchedule._End_Date.ToShortDateString & " " & objTrainingSchedule._End_Time.ToShortTimeString)
            Call ReportFunction.TextChange(objRpt, "txtContactPerson", objTrainingSchedule._Contact_Person)
            Call ReportFunction.TextChange(objRpt, "txtResultGroup", objRMaster._Name)
            Call ReportFunction.TextChange(objRpt, "txtVenue", objTrainingSchedule._Traning_Venue)
            Call ReportFunction.TextChange(objRpt, "txtEligibilityInfo", objTrainingSchedule._Eligibility_Criteria)
            Call ReportFunction.TextChange(objRpt, "txtTrainingRemark", objTrainingSchedule._Training_Remark)

            Dim prd As New System.Drawing.Printing.PrintDocument
            objRpt.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
            objRpt.PrintOptions.PaperSize = prd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind


            objTrainingSchedule = Nothing
            objQMaster = Nothing
            objRMaster = Nothing
            objInstituteMaster = Nothing
            objFYear = Nothing

            Select Case eAction
                Case enPrintAction.Preview
                    Dim viewer As New ArutiReportViewer()
                    viewer.crwArutiReportViewer.ReportSource = objRpt
                    viewer.crwArutiReportViewer.ShowExportButton = False
                    viewer.crwArutiReportViewer.ShowPrintButton = False
                    viewer.crwArutiReportViewer.EnableDrillDown = False
                    viewer.Show()
                Case enPrintAction.Print
                    objRpt.PrintToPrinter(1, False, 0, 0)
            End Select

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_Training_Info; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

End Class
