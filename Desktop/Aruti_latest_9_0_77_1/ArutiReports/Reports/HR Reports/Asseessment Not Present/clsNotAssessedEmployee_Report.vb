'************************************************************************************************************************************
'Class Name : clsNotAssessedEmployee_Report.vb
'Purpose    :
'Date       : 03 OCT 2013
'Written By : Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>
''' 
Public Class clsNotAssessedEmployee_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsNotAssessedEmployee_Report"
    Private mstrReportId As String = enArutiReport.Not_Asssessd_Employee_Report '115
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mintModeSelectedId As Integer = -1
    Private mstrModeSelectedName As String = String.Empty
    Private mintReportModeId As Integer = 0
    Private mstrReportModeName As String = String.Empty
    'S.SANDEEP [ 22 OCT 2013 ] -- START
    Private mintAppointmentTypeId As Integer = 0
    Private mstrAppointmentTypeName As String = String.Empty
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    'S.SANDEEP [ 22 OCT 2013 ] -- END


    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'S.SANDEEP [ 03 NOV 2014 ] -- START
    Private mblnConsiderEmployeeTerminationOnPeriodDate As Boolean = False
    'S.SANDEEP [ 03 NOV 2014 ] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ModeSelectedId() As Integer
        Set(ByVal value As Integer)
            mintModeSelectedId = value
        End Set
    End Property

    Public WriteOnly Property _ModeSelectedName() As String
        Set(ByVal value As String)
            mstrModeSelectedName = value
        End Set
    End Property

    Public WriteOnly Property _ReportModeId() As Integer
        Set(ByVal value As Integer)
            mintReportModeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportModeName() As String
        Set(ByVal value As String)
            mstrReportModeName = value
        End Set
    End Property

    'S.SANDEEP [ 22 OCT 2013 ] -- START
    Public WriteOnly Property _AppointmentTypeId() As Integer
        Set(ByVal value As Integer)
            mintAppointmentTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AppointmentTypeName() As String
        Set(ByVal value As String)
            mstrAppointmentTypeName = value
        End Set
    End Property

    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property
    'S.SANDEEP [ 22 OCT 2013 ] -- END
    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [  26 MAR 2014 ] -- END

    'S.SANDEEP [ 03 NOV 2014 ] -- START
    Public WriteOnly Property _ConsiderEmployeeTerminationOnPeriodDate() As Boolean
        Set(ByVal value As Boolean)
            mblnConsiderEmployeeTerminationOnPeriodDate = value
        End Set
    End Property
    'S.SANDEEP [ 03 NOV 2014 ] -- END
#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            mstrUserAccessFilter = String.Empty
            mstrAdvance_Filter = String.Empty
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mintModeSelectedId = -1
            mstrModeSelectedName = String.Empty
            mintReportModeId = 0
            mstrReportModeName = String.Empty
            'S.SANDEEP [ 22 OCT 2013 ] -- START
            mintAppointmentTypeId = 0
            mstrAppointmentTypeName = String.Empty
            mdtDate1 = Nothing
            mdtDate2 = Nothing
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            'S.SANDEEP [ 03 NOV 2014 ] -- START
            mblnConsiderEmployeeTerminationOnPeriodDate = False
            'S.SANDEEP [ 03 NOV 2014 ] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function Export_Not_Assessed_Report(ByVal strDatabaseName As String, _
                                               ByVal intUserUnkid As Integer, _
                                               ByVal intYearUnkid As Integer, _
                                               ByVal intCompanyUnkid As Integer, _
                                               ByVal dtPeriodStart As Date, _
                                               ByVal dtPeriodEnd As Date, _
                                               ByVal strUserModeSetting As String, _
                                               ByVal blnOnlyApproved As Boolean, _
                                               ByVal strExportPath As String, ByVal blnOpenAfterExport As Boolean) As Boolean 'S.SANDEEP [13-JUL-2017] -- START {strExportPath,blnOpenAfterExport} -- END


        'Public Function Export_Not_Assessed_Report() As Boolean
        'S.SANDEEP [04 JUN 2015] -- END
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet

        'Shani(01-MAR-2016) -- Start
        'Enhancement :PA External Approver Flow
        Dim strFinalQ As String = String.Empty
        'Shani(01-MAR-2016) -- End

        Try
            'S.SANDEEP [13-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : 
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid
            'S.SANDEEP [13-JUL-2017] -- END

            Dim objData As New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Select Case mintReportModeId
            '    Case enAssessmentMode.SELF_ASSESSMENT
            '        StrQ = "SELECT "
            '        If mintViewIndex > 0 Then
            '            StrQ &= " [Allocation Name] " & _
            '                    ",[Employee Code] " & _
            '                    ",[Employee Name] " & _
            '                    ",[Job Name] " & _
            '                    ",[Department Name] " & _
            '                    ",[Work Station] " & _
            '                    "FROM " & _
            '                    "( "
            '        Else
            '            StrQ &= " [Employee Code] " & _
            '                    ",[Employee Name] " & _
            '                    ",[Job Name] " & _
            '                    ",[Department Name] " & _
            '                    ",[Work Station] " & _
            '                    "FROM " & _
            '                    "( "
            '        End If
            '        Select Case mintModeSelectedId
            '            Case 0 'General Assessment
            '                StrQ &= "SELECT " & _
            '                              " employeecode AS [Employee Code] "
            '                'S.SANDEEP [ 26 MAR 2014 ] -- START
            '                ' ",firstname+' '+ surname AS [Employee Name] " & _
            '                If mblnFirstNamethenSurname = False Then
            '                    StrQ &= ",surname+' ' + firstname AS [Employee Name] "
            '                Else
            '                    StrQ &= ",firstname+' ' + surname AS [Employee Name] "
            '                End If
            '                'S.SANDEEP [ 26 MAR 2014 ] -- END

            '                StrQ &= ",ISNULL(jm.job_name,'') AS [Job Name] " & _
            '                              ",ISNULL(dpt.name,'') AS [Department Name] " & _
            '                              ",ISNULL(cm.name,'') AS [Work Station] "
            '                If mintViewIndex > 0 Then
            '                    StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
            '                Else
            '                    StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
            '                End If
            '                StrQ &= "FROM hremployee_master " & _
            '                              "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
            '                              "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
            '                              "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
            '                StrQ &= mstrAnalysis_Join
            '                'S.SANDEEP [ 05 NOV 2014 ] -- START
            '                'StrQ &= "WHERE employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1) "
            '                StrQ &= "WHERE employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0)) "
            '                'S.SANDEEP [ 05 NOV 2014 ] -- END
            '                If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
            '                    StrQ &= UserAccessLevel._AccessLevelFilterString
            '                End If
            '                If mstrAdvance_Filter.Trim.Length > 0 Then
            '                    StrQ &= " AND " & mstrAdvance_Filter
            '                End If
            '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            '                'S.SANDEEP [ 22 OCT 2013 ] -- START
            '                Select Case mintAppointmentTypeId
            '                    Case enAD_Report_Parameter.APP_DATE_FROM
            '                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
            '                        End If
            '                    Case enAD_Report_Parameter.APP_DATE_BEFORE
            '                        If mdtDate1 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
            '                        End If
            '                    Case enAD_Report_Parameter.APP_DATE_AFTER
            '                        If mdtDate1 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
            '                        End If
            '                End Select
            '                'S.SANDEEP [ 22 OCT 2013 ] -- END

            '            Case 1 'BSC
            '                StrQ &= "SELECT " & _
            '                               "employeecode AS [Employee Code] "
            '                'S.SANDEEP [ 26 MAR 2014 ] -- START
            '                ' ",firstname+' '+ surname AS [Employee Name] " & _
            '                If mblnFirstNamethenSurname = False Then
            '                    StrQ &= ",surname+' ' + firstname AS [Employee Name] "
            '                Else
            '                    StrQ &= ",firstname+' ' + surname AS [Employee Name] "
            '                End If
            '                'S.SANDEEP [ 26 MAR 2014 ] -- END

            '                StrQ &= ",ISNULL(jm.job_name,'') AS [Job Name] " & _
            '                              ",ISNULL(dpt.name,'') AS [Department Name] " & _
            '                              ",ISNULL(cm.name,'') AS [Work Station] "
            '                If mintViewIndex > 0 Then
            '                    StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
            '                Else
            '                    StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
            '                End If
            '                StrQ &= "FROM hremployee_master " & _
            '                              "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
            '                              "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
            '                              "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
            '                StrQ &= mstrAnalysis_Join
            '                'S.SANDEEP [ 05 NOV 2014 ] -- START
            '                'StrQ &= "WHERE employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrbsc_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1) "
            '                StrQ &= "WHERE employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0)) "
            '                'S.SANDEEP [ 05 NOV 2014 ] -- END
            '                If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
            '                    StrQ &= UserAccessLevel._AccessLevelFilterString
            '                End If
            '                If mstrAdvance_Filter.Trim.Length > 0 Then
            '                    StrQ &= " AND " & mstrAdvance_Filter
            '                End If
            '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

            '                'S.SANDEEP [ 22 OCT 2013 ] -- START
            '                Select Case mintAppointmentTypeId
            '                    Case enAD_Report_Parameter.APP_DATE_FROM
            '                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
            '                        End If
            '                    Case enAD_Report_Parameter.APP_DATE_BEFORE
            '                        If mdtDate1 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
            '                        End If
            '                    Case enAD_Report_Parameter.APP_DATE_AFTER
            '                        If mdtDate1 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
            '                        End If
            '                End Select
            '                'S.SANDEEP [ 22 OCT 2013 ] -- END

            '        End Select
            '        StrQ &= ") AS Slf WHERE 1 = 1 ORDER BY [Employee Name] "
            '    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
            '        StrQ = "SELECT "
            '        If mintViewIndex > 0 Then
            '            StrQ &= " [Allocation Name] " & _
            '                    ",[Employee Code] " & _
            '                    ",[Employee Name] " & _
            '                    ",[Job Name] " & _
            '                    ",[Department Name] " & _
            '                    ",[Work Station] " & _
            '                    ",[Assessor Code] " & _
            '                    ",[Assessor Name] " & _
            '                    "FROM " & _
            '                    "( "
            '        Else
            '            StrQ &= " [Employee Code] " & _
            '                    ",[Employee Name] " & _
            '                    ",[Job Name] " & _
            '                    ",[Department Name] " & _
            '                    ",[Work Station] " & _
            '                    ",[Assessor Code] " & _
            '                    ",[Assessor Name] " & _
            '                    "FROM " & _
            '                    "( "
            '        End If
            '        Select Case mintModeSelectedId
            '            Case 0 ' General Assessment
            '                StrQ &= "SELECT DISTINCT " & _
            '                              " hremployee_master.employeecode AS [Employee Code] "


            '                'S.SANDEEP [ 26 MAR 2014 ] -- START
            '                ' ",hremployee_master.firstname+' '+ hremployee_master.surname AS [Employee Name] " & _
            '                If mblnFirstNamethenSurname = False Then
            '                    StrQ &= ",hremployee_master.surname+' ' + hremployee_master.firstname AS [Employee Name] "
            '                Else
            '                    StrQ &= ",hremployee_master.firstname+' ' + hremployee_master.surname AS [Employee Name] "
            '                End If
            '                'S.SANDEEP [ 26 MAR 2014 ] -- END
            '                StrQ &= ",ISNULL(jm.job_name,'') AS [Job Name] " & _
            '                              ",ISNULL(dpt.name,'') AS [Department Name] " & _
            '                        ",ISNULL(cm.name,'') AS [Work Station] " & _
            '                              ",asses.employeecode AS [Assessor Code] " & _
            '                              ",asses.firstname +' '+asses.surname AS [Assessor Name] "
            '                If mintViewIndex > 0 Then
            '                    StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
            '                Else
            '                    StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
            '                End If
            '                'S.SANDEEP [ 05 NOV 2014 ] -- START
            '                'StrQ &= "FROM hrassess_analysis_master " & _
            '                '        "   JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_analysis_master.selfemployeeunkid " & _
            '                '        "   LEFT JOIN hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
            '                '        "   JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
            '                '        "   JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid " & _
            '                '              "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
            '                '        "   JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = '" & IIf(mintReportModeId = 2, 0, 1) & "' AND hrassessor_master.isvoid = 0 " & _
            '                '        "   JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid "
            '                'StrQ &= mstrAnalysis_Join
            '                'StrQ &= "WHERE hrassess_analysis_master.isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND selfemployeeunkid NOT IN " & _
            '                '        "(SELECT selfemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 " & _
            '                '        "INTERSECT " & _
            '                '        "SELECT assessedemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1) "

            '                StrQ &= "FROM hrevaluation_analysis_master " & _
            '                        "   JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.selfemployeeunkid " & _
            '                        "   LEFT JOIN hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
            '                        "   JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
            '                        "   JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid " & _
            '                              "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
            '                        "   JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = '" & IIf(mintReportModeId = 2, 0, 1) & "' AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
            '                        "   JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid "
            '                StrQ &= mstrAnalysis_Join
            '                StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND selfemployeeunkid NOT IN " & _
            '                        "(SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) " & _
            '                        "INTERSECT " & _
            '                        "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) ) "
            '                'S.SANDEEP [ 05 NOV 2014 ] -- END

            '                If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
            '                    StrQ &= UserAccessLevel._AccessLevelFilterString
            '                End If
            '                If mstrAdvance_Filter.Trim.Length > 0 Then
            '                    StrQ &= " AND " & mstrAdvance_Filter
            '                End If
            '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "


            '                'S.SANDEEP [ 03 NOV 2014 ] -- START
            '                StrQ &= " AND CONVERT(CHAR(8),asses.appointeddate,112) <= @enddate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),asses.termination_from_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),asses.termination_to_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),asses.empl_enddate,112), @startdate) >= @startdate "
            '                'S.SANDEEP [ 03 NOV 2014 ] -- END


            '                'S.SANDEEP [ 22 OCT 2013 ] -- START
            '                Select Case mintAppointmentTypeId
            '                    Case enAD_Report_Parameter.APP_DATE_FROM
            '                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
            '                        End If
            '                    Case enAD_Report_Parameter.APP_DATE_BEFORE
            '                        If mdtDate1 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
            '                        End If
            '                    Case enAD_Report_Parameter.APP_DATE_AFTER
            '                        If mdtDate1 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
            '                        End If
            '                End Select
            '                'S.SANDEEP [ 22 OCT 2013 ] -- END

            '            Case 1 ' BSC
            '                StrQ &= "SELECT DISTINCT " & _
            '                               "hremployee_master.employeecode AS [Employee Code] "
            '                'S.SANDEEP [ 26 MAR 2014 ] -- START
            '                ' ",hremployee_master.firstname+' '+ hremployee_master.surname AS [Employee Name] " & _
            '                If mblnFirstNamethenSurname = False Then
            '                    StrQ &= ",hremployee_master.surname+' ' + hremployee_master.firstname AS [Employee Name] "
            '                Else
            '                    StrQ &= ",hremployee_master.firstname+' ' + hremployee_master.surname AS [Employee Name] "
            '                End If
            '                'S.SANDEEP [ 26 MAR 2014 ] -- END
            '                StrQ &= ",ISNULL(jm.job_name,'') AS [Job Name] " & _
            '                              ",ISNULL(dpt.name,'') AS [Department Name] " & _
            '                        ",ISNULL(cm.name,'') AS [Work Station] " & _
            '                              ",asses.employeecode AS [Assessor Code] " & _
            '                              ",asses.firstname +' '+asses.surname AS [Assessor Name] "
            '                If mintViewIndex > 0 Then
            '                    StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
            '                Else
            '                    StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
            '                End If

            '                'S.SANDEEP [ 05 NOV 2014 ] -- START
            '                'StrQ &= "FROM hrbsc_analysis_master " & _
            '                '        "   JOIN hremployee_master ON hremployee_master.employeeunkid = hrbsc_analysis_master.selfemployeeunkid " & _
            '                '        "   LEFT JOIN hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
            '                '        "   JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
            '                '        "   JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid " & _
            '                '              "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
            '                '        "   JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = '" & IIf(mintReportModeId = 2, 0, 1) & "' AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
            '                '        "   JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid "
            '                'StrQ &= mstrAnalysis_Join
            '                'StrQ &= "WHERE hrbsc_analysis_master.isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND selfemployeeunkid NOT IN " & _
            '                '        "(SELECT selfemployeeunkid FROM hrbsc_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 " & _
            '                '        "INTERSECT " & _
            '                '        "SELECT assessedemployeeunkid FROM hrbsc_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1) "

            '                StrQ &= "FROM hrevaluation_analysis_master " & _
            '                        "   JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.selfemployeeunkid " & _
            '                        "   LEFT JOIN hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
            '                        "   JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
            '                        "   JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid " & _
            '                              "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
            '                        "   JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = '" & IIf(mintReportModeId = 2, 0, 1) & "' AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
            '                        "   JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid "
            '                StrQ &= mstrAnalysis_Join
            '                StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND selfemployeeunkid NOT IN " & _
            '                        "(SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0) " & _
            '                        "INTERSECT " & _
            '                        "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0)) "
            '                'S.SANDEEP [ 05 NOV 2014 ] -- END

            '                If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
            '                    StrQ &= UserAccessLevel._AccessLevelFilterString
            '                End If
            '                If mstrAdvance_Filter.Trim.Length > 0 Then
            '                    StrQ &= " AND " & mstrAdvance_Filter
            '                End If
            '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

            '                'S.SANDEEP [ 03 NOV 2014 ] -- START
            '                StrQ &= " AND CONVERT(CHAR(8),asses.appointeddate,112) <= @enddate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),asses.termination_from_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),asses.termination_to_date,112),@startdate) >= @startdate " & _
            '                        " AND ISNULL(CONVERT(CHAR(8),asses.empl_enddate,112), @startdate) >= @startdate "
            '                'S.SANDEEP [ 03 NOV 2014 ] -- END

            '                'S.SANDEEP [ 22 OCT 2013 ] -- START
            '                Select Case mintAppointmentTypeId
            '                    Case enAD_Report_Parameter.APP_DATE_FROM
            '                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
            '                        End If
            '                    Case enAD_Report_Parameter.APP_DATE_BEFORE
            '                        If mdtDate1 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
            '                        End If
            '                    Case enAD_Report_Parameter.APP_DATE_AFTER
            '                        If mdtDate1 <> Nothing Then
            '                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
            '                        End If
            '                End Select
            '                'S.SANDEEP [ 22 OCT 2013 ] -- END

            '        End Select
            '        StrQ &= ") AS Slf WHERE 1 = 1 ORDER BY [Employee Name] "
            'End Select



            If mblnConsiderEmployeeTerminationOnPeriodDate = True Then
                If mintPeriodId > 0 Then
                    Dim objPrd As New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPrd._Periodunkid = mintPeriodId
                    objPrd._Periodunkid(strDatabaseName) = mintPeriodId
                    'Sohail (21 Aug 2015) -- End
                    dtPeriodStart = objPrd._Start_Date
                    dtPeriodEnd = objPrd._End_Date
                    objPrd = Nothing
                End If
            Else
                dtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                dtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Select Case mintReportModeId

                Case enAssessmentMode.SELF_ASSESSMENT

                    StrQ = "SELECT "

                    If mintViewIndex > 0 Then

                        StrQ &= " [Allocation Name] " & _
                                ",[Employee Code] " & _
                                ",[Employee Name] " & _
                                ",[Job Name] " & _
                                ",[Department Name] " & _
                                ",[Work Station] " & _
                                "FROM " & _
                                "( "
                    Else

                        StrQ &= " [Employee Code] " & _
                                ",[Employee Name] " & _
                                ",[Job Name] " & _
                                ",[Department Name] " & _
                                ",[Work Station] " & _
                                "FROM " & _
                                "( "
                    End If

                    Select Case mintModeSelectedId

                        Case 0 'General Assessment

                            StrQ &= "SELECT " & _
                                    " employeecode AS [Employee Code] "

                            If mblnFirstNamethenSurname = False Then
                                StrQ &= ",surname+' ' + firstname AS [Employee Name] "
                            Else
                                StrQ &= ",firstname+' ' + surname AS [Employee Name] "
                            End If

                            StrQ &= ",ISNULL(jm.job_name,'') AS [Job Name] " & _
                                    ",ISNULL(dpt.name,'') AS [Department Name] " & _
                                    ",ISNULL(cm.name,'') AS [Work Station] "

                            If mintViewIndex > 0 Then
                                StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
                            Else
                                StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
                            End If

                            StrQ &= "FROM hremployee_master " & _
                                    "LEFT JOIN " & _
                                    "(" & _
                                    "   SELECT " & _
                                    "        employeeunkid " & _
                                    "       ,classunkid " & _
                                    "       ,departmentunkid " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                                    "   FROM hremployee_transfer_tran " & _
                                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.Rno = 1 " & _
                                    "LEFT JOIN  hrclasses_master AS cm ON Alloc.classunkid = cm.classesunkid " & _
                                    "JOIN hrdepartment_master AS dpt ON Alloc.departmentunkid = dpt.departmentunkid " & _
                                    "LEFT JOIN " & _
                                    "(" & _
                                    "   SELECT " & _
                                    "        employeeunkid " & _
                                    "       ,jobunkid " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                                    "   FROM hremployee_categorization_tran " & _
                                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                    ") AS Catr ON Catr.employeeunkid = hremployee_master.employeeunkid AND Catr.Rno = 1 " & _
                                    "JOIN hrjob_master AS jm ON Catr.jobunkid = jm.jobunkid "

                            If xDateJoinQry.Trim.Length > 0 Then
                                StrQ &= xDateJoinQry & " "
                            End If

                            If xUACQry.Trim.Length > 0 Then
                                StrQ &= xUACQry & " "
                            End If

                            If xAdvanceJoinQry.Trim.Length > 0 Then
                                StrQ &= xAdvanceJoinQry & " "
                            End If

                            StrQ &= mstrAnalysis_Join

                            StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0)) "

                            If xUACFiltrQry.Trim.Length > 0 Then
                                StrQ &= " AND " & xUACFiltrQry & " "
                            End If

                            If xDateFilterQry.Trim.Length > 0 Then
                                StrQ &= xDateFilterQry & " "
                            End If

                            If mstrAdvance_Filter.Trim.Length > 0 Then
                                StrQ &= " AND " & mstrAdvance_Filter
                            End If

                            Select Case mintAppointmentTypeId

                                Case enAD_Report_Parameter.APP_DATE_FROM

                                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                                    End If

                                Case enAD_Report_Parameter.APP_DATE_BEFORE

                                    If mdtDate1 <> Nothing Then
                                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                                    End If

                                Case enAD_Report_Parameter.APP_DATE_AFTER

                                    If mdtDate1 <> Nothing Then
                                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                                    End If

                            End Select


                        Case 1 'BSC

                            StrQ &= "SELECT " & _
                                    "employeecode AS [Employee Code] "

                            If mblnFirstNamethenSurname = False Then
                                StrQ &= ",surname+' ' + firstname AS [Employee Name] "
                            Else
                                StrQ &= ",firstname+' ' + surname AS [Employee Name] "
                            End If

                            StrQ &= ",ISNULL(jm.job_name,'') AS [Job Name] " & _
                                    ",ISNULL(dpt.name,'') AS [Department Name] " & _
                                    ",ISNULL(cm.name,'') AS [Work Station] "

                            If mintViewIndex > 0 Then
                                StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
                            Else
                                StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
                            End If

                            StrQ &= "FROM hremployee_master " & _
                                    "LEFT JOIN " & _
                                    "(" & _
                                    "   SELECT " & _
                                    "        employeeunkid " & _
                                    "       ,classunkid " & _
                                    "       ,departmentunkid " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                                    "   FROM hremployee_transfer_tran " & _
                                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.Rno = 1 " & _
                                    "LEFT JOIN  hrclasses_master AS cm ON Alloc.classunkid = cm.classesunkid " & _
                                    "JOIN hrdepartment_master AS dpt ON Alloc.departmentunkid = dpt.departmentunkid " & _
                                    "LEFT JOIN " & _
                                    "(" & _
                                    "   SELECT " & _
                                    "        employeeunkid " & _
                                    "       ,jobunkid " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                                    "   FROM hremployee_categorization_tran " & _
                                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                    ") AS Catr ON Catr.employeeunkid = hremployee_master.employeeunkid AND Catr.Rno = 1 " & _
                                    "JOIN hrjob_master AS jm ON Catr.jobunkid = jm.jobunkid "


                            If xDateJoinQry.Trim.Length > 0 Then
                                StrQ &= xDateJoinQry & " "
                            End If

                            If xUACQry.Trim.Length > 0 Then
                                StrQ &= xUACQry & " "
                            End If

                            If xAdvanceJoinQry.Trim.Length > 0 Then
                                StrQ &= xAdvanceJoinQry & " "
                            End If

                            StrQ &= mstrAnalysis_Join

                            StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0)) "

                            If xUACFiltrQry.Trim.Length > 0 Then
                                StrQ &= " AND " & xUACFiltrQry & " "
                            End If

                            If xDateFilterQry.Trim.Length > 0 Then
                                StrQ &= xDateFilterQry & " "
                            End If

                            If mstrAdvance_Filter.Trim.Length > 0 Then
                                StrQ &= " AND " & mstrAdvance_Filter
                            End If

                            Select Case mintAppointmentTypeId
                                Case enAD_Report_Parameter.APP_DATE_FROM
                                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                                    End If
                                Case enAD_Report_Parameter.APP_DATE_BEFORE
                                    If mdtDate1 <> Nothing Then
                                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                                    End If
                                Case enAD_Report_Parameter.APP_DATE_AFTER
                                    If mdtDate1 <> Nothing Then
                                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                                    End If
                            End Select

                    End Select
                    StrQ &= ") AS Slf WHERE 1 = 1 ORDER BY [Employee Name] "

                Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                    'Shani(01-MAR-2016) -- Start
                    'Enhancement :PA External Approver Flow

                    'StrQ = "SELECT "

                    'If mintViewIndex > 0 Then
                    '    StrQ &= " [Allocation Name] " & _
                    '            ",[Employee Code] " & _
                    '            ",[Employee Name] " & _
                    '            ",[Job Name] " & _
                    '            ",[Department Name] " & _
                    '            ",[Work Station] " & _
                    '            ",[Assessor Code] " & _
                    '            ",[Assessor Name] " & _
                    '            "FROM " & _
                    '            "( "
                    'Else
                    '    StrQ &= " [Employee Code] " & _
                    '            ",[Employee Name] " & _
                    '            ",[Job Name] " & _
                    '            ",[Department Name] " & _
                    '            ",[Work Station] " & _
                    '            ",[Assessor Code] " & _
                    '            ",[Assessor Name] " & _
                    '            "FROM " & _
                    '            "( "
                    'End If
                    'Select Case mintModeSelectedId

                    '    Case 0 ' General Assessment

                    '        StrQ &= "SELECT DISTINCT " & _
                    '                " hremployee_master.employeecode AS [Employee Code] "

                    '        If mblnFirstNamethenSurname = False Then
                    '            StrQ &= ",hremployee_master.surname+' ' + hremployee_master.firstname AS [Employee Name] "
                    '        Else
                    '            StrQ &= ",hremployee_master.firstname+' ' + hremployee_master.surname AS [Employee Name] "
                    '        End If

                    '        StrQ &= ",ISNULL(jm.job_name,'') AS [Job Name] " & _
                    '                ",ISNULL(dpt.name,'') AS [Department Name] " & _
                    '                ",ISNULL(cm.name,'') AS [Work Station] " & _
                    '                ",asses.employeecode AS [Assessor Code] " & _
                    '                ",asses.firstname +' '+asses.surname AS [Assessor Name] "

                    '        If mintViewIndex > 0 Then
                    '            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
                    '        Else
                    '            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
                    '        End If

                    '        StrQ &= "FROM hrevaluation_analysis_master " & _
                    '                "   JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.selfemployeeunkid " & _
                    '                "LEFT JOIN " & _
                    '                "(" & _
                    '                "   SELECT " & _
                    '                "        employeeunkid " & _
                    '                "       ,classunkid " & _
                    '                "       ,departmentunkid " & _
                    '                "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    '                "   FROM hremployee_transfer_tran " & _
                    '                "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    '                ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.Rno = 1 " & _
                    '                "LEFT JOIN  hrclasses_master AS cm ON Alloc.classunkid = cm.classesunkid " & _
                    '                "JOIN hrdepartment_master AS dpt ON Alloc.departmentunkid = dpt.departmentunkid " & _
                    '                "LEFT JOIN " & _
                    '                "(" & _
                    '                "   SELECT " & _
                    '                "        employeeunkid " & _
                    '                "       ,jobunkid " & _
                    '                "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    '                "   FROM hremployee_categorization_tran " & _
                    '                "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    '                ") AS Catr ON Catr.employeeunkid = hremployee_master.employeeunkid AND Catr.Rno = 1 " & _
                    '                "JOIN hrjob_master AS jm ON Catr.jobunkid = jm.jobunkid " & _
                    '                "   JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
                    '                "   JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = '" & IIf(mintReportModeId = 2, 0, 1) & "' AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                    '                "   JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid "


                    '        If xDateJoinQry.Trim.Length > 0 Then
                    '            StrQ &= xDateJoinQry & " "
                    '        End If

                    '        If xUACQry.Trim.Length > 0 Then
                    '            StrQ &= xUACQry & " "
                    '        End If

                    '        If xAdvanceJoinQry.Trim.Length > 0 Then
                    '            StrQ &= xAdvanceJoinQry & " "
                    '        End If


                    '        StrQ &= mstrAnalysis_Join

                    '        StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND selfemployeeunkid NOT IN " & _
                    '                "(SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) " & _
                    '                "INTERSECT " & _
                    '                "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) ) "

                    '        If xUACFiltrQry.Trim.Length > 0 Then
                    '            StrQ &= " AND " & xUACFiltrQry & " "
                    '        End If

                    '        If xDateFilterQry.Trim.Length > 0 Then
                    '            StrQ &= xDateFilterQry & " "
                    '            StrQ &= System.Text.RegularExpressions.Regex.Replace(xDateFilterQry, "\bhremployee_master\b", "asses") & " "
                    '        End If

                    '        If mstrAdvance_Filter.Trim.Length > 0 Then
                    '            StrQ &= " AND " & mstrAdvance_Filter
                    '        End If

                    '        Select Case mintAppointmentTypeId
                    '            Case enAD_Report_Parameter.APP_DATE_FROM
                    '                If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                    '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                    '                End If
                    '            Case enAD_Report_Parameter.APP_DATE_BEFORE
                    '                If mdtDate1 <> Nothing Then
                    '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                    '                End If
                    '            Case enAD_Report_Parameter.APP_DATE_AFTER
                    '                If mdtDate1 <> Nothing Then
                    '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                    '                End If
                    '        End Select


                    '    Case 1 ' BSC

                    '        StrQ &= "SELECT DISTINCT " & _
                    '                " hremployee_master.employeecode AS [Employee Code] "
                    '        If mblnFirstNamethenSurname = False Then
                    '            StrQ &= ",hremployee_master.surname+' ' + hremployee_master.firstname AS [Employee Name] "
                    '        Else
                    '            StrQ &= ",hremployee_master.firstname+' ' + hremployee_master.surname AS [Employee Name] "
                    '        End If

                    '        StrQ &= ",ISNULL(jm.job_name,'') AS [Job Name] " & _
                    '                ",ISNULL(dpt.name,'') AS [Department Name] " & _
                    '                ",ISNULL(cm.name,'') AS [Work Station] " & _
                    '                ",asses.employeecode AS [Assessor Code] " & _
                    '                ",asses.firstname +' '+asses.surname AS [Assessor Name] "
                    '        If mintViewIndex > 0 Then
                    '            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
                    '        Else
                    '            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
                    '        End If

                    '        StrQ &= "FROM hrevaluation_analysis_master " & _
                    '                "   JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.selfemployeeunkid " & _
                    '                "LEFT JOIN " & _
                    '                "(" & _
                    '                "   SELECT " & _
                    '                "        employeeunkid " & _
                    '                "       ,classunkid " & _
                    '                "       ,departmentunkid " & _
                    '                "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    '                "   FROM hremployee_transfer_tran " & _
                    '                "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    '                ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.Rno = 1 " & _
                    '                "LEFT JOIN  hrclasses_master AS cm ON Alloc.classunkid = cm.classesunkid " & _
                    '                "JOIN hrdepartment_master AS dpt ON Alloc.departmentunkid = dpt.departmentunkid " & _
                    '                "LEFT JOIN " & _
                    '                "(" & _
                    '                "   SELECT " & _
                    '                "        employeeunkid " & _
                    '                "       ,jobunkid " & _
                    '                "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    '                "   FROM hremployee_categorization_tran " & _
                    '                "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    '                ") AS Catr ON Catr.employeeunkid = hremployee_master.employeeunkid AND Catr.Rno = 1 " & _
                    '                "JOIN hrjob_master AS jm ON Catr.jobunkid = jm.jobunkid " & _
                    '                "   JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
                    '                "   JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = '" & IIf(mintReportModeId = 2, 0, 1) & "' AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                    '                "   JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid "

                    '        If xDateJoinQry.Trim.Length > 0 Then
                    '            StrQ &= xDateJoinQry & " "
                    '        End If

                    '        If xUACQry.Trim.Length > 0 Then
                    '            StrQ &= xUACQry & " "
                    '        End If

                    '        If xAdvanceJoinQry.Trim.Length > 0 Then
                    '            StrQ &= xAdvanceJoinQry & " "
                    '        End If

                    '        StrQ &= mstrAnalysis_Join

                    '        StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND selfemployeeunkid NOT IN " & _
                    '                "(SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0) " & _
                    '                "INTERSECT " & _
                    '                "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0)) "


                    '        If xUACFiltrQry.Trim.Length > 0 Then
                    '            StrQ &= " AND " & xUACFiltrQry & " "
                    '        End If

                    '        If xDateFilterQry.Trim.Length > 0 Then
                    '            StrQ &= xDateFilterQry & " "
                    '            StrQ &= System.Text.RegularExpressions.Regex.Replace(xDateFilterQry, "\bhremployee_master\b", "asses") & " "
                    '        End If

                    '        If mstrAdvance_Filter.Trim.Length > 0 Then
                    '            StrQ &= " AND " & mstrAdvance_Filter
                    '        End If

                    '        Select Case mintAppointmentTypeId
                    '            Case enAD_Report_Parameter.APP_DATE_FROM
                    '                If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                    '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                    '                End If
                    '            Case enAD_Report_Parameter.APP_DATE_BEFORE
                    '                If mdtDate1 <> Nothing Then
                    '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                    '                End If
                    '            Case enAD_Report_Parameter.APP_DATE_AFTER
                    '                If mdtDate1 <> Nothing Then
                    '                    StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                    '                End If
                    '        End Select


                    'End Select
                    'StrQ &= ") AS Slf WHERE 1 = 1 ORDER BY [Employee Name] "
                    'End Select

                    ''S.SANDEEP [04 JUN 2015] -- END

                    ''S.SANDEEP [ 08 OCT 2014 ] -- START
                    ''Issue {07 - Oct - 2014} : [DENNIS] Assessment NOT done Report(Self Assessment, Assessor Assessment and Reviewer Assessment  both BSC and General) Employee(s) Terminated with reason like death ,End of contract shown in this Reports, I think  report should check the current status of an Employee at that particular date when report was generated.
                    ''objData.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                    ''objData.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

                    ''S.SANDEEP [ 03 NOV 2014 ] -- START
                    ''If mintPeriodId > 0 Then
                    ''    Dim objPrd As New clscommom_period_Tran
                    ''    objPrd._Periodunkid = mintPeriodId
                    ''    objData.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(objPrd._Start_Date))
                    ''    objData.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(objPrd._End_Date))

                    ''    objPrd = Nothing
                    ''End If

                    ''S.SANDEEP [04 JUN 2015] -- START
                    ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    ''If mblnConsiderEmployeeTerminationOnPeriodDate = True Then
                    ''    If mintPeriodId > 0 Then
                    ''        Dim objPrd As New clscommom_period_Tran
                    ''        objPrd._Periodunkid = mintPeriodId
                    ''        objData.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(objPrd._Start_Date))
                    ''        objData.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(objPrd._End_Date))

                    ''        objPrd = Nothing
                    ''    End If
                    ''Else
                    ''    objData.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                    ''    objData.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                    ''End If
                    ''S.SANDEEP [04 JUN 2015] -- END


                    ''S.SANDEEP [ 03 NOV 2014 ] -- END

                    ''S.SANDEEP [ 08 OCT 2014 ] -- END

                    'dsList = objData.ExecQuery(StrQ, "DataTable")

                    'If objData.ErrorMessage <> "" Then
                    '    Throw New Exception(objData.ErrorNumber & ":" & objData.ErrorMessage)
                    'End If

                    StrQ = "SELECT "

                    If mintViewIndex > 0 Then
                        StrQ &= " [Allocation Name], "
                    End If

                    StrQ &= "    [Employee Code] " & _
                            "   ,[Employee Name] " & _
                            "   ,[Job Name] " & _
                            "   ,[Department Name] " & _
                            "   ,[Work Station] "
                    If mintReportModeId = enAssessmentMode.APPRAISER_ASSESSMENT Then
                        StrQ &= "   ,[Assessor Code] " & _
                                "   ,[Assessor Name] "
                    Else
                        StrQ &= "   ,[Assessor Code] AS [Reviewer Code] " & _
                                "   ,[Assessor Name] AS [Reviewer Name] "
                    End If

                    StrQ &= "FROM " & _
                            "   ( " & _
                            "       SELECT DISTINCT " & _
                                    " hremployee_master.employeecode AS [Employee Code] "
                    If mblnFirstNamethenSurname = False Then
                        StrQ &= ",hremployee_master.surname+' ' + hremployee_master.firstname AS [Employee Name] "
                    Else
                        StrQ &= ",hremployee_master.firstname+' ' + hremployee_master.surname AS [Employee Name] "
                    End If
                    StrQ &= ",ISNULL(jm.job_name,'') AS [Job Name] " & _
                            ",ISNULL(dpt.name,'') AS [Department Name] " & _
                            ",ISNULL(cm.name,'') AS [Work Station] " & _
                    "           ,#ASS_CODE# AS [Assessor Code] " & _
                    "           ,#ASS_NAME# AS [Assessor Name] "
                    '",asses.employeecode AS [Assessor Code] " & _
                    '",asses.firstname +' '+asses.surname AS [Assessor Name] "

                    If mintViewIndex > 0 Then
                        StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
                    Else
                        StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
                    End If

                    'S.SANDEEP [25-JAN-2017] -- START
                    'ISSUE/ENHANCEMENT : Report Picking Wrong Column For Checking {selfemployeeunkid -> assessedemployeeunkid}
                    'StrQ &= "FROM hrevaluation_analysis_master " & _
                    '        "   JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.selfemployeeunkid " & _
                    '        "LEFT JOIN " & _
                    '        "(" & _
                    '        "   SELECT " & _
                    '        "        employeeunkid " & _
                    '        "       ,classunkid " & _
                    '        "       ,departmentunkid " & _
                    '        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    '        "   FROM hremployee_transfer_tran " & _
                    '        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    '        ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.Rno = 1 " & _
                    '        "LEFT JOIN  hrclasses_master AS cm ON Alloc.classunkid = cm.classesunkid " & _
                    '        "JOIN hrdepartment_master AS dpt ON Alloc.departmentunkid = dpt.departmentunkid " & _
                    '        "LEFT JOIN " & _
                    '        "(" & _
                    '        "   SELECT " & _
                    '        "        employeeunkid " & _
                    '        "       ,jobunkid " & _
                    '        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    '        "   FROM hremployee_categorization_tran " & _
                    '        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    '        ") AS Catr ON Catr.employeeunkid = hremployee_master.employeeunkid AND Catr.Rno = 1 " & _
                    '        "JOIN hrjob_master AS jm ON Catr.jobunkid = jm.jobunkid " & _
                    '        "   JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                    '        "   JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = '" & IIf(mintReportModeId = 2, 0, 1) & "' AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                    '        "   #ASS_JOIN# " 'S.SANDEEP [27 DEC 2016] -- START {} -- END




                    'S.SANDEEP |07-FEB-2019| -- START
                    'ISSUE/ENHANCEMENT : {#0003479|ARUTI-560}
                    'StrQ &= "FROM hrevaluation_analysis_master " & _
                    '"   JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.assessedemployeeunkid " & _
                    '        "LEFT JOIN " & _
                    '        "(" & _
                    '        "   SELECT " & _
                    '        "        employeeunkid " & _
                    '        "       ,classunkid " & _
                    '        "       ,departmentunkid " & _
                    '        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    '        "   FROM hremployee_transfer_tran " & _
                    '        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    '        ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.Rno = 1 " & _
                    '        "LEFT JOIN  hrclasses_master AS cm ON Alloc.classunkid = cm.classesunkid " & _
                    '        "JOIN hrdepartment_master AS dpt ON Alloc.departmentunkid = dpt.departmentunkid " & _
                    '        "LEFT JOIN " & _
                    '        "(" & _
                    '        "   SELECT " & _
                    '        "        employeeunkid " & _
                    '        "       ,jobunkid " & _
                    '        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    '        "   FROM hremployee_categorization_tran " & _
                    '        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    '        ") AS Catr ON Catr.employeeunkid = hremployee_master.employeeunkid AND Catr.Rno = 1 " & _
                    '        "JOIN hrjob_master AS jm ON Catr.jobunkid = jm.jobunkid " & _
                    '"   JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                    '        "   JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = '" & IIf(mintReportModeId = 2, 0, 1) & "' AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                    '"   #ASS_JOIN# " 'S.SANDEEP [27 DEC 2016] -- START {} -- END
                    ''S.SANDEEP [25-JAN-2017] -- END

                    StrQ &= "FROM hremployee_master " & _
                                    "LEFT JOIN " & _
                                    "(" & _
                                    "   SELECT " & _
                                    "        employeeunkid " & _
                                    "       ,classunkid " & _
                                    "       ,departmentunkid " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                                    "   FROM hremployee_transfer_tran " & _
                                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.Rno = 1 " & _
                                    "LEFT JOIN  hrclasses_master AS cm ON Alloc.classunkid = cm.classesunkid " & _
                                    "JOIN hrdepartment_master AS dpt ON Alloc.departmentunkid = dpt.departmentunkid " & _
                                    "LEFT JOIN " & _
                                    "(" & _
                                    "   SELECT " & _
                                    "        employeeunkid " & _
                                    "       ,jobunkid " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                                    "   FROM hremployee_categorization_tran " & _
                                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                    ") AS Catr ON Catr.employeeunkid = hremployee_master.employeeunkid AND Catr.Rno = 1 " & _
                            "   JOIN hrjob_master AS jm ON Catr.jobunkid = jm.jobunkid " & _
                            "   JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                                    "   JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = '" & IIf(mintReportModeId = 2, 0, 1) & "' AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                            "   #ASS_JOIN# "
                    'S.SANDEEP |07-FEB-2019| -- END






                    '"   JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid "

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry & " "
                    End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry & " "
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry & " "
                    End If
                    StrQ &= mstrAnalysis_Join

                    Select Case mintModeSelectedId
                        'S.SANDEEP [27 DEC 2016] -- START
                        'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                        'Case 0 ' General Assessment
                        '    StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND selfemployeeunkid NOT IN " & _
                        '            "(SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) " & _
                        '            "INTERSECT " & _
                        '            "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) ) "
                        'Case 1 ' BSC
                        '    StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND selfemployeeunkid NOT IN " & _
                        '            "(SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND selfemployeeunkid > 0 AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0) " & _
                        '            "INTERSECT " & _
                        '            "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0)) "

                        'S.SANDEEP [25-JAN-2017] -- START
                        'ISSUE/ENHANCEMENT : Report Picking Wrong Column For Checking {selfemployeeunkid -> assessedemployeeunkid}
                        'Case 0 ' General Assessment
                        '    StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 AND assessmodeid = 1 AND assessedemployeeunkid > 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND assessedemployeeunkid NOT IN " & _
                        '            "(SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0 AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) " & _
                        '            "INTERSECT " & _
                        '            "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) ) "
                        'Case 1 ' BSC
                        '    StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 AND assessmodeid = 1 AND assessedemployeeunkid > 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND assessedemployeeunkid NOT IN " & _
                        '            "(SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0 AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0) " & _
                        '            "INTERSECT " & _
                        '            "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0)) "

                        'S.SANDEEP |07-FEB-2019| -- START
                        'ISSUE/ENHANCEMENT : {#0003479|ARUTI-560}
                        'Case 0 ' General Assessment
                        '    StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND assessedemployeeunkid NOT IN " & _
                        '            "(SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0 AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) " & _
                        '            "INTERSECT " & _
                        '            "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) ) "
                        'Case 1 ' BSC
                        '    StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND assessedemployeeunkid NOT IN " & _
                        '            "(SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0 AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0) " & _
                        '            "INTERSECT " & _
                        '            "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0)) "

                        Case 0 ' General Assessment
                            StrQ &= "WHERE hremployee_master.employeeunkid NOT IN " & _
                                    "(SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0 AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) " & _
                                    "INTERSECT " & _
                                    "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrcompetency_analysis_tran WHERE isvoid = 0) ) "
                        Case 1 ' BSC
                            StrQ &= "WHERE hremployee_master.employeeunkid NOT IN " & _
                                    "(SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0 AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0) " & _
                                    "INTERSECT " & _
                                    "SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND assessmodeid = '" & mintReportModeId & "' AND assessedemployeeunkid > 0  AND iscommitted = 1 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND analysisunkid IN (SELECT analysisunkid FROM hrgoals_analysis_tran WHERE isvoid = 0)) "
                            'S.SANDEEP |07-FEB-2019| -- END


                            'S.SANDEEP [25-JAN-2017] -- END



                            'S.SANDEEP [27 DEC 2016] -- END
                    End Select
                    StrQ &= " #CND# "
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry & " "
                    End If

                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If

                    Select Case mintAppointmentTypeId
                        Case enAD_Report_Parameter.APP_DATE_FROM
                            If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                            End If
                        Case enAD_Report_Parameter.APP_DATE_BEFORE
                            If mdtDate1 <> Nothing Then
                                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                            End If
                        Case enAD_Report_Parameter.APP_DATE_AFTER
                            If mdtDate1 <> Nothing Then
                                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                            End If
                    End Select
                    StrQ &= ") AS Slf WHERE 1 = 1 ORDER BY [Employee Name] "
            End Select

            If mintReportModeId = enAssessmentMode.SELF_ASSESSMENT Then
                strFinalQ = StrQ
            Else
                strFinalQ = StrQ
                strFinalQ = strFinalQ.Replace("#ASS_CODE#", "asses.employeecode ")
                strFinalQ = strFinalQ.Replace("#ASS_NAME#", "asses.firstname +' '+asses.surname ")
                strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid")
                strFinalQ = strFinalQ.Replace("#CND#", " AND hrassessor_master.isexternalapprover = 0 ")
            End If
            dsList = objData.ExecQuery(strFinalQ, "DataTable")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & ":" & objData.ErrorMessage)
            End If

            If mintReportModeId <> enAssessmentMode.SELF_ASSESSMENT Then
                Dim dsTemp As DataSet = (New clsAssessor).GetExternalAssessorReviewerList(objData, "List", , , , , IIf(mintReportModeId = enAssessmentMode.REVIEWER_ASSESSMENT, clsAssessor.enAssessorType.REVIEWER, clsAssessor.enAssessorType.ASSESSOR))
                For Each dRow As DataRow In dsTemp.Tables(0).Rows
                    strFinalQ = StrQ
                    If dRow.Item("companyunkid") <= 0 AndAlso dRow.Item("DBName").ToString.Trim.Length <= 0 Then
                        strFinalQ = strFinalQ.Replace("#ASS_CODE#", "'' ")
                        strFinalQ = strFinalQ.Replace("#ASS_NAME#", "ISNULL(asses.username,'') ")
                        strFinalQ = strFinalQ.Replace("#ASS_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS asses ON hrassessor_master.employeeunkid = asses.userunkid ")
                        strFinalQ = strFinalQ.Replace("#CND#", " AND hrassessor_master.isexternalapprover = 1 AND asses.companyunkid = '" & dRow.Item("companyunkid") & "' ")
                    Else
                        strFinalQ = strFinalQ.Replace("#ASS_CODE#", "asses.employeecode ")
                        strFinalQ = strFinalQ.Replace("#ASS_NAME#", "asses.firstname +' '+asses.surname ")
                        strFinalQ = strFinalQ.Replace("#ASS_JOIN#", " JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid " & _
                                                                    " JOIN #DBName#hremployee_master AS asses ON cfuser_master.employeeunkid = asses.employeeunkid")
                        strFinalQ = strFinalQ.Replace("#CND#", "AND hrassessor_master.isexternalapprover = 1 AND cfuser_master.companyunkid = '" & dRow.Item("companyunkid") & "' ")
                    End If
                    strFinalQ = strFinalQ.Replace("#DBName#", dRow.Item("DBName") & "..")

                    Dim dsTempData As DataSet = objData.ExecQuery(strFinalQ, "DataTable")

                    If objData.ErrorMessage <> "" Then
                        Throw New Exception(objData.ErrorNumber & ":" & objData.ErrorMessage)
                    End If

                    If dsList Is Nothing Then dsList = New DataSet

                    If dsList.Tables.Count <= 0 Then
                        dsList.Tables.Add(dsTempData.Tables(0).Copy)
                    Else
                        dsList.Tables(0).Merge(dsTempData.Tables(0), True)
                    End If
                Next
            End If

            Dim dsTempOrder As New DataSet
            dsTempOrder.Tables.Add(New DataView(dsList.Tables(0), "", "[Employee Name]", DataViewRowState.CurrentRows).ToTable)
            dsList = dsTempOrder

            'Shani(01-MAR-2016) -- End

            If dsList.Tables(0).Rows.Count > 0 AndAlso dsList.Tables(0) IsNot Nothing Then
                Dim intArrayColumnWidth As Integer() = Nothing
                ReDim intArrayColumnWidth(dsList.Tables(0).Columns.Count - 1)
                For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    intArrayColumnWidth(i) = 115
                Next

                Dim sExTitle As String = Language.getMessage(mstrModuleName, 1, "Period : ") & mstrPeriodName
                Dim sFilter As String = Language.getMessage(mstrModuleName, 2, "Report Mode Selected : ") & mstrReportModeName & " , "
                sFilter &= Language.getMessage(mstrModuleName, 3, "View Mode Selected : ") & mstrModeSelectedName

                'S.SANDEEP [13-JUL-2017] -- START
                'ISSUE/ENHANCEMENT : 
                'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dsList.Tables(0), intArrayColumnWidth, True, True, True, Nothing, Me._ReportName, sExTitle, sFilter, Nothing, "", True, Nothing, Nothing)
                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dsList.Tables(0), intArrayColumnWidth, True, True, True, Nothing, Me._ReportName, sExTitle, sFilter, Nothing, "", True, Nothing, Nothing)
                'S.SANDEEP [13-JUL-2017] -- END
                'S.SANDEEP [ 22 OCT 2013 ] -- START
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, no data to export."), enMsgBoxStyle.Information)
                'S.SANDEEP [ 22 OCT 2013 ] -- END
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Not_Assessed_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Public Function Export_Not_Assessed_Report() As Boolean
    '    Dim StrQ As String = String.Empty
    '    Dim dsList As New DataSet
    '    Dim blnAddInterset As Boolean = False
    '    Try
    '        Dim objData As New clsDataOperation
    '        Select Case mintReportModeId
    '            Case enAssessmentMode.SELF_ASSESSMENT
    '                StrQ = "SELECT "
    '                If mintViewIndex > 0 Then
    '                    StrQ &= " [Allocation Name] " & _
    '                            ",[Employee Code] " & _
    '                            ",[Employee Name] " & _
    '                            ",[Job Name] " & _
    '                            ",[Department Name] " & _
    '                            ",[Work Station] " & _
    '                            "FROM " & _
    '                            "( "
    '                Else
    '                    StrQ &= " [Employee Code] " & _
    '                            ",[Employee Name] " & _
    '                            ",[Job Name] " & _
    '                            ",[Department Name] " & _
    '                            ",[Work Station] " & _
    '                            "FROM " & _
    '                            "( "
    '                End If
    '                Select Case mintModeSelectedId
    '                    Case 0 'BOTH (General Assessment & BSC)
    '                        StrQ &= "SELECT " & _
    '                                      " employeecode AS [Employee Code] " & _
    '                                      ",firstname+' '+ surname AS [Employee Name] " & _
    '                                      ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                      ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                      ",ISNULL(cm.name,'') AS [Work Station] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrbsc_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND periodunkid = '" & mintPeriodId & "') "
    '                        If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
    '                            StrQ &= UserAccessLevel._AccessLevelFilterString
    '                        End If
    '                        If mstrAdvance_Filter.Trim.Length > 0 Then
    '                            StrQ &= " AND " & mstrAdvance_Filter
    '                        End If
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                        StrQ &= "INTERSECT " & _
    '                                 "SELECT " & _
    '                                       "employeecode AS [Employee Code] " & _
    '                                      ",firstname+' '+ surname AS [Employee Name] " & _
    '                                      ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                      ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                      ",ISNULL(cm.name,'') AS [Work Station] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND periodunkid = '" & mintPeriodId & "') "
    '                        If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
    '                            StrQ &= UserAccessLevel._AccessLevelFilterString
    '                        End If
    '                        If mstrAdvance_Filter.Trim.Length > 0 Then
    '                            StrQ &= " AND " & mstrAdvance_Filter
    '                        End If
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                    Case 1 'General Assessment
    '                        StrQ &= "SELECT " & _
    '                                       "employeecode AS [Employee Code] " & _
    '                                      ",firstname+' '+ surname AS [Employee Name] " & _
    '                                      ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                      ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                      ",ISNULL(cm.name,'') AS [Work Station] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND assessmodeid = 1 AND periodunkid = '" & mintPeriodId & "') "
    '                        If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
    '                            StrQ &= UserAccessLevel._AccessLevelFilterString
    '                        End If
    '                        If mstrAdvance_Filter.Trim.Length > 0 Then
    '                            StrQ &= " AND " & mstrAdvance_Filter
    '                        End If
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                    Case 2 'BSC
    '                        StrQ &= "SELECT " & _
    '                                      " employeecode AS [Employee Code] " & _
    '                                      ",firstname+' '+ surname AS [Employee Name] " & _
    '                                      ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                      ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                      ",ISNULL(cm.name,'') AS [Work Station] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrbsc_analysis_master WHERE isvoid = 0 AND assessmodeid = 1) "
    '                End Select
    '                StrQ &= ") AS Slf WHERE 1 = 1 ORDER BY [Employee Name] "
    '            Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                StrQ = "SELECT "
    '                If mintViewIndex > 0 Then
    '                    StrQ &= " [Allocation Name] " & _
    '                            ",[Employee Code] " & _
    '                            ",[Employee Name] " & _
    '                            ",[Job Name] " & _
    '                            ",[Department Name] " & _
    '                            ",[Work Station] " & _
    '                            ",[Assessor Code] " & _
    '                            ",[Assessor Name] " & _
    '                            "FROM " & _
    '                            "( "
    '                Else
    '                    StrQ &= " [Employee Code] " & _
    '                            ",[Employee Name] " & _
    '                            ",[Job Name] " & _
    '                            ",[Department Name] " & _
    '                            ",[Work Station] " & _
    '                            ",[Assessor Code] " & _
    '                            ",[Assessor Name] " & _
    '                            "FROM " & _
    '                            "( "
    '                End If
    '                Select Case mintModeSelectedId
    '                    Case 0 'BOTH (General Assessment & BSC)
    '                        StrQ &= "SELECT " & _
    '                                      " hremployee_master.employeecode AS [Employee Code] " & _
    '                                      ",hremployee_master.firstname+' '+ hremployee_master.surname AS [Employee Name] " & _
    '                                      ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                      ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                      ",ISNULL(cm.name,'') AS [Work Station] " & _
    '                                      ",asses.employeecode AS [Assessor Code] " & _
    '                                      ",asses.firstname +' '+asses.surname AS [Assessor Name] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
    '                                      "JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = 0 AND hrassessor_master.isvoid = 0 " & _
    '                                      "JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT assessedemployeeunkid FROM hrbsc_analysis_master WHERE isvoid = 0 AND assessmodeid = 2 AND periodunkid = '" & mintPeriodId & "') "
    '                        If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
    '                            StrQ &= UserAccessLevel._AccessLevelFilterString
    '                        End If
    '                        If mstrAdvance_Filter.Trim.Length > 0 Then
    '                            StrQ &= " AND " & mstrAdvance_Filter
    '                        End If
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                        StrQ &= "INTERSECT " & _
    '                                 "SELECT " & _
    '                                       "hremployee_master.employeecode AS [Employee Code] " & _
    '                                      ",hremployee_master.firstname+' '+ hremployee_master.surname AS [Employee Name] " & _
    '                                      ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                      ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                      ",ISNULL(cm.name,'') AS [Work Station] " & _
    '                                      ",asses.employeecode AS [Assessor Code] " & _
    '                                      ",asses.firstname +' '+asses.surname AS [Assessor Name] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
    '                                      "JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = 0 AND hrassessor_master.isvoid = 0 " & _
    '                                      "JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT assessedemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND assessmodeid = 2 AND periodunkid = '" & mintPeriodId & "') "
    '                        If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
    '                            StrQ &= UserAccessLevel._AccessLevelFilterString
    '                        End If
    '                        If mstrAdvance_Filter.Trim.Length > 0 Then
    '                            StrQ &= " AND " & mstrAdvance_Filter
    '                        End If
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                    Case 1 'General Assessment
    '                        StrQ &= "SELECT " & _
    '                                       "hremployee_master.employeecode AS [Employee Code] " & _
    '                                      ",hremployee_master.firstname+' '+ hremployee_master.surname AS [Employee Name] " & _
    '                                      ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                      ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                      ",ISNULL(cm.name,'') AS [Work Station] " & _
    '                                      ",asses.employeecode AS [Assessor Code] " & _
    '                                      ",asses.firstname +' '+asses.surname AS [Assessor Name] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
    '                                      "JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = 0 AND hrassessor_master.isvoid = 0 " & _
    '                                      "JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT assessedemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND assessmodeid = 2 AND periodunkid = '" & mintPeriodId & "') "
    '                        If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
    '                            StrQ &= UserAccessLevel._AccessLevelFilterString
    '                        End If
    '                        If mstrAdvance_Filter.Trim.Length > 0 Then
    '                            StrQ &= " AND " & mstrAdvance_Filter
    '                        End If
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                    Case 2 'BSC
    '                        StrQ &= "SELECT " & _
    '                                     " hremployee_master.employeecode AS [Employee Code] " & _
    '                                     ",hremployee_master.firstname+' '+ hremployee_master.surname AS [Employee Name] " & _
    '                                     ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                     ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                     ",ISNULL(cm.name,'') AS [Work Station] " & _
    '                                     ",asses.employeecode AS [Assessor Code] " & _
    '                                     ",asses.firstname +' '+asses.surname AS [Assessor Name] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
    '                                      "JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = 0 AND hrassessor_master.isvoid = 0 " & _
    '                                      "JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT assessedemployeeunkid FROM hrbsc_analysis_master WHERE isvoid = 0 AND assessmodeid = 2 AND periodunkid = '" & mintPeriodId & "') "
    '                        If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
    '                            StrQ &= UserAccessLevel._AccessLevelFilterString
    '                        End If
    '                        If mstrAdvance_Filter.Trim.Length > 0 Then
    '                            StrQ &= " AND " & mstrAdvance_Filter
    '                        End If
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                End Select
    '                StrQ &= ") AS Asr WHERE 1 = 1 ORDER BY [Employee Name] "
    '            Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                StrQ = "SELECT "
    '                If mintViewIndex > 0 Then
    '                    StrQ &= " [Allocation Name] " & _
    '                            ",[Employee Code] " & _
    '                            ",[Employee Name] " & _
    '                            ",[Job Name] " & _
    '                            ",[Department Name] " & _
    '                            ",[Work Station] " & _
    '                            ",[Reviewer Code] " & _
    '                            ",[Reviewer Name] " & _
    '                            "FROM " & _
    '                            "( "
    '                Else
    '                    StrQ &= " [Employee Code] " & _
    '                            ",[Employee Name] " & _
    '                            ",[Job Name] " & _
    '                            ",[Department Name] " & _
    '                            ",[Work Station] " & _
    '                            ",[Reviewer Code] " & _
    '                            ",[Reviewer Name] " & _
    '                            "FROM " & _
    '                            "( "
    '                End If
    '                Select Case mintModeSelectedId
    '                    Case 0 'BOTH (General Assessment & BSC)
    '                        StrQ &= "SELECT " & _
    '                                      " hremployee_master.employeecode AS [Employee Code] " & _
    '                                      ",hremployee_master.firstname+' '+ hremployee_master.surname AS [Employee Name] " & _
    '                                      ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                      ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                      ",ISNULL(cm.name,'') AS [Work Station] " & _
    '                                      ",asses.employeecode AS [Reviewer Code] " & _
    '                                      ",asses.firstname +' '+asses.surname AS [Reviewer Name] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
    '                                      "JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = 1 AND hrassessor_master.isvoid = 0 " & _
    '                                      "JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT assessedemployeeunkid FROM hrbsc_analysis_master WHERE isvoid = 0 AND assessmodeid = 3 AND periodunkid = '" & mintPeriodId & "') "
    '                        If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
    '                            StrQ &= UserAccessLevel._AccessLevelFilterString
    '                        End If
    '                        If mstrAdvance_Filter.Trim.Length > 0 Then
    '                            StrQ &= " AND " & mstrAdvance_Filter
    '                        End If
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                        StrQ &= "INTERSECT " & _
    '                                 "SELECT " & _
    '                                      " hremployee_master.employeecode AS [Employee Code] " & _
    '                                      ",hremployee_master.firstname+' '+ hremployee_master.surname AS [Employee Name] " & _
    '                                      ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                      ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                      ",ISNULL(cm.name,'') AS [Work Station] " & _
    '                                      ",asses.employeecode AS [Reviewer Code] " & _
    '                                      ",asses.firstname +' '+asses.surname AS [Reviewer Name] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
    '                                      "JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = 1 AND hrassessor_master.isvoid = 0 " & _
    '                                      "JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT assessedemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND assessmodeid = 3 AND periodunkid = '" & mintPeriodId & "') "
    '                        If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
    '                            StrQ &= UserAccessLevel._AccessLevelFilterString
    '                        End If
    '                        If mstrAdvance_Filter.Trim.Length > 0 Then
    '                            StrQ &= " AND " & mstrAdvance_Filter
    '                        End If
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                    Case 1 'General Assessment
    '                        StrQ &= "SELECT " & _
    '                                      " hremployee_master.employeecode AS [Employee Code] " & _
    '                                      ",hremployee_master.firstname+' '+ hremployee_master.surname AS [Employee Name] " & _
    '                                      ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                      ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                      ",ISNULL(cm.name,'') AS [Work Station] " & _
    '                                      ",asses.employeecode AS [Reviewer Code] " & _
    '                                      ",asses.firstname +' '+asses.surname AS [Reviewer Name] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
    '                                      "JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = 1 AND hrassessor_master.isvoid = 0 " & _
    '                                      "JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT assessedemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND assessmodeid = 3 AND periodunkid = '" & mintPeriodId & "') "
    '                        If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
    '                            StrQ &= UserAccessLevel._AccessLevelFilterString
    '                        End If
    '                        If mstrAdvance_Filter.Trim.Length > 0 Then
    '                            StrQ &= " AND " & mstrAdvance_Filter
    '                        End If
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                    Case 2 'BSC
    '                        StrQ &= "SELECT " & _
    '                                      " hremployee_master.employeecode AS [Employee Code] " & _
    '                                      ",hremployee_master.firstname+' '+ hremployee_master.surname AS [Employee Name] " & _
    '                                      ",ISNULL(jm.job_name,'') AS [Job Name] " & _
    '                                      ",ISNULL(dpt.name,'') AS [Department Name] " & _
    '                                      ",ISNULL(cm.name,'') AS [Work Station] " & _
    '                                      ",asses.employeecode AS [Reviewer Code] " & _
    '                                      ",asses.firstname +' '+asses.surname AS [Reviewer Name] "
    '                        If mintViewIndex > 0 Then
    '                            StrQ &= mstrAnalysis_Fields.Replace("GName", "[Allocation Name]")
    '                        Else
    '                            StrQ &= ", 0 AS Id, '' AS [Allocation Name] "
    '                        End If
    '                        StrQ &= "FROM hremployee_master " & _
    '                                      "JOIN hrassessor_tran ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid AND hrassessor_tran.isvoid = 0 " & _
    '                                      "JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND isreviewer = 1 AND hrassessor_master.isvoid = 0 " & _
    '                                      "JOIN hremployee_master AS asses ON hrassessor_master.employeeunkid = asses.employeeunkid " & _
    '                                      "LEFT JOIN  hrclasses_master AS cm ON hremployee_master.classunkid = cm.classesunkid " & _
    '                                      "JOIN hrjob_master AS jm ON hremployee_master.jobunkid = jm.jobunkid " & _
    '                                      "JOIN hrdepartment_master AS dpt ON hremployee_master.departmentunkid = dpt.departmentunkid "
    '                        StrQ &= mstrAnalysis_Join
    '                        StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT assessedemployeeunkid FROM hrbsc_analysis_master WHERE isvoid = 0 AND assessmodeid = 3 AND periodunkid = '" & mintPeriodId & "') "
    '                        If UserAccessLevel._AccessLevelFilterString.Trim.Length > 0 Then
    '                            StrQ &= UserAccessLevel._AccessLevelFilterString
    '                        End If
    '                        If mstrAdvance_Filter.Trim.Length > 0 Then
    '                            StrQ &= " AND " & mstrAdvance_Filter
    '                        End If
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                End Select
    '                StrQ &= ") AS Rev WHERE 1 = 1 ORDER BY [Employee Name] "
    '        End Select

    '        objData.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        objData.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

    '        dsList = objData.ExecQuery(StrQ, "DataTable")

    '        If objData.ErrorMessage <> "" Then
    '            Throw New Exception(objData.ErrorNumber & ":" & objData.ErrorMessage)
    '        End If


    '        If dsList.Tables(0).Rows.Count > 0 AndAlso dsList.Tables(0) IsNot Nothing Then
    '            Dim intArrayColumnWidth As Integer() = Nothing
    '            ReDim intArrayColumnWidth(dsList.Tables(0).Columns.Count - 1)
    '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
    '                intArrayColumnWidth(i) = 90
    '            Next

    '            Dim sExTitle As String = Language.getMessage(mstrModuleName, 1, "Period : ") & mstrPeriodName
    '            Dim sFilter As String = Language.getMessage(mstrModuleName, 2, "Report Mode Selected : ") & mstrReportModeName & " "
    '            sFilter &= Language.getMessage(mstrModuleName, 6, "View Mode Selected : ") & mstrModeSelectedName

    '            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dsList.Tables(0), intArrayColumnWidth, True, True, True, Nothing, Me._ReportName, sExTitle, sFilter, Nothing, "", True, Nothing, Nothing)

    '        End If

    '        Return True

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Export_Not_Assessed_Report; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period :")
            Language.setMessage(mstrModuleName, 2, "Report Mode Selected :")
            Language.setMessage(mstrModuleName, 3, "View Mode Selected :")
            Language.setMessage(mstrModuleName, 4, "Sorry, no data to export.")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
