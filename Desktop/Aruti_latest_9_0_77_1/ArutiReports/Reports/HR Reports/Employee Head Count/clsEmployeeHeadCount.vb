Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsEmployeeHeadCount
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsHeadCountReport"
    Private mstrReportId As String = enArutiReport.EmployeeHeadCount
    Dim objDataOperation As clsDataOperation

#Region "Constructor"
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region "Private Variables"
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""

    'S.SANDEEP [ 20 DEC 2013 ] -- START
    'Private mblnStation As Boolean = False
    'Private mblnGrade As Boolean = False
    'Private mblnUnit As Boolean = False
    'Private mblnDepartment As Boolean = False
    'Private mblnJob As Boolean = False
    'Private mblnSection As Boolean = False
    'Private mblnClass As Boolean = False
    'S.SANDEEP [ 20 DEC 2013 ] -- END



    Private mintId As Integer = 0
    Private mstrName As String = ""

    Private mstrOrderByQuery As String = ""

    'Sohail (26 Nov 2010) -- Start
    Private mintFemaleFrom As Integer = 0
    Private mintFemaleTo As Integer = 0
    Private mintMaleFrom As Integer = 0
    Private mintMaleTo As Integer = 0
    Private mintHeadFrom As Integer = 0
    Private mintHeadTo As Integer = 0
    'Sohail (26 Nov 2010) -- End


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End
    Private mblnShowChartDept As Boolean = False 'Sohail (16 Apr 2012)


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End

    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrUserAccessFilter As String = String.Empty
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 29 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsNAIncluded As Boolean = False
    'S.SANDEEP [ 29 JAN 2013 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'S.SANDEEP [ 16 NOV 2013 ] -- START

    'S.SANDEEP [ 20 DEC 2013 ] -- START
    'Private mblnNationality As Boolean = False
    'Private mblnReligion As Boolean = False
    'Private mblnEmploymentType As Boolean = False
    'S.SANDEEP [ 20 DEC 2013 ] -- END

    'S.SANDEEP [ 16 NOV 2013 ] -- END

    'S.SANDEEP [ 20 DEC 2013 ] -- START
    Private mintDisplayViewIdx As Integer = 1
    'S.SANDEEP [ 20 DEC 2013 ] -- END


    'S.SANDEEP [ 04 JAN 2014 ] -- START
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'S.SANDEEP [ 04 JAN 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End
#End Region

#Region "Properties"
    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _Report_Type_Name() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    'S.SANDEEP [ 20 DEC 2013 ] -- START
    'Public WriteOnly Property _Employee() As Boolean
    '    Set(ByVal value As Boolean)
    '        mblnStation = value
    '    End Set
    'End Property

    'Public WriteOnly Property _CostCenter() As Boolean
    '    Set(ByVal value As Boolean)
    '        mblnUnit = value
    '    End Set
    'End Property

    'Public WriteOnly Property _Department() As Boolean
    '    Set(ByVal value As Boolean)
    '        mblnDepartment = value
    '    End Set
    'End Property

    'Public WriteOnly Property _Grade() As Boolean
    '    Set(ByVal value As Boolean)
    '        mblnGrade = value
    '    End Set
    'End Property

    'Public WriteOnly Property _Class() As Boolean
    '    Set(ByVal value As Boolean)
    '        mblnClass = value
    '    End Set
    'End Property

    'Public WriteOnly Property _Job() As Boolean
    '    Set(ByVal value As Boolean)
    '        mblnJob = value
    '    End Set
    'End Property

    'Public WriteOnly Property _Section() As Boolean
    '    Set(ByVal value As Boolean)
    '        mblnSection = value
    '    End Set
    'End Property
    'S.SANDEEP [ 20 DEC 2013 ] -- END

    Public WriteOnly Property _Id() As Integer
        Set(ByVal value As Integer)
            mintId = value
        End Set
    End Property

    Public WriteOnly Property _Name() As String
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property


    'Sohail (26 Nov 2010) -- Start
    Public WriteOnly Property _FemaleFrom() As Integer
        Set(ByVal value As Integer)
            mintFemaleFrom = value
        End Set
    End Property

    Public WriteOnly Property _MaleFrom() As Integer
        Set(ByVal value As Integer)
            mintMaleFrom = value
        End Set
    End Property

    Public WriteOnly Property _HeadFrom() As Integer
        Set(ByVal value As Integer)
            mintHeadFrom = value
        End Set
    End Property

    Public WriteOnly Property _FemaleTo() As Integer
        Set(ByVal value As Integer)
            mintFemaleTo = value
        End Set
    End Property

    Public WriteOnly Property _MaleTo() As Integer
        Set(ByVal value As Integer)
            mintMaleTo = value
        End Set
    End Property

    Public WriteOnly Property _HeadTo() As Integer
        Set(ByVal value As Integer)
            mintHeadTo = value
        End Set
    End Property
    'Sohail (26 Nov 2010) -- End


    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'Sohail (16 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _ShowChartDept() As Boolean
        Set(ByVal value As Boolean)
            mblnShowChartDept = value
        End Set
    End Property
    'Sohail (16 Apr 2012) -- End

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End

    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 29 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _IsNAIncluded() As Boolean
        Set(ByVal value As Boolean)
            mblnIsNAIncluded = value
        End Set
    End Property
    'S.SANDEEP [ 29 JAN 2013 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 16 NOV 2013 ] -- START

    'S.SANDEEP [ 20 DEC 2013 ] -- START
    'Public WriteOnly Property _Nationality() As Boolean
    '    Set(ByVal value As Boolean)
    '        mblnNationality = value
    '    End Set
    'End Property

    'Public WriteOnly Property _Religion() As Boolean
    '    Set(ByVal value As Boolean)
    '        mblnReligion = value
    '    End Set
    'End Property

    'Public WriteOnly Property _EmploymentType() As Boolean
    '    Set(ByVal value As Boolean)
    '        mblnEmploymentType = value
    '    End Set
    'End Property
    'S.SANDEEP [ 20 DEC 2013 ] -- END

    'S.SANDEEP [ 16 NOV 2013 ] -- END


    'S.SANDEEP [ 20 DEC 2013 ] -- START
    Public WriteOnly Property _DisplayViewIdx() As Integer
        Set(ByVal value As Integer)
            mintDisplayViewIdx = value
        End Set
    End Property
    'S.SANDEEP [ 20 DEC 2013 ] -- END


    'S.SANDEEP [ 04 JAN 2014 ] -- START
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'S.SANDEEP [ 04 JAN 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

#End Region

#Region "Public Function & Procedures"

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            'S.SANDEEP [ 20 DEC 2013 ] -- START
            'mblnStation = False
            'mblnGrade = False
            'mblnUnit = False
            'mblnDepartment = False
            'mblnJob = False
            'mblnSection = False
            'mblnClass = False
            'S.SANDEEP [ 20 DEC 2013 ] -- END
            mintFemaleFrom = 0
            mintFemaleTo = 0
            mintMaleFrom = 0
            mintMaleTo = 0
            mintHeadFrom = 0
            mintHeadTo = 0
            mstrOrderByQuery = ""
            mintId = 0
            mstrName = ""


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 29 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnIsNAIncluded = False
            'S.SANDEEP [ 29 JAN 2013 ] -- END

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'S.SANDEEP [ 16 NOV 2013 ] -- START

            'S.SANDEEP [ 20 DEC 2013 ] -- START
            'mblnNationality = False
            'mblnReligion = False
            'mblnEmploymentType = False
            'S.SANDEEP [ 20 DEC 2013 ] -- END

            'S.SANDEEP [ 16 NOV 2013 ] -- END

            'S.SANDEEP [ 20 DEC 2013 ] -- START
            mintDisplayViewIdx = 1
            'S.SANDEEP [ 20 DEC 2013 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Dim strFilter As String = ""
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            'Sandeep [ 29 NOV 2010 ] -- Start

            'S.SANDEEP [ 20 DEC 2013 ] -- START
            'If mblnStation = True And mintId > 0 Then
            '    Me._FilterQuery &= " AND StationID = @Id "
            '    'Vimal (30 Nov 2010) -- Start 
            '    'Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, " Station : ") & " " & mstrName & " "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, " Branch : ") & " " & mstrName & " "
            '    'Vimal (30 Nov 2010) -- End
            'ElseIf mblnUnit = True And mintId > 0 Then
            '    Me._FilterQuery &= " AND UnitID = @Id "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, " Unit : ") & " " & mstrName & " "
            'ElseIf mblnDepartment = True And mintId > 0 Then
            '    Me._FilterQuery &= " AND DeptID = @Id "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, " Department : ") & " " & mstrName & " "
            'ElseIf mblnJob = True And mintId > 0 Then
            '    Me._FilterQuery &= " AND JobID = @Id "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, " Job : ") & " " & mstrName & " "
            'ElseIf mblnGrade = True And mintId > 0 Then
            '    Me._FilterQuery &= " AND GradeID = @Id "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, " Grade : ") & " " & mstrName & " "
            'ElseIf mblnSection = True And mintId > 0 Then
            '    Me._FilterQuery &= " AND SectionID = @Id "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, " Section : ") & " " & mstrName & " "
            'ElseIf mblnClass = True And mintId > 0 Then
            '    Me._FilterQuery &= " AND ClassID = @Id "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, " Class : ") & " " & mstrName & " "
            '    'S.SANDEEP [ 16 NOV 2013 ] -- START
            'ElseIf mblnNationality = True And mintId > 0 Then
            '    Me._FilterQuery &= " AND NatId = @Id "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 40, " Nationality : ") & " " & mstrName & " "
            'ElseIf mblnReligion = True And mintId > 0 Then
            '    Me._FilterQuery &= " AND RelgId = @Id "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 41, " Religion : ") & " " & mstrName & " "
            'ElseIf mblnEmploymentType = True And mintId > 0 Then
            '    Me._FilterQuery &= " AND EmplTypId = @Id "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 42, " Employment Type : ") & " " & mstrName & " "
            '    'S.SANDEEP [ 16 NOV 2013 ] -- END
            'End If
            If mintId > 0 Then
                Me._FilterQuery &= " AND DispId = @Id "
                Me._FilterTitle &= mstrReportTypeName & " : " & mstrName & " "
            End If
            'S.SANDEEP [ 20 DEC 2013 ] -- END

            objDataOperation.AddParameter("@Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintId)
            objDataOperation.AddParameter("@NA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 35, "N/A"))


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If mintFemaleFrom <> 0 OrElse mintFemaleTo <> 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 66, "Total Female ")
            End If
            'Shani(24-Aug-2015) -- End

            If mintFemaleFrom <> 0 Then
                Me._FilterQuery &= " AND Female >= @FemaleFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "From : ") & " " & mintFemaleFrom & " "
                objDataOperation.AddParameter("@FemaleFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintFemaleFrom))
            End If
            If mintFemaleTo <> 0 Then
                Me._FilterQuery &= " AND Female <= @FemaleTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "To :") & " " & mintFemaleTo & " "
                objDataOperation.AddParameter("@FemaleTo", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintFemaleTo))
            End If


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If mintMaleFrom <> 0 OrElse mintMaleTo <> 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 64, "Total Male ")
            End If
            'Shani(24-Aug-2015) -- End

            If mintMaleFrom <> 0 Then
                Me._FilterQuery &= " AND Male >= @MaleFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "From : ") & " " & mintMaleFrom & " "
                objDataOperation.AddParameter("@MaleFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintMaleFrom))
            End If
            If mintMaleTo <> 0 Then
                Me._FilterQuery &= " AND Male <= @MaleTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "To :") & " " & mintMaleTo & " "
                objDataOperation.AddParameter("@MaleTo", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintMaleTo))
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If mintHeadFrom <> 0 OrElse mintHeadTo <> 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 65, "Total Head ")
            End If
            'Shani(24-Aug-2015) -- End

            If mintHeadFrom <> 0 Then
                strFilter &= " AND (ISNULL(Male,0)+ISNULL(Female,0)) >= @HeadFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "From : ") & " " & mintHeadFrom & " "
                objDataOperation.AddParameter("@HeadFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintHeadFrom))
            End If

            If mintHeadTo <> 0 Then
                strFilter &= " AND (ISNULL(Male,0)+ISNULL(Female,0)) <= @HeadTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "To :") & " " & mintHeadTo & " "
                objDataOperation.AddParameter("@HeadTo", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintHeadTo))
            End If

            ''Sohail (26 Nov 2010) -- Start
            'If mintFemaleFrom <> 0 Then
            '    strFilter &= " AND SUM(Female) >= @FemaleFrom "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, " Total Female  >= ") & " " & mintFemaleFrom & " "
            '    objDataOperation.AddParameter("@FemaleFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintFemaleFrom))
            'End If
            'If mintFemaleTo <> 0 Then
            '    strFilter &= " AND SUM(Female) <= @FemaleTo "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, " Total Female  <= ") & " " & mintFemaleTo & " "
            '    objDataOperation.AddParameter("@FemaleTo", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintFemaleTo))
            'End If
            'If mintMaleFrom <> 0 Then
            '    strFilter &= " AND SUM(Male) >= @MaleFrom "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, " Total Male  >= ") & " " & mintMaleFrom & " "
            '    objDataOperation.AddParameter("@MaleFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintMaleFrom))
            'End If
            'If mintMaleTo <> 0 Then
            '    strFilter &= " AND SUM(Male) <= @MaleTo "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, " Total Male  <= ") & " " & mintMaleTo & " "
            '    objDataOperation.AddParameter("@MaleTo", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintMaleTo))
            'End If
            'If mintHeadFrom <> 0 Then
            '    strFilter &= " AND SUM(Male) + SUM(Female) >= @HeadFrom "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, " Total Head  >= ") & " " & mintHeadFrom & " "
            '    objDataOperation.AddParameter("@HeadFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintHeadFrom))
            'End If
            'If mintHeadTo <> 0 Then
            '    strFilter &= " AND SUM(Male) + SUM(Female) <= @HeadTo "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, " Total Head  <= ") & " " & mintHeadTo & " "
            '    objDataOperation.AddParameter("@HeadTo", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToDecimal(mintHeadTo))
            'End If
            'If strFilter.Trim.Length > 0 Then
            '    Me._FilterQuery = " HAVING " & strFilter.Substring(4)
            'End If
            ''Sohail (26 Nov 2010) -- End

            'Sandeep [ 29 NOV 2010 ] -- End 

            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= " ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Order By ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try

        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- END


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""

        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 DEC 2013 ] -- START
    Public Function GetDisplayBy() As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 1 AS id ,@Branch AS iName UNION " & _
                   "SELECT 2 AS id ,@DeptGrp AS iName UNION " & _
                   "SELECT 3 AS id ,@Dept AS iName UNION " & _
                   "SELECT 4 AS id ,@SectGrp AS iName UNION " & _
                   "SELECT 5 AS id ,@Sect AS iName UNION " & _
                   "SELECT 6 AS id ,@UnitGrp AS iName UNION " & _
                   "SELECT 7 AS id ,@Unit AS iName UNION " & _
                   "SELECT 8 AS id ,@Team AS iName UNION " & _
                   "SELECT 9 AS id ,@JobGrp AS iName UNION " & _
                   "SELECT 10 AS id ,@Job AS iName UNION " & _
                   "SELECT 11 AS id ,@GrdGrp AS iName UNION " & _
                   "SELECT 12 AS id ,@Grd AS iName UNION " & _
                   "SELECT 13 AS id ,@Grdlvl AS iName UNION " & _
                   "SELECT 14 AS id ,@ClsGrp AS iName UNION " & _
                   "SELECT 15 AS id ,@Cls AS iName UNION " & _
                   "SELECT 16 AS id ,@CostCenter AS iName UNION " & _
                   "SELECT 17 AS id ,@Religion AS iName UNION " & _
                   "SELECT 18 AS id ,@Nationality AS iName UNION " & _
                   "SELECT 19 AS id ,@EmplType AS iName UNION " & _
                   "SELECT 20 AS id ,@MaritalStatus AS iName "

            '"SELECT 21 AS id ,@Language AS iName " 'Anjan (28 Aug 2017)

            objDataOperation.AddParameter("@Branch", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Branch"))
            objDataOperation.AddParameter("@DeptGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 49, "Department Group"))
            objDataOperation.AddParameter("@Dept", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Department"))
            objDataOperation.AddParameter("@SectGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 50, "Section Group"))
            objDataOperation.AddParameter("@Sect", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Section"))
            objDataOperation.AddParameter("@UnitGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 51, "Unit Group"))
            objDataOperation.AddParameter("@Unit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "Unit"))
            objDataOperation.AddParameter("@Team", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 52, "Team"))
            objDataOperation.AddParameter("@JobGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 53, "Job Group"))
            objDataOperation.AddParameter("@Job", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Job"))
            objDataOperation.AddParameter("@GrdGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 54, "Grade Group"))
            objDataOperation.AddParameter("@Grd", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Grade"))
            objDataOperation.AddParameter("@Grdlvl", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 55, "Grade Level"))
            objDataOperation.AddParameter("@ClsGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 56, "Class Group"))
            objDataOperation.AddParameter("@Cls", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Class"))
            objDataOperation.AddParameter("@CostCenter", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 57, "Cost Center"))
            objDataOperation.AddParameter("@Religion", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 46, "Religion"))
            objDataOperation.AddParameter("@Nationality", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 44, "Nationality"))
            objDataOperation.AddParameter("@EmplType", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 48, "Employment Type"))
            objDataOperation.AddParameter("@MaritalStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 61, "Marital Status"))
            ' objDataOperation.AddParameter("@Language", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 62, "Language")) 'Anjan (28 Aug 2017) - As this was not correct coz we dont have indentification which language to show as we have 3 languages for employees.

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDisplayBy; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [ 20 DEC 2013 ] -- END

#End Region

#Region "Report Generation"
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Public Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("TotalFemale", Language.getMessage(mstrModuleName, 27, "Total Female")))
            iColumn_DetailReport.Add(New IColumn("TotalMale", Language.getMessage(mstrModuleName, 28, "Total Male")))
            iColumn_DetailReport.Add(New IColumn("TotalHead", Language.getMessage(mstrModuleName, 29, "Total Head")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 16 NOV 2013 ] -- START

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                          ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal dtPeriodStart As Date, _
                                          ByVal dtPeriodEnd As Date, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            'StrQ = "SELECT " & _
            '       " ISNULL(Code,@NA) As Code " & _
            '       ",ISNULL(Name,@NA) As Name " & _
            '       ",ISNULL(Male,0) AS Male " & _
            '       ",ISNULL(Female,0) AS Female " & _
            '       ",ISNULL(NA,0) AS NA "
            'If mblnIsNAIncluded = True Then
            '    StrQ &= ",(ISNULL(Male,0)+ISNULL(Female,0)+ISNULL(NA,0))  AS TotalHead "
            'Else
            '    StrQ &= ",(ISNULL(Male,0)+ISNULL(Female,0))  AS TotalHead "
            'End If

            ''S.SANDEEP [ 04 JAN 2014 ] -- START
            'StrQ &= ",Id ,GName "
            ''S.SANDEEP [ 04 JAN 2014 ] -- END

            'StrQ &= "FROM " & _
            '             "( " & _
            '             "	SELECT " & _
            '             "		 SUM(CASE WHEN ISNULL(hremployee_master.gender,0) = 1 THEN 1 ELSE 0 END) AS Male " & _
            '             "		,SUM(CASE WHEN ISNULL(hremployee_master.gender,0) = 2 THEN 1 ELSE 0 END) AS Female " & _
            '             "		,SUM(CASE WHEN ISNULL(hremployee_master.gender,0) = 0 THEN 1 ELSE 0 END) AS NA "

            ''S.SANDEEP [ 20 DEC 2013 ] -- START
            ''If mblnStation = True Then
            ''    StrQ &= "  , ISNULL(hrstation_master.stationunkid, 0) AS StationID " & _
            ''            ", hrstation_master.code AS Code " & _
            ''            ", hrstation_master.name AS Name " & _
            ''                " FROM  hremployee_master " & _
            ''                "   LEFT JOIN hrstation_master ON hremployee_master.Stationunkid = hrstation_master.stationunkid " & _
            ''            "WHERE ISNULL(hrstation_master.isactive, 0) = 1 "
            ''ElseIf mblnUnit = True Then
            ''    StrQ &= "  , ISNULL(hrunit_master.unitunkid, 0) AS UnitID " & _
            ''            ", hrunit_master.code AS Code " & _
            ''            ", hrunit_master.name AS Name " & _
            ''                " FROM  hremployee_master " & _
            ''                "   LEFT JOIN hrunit_master ON hremployee_master.Unitunkid = hrunit_master.unitunkid " & _
            ''            "WHERE ISNULL(hrunit_master.isactive, 0) = 1 "
            ''ElseIf mblnDepartment = True Then
            ''    StrQ &= "   ,hrdepartment_master.departmentunkid AS DeptID " & _
            ''            ", hrdepartment_master.code AS Code " & _
            ''            ", hrdepartment_master.name AS Name " & _
            ''                 " FROM  hremployee_master " & _
            ''                 "   LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            ''                 "WHERE  ISNULL(hrdepartment_master.isactive, 0) = 1  "
            ''ElseIf mblnJob = True Then
            ''    StrQ &= "  , ISNULL(hrjob_master.jobunkid, 0) AS JobID " & _
            ''            ", hrjob_master.job_code AS Code " & _
            ''            ", hrjob_master.job_name AS Name " & _
            ''                 " FROM  hremployee_master " & _
            ''                 "   LEFT JOIN hrjob_master ON hremployee_master.Jobunkid = hrjob_master.jobunkid " & _
            ''                 "WHERE  ISNULL(hrjob_master.isactive, 0) = 1"
            ''ElseIf mblnGrade = True Then
            ''    StrQ &= "  , ISNULL(hrgrade_master.gradeunkid, 0) AS GradeID " & _
            ''            ", hrgrade_master.code AS Code " & _
            ''            ", hrgrade_master.name AS Name " & _
            ''                 " FROM  hremployee_master " & _
            ''                 "   LEFT JOIN hrgrade_master ON hremployee_master.Gradeunkid = hrgrade_master.gradeunkid " & _
            ''                 "WHERE  ISNULL(hrgrade_master.isactive, 0) = 1 "
            ''ElseIf mblnSection = True Then
            ''    StrQ &= "  , ISNULL(hrsection_master.sectionunkid, 0) AS SectionID " & _
            ''            ", hrsection_master.code AS Code " & _
            ''            ", hrsection_master.name AS Name " & _
            ''                 " FROM  hremployee_master " & _
            ''                 "   LEFT JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid " & _
            ''            "WHERE  ISNULL(hrsection_master.isactive, 0) = 1 "
            ''ElseIf mblnClass = True Then
            ''    StrQ &= ", ISNULL(hrclasses_master.classesunkid, 0) AS ClassID " & _
            ''            ", hrclasses_master.code AS Code " & _
            ''            ", hrclasses_master.name AS Name " & _
            ''            "FROM  hremployee_master " & _
            ''            "  LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
            ''            "WHERE ISNULL(hrclasses_master.isactive, 0) = 1 "
            ''ElseIf mblnNationality = True Then
            ''    StrQ &= ", ISNULL(hrmsConfiguration..cfcountry_master.countryunkid, 0) AS NatId " & _
            ''            ", hrmsConfiguration..cfcountry_master.alias AS Code " & _
            ''            ", hrmsConfiguration..cfcountry_master.country_name AS Name " & _
            ''            "FROM  hremployee_master " & _
            ''            "  LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_master.nationalityunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
            ''                 "WHERE  1 = 1 "
            ''ElseIf mblnReligion = True Then
            ''    StrQ &= ", ISNULL(cfcommon_master.masterunkid, 0) AS RelgId " & _
            ''            ", ISNULL(cfcommon_master.code,'') AS Code " & _
            ''            ", cfcommon_master.name AS Name " & _
            ''            "FROM  hremployee_master " & _
            ''            "  LEFT JOIN cfcommon_master ON hremployee_master.religionunkid = cfcommon_master.masterunkid AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.RELIGION & "' " & _
            ''            "WHERE ISNULL(cfcommon_master.isactive, 0) = 1 "
            ''ElseIf mblnEmploymentType = True Then
            ''    StrQ &= ", ISNULL(cfcommon_master.masterunkid, 0) AS EmplTypId " & _
            ''            ", ISNULL(cfcommon_master.code,'') AS Code " & _
            ''            ", cfcommon_master.name AS Name " & _
            ''            "FROM  hremployee_master " & _
            ''            "  JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "' " & _
            ''            "WHERE ISNULL(cfcommon_master.isactive, 0) = 1 "
            ''End If


            ''S.SANDEEP [ 04 JAN 2014 ] -- START
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            ''S.SANDEEP [ 04 JAN 2014 ] -- END

            'Select Case mintDisplayViewIdx

            '    Case 1  'BRANCH
            '        StrQ &= ",ISNULL(SM.stationunkid, 0) AS DispId " & _
            '                ",SM.code AS Code " & _
            '                ",SM.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                " LEFT JOIN hrstation_master AS SM ON hremployee_master.stationunkid = SM.stationunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(SM.isactive, 0) = 1 "

            '    Case 2  'DEPARTMENT GROUP
            '        StrQ &= ",ISNULL(DGM.deptgroupunkid, 0) AS DispId " & _
            '                ",DGM.code AS Code " & _
            '                ",DGM.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                " LEFT JOIN hrdepartment_group_master AS DGM ON hremployee_master.deptgroupunkid = DGM.deptgroupunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(DGM.isactive, 0) = 1 "

            '    Case 3  'DEPARTMENT
            '        StrQ &= ",DM.departmentunkid AS DispId " & _
            '                ",DM.code AS Code " & _
            '                ",DM.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                " LEFT JOIN hrdepartment_master AS DM ON hremployee_master.departmentunkid = DM.departmentunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE  ISNULL(DM.isactive, 0) = 1  "

            '    Case 4  'SECTION GROUP
            '        StrQ &= ",ISNULL(SECG.sectiongroupunkid,0) AS DispId " & _
            '                ",SECG.code AS Code " & _
            '                ",SECG.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                " LEFT JOIN hrsectiongroup_master AS SECG ON hremployee_master.sectiongroupunkid = SECG.sectiongroupunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(SECG.isactive, 0) = 1  "

            '    Case 5  'SECTION
            '        StrQ &= ",ISNULL(SECM.sectionunkid, 0) AS DispId " & _
            '                ",SECM.code AS Code " & _
            '                ",SECM.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                " LEFT JOIN hrsection_master AS SECM ON hremployee_master.sectionunkid = SECM.sectionunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE  ISNULL(SECM.isactive, 0) = 1 "

            '    Case 6  'UNIT GROUP
            '        StrQ &= ",ISNULL(UG.unitgroupunkid, 0) AS DispId " & _
            '                ",UG.code AS Code " & _
            '                ",UG.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                "  LEFT JOIN hrunitgroup_master AS UG ON hremployee_master.unitgroupunkid = UG.unitgroupunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(UG.isactive, 0) = 1 "

            '    Case 7  'UNIT
            '        StrQ &= ",ISNULL(UM.unitunkid, 0) AS DispId " & _
            '                ",UM.code AS Code " & _
            '                ",UM.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                "  LEFT JOIN hrunit_master AS UM ON hremployee_master.unitunkid = UM.unitunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(UM.isactive, 0) = 1 "

            '    Case 8  'TEAM
            '        StrQ &= ",ISNULL(TM.teamunkid, 0) AS DispId " & _
            '                ",TM.code AS Code " & _
            '                ",TM.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                "  LEFT JOIN hrteam_master AS TM ON hremployee_master.teamunkid = TM.teamunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(TM.isactive, 0) = 1 "

            '    Case 9  'JOB GROUP
            '        StrQ &= ",ISNULL(JG.jobgroupunkid, 0) AS DispId " & _
            '                ",JG.code AS Code " & _
            '                ",JG.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                "  LEFT JOIN hrjobgroup_master AS JG ON hremployee_master.jobgroupunkid = JG.jobgroupunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(JG.isactive, 0) = 1 "

            '    Case 10 'JOB
            '        StrQ &= ",ISNULL(JN.jobunkid, 0) AS DispId " & _
            '                ", JN.job_code AS Code " & _
            '                ", JN.job_name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                "   LEFT JOIN hrjob_master AS JN ON hremployee_master.Jobunkid = JN.jobunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE  ISNULL(JN.isactive, 0) = 1"

            '    Case 11 'GRADE GROUP
            '        StrQ &= ",ISNULL(GG.gradegroupunkid, 0) AS DispId " & _
            '                ",GG.code AS Code " & _
            '                ",GG.name AS Name " & _
            '                "FROM hremployee_master " & _
            '                " LEFT JOIN hrgradegroup_master AS GG ON hremployee_master.gradegroupunkid = GG.gradegroupunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(GG.isactive, 0) = 1"

            '    Case 12 'GRADE
            '        StrQ &= ",ISNULL(GM.gradeunkid, 0) AS DispId " & _
            '                ", GM.code AS Code " & _
            '                ", GM.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                "   LEFT JOIN hrgrade_master AS GM ON hremployee_master.Gradeunkid = GM.gradeunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE  ISNULL(GM.isactive, 0) = 1 "

            '    Case 13 'GRADE LEVEL
            '        StrQ &= ",ISNULL(GL.gradelevelunkid, 0) AS DispId " & _
            '                ",GL.code AS Code " & _
            '                ",GL.name AS Name " & _
            '                "FROM hremployee_master " & _
            '                " LEFT JOIN hrgradelevel_master AS GL ON hremployee_master.gradelevelunkid = GL.gradelevelunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(GL.isactive, 0) = 1 "

            '    Case 14 'CLASS GROUP
            '        StrQ &= ",ISNULL(CGM.classgroupunkid, 0) AS DispId " & _
            '                ",CGM.code AS Code " & _
            '                ",CGM.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                " LEFT JOIN hrclassgroup_master AS CGM ON hremployee_master.classgroupunkid = CGM.classgroupunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(CGM.isactive, 0) = 1 "

            '    Case 15 'CLASS
            '        StrQ &= ",ISNULL(CM.classesunkid, 0) AS DispId " & _
            '                ", CM.code AS Code " & _
            '                ", CM.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                "  LEFT JOIN hrclasses_master AS CM ON hremployee_master.classunkid = CM.classesunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(CM.isactive, 0) = 1 "

            '    Case 16 'COSTCENTER
            '        StrQ &= ",ISNULL(CCM.costcenterunkid, 0) AS DispId " & _
            '                ",CCM.costcentercode AS Code " & _
            '                ",CCM.costcentername AS Name " & _
            '                "FROM hremployee_master " & _
            '                " LEFT JOIN prcostcenter_master AS CCM ON hremployee_master.costcenterunkid = CCM.costcenterunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(CCM.isactive, 0) = 1 "

            '    Case 17 'RELEGION
            '        StrQ &= ",ISNULL(RELG.masterunkid, 0) AS DispId " & _
            '                ",ISNULL(RELG.code,'') AS Code " & _
            '                ",RELG.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                " LEFT JOIN cfcommon_master AS RELG ON hremployee_master.religionunkid = RELG.masterunkid AND RELG.mastertype = '" & clsCommon_Master.enCommonMaster.RELIGION & "' "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(RELG.isactive, 0) = 1 "

            '    Case 18 'NATIONALITY
            '        StrQ &= ",ISNULL(NAT.countryunkid, 0) AS DispId " & _
            '                ",NAT.alias AS Code " & _
            '                ",NAT.country_name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                "  LEFT JOIN hrmsConfiguration..cfcountry_master AS NAT ON hremployee_master.nationalityunkid = NAT.countryunkid "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE  1 = 1 "

            '    Case 19 'EMPLOYMENT TYPE
            '        StrQ &= ",ISNULL(EMPL.masterunkid, 0) AS DispId " & _
            '                ", ISNULL(EMPL.code,'') AS Code " & _
            '                ", EMPL.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                " JOIN cfcommon_master AS EMPL ON hremployee_master.employmenttypeunkid = EMPL.masterunkid AND EMPL.mastertype = '" & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "' "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(EMPL.isactive, 0) = 1 "

            '    Case 20 'MARITAL STATUS
            '        StrQ &= ",ISNULL(MS.masterunkid, 0) AS DispId " & _
            '                ", ISNULL(MS.code,'') AS Code " & _
            '                ", MS.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                " JOIN cfcommon_master AS MS ON hremployee_master.employmenttypeunkid = MS.masterunkid AND MS.mastertype = '" & clsCommon_Master.enCommonMaster.MARRIED_STATUS & "' "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(MS.isactive, 0) = 1 "

            '    Case 21 'LANGUAGES
            '        StrQ &= ",ISNULL(LANG.masterunkid, 0) AS DispId " & _
            '                ",ISNULL(LANG.code,'') AS Code " & _
            '                ",LANG.name AS Name " & _
            '                "FROM  hremployee_master " & _
            '                " JOIN cfcommon_master AS LANG ON hremployee_master.employmenttypeunkid = LANG.masterunkid AND LANG.mastertype = '" & clsCommon_Master.enCommonMaster.LANGUAGES & "' "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- START
            '        StrQ &= mstrAnalysis_Join & " "
            '        'S.SANDEEP [ 04 JAN 2014 ] -- END
            '        StrQ &= "WHERE ISNULL(LANG.isactive, 0) = 1 "

            'End Select
            ''S.SANDEEP [ 20 DEC 2013 ] -- END

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            ''S.SANDEEP [ 20 DEC 2013 ] -- START
            ''If mblnStation = True Then
            ''    StrQ &= "GROUP BY ISNULL(hrstation_master.stationunkid, 0)  " & _
            ''            ",hrstation_master.code " & _
            ''            ",hrstation_master.name "
            ''ElseIf mblnUnit = True Then
            ''    StrQ &= "GROUP BY ISNULL(hrunit_master.unitunkid, 0)  " & _
            ''            ",hrunit_master.code " & _
            ''            ",hrunit_master.name "
            ''ElseIf mblnDepartment = True Then
            ''    StrQ &= "GROUP BY hrdepartment_master.departmentunkid  " & _
            ''            ",hrdepartment_master.code " & _
            ''            ",hrdepartment_master.name "
            ''ElseIf mblnJob = True Then
            ''    StrQ &= "GROUP BY ISNULL(hrjob_master.jobunkid, 0)  " & _
            ''            ",hrjob_master.job_code " & _
            ''            ",hrjob_master.job_name "
            ''ElseIf mblnGrade = True Then
            ''    StrQ &= "GROUP BY ISNULL(hrgrade_master.gradeunkid, 0)  " & _
            ''            ",hrgrade_master.code " & _
            ''            ",hrgrade_master.name "
            ''ElseIf mblnSection = True Then
            ''    StrQ &= "GROUP BY ISNULL(hrsection_master.sectionunkid, 0)  " & _
            ''            "   ,hrsection_master.code " & _
            ''            "   ,hrsection_master.name "
            ''ElseIf mblnClass = True Then
            ''    StrQ &= "GROUP BY ISNULL(hrclasses_master.classesunkid, 0)  " & _
            ''            "   ,hrclasses_master.code " & _
            ''            "   ,hrclasses_master.name "
            ''ElseIf mblnNationality = True Then
            ''    StrQ &= "GROUP BY  ISNULL(hrmsConfiguration..cfcountry_master.countryunkid, 0) " & _
            ''            ", hrmsConfiguration..cfcountry_master.alias" & _
            ''            ", hrmsConfiguration..cfcountry_master.country_name "
            ''ElseIf mblnReligion = True Then
            ''    StrQ &= "GROUP BY ISNULL(cfcommon_master.masterunkid, 0) " & _
            ''            ", ISNULL(cfcommon_master.code,'') " & _
            ''            ", cfcommon_master.name "
            ''ElseIf mblnEmploymentType = True Then
            ''    StrQ &= "GROUP BY ISNULL(cfcommon_master.masterunkid, 0) " & _
            ''            ", ISNULL(cfcommon_master.code,'') " & _
            ''            ", cfcommon_master.name "
            ''End If
            'Select Case mintDisplayViewIdx
            '    Case 1  'BRANCH
            '        StrQ &= "GROUP BY ISNULL(SM.stationunkid, 0)  " & _
            '                ",SM.code " & _
            '                ",SM.name "
            '    Case 2  'DEPARTMENT GROUP
            '        StrQ &= "GROUP BY ISNULL(DGM.deptgroupunkid, 0) " & _
            '                ",DGM.code " & _
            '                ",DGM.name "
            '    Case 3  'DEPARTMENT
            '        StrQ &= "GROUP BY DM.departmentunkid  " & _
            '                ",DM.code " & _
            '                ",DM.name "
            '    Case 4  'SECTION GROUP
            '        StrQ &= "GROUP BY ISNULL(SECG.sectiongroupunkid,0) " & _
            '                ",SECG.code " & _
            '                ",SECG.name "
            '    Case 5  'SECTION
            '        StrQ &= "GROUP BY ISNULL(SECM.sectionunkid, 0)  " & _
            '                ",SECM.code " & _
            '                ",SECM.name "
            '    Case 6  'UNIT GROUP
            '        StrQ &= "GROUP BY ISNULL(UG.unitgroupunkid, 0) " & _
            '                ",UG.code " & _
            '                ",UG.name "
            '    Case 7  'UNIT
            '        StrQ &= "GROUP BY ISNULL(UM.unitunkid, 0)  " & _
            '                ",UM.code " & _
            '                ",UM.name "
            '    Case 8  'TEAM
            '        StrQ &= "GROUP BY ISNULL(TM.teamunkid, 0) " & _
            '                ",TM.code " & _
            '                ",TM.name "
            '    Case 9  'JOB GROUP
            '        StrQ &= "GROUP BY ISNULL(JG.jobgroupunkid, 0) " & _
            '                ",JG.code " & _
            '                ",JG.name "
            '    Case 10 'JOB
            '        StrQ &= "GROUP BY ISNULL(JN.jobunkid, 0)  " & _
            '                ",JN.job_code " & _
            '                ",JN.job_name "
            '    Case 11 'GRADE GROUP
            '        StrQ &= "GROUP BY ISNULL(GG.gradegroupunkid, 0) " & _
            '                ",GG.code " & _
            '                ",GG.name "
            '    Case 12 'GRADE
            '        StrQ &= "GROUP BY ISNULL(GM.gradeunkid, 0)  " & _
            '                ",GM.code " & _
            '                ",GM.name "
            '    Case 13 'GRADE LEVEL
            '        StrQ &= "GROUP BY ISNULL(GL.gradelevelunkid, 0) " & _
            '                ",GL.code " & _
            '                ",GL.name "
            '    Case 14 'CLASS GROUP
            '        StrQ &= "GROUP BY ISNULL(CGM.classgroupunkid, 0) " & _
            '                ",CGM.code " & _
            '                ",CGM.name "
            '    Case 15 'CLASS
            '        StrQ &= "GROUP BY ISNULL(CM.classesunkid, 0)  " & _
            '                     "   ,CM.code " & _
            '                     "   ,CM.name "
            '    Case 16 'COSTCENTER
            '        StrQ &= "GROUP BY ISNULL(CCM.costcenterunkid, 0) " & _
            '                ",CCM.costcentercode " & _
            '                ",CCM.costcentername "
            '    Case 17 'RELEGION
            '        StrQ &= "GROUP BY ISNULL(RELG.masterunkid, 0) " & _
            '                ",ISNULL(RELG.code,'') " & _
            '                ",RELG.name "
            '    Case 18 'NATIONALITY
            '        StrQ &= "GROUP BY  ISNULL(NAT.countryunkid, 0) " & _
            '                ", NAT.alias" & _
            '                ", NAT.country_name "
            '    Case 19 'EMPLOYMENT TYPE
            '        StrQ &= "GROUP BY ISNULL(EMPL.masterunkid, 0) " & _
            '                ",ISNULL(EMPL.code,'') " & _
            '                ",EMPL.name "
            '    Case 20 'MARITAL STATUS
            '        StrQ &= "GROUP BY ISNULL(MS.masterunkid, 0) " & _
            '                ", ISNULL(MS.code,'') " & _
            '                ", MS.name "
            '    Case 21 'LANGUAGES
            '        StrQ &= "GROUP BY ISNULL(LANG.masterunkid, 0) " & _
            '                ", ISNULL(LANG.code,'') " & _
            '                ", LANG.name "
            'End Select
            ''S.SANDEEP [ 20 DEC 2013 ] -- END

            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "") & " "
            'End If

            StrQ = "SELECT " & _
                   " ISNULL(Code,@NA) As Code " & _
                   ",ISNULL(Name,@NA) As Name " & _
                   ",ISNULL(Male,0) AS Male " & _
                   ",ISNULL(Female,0) AS Female " & _
                   ",ISNULL(NA,0) AS NA "

            If mblnIsNAIncluded = True Then
                StrQ &= ",(ISNULL(Male,0)+ISNULL(Female,0)+ISNULL(NA,0))  AS TotalHead "
            Else
                StrQ &= ",(ISNULL(Male,0)+ISNULL(Female,0))  AS TotalHead "
            End If

            StrQ &= ",Id ,GName "

            StrQ &= "FROM " & _
                         "( " & _
                         "	SELECT " & _
                         "		 SUM(CASE WHEN ISNULL(hremployee_master.gender,0) = 1 THEN 1 ELSE 0 END) AS Male " & _
                         "		,SUM(CASE WHEN ISNULL(hremployee_master.gender,0) = 2 THEN 1 ELSE 0 END) AS Female " & _
                         "		,SUM(CASE WHEN ISNULL(hremployee_master.gender,0) = 0 THEN 1 ELSE 0 END) AS NA "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            Select Case mintDisplayViewIdx

                Case 1  'BRANCH
                    StrQ &= ",ISNULL(SM.stationunkid, 0) AS DispId " & _
                            ",SM.code AS Code " & _
                            ",SM.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_transfer_tran.stationunkid " & _
                            "       ,hremployee_transfer_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS ST ON ST.employeeunkid = hremployee_master.employeeunkid AND ST.rno = 1 " & _
                            "LEFT JOIN hrstation_master AS SM ON ST.stationunkid = SM.stationunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(SM.isactive, 0) = 1 "

                Case 2  'DEPARTMENT GROUP
                    StrQ &= ",ISNULL(DGM.deptgroupunkid, 0) AS DispId " & _
                            ",DGM.code AS Code " & _
                            ",DGM.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_transfer_tran.deptgroupunkid " & _
                            "       ,hremployee_transfer_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS DG ON DG.employeeunkid = hremployee_master.employeeunkid AND DG.rno = 1 " & _
                            " LEFT JOIN hrdepartment_group_master AS DGM ON DG.deptgroupunkid = DGM.deptgroupunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(DGM.isactive, 0) = 1 "

                Case 3  'DEPARTMENT
                    StrQ &= ",DM.departmentunkid AS DispId " & _
                            ",DM.code AS Code " & _
                            ",DM.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_transfer_tran.departmentunkid " & _
                            "       ,hremployee_transfer_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS DT ON DT.employeeunkid = hremployee_master.employeeunkid AND DT.rno = 1 " & _
                            " LEFT JOIN hrdepartment_master AS DM ON DT.departmentunkid = DM.departmentunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE  ISNULL(DM.isactive, 0) = 1  "

                Case 4  'SECTION GROUP
                    StrQ &= ",ISNULL(SECG.sectiongroupunkid,0) AS DispId " & _
                            ",SECG.code AS Code " & _
                            ",SECG.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_transfer_tran.sectiongroupunkid " & _
                            "       ,hremployee_transfer_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS SG ON SG.employeeunkid = hremployee_master.employeeunkid AND SG.rno = 1 " & _
                            " LEFT JOIN hrsectiongroup_master AS SECG ON SG.sectiongroupunkid = SECG.sectiongroupunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(SECG.isactive, 0) = 1  "

                Case 5  'SECTION
                    StrQ &= ",ISNULL(SECM.sectionunkid, 0) AS DispId " & _
                            ",SECM.code AS Code " & _
                            ",SECM.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_transfer_tran.sectionunkid " & _
                            "       ,hremployee_transfer_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS SE ON SE.employeeunkid = hremployee_master.employeeunkid AND SE.rno = 1 " & _
                            " LEFT JOIN hrsection_master AS SECM ON SE.sectionunkid = SECM.sectionunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE  ISNULL(SECM.isactive, 0) = 1 "

                Case 6  'UNIT GROUP
                    StrQ &= ",ISNULL(UG.unitgroupunkid, 0) AS DispId " & _
                            ",UG.code AS Code " & _
                            ",UG.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_transfer_tran.unitgroupunkid " & _
                            "       ,hremployee_transfer_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS UGM ON UGM.employeeunkid = hremployee_master.employeeunkid AND UGM.rno = 1 " & _
                            "  LEFT JOIN hrunitgroup_master AS UG ON UGM.unitgroupunkid = UG.unitgroupunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(UG.isactive, 0) = 1 "

                Case 7  'UNIT
                    StrQ &= ",ISNULL(UM.unitunkid, 0) AS DispId " & _
                            ",UM.code AS Code " & _
                            ",UM.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_transfer_tran.unitunkid " & _
                            "       ,hremployee_transfer_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS UT ON UT.employeeunkid = hremployee_master.employeeunkid AND UT.rno = 1 " & _
                            "  LEFT JOIN hrunit_master AS UM ON UT.unitunkid = UM.unitunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(UM.isactive, 0) = 1 "

                Case 8  'TEAM
                    StrQ &= ",ISNULL(TM.teamunkid, 0) AS DispId " & _
                            ",TM.code AS Code " & _
                            ",TM.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_transfer_tran.teamunkid " & _
                            "       ,hremployee_transfer_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS TTM ON TTM.employeeunkid = hremployee_master.employeeunkid AND TTM.rno = 1 " & _
                            "  LEFT JOIN hrteam_master AS TM ON TTM.teamunkid = TM.teamunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(TM.isactive, 0) = 1 "

                Case 9  'JOB GROUP
                    StrQ &= ",ISNULL(JG.jobgroupunkid, 0) AS DispId " & _
                            ",JG.code AS Code " & _
                            ",JG.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_categorization_tran.jobgroupunkid " & _
                            "       ,hremployee_categorization_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_categorization_tran " & _
                            "   WHERE hremployee_categorization_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS JBG ON JBG.employeeunkid = hremployee_master.employeeunkid AND JBG.rno = 1 " & _
                            "LEFT JOIN hrjobgroup_master AS JG ON JBG.jobgroupunkid = JG.jobgroupunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(JG.isactive, 0) = 1 "

                Case 10 'JOB
                    StrQ &= ",ISNULL(JN.jobunkid, 0) AS DispId " & _
                            ", JN.job_code AS Code " & _
                            ", JN.job_name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_categorization_tran.jobunkid " & _
                            "       ,hremployee_categorization_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_categorization_tran " & _
                            "   WHERE hremployee_categorization_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS JBM ON JBM.employeeunkid = hremployee_master.employeeunkid AND JBM.rno = 1 " & _
                            "   LEFT JOIN hrjob_master AS JN ON JBM.Jobunkid = JN.jobunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE  ISNULL(JN.isactive, 0) = 1"

                Case 11 'GRADE GROUP
                    StrQ &= ",ISNULL(GG.gradegroupunkid, 0) AS DispId " & _
                            ",GG.code AS Code " & _
                            ",GG.name AS Name " & _
                            "FROM hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        prsalaryincrement_tran.gradegroupunkid " & _
                            "       ,prsalaryincrement_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC,prsalaryincrement_tran.salaryincrementtranunkid DESC) AS rno " & _
                            "   FROM prsalaryincrement_tran " & _
                            "   WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                            "       AND CONVERT(NVARCHAR(8),prsalaryincrement_tran.incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS SGG ON SGG.employeeunkid = hremployee_master.employeeunkid AND SGG.rno = 1 " & _
                            " LEFT JOIN hrgradegroup_master AS GG ON SGG.gradegroupunkid = GG.gradegroupunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(GG.isactive, 0) = 1"

                Case 12 'GRADE
                    StrQ &= ",ISNULL(GM.gradeunkid, 0) AS DispId " & _
                            ", GM.code AS Code " & _
                            ", GM.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        prsalaryincrement_tran.gradeunkid " & _
                            "       ,prsalaryincrement_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC,prsalaryincrement_tran.salaryincrementtranunkid DESC) AS rno " & _
                            "   FROM prsalaryincrement_tran " & _
                            "   WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                            "       AND CONVERT(NVARCHAR(8),prsalaryincrement_tran.incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS SGM ON SGM.employeeunkid = hremployee_master.employeeunkid AND SGM.rno = 1 " & _
                            "   LEFT JOIN hrgrade_master AS GM ON SGM.Gradeunkid = GM.gradeunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE  ISNULL(GM.isactive, 0) = 1 "

                Case 13 'GRADE LEVEL
                    StrQ &= ",ISNULL(GL.gradelevelunkid, 0) AS DispId " & _
                            ",GL.code AS Code " & _
                            ",GL.name AS Name " & _
                            "FROM hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        prsalaryincrement_tran.gradelevelunkid " & _
                            "       ,prsalaryincrement_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC,prsalaryincrement_tran.salaryincrementtranunkid DESC) AS rno " & _
                            "   FROM prsalaryincrement_tran " & _
                            "   WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                            "       AND CONVERT(NVARCHAR(8),prsalaryincrement_tran.incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS SGL ON SGL.employeeunkid = hremployee_master.employeeunkid AND SGL.rno = 1 " & _
                            " LEFT JOIN hrgradelevel_master AS GL ON SGL.gradelevelunkid = GL.gradelevelunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(GL.isactive, 0) = 1 "

                Case 14 'CLASS GROUP
                    StrQ &= ",ISNULL(CGM.classgroupunkid, 0) AS DispId " & _
                            ",CGM.code AS Code " & _
                            ",CGM.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_transfer_tran.classgroupunkid " & _
                            "       ,hremployee_transfer_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS CGR ON CGR.employeeunkid = hremployee_master.employeeunkid AND CGR.rno = 1 " & _
                            " LEFT JOIN hrclassgroup_master AS CGM ON CGR.classgroupunkid = CGM.classgroupunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(CGM.isactive, 0) = 1 "

                Case 15 'CLASS
                    StrQ &= ",ISNULL(CM.classesunkid, 0) AS DispId " & _
                            ", CM.code AS Code " & _
                            ", CM.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_transfer_tran.classunkid " & _
                            "       ,hremployee_transfer_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS CL ON CL.employeeunkid = hremployee_master.employeeunkid AND CL.rno = 1 " & _
                            "  LEFT JOIN hrclasses_master AS CM ON CL.classunkid = CM.classesunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(CM.isactive, 0) = 1 "

                Case 16 'COSTCENTER
                    StrQ &= ",ISNULL(CCM.costcenterunkid, 0) AS DispId " & _
                            ",CCM.costcentercode AS Code " & _
                            ",CCM.costcentername AS Name " & _
                            "FROM hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "(" & _
                            "   SELECT " & _
                            "        hremployee_cctranhead_tran.cctranheadvalueid " & _
                            "       ,hremployee_cctranhead_tran.employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_cctranhead_tran.employeeunkid ORDER BY hremployee_cctranhead_tran.effectivedate DESC) AS rno " & _
                            "   FROM hremployee_cctranhead_tran " & _
                            "   WHERE hremployee_cctranhead_tran.isvoid = 0 AND hremployee_cctranhead_tran.istransactionhead = 0 " & _
                            "       AND CONVERT(NVARCHAR(8),hremployee_cctranhead_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS CCT ON CCT.employeeunkid = hremployee_master.employeeunkid AND CCT.rno = 1 " & _
                            " LEFT JOIN prcostcenter_master AS CCM ON CCT.cctranheadvalueid = CCM.costcenterunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(CCM.isactive, 0) = 1 "

                Case 17 'RELEGION
                    StrQ &= ",ISNULL(RELG.masterunkid, 0) AS DispId " & _
                            ",ISNULL(RELG.code,'') AS Code " & _
                            ",RELG.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= " LEFT JOIN cfcommon_master AS RELG ON hremployee_master.religionunkid = RELG.masterunkid AND RELG.mastertype = '" & clsCommon_Master.enCommonMaster.RELIGION & "' "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(RELG.isactive, 0) = 1 "

                Case 18 'NATIONALITY
                    StrQ &= ",ISNULL(NAT.countryunkid, 0) AS DispId " & _
                            ",NAT.alias AS Code " & _
                            ",NAT.country_name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "  LEFT JOIN hrmsConfiguration..cfcountry_master AS NAT ON hremployee_master.nationalityunkid = NAT.countryunkid "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE  1 = 1 "

                Case 19 'EMPLOYMENT TYPE
                    StrQ &= ",ISNULL(EMPL.masterunkid, 0) AS DispId " & _
                            ", ISNULL(EMPL.code,'') AS Code " & _
                            ", EMPL.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= " JOIN cfcommon_master AS EMPL ON hremployee_master.employmenttypeunkid = EMPL.masterunkid AND EMPL.mastertype = '" & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "' "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(EMPL.isactive, 0) = 1 "

                Case 20 'MARITAL STATUS
                    StrQ &= ",ISNULL(MS.masterunkid, 0) AS DispId " & _
                            ", ISNULL(MS.code,'') AS Code " & _
                            ", MS.name AS Name " & _
                            "FROM  hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= " JOIN cfcommon_master AS MS ON hremployee_master.maritalstatusunkid = MS.masterunkid AND MS.mastertype = '" & clsCommon_Master.enCommonMaster.MARRIED_STATUS & "' "
                    'Anjan (28 Aug 2017) -Start
                    '" JOIN cfcommon_master AS MS ON hremployee_master.employmenttypeunkid = MS.masterunkid AND MS.mastertype = '" & clsCommon_Master.enCommonMaster.MARRIED_STATUS & "' "
                    'Anjan (28 Aug 2017) -End
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= mstrAnalysis_Join & " "

                    StrQ &= "WHERE ISNULL(MS.isactive, 0) = 1 "

                    'Case 21 'LANGUAGES
                    'StrQ &= ",ISNULL(LANG.masterunkid, 0) AS DispId " & _
                    '        ",ISNULL(LANG.code,'') AS Code " & _
                    '        ",LANG.name AS Name " & _
                    '        "FROM  hremployee_master " & _
                    '        " JOIN cfcommon_master AS LANG ON hremployee_master.employmenttypeunkid = LANG.masterunkid AND LANG.mastertype = '" & clsCommon_Master.enCommonMaster.LANGUAGES & "' "



                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    'StrQ &= mstrAnalysis_Join & " "

                    'StrQ &= "WHERE ISNULL(LANG.isactive, 0) = 1 "

            End Select

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            Select Case mintDisplayViewIdx
                Case 1  'BRANCH
                    StrQ &= "GROUP BY ISNULL(SM.stationunkid, 0)  " & _
                            ",SM.code " & _
                            ",SM.name "
                Case 2  'DEPARTMENT GROUP
                    StrQ &= "GROUP BY ISNULL(DGM.deptgroupunkid, 0) " & _
                            ",DGM.code " & _
                            ",DGM.name "
                Case 3  'DEPARTMENT
                    StrQ &= "GROUP BY DM.departmentunkid  " & _
                            ",DM.code " & _
                            ",DM.name "
                Case 4  'SECTION GROUP
                    StrQ &= "GROUP BY ISNULL(SECG.sectiongroupunkid,0) " & _
                            ",SECG.code " & _
                            ",SECG.name "
                Case 5  'SECTION
                    StrQ &= "GROUP BY ISNULL(SECM.sectionunkid, 0)  " & _
                            ",SECM.code " & _
                            ",SECM.name "
                Case 6  'UNIT GROUP
                    StrQ &= "GROUP BY ISNULL(UG.unitgroupunkid, 0) " & _
                            ",UG.code " & _
                            ",UG.name "
                Case 7  'UNIT
                    StrQ &= "GROUP BY ISNULL(UM.unitunkid, 0)  " & _
                            ",UM.code " & _
                            ",UM.name "
                Case 8  'TEAM
                    StrQ &= "GROUP BY ISNULL(TM.teamunkid, 0) " & _
                            ",TM.code " & _
                            ",TM.name "
                Case 9  'JOB GROUP
                    StrQ &= "GROUP BY ISNULL(JG.jobgroupunkid, 0) " & _
                            ",JG.code " & _
                            ",JG.name "
                Case 10 'JOB
                    StrQ &= "GROUP BY ISNULL(JN.jobunkid, 0)  " & _
                            ",JN.job_code " & _
                            ",JN.job_name "
                Case 11 'GRADE GROUP
                    StrQ &= "GROUP BY ISNULL(GG.gradegroupunkid, 0) " & _
                            ",GG.code " & _
                            ",GG.name "
                Case 12 'GRADE
                    StrQ &= "GROUP BY ISNULL(GM.gradeunkid, 0)  " & _
                            ",GM.code " & _
                            ",GM.name "
                Case 13 'GRADE LEVEL
                    StrQ &= "GROUP BY ISNULL(GL.gradelevelunkid, 0) " & _
                            ",GL.code " & _
                            ",GL.name "
                Case 14 'CLASS GROUP
                    StrQ &= "GROUP BY ISNULL(CGM.classgroupunkid, 0) " & _
                            ",CGM.code " & _
                            ",CGM.name "
                Case 15 'CLASS
                    StrQ &= "GROUP BY ISNULL(CM.classesunkid, 0)  " & _
                                 "   ,CM.code " & _
                                 "   ,CM.name "
                Case 16 'COSTCENTER
                    StrQ &= "GROUP BY ISNULL(CCM.costcenterunkid, 0) " & _
                            ",CCM.costcentercode " & _
                            ",CCM.costcentername "
                Case 17 'RELEGION
                    StrQ &= "GROUP BY ISNULL(RELG.masterunkid, 0) " & _
                            ",ISNULL(RELG.code,'') " & _
                            ",RELG.name "
                Case 18 'NATIONALITY
                    StrQ &= "GROUP BY  ISNULL(NAT.countryunkid, 0) " & _
                            ", NAT.alias" & _
                            ", NAT.country_name "
                Case 19 'EMPLOYMENT TYPE
                    StrQ &= "GROUP BY ISNULL(EMPL.masterunkid, 0) " & _
                            ",ISNULL(EMPL.code,'') " & _
                            ",EMPL.name "
                Case 20 'MARITAL STATUS
                    StrQ &= "GROUP BY ISNULL(MS.masterunkid, 0) " & _
                            ", ISNULL(MS.code,'') " & _
                            ", MS.name "
                    'Anjan (28 Aug 2017) -Start
                    'Case 21 'LANGUAGES
                    '    StrQ &= "GROUP BY ISNULL(LANG.masterunkid, 0) " & _
                    '            ", ISNULL(LANG.code,'') " & _
                    '            ", LANG.name "
                    'Anjan (28 Aug 2017) -End
            End Select

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "") & " "
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= " ) AS HeadList WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("Code")
                rpt_Row.Item("Column2") = dtRow.Item("Name")

                rpt_Row.Item("Column3") = FormatNumber(dtRow.Item("Female"), Nothing)
                rpt_Row.Item("Column4") = FormatNumber(dtRow.Item("Male"), Nothing)
                rpt_Row.Item("Column5") = FormatNumber(dtRow.Item("TotalHead"), Nothing)
                rpt_Row.Item("Column6") = FormatNumber(dtRow.Item("NA"), Nothing)

                'S.SANDEEP [ 04 JAN 2014 ] -- START
                rpt_Row.Item("Column7") = dtRow.Item("GName")
                'S.SANDEEP [ 04 JAN 2014 ] -- END

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeHeadCount

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 39, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 36, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 37, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 38, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If mblnShowChartDept = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            'S.SANDEEP [ 20 DEC 2013 ] -- START
            'If mblnClass = True Then
            '    Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 13, "Class Code"))
            '    Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 14, "Class"))
            'ElseIf mblnDepartment = True Then
            '    Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 15, "Dept Code"))
            '    Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 16, "Department"))
            'ElseIf mblnGrade = True Then
            '    Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 17, "Grade Code"))
            '    Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 18, "Grade"))
            'ElseIf mblnJob = True Then
            '    Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 19, "Job Code"))
            '    Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 20, "Job"))
            'ElseIf mblnSection = True Then
            '    Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 21, "Section Code"))
            '    Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 22, "Section"))
            'ElseIf mblnStation = True Then
            '    Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 23, "Branch Code"))
            '    Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 24, "Branch"))
            'ElseIf mblnUnit = True Then
            '    Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 25, "Unit Code"))
            '    Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 26, "Unit"))
            '    'S.SANDEEP [ 16 NOV 2013 ] -- START
            'ElseIf mblnNationality = True Then
            '    Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 43, "Nationality Code"))
            '    Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 44, "Nationality"))
            'ElseIf mblnReligion = True Then
            '    Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 45, "Religion Code"))
            '    Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 46, "Religion"))
            'ElseIf mblnEmploymentType = True Then
            '    Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 47, "Employment Type Code"))
            '    Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 48, "Employment Type"))
            '    'S.SANDEEP [ 16 NOV 2013 ] -- END
            'End If
            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 58, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 59, "Name"))
            'S.SANDEEP [ 20 DEC 2013 ] -- END


            Call ReportFunction.TextChange(objRpt, "txtTotalFemale", Language.getMessage(mstrModuleName, 27, "Total Female"))
            Call ReportFunction.TextChange(objRpt, "txtTotalMale", Language.getMessage(mstrModuleName, 28, "Total Male"))
            Call ReportFunction.TextChange(objRpt, "txtTotalHead", Language.getMessage(mstrModuleName, 29, "Total Head"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 30, "Grand Total :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 31, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 32, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 33, "Page :"))
            Call ReportFunction.TextChange(objRpt, "txtTotatNA", Language.getMessage(mstrModuleName, 60, "Total N/A"))

            'S.SANDEEP [ 04 JAN 2014 ] -- START
            Call ReportFunction.TextChange(objRpt, "txtGrpTotal", Language.getMessage(mstrModuleName, 63, "Group Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrpTitle", mstrReport_GroupName)
            'S.SANDEEP [ 04 JAN 2014 ] -- END

            If mblnIsNAIncluded = False Then
                Call ReportFunction.EnableSuppress(objRpt, "txtTotatNA", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column61", True)
                Call ReportFunction.EnableSuppress(objRpt, "totCol61", True)
                'S.SANDEEP [ 04 JAN 2014 ] -- START
                Call ReportFunction.EnableSuppress(objRpt, "totGrpCol61", True)
                'S.SANDEEP [ 04 JAN 2014 ] -- END
                objRpt.ReportDefinition.ReportObjects("txtTotalHead").Left = objRpt.ReportDefinition.ReportObjects("txtTotatNA").Left
                objRpt.ReportDefinition.ReportObjects("Column51").Left = objRpt.ReportDefinition.ReportObjects("txtTotatNA").Left
                objRpt.ReportDefinition.ReportObjects("totCol51").Left = objRpt.ReportDefinition.ReportObjects("txtTotatNA").Left
                'S.SANDEEP [ 04 JAN 2014 ] -- START
                objRpt.ReportDefinition.ReportObjects("totGrpCol51").Left = objRpt.ReportDefinition.ReportObjects("txtTotatNA").Left
                'S.SANDEEP [ 04 JAN 2014 ] -- END
            End If
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName & " " & Language.getMessage(mstrModuleName, 34, "wise") & " " & Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyname", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'S.SANDEEP [ 16 NOV 2013 ] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 8, "From :")
            Language.setMessage(mstrModuleName, 9, "To :")
            Language.setMessage(mstrModuleName, 10, "From :")
            Language.setMessage(mstrModuleName, 11, "From :")
            Language.setMessage(mstrModuleName, 12, "Order By")
            Language.setMessage(mstrModuleName, 14, "Class")
            Language.setMessage(mstrModuleName, 16, "Department")
            Language.setMessage(mstrModuleName, 18, "Grade")
            Language.setMessage(mstrModuleName, 20, "Job")
            Language.setMessage(mstrModuleName, 22, "Section")
            Language.setMessage(mstrModuleName, 24, "Branch")
            Language.setMessage(mstrModuleName, 26, "Unit")
            Language.setMessage(mstrModuleName, 27, "Total Female")
            Language.setMessage(mstrModuleName, 28, "Total Male")
            Language.setMessage(mstrModuleName, 29, "Total Head")
            Language.setMessage(mstrModuleName, 30, "Grand Total :")
            Language.setMessage(mstrModuleName, 31, "Printed By :")
            Language.setMessage(mstrModuleName, 32, "Printed Date :")
            Language.setMessage(mstrModuleName, 33, "Page :")
            Language.setMessage(mstrModuleName, 34, "wise")
            Language.setMessage(mstrModuleName, 35, "N/A")
            Language.setMessage(mstrModuleName, 36, "Checked By :")
            Language.setMessage(mstrModuleName, 37, "Approved By :")
            Language.setMessage(mstrModuleName, 38, "Received By :")
            Language.setMessage(mstrModuleName, 39, "Prepared By :")
            Language.setMessage(mstrModuleName, 44, "Nationality")
            Language.setMessage(mstrModuleName, 46, "Religion")
            Language.setMessage(mstrModuleName, 48, "Employment Type")
            Language.setMessage(mstrModuleName, 49, "Department Group")
            Language.setMessage(mstrModuleName, 50, "Section Group")
            Language.setMessage(mstrModuleName, 51, "Unit Group")
            Language.setMessage(mstrModuleName, 52, "Team")
            Language.setMessage(mstrModuleName, 53, "Job Group")
            Language.setMessage(mstrModuleName, 54, "Grade Group")
            Language.setMessage(mstrModuleName, 55, "Grade Level")
            Language.setMessage(mstrModuleName, 56, "Class Group")
            Language.setMessage(mstrModuleName, 57, "Cost Center")
            Language.setMessage(mstrModuleName, 58, "Code")
            Language.setMessage(mstrModuleName, 59, "Name")
            Language.setMessage(mstrModuleName, 60, "Total N/A")
            Language.setMessage(mstrModuleName, 61, "Marital Status")
            Language.setMessage(mstrModuleName, 62, "Language")
            Language.setMessage(mstrModuleName, 63, "Group Total :")
            Language.setMessage(mstrModuleName, 64, "Total Male")
            Language.setMessage(mstrModuleName, 65, "Total Head")
            Language.setMessage(mstrModuleName, 66, "Total Female")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
'    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'    Dim StrQ As String = ""
'    Dim dsList As DataSet
'    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
'    Dim exForce As Exception
'    Try

'        objDataOperation = New clsDataOperation

'        StrQ = "SELECT "

'        If mblnStation = True Then
'            StrQ &= " ISNULL(StationCode,@NA) As StationCode " & _
'                         ",ISNULL(StationName,@NA) As StationName "
'        ElseIf mblnUnit = True Then
'            StrQ &= " ISNULL(UnitCode,@NA) As UnitCode " & _
'                         ",ISNULL(UnitName,@NA) As UnitName "
'        ElseIf mblnDepartment = True Then
'            StrQ &= " ISNULL(DeptCode,@NA) As DeptCode " & _
'                         ",ISNULL(DeptName,@NA) As DeptName "
'        ElseIf mblnJob = True Then
'            StrQ &= " ISNULL(JobCode,@NA) As JobCode " & _
'                         ",ISNULL(JobName,@NA) As JobName "
'        ElseIf mblnGrade = True Then
'            StrQ &= " ISNULL(GradeCode,@NA) As GradeCode " & _
'                         ",ISNULL(GradeName,@NA) As GradeName "
'        ElseIf mblnSection = True Then
'            StrQ &= " ISNULL(SectionCode,@NA) As SectionCode " & _
'                         ",ISNULL(SectionName,@NA) As SectionName "
'        ElseIf mblnClass = True Then
'            StrQ &= " ISNULL(ClassCode,@NA) As ClassCode " & _
'                         ",ISNULL(ClassName,@NA) As ClassName "
'        End If

'        'S.SANDEEP [ 29 JAN 2013 ] -- START
'        'ENHANCEMENT : TRA CHANGES
'        'StrQ &= ",ISNULL(Male,0) AS Male " & _
'        '        ",ISNULL(Female,0) AS Female " & _
'        '        ",(ISNULL(Male,0)+ISNULL(Female,0))  AS TotalHead " & _
'        '        "FROM " & _
'        '        "( " & _
'        '        "	SELECT " & _
'        '        "		 SUM(CASE WHEN ISNULL(hremployee_master.gender,0) = 1 THEN 1 ELSE 0 END) AS Male " & _
'        '        "		,SUM(CASE WHEN ISNULL(hremployee_master.gender,0) = 2 THEN 1 ELSE 0 END) AS Female "

'        StrQ &= "    ,ISNULL(Male,0) AS Male " & _
'                     "	  ,ISNULL(Female,0) AS Female " & _
'                ",ISNULL(NA,0) AS NA "
'        If mblnIsNAIncluded = True Then
'            StrQ &= ",(ISNULL(Male,0)+ISNULL(Female,0)+ISNULL(NA,0))  AS TotalHead "
'        Else
'            StrQ &= ",(ISNULL(Male,0)+ISNULL(Female,0))  AS TotalHead "
'        End If
'        StrQ &= "FROM " & _
'                     "( " & _
'                     "	SELECT " & _
'                     "		 SUM(CASE WHEN ISNULL(hremployee_master.gender,0) = 1 THEN 1 ELSE 0 END) AS Male " & _
'                "		,SUM(CASE WHEN ISNULL(hremployee_master.gender,0) = 2 THEN 1 ELSE 0 END) AS Female " & _
'                "		,SUM(CASE WHEN ISNULL(hremployee_master.gender,0) = 0 THEN 1 ELSE 0 END) AS NA "
'        'S.SANDEEP [ 29 JAN 2013 ] -- END
'        If mblnStation = True Then
'            StrQ &= "  , ISNULL(hrstation_master.stationunkid, 0) AS StationID " & _
'                        "   , hrstation_master.code AS StationCode " & _
'                        "   , hrstation_master.name AS StationName " & _
'                        " FROM  hremployee_master " & _
'                        "   LEFT JOIN hrstation_master ON hremployee_master.Stationunkid = hrstation_master.stationunkid " & _
'                        "WHERE 1= 1 "

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            'S.SANDEEP [ 13 FEB 2013 ] -- END

'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'            If mblnIsActive = False Then
'                'S.SANDEEP [ 12 MAY 2012 ] -- START
'                'ISSUE : TRA ENHANCEMENTS
'                'StrQ &= " AND ISNULL(hremployee_master.isactive, 0) = 1 "
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'                'S.SANDEEP [ 12 MAY 2012 ] -- END
'            End If
'            'Pinkal (24-Jun-2011) -- End

'            'Sohail (28 Jun 2011) -- Start
'            'Issue : According to prvilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 12 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            'End If

'            'S.SANDEEP [ 12 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= UserAccessLevel._AccessLevelFilterString
'            'End If
'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- END


'            'S.SANDEEP [ 12 MAY 2012 ] -- END


'            'Sohail (28 Jun 2011) -- End
'            StrQ &= "GROUP BY ISNULL(hrstation_master.stationunkid, 0)  " & _
'                        "   , hrstation_master.code " & _
'                        "   , hrstation_master.name "

'        ElseIf mblnUnit = True Then
'            StrQ &= "  , ISNULL(hrunit_master.unitunkid, 0) AS UnitID " & _
'                         "  , hrunit_master.code AS UnitCode " & _
'                         "  , hrunit_master.name AS UnitName " & _
'                        " FROM  hremployee_master " & _
'                        "   LEFT JOIN hrunit_master ON hremployee_master.Unitunkid = hrunit_master.unitunkid " & _
'                        "WHERE 1 = 1 "

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            'S.SANDEEP [ 13 FEB 2013 ] -- END

'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'            If mblnIsActive = False Then
'                'S.SANDEEP [ 12 MAY 2012 ] -- START
'                'ISSUE : TRA ENHANCEMENTS
'                'StrQ &= " AND  ISNULL(hremployee_master.isactive, 0) = 1 "
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                    " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'            End If
'            'S.SANDEEP [ 12 MAY 2012 ] -- END

'            'Pinkal (24-Jun-2011) -- End

'            'Sohail (28 Jun 2011) -- Start
'            'Issue : According to prvilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 12 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            'End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= UserAccessLevel._AccessLevelFilterString
'            'End If
'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- END
'            'S.SANDEEP [ 12 MAY 2012 ] -- END

'            'Sohail (28 Jun 2011) -- End
'            StrQ &= "GROUP BY ISNULL(hrunit_master.unitunkid, 0)  " & _
'                        "   , hrunit_master.code " & _
'                        "   , hrunit_master.name "

'        ElseIf mblnDepartment = True Then
'            StrQ &= "   ,hrdepartment_master.departmentunkid AS DeptID " & _
'                         "   ,hrdepartment_master.code AS DeptCode " & _
'                         "   ,hrdepartment_master.name AS DeptName " & _
'                         " FROM  hremployee_master " & _
'                         "   LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                         "WHERE  ISNULL(hrdepartment_master.isactive, 0) = 1  "


'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            'S.SANDEEP [ 13 FEB 2013 ] -- END

'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

'            If mblnIsActive = False Then
'                'S.SANDEEP [ 12 MAY 2012 ] -- START
'                'ISSUE : TRA ENHANCEMENTS
'                'StrQ &= " AND  ISNULL(hremployee_master.isactive, 0) = 1  "
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'                'S.SANDEEP [ 12 MAY 2012 ] -- END
'            End If

'            'Pinkal (24-Jun-2011) -- End

'            'Sohail (28 Jun 2011) -- Start
'            'Issue : According to prvilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 12 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            'End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= UserAccessLevel._AccessLevelFilterString
'            'End If
'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- END
'            'S.SANDEEP [ 12 MAY 2012 ] -- END

'            'Sohail (28 Jun 2011) -- End
'            StrQ &= "GROUP BY hrdepartment_master.departmentunkid  " & _
'                         "   ,hrdepartment_master.code " & _
'                         "   ,hrdepartment_master.name "

'        ElseIf mblnJob = True Then
'            StrQ &= "  , ISNULL(hrjob_master.jobunkid, 0) AS JobID " & _
'                         "  , hrjob_master.job_code AS JobCode " & _
'                         "  , hrjob_master.job_name AS JobName " & _
'                         " FROM  hremployee_master " & _
'                         "   LEFT JOIN hrjob_master ON hremployee_master.Jobunkid = hrjob_master.jobunkid " & _
'                         "WHERE  ISNULL(hrjob_master.isactive, 0) = 1"

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            'S.SANDEEP [ 13 FEB 2013 ] -- END

'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

'            If mblnIsActive = False Then
'                'S.SANDEEP [ 12 MAY 2012 ] -- START
'                'ISSUE : TRA ENHANCEMENTS
'                'StrQ &= "  AND  ISNULL(hremployee_master.isactive, 0) = 1 "
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                    " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'            End If
'            'S.SANDEEP [ 12 MAY 2012 ] -- END

'            'Pinkal (24-Jun-2011) -- End

'            'Sohail (28 Jun 2011) -- Start
'            'Issue : According to prvilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 12 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            'End If

'            'S.SANDEEP [ 12 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= UserAccessLevel._AccessLevelFilterString
'            'End If
'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- END
'            'S.SANDEEP [ 12 MAY 2012 ] -- END

'            'Sohail (28 Jun 2011) -- End
'            StrQ &= "GROUP BY ISNULL(hrjob_master.jobunkid, 0)  " & _
'                     "   ,hrjob_master.job_code " & _
'                     "   ,hrjob_master.job_name "
'        ElseIf mblnGrade = True Then
'            StrQ &= "  , ISNULL(hrgrade_master.gradeunkid, 0) AS GradeID " & _
'                         "  , hrgrade_master.code AS GradeCode " & _
'                         "  , hrgrade_master.name AS GradeName " & _
'                         " FROM  hremployee_master " & _
'                         "   LEFT JOIN hrgrade_master ON hremployee_master.Gradeunkid = hrgrade_master.gradeunkid " & _
'                         "WHERE  ISNULL(hrgrade_master.isactive, 0) = 1 "

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            'S.SANDEEP [ 13 FEB 2013 ] -- END

'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

'            If mblnIsActive = False Then
'                'S.SANDEEP [ 12 MAY 2012 ] -- START
'                'ISSUE : TRA ENHANCEMENTS
'                'StrQ &= " AND  ISNULL(hremployee_master.isactive, 0) = 1 "
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                    " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'                'S.SANDEEP [ 12 MAY 2012 ] -- END
'            End If

'            'Pinkal (24-Jun-2011) -- End

'            'Sohail (28 Jun 2011) -- Start
'            'Issue : According to prvilege that lower level user should not see superior level employees.
'            'S.SANDEEP [ 12 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            'End If

'            'S.SANDEEP [ 12 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= UserAccessLevel._AccessLevelFilterString
'            'End If
'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- END
'            'S.SANDEEP [ 12 MAY 2012 ] -- END
'            'Sohail (28 Jun 2011) -- End
'            StrQ &= "GROUP BY ISNULL(hrgrade_master.gradeunkid, 0)  " & _
'                     "   ,hrgrade_master.code " & _
'                     "   ,hrgrade_master.name "
'        ElseIf mblnSection = True Then
'            StrQ &= "  , ISNULL(hrsection_master.sectionunkid, 0) AS SectionID " & _
'                         "  , hrsection_master.code AS SectionCode " & _
'                         "  , hrsection_master.name AS SectionName " & _
'                         " FROM  hremployee_master " & _
'                         "   LEFT JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid " & _
'                         "WHERE  1 = 1 "

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            'S.SANDEEP [ 13 FEB 2013 ] -- END

'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'            If mblnIsActive = False Then
'                'S.SANDEEP [ 12 MAY 2012 ] -- START
'                'ISSUE : TRA ENHANCEMENTS
'                'StrQ &= " AND ISNULL(hremployee_master.isactive, 0) = 1"
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'                'S.SANDEEP [ 12 MAY 2012 ] -- END
'            End If

'            'Pinkal (24-Jun-2011) -- End

'            'Sohail (28 Jun 2011) -- Start
'            'Issue : According to prvilege that lower level user should not see superior level employees.

'            'S.SANDEEP [ 12 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            'End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= UserAccessLevel._AccessLevelFilterString
'            'End If
'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- END
'            'S.SANDEEP [ 12 MAY 2012 ] -- END


'            'Sohail (28 Jun 2011) -- End
'            StrQ &= "GROUP BY ISNULL(hrsection_master.sectionunkid, 0)  " & _
'                         "   ,hrsection_master.code " & _
'                         "   ,hrsection_master.name "
'        ElseIf mblnClass = True Then
'            StrQ &= "  , ISNULL(hrclasses_master.classesunkid, 0) AS ClassID " & _
'                         "  , hrclasses_master.code AS ClassCode " & _
'                         "  , hrclasses_master.name AS ClassName " & _
'                         " FROM  hremployee_master " & _
'                         "   LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
'                         "WHERE  1= 1 "

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            'S.SANDEEP [ 13 FEB 2013 ] -- END

'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'            If mblnIsActive = False Then
'                'S.SANDEEP [ 12 MAY 2012 ] -- START
'                'ISSUE : TRA ENHANCEMENTS
'                'StrQ &= " AND ISNULL(hremployee_master.isactive, 0) = 1"
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                    " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'            End If
'            'S.SANDEEP [ 12 MAY 2012 ] -- END

'            'Pinkal (24-Jun-2011) -- End

'            'Sohail (28 Jun 2011) -- Start
'            'Issue : According to prvilege that lower level user should not see superior level employees.
'            'S.SANDEEP [ 12 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            'End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= UserAccessLevel._AccessLevelFilterString
'            'End If
'            If mstrUserAccessFilter = "" Then
'                If UserAccessLevel._AccessLevel.Length > 0 Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                End If
'            Else
'                StrQ &= mstrUserAccessFilter
'            End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- END
'            'S.SANDEEP [ 12 MAY 2012 ] -- END
'            'Sohail (28 Jun 2011) -- End
'            StrQ &= "GROUP BY ISNULL(hrclasses_master.classesunkid, 0)  " & _
'                         "   ,hrclasses_master.code " & _
'                         "   ,hrclasses_master.name "
'        End If

'        StrQ &= " ) AS HeadList WHERE 1 = 1 "

'        Call FilterTitleAndFilterQuery()

'        StrQ &= Me._FilterQuery

'        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

'        If objDataOperation.ErrorMessage <> "" Then
'            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'            Throw exForce
'        End If

'        rpt_Data = New ArutiReport.Designer.dsArutiReport

'        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
'            Dim rpt_Row As DataRow
'            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

'            If mblnDepartment = True Then
'                rpt_Row.Item("Column1") = dtRow.Item("DeptCode")
'                rpt_Row.Item("Column2") = dtRow.Item("DeptName")
'            ElseIf mblnClass = True Then
'                rpt_Row.Item("Column1") = dtRow.Item("ClassCode")
'                rpt_Row.Item("Column2") = dtRow.Item("ClassName")
'            ElseIf mblnGrade = True Then
'                rpt_Row.Item("Column1") = dtRow.Item("GradeCode")
'                rpt_Row.Item("Column2") = dtRow.Item("GradeName")
'            ElseIf mblnJob = True Then
'                rpt_Row.Item("Column1") = dtRow.Item("JobCode")
'                rpt_Row.Item("Column2") = dtRow.Item("JobName")
'            ElseIf mblnSection = True Then
'                rpt_Row.Item("Column1") = dtRow.Item("SectionCode")
'                rpt_Row.Item("Column2") = dtRow.Item("SectionName")
'            ElseIf mblnStation = True Then
'                rpt_Row.Item("Column1") = dtRow.Item("StationCode")
'                rpt_Row.Item("Column2") = dtRow.Item("StationName")
'            ElseIf mblnUnit = True Then
'                rpt_Row.Item("Column1") = dtRow.Item("UnitCode")
'                rpt_Row.Item("Column2") = dtRow.Item("UnitName")
'            End If

'            rpt_Row.Item("Column3") = FormatNumber(dtRow.Item("Female"), Nothing)
'            rpt_Row.Item("Column4") = FormatNumber(dtRow.Item("Male"), Nothing)
'            rpt_Row.Item("Column5") = FormatNumber(dtRow.Item("TotalHead"), Nothing)
'            'S.SANDEEP [ 29 JAN 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            rpt_Row.Item("Column6") = FormatNumber(dtRow.Item("NA"), Nothing)
'            'S.SANDEEP [ 29 JAN 2013 ] -- END
'            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
'        Next

'        objRpt = New ArutiReport.Designer.rptEmployeeHeadCount

'        'Sandeep [ 10 FEB 2011 ] -- Start
'        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
'        Dim arrImageRow As DataRow = Nothing
'        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

'        ReportFunction.Logo_Display(objRpt, _
'                                    ConfigParameter._Object._IsDisplayLogo, _
'                                    ConfigParameter._Object._ShowLogoRightSide, _
'                                    "arutiLogo1", _
'                                    "arutiLogo2", _
'                                    arrImageRow, _
'                                    "txtCompanyName", _
'                                    "txtReportName", _
'                                    "txtFilterDescription", _
'                                    ConfigParameter._Object._GetLeftMargin, _
'                                    ConfigParameter._Object._GetRightMargin)

'        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

'        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
'            rpt_Data.Tables("ArutiTable").Rows.Add("")
'        End If

'        If ConfigParameter._Object._IsShowPreparedBy = True Then
'            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 39, "Prepared By :"))
'            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
'        Else
'            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
'        End If

'        If ConfigParameter._Object._IsShowCheckedBy = True Then
'            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 36, "Checked By :"))
'        Else
'            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
'        End If

'        If ConfigParameter._Object._IsShowApprovedBy = True Then
'            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 37, "Approved By :"))
'        Else
'            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
'        End If

'        If ConfigParameter._Object._IsShowReceivedBy = True Then
'            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 38, "Received By :"))
'        Else
'            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
'        End If
'        'Sandeep [ 10 FEB 2011 ] -- End 

'        'Sohail (16 Apr 2012) -- Start
'        'TRA - ENHANCEMENT
'        If mblnShowChartDept = False Then
'            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
'        End If
'        'Sohail (16 Apr 2012) -- End

'        objRpt.SetDataSource(rpt_Data)

'        If mblnClass = True Then
'            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 13, "Class Code"))
'            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 14, "Class"))
'        ElseIf mblnDepartment = True Then
'            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 15, "Dept Code"))
'            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 16, "Department"))
'        ElseIf mblnGrade = True Then
'            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 17, "Grade Code"))
'            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 18, "Grade"))
'        ElseIf mblnJob = True Then
'            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 19, "Job Code"))
'            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 20, "Job"))
'        ElseIf mblnSection = True Then
'            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 21, "Section Code"))
'            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 22, "Section"))
'        ElseIf mblnStation = True Then
'            'Vimal (30 Nov 2010) -- Start 
'            'Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 23, "Station Code"))
'            'Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 24, "Station"))

'            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 23, "Branch Code"))
'            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 24, "Branch"))
'            'Vimal (30 Nov 2010) -- End
'        ElseIf mblnUnit = True Then
'            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 25, "Unit Code"))
'            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 26, "Unit"))
'        End If



'        Call ReportFunction.TextChange(objRpt, "txtTotalFemale", Language.getMessage(mstrModuleName, 27, "Total Female"))
'        Call ReportFunction.TextChange(objRpt, "txtTotalMale", Language.getMessage(mstrModuleName, 28, "Total Male"))
'        Call ReportFunction.TextChange(objRpt, "txtTotalHead", Language.getMessage(mstrModuleName, 29, "Total Head"))
'        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 30, "Grand Total :"))

'        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 31, "Printed By :"))
'        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 32, "Printed Date :"))

'        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 33, "Page :"))

'        'S.SANDEEP [ 29 JAN 2013 ] -- START
'        'ENHANCEMENT : TRA CHANGES
'        Call ReportFunction.TextChange(objRpt, "txtTotatNA", Language.getMessage(mstrModuleName, 35, "Total N/A"))
'        If mblnIsNAIncluded = False Then
'            Call ReportFunction.EnableSuppress(objRpt, "txtTotatNA", True)
'            Call ReportFunction.EnableSuppress(objRpt, "Column61", True)
'            Call ReportFunction.EnableSuppress(objRpt, "totCol61", True)
'            objRpt.ReportDefinition.ReportObjects("txtTotalHead").Left = objRpt.ReportDefinition.ReportObjects("txtTotatNA").Left
'            objRpt.ReportDefinition.ReportObjects("Column51").Left = objRpt.ReportDefinition.ReportObjects("txtTotatNA").Left
'            objRpt.ReportDefinition.ReportObjects("totCol51").Left = objRpt.ReportDefinition.ReportObjects("txtTotatNA").Left
'        End If
'        'S.SANDEEP [ 29 JAN 2013 ] -- END


'        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
'        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

'        Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName & " " & Language.getMessage(mstrModuleName, 34, "wise") & " " & Me._ReportName)
'        Call ReportFunction.TextChange(objRpt, "txtCompanyname", Me._CompanyName)
'        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

'        Return objRpt

'    Catch ex As Exception
'        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
'        Return Nothing
'    End Try
'End Function
