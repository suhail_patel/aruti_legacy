
Imports Aruti.Data
Imports eZeeCommonLib


Public Class clsTerminatedEmpWithReason
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTerminatedEmpWithReason"
    Private mstrReportId As String = enArutiReport.TerminatedEmployeeWithReason
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mstrOrderByQuery As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mdtStartdate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (16 May 2012) -- End
    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIncludeInactiveEmp As Boolean = False
    'S.SANDEEP [ 12 MAY 2012 ] -- END

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End
    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrUserAccessFilter As String = String.Empty
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End


    'Gajanan [28-AUG-2019] -- Start      
    'ISSUE/ENHANCEMENT : Suspended Employee Report
    Private menReportType As enTerminatedEmployeeReportType = enTerminatedEmployeeReportType.TerminatedEmployee
    'Gajanan [28-AUG-2019] -- End

    'S.SANDEEP |02-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
    Private mintReportFilterIdx As Integer = 0
    Private mstrReportFilterName As String = String.Empty
    Private mdtEndDateFrom As Date = Nothing
    Private mdtEndDateTo As Date = Nothing
    'S.SANDEEP |02-MAR-2020| -- END

#End Region

#Region "Properties"

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As Date
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As Date
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (16 May 2012) -- End

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 12 MAY 2012 ] -- END


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End
    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End


    'Gajanan [28-AUG-2019] -- Start      
    'ISSUE/ENHANCEMENT : Suspended Employee Report
    Public WriteOnly Property _ReportType() As enTerminatedEmployeeReportType
        Set(ByVal value As enTerminatedEmployeeReportType)
            menReportType = value
        End Set
    End Property
    'Gajanan [28-AUG-2019] -- End

    'S.SANDEEP |02-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
    Public WriteOnly Property _ReportFilterIdx() As Integer
        Set(ByVal value As Integer)
            mintReportFilterIdx = value
        End Set
    End Property

    Public WriteOnly Property _ReportFilterName() As String
        Set(ByVal value As String)
            mstrReportFilterName = value
        End Set
    End Property

    Public WriteOnly Property _EndDateFrom() As Date
        Set(ByVal value As Date)
            mdtEndDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _EndDateTo() As Date
        Set(ByVal value As Date)
            mdtEndDateTo = value
        End Set
    End Property
    'S.SANDEEP |02-MAR-2020| -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintEmployeeId = 0
            mstrEmployeeName = ""
            mdtStartdate = Nothing
            mdtEndDate = Nothing
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (16 May 2012) -- End
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnIncludeInactiveEmp = False
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End


            'Gajanan [28-AUG-2019] -- Start      
            'ISSUE/ENHANCEMENT : Suspended Employee Report
            menReportType = enTerminatedEmployeeReportType.TerminatedEmployee
            'Gajanan [28-AUG-2019] -- End

            'S.SANDEEP |02-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
            mintReportFilterIdx = 0
            mstrReportFilterName = String.Empty
            mdtEndDateFrom = Nothing
            mdtEndDateTo = Nothing
            'S.SANDEEP |02-MAR-2020| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@eddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkId = @employeeunkId "
                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            'Gajanan [28-AUG-2019] -- Start      
            'ISSUE/ENHANCEMENT : Suspended Employee Report

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''Me._FilterQuery &= " AND CONVERT(CHAR(8),hremployee_master.termination_from_date,112) BETWEEN @startdate AND @enddate "
            'Me._FilterQuery &= " AND CONVERT(CHAR(8),TE.termination_from_date,112) BETWEEN @startdate AND @enddate "
            ''S.SANDEEP [04 JUN 2015] -- END
            'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartdate))
            'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
            'Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Start Date : ") & " " & mdtStartdate.Date & " "
            'Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "End Date : ") & " " & mdtEndDate.Date & " "


            If menReportType = enTerminatedEmployeeReportType.SuspensionEmployee Then
                'S.SANDEEP |02-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
                'Me._FilterQuery &= " AND (@startdate BETWEEN CONVERT(CHAR(8), TE.from_date, 112) AND CONVERT(CHAR(8), TE.to_date, 112) " & _
                '                    "OR @enddate BETWEEN CONVERT(CHAR(8), TE.from_date, 112) AND CONVERT(CHAR(8), TE.to_date, 112)) "
                Select Case mintReportFilterIdx
                    Case 0 'DISPLAY BETWEEN SELECTED DATE RANGE
                        Me._FilterQuery &= "AND (CONVERT(CHAR(8),TE.from_date,112) >= @startdate AND CONVERT(CHAR(8),TE.to_date,112) <= @enddate " & _
                                           "OR TE.to_date IS NULL) "
                    Case 1 'DISPLAY START BETWEEN SELECTED DATE RANGE
                        Me._FilterQuery &= "AND (CONVERT(CHAR(8),TE.from_date,112) BETWEEN @startdate AND @enddate) "
                    Case 2 'DISPLAY END BETWEEN SELECTED DATE RANGE
                        Me._FilterQuery &= "AND (CONVERT(CHAR(8),ISNULL(TE.to_date,GETDATE()),112) BETWEEN @startdate AND @enddate) "
                    Case 3 'DISPLAY START & END BETWEEN SELECTED START AND END DATE RANGE
                        Me._FilterQuery &= "AND (CONVERT(CHAR(8),TE.from_date,112) BETWEEN @startdate AND @enddate AND CONVERT(CHAR(8),ISNULL(TE.to_date,GETDATE()),112) BETWEEN @sdate AND @edate) "
                        objDataOperation.AddParameter("@sdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDateFrom))
                        objDataOperation.AddParameter("@edate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDateTo))
                End Select

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Filter By : ") & " " & mstrReportFilterName & " "
                'S.SANDEEP |02-MAR-2020| -- END

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartdate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))

            ElseIf menReportType = enTerminatedEmployeeReportType.TerminatedEmployee Then
                Me._FilterQuery &= " AND CONVERT(CHAR(8),TE.termination_from_date,112) BETWEEN @startdate AND @enddate "
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartdate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
            End If
            'Gajanan [28-AUG-2019] -- End

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Start Date : ") & " " & mdtStartdate.Date & " "
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "End Date : ") & " " & mdtEndDate.Date & " "

            'S.SANDEEP |02-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
            If mintReportFilterIdx = 3 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "End Start Date : ") & " " & mdtEndDateFrom.Date & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 34, "End Date To : ") & " " & mdtEndDateTo.Date & " "
            End If
            'S.SANDEEP |02-MAR-2020| -- END

            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            'Gajanan [28-AUG-2019] -- Start      
            'ISSUE/ENHANCEMENT : Suspended Employee Report
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If menReportType = enTerminatedEmployeeReportType.SuspensionEmployee Then
                objRpt = Generate_SuspensionDetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            ElseIf menReportType = enTerminatedEmployeeReportType.TerminatedEmployee Then
                objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            End If
            'Gajanan [28-AUG-2019] -- End

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 5, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("employeename", Language.getMessage(mstrModuleName, 6, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("terminationdate", Language.getMessage(mstrModuleName, 7, "Termination Date")))
            iColumn_DetailReport.Add(New IColumn("job_name", Language.getMessage(mstrModuleName, 16, "Job Title")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


            'StrQ = "SELECT "

            ''Sohail (16 May 2012) -- Start
            ''TRA - ENHANCEMENT
            ''Select Case mintViewIndex

            ''    Case enAnalysisReport.Branch
            ''        StrQ &= "  hrstation_master.name AS GName, "

            ''    Case enAnalysisReport.Department
            ''        StrQ &= "  hrdepartment_master.name AS GName, "

            ''    Case enAnalysisReport.Section
            ''        StrQ &= "  hrsection_master.name AS GName, "

            ''    Case enAnalysisReport.Unit
            ''        StrQ &= "  hrunit_master.name AS GName, "

            ''    Case enAnalysisReport.Job
            ''        StrQ &= " hrjob_master.job_name AS GName, "

            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " prcostcenter_master.costcentername AS GName, "

            ''    Case enAnalysisReport.SectionGroup
            ''        StrQ &= " hrsectiongroup_master.name AS GName,"

            ''    Case enAnalysisReport.UnitGroup
            ''        StrQ &= " hrunitgroup_master.name AS GName, "

            ''    Case enAnalysisReport.Team
            ''        StrQ &= " hrteam_master.name AS GName, "

            ''    Case Else
            ''        StrQ &= " '' AS GName, "
            ''End Select
            ''Sohail (16 May 2012) -- End


            'StrQ &= "  ISNULL(employeeunkid, 0) as employeeunkid " & _
            '             " ,ISNULL(employeecode,'') as employeecode "

            ''SANDEEP [ 26 MAR 2014 ] -- START
            ''ENHANCEMENT : Requested by Rutta
            ''" ,ISNULL(surname,'') + ' ' + ISNULL(firstname,'') as employeename "
            'If mblnFirstNamethenSurname = False Then
            '    StrQ &= " ,ISNULL(surname,'') + ' ' + ISNULL(firstname,'') as employeename "
            'Else
            '    StrQ &= " ,ISNULL(firstname,'') + ' ' + ISNULL(surname,'') as employeename "
            'End If
            ''SANDEEP [ 26 MAR 2014 ] -- END

            'StrQ &= " ,ISNULL(hremployee_master.termination_from_date,'')  as terminationdate " & _
            '             " ,ISNULL(TR.code,'') as reasoncode  " & _
            '             " ,ISNULL(TR.name,'') as reason " & _
            '             " ,ISNULL(jm.job_code,'') as job_code " & _
            '             " ,ISNULL(jm.job_level,'') as job_Level " & _
            '             " ,ISNULL(jm.job_name,'') as job_name  "

            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= " FROM hremployee_master " & _
            '             " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
            '             " LEFT JOIN hrjob_master jm ON hremployee_master.jobunkid = jm.jobunkid "

            ''S.SANDEEP [09 APR 2015] -- START
            '' LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid  AND hraction_reason_master.isreason = 1 ------ REMOVED
            '' LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
            ''S.SANDEEP [09 APR 2015] -- END


            ''Sohail (16 May 2012) -- Start
            ''TRA - ENHANCEMENT
            'StrQ &= mstrAnalysis_Join

            ''Select Case mintViewIndex

            ''    Case enAnalysisReport.Branch
            ''        StrQ &= " JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid  "

            ''    Case enAnalysisReport.Department
            ''        StrQ &= " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid "

            ''    Case enAnalysisReport.Section
            ''        StrQ &= " JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid "

            ''    Case enAnalysisReport.Unit
            ''        StrQ &= " JOIN hrunit_master ON hremployee_master.unitunkid = hrunit_master.unitunkid "

            ''    Case enAnalysisReport.Job
            ''        StrQ &= " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid "

            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid "

            ''    Case enAnalysisReport.SectionGroup
            ''        StrQ &= " JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid "

            ''    Case enAnalysisReport.UnitGroup
            ''        StrQ &= " JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid "

            ''    Case enAnalysisReport.Team
            ''        StrQ &= " JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid "

            ''End Select
            ''Sohail (16 May 2012) -- End

            'StrQ &= " WHERE hremployee_master.termination_from_date IS NOT NULL   "


            ''S.SANDEEP [ 13 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            ''S.SANDEEP [ 13 FEB 2013 ] -- END


            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ISSUE : TRA ENHANCEMENTS

            ''Sohail (16 May 2012) -- Start
            ''TRA - ENHANCEMENT
            ''*** ISSUE : No terminated employee is coming on report if IncludeInactiveEmployee checkbox is not checked.
            ''If mblnIncludeInactiveEmp = False Then
            ''    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @eddate " & _
            ''        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@stdate) >= @stdate " & _
            ''        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@stdate) >= @stdate " & _
            ''        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @stdate) >= @stdate "
            ''End If
            ''Sohail (16 May 2012) -- End


            ''S.SANDEEP [ 12 NOV 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- END

            ''S.SANDEEP [ 12 MAY 2012 ] -- END

            ''Sohail (16 May 2012) -- Start
            ''TRA - ENHANCEMENT
            ''Select Case mintViewIndex

            ''    Case enAnalysisReport.Branch
            ''        StrQ &= " AND hrstation_master.stationunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Department
            ''        StrQ &= " AND hrdepartment_master.departmentunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Section
            ''        StrQ &= "AND hrsection_master.sectionunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Unit
            ''        StrQ &= " AND hrunit_master.unitunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Job
            ''        StrQ &= " AND hrjob_master.jobunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " AND prcostcenter_master.costcenterunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.SectionGroup
            ''        StrQ &= " AND hrsectiongroup_master.sectiongroupunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.UnitGroup
            ''        StrQ &= " AND hrunitgroup_master.unitgroupunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Team
            ''        StrQ &= " AND hrteam_master.teamunkid in (" & mstrViewByIds & ")"

            ''End Select
            ''Sohail (16 May 2012) -- End


            StrQ = "SELECT " & _
                   "  ISNULL(hremployee_master.employeeunkid, 0) as employeeunkid " & _
                   " ,ISNULL(hremployee_master.employeecode,'') as employeecode "

            If mblnFirstNamethenSurname = False Then
                StrQ &= " ,ISNULL(hremployee_master.surname,'') + ' ' + ISNULL(hremployee_master.firstname,'') as employeename "
            Else
                StrQ &= " ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') as employeename "
            End If

            StrQ &= " ,ISNULL(TE.termination_from_date,'')  as terminationdate " & _
                    " ,ISNULL(TR.code,'') as reasoncode  " & _
                    " ,ISNULL(TR.name,'') as reason " & _
                    " ,ISNULL(jm.job_code,'') as job_code " & _
                    " ,ISNULL(jm.job_level,'') as job_Level " & _
                    " ,ISNULL(jm.job_name,'') as job_name  "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            'S.SANDEEP [12 Jan 2016] -- START
            'StrQ &= " FROM hremployee_master " & _
            '        " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
            '        " LEFT JOIN " & _
            '        " ( " & _
            '        "    SELECT " & _
            '        "        jobunkid " & _
            '        "       ,employeeunkid " & _
            '        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '        "    FROM hremployee_categorization_tran " & _
            '        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '        " )AS Job ON Job.employeeunkid = hremployee_master.employeeunkid AND Job.rno = 1 " & _
            '        " JOIN hrjob_master jm ON Job.jobunkid = jm.jobunkid " & _
            '        "   JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            employeeunkid " & _
            '        "           ,date2 AS termination_from_date " & _
            '        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate) AS rno " & _
            '        "       FROM hremployee_dates_tran " & _
            '        "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' " & _
            '        "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '        "   )TE ON TE.employeeunkid = hremployee_master.employeeunkid AND TE.rno = 1 "

            StrQ &= " FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry & " "
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= " LEFT JOIN " & _
                    " ( " & _
                    "    SELECT " & _
                    "        jobunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    " )AS Job ON Job.employeeunkid = hremployee_master.employeeunkid AND Job.rno = 1 " & _
                    " JOIN hrjob_master jm ON Job.jobunkid = jm.jobunkid " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            employeeunkid " & _
                    "           ,date2 AS termination_from_date " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "           ,actionreasonunkid " & _
                    "       FROM hremployee_dates_tran " & _
                    "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' " & _
                    "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   )TE ON TE.employeeunkid = hremployee_master.employeeunkid AND TE.rno = 1 " & _
                    " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = TE.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' "

            'S.SANDEEP [16 DEC 2016] -- START
            '-> ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate) AS rno       ***** REMOVED FROM TE
            '-> ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno  ***** ADDED TO TE
            'S.SANDEEP [16 DEC 2016] -- END


            'S.SANDEEP [12 Jan 2016] -- END


            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry & " "
            'End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE 1 = 1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If


            'S.SANDEEP [04 JUN 2015] -- END


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            Dim objEmpBank As New clsEmployeeBanks
            'Pinkal (07-Mar-2020) -- End

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            Dim dsEmpBankList As DataSet = Nothing
            Dim strEmpIds As String = ""
            Dim blnFlg As Boolean = False
            If dsList.Tables(0).Rows.Count > 0 Then
                strEmpIds = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
            End If
            If strEmpIds.Trim.Length > 0 Then
                dsEmpBankList = objEmpBank.GetList(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved, True, "List", mblnIncludeAccessFilterQry, "", strEmpIds, dtPeriodEnd, "")
            End If
            If dsEmpBankList IsNot Nothing AndAlso dsEmpBankList.Tables(0).Rows.Count > 0 Then
                blnFlg = True
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column3") = dtRow.Item("employeename")
                rpt_Rows.Item("Column4") = dtRow.Item("job_name")

                If dtRow.Item("terminationdate") IsNot Nothing AndAlso dtRow.Item("terminationdate").ToString() <> "" Then
                    rpt_Rows.Item("Column7") = CDate(dtRow.Item("terminationdate")).ToShortDateString
                End If

                rpt_Rows.Item("Column8") = dtRow.Item("reason")

                Dim drReasonTotal As DataRow() = Nothing
                If dtRow.Item("GName").ToString() <> "" Then
                    Dim drSubTotal As DataRow() = dsList.Tables(0).Select("GName = '" & dtRow.Item("GName").ToString() & "'")
                    rpt_Rows.Item("Column5") = drSubTotal.Length

                    drReasonTotal = dsList.Tables(0).Select("reason = '" & dtRow.Item("reason").ToString() & "' AND GName='" & dtRow.Item("GName").ToString() & "'")
                    rpt_Rows.Item("Column9") = drReasonTotal.Length
                Else
                    drReasonTotal = dsList.Tables(0).Select("reason = '" & dtRow.Item("reason").ToString() & "'")
                    rpt_Rows.Item("Column9") = drReasonTotal.Length
                End If


                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                ''Pinkal (07-Mar-2020) -- Start
                ''Enhancement - Changes Related to Payroll UAT for NMB.
                'Dim dsEmpBankList As DataSet = objEmpBank.GetList(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved, True, "List", mblnIncludeAccessFilterQry, "", dtRow.Item("employeeunkid").ToString(), dtPeriodEnd, "")
                'If dsEmpBankList IsNot Nothing AndAlso dsEmpBankList.Tables(0).Rows.Count > 0 Then
                '    rpt_Rows.Item("Column10") = String.Join(" , ", dsEmpBankList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("accountno")).ToArray())
                'End If
                ''Pinkal (07-Mar-2020) -- End

                If blnFlg Then
                    rpt_Rows.Item("Column10") = String.Join(" , ", dsEmpBankList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dtRow.Item("employeeunkid"))).Select(Function(x) x.Field(Of String)("accountno")).ToArray())
                End If
                'Pinkal (07-Mar-2020) -- End
                'S.SANDEEP |28-OCT-2021| -- END

                

                rpt_Rows.Item("Column6") = dsList.Tables("DataTable").Rows.Count

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next


            objRpt = New ArutiReport.Designer.rptTerminatedEmpwithReason


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            Dim drRow() As DataRow = dsList.Tables(0).Select("Reason=''")

            If drRow.Length = dsList.Tables(0).Rows.Count Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 8, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 9, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 10, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 11, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
            End If

            If mintViewIndex = enAnalysisReport.Job Then
                ReportFunction.EnableSuppress(objRpt, "Column41", True)
                ReportFunction.EnableSuppress(objRpt, "txtJobTitle", True)
            End If




            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 12, "Emp No."))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 13, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtTerminateDate", Language.getMessage(mstrModuleName, 14, "Date of Leaving"))
            Call ReportFunction.TextChange(objRpt, "txtReason", Language.getMessage(mstrModuleName, 15, "Reason : "))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 16, "Job Title"))


            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            Call ReportFunction.TextChange(objRpt, "txtBankAccountNo", Language.getMessage(mstrModuleName, 26, "Bank Account No"))
            'Pinkal (07-Mar-2020) -- End


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 17, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 18, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 19, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtReasonSubTotal", Language.getMessage(mstrModuleName, 20, "Reason Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 21, "Allocation Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 22, "Grand Total :"))

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Select Case mintViewIndex

            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModulename, 23, "Branch :"))

            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModulename, 24, "Department :"))

            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModulename, 25, "Section :"))

            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModulename, 26, "Unit :"))

            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModulename, 27, "Job :"))

            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModulename, 28, "Cost Center :"))

            '    Case enAnalysisReport.SectionGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModulename, 29, "Section Group :"))

            '    Case enAnalysisReport.UnitGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModulename, 30, "Unit Group :"))

            '    Case enAnalysisReport.Team
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModulename, 31, "Team :"))

            '    Case Else
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", "")

            'End Select
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'Sohail (16 May 2012) -- End

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function



    'Gajanan [28-AUG-2019] -- Start      
    'ISSUE/ENHANCEMENT : Suspended Employee Report
    Private Function Generate_SuspensionDetailReport(ByVal strDatabaseName As String, _
                                                     ByVal intUserUnkid As Integer, _
                                                     ByVal intYearUnkid As Integer, _
                                                     ByVal intCompanyUnkid As Integer, _
                                                     ByVal dtPeriodStart As Date, _
                                                     ByVal dtPeriodEnd As Date, _
                                                     ByVal strUserModeSetting As String, _
                                                     ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            'S.SANDEEP |02-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
            Dim mdtMaxDate As Date = dtPeriodEnd
            If mdtEndDateTo <> Nothing Then
                If mdtEndDateTo > dtPeriodEnd Then mdtMaxDate = mdtEndDateTo
            End If
            'S.SANDEEP |02-MAR-2020| -- END
            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            'S.SANDEEP |02-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
            'If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtMaxDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtMaxDate, strDatabaseName)
            'S.SANDEEP |02-MAR-2020| -- END

            StrQ = "SELECT " & _
                   "  ISNULL(hremployee_master.employeeunkid, 0) as employeeunkid " & _
                   " ,ISNULL(hremployee_master.employeecode,'') as employeecode "

            If mblnFirstNamethenSurname = False Then
                StrQ &= " ,ISNULL(hremployee_master.surname,'') + ' ' + ISNULL(hremployee_master.firstname,'') as employeename "
            Else
                StrQ &= " ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') as employeename "
            End If

            StrQ &= " ,ISNULL(TE.from_date,'')  as from_date " & _
                    " ,TE.to_date as to_date " & _
                    " ,ISNULL(TE.effectivedate,'')  as effectivedate " & _
                    " ,ISNULL(TR.code,'') as reasoncode  " & _
                    " ,ISNULL(TR.name,'') as reason " & _
                    " ,ISNULL(jm.job_code,'') as job_code " & _
                    " ,ISNULL(jm.job_level,'') as job_Level " & _
                    " ,ISNULL(jm.job_name,'') as job_name  " & _
                    " ,ISNULL(TE.disciplinefileunkid,0) as disciplinefileunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            'S.SANDEEP |02-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
            'StrQ &= " FROM hremployee_master " & _
            '        " LEFT JOIN " & _
            '        " ( " & _
            '        "    SELECT " & _
            '        "        jobunkid " & _
            '        "       ,employeeunkid " & _
            '        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '        "    FROM hremployee_categorization_tran " & _
            '        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '        " )AS Job ON Job.employeeunkid = hremployee_master.employeeunkid AND Job.rno = 1 " & _
            '        " JOIN hrjob_master jm ON Job.jobunkid = jm.jobunkid " & _
            '        "   JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            employeeunkid " & _
            '        "           ,date1 AS from_date " & _
            '        "           ,date2 AS to_date " & _
            '        "           ,effectivedate " & _
            '        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '        "           ,changereasonunkid " & _
            '        "       FROM hremployee_dates_tran " & _
            '        "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' " & _
            '        "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '        "   )TE ON TE.employeeunkid = hremployee_master.employeeunkid AND TE.rno = 1 " & _
            '        " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = TE.changereasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.SUSPENSION & "' "
            StrQ &= " FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry & " "
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= " LEFT JOIN " & _
                    " ( " & _
                    "    SELECT " & _
                    "        jobunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtMaxDate) & "' " & _
                    " )AS Job ON Job.employeeunkid = hremployee_master.employeeunkid AND Job.rno = 1 " & _
                    " JOIN hrjob_master jm ON Job.jobunkid = jm.jobunkid " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            employeeunkid " & _
                    "           ,date1 AS from_date " & _
                    "           ,date2 AS to_date " & _
                    "           ,effectivedate " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "           ,changereasonunkid " & _
                    "           ,disciplinefileunkid " & _
                    "       FROM hremployee_dates_tran " & _
                    "       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' " & _
                    "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtMaxDate) & "' " & _
                    "   )TE ON TE.employeeunkid = hremployee_master.employeeunkid AND TE.rno = 1 " & _
                    " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = TE.changereasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.SUSPENSION & "' "
            'S.SANDEEP |02-MAR-2020| -- END

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry & " "
            'End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE 1 = 1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column3") = dtRow.Item("employeename")
                'rpt_Rows.Item("Column4") = dtRow.Item("job_name")

                If dtRow.Item("effectivedate") IsNot Nothing AndAlso dtRow.Item("effectivedate").ToString() <> "" Then
                    rpt_Rows.Item("Column7") = CDate(dtRow.Item("effectivedate")).ToShortDateString
                End If

                If dtRow.Item("from_date") IsNot Nothing AndAlso dtRow.Item("from_date").ToString() <> "" Then
                    rpt_Rows.Item("Column4") = CDate(dtRow.Item("from_date")).ToShortDateString
                End If

                If dtRow.Item("to_date") IsNot Nothing AndAlso dtRow.Item("to_date").ToString() <> "" Then
                    rpt_Rows.Item("Column10") = CDate(dtRow.Item("to_date")).ToShortDateString
                End If
                rpt_Rows.Item("Column12") = dtRow.Item("job_name")

                rpt_Rows.Item("Column8") = dtRow.Item("reason")

                Dim drReasonTotal As DataRow() = Nothing
                If dtRow.Item("GName").ToString() <> "" Then
                    Dim drSubTotal As DataRow() = dsList.Tables(0).Select("GName = '" & dtRow.Item("GName").ToString() & "'")
                    rpt_Rows.Item("Column5") = drSubTotal.Length

                    drReasonTotal = dsList.Tables(0).Select("reason = '" & dtRow.Item("reason").ToString() & "' AND GName='" & dtRow.Item("GName").ToString() & "'")
                    rpt_Rows.Item("Column9") = drReasonTotal.Length
                Else
                    drReasonTotal = dsList.Tables(0).Select("reason = '" & dtRow.Item("reason").ToString() & "'")
                    rpt_Rows.Item("Column9") = drReasonTotal.Length
                End If

                rpt_Rows.Item("Column6") = dsList.Tables("DataTable").Rows.Count


                'Gajanan [18-May-2020] -- Start
                'Enhancement:Discipline Module Enhancement NMB
                rpt_Rows.Item("Column13") = dtRow.Item("disciplinefileunkid")
                'Gajanan [18-May-2020] -- End



                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            objRpt = New ArutiReport.Designer.rptSuspentionEmpwithReason


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            Dim drRow() As DataRow = dsList.Tables(0).Select("Reason=''")

            If drRow.Length = dsList.Tables(0).Rows.Count Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 8, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 9, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 10, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 11, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
            End If

            If mintViewIndex = enAnalysisReport.Job Then
                ReportFunction.EnableSuppress(objRpt, "Column41", True)
                ReportFunction.EnableSuppress(objRpt, "txtJobTitle", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 12, "Emp No."))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 13, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtEffectiveDate", Language.getMessage(mstrModuleName, 23, "Effective Date"))
            Call ReportFunction.TextChange(objRpt, "txtSuspentionFromDate", Language.getMessage(mstrModuleName, 24, "From Date"))
            Call ReportFunction.TextChange(objRpt, "txtSuspentionToDate", Language.getMessage(mstrModuleName, 25, "To Date"))
            Call ReportFunction.TextChange(objRpt, "txtReason", Language.getMessage(mstrModuleName, 15, "Reason : "))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 16, "Job Title"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 17, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 18, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 19, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtReasonSubTotal", Language.getMessage(mstrModuleName, 20, "Reason Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 21, "Allocation Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 22, "Grand Total :"))


            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Gajanan [28-AUG-2019] -- End


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Start Date :")
            Language.setMessage(mstrModuleName, 3, "End Date :")
            Language.setMessage(mstrModuleName, 4, " Order By :")
            Language.setMessage(mstrModuleName, 5, "Employee Code")
            Language.setMessage(mstrModuleName, 6, "Employee Name")
            Language.setMessage(mstrModuleName, 7, "Termination Date")
            Language.setMessage(mstrModuleName, 8, "Prepared By :")
            Language.setMessage(mstrModuleName, 9, "Checked By :")
            Language.setMessage(mstrModuleName, 10, "Approved By :")
            Language.setMessage(mstrModuleName, 11, "Received By :")
            Language.setMessage(mstrModuleName, 12, "Emp No.")
            Language.setMessage(mstrModuleName, 13, "Employee Name")
            Language.setMessage(mstrModuleName, 14, "Date of Leaving")
            Language.setMessage(mstrModuleName, 15, "Reason :")
            Language.setMessage(mstrModuleName, 16, "Job Title")
            Language.setMessage(mstrModuleName, 17, "Printed By :")
            Language.setMessage(mstrModuleName, 18, "Printed Date :")
            Language.setMessage(mstrModuleName, 19, "Page :")
            Language.setMessage(mstrModuleName, 20, "Reason Sub Total :")
            Language.setMessage(mstrModuleName, 21, "Allocation Sub Total :")
            Language.setMessage(mstrModuleName, 22, "Grand Total :")
            Language.setMessage(mstrModuleName, 23, "Effective Date")
            Language.setMessage(mstrModuleName, 24, "From Date")
            Language.setMessage(mstrModuleName, 25, "To Date")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
