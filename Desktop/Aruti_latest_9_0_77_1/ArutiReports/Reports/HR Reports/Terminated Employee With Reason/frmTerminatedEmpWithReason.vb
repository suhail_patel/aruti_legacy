'************************************************************************************************************************************
'Class Name : frmTerminatedEmpWithReason.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTerminatedEmpWithReason

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTerminatedEmpWithReason"
    Private objTerminatedEmp As clsTerminatedEmpWithReason
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (16 May 2012) -- End
    
    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Constructor "

    Public Sub New()
        objTerminatedEmp = New clsTerminatedEmpWithReason(User._Object._Languageunkid,Company._Object._Companyunkid)
        objTerminatedEmp.SetDefaultValue()
        InitializeComponent()
        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        _Show_AdvanceFilter = True
        'S.SANDEEP [ 13 FEB 2013 ] -- END
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try

            'Gajanan [28-AUG-2019] -- Start      
            'ISSUE/ENHANCEMENT : Suspended Employee Report
            Dim objMasterdata As New clsMasterData
            'Gajanan [28-AUG-2019] -- End


            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            'S.SANDEEP [ 15 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objEmp.GetEmployeeList("Emp", True, False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 15 MAY 2012 ] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing
            dsList = Nothing

            'S.SANDEEP |02-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
            With cboFilterType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Display Between Selected Date Range"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Display Start Between Selected Date Range"))
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Display End Between Selected Date Range"))
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Display Start & End Between Selected Start and End Date Range"))
                .SelectedIndex = 0
            End With
            'S.SANDEEP |02-MAR-2020| -- END

            'Gajanan [28-AUG-2019] -- Start      
            'ISSUE/ENHANCEMENT : Suspended Employee Report
            dsList = objMasterdata.GetTerminateEmployeeReportType(True, "ReportType")
            With cboReportType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("ReportType")
                .SelectedValue = 0
            End With
            dsList = Nothing
            objMasterdata = Nothing
            'Gajanan [28-AUG-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            'Gajanan [28-AUG-2019] -- Start            
            'ISSUE/ENcHANCEMENT : Suspended Employee Report
            If cboReportType.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Report type is mandatory information.Please select report type."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Return False
            End If
            'Gajanan [28-AUG-2019] -- End

            objTerminatedEmp.SetDefaultValue()

            objTerminatedEmp._EmployeeID = CInt(cboEmployee.SelectedValue)
            objTerminatedEmp._EmployeeName = cboEmployee.Text

            objTerminatedEmp._StartDate = dtpStartDate.Value.Date
            objTerminatedEmp._EndDate = dtpEndDate.Value.Date


            objTerminatedEmp._ViewByIds = mstrStringIds
            objTerminatedEmp._ViewIndex = mintViewIdx
            objTerminatedEmp._ViewByName = mstrStringName
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            objTerminatedEmp._Analysis_Fields = mstrAnalysis_Fields
            objTerminatedEmp._Analysis_Join = mstrAnalysis_Join
            objTerminatedEmp._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTerminatedEmp._Report_GroupName = mstrReport_GroupName
            'Sohail (16 May 2012) -- End

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objTerminatedEmp._IncludeInactiveEmp = chkInactiveemp.Checked
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objTerminatedEmp._Advance_Filter = mstrAdvanceFilter
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            'Gajanan [28-AUG-2019] -- Start      
            'ISSUE/ENHANCEMENT : Suspended Employee Report
            objTerminatedEmp._ReportType = CType(cboReportType.SelectedValue, enTerminatedEmployeeReportType)

            If CType(cboReportType.SelectedValue, enTerminatedEmployeeReportType) = enTerminatedEmployeeReportType.TerminatedEmployee Then
                objTerminatedEmp._ReportName = Language.getMessage(mstrModuleName, 3, "Terminated Employee Report")
            Else
                objTerminatedEmp._ReportName = Language.getMessage(mstrModuleName, 2, "Suspended Employee Report")
            End If
            'Gajanan [28-AUG-2019] -- End

            'S.SANDEEP |02-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
            objTerminatedEmp._ReportFilterIdx = cboFilterType.SelectedIndex
            objTerminatedEmp._ReportFilterName = cboFilterType.Text
            If pnlEndDateFilter.Enabled Then
                objTerminatedEmp._EndDateFrom = dtpEndDateFrom.Value.Date
                objTerminatedEmp._EndDateTo = dtpEndDateTo.Value.Date
            End If
            'S.SANDEEP |02-MAR-2020| -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objTerminatedEmp.setDefaultOrderBy(0)
            txtOrderBy.Text = objTerminatedEmp.OrderByDisplay
            cboEmployee.SelectedIndex = 0
            dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpEndDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            chkInactiveemp.Visible = False
            'Sohail (16 May 2012) -- End
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            chkInactiveemp.Checked = False
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'Gajanan [28-AUG-2019] -- Start            
            'ISSUE/ENcHANCEMENT : Suspended Employee Report
            cboReportType.SelectedIndex = 0
            'Gajanan [28-AUG-2019] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmTerminatedEmpWithReason_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTerminatedEmp = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTerminatedEmpWithReason_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTerminatedEmpWithReason_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objTerminatedEmp._ReportName
            Me._Message = objTerminatedEmp._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTerminatedEmpWithReason_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTerminatedEmpWithReason_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmTerminatedEmpWithReason_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub frmTerminatedEmpWithReason_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If SetFilter() = False Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTerminatedEmp.generateReport(0, e.Type, enExportAction.None)
            objTerminatedEmp.generateReportNew(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               dtpStartDate.Value.Date, _
                                               dtpEndDate.Value.Date, _
                                               ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                               ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTerminatedEmpWithReason_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTerminatedEmpWithReason_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If SetFilter() = False Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTerminatedEmp.generateReport(0, enPrintAction.None, e.Type)
            objTerminatedEmp.generateReportNew(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               dtpStartDate.Value.Date, _
                                               dtpEndDate.Value.Date, _
                                               ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                               ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTerminatedEmpWithReason_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTerminatedEmpWithReason_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTerminatedEmpWithReason_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTerminatedEmpWithReason_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTerminatedEmpWithReason_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTerminatedEmpWithReason.SetMessages()
            objfrm._Other_ModuleNames = "clsTerminatedEmpWithReason"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region "Control"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objTerminatedEmp.setOrderBy(0)
            txtOrderBy.Text = objTerminatedEmp.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP |02-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case CInt(cboReportType.SelectedValue)
                Case enTerminatedEmployeeReportType.SuspensionEmployee
                    cboFilterType.Enabled = True
                Case Else
                    cboFilterType.SelectedIndex = 0 : pnlEndDateFilter.Enabled = False
                    cboFilterType.Enabled = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFilterType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFilterType.SelectedIndexChanged
        Try
            If cboFilterType.SelectedIndex = 3 Then
                pnlEndDateFilter.Enabled = True
            Else
                pnlEndDateFilter.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFilterType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |02-MAR-2020| -- END

#End Region

#Region "LinkLabel Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Sohail (16 May 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblfromdate.Text = Language._Object.getCaption(Me.lblfromdate.Name, Me.lblfromdate.Text)
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
            Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "Report type is mandatory information.Please select report type.")
            Language.setMessage(mstrModuleName, 2, "Suspended Employee Report")
            Language.setMessage(mstrModuleName, 3, "Terminated Employee Report")
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>


End Class
