﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserDetailsLogReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserDetailsLogReport))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.ehUserDetailLogReport = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpFromdate = New System.Windows.Forms.DateTimePicker
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnSearchCompany = New eZee.Common.eZeeGradientButton
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 418)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(673, 55)
        Me.EZeeFooter1.TabIndex = 2
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(380, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(476, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(572, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'ehUserDetailLogReport
        '
        Me.ehUserDetailLogReport.BackColor = System.Drawing.SystemColors.Control
        Me.ehUserDetailLogReport.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.ehUserDetailLogReport.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ehUserDetailLogReport.Dock = System.Windows.Forms.DockStyle.Top
        Me.ehUserDetailLogReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ehUserDetailLogReport.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.ehUserDetailLogReport.GradientColor1 = System.Drawing.SystemColors.Window
        Me.ehUserDetailLogReport.GradientColor2 = System.Drawing.SystemColors.Control
        Me.ehUserDetailLogReport.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.ehUserDetailLogReport.Icon = Nothing
        Me.ehUserDetailLogReport.Location = New System.Drawing.Point(0, 0)
        Me.ehUserDetailLogReport.Message = ""
        Me.ehUserDetailLogReport.Name = "ehUserDetailLogReport"
        Me.ehUserDetailLogReport.Size = New System.Drawing.Size(673, 60)
        Me.ehUserDetailLogReport.TabIndex = 3
        Me.ehUserDetailLogReport.Title = "User Details Log Report"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchCompany)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromdate)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblCompany)
        Me.gbFilterCriteria.Controls.Add(Me.cboCompany)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(376, 133)
        Me.gbFilterCriteria.TabIndex = 4
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReportType
        '
        Me.lblReportType.BackColor = System.Drawing.Color.Transparent
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(15, 66)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(84, 15)
        Me.lblReportType.TabIndex = 197
        Me.lblReportType.Text = "Report Type"
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 230
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(105, 63)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(227, 21)
        Me.cboReportType.TabIndex = 0
        '
        'lblCompany
        '
        Me.lblCompany.BackColor = System.Drawing.Color.Transparent
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(15, 39)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(84, 15)
        Me.lblCompany.TabIndex = 234
        Me.lblCompany.Text = "Company"
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.DropDownWidth = 230
        Me.cboCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(105, 36)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(227, 21)
        Me.cboCompany.TabIndex = 233
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(15, 93)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(84, 15)
        Me.lblFromDate.TabIndex = 235
        Me.lblFromDate.Text = "Date"
        '
        'dtpFromdate
        '
        Me.dtpFromdate.Checked = False
        Me.dtpFromdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromdate.Location = New System.Drawing.Point(105, 90)
        Me.dtpFromdate.Name = "dtpFromdate"
        Me.dtpFromdate.Size = New System.Drawing.Size(93, 21)
        Me.dtpFromdate.TabIndex = 236
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(204, 93)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(26, 15)
        Me.lblToDate.TabIndex = 237
        Me.lblToDate.Text = "To"
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(236, 90)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(93, 21)
        Me.dtpToDate.TabIndex = 238
        '
        'objbtnSearchCompany
        '
        Me.objbtnSearchCompany.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCompany.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCompany.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCompany.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCompany.BorderSelected = False
        Me.objbtnSearchCompany.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCompany.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCompany.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCompany.Location = New System.Drawing.Point(338, 36)
        Me.objbtnSearchCompany.Name = "objbtnSearchCompany"
        Me.objbtnSearchCompany.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCompany.TabIndex = 239
        '
        'frmUserDetailsLogReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(673, 473)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.ehUserDetailLogReport)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUserDetailsLogReport"
        Me.Text = "User Details Log Report"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents ehUserDetailLogReport As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblReportType As System.Windows.Forms.Label
    Public WithEvents cboReportType As System.Windows.Forms.ComboBox
    Private WithEvents lblCompany As System.Windows.Forms.Label
    Public WithEvents cboCompany As System.Windows.Forms.ComboBox
    Private WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Private WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpFromdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchCompany As eZee.Common.eZeeGradientButton
End Class
