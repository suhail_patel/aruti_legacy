﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeAgeAnalysisReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlCriteria = New System.Windows.Forms.Panel
        Me.cboCondition = New System.Windows.Forms.ComboBox
        Me.lblCondition = New System.Windows.Forms.Label
        Me.nudYear = New System.Windows.Forms.NumericUpDown
        Me.lblYear = New System.Windows.Forms.Label
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.nudYearGap = New System.Windows.Forms.NumericUpDown
        Me.nudToYear = New System.Windows.Forms.NumericUpDown
        Me.nudFromYear = New System.Windows.Forms.NumericUpDown
        Me.lblYearGap = New System.Windows.Forms.Label
        Me.lblToYear = New System.Windows.Forms.Label
        Me.lblFromYear = New System.Windows.Forms.Label
        Me.chkSlabWiseReport = New System.Windows.Forms.CheckBox
        Me.objELine = New eZee.Common.eZeeLine
        Me.chkReportType = New System.Windows.Forms.CheckBox
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.dtpDateAsOn = New System.Windows.Forms.DateTimePicker
        Me.lblDateAsOn = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.gbSortBy.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.pnlCriteria.SuspendLayout()
        CType(Me.nudYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudYearGap, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 476)
        Me.NavPanel.Size = New System.Drawing.Size(773, 55)
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 290)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(433, 63)
        Me.gbSortBy.TabIndex = 17
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(404, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(78, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(92, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(306, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.pnlCriteria)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeLine1)
        Me.gbFilterCriteria.Controls.Add(Me.nudYearGap)
        Me.gbFilterCriteria.Controls.Add(Me.nudToYear)
        Me.gbFilterCriteria.Controls.Add(Me.nudFromYear)
        Me.gbFilterCriteria.Controls.Add(Me.lblYearGap)
        Me.gbFilterCriteria.Controls.Add(Me.lblToYear)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromYear)
        Me.gbFilterCriteria.Controls.Add(Me.chkSlabWiseReport)
        Me.gbFilterCriteria.Controls.Add(Me.objELine)
        Me.gbFilterCriteria.Controls.Add(Me.chkReportType)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDateAsOn)
        Me.gbFilterCriteria.Controls.Add(Me.lblDateAsOn)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.txtCode)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployeeCode)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(433, 218)
        Me.gbFilterCriteria.TabIndex = 16
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlCriteria
        '
        Me.pnlCriteria.Controls.Add(Me.cboCondition)
        Me.pnlCriteria.Controls.Add(Me.lblCondition)
        Me.pnlCriteria.Controls.Add(Me.nudYear)
        Me.pnlCriteria.Controls.Add(Me.lblYear)
        Me.pnlCriteria.Location = New System.Drawing.Point(3, 189)
        Me.pnlCriteria.Name = "pnlCriteria"
        Me.pnlCriteria.Size = New System.Drawing.Size(332, 25)
        Me.pnlCriteria.TabIndex = 83
        '
        'cboCondition
        '
        Me.cboCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCondition.FormattingEnabled = True
        Me.cboCondition.Location = New System.Drawing.Point(89, 2)
        Me.cboCondition.Name = "cboCondition"
        Me.cboCondition.Size = New System.Drawing.Size(101, 21)
        Me.cboCondition.TabIndex = 82
        '
        'lblCondition
        '
        Me.lblCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCondition.Location = New System.Drawing.Point(8, 5)
        Me.lblCondition.Name = "lblCondition"
        Me.lblCondition.Size = New System.Drawing.Size(78, 15)
        Me.lblCondition.TabIndex = 81
        Me.lblCondition.Text = "Condition"
        Me.lblCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudYear
        '
        Me.nudYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudYear.Location = New System.Drawing.Point(263, 2)
        Me.nudYear.Name = "nudYear"
        Me.nudYear.Size = New System.Drawing.Size(52, 21)
        Me.nudYear.TabIndex = 80
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(196, 5)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(61, 15)
        Me.lblYear.TabIndex = 79
        Me.lblYear.Text = "Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(11, 173)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(414, 12)
        Me.EZeeLine1.TabIndex = 82
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudYearGap
        '
        Me.nudYearGap.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudYearGap.Location = New System.Drawing.Point(347, 149)
        Me.nudYearGap.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.nudYearGap.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudYearGap.Name = "nudYearGap"
        Me.nudYearGap.Size = New System.Drawing.Size(53, 21)
        Me.nudYearGap.TabIndex = 80
        Me.nudYearGap.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'nudToYear
        '
        Me.nudToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToYear.Location = New System.Drawing.Point(217, 149)
        Me.nudToYear.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.nudToYear.Name = "nudToYear"
        Me.nudToYear.Size = New System.Drawing.Size(52, 21)
        Me.nudToYear.TabIndex = 79
        Me.nudToYear.Value = New Decimal(New Integer() {60, 0, 0, 0})
        '
        'nudFromYear
        '
        Me.nudFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromYear.Location = New System.Drawing.Point(92, 149)
        Me.nudFromYear.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.nudFromYear.Name = "nudFromYear"
        Me.nudFromYear.Size = New System.Drawing.Size(52, 21)
        Me.nudFromYear.TabIndex = 78
        Me.nudFromYear.Value = New Decimal(New Integer() {21, 0, 0, 0})
        '
        'lblYearGap
        '
        Me.lblYearGap.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYearGap.Location = New System.Drawing.Point(275, 152)
        Me.lblYearGap.Name = "lblYearGap"
        Me.lblYearGap.Size = New System.Drawing.Size(66, 15)
        Me.lblYearGap.TabIndex = 77
        Me.lblYearGap.Text = "Year Gap"
        Me.lblYearGap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblToYear
        '
        Me.lblToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToYear.Location = New System.Drawing.Point(150, 152)
        Me.lblToYear.Name = "lblToYear"
        Me.lblToYear.Size = New System.Drawing.Size(61, 15)
        Me.lblToYear.TabIndex = 76
        Me.lblToYear.Text = "To Year"
        Me.lblToYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFromYear
        '
        Me.lblFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromYear.Location = New System.Drawing.Point(11, 152)
        Me.lblFromYear.Name = "lblFromYear"
        Me.lblFromYear.Size = New System.Drawing.Size(75, 15)
        Me.lblFromYear.TabIndex = 75
        Me.lblFromYear.Text = "From Year"
        Me.lblFromYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSlabWiseReport
        '
        Me.chkSlabWiseReport.Checked = True
        Me.chkSlabWiseReport.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSlabWiseReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSlabWiseReport.Location = New System.Drawing.Point(11, 122)
        Me.chkSlabWiseReport.Name = "chkSlabWiseReport"
        Me.chkSlabWiseReport.Size = New System.Drawing.Size(414, 17)
        Me.chkSlabWiseReport.TabIndex = 74
        Me.chkSlabWiseReport.Text = "Slab Wise Report"
        Me.chkSlabWiseReport.UseVisualStyleBackColor = True
        '
        'objELine
        '
        Me.objELine.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objELine.Location = New System.Drawing.Point(11, 107)
        Me.objELine.Name = "objELine"
        Me.objELine.Size = New System.Drawing.Size(414, 12)
        Me.objELine.TabIndex = 18
        Me.objELine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkReportType
        '
        Me.chkReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkReportType.Location = New System.Drawing.Point(218, 86)
        Me.chkReportType.Name = "chkReportType"
        Me.chkReportType.Size = New System.Drawing.Size(207, 20)
        Me.chkReportType.TabIndex = 72
        Me.chkReportType.Text = "Generate on Appointment Date"
        Me.chkReportType.UseVisualStyleBackColor = True
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(11, 88)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(188, 16)
        Me.chkInactiveemp.TabIndex = 70
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(335, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 18
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpDateAsOn
        '
        Me.dtpDateAsOn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateAsOn.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDateAsOn.Location = New System.Drawing.Point(92, 33)
        Me.dtpDateAsOn.Name = "dtpDateAsOn"
        Me.dtpDateAsOn.Size = New System.Drawing.Size(100, 21)
        Me.dtpDateAsOn.TabIndex = 68
        '
        'lblDateAsOn
        '
        Me.lblDateAsOn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateAsOn.Location = New System.Drawing.Point(8, 36)
        Me.lblDateAsOn.Name = "lblDateAsOn"
        Me.lblDateAsOn.Size = New System.Drawing.Size(78, 15)
        Me.lblDateAsOn.TabIndex = 67
        Me.lblDateAsOn.Text = "As On Date"
        Me.lblDateAsOn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(406, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 59
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(282, 33)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(118, 21)
        Me.txtCode.TabIndex = 65
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(198, 36)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(78, 15)
        Me.lblEmployeeCode.TabIndex = 64
        Me.lblEmployeeCode.Text = "Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(78, 15)
        Me.lblEmployee.TabIndex = 57
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(92, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(308, 21)
        Me.cboEmployee.TabIndex = 58
        '
        'frmEmployeeAgeAnalysisReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(773, 531)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmEmployeeAgeAnalysisReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Age Analysis Report"
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.pnlCriteria.ResumeLayout(False)
        CType(Me.nudYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudYearGap, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpDateAsOn As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateAsOn As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents chkReportType As System.Windows.Forms.CheckBox
    Friend WithEvents chkSlabWiseReport As System.Windows.Forms.CheckBox
    Friend WithEvents objELine As eZee.Common.eZeeLine
    Friend WithEvents nudYearGap As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudToYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudFromYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblYearGap As System.Windows.Forms.Label
    Friend WithEvents lblToYear As System.Windows.Forms.Label
    Friend WithEvents lblFromYear As System.Windows.Forms.Label
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents pnlCriteria As System.Windows.Forms.Panel
    Friend WithEvents nudYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents cboCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lblCondition As System.Windows.Forms.Label
End Class
