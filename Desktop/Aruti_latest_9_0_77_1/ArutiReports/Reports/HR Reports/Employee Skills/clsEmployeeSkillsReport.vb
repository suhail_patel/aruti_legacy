'************************************************************************************************************************************
'Class Name : clsEmployeeSkills.vb
'Purpose    :
'Date       :10/9/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsEmployeeSkillsReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeSkillsReport"
    Private mstrReportId As String = enArutiReport.Employee_Skills  '19
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Varibales "

    Private mstrEmployeeCode As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintSkillCategoryId As Integer = -1
    Private mstrSkillCategoryName As String = String.Empty
    Private mintSkillId As Integer = -1
    Private mstrSkillName As String = String.Empty

    'Sandeep [ 12 MARCH 2011 ] -- Start
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'Sandeep [ 12 MARCH 2011 ] -- End
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (16 May 2012) -- End

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End
    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrUserAccessFilter As String = String.Empty
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _SkillCategoryId() As Integer
        Set(ByVal value As Integer)
            mintSkillCategoryId = value
        End Set
    End Property

    Public WriteOnly Property _SkillCategoryName() As String
        Set(ByVal value As String)
            mstrSkillCategoryName = value
        End Set
    End Property

    Public WriteOnly Property _SkillId() As Integer
        Set(ByVal value As Integer)
            mintSkillId = value
        End Set
    End Property

    Public WriteOnly Property _SkillName() As String
        Set(ByVal value As String)
            mstrSkillName = value
        End Set
    End Property

    'Sandeep [ 12 MARCH 2011 ] -- Start
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property
    'Sandeep [ 12 MARCH 2011 ] -- End 

    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (16 May 2012) -- End

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End
    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrEmployeeCode = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintSkillCategoryId = 0
            mstrSkillCategoryName = ""
            mintSkillId = 0
            mstrSkillName = ""
            'Sandeep [ 12 MARCH 2011 ] -- Start
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            'Sandeep [ 12 MARCH 2011 ] -- End 

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (16 May 2012) -- End

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            'Sandeep [ 12 MARCH 2011 ] -- Start
            'If mstrEmployeeCode.Trim <> "" Then
            '    objDataOperation.AddParameter("@EmployeeCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrEmployeeCode & "%")
            '    Me._FilterQuery &= " AND hremployee_master.employeecode LIKE @EmployeeCode "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Employee Code :") & " " & mstrEmployeeCode & " "
            'End If

            'If mintEmployeeId > 0 Then
            '    objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            '    Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Employee :") & " " & mstrEmployeeName & " "
            'End If

            'If mintSkillCategoryId > 0 Then
            '    objDataOperation.AddParameter("@SkillCategoryId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillCategoryId)
            '    Me._FilterQuery &= " AND cfcommon_master.masterunkid = @SkillCategoryId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Skill Category :") & " " & mstrSkillCategoryName & " "
            'End If

            'If mintSkillId > 0 Then
            '    objDataOperation.AddParameter("@SkillId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillId)
            '    Me._FilterQuery &= " AND hrskill_master.skillunkid = @SkillId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Skill :") & " " & mstrSkillName & " "
            'End If

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If mstrEmployeeCode.Trim <> "" Then
                objDataOperation.AddParameter("@EmployeeCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrEmployeeCode & "%")
                'Sohail (16 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= " AND ECode LIKE @EmployeeCode "
                Me._FilterQuery &= " AND hremployee_master.employeecode LIKE @EmployeeCode "
                'Sohail (16 May 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Employee Code :") & " " & mstrEmployeeCode & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                'Sohail (16 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= " AND EmpId = @EmployeeId "
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                'Sohail (16 May 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintSkillCategoryId > 0 Then
                objDataOperation.AddParameter("@SkillCategoryId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillCategoryId)
                'Sohail (16 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= " AND SkillCatId = @SkillCategoryId "
                Me._FilterQuery &= " AND cfcommon_master.masterunkid = @SkillCategoryId "
                'Sohail (16 May 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Skill Category :") & " " & mstrSkillCategoryName & " "
            End If

            If mintSkillId > 0 Then
                objDataOperation.AddParameter("@SkillId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillId)
                'Sohail (16 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= " AND SkillId = @SkillId "
                Me._FilterQuery &= " AND hrskill_master.skillunkid = @SkillId "
                'Sohail (16 May 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Skill :") & " " & mstrSkillName & " "
            End If

            If mstrViewByName.Length > 0 Then
                'Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Analysis By :") & " " & mstrViewByName & " " 'Sohail (16 May 2012)
            End If

            'Sandeep [ 12 MARCH 2011 ] -- End 

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If
        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()

        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'Sandeep [ 12 MARCH 2011 ] -- Start
            iColumn_DetailReport.Add(New IColumn("hremployee_master.employeecode", Language.getMessage(mstrModuleName, 1, "Code")))

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 2, "Employee Name :")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 19, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 19, "Employee Name")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            iColumn_DetailReport.Add(New IColumn("ISNULL(cfcommon_master.name,'')", Language.getMessage(mstrModuleName, 3, "Skill Category")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hrskill_master.skillname,'')", Language.getMessage(mstrModuleName, 5, "Skill")))

            'iColumn_DetailReport.Add(New IColumn("ISNULL(ECode,'')", Language.getMessage(mstrModuleName, 1, "Code")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(EmpName,'')", Language.getMessage(mstrModuleName, 2, "Employee Name :")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(SkillCat,'')", Language.getMessage(mstrModuleName, 3, "Skill Category")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(SkillName,'')", Language.getMessage(mstrModuleName, 5, "Skill")))
            'Sandeep [ 12 MARCH 2011 ] -- End 


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        'Sandeep [ 12 MARCH 2011 ] -- Start
        Dim blnFlag As Boolean = False
        'Sandeep [ 12 MARCH 2011 ] -- End 
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END


            'Sandeep [ 12 MARCH 2011 ] -- Start
            'StrQ = "SELECT " & _
            '           " hremployee_master.employeecode AS Code " & _
            '           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '           ",ISNULL(cfcommon_master.name,'') AS SkillCat " & _
            '           ",ISNULL(hrskill_master.skillname,'') AS SkillName " & _
            '           ",ISNULL(hrskill_master.description,'') AS SkillDesc " & _
            '           ",ISNULL(hremp_app_skills_tran.description,'') AS TranDesc " & _
            '       "FROM hremp_app_skills_tran " & _
            '           " JOIN hremployee_master ON hremployee_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid " & _
            '           " JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
            '           " JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid " & _
            '       "WHERE ISNULL(hremp_app_skills_tran.isvoid,0) = 0 " & _
            '           " AND ISNULL(isapplicant,0) = 0 " & _
            '           " AND ISNULL(hremployee_master.isactive,0) = 1 "

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'StrQ = " SELECT "
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        StrQ &= " stationunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Department
            '        StrQ &= " departmentunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Section
            '        StrQ &= "  sectionunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Unit
            '        StrQ &= "  unitunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Job
            '        StrQ &= "  jobunkid , job_name AS GName "
            '        blnFlag = True
            '        'S.SANDEEP [ 06 SEP 2011 ] -- START
            '    Case enAnalysisReport.CostCenter
            '        StrQ &= " costcenterunkid  , costcentername AS GName "
            '        blnFlag = True
            '        'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    Case Else
            '        StrQ &= " ISNULL(ECode,'') AS ECode " & _
            '                    ", ISNULL(EmpName,'') AS EmpName " & _
            '                    ", ISNULL(SkillCat,'') AS SkillCat " & _
            '                    ", ISNULL(SkillName,'') AS SkillName " & _
            '                    ", ISNULL(SkillDesc,'') AS SkillDesc " & _
            '                    ", ISNULL(TranDesc,'') AS TranDesc " & _
            '                    ", '' AS GName "
            '        blnFlag = False
            'End Select

            'If blnFlag = True Then
            '    StrQ &= ", ISNULL(ECode,'') AS ECode " & _
            '                 ", ISNULL(EmpName,'') AS EmpName " & _
            '                 ", ISNULL(SkillCat,'') AS SkillCat " & _
            '                 ", ISNULL(SkillName,'') AS SkillName " & _
            '                 ", ISNULL(SkillDesc,'') AS SkillDesc " & _
            '                 ", ISNULL(TranDesc,'') AS TranDesc "
            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= " FROM hrstation_master "
            '        Case enAnalysisReport.Department
            '            StrQ &= " FROM hrdepartment_master "
            '        Case enAnalysisReport.Section
            '            StrQ &= " FROM hrsection_master "
            '        Case enAnalysisReport.Unit
            '            StrQ &= " FROM hrunit_master "
            '        Case enAnalysisReport.Job
            '            StrQ &= " FROM hrjob_master "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= " FROM prcostcenter_master "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    End Select
            '    StrQ &= " LEFT JOIN " & _
            '                " ( " & _
            '                        "SELECT " & _
            '                           " hremployee_master.employeecode AS ECode " & _
            '           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '           ",ISNULL(cfcommon_master.name,'') AS SkillCat " & _
            '           ",ISNULL(hrskill_master.skillname,'') AS SkillName " & _
            '           ",ISNULL(hrskill_master.description,'') AS SkillDesc " & _
            '           ",ISNULL(hremp_app_skills_tran.description,'') AS TranDesc " & _
            '                           ",cfcommon_master.masterunkid  AS SkillCatId " & _
            '                           ",hrskill_master.skillunkid AS SkillId " & _
            '                           ",hremployee_master.employeeunkid AS EmpId "
            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= ", hremployee_master.stationunkid AS BranchId  "
            '        Case enAnalysisReport.Department
            '            StrQ &= ", hremployee_master.departmentunkid AS DeptId "
            '        Case enAnalysisReport.Section
            '            StrQ &= ", hremployee_master.sectionunkid AS SecId  "
            '        Case enAnalysisReport.Unit
            '            StrQ &= ", hremployee_master.unitunkid AS UnitId  "
            '        Case enAnalysisReport.Job
            '            StrQ &= ", hremployee_master.jobunkid AS JobId  "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= ", hremployee_master.costcenterunkid As CCId "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    End Select


            '    'Pinkal (24-Jun-2011) -- Start
            '    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            '    'StrQ &= "FROM hremp_app_skills_tran " & _
            '    '                  " JOIN hremployee_master ON hremployee_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid " & _
            '    '                  " JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
            '    '                  " JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid " & _
            '    '            "WHERE ISNULL(hremp_app_skills_tran.isvoid,0) = 0 " & _
            '    '                  " AND ISNULL(isapplicant,0) = 0 " & _
            '    '                  " AND ISNULL(hremployee_master.isactive,0) = 1 "

            'StrQ &= "FROM hremp_app_skills_tran " & _
            '                  " JOIN hremployee_master ON hremployee_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid " & _
            '                  " JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
            '                  " JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid " & _
            '            "WHERE ISNULL(hremp_app_skills_tran.isvoid,0) = 0 " & _
            '                     " AND ISNULL(isapplicant,0) = 0 "

            '    If mblIsActive = False Then
            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ISSUE : TRA ENHANCEMENTS
            '        'StrQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
            '        StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END
            '    End If

            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END

            '    'Pinkal (24-Jun-2011) -- End

            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= " ) AS ESR ON hrstation_master.stationunkid = ESR.BranchId " & _
            '                          " WHERE hrstation_master.stationunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Department
            '            StrQ &= " ) AS ESR ON hrdepartment_master.departmentunkid = ESR.DeptId " & _
            '                          " WHERE hrdepartment_master.departmentunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Section
            '            StrQ &= " ) AS ESR ON hrsection_master.sectionunkid = ESR.SecId " & _
            '                          " WHERE hrsection_master.sectionunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Unit
            '            StrQ &= " ) AS ESR ON hrunit_master.unitunkid = ESR.UnitId " & _
            '                          " WHERE hrunit_master.unitunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Job
            '            StrQ &= " ) AS ESR ON hrjob_master.jobunkid = ESR.JobId " & _
            '                          " WHERE hrjob_master.jobunkid IN ( " & mstrViewByIds & " ) "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= " ) AS ESR ON prcostcenter_master.costcenterunkid = ESR.CCId " & _
            '                          " WHERE prcostcenter_master.costcenterunkid IN ( " & mstrViewByIds & " ) "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    End Select
            'Else


            '    'Pinkal (24-Jun-2011) -- Start
            '    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            '    'StrQ &= " FROM " & _
            '    '            " ( " & _
            '    '                 "SELECT " & _
            '    '                   " hremployee_master.employeecode AS ECode " & _
            '    '                   ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '    '                   ",ISNULL(cfcommon_master.name,'') AS SkillCat " & _
            '    '                   ",ISNULL(hrskill_master.skillname,'') AS SkillName " & _
            '    '                   ",ISNULL(hrskill_master.description,'') AS SkillDesc " & _
            '    '                   ",ISNULL(hremp_app_skills_tran.description,'') AS TranDesc " & _
            '    '                   ",cfcommon_master.masterunkid  AS SkillCatId " & _
            '    '                   ",hrskill_master.skillunkid AS SkillId " & _
            '    '                   ",hremployee_master.employeeunkid AS EmpId " & _
            '    '   "FROM hremp_app_skills_tran " & _
            '    '       " JOIN hremployee_master ON hremployee_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid " & _
            '    '       " JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
            '    '       " JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid " & _
            '    '   "WHERE ISNULL(hremp_app_skills_tran.isvoid,0) = 0 " & _
            '    '       " AND ISNULL(isapplicant,0) = 0 " & _
            '    '                   " AND ISNULL(hremployee_master.isactive,0) = 1 ) AS ESR WHERE 1 = 1 "

            'StrQ &= " FROM " & _
            '            " ( " & _
            '                 "SELECT " & _
            '                   " hremployee_master.employeecode AS ECode " & _
            '                   ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '                   ",ISNULL(cfcommon_master.name,'') AS SkillCat " & _
            '                   ",ISNULL(hrskill_master.skillname,'') AS SkillName " & _
            '                   ",ISNULL(hrskill_master.description,'') AS SkillDesc " & _
            '                   ",ISNULL(hremp_app_skills_tran.description,'') AS TranDesc " & _
            '                   ",cfcommon_master.masterunkid  AS SkillCatId " & _
            '                   ",hrskill_master.skillunkid AS SkillId " & _
            '                   ",hremployee_master.employeeunkid AS EmpId " & _
            '   "FROM hremp_app_skills_tran " & _
            '       " JOIN hremployee_master ON hremployee_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid " & _
            '       " JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
            '       " JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid " & _
            '   "WHERE ISNULL(hremp_app_skills_tran.isvoid,0) = 0 " & _
            '         " AND ISNULL(isapplicant,0) = 0 "

            '    If mblIsActive = False Then
            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ISSUE : TRA ENHANCEMENTS
            '        'StrQ &= "  AND ISNULL(hremployee_master.isactive,0) = 1 "
            '        StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END
            '    End If

            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END

            '    StrQ &= " ) AS ESR WHERE 1 = 1 "

            '    'Pinkal (24-Jun-2011) -- End

            'End If
            ''Sandeep [ 12 MARCH 2011 ] -- End 

            StrQ = "SELECT hremployee_master.employeecode AS ECode "

            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            '",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= ",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EmpName "
            Else
                StrQ &= ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName "
            End If
            'SANDEEP [ 26 MAR 2014 ] -- END

            StrQ &= ",ISNULL(cfcommon_master.name,'') AS SkillCat " & _
                               ",ISNULL(hrskill_master.skillname,'') AS SkillName " & _
                               ",ISNULL(hrskill_master.description,'') AS SkillDesc " & _
                               ",ISNULL(hremp_app_skills_tran.description,'') AS TranDesc " & _
                               ",cfcommon_master.masterunkid  AS SkillCatId " & _
                               ",hrskill_master.skillunkid AS SkillId " & _
                  ", hremployee_master.employeeunkid AS EmpId "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM   hremp_app_skills_tran " & _
                       " JOIN hremployee_master ON hremployee_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            StrQ &= " JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
                        "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= mstrAnalysis_Join

            StrQ &= "WHERE  ISNULL(hremp_app_skills_tran.isvoid, 0) = 0 " & _
                     " AND ISNULL(isapplicant,0) = 0 "

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mblIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- END

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (16 May 2012) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                'S.SANDEEP [ 23 SEP 2011 ] -- START
                'ENHANCEMENT : CODE OPTIMIZATION
                If dtRow.Item("ECode").ToString.Trim.Length <= 0 Then Continue For
                'S.SANDEEP [ 23 SEP 2011 ] -- END


                'Sandeep [ 12 MARCH 2011 ] -- Start
                'rpt_Rows.Item("Column1") = dtRow.Item("Code")
                rpt_Rows.Item("Column1") = dtRow.Item("ECode")
                'Sandeep [ 12 MARCH 2011 ] -- End 
                rpt_Rows.Item("Column2") = dtRow.Item("EmpName")
                rpt_Rows.Item("Column3") = dtRow.Item("SkillCat")
                rpt_Rows.Item("Column4") = dtRow.Item("SkillName")
                rpt_Rows.Item("Column5") = dtRow.Item("SkillDesc")

                'Sandeep [ 12 MARCH 2011 ] -- Start
                rpt_Rows.Item("Column6") = dtRow.Item("GName")
                'Sandeep [ 12 MARCH 2011 ] -- End 

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeSkills

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription")

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 15, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 16, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 17, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 18, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End 


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 1, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 2, "Employee Name :"))
            Call ReportFunction.TextChange(objRpt, "txtSkillCategory", Language.getMessage(mstrModuleName, 3, "Skill Category"))
            Call ReportFunction.TextChange(objRpt, "txtSkillNAme", Language.getMessage(mstrModuleName, 5, "Skill"))
            Call ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 6, "Skill Description"))


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 7, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 8, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 9, "Page :"))

            'Sandeep [ 12 MARCH 2011 ] -- Start
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 31, "Branch :"))
            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 32, "Department :"))
            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 33, "Section :"))
            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 34, "Unit :"))
            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 35, "Job :"))
            '        'S.SANDEEP [ 06 SEP 2011 ] -- START
            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 36, "Cost Center :"))
            '        'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    Case Else
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", "")
            'End Select
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'Sohail (16 May 2012) -- End
            'Sandeep [ 12 MARCH 2011 ] -- End 


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Code")
            Language.setMessage(mstrModuleName, 2, "Employee Name :")
            Language.setMessage(mstrModuleName, 3, "Skill Category")
            Language.setMessage(mstrModuleName, 5, "Skill")
            Language.setMessage(mstrModuleName, 6, "Skill Description")
            Language.setMessage(mstrModuleName, 7, "Printed By :")
            Language.setMessage(mstrModuleName, 8, "Printed Date :")
            Language.setMessage(mstrModuleName, 9, "Page :")
            Language.setMessage(mstrModuleName, 10, "Employee Code :")
            Language.setMessage(mstrModuleName, 11, "Employee :")
            Language.setMessage(mstrModuleName, 12, "Skill Category :")
            Language.setMessage(mstrModuleName, 13, "Skill :")
            Language.setMessage(mstrModuleName, 14, "Order By :")
            Language.setMessage(mstrModuleName, 15, "Prepared By :")
            Language.setMessage(mstrModuleName, 16, "Checked By :")
            Language.setMessage(mstrModuleName, 17, "Approved By :")
            Language.setMessage(mstrModuleName, 18, "Received By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
