'************************************************************************************************************************************
'Class Name : clsEmployeeDisposition_Report.vb
'Purpose    :
'Date       :09/04/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsEmployeeDisposition_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeDisposition_Report"
    Private mstrReportId As String = enArutiReport.EmployeeDispositionReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "
    Private mintClassgroupUnkId As Integer = 0
    Private mstrClassgroupName As String = ""

    Private mblnIsActive As Boolean = True

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIncludeInactiveEmp As Boolean = False
    'S.SANDEEP [ 12 MAY 2012 ] -- END

    'S.SANDEEP [ 18 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'S.SANDEEP [ 18 AUG 2012 ] -- END

    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrUserAccessFilter As String = String.Empty
    'Pinkal (12-Nov-2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End
#End Region

#Region " Properties "
    Public WriteOnly Property _ClassGroupUnkId() As Integer
        Set(ByVal value As Integer)
            mintClassgroupUnkId = value
        End Set
    End Property

    Public WriteOnly Property _ClassGroupName() As String
        Set(ByVal value As String)
            mstrClassgroupName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 12 MAY 2012 ] -- END

    'S.SANDEEP [ 18 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 18 AUG 2012 ] -- END


    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'Pinkal (12-Nov-2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End
#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintClassgroupUnkId = 0
            mstrClassgroupName = ""

            'S.SANDEEP [ 18 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Rpt = Nothing
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            'S.SANDEEP [ 18 AUG 2012 ] -- END

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If mintClassgroupUnkId > 0 Then
                objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupUnkId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, " Region : ") & " " & mstrClassgroupName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Order By :") & " " & Me.OrderByDisplay
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY " & mstrAnalysis_OrderBy & ", " & Me.OrderByQuery
                Else
                    Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    'S.SANDEEP [ 18 AUG 2012 ] -- START
        '    'ENHANCEMENT : TRA CHANGES
        '    'objRpt = Generate_DetailReport()
        '    'If Not IsNothing(objRpt) Then
        '    '    Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    'End If

        '    If mintCompanyUnkid > 0 Then
        '        Company._Object._Companyunkid = mintCompanyUnkid
        '        ConfigParameter._Object._Companyunkid = mintCompanyUnkid
        '    End If
        '    If mintUserUnkid > 0 Then
        '        User._Object._Userunkid = mintUserUnkid
        '    End If

        'objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    If mintCompanyUnkid <= 0 Then
        'If Not IsNothing(objRpt) Then
        '    Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        'End If
        '    End If
        '    'S.SANDEEP [ 18 AUG 2012 ] -- END
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid > 0 Then
                Company._Object._Companyunkid = mintCompanyUnkid
                ConfigParameter._Object._Companyunkid = mintCompanyUnkid
            End If
            If mintUserUnkid > 0 Then
                User._Object._Userunkid = mintUserUnkid
            End If

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt

            If mintCompanyUnkid <= 0 Then
                If Not IsNothing(objRpt) Then
                    Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("classgroupname", Language.getMessage(mstrModuleName, 3, "Region")))
            'iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 5, "Employee Code")))
            'iColumn_DetailReport.Add(New IColumn("deptname", Language.getMessage(mstrModuleName, 6, "Department")))
            'iColumn_DetailReport.Add(New IColumn("jobname", Language.getMessage(mstrModuleName, 7, "Job Title")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            'StrQ = "SELECT  a.name AS classgroupname " & _
            '              ", COUNT(employeeunkid) AS TotEmp "
            StrQ = "SELECT   hrclassgroup_master.name AS classgroupname  " & _
                   "        ,COUNT(hremployee_master.employeeunkid) AS TotEmp "
            'Shani(24-Aug-2015) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= "FROM    hremployee_master " & _
            '                "JOIN hrclassgroup_master AS a ON a.classgroupunkid = hremployee_master.classgroupunkid "

            StrQ &= "FROM    hremployee_master "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= "LEFT JOIN " & _
                            "( " & _
                                "SELECT " & _
                                    "classgroupunkid " & _
                                    ",employeeunkid " & _
                                    ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "FROM hremployee_transfer_tran " & _
                                "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                        "JOIN hrclassgroup_master  ON hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid "
            'Shani(24-Aug-2015) -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= mstrAnalysis_Join

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            'Shani(24-Aug-2015) -- End

            StrQ &= "WHERE 1 = 1 "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ISSUE : TRA ENHANCEMENTS
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            'End If
            ''S.SANDEEP [ 12 MAY 2012 ] -- END
            ''S.SANDEEP [ 13 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            ''S.SANDEEP [ 13 FEB 2013 ] -- END
            ''Pinkal (12-Nov-2012) -- Start
            ''Enhancement : TRA Changes
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            'If mstrUserAccessFilter = "" Then
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            ''Pinkal (12-Nov-2012) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            'S.SANDEEP [20-Mar-2018] -- START
            'ISSUE/ENHANCEMENT : {#ARUTI-53}
            'If mblnIsActive = False Then
            If mblnIncludeInactiveEmp = False Then '
                'S.SANDEEP [20-Mar-2018] -- END
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Shani(24-Aug-2015) -- End

            If mintClassgroupUnkId > 0 Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'StrQ &= "AND a.classgroupunkid = @classgroupunkid "
                StrQ &= "AND hrclassgroup_master.classgroupunkid = @classgroupunkid "
                'Shani(24-Aug-2015) -- End
            End If


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= "GROUP BY a.name " & mstrAnalysis_Fields.Replace(" AS Id,", ",").Replace(" AS GName", "")
            StrQ &= "GROUP BY hrclassgroup_master.name " & mstrAnalysis_Fields.Replace(" AS Id,", ",").Replace(" AS GName", "")
            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then

                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                'rpt_Rows.Item("Column1") = dtRow.Item("Id").ToString
                rpt_Rows.Item("Column1") = dtRow.Item("GName").ToString
                rpt_Rows.Item("Column2") = dtRow.Item("classgroupname").ToString
                rpt_Rows.Item("Column3") = dtRow.Item("TotEmp").ToString


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

            Next

            objRpt = New ArutiReport.Designer.rptEmployeeDisposition_Report

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 4, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 5, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 6, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 7, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If



            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            If mintViewIndex > 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", False)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", False)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If
            Call ReportFunction.TextChange(objRpt, "txtRegion", Language.getMessage(mstrModuleName, 3, "Region"))
            Call ReportFunction.TextChange(objRpt, "txtTotalEmp", Language.getMessage(mstrModuleName, 8, "Total Employee"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 9, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 10, "Grand Total :"))

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, " Region :")
            Language.setMessage(mstrModuleName, 2, "Order By :")
            Language.setMessage(mstrModuleName, 3, "Region")
            Language.setMessage(mstrModuleName, 4, "Prepared By :")
            Language.setMessage(mstrModuleName, 5, "Checked By :")
            Language.setMessage(mstrModuleName, 6, "Approved By :")
            Language.setMessage(mstrModuleName, 7, "Received By :")
            Language.setMessage(mstrModuleName, 8, "Total Employee")
            Language.setMessage(mstrModuleName, 9, "Sub Total :")
            Language.setMessage(mstrModuleName, 10, "Grand Total :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
