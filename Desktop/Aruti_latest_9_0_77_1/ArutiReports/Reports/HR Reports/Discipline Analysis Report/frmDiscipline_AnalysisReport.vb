Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports


Public Class frmDiscipline_AnalysisReport
    Private mstrModuleName As String = "frmDiscipline_AnalysisReport"
    Private objDiscipline As clsDiscipline_analysisReport

#Region "Constructor"
    Public Sub New()
        objDiscipline = New clsDiscipline_analysisReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objDiscipline.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Try
            Dim dsList As New DataSet

            Dim objDisciplineType As New clsDisciplineType
            dsList = objDisciplineType.getComboList("Type", True)
            With cboDisciplineType
                .ValueMember = "disciplinetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            dsList = Nothing

            Dim objDisciplineAction As New clsAction_Reason
            dsList = objDisciplineAction.getComboList("Action", True, True)
            With cboDisciplineAction
                .ValueMember = "actionreasonunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            dsList = Nothing

            Dim objEmp As New clsEmployee_Master


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = objEmp.GetEmployeeList("Investigator", True, True)
            dsList = objEmp.GetEmployeeList("Investigator", True, False)
            'Pinkal (24-Jun-2011) -- End

            With cboInvestigator
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            dsList = Nothing


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = objEmp.GetEmployeeList("InvolvedPerson", True, True)
            dsList = objEmp.GetEmployeeList("InvolvedPerson", True, False)
            'Pinkal (24-Jun-2011) -- End

            With cboInvolvedPerson
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            dsList = Nothing


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = objEmp.GetEmployeeList("Against", True, True)
            dsList = objEmp.GetEmployeeList("Against", True, False)
            'Pinkal (24-Jun-2011) -- End

            With cboAgainst
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            dsList = Nothing
            objEmp = Nothing


            Dim objDisciplineStatus As New clsDiscipline_Status
            dsList = objDisciplineStatus.getComboList("Status", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Status")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpFromDate.Value = DateTime.Now.Date
            dtpToDate.Value = DateTime.Now.Date
            cboDisciplineType.SelectedValue = 0
            cboDisciplineAction.SelectedValue = 0
            cboInvestigator.SelectedValue = 0
            cboInvolvedPerson.SelectedValue = 0
            cboAgainst.SelectedValue = 0
            cboStatus.SelectedValue = 0
            objDiscipline.setDefaultOrderBy(0)
            txtOrderBy.Text = objDiscipline.OrderByDisplay


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objDiscipline.SetDefaultValue()

            objDiscipline._FromDate = dtpFromDate.Value.Date
            objDiscipline._ToDate = dtpToDate.Value.Date

            objDiscipline._DisciplineTypeId = cboDisciplineType.SelectedValue
            objDiscipline._DisciplineTypeName = cboDisciplineType.Text

            objDiscipline._ActionId = cboDisciplineAction.SelectedValue
            objDiscipline._ActionName = cboDisciplineAction.Text

            objDiscipline._InvastigatorId = cboInvestigator.SelectedValue
            objDiscipline._InvastigatorName = cboInvestigator.Text

            objDiscipline._InvolvedPersonId = cboInvolvedPerson.SelectedValue
            objDiscipline._InvolvedPersonName = cboInvolvedPerson.Text

            objDiscipline._AgaintstPersonId = cboAgainst.SelectedValue
            objDiscipline._AgainstPersonName = cboAgainst.Text

            objDiscipline._StatusId = cboStatus.SelectedValue
            objDiscipline._Status = cboStatus.Text


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objDiscipline._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Form's Events"

    Private Sub frmDiscipline_AnalysisReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objDiscipline = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDiscipline_AnalysisReport_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmDiscipline_AnalysisReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Me._Title = objDiscipline._ReportName
            Me._Message = objDiscipline._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDiscipline_AnalysisReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDiscipline_AnalysisReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmDiscipline_AnalysisReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDiscipline_AnalysisReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDiscipline_AnalysisReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDiscipline_AnalysisReport_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnSearchInvestigator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchInvestigator.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboInvestigator.DataSource
            frm.ValueMember = cboInvestigator.ValueMember
            frm.DisplayMember = cboInvestigator.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboInvestigator.SelectedValue = frm.SelectedValue
                cboInvestigator.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchInvestigator_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchobjbtnSearchInvPerson_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchInvPerson.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboInvolvedPerson.DataSource
            frm.ValueMember = cboInvolvedPerson.ValueMember
            frm.DisplayMember = cboInvolvedPerson.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboInvolvedPerson.SelectedValue = frm.SelectedValue
                objbtnSearchInvPerson.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchInvPerson_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAgainst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAgainst.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboAgainst.DataSource
            frm.ValueMember = cboAgainst.ValueMember
            frm.DisplayMember = cboAgainst.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboAgainst.SelectedValue = frm.SelectedValue
                objbtnSearchAgainst.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAgainst_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objDiscipline.setOrderBy(0)
            txtOrderBy.Text = objDiscipline.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmDiscipline_AnalysisReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub
            objDiscipline.generateReport(0, e.Type, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDiscipline_AnalysisReport_Report_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmDiscipline_AnalysisReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub
            objDiscipline.generateReport(0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDiscipline_AnalysisReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDiscipline_AnalysisReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDiscipline_AnalysisReport_Reset_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmDiscipline_AnalysisReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDiscipline_AnalysisReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblDisciplineType.Text = Language._Object.getCaption(Me.lblDisciplineType.Name, Me.lblDisciplineType.Text)
			Me.lblDisciplineAction.Text = Language._Object.getCaption(Me.lblDisciplineAction.Name, Me.lblDisciplineAction.Text)
			Me.lblInvestigator.Text = Language._Object.getCaption(Me.lblInvestigator.Name, Me.lblInvestigator.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.lblAgainstPerson.Text = Language._Object.getCaption(Me.lblAgainstPerson.Name, Me.lblAgainstPerson.Text)
			Me.lblInvolvedPerson.Text = Language._Object.getCaption(Me.lblInvolvedPerson.Name, Me.lblInvolvedPerson.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
