'************************************************************************************************************************************
'Class Name : clsTrainingVoidCancelReport.vb
'Purpose    :
'Date       :22/02/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsTrainingVoidCancelReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTrainingVoidCancelReport"
    Private mstrReportId As String = enArutiReport.Training_Void_CancelReport   '36
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = String.Empty
    Private mintVoidUserId As Integer = 0
    Private mstrVoidUserName As String = String.Empty
    Private mintCancelUserId As Integer = 0
    Private mstrCancelUserName As String = String.Empty
    Private mintInstituteId As Integer = 0
    Private mstrInstituteName As String = String.Empty
    Private mstrQGrpId As Integer = 0
    Private mstrQGrpName As String = String.Empty
    Private mintQualifId As Integer = 0
    Private mstrQualifName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _VoidUserId() As Integer
        Set(ByVal value As Integer)
            mintVoidUserId = value
        End Set
    End Property

    Public WriteOnly Property _VoidUserName() As String
        Set(ByVal value As String)
            mstrVoidUserName = value
        End Set
    End Property

    Public WriteOnly Property _CancelUserId() As Integer
        Set(ByVal value As Integer)
            mintCancelUserId = value
        End Set
    End Property

    Public WriteOnly Property _CancelUserName() As String
        Set(ByVal value As String)
            mstrCancelUserName = value
        End Set
    End Property

    Public WriteOnly Property _InstituteId() As Integer
        Set(ByVal value As Integer)
            mintInstituteId = value
        End Set
    End Property

    Public WriteOnly Property _InstituteName() As String
        Set(ByVal value As String)
            mstrInstituteName = value
        End Set
    End Property

    Public WriteOnly Property _QGrpId() As Integer
        Set(ByVal value As Integer)
            mstrQGrpId = value
        End Set
    End Property

    Public WriteOnly Property _QGrpName() As String
        Set(ByVal value As String)
            mstrQGrpName = value
        End Set
    End Property

    Public WriteOnly Property _QualifId() As Integer
        Set(ByVal value As Integer)
            mintQualifId = value
        End Set
    End Property

    Public WriteOnly Property _QualifName() As String
        Set(ByVal value As String)
            mstrQualifName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintStatusId = 0
            mstrStatusName = ""
            mintVoidUserId = 0
            mstrVoidUserName = ""
            mintCancelUserId = 0
            mstrCancelUserName = ""
            mintInstituteId = 0
            mstrInstituteName = ""
            mstrQGrpId = 0
            mstrQGrpName = ""
            mintQualifId = 0
            mstrQualifName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@CStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Cancelled"))
            objDataOperation.AddParameter("@VStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Voided"))

            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND VCStatusId = @StatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Status :") & " " & mstrStatusName & " "
            End If

            If mintVoidUserId > 0 Then
                objDataOperation.AddParameter("@VoidUserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserId)
                Me._FilterQuery &= " AND VId = @VoidUserId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Void User :") & " " & mstrVoidUserName & " "
            End If

            If mintCancelUserId > 0 Then
                objDataOperation.AddParameter("@CancelUserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelUserId)
                Me._FilterQuery &= " AND CanId = @CancelUserId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Cancel User :") & " " & mstrCancelUserName & " "
            End If

            If mintInstituteId > 0 Then
                objDataOperation.AddParameter("@InstituteId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteId)
                Me._FilterQuery &= " AND InstId = @InstituteId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Institute :") & " " & mstrInstituteName & " "
            End If

            If mstrQGrpId > 0 Then
                objDataOperation.AddParameter("@QGrpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrQGrpId)
                Me._FilterQuery &= " AND QGId = @QGrpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Qualification Group :") & " " & mstrQGrpName & " "
            End If

            If mintQualifId > 0 Then
                objDataOperation.AddParameter("@QualifId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualifId)
                Me._FilterQuery &= " AND QAId = @QualifId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Qualification :") & " " & mstrQualifName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport()
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Get_Status(Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 0 AS Id, @Select As Name " & _
                   "UNION SELECT 1 AS Id, @CStatus AS Name " & _
                   "UNION SELECT 2 AS Id, @VStatus As Name " & _
                   "ORDER BY Id "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Select"))
            objDataOperation.AddParameter("@CStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Cancelled"))
            objDataOperation.AddParameter("@VStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Voided"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Status; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'StrQ = "SELECT " & _
            '          " Title AS Title " & _
            '          ",QGroup AS QGroup " & _
            '          ",QAward AS QAward " & _
            '          ",institute_name AS institute_name " & _
            '          ",St_date AS St_date " & _
            '          ",St_Time AS St_Time " & _
            '          ",Ed_date AS Ed_date " & _
            '          ",Ed_Time AS Ed_Time " & _
            '          ",VCDate AS VCDate " & _
            '          ",VCTime AS VCTime " & _
            '          ",VCUser AS VCUser " & _
            '          ",VCReason AS VCReason " & _
            '          ",VCStatus AS VCStatus " & _
            '        "FROM " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "course_title AS Title " & _
            '                  ",cfcommon_master.name AS QGroup " & _
            '                  ",hrqualification_master.qualificationname AS QAward " & _
            '                  ",hrinstitute_master.institute_name " & _
            '                  ",CONVERT(CHAR(8),start_date,112) AS St_date " & _
            '                  ",CONVERT(CHAR(5),start_time,108) AS St_Time " & _
            '                  ",CONVERT     (CHAR(8),end_date,112) AS Ed_date " & _
            '                  ",CONVERT(CHAR(5),end_time,108) AS Ed_Time " & _
            '                  ",CASE WHEN iscancel = 1 THEN CONVERT(CHAR(8),cancellationdate,112) " & _
            '                              "WHEN isvoid = 1 THEN CONVERT(CHAR(8),voiddatetime,112) " & _
            '                   "END AS VCDate " & _
            '                  ",CASE WHEN iscancel = 1 THEN CONVERT(CHAR(5),cancellationdate,108) " & _
            '                              "WHEN isvoid = 1 THEN CONVERT(CHAR(5),voiddatetime,108) " & _
            '                   "END AS VCTime " & _
            '                  ",CASE WHEN iscancel = 1 THEN ISNULL(hrmsConfiguration..cfuser_master.username,'') " & _
            '                              "WHEN isvoid = 1 THEN ISNULL(VoidUser.username,'') " & _
            '                   "END AS VCUser " & _
            '                  ",CASE WHEN iscancel = 1 THEN cancellationreason " & _
            '                              "WHEN isvoid = 1 THEN voidreason " & _
            '                   "END AS VCReason " & _
            '                  ",CASE WHEN iscancel = 1 THEN @CStatus " & _
            '                              "WHEN isvoid = 1 THEN @VStatus " & _
            '                   "END AS VCStatus " & _
            '                  ",CASE WHEN iscancel = 1 THEN 1 " & _
            '                              "WHEN isvoid = 1 THEN 2 " & _
            '                   "END AS VCStatusId " & _
            '                  ",hrtraining_scheduling.trainingschedulingunkid AS VCId " & _
            '                  ",ISNULL(VoidUser.userunkid,0) AS VId " & _
            '                  ",ISNULL(hrmsConfiguration..cfuser_master.userunkid,0) AS CanId " & _
            '                  ",hrinstitute_master.instituteunkid AS InstId " & _
            '                  ",hrqualification_master.qualificationunkid AS QAId " & _
            '                  ",cfcommon_master.masterunkid AS QGId " & _
            '             "FROM hrtraining_scheduling " & _
            '                  "LEFT JOIN hrmsConfiguration..cfuser_master AS VoidUser ON hrtraining_scheduling.voiduserunkid = VoidUser.userunkid " & _
            '                  "LEFT JOIN hrmsConfiguration..cfuser_master ON hrtraining_scheduling.cancellationuserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
            '                  "JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid " & _
            '                  "JOIN hrqualification_master ON hrtraining_scheduling.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '                  "JOIN cfcommon_master ON hrtraining_scheduling.qualificationgroupunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
            '             "WHERE iscancel = 1 OR isvoid = 1 " & _
            '        ") AS TVC " & _
            '        "WHERE 1 = 1 "

            StrQ = "SELECT " & _
                      " Title AS Title " & _
                      ",QGroup AS QGroup " & _
                      ",QAward AS QAward " & _
                      ",institute_name AS institute_name " & _
                      ",St_date AS St_date " & _
                      ",St_Time AS St_Time " & _
                      ",Ed_date AS Ed_date " & _
                      ",Ed_Time AS Ed_Time " & _
                      ",VCDate AS VCDate " & _
                      ",VCTime AS VCTime " & _
                      ",VCUser AS VCUser " & _
                      ",VCReason AS VCReason " & _
                      ",VCStatus AS VCStatus " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                              "cm.name AS Title " & _
                              ",cfcommon_master.name AS QGroup " & _
                              ",hrqualification_master.qualificationname AS QAward " & _
                              ",hrinstitute_master.institute_name " & _
                              ",CONVERT(CHAR(8),start_date,112) AS St_date " & _
                              ",CONVERT(CHAR(5),start_time,108) AS St_Time " & _
                              ",CONVERT     (CHAR(8),end_date,112) AS Ed_date " & _
                              ",CONVERT(CHAR(5),end_time,108) AS Ed_Time " & _
                              ",CASE WHEN iscancel = 1 THEN CONVERT(CHAR(8),cancellationdate,112) " & _
                                          "WHEN isvoid = 1 THEN CONVERT(CHAR(8),voiddatetime,112) " & _
                               "END AS VCDate " & _
                              ",CASE WHEN iscancel = 1 THEN CONVERT(CHAR(5),cancellationdate,108) " & _
                                          "WHEN isvoid = 1 THEN CONVERT(CHAR(5),voiddatetime,108) " & _
                               "END AS VCTime " & _
                              ",CASE WHEN iscancel = 1 THEN ISNULL(hrmsConfiguration..cfuser_master.username,'') " & _
                                          "WHEN isvoid = 1 THEN ISNULL(VoidUser.username,'') " & _
                               "END AS VCUser " & _
                              ",CASE WHEN iscancel = 1 THEN cancellationreason " & _
                                          "WHEN isvoid = 1 THEN voidreason " & _
                               "END AS VCReason " & _
                              ",CASE WHEN iscancel = 1 THEN @CStatus " & _
                                          "WHEN isvoid = 1 THEN @VStatus " & _
                               "END AS VCStatus " & _
                              ",CASE WHEN iscancel = 1 THEN 1 " & _
                                          "WHEN isvoid = 1 THEN 2 " & _
                               "END AS VCStatusId " & _
                              ",hrtraining_scheduling.trainingschedulingunkid AS VCId " & _
                              ",ISNULL(VoidUser.userunkid,0) AS VId " & _
                              ",ISNULL(hrmsConfiguration..cfuser_master.userunkid,0) AS CanId " & _
                              ",hrinstitute_master.instituteunkid AS InstId " & _
                              ",hrqualification_master.qualificationunkid AS QAId " & _
                              ",cfcommon_master.masterunkid AS QGId " & _
                         "FROM hrtraining_scheduling " & _
                              "LEFT JOIN hrmsConfiguration..cfuser_master AS VoidUser ON hrtraining_scheduling.voiduserunkid = VoidUser.userunkid " & _
                              "LEFT JOIN hrmsConfiguration..cfuser_master ON hrtraining_scheduling.cancellationuserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                              "JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid " & _
                              "JOIN hrqualification_master ON hrtraining_scheduling.qualificationunkid = hrqualification_master.qualificationunkid " & _
                              "JOIN cfcommon_master ON hrtraining_scheduling.qualificationgroupunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                              "JOIN cfcommon_master as cm ON cm.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) " & _
                         "WHERE iscancel = 1 OR isvoid = 1 " & _
                    ") AS TVC " & _
                    "WHERE 1 = 1 "
            'Anjan (10 Feb 2012)-End 



            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("Title")
                rpt_Rows.Item("Column2") = dtRow.Item("QGroup")
                rpt_Rows.Item("Column3") = dtRow.Item("QAward")
                rpt_Rows.Item("Column4") = dtRow.Item("institute_name")
                rpt_Rows.Item("Column5") = eZeeDate.convertDate(dtRow.Item("St_date").ToString).ToShortDateString & vbCrLf & dtRow.Item("St_Time")
                rpt_Rows.Item("Column6") = eZeeDate.convertDate(dtRow.Item("Ed_date").ToString).ToShortDateString & vbCrLf & dtRow.Item("Ed_Time")
                rpt_Rows.Item("Column7") = dtRow.Item("VCUser")
                rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("VCDate").ToString).ToShortDateString & vbCrLf & dtRow.Item("VCTime")
                rpt_Rows.Item("Column9") = dtRow.Item("VCStatus")
                rpt_Rows.Item("Column10") = dtRow.Item("VCReason")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptTrainingVoidCancelReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 23, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 24, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 25, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 26, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtTitle", Language.getMessage(mstrModuleName, 3, "Title"))
            Call ReportFunction.TextChange(objRpt, "txtQGroup", Language.getMessage(mstrModuleName, 4, "Qualification Group"))
            Call ReportFunction.TextChange(objRpt, "txtQualification", Language.getMessage(mstrModuleName, 5, "Qualification"))
            Call ReportFunction.TextChange(objRpt, "txtInstitute", Language.getMessage(mstrModuleName, 6, "Institute"))
            Call ReportFunction.TextChange(objRpt, "txtStDateTime", Language.getMessage(mstrModuleName, 7, "Start Date & Time"))
            Call ReportFunction.TextChange(objRpt, "txtEdDateTime", Language.getMessage(mstrModuleName, 8, "End Date & Time"))
            Call ReportFunction.TextChange(objRpt, "txtVCUser", Language.getMessage(mstrModuleName, 9, "User"))
            Call ReportFunction.TextChange(objRpt, "txtVCDateTime", Language.getMessage(mstrModuleName, 10, "Void/Cancel Date & Time"))
            Call ReportFunction.TextChange(objRpt, "txtVCStatus", Language.getMessage(mstrModuleName, 11, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtVCReason", Language.getMessage(mstrModuleName, 12, "Reason"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 15, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Cancelled")
            Language.setMessage(mstrModuleName, 2, "Voided")
            Language.setMessage(mstrModuleName, 3, "Title")
            Language.setMessage(mstrModuleName, 4, "Qualification Group")
            Language.setMessage(mstrModuleName, 5, "Qualification")
            Language.setMessage(mstrModuleName, 6, "Institute")
            Language.setMessage(mstrModuleName, 7, "Start Date & Time")
            Language.setMessage(mstrModuleName, 8, "End Date & Time")
            Language.setMessage(mstrModuleName, 9, "User")
            Language.setMessage(mstrModuleName, 10, "Void/Cancel Date & Time")
            Language.setMessage(mstrModuleName, 11, "Status")
            Language.setMessage(mstrModuleName, 12, "Reason")
            Language.setMessage(mstrModuleName, 13, "Printed By :")
            Language.setMessage(mstrModuleName, 14, "Printed Date :")
            Language.setMessage(mstrModuleName, 15, "Page :")
            Language.setMessage(mstrModuleName, 16, "Status :")
            Language.setMessage(mstrModuleName, 17, "Void User :")
            Language.setMessage(mstrModuleName, 18, "Cancel User :")
            Language.setMessage(mstrModuleName, 19, "Institute :")
            Language.setMessage(mstrModuleName, 20, "Qualification Group :")
            Language.setMessage(mstrModuleName, 21, "Qualification :")
            Language.setMessage(mstrModuleName, 22, "Select")
            Language.setMessage(mstrModuleName, 23, "Prepared By :")
            Language.setMessage(mstrModuleName, 24, "Checked By :")
            Language.setMessage(mstrModuleName, 25, "Approved By :")
            Language.setMessage(mstrModuleName, 26, "Received By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
End Class
