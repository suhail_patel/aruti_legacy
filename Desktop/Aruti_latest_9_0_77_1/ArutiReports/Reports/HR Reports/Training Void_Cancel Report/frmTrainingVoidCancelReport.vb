'************************************************************************************************************************************
'Class Name : frmTrainingVoidCancelReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTrainingVoidCancelReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTrainingVoidCancelReport"
    Private objTrainingVCReport As clsTrainingVoidCancelReport

#End Region

#Region " Contructor "

    Public Sub New()
        objTrainingVCReport = New clsTrainingVoidCancelReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objTrainingVCReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objCMaster As New clsCommon_Master
        Dim objQMaster As New clsqualification_master
        Dim objIMaster As New clsinstitute_master
        Dim objUMaster As New clsUserAddEdit
        Dim dsCombo As New DataSet
        Try
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "List")
            With cboQGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objQMaster.GetComboList("List", True)
            With cboQualification
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objIMaster.getListForCombo(False, "List", True)
            With cboInstitute
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'dsCombo = objUMaster.getComboList("List", True)

            'Nilay (01-Mar-2016) -- Start
            'dsCombo = objUMaster.getNewComboList("List", , True)
            dsCombo = objUMaster.getNewComboList("List", , True, , , , True)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            With cboCancelUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List").Copy
            End With

            With cboVoidUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List").Copy
            End With

            dsCombo = objTrainingVCReport.Get_Status("List")
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objCMaster = Nothing
            objQMaster = Nothing
            objIMaster = Nothing
            objUMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboCancelUser.SelectedValue = 0
            cboInstitute.SelectedValue = 0
            cboQGroup.SelectedValue = 0
            cboQualification.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboVoidUser.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objTrainingVCReport.SetDefaultValue()

            objTrainingVCReport._CancelUserId = cboCancelUser.SelectedValue
            objTrainingVCReport._CancelUserName = cboCancelUser.Text

            objTrainingVCReport._InstituteId = cboInstitute.SelectedValue
            objTrainingVCReport._InstituteName = cboInstitute.Text

            objTrainingVCReport._QGrpId = cboQGroup.SelectedValue
            objTrainingVCReport._QGrpName = cboQGroup.Text

            objTrainingVCReport._QualifId = cboQualification.SelectedValue
            objTrainingVCReport._QualifName = cboQualification.Text

            objTrainingVCReport._StatusId = cboStatus.SelectedValue
            objTrainingVCReport._StatusName = cboStatus.Text

            objTrainingVCReport._VoidUserId = cboVoidUser.SelectedValue
            objTrainingVCReport._VoidUserName = cboVoidUser.Text

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmTrainingVoidCancelReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTrainingVCReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmTrainingVoidCancelReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingVoidCancelReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objTrainingVCReport._ReportName
            Me._Message = objTrainingVCReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingVoidCancelReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub
            objTrainingVCReport.generateReport(0, e.Type, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub

            objTrainingVCReport.generateReport(0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTrainingVoidCancelReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTrainingVoidCancelReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

#Region " Controls "

    Private Sub cboQGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQGroup.SelectedIndexChanged
        Try
            If cboQGroup.SelectedValue > 0 Then
                Dim objQMaster As New clsqualification_master
                Dim dsCombo As New DataSet

                dsCombo = objQMaster.GetComboList("List", True, cboQGroup.SelectedValue)
                With cboQualification
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombo.Tables("List")
                End With
            Else
                cboQualification.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboQGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            Select Case cboStatus.SelectedIndex
                Case 1  'Cancelled
                    cboVoidUser.SelectedValue = 0
                    cboVoidUser.Enabled = False

                    cboCancelUser.SelectedValue = 0
                    cboCancelUser.Enabled = True
                Case 2  'Void
                    cboVoidUser.SelectedValue = 0
                    cboVoidUser.Enabled = True

                    cboCancelUser.SelectedValue = 0
                    cboCancelUser.Enabled = False
                Case Else
                    cboVoidUser.SelectedValue = 0
                    cboCancelUser.SelectedValue = 0

                    cboVoidUser.Enabled = True
                    cboCancelUser.Enabled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblCancellationUser.Text = Language._Object.getCaption(Me.lblCancellationUser.Name, Me.lblCancellationUser.Text)
			Me.lblVoidUser.Text = Language._Object.getCaption(Me.lblVoidUser.Name, Me.lblVoidUser.Text)
			Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.Name, Me.lblQualification.Text)
			Me.lblInstitute.Text = Language._Object.getCaption(Me.lblInstitute.Name, Me.lblInstitute.Text)
			Me.lblQGroup.Text = Language._Object.getCaption(Me.lblQGroup.Name, Me.lblQGroup.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
