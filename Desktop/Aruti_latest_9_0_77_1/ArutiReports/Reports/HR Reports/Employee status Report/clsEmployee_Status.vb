'************************************************************************************************************************************
'Class Name : clsEmployee_Status.vb
'Purpose    : Requrement of ASP Company
'Date       : 15 JUL 2016
'Written By : Shani Sheladiya
'Modified   :
'Report Create Date  : 1 Dec 2015
'************************************************************************************************************************************

Imports Aruti.Data
Imports eZeeCommonLib

Public Class clsEmployee_Status
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_Status"
    Private mstrReportId As String = enArutiReport.Employee_Status_Report
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "
    Private mintEmployeeId As Integer
    Private mstrEmployeeName As String = String.Empty
    Private mintDepartmentId As Integer
    Private mstrDepartmentName As String = String.Empty
    Private mintJobId As Integer
    Private mstrJobName As String = String.Empty
    Private mintPeriodId As Integer
    Private mstrPeriod As String = String.Empty
    Private mintMembershipId As Integer
    Private mstrMemberShip As String = String.Empty
    Private mintTranHead1 As Integer
    Private mstrTranHead1 As String = String.Empty
    Private mintTranHead2 As Integer
    Private mstrTranHead2 As String = String.Empty
    Private mintTranHead3 As Integer
    Private mstrTranHead3 As String = String.Empty
    Private mblnIncludeInactiveEmp As Boolean = False
    Private mstrAdvance_Filter As String = String.Empty
    Private mblnShowEmpScale As Boolean = False
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mdicMembership As Dictionary(Of Integer, String) = Nothing
    Private mdtPeriodStartDt As Date = Nothing
    Private mdtPeriodEndDt As Date = Nothing
    Private mdtTableExcel As DataTable = Nothing
    'Private mblnExport As Boolean = False
    Private mstrReportTypeName As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentId() As Integer
        Set(ByVal value As Integer)
            mintDepartmentId = value
        End Set
    End Property

    Public WriteOnly Property _Department() As String
        Set(ByVal value As String)
            mstrDepartmentName = value
        End Set
    End Property

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _Job() As String
        Set(ByVal value As String)
            mstrJobName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _Period() As String
        Set(ByVal value As String)
            mstrPeriod = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _Membership() As String
        Set(ByVal value As String)
            mstrMemberShip = value
        End Set
    End Property

    Public WriteOnly Property _TranheadId1() As Integer
        Set(ByVal value As Integer)
            mintTranHead1 = value
        End Set
    End Property

    Public WriteOnly Property _TranHead1() As String
        Set(ByVal value As String)
            mstrTranHead1 = value
        End Set
    End Property

    Public WriteOnly Property _TranheadId2() As Integer
        Set(ByVal value As Integer)
            mintTranHead2 = value
        End Set
    End Property

    Public WriteOnly Property _TranHead2() As String
        Set(ByVal value As String)
            mstrTranHead2 = value
        End Set
    End Property

    Public WriteOnly Property _TranheadId3() As Integer
        Set(ByVal value As Integer)
            mintTranHead3 = value
        End Set
    End Property

    Public WriteOnly Property _TranHead3() As String
        Set(ByVal value As String)
            mstrTranHead3 = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ShowEmployeeScale() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmpScale = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _MembershipDic() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicMembership = value
        End Set
    End Property

    Public WriteOnly Property _Period_Start_Date() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDt = value
        End Set
    End Property

    Public WriteOnly Property _Period_End_Date() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDt = value
        End Set
    End Property

    'Public WriteOnly Property _IsExport() As Boolean
    '    Set(ByVal value As Boolean)
    '        mblnExport = value
    '    End Set
    'End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintDepartmentId = 0
            mstrDepartmentName = ""
            mintJobId = 0
            mstrJobName = ""
            mintPeriodId = 0
            mstrPeriod = ""
            mintMembershipId = 0
            mstrMemberShip = ""
            mintTranHead1 = 0
            mstrTranHead1 = ""
            mintTranHead2 = 0
            mstrTranHead2 = ""
            mintTranHead3 = 0
            mstrTranHead3 = ""
            mstrAdvance_Filter = ""
            mblnShowEmpScale = False
            mintViewIndex = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Period : ") & " " & mstrPeriod & " "
            End If

            If mintMembershipId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Membership : ") & " " & mstrMemberShip & " "
            End If

            If mintTranHead1 > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Transaction Head 1 : ") & " " & mstrTranHead1 & " "
            End If

            If mintTranHead2 > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Transaction Head 2 : ") & " " & mstrTranHead2 & " "
            End If

            If mintTranHead3 > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Transaction Head 3 : ") & " " & mstrTranHead3 & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkId = @employeeunkId "
                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintJobId > 0 Then
                Me._FilterQuery &= " AND hrjob_master.jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Job : ") & " " & mstrJobName & " "
            End If

            If mintDepartmentId > 0 Then
                Me._FilterQuery &= " AND hrdepartment_master.departmentunkid = @departmentunkid"
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Department : ") & " " & mstrDepartmentName & " "
            End If
            If mintPeriodId > 0 AndAlso mdtPeriodStartDt <> Nothing AndAlso mdtPeriodEndDt <> Nothing Then
                objDataOperation.AddParameter("@periodstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDt))
                objDataOperation.AddParameter("@periodenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDt))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Period : ") & " " & mstrPeriod & " "
            End If
            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, _
                                           Optional ByVal ExportAction As enExportAction = enExportAction.None, _
                                           Optional ByVal xBaseCurrencyId As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, pintReportType)

            Dim intArrayColumnWidth As Integer() = Nothing

            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next

            Dim strSubTotal As String = ""
            Dim strarrGroupColumns As String() = Nothing

            If mintViewIndex > 0 Then
                mdtTableExcel.Columns.Remove("ID")
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
                strSubTotal = Language.getMessage(mstrModuleName, 5, "Sub Total :")
            Else
                mdtTableExcel.Columns.Remove("ID")
                mdtTableExcel.Columns.Remove("GName")
            End If

            Call ReportExecute(objRpt, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, , , , strarrGroupColumns, mstrReportTypeName)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 8, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 9, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("Department", Language.getMessage(mstrModuleName, 10, "Department")))
            iColumn_DetailReport.Add(New IColumn("Job", Language.getMessage(mstrModuleName, 11, "Job Title")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intReportType As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet


        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            objDataOperation.ClearParameters()

            StrQ = "SELECT DISTINCT  Employeecode "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS EmpName "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS EmpName "
            End If

            StrQ &= ",ISNULL(DEPT.name, '') 'Department' " & _
                      ",ISNULL(hrjob_master.job_name, '') 'Job' " & _
                      ",ISNULL(CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female END, '') AS 'Gender' " & _
                      ",ISNULL(cfcommon_master.name, '') AS 'Marital Status' " & _
                      ",ISNULL(hremployee_master.birthdate, '') 'DOB' " & _
                      ",ISNULL(hremployee_master.appointeddate, '') 'DOJ' " & _
                      ",ISNULL(hrmsConfiguration..cfcountry_master.country_name, '') AS 'Nationality' " & _
                      ",ISNULL(Emp_bank.Bank_Info,'') AS Bank " & _
                      ",ISNULL(Scale.newscale, 0.00) AS 'Basic Salary' " & _
                      ",ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') AS JoinDate "

            '", CASE WHEN Len(ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname, '')) > 0 THEN " & _
            '"           ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname, '') + '-' " & _
            '" ELSE " & _
            '"           ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname, '')  " & _
            '" End " & _
            '" + ISNULL(hrmsconfiguration..cfbankbranch_master.branchname, '') 'Bank' " & _
            '",ISNULL(premployee_bank_tran.accountno, '') 'AccountNo' " & _

            If intReportType = 2 Then
                StrQ &= ",ISNULL(CONVERT(CHAR(8),EocDt.date2,112),'')  as terminationdate "
            End If

            If mdicMembership IsNot Nothing Then
                For Each dic As Integer In mdicMembership.Keys
                    StrQ &= ",ISNULL([" & mdicMembership(dic).Trim & "].membershipno, '') '" & mdicMembership(dic).Trim & "' "
                Next
            ElseIf mintMembershipId > 0 Then
                StrQ &= ",ISNULL(hremployee_meminfo_tran.membershipno, '') 'membership' "
            Else
                StrQ &= ",'' AS 'membership' "
            End If

            If mintTranHead1 > 0 Then
                StrQ &= ",ISNULL(benfhead1.amount, 0.00) 'Amount1' "
            Else
                StrQ &= ",0.00 AS  'Amount1' "
            End If

            If mintTranHead2 > 0 Then
                StrQ &= ",ISNULL(benfhead2.amount, 0.00) 'Amount2' "
            Else
                StrQ &= ",0.00 AS  'Amount2' "
            End If

            If mintTranHead3 > 0 Then
                StrQ &= ",ISNULL(benfhead3.amount, 0.00) 'Amount3' "
            Else
                StrQ &= ",0.00 AS  'Amount3' "
            End If

            StrQ &= ",ISNULL(CONVERT(CHAR(8), EocDt.date1,112),'') AS 'EOC' "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM hremployee_master "
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            StrQ &= "   JOIN prtnaleave_tran ON hremployee_master.employeeunkid = prtnaleave_tran.employeeunkid " & _
                    "   JOIN prpayrollprocess_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           departmentunkid" & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1  " & _
                    "   LEFT JOIN hrdepartment_master AS DEPT ON DEPT.departmentunkid=Alloc.departmentunkid" & _
                    "   LEFT JOIN " & _
                        "( " & _
                            "SELECT " & _
                                "jobunkid" & _
                                ",employeeunkid " & _
                                ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "FROM hremployee_categorization_tran " & _
                            "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                      " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid" & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.maritalstatusunkid  AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
                      " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.nationalityunkid " & _
                      "LEFT JOIN " & _
                      "( " & _
                      "     SELECT " & _
                      "         employeeunkid, " & _
                      "             STUFF((SELECT " & _
                      "                         '#10;,' + " & _
                      "                         ISNULL(hrmsconfiguration..cfpayrollgroup_master.groupname, '') + " & _
                      "                         CASE WHEN LEN(ISNULL(cfpayrollgroup_master.groupname, '')) > 0 THEN  '-' ELSE '' END " & _
                      "                         + ISNULL(hrmsconfiguration..cfbankbranch_master.branchname, '') " & _
                      "                         + CASE WHEN LEN(ISNULL(cfbankbranch_master.branchname, '')) > 0  THEN  '-' ELSE '' END " & _
                      "                         + ISNULL(premployee_bank_tran.accountno, '') " & _
                      "                     FROM premployee_bank_tran " & _
                      "                         LEFT JOIN hrmsconfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = cfbankbranch_master.branchunkid " & _
                      "                         LEFT JOIN hrmsconfiguration..cfpayrollgroup_master ON cfbankbranch_master.bankgroupunkid = cfpayrollgroup_master.groupmasterunkid AND grouptype_id = " & enPayrollGroupType.Bank & _
                      "                     where premployee_bank_tran.employeeunkid = t1.employeeunkid " & _
                      "             FOR XML PATH('')), " & _
                      "         1,5,'') AS Bank_Info " & _
                      "     FROM premployee_bank_tran t1 " & _
                      "     WHERE 1=1 " & _
                      "     GROUP BY employeeunkid " & _
                      ") AS Emp_bank ON Emp_bank.employeeunkid = hremployee_master.employeeunkid " & _
                      " Left Join " & _
                      "(" & _
                      "     SELECT " & _
                      "          newscale" & _
                      "         ,employeeunkid " & _
                      "         ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS rno " & _
                      "     FROM prsalaryincrement_tran " & _
                      "     WHERE prsalaryincrement_tran.isvoid = 0 " & _
                      "         AND prsalaryincrement_tran.periodunkid IN (" & mintPeriodId & ") AND prsalaryincrement_tran.isapproved = 1 " & _
                      ") AS Scale ON Scale.employeeunkid = hremployee_master.employeeunkid AND Scale.rno = 1" & _
                      "LEFT JOIN " & _
                      "    ( " & _
                      "        SELECT " & _
                      "             date1 " & _
                      "            ,date2 " & _
                      "            ,employeeunkid " & _
                      "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "        FROM hremployee_dates_tran " & _
                      "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                      "    )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 "
            '" LEFT JOIN premployee_bank_tran ON hremployee_master.employeeunkid = premployee_bank_tran.employeeunkid " & _
            '" LEFT JOIN hrmsconfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsconfiguration..cfbankbranch_master.branchunkid " & _
            '" LEFT JOIN hrmsconfiguration..cfpayrollgroup_master ON hrmsconfiguration..cfbankbranch_master.bankgroupunkid = hrmsconfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id = " & enPayrollGroupType.Bank & _
            If mdicMembership IsNot Nothing Then
                For Each dic As Integer In mdicMembership.Keys
                    StrQ &= " LEFT JOIN hremployee_meminfo_tran  AS [" & mdicMembership(dic) & "] ON hremployee_master.employeeunkid = [" & mdicMembership(dic) & "].employeeunkid AND [" & mdicMembership(dic) & "].membershipunkid = " & dic & _
                            " AND [" & mdicMembership(dic) & "].isactive = 1 " & _
                            " AND [" & mdicMembership(dic) & "].isdeleted  = 0 "
                Next
            ElseIf mintMembershipId > 0 Then
                StrQ &= " LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid AND membershipunkid = " & mintMembershipId & _
                " AND hremployee_Meminfo_tran.isactive = 1 " & _
                                     " AND hremployee_meminfo_tran.isdeleted  = 0 "
            End If

            If mintTranHead1 > 0 Then
                StrQ &= " LEFT JOIN " & _
                           " ( " & _
                                  " SELECT ISNULL(amount,0.00) amount,prpayrollprocess_tran.employeeunkid " & _
                                  " FROM prpayrollprocess_tran " & _
                                  " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid AND payperiodunkid IN ( " & mintPeriodId & " ) " & _
                                  " AND prtnaleave_tran.employeeunkid = prpayrollprocess_tran.employeeunkid AND prtnaleave_tran.isvoid = 0 " & _
                                  " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prtranhead_master.tranheadunkid IN (" & mintTranHead1 & ")" & _
                                  " WHERE prpayrollprocess_tran.isvoid = 0 " & _
                             ") AS benfhead1   " & _
                              "ON benfhead1.employeeunkid = hremployee_master.employeeunkid  "
            End If


            If mintTranHead2 > 0 Then
                StrQ &= " LEFT JOIN " & _
                           " ( " & _
                                  " SELECT ISNULL(amount,0.00) amount,prpayrollprocess_tran.employeeunkid " & _
                                  " FROM prpayrollprocess_tran " & _
                                  " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid AND payperiodunkid IN ( " & mintPeriodId & " ) " & _
                                  " AND prtnaleave_tran.employeeunkid = prpayrollprocess_tran.employeeunkid AND prtnaleave_tran.isvoid = 0 " & _
                                  " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prtranhead_master.tranheadunkid IN (" & mintTranHead2 & ")" & _
                                  " WHERE prpayrollprocess_tran.isvoid = 0 " & _
                             ") AS benfhead2   " & _
                              "ON benfhead2.employeeunkid = hremployee_master.employeeunkid  "
            End If

            If mintTranHead3 > 0 Then
                StrQ &= " LEFT JOIN " & _
                           " ( " & _
                                  " SELECT ISNULL(amount,0.00) amount,prpayrollprocess_tran.employeeunkid " & _
                                  " FROM prpayrollprocess_tran " & _
                                  " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid AND payperiodunkid IN ( " & mintPeriodId & " ) " & _
                                  " AND prtnaleave_tran.employeeunkid = prpayrollprocess_tran.employeeunkid AND prtnaleave_tran.isvoid = 0 " & _
                                  " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prtranhead_master.tranheadunkid IN (" & mintTranHead3 & ")" & _
                                  " WHERE prpayrollprocess_tran.isvoid = 0 " & _
                             ") AS benfhead3   " & _
                              "ON benfhead3.employeeunkid = hremployee_master.employeeunkid  "
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE 1 = 1 AND prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 AND prtnaleave_tran.payperiodunkid = '" & mintPeriodId & "' "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If intReportType = 0 Then
                If mblnIncludeInactiveEmp = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If
            ElseIf intReportType = 1 Then
                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) BETWEEN @periodstartdate AND @periodenddate "
            ElseIf intReportType = 2 Then
                StrQ &= " AND CONVERT(CHAR(8),EocDt.date2,112) BETWEEN @periodstartdate AND @periodenddate "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery


            objDataOperation.AddParameter("@Male", SqlDbType.NChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            mdtTableExcel = dsList.Tables(0)
            mdtTableExcel.Columns.Add("SrNo", System.Type.GetType("System.String"))
            For Each dRow As DataRow In mdtTableExcel.Rows
                dRow.Item("SrNo") = (mdtTableExcel.Rows.IndexOf(dRow) + 1).ToString
                If IsDBNull(dRow.Item("JoinDate")) Then
                    dRow.Item("JoinDate") = ""
                Else
                    dRow.Item("JoinDate") = eZeeDate.convertDate(dRow.Item("JoinDate").ToString).ToShortDateString
                End If
                If intReportType = 2 Then
                    If IsDBNull(dRow.Item("terminationdate")) OrElse dRow.Item("terminationdate").ToString.Trim.Length <= 0 Then
                        dRow.Item("terminationdate") = ""
                    Else
                        dRow.Item("terminationdate") = eZeeDate.convertDate(dRow.Item("terminationdate").ToString).ToShortDateString
                    End If
                End If
                dRow.Item("Basic Salary") = Format(CDec(dRow.Item("Basic Salary")), GUI.fmtCurrency)
                dRow.Item("Amount1") = Format(CDec(dRow.Item("Amount1")), GUI.fmtCurrency)
                dRow.Item("Amount2") = Format(CDec(dRow.Item("Amount2")), GUI.fmtCurrency)
                dRow.Item("Amount3") = Format(CDec(dRow.Item("Amount3")), GUI.fmtCurrency)

                If dRow.Item("EOC").ToString.Trim.Length <= 0 OrElse IsDBNull(dRow.Item("EOC")) Then
                    dRow.Item("EOC") = ""
                Else
                    dRow.Item("EOC") = eZeeDate.convertDate(dRow.Item("EOC").ToString).ToShortDateString
                End If
                dRow.AcceptChanges()
            Next

            Dim mintColumn As Integer = 0

            mdtTableExcel.Columns("SrNo").Caption = Language.getMessage(mstrModuleName, 17, "Sr. No.")
            mdtTableExcel.Columns("SrNo").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("Employeecode").Caption = Language.getMessage(mstrModuleName, 18, "EMPNo")
            mdtTableExcel.Columns("Employeecode").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("EmpName").Caption = Language.getMessage(mstrModuleName, 19, "EMP NAME")
            mdtTableExcel.Columns("EmpName").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("Job").Caption = Language.getMessage(mstrModuleName, 20, "DESIGNATION")
            mdtTableExcel.Columns("Job").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("JoinDate").Caption = Language.getMessage(mstrModuleName, 21, "DATE OF JOINING")
            mdtTableExcel.Columns("JoinDate").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("EOC").Caption = Language.getMessage(mstrModuleName, 22, "EOC Date")
            mdtTableExcel.Columns("EOC").SetOrdinal(mintColumn)
            mintColumn += 1

            If intReportType = 2 Then
                mdtTableExcel.Columns("terminationdate").Caption = Language.getMessage(mstrModuleName, 23, "Terminated date")
                mdtTableExcel.Columns("terminationdate").SetOrdinal(mintColumn)
                mintColumn += 1
            End If

            For Each dic As Integer In mdicMembership.Keys
                mdtTableExcel.Columns("" & mdicMembership(dic) & "").Caption = mdicMembership(dic)
                mdtTableExcel.Columns("" & mdicMembership(dic) & "").SetOrdinal(mintColumn)
                mintColumn += 1
            Next

            mdtTableExcel.Columns("Bank").Caption = Language.getMessage(mstrModuleName, 24, "BANK NAME")
            mdtTableExcel.Columns("Bank").SetOrdinal(mintColumn)
            mintColumn += 1

            'mdtTableExcel.Columns("AccountNo").Caption = Language.getMessage(mstrModuleName, 117, "BANK A/C NO")
            'mdtTableExcel.Columns("AccountNo").SetOrdinal(mintColumn)
            'mintColumn += 1

            If mblnShowEmpScale = True Then
                mdtTableExcel.Columns("Basic Salary").Caption = Language.getMessage(mstrModuleName, 25, "Basic Salary")
                mdtTableExcel.Columns("Basic Salary").SetOrdinal(mintColumn)
                mintColumn += 1
            End If

            mdtTableExcel.Columns("Amount1").Caption = mstrTranHead1
            mdtTableExcel.Columns("Amount1").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("Amount2").Caption = mstrTranHead2
            mdtTableExcel.Columns("Amount2").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("Amount3").Caption = mstrTranHead3
            mdtTableExcel.Columns("Amount3").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("Id").Caption = "ID"
            mdtTableExcel.Columns("Id").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtTableExcel.Columns("GName").Caption = "GName"
            mdtTableExcel.Columns("GName").SetOrdinal(mintColumn)
            mintColumn += 1

            For i = mintColumn To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(mintColumn)
            Next

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Job :")
            Language.setMessage(mstrModuleName, 3, "Department :")
            Language.setMessage(mstrModuleName, 4, " Order By :")
            Language.setMessage(mstrModuleName, 5, "Sub Total :")
            Language.setMessage(mstrModuleName, 6, "Male")
            Language.setMessage(mstrModuleName, 7, "Female")
            Language.setMessage(mstrModuleName, 8, "Employee Code")
            Language.setMessage(mstrModuleName, 9, "Employee Name")
            Language.setMessage(mstrModuleName, 10, "Department")
            Language.setMessage(mstrModuleName, 11, "Job Title")
            Language.setMessage(mstrModuleName, 12, "Period :")
            Language.setMessage(mstrModuleName, 13, "Membership :")
            Language.setMessage(mstrModuleName, 14, "Transaction Head 1 :")
            Language.setMessage(mstrModuleName, 15, "Transaction Head 2 :")
            Language.setMessage(mstrModuleName, 16, "Transaction Head 3 :")
            Language.setMessage(mstrModuleName, 17, "Sr. No.")
            Language.setMessage(mstrModuleName, 18, "EMPNo")
            Language.setMessage(mstrModuleName, 19, "EMP NAME")
            Language.setMessage(mstrModuleName, 20, "DESIGNATION")
            Language.setMessage(mstrModuleName, 21, "DATE OF JOINING")
            Language.setMessage(mstrModuleName, 22, "EOC Date")
            Language.setMessage(mstrModuleName, 23, "Terminated date")
            Language.setMessage(mstrModuleName, 24, "BANK NAME")
            Language.setMessage(mstrModuleName, 25, "Basic Salary")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
