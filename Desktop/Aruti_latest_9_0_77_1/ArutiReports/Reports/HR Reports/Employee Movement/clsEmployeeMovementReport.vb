'************************************************************************************************************************************
'Class Name : clsEmployeeMovementReport.vb
'Purpose    :
'Date       :02/04/2011
'Written By : Pinkal Jariwala
'Modified   : Sandeep Sharma { 04 Sep 2015 }
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' Modified By : Sandeep Sharma { 04 Sep 2015 }
''' </summary>

Public Class clsEmployeeMovementReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeMovementReport"
    Private mstrReportId As String = CStr(enArutiReport.Employee_Movement_Report)
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mdblFromScale As Double = 0
    Private mdblToScale As Double = 0
    Private mdtFromdate As Date = Nothing
    Private mdtTodate As Date = Nothing
    Private mblnIsActive As Boolean = True
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty
    Private mblnShowEmpScale As Boolean = False
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintAllocationReasonId As Integer = 0
    Private mstrAllocationReason As String = ""
    Private mstrCurrencyFormat As String = GUI.fmtCurrency

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    Private mblnIsWeb As Boolean = False
    Private mdtTable As DataTable = Nothing
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _FromScale() As Double
        Set(ByVal value As Double)
            mdblFromScale = value
        End Set
    End Property

    Public WriteOnly Property _ToScale() As Double
        Set(ByVal value As Double)
            mdblToScale = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromdate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtTodate = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ShowEmployeeScale() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmpScale = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationReasonId() As Integer
        Set(ByVal value As Integer)
            mintAllocationReasonId = value
        End Set
    End Property

    Public WriteOnly Property _AllocationReason() As String
        Set(ByVal value As String)
            mstrAllocationReason = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyFormat() As String
        Set(ByVal value As String)
            mstrCurrencyFormat = value
        End Set
    End Property

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    Public WriteOnly Property _IsWeb()
        Set(ByVal value)
            mblnIsWeb = value
        End Set
    End Property

    Public ReadOnly Property _dtTable()
        Get
            Return mdtTable
        End Get
    End Property
    'Shani(15-Feb-2016) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mdblFromScale = 0
            mdblToScale = 0
            mdtFromdate = Nothing
            mdtTodate = Nothing
            mblnIsActive = True
            mstrAdvance_Filter = ""
            mblnShowEmpScale = False
            mintReportId = 0
            mstrReportTypeName = ""
            mintAllocationReasonId = 0
            mstrAllocationReason = ""

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mdtFromdate <> Nothing And mdtTodate <> Nothing Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "From Date :") & " " & mdtFromdate.ToShortDateString & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "To Date :") & " " & mdtTodate.ToShortDateString & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mdblFromScale >= 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "From Scale :") & " " & mdblFromScale & " "
            End If

            If mdblToScale > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 34, "To Scale :") & " " & mdblToScale & " "
            End If

            If mintAllocationReasonId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 42, "Allocation Reason :") & " " & mstrAllocationReason & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End

        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            Dim dtExtraExcelTable As DataTable = Nothing

            dtExtraExcelTable = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, pintReportType)


            ''Shani(15-Feb-2016) -- Start
            ''Report not showing Data in ESS due to Access Level Filter
            'If mblnIsWeb = True Then
            '    mdtTable = dtExtraExcelTable
            '    Exit Sub
            'End If
            ''Shani(15-Feb-2016) -- End

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))


            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            If dtExtraExcelTable.Columns.Contains("employeeunkid") Then
                dtExtraExcelTable.Columns.Remove("employeeunkid")
            End If

            Dim strGrpCols As String() = Nothing
            strarrGroupColumns = strGrpCols
            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = dtExtraExcelTable.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 38, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 41, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 40, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            Dim intArrayColumnWidth As Integer() = Nothing

            If pintReportType <= 3 Then
                ReDim intArrayColumnWidth(dtExtraExcelTable.Columns.Count - 1)
                For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    intArrayColumnWidth(i) = 125
                Next
            ElseIf pintReportType = 4 Then
                strGrpCols = New String() {"Employee"}
                strarrGroupColumns = strGrpCols

                ReDim intArrayColumnWidth(dtExtraExcelTable.Columns.Count - 1)
                For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    Select Case i
                        Case 2
                            intArrayColumnWidth(i) = 300
                        Case Else
                            intArrayColumnWidth(i) = 125
                    End Select
                Next
            End If

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, dtExtraExcelTable, intArrayColumnWidth, True, True, False, strarrGroupColumns, mstrReportTypeName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intReportCategoryId As Integer) As DataTable
        Dim dtFinalTable As DataTable = Nothing
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Using objDataOperation As New clsDataOperation

                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
                If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

                '0 ---> EMPLOYEE MOVEMENT ALLOCATION WISE
                '1 ---> EMPLOYEE MOVEMENT RE-CATEGORIZATION WISE
                '2 ---> EMPLOYEE MOVEMENT GRADES WISE
                '3 ---> EMPLOYEE MOVEMENT COSTCENTER WISE
                '4 ---> EMPLOYEE MOVEMENT SUMMARY

                If intReportCategoryId < 4 Then 'EMPLOYEE MOVEMENT SUMMARY
                    StrQ = "SELECT " & _
                           "     hremployee_master.employeeunkid " & _
                           "    ,hremployee_master.employeecode AS [" & IIf(Language.getMessage(mstrModuleName, 200, "Employee Code") = "", "Employee Code", Language.getMessage(mstrModuleName, 200, "Employee Code")) & "]  " & _
                           "    ,hremployee_master.firstname+' '+hremployee_master.surname AS [" & IIf(Language.getMessage(mstrModuleName, 219, "Employee Name") = "", "Employee Name", Language.getMessage(mstrModuleName, 201, "Employee Name")) & "] " & _
                           "    ,EffDate AS [" & IIf(Language.getMessage(mstrModuleName, 220, "Movement Date") = "", "Movement Date", Language.getMessage(mstrModuleName, 220, "Movement Date")) & "] "

                    If mblnShowEmpScale Then
                        StrQ &= "   ,Scale.newscale AS [" & IIf(Language.getMessage(mstrModuleName, 202, "Scale") = "", "Scale", Language.getMessage(mstrModuleName, 202, "Scale")) & "] "
                    End If

                    Select Case intReportCategoryId
                        Case 0  'EMPLOYEE MOVEMENT ALLOCATION WISE
                            StrQ &= "   ,ISNULL(SM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 203, "Branch") = "", "Branch", Language.getMessage(mstrModuleName, 203, "Branch")) & "] " & _
                                    "   ,ISNULL(DGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 204, "Department Group") = "", "Department Group", Language.getMessage(mstrModuleName, 204, "Department Group")) & "] " & _
                                    "   ,ISNULL(DM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 205, "Department") = "", "Department", Language.getMessage(mstrModuleName, 205, "Department")) & "] " & _
                                    "   ,ISNULL(SGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 206, "Section Group") = "", "Section Group", Language.getMessage(mstrModuleName, 206, "Section Group")) & "] " & _
                                    "   ,ISNULL(SECM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 207, "Section") = "", "Section", Language.getMessage(mstrModuleName, 207, "Section")) & "] " & _
                                    "   ,ISNULL(UGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 208, "Unit Group") = "", "Unit Group", Language.getMessage(mstrModuleName, 208, "Unit Group")) & "] " & _
                                    "   ,ISNULL(UM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 209, "Unit") = "", "Unit", Language.getMessage(mstrModuleName, 209, "Unit")) & "] " & _
                                    "   ,ISNULL(TM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 210, "Team") = "", "Team", Language.getMessage(mstrModuleName, 210, "Team")) & "] " & _
                                    "   ,ISNULL(CGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 211, "Class Group") = "", "Class Group", Language.getMessage(mstrModuleName, 211, "Class Group")) & "] " & _
                                    "   ,ISNULL(CM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 212, "Class") = "", "Class", Language.getMessage(mstrModuleName, 212, "Class")) & "] "

                        Case 1  'EMPLOYEE MOVEMENT RE-CATEGORIZATION WISE
                            StrQ &= "   ,ISNULL(JGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 213, "Job Group") = "", "Job Group", Language.getMessage(mstrModuleName, 213, "Job Group")) & "] " & _
                                    "   ,ISNULL(JM.job_name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 214, "Job") = "", "Job", Language.getMessage(mstrModuleName, 214, "Job")) & "] "

                        Case 2  'EMPLOYEE MOVEMENT GRADES WISE
                            StrQ &= "   ,ISNULL(GGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 215, "Grade Group") = "", "Grade Group", Language.getMessage(mstrModuleName, 215, "Grade Group")) & "] " & _
                                    "   ,ISNULL(GM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 216, "Grade") = "", "Grade", Language.getMessage(mstrModuleName, 216, "Grade")) & "] " & _
                                    "   ,ISNULL(GLM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 217, "Grade Level") = "", "Grade Level", Language.getMessage(mstrModuleName, 217, "Grade Level")) & "] "

                        Case 3  'EMPLOYEE MOVEMENT COSTCENTER WISE
                            StrQ &= "   ,ISNULL(CCM.costcentername,'') AS [" & IIf(Language.getMessage(mstrModuleName, 218, "CostCenter") = "", "CostCenter", Language.getMessage(mstrModuleName, 218, "CostCenter")) & "] "

                    End Select

                    StrQ &= "FROM hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        newscale " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS rno " & _
                            "   FROM prsalaryincrement_tran " & _
                            "   WHERE prsalaryincrement_tran.isvoid = 0 " & _
                            "       AND CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) < = '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            "   AND prsalaryincrement_tran.isapproved = 1 "

                    If mintEmployeeId > 0 Then
                        StrQ &= " AND prsalaryincrement_tran.employeeunkid = '" & mintEmployeeId & "' "
                    End If

                    StrQ &= ") AS Scale ON Scale.employeeunkid = hremployee_master.employeeunkid AND Scale.rno = 1 "

                    Select Case intReportCategoryId
                        Case 0  'EMPLOYEE MOVEMENT ALLOCATION WISE
                            StrQ &= "JOIN " & _
                                    "( " & _
                                    "   SELECT " & _
                                    "        stationunkid " & _
                                    "       ,deptgroupunkid " & _
                                    "       ,departmentunkid " & _
                                    "       ,sectiongroupunkid " & _
                                    "       ,sectionunkid " & _
                                    "       ,unitgroupunkid " & _
                                    "       ,unitunkid " & _
                                    "       ,teamunkid " & _
                                    "       ,classgroupunkid " & _
                                    "       ,classunkid " & _
                                    "       ,employeeunkid " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                    "       ,CONVERT(CHAR(8),effectivedate,112) AS EffDate " & _
                                    "   FROM hremployee_transfer_tran " & _
                                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "

                            If mintEmployeeId > 0 Then
                                StrQ &= " AND hremployee_transfer_tran.employeeunkid = '" & mintEmployeeId & "' "
                            End If

                            If mdtFromdate <> Nothing AndAlso mdtTodate <> Nothing Then
                                StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(mdtFromdate) & "' AND '" & eZeeDate.convertDate(mdtTodate) & "' "
                            End If

                            If mintAllocationReasonId > 0 Then
                                StrQ &= " AND hremployee_transfer_tran.changereasonunkid = '" & mintAllocationReasonId & "' "
                            End If

                            StrQ &= ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                                    "LEFT JOIN hrstation_master AS SM ON SM.stationunkid = Alloc.stationunkid " & _
                                    "LEFT JOIN hrdepartment_group_master AS DGM ON Alloc.deptgroupunkid = DGM.deptgroupunkid " & _
                                    "JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                                    "LEFT JOIN hrsectiongroup_master AS SGM ON Alloc.sectiongroupunkid = SGM.sectiongroupunkid " & _
                                    "LEFT JOIN hrsection_master AS SECM ON SECM.sectionunkid  = Alloc.sectionunkid " & _
                                    "LEFT JOIN hrunitgroup_master AS UGM ON Alloc.unitgroupunkid = UGM.unitgroupunkid " & _
                                    "LEFT JOIN hrunit_master AS UM ON UM.unitunkid = Alloc.unitunkid " & _
                                    "LEFT JOIN hrteam_master AS TM ON Alloc.teamunkid = TM.teamunkid " & _
                                    "LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid " & _
                                    "LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid "

                        Case 1  'EMPLOYEE MOVEMENT RE-CATEGORIZATION WISE
                            StrQ &= "JOIN " & _
                                    "( " & _
                                    "   SELECT " & _
                                    "        jobunkid " & _
                                    "       ,jobgroupunkid " & _
                                    "       ,employeeunkid " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                    "       ,CONVERT(CHAR(8),effectivedate,112) AS EffDate " & _
                                    "   FROM hremployee_categorization_tran " & _
                                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "

                            If mintEmployeeId > 0 Then
                                StrQ &= " AND hremployee_categorization_tran.employeeunkid = '" & mintEmployeeId & "' "
                            End If

                            If mdtFromdate <> Nothing AndAlso mdtTodate <> Nothing Then
                                StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(mdtFromdate) & "' AND '" & eZeeDate.convertDate(mdtTodate) & "' "
                            End If

                            If mintAllocationReasonId > 0 Then
                                StrQ &= " AND hremployee_categorization_tran.changereasonunkid = '" & mintAllocationReasonId & "' "
                            End If

                            StrQ &= ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                    "LEFT JOIN hrjobgroup_master AS JGM ON Jobs.jobgroupunkid = JGM.jobgroupunkid " & _
                                    "JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid "

                        Case 2  'EMPLOYEE MOVEMENT GRADES WISE
                            StrQ &= "JOIN " & _
                                    "( " & _
                                    "   SELECT " & _
                                    "        gradegroupunkid " & _
                                    "       ,gradeunkid " & _
                                    "       ,gradelevelunkid " & _
                                    "       ,employeeunkid " & _
                                    "       ,currentscale " & _
                                    "       ,CONVERT(CHAR(8),incrementdate,112) AS EffDate " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                                    "   FROM prsalaryincrement_tran " & _
                                    "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "

                            If mintEmployeeId > 0 Then
                                StrQ &= " AND prsalaryincrement_tran.employeeunkid = '" & mintEmployeeId & "' "
                            End If

                            If mdtFromdate <> Nothing AndAlso mdtTodate <> Nothing Then
                                StrQ &= " AND CONVERT(CHAR(8),incrementdate,112) BETWEEN '" & eZeeDate.convertDate(mdtFromdate) & "' AND '" & eZeeDate.convertDate(mdtTodate) & "' "
                            End If

                            If mintAllocationReasonId > 0 Then
                                StrQ &= " AND prsalaryincrement_tran.reason_id = '" & mintAllocationReasonId & "' "
                            End If

                            StrQ &= ") AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                                    "JOIN hrgradegroup_master AS GGM ON GGM.gradegroupunkid = Grds.gradegroupunkid " & _
                                    "JOIN hrgrade_master AS GM ON Grds.gradeunkid = GM.gradeunkid " & _
                                    "JOIN hrgradelevel_master AS GLM ON Grds.gradelevelunkid = GLM.gradelevelunkid "

                        Case 3  'EMPLOYEE MOVEMENT COSTCENTER WISE
                            StrQ &= "JOIN " & _
                                    "( " & _
                                    "   SELECT " & _
                                    "        cctranheadvalueid " & _
                                    "       ,employeeunkid " & _
                                    "       ,CONVERT(CHAR(8),effectivedate,112) AS EffDate " & _
                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                    "   FROM hremployee_cctranhead_tran " & _
                                    "   WHERE istransactionhead = 0 AND isvoid = 0 " & _
                                    "   AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "

                            If mintEmployeeId > 0 Then
                                StrQ &= " AND hremployee_cctranhead_tran.employeeunkid = '" & mintEmployeeId & "' "
                            End If

                            If mdtFromdate <> Nothing AndAlso mdtTodate <> Nothing Then
                                StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(mdtFromdate) & "' AND '" & eZeeDate.convertDate(mdtTodate) & "' "
                            End If

                            If mintAllocationReasonId > 0 Then
                                StrQ &= " AND hremployee_cctranhead_tran.changereasonunkid = '" & mintAllocationReasonId & "' "
                            End If

                            StrQ &= ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                                    "JOIN prcostcenter_master AS CCM ON CCM.costcenterunkid = CC.cctranheadvalueid "

                    End Select

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END

                    StrQ &= " WHERE 1 = 1 "

                    If mdblFromScale >= 0 Then
                        StrQ &= " AND Scale.newscale >= " & mdblFromScale
                    End If

                    If mdblToScale > 0 Then
                        StrQ &= " AND Scale.newscale <= " & mdblToScale
                    End If

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry & " "
                    End If

                    If mblnIsActive = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQ &= xDateFilterQry & " "
                        End If
                    End If

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If

                    Call FilterTitleAndFilterQuery()

                    dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    For Each dr As DataRow In dsList.Tables(0).Rows
                        If dr(Language.getMessage(mstrModuleName, 220, "Movement Date")).ToString.Trim.Length > 0 Then

                            'Shani(15-Feb-2016) -- Start
                            'Report not showing Data in ESS due to Access Level Filter
                            'dr(Language.getMessage(mstrModuleName, 220, "Movement Date")) = eZeeDate.convertDate(dr(Language.getMessage(mstrModuleName, 220, "Movement Date")).ToShortDateString())
                            dr(Language.getMessage(mstrModuleName, 220, "Movement Date")) = eZeeDate.convertDate(dr(Language.getMessage(mstrModuleName, 220, "Movement Date"))).ToShortDateString()
                            'Shani(15-Feb-2016) -- End

                        End If
                        If mblnShowEmpScale Then
                            dr(Language.getMessage(mstrModuleName, 202, "Scale")) = Format(CDec(dr(Language.getMessage(mstrModuleName, 202, "Scale"))), mstrCurrencyFormat)
                        End If
                    Next

                    dsList.Tables(0).Columns.Remove("employeeunkid")

                    dtFinalTable = dsList.Tables(0)

                ElseIf mintReportId = 4 Then

                    StrQ = "SELECT " & _
                           "     eDate AS [" & IIf(Language.getMessage(mstrModuleName, 220, "Movement Date") = "", "Movement Date", Language.getMessage(mstrModuleName, 220, "Movement Date")) & "] " & _
                           "    ,employeecode + ' - ' + ISNULL(hremployee_master.surname,'') + ' ' + ISNULL(hremployee_master.firstname,'') AS [" & IIf(Language.getMessage(mstrModuleName, 222, "Employee") = "", "Employee", Language.getMessage(mstrModuleName, 222, "Employee")) & "] " & _
                           "    ,Particulars AS [" & IIf(Language.getMessage(mstrModuleName, 221, "Particulars") = "", "Particulars", Language.getMessage(mstrModuleName, 221, "Particulars")) & "] " & _
                           "    ,Scale.newscale AS [" & IIf(Language.getMessage(mstrModuleName, 202, "Scale") = "", "Scale", Language.getMessage(mstrModuleName, 202, "Scale")) & "] " & _
                           "    ,ChangeType AS [" & IIf(Language.getMessage(mstrModuleName, 223, "ChangeType") = "", "ChangeType", Language.getMessage(mstrModuleName, 223, "ChangeType")) & "] " & _
                           "    ,Reason AS [" & IIf(Language.getMessage(mstrModuleName, 224, "Reason") = "", "Reason", Language.getMessage(mstrModuleName, 224, "Reason")) & "] " & _
                           "    ,ChangeId " & _
                           "FROM hremployee_master "
                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    StrQ &= "LEFT JOIN " & _
                           "( " & _
                           "    SELECT " & _
                           "         newscale " & _
                           "        ,employeeunkid " & _
                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS rno " & _
                           "    FROM prsalaryincrement_tran " & _
                           "    WHERE prsalaryincrement_tran.isvoid = 0 " & _
                           "    AND CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) < = '" & eZeeDate.convertDate(dtPeriodEnd) & "' AND prsalaryincrement_tran.isapproved = 1 "

                    If mintEmployeeId > 0 Then
                        StrQ &= " AND prsalaryincrement_tran.employeeunkid = '" & mintEmployeeId & "' "
                    End If

                    StrQ &= ") AS Scale ON Scale.employeeunkid = hremployee_master.employeeunkid AND Scale.rno = 1 " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         Fin.eDate " & _
                            "        ,Fin.Particulars " & _
                            "        ,Fin.ChangeType " & _
                            "        ,Fin.employeeunkid AS GrpId " & _
                            "        ,Fin.ChangeId " & _
                            "        ,Fin.Reason " & _
                            "    FROM " & _
                            "    ( " & _
                            "        SELECT " & _
                            "             AEffDate AS eDate " & _
                            "            ,@SM+': ' + ISNULL(SM.name,' - ')+ '#10;'+ @DGM+': ' + ISNULL(DGM.name,' - ')+ '#10;'+ @DM+': ' + ISNULL(DM.name,' - ')+ '#10;'+ @SGM+': ' + ISNULL(SGM.name,' - ')+ '#10;'+ " & _
                            "             @SECM+': ' + ISNULL(SECM.name,' - ')+ '#10;'+ @UGM+': ' + ISNULL(UGM.name,' - ')+ '#10;'+ @UM+': ' + ISNULL(UM.name,' - ')+ '#10;'+ @TM+': ' + ISNULL(TM.name,' - ')+ '#10;'+ " & _
                            "             @CGM+': ' + ISNULL(CGM.name,' - ')+ '#10;'+ @CM+': ' + ISNULL(CM.name,' - ') AS Particulars " & _
                            "            ,@ALM AS ChangeType " & _
                            "            ,employeeunkid " & _
                            "            ,1 AS ChangeId " & _
                            "            ,AR.name AS Reason " & _
                            "        FROM " & _
                            "        ( " & _
                            "            SELECT " & _
                            "                 stationunkid " & _
                            "                ,deptgroupunkid " & _
                            "                ,departmentunkid " & _
                            "                ,sectiongroupunkid " & _
                            "                ,sectionunkid " & _
                            "                ,unitgroupunkid " & _
                            "                ,unitunkid " & _
                            "                ,teamunkid " & _
                            "                ,classgroupunkid " & _
                            "                ,classunkid " & _
                            "                ,employeeunkid " & _
                            "                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "                ,CONVERT(CHAR(8),effectivedate,112) AS AEffDate " & _
                            "                ,changereasonunkid " & _
                            "            FROM hremployee_transfer_tran " & _
                            "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "

                    If mintEmployeeId > 0 Then
                        StrQ &= " AND hremployee_transfer_tran.employeeunkid = '" & mintEmployeeId & "' "
                    End If

                    If mdtFromdate <> Nothing AndAlso mdtTodate <> Nothing Then
                        StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(mdtFromdate) & "' AND '" & eZeeDate.convertDate(mdtTodate) & "' "
                    End If

                    If mintAllocationReasonId > 0 Then
                        StrQ &= " AND hremployee_transfer_tran.changereasonunkid = '" & mintAllocationReasonId & "' "
                    End If

                    StrQ &= "        ) AS Alloc " & _
                            "        LEFT JOIN cfcommon_master AS AR ON AR.masterunkid = Alloc.changereasonunkid " & _
                            "        LEFT JOIN hrstation_master AS SM ON SM.stationunkid = Alloc.stationunkid " & _
                            "        LEFT JOIN hrdepartment_group_master AS DGM ON Alloc.deptgroupunkid = DGM.deptgroupunkid " & _
                            "        JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                            "        LEFT JOIN hrsectiongroup_master AS SGM ON Alloc.sectiongroupunkid = SGM.sectiongroupunkid " & _
                            "        LEFT JOIN hrsection_master AS SECM ON SECM.sectionunkid  = Alloc.sectionunkid " & _
                            "        LEFT JOIN hrunitgroup_master AS UGM ON Alloc.unitgroupunkid = UGM.unitgroupunkid " & _
                            "        LEFT JOIN hrunit_master AS UM ON UM.unitunkid = Alloc.unitunkid " & _
                            "        LEFT JOIN hrteam_master AS TM ON Alloc.teamunkid = TM.teamunkid " & _
                            "        LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid " & _
                            "        LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                            "        WHERE Alloc.rno = 1 " & _
                            "    UNION ALL " & _
                            "        SELECT " & _
                            "             JEffDate AS eDate " & _
                            "            ,@JGM+': '+ ISNULL(JGM.name,' - ')+ '#10;' + @JB+': ' + ISNULL(JM.job_name,' - ') " & _
                            "            ,@CRM AS ChangeType " & _
                            "            ,employeeunkid " & _
                            "            ,2 AS ChangeId " & _
                            "            ,JR.name AS Reason " & _
                            "        FROM " & _
                            "        ( " & _
                            "            SELECT " & _
                            "                 jobunkid " & _
                            "                ,jobgroupunkid " & _
                            "                ,employeeunkid " & _
                            "                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "                ,CONVERT(CHAR(8),effectivedate,112) AS JEffDate " & _
                            "                ,changereasonunkid " & _
                            "            FROM hremployee_categorization_tran " & _
                            "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "

                    If mintEmployeeId > 0 Then
                        StrQ &= " AND hremployee_categorization_tran.employeeunkid = '" & mintEmployeeId & "' "
                    End If

                    If mdtFromdate <> Nothing AndAlso mdtTodate <> Nothing Then
                        StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(mdtFromdate) & "' AND '" & eZeeDate.convertDate(mdtTodate) & "' "
                    End If

                    If mintAllocationReasonId > 0 Then
                        StrQ &= " AND hremployee_categorization_tran.changereasonunkid = '" & mintAllocationReasonId & "' "
                    End If

                    StrQ &= "        ) AS Jobs " & _
                            "        LEFT JOIN cfcommon_master AS JR ON JR.masterunkid = Jobs.changereasonunkid " & _
                            "        LEFT JOIN hrjobgroup_master AS JGM ON Jobs.jobgroupunkid = JGM.jobgroupunkid " & _
                            "        JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid " & _
                            "        WHERE Jobs.rno = 1 " & _
                            "    UNION ALL " & _
                            "        SELECT " & _
                            "             GEffDate " & _
                            "            ,@GG+': '+ ISNULL(GGM.name,' - ') + '#10;' + @GR+': '+ ISNULL(GM.name,' - ') + '#10;' + @GL+': '+ ISNULL(GLM.name,' - ') " & _
                            "            ,@SLM AS ChangeType " & _
                            "            ,Grds.employeeunkid " & _
                            "            ,3 AS ChangeId " & _
                            "            ,PR.name AS Reason " & _
                            "        FROM " & _
                            "        ( " & _
                            "            SELECT " & _
                            "                 gradegroupunkid " & _
                            "                ,gradeunkid " & _
                            "                ,gradelevelunkid " & _
                            "                ,employeeunkid " & _
                            "                ,CONVERT(CHAR(8),incrementdate,112) AS GEffDate " & _
                            "                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                            "                ,reason_id " & _
                            "            FROM prsalaryincrement_tran " & _
                            "            WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "

                    If mintEmployeeId > 0 Then
                        StrQ &= " AND prsalaryincrement_tran.employeeunkid = '" & mintEmployeeId & "' "
                    End If

                    If mdtFromdate <> Nothing AndAlso mdtTodate <> Nothing Then
                        StrQ &= " AND CONVERT(CHAR(8),incrementdate,112) BETWEEN '" & eZeeDate.convertDate(mdtFromdate) & "' AND '" & eZeeDate.convertDate(mdtTodate) & "' "
                    End If

                    If mintAllocationReasonId > 0 Then
                        StrQ &= " AND prsalaryincrement_tran.reason_id = '" & mintAllocationReasonId & "' "
                    End If

                    StrQ &= "        ) AS Grds " & _
                            "        LEFT JOIN cfcommon_master AS PR ON PR.masterunkid = Grds.reason_id " & _
                            "        JOIN hrgradegroup_master AS GGM ON GGM.gradegroupunkid = Grds.gradegroupunkid " & _
                            "        JOIN hrgrade_master AS GM ON Grds.gradeunkid = GM.gradeunkid " & _
                            "        JOIN hrgradelevel_master AS GLM ON Grds.gradelevelunkid = GLM.gradelevelunkid " & _
                            "        WHERE Grds.rno = 1 " & _
                            "    UNION ALL " & _
                            "        SELECT " & _
                            "             CEffDate " & _
                            "            ,@CC+': '+ ISNULL(CCM.costcentername,'') " & _
                            "            ,@CCM AS ChangeType " & _
                            "            ,employeeunkid " & _
                            "            ,4 AS ChangeId " & _
                            "            ,CCR.name AS Reason " & _
                            "        FROM " & _
                            "        ( " & _
                            "            SELECT " & _
                            "                 cctranheadvalueid " & _
                            "                ,employeeunkid " & _
                            "                ,CONVERT(CHAR(8),effectivedate,112) AS CEffDate " & _
                            "                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "                ,changereasonunkid " & _
                            "            FROM hremployee_cctranhead_tran " & _
                            "            WHERE istransactionhead = 0 AND isvoid = 0 " & _
                            "            AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "

                    If mintEmployeeId > 0 Then
                        StrQ &= " AND hremployee_cctranhead_tran.employeeunkid = '" & mintEmployeeId & "' "
                    End If

                    If mdtFromdate <> Nothing AndAlso mdtTodate <> Nothing Then
                        StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(mdtFromdate) & "' AND '" & eZeeDate.convertDate(mdtTodate) & "' "
                    End If

                    If mintAllocationReasonId > 0 Then
                        StrQ &= " AND hremployee_cctranhead_tran.changereasonunkid = '" & mintAllocationReasonId & "' "
                    End If

                    StrQ &= "        ) AS CC " & _
                            "        LEFT JOIN cfcommon_master AS CCR ON CCR.masterunkid = CC.changereasonunkid " & _
                            "        JOIN prcostcenter_master AS CCM ON CCM.costcenterunkid = CC.cctranheadvalueid " & _
                            "        WHERE CC.rno = 1 " & _
                            "    ) AS Fin " & _
                            ")Movement ON hremployee_master.employeeunkid = Movement.GrpId "

                    'S.SANDEEP |28-OCT-2021| -- START
                    'ISSUE : REPORT OPTIMZATION
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    'S.SANDEEP |28-OCT-2021| -- END
                    

                    StrQ &= "WHERE 1 = 1 "

                    If mdblFromScale >= 0 Then
                        StrQ &= " AND Scale.newscale >= " & mdblFromScale
                    End If

                    If mdblToScale > 0 Then
                        StrQ &= " AND Scale.newscale <= " & mdblToScale
                    End If

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry & " "
                    End If

                    If mblnIsActive = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQ &= xDateFilterQry & " "
                        End If
                    End If

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If

                    Call FilterTitleAndFilterQuery()

                    StrQ &= " ORDER BY ISNULL(hremployee_master.surname,'') + ' ' + ISNULL(hremployee_master.firstname,''),Movement.ChangeId "

                    objDataOperation.AddParameter("@CGM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 211, "Class Group"))
                    objDataOperation.AddParameter("@CM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 212, "Class"))
                    objDataOperation.AddParameter("@DGM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 204, "Department Group"))
                    objDataOperation.AddParameter("@DM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 205, "Department"))
                    objDataOperation.AddParameter("@SECM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 207, "Section"))
                    objDataOperation.AddParameter("@SGM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 206, "Section Group"))
                    objDataOperation.AddParameter("@SM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 203, "Branch"))
                    objDataOperation.AddParameter("@TM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 210, "Team"))
                    objDataOperation.AddParameter("@UGM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 208, "Unit Group"))
                    objDataOperation.AddParameter("@UM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 209, "Unit"))
                    objDataOperation.AddParameter("@JB", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 214, "Job"))
                    objDataOperation.AddParameter("@JGM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 213, "Job Group"))
                    objDataOperation.AddParameter("@GG", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 215, "Grade Group"))
                    objDataOperation.AddParameter("@GR", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 216, "Grade"))
                    objDataOperation.AddParameter("@GL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 217, "Grade Level"))
                    objDataOperation.AddParameter("@CC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 218, "CostCenter"))

                    objDataOperation.AddParameter("@ALM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 116, "Allocation Movement"))
                    objDataOperation.AddParameter("@SLM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 117, "Grade Change"))
                    objDataOperation.AddParameter("@CRM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 118, "Recategorization"))
                    objDataOperation.AddParameter("@CCM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 119, "Costcenter Movement"))

                    dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    For Each dr As DataRow In dsList.Tables(0).Rows
                        If dr(Language.getMessage(mstrModuleName, 220, "Movement Date")).ToString.Trim.Length > 0 Then

                            'Shani(15-Feb-2016) -- Start
                            'Report not showing Data in ESS due to Access Level Filter
                            'dr(Language.getMessage(mstrModuleName, 220, "Movement Date")) = eZeeDate.convertDate(Language.getMessage(mstrModuleName, 220, "Movement Date")).ToShortDateString()
                            dr(Language.getMessage(mstrModuleName, 220, "Movement Date")) = eZeeDate.convertDate(dr(Language.getMessage(mstrModuleName, 220, "Movement Date")))
                            'Shani(15-Feb-2016) -- End
                        End If
                        dr(Language.getMessage(mstrModuleName, 202, "Scale")) = Format(CDec(dr(Language.getMessage(mstrModuleName, 202, "Scale"))), mstrCurrencyFormat)

                        If mblnShowEmpScale Then
                            dr(Language.getMessage(mstrModuleName, 222, "Employee")) = dr(Language.getMessage(mstrModuleName, 222, "Employee")) & ", " & _
                               Language.getMessage(mstrModuleName, 120, "Employee Scale") & ": " & dr(Language.getMessage(mstrModuleName, 202, "Scale")).ToString
                        End If
                    Next

                    dsList.Tables(0).Columns.Remove("Scale")
                    dsList.Tables(0).Columns.Remove("ChangeId")

                    dtFinalTable = dsList.Tables(0)

                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtFinalTable
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 21, "From Date :")
            Language.setMessage(mstrModuleName, 22, "To Date :")
            Language.setMessage(mstrModuleName, 23, "Employee :")
            Language.setMessage(mstrModuleName, 33, "From Scale :")
            Language.setMessage(mstrModuleName, 34, "To Scale :")
            Language.setMessage(mstrModuleName, 37, "Prepared By :")
            Language.setMessage(mstrModuleName, 38, "Checked By :")
            Language.setMessage(mstrModuleName, 40, "Received By :")
            Language.setMessage(mstrModuleName, 41, "Approved By")
            Language.setMessage(mstrModuleName, 42, "Allocation Reason :")
            Language.setMessage(mstrModuleName, 116, "Allocation Movement")
            Language.setMessage(mstrModuleName, 117, "Grade Change")
            Language.setMessage(mstrModuleName, 118, "Recategorization")
            Language.setMessage(mstrModuleName, 119, "Costcenter Movement")
            Language.setMessage(mstrModuleName, 120, "Employee Scale")
            Language.setMessage(mstrModuleName, 200, "Employee Code")
            Language.setMessage(mstrModuleName, 201, "Employee Name")
            Language.setMessage(mstrModuleName, 202, "Scale")
            Language.setMessage(mstrModuleName, 203, "Branch")
            Language.setMessage(mstrModuleName, 204, "Department Group")
            Language.setMessage(mstrModuleName, 205, "Department")
            Language.setMessage(mstrModuleName, 206, "Section Group")
            Language.setMessage(mstrModuleName, 207, "Section")
            Language.setMessage(mstrModuleName, 208, "Unit Group")
            Language.setMessage(mstrModuleName, 209, "Unit")
            Language.setMessage(mstrModuleName, 210, "Team")
            Language.setMessage(mstrModuleName, 211, "Class Group")
            Language.setMessage(mstrModuleName, 212, "Class")
            Language.setMessage(mstrModuleName, 213, "Job Group")
            Language.setMessage(mstrModuleName, 214, "Job")
            Language.setMessage(mstrModuleName, 215, "Grade Group")
            Language.setMessage(mstrModuleName, 216, "Grade")
            Language.setMessage(mstrModuleName, 217, "Grade Level")
            Language.setMessage(mstrModuleName, 218, "CostCenter")
            Language.setMessage(mstrModuleName, 219, "Employee Name")
            Language.setMessage(mstrModuleName, 220, "Movement Date")
            Language.setMessage(mstrModuleName, 221, "Particulars")
            Language.setMessage(mstrModuleName, 222, "Employee")
            Language.setMessage(mstrModuleName, 223, "ChangeType")
            Language.setMessage(mstrModuleName, 224, "Reason")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class clsEmployeeMovementReport1
'    Inherits IReportData
'    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeMovementReport"
'    Private mstrReportId As String = CStr(enArutiReport.Employee_Movement_Report)
'    Dim objDataOperation As clsDataOperation

'#Region " Constructor "

'    Public Sub New()
'        Me.setReportData(CInt(mstrReportID),intLangId,intCompanyId)
'        Call Create_OnDetailReport()
'    End Sub

'#End Region

'#Region " Private Variables "

'    Private mintEmployeeId As Integer = -1
'    Private mstrEmployeeName As String = String.Empty
'    Private mintCostCenterId As Integer = -1
'    Private mstrCostCenterName As String = String.Empty
'    Private mintBranchId As Integer = -1
'    Private mstrBranchName As String = String.Empty
'    Private mintDepartmentId As Integer = -1
'    Private mstrDepartmentName As String = String.Empty
'    Private mintSectionId As Integer = -1
'    Private mstrSectionName As String = String.Empty
'    Private mintUnitId As Integer = -1
'    Private mstrUnitName As String = String.Empty
'    Private mintJobId As Integer = -1
'    Private mstrJobName As String = String.Empty
'    Private mintGradeGroupId As Integer = -1
'    Private mstrGradeGroupName As String = String.Empty
'    Private mintGradeId As Integer = -1
'    Private mstrGradeName As String = String.Empty
'    Private mintGradeLevelId As Integer = -1
'    Private mstrGradeLevelName As String = String.Empty
'    Private mdblFromScale As Double = 0
'    Private mdblToScale As Double = 0
'    Private mdtFromdate As Date = Nothing
'    Private mdtTodate As Date = Nothing


'    'Pinkal (24-Jun-2011) -- Start
'    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'    Private mblnIsActive As Boolean = True
'    'Pinkal (24-Jun-2011) -- End


'    'Pinkal (14-Aug-2012) -- Start
'    'Enhancement : TRA Changes
'    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
'    Private mintUserUnkid As Integer = -1
'    Private mintCompanyUnkid As Integer = -1
'    'Pinkal (14-Aug-2012) -- End

'    'S.SANDEEP [ 12 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mstrUserAccessFilter As String = String.Empty
'    'S.SANDEEP [ 12 NOV 2012 ] -- END

'    'S.SANDEEP [ 13 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mstrAdvance_Filter As String = String.Empty
'    'S.SANDEEP [ 13 FEB 2013 ] -- END


'    'Pinkal (24-Apr-2013) -- Start
'    'Enhancement : TRA Changes
'    Private mblnShowEmpScale As Boolean = False
'    'Pinkal (24-Apr-2013) -- End

'    'S.SANDEEP [ 26 MAR 2014 ] -- START
'    'ENHANCEMENT : Requested by Rutta
'    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
'    'S.SANDEEP [ 26 MAR 2014 ] -- END


'    'Pinkal (03-Nov-2014) -- Start
'    'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
'    Private mintReportId As Integer = 0
'    Private mstrReportTypeName As String = ""
'    Private mintAllocationReasonId As Integer = 0
'    Private mstrAllocationReason As String = ""
'    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
'    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
'    'Pinkal (03-Nov-2014) -- End


'#End Region

'#Region " Properties "

'    Public WriteOnly Property _EmployeeId() As Integer
'        Set(ByVal value As Integer)
'            mintEmployeeId = value
'        End Set
'    End Property

'    Public WriteOnly Property _EmployeeName() As String
'        Set(ByVal value As String)
'            mstrEmployeeName = value
'        End Set
'    End Property

'    Public WriteOnly Property _CostCenterId() As Integer
'        Set(ByVal value As Integer)
'            mintCostCenterId = value
'        End Set
'    End Property

'    Public WriteOnly Property _CostCenterName() As String
'        Set(ByVal value As String)
'            mstrCostCenterName = value
'        End Set
'    End Property

'    Public WriteOnly Property _BranchId() As Integer
'        Set(ByVal value As Integer)
'            mintBranchId = value
'        End Set
'    End Property

'    Public WriteOnly Property _BranchName() As String
'        Set(ByVal value As String)
'            mstrBranchName = value
'        End Set
'    End Property

'    Public WriteOnly Property _DepartmentId() As Integer
'        Set(ByVal value As Integer)
'            mintDepartmentId = value
'        End Set
'    End Property

'    Public WriteOnly Property _DepartmentName() As String
'        Set(ByVal value As String)
'            mstrDepartmentName = value
'        End Set
'    End Property

'    Public WriteOnly Property _SectionId() As Integer
'        Set(ByVal value As Integer)
'            mintSectionId = value
'        End Set
'    End Property

'    Public WriteOnly Property _SectionName() As String
'        Set(ByVal value As String)
'            mstrSectionName = value
'        End Set
'    End Property

'    Public WriteOnly Property _UnitId() As Integer
'        Set(ByVal value As Integer)
'            mintUnitId = value
'        End Set
'    End Property

'    Public WriteOnly Property _UnitName() As String
'        Set(ByVal value As String)
'            mstrUnitName = value
'        End Set
'    End Property

'    Public WriteOnly Property _JobId() As Integer
'        Set(ByVal value As Integer)
'            mintJobId = value
'        End Set
'    End Property

'    Public WriteOnly Property _JobName() As String
'        Set(ByVal value As String)
'            mstrJobName = value
'        End Set
'    End Property

'    Public WriteOnly Property _GradeGroupId() As Integer
'        Set(ByVal value As Integer)
'            mintGradeGroupId = value
'        End Set
'    End Property

'    Public WriteOnly Property _GradeGroupName() As String
'        Set(ByVal value As String)
'            mstrGradeGroupName = value
'        End Set
'    End Property

'    Public WriteOnly Property _GradeId() As Integer
'        Set(ByVal value As Integer)
'            mintGradeId = value
'        End Set
'    End Property

'    Public WriteOnly Property _GradeName() As String
'        Set(ByVal value As String)
'            mstrGradeName = value
'        End Set
'    End Property

'    Public WriteOnly Property _GradeLevelId() As Integer
'        Set(ByVal value As Integer)
'            mintGradeLevelId = value
'        End Set
'    End Property

'    Public WriteOnly Property _GradeLevelName() As String
'        Set(ByVal value As String)
'            mstrGradeLevelName = value
'        End Set
'    End Property

'    Public WriteOnly Property _FromScale() As Double
'        Set(ByVal value As Double)
'            mdblFromScale = value
'        End Set
'    End Property

'    Public WriteOnly Property _ToScale() As Double
'        Set(ByVal value As Double)
'            mdblToScale = value
'        End Set
'    End Property

'    Public WriteOnly Property _FromDate() As Date
'        Set(ByVal value As Date)
'            mdtFromdate = value
'        End Set
'    End Property

'    Public WriteOnly Property _ToDate() As Date
'        Set(ByVal value As Date)
'            mdtTodate = value
'        End Set
'    End Property

'    'Pinkal (24-Jun-2011) -- Start
'    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'    Public WriteOnly Property _IsActive() As Boolean
'        Set(ByVal value As Boolean)
'            mblnIsActive = value
'        End Set
'    End Property
'    'Pinkal (24-Jun-2011) -- End

'    'Pinkal (14-Aug-2012) -- Start
'    'Enhancement : TRA Changes

'    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
'        Get
'            Return Rpt
'        End Get
'    End Property

'    Public WriteOnly Property _CompanyUnkId() As Integer
'        Set(ByVal value As Integer)
'            mintCompanyUnkid = value
'        End Set
'    End Property

'    Public WriteOnly Property _UserUnkId() As Integer
'        Set(ByVal value As Integer)
'            mintUserUnkid = value
'        End Set
'    End Property

'    'Pinkal (14-Aug-2012) -- End

'    'S.SANDEEP [ 12 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public WriteOnly Property _UserAccessFilter() As String
'        Set(ByVal value As String)
'            mstrUserAccessFilter = value
'        End Set
'    End Property
'    'S.SANDEEP [ 12 NOV 2012 ] -- END

'    'S.SANDEEP [ 13 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public WriteOnly Property _Advance_Filter() As String
'        Set(ByVal value As String)
'            mstrAdvance_Filter = value
'        End Set
'    End Property
'    'S.SANDEEP [ 13 FEB 2013 ] -- END


'    'Pinkal (24-Apr-2013) -- Start
'    'Enhancement : TRA Changes

'    Public WriteOnly Property _ShowEmployeeScale() As Boolean
'        Set(ByVal value As Boolean)
'            mblnShowEmpScale = value
'        End Set
'    End Property

'    'Pinkal (24-Apr-2013) -- End

'    'S.SANDEEP [ 26 MAR 2014 ] -- START
'    Public WriteOnly Property _FirstNamethenSurname() As Boolean
'        Set(ByVal value As Boolean)
'            mblnFirstNamethenSurname = value
'        End Set
'    End Property
'    'S.SANDEEP [ 26 MAR 2014 ] -- END


'    'Pinkal (03-Nov-2014) -- Start
'    'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

'    Public WriteOnly Property _ReportId() As Integer
'        Set(ByVal value As Integer)
'            mintReportId = value
'        End Set
'    End Property

'    Public WriteOnly Property _ReportTypeName() As String
'        Set(ByVal value As String)
'            mstrReportTypeName = value
'        End Set
'    End Property

'    Public WriteOnly Property _AllocationReasonId() As Integer
'        Set(ByVal value As Integer)
'            mintAllocationReasonId = value
'        End Set
'    End Property

'    Public WriteOnly Property _AllocationReason() As String
'        Set(ByVal value As String)
'            mstrAllocationReason = value
'        End Set
'    End Property

'    Public WriteOnly Property _ExportReportPath() As String
'        Set(ByVal value As String)
'            mstrExportReportPath = value
'        End Set
'    End Property

'    Public WriteOnly Property _OpenAfterExport() As Boolean
'        Set(ByVal value As Boolean)
'            mblnOpenAfterExport = value
'        End Set
'    End Property

'    'Pinkal (03-Nov-2014) -- End


'#End Region

'#Region "Public Function & Procedures "

'    Public Sub SetDefaultValue()
'        Try
'            mintEmployeeId = 0
'            mstrEmployeeName = ""
'            mintCostCenterId = 0
'            mstrCostCenterName = ""
'            mintBranchId = 0
'            mstrBranchName = ""
'            mintDepartmentId = 0
'            mstrDepartmentName = ""
'            mintSectionId = 0
'            mstrSectionName = ""
'            mintUnitId = 0
'            mstrUnitName = ""
'            mintJobId = 0
'            mstrJobName = ""
'            mintGradeGroupId = 0
'            mstrGradeGroupName = ""
'            mintGradeId = 0
'            mstrGradeName = ""
'            mintGradeLevelId = 0
'            mstrGradeLevelName = ""
'            mdblFromScale = 0
'            mdblToScale = 0
'            mdtFromdate = Nothing
'            mdtTodate = Nothing


'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'            mblnIsActive = True
'            'Pinkal (24-Jun-2011) -- End

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            mstrAdvance_Filter = ""
'            'S.SANDEEP [ 13 FEB 2013 ] -- END


'            'Pinkal (24-Apr-2013) -- Start
'            'Enhancement : TRA Changes
'            mblnShowEmpScale = False
'            'Pinkal (24-Apr-2013) -- End


'            'Pinkal (03-Nov-2014) -- Start
'            'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
'            mintReportId = 0
'            mstrReportTypeName = ""
'            mintAllocationReasonId = 0
'            mstrAllocationReason = ""
'            'Pinkal (03-Nov-2014) -- End


'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FilterTitleAndFilterQuery()
'        Me._FilterQuery = ""
'        Me._FilterTitle = ""
'        Try

'            'S.SANDEEP [ 12 MAY 2012 ] -- START
'            If mblnIsActive = False Then
'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            End If
'            'S.SANDEEP [ 12 MAY 2012 ] -- END

'            If mdtFromdate <> Nothing And mdtTodate <> Nothing Then
'                objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromdate))
'                Me._FilterQuery &= " AND CONVERT(VARCHAR(10),athremployee_master.auditdatetime,112)  >= @FromDate "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "From Date :") & " " & mdtFromdate.ToShortDateString & " "

'                objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtTodate))
'                Me._FilterQuery &= " AND CONVERT(VARCHAR(10),athremployee_master.auditdatetime,112) <= @ToDate "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "To Date :") & " " & mdtTodate.ToShortDateString & " "

'            End If

'            If mintEmployeeId > 0 Then
'                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'                Me._FilterQuery &= " AND athremployee_master.employeeunkid = @EmployeeId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Employee :") & " " & mstrEmployeeName & " "
'            End If

'            If mintCostCenterId > 0 Then
'                objDataOperation.AddParameter("@CostCenterId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostCenterId)
'                Me._FilterQuery &= " AND prcostcenter_master.CostCenterunkId = @CostCenterId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Cost Center : ") & " " & mstrCostCenterName & " "
'            End If

'            If mintBranchId > 0 Then
'                objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
'                Me._FilterQuery &= " AND hrstation_master.stationunkid = @BranchId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, "Branch : ") & " " & mstrBranchName & " "
'            End If

'            If mintDepartmentId > 0 Then
'                objDataOperation.AddParameter("@DepartmentId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentId)
'                Me._FilterQuery &= " AND hrdepartment_master.departmentunkid = @DepartmentId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "Department : ") & " " & mstrDepartmentName & " "
'            End If

'            If mintSectionId > 0 Then
'                objDataOperation.AddParameter("@SectionId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionId)
'                Me._FilterQuery &= " AND hrsection_master.sectionunkid = @SectionId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Section : ") & "" & mstrSectionName & " "
'            End If

'            If mintUnitId > 0 Then
'                objDataOperation.AddParameter("@UnitId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitId)
'                Me._FilterQuery &= " AND hrUnit_master.unitunkid = @UnitId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "Unit : ") & " " & mstrUnitName & " "
'            End If


'            If mintJobId > 0 Then
'                objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
'                Me._FilterQuery &= " AND hrjob_master.jobunkid = @JobId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "Job : ") & " " & mstrJobName & " "
'            End If

'            If mintGradeGroupId > 0 Then
'                objDataOperation.AddParameter("@GradeGrpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeGroupId)
'                Me._FilterQuery &= " AND hrgradegroup_master.gradegroupunkid = @GradeGrpId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Grade Group :") & " " & mstrGradeGroupName & " "
'            End If

'            If mintGradeId > 0 Then
'                objDataOperation.AddParameter("@GradeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeId)
'                Me._FilterQuery &= " AND hrgrade_master.gradeunkid = @GradeId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "Grade : ") & " " & mstrGradeName & " "
'            End If

'            If mintGradeLevelId > 0 Then
'                objDataOperation.AddParameter("@GradeLevelId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeLevelId)
'                Me._FilterQuery &= " AND hrgradelevel_master.gradelevelunkid = @GradeLevelId "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Grade Level :") & " " & mstrGradeLevelName & " "
'            End If

'            If mdblFromScale >= 0 Then
'                objDataOperation.AddParameter("@FromScale", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblFromScale)
'                Me._FilterQuery &= " AND athremployee_master.scale >= @FromScale "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "From Scale :") & " " & mdblFromScale & " "
'            End If

'            If mdblToScale > 0 Then
'                objDataOperation.AddParameter("@ToScale", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblToScale)
'                Me._FilterQuery &= " AND athremployee_master.scale <= @ToScale "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 34, "To Scale :") & " " & mdblToScale & " "
'            End If


'            'Pinkal (03-Nov-2014) -- Start
'            'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

'            If mintAllocationReasonId > 0 Then
'                objDataOperation.AddParameter("@allocationreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrAllocationReason)
'                Me._FilterQuery &= " AND athremployee_master.allocationreason = @allocationreason "
'                Me._FilterTitle &= Language.getMessage(mstrModuleName, 42, "Allocation Reason :") & " " & mstrAllocationReason & " "
'            End If

'            If mintReportId = 0 Then
'                If Me.OrderByQuery <> "" Then
'                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, "Order By :") & " " & Me.OrderByDisplay & " "
'                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
'                End If
'            ElseIf mintReportId = 1 Then
'                Me._FilterQuery &= "ORDER BY athremployee_master.employeecode,athremployee_master.auditdatetime "
'            End If

'            'Pinkal (03-Nov-2014) -- End

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
'        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        Dim strReportExportFile As String = ""
'        Try

'            'Pinkal (14-Aug-2012) -- Start
'            'Enhancement : TRA Changes

'            If mintCompanyUnkid <= 0 Then
'                mintCompanyUnkid = Company._Object._Companyunkid
'            End If
'            Company._Object._Companyunkid = mintCompanyUnkid
'            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


'            If mintUserUnkid <= 0 Then
'                mintUserUnkid = User._Object._Userunkid
'            End If

'            User._Object._Userunkid = mintUserUnkid


'            objRpt = Generate_DetailReport()
'            Rpt = objRpt

'            'Pinkal (14-Aug-2012) -- End

'            objRpt = Generate_DetailReport()
'            If Not IsNothing(objRpt) Then
'                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
'            End If
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
'        OrderByDisplay = ""
'        OrderByQuery = ""
'        Try
'            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
'            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
'        Try
'            Call OrderByExecute(iColumn_DetailReport)
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Report Generation "
'    Dim iColumn_DetailReport As New IColumnCollection

'    Public Property Field_OnDetailReport() As IColumnCollection
'        Get
'            Return iColumn_DetailReport
'        End Get
'        Set(ByVal value As IColumnCollection)
'            iColumn_DetailReport = value
'        End Set
'    End Property

'    Private Sub Create_OnDetailReport()
'        Try
'            'iColumn_DetailReport.Clear()
'            'iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 1, "Employee Code")))
'            'iColumn_DetailReport.Add(New IColumn("employee", Language.getMessage(mstrModuleName, 2, "Employee Name")))
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
'        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation

'            StrQ = " SELECT athremployee_master.employeeunkid,athremployee_master.employeecode, "

'            'S.SANDEEP [ 26 MAR 2014 ] -- START
'            'ENHANCEMENT : Requested by Rutta
'            '" ISNULL(athremployee_master.firstname,'') + ' '  + ISNULL(athremployee_master.surname,'') employee, " & _
'            If mblnFirstNamethenSurname = False Then
'                StrQ &= " ISNULL(athremployee_master.surname,'') + ' '  + ISNULL(athremployee_master.firstname,'') employee, "
'            Else
'                StrQ &= " ISNULL(athremployee_master.firstname,'') + ' '  + ISNULL(athremployee_master.surname,'') employee, "
'            End If
'            'S.SANDEEP [ 26 MAR 2014 ] -- END

'            StrQ &= " CONVERT(VARCHAR(10),athremployee_master.auditdatetime,112) Movementdate, " & _
'                      " ISNULL(prcostcenter_master.costcenterunkid,-1)costcenterunkid,ISNULL(prcostcenter_master.costcentername,'') costcentername," & _
'                      " ISNULL(hrstation_master.stationunkid,-1)stationunkid,ISNULL(hrstation_master.name,'') Branch, " & _
'                      " ISNULL(hrdepartment_master.departmentunkid,-1)departmentunkid,ISNULL(hrdepartment_master.name,'') Department, " & _
'                      " ISNULL(hrsection_master.sectionunkid,-1)sectionunkid,ISNULL(hrsection_master.name,'') Section, " & _
'                      " ISNULL(hrUnit_master.unitunkid,-1)unitunkid,ISNULL(hrUnit_master.name,'') Unit, " & _
'                      " ISNULL(hrjob_master.jobunkid,-1)jobunkid,ISNULL(hrjob_master.job_name,'')  Job, " & _
'                      " ISNULL(hrgradegroup_master.gradegroupunkid,-1)gradegroupunkid,ISNULL(hrgradegroup_master.name,'') Gradegroup, " & _
'                      " ISNULL(hrgrade_master.gradeunkid,-1)gradeunkid,ISNULL(hrgrade_master.name,'') Grade, " & _
'                      " ISNULL(hrgradelevel_master.gradelevelunkid,-1)gradelevelunkid,ISNULL(hrgradelevel_master.name,'') Gradelevel, athremployee_master.scale " & _
'                      " FROM athremployee_master " & _
'                      " JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = athremployee_master.costcenterunkid " & _
'                      " JOIN hrjob_master ON athremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                      " JOIN hrdepartment_master ON athremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                      " JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid =athremployee_master.gradegroupunkid " & _
'                      " JOIN hrgrade_master ON athremployee_master.gradeunkid = hrgrade_master.gradeunkid " & _
'                      " JOIN hrgradelevel_master ON athremployee_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
'                      " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = athremployee_master.stationunkid " & _
'                      " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid  = athremployee_master.sectionunkid " & _
'                      " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = athremployee_master.unitunkid "

'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'            If mblnIsActive = False Then
'                StrQ &= " JOIN hremployee_master ON hremployee_master.employeeunkid = athremployee_master.employeeunkid "
'            End If

'            StrQ &= " WHERE 1 = 1 "

'            If mblnIsActive = False Then
'                'S.SANDEEP [ 12 MAY 2012 ] -- START
'                'ISSUE : TRA ENHANCEMENTS
'                'StrQ &= " AND hremployee_master.isactive = 1"
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'                'S.SANDEEP [ 12 MAY 2012 ] -- END
'            End If
'            'Pinkal (24-Jun-2011) -- End

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If
'            'S.SANDEEP [ 13 FEB 2013 ] -- END

'            'S.SANDEEP [ 12 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES

'            'S.SANDEEP [ 12 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= UserAccessLevel._AccessLevelFilterString
'            'End If
'            If mstrUserAccessFilter = "" Then
'                If mblnIsActive = False Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                Else
'                    StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "athremployee_master")
'                End If
'            Else
'                If mblnIsActive = False Then
'                    StrQ &= mstrUserAccessFilter
'                Else
'                    StrQ &= mstrUserAccessFilter.Replace("hremployee_master", "athremployee_master")
'                End If
'            End If
'            'S.SANDEEP [ 12 NOV 2012 ] -- END

'            If UserAccessLevel._AccessLevel.Length > 0 Then
'            End If
'            'S.SANDEEP [ 12 MAY 2012 ] -- END

'            Call FilterTitleAndFilterQuery()

'            StrQ &= Me._FilterQuery

'            objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 36, "Level"))

'            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            rpt_Data = New ArutiReport.Designer.dsArutiReport

'            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
'                Dim rpt_Row As DataRow
'                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

'                rpt_Row.Item("Column1") = dtRow.Item("employeecode")
'                rpt_Row.Item("Column2") = dtRow.Item("employee")
'                If dtRow.Item("Movementdate").ToString.Trim <> "" Then
'                    rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow.Item("Movementdate").ToString).ToShortDateString
'                Else
'                    rpt_Row.Item("Column3") = ""
'                End If
'                rpt_Row.Item("Column4") = dtRow.Item("costcentername")
'                rpt_Row.Item("Column5") = dtRow.Item("Branch")
'                rpt_Row.Item("Column6") = dtRow.Item("Department")
'                rpt_Row.Item("Column7") = dtRow.Item("Section")
'                rpt_Row.Item("Column8") = dtRow.Item("Unit")
'                rpt_Row.Item("Column9") = dtRow.Item("Job")
'                rpt_Row.Item("Column10") = dtRow.Item("Gradegroup")
'                rpt_Row.Item("Column11") = dtRow.Item("Grade")
'                rpt_Row.Item("Column12") = dtRow.Item("Gradelevel")


'                'Pinkal (24-Apr-2013) -- Start
'                'Enhancement : TRA Changes

'                If mblnShowEmpScale Then
'                    rpt_Row.Item("Column13") = Format(dtRow.Item("scale"), GUI.fmtCurrency)
'                End If

'                'Pinkal (24-Apr-2013) -- End



'                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
'            Next

'            objRpt = New ArutiReport.Designer.rptEmployeeMovement

'            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
'            Dim arrImageRow As DataRow = Nothing
'            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

'            ReportFunction.Logo_Display(objRpt, _
'                                        ConfigParameter._Object._IsDisplayLogo, _
'                                        ConfigParameter._Object._ShowLogoRightSide, _
'                                        "arutiLogo1", _
'                                        "arutiLogo2", _
'                                        arrImageRow, _
'                                        "txtCompanyName", _
'                                        "txtReportName", _
'                                        "txtFilterDescription")

'            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

'            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
'                rpt_Data.Tables("ArutiTable").Rows.Add("")
'            End If

'            If ConfigParameter._Object._IsShowPreparedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 37, "Prepared By :"))
'                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
'            End If

'            If ConfigParameter._Object._IsShowCheckedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 38, "Checked By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
'            End If

'            If ConfigParameter._Object._IsShowApprovedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 39, "Approved By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
'            End If

'            If ConfigParameter._Object._IsShowReceivedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 40, "Received By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
'            End If


'            'Pinkal (24-Apr-2013) -- Start
'            'Enhancement : TRA Changes

'            If mblnShowEmpScale Then
'                ReportFunction.TextChange(objRpt, "txtScale", Language.getMessage(mstrModuleName, 17, "Scale"))
'            Else
'                ReportFunction.EnableSuppress(objRpt, "txtScale", True)
'            End If

'            'Pinkal (24-Apr-2013) -- End

'            objRpt.SetDataSource(rpt_Data)


'            ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 1, "Employee Code"))
'            ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 2, "Employee Name"))
'            ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 7, "Date"))
'            ReportFunction.TextChange(objRpt, "txtCostcenter", Language.getMessage(mstrModuleName, 8, "Cost Center"))
'            ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 9, "Branch"))
'            ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 10, "Department"))
'            ReportFunction.TextChange(objRpt, "txtSection", Language.getMessage(mstrModuleName, 11, "Section"))
'            ReportFunction.TextChange(objRpt, "txtUnit", Language.getMessage(mstrModuleName, 12, "Unit"))
'            ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 13, "Job"))
'            ReportFunction.TextChange(objRpt, "txtGradeGroup", Language.getMessage(mstrModuleName, 14, "Grade Group"))
'            ReportFunction.TextChange(objRpt, "txtGrade", Language.getMessage(mstrModuleName, 15, "Grade"))
'            ReportFunction.TextChange(objRpt, "txtGradeLevel", Language.getMessage(mstrModuleName, 16, "Grade Level"))


'            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 18, "Printed By :"))
'            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 19, "Printed Date :"))
'            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 20, "Page :"))

'            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
'            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

'            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
'            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
'            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

'            Return objRpt

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    Public Sub Generate_MovementReportWithAllocations()
'        Dim mdtTable As DataTable = Nothing
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Try

'            If mintCompanyUnkid <= 0 Then
'                mintCompanyUnkid = Company._Object._Companyunkid
'            End If
'            Company._Object._Companyunkid = mintCompanyUnkid
'            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


'            If mintUserUnkid <= 0 Then
'                mintUserUnkid = User._Object._Userunkid
'            End If

'            User._Object._Userunkid = mintUserUnkid


'            objDataOperation = New clsDataOperation

'            StrQ = " SELECT athremployee_master.employeeunkid,athremployee_master.employeecode AS 'Employee Code', "

'            If mblnFirstNamethenSurname = False Then
'                StrQ &= " ISNULL(athremployee_master.surname,'') + ' '  + ISNULL(athremployee_master.firstname,'') Emp, " & _
'                             " athremployee_master.employeecode + ' - ' +  ISNULL(athremployee_master.surname,'') + '  '  + ISNULL(athremployee_master.firstname,'') Employee, "
'            Else
'                StrQ &= " ISNULL(athremployee_master.firstname,'') + ' '  + ISNULL(athremployee_master.surname,'') Emp, " & _
'                             " athremployee_master.employeecode + ' - ' +  ISNULL(athremployee_master.firstname,'') + ' '  + ISNULL(athremployee_master.surname,'') Employee, "
'            End If

'            StrQ &= " athremployee_master.auditdatetime AS 'Movement Date', " & _
'                      " ISNULL(prcostcenter_master.costcenterunkid,-1)costcenterunkid,ISNULL(prcostcenter_master.costcentername,'') Costcenter," & _
'                      " ISNULL(hrstation_master.stationunkid,-1)stationunkid,ISNULL(hrstation_master.name,'') Branch, " & _
'                      " ISNULL(hrdepartment_group_master.deptgroupunkid,-1)deptgroupunkid,ISNULL(hrdepartment_group_master.name,'') 'Department Group', " & _
'                      " ISNULL(hrdepartment_master.deptgroupunkid,-1)departmentunkid,ISNULL(hrdepartment_master.name,'') Department, " & _
'                      " ISNULL(hrsectiongroup_master.sectiongroupunkid,-1)sectiongroupunkid,ISNULL(hrsectiongroup_master.name,'') 'Section Group', " & _
'                      " ISNULL(hrsection_master.sectionunkid,-1)sectionunkid,ISNULL(hrsection_master.name,'') Section, " & _
'                      " ISNULL(hrunitgroup_master.unitgroupunkid,-1)unitgroupunkid,ISNULL(hrunitgroup_master.name,'') 'Unit Group', " & _
'                      " ISNULL(hrUnit_master.unitunkid,-1)unitunkid,ISNULL(hrUnit_master.name,'') Unit, " & _
'                      " ISNULL(hrteam_master.teamunkid,-1)teamunkid,ISNULL(hrteam_master.name,'') Team, " & _
'                      " ISNULL(hrjobgroup_master.jobgroupunkid,-1)jobgroupunkid,ISNULL(hrjobgroup_master.name,'')  'Job Group', " & _
'                      " ISNULL(hrjob_master.jobunkid,-1)jobunkid,ISNULL(hrjob_master.job_name,'')  Job, " & _
'                      " ISNULL(hrclassgroup_master.classgroupunkid,-1)classgroupunkid,ISNULL(hrclassgroup_master.name,'')  'Class Group', " & _
'                      " ISNULL(hrclasses_master.classesunkid,-1)classesunkid,ISNULL(hrclasses_master.name,'')  Class, " & _
'                      " ISNULL(hrgradegroup_master.gradegroupunkid,-1)gradegroupunkid,ISNULL(hrgradegroup_master.name,'') 'Grade Group', " & _
'                      " ISNULL(hrgrade_master.gradeunkid,-1)gradeunkid,ISNULL(hrgrade_master.name,'') Grade, " & _
'                      " ISNULL(hrgradelevel_master.gradelevelunkid,-1)gradelevelunkid,ISNULL(hrgradelevel_master.name,'') 'Grade Level', " & _
'                      " ISNULL(athremployee_master.allocationreason,'') 'Allocation Reason', " & _
'                      " athremployee_master.scale " & _
'                      " FROM athremployee_master " & _
'                      " JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = athremployee_master.costcenterunkid " & _
'                      " JOIN hrjobgroup_master ON athremployee_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
'                      " JOIN hrjob_master ON athremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                      " LEFT JOIN hrdepartment_group_master ON athremployee_master.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
'                      " JOIN hrdepartment_master ON athremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                      " JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid =athremployee_master.gradegroupunkid " & _
'                      " JOIN hrgrade_master ON athremployee_master.gradeunkid = hrgrade_master.gradeunkid " & _
'                      " JOIN hrgradelevel_master ON athremployee_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
'                      " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = athremployee_master.stationunkid " & _
'                      " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = athremployee_master.sectiongroupunkid " & _
'                      " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid  = athremployee_master.sectionunkid " & _
'                      " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = athremployee_master.unitgroupunkid " & _
'                      " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = athremployee_master.unitunkid " & _
'                      " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = athremployee_master.teamunkid " & _
'                      " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = athremployee_master.classgroupunkid " & _
'                      " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = athremployee_master.classunkid "

'            If mblnIsActive = False Then
'                StrQ &= " JOIN hremployee_master ON hremployee_master.employeeunkid = athremployee_master.employeeunkid "
'            End If

'            StrQ &= " WHERE 1 = 1 "

'            If mblnIsActive = False Then
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'            End If

'            If mstrAdvance_Filter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvance_Filter
'            End If

'            If mstrUserAccessFilter = "" Then
'                If mblnIsActive = False Then
'                    StrQ &= UserAccessLevel._AccessLevelFilterString
'                Else
'                    StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "athremployee_master")
'                End If
'            Else
'                If mblnIsActive = False Then
'                    StrQ &= mstrUserAccessFilter
'                Else
'                    StrQ &= mstrUserAccessFilter.Replace("hremployee_master", "athremployee_master")
'                End If
'            End If

'            Call FilterTitleAndFilterQuery()

'            StrQ &= Me._FilterQuery

'            objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 36, "Level"))

'            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If


'            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

'            Dim strarrGroupColumns As String() = Nothing
'            Dim rowsArrayHeader As New ArrayList
'            Dim rowsArrayFooter As New ArrayList
'            Dim row As WorksheetRow
'            Dim wcell As WorksheetCell

'            Dim mdtTableExcel As DataTable = dsList.Tables(0)

'            If mdtTableExcel.Columns.Contains("employeeunkid") Then
'                mdtTableExcel.Columns.Remove("employeeunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("Emp") Then
'                mdtTableExcel.Columns.Remove("Emp")
'            End If

'            If mdtTableExcel.Columns.Contains("costcenterunkid") Then
'                mdtTableExcel.Columns.Remove("costcenterunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("stationunkid") Then
'                mdtTableExcel.Columns.Remove("stationunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("deptgroupunkid") Then
'                mdtTableExcel.Columns.Remove("deptgroupunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("departmentunkid") Then
'                mdtTableExcel.Columns.Remove("departmentunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("sectiongroupunkid") Then
'                mdtTableExcel.Columns.Remove("sectiongroupunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("sectionunkid") Then
'                mdtTableExcel.Columns.Remove("sectionunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("unitgroupunkid") Then
'                mdtTableExcel.Columns.Remove("unitgroupunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("unitunkid") Then
'                mdtTableExcel.Columns.Remove("unitunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("teamunkid") Then
'                mdtTableExcel.Columns.Remove("teamunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("jobgroupunkid") Then
'                mdtTableExcel.Columns.Remove("jobgroupunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("jobunkid") Then
'                mdtTableExcel.Columns.Remove("jobunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("classgroupunkid") Then
'                mdtTableExcel.Columns.Remove("classgroupunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("classesunkid") Then
'                mdtTableExcel.Columns.Remove("classesunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("gradegroupunkid") Then
'                mdtTableExcel.Columns.Remove("gradegroupunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("gradeunkid") Then
'                mdtTableExcel.Columns.Remove("gradeunkid")
'            End If

'            If mdtTableExcel.Columns.Contains("gradelevelunkid") Then
'                mdtTableExcel.Columns.Remove("gradelevelunkid")
'            End If

'            If mblnShowEmpScale = False AndAlso mdtTableExcel.Columns.Contains("scale") Then
'                mdtTableExcel.Columns.Remove("scale")
'            End If


'            Dim strGrpCols As String() = {"Employee"}
'            strarrGroupColumns = strGrpCols
'            row = New WorksheetRow()

'            If Me._FilterTitle.ToString.Length > 0 Then
'                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
'                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
'                row.Cells.Add(wcell)
'            End If
'            rowsArrayHeader.Add(row)

'            row = New WorksheetRow()
'            wcell = New WorksheetCell("", "s10bw")
'            row.Cells.Add(wcell)
'            rowsArrayHeader.Add(row)


'            row = New WorksheetRow()
'            wcell = New WorksheetCell("", "s10bw")
'            row.Cells.Add(wcell)
'            rowsArrayFooter.Add(row)
'            '--------------------

'            If ConfigParameter._Object._IsShowPreparedBy = True Then
'                row = New WorksheetRow()
'                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
'                wcell.MergeAcross = 4
'                row.Cells.Add(wcell)

'                wcell = New WorksheetCell("", "s10bw")
'                row.Cells.Add(wcell)
'                rowsArrayFooter.Add(row)

'                row = New WorksheetRow()
'                wcell = New WorksheetCell("", "s10bw")
'                row.Cells.Add(wcell)
'                rowsArrayFooter.Add(row)
'            End If


'            If ConfigParameter._Object._IsShowCheckedBy = True Then
'                row = New WorksheetRow()
'                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 38, "Checked By :"), "s8bw")
'                wcell.MergeAcross = 4
'                row.Cells.Add(wcell)

'                wcell = New WorksheetCell("", "s10bw")
'                row.Cells.Add(wcell)
'                rowsArrayFooter.Add(row)

'                row = New WorksheetRow()
'                wcell = New WorksheetCell("", "s10bw")
'                row.Cells.Add(wcell)
'                rowsArrayFooter.Add(row)
'            End If


'            If ConfigParameter._Object._IsShowApprovedBy = True Then
'                row = New WorksheetRow()
'                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 41, "Approved By"), "s8bw")
'                wcell.MergeAcross = 4
'                row.Cells.Add(wcell)

'                wcell = New WorksheetCell("", "s10bw")
'                row.Cells.Add(wcell)
'                rowsArrayFooter.Add(row)

'                row = New WorksheetRow()
'                wcell = New WorksheetCell("", "s10bw")
'                row.Cells.Add(wcell)
'                rowsArrayFooter.Add(row)
'            End If

'            If ConfigParameter._Object._IsShowReceivedBy = True Then
'                row = New WorksheetRow()
'                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 40, "Received By :"), "s8bw")
'                wcell.MergeAcross = 4
'                row.Cells.Add(wcell)
'                rowsArrayFooter.Add(row)
'            End If


'            '--------------------

'            'SET EXCEL CELL WIDTH  
'            Dim intArrayColumnWidth As Integer() = Nothing
'            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
'            For i As Integer = 0 To intArrayColumnWidth.Length - 1
'                intArrayColumnWidth(i) = 125
'            Next
'            'SET EXCEL CELL WIDTH

'            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, mstrReportTypeName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Generate_MovementReportWithAllocations; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'#End Region


'    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None)

'    End Sub
'End Class

