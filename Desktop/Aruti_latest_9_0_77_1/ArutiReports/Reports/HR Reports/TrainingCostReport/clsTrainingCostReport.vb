'************************************************************************************************************************************
'Class Name : clsTrainingCostReport.vb
'Purpose    :
'Date       :12/23/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Anjan
''' </summary>
Public Class clsTrainingCostReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTrainingCostReport"
    Private mstrReportId As String = enArutiReport.TrainingCostReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "
    Private mintInstituteId As Integer
    Private mstrInstituteName As String

    'Anjan (10 Feb 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    'Private mstrCourseTitle As String
    Private mintTrainingCourseId As Integer
    Private mstrTrainingCourse As String
    'Anjan (10 Feb 2012)-End 

    Private mintCourseType As Integer
    Private mstrCourseType As String
    Private mintDepartmentId As Integer
    Private mstrDepartmentName As String
    Private mintReportTypeId As Integer
    Private mstrReportTypeName As String
    Private mintStatusId As Integer
    Private mstrStatusName As String
    Private mstrEmpCode As String
    Private mintEmployeeId As Integer
    Private mstrEmployeeName As String

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Private mstrAllocationTypeName As String = String.Empty
    Private mintAllocationTypeId As Integer = 0
    Private mstrAllocationNames As String = String.Empty
    Private mstrAllocationIds As String = String.Empty
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

#Region " Properties "
    Public WriteOnly Property _Instituteid() As Integer
        Set(ByVal value As Integer)
            mintInstituteId = value
        End Set
    End Property

    Public WriteOnly Property _InstituteName() As String
        Set(ByVal value As String)
            mstrInstituteName = value
        End Set
    End Property

    Public WriteOnly Property _CourseTitle() As String
        Set(ByVal value As String)
            mintTrainingCourseId = value
        End Set
    End Property

    Public WriteOnly Property _CourseTypeId() As Integer
        Set(ByVal value As Integer)
            mintCourseType = value
        End Set
    End Property

    Public WriteOnly Property _CourseType() As String
        Set(ByVal value As String)
            mstrCourseType = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentId() As Integer
        Set(ByVal value As Integer)
            mintDepartmentId = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentName() As String
        Set(ByVal value As String)
            mstrDepartmentName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _EmpCode() As String
        Set(ByVal value As String)
            mstrEmpCode = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End


    'Anjan (10 Feb 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingCourse = value
        End Set
    End Property

    Public WriteOnly Property _TrainingId() As Integer
        Set(ByVal value As Integer)
            mintTrainingCourseId = value
        End Set
    End Property
    'Anjan (10 Feb 2012)-End 

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END


    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Public WriteOnly Property _AllocationTypeName() As String
        Set(ByVal value As String)
            mstrAllocationTypeName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTypeId() As Integer
        Set(ByVal value As Integer)
            mintAllocationTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AllocationNames() As String
        Set(ByVal value As String)
            mstrAllocationNames = value
        End Set
    End Property

    Public WriteOnly Property _AllocationIds() As String
        Set(ByVal value As String)
            mstrAllocationIds = value
        End Set
    End Property
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintInstituteId = 0
            mstrInstituteName = ""
            'mintTrainingCourseId = ""
            mintCourseType = 0
            mstrCourseType = ""
            mintDepartmentId = 0
            mstrDepartmentName = ""
            mintStatusId = 0
            mstrStatusName = ""
            mstrEmpCode = ""


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            mintTrainingCourseId = 0
            mstrTrainingCourse = ""
            'Anjan (10 Feb 2012)-End 


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            mstrAllocationTypeName = String.Empty
            mintAllocationTypeId = 0
            mstrAllocationNames = String.Empty
            mstrAllocationIds = String.Empty
            'S.SANDEEP [08-FEB-2017] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            objDataOperation.AddParameter("@INP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "Enrolled"))
            objDataOperation.AddParameter("@PNP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 33, "Postponed"))
            objDataOperation.AddParameter("@CMP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 34, "Completed"))
            'S.SANDEEP [ 05 JULY 2011 ] -- END 


            If mintInstituteId > 0 Then
                objDataOperation.AddParameter("@Instituteid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteId)
                Me._FilterQuery &= " AND hrinstitute_master.instituteunkid = @Instituteid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Institute Name : ") & mstrInstituteName & " "
            End If


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'If mstrTrainingCourseId.Trim <> "" Then
            '    objDataOperation.AddParameter("@CourseTitle", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mintTrainingCourseId & "%")
            '    Me._FilterQuery &= " AND course_title LIKE  @CourseTitle "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 102, "Course Title : ") & mintTrainingCourseId & " "
            'End If
            If mintTrainingCourseId > 0 Then
                objDataOperation.AddParameter("@CourseTitle", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingCourseId)
                Me._FilterQuery &= " AND ISNULL(CAST((CASE WHEN ISNUMERIC(cm.masterunkid) <> 0 THEN cm.masterunkid ELSE 0 END)AS Int),0) = @CourseTitle "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Course Title : ") & mstrTrainingCourse & " "
            End If

            'Anjan (10 Feb 2012)-End 



            If mintCourseType > 0 Then
                objDataOperation.AddParameter("@CourseType", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseType)
                Me._FilterQuery &= " AND cfcommon_master.masterunkid = @CourseType "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Course Type : ") & mstrCourseType & " "
            End If

            If mintDepartmentId > 0 Then
                objDataOperation.AddParameter("@DepartmentId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentId)
                Me._FilterQuery &= " AND hrdepartment_master.departmentunkid = @DepartmentId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Department : ") & mstrDepartmentName & " "
            End If

            If mstrEmpCode.Trim <> "" Then
                objDataOperation.AddParameter("@EmpCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpCode)
                Me._FilterQuery &= " AND hremployee_master.employeecode = @EmpCode "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Emp Code : ") & mstrEmpCode & " "
            End If

            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND hrtraining_enrollment_tran.status_id = @StatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Status : ") & mstrStatusName & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Employee Name : ") & mstrEmployeeName & " "
            End If

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            If mintAllocationTypeId > 0 Then
                objDataOperation.AddParameter("@allocationtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId)
                Me._FilterQuery &= "AND allocationtypeunkid = @allocationtypeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 200, "Allocation Binded :") & " " & mstrAllocationTypeName & " "
            End If

            If mstrAllocationNames.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 300, "Allocation(s)  :") & " " & mstrAllocationNames & " "
            End If
            'S.SANDEEP [08-FEB-2017] -- END

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Order By :") & " " & Me.OrderByDisplay
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("institute", Language.getMessage(mstrModuleName, 9, "Institute")))
            iColumn_DetailReport.Add(New IColumn("coursetype", Language.getMessage(mstrModuleName, 10, "Type")))
            iColumn_DetailReport.Add(New IColumn("status", Language.getMessage(mstrModuleName, 11, "Status")))
            iColumn_DetailReport.Add(New IColumn("fees", Language.getMessage(mstrModuleName, 12, "Fees")))
            iColumn_DetailReport.Add(New IColumn("accomodation", Language.getMessage(mstrModuleName, 13, "Accomodation")))
            iColumn_DetailReport.Add(New IColumn("travel", Language.getMessage(mstrModuleName, 14, "Travel")))
            iColumn_DetailReport.Add(New IColumn("meals", Language.getMessage(mstrModuleName, 15, "Meals")))
            iColumn_DetailReport.Add(New IColumn("exam", Language.getMessage(mstrModuleName, 16, "Exam")))
            iColumn_DetailReport.Add(New IColumn("misc", Language.getMessage(mstrModuleName, 17, "Misc.")))
            iColumn_DetailReport.Add(New IColumn("allowance", Language.getMessage(mstrModuleName, 18, "Allowance")))
            iColumn_DetailReport.Add(New IColumn("materials", Language.getMessage(mstrModuleName, 19, "Materials")))
            iColumn_DetailReport.Add(New IColumn("totalcost", Language.getMessage(mstrModuleName, 20, "Total Cost")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            'StrQ = "SELECT  hrinstitute_master.institute_name AS institute  " & _
            '        " ,course_title AS course  " & _
            '        " ,cfcommon_master.name AS coursetype  " & _
            '        " ,(ISNULL(hrtraining_scheduling.fees,0) + ISNULL (hrtraining_scheduling.misc,0)+ ISNULL(hrtraining_scheduling.travel,0)+ISNULL(hrtraining_scheduling.accomodation,0)+ISNULL(hrtraining_scheduling.allowance,0)+ISNULL(hrtraining_scheduling.meals,0)+ISNULL(hrtraining_scheduling.material,0)+ISNULL(hrtraining_scheduling.exam,0)) AS totalcost  " & _
            '        " ,ISNULL(hrtraining_scheduling.fees,0)  AS fees  " & _
            '        " ,ISNULL(hrtraining_scheduling.accomodation,0) AS accomodation  " & _
            '        " ,ISNULL(hrtraining_scheduling.travel,0) AS travel  " & _
            '        " ,ISNULL(hrtraining_scheduling.meals,0) AS meals  " & _
            '        " ,ISNULL(hrtraining_scheduling.exam,0) AS exam    " & _
            '        " ,ISNULL (hrtraining_scheduling.misc,0) AS misc  " & _
            '        " ,ISNULL(hrtraining_scheduling.allowance,0) AS allowance  " & _
            '        " ,ISNULL(hrtraining_scheduling.material,0) AS materials  " & _
            '        " ,(hremployee_master.firstname+' '+hremployee_master.surname) AS employee  " & _
            '        " ,hremployee_master.employeecode AS empcode " & _
            '        " ,hrdepartment_master.name AS departmentname " & _
            '        " ,hrdepartment_master.code AS departmentcode " & _
            '        " ,CASE WHEN hrtraining_enrollment_tran.status_id = 1 THEN 'In Progress' " & _
            '             " WHEN hrtraining_enrollment_tran.status_id = 2 THEN 'Postponed' " & _
            '             " WHEN hrtraining_enrollment_tran.status_id = 3 THEN 'Completed' " & _
            '        " END AS status " & _
            '        " ,hrtraining_enrollment_tran.status_id AS statusid " & _
            '        " ,hrinstitute_master.instituteunkid AS instituteid " & _
            '        " ,hremployee_master.employeeunkid AS empid " & _
            '        " ,hrdepartment_master.departmentunkid AS deptid " & _
            '        " ,cfcommon_master.masterunkid AS coursetypeid " & _
            '        " FROM  " & _
            '        " hrtraining_scheduling  " & _
            '        " LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=hrtraining_scheduling.traininginstituteunkid  " & _
            '        " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid=qualificationgroupunkid AND mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
            '        " LEFT JOIN hrtraining_enrollment_tran ON  hrtraining_enrollment_tran.trainingschedulingunkid=hrtraining_scheduling.trainingschedulingunkid  " & _
            '        " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid=hrtraining_enrollment_tran.employeeunkid  " & _
            '        " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid  " & _
            '        " WHERE 1 = 1 "


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'StrQ = "SELECT " & _
            '        "	 hrinstitute_master.institute_name AS institute " & _
            '                " ,course_title AS course  " & _
            '                " ,cfcommon_master.name AS coursetype  " & _
            '                " ,(ISNULL(hrtraining_scheduling.fees,0) + ISNULL (hrtraining_scheduling.misc,0)+ ISNULL(hrtraining_scheduling.travel,0)+ISNULL(hrtraining_scheduling.accomodation,0)+ISNULL(hrtraining_scheduling.allowance,0)+ISNULL(hrtraining_scheduling.meals,0)+ISNULL(hrtraining_scheduling.material,0)+ISNULL(hrtraining_scheduling.exam,0)) AS totalcost  " & _
            '                " ,ISNULL(hrtraining_scheduling.fees,0)  AS fees  " & _
            '                " ,ISNULL(hrtraining_scheduling.accomodation,0) AS accomodation  " & _
            '                " ,ISNULL(hrtraining_scheduling.travel,0) AS travel  " & _
            '                " ,ISNULL(hrtraining_scheduling.meals,0) AS meals  " & _
            '                " ,ISNULL(hrtraining_scheduling.exam,0) AS exam    " & _
            '                " ,ISNULL (hrtraining_scheduling.misc,0) AS misc  " & _
            '                " ,ISNULL(hrtraining_scheduling.allowance,0) AS allowance  " & _
            '                " ,ISNULL(hrtraining_scheduling.material,0) AS materials  " & _
            '                " ,(hremployee_master.firstname+' '+hremployee_master.surname) AS employee  " & _
            '                " ,hremployee_master.employeecode AS empcode " & _
            '                " ,hrdepartment_master.name AS departmentname " & _
            '                " ,hrdepartment_master.code AS departmentcode " & _
            '        "	,CASE WHEN hrtraining_enrollment_tran.status_id = 1 THEN @INP " & _
            '        "		  WHEN hrtraining_enrollment_tran.status_id = 2 THEN @PNP " & _
            '        "	      WHEN hrtraining_enrollment_tran.status_id = 3 THEN @CMP END AS status " & _
            '                " ,hrtraining_enrollment_tran.status_id AS statusid " & _
            '                " ,hrinstitute_master.instituteunkid AS instituteid " & _
            '                " ,hremployee_master.employeeunkid AS empid " & _
            '                " ,hrdepartment_master.departmentunkid AS deptid " & _
            '                " ,cfcommon_master.masterunkid AS coursetypeid " & _
            '        "FROM hrtraining_scheduling " & _
            '        " LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=hrtraining_scheduling.traininginstituteunkid  " & _
            '        " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid=qualificationgroupunkid AND mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
            '        " LEFT JOIN hrtraining_enrollment_tran ON  hrtraining_enrollment_tran.trainingschedulingunkid=hrtraining_scheduling.trainingschedulingunkid  " & _
            '        " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid=hrtraining_enrollment_tran.employeeunkid  " & _
            '        " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid  " & _
            '        "WHERE hrtraining_scheduling.iscancel = 0 AND hrtraining_scheduling.isvoid = 0 " & _
            '        "	AND hrtraining_enrollment_tran.iscancel = 0 AND hrtraining_enrollment_tran.isvoid = 0 "
            StrQ = "SELECT " & _
                    "	 hrinstitute_master.institute_name AS institute " & _
                            " , cm.name AS course  " & _
                            " ,cfcommon_master.name AS coursetype  " & _
                            " ,(ISNULL(hrtraining_scheduling.fees,0) + ISNULL (hrtraining_scheduling.misc,0)+ ISNULL(hrtraining_scheduling.travel,0)+ISNULL(hrtraining_scheduling.accomodation,0)+ISNULL(hrtraining_scheduling.allowance,0)+ISNULL(hrtraining_scheduling.meals,0)+ISNULL(hrtraining_scheduling.material,0)+ISNULL(hrtraining_scheduling.exam,0)) AS totalcost  " & _
                            " ,ISNULL(hrtraining_scheduling.fees,0)  AS fees  " & _
                            " ,ISNULL(hrtraining_scheduling.accomodation,0) AS accomodation  " & _
                            " ,ISNULL(hrtraining_scheduling.travel,0) AS travel  " & _
                            " ,ISNULL(hrtraining_scheduling.meals,0) AS meals  " & _
                            " ,ISNULL(hrtraining_scheduling.exam,0) AS exam    " & _
                            " ,ISNULL (hrtraining_scheduling.misc,0) AS misc  " & _
                            " ,ISNULL(hrtraining_scheduling.allowance,0) AS allowance  " & _
                            " ,ISNULL(hrtraining_scheduling.material,0) AS materials  "

            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            '" ,(hremployee_master.firstname+' '+hremployee_master.surname) AS employee  " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= " ,(hremployee_master.surname+' '+hremployee_master.firstname) AS employee  "
            Else
                StrQ &= " ,(hremployee_master.firstname+' '+hremployee_master.surname) AS employee  "
            End If
            'SANDEEP [ 26 MAR 2014 ] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= " ,hremployee_master.employeecode AS empcode " & _
            '            " ,hrdepartment_master.name AS departmentname " & _
            '            " ,hrdepartment_master.code AS departmentcode " & _
            '    "	,CASE WHEN hrtraining_enrollment_tran.status_id = 1 THEN @INP " & _
            '    "		  WHEN hrtraining_enrollment_tran.status_id = 2 THEN @PNP " & _
            '    "	      WHEN hrtraining_enrollment_tran.status_id = 3 THEN @CMP END AS status " & _
            '            " ,hrtraining_enrollment_tran.status_id AS statusid " & _
            '            " ,hrinstitute_master.instituteunkid AS instituteid " & _
            '            " ,hremployee_master.employeeunkid AS empid " & _
            '            " ,hrdepartment_master.departmentunkid AS deptid " & _
            '            " ,cfcommon_master.masterunkid AS coursetypeid " & _
            '    "FROM hrtraining_scheduling " & _
            '    " LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=hrtraining_scheduling.traininginstituteunkid  " & _
            '    " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid=qualificationgroupunkid AND mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
            '    " LEFT JOIN hrtraining_enrollment_tran ON  hrtraining_enrollment_tran.trainingschedulingunkid=hrtraining_scheduling.trainingschedulingunkid  " & _
            '    " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid=hrtraining_enrollment_tran.employeeunkid  " & _
            '    " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid  " & _
            '    "JOIN cfcommon_master as cm ON cm.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) " & _
            '    "WHERE hrtraining_scheduling.iscancel = 0 AND hrtraining_scheduling.isvoid = 0 " & _
            '    "	AND hrtraining_enrollment_tran.iscancel = 0 AND hrtraining_enrollment_tran.isvoid = 0 "
            ''Anjan (10 Feb 2012)-End 
            ''S.SANDEEP [ 05 JULY 2011 ] -- END 
            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'If mblnIsActive = False Then
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ISSUE : TRA ENHANCEMENTS
            '    'StrQ &= " AND hremployee_master.isactive = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            'End If
            ''Pinkal (24-Jun-2011) -- End

            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''S.SANDEEP [ 12 MAY 2012 ] -- END

            StrQ &= " ,hremployee_master.employeecode AS empcode " & _
                        " ,hrdepartment_master.name AS departmentname " & _
                        " ,hrdepartment_master.code AS departmentcode " & _
                "	,CASE WHEN hrtraining_enrollment_tran.status_id = 1 THEN @INP " & _
                "		  WHEN hrtraining_enrollment_tran.status_id = 2 THEN @PNP " & _
                "	      WHEN hrtraining_enrollment_tran.status_id = 3 THEN @CMP END AS status " & _
                        " ,hrtraining_enrollment_tran.status_id AS statusid " & _
                        " ,hrinstitute_master.instituteunkid AS instituteid " & _
                        " ,hremployee_master.employeeunkid AS empid " & _
                        " ,hrdepartment_master.departmentunkid AS deptid " & _
                        " ,cfcommon_master.masterunkid AS coursetypeid " & _
                        " ,hrtraining_scheduling.allocationtypeunkid " & _
                        " ,hrtraining_scheduling.trainingschedulingunkid " & _
                "FROM hrtraining_scheduling " & _
                " LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=hrtraining_scheduling.traininginstituteunkid  " & _
                " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid=qualificationgroupunkid AND mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                " LEFT JOIN hrtraining_enrollment_tran ON  hrtraining_enrollment_tran.trainingschedulingunkid=hrtraining_scheduling.trainingschedulingunkid  " & _
                " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid=hrtraining_enrollment_tran.employeeunkid  " & _
                    " LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           departmentunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                    "JOIN cfcommon_master as cm ON cm.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) "

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            '/* ADDED */
            '" ,hrtraining_scheduling.allocationtypeunkid " & _
            '" ,hrtraining_scheduling.trainingschedulingunkid " & _
            'S.SANDEEP [08-FEB-2017] -- END


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE hrtraining_scheduling.iscancel = 0 AND hrtraining_scheduling.isvoid = 0 " & _
                    "	AND hrtraining_enrollment_tran.iscancel = 0 AND hrtraining_enrollment_tran.isvoid = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If
            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            Dim dsGrpTot As New DataSet
            Dim dsGrandTot As New DataSet

            StrQ = "SELECT "
            Select Case mintReportTypeId
                Case 0
                    StrQ &= "	hremployee_master.departmentunkid "
                Case 1
                    StrQ &= "	hrtraining_enrollment_tran.employeeunkid "
            End Select

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= "	,ISNULL(SUM(hrtraining_scheduling.fees),0) AS Fees " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.accomodation),0) AS Accomodation " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.travel),0) AS Travel " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.meals),0) AS Meals " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.exam),0) AS Exam " & _
            '        "	,ISNULL (SUM(hrtraining_scheduling.misc),0) AS Misc " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.allowance),0) AS Allowance " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.material),0) AS Materials " & _
            '        "	,SUM((ISNULL(hrtraining_scheduling.fees,0) + ISNULL (hrtraining_scheduling.misc,0)+ ISNULL(hrtraining_scheduling.travel,0)+ISNULL(hrtraining_scheduling.accomodation,0)+ISNULL(hrtraining_scheduling.allowance,0)+ISNULL(hrtraining_scheduling.meals,0)+ISNULL(hrtraining_scheduling.material,0)+ISNULL(hrtraining_scheduling.exam,0))) AS totalcost " & _
            '        "FROM hrtraining_enrollment_tran " & _
            '        "	JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "	JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
            '        "   JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.qualificationgroupunkid AND mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
            '        "   JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " & _
            '        "WHERE hrtraining_enrollment_tran.iscancel = 0 AND hrtraining_enrollment_tran.isvoid = 0 " & _
            '        "	AND hrtraining_scheduling.iscancel = 0 AND hrtraining_scheduling.isvoid = 0 "

            StrQ &= "	,ISNULL(SUM(hrtraining_scheduling.fees),0) AS Fees " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.accomodation),0) AS Accomodation " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.travel),0) AS Travel " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.meals),0) AS Meals " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.exam),0) AS Exam " & _
                    "	,ISNULL (SUM(hrtraining_scheduling.misc),0) AS Misc " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.allowance),0) AS Allowance " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.material),0) AS Materials " & _
                    "	,SUM((ISNULL(hrtraining_scheduling.fees,0) + ISNULL (hrtraining_scheduling.misc,0)+ ISNULL(hrtraining_scheduling.travel,0)+ISNULL(hrtraining_scheduling.accomodation,0)+ISNULL(hrtraining_scheduling.allowance,0)+ISNULL(hrtraining_scheduling.meals,0)+ISNULL(hrtraining_scheduling.material,0)+ISNULL(hrtraining_scheduling.exam,0))) AS totalcost " & _
                    "FROM hrtraining_enrollment_tran " & _
                    "	JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "	JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                    "   JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.qualificationgroupunkid AND mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                    "   LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 "


            'S.SANDEEP [06 AUG 2016] -- START
            '"   JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " -- REMOVED
            '"   LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " -- ADDED
            'S.SANDEEP [06 AUG 2016] -- END


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE hrtraining_enrollment_tran.iscancel = 0 AND hrtraining_enrollment_tran.isvoid = 0 " & _
                    "	AND hrtraining_scheduling.iscancel = 0 AND hrtraining_scheduling.isvoid = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            If mintAllocationTypeId > 0 Then
                StrQ &= " AND hrtraining_scheduling.allocationtypeunkid = '" & mintAllocationTypeId & "' "
            End If
            'S.SANDEEP [08-FEB-2017] -- END

            'Shani(24-Aug-2015) -- End

            If mintDepartmentId > 0 Then
                StrQ &= " AND hremployee_master.departmentunkid = " & mintDepartmentId
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND hrtraining_enrollment_tran.employeeunkid = " & mintEmployeeId
            End If

            If mintCourseType > 0 Then
                StrQ &= " AND cfcommon_master.masterunkid = " & mintCourseType
            End If

            If mintInstituteId > 0 Then
                StrQ &= " AND hrinstitute_master.instituteunkid = " & mintInstituteId
            End If


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

            'If mstrTrainingCourseId.Trim <> "" Then
            '    StrQ &= " AND hrtraining_scheduling.course_title LIKE '%" & mstrTrainingCourseId.Trim & "%'"
            'End If
            If mintTrainingCourseId > 0 Then
                StrQ &= " AND ISNULL(CAST((CASE WHEN ISNUMERIC(hrtraining_scheduling.course_title) <> 0 THEN hrtraining_scheduling.course_title ELSE 0 END)AS Int),0) = " & mintTrainingCourseId
            End If
            'Anjan (10 Feb 2012)-End 


            If mstrEmpCode.Trim <> "" Then
                StrQ &= " AND hremployee_master.employeecode = '" & mstrEmpCode & "'"
            End If

            If mintStatusId > 0 Then
                StrQ &= " AND hrtraining_enrollment_tran.status_id = '" & mintStatusId & "'"
            End If

            Select Case mintReportTypeId
                Case 0
                    StrQ &= "GROUP BY hremployee_master.departmentunkid "
                Case 1
                    StrQ &= "GROUP BY hrtraining_enrollment_tran.employeeunkid "
            End Select

            dsGrpTot = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT " & _
            '        "	 ISNULL(SUM(hrtraining_scheduling.fees),0) AS Fees " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.accomodation),0) AS Accomodation " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.travel),0) AS Travel " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.meals),0) AS Meals " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.exam),0) AS Exam " & _
            '        "	,ISNULL (SUM(hrtraining_scheduling.misc),0) AS Misc " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.allowance),0) AS Allowance " & _
            '        "	,ISNULL(SUM(hrtraining_scheduling.material),0) AS Materials " & _
            '        "	,ISNULL(SUM((ISNULL(hrtraining_scheduling.fees,0) + ISNULL (hrtraining_scheduling.misc,0)+ ISNULL(hrtraining_scheduling.travel,0)+ISNULL(hrtraining_scheduling.accomodation,0)+ISNULL(hrtraining_scheduling.allowance,0)+ISNULL(hrtraining_scheduling.meals,0)+ISNULL(hrtraining_scheduling.material,0)+ISNULL(hrtraining_scheduling.exam,0))),0) AS totalcost " & _
            '        "FROM hrtraining_enrollment_tran " & _
            '        "	JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "	JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
            '        "   JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.qualificationgroupunkid AND mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
            '        "   JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " & _
            '        "WHERE hrtraining_enrollment_tran.iscancel = 0 AND hrtraining_enrollment_tran.isvoid = 0 " & _
            '        "	AND hrtraining_scheduling.iscancel = 0 AND hrtraining_scheduling.isvoid = 0 "
            StrQ = "SELECT " & _
                    "	 ISNULL(SUM(hrtraining_scheduling.fees),0) AS Fees " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.accomodation),0) AS Accomodation " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.travel),0) AS Travel " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.meals),0) AS Meals " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.exam),0) AS Exam " & _
                    "	,ISNULL (SUM(hrtraining_scheduling.misc),0) AS Misc " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.allowance),0) AS Allowance " & _
                    "	,ISNULL(SUM(hrtraining_scheduling.material),0) AS Materials " & _
                    "	,ISNULL(SUM((ISNULL(hrtraining_scheduling.fees,0) + ISNULL (hrtraining_scheduling.misc,0)+ ISNULL(hrtraining_scheduling.travel,0)+ISNULL(hrtraining_scheduling.accomodation,0)+ISNULL(hrtraining_scheduling.allowance,0)+ISNULL(hrtraining_scheduling.meals,0)+ISNULL(hrtraining_scheduling.material,0)+ISNULL(hrtraining_scheduling.exam,0))),0) AS totalcost " & _
                    "FROM hrtraining_enrollment_tran " & _
                    "	JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "	JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                    "   JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.qualificationgroupunkid AND mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                    "   LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 "

            'S.SANDEEP [06 AUG 2016] -- START
            '"   JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " -- REMOVED
            '"   LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " -- ADDED
            'S.SANDEEP [06 AUG 2016] -- END

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE hrtraining_enrollment_tran.iscancel = 0 AND hrtraining_enrollment_tran.isvoid = 0 " & _
                    "	AND hrtraining_scheduling.iscancel = 0 AND hrtraining_scheduling.isvoid = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Shani(24-Aug-2015) -- End

            If mintDepartmentId > 0 Then
                StrQ &= " AND hremployee_master.departmentunkid = " & mintDepartmentId
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND hrtraining_enrollment_tran.employeeunkid = " & mintEmployeeId
            End If

            If mintCourseType > 0 Then
                StrQ &= " AND cfcommon_master.masterunkid = " & mintCourseType
            End If

            If mintInstituteId > 0 Then
                StrQ &= " AND hrinstitute_master.instituteunkid = " & mintInstituteId
            End If

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            If mintAllocationTypeId > 0 Then
                StrQ &= " AND hrtraining_scheduling.allocationtypeunkid = '" & mintAllocationTypeId & "' "
            End If
            'S.SANDEEP [08-FEB-2017] -- END


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'If mstrTrainingCourseId.Trim <> "" Then
            '    StrQ &= " AND hrtraining_scheduling.course_title LIKE '%" & mstrTrainingCourseId.Trim & "%'"
            'End If
            If mintTrainingCourseId > 0 Then
                StrQ &= " AND ISNULL(CAST((CASE WHEN ISNUMERIC(hrtraining_scheduling.course_title) <> 0 THEN hrtraining_scheduling.course_title ELSE 0 END)AS Int),0) = " & mintTrainingCourseId
            End If
            'Anjan (10 Feb 2012)-End 




            If mstrEmpCode.Trim <> "" Then
                StrQ &= " AND hremployee_master.employeecode = '" & mstrEmpCode & "'"
            End If

            If mintStatusId > 0 Then
                StrQ &= " AND hrtraining_enrollment_tran.status_id = '" & mintStatusId & "'"
            End If
            dsGrandTot = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [ 05 JULY 2011 ] -- END 


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim intEmpId As Integer = 0
            Dim intDeptId As Integer = 0
            Dim dtFilterTable As DataTable = Nothing  'TO STORE FILTERED DATA
            Dim mdicIsEmpAdded As New Dictionary(Of Integer, Integer)   'TO PREVENT THE DATA DUPLICATION OF EMPLOYEE

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Select Case mintReportTypeId
                    Case 0
                        intDeptId = Convert.ToInt32(dtRow.Item("deptid"))
                        If mdicIsEmpAdded.ContainsKey(intDeptId) Then Continue For
                        mdicIsEmpAdded.Add(intDeptId, intDeptId)

                    Case 1
                        intEmpId = Convert.ToInt32(dtRow.Item("empid"))
                        If mdicIsEmpAdded.ContainsKey(intEmpId) Then Continue For
                        mdicIsEmpAdded.Add(intEmpId, intEmpId)
                End Select



                Select Case mintReportTypeId
                    Case 0
                        dtFilterTable = New DataView(dsList.Tables("List"), "deptid = '" & intDeptId & "'", "", DataViewRowState.CurrentRows).ToTable
                    Case 1
                        dtFilterTable = New DataView(dsList.Tables("List"), "empid = '" & intEmpId & "'", "", DataViewRowState.CurrentRows).ToTable
                End Select

                Dim rpt_Row As DataRow = Nothing

                If dtFilterTable.Rows.Count > 0 Then
                    For Each dtFRow As DataRow In dtFilterTable.Rows
                        rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                        'S.SANDEEP [08-FEB-2017] -- START
                        'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                        Dim objTrainingAlloc As New clsTraining_Allocation_Tran
                        If mstrAllocationIds.Trim.Length > 0 Then
                            If objTrainingAlloc.GetCSV_AllocationName(dtRow.Item("allocationtypeunkid"), dtRow.Item("trainingschedulingunkid"), mstrAllocationIds).Trim.Length <= 0 Then Continue For
                        End If
                        'S.SANDEEP [08-FEB-2017] -- END
                        rpt_Row.Item("Column1") = dtFRow.Item("departmentname").ToString
                        rpt_Row.Item("Column2") = dtFRow.Item("empcode").ToString
                        rpt_Row.Item("Column3") = dtFRow.Item("employee").ToString
                        rpt_Row.Item("Column4") = dtFRow.Item("institute").ToString
                        rpt_Row.Item("Column5") = dtFRow.Item("course").ToString
                        rpt_Row.Item("Column6") = dtFRow.Item("coursetype").ToString
                        rpt_Row.Item("Column7") = dtFRow.Item("status").ToString
                        rpt_Row.Item("Column8") = Format(dtFRow.Item("fees"), GUI.fmtCurrency)
                        rpt_Row.Item("Column9") = Format(dtFRow.Item("accomodation"), GUI.fmtCurrency)
                        rpt_Row.Item("Column10") = Format(dtFRow.Item("travel"), GUI.fmtCurrency)
                        rpt_Row.Item("Column11") = Format(dtFRow.Item("meals"), GUI.fmtCurrency)
                        rpt_Row.Item("Column12") = Format(dtFRow.Item("exam"), GUI.fmtCurrency)
                        rpt_Row.Item("Column13") = Format(dtFRow.Item("misc"), GUI.fmtCurrency)
                        rpt_Row.Item("Column14") = Format(dtFRow.Item("allowance"), GUI.fmtCurrency)
                        rpt_Row.Item("Column15") = Format(dtFRow.Item("materials"), GUI.fmtCurrency)
                        rpt_Row.Item("Column16") = Format(dtFRow.Item("totalcost"), GUI.fmtCurrency)
                        rpt_Row.Item("Column17") = dtFRow.Item("deptid")

                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                    Next
                End If
                Dim rpt_dtRow() As DataRow = Nothing
                Select Case mintReportTypeId
                    Case 0
                        rpt_dtRow = rpt_Data.Tables("ArutiTable").Select("Column17 = '" & dtRow.Item("deptid") & "'")
                    Case 1
                        rpt_dtRow = rpt_Data.Tables("ArutiTable").Select("Column2 = '" & dtRow.Item("empcode") & "'")
                End Select

                If rpt_dtRow.Length > 0 Then
                    Dim dtTemp() As DataRow = Nothing
                    If dsGrpTot.Tables(0).Rows.Count > 0 Then
                        Select Case mintReportTypeId
                            Case 0
                                dtTemp = dsGrpTot.Tables(0).Select("departmentunkid = '" & intDeptId & "'")
                            Case 1
                                dtTemp = dsGrpTot.Tables(0).Select("employeeunkid = '" & intEmpId & "'")
                        End Select
                    End If
                    If dtTemp.Length > 0 Then
                        For i As Integer = 0 To rpt_dtRow.Length - 1
                            rpt_dtRow(i).Item("Column72") = Format(dtTemp(0).Item("fees"), GUI.fmtCurrency)
                            rpt_dtRow(i).Item("Column73") = Format(dtTemp(0).Item("accomodation"), GUI.fmtCurrency)
                            rpt_dtRow(i).Item("Column74") = Format(dtTemp(0).Item("travel"), GUI.fmtCurrency)
                            rpt_dtRow(i).Item("Column75") = Format(dtTemp(0).Item("meals"), GUI.fmtCurrency)
                            rpt_dtRow(i).Item("Column76") = Format(dtTemp(0).Item("exam"), GUI.fmtCurrency)
                            rpt_dtRow(i).Item("Column77") = Format(dtTemp(0).Item("misc"), GUI.fmtCurrency)
                            rpt_dtRow(i).Item("Column78") = Format(dtTemp(0).Item("allowance"), GUI.fmtCurrency)
                            rpt_dtRow(i).Item("Column79") = Format(dtTemp(0).Item("materials"), GUI.fmtCurrency)
                            rpt_dtRow(i).Item("Column80") = Format(dtTemp(0).Item("totalcost"), GUI.fmtCurrency)
                            rpt_Data.Tables("ArutiTable").AcceptChanges()
                        Next
                    End If
                End If
            Next



            'For Each dtRow As DataRow In dsList.Tables(0).Rows
            '    Dim rpt_Row As DataRow
            '    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
            '    Select Case mintReportTypeId
            '        Case 0
            '            rpt_Row.Item("Column1") = dtRow.Item("departmentname").ToString
            '        Case 1
            '            rpt_Row.Item("Column1") = dtRow.Item("employee").ToString
            '    End Select
            '    rpt_Row.Item("Column2") = dtRow.Item("empcode").ToString
            '    rpt_Row.Item("Column3") = dtRow.Item("employee").ToString
            '    rpt_Row.Item("Column4") = dtRow.Item("institute").ToString
            '    rpt_Row.Item("Column5") = dtRow.Item("course").ToString
            '    rpt_Row.Item("Column6") = dtRow.Item("coursetype").ToString
            '    rpt_Row.Item("Column7") = dtRow.Item("status").ToString
            '    rpt_Row.Item("Column8") = Format(dtRow.Item("fees"), GUI.fmtCurrency)
            '    rpt_Row.Item("Column9") = Format(dtRow.Item("accomodation"), GUI.fmtCurrency)
            '    rpt_Row.Item("Column10") = Format(dtRow.Item("travel"), GUI.fmtCurrency)
            '    rpt_Row.Item("Column11") = Format(dtRow.Item("meals"), GUI.fmtCurrency)
            '    rpt_Row.Item("Column12") = Format(dtRow.Item("exam"), GUI.fmtCurrency)
            '    rpt_Row.Item("Column13") = Format(dtRow.Item("misc"), GUI.fmtCurrency)
            '    rpt_Row.Item("Column14") = Format(dtRow.Item("allowance"), GUI.fmtCurrency)
            '    rpt_Row.Item("Column15") = Format(dtRow.Item("materials"), GUI.fmtCurrency)
            '    rpt_Row.Item("Column16") = Format(dtRow.Item("totalcost"), GUI.fmtCurrency)

            '    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            'Next

            objRpt = New ArutiReport.Designer.rptTrainingCost_DeptWise

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 21, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 22, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 23, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 24, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End 

            objRpt.SetDataSource(rpt_Data)



            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            'Select Case mintReportTypeId
            '    Case 0
            '        ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderEmployeeSection", True)
            '        ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderDeptSection", False)
            '    Case 1
            '        ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderDeptSection", True)
            '        ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderEmployeeSection", False)
            'End Select
            Select Case mintReportTypeId
                Case 0
                    ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderEmployeeSection", False)
                    ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", False)
                    ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
                Case 1
                    ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderEmployeeSection", True)
                    ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
                    ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", False)
            End Select
            'S.SANDEEP [ 05 JULY 2011 ] -- END 



            Call ReportFunction.TextChange(objRpt, "txtInstitute", Language.getMessage(mstrModuleName, 9, "Institute"))
            Call ReportFunction.TextChange(objRpt, "txtCourse", Language.getMessage(mstrModuleName, 25, "Course"))
            Call ReportFunction.TextChange(objRpt, "txtType", Language.getMessage(mstrModuleName, 10, "Type"))
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 11, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtFee", Language.getMessage(mstrModuleName, 12, "Fees"))
            Call ReportFunction.TextChange(objRpt, "txtAcc", Language.getMessage(mstrModuleName, 13, "Accomodation"))
            Call ReportFunction.TextChange(objRpt, "txtTravel", Language.getMessage(mstrModuleName, 14, "Travel"))
            Call ReportFunction.TextChange(objRpt, "txtMeals", Language.getMessage(mstrModuleName, 15, "Meals"))
            Call ReportFunction.TextChange(objRpt, "txtExam", Language.getMessage(mstrModuleName, 16, "Exam"))
            Call ReportFunction.TextChange(objRpt, "txtMisc", Language.getMessage(mstrModuleName, 17, "Misc."))
            Call ReportFunction.TextChange(objRpt, "txtAllowance", Language.getMessage(mstrModuleName, 18, "Allowance"))
            Call ReportFunction.TextChange(objRpt, "txtMaterial", Language.getMessage(mstrModuleName, 19, "Materials"))
            Call ReportFunction.TextChange(objRpt, "txtTotalCost", Language.getMessage(mstrModuleName, 20, "Total Cost"))

            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            'Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 124, "Employees #"))
            'Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 125, "Employees Name"))
            'S.SANDEEP [ 05 JULY 2011 ] -- END 



            Call ReportFunction.TextChange(objRpt, "txtEmployeeCode_emp", Language.getMessage(mstrModuleName, 26, "Employees #"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName_emp", Language.getMessage(mstrModuleName, 27, "Employees Name"))

            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 28, "Group Total"))
            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            Call ReportFunction.TextChange(objRpt, "txtSubTotal1", Language.getMessage(mstrModuleName, 28, "Group Total"))
            'S.SANDEEP [ 05 JULY 2011 ] -- END 
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 29, "Grand Total"))


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 30, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 31, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpCol81")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpCol91")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpCol101")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpCol111")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpCol121")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpCol131")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpCol141")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpCol151")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpCol161")

            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot81")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot91")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot101")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot111")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot121")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot131")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot141")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot151")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot161")

            If dsGrandTot.Tables(0).Rows.Count > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtGrndTot1", Format(dsGrandTot.Tables(0).Rows(0)("Fees"), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtGrndTot2", Format(dsGrandTot.Tables(0).Rows(0)("Accomodation"), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtGrndTot3", Format(dsGrandTot.Tables(0).Rows(0)("Travel"), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtGrndTot4", Format(dsGrandTot.Tables(0).Rows(0)("Meals"), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtGrndTot5", Format(dsGrandTot.Tables(0).Rows(0)("Exam"), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtGrndTot6", Format(dsGrandTot.Tables(0).Rows(0)("Misc"), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtGrndTot7", Format(dsGrandTot.Tables(0).Rows(0)("Allowance"), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtGrndTot8", Format(dsGrandTot.Tables(0).Rows(0)("Materials"), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtGrndTot9", Format(dsGrandTot.Tables(0).Rows(0)("totalcost"), GUI.fmtCurrency))
            End If
            'S.SANDEEP [ 05 JULY 2011 ] -- END 


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Institute Name :")
            Language.setMessage(mstrModuleName, 2, "Course Title :")
            Language.setMessage(mstrModuleName, 3, "Course Type :")
            Language.setMessage(mstrModuleName, 4, "Department :")
            Language.setMessage(mstrModuleName, 5, "Emp Code :")
            Language.setMessage(mstrModuleName, 6, "Status :")
            Language.setMessage(mstrModuleName, 7, "Employee Name :")
            Language.setMessage(mstrModuleName, 8, "Order By :")
            Language.setMessage(mstrModuleName, 9, "Institute")
            Language.setMessage(mstrModuleName, 10, "Type")
            Language.setMessage(mstrModuleName, 11, "Status")
            Language.setMessage(mstrModuleName, 12, "Fees")
            Language.setMessage(mstrModuleName, 13, "Accomodation")
            Language.setMessage(mstrModuleName, 14, "Travel")
            Language.setMessage(mstrModuleName, 15, "Meals")
            Language.setMessage(mstrModuleName, 16, "Exam")
            Language.setMessage(mstrModuleName, 17, "Misc.")
            Language.setMessage(mstrModuleName, 18, "Allowance")
            Language.setMessage(mstrModuleName, 19, "Materials")
            Language.setMessage(mstrModuleName, 20, "Total Cost")
            Language.setMessage(mstrModuleName, 21, "Prepared By :")
            Language.setMessage(mstrModuleName, 22, "Checked By :")
            Language.setMessage(mstrModuleName, 23, "Approved By :")
            Language.setMessage(mstrModuleName, 24, "Received By :")
            Language.setMessage(mstrModuleName, 25, "Course")
            Language.setMessage(mstrModuleName, 26, "Employees #")
            Language.setMessage(mstrModuleName, 27, "Employees Name")
            Language.setMessage(mstrModuleName, 28, "Group Total")
            Language.setMessage(mstrModuleName, 29, "Grand Total")
            Language.setMessage(mstrModuleName, 30, "Printed By :")
            Language.setMessage(mstrModuleName, 31, "Printed Date :")
            Language.setMessage(mstrModuleName, 32, "Enrolled")
            Language.setMessage(mstrModuleName, 33, "Postponed")
            Language.setMessage(mstrModuleName, 34, "Completed")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
