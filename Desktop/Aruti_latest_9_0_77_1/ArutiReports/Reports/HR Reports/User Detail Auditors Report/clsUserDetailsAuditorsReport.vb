'************************************************************************************************************************************
'Class Name : clsUserDetailsAuditorsReport.vb
'Purpose    :
'Date       : 27/01/2017
'Written By : Anjan Marfatia
'Modified   :
'************************************************************************************************************************************

#Region " Import "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

Public Class clsUserDetailsAuditorsReport
    Inherits IReportData

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "clsUserDetailsAuditorsReport"
    Private mstrReportId As String = enArutiReport.UserDetail_Auditors_Report
    Private mdtdate1 As Date = Nothing
    Private mdtdate2 As Date = Nothing

#End Region

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Properties "

    Public WriteOnly Property _mdtdate1() As Date
        Set(ByVal value As Date)
            mdtdate1 = value
        End Set
    End Property

    Public WriteOnly Property _mdtdate2() As Date
        Set(ByVal value As Date)
            mdtdate2 = value
        End Set
    End Property
   
#End Region

#Region "Private Methods"

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Sub SetDefaultValue()
        Try
            mdtdate1 = Nothing
            mdtdate2 = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_User_Details()
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Try
            Dim dtTable As DataTable = Nothing
            Dim objCompany As New clsCompany_Master
            dtTable = objCompany.Get_DataBaseList("Database", False)
            If dtTable.Rows.Count > 0 Then

                'StrQ = "SELECT " & _
                '                 "CONVERT(NVARCHAR(24),EM.appointeddate,106) AS [Appointment Date] " & _
                '                 ",EM.employeecode AS [Employee Code] " & _
                '                 ",EM.firstname+' '+ISNULL(EM.othername,'')+' '+EM.surname AS [Employee Name] " & _
                '                 ",JM.job_name AS [Job Name] " & _
                '                 ",ISNULL(CONVERT(NVARCHAR(8),ETERM.empl_enddate,112),'') AS [EOC Date] " & _
                '                 ",ISNULL(CONVERT(NVARCHAR(8),ETERM.termination_from_date,112),'') AS [Leaving Date] " & _
                '                 ",ISNULL(CONVERT(NVARCHAR(8),ERET.termination_to_date,112),'') AS [Retirement Date] " & _
                '                 ",CASE WHEN CONVERT(CHAR(8),EM.appointeddate, 112) <= @enddate " & _
                '                  "AND ISNULL(CONVERT(CHAR(8),ETERM.termination_from_date, 112),@startdate) >= @startdate " & _
                '                  "AND ISNULL(CONVERT(CHAR(8),ERET.termination_to_date, 112),@startdate) >= @startdate " & _
                '                  "AND ISNULL(CONVERT(CHAR(8),ETERM.empl_enddate, 112),@startdate) >= @startdate " & _
                '                  "THEN '@Active' ELSE '@Inactive' END AS [Active Status] " & _
                '                 ",ISNULL(Usr.username,'') AS [Created By User] " & _
                '                 ",ISNULL(CONVERT(NVARCHAR(8),ATM.auditdatetime,112),'') AS [Created Date] " & _
                '                 ",RM.name AS [User Role] " & _
                '      "FROM  hremployee_master AS EM " & _
                '          "LEFT JOIN " & _
                '                "( " & _
                '                    "SELECT " & _
                '                         "Cat.CatEmpId " & _
                '                        ",Cat.jobgroupunkid " & _
                '                        ",Cat.jobunkid " & _
                '                        ",Cat.CEfDt " & _
                '                     "FROM " & _
                '                    "( " & _
                '                        "SELECT " & _
                '                             "ECT.employeeunkid AS CatEmpId " & _
                '                            ",ECT.jobgroupunkid " & _
                '                            ",ECT.jobunkid " & _
                '                            ",CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                '                            ",ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                '                        "FROM hremployee_categorization_tran AS ECT " & _
                '                        "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @enddate " & _
                '                        ") AS Cat WHERE Cat.Rno = 1 " & _
                '                    ") AS ERECAT ON ERECAT.CatEmpId = EM.employeeunkid " & _
                '       "JOIN  hrjob_master AS JM ON JM.jobunkid = ERECAT.jobunkid " & _
                '       "LEFT JOIN " & _
                '                "( " & _
                '                    "SELECT " & _
                '                               "TERM.TEEmpId " & _
                '                              ",TERM.empl_enddate " & _
                '                              ",TERM.termination_from_date " & _
                '                              ",TERM.TEfDt " & _
                '                              ",TERM.isexclude_payroll " & _
                '                    "FROM " & _
                '                    "( " & _
                '                        "SELECT " & _
                '                             "TRM.employeeunkid AS TEEmpId " & _
                '                            ",CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                '                            ",CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                '                            ",CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                '                            ",TRM.isexclude_payroll " & _
                '                            ",ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                '                             "FROM hremployee_dates_tran AS TRM " & _
                '                           "WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "'  AND CONVERT(CHAR(8),TRM.effectivedate,112) <= @enddate " & _
                '                    ") AS TERM WHERE TERM.Rno = 1 " & _
                '                ") AS ETERM ON ETERM.TEEmpId = EM.employeeunkid " & _
                '        "LEFT JOIN " & _
                '                "( " & _
                '                    "SELECT " & _
                '                          "RET.REmpId " & _
                '                         ",RET.termination_to_date " & _
                '                         ",RET.REfDt " & _
                '                     "FROM " & _
                '                    "( " & _
                '                        "SELECT " & _
                '                             "RTD.employeeunkid AS REmpId " & _
                '                            ",CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                '                            ",CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                '                            ",ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                '                        "FROM hremployee_dates_tran AS RTD " & _
                '                          "WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= @enddate " & _
                '                    ") AS RET WHERE RET.Rno = 1 " & _
                '                ") AS ERET ON ERET.REmpId = EM.employeeunkid " & _
                '          "JOIN  athremployee_master AS ATM ON ATM.employeeunkid = EM.employeeunkid " & _
                '          "LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON ATM.audituserunkid = Usr.userunkid " & _
                '          "LEFT JOIN hrmsConfiguration..cfrole_master AS RM ON RM.roleunkid = Usr.roleunkid " & _
                '          "LEFT JOIN " & _
                '               "( SELECT " & _
                '                  "a.userunkid " & _
                '                  ",logindate,LOGIN_FROM " & _
                '                  "FROM " & _
                '                  "( " & _
                '                        "SELECT " & _
                '                            "cfuser_tracking_tran.userunkid,CASE WHEN login_modeid = 1 THEN @DESKTOP " & _
                '                                                                "WHEN login_modeid = 2 THEN @ESS " & _
                '                                                                 "WHEN login_modeid = 3 THEN @MSS END AS LOGIN_FROM " & _
                '                            ",ROW_NUMBER()OVER(PARTITION BY cfuser_tracking_tran.userunkid ORDER BY logindate DESC) AS rno " & _
                '                            ",logindate " & _
                '                        "FROM hrmsConfiguration..cfuser_tracking_tran " & _
                '                        ") AS A WHERE A.rno = 1 " & _
                '            ") AS B ON b.userunkid = EM.employeeunkid " & _
                '            "WHERE ATM.audittype = 1 "


                StrQ = "SELECT " & _
                        "ISNULL(CONVERT(NVARCHAR(8),EM.appointeddate,112),'') AS [Appointment Date] " & _
                        ",ISNULL(EM.employeecode,'') as [Employee Code] " & _
                        ",CASE WHEN Usr.employeeunkid > 0 THEN EM.firstname + ' '+ EM.othername + ' ' + EM.surname " & _
                               "WHEN Usr.employeeunkid <= 0 THEN CASE WHEN Usr.firstname+ ' '+ Usr.lastname is null THEN Usr.username ELSE Usr.firstname+ ' '+ Usr.lastname " & _
                         "END END AS [Employee Name] " & _
                        ",ISNULL(JM.job_name,'') AS [Job Name] " & _
                        ",ISNULL(CONVERT(NVARCHAR(8),ETERM.empl_enddate,112),'') AS [EOC Date] " & _
                        ",ISNULL(CONVERT(NVARCHAR(8),ETERM.termination_from_date,112),'') AS [Leaving Date] " & _
                        ",ISNULL(CONVERT(NVARCHAR(8),ERET.termination_to_date,112),'') AS [Retirement Date] " & _
                        ",CASE WHEN Usr.employeeunkid > 0 THEN CASE WHEN CONVERT(CHAR(8),EM.appointeddate, 112) <= @enddate " & _
                                                          "AND ISNULL(CONVERT(CHAR(8),ETERM.termination_from_date, 112),@startdate) >= @startdate " & _
                                                          "AND ISNULL(CONVERT(CHAR(8),ERET.termination_to_date, 112),@startdate) >= @startdate " & _
                                                          "AND ISNULL(CONVERT(CHAR(8),ETERM.empl_enddate, 112),@startdate) >= @startdate " & _
                                                          "THEN @Active ELSE @Inactive END " & _
                             "WHEN Usr.employeeunkid <= 0 THEN CASE WHEN usr.isactive=1 THEN @Active ELSE @Inactive END  END AS [Active Status] " & _
                        ",ISNULL(URM.name, '') AS [User Role] " & _
                        ",lgdt.ldate AS [Last Login Date] " & _
                        ",lgdt.ltime AS [Last Login Time] " & _
                        ",ISNULL(ByUsr.username,'') as [CreatedBy] " & _
                        ",ISNULL(CONVERT(NVARCHAR(8),Usr.creationdate,112),'') AS [Created Date] " & _
                        ",ISNULL(RM.name,'') as [Created By User Role] " & _
                        "FROM hrmsConfiguration..cfuser_master as Usr " & _
                        " LEFT join hrmsConfiguration..cfuser_master as ByUsr on usr.createdbyunkid=byusr.userunkid " & _
                        " LEFT JOIN hrmsConfiguration..cfrole_master as RM on RM.roleunkid=ByUsr.roleunkid " & _
                        " LEFT JOIN hrmsConfiguration..cfrole_master AS URM ON URM.roleunkid = usr.roleunkid " & _
                        " LEFT JOIN hremployee_master AS EM on Usr.employeeunkid = EM.employeeunkid AND Usr.companyunkid = EM.companyunkid " & _
                        " LEFT JOIN ( " & _
                        "               SELECT " & _
                        "                       userunkid AS Uid " & _
                        "                      ,ISNULL(CONVERT(CHAR(8),MAX(logindate),112),'') AS [ldate]  " & _
                        "                      ,ISNULL(CONVERT(CHAR(8),MAX(logindate),108),'') AS [ltime]  " & _
                        "               FROM hrmsconfiguration..cfuser_tracking_tran " & _
                        "               GROUP BY userunkid " & _
                        "   )  AS lgdt ON lgdt.Uid = Usr.userunkid " & _
                        " LEFT JOIN " & _
                                "( SELECT " & _
                                    "Cat.CatEmpId " & _
                                   ",Cat.jobgroupunkid " & _
                                   ",Cat.jobunkid " & _
                                   ",Cat.CEfDt " & _
                                  "FROM " & _
                                   "( " & _
                                      "SELECT " & _
                                           "ECT.employeeunkid AS CatEmpId " & _
                                           ",ECT.jobgroupunkid " & _
                                           ",ECT.jobunkid " & _
                                           ",CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                                           ",ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                                      "FROM hremployee_categorization_tran AS ECT " & _
                                          "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @enddate " & _
                                         ") AS Cat WHERE Cat.Rno = 1 " & _
                                     ") AS ERECAT ON ERECAT.CatEmpId = EM.employeeunkid " & _
                                      "LEFT JOIN  hrjob_master AS JM ON JM.jobunkid = ERECAT.jobunkid " & _
                         "LEFT JOIN " & _
                                      "( " & _
                                        "SELECT " & _
                                             "TERM.TEEmpId " & _
                                             ",TERM.empl_enddate " & _
                                             ",TERM.termination_from_date " & _
                                             ",TERM.TEfDt " & _
                                             ",TERM.isexclude_payroll " & _
                                        "FROM " & _
                                           "( " & _
                                        "SELECT " & _
                                              "TRM.employeeunkid AS TEEmpId " & _
                                              ",CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                                              ",CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                                              ",CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                                              ",TRM.isexclude_payroll " & _
                                              ",ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                                              "FROM hremployee_dates_tran AS TRM " & _
                                         "WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "'  AND CONVERT(CHAR(8),TRM.effectivedate,112) <= @enddate " & _
                                             ") AS TERM WHERE TERM.Rno = 1 " & _
                                             ") AS ETERM ON ETERM.TEEmpId = EM.employeeunkid " & _
                         "LEFT JOIN " & _
                                                        "( " & _
                                                            "SELECT " & _
                                                                  "RET.REmpId " & _
                                                                 ",RET.termination_to_date " & _
                                                                 ",RET.REfDt " & _
                                                             "FROM " & _
                                                            "( " & _
                                                                "SELECT " & _
                                                                     "RTD.employeeunkid AS REmpId " & _
                                                                    ",CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                                                                    ",CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                                                                    ",ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                                                                "FROM hremployee_dates_tran AS RTD " & _
                                                                  "WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= @enddate " & _
                                                            ") AS RET WHERE RET.Rno = 1 " & _
                                                        ") AS ERET ON ERET.REmpId = EM.employeeunkid " & _
                                                 "WHERE CASE WHEN Usr.employeeunkid > 0 THEN EM.firstname + ' '+ EM.othername + ' ' + EM.surname " & _
                                                        "WHEN Usr.employeeunkid <= 0 THEN CASE WHEN Usr.firstname+ ' '+ Usr.lastname is null THEN Usr.username ELSE Usr.firstname+ ' '+ Usr.lastname " & _
                                                        "END END <> '' "



                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtdate1).ToString)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtdate2).ToString)

                objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Active"))
                objDataOperation.AddParameter("@Inactive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Inactive"))

                'objDataOperation.AddParameter("@DESKTOP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 113, "DESKTOP"))
                'objDataOperation.AddParameter("@MSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 114, "MSS"))
                'objDataOperation.AddParameter("@ESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 115, "ESS"))


                Dim dsUser As New DataSet
                dsUser = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                For Each dr As DataRow In dsUser.Tables(0).Rows

                    If dr("Appointment Date").ToString().Trim.Length > 0 Then
                        dr("Appointment Date") = eZeeDate.convertDate(dr("Appointment Date").ToString).ToShortDateString
                    End If
                    If dr("EOC Date").ToString().Trim.Length > 0 Then
                        dr("EOC Date") = eZeeDate.convertDate(dr("EOC Date").ToString).ToShortDateString
                    End If
                    If dr("Leaving Date").ToString().Trim.Length > 0 Then
                        dr("Leaving Date") = eZeeDate.convertDate(dr("Leaving Date").ToString).ToShortDateString
                    End If
                    If dr("Retirement Date").ToString().Trim.Length > 0 Then
                        dr("Retirement Date") = eZeeDate.convertDate(dr("Retirement Date").ToString).ToShortDateString
                    End If
                    If dr("Created Date").ToString().Trim.Length > 0 Then
                        dr("Created Date") = eZeeDate.convertDate(dr("Created Date").ToString).ToShortDateString
                    End If
                    If dr("Last Login Date").ToString().Trim.Length > 0 Then
                        dr("Last Login Date") = eZeeDate.convertDate(dr("Last Login Date").ToString).ToShortDateString
                    End If
                Next
                dsUser.Tables(0).AcceptChanges()

                Dim intArrayColumnWidth As Integer() = Nothing
                ReDim intArrayColumnWidth(dsUser.Tables(0).Columns.Count - 1)
                For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    intArrayColumnWidth(i) = 125
                Next


                Dim strExtraFilter As String = String.Empty
                strExtraFilter &= Language.getMessage(mstrModuleName, 3, "FROM : ") & " " & mdtdate1.Date & Language.getMessage(mstrModuleName, 4, " TO ") & mdtdate2.Date

                Dim strarrGroupColumns As String() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell


                row = New WorksheetRow()
                wcell = New WorksheetCell(strExtraFilter, "s10bw")
                wcell.MergeAcross = dsUser.Tables(0).Columns.Count - 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)


                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)



                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dsUser.Tables(0), intArrayColumnWidth, True, True, True, Nothing, Me._ReportName, "", " ", Nothing, "", True, rowsArrayHeader, rowsArrayFooter, Nothing)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Public Sub Generate_User_Ability()
    '    Dim StrQ As String = String.Empty
    '    Dim objDataOperation As New clsDataOperation
    '    Try
    '        Dim dtUserAbility As New DataTable("User")
    '        Dim dCol As DataColumn
    '        dCol = New DataColumn("column1")
    '        dCol.Caption = Language.getMessage(mstrModuleName, 16, "User")
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtUserAbility.Columns.Add(dCol)

    '        dCol = New DataColumn("column2")
    '        dCol.Caption = Language.getMessage(mstrModuleName, 17, "Module Name")
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtUserAbility.Columns.Add(dCol)

    '        dCol = New DataColumn("column3")
    '        dCol.Caption = Language.getMessage(mstrModuleName, 18, "Event Type")
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtUserAbility.Columns.Add(dCol)

    '        dCol = New DataColumn("column4")
    '        dCol.Caption = Language.getMessage(mstrModuleName, 19, "Audit Date")
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtUserAbility.Columns.Add(dCol)

    '        dCol = New DataColumn("column5")
    '        dCol.Caption = Language.getMessage(mstrModuleName, 20, "Audit Time")
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtUserAbility.Columns.Add(dCol)

    '        dCol = New DataColumn("column6")
    '        dCol.Caption = Language.getMessage(mstrModuleName, 21, "User Role")
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtUserAbility.Columns.Add(dCol)

    '        dCol = New DataColumn("column7")
    '        dCol.Caption = Language.getMessage(mstrModuleName, 22, "Changed For User")
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtUserAbility.Columns.Add(dCol)

    '        dCol = New DataColumn("column8")
    '        dCol.Caption = Language.getMessage(mstrModuleName, 23, "Privilege Group")
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtUserAbility.Columns.Add(dCol)

    '        dCol = New DataColumn("column9")
    '        dCol.Caption = Language.getMessage(mstrModuleName, 24, "Privilege")
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtUserAbility.Columns.Add(dCol)

    '        StrQ = "SELECT " & _
    '                      "ISNULL(hrmsconfiguration..cfuser_master.username, '') AS column1, " & _
    '                      "ISNULL(CASE " & _
    '                        "WHEN @LangId = 0 THEN REPLACE(Mod1.LANGUAGE, '&', '') " & _
    '                        "WHEN @LangId = 1 THEN REPLACE(Mod1.language1, '&', '') " & _
    '                        "WHEN @LangId = 2 THEN REPLACE(Mod1.language2, '&', '') " & _
    '                      "END, 'module_name1') + CASE " & _
    '                        "WHEN ISNULL(atconfigcommon_log.module_name2, '') <> '' THEN +' -> ' + CASE " & _
    '                            "WHEN @LangId = 0 THEN REPLACE(Mod2.LANGUAGE, '&', '') " & _
    '                            "WHEN @LangId = 1 THEN REPLACE(Mod2.language1, '&', '') " & _
    '                            "WHEN @LangId = 2 THEN REPLACE(Mod2.language2, '&', '') " & _
    '                          "END " & _
    '                        "ELSE '' " & _
    '                      "END + CASE " & _
    '                        "WHEN ISNULL(atconfigcommon_log.module_name3, '') <> '' THEN +' -> ' + CASE " & _
    '                            "WHEN @LangId = 0 THEN REPLACE(Mod3.LANGUAGE, '&', '') " & _
    '                            "WHEN @LangId = 1 THEN REPLACE(Mod3.language1, '&', '') " & _
    '                            "WHEN @LangId = 2 THEN REPLACE(Mod3.language2, '&', '') " & _
    '                          "END " & _
    '                        "ELSE '' " & _
    '                      "END + CASE " & _
    '                        "WHEN ISNULL(atconfigcommon_log.module_name4, '') <> '' THEN +' -> ' + CASE " & _
    '                            "WHEN @LangId = 0 THEN REPLACE(Mod4.LANGUAGE, '&', '') " & _
    '                            "WHEN @LangId = 1 THEN REPLACE(Mod4.language1, '&', '') " & _
    '                            "WHEN @LangId = 2 THEN REPLACE(Mod4.language2, '&', '') " & _
    '                          "END " & _
    '                        "ELSE '' " & _
    '                      "END + CASE " & _
    '                        "WHEN ISNULL(atconfigcommon_log.module_name5, '') <> '' THEN +' -> ' + CASE " & _
    '                            "WHEN @LangId = 0 THEN REPLACE(Mod5.LANGUAGE, '&', '') " & _
    '                            "WHEN @LangId = 1 THEN REPLACE(Mod5.language1, '&', '') " & _
    '                            "WHEN @LangId = 2 THEN REPLACE(Mod5.language2, '&', '') " & _
    '                          "END " & _
    '                        "ELSE '' " & _
    '                      "END AS column2, " & _
    '                      "CASE " & _
    '                        "WHEN atconfigcommon_log.audittype = 1 THEN 'Add' " & _
    '                        "WHEN atconfigcommon_log.audittype = 2 THEN 'Edit' " & _
    '                        "WHEN atconfigcommon_log.audittype = 3 THEN 'Delete' " & _
    '                      "END AS column3, " & _
    '                      "CONVERT(char(8), atconfigcommon_log.auditdatetime, 112) AS column4, " & _
    '                      "CONVERT(char(8), atconfigcommon_log.auditdatetime, 108) AS column5, " & _
    '                      "ISNULL(hrmsConfiguration..cfrole_master.name, '') AS column6 , " & _
    '                      "ISNULL(CUsr.username, '') AS column7 , " & _
    '                      "ISNULL(hrmsConfiguration..cfuserprivilegegroup_master.name, '') AS column8, " & _
    '                      "ISNULL(hrmsConfiguration..cfuserprivilege_master.privilege_name, '') AS column9 " & _
    '                    "FROM atconfigcommon_log " & _
    '                         "LEFT JOIN cflanguage AS Mod1 ON Mod1.controlname = atconfigcommon_log.module_name1 " & _
    '                         "LEFT JOIN cflanguage AS Mod2 ON Mod2.controlname = atconfigcommon_log.module_name2 " & _
    '                         "LEFT JOIN cflanguage AS Mod3 ON Mod3.controlname = atconfigcommon_log.module_name3 " & _
    '                         "LEFT JOIN cflanguage AS Mod4 ON Mod4.controlname = atconfigcommon_log.module_name4 " & _
    '                         "LEFT JOIN cflanguage AS Mod5 ON Mod5.controlname = atconfigcommon_log.module_name5 " & _
    '                         "JOIN hrmsconfiguration..cfuser_master ON hrmsconfiguration..cfuser_master.userunkid = atconfigcommon_log.audituserunkid " & _
    '                         "JOIN hrmsConfiguration..cfuserprivilege_master ON hrmsConfiguration..cfuserprivilege_master.privilegeunkid = [value].value('(cfuser_privilege/privilegeunkid)[1]', 'INT') " & _
    '                         "JOIN hrmsConfiguration..cfuserprivilegegroup_master ON hrmsConfiguration..cfuserprivilegegroup_master.privilegegroupunkid = hrmsConfiguration..cfuserprivilege_master.privilegegroupunkid " & _
    '                         "JOIN hrmsConfiguration..cfuser_master AS CUsr ON CUsr.userunkid = [value].value('(cfuser_privilege/userunkid)[1]', 'INT') " & _
    '                         "JOIN hrmsConfiguration..cfrole_master ON hrmsConfiguration..cfrole_master.roleunkid = CUsr.roleunkid " & _
    '                    "WHERE form_name = 'frmAbilityLevel_Privilege' " & _
    '                    "AND CONVERT(char(8), atconfigcommon_log.auditdatetime, 112) BETWEEN CONVERT(CHAR(8), CAST('" & mdtdate1.Date & "' AS DATETIME), 112) AND CONVERT(CHAR(8), CAST('" & mdtdate2.Date & "' AS DATETIME), 112) "

    '        objDataOperation.AddParameter("@LangId", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Languageunkid)
    '        Dim dsUser As New DataSet
    '        dsUser = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        For Each dr As DataRow In dsUser.Tables(0).Rows
    '            If dr("column4").ToString().Trim.Length > 0 Then
    '                dr("column4") = eZeeDate.convertDate(dr("column4").ToString).ToShortDateString
    '            End If
    '            dtUserAbility.ImportRow(dr)
    '        Next

    '        Dim intArrayColumnWidth As Integer() = Nothing
    '        ReDim intArrayColumnWidth(dtUserAbility.Columns.Count - 1)
    '        For i As Integer = 0 To intArrayColumnWidth.Length - 1
    '            intArrayColumnWidth(i) = 125
    '        Next

    '        Dim strExtraFilter As String = String.Empty
    '        strExtraFilter = Language.getMessage(mstrModuleName, 25, "From Date : ") & " " & mdtdate1.Date & " " & Language.getMessage(mstrModuleName, 26, "To") & " " & mdtdate2.Date & " "
    '        Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dtUserAbility, intArrayColumnWidth, True, True, True, Nothing, mstrReportTypeName, "", strExtraFilter, Nothing, "", True, Nothing, Nothing, Nothing)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_User_Ability; Module Name: " & mstrModuleName
    '    End Try
    'End Sub
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Active")
            Language.setMessage(mstrModuleName, 2, "Inactive")
            Language.setMessage(mstrModuleName, 3, "FROM :")
            Language.setMessage(mstrModuleName, 4, " TO")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
