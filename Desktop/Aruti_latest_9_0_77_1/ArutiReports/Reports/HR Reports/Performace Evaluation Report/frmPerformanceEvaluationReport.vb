'************************************************************************************************************************************
'Class Name : frmPerformanceEvaluationReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPerformanceEvaluationReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPerformanceEvaluationReport"
    Private objPerformance As clsPerformanceEvaluationReport
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mCheckedItems As ListView.CheckedListViewItemCollection

#End Region

#Region " Contructor "

    Public Sub New()
        objPerformance = New clsPerformanceEvaluationReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objPerformance.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objMData As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True)
            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = objMData.GetEAllocation_Notification("List")
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dtTable As DataTable = New DataView(dsCombos.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & ")", "", DataViewRowState.CurrentRows).ToTable
            Dim dtTable As DataTable = New DataView(dsCombos.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            'S.SANDEEP [04 JUN 2015] -- END
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 1
            End With

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            Dim objCScoreMaster As New clsComputeScore_master
            dsCombos = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cboScoreOption
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
            End With
            objCScoreMaster = Nothing
            'S.SANDEEP |27-MAY-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case cboAllocations.SelectedValue
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            lvAllocation.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)

                lvAllocation.Items.Add(lvItem)
            Next
            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            'If lvAllocation.Items.Count >= 5 Then
            '    colhAllocations.Width = 250 - 20
            '    objchkAll.Location = New Point(301, 117)
            'Else
            '    colhAllocations.Width = 250
            '    objchkAll.Location = New Point(321, 117)
            'End If
            'S.SANDEEP |27-MAY-2019| -- END
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Do_Operation(ByVal blnOpt As Boolean)
        Try
            For Each LItem As ListViewItem In lvAllocation.Items
                LItem.Checked = blnOpt
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            cboAllocations.SelectedValue = 1
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        objPerformance.SetDefaultValue()
        Try
            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If lvAllocation.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select atleast one allocation to export report."), enMsgBoxStyle.Information)
                lvAllocation.Focus()
                Return False
            End If

            objPerformance._PeriodId = CInt(cboPeriod.SelectedValue)
            objPerformance._PeriodName = cboPeriod.Text
            objPerformance._StartDate = mdtStartDate
            objPerformance._EndDate = mdtEndDate

            Dim StrStringIds As String = ""
            If lvAllocation.CheckedItems.Count > 0 Then
                For Each lvItem As ListViewItem In lvAllocation.CheckedItems
                    StrStringIds &= "," & lvItem.Tag.ToString
                Next
                If StrStringIds.Length > 0 Then StrStringIds = Mid(StrStringIds, 2)
            End If
            mCheckedItems = lvAllocation.CheckedItems
            objPerformance._Allocation = cboAllocations.Text

            If StrStringIds.Length > 0 Then
                Select Case cboAllocations.SelectedValue
                    Case enAllocation.BRANCH
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.stationunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrstation_master.stationunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrstation_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "stationunkid"

                    Case enAllocation.DEPARTMENT_GROUP
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.deptgroupunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrdepartment_group_master.deptgroupunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrdepartment_group_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "deptgroupunkid"

                    Case enAllocation.DEPARTMENT
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.departmentunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrdepartment_master.departmentunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrdepartment_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "departmentunkid"

                    Case enAllocation.SECTION_GROUP
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.sectiongroupunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrsectiongroup_master.sectiongroupunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrsectiongroup_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "sectiongroupunkid"

                    Case enAllocation.SECTION
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.sectionunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrsection_master.sectionunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrsection_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "sectionunkid"

                    Case enAllocation.UNIT_GROUP
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.unitgroupunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrunitgroup_master.unitgroupunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrunitgroup_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "unitgroupunkid"

                    Case enAllocation.UNIT
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.unitunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrunit_master.unitunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrunit_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "unitunkid"

                    Case enAllocation.TEAM
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.teamunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrteam_master.teamunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrteam_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "teamunkid"

                    Case enAllocation.JOB_GROUP
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.jobgroupunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrjobgroup_master.jobgroupunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrjobgroup_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "jobgroupunkid"

                    Case enAllocation.JOBS
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.jobunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrjob_master.jobunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrjob_master"
                        objPerformance._iSelectColName = "job_name"
                        objPerformance._iUnkidColName = "jobunkid"

                    Case enAllocation.CLASS_GROUP
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.classgroupunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrclassgroup_master.classgroupunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrclassgroup_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "classgroupunkid"

                    Case enAllocation.CLASSES
                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                        objPerformance._InnFilter = "T.classunkid IN(" & StrStringIds & ") "
                        'S.SANDEEP |27-JUL-2019| -- END
                        StrStringIds = "hrclasses_master.classesunkid IN(" & StrStringIds & ") "
                        objPerformance._iJoinTblName = "hrclasses_master"
                        objPerformance._iSelectColName = "name"
                        objPerformance._iUnkidColName = "classesunkid"
                End Select
            End If
            objPerformance._AllocationIds = StrStringIds
            'S.SANDEEP [21 AUG 2015] -- START
            objPerformance._ScoringOptionId = ConfigParameter._Object._ScoringOptionId
            'S.SANDEEP [21 AUG 2015] -- END

            'S.SANDEEP [08 Jan 2016] -- START
            objPerformance._SelfAssignCompetencies = ConfigParameter._Object._Self_Assign_Competencies
            'S.SANDEEP [08 Jan 2016] -- END

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            objPerformance._IsCalibrationSettingActive = ConfigParameter._Object._IsCalibrationSettingActive
            objPerformance._DisplayScoreName = cboScoreOption.Text
            objPerformance._DisplayScoreType = cboScoreOption.SelectedValue
            'S.SANDEEP |27-MAY-2019| -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmPerformanceEvaluationReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPerformance = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPerformanceEvaluationReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPerformanceEvaluationReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me.EZeeHeader1.Title = objPerformance._ReportName
            Me.EZeeHeader1.Message = objPerformance._ReportDesc

            Call FillCombo()

            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentSoreRatingReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If objPerformance.Export_Evaluation_Report(FinancialYear._Object._DatabaseName, _
                                                       User._Object._Userunkid, _
                                                       FinancialYear._Object._YearUnkid, _
                                                       Company._Object._Companyunkid, _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       ConfigParameter._Object._UserAccessModeSetting, True, _
                                                       ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport) = False Then 'S.SANDEEP [13-JUL-2017] -- START -- END
				'Shani (19-Aug-2016) -- Change[mdtStartDate,mdtEndDate]-->[EmployeeAsOnDate]
				'Shani (19-Aug-2016) --'Issue - Inactive Employee will come base on pariod date give by sandra
                'If objPerformance.Export_Evaluation_Report() = False Then
                'S.SANDEEP [04 JUN 2015] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, No performace evaluation data present for the selected period."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPerformanceEvaluationReport.SetMessages()
            objfrm._Other_ModuleNames = "clsPerformanceEvaluationReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            colhAllocations.Text = cboAllocations.Text
            Call Fill_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            Call Do_Operation(CBool(objchkAll.CheckState))
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            lvAllocation.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocation.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtStartDate = objPrd._Start_Date
                mdtEndDate = objPrd._End_Date
                objPrd = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkDisplayChart_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkDisplayChart.LinkClicked
        Try
            If SetFilter() = False Then Exit Sub
            Dim dsChart As New DataSet
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsChart = objPerformance.Show_Analysis_Chart()
            dsChart = objPerformance.Show_Analysis_Chart(FinancialYear._Object._DatabaseName, _
                                                         User._Object._Userunkid, _
                                                         FinancialYear._Object._YearUnkid, _
                                                         Company._Object._Companyunkid, _
                                                         mdtStartDate, _
                                                         mdtEndDate, _
                                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                                         ConfigParameter._Object._IsIncludeInactiveEmp)
            'S.SANDEEP [04 JUN 2015] -- END

            If dsChart.Tables.Count > 0 Then
                If dsChart.Tables(0).Rows.Count > 0 Then
                    Dim frm As New frmPerformanceAnalyis_Chart
                    frm._ColHdr = cboAllocations.Text
                    frm._CheckedItems = mCheckedItems
                    frm._DataSet = dsChart
                    frm._ChartTitle = Language.getMessage(mstrModuleName, 3, "Performance Evaluation Status") & vbCrLf & _
                                      cboPeriod.Text
                    'S.SANDEEP |27-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                    frm._IsAnalysisChart = True
                    'S.SANDEEP |27-JUL-2019| -- END
                    frm.ShowDialog()
                Else
                    GoTo msg
                End If
            Else
msg:            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, No performace evaluation data present for the selected period."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkDisplayChart_LinkClicked", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Private Sub lnkDisplayHPOCurve_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkDisplayHPOCurve.LinkClicked
        Try
            If SetFilter() = False Then Exit Sub
            Dim dsChart As New DataSet
            'S.SANDEEP |22-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'dsChart = objPerformance.GenerateChartData(CInt(cboPeriod.SelectedValue), _
            '                                           FinancialYear._Object._DatabaseName, _
            '                                           User._Object._Userunkid, _
            '                                           FinancialYear._Object._YearUnkid, _
            '                                           Company._Object._Companyunkid, _
            '                                           ConfigParameter._Object._UserAccessModeSetting, True, False, _
            '                                           "List", , , , , cboScoreOption.SelectedValue)

            dsChart = objPerformance.GenerateChartData(CInt(cboPeriod.SelectedValue), _
                                                       FinancialYear._Object._DatabaseName, _
                                                       User._Object._Userunkid, _
                                                       FinancialYear._Object._YearUnkid, _
                                                       Company._Object._Companyunkid, _
                                                       ConfigParameter._Object._UserAccessModeSetting, True, _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       False, 0, "List", , , , , cboScoreOption.SelectedValue)
            'S.SANDEEP |22-OCT-2019| -- END
            
            If dsChart.Tables.Count > 0 Then
                If dsChart.Tables(0).Rows.Count > 0 Then
                    Dim frm As New frmPerformanceAnalyis_Chart
                    frm._DataSet = dsChart
                    frm._ChartTitle = Language.getMessage(mstrModuleName, 200, "Performance HPO Curve") & vbCrLf & _
                                      cboPeriod.Text
                    frm._IsAnalysisChart = False
                    frm.ShowDialog()
                Else
                    GoTo msg
                End If
            Else
msg:            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, No performace evaluation data present for the selected period."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkDisplayHPOCurve_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP |27-JUL-2019| -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.colhAllocations.Text = Language._Object.getCaption(CStr(Me.colhAllocations.Tag), Me.colhAllocations.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lnkDisplayChart.Text = Language._Object.getCaption(Me.lnkDisplayChart.Name, Me.lnkDisplayChart.Text)
            Me.lblDisplayScore.Text = Language._Object.getCaption(Me.lblDisplayScore.Name, Me.lblDisplayScore.Text)
            Me.lnkDisplayHPOCurve.Text = Language._Object.getCaption(Me.lnkDisplayHPOCurve.Name, Me.lnkDisplayHPOCurve.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 2, "Please select atleast one allocation to export report.")
			Language.setMessage(mstrModuleName, 3, "Performance Evaluation Status")
			Language.setMessage(mstrModuleName, 4, "Sorry, No performace evaluation data present for the selected period.")
            Language.setMessage(mstrModuleName, 5, "Sorry, No performace evaluation data present for the selected period.")
            Language.setMessage(mstrModuleName, 200, "Performance HPO Curve")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
