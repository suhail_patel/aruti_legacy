'************************************************************************************************************************************
'Class Name :clsDiscipline_Casebook.vb
'Purpose    :
'Date       :01 -Jun -2012
'Written By : Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region


Public Class clsDisciplinePendingCasesDetail
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsDiscipline_Casebook"
    Private mstrReportId As String = enArutiReport.DisciplinePendingCasesDetail
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mdtFromCharge As DateTime = Nothing
    Private mdtToCharge As DateTime = Nothing
    Private mdtFromInterdiction As DateTime = Nothing
    Private mdtToInterdiction As DateTime = Nothing
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'S.SANDEEP [ 16 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintDStatusId As Integer = 0
    Private mstrDStatusName As String = String.Empty
    'S.SANDEEP [ 16 JAN 2013 ] -- END


    'S.SANDEEP [ 26 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim strGTotal As String = Language.getMessage(mstrModuleName, 1, "Grand Total :")
    Dim strSubTotal As String = Language.getMessage(mstrModuleName, 2, "Group Total :")
    Dim strSGrpTotal As String = Language.getMessage(mstrModuleName, 3, "Sub Group Total :")
    Dim objDic As New Dictionary(Of Integer, Object)
    Dim strarrGroupColumns As String() = Nothing
    Dim rowsArrayHeader As New ArrayList
    Dim dtFinalTab As DataTable
    Dim intArrayColumnWidth As Integer() = Nothing
    Dim strarrRecordCount As String() = Nothing
    'S.SANDEEP [ 26 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _FromChargeDate() As Date
        Set(ByVal value As Date)
            mdtFromCharge = value
        End Set
    End Property

    Public WriteOnly Property _ToChargeDate() As Date
        Set(ByVal value As Date)
            mdtToCharge = value
        End Set
    End Property

    Public WriteOnly Property _FromInterdictionDate() As Date
        Set(ByVal value As Date)
            mdtFromInterdiction = value
        End Set
    End Property

    Public WriteOnly Property _ToInterdictionDate() As Date
        Set(ByVal value As Date)
            mdtToInterdiction = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'S.SANDEEP [ 16 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _DStatusId() As Integer
        Set(ByVal value As Integer)
            mintDStatusId = value
        End Set
    End Property

    Public WriteOnly Property _DStatusName() As String
        Set(ByVal value As String)
            mstrDStatusName = value
        End Set
    End Property
    'S.SANDEEP [ 16 JAN 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mdtFromCharge = Nothing
            mdtToCharge = Nothing
            mdtFromInterdiction = Nothing
            mdtToInterdiction = Nothing
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mintDStatusId = 0
            mstrDStatusName = ""
            'S.SANDEEP [ 16 JAN 2013 ] -- END

            'S.SANDEEP [ 26 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDic = New Dictionary(Of Integer, Object)
            strarrGroupColumns = Nothing
            rowsArrayHeader = New ArrayList
            intArrayColumnWidth = Nothing
            strarrRecordCount = Nothing
            'S.SANDEEP [ 26 FEB 2013 ] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Me._FilterQuery &= " AND employeeunkid = @EmployeeId "
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                'Shani(24-Aug-2015) -- End

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If Not (mdtFromInterdiction = Nothing AndAlso mdtToInterdiction = Nothing) Then
                objDataOperation.AddParameter("@InterdictionFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromInterdiction))
                objDataOperation.AddParameter("@InterdictionTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToInterdiction))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8),interdictdate,112),'') between @InterdictionFrom AND @InterdictionTo "
                Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8),hrdiscipline_file.interdictdate,112),'') between @InterdictionFrom AND @InterdictionTo "
                'Shani(24-Aug-2015) -- End

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, " Interdiction Date From : ") & " " & mdtFromInterdiction.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 14, " Interdiction Date To : ") & " " & mdtToInterdiction.ToShortDateString & " "
            End If

            If Not (mdtFromCharge = Nothing AndAlso mdtToCharge = Nothing) Then
                objDataOperation.AddParameter("@DateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromCharge))
                objDataOperation.AddParameter("@DateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToCharge))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8),trandate,112),'') between @DateFrom AND @DateTo "
                Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8),hrdiscipline_file.trandate,112),'') between @DateFrom AND @DateTo "
                'Shani(24-Aug-2015) -- End

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, " Charge Date From : ") & " " & mdtFromCharge.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 16, " Charge Date To : ") & " " & mdtToCharge.ToShortDateString & " "
            End If


            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintDStatusId > 0 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDStatusId)
                Me._FilterQuery &= " AND hrdisciplinestatus_master.disciplinestatusunkid = @StatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Discipline Status : ") & " " & mstrDStatusName
            End If
            'S.SANDEEP [ 16 JAN 2013 ] -- END


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 26 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim strReportExportFile As String = ""
    '    Try
    '        objRpt = Generate_DetailReport()
    '        If Not IsNothing(objRpt) Then
    '            Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Try
        '    Call Generate_DetailReport()
        '    If dtFinalTab IsNot Nothing Then
        '        Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dtFinalTab, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", "", strarrRecordCount, strGTotal, True, rowsArrayHeader, Nothing, Nothing)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub
    'S.SANDEEP [ 26 FEB 2013 ] -- END

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Try
            Call Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If dtFinalTab IsNot Nothing Then
                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, dtFinalTab, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", "", strarrRecordCount, strGTotal, True, rowsArrayHeader, Nothing, Nothing)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            ' iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 18, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 18, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 18, "Employee Name")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END
            iColumn_DetailReport.Add(New IColumn("ISNULL(CONVERT(CHAR(8),trandate,112),'')", Language.getMessage(mstrModuleName, 19, "Date Of Charge")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(CONVERT(CHAR(8),interdictdate,112),'')", Language.getMessage(mstrModuleName, 20, "Date Of Interdiction")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Sub Generate_DetailReport()
    Private Sub Generate_DetailReport(ByVal strDatabaseName As String, _
                                      ByVal intUserUnkid As Integer, _
                                      ByVal intYearUnkid As Integer, _
                                      ByVal intCompanyUnkid As Integer, _
                                      ByVal dtPeriodStart As Date, _
                                      ByVal dtPeriodEnd As Date, _
                                      ByVal strUserModeSetting As String, _
                                      ByVal blnOnlyApproved As Boolean)
        'Shani(24-Aug-2015) -- End
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            StrQ = "SELECT  " & _
                   " ROW_NUMBER() OVER ( ORDER BY disciplinefileunkid ) AS [S/N] "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '        ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS [Name Of Employee] " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= ",ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS [Name Of Employee] "
            Else
                StrQ &= ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS [Name Of Employee] "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= ",CASE WHEN gender = 1 THEN @MALE WHEN gender = 2 THEN @FEMALE END [Gender] " & _
            '       ",job.job_name AS [Designation] " & _
            '       ",dept.name AS [Department] " & _
            '       ",cl.name AS [Station] " & _
            '       ",ISNULL(hrdisciplinetype_master.code, '')+' - '+ISNULL(hrdisciplinetype_master.name, '') AS [Charge] " & _
            '       ",ISNULL(CONVERT(CHAR(8), trandate, 112), '') AS [Date Of Charge] " & _
            '       ",ISNULL(CONVERT(CHAR(8), interdictdate, 112), '') AS [Date Of Interdiction] " & _
            '       ",ISNULL(REPLACE(incident,CHAR(13),'#10;'), '') AS [Incident] " & _
            '       ",ISNULL(CONVERT(CHAR(8), response_date, 112), '') AS [Response Date] " & _
            '       ",ISNULL(response_remark, '') AS [Response Remark] " & _
            '       ",ISNULL(hrdisciplinestatus_master.name, '') AS [Status] " & _
            '       ",ISNULL(hremployee_master.employeeunkid,0)  EmpId "

            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= " FROM hrdiscipline_file " & _
            '              "     LEFT JOIN hrdisciplinestatus_master ON hrdiscipline_file.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid " & _
            '             " JOIN hrdisciplinetype_master ON hrdiscipline_file.disciplinetypeunkid = hrdisciplinetype_master.disciplinetypeunkid   " & _
            '             " JOIN hremployee_master ON hrdiscipline_file.involved_employeeunkid = hremployee_master.employeeunkid   " & _
            '             " JOIN hrjob_master job ON hremployee_master.jobunkid = job.jobunkid " & _
            '             " JOIN hrdepartment_master dept ON hremployee_master.departmentunkid = dept.departmentunkid " & _
            '              "     LEFT JOIN hrstation_master st ON st.stationunkid = hremployee_master.stationunkid " & _
            '              "     LEFT JOIN hrclasses_master AS cl ON cl.classesunkid = hremployee_master.classunkid "

            'StrQ &= mstrAnalysis_Join

            'StrQ &= " WHERE hrdiscipline_file.isvoid = 0 "


            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            StrQ &= ",CASE WHEN gender = 1 THEN @MALE WHEN gender = 2 THEN @FEMALE END [Gender] " & _
                   ",job.job_name AS [Designation] " & _
                   ",dept.name AS [Department] " & _
                   ",cl.name AS [Station] " & _
                   ",ISNULL(hrdisciplinetype_master.code, '')+' - '+ISNULL(hrdisciplinetype_master.name, '') AS [Charge] " & _
                   ",ISNULL(CONVERT(CHAR(8), trandate, 112), '') AS [Date Of Charge] " & _
                   ",ISNULL(CONVERT(CHAR(8), interdictdate, 112), '') AS [Date Of Interdiction] " & _
                   ",ISNULL(REPLACE(incident,CHAR(13),'#10;'), '') AS [Incident] " & _
                   ",ISNULL(CONVERT(CHAR(8), response_date, 112), '') AS [Response Date] " & _
                   ",ISNULL(response_remark, '') AS [Response Remark] " & _
                   ",ISNULL(hrdisciplinestatus_master.name, '') AS [Status] " & _
                   ",ISNULL(hremployee_master.employeeunkid,0)  EmpId "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM hrdiscipline_file " & _
                          "     LEFT JOIN hrdisciplinestatus_master ON hrdiscipline_file.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid " & _
                         " JOIN hrdisciplinetype_master ON hrdiscipline_file.disciplinetypeunkid = hrdisciplinetype_master.disciplinetypeunkid   " & _
                         " JOIN hremployee_master ON hrdiscipline_file.involved_employeeunkid = hremployee_master.employeeunkid   " & _
                    "   LEFT JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "               jobunkid " & _
                    "               ,employeeunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "           FROM hremployee_categorization_tran " & _
                    "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "       ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "   JOIN hrjob_master job ON Jobs.jobunkid = job.jobunkid " & _
                    "   LEFT JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "               departmentunkid " & _
                    "               ,stationunkid " & _
                    "               ,classunkid " & _
                    "               ,employeeunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "           FROM hremployee_transfer_tran " & _
                    "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "       ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "   JOIN hrdepartment_master dept ON Alloc.departmentunkid = dept.departmentunkid " & _
                    "   LEFT JOIN hrstation_master st ON st.stationunkid = Alloc.stationunkid " & _
                    "   LEFT JOIN hrclasses_master AS cl ON cl.classesunkid = Alloc.classunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE hrdiscipline_file.isvoid = 0 "


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery


            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim iEmpId As Integer = 0 : Dim mstrOffence As String = String.Empty : Dim mstrIncident As String = String.Empty

            dtFinalTab = dsList.Tables("DataTable").Clone
            dtFinalTab.Rows.Clear() : Dim dFRow As DataRow = Nothing
            Dim iCnt As Integer = 1
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                If iEmpId <> CInt(dtRow.Item("EmpId")) Then
                    iCnt = 1
                    dFRow = dtFinalTab.NewRow

                    dFRow.Item("S/N") = CInt(dtRow.Item("S/N"))
                    dFRow.Item("Name Of Employee") = dtRow.Item("Name Of Employee")
                    dFRow.Item("Gender") = dtRow.Item("Gender")
                    dFRow.Item("Designation") = dtRow.Item("Designation")
                    dFRow.Item("Department") = dtRow.Item("Department")
                    dFRow.Item("Station") = dtRow.Item("Station")

                    If dtRow.Item("Date Of Charge").ToString().Trim.Length > 0 Then
                        dFRow.Item("Date Of Charge") = eZeeDate.convertDate(dtRow.Item("Date Of Charge").ToString()).ToShortDateString
                    Else
                        dFRow.Item("Date Of Charge") = ""
                    End If

                    If dtRow.Item("Date Of Interdiction").ToString().Trim.Length > 0 Then
                        dFRow.Item("Date Of Interdiction") = eZeeDate.convertDate(dtRow.Item("Date Of Interdiction").ToString()).ToShortDateString
                    Else
                        dFRow.Item("Date Of Interdiction") = Language.getMessage(mstrModuleName, 21, "Not Interdicted")
                    End If

                End If
                If iEmpId <> CInt(dtRow.Item("EmpId")) Then
                    mstrOffence = iCnt.ToString & "). " & dtRow.Item("Charge") & "#10;"
                    mstrIncident = iCnt.ToString & "). " & dtRow.Item("Incident") & "#10;"
                    dtFinalTab.Rows.Add(dFRow)
                Else
                    mstrOffence &= "#10;" & iCnt.ToString & "). " & dtRow.Item("Charge") & "#10;"
                    mstrIncident &= "#10;" & iCnt.ToString & "). " & dtRow.Item("Incident") & "#10;"
                End If

                dFRow.Item("Charge") = mstrOffence
                dFRow.Item("Incident") = mstrIncident

                If dtRow.Item("Response Date").ToString.Trim.Length > 0 Then
                    dFRow.Item("Response Date") = eZeeDate.convertDate(dtRow.Item("Response Date").ToString).ToShortDateString
                Else
                    dFRow.Item("Response Date") = ""
                End If
                dFRow.Item("Response Remark") = dtRow.Item("Response Remark")
                dFRow.Item("Status") = dtRow.Item("Status")
                dFRow.Item("Id") = dtRow.Item("Id")
                dFRow.Item("GName") = dtRow.Item("GName")

                iEmpId = CInt(dtRow.Item("EmpId"))
                iCnt += 1
            Next

            dtFinalTab.Columns.Remove("Id")
            dtFinalTab.Columns.Remove("EmpId")

            Dim dView As DataView = dtFinalTab.DefaultView
            Dim strSort As String = String.Empty

            Dim strGrpCols As String()
            If mintViewIndex > 0 Then
                dtFinalTab.Columns("GName").Caption = mstrReport_GroupName
                strGrpCols = New String() {"GName", "Status"}
                strarrRecordCount = New String() {strSubTotal, strSGrpTotal}
                strSort = "S/N,GName,Status"
            Else
                strGrpCols = New String() {"Status"}
                strarrRecordCount = New String() {strSubTotal}
                dtFinalTab.Columns.Remove("GName")
                strSort = "S/N,Status"
            End If
            dView.Sort = strSort

            dtFinalTab = dView.ToTable

            strarrGroupColumns = strGrpCols

            ReDim intArrayColumnWidth(dtFinalTab.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = 9 Then
                    intArrayColumnWidth(i) = 300
                Else
                    intArrayColumnWidth(i) = 100
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#Region " CHANGED REPORT FORMAT FROM CRYSTAL REPORT TO EXTRA XLS DUE TO LENGTH OF INCIDENT. CHANGES MADE : MR. ANDREW'S COMMENTS (26 FEB 2013) "

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim blnFlag As Boolean = False
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try

    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        'S.SANDEEP [ 16 JAN 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'StrQ = "SELECT  " & _
    '        '           " ROW_NUMBER() OVER(ORDER BY disciplinefileunkid) AS RCNT   " & _
    '        '           ",ISNULL(hrdisciplinetype_master.code,'') AS OFFENCECODE  " & _
    '        '           ",ISNULL(hrdisciplinetype_master.name,'') AS OFFENCE   " & _
    '        '           " ,ISNULL(incident,'') AS INCIDENT  " & _
    '        '           " ,ISNULL(CONVERT(CHAR(8),trandate,112),'') AS CHARGEDATE  " & _
    '        '           " ,reference_no AS REFNO   " & _
    '        '           " ,ISNULL(CONVERT(CHAR(8),interdictdate,112),'') AS IDATE   " & _
    '        '           ",ISNULL(hremployee_master.employeeunkid,0)  EMPLOYEEID " & _
    '        '           " ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS IPERSON   " & _
    '        '           " ,ISNULL(hremployee_master.employeecode,'') AS ICODE   " & _
    '        '           ",CASE WHEN gender = 1  THEN @MALE " & _
    '        '           "          WHEN gender = 2 THEN  @FEMALE	" & _
    '        '           " END GENDER		  " & _
    '        '           ", job.job_name AS DESIGNATION " & _
    '        '           ", dept.name AS DEPARTMENT " & _
    '        '           ", st.name AS STATION "

    '        StrQ = "SELECT  " & _
    '                   " ROW_NUMBER() OVER(ORDER BY disciplinefileunkid) AS RCNT   " & _
    '                   ",ISNULL(hrdisciplinetype_master.code,'') AS OFFENCECODE  " & _
    '                   ",ISNULL(hrdisciplinetype_master.name,'') AS OFFENCE   " & _
    '                   " ,ISNULL(incident,'') AS INCIDENT  " & _
    '                   " ,ISNULL(CONVERT(CHAR(8),trandate,112),'') AS CHARGEDATE  " & _
    '                   " ,reference_no AS REFNO   " & _
    '                   " ,ISNULL(CONVERT(CHAR(8),interdictdate,112),'') AS IDATE   " & _
    '                   ",ISNULL(hremployee_master.employeeunkid,0)  EMPLOYEEID " & _
    '                   " ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS IPERSON   " & _
    '                   " ,ISNULL(hremployee_master.employeecode,'') AS ICODE   " & _
    '                   ",CASE WHEN gender = 1  THEN @MALE " & _
    '                   "          WHEN gender = 2 THEN  @FEMALE	" & _
    '                   " END GENDER		  " & _
    '                   ", job.job_name AS DESIGNATION " & _
    '                   ", dept.name AS DEPARTMENT " & _
    '                    ",st.name AS STATION1 " & _
    '                    ",cl.name AS STATION " & _
    '                    ",ISNULL(hrdisciplinestatus_master.name,'') AS DSTATUS " & _
    '                    ",ISNULL(hrdisciplinestatus_master.disciplinestatusunkid,0) AS STATUSID " & _
    '                    ",ISNULL(CONVERT(CHAR(8),response_date,112),'') AS RESP_DATE " & _
    '                    ",ISNULL(response_remark,'') AS RESP_REMARK "
    '        'S.SANDEEP [ 16 JAN 2013 ] -- END



    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If


    '        'S.SANDEEP [ 16 JAN 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'StrQ &= " FROM hrdiscipline_file " & _
    '        '              " JOIN hrdisciplinetype_master ON hrdiscipline_file.disciplinetypeunkid = hrdisciplinetype_master.disciplinetypeunkid   " & _
    '        '              " JOIN hremployee_master ON hrdiscipline_file.involved_employeeunkid = hremployee_master.employeeunkid   " & _
    '        '              " JOIN hrjob_master job ON hremployee_master.jobunkid = job.jobunkid " & _
    '        '              " JOIN hrdepartment_master dept ON hremployee_master.departmentunkid = dept.departmentunkid " & _
    '        '              " JOIN hrstation_master st ON st.stationunkid = hremployee_master.stationunkid "

    '        StrQ &= " FROM hrdiscipline_file " & _
    '                      "     LEFT JOIN hrdisciplinestatus_master ON hrdiscipline_file.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid " & _
    '                     " JOIN hrdisciplinetype_master ON hrdiscipline_file.disciplinetypeunkid = hrdisciplinetype_master.disciplinetypeunkid   " & _
    '                     " JOIN hremployee_master ON hrdiscipline_file.involved_employeeunkid = hremployee_master.employeeunkid   " & _
    '                     " JOIN hrjob_master job ON hremployee_master.jobunkid = job.jobunkid " & _
    '                     " JOIN hrdepartment_master dept ON hremployee_master.departmentunkid = dept.departmentunkid " & _
    '                      "     LEFT JOIN hrstation_master st ON st.stationunkid = hremployee_master.stationunkid " & _
    '                      "     LEFT JOIN hrclasses_master AS cl ON cl.classesunkid = hremployee_master.classunkid "
    '        'S.SANDEEP [ 16 JAN 2013 ] -- END


    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= " WHERE hrdiscipline_file.isvoid = 0 "


    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If


    '        Call FilterTitleAndFilterQuery()
    '        StrQ &= Me._FilterQuery


    '        objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
    '        objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If
    '        rpt_Data = New ArutiReport.Designer.dsArutiReport
    '        Dim rpt_Rows As DataRow = Nothing
    '        Dim EmployeeID As Integer = 0
    '        '  Dim intCount As Integer = 0
    '        Dim mstrOffence As String = String.Empty

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows

    '            If EmployeeID <> CInt(dtRow.Item("EMPLOYEEID")) Then
    '                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
    '                '  intCount += 1


    '                rpt_Rows.Item("Column1") = dtRow.Item("GName")
    '                'rpt_Rows.Item("Column2") = intCount
    '                rpt_Rows.Item("Column3") = dtRow.Item("IPERSON")
    '                rpt_Rows.Item("Column4") = dtRow.Item("GENDER")
    '                rpt_Rows.Item("Column5") = dtRow.Item("DESIGNATION")
    '                rpt_Rows.Item("Column6") = dtRow.Item("DEPARTMENT")
    '                rpt_Rows.Item("Column7") = dtRow.Item("STATION")

    '                If dtRow.Item("IDATE").ToString().Trim <> "" Then
    '                    rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("IDATE").ToString()).ToShortDateString
    '                Else
    '                    rpt_Rows.Item("Column8") = Language.getMessage(mstrModuleName, 21, "Not Interdicted")
    '                End If

    '                If dtRow.Item("CHARGEDATE").ToString().Trim Then
    '                    rpt_Rows.Item("Column9") = eZeeDate.convertDate(dtRow.Item("CHARGEDATE").ToString()).ToShortDateString
    '                Else
    '                    rpt_Rows.Item("Column9") = ""
    '                End If

    '            End If

    '            If EmployeeID <> CInt(dtRow.Item("EMPLOYEEID")) Then
    '                mstrOffence = dtRow.Item("OFFENCECODE") & " - " & dtRow.Item("OFFENCE")
    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '            Else
    '                mstrOffence &= vbCrLf & dtRow.Item("OFFENCECODE") & " - " & dtRow.Item("OFFENCE")
    '            End If

    '            rpt_Rows.Item("Column10") = mstrOffence

    '            'S.SANDEEP [ 16 JAN 2013 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            If dtRow.Item("RESP_DATE").ToString.Trim.Length > 0 Then
    '                rpt_Rows.Item("Column11") = eZeeDate.convertDate(dtRow.Item("RESP_DATE").ToString).ToShortDateString
    '            Else
    '                rpt_Rows.Item("Column11") = ""
    '            End If
    '            rpt_Rows.Item("Column12") = dtRow.Item("RESP_REMARK")
    '            rpt_Rows.Item("Column13") = dtRow.Item("DSTATUS")
    '            'S.SANDEEP [ 16 JAN 2013 ] -- END

    '            EmployeeID = CInt(dtRow.Item("EMPLOYEEID"))
    '        Next

    '        objRpt = New ArutiReport.Designer.rptDisciplinePendingCaseDetail

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If


    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 1, "S/N"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 2, "Name Of Employee"))
    '        Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 3, "Gender"))
    '        Call ReportFunction.TextChange(objRpt, "txtDesignation", Language.getMessage(mstrModuleName, 4, "Designation"))
    '        Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 5, "Department"))
    '        Call ReportFunction.TextChange(objRpt, "txtStation", Language.getMessage(mstrModuleName, 6, "Station"))

    '        Call ReportFunction.TextChange(objRpt, "txtInterdiction", Language.getMessage(mstrModuleName, 7, "Date Of Interdiction"))
    '        Call ReportFunction.TextChange(objRpt, "txtDateCharge", Language.getMessage(mstrModuleName, 8, "Date Of Charge"))
    '        Call ReportFunction.TextChange(objRpt, "txtCharge", Language.getMessage(mstrModuleName, 9, "Charge"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

    '        'S.SANDEEP [ 16 JAN 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 23, "Status"))
    '        Call ReportFunction.TextChange(objRpt, "txtRespDate", Language.getMessage(mstrModuleName, 24, "Response Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtRespRemark", Language.getMessage(mstrModuleName, 25, "Response Remark"))
    '        'S.SANDEEP [ 16 JAN 2013 ] -- END

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

#End Region

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Grand Total :")
            Language.setMessage(mstrModuleName, 2, "Group Total :")
            Language.setMessage(mstrModuleName, 3, "Sub Group Total :")
            Language.setMessage(mstrModuleName, 12, "Employee :")
            Language.setMessage(mstrModuleName, 13, " Interdiction Date From :")
            Language.setMessage(mstrModuleName, 14, " Interdiction Date To :")
            Language.setMessage(mstrModuleName, 15, " Charge Date From :")
            Language.setMessage(mstrModuleName, 16, " Charge Date To :")
            Language.setMessage(mstrModuleName, 17, "Order By :")
            Language.setMessage(mstrModuleName, 18, "Employee Name")
            Language.setMessage(mstrModuleName, 19, "Date Of Charge")
            Language.setMessage(mstrModuleName, 20, "Date Of Interdiction")
            Language.setMessage(mstrModuleName, 21, "Not Interdicted")
            Language.setMessage(mstrModuleName, 22, "Discipline Status :")
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
