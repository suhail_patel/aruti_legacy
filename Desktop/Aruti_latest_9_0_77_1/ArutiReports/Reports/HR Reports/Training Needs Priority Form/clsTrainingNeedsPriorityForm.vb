'************************************************************************************************************************************
'Class Name : clsTrainingNeedsPriorityForm.vb
'Purpose    :
'Date       :17/02/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text
Imports System.IO

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsTrainingNeedsPriorityForm
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTrainingNeedsPriorityForm"
    Private mstrReportId As String = enArutiReport.TrainingNeedsPriorityForm
    Dim objDataOperation As clsDataOperation
    Private mdTran As DataTable

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()

        mdTran = New DataTable("TnA")
        Dim dCol As DataColumn

        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "IsCheck"
        'dCol.DataType = System.Type.GetType("System.Boolean")
        'dCol.DefaultValue = False
        'mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "coursepriotranunkid"
        dCol.DataType = System.Type.GetType("System.Int32")
        dCol.DefaultValue = -1
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "prioritymasterunkid"
        dCol.DataType = System.Type.GetType("System.Int32")
        dCol.DefaultValue = -1
        mdTran.Columns.Add(dCol)


        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "courseemptranunkid"
        dCol.DataType = System.Type.GetType("System.Int32")
        dCol.DefaultValue = -1
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "courseunkid"
        dCol.DataType = System.Type.GetType("System.Int32")
        dCol.DefaultValue = 0
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "gappriority"
        dCol.DataType = System.Type.GetType("System.Int32")
        dCol.DefaultValue = 0
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "bussinesspriority"
        dCol.DataType = System.Type.GetType("System.Int32")
        dCol.DefaultValue = 0
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "providerunkid"
        dCol.DataType = System.Type.GetType("System.Int32")
        dCol.DefaultValue = 0
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "goalunkid"
        dCol.DataType = System.Type.GetType("System.Int32")
        dCol.DefaultValue = 0
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = Language.getMessage(mstrModuleName, 1, "KEY RESULT AREAS (KRA)")
        dCol.ColumnName = "kra"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = Language.getMessage(mstrModuleName, 2, "TRAINING NEEDS")
        dCol.ColumnName = "training_need"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = Language.getMessage(mstrModuleName, 3, "SKILL GAP")
        dCol.ColumnName = "skill_gap"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = Language.getMessage(mstrModuleName, 4, "IMPACT ON BUSINESS")
        dCol.ColumnName = "business_impact"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = Language.getMessage(mstrModuleName, 5, "PRIORITY RANKING")
        dCol.ColumnName = "priority_ranking"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = Language.getMessage(mstrModuleName, 6, "TARGET GROUP")
        dCol.ColumnName = "target_group"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = Language.getMessage(mstrModuleName, 7, "TRAINING SOLUTIONS")
        dCol.ColumnName = "training_sol"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = Language.getMessage(mstrModuleName, 8, "TRAINING OBJECTIVE")
        dCol.ColumnName = "training_objective"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = Language.getMessage(mstrModuleName, 9, "NUMBER OF PEOPLE")
        dCol.ColumnName = "total_emp"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = Language.getMessage(mstrModuleName, 10, "PREFERRED PROVIDER")
        dCol.ColumnName = "pref_provider"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = Language.getMessage(mstrModuleName, 11, "TRAINING VENUE")
        dCol.ColumnName = "training_venue"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = "" 'Language.getMessage(mstrModuleName, 12, "STRATEGIC GOAL")
        dCol.ColumnName = "strategic_goal"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "IsGrp"
        dCol.DataType = System.Type.GetType("System.Boolean")
        dCol.DefaultValue = False
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "employeeunkid"
        dCol.DataType = System.Type.GetType("System.Int32")
        dCol.DefaultValue = -1
        mdTran.Columns.Add(dCol)

        '============= FOR ALLOCATION===================

        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "GrpId"
        dCol.DataType = System.Type.GetType("System.Int32")
        dCol.DefaultValue = -1
        mdTran.Columns.Add(dCol)

        dCol = New DataColumn
        dCol.Caption = ""
        dCol.ColumnName = "GrpName"
        dCol.DataType = System.Type.GetType("System.String")
        dCol.DefaultValue = ""
        mdTran.Columns.Add(dCol)

        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "BranchId"
        'dCol.DataType = System.Type.GetType("System.Int32")
        'dCol.DefaultValue = -1
        'mdTran.Columns.Add(dCol)

        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "DepartmentId"
        'dCol.DataType = System.Type.GetType("System.Int32")
        'dCol.DefaultValue = -1
        'mdTran.Columns.Add(dCol)

        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "SectionGrpId"
        'dCol.DataType = System.Type.GetType("System.Int32")
        'dCol.DefaultValue = -1
        'mdTran.Columns.Add(dCol)

        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "SectionId"
        'dCol.DataType = System.Type.GetType("System.Int32")
        'dCol.DefaultValue = -1
        'mdTran.Columns.Add(dCol)

        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "UnitGrpId"
        'dCol.DataType = System.Type.GetType("System.Int32")
        'dCol.DefaultValue = -1
        'mdTran.Columns.Add(dCol)

        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "UnitId"
        'dCol.DataType = System.Type.GetType("System.Int32")
        'dCol.DefaultValue = -1
        'mdTran.Columns.Add(dCol)

        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "JobId"
        'dCol.DataType = System.Type.GetType("System.Int32")
        'dCol.DefaultValue = -1
        'mdTran.Columns.Add(dCol)

        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "CostcenterId"
        'dCol.DataType = System.Type.GetType("System.Int32")
        'dCol.DefaultValue = -1
        'mdTran.Columns.Add(dCol)


        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "TeamId"
        'dCol.DataType = System.Type.GetType("System.Int32")
        'dCol.DefaultValue = -1
        'mdTran.Columns.Add(dCol)

        '============= FOR ALLOCATION===================


        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "AUD"
        'dCol.DataType = System.Type.GetType("System.String")
        'dCol.DefaultValue = ""
        'mdTran.Columns.Add(dCol)

        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "GUID"
        'dCol.DataType = System.Type.GetType("System.String")
        'dCol.DefaultValue = ""
        'mdTran.Columns.Add(dCol)

        'dCol = New DataColumn
        'dCol.Caption = ""
        'dCol.ColumnName = "Sort"
        'dCol.DataType = System.Type.GetType("System.Int32")
        'dCol.DefaultValue = 0
        'mdTran.Columns.Add(dCol)

    End Sub
#End Region

#Region " Private variables "
    Private mstrReferenceNo As String = String.Empty
    Private mintProviderId As Integer = -1
    Private mstrProviderName As String = String.Empty
    Private mstrVenue As String = String.Empty

    Dim dblColTot As Decimal()

    Dim StrFinalPath As String = String.Empty

    Private mblnIsActive As Boolean = True

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Properties "

    Public WriteOnly Property _ReferenceNo() As String
        Set(ByVal value As String)
            mstrReferenceNo = value
        End Set
    End Property

    Public WriteOnly Property _ProviderId() As Integer
        Set(ByVal value As Integer)
            mintProviderId = value
        End Set
    End Property

    Public WriteOnly Property _ProviderName() As String
        Set(ByVal value As String)
            mstrProviderName = value
        End Set
    End Property

    Public WriteOnly Property _Venue() As String
        Set(ByVal value As String)
            mstrVenue = value
        End Set
    End Property


    Public WriteOnly Property _inalPath() As String
        Set(ByVal value As String)
            StrFinalPath = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mstrReferenceNo = ""
            mstrProviderName = ""
            mstrVenue = ""
            StrFinalPath = ""
            mblnIsActive = False
            mintViewIndex = 0
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mstrReferenceNo.Trim <> "" Then
                Me._FilterQuery &= " AND referenceno = @ReferenceNo "
                Me._FilterTitle &= ""
                objDataOperation.AddParameter("@ReferenceNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReferenceNo)
            End If
            If mintProviderId > 0 Then
                Me._FilterQuery &= " AND providerunkid = @providerunkid "
                Me._FilterTitle &= ""
                objDataOperation.AddParameter("@providerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProviderId)
            End If
            If mstrVenue.Trim <> "" Then
                Me._FilterQuery &= " AND training_venue LIKE @training_venue "
                Me._FilterTitle &= ""
                objDataOperation.AddParameter("@training_venue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrVenue & "%")
            End If


            If StrFinalPath.Trim <> "" Then
                objDataOperation.AddParameter("@inalPath", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrFinalPath)
                Me._FilterQuery &= ""
                Me._FilterTitle &= ""
            End If

            If mstrViewByName.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Analysis By :") & " " & mstrViewByName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY " & mstrAnalysis_OrderBy & ", " & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
            Else
                Me._FilterTitle &= ""
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY " & mstrAnalysis_OrderBy & ", hrtnacoursepriority_tran.courseunkid "
                Else
                    Me._FilterQuery &= "ORDER BY hrtnacoursepriority_tran.courseunkid "
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property
    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Public Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                          ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal dtPeriodStart As Date, _
                                          ByVal dtPeriodEnd As Date, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        'Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            StrQ = "SELECT DISTINCT hrtnaprirority_master.prioritymasterunkid " & _
                          ", hrtnacoursepriority_tran.coursepriotranunkid " & _
                          ", hrtnaprirority_master.periodunkid " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", referenceno " & _
                          ", ISNULL(hrtnaprirority_master.remark, '') remark " & _
                          ", ISNULL(isfinal, 0) isfinal " & _
                          ", hrtnacoursepriority_tran.courseunkid " & _
                          ", course.name AS course " & _
                          ", ISNULL(hremployee_master.jobgroupunkid,0) as jobgroupunkid " & _
                          ", ISNULL(hrjobgroup_master.name,'') as JobGroup " & _
                          ", hrtnacoursepriority_tran.gappriority AS gappriorityId " & _
                          ", CASE WHEN hrtnacoursepriority_tran.gappriority = 1 THEN @Low " & _
                                 "WHEN hrtnacoursepriority_tran.gappriority = 2 THEN @Medium " & _
                                 "WHEN hrtnacoursepriority_tran.gappriority = 3 THEN @High " & _
                            "END gappriority " & _
                          ", hrtnacoursepriority_tran.bussinesspriority AS bussinesspriorityId " & _
                          ", CASE WHEN hrtnacoursepriority_tran.bussinesspriority = 1 THEN @Low " & _
                                 "WHEN hrtnacoursepriority_tran.bussinesspriority = 2 THEN @Medium " & _
                                 "WHEN hrtnacoursepriority_tran.bussinesspriority = 3 THEN @High " & _
                            "END bussinesspriority " & _
                          ", ISNULL(kra, '') kra " & _
                          ", priority_ranking " & _
                          ", hrtnacoursepriority_tran.providerunkid " & _
                          ", hrinstitute_master.institute_name " & _
                          ", ISNULL(training_venue, '') AS training_venue " & _
                          ", ISNULL(training_solution, '') AS training_solution " & _
                          ", ISNULL(training_objective, '') AS training_objective " & _
                          ", hrtnacoursepriority_tran.goalunkid " & _
                          ", goal.name AS strategicgoal " & _
                          ", ( SELECT    COUNT(*) " & _
                              "FROM      hrtnaemp_course_tran " & _
                              "WHERE     hrtnaemp_course_tran.coursepriotranunkid = hrtnacoursepriority_tran.coursepriotranunkid " & _
                                        "AND hrtnaemp_course_tran.isvoid = 0 " & _
                            ") AS total_emp "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",-1 AS Id, '' AS GName "
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= "FROM    hrtnaprirority_master " & _
            '                "JOIN hrtnacoursepriority_tran ON hrtnacoursepriority_tran.prioritymasterunkid = hrtnaprirority_master.prioritymasterunkid " & _
            '                "JOIN cfcommon_master course ON course.masterunkid = hrtnacoursepriority_tran.courseunkid " & _
            '                                               "AND course.mastertype = 30 " & _
            '                "JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = hrtnacoursepriority_tran.providerunkid " & _
            '                                           "AND hrinstitute_master.ishospital = 0 " & _
            '                "JOIN cfcommon_master goal ON goal.masterunkid = hrtnacoursepriority_tran.goalunkid " & _
            '                                             "AND goal.mastertype = 31 " & _
            '                "JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrtnaprirority_master.periodunkid " & _
            '                "JOIN hrtnaemp_course_tran ON hrtnacoursepriority_tran.coursepriotranunkid = hrtnaemp_course_tran.coursepriotranunkid " & _
            '                                             "AND hrtnacoursepriority_tran.courseunkid = hrtnaemp_course_tran.courseunkid " & _
            '                "JOIN hremployee_master ON hrtnaemp_course_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                "JOIN hrjobgroup_master ON hremployee_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid "

            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If

            StrQ &= "FROM    hrtnaprirority_master " & _
                            "JOIN hrtnacoursepriority_tran ON hrtnacoursepriority_tran.prioritymasterunkid = hrtnaprirority_master.prioritymasterunkid " & _
                            "JOIN cfcommon_master course ON course.masterunkid = hrtnacoursepriority_tran.courseunkid AND course.mastertype = 30 " & _
                            "JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = hrtnacoursepriority_tran.providerunkid AND hrinstitute_master.ishospital = 0 " & _
                            "JOIN cfcommon_master goal ON goal.masterunkid = hrtnacoursepriority_tran.goalunkid AND goal.mastertype = 31 " & _
                            "JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrtnaprirority_master.periodunkid " & _
                            "JOIN hrtnaemp_course_tran ON hrtnacoursepriority_tran.coursepriotranunkid = hrtnaemp_course_tran.coursepriotranunkid AND hrtnacoursepriority_tran.courseunkid = hrtnaemp_course_tran.courseunkid " & _
                            "JOIN hremployee_master ON hrtnaemp_course_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "LEFT JOIN " & _
                            "   ( " & _
                            "       SELECT " & _
                            "           jobgroupunkid " & _
                            "           ,employeeunkid " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "       FROM hremployee_categorization_tran " & _
                            "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                            "JOIN hrjobgroup_master ON hremployee_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            'Shani(24-Aug-2015) -- End
            StrQ &= "WHERE   hrtnaprirority_master.isvoid = 0 "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mblnIsActive = False Then
            '    StrQ &= "	AND hremployee_master.isactive = 1 "
            'End If
            'StrQ &= UserAccessLevel._AccessLevelFilterString

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Shani(24-Aug-2015) -- End



            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            'End Select

            'S.SANDEEP [ 01 JUNE 2012 ] -- END


            'S.SANDEEP [ 18 FEB 2012 ] -- END

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            objDataOperation.AddParameter("@Low", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 332, "Low"))
            objDataOperation.AddParameter("@Medium", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 333, "Medium"))
            objDataOperation.AddParameter("@High", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 334, "High"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then

                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            mdTran.Rows.Clear()

            Dim drRow As DataRow
            Dim intPrevGrpID As Integer = -1
            Dim intPrevCourseID As Integer
            Dim objDic As New Dictionary(Of Integer, String)
            Dim dicCourse As New Dictionary(Of Integer, String)
            Dim strJobGrp As String = ""
            For Each dsRow As DataRow In dsList.Tables("DataTable").Rows

                If intPrevGrpID.ToString <> CInt(dsRow.Item("Id")) Then
                    intPrevCourseID = -1
                    If intPrevGrpID > 0 Then
                        If dicCourse.ContainsKey(intPrevCourseID) = False Then
                            strJobGrp = ""
                            For Each pair In objDic
                                If strJobGrp = "" Then
                                    strJobGrp = pair.Value
                                Else
                                    strJobGrp &= "," & pair.Value
                                End If
                            Next
                            dicCourse.Add(intPrevCourseID, strJobGrp)

                            objDic.Clear()
                        End If
                    End If
                End If

                If intPrevCourseID <> CInt(dsRow.Item("courseunkid")) Then
                    If intPrevCourseID > 0 Then
                        If dicCourse.ContainsKey(intPrevCourseID) = False Then
                            strJobGrp = ""
                            For Each pair In objDic
                                If strJobGrp = "" Then
                                    strJobGrp = pair.Value
                                Else
                                    strJobGrp &= "," & pair.Value
                                End If
                            Next
                            dicCourse.Add(intPrevCourseID, strJobGrp)

                            objDic.Clear()
                        End If
                    End If

                    drRow = mdTran.NewRow

                    With drRow
                        .Item("prioritymasterunkid") = CInt(dsRow.Item("prioritymasterunkid"))
                        .Item("coursepriotranunkid") = CInt(dsRow.Item("coursepriotranunkid"))
                        .Item("courseunkid") = CInt(dsRow.Item("courseunkid"))
                        .Item("gappriority") = CInt(dsRow.Item("gappriorityid"))
                        .Item("bussinesspriority") = CInt(dsRow.Item("bussinesspriorityid"))
                        .Item("providerunkid") = CInt(dsRow.Item("providerunkid"))
                        .Item("goalunkid") = CInt(dsRow.Item("goalunkid"))
                        .Item("kra") = dsRow.Item("kra").ToString
                        .Item("training_need") = dsRow.Item("course").ToString
                        .Item("skill_gap") = dsRow.Item("gappriority").ToString
                        .Item("business_impact") = dsRow.Item("bussinesspriority").ToString
                        .Item("priority_ranking") = dsRow.Item("priority_ranking").ToString
                        .Item("training_sol") = dsRow.Item("training_solution").ToString
                        .Item("training_objective") = dsRow.Item("training_objective").ToString
                        .Item("total_emp") = dsRow.Item("total_emp").ToString
                        .Item("pref_provider") = dsRow.Item("institute_name").ToString
                        .Item("training_venue") = dsRow.Item("training_venue").ToString
                        .Item("strategic_goal") = dsRow.Item("strategicgoal").ToString
                        If dsRow.Item("Id").ToString <> "" Then
                            .Item("GrpId") = CInt(dsRow.Item("Id"))
                        End If

                        '.Item("GrpName") = dsRow.Item("GName").ToString

                        If objDic.ContainsKey(CInt(dsRow.Item("jobgroupunkid"))) = False Then
                            objDic.Add(CInt(dsRow.Item("jobgroupunkid")), dsRow.Item("JobGroup").ToString)
                        End If

                    End With

                    mdTran.Rows.Add(drRow)

                Else

                    If objDic.ContainsKey(CInt(dsRow.Item("jobgroupunkid"))) = False Then
                        objDic.Add(CInt(dsRow.Item("jobgroupunkid")), dsRow.Item("JobGroup").ToString)
                    End If
                End If
                intPrevCourseID = CInt(dsRow.Item("courseunkid"))
                intPrevGrpID = CInt(dsRow.Item("Id"))
            Next

            If dicCourse.ContainsKey(intPrevCourseID) = False Then
                strJobGrp = ""
                For Each pair In objDic
                    If strJobGrp = "" Then
                        strJobGrp = pair.Value
                    Else
                        strJobGrp &= ", " & pair.Value
                    End If
                Next
                dicCourse.Add(intPrevCourseID, strJobGrp)

                objDic.Clear()
            End If

            Dim dRow() As DataRow
            For Each pair In dicCourse
                dRow = mdTran.Select("courseunkid = " & pair.Key & " ")

                If dRow.Length > 0 Then
                    For Each dtRow As DataRow In dRow
                        dtRow.Item("target_group") = pair.Value
                        dtRow.AcceptChanges()
                    Next
                End If
            Next
            'rpt_Data = New ArutiReport.Designer.dsArutiReport

            'For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
            '    Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

            '    '<LOOP LOGIC> PLEASE REMOVE AND PASTE YOUR LOGIC

            '    'rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

            'Next

            If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, mdTran) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
                Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
            End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try

            'HEADER PART
            strBuilder.Append(" <TITLE> " & Me._ReportName & " </TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If
                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 15, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='60%' colspan=15 align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 13, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='60%' colspan=15  align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            'Report Column Caption
            For j As Integer = 0 To objDataReader.Columns.Count - 1
                If objDataReader.Columns(j).Caption = "" Then Continue For

                If objDataReader.Columns(j).Caption.ToUpper.Trim = "GRPID" OrElse objDataReader.Columns(j).Caption.ToUpper.Trim = "GRPNAME" Then
                    '*** Do Nothing (GroupID and GroupName column)
                    'strBuilder.Append("<TD BORDER=1 WIDTH='0px' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                Else
                    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                End If
            Next

            strBuilder.Append(" </TR> " & vbCrLf)


            '*** Data Part
            Dim strPrevGrpID As String = ""
            Dim GroupArrayTotal(objDataReader.Columns.Count - 5) As Decimal
            For i As Integer = 0 To objDataReader.Rows.Count - 1

                If strPrevGrpID <> objDataReader.Rows(i)("GRPID").ToString Then

                    '**** Group Header ****
                    'If mintViewIndex > 0 Then
                    '    strBuilder.Append(" <TR> " & vbCrLf)
                    '    strBuilder.Append("<TD colspan=" & objDataReader.Columns.Count - 13 & " BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & mstrReport_GroupName & " " & objDataReader.Rows(i)("GrpName") & "</B></FONT></TD>" & vbCrLf)
                    '    strBuilder.Append(" </TR> " & vbCrLf)
                    'End If
                End If

                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1
                    If objDataReader.Columns(k).Caption = "" Then Continue For

                    Select Case k
                        Case 0, 1
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                        Case 22 To 23
                            '*** Do Nothing (GroupID and GroupName column)
                        Case Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                            'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2> &nbsp;" & Format(CDec(objDataReader.Rows(i)(k)), GUI.fmtCurrency) & "</FONT></TD>" & vbCrLf)
                    End Select

                Next

                strBuilder.Append(" </TR> " & vbCrLf)

                strPrevGrpID = objDataReader.Rows(i)("GRPID").ToString
            Next

            strBuilder.Append(" </TR>  " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)


            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try

            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "KEY RESULT AREAS (KRA)")
            Language.setMessage(mstrModuleName, 2, "TRAINING NEEDS")
            Language.setMessage(mstrModuleName, 3, "SKILL GAP")
            Language.setMessage(mstrModuleName, 4, "IMPACT ON BUSINESS")
            Language.setMessage(mstrModuleName, 5, "PRIORITY RANKING")
            Language.setMessage(mstrModuleName, 6, "TARGET GROUP")
            Language.setMessage(mstrModuleName, 7, "TRAINING SOLUTIONS")
            Language.setMessage(mstrModuleName, 8, "TRAINING OBJECTIVE")
            Language.setMessage(mstrModuleName, 9, "NUMBER OF PEOPLE")
            Language.setMessage(mstrModuleName, 10, "PREFERRED PROVIDER")
            Language.setMessage(mstrModuleName, 11, "TRAINING VENUE")
            Language.setMessage(mstrModuleName, 12, "STRATEGIC GOAL")
            Language.setMessage(mstrModuleName, 13, "Date :")
            Language.setMessage(mstrModuleName, 14, "Report successfully exported to Report export path")
            Language.setMessage(mstrModuleName, 15, "Prepared By :")
            Language.setMessage(mstrModuleName, 16, "Analysis By :")
            Language.setMessage("clsMasterData", 332, "Low")
            Language.setMessage("clsMasterData", 333, "Medium")
            Language.setMessage("clsMasterData", 334, "High")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
