'************************************************************************************************************************************
'Class Name : clsPerformance_ApprisalReport.vb
'Purpose    :
'Date       : 31/03/2012
'Written By : Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

#End Region

Public Class clsPerformance_ApprisalReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPerformance_ApprisalReport"
    Private mstrReportId As String = CStr(enArutiReport.Performance_AppraisalReport)  '72
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END

        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END

    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrGradesIds As String = String.Empty
    Private mstrGradeNames As String = String.Empty
    Private mblnViewByApprovedAmt As Boolean = False
    Private mblnViewByProposedAmt As Boolean = False
    Private mdtFinalTable As DataTable
    Private StrFinalPath As String = String.Empty
    Private StrCaption() As String
    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIncludeInactiveEmp As Boolean = False
    'S.SANDEEP [ 12 MAY 2012 ] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _GradesIds() As String
        Set(ByVal value As String)
            mstrGradesIds = value
        End Set
    End Property

    Public WriteOnly Property _GradeNames() As String
        Set(ByVal value As String)
            mstrGradeNames = value
        End Set
    End Property

    Public WriteOnly Property _ViewByApprovedAmt() As Boolean
        Set(ByVal value As Boolean)
            mblnViewByApprovedAmt = value
        End Set
    End Property

    Public WriteOnly Property _ViewByProposedAmt() As Boolean
        Set(ByVal value As Boolean)
            mblnViewByProposedAmt = value
        End Set
    End Property

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 12 MAY 2012 ] -- END

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeUnkid = 0
            mstrEmployeeName = String.Empty
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrGradesIds = String.Empty
            mstrGradeNames = String.Empty
            mblnViewByApprovedAmt = False
            mblnViewByProposedAmt = False
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnIncludeInactiveEmp = False
            'S.SANDEEP [ 12 MAY 2012 ] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterTitle = ""
        Try

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If mintEmployeeUnkid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mstrGradeNames.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Grades :") & " " & mstrGradeNames & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Sub CreateTable()
        Try
            mdtFinalTable = New DataTable("Data")
            Dim dCol As DataColumn

            dCol = New DataColumn
            dCol.ColumnName = "Column1"
            dCol.Caption = Language.getMessage(mstrModuleName, 1, "S/N")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column2"
            dCol.Caption = Language.getMessage(mstrModuleName, 2, "EMPL. NO.")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column3"
            dCol.Caption = Language.getMessage(mstrModuleName, 3, "SURNAME")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column4"
            dCol.Caption = Language.getMessage(mstrModuleName, 4, "FIRST NAME")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column5"
            dCol.Caption = Language.getMessage(mstrModuleName, 5, "INITIALS")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column6"
            dCol.Caption = Language.getMessage(mstrModuleName, 6, "PROCESSED QUALIFICATION")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column7"
            dCol.Caption = Language.getMessage(mstrModuleName, 7, "GRADE")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column8"
            dCol.Caption = Language.getMessage(mstrModuleName, 8, "DATE OF EMPLOYMENT")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = DBNull.Value
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column9"
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "DATE OF CONFIRMATION")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = DBNull.Value
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column10"
            dCol.Caption = Language.getMessage(mstrModuleName, 10, "JOB TITLE")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column11"
            dCol.Caption = Language.getMessage(mstrModuleName, 11, "DATE OF CURRENT JOB TITLE")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = DBNull.Value
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column12"
            dCol.Caption = Language.getMessage(mstrModuleName, 12, "DATE OF LAST PROMOTION")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = DBNull.Value
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column13"
            dCol.Caption = Language.getMessage(mstrModuleName, 13, "REGION")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column14"
            dCol.Caption = Language.getMessage(mstrModuleName, 14, "CURRENT WORKING STATION")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)


            dCol = New DataColumn
            dCol.ColumnName = "Column15"
            dCol.Caption = Language.getMessage(mstrModuleName, 15, "SCORE")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column16"
            dCol.Caption = Language.getMessage(mstrModuleName, 16, "REMARKS")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column17"
            dCol.Caption = Language.getMessage(mstrModuleName, 15, "SCORE")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column18"
            dCol.Caption = Language.getMessage(mstrModuleName, 16, "REMARKS")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column19"
            dCol.Caption = Language.getMessage(mstrModuleName, 17, "CURRENT SALARY")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column20"
            dCol.Caption = Language.getMessage(mstrModuleName, 18, "AWARDS")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column21"
            dCol.Caption = Language.getMessage(mstrModuleName, 18, "AWARDS")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column22"
            dCol.Caption = Language.getMessage(mstrModuleName, 19, "PROPOSED NEW TITLE")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column23"
            dCol.Caption = Language.getMessage(mstrModuleName, 20, "PROPOSED NEW SALARY")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column24"
            dCol.Caption = Language.getMessage(mstrModuleName, 21, "HOUSE ALLOWANCE")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Get_Evaluation(ByVal intEmpId As Integer, ByVal StrDataBaseName As String) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT " & _
            '        "CAST(ISNULL(CASE WHEN COUNT(Rcnt) = 0  THEN 0 ELSE ISNULL(SUM(F_RESULT),0)/COUNT(Rcnt) END,0) AS DECIMAL(10,2)) AS GE_RESULT " & _
            '       "FROM " & _
            '       "( " & _
            '             "SELECT " & _
            '                   "hrassess_analysis_master.assessmodeid " & _
            '                  ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS F_RESULT " & _
            '                  ",ROW_NUMBER() OVER (ORDER BY hrassess_analysis_master.assessmodeid) AS Rcnt " & _
            '             "FROM " & StrDataBaseName & "..hrassess_analysis_master " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                        "hrassess_analysis_master.analysisunkid " & _
            '                       ",hrassess_analysis_master.assessmodeid " & _
            '                       ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RESULT " & _
            '                  "FROM  " & StrDataBaseName & "..hrassess_analysis_tran " & _
            '                       "JOIN  " & StrDataBaseName & "..hrresult_master ON  " & StrDataBaseName & "..hrassess_analysis_tran.resultunkid =  " & StrDataBaseName & "..hrresult_master.resultunkid " & _
            '                       "JOIN  " & StrDataBaseName & "..hrassess_analysis_master ON  " & StrDataBaseName & "..hrassess_analysis_tran.analysisunkid =  " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid " & _
            '                  "WHERE hrassess_analysis_master.isvoid = 0 AND hrassess_analysis_tran.isvoid = 0 AND iscommitted = 1 " & _
            '                       "AND (hrassess_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrassess_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
            '                  "GROUP BY hrassess_analysis_master.assessmodeid,hrassess_analysis_master.analysisunkid " & _
            '             ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
            '             "WHERE isvoid = 0 AND iscommitted = 1 AND (hrassess_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrassess_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
            '             "GROUP BY  " & StrDataBaseName & "..hrassess_analysis_master.assessmodeid " & _
            '        ") AS GERESULT "


            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            'StrQ = "SELECT " & _
            '        "CAST(ISNULL(CASE WHEN COUNT(Rcnt) = 0  THEN 0 ELSE ISNULL(SUM(F_RESULT),0)/COUNT(Rcnt) END,0) AS DECIMAL(10,2)) AS GE_RESULT " & _
            '       "FROM " & _
            '       "( " & _
            '             "SELECT " & _
            '                   "hrevaluation_analysis_master.assessmodeid " & _
            '                  ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS F_RESULT " & _
            '                  ",ROW_NUMBER() OVER (ORDER BY hrevaluation_analysis_master.assessmodeid) AS Rcnt " & _
            '             "FROM " & StrDataBaseName & "..hrevaluation_analysis_master " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                        "hrevaluation_analysis_master.analysisunkid " & _
            '                       ",hrevaluation_analysis_master.assessmodeid " & _
            '                       ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(result) <> 0 THEN result ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RESULT " & _
            '                  "FROM  " & StrDataBaseName & "..hrcompetency_analysis_tran " & _
            '                       "JOIN  " & StrDataBaseName & "..hrevaluation_analysis_master ON  " & StrDataBaseName & "..hrcompetency_analysis_tran.analysisunkid =  " & StrDataBaseName & "..hrevaluation_analysis_master.analysisunkid " & _
            '                  "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND iscommitted = 1 " & _
            '                       "AND (hrevaluation_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
            '                  "GROUP BY hrevaluation_analysis_master.assessmodeid,hrevaluation_analysis_master.analysisunkid " & _
            '             ") AS A ON hrevaluation_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
            '             "WHERE isvoid = 0 AND iscommitted = 1 AND (hrevaluation_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
            '             "GROUP BY  " & StrDataBaseName & "..hrevaluation_analysis_master.assessmodeid " & _
            '        ") AS GERESULT "

            StrQ = "SELECT " & _
                    "CAST(ISNULL(CASE WHEN COUNT(Rcnt) = 0  THEN 0 ELSE ISNULL(SUM(F_RESULT),0)/COUNT(Rcnt) END,0) AS DECIMAL(36,2)) AS GE_RESULT " & _
                   "FROM " & _
                   "( " & _
                         "SELECT " & _
                               "hrevaluation_analysis_master.assessmodeid " & _
                              ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(36,2)) AS F_RESULT " & _
                              ",ROW_NUMBER() OVER (ORDER BY hrevaluation_analysis_master.assessmodeid) AS Rcnt " & _
                         "FROM " & StrDataBaseName & "..hrevaluation_analysis_master " & _
                         "JOIN " & _
                         "( " & _
                              "SELECT " & _
                                    "hrevaluation_analysis_master.analysisunkid " & _
                                   ",hrevaluation_analysis_master.assessmodeid " & _
                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(result) <> 0 THEN result ELSE CAST(0 AS DECIMAL(36,2)) END)AS DECIMAL(36,2)),0) AS RESULT " & _
                              "FROM  " & StrDataBaseName & "..hrcompetency_analysis_tran " & _
                                   "JOIN  " & StrDataBaseName & "..hrevaluation_analysis_master ON  " & StrDataBaseName & "..hrcompetency_analysis_tran.analysisunkid =  " & StrDataBaseName & "..hrevaluation_analysis_master.analysisunkid " & _
                              "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND iscommitted = 1 " & _
                                   "AND (hrevaluation_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
                              "GROUP BY hrevaluation_analysis_master.assessmodeid,hrevaluation_analysis_master.analysisunkid " & _
                         ") AS A ON hrevaluation_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
                         "WHERE isvoid = 0 AND iscommitted = 1 AND (hrevaluation_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
                         "GROUP BY  " & StrDataBaseName & "..hrevaluation_analysis_master.assessmodeid " & _
                    ") AS GERESULT "
            'S.SANDEEP |21-AUG-2019| -- END

            'S.SANDEEP [04 JUN 2015] -- END



            dsList = objDataOperation.ExecQuery(StrQ, "Evaluation")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Evaluation; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Private Function Get_BSC(ByVal intEmpId As Integer, ByVal StrDataBaseName As String) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT " & _
            '            "CAST(ISNULL(CASE WHEN COUNT(Rcnt) = 0  THEN 0 ELSE ISNULL(SUM(F_RESULT),0)/COUNT(Rcnt) END,0) AS DECIMAL(10,2)) AS BSC_RESULT " & _
            '       "FROM " & _
            '       "( " & _
            '             "SELECT " & _
            '                   "hrbsc_analysis_master.assessmodeid " & _
            '                  ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS F_RESULT " & _
            '                  ",ROW_NUMBER() OVER (ORDER BY hrbsc_analysis_master.assessmodeid) AS Rcnt " & _
            '             "FROM " & StrDataBaseName & "..hrbsc_analysis_master " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                        "hrbsc_analysis_master.analysisunkid " & _
            '                       ",hrbsc_analysis_master.assessmodeid " & _
            '                       ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RESULT " & _
            '                  "FROM " & StrDataBaseName & "..hrbsc_analysis_tran " & _
            '                       "JOIN  " & StrDataBaseName & "..hrresult_master ON  " & StrDataBaseName & "..hrbsc_analysis_tran.resultunkid =  " & StrDataBaseName & "..hrresult_master.resultunkid " & _
            '                       "JOIN  " & StrDataBaseName & "..hrbsc_analysis_master ON  " & StrDataBaseName & "..hrbsc_analysis_tran.analysisunkid = hrbsc_analysis_master.analysisunkid " & _
            '                  "WHERE hrbsc_analysis_master.isvoid = 0 AND hrbsc_analysis_tran.isvoid = 0 AND iscommitted = 1 " & _
            '                       "AND (hrbsc_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrbsc_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
            '                  "GROUP BY hrbsc_analysis_master.assessmodeid,hrbsc_analysis_master.analysisunkid " & _
            '             ") AS A ON hrbsc_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
            '             "WHERE isvoid = 0 AND iscommitted = 1 AND (hrbsc_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrbsc_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
            '             "GROUP BY hrbsc_analysis_master.assessmodeid " & _
            '       ") AS BSCRESULT "


            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            'StrQ = "SELECT " & _
            '            "CAST(ISNULL(CASE WHEN COUNT(Rcnt) = 0  THEN 0 ELSE ISNULL(SUM(F_RESULT),0)/COUNT(Rcnt) END,0) AS DECIMAL(10,2)) AS BSC_RESULT " & _
            '       "FROM " & _
            '       "( " & _
            '             "SELECT " & _
            '                   "hrevaluation_analysis_master.assessmodeid " & _
            '                  ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS F_RESULT " & _
            '                  ",ROW_NUMBER() OVER (ORDER BY hrevaluation_analysis_master.assessmodeid) AS Rcnt " & _
            '             "FROM " & StrDataBaseName & "..hrevaluation_analysis_master " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                        "hrevaluation_analysis_master.analysisunkid " & _
            '                       ",hrevaluation_analysis_master.assessmodeid " & _
            '                       ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(result) <> 0 THEN result ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RESULT " & _
            '                  "FROM " & StrDataBaseName & "..hrgoals_analysis_tran " & _
            '                       "JOIN  " & StrDataBaseName & "..hrevaluation_analysis_master ON  " & StrDataBaseName & "..hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '                  "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND iscommitted = 1 " & _
            '                       "AND (hrevaluation_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
            '                  "GROUP BY hrevaluation_analysis_master.assessmodeid,hrevaluation_analysis_master.analysisunkid " & _
            '             ") AS A ON hrevaluation_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
            '             "WHERE isvoid = 0 AND iscommitted = 1 AND (hrevaluation_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
            '             "GROUP BY hrevaluation_analysis_master.assessmodeid " & _
            '       ") AS BSCRESULT "

            StrQ = "SELECT " & _
                        "CAST(ISNULL(CASE WHEN COUNT(Rcnt) = 0  THEN 0 ELSE ISNULL(SUM(F_RESULT),0)/COUNT(Rcnt) END,0) AS DECIMAL(36,2)) AS BSC_RESULT " & _
                   "FROM " & _
                   "( " & _
                         "SELECT " & _
                               "hrevaluation_analysis_master.assessmodeid " & _
                              ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(36,2)) AS F_RESULT " & _
                              ",ROW_NUMBER() OVER (ORDER BY hrevaluation_analysis_master.assessmodeid) AS Rcnt " & _
                         "FROM " & StrDataBaseName & "..hrevaluation_analysis_master " & _
                         "JOIN " & _
                         "( " & _
                              "SELECT " & _
                                    "hrevaluation_analysis_master.analysisunkid " & _
                                   ",hrevaluation_analysis_master.assessmodeid " & _
                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(result) <> 0 THEN result ELSE CAST(0 AS DECIMAL(36,2)) END)AS DECIMAL(36,2)),0) AS RESULT " & _
                              "FROM " & StrDataBaseName & "..hrgoals_analysis_tran " & _
                                   "JOIN  " & StrDataBaseName & "..hrevaluation_analysis_master ON  " & StrDataBaseName & "..hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                              "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND iscommitted = 1 " & _
                                   "AND (hrevaluation_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
                              "GROUP BY hrevaluation_analysis_master.assessmodeid,hrevaluation_analysis_master.analysisunkid " & _
                         ") AS A ON hrevaluation_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
                         "WHERE isvoid = 0 AND iscommitted = 1 AND (hrevaluation_analysis_master.selfemployeeunkid = '" & intEmpId & "' OR hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmpId & "') " & _
                         "GROUP BY hrevaluation_analysis_master.assessmodeid " & _
                   ") AS BSCRESULT "
            'S.SANDEEP |21-AUG-2019| -- END

            'S.SANDEEP [04 JUN 2015] -- END


            dsList = objDataOperation.ExecQuery(StrQ, "Evaluation")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_BSC; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Private Function Get_Awards(ByVal intEmpId As Integer, ByVal StrDataBase As String) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT TOP 1 " & _
                     " hrapps_finalemployee.employeeunkid " & _
                     ",hrapps_finalemployee.operationmodeid " & _
                     ",CASE WHEN operationmodeid = 1 THEN @SALARY_INCREMENT " & _
                            "WHEN operationmodeid = 2 THEN @COMMENDABLE_LETTER " & _
                            "WHEN operationmodeid = 3 THEN @IMPROVEMENT_LETTER " & _
                            "WHEN operationmodeid = 4 THEN @WARNING_LETTER " & _
                            "WHEN operationmodeid = 5 THEN @SETTING_REMINDER " & _
                      "ELSE '' END AS Remark " & _
                     ",CASE WHEN operationmodeid = 1 THEN " & _
                          "CASE WHEN prsalaryincrement_tran.increment_mode = 1 THEN @BYGRADE " & _
                               "WHEN prsalaryincrement_tran.increment_mode = 2 THEN @BYPERCENT " & _
                               "WHEN prsalaryincrement_tran.increment_mode = 3 THEN @BYVALUE ELSE '' " & _
                          "END " & _
                      "END AS Mode " & _
                      ",CASE WHEN operationmodeid = 1 THEN " & _
                          "CASE WHEN prsalaryincrement_tran.increment_mode = 1 THEN 1 " & _
                               "WHEN prsalaryincrement_tran.increment_mode = 2 THEN 2 " & _
                               "WHEN prsalaryincrement_tran.increment_mode = 3 THEN 3 ELSE 0 " & _
                          "END " & _
                      "END AS ModeId " & _
                     ",CASE WHEN operationmodeid = 1 THEN " & _
                          "CASE WHEN prsalaryincrement_tran.increment_mode = 2 THEN percentage " & _
                                "WHEN prsalaryincrement_tran.increment_mode = 3 THEN increment " & _
                          "ELSE 0 END " & _
                      "END AS ModeValue " & _
                   "FROM " & StrDataBase & "..hrapps_finalemployee " & _
                     "LEFT JOIN  " & StrDataBase & "..prsalaryincrement_tran ON  " & StrDataBase & "..hrapps_finalemployee.employeeunkid =  " & StrDataBase & "..prsalaryincrement_tran.employeeunkid " & _
                          "AND  " & StrDataBase & "..hrapps_finalemployee.periodunkid =  " & StrDataBase & "..prsalaryincrement_tran.periodunkid AND operationmodeid = '" & enAppraisal_Modes.SALARY_INCREMENT & "' AND prsalaryincrement_tran.isvoid = 0 " & _
                   "WHERE hrapps_finalemployee.employeeunkid = '" & intEmpId & "' AND isfinalshortlisted = 1 " & _
                   "ORDER BY finalemployeeunkid DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@SALARY_INCREMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Salary Increment"))
            objDataOperation.AddParameter("@COMMENDABLE_LETTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 33, "Commendable Letter"))
            objDataOperation.AddParameter("@IMPROVEMENT_LETTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "Improvement Letter"))
            objDataOperation.AddParameter("@WARNING_LETTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Warning Letter"))
            objDataOperation.AddParameter("@SETTING_REMINDER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "Reminder Setting"))
            objDataOperation.AddParameter("@BYGRADE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "By Grade"))
            objDataOperation.AddParameter("@BYPERCENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "By Percentage"))
            objDataOperation.AddParameter("@BYVALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 28, "By Amount"))


            dsList = objDataOperation.ExecQuery(StrQ, "Evaluation")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Awards; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Private Function Get_DataBases() As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                      " yearunkid " & _
                      ",financialyear_name " & _
                      ",database_name " & _
                      ",SYear AS SYear " & _
                      ",EYear AS EYear " & _
                      ",1 AS isQry " & _
                   "FROM " & _
                   "( " & _
                      "SELECT TOP 2 " & _
                           " yearunkid " & _
                           ",financialyear_name " & _
                           ",database_name " & _
                           ",YEAR(start_date)AS SYear " & _
                           ",YEAR(end_date)AS EYear " & _
                           ",ROW_NUMBER() OVER (ORDER BY yearunkid ASC) AS Rno " & _
                      "FROM hrmsConfiguration..cffinancial_year_tran " & _
                      "WHERE companyunkid = '" & Company._Object._Companyunkid & "' ORDER BY yearunkid DESC " & _
                   ")AS dbNames " & _
                   "WHERE 1 = 1 ORDER BY Rno ASC "

            dsList = objDataOperation.ExecQuery(StrQ, "Databases")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("Databases").Rows.Count = 1 Then
                Dim drRow As DataRow = dsList.Tables("Databases").NewRow
                Dim StrOldDataBase As String = ""

                StrOldDataBase = dsList.Tables("Databases").Rows(0).Item("database_name")

                StrOldDataBase = StrOldDataBase.Replace(dsList.Tables("Databases").Rows(0).Item("SYear"), CInt(dsList.Tables("Databases").Rows(0).Item("SYear")) - 1)
                StrOldDataBase = StrOldDataBase.Replace(dsList.Tables("Databases").Rows(0).Item("EYear"), CInt(dsList.Tables("Databases").Rows(0).Item("EYear")) - 1)

                drRow.Item("yearunkid") = 0
                drRow.Item("financialyear_name") = (CInt(dsList.Tables("Databases").Rows(0).Item("SYear")) - 1).ToString & "-" & (CInt(dsList.Tables("Databases").Rows(0).Item("EYear")) - 1).ToString
                drRow.Item("database_name") = StrOldDataBase
                drRow.Item("SYear") = CInt(dsList.Tables("Databases").Rows(0).Item("SYear")) - 1
                drRow.Item("EYear") = CInt(dsList.Tables("Databases").Rows(0).Item("EYear")) - 1
                drRow.Item("isQry") = 0

                dsList.Tables("Databases").Rows.InsertAt(drRow, 0)

            End If

            StrCaption = New String(1) {}
            Dim k As Integer = 0
            For Each drRow As DataRow In dsList.Tables(0).Rows
                Select Case k
                    Case 0
                        StrCaption(0) = drRow.Item("financialyear_name")
                    Case 1
                        StrCaption(1) = drRow.Item("financialyear_name")
                End Select
                k += 1
            Next

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_DataBases; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try
            'HEADER PART
            strBuilder.Append(" <TITLE>" & Me._ReportName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            'LOGO SETTINGS
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If

                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 31, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='60%' colspan = 14 align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 32, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(ConfigParameter._Object._CurrentDateAndTime.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='60%' colspan = 14  align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            For j As Integer = 0 To objDataReader.Columns.Count - 1
                Select Case j
                    Case 14, 15, 19
                        strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & vbCrLf & StrCaption(0) & "</B></FONT></TD>" & vbCrLf)
                    Case 16, 17, 20
                        strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & vbCrLf & StrCaption(1) & "</B></FONT></TD>" & vbCrLf)
                    Case Else
                        strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                End Select
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            For i As Integer = 0 To objDataReader.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1
                    If k = 18 Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    Else
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)

            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '
    '
    '
    '
    '
    '
    '
    '
    '
    '-------------------------------------------------------------------------------------
    'Public Sub Export_Performance_Appraisal_Report()
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Try
    '        objDataOperation = New clsDataOperation

    '        '******************* CREATING TABLE STRUCTURE **************> START
    '        Call CreateTable()
    '        '******************* CREATING TABLE STRUCTURE **************> END



    '        '******************* GETTING DATABASE NAMES **************> START
    '        Dim dsYears As New DataSet
    '        dsYears = Get_DataBases()
    '        '******************* GETTING DATABASE NAMES **************> END


    '        '******************* EMPLOYEE DATA **************> START
    '        StrQ = "SELECT " & _
    '                " ISNULL(employeecode,'') AS EMPL_NO " & _
    '                ",ISNULL(surname,'') AS SURNAME " & _
    '                ",ISNULL(firstname,'') AS FIRSTNAME " & _
    '                ",ISNULL(cfcommon_master.name,'') AS INITIALS " & _
    '                ",ISNULL(STUFF((SELECT '\r\n' + qualificationname FROM hremp_qualification_tran t2 " & _
    '                               "JOIN hrqualification_master ON t2.qualificationunkid = hrqualification_master.qualificationunkid " & _
    '                        "WHERE hremployee_master.employeeunkid = t2.employeeunkid FOR XML PATH('')),1,0,''),'') POSSESED_QUALIFICATION " & _
    '                ",ISNULL(hrgradegroup_master.name,'') AS GRADE " & _
    '                ",ISNULL(CONVERT(CHAR(8),appointeddate,112),'') AS EMPLOYMENT_DATE " & _
    '                ",ISNULL(CONVERT(CHAR(8),confirmation_date,112),'') AS CONFERMATION_DATE " & _
    '                ",ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
    '                ",ISNULL(CONVERT(CHAR(8),auditdatetime,112),'') AS DATE_CURR_TITLE " & _
    '                ",ISNULL(CONVERT(CHAR(8),ODATE,112),'') AS LAST_PROMO_DATE " & _
    '                ",ISNULL(hrclassgroup_master.name,'') AS REGION " & _
    '                ",ISNULL(hrclasses_master.name,'') AS CURR_WORKING_STATION " & _
    '                ",ISNULL(CurrentSalary,0) AS CURRENT_SALARY " & _
    '                ",ISNULL(hremployee_master.jobgroupunkid,0) AS JOBGRPID " & _
    '                ",ISNULL(hrjobgroup_master.weight,0) AS GEWEIGHT " & _
    '                ",(100 - ISNULL(hrjobgroup_master.weight,0)) AS BSCWEIGHT " & _
    '                ",hremployee_master.employeeunkid AS EmpId " & _
    '                "FROM hremployee_master " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                     "SELECT " & _
    '                           "employeeunkid " & _
    '                          ",incrementdate " & _
    '                          ",CurrentSalary " & _
    '                     "FROM " & _
    '                     "( " & _
    '                          "SELECT " & _
    '                                "ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC) AS rn " & _
    '                               ",employeeunkid " & _
    '                               ",prsalaryincrement_tran.incrementdate " & _
    '                               ",newscale AS CurrentSalary " & _
    '                          "FROM prsalaryincrement_tran " & _
    '                          "WHERE  CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) <= @DATE " & _
    '                          "AND prsalaryincrement_tran.isapproved = 1 " & _
    '                     ") AS SAL " & _
    '                     "WHERE rn = 1 " & _
    '                ")AS CURR_SAL ON hremployee_master.employeeunkid = CURR_SAL.employeeunkid " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                     "SELECT " & _
    '                           "employeeunkid " & _
    '                          ",jobunkid " & _
    '                          ",auditdatetime " & _
    '                          ",atemployeeunkid " & _
    '                     "FROM " & _
    '                     "( " & _
    '                          "SELECT " & _
    '                                "ROW_NUMBER() OVER (PARTITION BY athremployee_master.employeeunkid ORDER BY auditdatetime ASC) AS rn " & _
    '                               ",athremployee_master.employeeunkid " & _
    '                               ",athremployee_master.jobunkid " & _
    '                               ",auditdatetime " & _
    '                               ",atemployeeunkid " & _
    '                          "FROM athremployee_master " & _
    '                          "JOIN " & _
    '                          "( " & _
    '                               "SELECT " & _
    '                                    "employeeunkid,jobunkid " & _
    '                               "FROM hremployee_master " & _
    '                          ") AS Job ON athremployee_master.employeeunkid = Job.employeeunkid AND athremployee_master.jobunkid = Job.jobunkid " & _
    '                          "WHERE 1 = 1 " & _
    '                     ") AS CURR " & _
    '                     "WHERE rn = 1 " & _
    '                ")AS CURR_DATE ON hremployee_master.employeeunkid = CURR_DATE.employeeunkid " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                     "SELECT " & _
    '                           "employeeunkid " & _
    '                          ",jobunkid " & _
    '                          ",auditdatetime AS ODATE " & _
    '                     "FROM " & _
    '                     "( " & _
    '                          "SELECT " & _
    '                                "ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY auditdatetime DESC) AS CN " & _
    '                               ",employeeunkid " & _
    '                               ",jobunkid " & _
    '                               ",auditdatetime " & _
    '                               ",atemployeeunkid " & _
    '                          "FROM " & _
    '                          "( " & _
    '                               "SELECT " & _
    '                                     "ROW_NUMBER() OVER (PARTITION BY athremployee_master.employeeunkid,athremployee_master.jobunkid ORDER BY auditdatetime ASC) AS rn " & _
    '                                    ",athremployee_master.employeeunkid " & _
    '                                    ",athremployee_master.jobunkid " & _
    '                                    ",auditdatetime " & _
    '                                    ",atemployeeunkid " & _
    '                               "FROM athremployee_master " & _
    '                               "JOIN " & _
    '                               "( " & _
    '                                    "SELECT " & _
    '                                         "employeeunkid,jobunkid " & _
    '                                    "FROM hremployee_master " & _
    '                               ") AS Job ON athremployee_master.employeeunkid = Job.employeeunkid AND athremployee_master.jobunkid <> Job.jobunkid " & _
    '                               "WHERE 1 = 1 " & _
    '                          ") AS OLD_DATE " & _
    '                          "WHERE rn = 1 " & _
    '                     ")AS OLD_PROMO_DATE " & _
    '                     "WHERE CN = 1 " & _
    '                ") AS OLD_PRO_DATE ON hremployee_master.employeeunkid = OLD_PRO_DATE.employeeunkid " & _
    '                     "JOIN hrjobgroup_master ON hremployee_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
    '                     "JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '                     "JOIN hrgradegroup_master ON hremployee_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
    '                     "LEFT JOIN cfcommon_master ON hremployee_master.titleunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.TITLE & "' " & _
    '                     "LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
    '                     "LEFT JOIN hrclassgroup_master ON hremployee_master.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
    '                "WHERE hremployee_master.gradegroupunkid IN (" & mstrGradesIds & ") "
    '        '       'Sohail (24 Sep 2012) - [isapproved]

    '        'S.SANDEEP [ 12 MAY 2012 ] -- START
    '        'ISSUE : TRA ENHANCEMENTS
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If
    '        'S.SANDEEP [ 12 MAY 2012 ] -- END


    '        If mintEmployeeUnkid > 0 Then
    '            StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeUnkid & "'"
    '        End If

    '        Call FilterTitleAndFilterQuery()

    '        objDataOperation.AddParameter("@DATE", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime))

    '        Dim dsEmpData As New DataSet
    '        dsEmpData = objDataOperation.ExecQuery(StrQ, "EmpData")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '******************* EMPLOYEE DATA **************> END

    '        '******************* LOGIC FOR SETTING DATA **************> START
    '        Dim i As Integer = 1
    '        For Each dRow As DataRow In dsEmpData.Tables("EmpData").Rows
    '            Dim dtRow As DataRow = mdtFinalTable.NewRow

    '            dtRow.Item("Column1") = i.ToString
    '            dtRow.Item("Column2") = dRow.Item("EMPL_NO")
    '            dtRow.Item("Column3") = dRow.Item("SURNAME")
    '            dtRow.Item("Column4") = dRow.Item("FIRSTNAME")
    '            dtRow.Item("Column5") = dRow.Item("INITIALS")
    '            dtRow.Item("Column6") = dRow.Item("POSSESED_QUALIFICATION").ToString.Replace("\r\n", vbCrLf)
    '            dtRow.Item("Column7") = dRow.Item("GRADE")
    '            If dRow.Item("EMPLOYMENT_DATE").ToString.Trim.Length > 0 Then
    '                dtRow.Item("Column8") = eZeeDate.convertDate(dRow.Item("EMPLOYMENT_DATE").ToString).ToShortDateString
    '            Else
    '                dtRow.Item("Column8") = ""
    '            End If
    '            If dRow.Item("CONFERMATION_DATE").ToString.Trim.Length > 0 Then
    '                dtRow.Item("Column9") = eZeeDate.convertDate(dRow.Item("CONFERMATION_DATE").ToString).ToShortDateString
    '            Else
    '                dtRow.Item("Column9") = ""
    '            End If
    '            dtRow.Item("Column10") = dRow.Item("JOB_TITLE")
    '            If dRow.Item("DATE_CURR_TITLE").ToString.Trim.Length > 0 Then
    '                dtRow.Item("Column11") = eZeeDate.convertDate(dRow.Item("DATE_CURR_TITLE").ToString).ToShortDateString
    '            Else
    '                dtRow.Item("Column11") = ""
    '            End If
    '            If dRow.Item("LAST_PROMO_DATE").ToString.Trim.Length > 0 Then
    '                dtRow.Item("Column12") = eZeeDate.convertDate(dRow.Item("LAST_PROMO_DATE").ToString).ToShortDateString
    '            Else
    '                dtRow.Item("Column12") = ""
    '            End If
    '            dtRow.Item("Column13") = dRow.Item("REGION")
    '            dtRow.Item("Column14") = dRow.Item("CURR_WORKING_STATION")

    '            '********* OLD YEAR DATA *************
    '            Dim j As Integer = 0
    '            For Each dYRow As DataRow In dsYears.Tables(0).Rows
    '                If dYRow.Item("isQry") = 0 Then
    '                    dtRow.Item("Column15") = 0 & " % "
    '                    dtRow.Item("Column16") = ""
    '                Else
    '                    Dim dsBSC As New DataSet
    '                    Dim dsGE As New DataSet
    '                    dsBSC = Get_BSC(dRow.Item("EmpId"), dYRow.Item("database_name"))
    '                    dsGE = Get_Evaluation(dRow.Item("EmpId"), dYRow.Item("database_name"))
    '                    Dim dblBSCTotal As Double = 0 : Dim dblGETotal As Double = 0
    '                    If dsBSC.Tables(0).Rows.Count > 0 Then
    '                        dblBSCTotal = (CDbl(dsBSC.Tables(0).Rows(0).Item("BSC_RESULT")) * CDbl(dRow.Item("BSCWEIGHT"))) / 100
    '                    End If

    '                    If dsGE.Tables(0).Rows.Count > 0 Then
    '                        dblGETotal = (CDbl(dsGE.Tables(0).Rows(0).Item("GE_RESULT")) * CDbl(dRow.Item("GEWEIGHT"))) / 100
    '                    End If
    '                    If j = 0 Then
    '                        dtRow.Item("Column15") = CDbl(dblGETotal + dblBSCTotal) & "%"
    '                    Else
    '                        dtRow.Item("Column17") = CDbl(dblGETotal + dblBSCTotal) & "%"
    '                    End If
    '                    Dim dsRemark As New DataSet
    '                    dsRemark = Get_Awards(dRow.Item("EmpId"), dYRow.Item("database_name"))

    '                    If dsRemark.Tables(0).Rows.Count > 0 Then

    '                        If j = 0 Then
    '                            dtRow.Item("Column16") = dsRemark.Tables(0).Rows(0)("Remark")
    '                        Else
    '                            dtRow.Item("Column18") = dsRemark.Tables(0).Rows(0)("Remark")
    '                        End If

    '                        If dsRemark.Tables(0).Rows(0).Item("operationmodeid") = 1 Then
    '                            If j = 0 Then
    '                                Select Case dsRemark.Tables(0).Rows(0)("ModeId")
    '                                    Case 2
    '                                        dtRow.Item("Column20") = dsRemark.Tables(0).Rows(0)("Mode") & " [ " & dsRemark.Tables(0).Rows(0)("ModeValue").ToString("###.##") & " ]"
    '                                    Case 3
    '                                        dtRow.Item("Column20") = dsRemark.Tables(0).Rows(0)("Mode") & " [ " & Format(CDec(dsRemark.Tables(0).Rows(0)("ModeValue")), GUI.fmtCurrency) & " ]"
    '                                End Select
    '                            Else
    '                                Select Case dsRemark.Tables(0).Rows(0)("ModeId")
    '                                    Case 2
    '                                        dtRow.Item("Column21") = dsRemark.Tables(0).Rows(0)("Mode") & " [ " & dsRemark.Tables(0).Rows(0)("ModeValue").ToString("###.##") & " ]"
    '                                    Case 3
    '                                        dtRow.Item("Column21") = dsRemark.Tables(0).Rows(0)("Mode") & " [ " & Format(CDec(dsRemark.Tables(0).Rows(0)("ModeValue")), GUI.fmtCurrency) & " ]"
    '                                End Select
    '                            End If

    '                        End If

    '                    Else
    '                        If j = 0 Then
    '                            dtRow.Item("Column16") = ""
    '                        Else
    '                            dtRow.Item("Column18") = ""
    '                        End If
    '                    End If
    '                End If
    '                j += 1
    '            Next
    '            '********* OLD YEAR DATA *************

    '            dtRow.Item("Column19") = Format(CDec(dRow.Item("CURRENT_SALARY")), GUI.fmtCurrency)
    '            dtRow.Item("Column22") = ""
    '            dtRow.Item("Column23") = ""
    '            dtRow.Item("Column24") = ""

    '            mdtFinalTable.Rows.Add(dtRow)
    '            i += 1
    '        Next
    '        '******************* LOGIC FOR SETTING DATA **************> END

    '        '******************* EXPORTING REPORT **************> START
    '        If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, mdtFinalTable) Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
    '            Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
    '        End If
    '        '******************* EXPORTING REPORT **************> END

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Export_Performance_Appraisal_Report; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Public Sub Export_Performance_Appraisal_Report(ByVal strDatabaseName As String, _
                                                   ByVal intUserUnkid As Integer, _
                                                   ByVal intYearUnkid As Integer, _
                                                   ByVal intCompanyUnkid As Integer, _
                                                   ByVal dtPeriodStart As Date, _
                                                   ByVal dtPeriodEnd As Date, _
                                                   ByVal strUserModeSetting As String, _
                                                   ByVal blnOnlyApproved As Boolean, _
                                                   ByVal strExportReportPath As String, _
                                                   ByVal blnOpenReportAfterExport As Boolean, _
                                                   ByRef strExportFileName As String) 'S.SANDEEP [13-JUL-2017] -- START -- END
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [13-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : 
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid
            'S.SANDEEP [13-JUL-2017] -- END


            '******************* CREATING TABLE STRUCTURE **************> START
            Call CreateTable()
            '******************* CREATING TABLE STRUCTURE **************> END



            '******************* GETTING DATABASE NAMES **************> START
            Dim dsYears As New DataSet
            dsYears = Get_DataBases()
            '******************* GETTING DATABASE NAMES **************> END


            '******************* EMPLOYEE DATA **************> START
            Dim xDateJoinQry, xDateFilterQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACFiltrQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            'S.SANDEEP [15 NOV 2016] -- START
            Dim xUACQry As String = String.Empty
            'Call NewAccessLevelFilterString("", xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call NewAccessLevelFilterString(xUACQry, "", dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [15 NOV 2016] -- END

            StrQ = "SELECT " & _
                   "     employeecode AS EMPL_NO " & _
                   "    ,surname AS SURNAME " & _
                   "    ,firstname AS FIRSTNAME " & _
                   "    ,ISNULL(cfcommon_master.name,'') AS INITIALS " & _
                   "    ,ISNULL(STUFF((SELECT '\r\n' + qualificationname FROM hremp_qualification_tran t2 " & _
                   "                   JOIN hrqualification_master ON t2.qualificationunkid = hrqualification_master.qualificationunkid " & _
                   "                   WHERE hremployee_master.employeeunkid = t2.employeeunkid FOR XML PATH('')),1,4,''),'') POSSESED_QUALIFICATION " & _
                   "    ,ISNULL(hrgradegroup_master.name,'') AS GRADE " & _
                   "    ,ISNULL(CONVERT(NVARCHAR(8),appointeddate,112),'') AS EMPLOYMENT_DATE " & _
                   "    ,ISNULL(CONVERT(NVARCHAR(8),CNF.date1,112),'') AS CONFERMATION_DATE " & _
                   "    ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                   "    ,ISNULL(CONVERT(NVARCHAR(8),JCRR.effectivedate,112),'') AS DATE_CURR_TITLE " & _
                   "    ,ISNULL(CONVERT(NVARCHAR(8),LPD.effectivedate,112),'') AS LAST_PROMO_DATE " & _
                   "    ,ISNULL(hrclassgroup_master.name,'') AS REGION " & _
                   "    ,ISNULL(hrclasses_master.name,'') AS CURR_WORKING_STATION " & _
                   "    ,ISNULL(CurrentSalary,0) AS CURRENT_SALARY " & _
                   "    ,ISNULL(JCRR.jobgroupunkid,0) AS JOBGRPID " & _
                   "    ,ISNULL(hrjobgroup_master.weight,0) AS GEWEIGHT " & _
                   "    ,(100 - ISNULL(hrjobgroup_master.weight,0)) AS BSCWEIGHT " & _
                   "    ,hremployee_master.employeeunkid AS EmpId " & _
                   "FROM hremployee_master " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             employeeunkid " & _
                   "            ,classgroupunkid " & _
                   "            ,classunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_transfer_tran " & _
                   "        WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "    ) AS EWR ON EWR.employeeunkid = hremployee_master.employeeunkid AND EWR.rno = 1 " & _
                   "    LEFT JOIN hrclassgroup_master ON EWR.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                   "    LEFT JOIN hrclasses_master ON EWR.classunkid = hrclasses_master.classesunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "    SELECT " & _
                   "         employeeunkid " & _
                   "        ,jobunkid " & _
                   "        ,jobgroupunkid " & _
                   "        ,effectivedate " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "    FROM hremployee_categorization_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "    ) AS JCRR ON JCRR.employeeunkid = hremployee_master.employeeunkid AND JCRR.rno = 1 " & _
                   "    LEFT JOIN hrjob_master ON JCRR.jobunkid = hrjob_master.jobunkid " & _
                   "    LEFT JOIN hrjobgroup_master ON JCRR.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "    SELECT " & _
                   "         employeeunkid " & _
                   "        ,effectivedate " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "    FROM hremployee_categorization_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "    ) AS LPD ON LPD.employeeunkid = hremployee_master.employeeunkid AND LPD.rno = 2 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "    SELECT " & _
                   "         employeeunkid " & _
                   "        ,date1 " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "    FROM hremployee_dates_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "    AND datetypeunkid = 2 " & _
                   "    ) AS CNF ON CNF.employeeunkid = hremployee_master.employeeunkid AND CNF.rno = 1 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "    SELECT " & _
                   "         employeeunkid " & _
                   "        ,newscale AS CurrentSalary " & _
                   "        ,gradegroupunkid " & _
                   "        ,gradeunkid " & _
                   "        ,gradelevelunkid " & _
                   "        ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                   "    FROM prsalaryincrement_tran " & _
                   "    WHERE isvoid = 0 AND isapproved = 1 " & _
                   "    AND CONVERT(NVARCHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "    ) AS SAL ON SAL.employeeunkid = hremployee_master.employeeunkid AND SAL.rno = 1 " & _
                   "    JOIN hrgradegroup_master ON SAL.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                   "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.TITLE & "' "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            StrQ &= "WHERE 1 = 1 AND SAL.gradegroupunkid IN (" & mstrGradesIds & ") "

            If mblnIncludeInactiveEmp = False Then
                StrQ &= xDateFilterQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END


            If mintEmployeeUnkid > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeUnkid & "'"
            End If

            Call FilterTitleAndFilterQuery()

            Dim dsEmpData As New DataSet
            dsEmpData = objDataOperation.ExecQuery(StrQ, "EmpData")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            '******************* EMPLOYEE DATA **************> END

            '******************* LOGIC FOR SETTING DATA **************> START
            Dim i As Integer = 1
            For Each dRow As DataRow In dsEmpData.Tables("EmpData").Rows
                Dim dtRow As DataRow = mdtFinalTable.NewRow

                dtRow.Item("Column1") = i.ToString
                dtRow.Item("Column2") = dRow.Item("EMPL_NO")
                dtRow.Item("Column3") = dRow.Item("SURNAME")
                dtRow.Item("Column4") = dRow.Item("FIRSTNAME")
                dtRow.Item("Column5") = dRow.Item("INITIALS")
                dtRow.Item("Column6") = dRow.Item("POSSESED_QUALIFICATION").ToString.Replace("\r\n", vbCrLf)
                dtRow.Item("Column7") = dRow.Item("GRADE")
                If dRow.Item("EMPLOYMENT_DATE").ToString.Trim.Length > 0 Then
                    dtRow.Item("Column8") = eZeeDate.convertDate(dRow.Item("EMPLOYMENT_DATE").ToString).ToShortDateString
                Else
                    dtRow.Item("Column8") = ""
                End If
                If dRow.Item("CONFERMATION_DATE").ToString.Trim.Length > 0 Then
                    dtRow.Item("Column9") = eZeeDate.convertDate(dRow.Item("CONFERMATION_DATE").ToString).ToShortDateString
                Else
                    dtRow.Item("Column9") = ""
                End If
                dtRow.Item("Column10") = dRow.Item("JOB_TITLE")
                If dRow.Item("DATE_CURR_TITLE").ToString.Trim.Length > 0 Then
                    dtRow.Item("Column11") = eZeeDate.convertDate(dRow.Item("DATE_CURR_TITLE").ToString).ToShortDateString
                Else
                    dtRow.Item("Column11") = ""
                End If
                If dRow.Item("LAST_PROMO_DATE").ToString.Trim.Length > 0 Then
                    dtRow.Item("Column12") = eZeeDate.convertDate(dRow.Item("LAST_PROMO_DATE").ToString).ToShortDateString
                Else
                    dtRow.Item("Column12") = ""
                End If
                dtRow.Item("Column13") = dRow.Item("REGION")
                dtRow.Item("Column14") = dRow.Item("CURR_WORKING_STATION")

                '********* OLD YEAR DATA *************
                Dim j As Integer = 0
                For Each dYRow As DataRow In dsYears.Tables(0).Rows
                    If dYRow.Item("isQry") = 0 Then
                        dtRow.Item("Column15") = 0 & " % "
                        dtRow.Item("Column16") = ""
                    Else
                        Dim dsBSC As New DataSet
                        Dim dsGE As New DataSet
                        dsBSC = Get_BSC(dRow.Item("EmpId"), dYRow.Item("database_name"))
                        dsGE = Get_Evaluation(dRow.Item("EmpId"), dYRow.Item("database_name"))
                        Dim dblBSCTotal As Double = 0 : Dim dblGETotal As Double = 0
                        If dsBSC.Tables(0).Rows.Count > 0 Then
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'dblBSCTotal = (CDbl(dsBSC.Tables(0).Rows(0).Item("BSC_RESULT")) * CDbl(dRow.Item("BSCWEIGHT"))) / 100
                            dblBSCTotal = Format(CDec((CDbl(dsBSC.Tables(0).Rows(0).Item("BSC_RESULT")) * CDbl(dRow.Item("BSCWEIGHT"))) / 100), "###################0.#0")
                            'S.SANDEEP |21-AUG-2019| -- END
                        End If

                        If dsGE.Tables(0).Rows.Count > 0 Then
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'dblGETotal = (CDbl(dsGE.Tables(0).Rows(0).Item("GE_RESULT")) * CDbl(dRow.Item("GEWEIGHT"))) / 100
                            dblGETotal = Format(CDec((CDbl(dsGE.Tables(0).Rows(0).Item("GE_RESULT")) * CDbl(dRow.Item("GEWEIGHT"))) / 100), "###################0.#0")
                            'S.SANDEEP |21-AUG-2019| -- END
                        End If
                        If j = 0 Then
                            dtRow.Item("Column15") = CDbl(dblGETotal + dblBSCTotal) & "%"
                        Else
                            dtRow.Item("Column17") = CDbl(dblGETotal + dblBSCTotal) & "%"
                        End If
                        Dim dsRemark As New DataSet
                        dsRemark = Get_Awards(dRow.Item("EmpId"), dYRow.Item("database_name"))

                        If dsRemark.Tables(0).Rows.Count > 0 Then

                            If j = 0 Then
                                dtRow.Item("Column16") = dsRemark.Tables(0).Rows(0)("Remark")
                            Else
                                dtRow.Item("Column18") = dsRemark.Tables(0).Rows(0)("Remark")
                            End If

                            If dsRemark.Tables(0).Rows(0).Item("operationmodeid") = 1 Then
                                If j = 0 Then
                                    Select Case dsRemark.Tables(0).Rows(0)("ModeId")
                                        Case 2
                                            'S.SANDEEP |21-AUG-2019| -- START
                                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                                            'dtRow.Item("Column20") = dsRemark.Tables(0).Rows(0)("Mode") & " [ " & dsRemark.Tables(0).Rows(0)("ModeValue").ToString("###.##") & " ]"
                                            dtRow.Item("Column20") = dsRemark.Tables(0).Rows(0)("Mode") & " [ " & dsRemark.Tables(0).Rows(0)("ModeValue").ToString("##########0.#0") & " ]"
                                            'S.SANDEEP |21-AUG-2019| -- END
                                        Case 3
                                            dtRow.Item("Column20") = dsRemark.Tables(0).Rows(0)("Mode") & " [ " & Format(CDec(dsRemark.Tables(0).Rows(0)("ModeValue")), GUI.fmtCurrency) & " ]"
                                    End Select
                                Else
                                    Select Case dsRemark.Tables(0).Rows(0)("ModeId")
                                        Case 2
                                            dtRow.Item("Column21") = dsRemark.Tables(0).Rows(0)("Mode") & " [ " & dsRemark.Tables(0).Rows(0)("ModeValue").ToString("###.##") & " ]"
                                        Case 3
                                            dtRow.Item("Column21") = dsRemark.Tables(0).Rows(0)("Mode") & " [ " & Format(CDec(dsRemark.Tables(0).Rows(0)("ModeValue")), GUI.fmtCurrency) & " ]"
                                    End Select
                                End If

                            End If

                        Else
                            If j = 0 Then
                                dtRow.Item("Column16") = ""
                            Else
                                dtRow.Item("Column18") = ""
                            End If
                        End If
                    End If
                    j += 1
                Next
                '********* OLD YEAR DATA *************

                dtRow.Item("Column19") = Format(CDec(dRow.Item("CURRENT_SALARY")), GUI.fmtCurrency)
                dtRow.Item("Column22") = ""
                dtRow.Item("Column23") = ""
                dtRow.Item("Column24") = ""

                mdtFinalTable.Rows.Add(dtRow)
                i += 1
            Next
            '******************* LOGIC FOR SETTING DATA **************> END

            '******************* EXPORTING REPORT **************> START

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, mdtFinalTable) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
            '    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
            'End If

            If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), strExportReportPath, mdtFinalTable) Then
                strExportFileName = Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
                Call ReportFunction.Open_ExportedFile(blnOpenReportAfterExport, StrFinalPath)
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            '******************* EXPORTING REPORT **************> END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Performance_Appraisal_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "S/N")
            Language.setMessage(mstrModuleName, 2, "EMPL. NO.")
            Language.setMessage(mstrModuleName, 3, "SURNAME")
            Language.setMessage(mstrModuleName, 4, "FIRST NAME")
            Language.setMessage(mstrModuleName, 5, "INITIALS")
            Language.setMessage(mstrModuleName, 6, "PROCESSED QUALIFICATION")
            Language.setMessage(mstrModuleName, 7, "GRADE")
            Language.setMessage(mstrModuleName, 8, "DATE OF EMPLOYMENT")
            Language.setMessage(mstrModuleName, 9, "DATE OF CONFIRMATION")
            Language.setMessage(mstrModuleName, 10, "JOB TITLE")
            Language.setMessage(mstrModuleName, 11, "DATE OF CURRENT JOB TITLE")
            Language.setMessage(mstrModuleName, 12, "DATE OF LAST PROMOTION")
            Language.setMessage(mstrModuleName, 13, "REGION")
            Language.setMessage(mstrModuleName, 14, "CURRENT WORKING STATION")
            Language.setMessage(mstrModuleName, 15, "SCORE")
            Language.setMessage(mstrModuleName, 16, "REMARKS")
            Language.setMessage(mstrModuleName, 17, "CURRENT SALARY")
            Language.setMessage(mstrModuleName, 18, "AWARDS")
            Language.setMessage(mstrModuleName, 19, "PROPOSED NEW TITLE")
            Language.setMessage(mstrModuleName, 20, "PROPOSED NEW SALARY")
            Language.setMessage(mstrModuleName, 21, "HOUSE ALLOWANCE")
            Language.setMessage(mstrModuleName, 22, "Salary Increment")
            Language.setMessage(mstrModuleName, 23, "Improvement Letter")
            Language.setMessage(mstrModuleName, 24, "Warning Letter")
            Language.setMessage(mstrModuleName, 25, "Reminder Setting")
            Language.setMessage(mstrModuleName, 26, "By Grade")
            Language.setMessage(mstrModuleName, 27, "By Percentage")
            Language.setMessage(mstrModuleName, 28, "By Amount")
            Language.setMessage(mstrModuleName, 29, "Employee :")
            Language.setMessage(mstrModuleName, 30, "Grades :")
            Language.setMessage(mstrModuleName, 31, "Prepared By :")
            Language.setMessage(mstrModuleName, 32, "Date :")
            Language.setMessage(mstrModuleName, 33, "Commendable Letter")
            Language.setMessage(mstrModuleName, 34, "Report successfully exported to Report export path")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
