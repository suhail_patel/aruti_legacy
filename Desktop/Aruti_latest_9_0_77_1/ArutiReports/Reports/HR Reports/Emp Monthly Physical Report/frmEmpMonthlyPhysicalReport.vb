'************************************************************************************************************************************
'Class Name : frmEmpMonthlyPhysicalReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmpMonthlyPhysicalReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmpMonthlyPhysicalReport"
    Private objEPhysicalReport As clsEmpMonthlyPhysicalReport

#End Region

#Region " Contructor "

    Public Sub New()
        objEPhysicalReport = New clsEmpMonthlyPhysicalReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEPhysicalReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Try
            '--------- Year

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objEPhysicalReport.GetYearList("List")
            dsCombos = objEPhysicalReport.GetYearList(Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, "List")
            'S.SANDEEP [04 JUN 2015] -- END
            With cboYear
                .DisplayMember = "Years"
                .DataSource = dsCombos.Tables("List")
            End With
            '--------- Month
            dsCombos = objEPhysicalReport.GetMonthList("List")
            With cboMonth
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboMonth.SelectedValue = 0

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try

            objEPhysicalReport.SetDefaultValue()

            If cboMonth.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select month to continue."), enMsgBoxStyle.Information)
                cboMonth.Focus()
                Return False
            End If

            objEPhysicalReport._CMonthId = cboMonth.SelectedValue
            objEPhysicalReport._CMonthName = cboMonth.Text
            objEPhysicalReport._CYearName = cboYear.Text

            Dim iCurr_Date, iPrev_Date As String
            iCurr_Date = "" : iPrev_Date = ""

            'DateAdd(DateInterval.Month, CInt(cboMonth.SelectedValue) - 1, DateAdd(DateInterval.Year, CInt(cboYear.SelectedValue) - 1900, 0)) /* FIRST DATE OF SELECTED MONTH */
            'DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, CInt(cboMonth.SelectedValue), DateAdd(DateInterval.Year, CInt(cboYear.SelectedValue) - 1900, 0))) /* LAST DATE OF SELECTED MONTH */
            'S.SANDEEP [20 OCT 2016] -- START
            'iCurr_Date = eZeeDate.convertDate(DateAdd(DateInterval.Month, 0, eZeeDate.convertDate(cboYear.Text & Format(cboMonth.SelectedValue, "0#") & "01")).AddDays(-1))
            Dim startDate = New DateTime(CInt(cboYear.Text), CInt(cboMonth.SelectedValue), 1)
            Dim endDate = startDate.AddMonths(1).AddDays(-1)
            iCurr_Date = eZeeDate.convertDate(endDate) '/* LAST DATE OF SELECTED MONTH */
            'S.SANDEEP [20 OCT 2016] -- END



            If cboMonth.SelectedValue = 1 Then
                objEPhysicalReport._PMonthId = 12
                objEPhysicalReport._PMonthName = MonthName(12, False)
                objEPhysicalReport._PYearName = CInt(cboYear.Text) - 1
                'S.SANDEEP [20 OCT 2016] -- START
                'iPrev_Date = eZeeDate.convertDate(DateAdd(DateInterval.Month, 0, eZeeDate.convertDate(CInt(cboYear.Text) - 1 & "12" & "01")).AddDays(-1))
                iPrev_Date = eZeeDate.convertDate(DateAdd(DateInterval.Day, -1, startDate))
                'S.SANDEEP [20 OCT 2016] -- END
            Else
                objEPhysicalReport._PMonthId = CInt(cboMonth.SelectedValue) - 1
                objEPhysicalReport._PMonthName = MonthName(CInt(cboMonth.SelectedValue) - 1, False)
                objEPhysicalReport._PYearName = cboYear.Text
                'S.SANDEEP [20 OCT 2016] -- START
                'iPrev_Date = eZeeDate.convertDate(DateAdd(DateInterval.Month, 0, eZeeDate.convertDate(cboYear.Text & Format(CInt(cboMonth.SelectedValue) - 1, "0#") & "01")).AddDays(-1))
                iPrev_Date = eZeeDate.convertDate(DateAdd(DateInterval.Day, -1, startDate))
                'S.SANDEEP [20 OCT 2016] -- END
            End If

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objEPhysicalReport._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End

            objEPhysicalReport._iCDate = iCurr_Date
            objEPhysicalReport._iPDate = iPrev_Date

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmEmpMonthlyPhysicalReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEPhysicalReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmpMonthlyPhysicalReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpMonthlyPhysicalReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objEPhysicalReport._ReportName
            Me._Message = objEPhysicalReport._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpMonthlyPhysicalReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEPhysicalReport.generateReport(0, e.Type, enExportAction.None)
            objEPhysicalReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEPhysicalReport.generateReport(0, enPrintAction.None, e.Type)
            objEPhysicalReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmpMonthlyPhysicalReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpMonthlyPhysicalReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select month to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
