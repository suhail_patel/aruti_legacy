'************************************************************************************************************************************
'Class Name : frmCurrSalaryGrade_Report.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCurrSalaryGrade_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCurrSalaryGrade_Report"
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private objSalaryGrade As clsCurrentGradeSalaryReport

    'Anjan (17 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Anjan (17 May 2012)-End 


#End Region

#Region " Constructor "

    Public Sub New()
        InitializeComponent()
        objSalaryGrade = New clsCurrentGradeSalaryReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objSalaryGrade.SetDefaultValue()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objGradeGroup As New clsGradeGroup
        Dim dsList As New DataSet
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvGradeGroup.ItemChecked, AddressOf lvGradeGroup_ItemChecked

            dsList = objGradeGroup.GetList("List", True)
            lvGradeGroup.Items.Clear()
            For Each dRow As DataRow In dsList.Tables(0).Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dRow.Item("name").ToString)
                lvItem.Tag = dRow.Item("gradegroupunkid")

                lvGradeGroup.Items.Add(lvItem)

                lvItem = Nothing
            Next

            If lvGradeGroup.Items.Count > 9 Then
                colhGradeGroup.Width = 320 - 20
            Else
                colhGradeGroup.Width = 320
            End If

            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvGradeGroup.ItemChecked, AddressOf lvGradeGroup_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            objchkAll.Checked = False
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            chkIncludeInactiveEmp.Checked = False
            'S.SANDEEP [ 12 MAY 2012 ] -- END


            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Anjan (17 May 2012)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objSalaryGrade.SetDefaultValue()

            Dim StrGradesIds, StrGradeNames As String
            StrGradesIds = String.Empty : StrGradeNames = String.Empty

            'If lvGradeGroup.CheckedItems.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Check atleast one Grade Group to export report."), enMsgBoxStyle.Information)
            '    Return False
            'End If

            For Each lvItem As ListViewItem In lvGradeGroup.CheckedItems
                StrGradesIds &= "," & lvItem.Tag
                StrGradeNames &= "," & lvItem.SubItems(colhGradeGroup.Index).Text
            Next

            If StrGradesIds.Trim.Length > 0 Then StrGradesIds = Mid(StrGradesIds, 2)
            If StrGradeNames.Trim.Length > 0 Then StrGradeNames = Mid(StrGradeNames, 2)

            objSalaryGrade._GradesIds = StrGradesIds
            objSalaryGrade._GradeNames = StrGradeNames

            objSalaryGrade._ViewByIds = mstrStringIds
            objSalaryGrade._ViewIndex = mintViewIdx
            objSalaryGrade._ViewByName = mstrStringName

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objSalaryGrade._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objSalaryGrade._Analysis_Fields = mstrAnalysis_Fields
            objSalaryGrade._Analysis_Join = mstrAnalysis_Join
            objSalaryGrade._Analysis_OrderBy = mstrAnalysis_OrderBy
            objSalaryGrade._Report_GroupName = mstrReport_GroupName
            'Anjan (17 May 2012)-End 


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmCurrSalaryGrade_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSalaryGrade = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCurrSalaryGrade_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCurrSalaryGrade_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objSalaryGrade._ReportName
            Me._Message = objSalaryGrade._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCurrSalaryGrade_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objSalaryGrade.generateReport(0, e.Type, enExportAction.None)
            objSalaryGrade.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objSalaryGrade.generateReport(0, enPrintAction.None, e.Type)
            objSalaryGrade.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCurrentGradeSalaryReport.SetMessages()
            objfrm._Other_ModuleNames = "clsCurrentGradeSalaryReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

#Region " Controls Events "

    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try

            RemoveHandler lvGradeGroup.ItemChecked, AddressOf lvGradeGroup_ItemChecked
            For Each lvItem As ListViewItem In lvGradeGroup.Items
                lvItem.Checked = objchkAll.CheckState
            Next
            AddHandler lvGradeGroup.ItemChecked, AddressOf lvGradeGroup_ItemChecked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvGradeGroup_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvGradeGroup.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvGradeGroup.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvGradeGroup.CheckedItems.Count < lvGradeGroup.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvGradeGroup.CheckedItems.Count = lvGradeGroup.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex


            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Anjan (17 May 2012)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.colhGradeGroup.Text = Language._Object.getCaption(CStr(Me.colhGradeGroup.Tag), Me.colhGradeGroup.Text)
			Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
			Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
