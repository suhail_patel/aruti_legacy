'************************************************************************************************************************************
'Class Name :clsCurrentGradeSalaryReport.vb
'Purpose    :
'Date       :03/04/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsCurrentGradeSalaryReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsCurrentGradeSalaryReport"
    Private mstrReportId As String = enArutiReport.Current_SalaryGradesReport   '74
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mstrGradesIds As String = String.Empty
    Private mstrGradeNames As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIncludeInactiveEmp As Boolean = False
    'S.SANDEEP [ 12 MAY 2012 ] -- END


    'Anjan (17 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Anjan (17 May 2012)-End 

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End

    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrUserAccessFilter As String = String.Empty
    'Pinkal (12-Nov-2012) -- End


    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _GradesIds() As String
        Set(ByVal value As String)
            mstrGradesIds = value
        End Set
    End Property

    Public WriteOnly Property _GradeNames() As String
        Set(ByVal value As String)
            mstrGradeNames = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 12 MAY 2012 ] -- END

    'Anjan (17 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Anjan (17 May 2012)-End 

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End

    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (12-Nov-2012) -- End

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrGradesIds = String.Empty
            mstrGradeNames = String.Empty
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnIncludeInactiveEmp = False
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mstrGradesIds.Trim.Length > 0 Then
                Me._FilterQuery &= " AND hrgradegroup_master.gradegroupunkid IN(" & mstrGradesIds & ")"
            End If

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If
        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()

        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If
            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            'StrQ = "SELECT " & _
            '       "  name AS Grade_Group " & _
            '       " ,COUNT(Cnt) AS Cnt " & _
            '       " ,GName AS GName " & _
            '       "FROM hrgradegroup_master " & _
            '       "JOIN " & _
            '       "( " & _
            '       "  SELECT "
            ''Select Case mintViewIndex
            ''    Case enAnalysisReport.Branch
            ''        StrQ &= "  hrstation_master.name AS GName "
            ''    Case enAnalysisReport.Department
            ''        StrQ &= "  hrdepartment_master.name AS GName "
            ''    Case enAnalysisReport.Section
            ''        StrQ &= "  hrsection_master.name AS GName "
            ''    Case enAnalysisReport.Unit
            ''        StrQ &= "  hrunit_master.name AS GName "
            ''    Case enAnalysisReport.Job
            ''        StrQ &= " hrjob_master.job_name AS GName "
            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " prcostcenter_master.costcentername AS GName "
            ''    Case enAnalysisReport.SectionGroup
            ''        StrQ &= " hrsectiongroup_master.name AS GName "
            ''    Case enAnalysisReport.UnitGroup
            ''        StrQ &= " hrunitgroup_master.name AS GName "
            ''    Case enAnalysisReport.Team
            ''        StrQ &= " hrteam_master.name AS GName "
            ''    Case Else
            ''        StrQ &= " '' AS GName "
            ''End Select
            'StrQ &= " gradegroupunkid AS GId " & _
            '        " ,employeeunkid AS Cnt "
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "FROM hremployee_master "


            ''Anjan (17 May 2012)-Start
            ''ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

            ''Select Case mintViewIndex
            ''    Case enAnalysisReport.Branch
            ''        StrQ &= " JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid  "
            ''    Case enAnalysisReport.Department
            ''        StrQ &= " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid "
            ''    Case enAnalysisReport.Section
            ''        StrQ &= " JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid "
            ''    Case enAnalysisReport.Unit
            ''        StrQ &= " JOIN hrunit_master ON hremployee_master.unitunkid = hrunit_master.unitunkid "
            ''    Case enAnalysisReport.Job
            ''        StrQ &= " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid "
            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid "
            ''    Case enAnalysisReport.SectionGroup
            ''        StrQ &= " JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid "
            ''    Case enAnalysisReport.UnitGroup
            ''        StrQ &= " JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid "
            ''    Case enAnalysisReport.Team
            ''        StrQ &= " JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid "
            ''End Select

            'StrQ &= mstrAnalysis_Join

            ''Anjan (17 May 2012)-End 

            'StrQ &= " WHERE 1 = 1 "

            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ISSUE : TRA ENHANCEMENTS
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            'End If
            ''S.SANDEEP [ 12 MAY 2012 ] -- END

            ''Select Case mintViewIndex
            ''    Case enAnalysisReport.Branch
            ''        StrQ &= " AND hrstation_master.stationunkid in (" & mstrViewByIds & ")"
            ''    Case enAnalysisReport.Department
            ''        StrQ &= " AND hrdepartment_master.departmentunkid in (" & mstrViewByIds & ")"
            ''    Case enAnalysisReport.Section
            ''        StrQ &= "AND hrsection_master.sectionunkid in (" & mstrViewByIds & ")"
            ''    Case enAnalysisReport.Unit
            ''        StrQ &= " AND hrunit_master.unitunkid in (" & mstrViewByIds & ")"
            ''    Case enAnalysisReport.Job
            ''        StrQ &= " AND hrjob_master.jobunkid in (" & mstrViewByIds & ")"
            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " AND prcostcenter_master.costcenterunkid in (" & mstrViewByIds & ")"
            ''    Case enAnalysisReport.SectionGroup
            ''        StrQ &= " AND hrsectiongroup_master.sectiongroupunkid in (" & mstrViewByIds & ")"
            ''    Case enAnalysisReport.UnitGroup
            ''        StrQ &= " AND hrunitgroup_master.unitgroupunkid in (" & mstrViewByIds & ")"
            ''    Case enAnalysisReport.Team
            ''        StrQ &= " AND hrteam_master.teamunkid in (" & mstrViewByIds & ")"
            ''End Select

            ''Pinkal (12-Nov-2012) -- Start
            ''Enhancement : TRA Changes

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            ''Pinkal (12-Nov-2012) -- End

            StrQ = "SELECT " & _
                   "     name AS Grade_Group " & _
                   "    ,COUNT(Cnt) AS Cnt " & _
                   "    ,GName AS GName " & _
                   "FROM hrgradegroup_master " & _
                   "JOIN " & _
                   "(" & _
                   "    SELECT " & _
                   "	     GG.gradegroupunkid AS GId " & _
                   "	    ,hremployee_master.employeeunkid AS Cnt "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            StrQ &= "    FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END            
            StrQ &= "    JOIN " & _
                    "    ( " & _
                    "	    SELECT " & _
                    "		     employeeunkid " & _
                    "		    ,gradegroupunkid " & _
                    "		    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                    "	    FROM prsalaryincrement_tran " & _
                    "	    WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "    ) AS GG ON GG.employeeunkid = hremployee_master.employeeunkid AND GG.rno = 1 "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= mstrAnalysis_Join


            StrQ &= " WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= " ) AS Counter ON Counter.GId = hrgradegroup_master.gradegroupunkid "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= " GROUP BY name,GName "

            If mstrGradesIds.Trim.Length > 0 Then
                StrQ &= " ,gradegroupunkid "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("Grade_Group")
                rpt_Rows.Item("Column2") = dtRow.Item("Cnt")
                rpt_Rows.Item("Column3") = dtRow.Item("GName")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptCurrentSal_Distribution

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSalGrade", Language.getMessage(mstrModuleName, 5, "Salary Grade"))
            Call ReportFunction.TextChange(objRpt, "txtTotalEmp", Language.getMessage(mstrModuleName, 6, "No. Of Employee"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 7, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 8, "Grand Total :"))


            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 17, "Branch :"))
            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 18, "Department :"))
            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 19, "Section :"))
            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 20, "Unit :"))
            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 21, "Job :"))
            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 22, "Cost Center :"))
            '    Case enAnalysisReport.SectionGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 23, "Section Group :"))
            '    Case enAnalysisReport.UnitGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 24, "Unit Group :"))
            '    Case enAnalysisReport.Team
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 25, "Team :"))
            '    Case Else
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", "")
            'End Select
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'Anjan (17 May 2012)-End 



            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By :")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Salary Grade")
            Language.setMessage(mstrModuleName, 6, "No. Of Employee")
            Language.setMessage(mstrModuleName, 7, "Sub Total :")
            Language.setMessage(mstrModuleName, 8, "Grand Total :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
