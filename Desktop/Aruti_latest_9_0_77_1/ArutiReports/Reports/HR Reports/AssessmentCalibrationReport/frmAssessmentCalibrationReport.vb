Option Strict On
'************************************************************************************************************************************
'Class Name : frmAssessmentCalibrationReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessmentCalibrationReport
    Private ReadOnly mstrModuleName As String = "frmAssessmentCalibrationReport"
    Private objCalibrationReport As clsAssessmentCalibrationReport

#Region " Private Variables "

    Private mstrAdvanceFilter As String = String.Empty
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Contructor "

    Public Sub New()
        objCalibrationReport = New clsAssessmentCalibrationReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objCalibrationReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objScApprl As New clsScoreCalibrationApproval
        Dim dsCombos As New DataSet
        Try

            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Not Calibrated Employee"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Calibrated Employee"))
                .Items.Add(Language.getMessage(mstrModuleName, 9, "Calibration Status"))
                'S.SANDEEP |01-MAY-2020| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
                .Items.Add(Language.getMessage(mstrModuleName, 10, "Approver Wise Calibration Status"))
                'S.SANDEEP |01-MAY-2020| -- END
                .SelectedIndex = 0
            End With

            dsCombos = objScApprl.GetStatusList("List", True, Nothing)
            With cboCalibrationStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
            End With            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose() : ObjEmp = Nothing : ObjPeriod = Nothing
        End Try
    End Sub

    Private Sub Fill_Column_List()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objMData.GetEAllocation_Notification("List")
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 900
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 3, "Grade Group")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 901
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Grade")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 902
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 5, "Grade Level")
                dsList.Tables(0).Rows.Add(dRow)

                'dRow = dsList.Tables(0).NewRow
                'dRow.Item("Id") = 903
                'dRow.Item("Name") = Language.getMessage(mstrModuleName, 6, "Cost Center")
                'dsList.Tables(0).Rows.Add(dRow)

                Dim lvItem As ListViewItem
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("Name").ToString
                    Select Case CInt(dtRow.Item("Id"))
                        Case enAllocation.BRANCH
                            lvItem.SubItems.Add(",ISNULL(esm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrstation_master AS esm ON T.stationunkid = esm.stationunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.DEPARTMENT_GROUP
                            lvItem.SubItems.Add(",ISNULL(edg.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrdepartment_group_master AS edg ON T.deptgroupunkid = edg.deptgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.DEPARTMENT
                            lvItem.SubItems.Add(",ISNULL(edp.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("JOIN hrdepartment_master AS edp ON T.departmentunkid = edp.departmentunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.SECTION_GROUP
                            lvItem.SubItems.Add(",ISNULL(esg.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrsectiongroup_master AS esg ON T.sectiongroupunkid = esg.sectiongroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.SECTION
                            lvItem.SubItems.Add(",ISNULL(esc.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrsection_master AS esc ON T.sectionunkid = esc.sectionunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.UNIT_GROUP
                            lvItem.SubItems.Add(",ISNULL(eug.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrunitgroup_master AS eug ON T.unitgroupunkid = eug.unitgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.UNIT
                            lvItem.SubItems.Add(",ISNULL(eum.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrunit_master AS eum ON T.unitunkid = eum.unitunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.TEAM
                            lvItem.SubItems.Add(",ISNULL(etm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrteam_master AS etm ON T.teamunkid = etm.teamunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.JOB_GROUP
                            lvItem.SubItems.Add(",ISNULL(ejg.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrjobgroup_master AS ejg ON J.jobgroupunkid = ejg.jobgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.JOBS
                            lvItem.SubItems.Add(",ISNULL(ejb.job_name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("JOIN hrjob_master AS ejb ON J.jobunkid = ejb.jobunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.CLASS_GROUP
                            lvItem.SubItems.Add(",ISNULL(ecg.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrclassgroup_master AS ecg ON T.classgroupunkid = ecg.classgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.CLASSES
                            lvItem.SubItems.Add(",ISNULL(ecl.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrclasses_master AS ecl ON T.classunkid = ecl.classesunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case 900
                            lvItem.SubItems.Add(",ISNULL(egg.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("JOIN hrgradegroup_master AS egg ON G.gradegroupunkid = egg.gradegroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case 901
                            lvItem.SubItems.Add(",ISNULL(egd.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("JOIN hrgrade_master AS egd ON G.gradeunkid = egd.gradeunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case 902
                            lvItem.SubItems.Add(",ISNULL(egl.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("JOIN hrgradelevel_master AS egl ON G.gradelevelunkid = egl.gradelevelunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.COST_CENTER
                            lvItem.SubItems.Add(",ISNULL(ecc.costcentername,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN prcostcenter_master AS ecc ON C.costcenterunkid = ecc.costcenterunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                    End Select

                    lvDisplayCol.Items.Add(lvItem)
                Next

                If lvDisplayCol.Items.Count > 12 Then
                    colhDisplayName.Width = 245 - 18
                Else
                    colhDisplayName.Width = 245
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            objMData = Nothing : dsList = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboPeriod.SelectedValue = 0
            cboCalibrationStatus.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            objchkdisplay.Checked = False
            Dim dgvRows As IEnumerable(Of DataGridViewRow) = dgvRating.Rows.Cast(Of DataGridViewRow)().AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhCheck.Index).Value) = True)
            If dgvRows IsNot Nothing AndAlso dgvRows.Count > 0 Then
                For Each grow In dgvRows
                    grow.Cells(objdgcolhCheck.Index).Value = False
                Next
            End If
            dgvRating.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objCalibrationReport.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Period is mandatory information. Please select period to continue."), enMsgBoxStyle.Information)
                Return False
            End If
            If lvDisplayCol.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Please check atleast one column to display in report."), enMsgBoxStyle.Information)
                Return False
            End If
            objCalibrationReport._ApplyUserAccessFilter = True
            objCalibrationReport._CalibrationStatusId = CInt(cboCalibrationStatus.SelectedValue)
            objCalibrationReport._CalibrationStatusName = cboCalibrationStatus.Text
            objCalibrationReport._EmployeeId = CInt(cboEmployee.SelectedValue)
            objCalibrationReport._EmplyeeName = cboEmployee.Text
            objCalibrationReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objCalibrationReport._PeriodName = cboPeriod.Text
            objCalibrationReport._ReportViewTypeId = cboReportType.SelectedIndex
            objCalibrationReport._ReportViewName = cboReportType.Text
            objCalibrationReport._ViewByIds = mstrStringIds
            objCalibrationReport._ViewIndex = mintViewIdx
            objCalibrationReport._ViewByName = mstrStringName
            objCalibrationReport._Analysis_Fields = mstrAnalysis_Fields
            objCalibrationReport._Analysis_Join = mstrAnalysis_Join
            objCalibrationReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCalibrationReport._Report_GroupName = mstrReport_GroupName
            objCalibrationReport._Advance_Filter = mstrAdvanceFilter

            Dim iCols, iJoins, iDisplay As String
            iCols = String.Empty : iJoins = String.Empty : iDisplay = String.Empty
            For Each lCheck As ListViewItem In lvDisplayCol.CheckedItems
                iCols &= lCheck.SubItems(objcolhSelectCol.Index).Text & vbCrLf
                iJoins &= lCheck.SubItems(objcolhJoin.Index).Text & vbCrLf
                iDisplay &= lCheck.SubItems(objcolhDisplay.Index).Text & vbCrLf
            Next
            objCalibrationReport._SelectedCols = iCols
            objCalibrationReport._SelectedJoin = iJoins
            objCalibrationReport._DisplayCols = iDisplay

            Select Case CInt(cboReportType.SelectedIndex)
                Case 0, 1
                    If dgvRating.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhCheck.Index).Value) = True).Count > 0 Then
                        objCalibrationReport._mDictRatingFrom = dgvRating.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhCheck.Index).Value) = True).ToDictionary(Function(x) CInt(x.Cells(objdgcolhRatingId.Index).Value), Function(y) CDec(y.Cells(objdgcolhScF.Index).Value))
                        objCalibrationReport._mDictRatingTo = dgvRating.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhCheck.Index).Value) = True).ToDictionary(Function(x) CInt(x.Cells(objdgcolhRatingId.Index).Value), Function(y) CDec(y.Cells(objdgcolhScT.Index).Value))
                    End If
                Case 2, 3
                    If dgvRating.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhCheck.Index).Value) = True).Count > 0 Then
                        objCalibrationReport._CalibrationUnkids = String.Join(",", dgvRating.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhCheck.Index).Value) = True).Select(Function(x) x.Cells(objdgcolhCalibrationId.Index).Value.ToString()).ToArray())
                    End If
            End Select

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub FillGrid(ByVal intReportType As Integer)
        Try
            Dim dsCombos As New DataSet
            dgvRating.DataSource = Nothing
            Select Case intReportType
                Case 0, 1
                    Dim objRating As New clsAppraisal_Rating
                    dsCombos = objRating.getComboList("List", False, FinancialYear._Object._DatabaseName)
                    Dim icol As New DataColumn
                    With icol
                        .ColumnName = "icheck"
                        .DataType = GetType(System.Boolean)
                        .DefaultValue = False
                    End With
                    dsCombos.Tables(0).Columns.Add(icol)

                    dgvRating.AutoGenerateColumns = False
                    objdgcolhCheck.DataPropertyName = "icheck"
                    dgcolhRating.DataPropertyName = "name"
                    objdgcolhScF.DataPropertyName = "scrf"
                    objdgcolhScT.DataPropertyName = "scrt"
                    objdgcolhRatingId.DataPropertyName = "id"
                    dgvRating.DataSource = dsCombos.Tables(0).Copy
                    objRating = Nothing
                Case 2, 3
                    Dim objCSMaster As New clsComputeScore_master
                    dsCombos = objCSMaster.GetCalibrationNoList(CInt(cboPeriod.SelectedValue), "List", Nothing)
                    dgvRating.AutoGenerateColumns = False
                    objdgcolhCheck.DataPropertyName = "icheck"
                    dgcolhCalibrationNo.DataPropertyName = "calibration_no"
                    objdgcolhCalibrationId.DataPropertyName = "calibratnounkid"
                    dgvRating.DataSource = dsCombos.Tables(0).Copy
                    objCSMaster = Nothing
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Forms "

    Private Sub frmAssessmentCalibrationReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCalibrationReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentCalibrationReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentCalibrationReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Me.EZeeHeader1.Title = objCalibrationReport._ReportName
            Me.EZeeHeader1.Message = objCalibrationReport._ReportDesc

            Call FillCombo()
            Call Fill_Column_List()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentCalibrationReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Select Case CInt(cboReportType.SelectedIndex)
                Case 0, 1
                    objCalibrationReport.Export_Calibratrion_Report(FinancialYear._Object._DatabaseName, _
                                                                    User._Object._Userunkid, _
                                                                    FinancialYear._Object._YearUnkid, _
                                                                    Company._Object._Companyunkid, _
                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                    ConfigParameter._Object._ExportReportPath, _
                                                                    ConfigParameter._Object._OpenAfterExport)
                Case 2
                    objCalibrationReport.Export_CalibrationStatus_Report(FinancialYear._Object._DatabaseName, _
                                                                         Company._Object._Companyunkid, _
                                                                         FinancialYear._Object._YearUnkid, _
                                                                         ConfigParameter._Object._UserAccessModeSetting, _
                                                                         enUserPriviledge.AllowToApproveRejectCalibratedScore, _
                                                                         ConfigParameter._Object._EmployeeAsOnDate, _
                                                                         User._Object._Userunkid, _
                                                                         False, _
                                                                         ConfigParameter._Object._ExportReportPath, _
                                                                         ConfigParameter._Object._OpenAfterExport)
                    'S.SANDEEP |01-MAY-2020| -- START
                    'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
                Case 3
                    objCalibrationReport.GetNextEmployeeApproversNew(CInt(cboPeriod.SelectedValue), _
                                                                     FinancialYear._Object._DatabaseName, _
                                                                     Company._Object._Companyunkid, _
                                                                     FinancialYear._Object._YearUnkid, _
                                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                                     ConfigParameter._Object._EmployeeAsOnDate, _
                                                                     User._Object._Userunkid, _
                                                                     ConfigParameter._Object._ExportReportPath, _
                                                                     ConfigParameter._Object._OpenAfterExport)
                    'S.SANDEEP |01-MAY-2020| -- END
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessmentCalibrationReport.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessmentCalibrationReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            Select Case cboReportType.SelectedIndex
                Case 0, 1
                    frm._Hr_EmployeeTable_Alias = "hremployee_master"
                Case 2, 3
                    frm._Hr_EmployeeTable_Alias = "AEM"
            End Select
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case cboReportType.SelectedIndex
                Case 0
                    lblCalibrationRating.Visible = False : lblProvisionalRating.Visible = True : lblCalibrationNo.Visible = False
                    dgcolhCalibrationNo.Visible = False : dgcolhRating.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill : dgcolhRating.Visible = True

                    lnkSetAnalysis.Visible = True

                Case 1
                    lblCalibrationRating.Visible = True : lblProvisionalRating.Visible = False : lblCalibrationNo.Visible = False
                    dgcolhCalibrationNo.Visible = False : dgcolhRating.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill : dgcolhRating.Visible = True

                    lnkSetAnalysis.Visible = True

                Case 2, 3
                    lblCalibrationNo.Visible = True : lblCalibrationRating.Visible = False : lblProvisionalRating.Visible = False
                    dgcolhCalibrationNo.Visible = True : dgcolhCalibrationNo.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill : dgcolhRating.Visible = False

                    lnkSetAnalysis.Visible = False
                    mstrStringIds = "" : mstrStringName = "" : mintViewIdx = 0 : mstrAnalysis_Fields = "" : mstrAnalysis_Join = "" : mstrAnalysis_OrderBy = "" : mstrReport_GroupName = ""
            End Select
            FillGrid(cboReportType.SelectedIndex)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkdisplay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkdisplay.CheckedChanged
        Try
            For Each lvitem As ListViewItem In lvDisplayCol.Items
                lvitem.Checked = objchkdisplay.Checked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkdisplay_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            Dim gvRows As IEnumerable(Of DataGridViewRow) = Nothing
            gvRows = dgvRating.Rows.Cast(Of DataGridViewRow).AsEnumerable().Select(Function(x) x)
            If gvRows IsNot Nothing AndAlso gvRows.Count > 0 Then
                For Each gvR In gvRows
                    gvR.Cells(objdgcolhCheck.Index).Value = objchkAll.Checked
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblReportPlanning.Text = Language._Object.getCaption(Me.lblReportPlanning.Name, Me.lblReportPlanning.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblCalibrationRating.Text = Language._Object.getCaption(Me.lblCalibrationRating.Name, Me.lblCalibrationRating.Text)
            Me.lblCalibrationStaus.Text = Language._Object.getCaption(Me.lblCalibrationStaus.Name, Me.lblCalibrationStaus.Text)
            Me.dgcolhRating.HeaderText = Language._Object.getCaption(Me.dgcolhRating.Name, Me.dgcolhRating.HeaderText)
            Me.lblProvisionalRating.Text = Language._Object.getCaption(Me.lblProvisionalRating.Name, Me.lblProvisionalRating.Text)
            Me.colhDisplayName.Text = Language._Object.getCaption(CStr(Me.colhDisplayName.Tag), Me.colhDisplayName.Text)
            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.btnAdvanceFilter.Text = Language._Object.getCaption(Me.btnAdvanceFilter.Name, Me.btnAdvanceFilter.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Not Calibrated Employee")
            Language.setMessage(mstrModuleName, 2, "Calibrated Employee")
            Language.setMessage(mstrModuleName, 3, "Grade Group")
            Language.setMessage(mstrModuleName, 4, "Grade")
            Language.setMessage(mstrModuleName, 5, "Grade Level")
            Language.setMessage(mstrModuleName, 6, "Sorry, Period is mandatory information. Please select period to continue.")
            Language.setMessage(mstrModuleName, 7, "Sorry, Please check atleast one column to display in report.")
            Language.setMessage(mstrModuleName, 9, "Calibration Status")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
