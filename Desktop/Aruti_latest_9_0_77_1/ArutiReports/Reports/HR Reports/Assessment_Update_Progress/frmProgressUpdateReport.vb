﻿'************************************************************************************************************************************
'Class Name : frmProgressUpdateReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmProgressUpdateReport

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmProgressUpdateReport"
    Private objProgress As clsProgressUpdateReport
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Contructor "

    Public Sub New()
        objProgress = New clsProgressUpdateReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objProgress.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objStatus As New clsProgressUpdateReport(0, 0)
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = objStatus.GetProgressStatus("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboStatus.SelectedValue = 0
            objchkdisplay.Checked = False
            Call objchkdisplay_CheckedChanged(New Object, New EventArgs)
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Column_List()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objMData.GetEAllocation_Notification("List")
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 900
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 8, "Grade Group")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 901
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 9, "Grade")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 902
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 5, "Grade Level")
                dsList.Tables(0).Rows.Add(dRow)

                Dim lvItem As ListViewItem
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    lvItem = New ListViewItem

                    If CInt(dtRow.Item("Id")) = enAllocation.JOBS Then Continue For

                    lvItem.Text = dtRow.Item("Name").ToString
                    Select Case CInt(dtRow.Item("Id"))
                        Case enAllocation.BRANCH
                            lvItem.SubItems.Add(",ISNULL(estm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrstation_master AS estm ON estm.stationunkid = Alloc.stationunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.DEPARTMENT_GROUP
                            lvItem.SubItems.Add(",ISNULL(edgm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrdepartment_group_master AS edgm ON edgm.deptgroupunkid = Alloc.deptgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.DEPARTMENT
                            lvItem.SubItems.Add(",ISNULL(edpt.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("JOIN hrdepartment_master AS edpt ON edpt.departmentunkid = Alloc.departmentunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.SECTION_GROUP
                            lvItem.SubItems.Add(",ISNULL(esgm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrsectiongroup_master AS esgm ON esgm.sectiongroupunkid = Alloc.sectiongroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.SECTION
                            lvItem.SubItems.Add(",ISNULL(esec.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrsection_master AS esec ON esec.sectionunkid = Alloc.sectionunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.UNIT_GROUP
                            lvItem.SubItems.Add(",ISNULL(eugm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrunitgroup_master AS eugm ON eugm.unitgroupunkid = Alloc.unitgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.UNIT
                            lvItem.SubItems.Add(",ISNULL(eutm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrunit_master AS eutm ON eutm.unitunkid = Alloc.unitunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.TEAM
                            lvItem.SubItems.Add(",ISNULL(etem.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrteam_master AS etem ON etem.teamunkid = Alloc.teamunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.JOB_GROUP
                            lvItem.SubItems.Add(",ISNULL(ejgm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrjobgroup_master AS ejgm ON ejgm.jobgroupunkid = Jobs.jobgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.JOBS
                            lvItem.SubItems.Add(",ISNULL(ejbm.job_name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("JOIN hrjob_master AS ejbm ON ejbm.jobunkid = Jobs.jobunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.CLASS_GROUP
                            lvItem.SubItems.Add(",ISNULL(ecgm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrclassgroup_master AS ecgm ON ecgm.classgroupunkid = Alloc.classgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.CLASSES
                            lvItem.SubItems.Add(",ISNULL(ecls.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrclasses_master AS ecls ON ecls.classesunkid = Alloc.classunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case 900
                            lvItem.SubItems.Add(",ISNULL(eggm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("JOIN hrgradegroup_master eggm ON grds.gradegroupunkid = eggm.gradegroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case 901
                            lvItem.SubItems.Add(",ISNULL(egdm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrgrade_master egdm ON grds.gradeunkid = egdm.gradeunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case 902
                            lvItem.SubItems.Add(",ISNULL(egdl.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrgradelevel_master egdl ON grds.gradelevelunkid = egdl.gradelevelunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.COST_CENTER
                            lvItem.SubItems.Add(",ISNULL(ecct.costcentername,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN prcostcenter_master AS ecct ON ecct.costcenterunkid = CC.costcenterunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                    End Select

                    lvDisplayCol.Items.Add(lvItem)
                Next

                If lvDisplayCol.Items.Count > 12 Then
                    colhDisplayName.Width = 210 - 18
                Else
                    colhDisplayName.Width = 210
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Column_List", mstrModuleName)
        Finally
            objMData = Nothing : dsList = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objProgress.SetDefaultValue()
            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If lvDisplayCol.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select atleast one column to display in report."), enMsgBoxStyle.Information)
                lvDisplayCol.Focus()
                Return False
            End If

            objProgress._PeriodId = cboPeriod.SelectedValue
            objProgress._PeriodName = cboPeriod.Text
            Dim iCols, iJoins, iDisplay As String
            iCols = String.Empty : iJoins = String.Empty : iDisplay = String.Empty
            For Each lCheck As ListViewItem In lvDisplayCol.CheckedItems
                iCols &= lCheck.SubItems(objcolhSelectCol.Index).Text & vbCrLf
                iJoins &= lCheck.SubItems(objcolhJoin.Index).Text & vbCrLf
                iDisplay &= lCheck.SubItems(objcolhDisplay.Index).Text & vbCrLf
            Next
            objProgress._SelectedCols = iCols
            objProgress._SelectedJoin = iJoins
            objProgress._DisplayCols = iDisplay
            objProgress._AdvanceFilter = mstrAdvanceFilter
            objProgress._EmployeeId = cboEmployee.SelectedValue
            objProgress._EmployeeName = cboEmployee.Text
            objProgress._ViewByIds = mstrStringIds
            objProgress._ViewIndex = mintViewIdx
            objProgress._ViewByName = mstrStringName
            objProgress._Analysis_Fields = mstrAnalysis_Fields
            objProgress._Analysis_Join = mstrAnalysis_Join
            objProgress._Analysis_OrderBy = mstrAnalysis_OrderBy
            objProgress._Report_GroupName = mstrReport_GroupName
            objProgress._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            objProgress._StatusId = CInt(cboStatus.SelectedValue)
            objProgress._StatusName = cboStatus.Text

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmProgressUpdateReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objProgress = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmProgressUpdateReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProgressUpdateReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me.EZeeHeader1.Title = objProgress._ReportName
            Me.EZeeHeader1.Message = objProgress._ReportDesc

            Call FillCombo()
            Call Fill_Column_List()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProgressUpdateReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objProgress.ExportProgressUpdate(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsProgressUpdateReport.SetMessages()
            objfrm._Other_ModuleNames = "clsProgressUpdateReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkdisplay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkdisplay.CheckedChanged
        Try
            For Each lvitem As ListViewItem In lvDisplayCol.Items
                lvitem.Checked = objchkdisplay.Checked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkdisplay_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.btnAdvanceFilter.Text = Language._Object.getCaption(Me.btnAdvanceFilter.Name, Me.btnAdvanceFilter.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.colhDisplayName.Text = Language._Object.getCaption(CStr(Me.colhDisplayName.Tag), Me.colhDisplayName.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 2, "Please select atleast one column to display in report.")
            Language.setMessage(mstrModuleName, 5, "Grade Level")
            Language.setMessage(mstrModuleName, 8, "Grade Group")
            Language.setMessage(mstrModuleName, 9, "Grade")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class