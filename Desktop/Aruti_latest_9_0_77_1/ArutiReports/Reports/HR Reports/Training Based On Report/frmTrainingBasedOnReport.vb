'************************************************************************************************************************************
'Class Name : frmTrainingBasedOnReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTrainingBasedOnReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTrainingBasedOnReport"
    Private objTrainingBasedOn As clsTraningBasedOnReport

#End Region

#Region " Contructor "

    Public Sub New()
        objTrainingBasedOn = New clsTraningBasedOnReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objTrainingBasedOn.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjTrain_Sched As New clsTraining_Scheduling
        Dim objMaster As New clsMasterData
        Dim ObjInstitute As New clsinstitute_master
        Dim dsCombo As New DataSet
        Try
            dsCombo = ObjTrain_Sched.getComboList("Training", True, , True)
            With cboTraining
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Training")
                .SelectedValue = 0
            End With

            dsCombo = objMaster.GetTraining_Status("Status", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsCombo.Tables("Status")
                .SelectedValue = 0
            End With

            dsCombo = ObjInstitute.getListForCombo(False, "Institute", True)
            With cboInstitute
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Institute")
                .SelectedValue = 0
            End With

            Dim objMData As New clsMasterData
            dsCombo = objMData.GetEAllocation_Notification("List")
            Dim row As DataRow = dsCombo.Tables("List").NewRow
            With row
                .Item("Id") = 0
                .Item("Name") = Language.getMessage(mstrModuleName, 100, "Select")
            End With

            dsCombo.Tables("List").Rows.InsertAt(row, 0)
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 101, "Duration Wise"))           '0
                .Items.Add(Language.getMessage(mstrModuleName, 102, "Venue Wise"))              '1
                .Items.Add(Language.getMessage(mstrModuleName, 103, "Provider Wise"))           '2
                .Items.Add(Language.getMessage(mstrModuleName, 104, "Sponser Wise"))            '3
                .Items.Add(Language.getMessage(mstrModuleName, 105, "Binded Allocation Wise"))  '4
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case CInt(cboAllocations.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJGrp As New clsJobGroup
                    dList = objJGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.COST_CENTER
                    Dim objCC As New clscostcenter_master
                    dList = objCC.GetList("List")
                    Call Fill_List(dList.Tables(0), "costcenterunkid", "costcentername")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            lvAllocation.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)

                lvAllocation.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count > 8 Then
                colhAllocations.Width = 270 - 20
            Else
                colhAllocations.Width = 270
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Do_Operation(ByVal blnOpt As Boolean, ByVal lv As ListView)
        Try
            For Each LItem As ListViewItem In lv.Items
                LItem.Checked = blnOpt
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        End Try
    End Sub

    Private Sub Sources_List()
        Dim dsFunding As New DataSet
        Dim objCMaster As New clsCommon_Master
        Dim lvItem As ListViewItem
        Try
            dsFunding = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SOURCES_FUNDINGS, False, "List")
            lvSourceFunding.Items.Clear()
            For Each dRow As DataRow In dsFunding.Tables("List").Rows
                lvItem = New ListViewItem

                lvItem.Text = dRow.Item("name").ToString
                lvItem.Tag = dRow.Item("masterunkid")

                lvSourceFunding.Items.Add(lvItem)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Sources_List", mstrModuleName)
        Finally
            dsFunding.Dispose() : objCMaster = Nothing : lvItem = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboTraining.SelectedValue = 0
            dtpFromDate.Checked = False
            dtpToDate.Checked = False
            cboInstitute.SelectedValue = 0
            cboAllocations.SelectedValue = 0
            Call Sources_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objTrainingBasedOn.SetDefaultValue()

            objTrainingBasedOn._ReportTypeId = cboReportType.SelectedIndex
            objTrainingBasedOn._ReportTypeName = cboReportType.Text
            objTrainingBasedOn._TrainingScheduleId = cboTraining.SelectedValue
            objTrainingBasedOn._TrainingScheduleName = cboTraining.Text
            If dtpFromDate.Checked = True AndAlso dtpToDate.Checked = True Then
                objTrainingBasedOn._StartDate = dtpFromDate.Value
                objTrainingBasedOn._EndDate = dtpToDate.Value
            End If
            objTrainingBasedOn._ProviderId = cboInstitute.SelectedValue
            objTrainingBasedOn._ProviderName = cboInstitute.Text
            If lvSourceFunding.CheckedItems.Count > 0 Then
                objTrainingBasedOn._SponsersIds = String.Join(",", lvSourceFunding.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToArray)
                objTrainingBasedOn._SponsersName = String.Join(",", lvSourceFunding.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.SubItems(colhFundingSource.Index).Text).ToArray)
            End If
            objTrainingBasedOn._AllocationTypeId = cboAllocations.SelectedValue
            objTrainingBasedOn._AllocationTypeName = cboAllocations.Text
            If lvAllocation.CheckedItems.Count > 0 Then
                objTrainingBasedOn._AllocationIds = String.Join(",", lvAllocation.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToArray)
                objTrainingBasedOn._AllocationNames = String.Join(",", lvAllocation.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.SubItems(colhAllocations.Index).Text).ToArray)
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Private Sub frmTrainingBasedOnReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTrainingBasedOn = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmTrainingBasedOnReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingBasedOnReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            eZeeHeader.Title = objTrainingBasedOn._ReportName
            eZeeHeader.Message = objTrainingBasedOn._ReportDesc

            Call FillCombo()
            Call Sources_List()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingBasedOnReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then

                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearchTraining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTraining.Click
        Dim frm As New frmCommonSearch
        Try
            If CType(cboTraining.DataSource, DataTable) Is Nothing Then Exit Sub

            frm.DataSource = CType(cboTraining.DataSource, DataTable)
            frm.ValueMember = cboTraining.ValueMember
            frm.DisplayMember = cboTraining.DisplayMember
            If frm.DisplayDialog Then
                cboTraining.SelectedValue = frm.SelectedValue
                cboTraining.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTraining_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchProvider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchProvider.Click
        Dim frm As New frmCommonSearch
        Try
            If CType(cboInstitute.DataSource, DataTable) Is Nothing Then Exit Sub
            frm.DataSource = CType(cboInstitute.DataSource, DataTable)
            frm.ValueMember = cboInstitute.ValueMember
            frm.DisplayMember = cboInstitute.DisplayMember
            If frm.DisplayDialog Then
                cboInstitute.SelectedValue = frm.SelectedValue
                cboInstitute.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchProvider_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraningBasedOnReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTraningBasedOnReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objTrainingBasedOn.Export_Training_BasedOn_Report(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._OpenAfterExport, _
                                                              ConfigParameter._Object._ExportReportPath)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            colhAllocations.Text = cboAllocations.Text
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Listview Events "

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvSourceFunding_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvSourceFunding.ItemChecked
        Try
            RemoveHandler objchkSAll.CheckedChanged, AddressOf objchkSAll_CheckedChanged
            If lvSourceFunding.CheckedItems.Count <= 0 Then
                objchkSAll.CheckState = CheckState.Unchecked
            ElseIf lvSourceFunding.CheckedItems.Count < lvSourceFunding.Items.Count Then
                objchkSAll.CheckState = CheckState.Indeterminate
            ElseIf lvSourceFunding.CheckedItems.Count = lvSourceFunding.Items.Count Then
                objchkSAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkSAll.CheckedChanged, AddressOf objchkSAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvSourceFunding_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Checkbox Events "

    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            Call Do_Operation(CBool(objchkAll.CheckState), lvAllocation)
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkSAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkSAll.CheckedChanged
        Try
            RemoveHandler lvSourceFunding.ItemChecked, AddressOf lvSourceFunding_ItemChecked
            Call Do_Operation(CBool(objchkSAll.CheckState), lvSourceFunding)
            AddHandler lvSourceFunding.ItemChecked, AddressOf lvSourceFunding_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            lvAllocation.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocation.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.colhAllocations.Text = Language._Object.getCaption(CStr(Me.colhAllocations.Tag), Me.colhAllocations.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblTraining.Text = Language._Object.getCaption(Me.lblTraining.Name, Me.lblTraining.Text)
            Me.lblInstitute.Text = Language._Object.getCaption(Me.lblInstitute.Name, Me.lblInstitute.Text)
            Me.colhFundingSource.Text = Language._Object.getCaption(CStr(Me.colhFundingSource.Tag), Me.colhFundingSource.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblReporttype.Text = Language._Object.getCaption(Me.lblReporttype.Name, Me.lblReporttype.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
