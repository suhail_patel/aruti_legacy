'************************************************************************************************************************************
'Class Name : clsStaffMovementReport.vb
'Purpose    :
'Date       : 27/09/2014
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsStaffMovementReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsStaffMovementReport"
    Private mstrReportId As String = enArutiReport.Staff_Movement_Report  '158
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintPeriodId As Integer = 0
    Private mstrPeriod As String = ""
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintJobId As Integer = 0
    Private mstrJob As String = ""
    Private mintLocationId As Integer = 0
    Private mstrLocation As String = ""
    Private mintSpouseId As Integer = 0
    Private mstrSpouse As String = ""
    Private mintChildId As Integer = 0
    Private mstrChild As String = ""
    Private mintMembershipId As Integer = 0
    Private mstrMembership As String = ""
    Private mintAbsentId As Integer = 0
    Private mstrAbsent As String = ""
    Private mstrLeaveIds As String = ""
    Private mstrOrderByQuery As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeInactiveEmp As Boolean = False
    Private mintAnnualLeaveID As Integer = -1
    Private mstrAnnualLeaveType As String = ""
    Private mintSickLeaveID As Integer = -1
    Private mstrSickLeave As String = ""
    Private mintLeaveBalanceSetting As Integer = 0
    Private mintYearID As Integer = 0
    Private mintOtherEarningTranId As Integer = -1

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _Period() As String
        Set(ByVal value As String)
            mstrPeriod = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _JobID() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _Job() As String
        Set(ByVal value As String)
            mstrJob = value
        End Set
    End Property

    Public WriteOnly Property _LocationID() As Integer
        Set(ByVal value As Integer)
            mintLocationId = value
        End Set
    End Property

    Public WriteOnly Property _Location() As String
        Set(ByVal value As String)
            mstrLocation = value
        End Set
    End Property

    Public WriteOnly Property _SpouseID() As Integer
        Set(ByVal value As Integer)
            mintSpouseId = value
        End Set
    End Property

    Public WriteOnly Property _Spouse() As String
        Set(ByVal value As String)
            mstrSpouse = value
        End Set
    End Property

    Public WriteOnly Property _ChildID() As Integer
        Set(ByVal value As Integer)
            mintChildId = value
        End Set
    End Property

    Public WriteOnly Property _Child() As String
        Set(ByVal value As String)
            mstrChild = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _Membership() As String
        Set(ByVal value As String)
            mstrMembership = value
        End Set
    End Property

    Public WriteOnly Property _AbsentId() As Integer
        Set(ByVal value As Integer)
            mintAbsentId = value
        End Set
    End Property

    Public WriteOnly Property _Absent() As String
        Set(ByVal value As String)
            mstrAbsent = value
        End Set
    End Property

    Public WriteOnly Property _LeaveIds() As String
        Set(ByVal value As String)
            mstrLeaveIds = value
        End Set
    End Property

    Public WriteOnly Property _AnnualLeaveId() As Integer
        Set(ByVal value As Integer)
            mintAnnualLeaveID = value
        End Set
    End Property

    Public WriteOnly Property _AnnualLeave() As String
        Set(ByVal value As String)
            mstrAnnualLeaveType = value
        End Set
    End Property

    Public WriteOnly Property _SickLeaveId() As Integer
        Set(ByVal value As Integer)
            mintSickLeaveID = value
        End Set
    End Property

    Public WriteOnly Property _SickLeave() As String
        Set(ByVal value As String)
            mstrSickLeave = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    Public WriteOnly Property _YearID() As Integer
        Set(ByVal value As Integer)
            mintYearID = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Private mdtAsOnDate As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
    Public WriteOnly Property _AsOnDate() As Date
        Set(ByVal value As Date)
            mdtAsOnDate = value
        End Set
    End Property
    'Sohail (18 May 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriod = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintJobId = 0
            mstrJob = ""
            mintLocationId = 0
            mstrLocation = ""
            mintSpouseId = 0
            mstrSpouse = ""
            mintChildId = 0
            mstrChild = ""
            mintMembershipId = 0
            mstrMembership = ""
            mintAbsentId = 0
            mstrAbsent = ""
            mstrLeaveIds = ""
            mintAnnualLeaveID = 0
            mstrAnnualLeaveType = ""
            mintSickLeaveID = 0
            mstrSickLeave = ""
            mstrOrderByQuery = ""
            mblnIncludeInactiveEmp = False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Period : ") & " " & mstrPeriod & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND  hremployee_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintJobId > 0 Then
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                Me._FilterQuery &= " AND  hremployee_master.jobunkid = @jobunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, "Job : ") & " " & mstrJob & " "
            End If

            If mintLocationId > 0 Then
                objDataOperation.AddParameter("@locationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLocationId)
                Me._FilterQuery &= " AND  hremployee_master.classgroupunkid = @locationunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "Location : ") & " " & mstrLocation & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '   If Not IsNothing(objRpt) Then
        '       Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '   End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- End
    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 6, "Code")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
    'Public Sub Generate_DetailReport()
    Public Sub Generate_DetailReport(ByVal strDatabaseName As String, _
                                     ByVal intUserUnkid As Integer, _
                                     ByVal intYearUnkid As Integer, _
                                     ByVal intCompanyUnkid As Integer, _
                                     ByVal dtPeriodStart As Date, _
                                     ByVal dtPeriodEnd As Date, _
                                     ByVal strUserModeSetting As String, _
                                     ByVal blnOnlyApproved As Boolean, _
                                     ByVal blnIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal blnApplyUserAccessFilter As Boolean, _
                                     ByVal intBaseCurrencyId As Integer)
        'Sohail (21 Aug 2015) - [blnIncludeIn_ActiveEmployee, blnApplyUserAccessFilter]
        'S.SANDEEP [04 JUN 2015] -- START
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        '   intBaseCurrencyId ------------ ADDED
        'S.SANDEEP [04 JUN 2015] -- END

        'Shani(24-Aug-2015) -- End
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try


            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = mintPeriodId
            objPeriod._Periodunkid(strDatabaseName) = mintPeriodId
            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            objDataOperation.ClearParameters()


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = " SELECT " & _
            '            "  CAST(ROW_NUMBER() OVER (ORDER by ISNULL(hremployee_master.employeecode, '')) AS nvarchar(max)) SrNo 	" & _
            '            ", hremployee_master.employeeunkid " & _
            '            ", ISNULL(hremployee_master.employeecode, '') Code " & _
            '            ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS " & mstrMembership & _
            '            ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') Name " & _
            '            ", ISNULL(hrsection_master.name,'') Grade " & _
            '            ", ISNULL(hrstation_master.name,'') Branch " & _
            '            ", ISNULL(hrdepartment_master.name,'') Department " & _
            '            ", CASE WHEN gender = 1 then @male " & _
            '            "            WHEN gender = 2 then @female " & _
            '            "            WHEN gender = 3 then @other " & _
            '            "  ELSE  '' END as 'Gender Type'  " & _
            '            ", ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') As 'Hire Date' " & _
            '            ", CAST(ISNULL(DATEDIFF(YEAR,hremployee_master.appointeddate,'" & objPeriod._End_Date & "'),0) AS BIGINT)  AS YearService " & _
            '            ", '' AS 'Checkcode' " & _
            '            ", ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS birthdate " & _
            '            ", hremployee_master.termination_from_date " & _
            '            ", hremployee_master.termination_to_date " & _
            '            ", hremployee_master.empl_enddate " & _
            '            ", ISNULL(DATEDIFF(YEAR,hremployee_master.birthdate,'" & objPeriod._End_Date & "'),0) AS Age " & _
            '            ", ISNULL(cfcommon_master.name,'') As 'Civil Status' " & _
            '            ", CAST(ISNULL(A.Spouse,0) AS BIGINT) AS 'Spouse' " & _
            '            ", CAST(ISNULL(B.Child,0) AS BIGINT) AS 'Child' " & _
            '            ", CAST(ISNULL(A.Spouse,0) AS BIGINT) +  CAST(ISNULL(B.Child,0) AS BIGINT)  AS Total " & _
            '            ", ISNULL(hrjob_master.job_name,'') AS 'Job' " & _
            '            ", ISNULL(hrclassgroup_master.name,'') 'Location' " & _
            '            ", ISNULL(C.BasicSal,0.00) AS 'Basic Salary' " & _
            '            " FROM hremployee_master " & _
            '            " LEFT JOIN hrsection_master on hrsection_master.sectionunkid  = hremployee_master.sectionunkid " & _
            '            " LEFT JOIN hrstation_master on hrstation_master.stationunkid = hremployee_master.stationunkid " & _
            '            " LEFT JOIN hrdepartment_master on hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
            '            " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hremployee_master.maritalstatusunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
            '            " LEFT JOIN hrjob_master on hrjob_master.jobunkid = hremployee_master.jobunkid " & _
            '            " LEFT JOIN hrclassgroup_master on hrclassgroup_master.classgroupunkid = hremployee_master.classgroupunkid " & _
            '            " LEFT JOIN hremployee_meminfo_tran on hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND membershipunkid  = " & mintMembershipId & _
            '            " LEFT JOIN (SELECT count(*) AS 'Spouse',hrdependants_beneficiaries_tran.employeeunkid From hrdependants_beneficiaries_tran WHERE hrdependants_beneficiaries_tran.relationunkid = " & mintSpouseId & " and hrdependants_beneficiaries_tran.isvoid = 0 " & _
            '            " GROUP BY hrdependants_beneficiaries_tran.employeeunkid) AS A on A.employeeunkid = hremployee_master.employeeunkid " & _
            '            " LEFT JOIN (SELECT count(*) AS 'Child',hrdependants_beneficiaries_tran.employeeunkid From hrdependants_beneficiaries_tran WHERE hrdependants_beneficiaries_tran.relationunkid = " & mintChildId & " and hrdependants_beneficiaries_tran.isvoid = 0 " & _
            '            " GROUP BY  hrdependants_beneficiaries_tran.employeeunkid) AS B on B.employeeunkid = hremployee_master.employeeunkid " & _
            '            " LEFT JOIN  " & _
            '            " ( Select " & _
            '            "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
            '            "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
            '            "      ,cfcommon_period_tran.periodunkid AS PrdId " & _
            '            "      FROM      prpayrollprocess_tran " & _
            '            "      JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '            "      JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '            "      JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
            '            "	   JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid  " & _
            '            "	    WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '            "       AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '            "       AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '            "       AND prpayrollprocess_tran.tranheadunkid > 0 " & _
            '            "       AND cfcommon_period_tran.isactive = 1 " & _
            '            "       AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ")   "

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            If mintEmployeeId > 0 Then
                StrQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If mintEmployeeId > 0 Then
                StrQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @Employeeunkid "
            End If

            If mdtAsOnDate <> Nothing Then
                StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @AsonDate "
                objDataOperation.AddParameter("@AsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            StrQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            StrQ &= " SELECT " & _
                        "  CAST(ROW_NUMBER() OVER (ORDER by ISNULL(hremployee_master.employeecode, '')) AS nvarchar(max)) SrNo 	" & _
                        ", hremployee_master.employeeunkid " & _
                        ", ISNULL(hremployee_master.employeecode, '') Code " & _
                        ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS [" & mstrMembership & "] " & _
                        ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') Name " & _
                        ", ISNULL(hrsection_master.name,'') Grade " & _
                        ", ISNULL(hrstation_master.name,'') Branch " & _
                        ", ISNULL(hrdepartment_master.name,'') Department " & _
                        ", CASE WHEN gender = 1 then @male " & _
                        "            WHEN gender = 2 then @female " & _
                        "            WHEN gender = 3 then @other " & _
                        "  ELSE  '' END as 'Gender Type'  " & _
                        ", ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') As 'Hire Date' " & _
                        ", CAST(ISNULL(DATEDIFF(YEAR,hremployee_master.appointeddate, CAST('" & eZeeDate.convertDateTime(objPeriod._End_Date) & "' AS DATETIME )),0) AS BIGINT)  AS YearService " & _
                        ", '' AS 'Checkcode' " & _
                        ", ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS birthdate " & _
                        ",  hremployee_master.termination_from_date " & _
                        ",  hremployee_master.termination_to_date " & _
                        ",  hremployee_master.empl_enddate " & _
                        ", ISNULL(DATEDIFF(YEAR,hremployee_master.birthdate, CAST('" & eZeeDate.convertDateTime(objPeriod._End_Date) & "' AS DATETIME )),0) AS Age " & _
                        ", ISNULL(cfcommon_master.name,'') As 'Civil Status' " & _
                        ", CAST(ISNULL(A.Spouse,0) AS BIGINT) AS 'Spouse' " & _
                        ", CAST(ISNULL(B.Child,0) AS BIGINT) AS 'Child' " & _
                        ",CAST(ISNULL(A.Spouse,0) AS BIGINT) +  CAST(ISNULL(B.Child,0) AS BIGINT)  AS Total " & _
                        ", ISNULL(hrjob_master.job_name,'') AS 'Job' " & _
                        ", ISNULL(hrclassgroup_master.name,'') 'Location' " & _
                        ", ISNULL(C.BasicSal,0.00) AS 'Basic Salary' " & _
                        " FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            sectionunkid " & _
                        "           ,stationunkid " & _
                        "           ,departmentunkid " & _
                        "           ,classgroupunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "       FROM hremployee_transfer_tran " & _
                        "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                        " LEFT JOIN hrsection_master on hrsection_master.sectionunkid  = Alloc.sectionunkid " & _
                        " LEFT JOIN hrstation_master on hrstation_master.stationunkid = Alloc.stationunkid " & _
                        " LEFT JOIN hrdepartment_master on hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                        " LEFT JOIN hrclassgroup_master on hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid " & _
                        " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hremployee_master.maritalstatusunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
                        "LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            jobunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "       FROM hremployee_categorization_tran " & _
                        "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                        " LEFT JOIN hrjob_master on hrjob_master.jobunkid = Jobs.jobunkid " & _
                        " LEFT JOIN hremployee_meminfo_tran on hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND membershipunkid  = " & mintMembershipId & _
                        " LEFT JOIN (SELECT count(1) AS 'Spouse',hrdependants_beneficiaries_tran.employeeunkid From hrdependants_beneficiaries_tran JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid WHERE hrdependants_beneficiaries_tran.relationunkid = " & mintSpouseId & " and hrdependants_beneficiaries_tran.isvoid = 0 AND #TableDepn.isactive = 1 " & _
                        " GROUP BY hrdependants_beneficiaries_tran.employeeunkid) AS A on A.employeeunkid = hremployee_master.employeeunkid " & _
                        " LEFT JOIN (SELECT count(1) AS 'Child',hrdependants_beneficiaries_tran.employeeunkid From hrdependants_beneficiaries_tran JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid WHERE hrdependants_beneficiaries_tran.relationunkid = " & mintChildId & " and hrdependants_beneficiaries_tran.isvoid = 0 AND #TableDepn.isactive = 1 " & _
                        " GROUP BY  hrdependants_beneficiaries_tran.employeeunkid) AS B on B.employeeunkid = hremployee_master.employeeunkid " & _
                        " LEFT JOIN  " & _
                        "   ( " & _
                        "       Select  " & _
                        "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                        "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
                        "      ,cfcommon_period_tran.periodunkid AS PrdId " & _
                        "      FROM      prpayrollprocess_tran " & _
                        "      JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "      JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "      JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "	   JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid  " & _
                        "	    WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "       AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "       AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "       AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                        "       AND cfcommon_period_tran.isactive = 1 " & _
                        "       AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ")   "
            'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]
            'Shani(24-Aug-2015) -- End

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND prpayrollprocess_tran.tranheadunkid =  " & mintOtherEarningTranId
            Else
                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= "	GROUP BY prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid )  AS C  ON C.Empid  = hremployee_master.employeeunkid " & _
            '             " WHERE 1 = 1 "
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._Start_Date))
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._End_Date))
            'End If

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If

            StrQ &= "	GROUP BY prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid )  AS C  ON C.Empid  = hremployee_master.employeeunkid "


            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= " WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Shani(24-Aug-2015) -- End

            FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            objDataOperation.AddParameter("@male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 35, "Male"))
            objDataOperation.AddParameter("@female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 36, "Female"))
            objDataOperation.AddParameter("@other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "Other"))
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            dsList.Tables(0).Columns.Add(Language.getMessage(mstrModuleName, 27, "Absent"), Type.GetType("System.Decimal"))
            dsList.Tables(0).Columns(Language.getMessage(mstrModuleName, 27, "Absent")).DefaultValue = 0

            dsList.Tables(0).Columns.Add(Language.getMessage(mstrModuleName, 28, "No Of Working Days"), Type.GetType("System.Decimal"))
            dsList.Tables(0).Columns(Language.getMessage(mstrModuleName, 28, "No Of Working Days")).DefaultValue = 0

            If mintAnnualLeaveID > 0 Then
                dsList.Tables(0).Columns.Add(mstrAnnualLeaveType & " " & Language.getMessage(mstrModuleName, 29, "Taken"), Type.GetType("System.Decimal"))
                dsList.Tables(0).Columns(mstrAnnualLeaveType & " " & Language.getMessage(mstrModuleName, 29, "Taken")).DefaultValue = 0

                dsList.Tables(0).Columns.Add(mstrAnnualLeaveType, Type.GetType("System.Decimal"))
                dsList.Tables(0).Columns(mstrAnnualLeaveType).DefaultValue = 0
            End If

            dsList.Tables(0).Columns(Language.getMessage(mstrModuleName, 28, "No Of Working Days")).SetOrdinal(dsList.Tables(0).Columns.Count - 2)

            If mintSickLeaveID > 0 Then
                dsList.Tables(0).Columns.Add(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 30, "less 30 Days"), Type.GetType("System.Decimal"))
                dsList.Tables(0).Columns(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 30, "less 30 Days")).DefaultValue = 0

                dsList.Tables(0).Columns.Add(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 31, "more 30 Days"), Type.GetType("System.Decimal"))
                dsList.Tables(0).Columns(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 31, "more 30 Days")).DefaultValue = 0
            End If


            Dim objLeaveType As New clsleavetype_master
            Dim dsLeaveType As DataSet = objLeaveType.GetList("List", True, True, "leavetypeunkid in (" & mstrLeaveIds & ")")

            If dsLeaveType IsNot Nothing AndAlso dsLeaveType.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To dsLeaveType.Tables(0).Rows.Count - 1
                    dsList.Tables(0).Columns.Add(dsLeaveType.Tables(0).Rows(i)("leavename").ToString(), Type.GetType("System.Decimal"))
                    dsList.Tables(0).Columns(dsLeaveType.Tables(0).Rows(i)("leavename").ToString()).ExtendedProperties.Add(CInt(dsLeaveType.Tables(0).Rows(i)("leavetypeunkid")), dsLeaveType.Tables(0).Rows(i)("leavename").ToString())
                    dsList.Tables(0).Columns(dsLeaveType.Tables(0).Rows(i)("leavename").ToString()).DefaultValue = 0
                    dsList.Tables(0).Columns(dsLeaveType.Tables(0).Rows(i)("leavename").ToString()).Caption = dsLeaveType.Tables(0).Rows(i)("leavename").ToString()
                Next

            End If
            objLeaveType = Nothing

            dsList.Tables(0).Columns.Add(Language.getMessage(mstrModuleName, 32, "Public Holidays"), Type.GetType("System.Int64"))
            dsList.Tables(0).Columns(Language.getMessage(mstrModuleName, 32, "Public Holidays")).DefaultValue = 0

            dsList.Tables(0).Columns.Add(Language.getMessage(mstrModuleName, 33, "Bank"), Type.GetType("System.String"))
            dsList.Tables(0).Columns(Language.getMessage(mstrModuleName, 33, "Bank")).DefaultValue = 0

            dsList.Tables(0).Columns.Add(Language.getMessage(mstrModuleName, 34, "Account No"), Type.GetType("System.String"))
            dsList.Tables(0).Columns(Language.getMessage(mstrModuleName, 34, "Account No")).DefaultValue = 0


            Dim objPayroll As New clsPayrollProcessTran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Dim dsAbsent As DataSet = objPayroll.GetList("List", mintEmployeeId.ToString(), objPeriod._Periodunkid, , , , "prpayrollprocess_tran.tranheadunkid = " & mintAbsentId)
            Dim dsAbsent As DataSet = objPayroll.GetList(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved, blnIncludeIn_ActiveEmployee, "List", mintEmployeeId.ToString(), objPeriod._Periodunkid(strDatabaseName), , blnApplyUserAccessFilter, "prpayrollprocess_tran.tranheadunkid = " & mintAbsentId)
            'Sohail (21 Aug 2015) -- End
            Dim mdicAbsent As Dictionary(Of Integer, Decimal) = (From p In dsAbsent.Tables(0).AsEnumerable() Select New With {Key .ID = CInt(p.Item("employeeunkid")), Key .Amount = CDec(p.Item("amount"))}).ToDictionary(Function(x) x.ID, Function(x) x.Amount)
            objPayroll = Nothing

            Dim objEmpHoliday As New clsemployee_holiday

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dtHoliday As DataTable = objEmpHoliday.GetEmployeeHolidayCount(mintEmployeeId.ToString(), objPeriod._Start_Date.Date, objPeriod._End_Date.Date)
            Dim dtHoliday As DataTable = objEmpHoliday.GetEmployeeHolidayCount(strDatabaseName, objPeriod._Start_Date.Date, objPeriod._End_Date.Date, mintEmployeeId.ToString())
            'Shani(24-Aug-2015) -- End
            Dim mdicEmpHoliday As Dictionary(Of Integer, Integer) = (From p In dtHoliday.AsEnumerable() Select New With {Key .ID = CInt(p.Item("employeeunkid")), Key .HolidayCount = CInt(p.Item("TotalHoliday"))}).ToDictionary(Function(x) x.ID, Function(x) x.HolidayCount)
            objEmpHoliday = Nothing



            Dim dcColumns As DataColumnCollection = dsList.Tables(0).Columns
            Dim objEmpBank As New clsEmployeeBanks
            Dim objLeaveIssue As New clsleaveissue_Tran
            Dim objLeaveAccrue As New clsleavebalance_tran
            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
            Dim mintWorkingDays As Integer = 0

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                If dsList.Tables(0).Rows(i)("Hire Date").ToString().Trim.Length > 0 Then
                    dsList.Tables(0).Rows(i)("Hire Date") = eZeeDate.convertDate(dsList.Tables(0).Rows(i)("Hire Date").ToString().Trim).ToShortDateString
                End If
                If dsList.Tables(0).Rows(i)("birthdate").ToString().Trim.Length > 0 Then
                    dsList.Tables(0).Rows(i)("birthdate") = eZeeDate.convertDate(dsList.Tables(0).Rows(i)("birthdate").ToString().Trim).ToShortDateString
                End If

                'GETTING TOTAL WORKING DAYS OF PERIOD
                Dim mdtFromDate As Date = objPeriod._Start_Date.Date
                Dim mdtToDate As Date = objPeriod._End_Date.Date

                If CDate(dsList.Tables(0).Rows(i)("Hire Date")).Date > objPeriod._Start_Date.Date Then
                    mdtFromDate = CDate(dsList.Tables(0).Rows(i)("Hire Date")).Date
                End If

                If Not IsDBNull(dsList.Tables(0).Rows(i)("termination_from_date")) AndAlso CDate(dsList.Tables(0).Rows(i)("termination_from_date")).Date < objPeriod._End_Date.Date Then
                    mdtToDate = CDate(dsList.Tables(0).Rows(i)("termination_from_date")).Date
                End If
                If Not IsDBNull(dsList.Tables(0).Rows(i)("termination_to_date")) AndAlso CDate(dsList.Tables(0).Rows(i)("termination_to_date")).Date < mdtToDate.Date Then
                    mdtToDate = CDate(dsList.Tables(0).Rows(i)("termination_to_date")).Date
                End If
                If Not IsDBNull(dsList.Tables(0).Rows(i)("empl_enddate")) AndAlso CDate(dsList.Tables(0).Rows(i)("empl_enddate")).Date < mdtToDate.Date Then
                    mdtToDate = CDate(dsList.Tables(0).Rows(i)("empl_enddate")).Date
                End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                'objEmpShiftTran.GetWorkingDaysAndHours(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), mdtFromDate.Date, mdtToDate.Date, mintWorkingDays, 0)
                objEmpShiftTran.GetWorkingDaysAndHours(strDatabaseName, _
                                                       intUserUnkid, _
                                                       intYearUnkid, _
                                                       intCompanyUnkid, _
                                                       strUserModeSetting, _
                                                       CInt(dsList.Tables(0).Rows(i)("employeeunkid")), _
                                                       mdtFromDate.Date, _
                                                       mdtToDate.Date, _
                                                       mintWorkingDays, 0)
                'Shani(24-Aug-2015) -- End

                Dim dsBalance As DataSet = Nothing

                'START GETTING ANNUAL LEAVE
                If mintAnnualLeaveID > 0 Then
                    ' GETTING ANNUAL LEAVE REMAINING BALANCE
                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        dsBalance = objLeaveAccrue.GetEmployeeBalanceData(mintAnnualLeaveID, CInt(dsList.Tables(0).Rows(i)("employeeunkid")), True, mintYearID, False, False, False)
                    ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        dsBalance = objLeaveAccrue.GetEmployeeBalanceData(mintAnnualLeaveID, CInt(dsList.Tables(0).Rows(i)("employeeunkid")), True, mintYearID, True, True, False)
                    End If
                    If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                        Dim objPrePeriod As New clscommom_period_Tran
                        Dim objMasterData As New clsMasterData
                        Dim mdecTotalIssueDayCount As Decimal = 0

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'Dim mintPreviousPeriodID As Integer = objMasterData.getCurrentPeriodID(enModuleReference.Payroll, objPeriod._Start_Date.AddDays(-1).Date)
                        Dim mintPreviousPeriodID As Integer = objMasterData.getCurrentPeriodID(enModuleReference.Payroll, objPeriod._Start_Date.AddDays(-1).Date, intYearUnkid)
                        'S.SANDEEP [04 JUN 2015] -- END

                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPrePeriod._Periodunkid = mintPreviousPeriodID
                        objPrePeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPreviousPeriodID
                        'Sohail (21 Aug 2015) -- End

                        If objPrePeriod._End_Date.Date < CDate(dsBalance.Tables(0).Rows(0)("startdate")).Date Then
                            mdecTotalIssueDayCount = 0
                            ' GETTING ANNUAL LEAVE ISSUED
                            dsList.Tables(0).Rows(i)(mstrAnnualLeaveType & " " & Language.getMessage(mstrModuleName, 29, "Taken")) = 0
                        Else
                            ' GETTING ANNUAL LEAVE ISSUED
                            dsList.Tables(0).Rows(i)(mstrAnnualLeaveType & " " & Language.getMessage(mstrModuleName, 29, "Taken")) = objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), mintAnnualLeaveID, mintYearID, Nothing, objPeriod._Start_Date.Date, objPeriod._End_Date.Date)
                            mdecTotalIssueDayCount = objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), mintAnnualLeaveID, mintYearID, objPrePeriod._End_Date.Date)
                        End If
                        dsList.Tables(0).Rows(i)(mstrAnnualLeaveType) = CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")) - mdecTotalIssueDayCount - CDec(dsList.Tables(0).Rows(i)(mstrAnnualLeaveType & " " & Language.getMessage(mstrModuleName, 29, "Taken")))
                        objPrePeriod = Nothing
                        objMasterData = Nothing
                    Else
                        ' GETTING ANNUAL LEAVE ISSUED
                        dsList.Tables(0).Rows(i)(mstrAnnualLeaveType & " " & Language.getMessage(mstrModuleName, 29, "Taken")) = 0
                        dsList.Tables(0).Rows(i)(mstrAnnualLeaveType) = 0
                    End If
                End If
                'END GETTING ANNUAL LEAVE

                ' START FOR GETTING SICK LEAVE 
                dsBalance.Clear()
                dsBalance = Nothing
                If mintSickLeaveID > 0 Then
                    ' GETTING SICK LEAVE ISSUED
                    Dim mdecIssuedLeave As Decimal = objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), mintSickLeaveID, mintYearID, Nothing, objPeriod._Start_Date.Date, objPeriod._End_Date.Date)

                    Dim objPrePeriod As New clscommom_period_Tran
                    Dim objMasterData As New clsMasterData
                    Dim mdecTotalIssueDayCount As Decimal = 0

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim mintPreviousPeriodID As Integer = objMasterData.getCurrentPeriodID(enModuleReference.Payroll, objPeriod._Start_Date.AddDays(-1).Date)
                    Dim mintPreviousPeriodID As Integer = objMasterData.getCurrentPeriodID(enModuleReference.Payroll, objPeriod._Start_Date.AddDays(-1).Date, intYearUnkid)
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPrePeriod._Periodunkid = mintPreviousPeriodID
                    objPrePeriod._Periodunkid(strDatabaseName) = mintPreviousPeriodID
                    'Sohail (21 Aug 2015) -- End

                    ' GETTING SICK LEAVE ISSUED
                    mdecTotalIssueDayCount = objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), mintSickLeaveID, mintYearID, Nothing, FinancialYear._Object._Database_Start_Date.Date, objPrePeriod._End_Date.Date)

                    If mdecTotalIssueDayCount > 30 Then
                        dsList.Tables(0).Rows(i)(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 30, "less 30 Days")) = 0
                        dsList.Tables(0).Rows(i)(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 31, "more 30 Days")) = objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), mintSickLeaveID, mintYearID, Nothing, objPeriod._Start_Date.Date, objPeriod._End_Date.Date)
                    ElseIf mdecTotalIssueDayCount <= 30 Then
                        Dim mdecTotalSickLeave As Decimal = objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), mintSickLeaveID, mintYearID, Nothing, objPeriod._Start_Date.Date, objPeriod._End_Date.Date)

                        If mdecTotalIssueDayCount + mdecTotalSickLeave > 30 Then
                            dsList.Tables(0).Rows(i)(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 31, "more 30 Days")) = (mdecTotalIssueDayCount + mdecTotalSickLeave) - 30
                            mdecTotalSickLeave = mdecTotalSickLeave - ((mdecTotalIssueDayCount + mdecTotalSickLeave) - 30)
                            dsList.Tables(0).Rows(i)(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 30, "less 30 Days")) = mdecTotalSickLeave
                        ElseIf mdecTotalIssueDayCount + mdecTotalSickLeave <= 30 Then
                            dsList.Tables(0).Rows(i)(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 30, "less 30 Days")) = mdecTotalSickLeave
                            dsList.Tables(0).Rows(i)(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 31, "more 30 Days")) = 0
                        End If
                    Else
                        dsList.Tables(0).Rows(i)(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 30, "less 30 Days")) = 0
                        dsList.Tables(0).Rows(i)(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 31, "more 30 Days")) = 0
                    End If
                    objPrePeriod = Nothing
                    objMasterData = Nothing
                End If
                ' END FOR GETTING SICK LEAVE 

                'START FOR GETTING OTHER LEAVE DAYS FOR PERIOD
                Dim mintColumnindex As Integer = 0
                Dim mdecSumofLeaveDays As Decimal = 0
                If dcColumns.Contains(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 31, "more 30 Days")) Then
                    mintColumnindex = dcColumns.IndexOf(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 31, "more 30 Days"))
                End If
                Dim intIndex As Integer = 0
                Dim arLeaveIDs() As String = mstrLeaveIds.Split(",")
                For k As Integer = mintColumnindex + 1 To mintColumnindex + arLeaveIDs.Length
                    If dsList.Tables(0).Columns(k).ExtendedProperties.ContainsKey(CInt(arLeaveIDs(intIndex))) Then
                        dsList.Tables(0).Rows(i)(k) = objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveIDs(intIndex).ToString), mintYearID, Nothing, objPeriod._Start_Date.Date, objPeriod._End_Date.Date)
                        mdecSumofLeaveDays += CDec(dsList.Tables(0).Rows(i)(k))
                    Else
                        dsList.Tables(0).Rows(i)(k) = 0
                    End If
                    intIndex += 1
                Next
                arLeaveIDs = Nothing
                'END FOR GETTING OTHER LEAVE DAYS FOR PERIOD


                'START FOR GETTIGN ABSENCE
                If mdicAbsent IsNot Nothing AndAlso mdicAbsent.ContainsKey(CInt(dsList.Tables(0).Rows(i)("employeeunkid"))) Then
                    dsList.Tables(0).Rows(i)(Language.getMessage(mstrModuleName, 27, "Absent")) = CDec(mdicAbsent.Item(CInt(dsList.Tables(0).Rows(i)("employeeunkid"))))
                Else
                    dsList.Tables(0).Rows(i)(Language.getMessage(mstrModuleName, 27, "Absent")) = 0
                End If
                'END FOR GETTIGN ABSENCE


                'START FOR GETTING PUBLIC HOLIDAY
                If mdicEmpHoliday IsNot Nothing AndAlso mdicEmpHoliday.ContainsKey(CInt(dsList.Tables(0).Rows(i)("employeeunkid"))) Then
                    dsList.Tables(0).Rows(i)("Public Holidays") = CInt(mdicEmpHoliday.Item(CInt(dsList.Tables(0).Rows(i)("employeeunkid"))))
                Else
                    dsList.Tables(0).Rows(i)("Public Holidays") = 0
                End If
                'END FOR GETTING PUBLIC HOLIDAY


                dsList.Tables(0).Rows(i)(Language.getMessage(mstrModuleName, 28, "No Of Working Days")) = mintWorkingDays - CDec(dsList.Tables(0).Rows(i)(Language.getMessage(mstrModuleName, 27, "Absent"))) - mdecSumofLeaveDays _
                                                                                                                                                              - CDec(dsList.Tables(0).Rows(i)(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 30, "less 30 Days"))) _
                                                                                                                                                              - CDec(dsList.Tables(0).Rows(i)(mstrSickLeave & " " & Language.getMessage(mstrModuleName, 31, "more 30 Days"))) _
                                                                                                                                                              - CDec(dsList.Tables(0).Rows(i)(mstrAnnualLeaveType & " " & Language.getMessage(mstrModuleName, 29, "Taken"))) _
                                                                                                                                                              - CDec(dsList.Tables(0).Rows(i)("Public Holidays"))



                'START FOR GETTING BANK AND ACCOUNT NO
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Dim dsBank As DataSet = objEmpBank.GetList("List", CInt(dsList.Tables(0).Rows(i)("employeeunkid")), objPeriod._End_Date.Date, "", "", mintPeriodId)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim dsBank As DataSet = objEmpBank.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , CInt(dsList.Tables(0).Rows(i)("employeeunkid")), "", "")
                Dim dsBank As DataSet = objEmpBank.GetList(strDatabaseName, _
                                                           intUserUnkid, _
                                                           intYearUnkid, _
                                                           intCompanyUnkid, _
                                                           objPeriod._Start_Date, _
                                                           objPeriod._End_Date, _
                                                           strUserModeSetting, True, _
                                                           blnOnlyApproved, "List", True, , _
                                                           CInt(dsList.Tables(0).Rows(i)("employeeunkid")), objPeriod._End_Date, )

                'Shani (24-May-2016) -- [CInt(dsList.Tables(0).Rows(i)("employeeunkid")),"" , "")]


                'Shani(24-Aug-2015) -- End

                'Sohail (21 Aug 2015) -- End
                If dsBank IsNot Nothing AndAlso dsBank.Tables(0).Rows.Count > 0 Then
                    Dim mstrBank As String = ""
                    Dim mstrAccNo As String = ""

                    Dim lstEmp As List(Of String) = (From p In dsBank.Tables(0) Select (p.Item("BranchName").ToString())).ToList
                    mstrBank = String.Join(", #10;", lstEmp.ToArray)

                    lstEmp = Nothing

                    lstEmp = (From p In dsBank.Tables(0) Select (p.Item("accountno").ToString())).ToList
                    mstrAccNo = String.Join(", #10;", lstEmp.ToArray)

                    dsList.Tables(0).Rows(i)("Bank") = mstrBank
                    dsList.Tables(0).Rows(i)("Account No") = mstrAccNo
                End If
                dsBank.Clear()
                dsBank = Nothing
                'END FOR GETTING BANK AND ACCOUNT NO

            Next
            mdicAbsent.Clear()
            mdicAbsent = Nothing
            mdicEmpHoliday.Clear()
            mdicEmpHoliday = Nothing
            objLeaveIssue = Nothing
            objEmpHoliday = Nothing
            objEmpBank = Nothing

            If dsList IsNot Nothing Then
                If dsList.Tables(0).Columns.Contains("employeeunkid") Then
                    dsList.Tables(0).Columns.Remove("employeeunkid")
                End If

                If dsList.Tables(0).Columns.Contains("termination_from_date") Then
                    dsList.Tables(0).Columns.Remove("termination_from_date")
                End If

                If dsList.Tables(0).Columns.Contains("termination_to_date") Then
                    dsList.Tables(0).Columns.Remove("termination_to_date")
                End If

                If dsList.Tables(0).Columns.Contains("empl_enddate") Then
                    dsList.Tables(0).Columns.Remove("empl_enddate")
                End If
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dsList.Tables(0)


            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            Dim mintStartIndex As Integer = dcColumns.IndexOf(Language.getMessage(mstrModuleName, 27, "Absent"))
            Dim mintEndIIndex As Integer = dcColumns.IndexOf(Language.getMessage(mstrModuleName, 32, "Public Holidays"))
            row = New WorksheetRow()
            For i As Integer = 0 To mintStartIndex - 1
                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)
            Next
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 38, "Transactions Of the Current Period"), "HeaderStyle")
            wcell.MergeAcross = mintEndIIndex - mintStartIndex
            row.Cells.Add(wcell)
            For i As Integer = mintEndIIndex + 1 To dcColumns.Count - 1
                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)
            Next
            rowsArrayHeader.Add(row)


            mintStartIndex = dcColumns.IndexOf(Language.getMessage(mstrModuleName, 28, "No Of Working Days")) + 1
            mintEndIIndex = dcColumns.IndexOf(Language.getMessage(mstrModuleName, 32, "Public Holidays"))
            row = New WorksheetRow()
            For i As Integer = 0 To mintStartIndex - 1
                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)
            Next
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 39, "No Of Days"), "HeaderStyle")
            wcell.MergeAcross = mintEndIIndex - mintStartIndex
            row.Cells.Add(wcell)
            For i As Integer = mintEndIIndex + 1 To dcColumns.Count - 1
                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)
            Next
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 4, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            '--------------------

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If mdtTableExcel.Columns(i).ColumnName = "Spouse" OrElse mdtTableExcel.Columns(i).ColumnName = "Child" OrElse mdtTableExcel.Columns(i).ColumnName = "Age" OrElse mdtTableExcel.Columns(i).ColumnName = "Gender Type" Then
                    intArrayColumnWidth(i) = 60
                ElseIf mdtTableExcel.Columns(i).ColumnName = "SrNo" OrElse mdtTableExcel.Columns(i).ColumnName = "YearService" OrElse mdtTableExcel.Columns(i).ColumnName = "birthdate" OrElse mdtTableExcel.Columns(i).ColumnName = "Hire Date" _
                        OrElse mdtTableExcel.Columns(i).ColumnName = Language.getMessage(mstrModuleName, 32, "Public Holidays") Then
                    intArrayColumnWidth(i) = 80
                ElseIf mdtTableExcel.Columns(i).ColumnName = "Name" Then
                    intArrayColumnWidth(i) = 150
                Else
                    intArrayColumnWidth(i) = 125
                End If
            Next
            'SET EXCEL CELL WIDTH


            mdtTableExcel.Columns("SrNo").Caption = Language.getMessage(mstrModuleName, 5, "Sr.No")
            mdtTableExcel.Columns("Code").Caption = Language.getMessage(mstrModuleName, 6, "Code")
            mdtTableExcel.Columns("Name").Caption = Language.getMessage(mstrModuleName, 7, "Name")
            mdtTableExcel.Columns("Grade").Caption = Language.getMessage(mstrModuleName, 8, "Grade")
            mdtTableExcel.Columns("Branch").Caption = Language.getMessage(mstrModuleName, 9, "Branch")
            mdtTableExcel.Columns("Department").Caption = Language.getMessage(mstrModuleName, 10, "Department")
            mdtTableExcel.Columns("Gender Type").Caption = Language.getMessage(mstrModuleName, 11, "Gender")
            mdtTableExcel.Columns("Hire Date").Caption = Language.getMessage(mstrModuleName, 12, "Hire Date")
            mdtTableExcel.Columns("YearService").Caption = Language.getMessage(mstrModuleName, 13, "Year of Service")
            mdtTableExcel.Columns("Checkcode").Caption = Language.getMessage(mstrModuleName, 14, "13th Check Code")
            mdtTableExcel.Columns("birthdate").Caption = Language.getMessage(mstrModuleName, 15, "Birth Date")
            mdtTableExcel.Columns("Age").Caption = Language.getMessage(mstrModuleName, 16, "Age")
            mdtTableExcel.Columns("Civil Status").Caption = Language.getMessage(mstrModuleName, 17, "Civil Status")
            mdtTableExcel.Columns("Spouse").Caption = Language.getMessage(mstrModuleName, 18, "Spouse")
            mdtTableExcel.Columns("Child").Caption = Language.getMessage(mstrModuleName, 19, "Child")
            mdtTableExcel.Columns("Total").Caption = Language.getMessage(mstrModuleName, 20, "Total Dependant")
            mdtTableExcel.Columns("Job").Caption = Language.getMessage(mstrModuleName, 21, "Job Title")
            mdtTableExcel.Columns("Location").Caption = Language.getMessage(mstrModuleName, 22, "Location")

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, , strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

            dcColumns = Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Sr.No")
            Language.setMessage(mstrModuleName, 6, "Code")
            Language.setMessage(mstrModuleName, 7, "Name")
            Language.setMessage(mstrModuleName, 8, "Grade")
            Language.setMessage(mstrModuleName, 9, "Branch")
            Language.setMessage(mstrModuleName, 10, "Department")
            Language.setMessage(mstrModuleName, 11, "Gender")
            Language.setMessage(mstrModuleName, 12, "Hire Date")
            Language.setMessage(mstrModuleName, 13, "Year of Service")
            Language.setMessage(mstrModuleName, 14, "13th Check Code")
            Language.setMessage(mstrModuleName, 15, "Birth Date")
            Language.setMessage(mstrModuleName, 16, "Age")
            Language.setMessage(mstrModuleName, 17, "Civil Status")
            Language.setMessage(mstrModuleName, 18, "Spouse")
            Language.setMessage(mstrModuleName, 19, "Child")
            Language.setMessage(mstrModuleName, 20, "Total Dependant")
            Language.setMessage(mstrModuleName, 21, "Job Title")
            Language.setMessage(mstrModuleName, 22, "Location")
            Language.setMessage(mstrModuleName, 23, "Period :")
            Language.setMessage(mstrModuleName, 24, "Employee :")
            Language.setMessage(mstrModuleName, 25, "Job :")
            Language.setMessage(mstrModuleName, 26, "Location :")
            Language.setMessage(mstrModuleName, 27, "Absent")
            Language.setMessage(mstrModuleName, 28, "No Of Working Days")
            Language.setMessage(mstrModuleName, 29, "Taken")
            Language.setMessage(mstrModuleName, 30, "less 30 Days")
            Language.setMessage(mstrModuleName, 31, "more 30 Days")
            Language.setMessage(mstrModuleName, 32, "Public Holidays")
            Language.setMessage(mstrModuleName, 33, "Bank")
            Language.setMessage(mstrModuleName, 34, "Account No")
            Language.setMessage(mstrModuleName, 35, "Male")
            Language.setMessage(mstrModuleName, 36, "Female")
            Language.setMessage(mstrModuleName, 37, "Other")
            Language.setMessage(mstrModuleName, 38, "Transactions Of the Current Period")
            Language.setMessage(mstrModuleName, 39, "No Of Days")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
