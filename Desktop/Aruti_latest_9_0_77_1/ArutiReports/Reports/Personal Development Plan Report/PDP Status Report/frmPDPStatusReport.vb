﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmPDPStatusReport
#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPDPStatusReport"
    Private objPDPStatusReport As clsPDPStatusReport

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty
    Private dvEmployee As DataView
    Private mstrSearchEmpText As String = ""
    Dim strEmployeeId As String = ""
#End Region

#Region " Constructor "
    Public Sub New()
        objPDPStatusReport = New clsPDPStatusReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objPDPStatusReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmPDPStatusReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPDPStatusReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPStatusReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPDPStatusReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Call Language.setLanguage(Me.Name)
            'OtherSettings()
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPStatusReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPDPStatusReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPStatusReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPDPStatusReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPStatusReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTalentExitsReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTalentExitsReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objMaster As New clsMasterData
        Dim ObjEmp As New clsEmployee_Master

        Try
            dsCombo = objMaster.GetPDPFormStatuList(True, "FormStatusList")
            With cboStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("FormStatusList")
                .SelectedValue = 0
            End With

            dsCombo = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Emp")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            chkIncludeInactiveEmp.Checked = False
            cboStatus.SelectedIndex = 0
            If cboEmployee.DataSource IsNot Nothing AndAlso cboEmployee.Items.Count > 0 Then cboEmployee.SelectedIndex = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            objPDPStatusReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objPDPStatusReport.SetDefaultValue()

            objPDPStatusReport._Statusid = cboStatus.SelectedValue
            objPDPStatusReport._StatusName = cboStatus.Text
            objPDPStatusReport._EmployeeUnkid = cboEmployee.SelectedValue
            objPDPStatusReport._EmployeeName = cboEmployee.Text
            objPDPStatusReport._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            objPDPStatusReport._ViewByIds = mstrStringIds
            objPDPStatusReport._ViewIndex = mintViewIdx
            objPDPStatusReport._ViewByName = mstrStringName
            objPDPStatusReport._Analysis_Fields = mstrAnalysis_Fields
            objPDPStatusReport._Analysis_Join = mstrAnalysis_Join
            objPDPStatusReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPDPStatusReport._Report_GroupName = mstrReport_GroupName
            objPDPStatusReport._Advance_Filter = mstrAdvanceFilter
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Buttons Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            'If CInt(cboStatus.SelectedIndex) = 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Status."), enMsgBoxStyle.Information)
            '    cboStatus.Focus()
            '    Exit Sub
            'End If

            If SetFilter() = False Then Exit Sub


            objPDPStatusReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport, _
                                                                0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchStatus.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboStatus.DataSource
            frm.ValueMember = cboStatus.ValueMember
            frm.DisplayMember = cboStatus.DisplayMember
            If frm.DisplayDialog Then
                cboStatus.SelectedValue = frm.SelectedValue
                cboStatus.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchStatus_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Combobox Events"
    
#End Region

#Region " Other Control's Events "

    'Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
    '    Dim frm As New frmViewAnalysis
    '    Try
    '        frm.displayDialog()
    '        mstrStringIds = frm._ReportBy_Ids
    '        mstrStringName = frm._ReportBy_Name
    '        mintViewIdx = frm._ViewIndex
    '        mstrAnalysis_Fields = frm._Analysis_Fields
    '        mstrAnalysis_Join = frm._Analysis_Join
    '        mstrAnalysis_OrderBy = frm._Analysis_OrderBy
    '        mstrReport_GroupName = frm._Report_GroupName
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
    '    Finally
    '        frm = Nothing
    '    End Try
    'End Sub


    Private Sub lnkAdvanceFilter_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Status.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class