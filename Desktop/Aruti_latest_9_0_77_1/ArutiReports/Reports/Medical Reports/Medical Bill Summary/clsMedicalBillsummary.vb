Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

Public Class clsMedicalBillsummary
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsMedicalBillsummary"
    Private mstrReportId As String = enArutiReport.MedicalBillSummaryReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnEmpSummaryReport()
        Call Create_OnProviderSummaryReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintFromPeriodId As Integer = -1
    Private mstrFromPeriodName As String = String.Empty
    Private mintToPeriodId As Integer = -1
    Private mstrToPeriodName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintProviderId As Integer = -1
    Private mstrProviderName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mstrOrderByQuery As String = ""
    Private mstrPeriodId As String = String.Empty
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : Including inactive employee condition.
    Private mblnIncludeInactiveEmp As Boolean = False
    'Anjan [08 September 2015] -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _mstrPeriodId() As String
        Set(ByVal value As String)
            mstrPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodId() As Integer
        Set(ByVal value As Integer)
            mintFromPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodName() As String
        Set(ByVal value As String)
            mstrFromPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ProviderId() As Integer
        Set(ByVal value As Integer)
            mintProviderId = value
        End Set
    End Property

    Public WriteOnly Property _ProviderName() As String
        Set(ByVal value As String)
            mstrProviderName = value
        End Set
    End Property


    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property


    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : Including inactive employee condition.
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'Anjan [08 September 2015] -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mintFromPeriodId = -1
            mstrFromPeriodName = ""
            mintToPeriodId = -1
            mstrToPeriodName = ""
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintProviderId = -1
            mstrProviderName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrReport_GroupName = ""

            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : Including inactive employee condition.
            mblnIncludeInactiveEmp = False
            'Anjan [08 September 2015] -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mstrPeriodId.Trim.Length > 0 Then
                Me._FilterQuery &= " AND mdmedical_claim_master.periodunkid in (" & mstrPeriodId & ") "
            End If


            If mintFromPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "From Period :") & " " & mstrFromPeriodName & " "
            End If

            If mintToPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "To Period :") & " " & mstrToPeriodName & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND mdmedical_claim_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintProviderId > 0 Then
                Me._FilterQuery &= " AND mdmedical_claim_master.instituteunkid = @instituteunkid "
                objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProviderId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Provider :") & " " & mstrProviderName & " "
            End If


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                If mintViewIndex > 0 Then
                    If Me.OrderByQuery.Trim = "" Then
                        mstrOrderByQuery &= " ORDER BY GNAME"
                    Else
                        mstrOrderByQuery &= "ORDER BY GNAME, " & Me.OrderByQuery
                    End If

                Else
                    mstrOrderByQuery &= "ORDER BY " & Me.OrderByQuery
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, " Order By : ") & " " & Me.OrderByDisplay
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    If mintReportTypeId = 0 Then
        '        objRpt = Generate_EmployeeWiseDetailReport()
        '    ElseIf mintReportTypeId = 1 Then
        '        objRpt = Generate_ProviderWiseSummaryReport()
        '    End If

        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyUnkid As Integer = 0)
        Dim ObjRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid < 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid < 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If
            User._Object._Userunkid = mintUserUnkid

            If mintReportTypeId = 0 Then
                ObjRpt = Generate_EmployeeWiseDetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)
            ElseIf mintReportTypeId = 1 Then
                ObjRpt = Generate_ProviderWiseSummaryReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)
            End If


            Rpt = ObjRpt
            If Not IsNothing(ObjRpt) Then
                Call ReportExecute(ObjRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Anjan [08 September 2015] -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            If intReportType = 0 Then
                OrderByDisplay = iColumnEmp_SummaryReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumnEmp_SummaryReport.ColumnItem(0).Name
            ElseIf intReportType = 1 Then
                OrderByDisplay = iColumnProvider_SummaryReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumnProvider_SummaryReport.ColumnItem(0).Name
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            If intReportType = 0 Then
                Call OrderByExecute(iColumnEmp_SummaryReport)
            ElseIf intReportType = 1 Then
                Call OrderByExecute(iColumnProvider_SummaryReport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Employee Wise Summary "

    Dim iColumnEmp_SummaryReport As New IColumnCollection

    Public Property Field_OnEmpSummaryReport() As IColumnCollection
        Get
            Return iColumnEmp_SummaryReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumnEmp_SummaryReport = value
        End Set
    End Property

    Private Sub Create_OnEmpSummaryReport()
        Try
            iColumnEmp_SummaryReport.Clear()

            iColumnEmp_SummaryReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            iColumnEmp_SummaryReport.Add(New IColumn("employee", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumnEmp_SummaryReport.Add(New IColumn("amount", Language.getMessage(mstrModuleName, 5, "Amount")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnEmpSummaryReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function Generate_EmployeeWiseDetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Public Function Generate_EmployeeWiseDetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Anjan [08 September 2015] -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Anjan [08 September 2015] -- End


            StrQ = " SELECT " & _
                       " employeeunkid " & _
                       ", employeecode " & _
                       ", employee " & _
                       ", department " & _
                       ", job " & _
                       ",SUM(Amount) AS 'Amount' " & _
                       ",SUM(Amount1) AS 'Amount1'  " & _
                       ",SUM(Amount2) AS 'Amount2' "

            If mintViewIndex > 0 Then
                StrQ &= " , id, GName "
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM " & _
                       "( " & _
                            " SELECT  mdmedical_claim_tran.employeeunkid " & _
                         " ,ISNULL(hremployee_master.employeecode, '') employeecode " & _
                         " ,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') employee  " & _
                         " ,dept.name as department  " & _
                         " ,jb.job_name as job " & _
                         " ,SUM(ISNULL(mdmedical_claim_tran.amount,0.00)) amount " & _
                             ",CASE WHEN mdmedical_claim_tran.isempexempted = 1 THEN  SUM(Amount) ELSE  SUM(Amount)* 80 / 100 END AS 'Amount1' " & _
                            ",CASE WHEN mdmedical_claim_tran.isempexempted = 1 THEN 0.00 ELSE SUM(Amount)* 20 / 100 END AS 'Amount2' "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            'AND mdmedical_claim_tran.dependantsunkid <= 0
            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            StrQ &= " FROM mdmedical_claim_master " & _
                         " JOIN mdmedical_claim_tran ON mdmedical_claim_master.claimunkid = mdmedical_claim_tran.claimunkid  AND mdmedical_claim_tran.isvoid = 0  " & _
                         " JOIN hremployee_master ON mdmedical_claim_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        " JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "        departmentunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         " JOIN hrdepartment_master dept ON Alloc.departmentunkid = dept.departmentunkid " & _
                     " JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                         " JOIN hrjob_master jb ON Jobs.jobunkid = jb.jobunkid"

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If


            'Anjan [08 September 2015] -- End

            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE mdmedical_claim_master.isvoid = 0  AND mdmedical_claim_master.isfinal = 1 "


            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= "AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Anjan [08 September 2015] -- End



            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= " GROUP BY mdmedical_claim_tran.employeeunkid " & _
                         ",ISNULL(hremployee_master.employeecode, '') " & _
                         ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '')  " & _
                         ",dept.name ,jb.job_name,mdmedical_claim_tran.isempexempted  "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            End If

            StrQ &= ") AS A GROUP BY employeecode ,employeeunkid ,employee,department,job "

            If mintViewIndex > 0 Then
                StrQ &= " , Id, GName "
            End If

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim mstrAllocation As String = String.Empty
            Dim mdlAllocationAmount As Decimal = 0
            Dim mdlAllocationAmount1 As Decimal = 0
            Dim mdlAllocationAmount2 As Decimal = 0
            Dim mdlGrandTotal As Decimal = 0
            Dim mdlGrandTotal1 As Decimal = 0
            Dim mdlGrandTotal2 As Decimal = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("GName")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employee")
                rpt_Row.Item("Column4") = dtRow.Item("department")
                rpt_Row.Item("Column5") = dtRow.Item("job")
                rpt_Row.Item("Column6") = Format(dtRow.Item("amount"), GUI.fmtCurrency)
                rpt_Row.Item("Column9") = Format(dtRow.Item("amount1"), GUI.fmtCurrency)
                rpt_Row.Item("Column10") = Format(dtRow.Item("amount2"), GUI.fmtCurrency)

                If dtRow.Item("GName").ToString() <> mstrAllocation Then
                    mstrAllocation = dtRow.Item("GName").ToString()
                    mdlAllocationAmount = 0
                End If

                mdlAllocationAmount += CDec(dtRow.Item("amount"))
                mdlAllocationAmount1 += CDec(dtRow.Item("amount1"))
                mdlAllocationAmount2 += CDec(dtRow.Item("amount2"))

                mdlGrandTotal += CDec(dtRow.Item("amount"))
                mdlGrandTotal1 += CDec(dtRow.Item("amount1"))
                mdlGrandTotal2 += CDec(dtRow.Item("amount2"))

                rpt_Row.Item("Column7") = Format(mdlAllocationAmount, GUI.fmtCurrency)
                rpt_Row.Item("Column8") = Format(mdlGrandTotal, GUI.fmtCurrency)
                rpt_Row.Item("Column11") = Format(mdlAllocationAmount1, GUI.fmtCurrency)
                rpt_Row.Item("Column12") = Format(mdlGrandTotal1, GUI.fmtCurrency)
                rpt_Row.Item("Column13") = Format(mdlAllocationAmount2, GUI.fmtCurrency)
                rpt_Row.Item("Column14") = Format(mdlGrandTotal2, GUI.fmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptMedicalBillSummaryEmpWise

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 13, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 14, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 15, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If


            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 1, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 2, "Employee "))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 3, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 4, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 5, "Amount"))


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'Call ReportFunction.TextChange(objRpt, "txtAmount1", Language.getMessage(mstrModuleName, 6, "80%"))
            'Call ReportFunction.TextChange(objRpt, "txtAmount2", Language.getMessage(mstrModuleName, 7, "20%"))

            Call ReportFunction.TextChange(objRpt, "txtAmount1", Language.getMessage(mstrModuleName, 6, "Employer"))
            Call ReportFunction.TextChange(objRpt, "txtAmount2", Language.getMessage(mstrModuleName, 7, "Employee"))

            'Pinkal (18-Dec-2012) -- End



            Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", Language.getMessage(mstrModuleName, 8, "Allocation Total : "))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 9, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EmployeeWiseSummaryReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try

    End Function

#End Region

#Region " Provider Wise Summary "

    Dim iColumnProvider_SummaryReport As New IColumnCollection

    Public Property Field_OnProviderSummaryReport() As IColumnCollection
        Get
            Return iColumnProvider_SummaryReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumnProvider_SummaryReport = value
        End Set
    End Property

    Private Sub Create_OnProviderSummaryReport()
        Try
            iColumnProvider_SummaryReport.Clear()
            iColumnProvider_SummaryReport.Add(New IColumn("hrinstitute_master.institute_name", Language.getMessage(mstrModuleName, 21, "Service Provider")))
            iColumnProvider_SummaryReport.Add(New IColumn("amount", Language.getMessage(mstrModuleName, 5, "Amount")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnProviderSummaryReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function Generate_ProviderWiseSummaryReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Public Function Generate_ProviderWiseSummaryReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Anjan [08 September 2015] -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()




            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'No need to modify this report for new active employee condition.
            StrQ &= " SELECT hrinstitute_master.institute_name as Provider " & _
                         ", SUM(ISNULL(mdmedical_claim_master.invoice_amt, 0.00)) amount " & _
                         " FROM mdmedical_claim_master " & _
                         " JOIN hrinstitute_master ON mdmedical_claim_master.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.isactive = 1 " & _
                         " WHERE mdmedical_claim_master.isvoid = 0  AND mdmedical_claim_master.isfinal = 1 "


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= " GROUP BY hrinstitute_master.institute_name "

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim mdclAmount As Decimal = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("Provider")
                rpt_Row.Item("Column2") = Format(dtRow.Item("amount"), GUI.fmtCurrency)

                mdclAmount += CDec(dtRow.Item("amount"))

                rpt_Row.Item("Column3") = Format(mdclAmount, GUI.fmtCurrency)
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptMedicalBillSummaryProviderWise

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 13, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 14, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 15, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If


            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtProvider", Language.getMessage(mstrModuleName, 21, "Service Provider"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 5, "Amount"))

            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 9, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_ProviderWiseSummaryReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try

    End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "Department")
            Language.setMessage(mstrModuleName, 4, "Job")
            Language.setMessage(mstrModuleName, 5, "Amount")
            Language.setMessage(mstrModuleName, 6, "Employer")
            Language.setMessage(mstrModuleName, 7, "Employee")
            Language.setMessage(mstrModuleName, 8, "Allocation Total :")
            Language.setMessage(mstrModuleName, 9, "Grand Total :")
            Language.setMessage(mstrModuleName, 10, "Printed By :")
            Language.setMessage(mstrModuleName, 11, "Printed Date :")
            Language.setMessage(mstrModuleName, 12, "Prepared By :")
            Language.setMessage(mstrModuleName, 13, "Checked By :")
            Language.setMessage(mstrModuleName, 14, "Approved By :")
            Language.setMessage(mstrModuleName, 15, "Received By :")
            Language.setMessage(mstrModuleName, 16, "From Period :")
            Language.setMessage(mstrModuleName, 17, "To Period :")
            Language.setMessage(mstrModuleName, 18, "Employee :")
            Language.setMessage(mstrModuleName, 19, "Provider :")
            Language.setMessage(mstrModuleName, 20, " Order By :")
            Language.setMessage(mstrModuleName, 21, "Service Provider")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
