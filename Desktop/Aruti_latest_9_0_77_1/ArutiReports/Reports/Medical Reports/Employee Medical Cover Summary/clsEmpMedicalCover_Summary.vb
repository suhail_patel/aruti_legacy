Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

Public Class clsEmpMedicalCover_Summary
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmpMedicalCover_Summary"
    Private mstrReportId As String = enArutiReport.Employee_Medical_Cover_Summary
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private variables "

    Private mintPeriodId As Integer = 0
    Private mstrPeriod As String = ""
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mintEmploymentType As Integer = 0
    Private mstrEmploymentType As String = ""
    Private mintStationId As Integer = 0
    Private mstrStation As String = ""
    Private mstrServiceProviderIDs As String = ""
    Private mstrServiceProviderNames As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintUserUnkid As Integer = 0
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Dim StrFinalPath As String = String.Empty
    Dim mdctMaritalStatus As Dictionary(Of Integer, String)
    Dim mdctProviderMtlStatus As Dictionary(Of Integer, String)

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodID() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _Period() As String
        Set(ByVal value As String)
            mstrPeriod = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As Date
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As Date
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _EmploymentTypeID() As Integer
        Set(ByVal value As Integer)
            mintEmploymentType = value
        End Set
    End Property

    Public WriteOnly Property _EmploymentType() As String
        Set(ByVal value As String)
            mstrEmploymentType = value
        End Set
    End Property

    Public WriteOnly Property _StationID() As Integer
        Set(ByVal value As Integer)
            mintStationId = value
        End Set
    End Property

    Public WriteOnly Property _Station() As String
        Set(ByVal value As String)
            mstrStation = value
        End Set
    End Property

    Public WriteOnly Property _ServiceProviderIDs() As String
        Set(ByVal value As String)
            mstrServiceProviderIDs = value
        End Set
    End Property

    Public WriteOnly Property _ServiceProviderNames() As String
        Set(ByVal value As String)
            mstrServiceProviderNames = value
        End Set
    End Property

    Public WriteOnly Property _CompanyID() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserID() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriod = ""
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mintEmploymentType = 0
            mstrEmploymentType = ""
            mintStationId = 0
            mstrStation = ""
            mstrServiceProviderIDs = ""
            mstrServiceProviderNames = ""
            mdctMaritalStatus = Nothing
            mdctProviderMtlStatus = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Period:") & " " & mstrPeriod & " "
            End If

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))

            If mintEmploymentType > 0 Then
                Me._FilterQuery &= " AND cfcommon_master.masterunkid = @employmenttypeunkid "
                objDataOperation.AddParameter("@employmenttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmploymentType)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Employment Type:") & " " & mstrEmploymentType & " "
            End If

            If mintStationId > 0 Then
                Me._FilterQuery &= " AND hrstation_master.stationunkid = @stationunkid "
                objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Station:") & " " & mstrStation & " "
            End If

            If mstrServiceProviderNames.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Service Provider:") & " " & mstrServiceProviderNames & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try


            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Sub Generate_DetailReport()
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrQFilter As String = ""
    '    Dim dsList As New DataSet
    '    Try

    '        If mintCompanyUnkid <= 0 Then
    '            mintCompanyUnkid = Company._Object._Companyunkid
    '        End If

    '        Company._Object._Companyunkid = mintCompanyUnkid
    '        ConfigParameter._Object._Companyunkid = mintCompanyUnkid

    '        If mintUserUnkid <= 0 Then
    '            mintUserUnkid = User._Object._Userunkid
    '        End If

    '        User._Object._Userunkid = mintUserUnkid

    '        mdctMaritalStatus = New Dictionary(Of Integer, String)
    '        mdctProviderMtlStatus = New Dictionary(Of Integer, String)


    '        objDataOperation = New clsDataOperation

    '        objDataOperation.ClearParameters()


    '        StrQ = " SELECT ROW_NUMBER() OVER (PARTITION BY ISNULL( cfcommon_master.name,'') ORDER by ISNULL(hrstation_master.name,'')) SrNo " & _
    '                    ", ISNULL( cfcommon_master.masterunkid,'') AS employmenttypeId " & _
    '                    ", ISNULL( cfcommon_master.name,'') AS EmploymentType " & _
    '                    ", hrstation_master.stationunkid " & _
    '                    ", hrstation_master.name AS Branch " & _
    '                    ", COUNT(*)  Headcount " & _
    '                    " FROM hrstation_master " & _
    '                    " JOIN hremployee_master on hrstation_master.stationunkid = hremployee_master.stationunkid " & _
    '                    " JOIN cfcommon_master on hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid and mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & _
    '                    " WHERE 1 = 1 AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= "   GROUP by hrstation_master.name,ISNULL( cfcommon_master.name,''),ISNULL( cfcommon_master.masterunkid,''),hrstation_master.stationunkid"

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        StrQ = "select hrinstitute_master.instituteunkid  " & _
    '                    ", ISNULL(hrinstitute_master.institute_name,'') AS Provider " & _
    '                   ", mdmedical_master.mdmasterunkid " & _
    '                   ", mdmedical_master.mdmastername AS Category " & _
    '                   ", mdmedical_category_master.maritalstatusunkid AS maritalstatusunkid " & _
    '                   ", ISNULL(cfcommon_master.name,'') AS 'Marital Status' " & _
    '                   ", ISNULL(mdmedical_category_master.amount,0) AS Amount " & _
    '                   " from mdcategory_providermapping " & _
    '                   " JOIN hrinstitute_master on hrinstitute_master.instituteunkid = mdcategory_providermapping.providerunkid AND ishospital = 1 " & _
    '                   " JOIN mdmedical_master on mdmedical_master.mdmasterunkid = mdcategory_providermapping.mdcategorymasterunkid and mdmedical_master.mdmastertype_id =  " & enMedicalMasterType.Medical_Category & _
    '                   " JOIN mdmedical_category_master ON mdmedical_category_master.medicalcategoryunkid = mdcategory_providermapping.medicalcategoryunkid " & _
    '                   " JOIN cfcommon_master ON cfcommon_master.masterunkid = mdmedical_category_master.maritalstatusunkid AND cfcommon_master.mastertype =  " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
    '                   " WHERE mdcategory_providermapping.isvoid = 0 AND mdcategory_providermapping.providerunkid in (" & mstrServiceProviderIDs & ") " & _
    '                   " order by ISNULL(hrinstitute_master.institute_name,''),mdmedical_master.mdmastername"

    '        objDataOperation.ClearParameters()
    '        Dim dsCategory As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If



    '        If dsCategory IsNot Nothing AndAlso dsCategory.Tables(0).Rows.Count > 0 Then
    '            Dim mintProviderID As Integer = 0
    '            For Each dr As DataRow In dsCategory.Tables(0).Rows
    '                dsList.Tables(0).Columns.Add(dr("instituteunkid").ToString().Trim & "_" & dr("mdmasterunkid").ToString().Trim, Type.GetType("System.Int32"))
    '                dsList.Tables(0).Columns(dr("instituteunkid").ToString().Trim & "_" & dr("mdmasterunkid").ToString().Trim).ExtendedProperties.Add(dr("instituteunkid").ToString().Trim & "_" & dr("mdmasterunkid").ToString().Trim, CInt(dr("maritalstatusunkid")))
    '                dsList.Tables(0).Columns(dr("instituteunkid").ToString().Trim & "_" & dr("mdmasterunkid").ToString().Trim).Caption = dr("Category").ToString().Trim
    '                dsList.Tables(0).Columns(dr("instituteunkid").ToString().Trim & "_" & dr("mdmasterunkid").ToString().Trim).DefaultValue = 0
    '                If mintProviderID <> CInt(dr("instituteunkid").ToString().Trim) Then
    '                    dsList.Tables(0).Columns.Add(dr("instituteunkid").ToString().Trim & "_" & Language.getMessage(mstrModuleName, 4, "Total"), Type.GetType("System.Decimal"))
    '                    dsList.Tables(0).Columns(dr("instituteunkid").ToString().Trim & "_" & Language.getMessage(mstrModuleName, 4, "Total")).DefaultValue = 0
    '                    mintProviderID = CInt(dr("instituteunkid").ToString())
    '                Else
    '                    dsList.Tables(0).Columns(dr("instituteunkid").ToString().Trim & "_" & Language.getMessage(mstrModuleName, 4, "Total")).SetOrdinal(dsList.Tables(0).Columns.Count - 1)
    '                End If

    '                If mdctMaritalStatus.ContainsKey(dr("maritalstatusunkid")) = False Then
    '                    mdctMaritalStatus.Add(CInt(dr("maritalstatusunkid")), dr("Marital Status").ToString())
    '                End If

    '                If mdctProviderMtlStatus.ContainsKey(mintProviderID) = False Then
    '                    mdctProviderMtlStatus.Add(mintProviderID, CDec(dr("Amount")))
    '                Else
    '                    mdctProviderMtlStatus(mintProviderID) &= "|" & CDec(dr("Amount")).ToString()
    '                End If

    '            Next

    '            If mdctMaritalStatus.Count > 0 Then
    '                For Each key As Integer In mdctMaritalStatus.Keys
    '                    dsList.Tables(0).Columns.Add(mdctMaritalStatus(key).ToString(), Type.GetType("System.Int64"))
    '                    dsList.Tables(0).Columns(mdctMaritalStatus(key).ToString()).ExtendedProperties.Add(key, mdctMaritalStatus(key))
    '                    dsList.Tables(0).Columns(mdctMaritalStatus(key).ToString()).DefaultValue = 0
    '                Next
    '                dsList.Tables(0).Columns.Add("Total_amount", Type.GetType("System.Decimal"))
    '                dsList.Tables(0).Columns("Total_amount").DefaultValue = 0
    '                dsList.Tables(0).Columns("Total_amount").Caption = Language.getMessage(mstrModuleName, 4, "Total")

    '                dsList.Tables(0).Columns.Add("AvgCost", Type.GetType("System.Decimal"))
    '                dsList.Tables(0).Columns("AvgCost").DefaultValue = 0
    '                dsList.Tables(0).Columns("AvgCost").Caption = Language.getMessage(mstrModuleName, 8, "Average cost per EE")

    '                dsList.Tables(0).Columns.Add("Comments", Type.GetType("System.String"))
    '                dsList.Tables(0).Columns("Comments").DefaultValue = "0"
    '                dsList.Tables(0).Columns("Comments").Caption = Language.getMessage(mstrModuleName, 9, "Comments")

    '            End If

    '        End If

    '        Dim dcColumns As DataColumnCollection = dsList.Tables(0).Columns
    '        Dim mintIndexCategoryStartIndex As Integer = dcColumns.IndexOf("Headcount") + 1
    '        Dim mintIndexCategoryEndIndex As Integer = 0
    '        If mdctMaritalStatus.Count > 0 Then
    '            mintIndexCategoryEndIndex = dcColumns.Count - (mdctMaritalStatus.Count + 3) - 1
    '        Else
    '            mintIndexCategoryEndIndex = dsList.Tables(0).Columns.Count - 1
    '        End If

    '        For Each drRow As DataRow In dsList.Tables(0).Rows
    '            Dim mdecTotal As Decimal = 0
    '            Dim mintProviderID As Integer = 0
    '            Dim mintSumofKey As Integer = 0
    '            For i As Integer = mintIndexCategoryStartIndex To mintIndexCategoryEndIndex
    '                If dsList.Tables(0).Columns(i).ColumnName.Contains(Language.getMessage(mstrModuleName, 4, "Total")) = False Then
    '                    Dim mintCategoryID As Integer = CInt(dsList.Tables(0).Columns(i).ColumnName.ToString().Substring(dsList.Tables(0).Columns(i).ColumnName.ToString().IndexOf("_") + 1))
    '                    Dim mintKey As Integer = 0
    '                    Dim mdtTable As DataTable = GetEmployeeHeadCount(CInt(drRow("employmenttypeId")), CInt(drRow("stationunkid")), mintCategoryID)
    '                    If mdtTable IsNot Nothing AndAlso mdtTable.Rows.Count > 0 Then
    '                        drRow(dsList.Tables(0).Columns(i).ColumnName) = CInt(mdtTable.Rows(0)("HeadCount"))
    '                        mdecTotal += CDec(mdtTable.Rows(0)("Total"))
    '                        If dsList.Tables(0).Columns(i).ExtendedProperties.Count > 0 Then
    '                            mintKey = dsList.Tables(0).Columns(i).ExtendedProperties(dsList.Tables(0).Columns(i).ColumnName)
    '                            If IsDBNull(drRow(mdctMaritalStatus(mintKey).ToString())) = True Then drRow(mdctMaritalStatus(mintKey).ToString()) = 0
    '                            drRow(mdctMaritalStatus(mintKey).ToString()) += CInt(drRow(dsList.Tables(0).Columns(i).ColumnName))
    '                            If IsDBNull(drRow("Comments")) = True Then drRow("Comments") = 0
    '                            drRow("Comments") = CInt(drRow("Comments")) + CInt(drRow(dsList.Tables(0).Columns(i).ColumnName))
    '                        End If
    '                    Else
    '                        drRow(dsList.Tables(0).Columns(i).ColumnName) = 0
    '                        If dsList.Tables(0).Columns(i).ExtendedProperties.Count > 0 Then
    '                            mintKey = dsList.Tables(0).Columns(i).ExtendedProperties(dsList.Tables(0).Columns(i).ColumnName)
    '                            If IsDBNull(drRow(mdctMaritalStatus(mintKey).ToString())) = True Then drRow(mdctMaritalStatus(mintKey).ToString()) = 0
    '                            drRow(mdctMaritalStatus(mintKey).ToString()) += CInt(drRow(dsList.Tables(0).Columns(i).ColumnName))
    '                            If IsDBNull(drRow("Comments")) = True Then drRow("Comments") = 0
    '                            drRow("Comments") = CInt(drRow("Comments")) + CInt(drRow(dsList.Tables(0).Columns(i).ColumnName))
    '                        End If
    '                    End If
    '                Else
    '                    drRow(dsList.Tables(0).Columns(i).ColumnName) = mdecTotal
    '                    If IsDBNull(drRow("Total_Amount")) Then drRow("Total_Amount") = 0
    '                    drRow("Total_Amount") += CDec(drRow(dsList.Tables(0).Columns(i).ColumnName))
    '                    drRow("AvgCost") = Math.Round(CDec(CDec(drRow("Total_Amount")) / CInt(drRow("HeadCount"))), 2)
    '                    mdecTotal = 0
    '                End If
    '            Next
    '        Next

    '        dcColumns = Nothing

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

    '        Dim mdtTableExcel As DataTable = dsList.Tables(0)

    '        If mdtTableExcel IsNot Nothing Then

    '            If mdtTableExcel.Columns.Contains("employmenttypeId") Then
    '                mdtTableExcel.Columns.Remove("employmenttypeId")
    '            End If

    '            If mdtTableExcel.Columns.Contains("stationunkid") Then
    '                mdtTableExcel.Columns.Remove("stationunkid")
    '            End If

    '            For i As Integer = 4 To mdtTableExcel.Columns.Count - 1
    '                If mdtTableExcel.Columns(i).ColumnName.Contains("_Total") Then
    '                    mdtTableExcel.Columns(i).Caption = Language.getMessage(mstrModuleName, 4, "Total")
    '                End If
    '            Next


    '            If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), mstrExportReportPath, mdtTableExcel) Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Report successfully exported to Report export path."), enMsgBoxStyle.Information)
    '                Call ReportFunction.Open_ExportedFile(mblnOpenAfterExport, StrFinalPath)
    '            End If


    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub

    'Public Function GetEmployeeHeadCount(ByVal intEmploymentTypId As Integer, ByVal intStationunkid As Integer, ByVal intMedicalCategory As Integer) As DataTable
    '    Dim mdtTable As DataTable = Nothing
    '    Dim objDataOperation As New clsDataOperation
    '    Try

    '        Dim strQ As String = " SELECT count(*) AS HeadCount,count(*) * mdmedical_category_master.amount AS Total,mdmedical_category_master.amount from hremployee_master " & _
    '                                        " JOIN hrstation_master on hrstation_master.stationunkid = hremployee_master.stationunkid " & _
    '                                        " JOIN cfcommon_master on cfcommon_master.masterunkid = hremployee_master.employmenttypeunkid  and cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & _
    '                                        " JOIN mdmedical_cover on mdmedical_cover.employeeunkid = hremployee_master.employeeunkid and mdmedical_cover.isvoid =0 " & _
    '                                        " JOIN mdcategory_providermapping on mdcategory_providermapping.mdcategorymasterunkid = mdmedical_cover.medicalcategoryunkid AND mdcategory_providermapping.isvoid = 0 " & _
    '                                        " JOIN mdmedical_master on mdmedical_master.mdmasterunkid = mdcategory_providermapping.mdcategorymasterunkid " & _
    '                                        " JOIN mdmedical_category_master on mdmedical_category_master.mdcategorymasterunkid = mdcategory_providermapping.mdcategorymasterunkid AND mdmedical_category_master.isactive = 1 " & _
    '                                        " WHERE hrstation_master.stationunkid = " & intStationunkid & "  And mdmedical_master.mdmasterunkid = " & intMedicalCategory & " And cfcommon_master.masterunkid = " & intEmploymentTypId & _
    '                                        " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate " & _
    '                                        " AND (@startdate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate) " & _
    '                                        " OR @enddate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate ) ) " & _
    '                                        " GROUP by mdmedical_category_master.amount "


    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
    '        Dim dsHeadCount As DataSet = objDataOperation.ExecQuery(strQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        If dsHeadCount IsNot Nothing Then
    '            mdtTable = dsHeadCount.Tables(0)
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
    '    Finally
    '        objDataOperation = Nothing
    '    End Try
    '    Return mdtTable
    'End Function


    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                               , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date, ByVal mblnIncludeInactiveEmp As Boolean)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            mdctMaritalStatus = New Dictionary(Of Integer, String)
            mdctProviderMtlStatus = New Dictionary(Of Integer, String)


            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)



            'StrQ = " SELECT ROW_NUMBER() OVER (PARTITION BY ISNULL( cfcommon_master.name,'') ORDER by ISNULL(hrstation_master.name,'')) SrNo " & _
            '            ", ISNULL( cfcommon_master.masterunkid,'') AS employmenttypeId " & _
            '            ", ISNULL( cfcommon_master.name,'') AS EmploymentType " & _
            '            ", hrstation_master.stationunkid " & _
            '            ", hrstation_master.name AS Branch " & _
            '            ", COUNT(*)  Headcount " & _
            '            " FROM hrstation_master " & _
            '            " JOIN hremployee_master on hrstation_master.stationunkid = hremployee_master.stationunkid " & _
            '            " JOIN cfcommon_master on hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid and mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & _
            '            " WHERE 1 = 1 AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "


            StrQ = " SELECT ROW_NUMBER() OVER (PARTITION BY ISNULL( cfcommon_master.name,'') ORDER by ISNULL(hrstation_master.name,'')) SrNo " & _
                        ", ISNULL( cfcommon_master.masterunkid,'') AS employmenttypeId " & _
                        ", ISNULL( cfcommon_master.name,'') AS EmploymentType " & _
                        ", hrstation_master.stationunkid " & _
                        ", hrstation_master.name AS Branch " & _
                        ", COUNT(*)  Headcount " & _
                        " FROM hrstation_master " & _
                        " JOIN hremployee_master on hrstation_master.stationunkid = hremployee_master.stationunkid " & _
                   " JOIN " & _
                   " ( " & _
                   "    SELECT " & _
                   "         stationunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "    FROM hremployee_transfer_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                   " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                   " JOIN cfcommon_master on hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid and mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE 1 = 1 "


            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= "   GROUP by hrstation_master.name,ISNULL( cfcommon_master.name,''),ISNULL( cfcommon_master.masterunkid,''),hrstation_master.stationunkid"

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrQ = "select hrinstitute_master.instituteunkid  " & _
                        ", ISNULL(hrinstitute_master.institute_name,'') AS Provider " & _
                       ", mdmedical_master.mdmasterunkid " & _
                       ", mdmedical_master.mdmastername AS Category " & _
                       ", mdmedical_category_master.maritalstatusunkid AS maritalstatusunkid " & _
                       ", ISNULL(cfcommon_master.name,'') AS 'Marital Status' " & _
                       ", ISNULL(mdmedical_category_master.amount,0) AS Amount " & _
                       " from mdcategory_providermapping " & _
                       " JOIN hrinstitute_master on hrinstitute_master.instituteunkid = mdcategory_providermapping.providerunkid AND ishospital = 1 " & _
                       " JOIN mdmedical_master on mdmedical_master.mdmasterunkid = mdcategory_providermapping.mdcategorymasterunkid and mdmedical_master.mdmastertype_id =  " & enMedicalMasterType.Medical_Category & _
                       " JOIN mdmedical_category_master ON mdmedical_category_master.medicalcategoryunkid = mdcategory_providermapping.medicalcategoryunkid " & _
                       " JOIN cfcommon_master ON cfcommon_master.masterunkid = mdmedical_category_master.maritalstatusunkid AND cfcommon_master.mastertype =  " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
                       " WHERE mdcategory_providermapping.isvoid = 0 AND mdcategory_providermapping.providerunkid in (" & mstrServiceProviderIDs & ") " & _
                       " order by ISNULL(hrinstitute_master.institute_name,''),mdmedical_master.mdmastername"

            objDataOperation.ClearParameters()
            Dim dsCategory As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If



            If dsCategory IsNot Nothing AndAlso dsCategory.Tables(0).Rows.Count > 0 Then
                Dim mintProviderID As Integer = 0
                For Each dr As DataRow In dsCategory.Tables(0).Rows
                    dsList.Tables(0).Columns.Add(dr("instituteunkid").ToString().Trim & "_" & dr("mdmasterunkid").ToString().Trim, Type.GetType("System.Int32"))
                    dsList.Tables(0).Columns(dr("instituteunkid").ToString().Trim & "_" & dr("mdmasterunkid").ToString().Trim).ExtendedProperties.Add(dr("instituteunkid").ToString().Trim & "_" & dr("mdmasterunkid").ToString().Trim, CInt(dr("maritalstatusunkid")))
                    dsList.Tables(0).Columns(dr("instituteunkid").ToString().Trim & "_" & dr("mdmasterunkid").ToString().Trim).Caption = dr("Category").ToString().Trim
                    dsList.Tables(0).Columns(dr("instituteunkid").ToString().Trim & "_" & dr("mdmasterunkid").ToString().Trim).DefaultValue = 0
                    If mintProviderID <> CInt(dr("instituteunkid").ToString().Trim) Then
                        dsList.Tables(0).Columns.Add(dr("instituteunkid").ToString().Trim & "_" & Language.getMessage(mstrModuleName, 4, "Total"), Type.GetType("System.Decimal"))
                        dsList.Tables(0).Columns(dr("instituteunkid").ToString().Trim & "_" & Language.getMessage(mstrModuleName, 4, "Total")).DefaultValue = 0
                        mintProviderID = CInt(dr("instituteunkid").ToString())
                    Else
                        dsList.Tables(0).Columns(dr("instituteunkid").ToString().Trim & "_" & Language.getMessage(mstrModuleName, 4, "Total")).SetOrdinal(dsList.Tables(0).Columns.Count - 1)
                    End If

                    If mdctMaritalStatus.ContainsKey(dr("maritalstatusunkid")) = False Then
                        mdctMaritalStatus.Add(CInt(dr("maritalstatusunkid")), dr("Marital Status").ToString())
                    End If

                    If mdctProviderMtlStatus.ContainsKey(mintProviderID) = False Then
                        mdctProviderMtlStatus.Add(mintProviderID, CDec(dr("Amount")))
                    Else
                        mdctProviderMtlStatus(mintProviderID) &= "|" & CDec(dr("Amount")).ToString()
                    End If

                Next

                If mdctMaritalStatus.Count > 0 Then
                    For Each key As Integer In mdctMaritalStatus.Keys
                        dsList.Tables(0).Columns.Add(mdctMaritalStatus(key).ToString(), Type.GetType("System.Int64"))
                        dsList.Tables(0).Columns(mdctMaritalStatus(key).ToString()).ExtendedProperties.Add(key, mdctMaritalStatus(key))
                        dsList.Tables(0).Columns(mdctMaritalStatus(key).ToString()).DefaultValue = 0
                    Next
                    dsList.Tables(0).Columns.Add("Total_amount", Type.GetType("System.Decimal"))
                    dsList.Tables(0).Columns("Total_amount").DefaultValue = 0
                    dsList.Tables(0).Columns("Total_amount").Caption = Language.getMessage(mstrModuleName, 4, "Total")

                    dsList.Tables(0).Columns.Add("AvgCost", Type.GetType("System.Decimal"))
                    dsList.Tables(0).Columns("AvgCost").DefaultValue = 0
                    dsList.Tables(0).Columns("AvgCost").Caption = Language.getMessage(mstrModuleName, 8, "Average cost per EE")

                    dsList.Tables(0).Columns.Add("Comments", Type.GetType("System.String"))
                    dsList.Tables(0).Columns("Comments").DefaultValue = "0"
                    dsList.Tables(0).Columns("Comments").Caption = Language.getMessage(mstrModuleName, 9, "Comments")

                End If

            End If

            Dim dcColumns As DataColumnCollection = dsList.Tables(0).Columns
            Dim mintIndexCategoryStartIndex As Integer = dcColumns.IndexOf("Headcount") + 1
            Dim mintIndexCategoryEndIndex As Integer = 0
            If mdctMaritalStatus.Count > 0 Then
                mintIndexCategoryEndIndex = dcColumns.Count - (mdctMaritalStatus.Count + 3) - 1
            Else
                mintIndexCategoryEndIndex = dsList.Tables(0).Columns.Count - 1
            End If

            For Each drRow As DataRow In dsList.Tables(0).Rows
                Dim mdecTotal As Decimal = 0
                Dim mintProviderID As Integer = 0
                Dim mintSumofKey As Integer = 0
                For i As Integer = mintIndexCategoryStartIndex To mintIndexCategoryEndIndex
                    If dsList.Tables(0).Columns(i).ColumnName.Contains(Language.getMessage(mstrModuleName, 4, "Total")) = False Then
                        Dim mintCategoryID As Integer = CInt(dsList.Tables(0).Columns(i).ColumnName.ToString().Substring(dsList.Tables(0).Columns(i).ColumnName.ToString().IndexOf("_") + 1))
                        Dim mintKey As Integer = 0

                        'Pinkal (24-Aug-2015) -- Start
                        'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                        'Dim mdtTable As DataTable = GetEmployeeHeadCount(CInt(drRow("employmenttypeId")), CInt(drRow("stationunkid")), mintCategoryID)
                        Dim mdtTable As DataTable = GetEmployeeHeadCount(CInt(drRow("employmenttypeId")), CInt(drRow("stationunkid")), mintCategoryID _
                                                                                                        , xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                                                        , mdtEmployeeAsonDate, mblnIncludeInactiveEmp)
                        'Pinkal (24-Aug-2015) -- End


                        If mdtTable IsNot Nothing AndAlso mdtTable.Rows.Count > 0 Then
                            drRow(dsList.Tables(0).Columns(i).ColumnName) = CInt(mdtTable.Rows(0)("HeadCount"))
                            mdecTotal += CDec(mdtTable.Rows(0)("Total"))
                            If dsList.Tables(0).Columns(i).ExtendedProperties.Count > 0 Then
                                mintKey = dsList.Tables(0).Columns(i).ExtendedProperties(dsList.Tables(0).Columns(i).ColumnName)
                                If IsDBNull(drRow(mdctMaritalStatus(mintKey).ToString())) = True Then drRow(mdctMaritalStatus(mintKey).ToString()) = 0
                                drRow(mdctMaritalStatus(mintKey).ToString()) += CInt(drRow(dsList.Tables(0).Columns(i).ColumnName))
                                If IsDBNull(drRow("Comments")) = True Then drRow("Comments") = 0
                                drRow("Comments") = CInt(drRow("Comments")) + CInt(drRow(dsList.Tables(0).Columns(i).ColumnName))
                            End If
                        Else
                            drRow(dsList.Tables(0).Columns(i).ColumnName) = 0
                            If dsList.Tables(0).Columns(i).ExtendedProperties.Count > 0 Then
                                mintKey = dsList.Tables(0).Columns(i).ExtendedProperties(dsList.Tables(0).Columns(i).ColumnName)
                                If IsDBNull(drRow(mdctMaritalStatus(mintKey).ToString())) = True Then drRow(mdctMaritalStatus(mintKey).ToString()) = 0
                                drRow(mdctMaritalStatus(mintKey).ToString()) += CInt(drRow(dsList.Tables(0).Columns(i).ColumnName))
                                If IsDBNull(drRow("Comments")) = True Then drRow("Comments") = 0
                                drRow("Comments") = CInt(drRow("Comments")) + CInt(drRow(dsList.Tables(0).Columns(i).ColumnName))
                            End If
                        End If
                    Else
                        drRow(dsList.Tables(0).Columns(i).ColumnName) = mdecTotal
                        If IsDBNull(drRow("Total_Amount")) Then drRow("Total_Amount") = 0
                        drRow("Total_Amount") += CDec(drRow(dsList.Tables(0).Columns(i).ColumnName))
                        drRow("AvgCost") = Math.Round(CDec(CDec(drRow("Total_Amount")) / CInt(drRow("HeadCount"))), 2)
                        mdecTotal = 0
                    End If
                Next
            Next

            dcColumns = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim mdtTableExcel As DataTable = dsList.Tables(0)

            If mdtTableExcel IsNot Nothing Then

                If mdtTableExcel.Columns.Contains("employmenttypeId") Then
                    mdtTableExcel.Columns.Remove("employmenttypeId")
                End If

                If mdtTableExcel.Columns.Contains("stationunkid") Then
                    mdtTableExcel.Columns.Remove("stationunkid")
                End If

                For i As Integer = 4 To mdtTableExcel.Columns.Count - 1
                    If mdtTableExcel.Columns(i).ColumnName.Contains("_Total") Then
                        mdtTableExcel.Columns(i).Caption = Language.getMessage(mstrModuleName, 4, "Total")
                    End If
                Next


                If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), mstrExportReportPath, mdtTableExcel) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Report successfully exported to Report export path."), enMsgBoxStyle.Information)
                    Call ReportFunction.Open_ExportedFile(mblnOpenAfterExport, StrFinalPath)
                End If


            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Function GetEmployeeHeadCount(ByVal intEmploymentTypId As Integer, ByVal intStationunkid As Integer, ByVal intMedicalCategory As Integer _
                                                              , ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                               , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date, ByVal mblnIncludeInactiveEmp As Boolean) As DataTable
        Dim mdtTable As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)


            'Dim strQ As String = " SELECT count(*) AS HeadCount,count(*) * mdmedical_category_master.amount AS Total,mdmedical_category_master.amount from hremployee_master " & _
            '                                " JOIN hrstation_master on hrstation_master.stationunkid = hremployee_master.stationunkid " & _
            '                                " JOIN cfcommon_master on cfcommon_master.masterunkid = hremployee_master.employmenttypeunkid  and cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & _
            '                                " JOIN mdmedical_cover on mdmedical_cover.employeeunkid = hremployee_master.employeeunkid and mdmedical_cover.isvoid =0 " & _
            '                                " JOIN mdcategory_providermapping on mdcategory_providermapping.mdcategorymasterunkid = mdmedical_cover.medicalcategoryunkid AND mdcategory_providermapping.isvoid = 0 " & _
            '                                " JOIN mdmedical_master on mdmedical_master.mdmasterunkid = mdcategory_providermapping.mdcategorymasterunkid " & _
            '                                " JOIN mdmedical_category_master on mdmedical_category_master.mdcategorymasterunkid = mdcategory_providermapping.mdcategorymasterunkid AND mdmedical_category_master.isactive = 1 " & _
            '                                " WHERE hrstation_master.stationunkid = " & intStationunkid & "  And mdmedical_master.mdmasterunkid = " & intMedicalCategory & " And cfcommon_master.masterunkid = " & intEmploymentTypId & _
            '                                " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                                " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate " & _
            '                                " AND (@startdate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate) " & _
            '                                " OR @enddate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate ) ) " & _
            '                                " GROUP by mdmedical_category_master.amount "


            Dim strQ As String = " SELECT count(*) AS HeadCount,count(*) * mdmedical_category_master.amount AS Total,mdmedical_category_master.amount from hremployee_master " & _
                                           " JOIN " & _
                                           " ( " & _
                                           "    SELECT " & _
                                           "         stationunkid " & _
                                           "        ,employeeunkid " & _
                                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                           "    FROM hremployee_transfer_tran " & _
                                           "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                                           " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                                           " JOIN hrstation_master on hrstation_master.stationunkid = Alloc.stationunkid " & _
                                            " JOIN cfcommon_master on cfcommon_master.masterunkid = hremployee_master.employmenttypeunkid  and cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & _
                                            " JOIN mdmedical_cover on mdmedical_cover.employeeunkid = hremployee_master.employeeunkid and mdmedical_cover.isvoid =0 " & _
                                            " JOIN mdcategory_providermapping on mdcategory_providermapping.mdcategorymasterunkid = mdmedical_cover.medicalcategoryunkid AND mdcategory_providermapping.isvoid = 0 " & _
                                            " JOIN mdmedical_master on mdmedical_master.mdmasterunkid = mdcategory_providermapping.mdcategorymasterunkid " & _
                                           " JOIN mdmedical_category_master on mdmedical_category_master.mdcategorymasterunkid = mdcategory_providermapping.mdcategorymasterunkid AND mdmedical_category_master.isactive = 1 "


            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If


            strQ &= " WHERE hrstation_master.stationunkid = " & intStationunkid & "  And mdmedical_master.mdmasterunkid = " & intMedicalCategory & " And cfcommon_master.masterunkid = " & intEmploymentTypId & _
                                            " AND (@startdate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate) " & _
                        " OR @enddate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate ) ) "


            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If


            strQ &= " GROUP by mdmedical_category_master.amount "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
            Dim dsHeadCount As DataSet = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsHeadCount IsNot Nothing Then
                mdtTable = dsHeadCount.Tables(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return mdtTable
    End Function

    'Pinkal (24-Aug-2015) -- End

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try

            'HEADER PART
            strBuilder.Append(" <TITLE>" & Me._ReportName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If

                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 7, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='60%' colspan=15 align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 5, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='60%' colspan=15  align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)

            Dim mstrEmploymentType As String = ""
            For P As Integer = 0 To objDataReader.Rows.Count - 1

                If mstrEmploymentType <> objDataReader.Rows(P)("EmploymentType").ToString.Trim Then

                    Dim drRow() As DataRow = objDataReader.Select("EmploymentType = '" & mstrEmploymentType.Trim & "'")
                    If drRow.Length > 0 Then
                        strBuilder.Append(" <TR> " & vbCrLf)
                        For k As Integer = 0 To objDataReader.Columns.Count - 1
                            If objDataReader.Columns(k).ColumnName = "EmploymentType" Then Continue For
                            If objDataReader.Columns(k).ColumnName <> "SrNo" AndAlso objDataReader.Columns(k).ColumnName <> "Branch" AndAlso objDataReader.Columns(k).ColumnName <> "Comments" Then
                                If objDataReader.Columns(k).ColumnName <> "Headcount" AndAlso objDataReader.Columns(k).ColumnName <> "AvgCost" AndAlso objDataReader.Columns(k).ColumnName <> "Comments" Then
                                    strBuilder.Append("<TD BORDER=1 BGCOLOR = 'GRAY' BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Format(CDec(objDataReader.Compute("SUM([" & objDataReader.Columns(k).ColumnName & "])", "[EmploymentType]= '" & mstrEmploymentType & "'")), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                                ElseIf objDataReader.Columns(k).ColumnName <> "AvgCost" AndAlso objDataReader.Columns(k).ColumnName <> "Comments" Then
                                    strBuilder.Append("<TD BORDER=1 BGCOLOR = 'GRAY' BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & CDec(objDataReader.Compute("SUM([" & objDataReader.Columns(k).ColumnName & "])", "[EmploymentType]= '" & mstrEmploymentType & "'")) & "</B></FONT></TD>" & vbCrLf)
                                ElseIf objDataReader.Columns(k).ColumnName = "AvgCost" Then
                                    strBuilder.Append("<TD BORDER=1 BGCOLOR = 'GRAY' BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Math.Round(CDec(objDataReader.Compute("SUM([Total_amount])", "[EmploymentType]= '" & mstrEmploymentType & "'")) / CDec(objDataReader.Compute("SUM([Headcount])", "[EmploymentType]= '" & mstrEmploymentType & "'")), 2) & "</B></FONT></TD>" & vbCrLf)
                                End If
                            Else
                                strBuilder.Append("<TD BORDER=1 BGCOLOR = 'GRAY' BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & "" & "</B></FONT></TD>" & vbCrLf)
                            End If
                        Next
                        strBuilder.Append(" </TR> " & vbCrLf)
                        strBuilder.Append(" </TABLE> ")
                    End If

                    strBuilder.Append(" <BR> " & vbCrLf)
                    strBuilder.Append(" <FONT SIZE=2><b> " & Language.getMessage(mstrModuleName, 3, "Employment Type: ") & objDataReader.Rows(P)("EmploymentType").ToString.Trim & "</B></FONT> " & vbCrLf)
                    strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
                    strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
                    'Report Column Caption
                    For j As Integer = 0 To objDataReader.Columns.Count - 1
                        If objDataReader.Columns(j).ColumnName <> "EmploymentType" Then
                            strBuilder.Append("<TD BORDER=1 BGCOLOR = 'GRAY' WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                        End If
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)

                End If

                'Data Part
                ' For i As Integer = 0 To objDataReader.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1
                    If objDataReader.Columns(k).ColumnName <> "EmploymentType" Then
                        If objDataReader.Columns(k).ColumnName <> "Branch" AndAlso objDataReader.Columns(k).Caption <> Language.getMessage(mstrModuleName, 4, "Total") Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & objDataReader.Rows(P)(k) & "</FONT></TD>" & vbCrLf)
                        ElseIf objDataReader.Columns(k).ColumnName <> "Branch" AndAlso objDataReader.Columns(k).Caption = Language.getMessage(mstrModuleName, 4, "Total") Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & CDec(objDataReader.Rows(P)(k)).ToString(GUI.fmtCurrency) & "</FONT></TD>" & vbCrLf)
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataReader.Rows(P)(k) & "</FONT></TD>" & vbCrLf)
                        End If
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)

                mstrEmploymentType = objDataReader.Rows(P)("EmploymentType").ToString.Trim

            Next

            Dim dLRow() As DataRow = objDataReader.Select("EmploymentType = '" & mstrEmploymentType.Trim & "'")
            If dLRow.Length > 0 Then
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1
                    If objDataReader.Columns(k).ColumnName = "EmploymentType" Then Continue For
                    If objDataReader.Columns(k).ColumnName <> "SrNo" AndAlso objDataReader.Columns(k).ColumnName <> "Branch" AndAlso objDataReader.Columns(k).ColumnName <> "Comments" Then
                        If objDataReader.Columns(k).ColumnName <> "Headcount" AndAlso objDataReader.Columns(k).ColumnName <> "AvgCost" Then
                            strBuilder.Append("<TD BORDER=1 BGCOLOR = 'GRAY' BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Format(CDec(objDataReader.Compute("SUM([" & objDataReader.Columns(k).ColumnName & "])", "[EmploymentType]= '" & mstrEmploymentType & "'")), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                        ElseIf objDataReader.Columns(k).ColumnName <> "AvgCost" AndAlso objDataReader.Columns(k).ColumnName <> "Comments" Then
                            strBuilder.Append("<TD BORDER=1 BGCOLOR = 'GRAY' BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & CDec(objDataReader.Compute("SUM([" & objDataReader.Columns(k).ColumnName & "])", "[EmploymentType]= '" & mstrEmploymentType & "'")) & "</B></FONT></TD>" & vbCrLf)
                        ElseIf objDataReader.Columns(k).ColumnName = "AvgCost" Then
                            strBuilder.Append("<TD BORDER=1 BGCOLOR = 'GRAY' BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Math.Round(CDec(objDataReader.Compute("SUM([Total_amount])", "[EmploymentType]= '" & mstrEmploymentType & "'")) / CDec(objDataReader.Compute("SUM([Headcount])", "[EmploymentType]= '" & mstrEmploymentType & "'")), 2) & "</B></FONT></TD>" & vbCrLf)
                        End If
                    Else
                        strBuilder.Append("<TD BORDER=1 BGCOLOR = 'GRAY' BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & "" & "</B></FONT></TD>" & vbCrLf)
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
                strBuilder.Append(" </TABLE> " & vbCrLf)
            End If

            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)

            Dim dcColumns As DataColumnCollection = objDataReader.Columns
            Dim mintIndexCategoryStartIndex As Integer = dcColumns.IndexOf("Headcount")
            Dim mintIndexCategoryEndIndex As Integer = 0
            If mdctMaritalStatus.Count > 0 Then
                mintIndexCategoryEndIndex = dcColumns.IndexOf("Total_Amount")
            Else
                mintIndexCategoryEndIndex = objDataReader.Columns.Count - 1
            End If

            Dim intCount As Integer = 0  'USED FOR CHANGING COLUMN INDEX IN LOOP

            For i As Integer = 0 To 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For j As Integer = 0 To mintIndexCategoryEndIndex - 1
                    If j = mintIndexCategoryStartIndex - 1 Then
                        If i = 0 Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 10, "Unit Cost USD") & "</B></FONT></TD>" & vbCrLf)
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 16, "Total USD") & "</B></FONT></TD>" & vbCrLf)
                        End If
                    ElseIf j = mintIndexCategoryEndIndex - (mdctProviderMtlStatus.Count + 1) AndAlso i = 0 Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK BGCOLOR = 'GRAY' ALIGN='CENTER' COLSPAN = '3'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Total") & "</B></FONT></TD>" & vbCrLf)
                        Exit For
                    Else
                        If j = mintIndexCategoryStartIndex AndAlso mdctProviderMtlStatus.Count > 0 Then
                            Dim intKeyCount As Integer = 0   'USED FOR COUNTING KEY IN LOOP

                            Dim mdctMaritalStatusTotal As New Dictionary(Of Integer, String)
                            For Each Key As Integer In mdctProviderMtlStatus.Keys
                                Dim arAmount() As String = mdctProviderMtlStatus(Key).Split("|")
                                If arAmount.Length > 0 Then
                                    If i = 0 Then  'FOR FIRST ROW
                                        intKeyCount += 1
                                        For p As Integer = 0 To arAmount.Length - 1
                                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ><FONT SIZE=2><B>" & Format(CDec(arAmount(p)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                                            j += 1
                                        Next
                                        If intKeyCount <> mdctProviderMtlStatus.Count Then
                                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                                        End If
                                    ElseIf i = 1 Then   'FOR SECOND ROW
                                        intKeyCount += 1
                                        Dim mdecTotal As Decimal = 0
                                        For p As Integer = 0 To arAmount.Length - 1
                                            Dim intHeadCount As Integer = 0
                                            If objDataReader.Columns(mintIndexCategoryStartIndex + intCount + 1).Caption = Language.getMessage(mstrModuleName, 4, "Total") Then
                                                intCount += 1
                                            End If
                                            intHeadCount = IIf(IsDBNull(objDataReader.Compute("SUM([" & objDataReader.Columns(mintIndexCategoryStartIndex + intCount + 1).ColumnName & "])", "1=1")), 0, objDataReader.Compute("SUM([" & objDataReader.Columns(mintIndexCategoryStartIndex + intCount + 1).ColumnName & "])", "1=1"))
                                            mdecTotal += CDec(intHeadCount * CDec(arAmount(p)))
                                            j += 1
                                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ><FONT SIZE=2><B>" & Format(CDec(intHeadCount * CDec(arAmount(p))), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)

                                            Dim mintCategoryID As Integer = CInt(objDataReader.Columns(mintIndexCategoryStartIndex + intCount + 1).ColumnName.ToString().Substring(objDataReader.Columns(mintIndexCategoryStartIndex + intCount + 1).ColumnName.IndexOf("_") + 1))
                                            Dim objmdCategory_mst As New clsmedical_category_master
                                            Dim dsList As DataSet = objmdCategory_mst.GetList("List", True, mintCategoryID)
                                            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                                objmdCategory_mst._Medicalcategoryunkid = CInt(dsList.Tables(0).Rows(0)("medicalcategoryunkid"))
                                            End If
                                            If mdctMaritalStatusTotal.ContainsKey(objmdCategory_mst._MaritalStatusunkid) = False Then
                                                mdctMaritalStatusTotal.Add(objmdCategory_mst._MaritalStatusunkid, CDec(intHeadCount * CDec(arAmount(p))))
                                            Else
                                                mdctMaritalStatusTotal(objmdCategory_mst._MaritalStatusunkid) &= "|" & CDec(intHeadCount * CDec(arAmount(p)))
                                            End If
                                            objmdCategory_mst = Nothing

                                            If p = arAmount.Length - 1 Then
                                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'  BGCOLOR = 'GRAY'><FONT SIZE=2><B> " & Format(mdecTotal, GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                                                If mdctMaritalStatusTotal.ContainsKey(-1) = False Then
                                                    mdctMaritalStatusTotal.Add(-1, CDec(mdecTotal))
                                                Else
                                                    mdctMaritalStatusTotal(-1) &= "|" & CDec(mdecTotal)
                                                End If
                                            End If
                                            intCount += 1
                                        Next

                                        If intKeyCount = mdctProviderMtlStatus.Count Then
                                            If mdctMaritalStatusTotal.Count > 0 Then
                                                j += (mdctProviderMtlStatus.Count - 1)
                                                For Each KeysID As Integer In mdctMaritalStatusTotal.Keys
                                                    Dim arTotal() As String = mdctMaritalStatusTotal(KeysID).Split("|")
                                                    If arTotal.Length > 0 Then
                                                        Dim mdecFinalTotalAmount As Decimal = 0
                                                        For m As Integer = 0 To arTotal.Length - 1
                                                            mdecFinalTotalAmount += CDec(arTotal(m))
                                                        Next
                                                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'  BGCOLOR = #F5CCB0><FONT SIZE=2><B> " & Format(mdecFinalTotalAmount, GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                                                        j += 1
                                                    End If
                                                Next
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                        End If
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)

            For P As Integer = 0 To 6
                strBuilder.Append(" <TR> " & vbCrLf)
                If P = 0 Then
                    For k As Integer = 0 To 4
                        If k = 0 Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN='3' COLSPAN = '3'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 13, "Compiled By : ") & "</B></FONT></TD>" & vbCrLf)
                        ElseIf k = 1 Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN='3' COLSPAN = '3'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 14, "Reviewed By : ") & "</B></FONT></TD>" & vbCrLf)
                        ElseIf k > 1 AndAlso k <= 3 Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN='3' COLSPAN = '3'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 15, "Approved By : ") & "</B></FONT></TD>" & vbCrLf)
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN='3' COLSPAN = '2'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 15, "Approved By : ") & "</B></FONT></TD>" & vbCrLf)
                        End If
                    Next
                ElseIf P > 2 Then
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'  COLSPAN = '3'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'  COLSPAN = '3'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN = '3'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN = '3'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'  COLSPAN = '2'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                End If
                strBuilder.Append(" </TR> " & vbCrLf)
            Next
            strBuilder.Append(" </TABLE> ")


            strBuilder.Append(" </HTML> " & vbCrLf)

            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period:")
            Language.setMessage(mstrModuleName, 2, "Station:")
            Language.setMessage(mstrModuleName, 3, "Employment Type:")
            Language.setMessage(mstrModuleName, 4, "Total")
            Language.setMessage(mstrModuleName, 5, "Date :")
            Language.setMessage(mstrModuleName, 6, "Report successfully exported to Report export path.")
            Language.setMessage(mstrModuleName, 7, "Prepared By :")
            Language.setMessage(mstrModuleName, 8, "Average cost per EE")
            Language.setMessage(mstrModuleName, 9, "Comments")
            Language.setMessage(mstrModuleName, 10, "Unit Cost USD")
            Language.setMessage(mstrModuleName, 11, "Total")
            Language.setMessage(mstrModuleName, 12, "Service Provider:")
            Language.setMessage(mstrModuleName, 13, "Compiled By :")
            Language.setMessage(mstrModuleName, 14, "Reviewed By :")
            Language.setMessage(mstrModuleName, 15, "Approved By :")
            Language.setMessage(mstrModuleName, 16, "Total USD")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
End Class
