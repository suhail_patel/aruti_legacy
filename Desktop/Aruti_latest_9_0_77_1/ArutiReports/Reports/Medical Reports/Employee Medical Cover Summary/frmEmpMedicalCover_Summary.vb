Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports


Public Class frmEmpMedicalCover_Summary

#Region "Private Variables"

    Private mstrModuleName As String = "frmMdAIDManagement_Summary"
    Dim objMdAIDSummary As clsEmpMedicalCover_Summary
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region "Constructor"

    Public Sub New()
        objMdAIDSummary = New clsEmpMedicalCover_Summary(User._Object._Languageunkid,Company._Object._Companyunkid)
        objMdAIDSummary.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objPeriod = Nothing

            Dim objCommonMst As New clsCommon_Master
            dsList = objCommonMst.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "List")
            With cboEmploymentType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objCommonMst = Nothing

            Dim objStation As New clsStation
            dsList = objStation.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objStation = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FIllMappedServiceProvider()
        Try
            Dim objProvider As New clscategory_providermapping
            Dim dtProvider As DataTable = objProvider.GetDistinctProviderFromMapping()
            lvServiceProvider.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dtProvider.Rows
                lvItem = New ListViewItem
                lvItem.Text = dtRow.Item("Provider").ToString
                lvItem.Tag = CInt(dtRow.Item("providerunkid").ToString)
                lvServiceProvider.Items.Add(lvItem)
            Next
            lvServiceProvider.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FIllMappedServiceProvider", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboBranch.SelectedValue = 0
            cboEmploymentType.SelectedValue = 0
            chkSelectAll.Checked = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False

            ElseIf lvServiceProvider.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Service Provider is compulsory information.Please checked atleast one Service Provider."), enMsgBoxStyle.Information)
                lvServiceProvider.Select()
                Return False

            End If



            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End

            objMdAIDSummary._PeriodID = CInt(cboPeriod.SelectedValue)
            objMdAIDSummary._Period = cboPeriod.Text

            objMdAIDSummary._StartDate = objPeriod._Start_Date.Date
            objMdAIDSummary._EndDate = objPeriod._End_Date.Date
            objMdAIDSummary._EmploymentTypeID = CInt(cboEmploymentType.SelectedValue)
            If CInt(cboEmploymentType.SelectedValue) > 0 Then objMdAIDSummary._EmploymentType = cboEmploymentType.Text
            objMdAIDSummary._StationID = CInt(cboBranch.SelectedValue)
            If CInt(cboBranch.SelectedValue) > 0 Then objMdAIDSummary._Station = cboBranch.Text

            Dim selectedTags As List(Of String) = lvServiceProvider.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToList()
            objMdAIDSummary._ServiceProviderIDs = String.Join(",", selectedTags.ToArray)

            Dim selectedName As List(Of String) = lvServiceProvider.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Text.ToString).ToList()
            objMdAIDSummary._ServiceProviderNames = String.Join(",", selectedName.ToArray)


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objMdAIDSummary.Generate_DetailReport()
            objMdAIDSummary.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                         , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                         , ConfigParameter._Object._IsIncludeInactiveEmp)
            'Pinkal (24-Aug-2015) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmMdAIDManagement_Summary_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objMdAIDSummary = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMdAIDManagement_Summary_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmMdAIDManagement_Summary_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            eZeeHeader.Title = objMdAIDSummary._ReportName
            eZeeHeader.Message = objMdAIDSummary._ReportDesc
            Call FillCombo()
            FIllMappedServiceProvider()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMdAIDManagement_Summary_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMdAIDManagement_Summary_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    'Call frmLvForm_ApprovalStatus_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMdAIDManagement_Summary_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMdAIDManagement_Summary_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMdAIDManagement_Summary_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            clsEmpMedicalCover_Summary.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpMedicalCover_Summary"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()
        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboPeriod.DataSource, DataTable)
            With cboPeriod
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchEmploymentType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmploymentType.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmploymentType.DataSource, DataTable)
            With cboEmploymentType
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmploymentType_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBranch.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboBranch
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBranch_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            For i As Integer = 0 To lvServiceProvider.Items.Count - 1
                RemoveHandler lvServiceProvider.ItemChecked, AddressOf lvServiceProvider_ItemChecked
                lvServiceProvider.Items(i).Checked = chkSelectAll.Checked
                AddHandler lvServiceProvider.ItemChecked, AddressOf lvServiceProvider_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Listview Event"

    Private Sub lvServiceProvider_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvServiceProvider.ItemChecked
        Try
            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            If lvServiceProvider.CheckedItems.Count <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked

            ElseIf lvServiceProvider.CheckedItems.Count < lvServiceProvider.Items.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate

            ElseIf lvServiceProvider.CheckedItems.Count = lvServiceProvider.Items.Count Then
                chkSelectAll.CheckState = CheckState.Checked

            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvServiceProvider_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor



            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.LblEmploymentType.Text = Language._Object.getCaption(Me.LblEmploymentType.Name, Me.LblEmploymentType.Text)
            Me.LblBranch.Text = Language._Object.getCaption(Me.LblBranch.Name, Me.LblBranch.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.LblServiceProcvider.Text = Language._Object.getCaption(Me.LblServiceProcvider.Name, Me.LblServiceProcvider.Text)
            Me.ColumnHeader2.Text = Language._Object.getCaption(CStr(Me.ColumnHeader2.Tag), Me.ColumnHeader2.Text)
            Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 2, "Service Provider is compulsory information.Please checked atleast one Service Provider.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
