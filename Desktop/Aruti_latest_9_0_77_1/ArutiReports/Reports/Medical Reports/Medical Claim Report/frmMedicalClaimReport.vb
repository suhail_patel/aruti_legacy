'************************************************************************************************************************************
'Class Name : frmMedicalClaimReport.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMedicalClaimReport

    Private mstrModuleName As String = "frmMedicalClaimReport"
    Private objMedicalClaim As clsMedicalClaimReport
    Private strPeriodId As String = ""
    Private strPeriodName As String = ""
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#Region "Constructor"

    Public Sub New()
        objMedicalClaim = New clsMedicalClaimReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objMedicalClaim.SetDefaultValue()
        InitializeComponent()
        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End
    End Sub

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try
            Dim objProvider As New clsinstitute_master


            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes
            'dsList = objProvider.getListForCombo(True, "List", True)
            dsList = objProvider.getListForCombo(True, "List", True, 1)
            'Pinkal (5-MAY-2012) -- End

            With cboProvider
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub FillPeriods()
        Dim dsList As DataSet = Nothing
        Try
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period")
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period")
            'Sohail (21 Aug 2015) -- End

            Dim lvItem As ListViewItem

            lvPeriod.Items.Clear()

            For Each dr As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Tag = dr("periodunkid").ToString()
                lvItem.Text = dr("name").ToString()
                lvPeriod.Items.Add(lvItem)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPeriods", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            objMedicalClaim.setDefaultOrderBy(0)
            txtOrderBy.Text = objMedicalClaim.OrderByDisplay
            cboProvider.SelectedIndex = 0
            chkSelectallPeriodList.Checked = False
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1


            'Pinkal (03-Jun-2012) -- Start
            'Enhancement : TRA Changes
            chkMyInvoice.Checked = True
            'Pinkal (03-Jun-2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End

            'Pinkal (14-Sep-2015) -- Start
            'Enhancement - Bug Solved for 58.1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Pinkal (14-Sep-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            objMedicalClaim.SetDefaultValue()

            objMedicalClaim._ProviderId = CInt(cboProvider.SelectedValue)
            objMedicalClaim._ProviderName = cboProvider.Text

            objMedicalClaim._PeriodIds = strPeriodId
            objMedicalClaim._PeriodName = strPeriodName

            objMedicalClaim._ViewByIds = mstrStringIds
            objMedicalClaim._ViewIndex = mintViewIdx
            objMedicalClaim._ViewByName = mstrStringName

            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            objMedicalClaim._IncludeInactiveEmp = chkInactiveemp.Checked
            'Pinkal (11-MAY-2012) -- End

            'Pinkal (15-MAY-2012) -- Start
            'Enhancement : TRA Changes
            objMedicalClaim._Analysis_Fields = mstrAnalysis_Fields
            objMedicalClaim._Analysis_Join = mstrAnalysis_Join
            objMedicalClaim._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMedicalClaim._Report_GroupName = mstrReport_GroupName
            'Pinkal (15-MAY-2012) -- End


            'Pinkal (03-Jun-2012) -- Start
            'Enhancement : TRA Changes
            objMedicalClaim._MyInvoiceOnly = chkMyInvoice.Checked
            'Pinkal (03-Jun-2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objMedicalClaim._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            objMedicalClaim._AsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
            'Sohail (18 May 2019) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

#End Region

#Region "Form Events"

    Private Sub frmMedicalClaimReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objMedicalClaim._ReportName
            Me._Message = objMedicalClaim._ReportDesc
            Call FillCombo()
            FillPeriods()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalClaimReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalClaimReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalClaimReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub frmMedicalClaimReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objMedicalClaim = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalClaimReport_FormClosed", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub frmMedicalClaimReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        strPeriodId = ""
        strPeriodName = ""
        Try
            If CInt(cboProvider.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select atleast one Provider."), enMsgBoxStyle.Information)
                cboProvider.Select()
                Exit Sub
            ElseIf lvPeriod.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select atleast one Period."), enMsgBoxStyle.Information)
                lvPeriod.Select()
                Exit Sub
            End If

            If lvPeriod.CheckedItems.Count > 0 Then
                For j As Integer = 0 To lvPeriod.CheckedItems.Count - 1
                    strPeriodId &= lvPeriod.CheckedItems(j).Tag.ToString & ","
                    strPeriodName &= lvPeriod.CheckedItems(j).Text() & ","
                Next
                strPeriodId = strPeriodId.Substring(0, strPeriodId.Length - 1)
                strPeriodName = strPeriodName.Substring(0, strPeriodName.Length - 1)
            End If

            If SetFilter() = False Then Exit Sub


            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objMedicalClaim.generateReport(0, e.Type, enExportAction.None)
            objMedicalClaim.generateReportNew(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, _
                                               True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Anjan [08 September 2015] -- End




        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalClaimReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalClaimReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        strPeriodId = ""
        strPeriodName = ""
        Try
            If CInt(cboProvider.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select atleast one Provider."), enMsgBoxStyle.Information)
                cboProvider.Select()
                Exit Sub
            ElseIf lvPeriod.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select atleast one Period."), enMsgBoxStyle.Information)
                lvPeriod.Select()
                Exit Sub
            End If

            If lvPeriod.CheckedItems.Count > 0 Then
                For j As Integer = 0 To lvPeriod.CheckedItems.Count - 1
                    strPeriodId &= "" & lvPeriod.CheckedItems(j).Tag.ToString & ","
                    strPeriodName &= lvPeriod.CheckedItems(j).Text.ToString() & ","
                Next
                strPeriodId = strPeriodId.Substring(0, strPeriodId.Length - 1)
                strPeriodName = strPeriodName.Substring(0, strPeriodName.Length - 1)
            End If
            If SetFilter() = False Then Exit Sub


            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objMedicalClaim.generateReport(0, enPrintAction.None, e.Type)
            objMedicalClaim.generateReportNew(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, _
                                               True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Anjan [08 September 2015] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalClaimReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalClaimReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalClaimReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalClaimReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalClaimReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub


    'Pinkal (5-MAY-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchProvider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchProvider.Click
        Try
            Dim objfrm As New frmCommonSearch
            With objfrm
                .ValueMember = cboProvider.ValueMember
                .DisplayMember = cboProvider.DisplayMember
                .DataSource = CType(cboProvider.DataSource, DataTable)
            End With

            If objfrm.DisplayDialog Then
                cboProvider.SelectedValue = objfrm.SelectedValue
                cboProvider.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchProvider_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (5-MAY-2012) -- End


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub frmMedicalClaimReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMedicalClaimReport.SetMessages()
            objfrm._Other_ModuleNames = "clsMedicalClaimReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmMedicalClaimReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End


#End Region

#Region "CheckBox Event"

    Private Sub chkSelectallPeriodList_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectallPeriodList.CheckedChanged
        Try
            If lvPeriod.Items.Count <= 0 Then Exit Sub
            For Each lvItem As ListViewItem In lvPeriod.Items
                RemoveHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
                lvItem.Checked = chkSelectallPeriodList.Checked
                AddHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectallPeriodList_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvPeriod_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriod.ItemChecked
        Try
            RemoveHandler chkSelectallPeriodList.CheckedChanged, AddressOf chkSelectallPeriodList_CheckedChanged
            If lvPeriod.CheckedItems.Count <= 0 Then
                chkSelectallPeriodList.CheckState = CheckState.Unchecked
            ElseIf lvPeriod.CheckedItems.Count < lvPeriod.Items.Count Then
                chkSelectallPeriodList.CheckState = CheckState.Indeterminate
            ElseIf lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
                chkSelectallPeriodList.CheckState = CheckState.Checked
            End If
            AddHandler chkSelectallPeriodList.CheckedChanged, AddressOf chkSelectallPeriodList_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            'Pinkal (15-MAY-2012) -- Start
            'Enhancement : TRA Changes
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Pinkal (15-MAY-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "Control"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objMedicalClaim.setOrderBy(0)
            txtOrderBy.Text = objMedicalClaim.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblProvider.Text = Language._Object.getCaption(Me.lblProvider.Name, Me.lblProvider.Text)
			Me.chkSelectallPeriodList.Text = Language._Object.getCaption(Me.chkSelectallPeriodList.Name, Me.chkSelectallPeriodList.Text)
			Me.lblPeriods.Text = Language._Object.getCaption(Me.lblPeriods.Name, Me.lblPeriods.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.chkMyInvoice.Text = Language._Object.getCaption(Me.chkMyInvoice.Name, Me.chkMyInvoice.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select atleast one Provider.")
			Language.setMessage(mstrModuleName, 2, "Please Select atleast one Period.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
