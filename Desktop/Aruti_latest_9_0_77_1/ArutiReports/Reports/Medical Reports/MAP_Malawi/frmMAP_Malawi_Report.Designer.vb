﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMAP_Malawi_Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblDependantTo = New System.Windows.Forms.Label
        Me.lblDependantFrom = New System.Windows.Forms.Label
        Me.lblCatAmtTo = New System.Windows.Forms.Label
        Me.lblCatAmtFrom = New System.Windows.Forms.Label
        Me.txtDepTo = New eZee.TextBox.IntegerTextBox
        Me.txtDepFrom = New eZee.TextBox.IntegerTextBox
        Me.txtAmountTo = New eZee.TextBox.NumericTextBox
        Me.txtAmountFrom = New eZee.TextBox.NumericTextBox
        Me.lblMedCategory = New System.Windows.Forms.Label
        Me.cboMedCategory = New System.Windows.Forms.ComboBox
        Me.lblEmplType = New System.Windows.Forms.Label
        Me.cboEmplType = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbMandatoryInfo.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 472)
        Me.NavPanel.Size = New System.Drawing.Size(754, 55)
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.chkInactiveemp)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.lblDependantTo)
        Me.gbMandatoryInfo.Controls.Add(Me.lblDependantFrom)
        Me.gbMandatoryInfo.Controls.Add(Me.lblCatAmtTo)
        Me.gbMandatoryInfo.Controls.Add(Me.lblCatAmtFrom)
        Me.gbMandatoryInfo.Controls.Add(Me.txtDepTo)
        Me.gbMandatoryInfo.Controls.Add(Me.txtDepFrom)
        Me.gbMandatoryInfo.Controls.Add(Me.txtAmountTo)
        Me.gbMandatoryInfo.Controls.Add(Me.txtAmountFrom)
        Me.gbMandatoryInfo.Controls.Add(Me.lblMedCategory)
        Me.gbMandatoryInfo.Controls.Add(Me.cboMedCategory)
        Me.gbMandatoryInfo.Controls.Add(Me.lblEmplType)
        Me.gbMandatoryInfo.Controls.Add(Me.cboEmplType)
        Me.gbMandatoryInfo.Controls.Add(Me.lblEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.cboEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.lblMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(395, 192)
        Me.gbMandatoryInfo.TabIndex = 69
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(110, 168)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(246, 17)
        Me.chkInactiveemp.TabIndex = 204
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(362, 87)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 105
        '
        'lblDependantTo
        '
        Me.lblDependantTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDependantTo.Location = New System.Drawing.Point(228, 316)
        Me.lblDependantTo.Name = "lblDependantTo"
        Me.lblDependantTo.Size = New System.Drawing.Size(95, 15)
        Me.lblDependantTo.TabIndex = 103
        Me.lblDependantTo.Text = "Dep. To"
        Me.lblDependantTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDependantFrom
        '
        Me.lblDependantFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDependantFrom.Location = New System.Drawing.Point(9, 316)
        Me.lblDependantFrom.Name = "lblDependantFrom"
        Me.lblDependantFrom.Size = New System.Drawing.Size(85, 15)
        Me.lblDependantFrom.TabIndex = 102
        Me.lblDependantFrom.Text = "Dep. From"
        Me.lblDependantFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCatAmtTo
        '
        Me.lblCatAmtTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCatAmtTo.Location = New System.Drawing.Point(228, 289)
        Me.lblCatAmtTo.Name = "lblCatAmtTo"
        Me.lblCatAmtTo.Size = New System.Drawing.Size(95, 15)
        Me.lblCatAmtTo.TabIndex = 101
        Me.lblCatAmtTo.Text = "Cat. Amt. To"
        Me.lblCatAmtTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCatAmtFrom
        '
        Me.lblCatAmtFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCatAmtFrom.Location = New System.Drawing.Point(9, 289)
        Me.lblCatAmtFrom.Name = "lblCatAmtFrom"
        Me.lblCatAmtFrom.Size = New System.Drawing.Size(85, 15)
        Me.lblCatAmtFrom.TabIndex = 100
        Me.lblCatAmtFrom.Text = "Cat. Amt. From"
        Me.lblCatAmtFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDepTo
        '
        Me.txtDepTo.AllowNegative = True
        Me.txtDepTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDepTo.DigitsInGroup = 0
        Me.txtDepTo.Flags = 0
        Me.txtDepTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepTo.Location = New System.Drawing.Point(329, 313)
        Me.txtDepTo.MaxDecimalPlaces = 0
        Me.txtDepTo.MaxWholeDigits = 9
        Me.txtDepTo.Name = "txtDepTo"
        Me.txtDepTo.Prefix = ""
        Me.txtDepTo.RangeMax = 1.7976931348623157E+308
        Me.txtDepTo.RangeMin = -1.7976931348623157E+308
        Me.txtDepTo.Size = New System.Drawing.Size(122, 21)
        Me.txtDepTo.TabIndex = 99
        Me.txtDepTo.Text = "0"
        Me.txtDepTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDepFrom
        '
        Me.txtDepFrom.AllowNegative = True
        Me.txtDepFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDepFrom.DigitsInGroup = 0
        Me.txtDepFrom.Flags = 0
        Me.txtDepFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepFrom.Location = New System.Drawing.Point(100, 313)
        Me.txtDepFrom.MaxDecimalPlaces = 0
        Me.txtDepFrom.MaxWholeDigits = 9
        Me.txtDepFrom.Name = "txtDepFrom"
        Me.txtDepFrom.Prefix = ""
        Me.txtDepFrom.RangeMax = 1.7976931348623157E+308
        Me.txtDepFrom.RangeMin = -1.7976931348623157E+308
        Me.txtDepFrom.Size = New System.Drawing.Size(122, 21)
        Me.txtDepFrom.TabIndex = 98
        Me.txtDepFrom.Text = "0"
        Me.txtDepFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAmountTo
        '
        Me.txtAmountTo.AllowNegative = True
        Me.txtAmountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmountTo.DigitsInGroup = 0
        Me.txtAmountTo.Flags = 0
        Me.txtAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountTo.Location = New System.Drawing.Point(329, 286)
        Me.txtAmountTo.MaxDecimalPlaces = 4
        Me.txtAmountTo.MaxWholeDigits = 9
        Me.txtAmountTo.Name = "txtAmountTo"
        Me.txtAmountTo.Prefix = ""
        Me.txtAmountTo.RangeMax = 1.7976931348623157E+308
        Me.txtAmountTo.RangeMin = -1.7976931348623157E+308
        Me.txtAmountTo.Size = New System.Drawing.Size(122, 21)
        Me.txtAmountTo.TabIndex = 97
        Me.txtAmountTo.Text = "0"
        Me.txtAmountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAmountFrom
        '
        Me.txtAmountFrom.AllowNegative = True
        Me.txtAmountFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmountFrom.DigitsInGroup = 0
        Me.txtAmountFrom.Flags = 0
        Me.txtAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountFrom.Location = New System.Drawing.Point(100, 286)
        Me.txtAmountFrom.MaxDecimalPlaces = 4
        Me.txtAmountFrom.MaxWholeDigits = 9
        Me.txtAmountFrom.Name = "txtAmountFrom"
        Me.txtAmountFrom.Prefix = ""
        Me.txtAmountFrom.RangeMax = 1.7976931348623157E+308
        Me.txtAmountFrom.RangeMin = -1.7976931348623157E+308
        Me.txtAmountFrom.Size = New System.Drawing.Size(122, 21)
        Me.txtAmountFrom.TabIndex = 70
        Me.txtAmountFrom.Text = "0"
        Me.txtAmountFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMedCategory
        '
        Me.lblMedCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedCategory.Location = New System.Drawing.Point(9, 143)
        Me.lblMedCategory.Name = "lblMedCategory"
        Me.lblMedCategory.Size = New System.Drawing.Size(95, 17)
        Me.lblMedCategory.TabIndex = 96
        Me.lblMedCategory.Text = "Medical Category"
        Me.lblMedCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMedCategory
        '
        Me.cboMedCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMedCategory.DropDownWidth = 250
        Me.cboMedCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMedCategory.FormattingEnabled = True
        Me.cboMedCategory.Location = New System.Drawing.Point(110, 141)
        Me.cboMedCategory.Name = "cboMedCategory"
        Me.cboMedCategory.Size = New System.Drawing.Size(246, 21)
        Me.cboMedCategory.TabIndex = 95
        '
        'lblEmplType
        '
        Me.lblEmplType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmplType.Location = New System.Drawing.Point(9, 116)
        Me.lblEmplType.Name = "lblEmplType"
        Me.lblEmplType.Size = New System.Drawing.Size(95, 17)
        Me.lblEmplType.TabIndex = 93
        Me.lblEmplType.Text = "Employment Type"
        Me.lblEmplType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmplType
        '
        Me.cboEmplType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmplType.DropDownWidth = 250
        Me.cboEmplType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmplType.FormattingEnabled = True
        Me.cboEmplType.Location = New System.Drawing.Point(110, 114)
        Me.cboEmplType.Name = "cboEmplType"
        Me.cboEmplType.Size = New System.Drawing.Size(246, 21)
        Me.cboEmplType.TabIndex = 94
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(9, 89)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(95, 17)
        Me.lblEmployee.TabIndex = 92
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 250
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(110, 87)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(246, 21)
        Me.cboEmployee.TabIndex = 91
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(297, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 69
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(9, 62)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(95, 17)
        Me.lblMembership.TabIndex = 64
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 250
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(110, 60)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(246, 21)
        Me.cboMembership.TabIndex = 65
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(9, 35)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(95, 17)
        Me.lblPeriod.TabIndex = 57
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 180
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(110, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(246, 21)
        Me.cboPeriod.TabIndex = 58
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 264)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(395, 63)
        Me.gbSortBy.TabIndex = 70
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(362, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(9, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(85, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(110, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(246, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'frmMAP_Malawi_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(754, 527)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Name = "frmMAP_Malawi_Report"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Monthly Advance Payment"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbMandatoryInfo, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbMandatoryInfo.PerformLayout()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblMedCategory As System.Windows.Forms.Label
    Friend WithEvents cboMedCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmplType As System.Windows.Forms.Label
    Friend WithEvents cboEmplType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDependantTo As System.Windows.Forms.Label
    Friend WithEvents lblDependantFrom As System.Windows.Forms.Label
    Friend WithEvents lblCatAmtTo As System.Windows.Forms.Label
    Friend WithEvents lblCatAmtFrom As System.Windows.Forms.Label
    Friend WithEvents txtAmountTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtAmountFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents txtDepTo As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtDepFrom As eZee.TextBox.IntegerTextBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
End Class
