#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmpMedicalCover_Detail_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmpMedicalCover_Dtetail_Report"
    Private objMedicalCoverDetail As clsEmpMedicalCover_DetailReport
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region "Constructor"

    Public Sub New()
        objMedicalCoverDetail = New clsEmpMedicalCover_DetailReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objMedicalCoverDetail.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objPeriod = Nothing

            Dim objMData As New clsMasterData

            'Pinkal (04-Jan-2020) -- Start
            'Enhancement - FINCA DRC [0003074] - Medical cover detail report Enhancement.
            'dsList = objMData.GetEAllocation_Notification("List")
            dsList = objMData.GetEAllocation_Notification("List", "", False, True)
            'Pinkal (04-Jan-2020) -- End

            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable

                'Pinkal (04-Jan-2020) -- Start
                'Enhancement - FINCA DRC [0003074] - Medical cover detail report Enhancement.
                '.SelectedValue = 1
                .SelectedValue = 0
                'Pinkal (04-Jan-2020) -- End
            End With
            objMData = Nothing

            Dim objCMaster As New clsCommon_Master
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "List")
            With cboSpouse
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
            End With
            With cboChild
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FIllServiceProvider()
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvServiceProvider.ItemChecked, AddressOf lvServiceProvider_ItemChecked

            Dim objProvider As New clscategory_providermapping
            Dim dtProvider As DataTable = objProvider.GetDistinctProviderFromMapping()
            lvServiceProvider.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dtProvider.Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("Provider").ToString)
                lvItem.Tag = CInt(dtRow.Item("providerunkid").ToString)
                lvItem.Checked = True
                lvServiceProvider.Items.Add(lvItem)
            Next
            objchkAll.CheckState = CheckState.Checked
            lvServiceProvider.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvServiceProvider.ItemChecked, AddressOf lvServiceProvider_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FIllServiceProvider", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try

            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'cboAllocations.SelectedValue = enAllocation.BRANCH
            cboAllocations.SelectedValue = 0
            'Pinkal (03-Jan-2020) -- End
            cboPeriod.SelectedValue = 0
            Call FIllServiceProvider()
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case cboAllocations.SelectedValue
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
                Case Else
                    Call Fill_List(Nothing, "", "")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            RemoveHandler objchkAllocAll.CheckedChanged, AddressOf objchkAllocAll_CheckedChanged


            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            lvAllocation.Items.Clear()
            If dTable Is Nothing Then Exit Sub
            'Pinkal (03-Jan-2020) -- End

            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item(StrDisColName).ToString)
                lvItem.Tag = dtRow.Item(StrIdColName)
                lvItem.Checked = True
                lvAllocation.Items.Add(lvItem)
            Next
            objchkAllocAll.CheckState = CheckState.Checked
            If lvAllocation.Items.Count > 8 Then
                objcolhAllocation.Width = 200 - 20
            Else
                objcolhAllocation.Width = 200
            End If
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            AddHandler objchkAllocAll.CheckedChanged, AddressOf objchkAllocAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Employee_Medical_Cover_Detail)
            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))
                        Case 1  'SPOUSE
                            cboSpouse.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                        Case 2  'CHILD
                            cboChild.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objMedicalCoverDetail.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Period is mandatory information.Please Select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False
            End If


            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'If lvAllocation.CheckedItems.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Allocation is mandatory information. Please check atleast one Allocation in order to generate report."), enMsgBoxStyle.Information)
            '    lvAllocation.Select()
            '    Return False
            'End If
            'Pinkal (03-Jan-2020) -- End

            If CInt(cboSpouse.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Spouse is mandatory information. Please select Spouse to continue."), enMsgBoxStyle.Information)
                cboSpouse.Select()
                Return False
            End If

            If CInt(cboChild.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Child is mandatory information. Please select Child to continue."), enMsgBoxStyle.Information)
                cboChild.Select()
                Return False
            End If

            If lvServiceProvider.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Service Provider is mandatory information. Please check atleast one Service Provider in order to generate report"), enMsgBoxStyle.Information)
                lvServiceProvider.Select()
                Return False
            End If

            objMedicalCoverDetail._ChildRelationId = CInt(cboChild.SelectedValue)
            objMedicalCoverDetail._ChildRelationName = cboChild.Text
            objMedicalCoverDetail._IncludeInactiveEmp = chkInactiveemp.Checked
            objMedicalCoverDetail._PeriodId = CInt(cboPeriod.SelectedValue)
            objMedicalCoverDetail._PeriodName = cboPeriod.Text
            objMedicalCoverDetail._ServiceProviderIDs = String.Join(",", lvServiceProvider.CheckedItems.Cast(Of ListViewItem).Select(Function(x) x.Tag.ToString()).ToArray())
            objMedicalCoverDetail._SpouseRelationId = CInt(cboSpouse.SelectedValue)
            objMedicalCoverDetail._SpouseRelationName = cboSpouse.Text
            objMedicalCoverDetail._ViewAllocationIds = String.Join(",", lvAllocation.CheckedItems.Cast(Of ListViewItem).Select(Function(x) x.Tag.ToString()).ToArray())
            objMedicalCoverDetail._ViewAllocationRefId = CInt(cboAllocations.SelectedValue)
            objMedicalCoverDetail._ViewAllocationRefName = cboAllocations.Text

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmEmpMedicalCover_Dtetail_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpMedicalCover_Dtetail_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpMedicalCover_Dtetail_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            'Me._Title = objBillsummary._ReportName
            'Me._Message = objBillsummary._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpMedicalCover_Dtetail_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpMedicalCover_Dtetail_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmpMedicalCover_Dtetail_Report_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmpMedicalCover_DetailReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpMedicalCover_DetailReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            'Gajanan [31-JUL-2019] -- START
            'Defect  [0003054 : FINCA DRC] : Medical cover detail report does not filter by service provider.


            'If objMedicalCoverDetail.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
            '                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
            '                                                                    , ConfigParameter._Object._IsIncludeInactiveEmp) = False Then
            If objMedicalCoverDetail.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                , chkInactiveemp.Checked) = False Then

                'Gajanan [31-JUL-2019] -- END


                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Problem in exporting report."), enMsgBoxStyle.Information)
                Exit Sub
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Report exported successfully to the given path."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchSpouse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSpouse.Click, objbtnSearchChild.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim xCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchSpouse.Name.ToUpper
                    xCbo = cboSpouse
                Case objbtnSearchChild.Name.ToUpper
                    xCbo = cboChild
            End Select
            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                .DataSource = xCbo.DataSource
                If .DisplayDialog Then
                    xCbo.SelectedValue = .SelectedValue
                    xCbo.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSpouse_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            objcolhAllocation.Text = cboAllocations.Text
            'Pinkal (04-Jan-2020) -- Start
            'Enhancement - FINCA DRC [0003074] - Medical cover detail report Enhancement.
            objchkAllocAll.Checked = False
            'Pinkal (04-Jan-2020) -- End
            Call Fill_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSpouse_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSpouse.SelectedIndexChanged
        Try
            If CInt(cboSpouse.SelectedValue) > 0 Then
                If CInt(cboChild.SelectedValue) = CInt(cboSpouse.SelectedValue) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, this selected relation is already used. Please select other relation."), enMsgBoxStyle.Information)
                    cboSpouse.SelectedValue = 0
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSpouse_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboChild_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboChild.SelectedIndexChanged
        Try
            If CInt(cboChild.SelectedValue) > 0 Then
                If CInt(cboSpouse.SelectedValue) = CInt(cboChild.SelectedValue) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, this selected relation is already used. Please select other relation."), enMsgBoxStyle.Information)
                    cboChild.SelectedValue = 0
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboChild_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " TextChange Event(s) "

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvServiceProvider.Items.Count <= 0 Then Exit Sub
            lvServiceProvider.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvServiceProvider.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvServiceProvider.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchAlloctation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchAlloctation.TextChanged
        Try
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            lvAllocation.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearchAlloctation.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocation.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchAlloctation_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " CheckBox Event(s) "

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvServiceProvider.ItemChecked, AddressOf lvServiceProvider_ItemChecked
            For Each lvItem As ListViewItem In lvServiceProvider.Items
                lvItem.Checked = objchkAll.Checked
            Next
            AddHandler lvServiceProvider.ItemChecked, AddressOf lvServiceProvider_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAllocAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllocAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            For Each lvItem As ListViewItem In lvAllocation.Items
                lvItem.Checked = objchkAllocAll.Checked
            Next
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllocAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Listview Event(s) "

    Private Sub lvServiceProvider_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvServiceProvider.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvServiceProvider.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvServiceProvider.CheckedItems.Count < lvServiceProvider.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvServiceProvider.CheckedItems.Count = lvServiceProvider.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvServiceProvider_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAllocAll.CheckedChanged, AddressOf objchkAllocAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                objchkAllocAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                objchkAllocAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                objchkAllocAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllocAll.CheckedChanged, AddressOf objchkAllocAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Try
            If CInt(cboSpouse.SelectedValue) <= 0 Or CInt(cboChild.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Spouse,Child in order to save."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objUserDefRMode As clsUserDef_ReportMode
            Dim intUnkid As Integer = -1

            objUserDefRMode = New clsUserDef_ReportMode()

            objUserDefRMode._Reportunkid = enArutiReport.Employee_Medical_Cover_Detail
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0

            objUserDefRMode._Headtypeid = 1
            objUserDefRMode._EarningTranHeadIds = CInt(cboSpouse.SelectedValue)
            intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Medical_Cover_Detail, 0, 0, objUserDefRMode._Headtypeid)

            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If

            objUserDefRMode = New clsUserDef_ReportMode()

            intUnkid = -1
            objUserDefRMode._Reportunkid = enArutiReport.Employee_Medical_Cover_Detail
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0

            objUserDefRMode._Headtypeid = 2
            objUserDefRMode._EarningTranHeadIds = CInt(cboChild.SelectedValue)
            intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Medical_Cover_Detail, 0, 0, objUserDefRMode._Headtypeid)

            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Selection Saved Successfully."), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


		
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.colhServiceProvider.Text = Language._Object.getCaption(CStr(Me.colhServiceProvider.Tag), Me.colhServiceProvider.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.LblChild.Text = Language._Object.getCaption(Me.LblChild.Name, Me.LblChild.Text)
			Me.LblSpouse.Text = Language._Object.getCaption(Me.LblSpouse.Name, Me.LblSpouse.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
			Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, this selected relation is already used. Please select other relation.")
			Language.setMessage(mstrModuleName, 2, "Please select Spouse,Child in order to save.")
			Language.setMessage(mstrModuleName, 3, "Selection Saved Successfully.")
			Language.setMessage(mstrModuleName, 4, "Period is mandatory information.Please Select Period to continue.")
			Language.setMessage(mstrModuleName, 5, "Allocation is mandatory information. Please check atleast one Allocation in order to generate report.")
			Language.setMessage(mstrModuleName, 6, "Spouse is mandatory information. Please select Spouse to continue.")
			Language.setMessage(mstrModuleName, 7, "Child is mandatory information. Please select Child to continue.")
			Language.setMessage(mstrModuleName, 8, "Service Provider is mandatory information. Please check atleast one Service Provider in order to generate report")
			Language.setMessage(mstrModuleName, 9, "Problem in exporting report.")
			Language.setMessage(mstrModuleName, 10, "Report exported successfully to the given path.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
