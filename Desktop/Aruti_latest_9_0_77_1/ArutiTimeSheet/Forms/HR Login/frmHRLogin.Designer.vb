﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHRLogin
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHRLogin))
        Me.pnlHrLogin = New System.Windows.Forms.Panel
        Me.btnclockin = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClockout = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtpassword = New eZee.TextBox.AlphanumericTextBox
        Me.lblpassword = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnswipecard = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnclose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnexit = New eZee.Common.eZeeLightButton(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtSearchEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.dgcolEmployeeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolshiftunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolEmpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSwipedata = New System.Windows.Forms.TextBox
        Me.tmrSwipecard = New System.Windows.Forms.Timer(Me.components)
        Me.objntfLogin = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.pnlHrLogin.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlHrLogin
        '
        Me.pnlHrLogin.Controls.Add(Me.btnclockin)
        Me.pnlHrLogin.Controls.Add(Me.btnClockout)
        Me.pnlHrLogin.Controls.Add(Me.txtpassword)
        Me.pnlHrLogin.Controls.Add(Me.lblpassword)
        Me.pnlHrLogin.Controls.Add(Me.EZeeFooter1)
        Me.pnlHrLogin.Controls.Add(Me.GroupBox1)
        Me.pnlHrLogin.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlHrLogin.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.pnlHrLogin.Location = New System.Drawing.Point(0, 0)
        Me.pnlHrLogin.Name = "pnlHrLogin"
        Me.pnlHrLogin.Size = New System.Drawing.Size(396, 461)
        Me.pnlHrLogin.TabIndex = 0
        '
        'btnclockin
        '
        Me.btnclockin.BackColor = System.Drawing.Color.White
        Me.btnclockin.BackgroundImage = CType(resources.GetObject("btnclockin.BackgroundImage"), System.Drawing.Image)
        Me.btnclockin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnclockin.BorderColor = System.Drawing.Color.Empty
        Me.btnclockin.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnclockin.FlatAppearance.BorderSize = 0
        Me.btnclockin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnclockin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclockin.ForeColor = System.Drawing.Color.Black
        Me.btnclockin.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnclockin.GradientForeColor = System.Drawing.Color.Black
        Me.btnclockin.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnclockin.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnclockin.Location = New System.Drawing.Point(291, 371)
        Me.btnclockin.Name = "btnclockin"
        Me.btnclockin.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnclockin.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnclockin.Size = New System.Drawing.Size(90, 30)
        Me.btnclockin.TabIndex = 4
        Me.btnclockin.Text = "Clock &In"
        Me.btnclockin.UseVisualStyleBackColor = True
        '
        'btnClockout
        '
        Me.btnClockout.BackColor = System.Drawing.Color.White
        Me.btnClockout.BackgroundImage = CType(resources.GetObject("btnClockout.BackgroundImage"), System.Drawing.Image)
        Me.btnClockout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClockout.BorderColor = System.Drawing.Color.Empty
        Me.btnClockout.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClockout.FlatAppearance.BorderSize = 0
        Me.btnClockout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClockout.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClockout.ForeColor = System.Drawing.Color.Black
        Me.btnClockout.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClockout.GradientForeColor = System.Drawing.Color.Black
        Me.btnClockout.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClockout.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClockout.Location = New System.Drawing.Point(291, 371)
        Me.btnClockout.Name = "btnClockout"
        Me.btnClockout.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClockout.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClockout.Size = New System.Drawing.Size(90, 30)
        Me.btnClockout.TabIndex = 11
        Me.btnClockout.Text = "Clock &Out"
        Me.btnClockout.UseVisualStyleBackColor = True
        '
        'txtpassword
        '
        Me.txtpassword.Flags = 0
        Me.txtpassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpassword.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtpassword.Location = New System.Drawing.Point(99, 375)
        Me.txtpassword.Name = "txtpassword"
        Me.txtpassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtpassword.Size = New System.Drawing.Size(182, 21)
        Me.txtpassword.TabIndex = 3
        '
        'lblpassword
        '
        Me.lblpassword.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpassword.Location = New System.Drawing.Point(12, 375)
        Me.lblpassword.Name = "lblpassword"
        Me.lblpassword.Size = New System.Drawing.Size(88, 20)
        Me.lblpassword.TabIndex = 2
        Me.lblpassword.Text = "&Password"
        Me.lblpassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnswipecard)
        Me.EZeeFooter1.Controls.Add(Me.btnclose)
        Me.EZeeFooter1.Controls.Add(Me.btnexit)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 406)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(396, 55)
        Me.EZeeFooter1.TabIndex = 10
        '
        'btnswipecard
        '
        Me.btnswipecard.BackColor = System.Drawing.Color.White
        Me.btnswipecard.BackgroundImage = CType(resources.GetObject("btnswipecard.BackgroundImage"), System.Drawing.Image)
        Me.btnswipecard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnswipecard.BorderColor = System.Drawing.Color.Empty
        Me.btnswipecard.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnswipecard.FlatAppearance.BorderSize = 0
        Me.btnswipecard.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnswipecard.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnswipecard.ForeColor = System.Drawing.Color.Black
        Me.btnswipecard.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnswipecard.GradientForeColor = System.Drawing.Color.Black
        Me.btnswipecard.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnswipecard.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnswipecard.Location = New System.Drawing.Point(15, 13)
        Me.btnswipecard.Name = "btnswipecard"
        Me.btnswipecard.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnswipecard.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnswipecard.Size = New System.Drawing.Size(90, 30)
        Me.btnswipecard.TabIndex = 2
        Me.btnswipecard.TabStop = False
        Me.btnswipecard.Text = "S&wipe Card"
        Me.btnswipecard.UseVisualStyleBackColor = True
        Me.btnswipecard.Visible = False
        '
        'btnclose
        '
        Me.btnclose.BackColor = System.Drawing.Color.White
        Me.btnclose.BackgroundImage = CType(resources.GetObject("btnclose.BackgroundImage"), System.Drawing.Image)
        Me.btnclose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnclose.BorderColor = System.Drawing.Color.Empty
        Me.btnclose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnclose.FlatAppearance.BorderSize = 0
        Me.btnclose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnclose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.ForeColor = System.Drawing.Color.Black
        Me.btnclose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnclose.GradientForeColor = System.Drawing.Color.Black
        Me.btnclose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnclose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnclose.Location = New System.Drawing.Point(195, 13)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnclose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnclose.Size = New System.Drawing.Size(90, 30)
        Me.btnclose.TabIndex = 5
        Me.btnclose.Text = "&Close"
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'btnexit
        '
        Me.btnexit.BackColor = System.Drawing.Color.White
        Me.btnexit.BackgroundImage = CType(resources.GetObject("btnexit.BackgroundImage"), System.Drawing.Image)
        Me.btnexit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnexit.BorderColor = System.Drawing.Color.Empty
        Me.btnexit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnexit.FlatAppearance.BorderSize = 0
        Me.btnexit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnexit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnexit.ForeColor = System.Drawing.Color.Black
        Me.btnexit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnexit.GradientForeColor = System.Drawing.Color.Black
        Me.btnexit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnexit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnexit.Location = New System.Drawing.Point(291, 13)
        Me.btnexit.Name = "btnexit"
        Me.btnexit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnexit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnexit.Size = New System.Drawing.Size(90, 30)
        Me.btnexit.TabIndex = 6
        Me.btnexit.Text = "E&xit"
        Me.btnexit.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtSearchEmployee)
        Me.GroupBox1.Controls.Add(Me.dgEmployee)
        Me.GroupBox1.Controls.Add(Me.txtSwipedata)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(13, 11)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(368, 354)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Employee"
        '
        'txtSearchEmployee
        '
        Me.txtSearchEmployee.Flags = 0
        Me.txtSearchEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearchEmployee.Location = New System.Drawing.Point(14, 321)
        Me.txtSearchEmployee.Name = "txtSearchEmployee"
        Me.txtSearchEmployee.Size = New System.Drawing.Size(340, 21)
        Me.txtSearchEmployee.TabIndex = 2
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeColumns = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.White
        Me.dgEmployee.ColumnHeadersHeight = 4
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.ColumnHeadersVisible = False
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolEmployeeName, Me.objcolEmployeeunkid, Me.objcolshiftunkid, Me.dgcolEmpName})
        Me.dgEmployee.Location = New System.Drawing.Point(14, 25)
        Me.dgEmployee.MultiSelect = False
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 4
        Me.dgEmployee.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmployee.Size = New System.Drawing.Size(340, 290)
        Me.dgEmployee.TabIndex = 1
        '
        'dgcolEmployeeName
        '
        Me.dgcolEmployeeName.HeaderText = "Employee"
        Me.dgcolEmployeeName.Name = "dgcolEmployeeName"
        Me.dgcolEmployeeName.ReadOnly = True
        Me.dgcolEmployeeName.Width = 334
        '
        'objcolEmployeeunkid
        '
        Me.objcolEmployeeunkid.HeaderText = "Employeeunkid"
        Me.objcolEmployeeunkid.Name = "objcolEmployeeunkid"
        Me.objcolEmployeeunkid.ReadOnly = True
        Me.objcolEmployeeunkid.Visible = False
        '
        'objcolshiftunkid
        '
        Me.objcolshiftunkid.HeaderText = "Shiftunkid"
        Me.objcolshiftunkid.Name = "objcolshiftunkid"
        Me.objcolshiftunkid.ReadOnly = True
        Me.objcolshiftunkid.Visible = False
        '
        'dgcolEmpName
        '
        Me.dgcolEmpName.HeaderText = "EmpName"
        Me.dgcolEmpName.Name = "dgcolEmpName"
        Me.dgcolEmpName.ReadOnly = True
        Me.dgcolEmpName.Visible = False
        '
        'txtSwipedata
        '
        Me.txtSwipedata.Location = New System.Drawing.Point(43, 66)
        Me.txtSwipedata.Name = "txtSwipedata"
        Me.txtSwipedata.Size = New System.Drawing.Size(291, 26)
        Me.txtSwipedata.TabIndex = 245
        '
        'tmrSwipecard
        '
        Me.tmrSwipecard.Interval = 5000
        '
        'objntfLogin
        '
        Me.objntfLogin.Visible = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 334
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employeeunkid"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "EmpName"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "EmpName"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 2500
        '
        'frmHRLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(396, 461)
        Me.Controls.Add(Me.pnlHrLogin)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHRLogin"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Login"
        Me.pnlHrLogin.ResumeLayout(False)
        Me.pnlHrLogin.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlHrLogin As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSearchEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnclose As eZee.Common.eZeeLightButton
    Friend WithEvents btnexit As eZee.Common.eZeeLightButton
    Friend WithEvents txtpassword As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblpassword As System.Windows.Forms.Label
    Friend WithEvents btnswipecard As eZee.Common.eZeeLightButton
    Friend WithEvents btnclockin As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnClockout As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolEmployeeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolshiftunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolEmpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tmrSwipecard As System.Windows.Forms.Timer
    Friend WithEvents txtSwipedata As System.Windows.Forms.TextBox
    Friend WithEvents objntfLogin As System.Windows.Forms.NotifyIcon
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
