﻿'S.SANDEEP [ 09 AUG 2011 ] -- START
'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
Public Enum enCore
    ARUTI
End Enum
'S.SANDEEP [ 09 AUG 2011 ] -- END
Public Class core

    Public Const Version As String = "9.0.77.001"
    Public Const mintDBVersion_Major As Integer = 9
    Public Const mintDBVersion_Minor As Integer = 0
    Public Const mintDBVersion_Build As Integer = 77
    Public Const mintDBVersion_Revision As Integer = 1
    Public Const Copyrightversion As String = "Copyright © Aruti 2020"
    'S.SANDEEP [ 09 AUG 2011 ] -- START
    'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
    Public Const AutoBackupFile As String = "ArutiAutoBackup.exe"
    Public Const iCore As enCore = enCore.ARUTI
    'S.SANDEEP [ 09 AUG 2011 ] -- END 
    Public Const AutoImportRecruitment As String = "ArutiAutoRecruit.exe" 'Sohail (23 Oct 2012)
#Region " Database Version Property "
    Public Shared ReadOnly Property Database_Version_Major() As Integer
        Get
            Return mintDBVersion_Major
        End Get
    End Property

    Public Shared ReadOnly Property Database_Version_Minor() As Integer
        Get
            Return mintDBVersion_Minor
        End Get
    End Property

    Public Shared ReadOnly Property Database_Version_Build() As Integer
        Get
            Return mintDBVersion_Build
        End Get
    End Property

    Public Shared ReadOnly Property Database_Version_Revision() As Integer
        Get
            Return mintDBVersion_Revision
        End Get
    End Property
#End Region


    'S.SANDEEP [ 09 AUG 2011 ] -- START
    'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
    Public Shared ReadOnly Property SupportTeam() As String
        Get
            Return "Aruti Support Team"
        End Get
    End Property
    'S.SANDEEP [ 09 AUG 2011 ] -- END 


    'Anjan [ 15 Apr 2013 ] -- Start
    'ENHANCEMENT : License CHANGES
    Public Shared ReadOnly Property HD() As String
        Get
            Return "20220630"
        End Get
    End Property
    'Anjan [ 15 Apr 2013 ] -- End

End Class
