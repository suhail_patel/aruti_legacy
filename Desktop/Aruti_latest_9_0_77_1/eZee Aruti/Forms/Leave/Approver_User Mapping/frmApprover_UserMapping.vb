﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message index = 3

Public Class frmApprover_UserMapping

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmApprover_UserMapping"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objUserMapping As clsapprover_Usermapping
    Friend mintMappingUnkid As Integer = -1
    Private mintApproverunkid As Integer = -1
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal _intApproverunkid As Integer) As Boolean
        Try
            mintMappingUnkid = intUnkId
            mintApproverunkid = _intApproverunkid
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintMappingUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub GetValue()
        Try
            cboUser.SelectedValue = objUserMapping._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetColor()
        Try
            cboUser.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objUserMapping._Approverunkid = mintApproverunkid
            objUserMapping._Userunkid = CInt(cboUser.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim mblnAdUser As Boolean = False
        Try


            'Pinkal (09-Jan-2013) -- Start
            'Enhancement : TRA Changes

            'dsFill = objUserMapping.GetUserWithAppoverPrivilage("User", True)
            'cboUser.DisplayMember = "username"
            'cboUser.ValueMember = "userunkid"
            'cboUser.DataSource = dsFill.Tables("User")


            'PRIVILEGE NO 264 :- ALLOW TO APPROVE LEAVE

            Dim objUser As New clsUserAddEdit
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'Dim objOption As New clsPassowdOptions
            'Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

            'If drOption.Length > 0 Then
            '    If objOption._UserLogingModeId <> enAuthenticationMode.BASIC_AUTHENTICATION Then mblnAdUser = True
            'End If

            'dsFill = objUser.getComboList("User", True, mblnAdUser, objOption._IsEmployeeAsUser, Company._Object._Companyunkid, 264)

            'Nilay (01-Mar-2016) -- Start
            'dsFill = objUser.getNewComboList("User", , True, Company._Object._Companyunkid, 264)
            dsFill = objUser.getNewComboList("User", , True, Company._Object._Companyunkid, CStr(264), FinancialYear._Object._YearUnkid, True)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            cboUser.DisplayMember = "name"
            cboUser.ValueMember = "userunkid"
            cboUser.DataSource = dsFill.Tables("User")
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'objOption = Nothing
            'S.SANDEEP [10 AUG 2015] -- END

            objUser = Nothing

            'Pinkal (09-Jan-2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub GetApproverInfo()
        Try

            Dim objapprover As New clsleaveapprover_master
            objapprover._Approverunkid = mintApproverunkid

            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = objapprover._leaveapproverunkid
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objapprover._leaveapproverunkid
            'S.SANDEEP [04 JUN 2015] -- END

            objlblApprover.Text = objEmployee._Firstname & "  " & objEmployee._Surname

            Dim objLevel As New clsapproverlevel_master
            objLevel._Levelunkid = objapprover._Levelunkid
            objlblApproverLevel.Text = objLevel._Levelname

            '    FillList(objapprover._Departmentunkid, objapprover._Sectionunkid, objapprover._Jobunkid)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverInfo", mstrModuleName)
        End Try
    End Sub

    'Private Sub FillList(ByVal intDepartmentunkid As Integer, ByVal intSectionunkid As Integer, ByVal intJobunkid As Integer)
    '    Dim dsFill As DataSet = Nothing
    '    Dim dtFill As DataTable = Nothing
    '    Dim lvItem As ListViewItem
    '    Dim mstrSectionunkid As String = ""
    '    Try

    '        'START FOR APPROVE DEPARTMENT

    '        Dim objDepartment As New clsDepartment
    '        dsFill = objDepartment.GetList("Department", True)

    '        If intDepartmentunkid > 0 Then
    '            dtFill = New DataView(dsFill.Tables("Department"), "departmentunkid = " & intDepartmentunkid, "", DataViewRowState.CurrentRows).ToTable
    '        Else
    '            dtFill = dsFill.Tables("Department")
    '        End If

    '        If dtFill.Rows.Count > 0 Then
    '            lvDepartment.Items.Clear()

    '            For Each drRow As DataRow In dtFill.Rows
    '                lvItem = New ListViewItem
    '                lvItem.Tag = CInt(drRow("departmentunkid"))
    '                lvItem.Text = drRow("name").ToString
    '                lvDepartment.Items.Add(lvItem)
    '            Next

    '            If lvDepartment.Items.Count > 8 Then
    '                colhDepartment.Width = 155 - 18
    '            Else
    '                colhDepartment.Width = 155
    '            End If

    '        End If

    '        'END FOR APPROVE DEPARTMENT


    '        'START FOR APPROVE SECTION
    '        Dim objSection As New clsSections
    '        dsFill = objSection.GetList("Section", True)

    '        If intSectionunkid > 0 Then
    '            If intDepartmentunkid > 0 Then
    '                dtFill = New DataView(dsFill.Tables("Section"), "Sectionunkid = " & intSectionunkid & " AND departmentunkid = " & intDepartmentunkid, "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtFill = New DataView(dsFill.Tables("Section"), "Sectionunkid = " & intSectionunkid, "", DataViewRowState.CurrentRows).ToTable
    '            End If

    '        Else
    '            If intDepartmentunkid > 0 Then
    '                dtFill = New DataView(dsFill.Tables("Section"), "departmentunkid = " & intDepartmentunkid, "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtFill = dsFill.Tables("Section")
    '            End If
    '        End If

    '        If dtFill.Rows.Count > 0 Then
    '            lvSection.Items.Clear()

    '            For Each drRow As DataRow In dtFill.Rows
    '                lvItem = New ListViewItem
    '                lvItem.Tag = CInt(drRow("Sectionunkid"))
    '                lvItem.Text = drRow("name").ToString
    '                lvSection.Items.Add(lvItem)
    '                mstrSectionunkid &= CInt(drRow("Sectionunkid")) & ","
    '            Next


    '            If lvSection.Items.Count > 8 Then
    '                colhSection.Width = 155 - 18
    '            Else
    '                colhSection.Width = 155
    '            End If

    '            If mstrSectionunkid.Length > 0 Then
    '                mstrSectionunkid = mstrSectionunkid.Substring(0, mstrSectionunkid.Length - 1)
    '            End If

    '        End If
    '        'END FOR APPROVE SECTION


    '        'START FOR APPROVE JOB

    '        Dim objJob As New clsJobs
    '        dsFill = objJob.GetList("Job", True)

    '        If intJobunkid > 0 Then
    '            If intSectionunkid > 0 Then
    '                dtFill = New DataView(dsFill.Tables("Job"), "Jobunkid = " & intJobunkid & " AND jobsectionunkid= " & intSectionunkid, "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtFill = New DataView(dsFill.Tables("Job"), "Jobunkid = " & intJobunkid, "", DataViewRowState.CurrentRows).ToTable
    '            End If

    '        Else
    '            If intDepartmentunkid > 0 And mstrSectionunkid.Length > 0 Then
    '                dtFill = New DataView(dsFill.Tables("Job"), "jobsectionunkid in (" & mstrSectionunkid & " )", "", DataViewRowState.CurrentRows).ToTable
    '            ElseIf intJobunkid <= 0 And mstrSectionunkid.Length > 0 Then
    '                dtFill = dsFill.Tables("Job")
    '            End If
    '        End If

    '        If dtFill.Rows.Count > 0 Then
    '            lvJob.Items.Clear()

    '            For Each drRow As DataRow In dtFill.Rows
    '                lvItem = New ListViewItem
    '                lvItem.Tag = CInt(drRow("Jobunkid"))
    '                lvItem.Text = drRow("JobName").ToString
    '                lvJob.Items.Add(lvItem)
    '            Next

    '            If lvJob.Items.Count > 8 Then
    '                colhJob.Width = 155 - 18
    '            Else
    '                colhJob.Width = 155
    '            End If

    '        End If

    '        'END FOR APPROVE JOB





    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region " Form's Events "

    Private Sub frmApprover_UserMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUserMapping = New clsapprover_Usermapping
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            SetColor()
            FillCombo()
            GetApproverInfo()
            objUserMapping._UserTypeid = enUserType.Approver
            If menAction = enAction.EDIT_ONE Then
                objUserMapping._Mappingunkid = mintMappingUnkid
                btnDelete.Enabled = True
            Else
                btnDelete.Enabled = False
            End If
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_UserMapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApprover_UserMapping_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_UserMapping_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApprover_UserMapping_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_UserMapping_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApprover_UserMapping_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objUserMapping = Nothing
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsapprover_Usermapping.SetMessages()
            objfrm._Other_ModuleNames = "clsapprover_Usermapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboUser.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "User is compulsory information.Please Select User."), enMsgBoxStyle.Information)
                cboUser.Select()
                Exit Sub
            End If

            SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objUserMapping._FormName = mstrModuleName
            objUserMapping._LoginEmployeeunkid = 0
            objUserMapping._ClientIP = getIP()
            objUserMapping._HostName = getHostName()
            objUserMapping._FromWeb = False
            objUserMapping._AuditUserId = User._Object._Userunkid
objUserMapping._CompanyUnkid = Company._Object._Companyunkid
            objUserMapping._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If mintMappingUnkid > 0 Then
                blnFlag = objUserMapping.Update()
            Else
                blnFlag = objUserMapping.Insert()
            End If


            If blnFlag = False And objUserMapping._Message <> "" Then
                eZeeMsgBox.Show(objUserMapping._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                mintMappingUnkid = objUserMapping._Mappingunkid
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If CInt(cboUser.SelectedValue) < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select User from the list to perform further operation on it."), enMsgBoxStyle.Information)
                cboUser.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this User Mapping?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objUserMapping._FormName = mstrModuleName
                objUserMapping._LoginEmployeeunkid = 0
                objUserMapping._ClientIP = getIP()
                objUserMapping._HostName = getHostName()
                objUserMapping._FromWeb = False
                objUserMapping._AuditUserId = User._Object._Userunkid
objUserMapping._CompanyUnkid = Company._Object._Companyunkid
                objUserMapping._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objUserMapping.Delete(mintMappingUnkid)
                cboUser.SelectedIndex = 0
                mintMappingUnkid = 0
                btnDelete.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub



    'Pinkal (07-APR-2012) -- Start
    'Enhancement : TRA Changes
    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboUser.DataSource, DataTable)
            With cboUser
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        End Try
    End Sub
    'Pinkal (07-APR-2012) -- End


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbLeavedetail.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLeavedetail.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbLeavedetail.Text = Language._Object.getCaption(Me.gbLeavedetail.Name, Me.gbLeavedetail.Text)
			Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
			Me.lblApproverLevel.Text = Language._Object.getCaption(Me.lblApproverLevel.Name, Me.lblApproverLevel.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.Name, Me.lblUser.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "User is compulsory information.Please Select User.")
			Language.setMessage(mstrModuleName, 2, "Please select User from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this User Mapping?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

    
End Class
