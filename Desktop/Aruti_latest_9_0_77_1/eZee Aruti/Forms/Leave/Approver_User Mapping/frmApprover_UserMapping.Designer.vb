﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApprover_UserMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApprover_UserMapping))
        Me.gbLeavedetail = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchUser = New eZee.Common.eZeeGradientButton
        Me.cboUser = New System.Windows.Forms.ComboBox
        Me.lblUser = New System.Windows.Forms.Label
        Me.objlblJob = New System.Windows.Forms.Label
        Me.objlblSection = New System.Windows.Forms.Label
        Me.objlblDepartment = New System.Windows.Forms.Label
        Me.objlblApprover = New System.Windows.Forms.Label
        Me.lblApprover = New System.Windows.Forms.Label
        Me.objlblApproverLevel = New System.Windows.Forms.Label
        Me.lblApproverLevel = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbLeavedetail.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbLeavedetail
        '
        Me.gbLeavedetail.BorderColor = System.Drawing.Color.Black
        Me.gbLeavedetail.Checked = False
        Me.gbLeavedetail.CollapseAllExceptThis = False
        Me.gbLeavedetail.CollapsedHoverImage = Nothing
        Me.gbLeavedetail.CollapsedNormalImage = Nothing
        Me.gbLeavedetail.CollapsedPressedImage = Nothing
        Me.gbLeavedetail.CollapseOnLoad = False
        Me.gbLeavedetail.Controls.Add(Me.objbtnSearchUser)
        Me.gbLeavedetail.Controls.Add(Me.cboUser)
        Me.gbLeavedetail.Controls.Add(Me.lblUser)
        Me.gbLeavedetail.Controls.Add(Me.objlblJob)
        Me.gbLeavedetail.Controls.Add(Me.objlblSection)
        Me.gbLeavedetail.Controls.Add(Me.objlblDepartment)
        Me.gbLeavedetail.Controls.Add(Me.objlblApprover)
        Me.gbLeavedetail.Controls.Add(Me.lblApprover)
        Me.gbLeavedetail.Controls.Add(Me.objlblApproverLevel)
        Me.gbLeavedetail.Controls.Add(Me.lblApproverLevel)
        Me.gbLeavedetail.ExpandedHoverImage = Nothing
        Me.gbLeavedetail.ExpandedNormalImage = Nothing
        Me.gbLeavedetail.ExpandedPressedImage = Nothing
        Me.gbLeavedetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeavedetail.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeavedetail.HeaderHeight = 25
        Me.gbLeavedetail.HeaderMessage = ""
        Me.gbLeavedetail.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeavedetail.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeavedetail.HeightOnCollapse = 0
        Me.gbLeavedetail.LeftTextSpace = 0
        Me.gbLeavedetail.Location = New System.Drawing.Point(10, 4)
        Me.gbLeavedetail.Name = "gbLeavedetail"
        Me.gbLeavedetail.OpenHeight = 300
        Me.gbLeavedetail.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeavedetail.ShowBorder = True
        Me.gbLeavedetail.ShowCheckBox = False
        Me.gbLeavedetail.ShowCollapseButton = False
        Me.gbLeavedetail.ShowDefaultBorderColor = True
        Me.gbLeavedetail.ShowDownButton = False
        Me.gbLeavedetail.ShowHeader = True
        Me.gbLeavedetail.Size = New System.Drawing.Size(416, 122)
        Me.gbLeavedetail.TabIndex = 8
        Me.gbLeavedetail.Temp = 0
        Me.gbLeavedetail.Text = "Approver Detail"
        Me.gbLeavedetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchUser
        '
        Me.objbtnSearchUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUser.BorderSelected = False
        Me.objbtnSearchUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUser.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUser.Location = New System.Drawing.Point(301, 91)
        Me.objbtnSearchUser.Name = "objbtnSearchUser"
        Me.objbtnSearchUser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUser.TabIndex = 298
        '
        'cboUser
        '
        Me.cboUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(121, 91)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(174, 21)
        Me.cboUser.TabIndex = 1
        '
        'lblUser
        '
        Me.lblUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.Location = New System.Drawing.Point(11, 94)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(104, 14)
        Me.lblUser.TabIndex = 291
        Me.lblUser.Text = "User"
        Me.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblJob
        '
        Me.objlblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblJob.Location = New System.Drawing.Point(76, 222)
        Me.objlblJob.Name = "objlblJob"
        Me.objlblJob.Size = New System.Drawing.Size(246, 14)
        Me.objlblJob.TabIndex = 289
        Me.objlblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSection
        '
        Me.objlblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSection.Location = New System.Drawing.Point(76, 196)
        Me.objlblSection.Name = "objlblSection"
        Me.objlblSection.Size = New System.Drawing.Size(246, 14)
        Me.objlblSection.TabIndex = 286
        Me.objlblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblDepartment
        '
        Me.objlblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblDepartment.Location = New System.Drawing.Point(119, 89)
        Me.objlblDepartment.Name = "objlblDepartment"
        Me.objlblDepartment.Size = New System.Drawing.Size(246, 14)
        Me.objlblDepartment.TabIndex = 284
        Me.objlblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblApprover
        '
        Me.objlblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblApprover.Location = New System.Drawing.Point(119, 37)
        Me.objlblApprover.Name = "objlblApprover"
        Me.objlblApprover.Size = New System.Drawing.Size(269, 14)
        Me.objlblApprover.TabIndex = 282
        Me.objlblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(8, 37)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(104, 14)
        Me.lblApprover.TabIndex = 281
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblApproverLevel
        '
        Me.objlblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblApproverLevel.Location = New System.Drawing.Point(119, 63)
        Me.objlblApproverLevel.Name = "objlblApproverLevel"
        Me.objlblApproverLevel.Size = New System.Drawing.Size(269, 14)
        Me.objlblApproverLevel.TabIndex = 280
        Me.objlblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApproverLevel
        '
        Me.lblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverLevel.Location = New System.Drawing.Point(8, 63)
        Me.lblApproverLevel.Name = "lblApproverLevel"
        Me.lblApproverLevel.Size = New System.Drawing.Size(104, 14)
        Me.lblApproverLevel.TabIndex = 279
        Me.lblApproverLevel.Text = "Approver Level"
        Me.lblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 132)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(436, 55)
        Me.objFooter.TabIndex = 9
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(223, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 3
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(120, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(326, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmApprover_UserMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(436, 187)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbLeavedetail)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApprover_UserMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "User Mapping "
        Me.gbLeavedetail.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbLeavedetail As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblSection As System.Windows.Forms.Label
    Friend WithEvents objlblDepartment As System.Windows.Forms.Label
    Friend WithEvents objlblApprover As System.Windows.Forms.Label
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents objlblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents lblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents objlblJob As System.Windows.Forms.Label
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents cboUser As System.Windows.Forms.ComboBox
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchUser As eZee.Common.eZeeGradientButton
End Class
