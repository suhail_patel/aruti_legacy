﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveApproverLevel_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeaveApproverLevel_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbApproverLevelInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.nudEscalation = New System.Windows.Forms.NumericUpDown
        Me.LblEscalationDays = New System.Windows.Forms.Label
        Me.lblPRemark = New System.Windows.Forms.Label
        Me.nudPriority = New System.Windows.Forms.NumericUpDown
        Me.lblPriority = New System.Windows.Forms.Label
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblLevelName = New System.Windows.Forms.Label
        Me.lblLevelCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkmandatory = New System.Windows.Forms.CheckBox
        Me.lblMandatory = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbApproverLevelInfo.SuspendLayout()
        CType(Me.nudEscalation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPriority, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbApproverLevelInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.chkmandatory)
        Me.pnlMainInfo.Controls.Add(Me.lblMandatory)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(360, 201)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbApproverLevelInfo
        '
        Me.gbApproverLevelInfo.BorderColor = System.Drawing.Color.Black
        Me.gbApproverLevelInfo.Checked = False
        Me.gbApproverLevelInfo.CollapseAllExceptThis = False
        Me.gbApproverLevelInfo.CollapsedHoverImage = Nothing
        Me.gbApproverLevelInfo.CollapsedNormalImage = Nothing
        Me.gbApproverLevelInfo.CollapsedPressedImage = Nothing
        Me.gbApproverLevelInfo.CollapseOnLoad = False
        Me.gbApproverLevelInfo.Controls.Add(Me.nudEscalation)
        Me.gbApproverLevelInfo.Controls.Add(Me.LblEscalationDays)
        Me.gbApproverLevelInfo.Controls.Add(Me.lblPRemark)
        Me.gbApproverLevelInfo.Controls.Add(Me.nudPriority)
        Me.gbApproverLevelInfo.Controls.Add(Me.lblPriority)
        Me.gbApproverLevelInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbApproverLevelInfo.Controls.Add(Me.txtName)
        Me.gbApproverLevelInfo.Controls.Add(Me.txtCode)
        Me.gbApproverLevelInfo.Controls.Add(Me.lblLevelName)
        Me.gbApproverLevelInfo.Controls.Add(Me.lblLevelCode)
        Me.gbApproverLevelInfo.ExpandedHoverImage = Nothing
        Me.gbApproverLevelInfo.ExpandedNormalImage = Nothing
        Me.gbApproverLevelInfo.ExpandedPressedImage = Nothing
        Me.gbApproverLevelInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApproverLevelInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApproverLevelInfo.HeaderHeight = 25
        Me.gbApproverLevelInfo.HeaderMessage = ""
        Me.gbApproverLevelInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbApproverLevelInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApproverLevelInfo.HeightOnCollapse = 0
        Me.gbApproverLevelInfo.LeftTextSpace = 0
        Me.gbApproverLevelInfo.Location = New System.Drawing.Point(2, 2)
        Me.gbApproverLevelInfo.Name = "gbApproverLevelInfo"
        Me.gbApproverLevelInfo.OpenHeight = 300
        Me.gbApproverLevelInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApproverLevelInfo.ShowBorder = True
        Me.gbApproverLevelInfo.ShowCheckBox = False
        Me.gbApproverLevelInfo.ShowCollapseButton = False
        Me.gbApproverLevelInfo.ShowDefaultBorderColor = True
        Me.gbApproverLevelInfo.ShowDownButton = False
        Me.gbApproverLevelInfo.ShowHeader = True
        Me.gbApproverLevelInfo.Size = New System.Drawing.Size(356, 143)
        Me.gbApproverLevelInfo.TabIndex = 0
        Me.gbApproverLevelInfo.Temp = 0
        Me.gbApproverLevelInfo.Text = "Approver Level Info"
        Me.gbApproverLevelInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudEscalation
        '
        Me.nudEscalation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudEscalation.Location = New System.Drawing.Point(114, 114)
        Me.nudEscalation.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nudEscalation.Name = "nudEscalation"
        Me.nudEscalation.Size = New System.Drawing.Size(50, 21)
        Me.nudEscalation.TabIndex = 9
        Me.nudEscalation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblEscalationDays
        '
        Me.LblEscalationDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEscalationDays.Location = New System.Drawing.Point(8, 117)
        Me.LblEscalationDays.Name = "LblEscalationDays"
        Me.LblEscalationDays.Size = New System.Drawing.Size(100, 15)
        Me.LblEscalationDays.TabIndex = 8
        Me.LblEscalationDays.Text = "Escalation Days"
        Me.LblEscalationDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPRemark
        '
        Me.lblPRemark.ForeColor = System.Drawing.Color.Red
        Me.lblPRemark.Location = New System.Drawing.Point(170, 88)
        Me.lblPRemark.Name = "lblPRemark"
        Me.lblPRemark.Size = New System.Drawing.Size(179, 28)
        Me.lblPRemark.TabIndex = 7
        Me.lblPRemark.Text = "Higher Priority means High Level."
        '
        'nudPriority
        '
        Me.nudPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPriority.Location = New System.Drawing.Point(114, 87)
        Me.nudPriority.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.nudPriority.Name = "nudPriority"
        Me.nudPriority.Size = New System.Drawing.Size(50, 21)
        Me.nudPriority.TabIndex = 6
        Me.nudPriority.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudPriority.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblPriority
        '
        Me.lblPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPriority.Location = New System.Drawing.Point(8, 90)
        Me.lblPriority.Name = "lblPriority"
        Me.lblPriority.Size = New System.Drawing.Size(100, 15)
        Me.lblPriority.TabIndex = 5
        Me.lblPriority.Text = "Priority"
        Me.lblPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(309, 60)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(114, 60)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(190, 21)
        Me.txtName.TabIndex = 3
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(114, 33)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(120, 21)
        Me.txtCode.TabIndex = 1
        '
        'lblLevelName
        '
        Me.lblLevelName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelName.Location = New System.Drawing.Point(8, 62)
        Me.lblLevelName.Name = "lblLevelName"
        Me.lblLevelName.Size = New System.Drawing.Size(100, 15)
        Me.lblLevelName.TabIndex = 2
        Me.lblLevelName.Text = "Level Name"
        Me.lblLevelName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLevelCode
        '
        Me.lblLevelCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelCode.Location = New System.Drawing.Point(8, 36)
        Me.lblLevelCode.Name = "lblLevelCode"
        Me.lblLevelCode.Size = New System.Drawing.Size(100, 15)
        Me.lblLevelCode.TabIndex = 0
        Me.lblLevelCode.Text = "Level Code"
        Me.lblLevelCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 146)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(360, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(148, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(251, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'chkmandatory
        '
        Me.chkmandatory.AutoSize = True
        Me.chkmandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkmandatory.Location = New System.Drawing.Point(117, 104)
        Me.chkmandatory.Name = "chkmandatory"
        Me.chkmandatory.Size = New System.Drawing.Size(15, 14)
        Me.chkmandatory.TabIndex = 5
        Me.chkmandatory.UseVisualStyleBackColor = True
        '
        'lblMandatory
        '
        Me.lblMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMandatory.Location = New System.Drawing.Point(43, 103)
        Me.lblMandatory.Name = "lblMandatory"
        Me.lblMandatory.Size = New System.Drawing.Size(67, 15)
        Me.lblMandatory.TabIndex = 7
        Me.lblMandatory.Text = "Mandatory"
        Me.lblMandatory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmLeaveApproverLevel_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(360, 201)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeaveApproverLevel_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit  Approver Level"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        Me.gbApproverLevelInfo.ResumeLayout(False)
        Me.gbApproverLevelInfo.PerformLayout()
        CType(Me.nudEscalation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPriority, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbApproverLevelInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblLevelName As System.Windows.Forms.Label
    Friend WithEvents lblLevelCode As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPriority As System.Windows.Forms.Label
    Friend WithEvents nudPriority As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMandatory As System.Windows.Forms.Label
    Friend WithEvents chkmandatory As System.Windows.Forms.CheckBox
    Friend WithEvents lblPRemark As System.Windows.Forms.Label
    Friend WithEvents nudEscalation As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblEscalationDays As System.Windows.Forms.Label
End Class
