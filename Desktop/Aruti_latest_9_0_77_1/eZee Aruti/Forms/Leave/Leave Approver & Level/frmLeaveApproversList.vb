﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmLeaveApproversList

#Region "Private Variable"

    Private objLeaveApprover As clsleaveapprover_master
    Private ReadOnly mstrModuleName As String = "frmLeaveApproversList"
    Private mintUserMappunkid As Integer = -1

#End Region

#Region "Form's Event"

    Private Sub frmLeaveApproversList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeaveApprover = New clsleaveapprover_master
        Try

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)


            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            FillCombo()
            'Pinkal (20-Jan-2012) -- End



            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            If ConfigParameter._Object._IsLeaveApprover_ForLeaveType Then
                mnuLeaveType.Visible = True
            Else
                mnuLeaveType.Visible = False
            End If
            'Pinkal (06-Feb-2012) -- End


            Call SetVisibility()

            'S.SANDEEP [30 JAN 2016] -- START
            'fillList()
            'S.SANDEEP [30 JAN 2016] -- END

            If lvApproverList.Items.Count > 0 Then lvApproverList.Items(0).Selected = True
            lvApproverList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveApproversList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveApproversList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveApproversList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveApproversList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeaveApprover = Nothing
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleaveapprover_master.SetMessages()
            clsleaveapprover_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleaveapprover_master,clsleaveapprover_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmLeaveApprover_AddEdit
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvApproverList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApproverList.Select()
            Exit Sub
        End If
        Dim objfrmLeaveApprover_AddEdit As New frmLeaveApprover_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverList.SelectedItems(0).Index
            If objfrmLeaveApprover_AddEdit.displayDialog(CInt(lvApproverList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            objfrmLeaveApprover_AddEdit = Nothing

            lvApproverList.Items(intSelectedIndex).Selected = True
            lvApproverList.EnsureVisible(intSelectedIndex)
            lvApproverList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmLeaveApprover_AddEdit IsNot Nothing Then objfrmLeaveApprover_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvApproverList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApproverList.Select()
            Exit Sub
        End If
        If objLeaveApprover.isUsed(CInt(lvApproverList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver. Reason: This Approver is in use."), enMsgBoxStyle.Information) '?2
            lvApproverList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.LEAVE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objLeaveApprover._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objLeaveApprover._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objLeaveApprover._Voiduserunkid = User._Object._Userunkid

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objLeaveApprover.Delete(CInt(lvApproverList.SelectedItems(0).Tag))

'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objLeaveApprover._FormName = mstrModuleName
                objLeaveApprover._LoginEmployeeunkid = 0
                objLeaveApprover._ClientIP = getIP()
                objLeaveApprover._HostName = getHostName()
                objLeaveApprover._FromWeb = False
                objLeaveApprover._AuditUserId = User._Object._Userunkid
objLeaveApprover._CompanyUnkid = Company._Object._Companyunkid
                objLeaveApprover._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                'objLeaveApprover.Delete(CInt(lvApproverList.SelectedItems(0).Tag), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                objLeaveApprover.Delete(CInt(lvApproverList.SelectedItems(0).Tag), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), FinancialYear._Object._DatabaseName)
                'Pinkal (12-Oct-2020) -- End


                lvApproverList.SelectedItems(0).Remove()

                If lvApproverList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvApproverList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvApproverList.Items.Count - 1
                    lvApproverList.Items(intSelectedIndex).Selected = True
                    lvApproverList.EnsureVisible(intSelectedIndex)
                ElseIf lvApproverList.Items.Count <> 0 Then
                    lvApproverList.Items(intSelectedIndex).Selected = True
                    lvApproverList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvApproverList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes

    'Private Sub btnMapUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMapUser.Click
    '    Try
    '        If lvApproverList.SelectedItems.Count = 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
    '            lvApproverList.Select()
    '            Exit Sub
    '        End If
    '        Dim objFrm As New frmApprover_UserMapping
    '        If mintUserMappunkid > 0 Then
    '            objFrm.displayDialog(mintUserMappunkid, enAction.EDIT_ONE, CInt(lvApproverList.SelectedItems(0).Tag))
    '        Else
    '            objFrm.displayDialog(mintUserMappunkid, enAction.ADD_ONE, CInt(lvApproverList.SelectedItems(0).Tag))
    '        End If
    '        mintUserMappunkid = objFrm.mintMappingUnkid

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnMapUser_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (06-Feb-2012) -- End

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboApprover.DataSource, DataTable)
            With cboApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try

            'Pinkal (07-APR-2012) -- Start
            'Enhancement : TRA Changes
            cboLevel.SelectedIndex = 0
            'Pinkal (07-APR-2012) -- End

            cboApprover.SelectedIndex = 0

            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            cboStatus.SelectedValue = 1
            'Shani(17-Aug-2015) -- End

            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End


    'Pinkal (07-APR-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboLevel.DataSource, DataTable)
            With cboLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (07-APR-2012) -- End



#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsApproverList As New DataSet
        Dim strSearching As String = ""
        Try


            If User._Object.Privilege._AllowToViewLeaveApproverList = True Then

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'dsApproverList = objLeaveApprover.GetList("List", True, "False")


                If CInt(cboLevel.SelectedValue) > 0 Then
                    strSearching &= "AND lvleaveapprover_master.levelunkid = " & CInt(cboLevel.SelectedValue)
                End If

                If CInt(cboApprover.SelectedValue) > 0 Then
                    strSearching &= "AND lvleaveapprover_master.leaveapproverunkid = " & CInt(cboApprover.SelectedValue)
                End If


                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.

                'dsApproverList = objLeaveApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                '                                                               , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                '                                                               , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                               , True, ConfigParameter._Object._IsIncludeInactiveEmp, CBool(IIf(CInt(cboStatus.SelectedValue) = 1, True, False)), False, -1, Nothing _
                '                                                               , strSearching, "")


                dsApproverList = objLeaveApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                              , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                              , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                              , True, True, CBool(IIf(CInt(cboStatus.SelectedValue) = 1, True, False)), False, -1, Nothing _
                                                                              , strSearching, "")

                'Pinkal (06-Jan-2016) -- End

                'If strSearching.Length > 0 Then
                '    strSearching = strSearching.Substring(3)
                '    dtApprover = New DataView(dsApproverList.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable()
                'Else
                '    dtApprover = dsApproverList.Tables("List")
                'End If

                Dim lvItem As ListViewItem

                lvApproverList.Items.Clear()

                lvApproverList.BeginUpdate()
                ' For Each drRow As DataRow In dtApprover.Rows
                For Each drRow As DataRow In dsApproverList.Tables(0).Rows
                    'Pinkal (24-Aug-2015) -- End
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("code").ToString
                    lvItem.Tag = drRow("approverunkid")
                    lvItem.SubItems.Add(drRow("levelname").ToString)
                    lvItem.SubItems.Add(drRow("name").ToString)
                    lvItem.SubItems.Add(drRow("departmentname").ToString())
                    lvItem.SubItems.Add(drRow("sectionname").ToString())
                    lvItem.SubItems.Add(drRow("jobname").ToString())

                    'S.SANDEEP [30 JAN 2016] -- START
                    lvItem.SubItems.Add(drRow("ExAppr").ToString())
                    'S.SANDEEP [30 JAN 2016] -- END

                    lvApproverList.Items.Add(lvItem)
                Next

                lvApproverList.GroupingColumn = colhLevel
                lvApproverList.SortBy(colhLevel.Index, SortOrder.Ascending)
                lvApproverList.DisplayGroups(True)

                If lvApproverList.Items.Count > 16 Then
                    colhJob.Width = 160 - 20
                Else
                    colhJob.Width = 160
                End If

                'Shani(17-Aug-2015) -- Start
                'Leave Enhancement : Putting Activate Feature in Leave Approver Master
                If CInt(cboStatus.SelectedValue) = 1 Then
                    Call SetVisibility()
                Else
                    mnuMapUser.Enabled = False
                    mnuLeaveType.Enabled = False
                    mnuAssignIssueUser.Enabled = False
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                End If
                'Shani(17-Aug-2015) -- End

                lvApproverList.EndUpdate()
            End If


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsApproverList.Dispose()
        End Try

    End Sub

    'Pinkal (20-Jan-2012) -- End

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddLeaveApprover
            btnDelete.Enabled = User._Object.Privilege._DeleteLeaveApprover
            btnEdit.Enabled = User._Object.Privilege._EditLeaveApprover

            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            'btnMapUser.Enabled = User._Object.Privilege._AllowMapApproverWithUser

            'S.SANDEEP [30 JAN 2016] -- START
            'mnuMapUser.Enabled = User._Object.Privilege._AllowMapApproverWithUser
            mnuMapUser.Visible = False
            'S.SANDEEP [30 JAN 2016] -- END


            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            mnuLeaveType.Enabled = User._Object.Privilege._AllowtoMapLeaveType
            'Anjan (25 Oct 2012)-End 

            'Pinkal (06-Feb-2012) -- End


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            mnuAssignIssueUser.Enabled = User._Object.Privilege._AllowToAssignIssueUsertoEmp
            'Pinkal (18-Dec-2012) -- End


            'Pinkal (06-Feb-2012) -- End


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in Leave module.
            mnuImportLeaveApprovers.Enabled = User._Object.Privilege._AllowToImportLeaveApprovers

            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub FillCombo()
        Try
            'Pinkal (07-APR-2012) -- Start
            'Enhancement : TRA Changes
            Dim objLevel As New clsapproverlevel_master
            Dim dsList As DataSet = objLevel.getListForCombo("Level", True)
            cboLevel.DisplayMember = "name"
            cboLevel.ValueMember = "levelunkid"
            cboLevel.DataSource = dsList.Tables(0)

            'Pinkal (07-APR-2012) -- End

            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            Dim objMaster As New clsMasterData
            Dim dsCombo As DataSet = objMaster.getComboListTranHeadActiveInActive("List", False)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 1
            End With
            'Shani(17-Aug-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End


#End Region

#Region "ListView Event"

    Private Sub lvApproverList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvApproverList.SelectedIndexChanged
        Dim blnflag As Boolean
        Try
            If lvApproverList.SelectedItems.Count = 0 Then Exit Sub
            Dim objUserMapping As New clsapprover_Usermapping
            blnflag = objUserMapping.isExist(enUserType.Approver, CInt(lvApproverList.SelectedItems(0).Tag))

            If blnflag Then

                Dim dsFill As DataSet = objUserMapping.GetList("List")
                Dim dtFill As DataTable = New DataView(dsFill.Tables("List"), "approverunkid=" & CInt(lvApproverList.SelectedItems(0).Tag) & " AND usertypeid =  " & enUserType.Approver, "", DataViewRowState.CurrentRows).ToTable
                If dtFill.Rows.Count > 0 Then
                    mintUserMappunkid = CInt(dtFill.Rows(0)("mappingunkid"))
                Else
                    mintUserMappunkid = 0
                End If
            Else
                mintUserMappunkid = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvApproverList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Menu Event"

    Private Sub mnuMapUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMapUser.Click
        Try
            If lvApproverList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvApproverList.Select()
                Exit Sub
            End If
            Dim objFrm As New frmApprover_UserMapping
            If mintUserMappunkid > 0 Then
                objFrm.displayDialog(mintUserMappunkid, enAction.EDIT_ONE, CInt(lvApproverList.SelectedItems(0).Tag))
            Else
                objFrm.displayDialog(mintUserMappunkid, enAction.ADD_ONE, CInt(lvApproverList.SelectedItems(0).Tag))
            End If
            mintUserMappunkid = objFrm.mintMappingUnkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuMapUser_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLeaveType.Click
        Try
            If lvApproverList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvApproverList.Select()
                Exit Sub
            End If
            Dim objFrm As New frmApprover_leavetypeMapping
            objFrm.displayDialog(-1, enAction.ADD_ONE, CInt(lvApproverList.SelectedItems(0).Tag))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuLeaveType_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuAssignIssueUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssignIssueUser.Click
        Try
            Dim objFrm As New frmAssignIssueUserList
            objFrm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAssignIssueUser_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuImportLeaveApprovers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportLeaveApprovers.Click
        Dim frm As New frmLeaveApprImportWizard
        Try
            frm.ShowDialog()
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportLeaveApprovers_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'Pinkal (17-Jun-2015) -- Start
    'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.

    Private Sub mnuInActiveApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuInActiveApprover.Click
        Try
            If lvApproverList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvApproverList.Select()
                Exit Sub
            End If

            Dim objLeavForm As New clsleaveform
            If objLeavForm.GetApproverPendingLeaveFormCount(CInt(lvApproverList.SelectedItems(0).Tag), "") > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You cannot inactivate this approver.Reason :This Approver has Pending Leave Application Form(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            objLeavForm = Nothing


            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            'If objLeaveApprover.InActiveApprover(CInt(lvApproverList.SelectedItems(0).Tag), Company._Object._Companyunkid) = False Then

'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objLeaveApprover._FormName = mstrModuleName
            objLeaveApprover._LoginEmployeeunkid = 0
            objLeaveApprover._ClientIP = getIP()
            objLeaveApprover._HostName = getHostName()
            objLeaveApprover._FromWeb = False
            objLeaveApprover._AuditUserId = User._Object._Userunkid
objLeaveApprover._CompanyUnkid = Company._Object._Companyunkid
            objLeaveApprover._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objLeaveApprover.InActiveApprover(CInt(lvApproverList.SelectedItems(0).Tag), Company._Object._Companyunkid, False) = False Then
                'Shani(17-Aug-2015) -- End
                eZeeMsgBox.Show(objLeaveApprover._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                fillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuInActiveApprover_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (17-Jun-2015) -- End

    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
    Private Sub mnuActiveApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuActiveApprover.Click
        Try
            If lvApproverList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvApproverList.Select()
                Exit Sub
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objLeaveApprover._FormName = mstrModuleName
            objLeaveApprover._LoginEmployeeunkid = 0
            objLeaveApprover._ClientIP = getIP()
            objLeaveApprover._HostName = getHostName()
            objLeaveApprover._FromWeb = False
            objLeaveApprover._AuditUserId = User._Object._Userunkid
objLeaveApprover._CompanyUnkid = Company._Object._Companyunkid
            objLeaveApprover._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objLeaveApprover.InActiveApprover(CInt(lvApproverList.SelectedItems(0).Tag), Company._Object._Companyunkid, True) = False Then
                eZeeMsgBox.Show(objLeaveApprover._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                fillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuActiveApprover_Click", mstrModuleName)
        End Try
    End Sub
    'Shani(17-Aug-2015) -- End

#End Region

#Region "Dropdown Event"

    Private Sub cboLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLevel.SelectedIndexChanged
        Try

            Dim objApprover As New clsleaveapprover_master

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'Dim dsList As DataSet = objApprover.GetList("List", True)
            Dim dsList As DataSet = objApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                              , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                              , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                              , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, False, -1 _
                                                                              , Nothing, "", "")
            'Pinkal (24-Aug-2015) -- End



            Dim dtTable As DataTable = Nothing
            If CInt(cboLevel.SelectedValue) > 0 Then
                dtTable = New DataView(dsList.Tables(0), "levelunkid = " & CInt(cboLevel.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
            Else
                'S.SANDEEP [30 JAN 2016] -- START
                'dtTable = dsList.Tables(0).DefaultView.ToTable(True, "leaveapproverunkid", "name")
                dtTable = dsList.Tables(0).DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")
                'S.SANDEEP [30 JAN 2016] -- END
            End If

            Dim dr As DataRow = dtTable.NewRow
            dr("leaveapproverunkid") = 0
            dr("name") = Language.getMessage(mstrModuleName, 4, "Select")
            'S.SANDEEP [30 JAN 2016] -- START
            dr("isexternalapprover") = False
            'S.SANDEEP [30 JAN 2016] -- END
            dtTable.Rows.InsertAt(dr, 0)

            cboApprover.DisplayMember = "name"
            cboApprover.ValueMember = "leaveapproverunkid"
            cboApprover.DataSource = dtTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub


    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboStatus.SelectedValue) = 1 Then
                mnuInActiveApprover.Enabled = User._Object.Privilege._AllowtoSetLeaveInactiveApprover
                mnuActiveApprover.Enabled = False
            ElseIf CInt(cboStatus.SelectedValue) = 2 Then
                mnuInActiveApprover.Enabled = False
                mnuActiveApprover.Enabled = User._Object.Privilege._AllowtoSetLeaveActiveApprover
            Else
                mnuInActiveApprover.Enabled = False
                mnuActiveApprover.Enabled = False
            End If
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Shani(17-Aug-2015) -- End

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOpearation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpearation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhApproveCode.Text = Language._Object.getCaption(CStr(Me.colhApproveCode.Tag), Me.colhApproveCode.Text)
            Me.colhApproverName.Text = Language._Object.getCaption(CStr(Me.colhApproverName.Tag), Me.colhApproverName.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.colhSection.Text = Language._Object.getCaption(CStr(Me.colhSection.Tag), Me.colhSection.Text)
            Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.btnOpearation.Text = Language._Object.getCaption(Me.btnOpearation.Name, Me.btnOpearation.Text)
            Me.mnuMapUser.Text = Language._Object.getCaption(Me.mnuMapUser.Name, Me.mnuMapUser.Text)
            Me.mnuLeaveType.Text = Language._Object.getCaption(Me.mnuLeaveType.Name, Me.mnuLeaveType.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
            Me.mnuAssignIssueUser.Text = Language._Object.getCaption(Me.mnuAssignIssueUser.Name, Me.mnuAssignIssueUser.Text)
            Me.mnuImportLeaveApprovers.Text = Language._Object.getCaption(Me.mnuImportLeaveApprovers.Name, Me.mnuImportLeaveApprovers.Text)
            Me.mnuInActiveApprover.Text = Language._Object.getCaption(Me.mnuInActiveApprover.Name, Me.mnuInActiveApprover.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.mnuActiveApprover.Text = Language._Object.getCaption(Me.mnuActiveApprover.Name, Me.mnuActiveApprover.Text)
            Me.colhIsExternalApprover.Text = Language._Object.getCaption(CStr(Me.colhIsExternalApprover.Tag), Me.colhIsExternalApprover.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver. Reason: This Approver is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "You cannot inactivate this approver.Reason :This Approver has Pending Leave Application Form(s).")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class