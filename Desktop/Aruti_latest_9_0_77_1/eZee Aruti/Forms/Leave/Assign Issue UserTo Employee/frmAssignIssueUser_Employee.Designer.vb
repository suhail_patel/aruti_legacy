﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssignIssueUser_Employee
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssignIssueUser_Employee))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbApproversInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboToIssueUser = New System.Windows.Forms.ComboBox
        Me.LblToIssueUser = New System.Windows.Forms.Label
        Me.objbtnSearchToIssueUser = New eZee.Common.eZeeGradientButton
        Me.cboIssueUser = New System.Windows.Forms.ComboBox
        Me.lblIssueUser = New System.Windows.Forms.Label
        Me.objbtnSearchIssueUser = New eZee.Common.eZeeGradientButton
        Me.gbSelectedEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboFilterJob = New System.Windows.Forms.ComboBox
        Me.lblFilterjob = New System.Windows.Forms.Label
        Me.objelLine3 = New eZee.Common.eZeeLine
        Me.cboFilterSection = New System.Windows.Forms.ComboBox
        Me.lblFilterSection = New System.Windows.Forms.Label
        Me.cboeFilterDept = New System.Windows.Forms.ComboBox
        Me.lblFilterDepartment = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.pnlSelectedEmployee = New System.Windows.Forms.Panel
        Me.chkAllSelectedEmployee = New System.Windows.Forms.CheckBox
        Me.lvSelectedEmployee = New eZee.Common.eZeeListView(Me.components)
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhIssueUser = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhDept = New System.Windows.Forms.ColumnHeader
        Me.colhSection = New System.Windows.Forms.ColumnHeader
        Me.colhJob = New System.Windows.Forms.ColumnHeader
        Me.colhGUID = New System.Windows.Forms.ColumnHeader
        Me.colhemployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.gbEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSelectedEmployee = New eZee.Common.eZeeGradientButton
        Me.txtSearchedEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.objAlloacationReset = New eZee.Common.eZeeGradientButton
        Me.chkEmployeeAll = New System.Windows.Forms.CheckBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.lvEmployee = New System.Windows.Forms.ListView
        Me.ColhEmployeecode = New System.Windows.Forms.ColumnHeader
        Me.ColhEmp = New System.Windows.Forms.ColumnHeader
        Me.objcolhDepartment = New System.Windows.Forms.ColumnHeader
        Me.objColhSection = New System.Windows.Forms.ColumnHeader
        Me.objColhJob = New System.Windows.Forms.ColumnHeader
        Me.objcolhDepartmentID = New System.Windows.Forms.ColumnHeader
        Me.objColhSectionID = New System.Windows.Forms.ColumnHeader
        Me.objColhJobID = New System.Windows.Forms.ColumnHeader
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.tvLevels = New System.Windows.Forms.TreeView
        Me.lblApprovalTo = New System.Windows.Forms.Label
        Me.cboApprovalTo = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbApproversInfo.SuspendLayout()
        Me.gbSelectedEmployee.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSelectedEmployee.SuspendLayout()
        Me.gbEmployee.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbApproversInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(945, 548)
        Me.pnlMainInfo.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(945, 60)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Assign Issue User to Employee"
        '
        'gbApproversInfo
        '
        Me.gbApproversInfo.BorderColor = System.Drawing.Color.Black
        Me.gbApproversInfo.Checked = False
        Me.gbApproversInfo.CollapseAllExceptThis = False
        Me.gbApproversInfo.CollapsedHoverImage = Nothing
        Me.gbApproversInfo.CollapsedNormalImage = Nothing
        Me.gbApproversInfo.CollapsedPressedImage = Nothing
        Me.gbApproversInfo.CollapseOnLoad = False
        Me.gbApproversInfo.Controls.Add(Me.cboToIssueUser)
        Me.gbApproversInfo.Controls.Add(Me.LblToIssueUser)
        Me.gbApproversInfo.Controls.Add(Me.objbtnSearchToIssueUser)
        Me.gbApproversInfo.Controls.Add(Me.cboIssueUser)
        Me.gbApproversInfo.Controls.Add(Me.lblIssueUser)
        Me.gbApproversInfo.Controls.Add(Me.objbtnSearchIssueUser)
        Me.gbApproversInfo.Controls.Add(Me.gbSelectedEmployee)
        Me.gbApproversInfo.Controls.Add(Me.gbEmployee)
        Me.gbApproversInfo.Controls.Add(Me.lblApprovalTo)
        Me.gbApproversInfo.Controls.Add(Me.cboApprovalTo)
        Me.gbApproversInfo.ExpandedHoverImage = Nothing
        Me.gbApproversInfo.ExpandedNormalImage = Nothing
        Me.gbApproversInfo.ExpandedPressedImage = Nothing
        Me.gbApproversInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApproversInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApproversInfo.HeaderHeight = 25
        Me.gbApproversInfo.HeaderMessage = ""
        Me.gbApproversInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbApproversInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApproversInfo.HeightOnCollapse = 0
        Me.gbApproversInfo.LeftTextSpace = 0
        Me.gbApproversInfo.Location = New System.Drawing.Point(10, 65)
        Me.gbApproversInfo.Name = "gbApproversInfo"
        Me.gbApproversInfo.OpenHeight = 300
        Me.gbApproversInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApproversInfo.ShowBorder = True
        Me.gbApproversInfo.ShowCheckBox = False
        Me.gbApproversInfo.ShowCollapseButton = False
        Me.gbApproversInfo.ShowDefaultBorderColor = True
        Me.gbApproversInfo.ShowDownButton = False
        Me.gbApproversInfo.ShowHeader = True
        Me.gbApproversInfo.Size = New System.Drawing.Size(926, 424)
        Me.gbApproversInfo.TabIndex = 0
        Me.gbApproversInfo.Temp = 0
        Me.gbApproversInfo.Text = "Approvers Info"
        Me.gbApproversInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToIssueUser
        '
        Me.cboToIssueUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToIssueUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToIssueUser.FormattingEnabled = True
        Me.cboToIssueUser.Location = New System.Drawing.Point(115, 60)
        Me.cboToIssueUser.Name = "cboToIssueUser"
        Me.cboToIssueUser.Size = New System.Drawing.Size(205, 21)
        Me.cboToIssueUser.TabIndex = 222
        Me.cboToIssueUser.Visible = False
        '
        'LblToIssueUser
        '
        Me.LblToIssueUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToIssueUser.Location = New System.Drawing.Point(8, 63)
        Me.LblToIssueUser.Name = "LblToIssueUser"
        Me.LblToIssueUser.Size = New System.Drawing.Size(97, 15)
        Me.LblToIssueUser.TabIndex = 220
        Me.LblToIssueUser.Text = "To Issue User"
        Me.LblToIssueUser.Visible = False
        '
        'objbtnSearchToIssueUser
        '
        Me.objbtnSearchToIssueUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchToIssueUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchToIssueUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchToIssueUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchToIssueUser.BorderSelected = False
        Me.objbtnSearchToIssueUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchToIssueUser.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchToIssueUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchToIssueUser.Location = New System.Drawing.Point(325, 60)
        Me.objbtnSearchToIssueUser.Name = "objbtnSearchToIssueUser"
        Me.objbtnSearchToIssueUser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchToIssueUser.TabIndex = 221
        Me.objbtnSearchToIssueUser.Visible = False
        '
        'cboIssueUser
        '
        Me.cboIssueUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIssueUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIssueUser.FormattingEnabled = True
        Me.cboIssueUser.Location = New System.Drawing.Point(115, 31)
        Me.cboIssueUser.Name = "cboIssueUser"
        Me.cboIssueUser.Size = New System.Drawing.Size(205, 21)
        Me.cboIssueUser.TabIndex = 218
        '
        'lblIssueUser
        '
        Me.lblIssueUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIssueUser.Location = New System.Drawing.Point(8, 34)
        Me.lblIssueUser.Name = "lblIssueUser"
        Me.lblIssueUser.Size = New System.Drawing.Size(97, 15)
        Me.lblIssueUser.TabIndex = 2
        Me.lblIssueUser.Text = "Issue User"
        '
        'objbtnSearchIssueUser
        '
        Me.objbtnSearchIssueUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchIssueUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchIssueUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchIssueUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchIssueUser.BorderSelected = False
        Me.objbtnSearchIssueUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchIssueUser.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchIssueUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchIssueUser.Location = New System.Drawing.Point(325, 31)
        Me.objbtnSearchIssueUser.Name = "objbtnSearchIssueUser"
        Me.objbtnSearchIssueUser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchIssueUser.TabIndex = 93
        '
        'gbSelectedEmployee
        '
        Me.gbSelectedEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbSelectedEmployee.Checked = True
        Me.gbSelectedEmployee.CollapseAllExceptThis = False
        Me.gbSelectedEmployee.CollapsedHoverImage = Nothing
        Me.gbSelectedEmployee.CollapsedNormalImage = Nothing
        Me.gbSelectedEmployee.CollapsedPressedImage = Nothing
        Me.gbSelectedEmployee.CollapseOnLoad = False
        Me.gbSelectedEmployee.Controls.Add(Me.cboFilterJob)
        Me.gbSelectedEmployee.Controls.Add(Me.lblFilterjob)
        Me.gbSelectedEmployee.Controls.Add(Me.objelLine3)
        Me.gbSelectedEmployee.Controls.Add(Me.cboFilterSection)
        Me.gbSelectedEmployee.Controls.Add(Me.lblFilterSection)
        Me.gbSelectedEmployee.Controls.Add(Me.cboeFilterDept)
        Me.gbSelectedEmployee.Controls.Add(Me.lblFilterDepartment)
        Me.gbSelectedEmployee.Controls.Add(Me.objbtnReset)
        Me.gbSelectedEmployee.Controls.Add(Me.objbtnSearch)
        Me.gbSelectedEmployee.Controls.Add(Me.btnDelete)
        Me.gbSelectedEmployee.Controls.Add(Me.objelLine2)
        Me.gbSelectedEmployee.Controls.Add(Me.pnlSelectedEmployee)
        Me.gbSelectedEmployee.ExpandedHoverImage = Nothing
        Me.gbSelectedEmployee.ExpandedNormalImage = Nothing
        Me.gbSelectedEmployee.ExpandedPressedImage = Nothing
        Me.gbSelectedEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSelectedEmployee.HeaderHeight = 25
        Me.gbSelectedEmployee.HeaderMessage = ""
        Me.gbSelectedEmployee.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSelectedEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSelectedEmployee.HeightOnCollapse = 0
        Me.gbSelectedEmployee.LeftTextSpace = 0
        Me.gbSelectedEmployee.Location = New System.Drawing.Point(352, 33)
        Me.gbSelectedEmployee.Name = "gbSelectedEmployee"
        Me.gbSelectedEmployee.OpenHeight = 300
        Me.gbSelectedEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSelectedEmployee.ShowBorder = True
        Me.gbSelectedEmployee.ShowCheckBox = False
        Me.gbSelectedEmployee.ShowCollapseButton = False
        Me.gbSelectedEmployee.ShowDefaultBorderColor = True
        Me.gbSelectedEmployee.ShowDownButton = False
        Me.gbSelectedEmployee.ShowHeader = True
        Me.gbSelectedEmployee.Size = New System.Drawing.Size(561, 386)
        Me.gbSelectedEmployee.TabIndex = 1
        Me.gbSelectedEmployee.Temp = 0
        Me.gbSelectedEmployee.Text = "Selected Employee"
        Me.gbSelectedEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFilterJob
        '
        Me.cboFilterJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterJob.DropDownWidth = 300
        Me.cboFilterJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilterJob.FormattingEnabled = True
        Me.cboFilterJob.Location = New System.Drawing.Point(432, 33)
        Me.cboFilterJob.Name = "cboFilterJob"
        Me.cboFilterJob.Size = New System.Drawing.Size(104, 21)
        Me.cboFilterJob.TabIndex = 108
        '
        'lblFilterjob
        '
        Me.lblFilterjob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilterjob.Location = New System.Drawing.Point(381, 36)
        Me.lblFilterjob.Name = "lblFilterjob"
        Me.lblFilterjob.Size = New System.Drawing.Size(48, 15)
        Me.lblFilterjob.TabIndex = 107
        Me.lblFilterjob.Text = "Job"
        Me.lblFilterjob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine3
        '
        Me.objelLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine3.Location = New System.Drawing.Point(11, 58)
        Me.objelLine3.Name = "objelLine3"
        Me.objelLine3.Size = New System.Drawing.Size(542, 11)
        Me.objelLine3.TabIndex = 105
        Me.objelLine3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFilterSection
        '
        Me.cboFilterSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterSection.DropDownWidth = 300
        Me.cboFilterSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilterSection.FormattingEnabled = True
        Me.cboFilterSection.Location = New System.Drawing.Point(266, 33)
        Me.cboFilterSection.Name = "cboFilterSection"
        Me.cboFilterSection.Size = New System.Drawing.Size(104, 21)
        Me.cboFilterSection.TabIndex = 104
        '
        'lblFilterSection
        '
        Me.lblFilterSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilterSection.Location = New System.Drawing.Point(202, 36)
        Me.lblFilterSection.Name = "lblFilterSection"
        Me.lblFilterSection.Size = New System.Drawing.Size(58, 15)
        Me.lblFilterSection.TabIndex = 103
        Me.lblFilterSection.Text = "Section"
        Me.lblFilterSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboeFilterDept
        '
        Me.cboeFilterDept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboeFilterDept.DropDownWidth = 300
        Me.cboeFilterDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboeFilterDept.FormattingEnabled = True
        Me.cboeFilterDept.Location = New System.Drawing.Point(89, 33)
        Me.cboeFilterDept.Name = "cboeFilterDept"
        Me.cboeFilterDept.Size = New System.Drawing.Size(104, 21)
        Me.cboeFilterDept.TabIndex = 102
        '
        'lblFilterDepartment
        '
        Me.lblFilterDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilterDepartment.Location = New System.Drawing.Point(8, 36)
        Me.lblFilterDepartment.Name = "lblFilterDepartment"
        Me.lblFilterDepartment.Size = New System.Drawing.Size(75, 15)
        Me.lblFilterDepartment.TabIndex = 101
        Me.lblFilterDepartment.Text = "Department"
        Me.lblFilterDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(534, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 71
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(510, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 70
        Me.objbtnSearch.TabStop = False
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(464, 348)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(89, 29)
        Me.btnDelete.TabIndex = 1
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(6, 339)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(547, 6)
        Me.objelLine2.TabIndex = 11
        '
        'pnlSelectedEmployee
        '
        Me.pnlSelectedEmployee.AutoSize = True
        Me.pnlSelectedEmployee.Controls.Add(Me.chkAllSelectedEmployee)
        Me.pnlSelectedEmployee.Controls.Add(Me.lvSelectedEmployee)
        Me.pnlSelectedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlSelectedEmployee.Location = New System.Drawing.Point(7, 72)
        Me.pnlSelectedEmployee.Name = "pnlSelectedEmployee"
        Me.pnlSelectedEmployee.Size = New System.Drawing.Size(549, 258)
        Me.pnlSelectedEmployee.TabIndex = 2
        '
        'chkAllSelectedEmployee
        '
        Me.chkAllSelectedEmployee.Location = New System.Drawing.Point(7, 6)
        Me.chkAllSelectedEmployee.Name = "chkAllSelectedEmployee"
        Me.chkAllSelectedEmployee.Size = New System.Drawing.Size(13, 14)
        Me.chkAllSelectedEmployee.TabIndex = 17
        Me.chkAllSelectedEmployee.UseVisualStyleBackColor = True
        '
        'lvSelectedEmployee
        '
        Me.lvSelectedEmployee.BackColorOnChecked = True
        Me.lvSelectedEmployee.CheckBoxes = True
        Me.lvSelectedEmployee.ColumnHeaders = Nothing
        Me.lvSelectedEmployee.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhIssueUser, Me.colhEmployee, Me.colhDept, Me.colhSection, Me.colhJob, Me.colhGUID, Me.colhemployeeunkid})
        Me.lvSelectedEmployee.CompulsoryColumns = ""
        Me.lvSelectedEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvSelectedEmployee.FullRowSelect = True
        Me.lvSelectedEmployee.GridLines = True
        Me.lvSelectedEmployee.GroupingColumn = Nothing
        Me.lvSelectedEmployee.HideSelection = False
        Me.lvSelectedEmployee.Location = New System.Drawing.Point(0, 0)
        Me.lvSelectedEmployee.MinColumnWidth = 50
        Me.lvSelectedEmployee.MultiSelect = False
        Me.lvSelectedEmployee.Name = "lvSelectedEmployee"
        Me.lvSelectedEmployee.OptionalColumns = ""
        Me.lvSelectedEmployee.ShowMoreItem = False
        Me.lvSelectedEmployee.ShowSaveItem = False
        Me.lvSelectedEmployee.ShowSelectAll = True
        Me.lvSelectedEmployee.ShowSizeAllColumnsToFit = True
        Me.lvSelectedEmployee.Size = New System.Drawing.Size(549, 258)
        Me.lvSelectedEmployee.Sortable = True
        Me.lvSelectedEmployee.TabIndex = 0
        Me.lvSelectedEmployee.UseCompatibleStateImageBehavior = False
        Me.lvSelectedEmployee.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Tag = "colhCheck"
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 20
        '
        'colhIssueUser
        '
        Me.colhIssueUser.Tag = "colhIssueUser"
        Me.colhIssueUser.Text = "Issue User"
        Me.colhIssueUser.Width = 90
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 150
        '
        'colhDept
        '
        Me.colhDept.Tag = "colhDept"
        Me.colhDept.Text = "Department"
        Me.colhDept.Width = 105
        '
        'colhSection
        '
        Me.colhSection.Tag = "colhSection"
        Me.colhSection.Text = "Section"
        Me.colhSection.Width = 90
        '
        'colhJob
        '
        Me.colhJob.Tag = "colhJob"
        Me.colhJob.Text = "Job"
        Me.colhJob.Width = 95
        '
        'colhGUID
        '
        Me.colhGUID.Tag = "colhGUID"
        Me.colhGUID.Text = "GUID"
        Me.colhGUID.Width = 0
        '
        'colhemployeeunkid
        '
        Me.colhemployeeunkid.Tag = "colhemployeeunkid"
        Me.colhemployeeunkid.Text = "Employeeunkid"
        Me.colhemployeeunkid.Width = 0
        '
        'gbEmployee
        '
        Me.gbEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbEmployee.Checked = False
        Me.gbEmployee.CollapseAllExceptThis = False
        Me.gbEmployee.CollapsedHoverImage = Nothing
        Me.gbEmployee.CollapsedNormalImage = Nothing
        Me.gbEmployee.CollapsedPressedImage = Nothing
        Me.gbEmployee.CollapseOnLoad = False
        Me.gbEmployee.Controls.Add(Me.objbtnSelectedEmployee)
        Me.gbEmployee.Controls.Add(Me.txtSearchedEmployee)
        Me.gbEmployee.Controls.Add(Me.objAlloacationReset)
        Me.gbEmployee.Controls.Add(Me.chkEmployeeAll)
        Me.gbEmployee.Controls.Add(Me.lnkAllocation)
        Me.gbEmployee.Controls.Add(Me.lvEmployee)
        Me.gbEmployee.Controls.Add(Me.objelLine1)
        Me.gbEmployee.Controls.Add(Me.btnAdd)
        Me.gbEmployee.Controls.Add(Me.tvLevels)
        Me.gbEmployee.ExpandedHoverImage = Nothing
        Me.gbEmployee.ExpandedNormalImage = Nothing
        Me.gbEmployee.ExpandedPressedImage = Nothing
        Me.gbEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployee.HeaderHeight = 25
        Me.gbEmployee.HeaderMessage = ""
        Me.gbEmployee.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployee.HeightOnCollapse = 0
        Me.gbEmployee.LeftTextSpace = 0
        Me.gbEmployee.Location = New System.Drawing.Point(11, 87)
        Me.gbEmployee.Name = "gbEmployee"
        Me.gbEmployee.OpenHeight = 300
        Me.gbEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployee.ShowBorder = True
        Me.gbEmployee.ShowCheckBox = False
        Me.gbEmployee.ShowCollapseButton = False
        Me.gbEmployee.ShowDefaultBorderColor = True
        Me.gbEmployee.ShowDownButton = False
        Me.gbEmployee.ShowHeader = True
        Me.gbEmployee.Size = New System.Drawing.Size(335, 328)
        Me.gbEmployee.TabIndex = 0
        Me.gbEmployee.Temp = 0
        Me.gbEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSelectedEmployee
        '
        Me.objbtnSelectedEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSelectedEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSelectedEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSelectedEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSelectedEmployee.BorderSelected = False
        Me.objbtnSelectedEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSelectedEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSelectedEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSelectedEmployee.Location = New System.Drawing.Point(210, 294)
        Me.objbtnSelectedEmployee.Name = "objbtnSelectedEmployee"
        Me.objbtnSelectedEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSelectedEmployee.TabIndex = 239
        '
        'txtSearchedEmployee
        '
        Me.txtSearchedEmployee.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchedEmployee.Flags = 0
        Me.txtSearchedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchedEmployee.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSearchedEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearchedEmployee.Location = New System.Drawing.Point(8, 293)
        Me.txtSearchedEmployee.Name = "txtSearchedEmployee"
        Me.txtSearchedEmployee.ReadOnly = True
        Me.txtSearchedEmployee.Size = New System.Drawing.Size(198, 21)
        Me.txtSearchedEmployee.TabIndex = 218
        '
        'objAlloacationReset
        '
        Me.objAlloacationReset.BackColor = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objAlloacationReset.BorderSelected = False
        Me.objAlloacationReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objAlloacationReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objAlloacationReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objAlloacationReset.Location = New System.Drawing.Point(311, 2)
        Me.objAlloacationReset.Name = "objAlloacationReset"
        Me.objAlloacationReset.Size = New System.Drawing.Size(21, 21)
        Me.objAlloacationReset.TabIndex = 237
        '
        'chkEmployeeAll
        '
        Me.chkEmployeeAll.AutoSize = True
        Me.chkEmployeeAll.BackColor = System.Drawing.Color.Transparent
        Me.chkEmployeeAll.Location = New System.Drawing.Point(16, 5)
        Me.chkEmployeeAll.Name = "chkEmployeeAll"
        Me.chkEmployeeAll.Size = New System.Drawing.Size(78, 17)
        Me.chkEmployeeAll.TabIndex = 16
        Me.chkEmployeeAll.Text = "Select All"
        Me.chkEmployeeAll.UseVisualStyleBackColor = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(224, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 15)
        Me.lnkAllocation.TabIndex = 235
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'lvEmployee
        '
        Me.lvEmployee.CheckBoxes = True
        Me.lvEmployee.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColhEmployeecode, Me.ColhEmp, Me.objcolhDepartment, Me.objColhSection, Me.objColhJob, Me.objcolhDepartmentID, Me.objColhSectionID, Me.objColhJobID})
        Me.lvEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvEmployee.FullRowSelect = True
        Me.lvEmployee.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEmployee.HideSelection = False
        Me.lvEmployee.Location = New System.Drawing.Point(8, 31)
        Me.lvEmployee.Name = "lvEmployee"
        Me.lvEmployee.Size = New System.Drawing.Size(318, 244)
        Me.lvEmployee.TabIndex = 12
        Me.lvEmployee.UseCompatibleStateImageBehavior = False
        Me.lvEmployee.View = System.Windows.Forms.View.Details
        '
        'ColhEmployeecode
        '
        Me.ColhEmployeecode.Tag = "ColhEmployeecode"
        Me.ColhEmployeecode.Text = "Employee Code"
        Me.ColhEmployeecode.Width = 100
        '
        'ColhEmp
        '
        Me.ColhEmp.Tag = "ColhEmp"
        Me.ColhEmp.Text = "Employee"
        Me.ColhEmp.Width = 210
        '
        'objcolhDepartment
        '
        Me.objcolhDepartment.Tag = "objcolhDepartment"
        Me.objcolhDepartment.Text = "Department"
        Me.objcolhDepartment.Width = 0
        '
        'objColhSection
        '
        Me.objColhSection.Tag = "objColhSection"
        Me.objColhSection.Text = "Section"
        Me.objColhSection.Width = 0
        '
        'objColhJob
        '
        Me.objColhJob.Tag = "objColhJob"
        Me.objColhJob.Text = "Job"
        Me.objColhJob.Width = 0
        '
        'objcolhDepartmentID
        '
        Me.objcolhDepartmentID.Tag = "objcolhDepartmentID"
        Me.objcolhDepartmentID.Text = "DepartmentID"
        Me.objcolhDepartmentID.Width = 0
        '
        'objColhSectionID
        '
        Me.objColhSectionID.Tag = "objColhSectionID"
        Me.objColhSectionID.Text = "SectionID"
        Me.objColhSectionID.Width = 0
        '
        'objColhJobID
        '
        Me.objColhJobID.Tag = "objColhJobID"
        Me.objColhJobID.Text = "JobID"
        Me.objColhJobID.Width = 0
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(8, 281)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(315, 6)
        Me.objelLine1.TabIndex = 10
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(237, 290)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(89, 29)
        Me.btnAdd.TabIndex = 2
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'tvLevels
        '
        Me.tvLevels.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvLevels.Location = New System.Drawing.Point(12, 33)
        Me.tvLevels.Name = "tvLevels"
        Me.tvLevels.Size = New System.Drawing.Size(139, 160)
        Me.tvLevels.TabIndex = 0
        '
        'lblApprovalTo
        '
        Me.lblApprovalTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalTo.Location = New System.Drawing.Point(8, 167)
        Me.lblApprovalTo.Name = "lblApprovalTo"
        Me.lblApprovalTo.Size = New System.Drawing.Size(67, 15)
        Me.lblApprovalTo.TabIndex = 98
        Me.lblApprovalTo.Text = "Approval To"
        Me.lblApprovalTo.Visible = False
        '
        'cboApprovalTo
        '
        Me.cboApprovalTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprovalTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprovalTo.FormattingEnabled = True
        Me.cboApprovalTo.Location = New System.Drawing.Point(81, 164)
        Me.cboApprovalTo.Name = "cboApprovalTo"
        Me.cboApprovalTo.Size = New System.Drawing.Size(165, 21)
        Me.cboApprovalTo.TabIndex = 6
        Me.cboApprovalTo.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 493)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(945, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(733, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(836, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmAssignIssueUser_Employee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(945, 548)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssignIssueUser_Employee"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assign Issue User to Employee "
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbApproversInfo.ResumeLayout(False)
        Me.gbSelectedEmployee.ResumeLayout(False)
        Me.gbSelectedEmployee.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSelectedEmployee.ResumeLayout(False)
        Me.gbEmployee.ResumeLayout(False)
        Me.gbEmployee.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbApproversInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblIssueUser As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchIssueUser As eZee.Common.eZeeGradientButton
    Friend WithEvents lblApprovalTo As System.Windows.Forms.Label
    Friend WithEvents cboApprovalTo As System.Windows.Forms.ComboBox
    Friend WithEvents gbSelectedEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tvLevels As System.Windows.Forms.TreeView
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents pnlSelectedEmployee As System.Windows.Forms.Panel
    Friend WithEvents lvSelectedEmployee As eZee.Common.eZeeListView
    Friend WithEvents colhDept As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSection As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents cboFilterSection As System.Windows.Forms.ComboBox
    Friend WithEvents lblFilterSection As System.Windows.Forms.Label
    Friend WithEvents cboeFilterDept As System.Windows.Forms.ComboBox
    Friend WithEvents lblFilterDepartment As System.Windows.Forms.Label
    Friend WithEvents objelLine3 As eZee.Common.eZeeLine
    Friend WithEvents lvEmployee As System.Windows.Forms.ListView
    Friend WithEvents ColhEmployeecode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkEmployeeAll As System.Windows.Forms.CheckBox
    Friend WithEvents chkAllSelectedEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents colhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhemployeeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJob As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboFilterJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblFilterjob As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents ColhEmp As System.Windows.Forms.ColumnHeader
    Friend WithEvents objAlloacationReset As eZee.Common.eZeeGradientButton
    Friend WithEvents objcolhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhSection As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhJob As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDepartmentID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhSectionID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhJobID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSelectedEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents txtSearchedEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboIssueUser As System.Windows.Forms.ComboBox
    Friend WithEvents colhIssueUser As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboToIssueUser As System.Windows.Forms.ComboBox
    Friend WithEvents LblToIssueUser As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchToIssueUser As eZee.Common.eZeeGradientButton
End Class
