﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmAssignIssueUser_Employee

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmAssignIssueUser_Employee"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintIssueUserTranunkid As Integer = -1
    Private mdtTran As DataTable
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = ""
    Private mdtSearchEmployee As DataTable = Nothing
    Private objIssueUser As clsissueUser_Tran

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintIssueUserTranunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintIssueUserTranunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmAssignIssueUser_Employee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objIssueUser = New clsissueUser_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objIssueUser._Issueusertranunkid = mintIssueUserTranunkid
                objIssueUser.GetList("List", objIssueUser._Issueuserunkid)
                lblIssueUser.Text = Language.getMessage(mstrModuleName, 5, "From Issue User")
                cboIssueUser.Enabled = False
                objbtnSearchIssueUser.Enabled = False
                LblToIssueUser.Visible = True
                cboToIssueUser.Visible = True
                objbtnSearchToIssueUser.Visible = True
            End If
            mdtTran = objIssueUser._dtEmployee
            FillEmployeeList()
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignIssueUser_Employee_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignIssueUser_Employee_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignIssueUser_Employee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignIssueUser_Employee_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignIssueUser_Employee_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignIssueUser_EmployeeFormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsissueUser_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsissueUser_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " Private Methods"

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim mblnAdUser As Boolean = False
        Try
            'FOR ISSUE USER

            'dsFill = objIssueUser.GetUserWithIssueLeavePrivilage("IssueUser", True)
            'cboIssueUser.ValueMember = "userunkid"
            'cboIssueUser.DisplayMember = "username"
            'cboIssueUser.DataSource = dsFill.Tables("IssueUser")


            Dim objUser As New clsUserAddEdit
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'Dim objOption As New clsPassowdOptions
            'Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

            'If drOption.Length > 0 Then
            '    If objOption._UserLogingModeId <> enAuthenticationMode.BASIC_AUTHENTICATION Then mblnAdUser = True
            'End If

            'dsFill = objUser.getComboList("User", True, mblnAdUser, objOption._IsEmployeeAsUser, Company._Object._Companyunkid, 264)

            'Nilay (01-Mar-2016) -- Start
            'dsFill = objUser.getNewComboList("User", , True, Company._Object._Companyunkid, 264)
            dsFill = objUser.getNewComboList("User", , True, Company._Object._Companyunkid, CStr(264), FinancialYear._Object._YearUnkid, True)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            cboIssueUser.DisplayMember = "name"
            cboIssueUser.ValueMember = "userunkid"
            cboIssueUser.DataSource = dsFill.Tables("User")

            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'objOption = Nothing
            'S.SANDEEP [10 AUG 2015] -- END
            objUser = Nothing

            'FOR DEPARTMENT
            Dim objDepartment As New clsDepartment
            dsFill = objDepartment.getComboList("Department", True)
            cboeFilterDept.ValueMember = "departmentunkid"
            cboeFilterDept.DisplayMember = "name"
            cboeFilterDept.DataSource = dsFill.Tables("Department")

            ''FOR FILTER SECTION
            dsFill = Nothing
            Dim objSection As New clsSections
            dsFill = objSection.getComboList("Section", True)
            cboFilterSection.ValueMember = "sectionunkid"
            cboFilterSection.DisplayMember = "name"
            cboFilterSection.DataSource = dsFill.Tables("Section")

            ''FOR FILTER Job
            dsFill = Nothing
            Dim objjob As New clsJobs
            dsFill = objjob.getComboList("Job", True)
            cboFilterJob.ValueMember = "jobunkid"
            cboFilterJob.DisplayMember = "name"
            cboFilterJob.DataSource = dsFill.Tables("Job")


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboIssueUser.SelectedValue = objIssueUser._Issueuserunkid
            SelectedEmplyeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployeeList()
        Dim dsEmployee As DataSet = Nothing
        Dim dtEmployee As DataTable = Nothing
        Dim strSearch As String = String.Empty
        Try

            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            '            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '                dsEmployee = objEmployee.GetList("Employee", True, , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            '            Else
            '                dsEmployee = objEmployee.GetList("Employee", True)
            '            End If


            '            If mstrAdvanceFilter.Trim.Length > 0 Then
            '                strSearch &= "AND " & mstrAdvanceFilter
            '            End If

            '            mstrEmployeeIDs = objIssueUser.GetEmployeeIdFromIssueUser()
            '            If mstrEmployeeIDs.Trim.Length > 0 Then
            '                strSearch &= "AND employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )"
            '            End If

            '            If menAction = enAction.EDIT_ONE Then

            'FillEmployee:
            '                If ConfigParameter._Object._IsIncludeInactiveEmp Then

            '                    If strSearch.Length > 0 Then
            '                        strSearch = strSearch.Substring(3)
            '                        dtEmployee = New DataView(dsEmployee.Tables("Employee"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            '                    Else
            '                        dtEmployee = dsEmployee.Tables(0)
            '                    End If

            '                Else
            '                    If strSearch.Length > 0 Then
            '                        strSearch = strSearch.Substring(3)
            '                        dtEmployee = New DataView(dsEmployee.Tables("Employee"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            '                    Else
            '                        dtEmployee = dsEmployee.Tables("Employee")
            '                    End If

            '                End If


            '            Else

            '                GoTo FillEmployee

            '            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearch &= "AND " & mstrAdvanceFilter
            End If

            mstrEmployeeIDs = objIssueUser.GetEmployeeIdFromIssueUser()
            If mstrEmployeeIDs.Trim.Length > 0 Then
                strSearch &= "AND hremployee_master.employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )"
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._IsIncludeInactiveEmp, _
                                             "Employee", _
                                             ConfigParameter._Object._ShowFirstAppointmentDate, , , strSearch)

            dtEmployee = dsEmployee.Tables(0)
            'S.SANDEEP [04 JUN 2015] -- END


            RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked

            mdtSearchEmployee = dtEmployee

            lvEmployee.Items.Clear()
            If Not dtEmployee Is Nothing Then
                Dim lvItem As ListViewItem
                For Each drRow As DataRow In dtEmployee.Rows
                    lvItem = New ListViewItem
                    lvItem.Tag = drRow("employeeunkid")
                    lvItem.Text = drRow("employeecode").ToString
                    lvItem.SubItems.Add(drRow("name").ToString)
                    lvItem.SubItems.Add(drRow("DeptName").ToString)
                    lvItem.SubItems.Add(drRow("section").ToString)
                    lvItem.SubItems.Add(drRow("job_name").ToString)
                    lvItem.SubItems.Add(drRow("departmentunkid").ToString)
                    lvItem.SubItems.Add(drRow("sectionunkid").ToString)
                    lvItem.SubItems.Add(drRow("jobunkid").ToString)
                    lvEmployee.Items.Add(lvItem)
                Next

                If lvEmployee.Items.Count > 20 Then
                    ColhEmp.Width = 210 - 18
                Else
                    ColhEmp.Width = 210
                End If
            End If

            If lvEmployee.Items.Count > 0 Then
                chkEmployeeAll.Enabled = True
            Else
                chkEmployeeAll.Checked = False
                chkEmployeeAll.Enabled = False
            End If

            AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeList", mstrModuleName)
        End Try
    End Sub

    Private Sub SelectedEmplyeeList()
        Dim strSearch As String = String.Empty
        Dim dtEmployee As DataTable = Nothing
        Try

            strSearch = "AND AUD <> 'D' "

            If CInt(cboeFilterDept.SelectedValue) > 0 Then
                strSearch &= "AND departmentunkid=" & CInt(cboeFilterDept.SelectedValue) & " "
            End If
            If CInt(cboFilterSection.SelectedValue) > 0 Then
                strSearch &= "AND sectionunkid=" & CInt(cboFilterSection.SelectedValue)
            End If
            If CInt(cboFilterJob.SelectedValue) > 0 Then
                strSearch &= "AND jobunkid=" & CInt(cboFilterJob.SelectedValue)
            End If


            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtEmployee = New DataView(mdtTran, strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtEmployee = mdtTran
            End If

            RemoveHandler lvSelectedEmployee.ItemChecked, AddressOf lvSelectedEmployee_ItemChecked

            lvSelectedEmployee.Items.Clear()
            If Not dtEmployee Is Nothing Then
                Dim lvItem As ListViewItem
                For Each drRow As DataRow In dtEmployee.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = ""
                    lvItem.Tag = CInt(drRow("issueusertranunkid"))
                    lvItem.SubItems.Add(drRow("issueuser").ToString)
                    lvItem.SubItems.Add(drRow("employee").ToString)
                    lvItem.SubItems.Add(drRow("department").ToString)
                    lvItem.SubItems.Add(drRow("section").ToString)
                    lvItem.SubItems.Add(drRow("job").ToString)
                    lvItem.SubItems.Add(drRow("GUID").ToString)
                    lvItem.SubItems.Add(drRow("employeeunkid").ToString)
                    lvSelectedEmployee.Items.Add(lvItem)
                Next
            End If
            If lvSelectedEmployee.Items.Count > 0 Then
                chkAllSelectedEmployee.Enabled = True
            Else
                chkAllSelectedEmployee.Checked = False
                chkAllSelectedEmployee.Enabled = False
            End If

            AddHandler lvSelectedEmployee.ItemChecked, AddressOf lvSelectedEmployee_ItemChecked

            If menAction = enAction.EDIT_ONE AndAlso CInt(cboToIssueUser.SelectedValue) > 0 Then
                UpdateMigratedEmployee()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SelectedEmplyeeList", mstrModuleName)
        Finally
            dtEmployee.Dispose()
        End Try

    End Sub

    Private Sub UpdateMigratedEmployee()
        Try
            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                Dim drRow() As DataRow = mdtTran.Select("AUD <> 'D'")
                If drRow.Length > 0 Then

                    For i As Integer = 0 To drRow.Length - 1
                        If drRow(i)("AUD").ToString() <> "A" Then
                            drRow(i)("oldissueuserunkid") = CInt(cboIssueUser.SelectedValue)
                            drRow(i)("oldissueuser") = cboIssueUser.Text
                        End If
                        drRow(i)("issueuserunkid") = CInt(cboToIssueUser.SelectedValue)
                        drRow(i)("issueuser") = cboToIssueUser.Text
                        drRow(i).AcceptChanges()
                    Next

                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateMigratedEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub SetOperation(ByVal blnOperation As Boolean)
        Try
            For Each Item As ListViewItem In lvEmployee.Items
                RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
                Item.Checked = blnOperation
                AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetOperation", mstrModuleName)
        End Try
    End Sub

    Private Sub SetSelectedEmployeeOperation(ByVal blnOperation As Boolean)
        Try
            For Each Item As ListViewItem In lvSelectedEmployee.Items
                RemoveHandler lvSelectedEmployee.ItemChecked, AddressOf lvSelectedEmployee_ItemChecked
                Item.Checked = blnOperation
                AddHandler lvSelectedEmployee.ItemChecked, AddressOf lvSelectedEmployee_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetSelectedEmployeeOperation", mstrModuleName)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboIssueUser.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Issue User is compulsory information.Please Select Issue User."), enMsgBoxStyle.Information)
                cboIssueUser.Focus()
                Return False
            End If

            If CInt(cboIssueUser.SelectedValue) > 0 AndAlso cboIssueUser.Enabled Then

                'Pinkal (06-Feb-2013) -- Start
                'Enhancement : TRA Changes

                'Dim dRow As DataRowView = CType(cboIssueUser.SelectedItem, DataRowView)
                'If dRow("email").ToString().Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "There is no email address specified for the selected user. Please set the valid email address for this user from Aruti Configuration -> User Creation -> User Add/Edit."), enMsgBoxStyle.Information)
                '    Return False
                'End If

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(cboIssueUser.SelectedValue)
                If objUser._Email.ToString().Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "There is no email address specified for the selected user. Please set the valid email address for this user from Aruti Configuration -> User Creation -> User Add/Edit."), enMsgBoxStyle.Information)
                    Return False
                End If

                'Pinkal (06-Feb-2013) -- End
            End If

            If menAction = enAction.EDIT_ONE Then
                If CInt(cboToIssueUser.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "To Issue User is compulsory information.Please Select To Issue User."), enMsgBoxStyle.Information)
                    cboToIssueUser.Select()
                    Return False
                End If

                If CInt(cboToIssueUser.SelectedValue) > 0 AndAlso cboToIssueUser.Visible AndAlso cboIssueUser.Enabled Then
                    Dim dRow As DataRowView = CType(cboToIssueUser.SelectedItem, DataRowView)
                    If dRow("email").ToString().Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "There is no email address specified for the selected user. Please set the valid email address for this user from Aruti Configuration -> User Creation -> User Add/Edit."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region "Button's Event"

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() = False Then Exit Sub

            If lvEmployee.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee."), enMsgBoxStyle.Information)
                lvEmployee.Select()
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            Dim dtRow As DataRow
            Dim count As Integer = 0

            For i As Integer = 0 To lvEmployee.CheckedItems.Count - 1
                count = CInt(mdtTran.Compute("count(employeeunkid)", "employeeunkid=" & CInt(lvEmployee.CheckedItems(i).Tag) & " AND AUD <> 'D'"))
                If count > 0 Then
                    Dim dr() As DataRow = Nothing
                    dr = mdtTran.Select("employeeunkid=" & CInt(lvEmployee.CheckedItems(i).Tag))
                    If dr.Length > 0 Then
                        Me.Cursor = Cursors.Default
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Some of the employees already exist in the selected employee List.Please Select Other employees."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                Else
                    dtRow = mdtTran.NewRow

                    If menAction <> enAction.EDIT_ONE Then
                        dtRow("issueusertranunkid") = mintIssueUserTranunkid
                        dtRow("issueuserunkid") = CInt(cboIssueUser.SelectedValue)
                        dtRow("issueuser") = cboIssueUser.Text.Trim
                    Else
                        dtRow("issueusertranunkid") = -1
                        dtRow("issueuserunkid") = CInt(cboToIssueUser.SelectedValue)
                        dtRow("issueuser") = cboToIssueUser.Text.Trim
                    End If

                    dtRow("oldissueuserunkid") = -1
                    dtRow("oldissueuser") = ""
                    dtRow("employeeunkid") = CInt(lvEmployee.CheckedItems(i).Tag)
                    dtRow("employee") = lvEmployee.CheckedItems(i).SubItems(ColhEmp.Index).Text.Trim
                    dtRow("departmentunkid") = lvEmployee.CheckedItems(i).SubItems(objcolhDepartmentID.Index).Text.Trim
                    dtRow("department") = lvEmployee.CheckedItems(i).SubItems(objcolhDepartment.Index).Text.Trim
                    dtRow("sectionunkid") = lvEmployee.CheckedItems(i).SubItems(objColhSectionID.Index).Text.Trim
                    dtRow("section") = lvEmployee.CheckedItems(i).SubItems(objColhSection.Index).Text.Trim
                    dtRow("jobunkid") = lvEmployee.CheckedItems(i).SubItems(objColhJobID.Index).Text.Trim
                    dtRow("job") = lvEmployee.CheckedItems(i).SubItems(objColhJob.Index).Text.Trim
                    dtRow("AUD") = "A"
                    dtRow("GUID") = Guid.NewGuid.ToString
                    mdtTran.Rows.Add(dtRow)
                End If
            Next
            chkEmployeeAll.Checked = False
            SelectedEmplyeeList()

            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If menAction = enAction.EDIT_ONE Then
                If CInt(cboToIssueUser.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "To Issue User is compulsory information.Please Select To Issue User."), enMsgBoxStyle.Information)
                    cboToIssueUser.Select()
                    Exit Sub
                End If
            End If

            If lvSelectedEmployee.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Selected Employee is compulsory information.Please Check atleast One Selected Employee."), enMsgBoxStyle.Information)
                lvSelectedEmployee.Select()
                Exit Sub
            End If
            Dim drTemp As DataRow() = Nothing

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete Selected Employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Me.Cursor = Cursors.WaitCursor

                For i As Integer = 0 To lvSelectedEmployee.CheckedItems.Count - 1

                    If CInt(lvSelectedEmployee.CheckedItems(i).Tag) = -1 Then
                        drTemp = mdtTran.Select("GUID = '" & lvSelectedEmployee.CheckedItems(i).SubItems(colhGUID.Index).Text & "'")
                    Else
                        drTemp = mdtTran.Select("issueusertranunkid = " & CInt(lvSelectedEmployee.CheckedItems(i).Tag))
                    End If

                    If drTemp IsNot Nothing AndAlso drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                    End If

                Next
            End If
            SelectedEmplyeeList()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub

            Dim count As Integer = CInt(mdtTran.Compute("Count(employeeunkid)", "AUD <> 'D'"))
            If mdtTran.Rows.Count = 0 Or count = 0 Or lvSelectedEmployee.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee."), enMsgBoxStyle.Information)
                lvEmployee.Select()
                Exit Sub
            End If

            objIssueUser._Userunkid = User._Object._Userunkid
            objIssueUser._dtEmployee = mdtTran

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objIssueUser._FormName = mstrModuleName
            objIssueUser._LoginEmployeeunkid = 0
            objIssueUser._ClientIP = getIP()
            objIssueUser._HostName = getHostName()
            objIssueUser._FromWeb = False
            objIssueUser._AuditUserId = User._Object._Userunkid
objIssueUser._CompanyUnkid = Company._Object._Companyunkid
            objIssueUser._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction <> enAction.EDIT_ONE Then
                blnFlag = objIssueUser.Insert()
            Else

                'Pinkal (15-Sep-2013) -- Start
                'Enhancement : TRA Changes
                If CInt(cboToIssueUser.SelectedValue) > 0 Then UpdateMigratedEmployee()
                'Pinkal (15-Sep-2013) -- End
                blnFlag = objIssueUser.Migration_Insert()
            End If

            If blnFlag = False And objIssueUser._Message <> "" Then
                eZeeMsgBox.Show(objIssueUser._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objIssueUser = New clsissueUser_Tran
                    chkAllSelectedEmployee.Checked = False
                    chkEmployeeAll.Checked = False
                    chkAllSelectedEmployee.Checked = False
                    cboeFilterDept.SelectedIndex = 0
                    cboFilterSection.SelectedItem = 0
                    cboFilterJob.SelectedIndex = 0
                    lvEmployee.Items.Clear()
                    mstrEmployeeIDs = ""
                    mstrAdvanceFilter = ""
                    mdtTran.Rows.Clear()
                    FillEmployeeList()
                    GetValue()
                Else
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            SelectedEmplyeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboeFilterDept.SelectedIndex = 0
            cboFilterSection.SelectedIndex = 0
            cboFilterJob.SelectedIndex = 0
            chkAllSelectedEmployee.Checked = False
            SelectedEmplyeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchIssueUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchIssueUser.Click
        Try
            Dim objfrm As New frmCommonSearch
            objfrm.DataSource = CType(cboIssueUser.DataSource, DataTable)
            objfrm.ValueMember = cboIssueUser.ValueMember
            objfrm.DisplayMember = cboIssueUser.DisplayMember
            If objfrm.DisplayDialog() Then
                cboIssueUser.SelectedValue = objfrm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchIssueUser_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchToIssueUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchToIssueUser.Click
        Try
            Dim objfrm As New frmCommonSearch
            objfrm.DataSource = CType(cboToIssueUser.DataSource, DataTable)
            objfrm.ValueMember = cboToIssueUser.ValueMember
            objfrm.DisplayMember = cboToIssueUser.DisplayMember
            If objfrm.DisplayDialog() Then
                cboToIssueUser.SelectedValue = objfrm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchToIssueUser_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = ""
            txtSearchedEmployee.Text = ""
            FillEmployeeList()
            lvEmployee_ItemChecked(sender, New ItemCheckedEventArgs(Nothing))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSelectedEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSelectedEmployee.Click
        Try
            If mdtSearchEmployee Is Nothing Then Exit Sub

            Dim objfrm As New frmCommonSearch
            objfrm.DataSource = mdtSearchEmployee
            objfrm.ValueMember = "employeeunkid"
            objfrm.DisplayMember = "name"
            objfrm.CodeMember = "employeecode"
            If objfrm.DisplayDialog() Then
                txtSearchedEmployee.Text = objfrm.SelectedText
                lvEmployee.SelectedIndices.Clear()
                Dim lvItem As ListViewItem
                lvItem = lvEmployee.FindItemWithText(objfrm.SelectedText, True, 0, False)
                If lvItem IsNot Nothing Then
                    lvEmployee.Focus()
                    lvEmployee.TopItem = lvItem
                    lvItem.Selected = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSelectedEmployee_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkEmployeeAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmployeeAll.CheckedChanged
        Try
            If lvEmployee.Items.Count = 0 Then Exit Sub
            SetOperation(chkEmployeeAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkEmployeeAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkAllSelectedEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAllSelectedEmployee.CheckedChanged
        Try
            If lvSelectedEmployee.Items.Count = 0 Then Exit Sub
            SetSelectedEmployeeOperation(chkAllSelectedEmployee.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkAllSelectedEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DropDown Event"

    Private Sub cboIssueUser_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboIssueUser.SelectedIndexChanged
        Try
            If menAction = enAction.EDIT_ONE Then


                'Pinkal (06-Mar-2013) -- Start
                'Enhancement : TRA Changes
                'Dim dsFill As DataSet = objIssueUser.GetUserWithIssueLeavePrivilage("IssueUser", True)
                Dim dsFill As DataSet = objIssueUser.GetUserWithIssueLeavePrivilage(FinancialYear._Object._YearUnkid, "IssueUser", True)
                'Pinkal (06-Mar-2013) -- End

                cboToIssueUser.ValueMember = "userunkid"
                cboToIssueUser.DisplayMember = "username"
                cboToIssueUser.DataSource = New DataView(dsFill.Tables(0), "userunkid <> " & CInt(cboIssueUser.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboIssueUser_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


#Region "ListView Event"

    Private Sub lvEmployee_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployee.ItemChecked
        Try
            RemoveHandler chkEmployeeAll.CheckedChanged, AddressOf chkEmployeeAll_CheckedChanged
            If lvEmployee.CheckedItems.Count <= 0 Then
                chkEmployeeAll.CheckState = CheckState.Unchecked

            ElseIf lvEmployee.CheckedItems.Count < lvEmployee.Items.Count Then
                chkEmployeeAll.CheckState = CheckState.Indeterminate

            ElseIf lvEmployee.CheckedItems.Count = lvEmployee.Items.Count Then
                chkEmployeeAll.CheckState = CheckState.Checked

            End If
            AddHandler chkEmployeeAll.CheckedChanged, AddressOf chkEmployeeAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMasters_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvSelectedEmployee_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvSelectedEmployee.ItemChecked
        Try
            RemoveHandler chkAllSelectedEmployee.CheckedChanged, AddressOf chkAllSelectedEmployee_CheckedChanged
            If lvSelectedEmployee.CheckedItems.Count <= 0 Then
                chkAllSelectedEmployee.CheckState = CheckState.Unchecked

            ElseIf lvSelectedEmployee.CheckedItems.Count < lvSelectedEmployee.Items.Count Then
                chkAllSelectedEmployee.CheckState = CheckState.Indeterminate

            ElseIf lvSelectedEmployee.CheckedItems.Count = lvSelectedEmployee.Items.Count Then
                chkAllSelectedEmployee.CheckState = CheckState.Checked

            End If
            AddHandler chkAllSelectedEmployee.CheckedChanged, AddressOf chkAllSelectedEmployee_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvSelectedEmployee_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Link Button Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                FillEmployeeList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbApproversInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApproversInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSelectedEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSelectedEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbApproversInfo.Text = Language._Object.getCaption(Me.gbApproversInfo.Name, Me.gbApproversInfo.Text)
            Me.lblIssueUser.Text = Language._Object.getCaption(Me.lblIssueUser.Name, Me.lblIssueUser.Text)
            Me.lblApprovalTo.Text = Language._Object.getCaption(Me.lblApprovalTo.Name, Me.lblApprovalTo.Text)
            Me.gbSelectedEmployee.Text = Language._Object.getCaption(Me.gbSelectedEmployee.Name, Me.gbSelectedEmployee.Text)
            Me.gbEmployee.Text = Language._Object.getCaption(Me.gbEmployee.Name, Me.gbEmployee.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhDept.Text = Language._Object.getCaption(CStr(Me.colhDept.Tag), Me.colhDept.Text)
            Me.colhSection.Text = Language._Object.getCaption(CStr(Me.colhSection.Tag), Me.colhSection.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.lblFilterSection.Text = Language._Object.getCaption(Me.lblFilterSection.Name, Me.lblFilterSection.Text)
            Me.lblFilterDepartment.Text = Language._Object.getCaption(Me.lblFilterDepartment.Name, Me.lblFilterDepartment.Text)
            Me.ColhEmployeecode.Text = Language._Object.getCaption(CStr(Me.ColhEmployeecode.Tag), Me.ColhEmployeecode.Text)
            Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
            Me.chkEmployeeAll.Text = Language._Object.getCaption(Me.chkEmployeeAll.Name, Me.chkEmployeeAll.Text)
            Me.chkAllSelectedEmployee.Text = Language._Object.getCaption(Me.chkAllSelectedEmployee.Name, Me.chkAllSelectedEmployee.Text)
            Me.colhGUID.Text = Language._Object.getCaption(CStr(Me.colhGUID.Tag), Me.colhGUID.Text)
            Me.colhemployeeunkid.Text = Language._Object.getCaption(CStr(Me.colhemployeeunkid.Tag), Me.colhemployeeunkid.Text)
            Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
            Me.lblFilterjob.Text = Language._Object.getCaption(Me.lblFilterjob.Name, Me.lblFilterjob.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.ColhEmp.Text = Language._Object.getCaption(CStr(Me.ColhEmp.Tag), Me.ColhEmp.Text)
            Me.colhIssueUser.Text = Language._Object.getCaption(CStr(Me.colhIssueUser.Tag), Me.colhIssueUser.Text)
            Me.LblToIssueUser.Text = Language._Object.getCaption(Me.LblToIssueUser.Name, Me.LblToIssueUser.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Issue User is compulsory information.Please Select Issue User.")
            Language.setMessage(mstrModuleName, 2, "Selected Employee is compulsory information.Please Check atleast One Selected Employee.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete Selected Employee?")
            Language.setMessage(mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee.")
            Language.setMessage(mstrModuleName, 5, "From Issue User")
            Language.setMessage(mstrModuleName, 6, "Some of the employees already exist in the selected employee List.Please Select Other employees.")
            Language.setMessage(mstrModuleName, 7, "To Issue User is compulsory information.Please Select To Issue User.")
            Language.setMessage(mstrModuleName, 8, "There is no email address specified for the selected user. Please set the valid email address for this user from Aruti Configuration -> User Creation -> User Add/Edit.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class