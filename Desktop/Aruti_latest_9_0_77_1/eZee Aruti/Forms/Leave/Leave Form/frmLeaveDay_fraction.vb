﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmLeaveDay_fraction


#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmLeaveDay_fraction"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtStartdate As DateTime
    Private mdtReturndate As DateTime

    'Pinkal (15-Nov-2013) -- Start
    'Enhancement : Oman Changes
    'Private mintFormunkid As Integer
    Public mintFormunkid As Integer
    'Pinkal (15-Nov-2013) -- End
    Private dtFraction As DataTable
    Private objLeaveFraction As clsleaveday_fraction
    Private dtDelete As DataTable


    'Pinkal (07-APR-2012) -- Start
    'Enhancement : TRA Changes
    Private mintLeaveTypeID As Integer
    Private mintEmpId As Integer
    'Pinkal (07-APR-2012) -- End

    'Pinkal (15-Jul-2013) -- Start
    'Enhancement : TRA Changes
    Private mintApproverID As Integer = -1
    Friend mblnFromProcessPending As Boolean = False
    'Pinkal (15-Jul-2013) -- End


#End Region


#Region "Properties"

    Public Property _Startdate() As DateTime
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As DateTime)
            mdtStartdate = value
        End Set
    End Property

    Public Property _Enddate() As DateTime
        Get
            Return mdtReturndate
        End Get
        Set(ByVal value As DateTime)
            mdtReturndate = value
        End Set
    End Property

    Public Property _dtFraction() As DataTable
        Get
            Return dtFraction
        End Get
        Set(ByVal value As DataTable)
            dtFraction = value
        End Set
    End Property


    'Pinkal (07-APR-2012) -- Start
    'Enhancement : TRA Changes

    Public Property _EmployeeId() As Integer
        Get
            Return mintEmpId
        End Get
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public Property _LeaveTypeId() As Integer
        Get
            Return mintLeaveTypeID
        End Get
        Set(ByVal value As Integer)
            mintLeaveTypeID = value
        End Set
    End Property

    'Pinkal (07-APR-2012) -- End


    'Pinkal (15-Jul-2013) -- Start
    'Enhancement : TRA Changes

    Public Property _ApproverId() As Integer
        Get
            Return mintApproverID
        End Get
        Set(ByVal value As Integer)
            mintApproverID = value
        End Set
    End Property

    'Pinkal (15-Jul-2013) -- End



#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal _Formunkid As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            mintFormunkid = _Formunkid
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function(s) & Method(s) "

    Public Sub GenerateDays()
        Try

            If dtFraction Is Nothing Then Exit Sub

            Dim mintTotalDays As Integer = CInt(DateDiff(DateInterval.Day, mdtStartdate, mdtReturndate.AddDays(1)))

            Dim drRow As DataRow() = dtFraction.Select("leavedate > '" & mdtReturndate.Date & "'")

            If drRow.Length > 0 Then
                For i As Integer = 0 To drRow.Length - 1
                    drRow(i)("AUD") = "D"
                Next
                dtFraction.AcceptChanges()
            End If


            Dim objLeaveType As New clsleavetype_master
            Dim blnIssueonHoliday As Boolean = False

            objLeaveType._Leavetypeunkid = mintLeaveTypeID
            blnIssueonHoliday = objLeaveType._Isissueonholiday

            'Pinkal (15-Oct-2013) -- Start
            'Enhancement : TRA Changes
            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
            'Pinkal (15-Oct-2013) -- End


            'Pinkal (01-Feb-2014) -- Start
            'Enhancement : TRA Changes
            Dim blnIssueonweekend As Boolean = False
            Dim blnConsiderLVHlOnWk As Boolean = False
            Dim objEmp As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = mintEmpId
            objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = mintEmpId
            'S.SANDEEP [04 JUN 2015] -- END

            Dim objShift As New clsNewshift_master
            Dim objShiftTran As New clsshift_tran

            blnIssueonweekend = objLeaveType._Isissueonweekend
            blnConsiderLVHlOnWk = objLeaveType._IsLeaveHL_onWK

            For i As Integer = 0 To mintTotalDays - 1

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtStartdate.AddDays(i).Date, objEmp._Employeeunkid)
                objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtStartdate.AddDays(i).Date, objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)))
                'S.SANDEEP [04 JUN 2015] -- END

                objShiftTran.GetShiftTran(objShift._Shiftunkid)

                If blnIssueonHoliday = False AndAlso blnIssueonweekend = True Then
                    Dim objEmpHoliday As New clsemployee_holiday
                    If objEmpHoliday.GetEmployeeHoliday(mintEmpId, mdtStartdate.AddDays(i).Date).Rows.Count > 0 Then
                        Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartdate.AddDays(i).Date & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dRow(0)("AUD") = "D"
                            dtFraction.AcceptChanges()
                        End If

                        If blnIssueonweekend AndAlso blnConsiderLVHlOnWk = False Then

                            If objShiftTran._dtShiftday IsNot Nothing Then
                                Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtStartdate.AddDays(i).DayOfWeek.ToString()) & " AND isweekend = 1")

                                If drShift.Length > 0 Then
                                    GoTo AddRecord
                                End If

                            End If

                        End If
                        Continue For
                    End If


                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
                    Dim objReccurent As New clsemployee_holiday
                    If objReccurent.GetEmployeeReCurrentHoliday(mintEmpId, mdtStartdate.AddDays(i).Date) Then
                        Continue For
                    End If
                    'Pinkal (06-May-2014) -- End


                End If


                If blnIssueonweekend = False AndAlso blnIssueonHoliday = True Then
                    If objShiftTran._dtShiftday IsNot Nothing Then
                        Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtStartdate.AddDays(i).DayOfWeek.ToString()) & " AND isweekend = 1")
                        If drShift.Length > 0 Then

                            Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartdate.AddDays(i).Date & "' AND AUD <> 'D'")
                            If dRow.Length > 0 Then
                                dRow(0)("AUD") = "D"
                                dtFraction.AcceptChanges()
                            End If

                            If blnIssueonHoliday AndAlso blnConsiderLVHlOnWk = False Then
                                Dim objEmpHoliday As New clsemployee_holiday
                                If objEmpHoliday.GetEmployeeHoliday(mintEmpId, mdtStartdate.AddDays(i).Date).Rows.Count > 0 Then
                                    GoTo AddRecord
                                End If

                            End If
                            Continue For

                        End If

                    End If

                End If


                If blnIssueonweekend = False AndAlso blnIssueonHoliday = False Then

                    If objShiftTran._dtShiftday IsNot Nothing Then
                        Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtStartdate.AddDays(i).DayOfWeek.ToString()) & " AND isweekend = 1")
                        If drShift.Length > 0 Then
                        Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartdate.AddDays(i).Date & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dRow(0)("AUD") = "D"
                            dtFraction.AcceptChanges()
                        End If
                        Continue For
                        Else
                            Dim objEmpHoliday As New clsemployee_holiday
                            If objEmpHoliday.GetEmployeeHoliday(mintEmpId, mdtStartdate.AddDays(i).Date).Rows.Count > 0 Then
                                Continue For
                            End If

                            'Pinkal (06-May-2014) -- Start
                            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
                            If objEmpHoliday.GetEmployeeReCurrentHoliday(mintEmpId, mdtStartdate.AddDays(i).Date) Then
                                Continue For
                            End If
                            'Pinkal (06-May-2014) -- End

                    End If
                    End If

                End If

                'Pinkal (01-Feb-2014) -- End


AddRecord:

                'Pinkal (15-Jul-2013) -- Start
                'Enhancement : TRA Changes

                If mblnFromProcessPending Then    'FOR PROCESS PENDING FORM
                    drRow = dtFraction.Select("leavedate < '" & mdtStartdate.Date & "'")

                    If drRow.Length > 0 Then
                        For k As Integer = 0 To drRow.Length - 1
                            drRow(k)("AUD") = "D"
                        Next
                        dtFraction.AcceptChanges()
                    End If
                End If

                'Pinkal (15-Jul-2013) -- End

                Dim dtRow As DataRow() = dtFraction.Select("leavedate = '" & mdtStartdate.AddDays(i).Date & "' AND AUD = ''")
                If dtRow.Length = 0 Then
                Dim dr As DataRow = dtFraction.NewRow
                dr("dayfractionunkid") = -1
                dr("formunkid") = mintFormunkid
                dr("leavedate") = mdtStartdate.AddDays(i).ToShortDateString
                dr("dayfraction") = 1
                If dr("AUD").ToString() = "" Then
                    dr("AUD") = "A"
                End If


                    'Pinkal (15-Nov-2013) -- Start
                    'Enhancement : Oman Changes
                    dr("approverunkid") = mintApproverID
                    'Pinkal (15-Nov-2013) -- End

                dr("GUID") = Guid.NewGuid.ToString()
                dtFraction.Rows.Add(dr)
                End If
            Next

            FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenerateDays", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGrid()
        Try


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            'Dim dtView As DataTable = New DataView(dtFraction, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable()
            Dim dtView As DataTable = New DataView(dtFraction, "AUD <> 'D'", "leavedate asc", DataViewRowState.CurrentRows).ToTable()
            'Pinkal (20-Jan-2012) -- End


            dtDelete = New DataView(dtFraction, "AUD = 'D'", "", DataViewRowState.CurrentRows).ToTable()
            dgLeaveFraction.AutoGenerateColumns = False
            dgLeaveFraction.DataSource = dtView
            dgColhDate.DataPropertyName = "leavedate"
            dgcolhFraction.DataPropertyName = "dayfraction"
            dtFraction = dtView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmLeaveDayCount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Pinkal (06-Feb-2012) -- End

            objLeaveFraction = New clsleaveday_fraction

            'Pinkal (15-Jul-2013) -- Start
            'Enhancement : TRA Changes

            'If menAction = enAction.EDIT_ONE Then
            '    dtFraction = objLeaveFraction.GetList("List", mintFormunkid, True).Tables(0)
            'Else
            '    dtFraction = objLeaveFraction.GetList("List", mintFormunkid, True).Tables(0).Clone
            'End If

            If menAction = enAction.EDIT_ONE Then
                dtFraction = objLeaveFraction.GetList("List", mintFormunkid, True, -1, mintApproverID).Tables(0)
            Else
                dtFraction = objLeaveFraction.GetList("List", mintFormunkid, True, -1, mintApproverID).Tables(0).Clone
            End If

            'Pinkal (15-Jul-2013) -- End

            GenerateDays()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveDayCount_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsleaveday_fraction.SetMessages()
            'objfrm._Other_ModuleNames = "clsleaveday_fraction"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub BtnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOk.Click
        Try

            If dtDelete IsNot Nothing AndAlso dtDelete.Rows.Count > 0 Then

                For i As Integer = 0 To dtDelete.Rows.Count - 1
                    dtFraction.ImportRow(dtDelete.Rows(i))
                Next
                dtFraction.AcceptChanges()
            End If

            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "BtnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        mblnCancel = True
        Me.Close()
    End Sub

#End Region

#Region " GridView's Event(s) "

    Private Sub dgLeaveFraction_CellValidated(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgLeaveFraction.CellValidated
        Try
            If e.RowIndex < -1 Then Exit Sub

            If CDec(dgLeaveFraction.Rows(e.RowIndex).Cells(dgcolhFraction.Name).Value) < 0.05 Or CDec(dgLeaveFraction.Rows(e.RowIndex).Cells(dgcolhFraction.Name).Value) > 1 Then
                dgLeaveFraction.Rows(e.RowIndex).Cells(dgcolhFraction.Name).Value = 1
                dgLeaveFraction.Rows(e.RowIndex).Cells(dgcolhFraction.Name).Selected = True
                dgLeaveFraction.Rows(e.RowIndex).Cells(dgcolhFraction.Name).KeyEntersEditMode(New KeyEventArgs(Keys.F2))
            Else

                If dgLeaveFraction.IsCurrentRowDirty = True Then
                    If dtFraction.Rows(e.RowIndex)("AUD").ToString() <> "A" Then
                        dtFraction.Rows(e.RowIndex)("AUD") = "U"
                        dtFraction.AcceptChanges()
                    End If
                End If

                If e.RowIndex < dgLeaveFraction.RowCount - 1 Then
                    dgLeaveFraction.Rows(e.RowIndex + 1).Cells(dgcolhFraction.Name).Selected = True
                    dgLeaveFraction.Rows(e.RowIndex + 1).Cells(dgcolhFraction.Name).KeyEntersEditMode(New KeyEventArgs(Keys.Enter))

                ElseIf e.RowIndex = dgLeaveFraction.RowCount - 1 Then
                    dgLeaveFraction.Rows(e.RowIndex).Cells(dgcolhFraction.Name).Selected = True
                    dgLeaveFraction.Rows(e.RowIndex).Cells(dgcolhFraction.Name).KeyEntersEditMode(New KeyEventArgs(Keys.Enter))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLeaveFraction_CellValidated", mstrModuleName)
        End Try

    End Sub

     'Please  don't remove this , if '.' is given then this handler will not give exception.
    Private Sub dgLeaveFraction_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgLeaveFraction.DataError
             
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.BtnOk.GradientBackColor = GUI._ButttonBackColor
            Me.BtnOk.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.BtnOk.Text = Language._Object.getCaption(Me.BtnOk.Name, Me.BtnOk.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.dgColhDate.HeaderText = Language._Object.getCaption(Me.dgColhDate.Name, Me.dgColhDate.HeaderText)
            Me.dgcolhFraction.HeaderText = Language._Object.getCaption(Me.dgcolhFraction.Name, Me.dgcolhFraction.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class