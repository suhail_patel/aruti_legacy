﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveForm_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeaveForm_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbLeaveForm = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkScanDocuements = New System.Windows.Forms.LinkLabel
        Me.objAsonDateAccrue = New System.Windows.Forms.Label
        Me.LblAsonDateAccrue = New System.Windows.Forms.Label
        Me.objAsonDateBalance = New System.Windows.Forms.Label
        Me.lblAsonDateBalance = New System.Windows.Forms.Label
        Me.objLeaveAccrue = New System.Windows.Forms.Label
        Me.LblLeaveAccrue = New System.Windows.Forms.Label
        Me.LblELCStart = New System.Windows.Forms.Label
        Me.lvLeaveExpense = New System.Windows.Forms.LinkLabel
        Me.lnkLeaveDayCount = New System.Windows.Forms.LinkLabel
        Me.objbtnAddLeaveType = New eZee.Common.eZeeGradientButton
        Me.lnkCopyEmpAddress = New System.Windows.Forms.LinkLabel
        Me.objBalance = New System.Windows.Forms.Label
        Me.lblBalance = New System.Windows.Forms.Label
        Me.objNoofDays = New System.Windows.Forms.Label
        Me.lblDays = New System.Windows.Forms.Label
        Me.lnkAccrueleave = New System.Windows.Forms.LinkLabel
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.txtLeaveAddress = New eZee.TextBox.AlphanumericTextBox
        Me.txtLeaveReason = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.lblReturnDate = New System.Windows.Forms.Label
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.objLine5 = New eZee.Common.eZeeStraightLine
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblLeaveAddress = New System.Windows.Forms.Label
        Me.objLine4 = New eZee.Common.eZeeStraightLine
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.dtpApplyDate = New System.Windows.Forms.DateTimePicker
        Me.txtFormNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblFormNo = New System.Windows.Forms.Label
        Me.lblApplyDate = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbLeaveForm.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbLeaveForm)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(681, 404)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbLeaveForm
        '
        Me.gbLeaveForm.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveForm.Checked = False
        Me.gbLeaveForm.CollapseAllExceptThis = False
        Me.gbLeaveForm.CollapsedHoverImage = Nothing
        Me.gbLeaveForm.CollapsedNormalImage = Nothing
        Me.gbLeaveForm.CollapsedPressedImage = Nothing
        Me.gbLeaveForm.CollapseOnLoad = False
        Me.gbLeaveForm.Controls.Add(Me.lnkScanDocuements)
        Me.gbLeaveForm.Controls.Add(Me.objAsonDateAccrue)
        Me.gbLeaveForm.Controls.Add(Me.LblAsonDateAccrue)
        Me.gbLeaveForm.Controls.Add(Me.objAsonDateBalance)
        Me.gbLeaveForm.Controls.Add(Me.lblAsonDateBalance)
        Me.gbLeaveForm.Controls.Add(Me.objLeaveAccrue)
        Me.gbLeaveForm.Controls.Add(Me.LblLeaveAccrue)
        Me.gbLeaveForm.Controls.Add(Me.LblELCStart)
        Me.gbLeaveForm.Controls.Add(Me.lvLeaveExpense)
        Me.gbLeaveForm.Controls.Add(Me.lnkLeaveDayCount)
        Me.gbLeaveForm.Controls.Add(Me.objbtnAddLeaveType)
        Me.gbLeaveForm.Controls.Add(Me.lnkCopyEmpAddress)
        Me.gbLeaveForm.Controls.Add(Me.objBalance)
        Me.gbLeaveForm.Controls.Add(Me.lblBalance)
        Me.gbLeaveForm.Controls.Add(Me.objNoofDays)
        Me.gbLeaveForm.Controls.Add(Me.lblDays)
        Me.gbLeaveForm.Controls.Add(Me.lnkAccrueleave)
        Me.gbLeaveForm.Controls.Add(Me.cboEmployee)
        Me.gbLeaveForm.Controls.Add(Me.cboLeaveType)
        Me.gbLeaveForm.Controls.Add(Me.lblLeaveType)
        Me.gbLeaveForm.Controls.Add(Me.txtLeaveAddress)
        Me.gbLeaveForm.Controls.Add(Me.txtLeaveReason)
        Me.gbLeaveForm.Controls.Add(Me.lblRemarks)
        Me.gbLeaveForm.Controls.Add(Me.lblReturnDate)
        Me.gbLeaveForm.Controls.Add(Me.dtpEndDate)
        Me.gbLeaveForm.Controls.Add(Me.lblStartDate)
        Me.gbLeaveForm.Controls.Add(Me.objLine5)
        Me.gbLeaveForm.Controls.Add(Me.dtpStartDate)
        Me.gbLeaveForm.Controls.Add(Me.lblLeaveAddress)
        Me.gbLeaveForm.Controls.Add(Me.objLine4)
        Me.gbLeaveForm.Controls.Add(Me.objLine1)
        Me.gbLeaveForm.Controls.Add(Me.dtpApplyDate)
        Me.gbLeaveForm.Controls.Add(Me.txtFormNo)
        Me.gbLeaveForm.Controls.Add(Me.lblFormNo)
        Me.gbLeaveForm.Controls.Add(Me.lblApplyDate)
        Me.gbLeaveForm.Controls.Add(Me.lblEmployee)
        Me.gbLeaveForm.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbLeaveForm.ExpandedHoverImage = Nothing
        Me.gbLeaveForm.ExpandedNormalImage = Nothing
        Me.gbLeaveForm.ExpandedPressedImage = Nothing
        Me.gbLeaveForm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveForm.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveForm.HeaderHeight = 25
        Me.gbLeaveForm.HeaderMessage = ""
        Me.gbLeaveForm.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveForm.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveForm.HeightOnCollapse = 0
        Me.gbLeaveForm.LeftTextSpace = 0
        Me.gbLeaveForm.Location = New System.Drawing.Point(2, 2)
        Me.gbLeaveForm.Name = "gbLeaveForm"
        Me.gbLeaveForm.OpenHeight = 300
        Me.gbLeaveForm.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveForm.ShowBorder = True
        Me.gbLeaveForm.ShowCheckBox = False
        Me.gbLeaveForm.ShowCollapseButton = False
        Me.gbLeaveForm.ShowDefaultBorderColor = True
        Me.gbLeaveForm.ShowDownButton = False
        Me.gbLeaveForm.ShowHeader = True
        Me.gbLeaveForm.Size = New System.Drawing.Size(677, 348)
        Me.gbLeaveForm.TabIndex = 0
        Me.gbLeaveForm.Temp = 0
        Me.gbLeaveForm.Text = "Leave Form Information"
        Me.gbLeaveForm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkScanDocuements
        '
        Me.lnkScanDocuements.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkScanDocuements.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkScanDocuements.Location = New System.Drawing.Point(521, 110)
        Me.lnkScanDocuements.Name = "lnkScanDocuements"
        Me.lnkScanDocuements.Size = New System.Drawing.Size(119, 17)
        Me.lnkScanDocuements.TabIndex = 273
        Me.lnkScanDocuements.TabStop = True
        Me.lnkScanDocuements.Text = "Browse"
        Me.lnkScanDocuements.Visible = False
        '
        'objAsonDateAccrue
        '
        Me.objAsonDateAccrue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objAsonDateAccrue.Location = New System.Drawing.Point(421, 110)
        Me.objAsonDateAccrue.Name = "objAsonDateAccrue"
        Me.objAsonDateAccrue.Size = New System.Drawing.Size(67, 17)
        Me.objAsonDateAccrue.TabIndex = 271
        Me.objAsonDateAccrue.Text = "0.00"
        Me.objAsonDateAccrue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblAsonDateAccrue
        '
        Me.LblAsonDateAccrue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAsonDateAccrue.Location = New System.Drawing.Point(260, 110)
        Me.LblAsonDateAccrue.Name = "LblAsonDateAccrue"
        Me.LblAsonDateAccrue.Size = New System.Drawing.Size(155, 16)
        Me.LblAsonDateAccrue.TabIndex = 270
        Me.LblAsonDateAccrue.Text = "Leave Accrue As on End Date"
        '
        'objAsonDateBalance
        '
        Me.objAsonDateBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objAsonDateBalance.Location = New System.Drawing.Point(421, 132)
        Me.objAsonDateBalance.Name = "objAsonDateBalance"
        Me.objAsonDateBalance.Size = New System.Drawing.Size(67, 17)
        Me.objAsonDateBalance.TabIndex = 269
        Me.objAsonDateBalance.Text = "0.00"
        Me.objAsonDateBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAsonDateBalance
        '
        Me.lblAsonDateBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsonDateBalance.Location = New System.Drawing.Point(260, 133)
        Me.lblAsonDateBalance.Name = "lblAsonDateBalance"
        Me.lblAsonDateBalance.Size = New System.Drawing.Size(155, 16)
        Me.lblAsonDateBalance.TabIndex = 268
        Me.lblAsonDateBalance.Text = "Leave Balance As on End Date"
        '
        'objLeaveAccrue
        '
        Me.objLeaveAccrue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLeaveAccrue.Location = New System.Drawing.Point(160, 110)
        Me.objLeaveAccrue.Name = "objLeaveAccrue"
        Me.objLeaveAccrue.Size = New System.Drawing.Size(67, 17)
        Me.objLeaveAccrue.TabIndex = 266
        Me.objLeaveAccrue.Text = "0.00"
        Me.objLeaveAccrue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblLeaveAccrue
        '
        Me.LblLeaveAccrue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeaveAccrue.Location = New System.Drawing.Point(9, 110)
        Me.LblLeaveAccrue.Name = "LblLeaveAccrue"
        Me.LblLeaveAccrue.Size = New System.Drawing.Size(141, 16)
        Me.LblLeaveAccrue.TabIndex = 265
        Me.LblLeaveAccrue.Text = "Total Leave Accrue"
        '
        'LblELCStart
        '
        Me.LblELCStart.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblELCStart.Location = New System.Drawing.Point(10, 155)
        Me.LblELCStart.Name = "LblELCStart"
        Me.LblELCStart.Size = New System.Drawing.Size(235, 16)
        Me.LblELCStart.TabIndex = 263
        '
        'lvLeaveExpense
        '
        Me.lvLeaveExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvLeaveExpense.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lvLeaveExpense.Location = New System.Drawing.Point(521, 88)
        Me.lvLeaveExpense.Name = "lvLeaveExpense"
        Me.lvLeaveExpense.Size = New System.Drawing.Size(119, 17)
        Me.lvLeaveExpense.TabIndex = 261
        Me.lvLeaveExpense.TabStop = True
        Me.lvLeaveExpense.Text = "Leave Expense"
        '
        'lnkLeaveDayCount
        '
        Me.lnkLeaveDayCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkLeaveDayCount.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkLeaveDayCount.Location = New System.Drawing.Point(384, 88)
        Me.lnkLeaveDayCount.Name = "lnkLeaveDayCount"
        Me.lnkLeaveDayCount.Size = New System.Drawing.Size(131, 17)
        Me.lnkLeaveDayCount.TabIndex = 259
        Me.lnkLeaveDayCount.TabStop = True
        Me.lnkLeaveDayCount.Text = "Leave Day Fraction"
        '
        'objbtnAddLeaveType
        '
        Me.objbtnAddLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddLeaveType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddLeaveType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddLeaveType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddLeaveType.BorderSelected = False
        Me.objbtnAddLeaveType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddLeaveType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddLeaveType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddLeaveType.Location = New System.Drawing.Point(466, 33)
        Me.objbtnAddLeaveType.Name = "objbtnAddLeaveType"
        Me.objbtnAddLeaveType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddLeaveType.TabIndex = 257
        '
        'lnkCopyEmpAddress
        '
        Me.lnkCopyEmpAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkCopyEmpAddress.Location = New System.Drawing.Point(120, 322)
        Me.lnkCopyEmpAddress.Name = "lnkCopyEmpAddress"
        Me.lnkCopyEmpAddress.Size = New System.Drawing.Size(184, 13)
        Me.lnkCopyEmpAddress.TabIndex = 255
        Me.lnkCopyEmpAddress.TabStop = True
        Me.lnkCopyEmpAddress.Text = "Copy Employee Domicile Address"
        Me.lnkCopyEmpAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objBalance
        '
        Me.objBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objBalance.Location = New System.Drawing.Point(160, 132)
        Me.objBalance.Name = "objBalance"
        Me.objBalance.Size = New System.Drawing.Size(67, 17)
        Me.objBalance.TabIndex = 253
        Me.objBalance.Text = "0.00"
        Me.objBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBalance
        '
        Me.lblBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalance.Location = New System.Drawing.Point(9, 133)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(141, 16)
        Me.lblBalance.TabIndex = 252
        Me.lblBalance.Text = "Total Leave Balance"
        '
        'objNoofDays
        '
        Me.objNoofDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objNoofDays.Location = New System.Drawing.Point(160, 88)
        Me.objNoofDays.Name = "objNoofDays"
        Me.objNoofDays.Size = New System.Drawing.Size(67, 17)
        Me.objNoofDays.TabIndex = 251
        Me.objNoofDays.Text = "0.00"
        Me.objNoofDays.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDays
        '
        Me.lblDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDays.Location = New System.Drawing.Point(9, 88)
        Me.lblDays.Name = "lblDays"
        Me.lblDays.Size = New System.Drawing.Size(141, 16)
        Me.lblDays.TabIndex = 250
        Me.lblDays.Text = "No of Days to Apply"
        '
        'lnkAccrueleave
        '
        Me.lnkAccrueleave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAccrueleave.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAccrueleave.Location = New System.Drawing.Point(258, 88)
        Me.lnkAccrueleave.Name = "lnkAccrueleave"
        Me.lnkAccrueleave.Size = New System.Drawing.Size(102, 17)
        Me.lnkAccrueleave.TabIndex = 248
        Me.lnkAccrueleave.TabStop = True
        Me.lnkAccrueleave.Text = "Accrue Leave"
        Me.lnkAccrueleave.Visible = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(89, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(125, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.DropDownWidth = 400
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(329, 33)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(134, 21)
        Me.cboLeaveType.TabIndex = 2
        '
        'lblLeaveType
        '
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(255, 36)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(68, 15)
        Me.lblLeaveType.TabIndex = 246
        Me.lblLeaveType.Text = "Leave Type"
        '
        'txtLeaveAddress
        '
        Me.txtLeaveAddress.Flags = 0
        Me.txtLeaveAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLeaveAddress.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLeaveAddress.Location = New System.Drawing.Point(12, 201)
        Me.txtLeaveAddress.Multiline = True
        Me.txtLeaveAddress.Name = "txtLeaveAddress"
        Me.txtLeaveAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtLeaveAddress.Size = New System.Drawing.Size(295, 112)
        Me.txtLeaveAddress.TabIndex = 6
        '
        'txtLeaveReason
        '
        Me.txtLeaveReason.Flags = 0
        Me.txtLeaveReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLeaveReason.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLeaveReason.Location = New System.Drawing.Point(329, 201)
        Me.txtLeaveReason.Multiline = True
        Me.txtLeaveReason.Name = "txtLeaveReason"
        Me.txtLeaveReason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtLeaveReason.Size = New System.Drawing.Size(311, 112)
        Me.txtLeaveReason.TabIndex = 7
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(329, 182)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(188, 15)
        Me.lblRemarks.TabIndex = 244
        Me.lblRemarks.Text = "Remarks"
        '
        'lblReturnDate
        '
        Me.lblReturnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReturnDate.Location = New System.Drawing.Point(493, 63)
        Me.lblReturnDate.Name = "lblReturnDate"
        Me.lblReturnDate.Size = New System.Drawing.Size(62, 15)
        Me.lblReturnDate.TabIndex = 230
        Me.lblReturnDate.Text = "End Date"
        '
        'dtpEndDate
        '
        Me.dtpEndDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(561, 60)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpEndDate.TabIndex = 5
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(493, 36)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(62, 15)
        Me.lblStartDate.TabIndex = 228
        Me.lblStartDate.Text = "Start Date"
        '
        'objLine5
        '
        Me.objLine5.BackColor = System.Drawing.Color.Transparent
        Me.objLine5.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine5.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine5.Location = New System.Drawing.Point(313, 190)
        Me.objLine5.Name = "objLine5"
        Me.objLine5.Size = New System.Drawing.Size(10, 119)
        Me.objLine5.TabIndex = 241
        Me.objLine5.Text = "EZeeStraightLine2"
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(561, 33)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpStartDate.TabIndex = 4
        '
        'lblLeaveAddress
        '
        Me.lblLeaveAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveAddress.Location = New System.Drawing.Point(10, 182)
        Me.lblLeaveAddress.Name = "lblLeaveAddress"
        Me.lblLeaveAddress.Size = New System.Drawing.Size(299, 15)
        Me.lblLeaveAddress.TabIndex = 240
        Me.lblLeaveAddress.Text = "Address and Telephone Number While On leave "
        '
        'objLine4
        '
        Me.objLine4.BackColor = System.Drawing.Color.Transparent
        Me.objLine4.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine4.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.objLine4.Location = New System.Drawing.Point(11, 171)
        Me.objLine4.Name = "objLine4"
        Me.objLine4.Size = New System.Drawing.Size(656, 9)
        Me.objLine4.TabIndex = 236
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(249, 33)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(5, 137)
        Me.objLine1.TabIndex = 225
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'dtpApplyDate
        '
        Me.dtpApplyDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplyDate.Checked = False
        Me.dtpApplyDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplyDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApplyDate.Location = New System.Drawing.Point(329, 60)
        Me.dtpApplyDate.Name = "dtpApplyDate"
        Me.dtpApplyDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpApplyDate.TabIndex = 3
        '
        'txtFormNo
        '
        Me.txtFormNo.Flags = 0
        Me.txtFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFormNo.Location = New System.Drawing.Point(89, 60)
        Me.txtFormNo.Name = "txtFormNo"
        Me.txtFormNo.Size = New System.Drawing.Size(125, 21)
        Me.txtFormNo.TabIndex = 1
        '
        'lblFormNo
        '
        Me.lblFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormNo.Location = New System.Drawing.Point(8, 63)
        Me.lblFormNo.Name = "lblFormNo"
        Me.lblFormNo.Size = New System.Drawing.Size(75, 15)
        Me.lblFormNo.TabIndex = 224
        Me.lblFormNo.Text = "Application No"
        '
        'lblApplyDate
        '
        Me.lblApplyDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplyDate.Location = New System.Drawing.Point(255, 63)
        Me.lblApplyDate.Name = "lblApplyDate"
        Me.lblApplyDate.Size = New System.Drawing.Size(68, 15)
        Me.lblApplyDate.TabIndex = 226
        Me.lblApplyDate.Text = "Apply Date"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(75, 15)
        Me.lblEmployee.TabIndex = 217
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(220, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 349)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(681, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(469, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(572, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmLeaveForm_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(681, 404)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeaveForm_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Leave Form"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbLeaveForm.ResumeLayout(False)
        Me.gbLeaveForm.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbLeaveForm As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents txtFormNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFormNo As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents dtpApplyDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblApplyDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReturnDate As System.Windows.Forms.Label
    Friend WithEvents objLine4 As eZee.Common.eZeeStraightLine
    Friend WithEvents objLine5 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblLeaveAddress As System.Windows.Forms.Label
    Friend WithEvents txtLeaveReason As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtLeaveAddress As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveType As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lnkAccrueleave As System.Windows.Forms.LinkLabel
    Friend WithEvents objNoofDays As System.Windows.Forms.Label
    Friend WithEvents lblDays As System.Windows.Forms.Label
    Friend WithEvents objBalance As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents lnkCopyEmpAddress As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkLeaveDayCount As System.Windows.Forms.LinkLabel
    Friend WithEvents lvLeaveExpense As System.Windows.Forms.LinkLabel
    Friend WithEvents LblELCStart As System.Windows.Forms.Label
    Friend WithEvents objLeaveAccrue As System.Windows.Forms.Label
    Friend WithEvents LblLeaveAccrue As System.Windows.Forms.Label
    Friend WithEvents objAsonDateAccrue As System.Windows.Forms.Label
    Friend WithEvents LblAsonDateAccrue As System.Windows.Forms.Label
    Friend WithEvents objAsonDateBalance As System.Windows.Forms.Label
    Friend WithEvents lblAsonDateBalance As System.Windows.Forms.Label
    Friend WithEvents objbtnAddLeaveType As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkScanDocuements As System.Windows.Forms.LinkLabel
End Class
