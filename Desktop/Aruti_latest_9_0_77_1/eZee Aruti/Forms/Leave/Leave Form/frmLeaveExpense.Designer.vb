﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveExpense
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeaveExpense))
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbDependantsList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnDependentClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgDepedent = New System.Windows.Forms.DataGridView
        Me.pnlExpense = New System.Windows.Forms.Panel
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtTotalAmount = New eZee.TextBox.AlphanumericTextBox
        Me.lblTotalAmount = New System.Windows.Forms.Label
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgExpense = New System.Windows.Forms.DataGridView
        Me.dgcolhDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhQty = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhUnitcost = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhExpenseId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbLeaveExpense = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.nudQty = New System.Windows.Forms.NumericUpDown
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtUnitCost = New eZee.TextBox.NumericTextBox
        Me.lblUnitcost = New System.Windows.Forms.Label
        Me.lblQty = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.LnkViewDependants = New System.Windows.Forms.LinkLabel
        Me.LblDomicileAdd = New System.Windows.Forms.Label
        Me.txtDomicileAddress = New eZee.TextBox.AlphanumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.BtnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgcolhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAge = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhMonth = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhRelation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.gbDependantsList.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgDepedent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlExpense.SuspendLayout()
        CType(Me.dgExpense, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbLeaveExpense.SuspendLayout()
        CType(Me.nudQty, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbDependantsList)
        Me.pnlMain.Controls.Add(Me.pnlExpense)
        Me.pnlMain.Controls.Add(Me.gbLeaveExpense)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(599, 459)
        Me.pnlMain.TabIndex = 0
        '
        'gbDependantsList
        '
        Me.gbDependantsList.BorderColor = System.Drawing.Color.Black
        Me.gbDependantsList.Checked = False
        Me.gbDependantsList.CollapseAllExceptThis = False
        Me.gbDependantsList.CollapsedHoverImage = Nothing
        Me.gbDependantsList.CollapsedNormalImage = Nothing
        Me.gbDependantsList.CollapsedPressedImage = Nothing
        Me.gbDependantsList.CollapseOnLoad = False
        Me.gbDependantsList.Controls.Add(Me.Panel1)
        Me.gbDependantsList.ExpandedHoverImage = Nothing
        Me.gbDependantsList.ExpandedNormalImage = Nothing
        Me.gbDependantsList.ExpandedPressedImage = Nothing
        Me.gbDependantsList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDependantsList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDependantsList.HeaderHeight = 25
        Me.gbDependantsList.HeaderMessage = ""
        Me.gbDependantsList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbDependantsList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDependantsList.HeightOnCollapse = 0
        Me.gbDependantsList.LeftTextSpace = 0
        Me.gbDependantsList.Location = New System.Drawing.Point(65, 68)
        Me.gbDependantsList.Name = "gbDependantsList"
        Me.gbDependantsList.OpenHeight = 300
        Me.gbDependantsList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDependantsList.ShowBorder = True
        Me.gbDependantsList.ShowCheckBox = False
        Me.gbDependantsList.ShowCollapseButton = False
        Me.gbDependantsList.ShowDefaultBorderColor = True
        Me.gbDependantsList.ShowDownButton = False
        Me.gbDependantsList.ShowHeader = True
        Me.gbDependantsList.Size = New System.Drawing.Size(453, 254)
        Me.gbDependantsList.TabIndex = 272
        Me.gbDependantsList.Temp = 0
        Me.gbDependantsList.Text = "Depedants List"
        Me.gbDependantsList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbDependantsList.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnDependentClose)
        Me.Panel1.Controls.Add(Me.dgDepedent)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(3, 26)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(447, 226)
        Me.Panel1.TabIndex = 273
        '
        'btnDependentClose
        '
        Me.btnDependentClose.BackColor = System.Drawing.Color.White
        Me.btnDependentClose.BackgroundImage = CType(resources.GetObject("btnDependentClose.BackgroundImage"), System.Drawing.Image)
        Me.btnDependentClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDependentClose.BorderColor = System.Drawing.Color.Empty
        Me.btnDependentClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDependentClose.FlatAppearance.BorderSize = 0
        Me.btnDependentClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDependentClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDependentClose.ForeColor = System.Drawing.Color.Black
        Me.btnDependentClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDependentClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnDependentClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDependentClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDependentClose.Location = New System.Drawing.Point(354, 193)
        Me.btnDependentClose.Name = "btnDependentClose"
        Me.btnDependentClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDependentClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDependentClose.Size = New System.Drawing.Size(90, 30)
        Me.btnDependentClose.TabIndex = 10
        Me.btnDependentClose.Text = "&Close"
        Me.btnDependentClose.UseVisualStyleBackColor = True
        '
        'dgDepedent
        '
        Me.dgDepedent.AllowUserToAddRows = False
        Me.dgDepedent.AllowUserToDeleteRows = False
        Me.dgDepedent.AllowUserToResizeColumns = False
        Me.dgDepedent.AllowUserToResizeRows = False
        Me.dgDepedent.BackgroundColor = System.Drawing.Color.White
        Me.dgDepedent.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhName, Me.dgcolhGender, Me.dgcolhAge, Me.dgcolhMonth, Me.dgcolhRelation})
        Me.dgDepedent.Location = New System.Drawing.Point(3, 2)
        Me.dgDepedent.Name = "dgDepedent"
        Me.dgDepedent.ReadOnly = True
        Me.dgDepedent.RowHeadersVisible = False
        Me.dgDepedent.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgDepedent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgDepedent.Size = New System.Drawing.Size(441, 186)
        Me.dgDepedent.TabIndex = 272
        Me.dgDepedent.TabStop = False
        '
        'pnlExpense
        '
        Me.pnlExpense.Controls.Add(Me.btnEdit)
        Me.pnlExpense.Controls.Add(Me.txtTotalAmount)
        Me.pnlExpense.Controls.Add(Me.lblTotalAmount)
        Me.pnlExpense.Controls.Add(Me.btnDelete)
        Me.pnlExpense.Controls.Add(Me.btnAdd)
        Me.pnlExpense.Controls.Add(Me.dgExpense)
        Me.pnlExpense.Location = New System.Drawing.Point(11, 181)
        Me.pnlExpense.Name = "pnlExpense"
        Me.pnlExpense.Size = New System.Drawing.Size(581, 219)
        Me.pnlExpense.TabIndex = 273
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(488, 42)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 6
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.BackColor = System.Drawing.Color.White
        Me.txtTotalAmount.Flags = 0
        Me.txtTotalAmount.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTotalAmount.Location = New System.Drawing.Point(370, 194)
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.ReadOnly = True
        Me.txtTotalAmount.Size = New System.Drawing.Size(115, 21)
        Me.txtTotalAmount.TabIndex = 271
        Me.txtTotalAmount.Text = "0"
        Me.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalAmount.Location = New System.Drawing.Point(266, 197)
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Size = New System.Drawing.Size(98, 15)
        Me.lblTotalAmount.TabIndex = 270
        Me.lblTotalAmount.Text = "Total Amount"
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(488, 78)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 7
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(488, 5)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(90, 30)
        Me.btnAdd.TabIndex = 5
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'dgExpense
        '
        Me.dgExpense.AllowUserToAddRows = False
        Me.dgExpense.AllowUserToDeleteRows = False
        Me.dgExpense.AllowUserToResizeColumns = False
        Me.dgExpense.AllowUserToResizeRows = False
        Me.dgExpense.BackgroundColor = System.Drawing.Color.White
        Me.dgExpense.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgExpense.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhDescription, Me.dgcolhQty, Me.dgcolhUnitcost, Me.dgcolhAmount, Me.objdgcolhExpenseId})
        Me.dgExpense.Location = New System.Drawing.Point(2, 5)
        Me.dgExpense.Name = "dgExpense"
        Me.dgExpense.ReadOnly = True
        Me.dgExpense.RowHeadersVisible = False
        Me.dgExpense.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgExpense.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgExpense.Size = New System.Drawing.Size(482, 186)
        Me.dgExpense.TabIndex = 4
        Me.dgExpense.TabStop = False
        '
        'dgcolhDescription
        '
        Me.dgcolhDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhDescription.HeaderText = "Description"
        Me.dgcolhDescription.Name = "dgcolhDescription"
        Me.dgcolhDescription.ReadOnly = True
        Me.dgcolhDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhQty
        '
        Me.dgcolhQty.DecimalLength = 2
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.NullValue = Nothing
        Me.dgcolhQty.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgcolhQty.HeaderText = "Qty"
        Me.dgcolhQty.Name = "dgcolhQty"
        Me.dgcolhQty.ReadOnly = True
        Me.dgcolhQty.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhQty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhQty.Width = 75
        '
        'dgcolhUnitcost
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.NullValue = Nothing
        Me.dgcolhUnitcost.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgcolhUnitcost.HeaderText = "Unit Cost"
        Me.dgcolhUnitcost.Name = "dgcolhUnitcost"
        Me.dgcolhUnitcost.ReadOnly = True
        Me.dgcolhUnitcost.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhUnitcost.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhUnitcost.Width = 75
        '
        'dgcolhAmount
        '
        Me.dgcolhAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.NullValue = Nothing
        Me.dgcolhAmount.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgcolhAmount.HeaderText = "Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        Me.dgcolhAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhExpenseId
        '
        Me.objdgcolhExpenseId.HeaderText = "ExpenseId"
        Me.objdgcolhExpenseId.Name = "objdgcolhExpenseId"
        Me.objdgcolhExpenseId.ReadOnly = True
        Me.objdgcolhExpenseId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhExpenseId.Visible = False
        '
        'gbLeaveExpense
        '
        Me.gbLeaveExpense.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveExpense.Checked = False
        Me.gbLeaveExpense.CollapseAllExceptThis = False
        Me.gbLeaveExpense.CollapsedHoverImage = Nothing
        Me.gbLeaveExpense.CollapsedNormalImage = Nothing
        Me.gbLeaveExpense.CollapsedPressedImage = Nothing
        Me.gbLeaveExpense.CollapseOnLoad = False
        Me.gbLeaveExpense.Controls.Add(Me.nudQty)
        Me.gbLeaveExpense.Controls.Add(Me.txtAmount)
        Me.gbLeaveExpense.Controls.Add(Me.lblAmount)
        Me.gbLeaveExpense.Controls.Add(Me.txtUnitCost)
        Me.gbLeaveExpense.Controls.Add(Me.lblUnitcost)
        Me.gbLeaveExpense.Controls.Add(Me.lblQty)
        Me.gbLeaveExpense.Controls.Add(Me.lblDescription)
        Me.gbLeaveExpense.Controls.Add(Me.txtDescription)
        Me.gbLeaveExpense.Controls.Add(Me.LnkViewDependants)
        Me.gbLeaveExpense.Controls.Add(Me.LblDomicileAdd)
        Me.gbLeaveExpense.Controls.Add(Me.txtDomicileAddress)
        Me.gbLeaveExpense.ExpandedHoverImage = Nothing
        Me.gbLeaveExpense.ExpandedNormalImage = Nothing
        Me.gbLeaveExpense.ExpandedPressedImage = Nothing
        Me.gbLeaveExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveExpense.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveExpense.HeaderHeight = 25
        Me.gbLeaveExpense.HeaderMessage = ""
        Me.gbLeaveExpense.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveExpense.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveExpense.HeightOnCollapse = 0
        Me.gbLeaveExpense.LeftTextSpace = 0
        Me.gbLeaveExpense.Location = New System.Drawing.Point(12, 7)
        Me.gbLeaveExpense.Name = "gbLeaveExpense"
        Me.gbLeaveExpense.OpenHeight = 300
        Me.gbLeaveExpense.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveExpense.ShowBorder = True
        Me.gbLeaveExpense.ShowCheckBox = False
        Me.gbLeaveExpense.ShowCollapseButton = False
        Me.gbLeaveExpense.ShowDefaultBorderColor = True
        Me.gbLeaveExpense.ShowDownButton = False
        Me.gbLeaveExpense.ShowHeader = True
        Me.gbLeaveExpense.Size = New System.Drawing.Size(578, 170)
        Me.gbLeaveExpense.TabIndex = 3
        Me.gbLeaveExpense.Temp = 0
        Me.gbLeaveExpense.Text = "Leave Expense Information"
        Me.gbLeaveExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudQty
        '
        Me.nudQty.DecimalPlaces = 2
        Me.nudQty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudQty.Location = New System.Drawing.Point(420, 30)
        Me.nudQty.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.nudQty.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudQty.Name = "nudQty"
        Me.nudQty.Size = New System.Drawing.Size(149, 21)
        Me.nudQty.TabIndex = 2
        Me.nudQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudQty.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = False
        Me.txtAmount.BackColor = System.Drawing.Color.White
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 65536
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(420, 84)
        Me.txtAmount.MaxDecimalPlaces = 4
        Me.txtAmount.MaxWholeDigits = 9
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.ReadOnly = True
        Me.txtAmount.Size = New System.Drawing.Size(149, 21)
        Me.txtAmount.TabIndex = 4
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(334, 87)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(80, 15)
        Me.lblAmount.TabIndex = 267
        Me.lblAmount.Text = "Amount"
        '
        'txtUnitCost
        '
        Me.txtUnitCost.AllowNegative = False
        Me.txtUnitCost.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtUnitCost.DigitsInGroup = 0
        Me.txtUnitCost.Flags = 65536
        Me.txtUnitCost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUnitCost.Location = New System.Drawing.Point(420, 57)
        Me.txtUnitCost.MaxDecimalPlaces = 4
        Me.txtUnitCost.MaxWholeDigits = 9
        Me.txtUnitCost.Name = "txtUnitCost"
        Me.txtUnitCost.Prefix = ""
        Me.txtUnitCost.RangeMax = 1.7976931348623157E+308
        Me.txtUnitCost.RangeMin = -1.7976931348623157E+308
        Me.txtUnitCost.Size = New System.Drawing.Size(149, 21)
        Me.txtUnitCost.TabIndex = 3
        Me.txtUnitCost.Text = "0"
        Me.txtUnitCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblUnitcost
        '
        Me.lblUnitcost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitcost.Location = New System.Drawing.Point(334, 60)
        Me.lblUnitcost.Name = "lblUnitcost"
        Me.lblUnitcost.Size = New System.Drawing.Size(80, 15)
        Me.lblUnitcost.TabIndex = 264
        Me.lblUnitcost.Text = "Unit Cost"
        '
        'lblQty
        '
        Me.lblQty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQty.Location = New System.Drawing.Point(334, 34)
        Me.lblQty.Name = "lblQty"
        Me.lblQty.Size = New System.Drawing.Size(80, 15)
        Me.lblQty.TabIndex = 263
        Me.lblQty.Text = "Qty"
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(6, 33)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(80, 15)
        Me.lblDescription.TabIndex = 240
        Me.lblDescription.Text = "Description"
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(95, 32)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(233, 73)
        Me.txtDescription.TabIndex = 1
        '
        'LnkViewDependants
        '
        Me.LnkViewDependants.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LnkViewDependants.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.LnkViewDependants.Location = New System.Drawing.Point(420, 113)
        Me.LnkViewDependants.Name = "LnkViewDependants"
        Me.LnkViewDependants.Size = New System.Drawing.Size(146, 13)
        Me.LnkViewDependants.TabIndex = 271
        Me.LnkViewDependants.TabStop = True
        Me.LnkViewDependants.Text = "View Depedents List"
        Me.LnkViewDependants.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblDomicileAdd
        '
        Me.LblDomicileAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDomicileAdd.Location = New System.Drawing.Point(6, 123)
        Me.LblDomicileAdd.Name = "LblDomicileAdd"
        Me.LblDomicileAdd.Size = New System.Drawing.Size(80, 15)
        Me.LblDomicileAdd.TabIndex = 269
        Me.LblDomicileAdd.Text = "Domicile Add."
        '
        'txtDomicileAddress
        '
        Me.txtDomicileAddress.BackColor = System.Drawing.SystemColors.Window
        Me.txtDomicileAddress.Flags = 0
        Me.txtDomicileAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileAddress.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileAddress.Location = New System.Drawing.Point(95, 111)
        Me.txtDomicileAddress.Multiline = True
        Me.txtDomicileAddress.Name = "txtDomicileAddress"
        Me.txtDomicileAddress.ReadOnly = True
        Me.txtDomicileAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDomicileAddress.Size = New System.Drawing.Size(233, 53)
        Me.txtDomicileAddress.TabIndex = 270
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.BtnOk)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 404)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(599, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(500, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 9
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'BtnOk
        '
        Me.BtnOk.BackColor = System.Drawing.Color.White
        Me.BtnOk.BackgroundImage = CType(resources.GetObject("BtnOk.BackgroundImage"), System.Drawing.Image)
        Me.BtnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnOk.BorderColor = System.Drawing.Color.Empty
        Me.BtnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.BtnOk.FlatAppearance.BorderSize = 0
        Me.BtnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnOk.ForeColor = System.Drawing.Color.Black
        Me.BtnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.BtnOk.GradientForeColor = System.Drawing.Color.Black
        Me.BtnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.BtnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.BtnOk.Location = New System.Drawing.Point(404, 13)
        Me.BtnOk.Name = "BtnOk"
        Me.BtnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.BtnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.BtnOk.Size = New System.Drawing.Size(90, 30)
        Me.BtnOk.TabIndex = 8
        Me.BtnOk.Text = "&Ok"
        Me.BtnOk.UseVisualStyleBackColor = True
        '
        'dgcolhName
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.dgcolhName.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhName.HeaderText = "Name"
        Me.dgcolhName.Name = "dgcolhName"
        Me.dgcolhName.ReadOnly = True
        Me.dgcolhName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhName.Width = 137
        '
        'dgcolhGender
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.NullValue = Nothing
        Me.dgcolhGender.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhGender.HeaderText = "Gender"
        Me.dgcolhGender.Name = "dgcolhGender"
        Me.dgcolhGender.ReadOnly = True
        Me.dgcolhGender.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhGender.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhGender.Width = 75
        '
        'dgcolhAge
        '
        Me.dgcolhAge.AllowNegative = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "F0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.dgcolhAge.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhAge.HeaderText = "Age"
        Me.dgcolhAge.Name = "dgcolhAge"
        Me.dgcolhAge.ReadOnly = True
        Me.dgcolhAge.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhAge.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAge.Width = 75
        '
        'dgcolhMonth
        '
        Me.dgcolhMonth.AllowNegative = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "F0"
        Me.dgcolhMonth.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhMonth.HeaderText = "Month"
        Me.dgcolhMonth.Name = "dgcolhMonth"
        Me.dgcolhMonth.ReadOnly = True
        Me.dgcolhMonth.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhMonth.Width = 60
        '
        'dgcolhRelation
        '
        Me.dgcolhRelation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.NullValue = Nothing
        Me.dgcolhRelation.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgcolhRelation.HeaderText = "Relation"
        Me.dgcolhRelation.Name = "dgcolhRelation"
        Me.dgcolhRelation.ReadOnly = True
        Me.dgcolhRelation.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhRelation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'frmLeaveExpense
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(599, 459)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeaveExpense"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Leave Expense"
        Me.pnlMain.ResumeLayout(False)
        Me.gbDependantsList.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgDepedent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlExpense.ResumeLayout(False)
        Me.pnlExpense.PerformLayout()
        CType(Me.dgExpense, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbLeaveExpense.ResumeLayout(False)
        Me.gbLeaveExpense.PerformLayout()
        CType(Me.nudQty, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents BtnOk As eZee.Common.eZeeLightButton
    Friend WithEvents gbLeaveExpense As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblQty As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtUnitCost As eZee.TextBox.NumericTextBox
    Friend WithEvents lblUnitcost As System.Windows.Forms.Label
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents nudQty As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents dgExpense As System.Windows.Forms.DataGridView
    Friend WithEvents lblTotalAmount As System.Windows.Forms.Label
    Friend WithEvents dgcolhDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhQty As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhUnitcost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhExpenseId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtTotalAmount As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDomicileAddress As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents LblDomicileAdd As System.Windows.Forms.Label
    Friend WithEvents LnkViewDependants As System.Windows.Forms.LinkLabel
    Friend WithEvents gbDependantsList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dgDepedent As System.Windows.Forms.DataGridView
    Friend WithEvents btnDependentClose As eZee.Common.eZeeLightButton
    Friend WithEvents pnlExpense As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgcolhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAge As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhMonth As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhRelation As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
