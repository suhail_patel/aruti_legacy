﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization
Imports Aruti.Data.Language
Imports System.Threading

'Last Message Index = 9

Public Class frmLeaveIssue_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmLeaveIssue_AddEdit"
    Private mblnCancel As Boolean = True
    Private objLeaveIssueMaster As clsleaveissue_master
    Private objLeaveIssueTran As clsleaveissue_Tran
    Dim objShift As clsNewshift_master
    Dim objShiftTran As New clsshift_tran
    Dim objEmployee As clsEmployee_Master
    Dim objPending As New clspendingleave_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Friend mintLeaveIssueUnkid As Integer = -1
    Dim mintColumnNo As Integer = 0
    Dim mdblLeaveAmout As Double = 0
    Dim mintFormNounkid As Integer = -1
    Dim mintLeaveTypeunkid As Integer = -1
    Dim mintColor As Integer = -1
    Dim blnEdit As Boolean = False
    Dim mdtIssuetran As DataSet
    Dim blnViewer As Boolean = False
    Dim blnIssueOnHoliday As Boolean = False
    Dim blnIsHoliday As Boolean = False '---THIS IS USED FOR ONLY EDIT PURPOSE FOR NORMAL DAYS 
    'Dim intSavedLeaveDays As Integer = 0 ' -- THIS IS USED FOR TO GET SAVED LEAVE DAYS COUNT
    Dim mdblSavedLeavedays As Decimal = 0 ' -- THIS IS USED FOR TO GET SAVED LEAVE DAYS COUNT
    Public mintPedingProcessunkid As Integer = -1  ' -- THIS IS USED FOR TO GET PENDING PROCESS TRAN UNK ID
    Dim objEmpShift As clsEmployee_Shift_Tran
    Dim blnIssueOnWeekend As Boolean = False
    Dim blnConsiderLVHlOnWk As Boolean = False
    Dim mstrAdvanceFilter As String = ""
    Private trd As Thread

    'Pinkal (01-Mar-2016) -- Start
    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
    Private mblnIsExternalApprover As Boolean = False
    'Pinkal (01-Mar-2016) -- End


    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
    Private mdtFormStartDate As Date = Nothing
    Private mdtFormEndDate As Date = Nothing
    'Pinkal (18-Nov-2016) -- End

    'Pinkal (29-Sep-2017) -- End


    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mintLvformMinDays As Integer = 0
    Private mintLvformMaxDays As Integer = 0
    'Pinkal (08-Oct-2018) -- End


    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
    Private mblnConsiderLeaveOnTnAPeriod As Boolean = False
    'Pinkal (01-Jan-2019) -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal mblnViewer As Boolean, ByVal eAction As enAction _
                                                , Optional ByVal intEmployeeunkid As Integer = -1 _
                                                , Optional ByVal intFormNounkid As Integer = -1, Optional ByVal intLeaveTypeunkid As Integer = -1 _
                                                , Optional ByVal strEmplyeeName As String = "" _
                                                , Optional ByVal blnIsExternalApprover As Boolean = False) As Boolean
        Try
            mintLeaveIssueUnkid = intUnkId
            menAction = eAction
            txtEmployee.Tag = intEmployeeunkid
            txtEmployee.Text = strEmplyeeName
            mintFormNounkid = intFormNounkid
            mintLeaveTypeunkid = intLeaveTypeunkid
            blnViewer = mblnViewer
            If mintLeaveIssueUnkid > 0 Then blnEdit = True

            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            mblnIsExternalApprover = blnIsExternalApprover
            'Pinkal (01-Mar-2016) -- End


            Me.ShowDialog()
            intUnkId = mintLeaveIssueUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmLeaveIssue_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objLeaveIssueMaster = New clsleaveissue_master
        objLeaveIssueTran = New clsleaveissue_Tran
        objEmployee = New clsEmployee_Master
        objShift = New clsNewshift_master
        objEmpShift = New clsEmployee_Shift_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            setColor()
            If mintLeaveIssueUnkid > 0 Then
                objLeaveIssueMaster._Leaveissueunkid = mintLeaveIssueUnkid
            End If
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(txtEmployee.Tag)
            GenerateCalendar()


            If blnViewer Then
                FillViewerItems()
                dtpFromDate.Checked = False
                dtpToDate.Checked = False
            Else
                FillIssueCombo()
            End If

            GetValue()

            If blnViewer = False Then

                If ConfigParameter._Object._CurrentDateAndTime.Date > FinancialYear._Object._Database_End_Date Then
                    txtAsDate.Text = FinancialYear._Object._Database_End_Date.ToShortDateString

                ElseIf ConfigParameter._Object._CurrentDateAndTime.Date <= FinancialYear._Object._Database_End_Date Then
                    txtAsDate.Text = ConfigParameter._Object._CurrentDateAndTime.Date.ToShortDateString

                End If

                GetBalanceInfo(CDate(txtAsDate.Text))
                Dim objLeaveType As New clsleavetype_master
                objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)

                If objLeaveType._IsCheckDocOnLeaveIssue Then
                    lnkPreviewDocument.Visible = True
                Else
                    lnkPreviewDocument.Visible = False
                End If

            End If


            dgLeaveIssue.ClearSelection()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveIssue_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveIssue_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveIssue_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveIssue_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveIssue_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveIssue_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeaveIssueMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleaveissue_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleaveissue_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Dropdown Event"

    Private Sub cboFormNo_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFormNo.SelectionChangeCommitted
        Dim GetDayDiff As Integer = 0
        Dim GetMonthDiff As Integer = 0
        Dim GetYearDiff As Integer = 0
        Dim arStrDays() As String = Nothing
        Dim arStrHoliday() As String = Nothing
        Dim arStrHolidayColor() As String = Nothing
        Try



            dgLeaveIssue.Rows.Clear()
            dgLeaveIssue.Refresh()
            If CInt(cboFormNo.SelectedValue) > 0 Then
                Dim objFormNo As New clsleaveform
                objFormNo._Formunkid = CInt(cboFormNo.SelectedValue)

                If mintPedingProcessunkid <= 0 Then
                    Dim dsList As DataSet = objPending.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                     , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                     , True, "lvpendingleave_tran.formunkid=" & mintFormNounkid & " AND (lvpendingleave_tran.statusunkid = 1 or lvpendingleave_tran.statusunkid = 7)")
                    Dim dtList As DataTable = dsList.Tables(0)

                    If dtList.Rows.Count > 0 Then
                        mintPedingProcessunkid = CInt(dtList.Rows(0)("pendingleavetranunkid"))
                    End If

                End If

                objPending._Pendingleavetranunkid = mintPedingProcessunkid
                objlblDateRange.Text = objPending._Startdate.ToShortDateString & " To " & objPending._Enddate.ToShortDateString


                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                mdtFormStartDate = objPending._Startdate.Date
                mdtFormEndDate = objPending._Enddate.Date
                'Pinkal (18-Nov-2016) -- End


                GetMonthDiff = CInt(DateDiff(DateInterval.Month, objPending._Startdate, objPending._Enddate))
                GetYearDiff = CInt(DateDiff(DateInterval.Year, objPending._Startdate, objPending._Enddate))
                GetDayDiff = CInt(DateDiff(DateInterval.Day, objPending._Startdate.Date, objPending._Enddate.Date.AddDays(1)))


                'START FOR HOLIDAY MASTER

                Dim objEmpHoliday As New clsemployee_holiday
                Dim dsFill As DataSet = objEmpHoliday.GetList("List", FinancialYear._Object._DatabaseName _
                                                                                    , User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, CInt(txtEmployee.Tag), Nothing, "", "")


                Dim dtFill As DataTable = New DataView(dsFill.Tables("List"), "holidaydate >='" & eZeeDate.convertDate(objPending._Startdate.Date) _
                                             & "' AND holidaydate <= '" & eZeeDate.convertDate(objPending._Enddate.Date) & "'", "holidaydate asc", _
                                            DataViewRowState.CurrentRows).ToTable


                ReDim arStrHoliday(dtFill.Rows.Count - 1) : ReDim arStrHolidayColor(dtFill.Rows.Count - 1)
                For i As Integer = 0 To dtFill.Rows.Count - 1
                    arStrHoliday(i) = dtFill.Rows(i)("holidaydate").ToString
                    arStrHolidayColor(i) = dtFill.Rows(i)("color").ToString
                Next
                'END FOR HOLIDAY MASTER


                If blnEdit Then
                    If Not mdtIssuetran Is Nothing Then
                        If mdtIssuetran.Tables("IssueTran").Rows.Count > 0 Then
                            Dim drRow As DataRow
                            For i As Integer = 0 To dtFill.Rows.Count - 1
                                'CHECK FOR WHETHER LEAVE ISSUE ON HOLIDAY
                                Dim dRow As DataRow() = mdtIssuetran.Tables("IssueTran").Select("leavedate ='" & dtFill.Rows(i)("holidaydate").ToString & "'")
                                If dRow.Length = 0 Then
                                    drRow = mdtIssuetran.Tables("IssueTran").NewRow
                                    drRow("leavedate") = dtFill.Rows(i)("holidaydate")
                                    mdtIssuetran.Tables("IssueTran").Rows.Add(drRow)
                                End If
                                'CHECK FOR WHETHER LEAVE ISSUE ON HOLIDAY
                            Next

                            Dim dtTable As DataTable = New DataView(mdtIssuetran.Tables("IssueTran"), "1=1", "leavedate asc", DataViewRowState.CurrentRows).ToTable()
                            mdtIssuetran.Tables.Clear()
                            mdtIssuetran.Tables.Add(dtTable)

                            Dim objLeaveFraction As New clsleaveday_fraction

                            ReDim arStrDays(mdtIssuetran.Tables("IssueTran").Rows.Count - 1)
                            For i As Integer = 0 To mdtIssuetran.Tables("IssueTran").Rows.Count - 1
                                arStrDays(i) = CStr(mdtIssuetran.Tables("IssueTran").Rows(i)("leavedate")).ToString()
                                mdblSavedLeavedays += objLeaveFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(CDate(arStrDays(i))), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag))
                            Next

                        End If
                    End If
                Else
                    ReDim arStrDays(GetDayDiff - 1)
                    For i As Integer = 0 To arStrDays.Length - 1
                        arStrDays(i) = CInt(objPending._Startdate.AddDays(i).Date.ToString("yyyyMMdd")).ToString
                    Next
                End If


                If GetMonthDiff = 0 And GetYearDiff = 0 Then

                    Dim arStrMonthDays() As String = {Date.DaysInMonth(objPending._Startdate.Year, objPending._Startdate.Month).ToString()}
                    Dim firstDayOfCurrentMonth As New DateTime(objPending._Startdate.Year, objPending._Startdate.Month, 1)

                    Dim arStrMonth() As String = {CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(objPending._Startdate.Month) & " - " & objPending._Startdate.Year.ToString}
                    Dim arStrFirstday() As String = {CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(firstDayOfCurrentMonth.DayOfWeek)}
                    GenerateDays(arStrMonth, arStrMonthDays, arStrFirstday, arStrDays, Nothing, arStrHoliday, arStrHolidayColor)

                Else
                    Dim arStrMonth(GetMonthDiff) As String
                    Dim arStrFirstday(GetMonthDiff) As String
                    Dim arStrMonthDays(GetMonthDiff) As String
                    For i As Integer = 0 To GetMonthDiff

                        arStrMonthDays(i) = Date.DaysInMonth(objPending._Startdate.AddMonths(i).Year, objPending._Startdate.AddMonths(i).Month).ToString()
                        Dim firstDayOfCurrentMonth As New DateTime(objPending._Startdate.AddMonths(i).Year, objPending._Startdate.AddMonths(i).Month, 1)
                        arStrMonth(i) = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(objPending._Startdate.AddMonths(i).Month) & " - " & objPending._Startdate.AddMonths(i).Year.ToString()
                        arStrFirstday(i) = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(firstDayOfCurrentMonth.DayOfWeek)
                    Next
                    GenerateDays(arStrMonth, arStrMonthDays, arStrFirstday, arStrDays, Nothing, arStrHoliday, arStrHolidayColor)
                End If
            Else
                dgLeaveIssue.Rows.Clear()
                objlblDateRange.Text = ""
            End If

            objTotalLeaveDays.Text = GetLeavedaysBalance(GetLeavedays()).ToString()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFormNo_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLeaveType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged, cboLeaveYear.SelectedIndexChanged
        Try
            If CInt(cboLeaveType.SelectedValue) > 0 Then
                Dim objLeaveType As New clsleavetype_master
                objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
                cboLeaveType.Tag = CBool(objLeaveType._IsPaid)

                blnIssueOnHoliday = objLeaveType._Isissueonholiday
                blnIssueOnWeekend = objLeaveType._Isissueonweekend
                blnConsiderLVHlOnWk = objLeaveType._IsLeaveHL_onWK


                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                mintLvformMinDays = objLeaveType._LVDaysMinLimit
                mintLvformMaxDays = objLeaveType._LVDaysMaxLimit
                'Pinkal (08-Oct-2018) -- End


                If objLeaveType._IsPaid Then
                    Dim objAcccure As New clsleavebalance_tran

                    Dim dsList As DataSet = Nothing
                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        dsList = objAcccure.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                          , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                          , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1, False, False, False, "", Nothing)
                    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        dsList = objAcccure.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                        , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1, False, False, False, "", Nothing)
                    End If


                    Dim dtList As DataTable = New DataView(dsList.Tables("List"), "employeeunkid = " & CInt(txtEmployee.Tag) & " AND yearunkid = " & CInt(cboLeaveYear.SelectedValue) & " AND leavetypeunkid = " & CInt(cboLeaveType.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
                    If dtList IsNot Nothing Then
                        If dtList.Rows.Count > 0 Then
                            mdblLeaveAmout = CDec(dtList.Rows(0)("accrue_amount"))
                        End If
                    End If
                Else
                    mdblLeaveAmout = objLeaveType._MaxAmount
                End If

                objlblcolor.BackColor = Color.FromArgb(objLeaveType._Color)
                mintColor = objLeaveType._Color
            Else
                objlblcolor.BackColor = Color.Transparent
                mintColor = objlblcolor.BackColor.ToArgb
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLeaveType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Datagrid's Event"

    Private Sub dgLeaveIssue_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgLeaveIssue.CellClick
        If CInt(e.RowIndex) < 0 Then Exit Sub
        If blnViewer Then dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Selected = False : Exit Sub
        Dim objCellStyle As New DataGridViewCellStyle
        Dim intholidayColor As Integer = 0
        Dim objFormNo As New clsleaveform
        Dim blnIsWeekend As Boolean = False
        Dim objFraction As New clsleaveday_fraction

        Try
            If e.RowIndex = -1 Then Exit Sub

            If e.ColumnIndex > 0 And dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString <> "" Then

                objCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                objFormNo._Formunkid = CInt(cboFormNo.SelectedValue)

                If blnEdit Then blnIsHoliday = True

                If (dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString >= eZeeDate.convertDate(objPending._Startdate) And _
                   dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString <= eZeeDate.convertDate(objPending._Enddate)) Then

                    'GET THE HOLIDAY COLOR
                    Dim objHoliday As New clsholiday_master
                    intholidayColor = objHoliday.GetHolidayColor(eZeeDate.convertDate(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString))


                    objShift._Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(eZeeDate.convertDate(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString()).Date, CInt(txtEmployee.Tag))
                    objShiftTran.GetShiftTran(objShift._Shiftunkid)
                    Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(eZeeDate.convertDate(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString).DayOfWeek.ToString) & " AND isweekend = 1")
                    If objShiftTran._dtShiftday IsNot Nothing AndAlso drShift.Length > 0 Then
                        blnIsWeekend = True
                    End If

                    If blnIssueOnHoliday = False And intholidayColor < 0 Then

                        If blnIssueOnWeekend And blnIsWeekend Then
                            If dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.FromArgb(mintColor) Then
                                objCellStyle.BackColor = Color.FromArgb(intholidayColor)
                                objCellStyle.ForeColor = Color.White
                                objCellStyle.SelectionBackColor = Color.FromArgb(intholidayColor)
                                objCellStyle.SelectionForeColor = Color.White
                                dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle

                                If CDec(objTotalLeaveDays.Text) = 0 Then
                                    objTotalLeaveDays.Text = CStr(objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid) - CDec(objTotalLeaveDays.Text))
                                Else
                                    objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) - objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))
                                End If

                                Exit Sub
                            ElseIf dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.FromArgb(intholidayColor) Then

                                If blnConsiderLVHlOnWk = True Then
                                    objCellStyle.BackColor = Color.FromArgb(intholidayColor)
                                    objCellStyle.ForeColor = Color.White
                                    objCellStyle.SelectionBackColor = Color.FromArgb(intholidayColor)
                                    objCellStyle.SelectionForeColor = Color.White
                                    dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                                    Exit Sub
                                End If


                                objCellStyle.BackColor = Color.FromArgb(mintColor)
                                objCellStyle.ForeColor = Color.White
                                objCellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                objCellStyle.SelectionForeColor = Color.White
                                dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                                If CDec(objTotalLeaveDays.Text) = 0 Then
                                    objTotalLeaveDays.Text = CStr(objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid) + CDec(objTotalLeaveDays.Text))
                                Else
                                    objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) + objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))
                                End If
                                Exit Sub

                            End If
                        ElseIf blnIssueOnWeekend = False AndAlso blnIsWeekend = False Then
                            objCellStyle.BackColor = Color.FromArgb(intholidayColor)
                            objCellStyle.ForeColor = Color.White
                            objCellStyle.SelectionBackColor = Color.FromArgb(intholidayColor)
                            objCellStyle.SelectionForeColor = Color.White
                            dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                            Exit Sub
                        ElseIf blnIssueOnWeekend = True OrElse blnIsWeekend = True Then
                            objCellStyle.BackColor = Color.FromArgb(intholidayColor)
                            objCellStyle.ForeColor = Color.White
                            objCellStyle.SelectionBackColor = Color.FromArgb(intholidayColor)
                            objCellStyle.SelectionForeColor = Color.White
                            dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                            Exit Sub
                        End If


                    ElseIf blnIssueOnHoliday And intholidayColor < 0 Then

                        If blnIssueOnWeekend And blnIsWeekend Then

SetColor1:
                            If dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.FromArgb(mintColor) Then
                                objCellStyle.BackColor = Color.FromArgb(intholidayColor)
                                objCellStyle.ForeColor = Color.White
                                objCellStyle.SelectionBackColor = Color.FromArgb(intholidayColor)
                                objCellStyle.SelectionForeColor = Color.White
                                dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle

                                If CDec(objTotalLeaveDays.Text) = 0 Then
                                    objTotalLeaveDays.Text = CStr(objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid) - CDec(objTotalLeaveDays.Text))
                                Else
                                    objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) - objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))
                                End If
                                blnIsHoliday = False
                                Exit Sub
                            ElseIf dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.FromArgb(intholidayColor) Then
                                Dim mdecDayFraction As Decimal = objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid)

                                If blnConsiderLVHlOnWk = True AndAlso mdecDayFraction <= 0 Then
                                    objCellStyle.BackColor = Color.FromArgb(intholidayColor)
                                    objCellStyle.ForeColor = Color.White
                                    objCellStyle.SelectionBackColor = Color.FromArgb(intholidayColor)
                                    objCellStyle.SelectionForeColor = Color.White
                                ElseIf (blnConsiderLVHlOnWk = False AndAlso mdecDayFraction > 0) OrElse (blnConsiderLVHlOnWk = True AndAlso mdecDayFraction > 0) Then
                                    objCellStyle.BackColor = Color.FromArgb(mintColor)
                                    objCellStyle.ForeColor = Color.White
                                    objCellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                    objCellStyle.SelectionForeColor = Color.White
                                End If
                                dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                                If CDec(objTotalLeaveDays.Text) = 0 Then
                                    objTotalLeaveDays.Text = CStr(mdecDayFraction + CDec(objTotalLeaveDays.Text))
                                Else
                                    objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) + mdecDayFraction)
                                End If
                                blnIsHoliday = True
                                Exit Sub

                            End If
                        Else
                            GoTo SetColor1
                        End If

                    End If


                    '-- START FOR CHECK WEEKEND

                    '-- START objShiftTran._dtShiftday IF

                    If objShiftTran._dtShiftday IsNot Nothing Then

                        drShift = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(eZeeDate.convertDate(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString).DayOfWeek.ToString) & " AND isweekend = 1")

                        '--START drShift  IF
                        If drShift.Length > 0 Then

                            'START FOR CHECK ISSUE ON WEEKEND OR NOT
                            '-- START blnIssueOnWeekend IF

                            If blnIssueOnWeekend Then
                                If dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.White Or dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.Empty Then
                                    objCellStyle.BackColor = Color.FromArgb(mintColor)
                                    objCellStyle.ForeColor = Color.White
                                    objCellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                    objCellStyle.SelectionForeColor = Color.White
                                    objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) + objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))
                                    dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                                    Exit Sub

                                ElseIf dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor <> Color.White Or dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor <> Color.Empty Then

                                    If blnIssueOnHoliday AndAlso dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.FromArgb(intholidayColor) Then
                                        objCellStyle.BackColor = Color.FromArgb(mintColor)
                                        objCellStyle.ForeColor = Color.White
                                        objCellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                        objCellStyle.SelectionForeColor = Color.White
                                        objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) + objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))
                                        dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                                        Exit Sub
                                    ElseIf blnIssueOnHoliday AndAlso dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor <> Color.FromArgb(intholidayColor) AndAlso intholidayColor <> 0 Then
                                        objCellStyle.BackColor = Color.FromArgb(intholidayColor)
                                        objCellStyle.ForeColor = Color.White
                                        objCellStyle.SelectionBackColor = Color.FromArgb(intholidayColor)
                                        objCellStyle.SelectionForeColor = Color.White
                                        objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) - objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))
                                        dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                                        Exit Sub
                                    End If

                                End If

                            ElseIf blnIssueOnWeekend = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "This is Weekend holiday.You cannot issue leave for this day."), enMsgBoxStyle.Information)
                                Exit Sub

                            End If

                            '-- END blnIssueOnWeekend  IF

                        Else

                            If blnIssueOnHoliday = False Then
                                Dim objEmpHoliday As New clsemployee_holiday
                                If objEmpHoliday.GetEmployeeReCurrentHoliday(CInt(txtEmployee.Tag), eZeeDate.convertDate(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString())) Then
                                    Exit Sub
                                End If
                            End If

                            '--START  dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor  IF
                            If dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.White OrElse dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.Empty Then
                                objCellStyle.BackColor = Color.FromArgb(mintColor)
                                objCellStyle.ForeColor = Color.White
                                objCellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                objCellStyle.SelectionForeColor = Color.White

                                If CDec(objTotalLeaveDays.Text) = 0 Then
                                    objTotalLeaveDays.Text = CStr(objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid) + CDec(objTotalLeaveDays.Text))
                                Else
                                    objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) + objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))
                                End If
                                dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                                Exit Sub

                            ElseIf blnIssueOnHoliday AndAlso dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor <> Color.FromArgb(intholidayColor) AndAlso intholidayColor <> 0 Then
                                objCellStyle.BackColor = Color.FromArgb(intholidayColor)
                                objCellStyle.ForeColor = Color.White
                                objCellStyle.SelectionBackColor = Color.FromArgb(intholidayColor)
                                objCellStyle.SelectionForeColor = Color.White
                                objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) - objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))
                                dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                                Exit Sub
                            ElseIf blnIssueOnHoliday AndAlso dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.FromArgb(intholidayColor) Then
                                objCellStyle.BackColor = Color.FromArgb(mintColor)
                                objCellStyle.ForeColor = Color.White
                                objCellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                objCellStyle.SelectionForeColor = Color.White
                                objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) + objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))
                                dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                                Exit Sub
                            Else
                                objCellStyle.BackColor = Color.White
                                objCellStyle.ForeColor = Color.Black
                                objCellStyle.SelectionBackColor = Color.White
                                objCellStyle.SelectionForeColor = Color.Black

                                If CDec(objTotalLeaveDays.Text) = 0 Then
                                    objTotalLeaveDays.Text = CStr(objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid) - CDec(objTotalLeaveDays.Text))
                                Else
                                    objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) - objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))
                                End If
                                dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                                Exit Sub

                            End If   '-- END dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor  IF

                        End If    '--END drShift  IF

                    End If  '-- END objShiftTran._dtShiftday IF


                    '-- END FOR CHECK WEEKEND


                    If (dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.White Or _
                        dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor = Color.Empty) Then

                        Dim objEmpHoliday As New clsemployee_holiday
                        Dim dtEmpHoliday As DataTable = objEmpHoliday.GetEmployeeHoliday(CInt(txtEmployee.Tag), eZeeDate.convertDate(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString()))

                        If dtEmpHoliday IsNot Nothing AndAlso dtEmpHoliday.Rows.Count > 0 Then
                            objCellStyle.BackColor = Color.FromArgb(intholidayColor)
                            objCellStyle.ForeColor = Color.White
                            objCellStyle.SelectionBackColor = Color.FromArgb(intholidayColor)
                            objCellStyle.SelectionForeColor = Color.White
                            blnIsHoliday = True
                        Else
                            objCellStyle.BackColor = Color.FromArgb(mintColor)
                            objCellStyle.ForeColor = Color.White
                            objCellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                            objCellStyle.SelectionForeColor = Color.White
                            blnIsHoliday = False
                        End If

                        objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) + objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))


                        'CHECK FOR WHETHER ISSUE ON HOLDAY OR NOT WHEN ISSUE ON HOLIDAY IS TRUE FOR PARTICULAR LEAVE TYPE
                    ElseIf mintColor <> dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor.ToArgb And _
                          (dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor <> Color.White Or _
                           dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor <> Color.Empty) _
                           And blnIssueOnHoliday AndAlso blnIssueOnWeekend Then

                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Are you sure you want to remove Issue Leave on this Holiday?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            objCellStyle.BackColor = Color.Empty
                            objCellStyle.ForeColor = Color.Black
                            objCellStyle.SelectionBackColor = Color.White
                            objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) - objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))
                            blnIsHoliday = False
                        Else
                            blnIsHoliday = True
                            Exit Sub
                        End If



                        'CHECK FOR WHETHER ISSUE ON HOLDAY OR NOT WHEN ISSUE ON HOLIDAY IS TRUE AND TO ASSIGN HOLIDAY COLOR
                    ElseIf mintColor = dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor.ToArgb And blnIssueOnHoliday And blnIsHoliday And intholidayColor < 0 Then
                        objCellStyle.BackColor = Color.FromArgb(intholidayColor)
                        objCellStyle.ForeColor = Color.White
                        objCellStyle.SelectionBackColor = Color.FromArgb(intholidayColor)
                        objCellStyle.SelectionForeColor = Color.White
                        blnIsHoliday = False
                        objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) - objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))

                        'CHECK FOR WHETHER ISSUE ON HOLDAY OR NOT WHEN ISSUE ON HOLIDAY IS FALSE FOR PARTICULAR LEAVE TYPE
                    ElseIf mintColor <> dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor.ToArgb And _
                           (dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor <> Color.White Or _
                           dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor <> Color.Empty) And blnIssueOnHoliday = False Or blnIssueOnWeekend = False Then
                        Exit Sub
                    Else
                        objCellStyle.BackColor = Color.White
                        objCellStyle.ForeColor = Color.Black
                        objCellStyle.SelectionBackColor = Color.White
                        objCellStyle.SelectionForeColor = Color.Black
                        objTotalLeaveDays.Text = CStr(CDec(objTotalLeaveDays.Text) - objFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Tag.ToString(), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid))

                    End If

                    dgLeaveIssue.Rows(e.RowIndex).Cells(e.ColumnIndex).Style = objCellStyle
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLeaveIssue_CellClick", mstrModuleName)
        Finally
            objCellStyle = Nothing
        End Try
    End Sub

    Private Sub dgLeaveIssue_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgLeaveIssue.Scroll
        Try
            dgLeaveIssue.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLeaveIssue_Scroll", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If CInt(cboFormNo.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Form No is compulsory information.Please Select Form No."), enMsgBoxStyle.Information)
                cboFormNo.Select()
                Exit Sub
            ElseIf CInt(cboLeaveType.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)
                cboLeaveType.Select()
                Exit Sub
            End If

            If gobjEmailList.Count > 0 AndAlso ConfigParameter._Object._Notify_IssuedLeave_Users.Trim.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sending Email(s) process is in progress from other module. Please wait."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim arGetdate() As String = GetLeavedays()

            Dim mdblIssueAmount As Decimal = GetLeavedaysBalance(arGetdate)
            If arGetdate Is Nothing OrElse arGetdate.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Leave Date is compulsory information.Please Select Leave Date between given Leave Form Period."), enMsgBoxStyle.Information)
                dgLeaveIssue.Select()
                Exit Sub
            End If




            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            Dim objAccruetran As New clsleavebalance_tran
            If objLeaveType._IsPaid And objLeaveType._IsAccrueAmount Then

                Dim drAccrue() As DataRow = Nothing


                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.

                'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), False, False, False, "", Nothing).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                '    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                'End If


                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                     , True, True, True, True, False, CInt(txtEmployee.Tag), False, False, False, "", Nothing).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                '    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                     , True, True, True, True, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                'End If

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                     , True, True, True, True, False, CInt(txtEmployee.Tag), False, False, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                     , True, True, True, True, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                End If

                'Pinkal (01-Mar-2016) -- End


                'Pinkal (06-Jan-2016) -- End

                If drAccrue.Length = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please Enter Accrue amount for this paid leave."), enMsgBoxStyle.Information)
                    Exit Sub
                End If


            ElseIf objLeaveType._IsPaid And objLeaveType._IsAccrueAmount = False And objLeaveType._DeductFromLeaveTypeunkid > 0 Then

                Dim drAccrue() As DataRow = Nothing

                'Pinkal (02-Jan-2018) -- Start
                'Enhancement - Ref 125 For leave types whose days are deducted from leave types of accrue nature, don't allow employee to apply more than the maximum set on the Add/Edit Leave type screen. 
                'As at now, employee can apply more than the set maximum; approver is only given a warning if he still wishes to issue. If he still wants to approve even with the exceeding leave days he can approve.
                'Control to be put on application/not just issuing. employee to be warned and stopped.

                'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), False, False, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                '    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                'End If

                Dim dsList As DataSet = Nothing
                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'dsList = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                    '                                                 , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                    '                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), False, False, False, "", Nothing, mblnIsExternalApprover)

                    dsList = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, False, False, CInt(txtEmployee.Tag), False, False, False, "", Nothing, mblnIsExternalApprover)
                    'Pinkal (26-Feb-2019) -- End

                    
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'dsList = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                    '                                                 , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                    '                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing, mblnIsExternalApprover)
                    dsList = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, False, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing, mblnIsExternalApprover)
                    'Pinkal (26-Feb-2019) -- End
                End If

                drAccrue = dsList.Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)

                If drAccrue.Length = 0 Then
                    objLeaveType._Leavetypeunkid = objLeaveType._DeductFromLeaveTypeunkid
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "This particular leave is mapped with ") & objLeaveType._Leavename & Language.getMessage(mstrModuleName, 14, " , which does not have the accrue amount. Please add accrue amount for ") & objLeaveType._Leavename & Language.getMessage(mstrModuleName, 15, " inorder to issue leave."), enMsgBoxStyle.Information)
                    Exit Sub

                Else

                    Dim mdblIssue As Decimal = 0
                    Dim mdtStartDate As Date = Nothing
                    Dim mdtEndDate As Date = Nothing

                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC AndAlso drAccrue.Length > 0 Then
                        mdtStartDate = CDate(drAccrue(0)("startdate")).Date
                        mdtEndDate = CDate(drAccrue(0)("enddate")).Date
                    End If

                    If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                        mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(txtEmployee.Tag), CInt(cboLeaveType.SelectedValue), FinancialYear._Object._YearUnkid, Nothing, mdtStartDate, mdtEndDate)
                    Else
                        mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(txtEmployee.Tag), CInt(cboLeaveType.SelectedValue), FinancialYear._Object._YearUnkid)
                    End If

                    If objLeaveType._MaxAmount < (mdblIssue + mdblIssueAmount) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), CType(enMsgBoxStyle.Question + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    'Pinkal (02-Jan-2018) -- End
                End If


            ElseIf objLeaveType._IsPaid And objLeaveType._IsShortLeave Then

                Dim drShortLeave() As DataRow = Nothing

                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '    drShortLeave = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), False, False, False, "", Nothing).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND isshortleave = 1 " & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                '    drShortLeave = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND isshortleave = 1 " & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                'End If

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'drShortLeave = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                    '                                                 , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                    '                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), False, False, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND isshortleave = 1 " & " AND yearunkid = " & FinancialYear._Object._YearUnkid)

                    drShortLeave = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, False, False, CInt(txtEmployee.Tag), False, False, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND isshortleave = 1 " & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                    'Pinkal (26-Feb-2019) -- End

                    
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'drShortLeave = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                    '                                                 , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                    '                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND isshortleave = 1 " & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                    drShortLeave = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, False, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND isshortleave = 1 " & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                    'Pinkal (26-Feb-2019) -- End
                End If

                'Pinkal (01-Mar-2016) -- End

                If drShortLeave.Length = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, there is no leave frequency accrue amount set for the selected short leave, please enter accrue amount for this leave from, Leave Information -> Leave Frequency."), enMsgBoxStyle.Information)
                    Exit Sub
                End If


            End If


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If CBool(cboLeaveType.Tag) = True Then  'Ispaid = True
                If mintLvformMinDays <> 0 And mintLvformMinDays > mdblIssueAmount Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, you cannot issue for this leave type as it has been configured with setting for minimum") & " " & mintLvformMinDays & " " & Language.getMessage(mstrModuleName, 28, "day(s) to be applied for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
                If mintLvformMaxDays <> 0 And mintLvformMaxDays < mdblIssueAmount Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry, you cannot issue for this leave type as it has been configured with setting for maximum") & " " & mintLvformMaxDays & " " & Language.getMessage(mstrModuleName, 28, "day(s) to be applied for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
            End If
            'Pinkal (08-Oct-2018) -- End



            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                Dim objLvForm As New clsleaveform
                objLvForm._Formunkid = CInt(cboFormNo.SelectedValue)
                Dim objPaymentTran As New clsPayment_tran
                Dim objPeriod As New clsMasterData

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                mblnConsiderLeaveOnTnAPeriod = objLvForm._ConsiderLeaveOnTnAPeriod
                'Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, objLvForm._Startdate.Date)
                Dim intPeriodId As Integer = 0
                If mblnConsiderLeaveOnTnAPeriod Then
                    intPeriodId = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, objLvForm._Startdate.Date)
                Else
                    intPeriodId = objPeriod.getCurrentPeriodID(enModuleReference.Payroll, objLvForm._Startdate.Date, FinancialYear._Object._YearUnkid, 0, False, True, Nothing, True)
                End If

                'Dim mdtTnAStartdate As Date = Nothing
                'If intPeriodId > 0 Then
                '    Dim objTnAPeriod As New clscommom_period_Tran
                '    mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                'End If
                'If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(txtEmployee.Tag), mdtTnAStartdate, intPeriodId, objLvForm._Startdate.Date) > 0 Then

                Dim mdtTnAStartdate As Date = Nothing
                If intPeriodId > 0 Then
                    Dim objTnAPeriod As New clscommom_period_Tran
                    If mblnConsiderLeaveOnTnAPeriod Then
                    mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                    Else
                        objTnAPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
                        mdtTnAStartdate = objTnAPeriod._Start_Date
                    End If
                    objTnAPeriod = Nothing
                End If

                'If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(txtEmployee.Tag), mdtTnAStartdate, intPeriodId, objLvForm._Startdate.Date) > 0 Then
                If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(txtEmployee.Tag), mdtTnAStartdate, intPeriodId, objLvForm._Startdate.Date, "", Nothing, mblnConsiderLeaveOnTnAPeriod) > 0 Then
                    'Pinkal (01-Jan-2019) -- End
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to Issue this leave ?  "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                End If
                objLvForm = Nothing
            End If

            SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objLeaveIssueMaster._FormName = mstrModuleName
            objLeaveIssueMaster._LoginEmployeeunkid = 0
            objLeaveIssueMaster._ClientIP = getIP()
            objLeaveIssueMaster._HostName = getHostName()
            objLeaveIssueMaster._FromWeb = False
            objLeaveIssueMaster._AuditUserId = User._Object._Userunkid
objLeaveIssueMaster._CompanyUnkid = Company._Object._Companyunkid
            objLeaveIssueMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If mintLeaveIssueUnkid > 0 Then

                If mdtIssuetran.Tables(0).Rows.Count > 0 Then

                    If CheckForUpdate(mdtIssuetran.Tables(0), arGetdate) And GetPromptForBalance(mdblIssueAmount, True, mdblSavedLeavedays) Then


                        'Pinkal (26-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                        'blnFlag = objLeaveIssueMaster.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                        '                                                         , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate _
                        '                                                         , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True _
                        '                                                         , arGetdate, CBool(cboLeaveType.Tag), mdblLeaveAmout, ConfigParameter._Object._LeaveBalanceSetting _
                        '                                                         , ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString, objPending._Approverunkid, True)

                        blnFlag = objLeaveIssueMaster.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate _
                                                                                , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True _
                                                                                , arGetdate, CBool(cboLeaveType.Tag), mdblLeaveAmout, ConfigParameter._Object._LeaveBalanceSetting _
                                                                                 , ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString, objPending._Approverunkid, False)

                        'Pinkal (26-Feb-2019) -- End

                    Else
                        blnFlag = True
                    End If

                End If
            Else

                If blnIsHoliday Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "In this Leave form, Holiday is considered as leave.are you sure you want to save holiday(s) as leave?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                End If

                If GetPromptForBalance(mdblIssueAmount, False) Then

                    'Pinkal (29-Mar-2019) -- Start
                    'Bug - Worked on Leave Issue User Access Issue for all companies.

                    'blnFlag = objLeaveIssueMaster.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                    '                                                       , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                    '                                                       , True, ConfigParameter._Object._IsIncludeInactiveEmp, arGetdate, CBool(cboLeaveType.Tag), mdblLeaveAmout _
                    '                                                       , ConfigParameter._Object._LeaveBalanceSetting, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), objPending._Approverunkid, Nothing, True, mblnIsExternalApprover)

                    blnFlag = objLeaveIssueMaster.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                           , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                           , True, ConfigParameter._Object._IsIncludeInactiveEmp, arGetdate, CBool(cboLeaveType.Tag), mdblLeaveAmout _
                                                                        , ConfigParameter._Object._LeaveBalanceSetting, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), objPending._Approverunkid, Nothing, False, mblnIsExternalApprover, objLeaveType._Isexempt_from_payroll, ConfigParameter._Object._SkipEmployeeMovementApprovalFlow, ConfigParameter._Object._CreateADUserFromEmpMst)
                    'Sohail (21 Oct 2019) - [blnExemptFromPayroll, blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]
                    'Pinkal (29-Mar-2019) -- End



                End If

            End If



            If blnFlag = False And objLeaveIssueMaster._Message <> "" Then
                eZeeMsgBox.Show(objLeaveIssueMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then

                If ConfigParameter._Object._Notify_IssuedLeave_Users.Trim.Length > 0 Then

                    Dim objMaster As New clsMasterData
                    gobjEmailList = New List(Of clsEmailCollection)
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty

                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(txtEmployee.Tag)


                    For Each sId As String In ConfigParameter._Object._Notify_IssuedLeave_Users.Trim.Split(CChar(","))
                        If sId.Trim = "" Then Continue For
                        Dim dtUserAccess As DataTable = objUsr.GetUserAccessFromUser(CInt(sId), Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting)
                        If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then
                            Dim mblnFlag As Boolean = False
                            For Each AID As String In ConfigParameter._Object._UserAccessModeSetting.Trim.Split(CChar(","))
                                Dim drRow() As DataRow = Nothing
                                Select Case CInt(AID)

                                    Case enAllocation.DEPARTMENT
                                        drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Departmentunkid & " AND referenceunkid = " & enAllocation.DEPARTMENT)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case enAllocation.JOBS
                                        drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Jobunkid & " AND referenceunkid = " & enAllocation.JOBS)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case enAllocation.CLASS_GROUP
                                        drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classgroupunkid & " AND referenceunkid = " & enAllocation.CLASS_GROUP)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case enAllocation.CLASSES
                                        drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classunkid & " AND referenceunkid = " & enAllocation.CLASSES)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case Else
                                        mblnFlag = False
                                End Select

                            Next

                            If mblnFlag Then
                                objUsr._Userunkid = CInt(sId)

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objLeaveIssueMaster._FormName = mstrModuleName
                                objLeaveIssueMaster._LoginEmployeeunkid = 0
                                objLeaveIssueMaster._ClientIP = getIP()
                                objLeaveIssueMaster._HostName = getHostName()
                                objLeaveIssueMaster._FromWeb = False
                                objLeaveIssueMaster._AuditUserId = User._Object._Userunkid
objLeaveIssueMaster._CompanyUnkid = Company._Object._Companyunkid
                                objLeaveIssueMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                'S.SANDEEP [28-May-2018] -- END

                                StrMessage = objLeaveIssueMaster.SetNotificationForIssuedUser(objUsr._Firstname & " " & objUsr._Lastname, txtEmployee.Text, cboFormNo.Text, cboLeaveType.Text, objPending._Startdate.Date, objPending._Enddate.Date)
                                gobjEmailList.Add(New clsEmailCollection(objUsr._Email, Language.getMessage(mstrModuleName, 22, "Issued Leave Application form notification"), StrMessage, mstrModuleName, 0, getIP(), getHostName(), 0, 0, 0, ""))
                            End If

                        End If
                    Next
                    objEmployee = Nothing
                    objUsr = Nothing
                    trd = New Thread(AddressOf Send_Notification)
                    trd.IsBackground = True
                    trd.Start()
                End If

                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objLeaveIssueMaster = Nothing
                    objLeaveIssueMaster = New clsleaveissue_master
                    GetValue()
                    cboLeaveYear.Select()
                Else
                    mintLeaveIssueUnkid = objLeaveIssueMaster._Leaveissueunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Dim strSearching As String = String.Empty
        Dim strFormSearching As String = String.Empty
        Try
            If dtpFromDate.Checked = False And dtpToDate.Checked = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "From Date is compulsory information.Please Select From Date."), enMsgBoxStyle.Information)
                dtpFromDate.Select()
                Exit Sub
            ElseIf dtpFromDate.Checked = True And dtpToDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "To Date is compulsory information.Please Select To Date."), enMsgBoxStyle.Information)
                dtpToDate.Select()
                Exit Sub

            ElseIf txtEmpviewer.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
                Exit Sub

            End If

            If dtpFromDate.Checked Then
                strSearching &= "AND leavedate >= '" & eZeeDate.convertDate(dtpFromDate.Value.Date) & "' "
                strFormSearching &= "AND startdate >= '" & eZeeDate.convertDate(dtpFromDate.Value.Date) & "' "
            End If

            If dtpToDate.Checked Then
                strSearching &= "AND leavedate <= '" & eZeeDate.convertDate(dtpToDate.Value.Date) & "' "
                strFormSearching &= "AND returndate <= '" & eZeeDate.convertDate(dtpToDate.Value.Date) & "' "
            End If

            If strSearching.Length > 0 And strFormSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                strFormSearching = strFormSearching.Substring(3)
                GetDataForViewer(strSearching, strFormSearching)
            Else
                GetDataForViewer()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If FinancialYear._Object._Database_End_Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpFromDate.Value = FinancialYear._Object._Database_End_Date
                dtpToDate.Value = FinancialYear._Object._Database_End_Date
            ElseIf FinancialYear._Object._Database_End_Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            End If
            dtpFromDate.Checked = False
            dtpToDate.Checked = False
            GetDataForViewer()

            txtEmpviewer.Tag = Nothing
            txtEmpviewer.Text = ""
            dgLeaveIssue.Rows.Clear()
            mstrAdvanceFilter = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try

            Dim objEmployee As New clsEmployee_Master

            'Pinkal (06-May-2014) -- Start
            'Enhancement : TRA Changes
            'Dim dsFill As DataSet = objEmployee.GetEmployeeList("List", False, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsFill As DataSet = objEmployee.GetList("List", False, True, , , , , , mstrAdvanceFilter)

            'S.SANDEEP [15 NOV 2016] -- START
            'ENHANCEMENT : QUERY OPTIMIZATION
            'Dim dsFill As DataSet = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                                            User._Object._Userunkid, _
            '                                            FinancialYear._Object._YearUnkid, _
            '                                            Company._Object._Companyunkid, _
            '                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                            ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                            False, _
            '                                            "List", _
            '                                            ConfigParameter._Object._ShowFirstAppointmentDate, , , mstrAdvanceFilter)

            Dim dsFill As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        FinancialYear._Object._YearUnkid, _
                                                        Company._Object._Companyunkid, _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                ConfigParameter._Object._UserAccessModeSetting, True, False, "List", False, , , , , , , , , , , , , , , mstrAdvanceFilter)
            'S.SANDEEP [15 NOV 2016] -- END


            'S.SANDEEP [04 JUN 2015] -- END


            txtEmpviewer.Tag = Nothing
            txtEmpviewer.Text = ""
            dgLeaveIssue.Rows.Clear()

            Dim objSearch As New frmCommonSearch
            objSearch.DataSource = dsFill.Tables(0)

            objSearch.ValueMember = "employeeunkid"
            'S.SANDEEP [15 NOV 2016] -- START
            'ENHANCEMENT : QUERY OPTIMIZATION
            'objSearch.DisplayMember = "name"
            objSearch.DisplayMember = "employeename"
            'S.SANDEEP [15 NOV 2016] -- END
            objSearch.CodeMember = "employeecode"

            If objSearch.DisplayDialog Then
                txtEmpviewer.Tag = objSearch.SelectedValue

                'S.SANDEEP [15 NOV 2016] -- START
                'ENHANCEMENT : QUERY OPTIMIZATION
                'txtEmpviewer.Text = objSearch.SelectedText
                txtEmpviewer.Text = objSearch.SelectedAlias & " - " & objSearch.SelectedText
                'S.SANDEEP [15 NOV 2016] -- END


                'Dim lvItem As ListViewItem = lvEmployee.FindItemWithText(txtEmpviewer.Text.Trim)
                'If lvItem IsNot Nothing Then
                '    lvItem.Selected = True
                '    lvEmployee.Focus()
                '    lvEmployee.TopItem = lvItem
                'End If

                objbtnSearch_Click(sender, e)

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboLeaveYear.BackColor = GUI.ColorComp
            cboFormNo.BackColor = GUI.ColorComp
            cboLeaveType.BackColor = GUI.ColorComp
            txtEmployee.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If blnViewer = False Then
                If mintFormNounkid > 0 And objLeaveIssueMaster._Leaveissueunkid > 0 Then
                    cboLeaveYear.SelectedValue = objLeaveIssueMaster._Leaveyearunkid
                    cboFormNo.SelectedValue = objLeaveIssueMaster._Formunkid
                    cboLeaveType.SelectedValue = objLeaveIssueMaster._Leavetypeunkid
                    txtEmployee.Tag = objLeaveIssueMaster._Employeeunkid


                    mdtIssuetran = objLeaveIssueTran.GetList("IssueTran")
                    Dim mdIssueTran As DataTable = New DataView(mdtIssuetran.Tables("IssueTran"), "leaveissueunkid=" & mintLeaveIssueUnkid, "", DataViewRowState.CurrentRows).ToTable
                    mdtIssuetran.Tables.Clear()
                    mdtIssuetran.Tables.Add(mdIssueTran)

                Else
                    cboFormNo.SelectedValue = mintFormNounkid
                    cboLeaveType.SelectedValue = mintLeaveTypeunkid
                End If
                cboFormNo_SelectionChangeCommitted(Nothing, Nothing)
            Else

                'Pinkal (06-May-2014) -- Start
                'Enhancement : TRA Changes
                'If lvEmployee.Items.Count > 0 Then lvEmployee.Items(0).Selected = True
                'Pinkal (06-May-2014) -- End
                GetDataForViewer()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objLeaveIssueMaster._Leaveyearunkid = FinancialYear._Object._YearUnkid
            objLeaveIssueMaster._Formunkid = CInt(cboFormNo.SelectedValue)
            objLeaveIssueMaster._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            objLeaveIssueMaster._Employeeunkid = CInt(txtEmployee.Tag)
            objLeaveIssueMaster._Userunkid = User._Object._Userunkid
            Dim objfrm As New clsleaveform
            objfrm._Formunkid = objLeaveIssueMaster._Formunkid
            objLeaveIssueMaster._StartDate = objfrm._Startdate
            objLeaveIssueMaster._EndDate = objfrm._Returndate
            objLeaveIssueMaster._TotalIssue = CDec(objTotalLeaveDays.Text)

            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            mblnConsiderLeaveOnTnAPeriod = objfrm._ConsiderLeaveOnTnAPeriod
            objLeaveIssueMaster._ConsiderLeaveOnTnAPeriod = mblnConsiderLeaveOnTnAPeriod
            'Pinkal (01-Jan-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillIssueCombo()
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Try

            'FOR YEAR
            Dim objYear As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsFill = objYear.getComboListPAYYEAR("Year", True)
            dsFill = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboLeaveYear.ValueMember = "Id"
            cboLeaveYear.DisplayMember = "name"
            cboLeaveYear.DataSource = dsFill.Tables("Year")

            'FOR EMPLOYEE
            ' dsFill = Nothing
            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = CInt(txtEmployee.Tag)
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(txtEmployee.Tag)
            'S.SANDEEP [04 JUN 2015] -- END

            txtEmployee.Text = objEmployee._Firstname & " " & objEmployee._Surname


            'FOR LEAVE FORM
            dsFill = Nothing
            Dim objLeaveForm As New clsleaveform
            objLeaveForm._Leavetypeunkid = mintLeaveTypeunkid


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'dsFill = objLeaveForm.GetList("FormNo", True)
            'dtFill = New DataView(dsFill.Tables("FormNo"), "employeeunkid = " & CInt(txtEmployee.Tag) & " AND (statusunkid=1 or statusunkid = 7) AND formunkid = " & mintFormNounkid, "", DataViewRowState.CurrentRows).ToTable



            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'dsFill = objLeaveForm.GetList("FormNo", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
            '                                           , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
            '                                           , True, -1, True, -1, "AND lvleaveform.employeeunkid = " & CInt(txtEmployee.Tag) & " AND (lvleaveform.statusunkid=1 or lvleaveform.statusunkid = 7) AND lvleaveform.formunkid = " & mintFormNounkid)

            dsFill = objLeaveForm.GetList("FormNo", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                       , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                     , True, -1, True, -1, "AND lvleaveform.employeeunkid = " & CInt(txtEmployee.Tag) & " AND (lvleaveform.statusunkid=1 or lvleaveform.statusunkid = 7) AND lvleaveform.formunkid = " & mintFormNounkid, mblnIsExternalApprover)



            'Pinkal (01-Mar-2016) -- End


            cboFormNo.ValueMember = "formunkid"
            cboFormNo.DisplayMember = "formno"
            cboFormNo.DataSource = dsFill.Tables(0)

            'Pinkal (24-Aug-2015) -- End

            'FOR LEAVE TYPE
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.getListForCombo("LeaveType", True)
            dtFill = New DataView(dsFill.Tables("LeaveType"), "leavetypeunkid = " & mintLeaveTypeunkid, "", DataViewRowState.CurrentRows).ToTable
            cboLeaveType.ValueMember = "leavetypeunkid"
            cboLeaveType.DisplayMember = "name"
            cboLeaveType.DataSource = dtFill



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillViewerItems()
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Dim lvItem As ListViewItem
        Try

            'FOR EMPLOYEE
            'dsFill = Nothing
            'lvItem = Nothing
            'Dim objEmployee As New clsEmployee_Master
            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsFill = objEmployee.GetEmployeeList("List", False, False)
            'Else
            '    dsFill = objEmployee.GetEmployeeList("List", False, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If
            'lvEmployee.Items.Clear()
            'For Each drRow As DataRow In dsFill.Tables("List").Rows
            '    lvItem = New ListViewItem
            '    lvItem.Text = drRow("employeecode").ToString
            '    lvItem.Tag = drRow("employeeunkid")
            '    lvItem.SubItems.Add(drRow("employeename").ToString)
            '    lvEmployee.Items.Add(lvItem)
            'Next


            'FOR LEAVE TYPE
            lvItem = Nothing
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.GetList("LeaveType", True)
            lvLeaveCodes.Items.Clear()
            For Each drRow As DataRow In dsFill.Tables("LeaveType").Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("leavename").ToString
                lvItem.Tag = drRow("leavetypeunkid")
                lvLeaveCodes.Items.Add(lvItem)
                If CInt(drRow("color")) >= 0 Then
                    lvLeaveCodes.Items(lvItem.Index).ForeColor = Color.Black
                Else
                    lvLeaveCodes.Items(lvItem.Index).BackColor = Color.FromArgb(CInt(drRow("color")))
                    lvLeaveCodes.Items(lvItem.Index).ForeColor = Color.White
                End If
            Next

            'Pinkal (06-May-2014) -- Start
            'Enhancement : TRA Changes

            'If lvLeaveCodes.Items.Count > 8 Then
            '    colhLeavetype.Width = 233 - 20
            'Else
            '    colhLeavetype.Width = 233
            'End If

            If lvLeaveCodes.Items.Count > 8 Then
                colhLeavetype.Width = 450 - 20
            Else
                colhLeavetype.Width = 450
            End If

            'Pinkal (06-May-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillViewerItems", mstrModuleName)
        End Try
    End Sub

    Private Sub GenerateCalendar()
        Try
            Dim objColumn As DataGridViewColumn
            For i As Integer = 0 To 6
                objColumn = New DataGridViewColumn
                objColumn.HeaderCell.Style.Font = New Font("Tahoma", 8.25, FontStyle.Bold)
                If i = 0 Then
                    objColumn.Width = 130
                    objColumn.Name = "colhMonth"
                    objColumn.HeaderText = Language.getMessage(mstrModuleName, 12, "Month")
                    objColumn.Frozen = True
                    objColumn.Resizable = DataGridViewTriState.False
                    dgLeaveIssue.Columns.Add(objColumn)
                Else
                    GenerateWeekDay()
                End If
            Next
            mintColumnNo = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenerateCalendar", mstrModuleName)
        End Try
    End Sub

    Private Sub GenerateWeekDay()
        Try
            Dim objColumn As DataGridViewColumn
            Dim dtDays() As String = CultureInfo.CurrentCulture.DateTimeFormat.DayNames

            For i As Integer = 0 To dtDays.Length - 1
                objColumn = New DataGridViewColumn
                objColumn.Name = "objcolh" & mintColumnNo + 1
                objColumn.HeaderCell.Style.Font = New Font("Tahoma", 8.25, FontStyle.Bold)
                objColumn.Width = 60
                objColumn.HeaderText = dtDays(i).ToString.Substring(0, 3)
                objColumn.Resizable = DataGridViewTriState.False
                dgLeaveIssue.Columns.Add(objColumn)
                mintColumnNo += 1
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenerateDay", mstrModuleName)
        End Try
    End Sub

    Private Sub GenerateDays(ByVal arStrMonthname() As String, ByVal arStrMonthdays() As String, ByVal arStrFirstday() As String, ByVal arStrDays() As String, Optional ByVal arDaysColor() As String = Nothing, Optional ByVal arStrHoliday() As String = Nothing, Optional ByVal arStrHolidayColor() As String = Nothing)
        Try
            Dim objRow As DataGridViewRow
            Dim objCell As DataGridViewCell
            Dim objcellStyle As DataGridViewCellStyle
            Dim objLeaveFraction As New clsleaveday_fraction
            Dim mintEmployeeunkid As Integer = -1

            If blnViewer Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : TRA Changes
                'mintEmployeeunkid = CInt(lvEmployee.SelectedItems(0).Tag)
                mintEmployeeunkid = CInt(txtEmpviewer.Tag)
                'Pinkal (06-May-2014) -- End
            Else
                mintEmployeeunkid = CInt(txtEmployee.Tag)
            End If

            For i As Integer = 0 To arStrMonthname.Length - 1
                If arStrMonthname.GetValue(i) Is Nothing Then Continue For
                objRow = New DataGridViewRow
                If dgLeaveIssue.Columns.Count = 0 Then Exit Sub
                For k As Integer = 0 To dgLeaveIssue.Columns.Count - 1
                    objCell = New DataGridViewTextBoxCell()
                    objcellStyle = New DataGridViewCellStyle
                    objcellStyle.SelectionBackColor = Color.Transparent
                    objcellStyle.SelectionForeColor = Color.Black
                    If k = 0 Then
                        objCell.Value = arStrMonthname.GetValue(i).ToString
                        objcellStyle.Font = New Font("Tahoma", 8.25, FontStyle.Bold)
                        objcellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                    Else
                        objcellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        objCell.Value = ""
                    End If
                    objCell.Style = objcellStyle
                    objRow.Resizable = DataGridViewTriState.False
                    objRow.Cells.Add(objCell)
                Next
                dgLeaveIssue.Rows.Add(objRow)
                dgLeaveIssue.Rows(i).ReadOnly = True
            Next

            'STRAT FOR GENERATE DAY FOR SPECIFIC MONTH

            Dim intWeekNo As Integer = 0
            Dim Daycount As Integer = 0
            Dim Dayholiday As Integer = 0

            For j As Integer = 0 To arStrMonthdays.Length - 1

                If arStrMonthdays.GetValue(j) Is Nothing Then Continue For

                Dim intDays As Integer = CInt(arStrMonthdays.GetValue(j))
                Dim m As Integer = 0

                If CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Sunday) = arStrFirstday(j).ToString Then
                    intWeekNo = 1
                ElseIf CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Monday) = arStrFirstday(j).ToString Then
                    intWeekNo = 2
                ElseIf CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Tuesday) = arStrFirstday(j).ToString Then
                    intWeekNo = 3
                ElseIf CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Wednesday) = arStrFirstday(j).ToString Then
                    intWeekNo = 4
                ElseIf CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Thursday) = arStrFirstday(j).ToString Then
                    intWeekNo = 5
                ElseIf CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Friday) = arStrFirstday(j).ToString Then
                    intWeekNo = 6
                ElseIf CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DayOfWeek.Saturday) = arStrFirstday(j).ToString Then
                    intWeekNo = 7
                End If


                For intRow As Integer = j To dgLeaveIssue.Rows.Count - 1

                    For intCell As Integer = intWeekNo To dgLeaveIssue.Rows(intRow).Cells.Count - 1

                        For m = m To intDays

                            If arStrMonthname(intRow).ToString = "" Then Continue For
                            If m = intDays Then Exit For
                            m += 1
                            dgLeaveIssue.Rows(intRow).Cells(intCell).Value = m

                            'START FOR GET MONTH NO FROM MONTH NAME
                            Dim MonthNo As Integer = CInt(DateTime.ParseExact(arStrMonthname(intRow).ToString.Substring(0, arStrMonthname(intRow).ToString.IndexOf("-")).Trim, "MMMM", CultureInfo.CurrentCulture).Month)
                            'END FOR GET MONTH NO FROM MONTH NAME

                            dgLeaveIssue.Rows(intRow).Cells(intCell).Tag = dgLeaveIssue.Rows(intRow).Cells(0).Value.ToString.Substring(dgLeaveIssue.Rows(intRow).Cells(0).Value.ToString.Length - 4) & MonthNo.ToString("0#") & m.ToString("0#")


                            If arStrDays Is Nothing Then Exit For

                            If arStrDays.Length > 0 Then

                                If Daycount = arStrDays.Length Then GoTo ForExit

                                If arStrDays(Daycount).ToString.Substring(arStrDays(Daycount).ToString.Length - 4).ToString = MonthNo.ToString("0#") & m.ToString("0#") Then
                                    Daycount += 1

                                    If dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString() = arStrDays(Daycount - 1).ToString() Then
                                        objcellStyle = New DataGridViewCellStyle
                                        objcellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                                        'Pinkal (15-Jul-2013) -- Start
                                        'Enhancement : TRA Changes

                                        'Dim mdclFraction As Decimal = objLeaveFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString(), CInt(cboFormNo.SelectedValue), mintEmployeeunkid)
                                        'If mdclFraction > 0 Then
                                        '    dgLeaveIssue.Rows(intRow).Cells(intCell).Value = m & " / " & mdclFraction
                                        'Else
                                        '    dgLeaveIssue.Rows(intRow).Cells(intCell).Value = m
                                        'End If

                                        'Pinkal (06-May-2014) -- Start
                                        'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]

                                        If blnIssueOnHoliday = False Then
                                            Dim objEmpHoliday As New clsemployee_holiday
                                            If objEmpHoliday.GetEmployeeReCurrentHoliday(mintEmployeeunkid, eZeeDate.convertDate(dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString())) Then
                                                Dim dtHoliday As DataTable = objEmpHoliday.GetEmployeeHoliday(mintEmployeeunkid, eZeeDate.convertDate(dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString()).AddYears(-1))
                                                If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                                                    objcellStyle.SelectionBackColor = Color.FromArgb(CInt(dtHoliday.Rows(0)("color")))
                                                    objcellStyle.BackColor = Color.FromArgb(CInt(dtHoliday.Rows(0)("color")))
                                                    objcellStyle.ForeColor = Color.White
                                                    objcellStyle.SelectionBackColor = Color.FromArgb(CInt(dtHoliday.Rows(0)("color")))
                                                    objcellStyle.SelectionForeColor = Color.White
                                                    dgLeaveIssue.Rows(intRow).Cells(intCell).Style = objcellStyle
                                                End If
                                                Exit For
                                            End If
                                        End If

                                        'Pinkal (06-May-2014) -- End

                                        'Pinkal (27-May-2014) -- Start
                                        'Enhancement : OMAN Changes [Adding Leave Day Fraction In Login Summary FOR Getting Proper Leave Day Fraction]
                                        Dim mdclFraction As Decimal
                                        If blnViewer = False Then
                                            mdclFraction = objLeaveFraction.GetEmployeeLeaveDay_Fraction(dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString(), CInt(cboFormNo.SelectedValue), mintEmployeeunkid, "", objPending._Approverunkid)
                                            If mdclFraction > 0 Then
                                                dgLeaveIssue.Rows(intRow).Cells(intCell).Value = m & " / " & mdclFraction
                                            Else
                                                dgLeaveIssue.Rows(intRow).Cells(intCell).Value = m
                                            End If
                                        Else
                                            mdclFraction = objLeaveIssueTran.GetIssuedDayFractionForViewer(mintEmployeeunkid, dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString())
                                            If mdclFraction > 0 Then
                                                dgLeaveIssue.Rows(intRow).Cells(intCell).Value = m & " / " & mdclFraction
                                            Else
                                                dgLeaveIssue.Rows(intRow).Cells(intCell).Value = m
                                            End If
                                        End If

                                        'Pinkal (27-May-2014) -- End

                                        'Pinkal (15-Jul-2013) -- End


                                        If blnViewer Then   '''''START FOR blnViewer

                                            If CInt(arDaysColor(Daycount - 1)) < 0 Then
                                                objcellStyle.SelectionBackColor = Color.FromArgb(CInt(arDaysColor(Daycount - 1)))
                                                objcellStyle.BackColor = Color.FromArgb(CInt(arDaysColor(Daycount - 1)))
                                                objcellStyle.ForeColor = Color.White
                                                objcellStyle.SelectionBackColor = Color.FromArgb(CInt(arDaysColor(Daycount - 1)))
                                                objcellStyle.SelectionForeColor = Color.White
                                            End If

                                        Else

                                            'Pinkal (15-Oct-2013) -- Start
                                            'Enhancement : TRA Changes
                                            objShift._Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(eZeeDate.convertDate(dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString()).Date, CInt(txtEmployee.Tag))
                                            objShiftTran.GetShiftTran(objShift._Shiftunkid)
                                            'Pinkal (15-Oct-2013) -- End

                                            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(eZeeDate.convertDate(dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString).DayOfWeek.ToString()) & " AND isweekend = 1")
                                            If mintColor < 0 Then

                                                If drShift.Length > 0 Then

                                                    If blnEdit And arStrDays(Daycount - 1) = dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString() Then
                                                        objcellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                                        objcellStyle.BackColor = Color.FromArgb(mintColor)
                                                        objcellStyle.ForeColor = Color.White
                                                        objcellStyle.SelectionForeColor = Color.FromArgb(mintColor)
                                                    Else

                                                        'Pinkal (01-Feb-2014) -- Start
                                                        'Enhancement : TRA Changes
                                                        If blnIssueOnWeekend Then
                                                            objcellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                                            objcellStyle.BackColor = Color.FromArgb(mintColor)
                                                            objcellStyle.ForeColor = Color.White
                                                            objcellStyle.SelectionForeColor = Color.FromArgb(mintColor)
                                                        Else
                                                            objcellStyle.BackColor = Color.White
                                                            objcellStyle.ForeColor = Color.Black
                                                            objcellStyle.SelectionBackColor = Color.White
                                                            objcellStyle.SelectionForeColor = Color.Black
                                                        End If
                                                        'Pinkal (01-Feb-2014) -- End 

                                                    End If

                                                Else
                                                    objcellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                                    objcellStyle.BackColor = Color.FromArgb(mintColor)
                                                    objcellStyle.ForeColor = Color.White
                                                End If

                                            ElseIf drShift.Length > 0 Then
                                                objcellStyle.BackColor = Color.White
                                                objcellStyle.ForeColor = Color.Black
                                                objcellStyle.SelectionBackColor = Color.White
                                                objcellStyle.SelectionForeColor = Color.Black
                                            End If

                                        End If  '''''END FOR blnViewer



                                        If arStrHoliday IsNot Nothing Then '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''START FOR HOLIDAY 

                                            If Dayholiday < arStrHoliday.Length Then

                                                If arStrHoliday(Dayholiday) = dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString Then

                                                    If blnIssueOnHoliday Then 'START CHECK FOR UNPAID LEAVE ISSUE ON HOLIDAY OR NOT..

                                                        'CHECK AND ASSIGN HOLIDAY COLOR ON THAT DATE...
                                                        If objLeaveIssueTran.isIssueLeaveOnHoliday(CInt(cboLeaveYear.SelectedValue), CInt(cboFormNo.SelectedValue), _
                                                           CInt(cboLeaveType.SelectedValue), CInt(txtEmployee.Tag), _
                                                           eZeeDate.convertDate(dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString)) Then

                                                            objcellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                                            objcellStyle.BackColor = Color.FromArgb(mintColor)
                                                            objcellStyle.ForeColor = Color.White
                                                        Else 'START FOR CHECK WHETHER HOLIDAY HAS COLOR OR NOT

                                                            If CInt(arStrHolidayColor(Dayholiday)) <> 0 Then

                                                                'Pinkal (01-Feb-2014) -- Start
                                                                'Enhancement : TRA Changes

                                                                If blnIssueOnWeekend AndAlso blnConsiderLVHlOnWk = False Then
                                                                    objcellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                                                    objcellStyle.BackColor = Color.FromArgb(mintColor)
                                                                    objcellStyle.ForeColor = Color.White
                                                                ElseIf blnIssueOnWeekend = False AndAlso mdclFraction <= 0 Then
                                                                    objcellStyle.SelectionBackColor = Color.FromArgb(CInt(arStrHolidayColor(Dayholiday)))
                                                                    objcellStyle.BackColor = Color.FromArgb(CInt(arStrHolidayColor(Dayholiday)))
                                                                    objcellStyle.ForeColor = Color.White
                                                                ElseIf blnIssueOnWeekend = False AndAlso blnConsiderLVHlOnWk = False AndAlso mdclFraction > 0 Then
                                                                    objcellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                                                    objcellStyle.BackColor = Color.FromArgb(mintColor)
                                                                    objcellStyle.ForeColor = Color.White
                                                                End If

                                                                'Pinkal (03-Jan-2014) -- Start
                                                                'Enhancement : Oman Changes
                                                                blnIsHoliday = True
                                                                'Pinkal (03-Jan-2014) -- End

                                                            Else

                                                                'Pinkal (01-Feb-2014) -- Start
                                                                'Enhancement : TRA Changes
                                                                If blnIssueOnWeekend AndAlso mdclFraction > 0 Then
                                                                    objcellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                                                    objcellStyle.BackColor = Color.FromArgb(mintColor)
                                                                    objcellStyle.ForeColor = Color.White
                                                                    blnIsHoliday = True
                                                                    'ElseIf blnIssueOnWeekend = False AndAlso mdclFraction <= 0 AndAlso blnConsiderLVHlOnWk = True Then
                                                                    '    objcellStyle.SelectionBackColor = Color.FromArgb(CInt(arStrHolidayColor(Dayholiday)))
                                                                    '    objcellStyle.BackColor = Color.FromArgb(CInt(arStrHolidayColor(Dayholiday)))
                                                                    '    objcellStyle.ForeColor = Color.White
                                                                Else
                                                                    objcellStyle.BackColor = Color.White
                                                                    objcellStyle.ForeColor = Color.Black
                                                                End If
                                                                'Pinkal (01-Feb-2014) -- End

                                                            End If

                                                        End If 'END FOR CHECK WHETHER HOLIDAY HAS COLOR OR NOT

                                                    ElseIf blnIssueOnHoliday = False Then


                                                        'Pinkal (01-Feb-2014) -- Start
                                                        'Enhancement : TRA Changes

                                                        If blnIssueOnWeekend AndAlso mdclFraction > 0 Then
                                                            objcellStyle.SelectionBackColor = Color.FromArgb(mintColor)
                                                            objcellStyle.BackColor = Color.FromArgb(mintColor)
                                                            objcellStyle.ForeColor = Color.White
                                                            blnIsHoliday = True
                                                        Else
                                                            'START FOR CHECK WHETHER HOLIDAY HAS COLOR OR NOT
                                                            If CInt(arStrHolidayColor(Dayholiday)) <> 0 Then
                                                                objcellStyle.SelectionBackColor = Color.FromArgb(CInt(arStrHolidayColor(Dayholiday)))
                                                                objcellStyle.BackColor = Color.FromArgb(CInt(arStrHolidayColor(Dayholiday)))
                                                                objcellStyle.ForeColor = Color.White
                                                            Else
                                                                objcellStyle.BackColor = Color.White
                                                                objcellStyle.ForeColor = Color.Black
                                                            End If
                                                            'END FOR CHECK WHETHER HOLIDAY HAS COLOR OR NOT
                                                        End If

                                                        'Pinkal (01-Feb-2014) -- End

                                                    End If

                                                    Dayholiday += 1

                                                End If 'END CHECK FOR UNPAID LEAVE ISSUE ON HOLIDAY OR NOT..

                                            End If



                                        End If ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''END FOR HOLIDAY
                                        dgLeaveIssue.Rows(intRow).Cells(intCell).Style = objcellStyle
                                    End If

                                End If

                            End If

ForExit:
                            Exit For

                        Next

                    Next

                    Exit For

                Next

            Next
            'END FOR GENERATE DAY FOR SPECIFIC MONTH

        Catch ex As Exception
            DisplayError.Show("-", ex.Message, "GenerateDays", mstrModuleName)
        Finally
            If arDaysColor IsNot Nothing Then arDaysColor = Nothing
            If arStrDays IsNot Nothing Then arStrDays = Nothing
            If arStrFirstday IsNot Nothing Then arStrFirstday = Nothing
            If arStrHoliday IsNot Nothing Then arStrHoliday = Nothing
            If arStrHolidayColor IsNot Nothing Then arStrHolidayColor = Nothing
            If arStrMonthdays IsNot Nothing Then arStrMonthdays = Nothing
            If arStrMonthname IsNot Nothing Then arStrMonthname = Nothing
        End Try
    End Sub

    Private Function GetLeavedays() As String()
        Dim arGetdate() As String = Nothing
        Dim arGetLeaveDate As ArrayList = Nothing
        Dim index As Integer = 0
        Try
            arGetLeaveDate = New ArrayList
            For intRow As Integer = 0 To dgLeaveIssue.Rows.Count - 1
                For intCell As Integer = 1 To dgLeaveIssue.Rows(intRow).Cells.Count - 1

                    'Pinkal (03-Jan-2014) -- Start
                    'Enhancement : Oman Changes  'FOR INCLUDE HOLIDAY IN TOTAL AS ISSUE ON HOLIDAY IS TRUE THEN IT INCLUDED

                    '' If dgLeaveIssue.Rows(intRow).Cells(intCell).Value.ToString() <> "" And dgLeaveIssue.Rows(intRow).Cells(intCell).Style.BackColor = Color.FromArgb(mintColor) Then


                    'Pinkal (01-Feb-2014) -- Start
                    'Enhancement : TRA Changes


                    If dgLeaveIssue.Rows(intRow).Cells(intCell).Value.ToString() = "" Then Continue For

                    'If blnIssueOnHoliday = True Then

                    '    objShift._Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(eZeeDate.convertDate(dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString()).Date, CInt(txtEmployee.Tag))
                    '    objShiftTran.GetShiftTran(objShift._Shiftunkid)

                    '    If objShiftTran._dtShiftday IsNot Nothing Then
                    '        Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(eZeeDate.convertDate(dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString()).DayOfWeek.ToString) & " AND isweekend = 1")
                    '        If drShift.Length > 0 AndAlso (dgLeaveIssue.Rows(intRow).Cells(intCell).Style.BackColor = Color.Empty OrElse dgLeaveIssue.Rows(intRow).Cells(intCell).Style.BackColor = Color.White OrElse dgLeaveIssue.Rows(intRow).Cells(intCell).Style.BackColor <> Color.FromArgb(mintColor)) Then
                    '            Continue For
                    '        End If

                    '    End If

                    '    If dgLeaveIssue.Rows(intRow).Cells(intCell).Value.ToString() <> "" AndAlso dgLeaveIssue.Rows(intRow).Cells(intCell).Style.BackColor <> Color.Empty AndAlso dgLeaveIssue.Rows(intRow).Cells(intCell).Style.BackColor <> Color.White Then
                    '        arGetLeaveDate.Add(eZeeDate.convertDate(dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString()).ToShortDateString)
                    '    End If

                    'Else

                    If dgLeaveIssue.Rows(intRow).Cells(intCell).Value.ToString() <> "" AndAlso dgLeaveIssue.Rows(intRow).Cells(intCell).Style.BackColor = Color.FromArgb(mintColor) Then
                        arGetLeaveDate.Add(eZeeDate.convertDate(dgLeaveIssue.Rows(intRow).Cells(intCell).Tag.ToString()).ToShortDateString)
                    End If

                    'End If

                    'Pinkal (01-Feb-2014) -- End

                Next
            Next

            If arGetLeaveDate IsNot Nothing Then
                arGetdate = CType(arGetLeaveDate.ToArray(GetType(String)), String())
            End If

            'Pinkal (03-Jan-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLeavedays", mstrModuleName)
        End Try
        Return arGetdate
    End Function

    Private Function GetLeavedaysBalance(ByVal ardays As String()) As Decimal
        Dim mblIssueBalance As Decimal = 0
        Try
            If ardays Is Nothing Then Return 0
            Dim objLeaveFraction As New clsleaveday_fraction
            For i As Integer = 0 To ardays.Length - 1

                'Pinkal (15-Jul-2013) -- Start
                'Enhancement : TRA Changes
                'mblIssueBalance += objLeaveFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(CDate(ardays(i))), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag))
                mblIssueBalance += objLeaveFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(CDate(ardays(i))), CInt(cboFormNo.SelectedValue), CInt(txtEmployee.Tag), "", objPending._Approverunkid)
                'Pinkal (15-Jul-2013) -- End
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLeavedays", mstrModuleName)
        End Try
        Return mblIssueBalance
    End Function

    Private Sub GetDataForViewer(Optional ByVal strSearching As String = Nothing, Optional ByVal strFormSearching As String = Nothing)
        Dim GetDayDiff As Integer = 0
        Dim GetMonthDiff As Integer = 0
        Dim GetYearDiff As Integer = 0
        Dim arStrDays() As String = Nothing
        Dim arStrMonth() As String = Nothing
        Dim arStrMonthDays() As String = Nothing
        Dim arStrFirstday() As String = Nothing
        Dim arDaysColor() As String = Nothing
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Dim mdtStartdate As DateTime = Nothing
        Dim mdtEnddate As DateTime = Nothing
        Try
            If blnViewer And User._Object.Privilege._AllowToViewLeaveViewer = False Then Exit Sub

            'Pinkal (06-May-2014) -- Start
            'Enhancement : TRA Changes

            ' If lvEmployee.SelectedItems.Count = 0 Then Exit Sub
            'If lvEmployee.SelectedItems(0).Tag IsNot Nothing Then

            If txtEmpviewer.Tag IsNot Nothing Then

                'Pinkal (06-May-2014) -- End


                dgLeaveIssue.Rows.Clear()
                dgLeaveIssue.Refresh()

                'Pinkal (06-May-2014) -- Start
                'Enhancement : TRA Changes
                'dsFill = objLeaveIssueMaster.GetEmployeeIssueData(CInt(lvEmployee.SelectedItems(0).Tag))
                dsFill = objLeaveIssueMaster.GetEmployeeIssueData(CInt(txtEmpviewer.Tag))
                'Pinkal (06-May-2014) -- End


                If strSearching Is Nothing Then
                    dtFill = dsFill.Tables(0)
                Else
                    dtFill = New DataView(dsFill.Tables(0), strSearching, "", DataViewRowState.CurrentRows).ToTable
                End If


                If dtFill.Rows.Count > 0 Then
                    Dim objForm As New clsleaveform

                    'Pinkal (24-Aug-2015) -- Start
                    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                    'Dim dsForm As DataSet = objForm.GetList("List", True, , , , , , " lvleaveform.employeeunkid=" & CInt(txtEmpviewer.Tag) & " AND (lvleaveform.statusunkid = 1 OR lvleaveform.statusunkid = 7)")
                    Dim dsForm As DataSet = objForm.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                   , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                   , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                   , True, -1, True, -1, "AND lvleaveform.employeeunkid=" & CInt(txtEmpviewer.Tag) & " AND (lvleaveform.statusunkid = 1 OR lvleaveform.statusunkid = 7)")
                    'Pinkal (24-Aug-2015) -- End


                    Dim dtForm As DataTable
                    dtForm = New DataView(dsForm.Tables("List"), "", "Approved_StartDate asc", DataViewRowState.CurrentRows).ToTable

                    If dtForm.Rows.Count = 0 Then Exit Sub

                    Dim arrSize As Integer = 0
                    Dim prearrSize As Integer = 0
                    For j As Integer = 0 To dtForm.Rows.Count - 1
                        mdtStartdate = eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString)
                        mdtEnddate = eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString)
                        GetMonthDiff = CInt(DateDiff(DateInterval.Month, eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString), eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString)))
                        GetYearDiff = CInt(DateDiff(DateInterval.Year, eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString), eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString)))
                        GetDayDiff = CInt(DateDiff(DateInterval.Day, eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString), eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString)))
                        arrSize += GetMonthDiff
                        If j = 0 Then
                            ReDim Preserve arStrMonth(GetMonthDiff)
                            ReDim Preserve arStrMonthDays(GetMonthDiff)
                            ReDim Preserve arStrFirstday(GetMonthDiff)
                        End If

                        If Array.IndexOf(arStrMonth, CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Year.ToString) <= 0 Or _
               Array.IndexOf(arStrMonth, CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).Year.ToString) <= 0 Then

                            If j <> 0 And _
                            (Array.IndexOf(arStrMonth, CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Year.ToString) < 0 Or _
                             Array.IndexOf(arStrMonth, CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).Year.ToString) < 0) Then
                                ReDim Preserve arStrMonth(arrSize) : ReDim Preserve arStrMonthDays(arrSize) : ReDim Preserve arStrFirstday(arrSize)
                            End If


                            If GetMonthDiff > 0 Then
                                Dim intMonth As Integer = 0
                                For i As Integer = prearrSize To arrSize

                                    If arStrMonth.Contains(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).AddMonths(intMonth).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Year.ToString) = False Then

                                        'Pinkal (03-Jan-2014) -- Start
                                        'Enhancement : Oman Changes
                                        'arStrMonth(i) = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).AddMonths(intMonth).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Year.ToString
                                        Dim intYear As Integer = CInt(DateDiff(DateInterval.Year, eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()), eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).AddMonths(intMonth)))
                                        arStrMonth(i) = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).AddMonths(intMonth).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).AddYears(intYear).Year.ToString
                                        'Pinkal (03-Jan-2014) -- End

                                        arStrMonthDays(i) = Date.DaysInMonth(eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Year, eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).AddMonths(intMonth).Month).ToString()
                                        Dim firstDayOfCurrentMonth As New DateTime(eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Year, eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).AddMonths(intMonth).Month, 1)
                                        arStrFirstday(i) = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(firstDayOfCurrentMonth.DayOfWeek)
                                        intMonth += 1

                                    ElseIf arStrMonth.Contains(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).AddMonths(intMonth).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).Year.ToString) = False Then
                                        'Pinkal (03-Jan-2014) -- Start
                                        'Enhancement : Oman Changes
                                        'arStrMonth(i) = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).AddMonths(intMonth).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).Year.ToString
                                        Dim intYear As Integer = CInt(DateDiff(DateInterval.Year, eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()), eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).AddMonths(intMonth)))
                                        arStrMonth(i) = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).AddMonths(intMonth).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).AddYears(intYear).Year.ToString
                                        'Pinkal (03-Jan-2014) -- End

                                        arStrMonthDays(i) = Date.DaysInMonth(eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).Year, eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).AddMonths(intMonth).Month).ToString()
                                        Dim firstDayOfCurrentMonth As New DateTime(eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).Year, eZeeDate.convertDate(dtForm.Rows(j)("Approved_EndDate").ToString()).AddMonths(intMonth).Month, 1)
                                        arStrFirstday(i) = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(firstDayOfCurrentMonth.DayOfWeek)
                                    End If
                                Next

                            Else

                                If Array.IndexOf(arStrMonth, CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Year.ToString) < 0 Then

                                    arStrMonth(arrSize) = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Month) & " - " & eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Year.ToString
                                    arStrMonthDays(arrSize) = Date.DaysInMonth(eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Year, eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Month).ToString()
                                    Dim firstDayOfCurrentMonth As New DateTime(eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Year, eZeeDate.convertDate(dtForm.Rows(j)("Approved_StartDate").ToString()).Month, 1)
                                    arStrFirstday(arrSize) = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(firstDayOfCurrentMonth.DayOfWeek)
                                    arrSize += 1
                                    prearrSize = arrSize
                                End If


                            End If
                        End If

                    Next


                    Dim objminDate As String = dtFill.Compute("min(leavedate)", "1=1").ToString
                    Dim objmaxDate As String = dtFill.Compute("max(leavedate)", "1=1").ToString

                    'START FOR HOLIDAY MASTER
                    Dim drrow As DataRow
                    Dim objHoliday As New clsemployee_holiday

                    'Pinkal (24-Aug-2015) -- Start
                    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                    'dsFill = objHoliday.GetList("Holiday", CInt(txtEmpviewer.Tag))
                    dsFill = objHoliday.GetList("Holiday", FinancialYear._Object._DatabaseName _
                                                            , User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                            , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, CInt(txtEmpviewer.Tag), Nothing, "", "")
                    'Pinkal (24-Aug-2015) -- End


                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : TRA Changes

                    'Dim dtholiday As DataTable = New DataView(dsFill.Tables("Holiday"), "employeeunkid = " & CInt(lvEmployee.SelectedItems(0).Tag) & "  AND holidaydate >='" & eZeeDate.convertDate(objminDate) _
                    '                           & "' AND holidaydate <= '" & eZeeDate.convertDate(objmaxDate) & "' AND color <> 0", "", _
                    '                           DataViewRowState.CurrentRows).ToTable

                    Dim dtholiday As DataTable = New DataView(dsFill.Tables("Holiday"), "employeeunkid = " & CInt(txtEmpviewer.Tag) & "  AND holidaydate >='" & eZeeDate.convertDate(objminDate) _
                                               & "' AND holidaydate <= '" & eZeeDate.convertDate(objmaxDate) & "' AND color <> 0", "", _
                                               DataViewRowState.CurrentRows).ToTable


                    'Pinkal (06-May-2014) -- End


                    For i As Integer = 0 To dtholiday.Rows.Count - 1


                        'Pinkal (01-Feb-2014) -- Start
                        'Enhancement : TRA Changes

                        Dim drHoliday() As DataRow = dtFill.Select("leavedate ='" & dtholiday.Rows(i)("holidaydate").ToString() & "'")
                        If drHoliday.Length > 0 Then Continue For

                        'Pinkal (01-Feb-2014) -- End

                        drrow = dtFill.NewRow
                        drrow("leavedate") = dtholiday.Rows(i)("holidaydate")
                        drrow("color") = dtholiday.Rows(i)("color")
                        dtFill.Rows.Add(drrow)
                    Next
                    dtFill = New DataView(dtFill, "1=1", "leavedate asc", DataViewRowState.CurrentRows).ToTable
                    'END FOR HOLIDAY MASTER

                    ReDim arStrDays(dtFill.Rows.Count - 1)
                    ReDim arDaysColor(dtFill.Rows.Count - 1)
                    For i = 0 To dtFill.Rows.Count - 1
                        arStrDays(i) = dtFill.Rows(i)("leavedate").ToString
                        arDaysColor(i) = (dtFill.Rows(i)("color")).ToString
                    Next


                    'START FOR GENERATING LEAVE DAYS
                    GenerateDays(arStrMonth, arStrMonthDays, arStrFirstday, arStrDays, arDaysColor)
                    'END FOR GENERATING LEAVE DAYS

                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDataForViewer", mstrModuleName)
        Finally
            If dsFill IsNot Nothing Then dsFill.Dispose()
            If dtFill IsNot Nothing Then dtFill.Dispose()
            If arStrDays IsNot Nothing Then arStrDays = Nothing
            If arStrMonth IsNot Nothing Then arStrMonth = Nothing
            If arStrMonthDays IsNot Nothing Then arStrMonthDays = Nothing
            If arStrFirstday IsNot Nothing Then arStrFirstday = Nothing
            If arDaysColor IsNot Nothing Then arDaysColor = Nothing
        End Try
    End Sub

    Private Function GetPromptForBalance(ByVal mdblCurrentLeavedays As Decimal, ByVal isUpdate As Boolean, Optional ByVal mdblLastLeavedays As Decimal = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Try

            Dim objLeaveType As New clsleavetype_master
            Dim objbalance As New clsleavebalance_tran
            objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)

            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
            '    dsList = objbalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
            '                                            , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
            '                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1, False, False, False, "", Nothing)
            'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '    dsList = objbalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
            '                                            , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
            '                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1, True, True, False, "", Nothing)

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                dsList = objbalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                        , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1, False, False, False, "lvleavebalance_tran.employeeunkid = " & CInt(txtEmployee.Tag), Nothing, mblnIsExternalApprover)
            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                dsList = objbalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                        , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1, True, True, False, "lvleavebalance_tran.employeeunkid = " & CInt(txtEmployee.Tag), Nothing, mblnIsExternalApprover)



                'Pinkal (01-Mar-2016) -- End

            End If


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            'Dim drRow As DataRow() = dsList.Tables("List").Select("leavetypeunkid=" & CInt(cboLeaveType.SelectedValue) & " AND employeeunkid = " & CInt(txtEmployee.Tag))
            Dim drRow As DataRow() = dsList.Tables("List").Select("leavetypeunkid=" & CInt(cboLeaveType.SelectedValue))
            'Pinkal (18-Nov-2016) -- End


            Dim mdtStartDate As Date = Nothing
            Dim mdtEndDate As Date = Nothing

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC AndAlso drRow.Length > 0 Then
                mdtStartDate = CDate(drRow(0)("startdate")).Date
                mdtEndDate = CDate(drRow(0)("enddate")).Date
            End If


            If objLeaveType._DeductFromLeaveTypeunkid > 0 Then

                Dim mdblIssue As Decimal = 0
                If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                    mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(txtEmployee.Tag), CInt(cboLeaveType.SelectedValue), FinancialYear._Object._YearUnkid, Nothing, mdtStartDate, mdtEndDate)
                Else
                    mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(txtEmployee.Tag), CInt(cboLeaveType.SelectedValue), FinancialYear._Object._YearUnkid)
                End If

                If isUpdate = False Then

                    If objLeaveType._MaxAmount < (mdblIssue + mdblCurrentLeavedays) Then

                        'Pinkal (02-Jan-2018) -- Start
                        'Enhancement - Ref 125 For leave types whose days are deducted from leave types of accrue nature, don't allow employee to apply more than the maximum set on the Add/Edit Leave type screen. 
                        'As at now, employee can apply more than the set maximum; approver is only given a warning if he still wishes to issue. If he still wants to approve even with the exceeding leave days he can approve.
                        'Control to be put on application/not just issuing. employee to be warned and stopped.
                        'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        'Return True
                        'Else
                        'Return False
                        'End If
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), CType(enMsgBoxStyle.Question + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False
                        'Pinkal (02-Jan-2018) -- End

                    End If

                ElseIf isUpdate Then

                    If mdblIssue > 0 And objLeaveType._MaxAmount < (mdblIssue - mdblLastLeavedays) + mdblCurrentLeavedays Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Return True
                        Else
                            Return False
                        End If

                    End If

                End If

            ElseIf objLeaveType._DeductFromLeaveTypeunkid <= 0 AndAlso objLeaveType._IsPaid = False Then

                Dim mdblIssue As Decimal = 0
                If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                    mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(txtEmployee.Tag), CInt(cboLeaveType.SelectedValue), FinancialYear._Object._YearUnkid, Nothing, mdtStartDate, mdtEndDate)
                Else
                    mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(txtEmployee.Tag), CInt(cboLeaveType.SelectedValue), FinancialYear._Object._YearUnkid)
                End If

                If isUpdate = False Then

                    If objLeaveType._MaxAmount < (mdblIssue + mdblCurrentLeavedays) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), CType(enMsgBoxStyle.Question + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Return False
                    End If

                ElseIf isUpdate Then

                    If mdblIssue > 0 And objLeaveType._MaxAmount < (mdblIssue - mdblLastLeavedays) + mdblCurrentLeavedays Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), CType(enMsgBoxStyle.Question + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Return False

                    End If

                End If

            End If

            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

            If drRow.Length > 0 Then

                If CBool(drRow(0)("isshortleave")) = False Then

                    If CInt(drRow(0)("accruesetting")) <> enAccrueSetting.Donot_Issue_On_Exceeding_Balance AndAlso CInt(drRow(0)("accruesetting")) <> enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then

                        If isUpdate = False Then

                            If CDec(drRow(0)("Accrue_Amount")) < (CDec(drRow(0)("Issue_amount")) + mdblCurrentLeavedays) Then
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                    Return True
                                Else
                                    Return False
                                End If

                            End If

                        ElseIf isUpdate Then

                            If CDec(drRow(0)("Issue_amount")) > 0 And CDec(drRow(0)("Accrue_Amount")) < ((CDec(drRow(0)("Issue_amount")) - mdblLastLeavedays) + mdblCurrentLeavedays) Then
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                    Return True
                                Else
                                    Return False
                                End If

                            End If

                        End If

                    ElseIf CInt(drRow(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance Then

                        If CDec(drRow(0)("Accrue_Amount")) < (CDec(drRow(0)("Issue_amount")) + mdblCurrentLeavedays) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "You can't issue this leave form.Reason : Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False
                        End If

                    ElseIf CInt(drRow(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then
                        Dim mdecAsonDateAccrue As Decimal = 0
                        Dim mdecAsonDateBalance As Decimal = 0

                        'Pinkal (06-Apr-2018) -- Start
                        'bug - 0002164  Employee unable to apply for leave. message indicating that leave has exceeded max limit [CCK].

                        'objbalance.GetAsonDateAccrue(FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date _
                        '                                             , ConfigParameter._Object._LeaveBalanceSetting, CInt(txtEmployee.Tag), objLeaveType._Leavetypeunkid _
                        '                                             , mdtFormEndDate.Date, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth _
                        '                                             , mdecAsonDateAccrue, mdecAsonDateBalance)

                        objbalance.GetAsonDateAccrue(FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date _
                                                                     , ConfigParameter._Object._LeaveBalanceSetting, CInt(txtEmployee.Tag), objLeaveType._Leavetypeunkid _
                                                                     , mdtFormEndDate.Date, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth _
                                                                     , mdecAsonDateAccrue, mdecAsonDateBalance, FinancialYear._Object._YearUnkid)

                        'Pinkal (06-Apr-2018) -- End
                        
                        'Pinkal (16-Dec-2016) -- Start
                        'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                        'If mdecAsonDateAccrue < (CDec(drRow(0)("Issue_amount")) + mdblCurrentLeavedays) Then
                        If mdecAsonDateBalance < mdblCurrentLeavedays Then
                            'Pinkal (16-Dec-2016) -- End
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "You cannot issue this leave form.Reason : This Leave Limit has exceeded the Maximum Leave balance as on date Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False
                        End If


                    End If


                ElseIf CBool(drRow(0)("isshortleave")) Then

                    If CDec(drRow(0)("Accrue_Amount")) < (CDec(drRow(0)("Issue_amount")) + mdblCurrentLeavedays) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), enMsgBoxStyle.Information)
                        Return False
                    End If

                End If

            End If

            'Pinkal (18-Nov-2016) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetPromptForBalance", mstrModuleName)
        End Try
    End Function

    Private Function CheckForUpdate(ByVal mdtIssuetran As DataTable, ByVal arGetdate As String()) As Boolean
        Try
            If arGetdate.Length > 0 Then

                If mdtIssuetran.Rows.Count <> arGetdate.Length Then Return True

                For i As Integer = 0 To mdtIssuetran.Rows.Count - 1
                    For j As Integer = 0 To arGetdate.Length - 1
                        If mdtIssuetran.Rows(i)("leavedate").ToString <> eZeeDate.convertDate(CDate(arGetdate(j))) Then
                            Return True
                        End If
                    Next
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckForUpdate", mstrModuleName)
        End Try
        Return False
    End Function

    Private Sub GetBalanceInfo(ByVal mdtDate As DateTime)
        Try
            Dim objLeaveAccrue As New clsleavebalance_tran
            Dim dsList As DataSet = Nothing
            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsList = objLeaveAccrue.GetLeaveBalanceInfo(mdtDate, CStr(mintLeaveTypeunkid), txtEmployee.Tag.ToString)
                dsList = objLeaveAccrue.GetLeaveBalanceInfo(mdtDate, CStr(mintLeaveTypeunkid), txtEmployee.Tag.ToString, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth)
                'Pinkal (18-Nov-2016) -- End

            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                objLeaveAccrue._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
                objLeaveAccrue._DBEnddate = FinancialYear._Object._Database_End_Date.Date

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsList = objLeaveAccrue.GetLeaveBalanceInfo(mdtDate, CStr(mintLeaveTypeunkid), txtEmployee.Tag.ToString, Nothing, Nothing, True, True)
                dsList = objLeaveAccrue.GetLeaveBalanceInfo(mdtDate, CStr(mintLeaveTypeunkid), txtEmployee.Tag.ToString, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True)
                'Pinkal (18-Nov-2016) -- End

            End If

            If dsList.Tables("Balanceinfo").Rows.Count > 0 Then
                objlblLastYearAccruedValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LstyearAccrue_amount")).ToString("#0.00")
                objlblLastYearIssuedValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LstyearIssue_amount")).ToString("#0.00")
                objlblAccruedToDateValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
                'objlblIssuedToDateValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")).ToString("#0.00")
                objlblBalanceValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Balance")).ToString("#0.00")

                'Pinkal (24-Aug-2016) -- Start
                'Enhancement - Removing 2 columns(Leave Accrue Upto Last Year AND Leave Issued Upto Last Year) From Whole Leave Module AS Per Rutta's Request For VFT
                'objlblIssuedToDateValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")).ToString("#0.00")
                objlblIssuedToDateValue.Text = CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment"))).ToString("#0.00")
                objlblLeaveBFvalue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")).ToString("#0.00")
                objlblTotalAdjustment.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")).ToString("#0.00")
                'Pinkal (24-Aug-2016) -- End


            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpAsOnDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-Nov-2014) -- Start
    'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

    Private Sub Send_Notification()
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    objSendMail._FormName = obj._FormName
                    objSendMail._ClientIP = getIP()
                    objSendMail._HostName = getHostName()
                    objSendMail._FromWeb = False
                    objSendMail._AuditUserId = User._Object._Userunkid
objSendMail._CompanyUnkid = Company._Object._Companyunkid
                    objSendMail._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    objSendMail._LoginEmployeeunkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = User._Object._Userunkid
                    objSendMail._SenderAddress = Company._Object._Senderaddress
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception
                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub

    'Pinkal (03-Nov-2014) -- End


#End Region

#Region "Listview's Event"

    Private Sub lvLeaveCodes_ItemSelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvLeaveCodes.ItemSelectionChanged
        Try
            e.Item.Selected = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLeaveCodes_ItemSelectionChanged", mstrModuleName)
        End Try
    End Sub


    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes

    'Private Sub lvEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    'If lvEmployee.SelectedItems.Count > 0 Then
    '    objbtnSearch_Click(sender, e)
    '    Dim lvItem As ListViewItem = lvEmployee.SelectedItems(0)
    '    If lvItem IsNot Nothing Then
    '        txtEmpviewer.Text = lvItem.SubItems(1).Text
    '    End If
    'End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmployee_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (06-May-2014) -- End

#End Region

#Region "LinkLabel Event"

    Private Sub lnkPreviewDocument_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkPreviewDocument.LinkClicked
        Dim objForm As New frmPreviewDocuments
        Try

            If User._Object._Isrighttoleft = True Then
                objForm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objForm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objForm)
            End If

            Dim mintTranId As Integer = -1
            Dim objScanAttach As New clsScan_Attach_Documents
            Dim dtTable As DataTable = objScanAttach.GetAttachmentTranunkIds(CInt(txtEmployee.Tag) _
                                                                               , enScanAttactRefId.LEAVEFORMS, enImg_Email_RefId.Leave_Module _
                                                                               , CInt(cboFormNo.SelectedValue))

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mintTranId = CInt(dtTable.Rows(0)("scanattachtranunkid"))
            End If

            objForm.displayDialog(mintTranId.ToString)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkPreviewDocument_LinkClicked", mstrModuleName)
        End Try
    End Sub


    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes

    Private Sub lnkAllocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (06-May-2014) -- End


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbLeaveIssue.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeaveIssue.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbLeaveViewer.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeaveViewer.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbLeaveIssue.Text = Language._Object.getCaption(Me.gbLeaveIssue.Name, Me.gbLeaveIssue.Text)
            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
            Me.lblLeaveYear.Text = Language._Object.getCaption(Me.lblLeaveYear.Name, Me.lblLeaveYear.Text)
            Me.lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.Name, Me.lblFormNo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblLeaveEndDate.Text = Language._Object.getCaption(Me.lblLeaveEndDate.Name, Me.lblLeaveEndDate.Text)
            Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.Name, Me.lblBalance.Text)
            Me.lblTotalIssuedToDate.Text = Language._Object.getCaption(Me.lblTotalIssuedToDate.Name, Me.lblTotalIssuedToDate.Text)
            Me.lblLastYearIssued.Text = Language._Object.getCaption(Me.lblLastYearIssued.Name, Me.lblLastYearIssued.Text)
            Me.lblToDateAccrued.Text = Language._Object.getCaption(Me.lblToDateAccrued.Name, Me.lblToDateAccrued.Text)
            Me.lblLastYearAccrued.Text = Language._Object.getCaption(Me.lblLastYearAccrued.Name, Me.lblLastYearAccrued.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
            Me.gbLeaveViewer.Text = Language._Object.getCaption(Me.gbLeaveViewer.Name, Me.gbLeaveViewer.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
            Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
            Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
            Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
            Me.DataGridViewTextBoxColumn23.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn23.Name, Me.DataGridViewTextBoxColumn23.HeaderText)
            Me.DataGridViewTextBoxColumn24.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn24.Name, Me.DataGridViewTextBoxColumn24.HeaderText)
            Me.DataGridViewTextBoxColumn25.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn25.Name, Me.DataGridViewTextBoxColumn25.HeaderText)
            Me.DataGridViewTextBoxColumn26.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn26.Name, Me.DataGridViewTextBoxColumn26.HeaderText)
            Me.DataGridViewTextBoxColumn27.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn27.Name, Me.DataGridViewTextBoxColumn27.HeaderText)
            Me.DataGridViewTextBoxColumn28.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn28.Name, Me.DataGridViewTextBoxColumn28.HeaderText)
            Me.DataGridViewTextBoxColumn29.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn29.Name, Me.DataGridViewTextBoxColumn29.HeaderText)
            Me.DataGridViewTextBoxColumn30.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn30.Name, Me.DataGridViewTextBoxColumn30.HeaderText)
            Me.DataGridViewTextBoxColumn31.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn31.Name, Me.DataGridViewTextBoxColumn31.HeaderText)
            Me.DataGridViewTextBoxColumn32.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn32.Name, Me.DataGridViewTextBoxColumn32.HeaderText)
            Me.DataGridViewTextBoxColumn33.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn33.Name, Me.DataGridViewTextBoxColumn33.HeaderText)
            Me.DataGridViewTextBoxColumn34.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn34.Name, Me.DataGridViewTextBoxColumn34.HeaderText)
            Me.colhLeavetype.Text = Language._Object.getCaption(CStr(Me.colhLeavetype.Tag), Me.colhLeavetype.Text)
            Me.lblTotalLeaveDays.Text = Language._Object.getCaption(Me.lblTotalLeaveDays.Name, Me.lblTotalLeaveDays.Text)
            Me.LblEmpViewer.Text = Language._Object.getCaption(Me.LblEmpViewer.Name, Me.LblEmpViewer.Text)
            Me.lnkPreviewDocument.Text = Language._Object.getCaption(Me.lnkPreviewDocument.Name, Me.lnkPreviewDocument.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.LblLeaveBF.Text = Language._Object.getCaption(Me.LblLeaveBF.Name, Me.LblLeaveBF.Text)
            Me.LblTotalAdjustment.Text = Language._Object.getCaption(Me.LblTotalAdjustment.Name, Me.LblTotalAdjustment.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Form No is compulsory information.Please Select Form No.")
            Language.setMessage(mstrModuleName, 3, "Leave Date is compulsory information.Please Select Leave Date between given Leave Form Period.")
            Language.setMessage(mstrModuleName, 4, "From Date is compulsory information.Please Select From Date.")
            Language.setMessage(mstrModuleName, 7, "This is Weekend holiday.You cannot issue leave for this day.")
            Language.setMessage(mstrModuleName, 8, "Please Enter Accrue amount for this paid leave.")
            Language.setMessage(mstrModuleName, 9, "Leave Type is compulsory information.Please Select Leave Type.")
            Language.setMessage(mstrModuleName, 10, "To Date is compulsory information.Please Select To Date.")
            Language.setMessage(mstrModuleName, 11, "Please select Employee from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 12, "Month")
            Language.setMessage(mstrModuleName, 13, "This particular leave is mapped with")
            Language.setMessage(mstrModuleName, 14, " , which does not have the accrue amount. Please add accrue amount for")
            Language.setMessage(mstrModuleName, 15, " inorder to issue leave.")
            Language.setMessage(mstrModuleName, 16, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?")
            Language.setMessage(mstrModuleName, 17, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form.")
            Language.setMessage(mstrModuleName, 18, "You can't issue this leave form.Reason : Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.")
            Language.setMessage(mstrModuleName, 19, "Are you sure you want to remove Issue Leave on this Holiday?")
            Language.setMessage(mstrModuleName, 20, "In this Leave form, Holiday is considered as leave.are you sure you want to save holiday(s) as leave?")
            Language.setMessage(mstrModuleName, 21, "Sorry, there is no leave frequency accrue amount set for the selected short leave, please enter accrue amount for this leave from, Leave Information -> Leave Frequency.")
            Language.setMessage(mstrModuleName, 22, "Issued Leave Application form notification")
            Language.setMessage(mstrModuleName, 23, "Sending Email(s) process is in progress from other module. Please wait.")
            Language.setMessage(mstrModuleName, 24, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to Issue this leave ?")
            Language.setMessage(mstrModuleName, 25, "You cannot issue this leave form.Reason : This Leave Limit has exceeded the Maximum Leave balance as on date Limit for this leave type.")
	    Language.setMessage(mstrModuleName, 26, "Sorry, you cannot issue for this leave type as it has been configured with setting for minimum")
     	    Language.setMessage(mstrModuleName, 27, "Sorry, you cannot issue for this leave type as it has been configured with setting for maximum")
	    Language.setMessage(mstrModuleName, 28, "day(s) to be applied for this leave type.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class