﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmLeaveTypeList


#Region "Private Variable"

    Private objLeaveTypeMaster As clsleavetype_master
    Private ReadOnly mstrModuleName As String = "frmLeaveTypeList"

#End Region

#Region "Form's Event"

    Private Sub frmLeaveTypeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeaveTypeMaster = New clsleavetype_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)


            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()

            fillList()

            If lvLeaveTypeCodes.Items.Count > 0 Then lvLeaveTypeCodes.Items(0).Selected = True
            lvLeaveTypeCodes.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveTypeList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveTypeList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveTypeList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveTypeList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeaveTypeMaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleavetype_master.SetMessages()
            objfrm._Other_ModuleNames = "clsleavetype_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmLeaveType_AddEdit
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvLeaveTypeCodes.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Type from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvLeaveTypeCodes.Select()
            Exit Sub
        End If
        Dim objfrmLeaveType_AddEdit As New frmLeaveType_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLeaveTypeCodes.SelectedItems(0).Index
            If objfrmLeaveType_AddEdit.displayDialog(CInt(lvLeaveTypeCodes.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            objfrmLeaveType_AddEdit = Nothing

            lvLeaveTypeCodes.Items(intSelectedIndex).Selected = True
            lvLeaveTypeCodes.EnsureVisible(intSelectedIndex)
            lvLeaveTypeCodes.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmLeaveType_AddEdit IsNot Nothing Then objfrmLeaveType_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvLeaveTypeCodes.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Type from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvLeaveTypeCodes.Select()
            Exit Sub
        End If
        If objLeaveTypeMaster.isUsed(CInt(lvLeaveTypeCodes.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Leave Type. Reason: This Leave Type is in use."), enMsgBoxStyle.Information) '?2
            lvLeaveTypeCodes.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLeaveTypeCodes.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Leave Type?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objLeaveTypeMaster._FormName = mstrModuleName
                objLeaveTypeMaster._LoginEmployeeunkid = 0
                objLeaveTypeMaster._ClientIP = getIP()
                objLeaveTypeMaster._HostName = getHostName()
                objLeaveTypeMaster._FromWeb = False
                objLeaveTypeMaster._AuditUserId = User._Object._Userunkid
objLeaveTypeMaster._CompanyUnkid = Company._Object._Companyunkid
                objLeaveTypeMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objLeaveTypeMaster.Delete(CInt(lvLeaveTypeCodes.SelectedItems(0).Tag))
                lvLeaveTypeCodes.SelectedItems(0).Remove()

                If lvLeaveTypeCodes.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvLeaveTypeCodes.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvLeaveTypeCodes.Items.Count - 1
                    lvLeaveTypeCodes.Items(intSelectedIndex).Selected = True
                    lvLeaveTypeCodes.EnsureVisible(intSelectedIndex)
                ElseIf lvLeaveTypeCodes.Items.Count <> 0 Then
                    lvLeaveTypeCodes.Items(intSelectedIndex).Selected = True
                    lvLeaveTypeCodes.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvLeaveTypeCodes.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsLeaveTypeList As New DataSet
        Try

            If User._Object.Privilege._AllowToViewLeaveTypeList = True Then                'Pinkal (02-Jul-2012) -- Start


                dsLeaveTypeList = objLeaveTypeMaster.GetList("List")

                Dim lvItem As ListViewItem

                lvLeaveTypeCodes.Items.Clear()
                For Each drRow As DataRow In dsLeaveTypeList.Tables(0).Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("leavetypecode").ToString
                    lvItem.Tag = drRow("leavetypeunkid")
                    lvItem.SubItems.Add(drRow("leavename").ToString)
                    lvItem.SubItems.Add(drRow("accuretype").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvLeaveTypeCodes.Items.Add(lvItem)
                Next

                If lvLeaveTypeCodes.Items.Count > 16 Then
                    colhDescription.Width = 322 - 18
                Else
                    colhDescription.Width = 322
                End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsLeaveTypeList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddLeaveType
            btnEdit.Enabled = User._Object.Privilege._EditLeaveType
            btnDelete.Enabled = User._Object.Privilege._DeleteLeaveType

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhLeaveType.Text = Language._Object.getCaption(CStr(Me.colhLeaveType.Tag), Me.colhLeaveType.Text)
            Me.colhLeaveCategory.Text = Language._Object.getCaption(CStr(Me.colhLeaveCategory.Tag), Me.colhLeaveCategory.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Leave Type from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Leave Type. Reason: This Leave Type is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Leave Type?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class