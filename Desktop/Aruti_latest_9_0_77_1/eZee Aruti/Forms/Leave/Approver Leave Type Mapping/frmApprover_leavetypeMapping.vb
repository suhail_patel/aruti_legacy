﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmApprover_leavetypeMapping

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmApprover_leavetypeMapping"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objLeaveTypeMapping As clsapprover_leavetype_mapping
    Friend mintMappingUnkid As Integer = -1
    Private mintApproverunkid As Integer = -1

    'Pinkal (06-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Private mstrEmployeeName As String = String.Empty
    Private mstrApproverLevel As String = String.Empty
    Private mdtLeaveTypeMapping As DataTable = Nothing

    'Pinkal (06-Apr-2013) -- End

#End Region


    'Pinkal (06-Apr-2013) -- Start
    'Enhancement : TRA Changes

#Region "Property"

    Public Property _EmployeeName() As String
        Get
            Return mstrEmployeeName
        End Get
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public Property _ApproverLevel() As String
        Get
            Return mstrApproverLevel
        End Get
        Set(ByVal value As String)
            mstrApproverLevel = value
        End Set
    End Property

    Public Property _dtLeaveTypeMapping() As DataTable
        Get
            Return mdtLeaveTypeMapping
        End Get
        Set(ByVal value As DataTable)
            mdtLeaveTypeMapping = value
        End Set
    End Property


#End Region

    'Pinkal (06-Apr-2013) -- End

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal _intApproverunkid As Integer) As Boolean
        Try
            mintMappingUnkid = intUnkId
            mintApproverunkid = _intApproverunkid
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintMappingUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub GetApproverInfo()
        Try

            'Pinkal (06-Apr-2013) -- Start
            'Enhancement : TRA Changes

            If mstrEmployeeName.Trim.ToString().Length <= 0 Then
                Dim objapprover As New clsleaveapprover_master
                objapprover._Approverunkid = mintApproverunkid

                Dim objEmployee As New clsEmployee_Master

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = objapprover._leaveapproverunkid
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objapprover._leaveapproverunkid
                'S.SANDEEP [04 JUN 2015] -- END

                objlblApprover.Text = objEmployee._Firstname & "  " & objEmployee._Surname

                Dim objLevel As New clsapproverlevel_master
                objLevel._Levelunkid = objapprover._Levelunkid
                objlblApproverLevel.Text = objLevel._Levelname

                objApproverFooter.Visible = False
                objFooter.Visible = True

            Else
                objlblApprover.Text = mstrEmployeeName
                objlblApproverLevel.Text = mstrApproverLevel
                objApproverFooter.Visible = True
                objFooter.Visible = False
            End If

            'Pinkal (06-Apr-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverInfo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillLeaveType()
        Dim dsFill As DataSet = Nothing
        Try
            Dim objLeaveType As New clsleavetype_master

            If mdtLeaveTypeMapping Is Nothing Then

                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                'dsFill = objLeaveType.GetList("List", True, True, )
                dsFill = objLeaveType.GetList("List", True, True, "ISNULL(skipapproverflow,0) = 0")
                'Pinkal (01-Oct-2018) -- End
            Else
                Dim dsLeaveType As DataSet = Nothing
                dsLeaveType = New DataSet

                'Pinkal (01-Apr-2020) -- Start
                'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
                'dsLeaveType.Tables.Add(mdtLeaveTypeMapping)
                dsLeaveType.Tables.Add(mdtLeaveTypeMapping.Copy())
                'Pinkal (01-Apr-2020) -- End


                dsFill = dsLeaveType
            End If

            For Each dr As DataRow In dsFill.Tables(0).Rows

                Dim drRow As DataRow = objLeaveTypeMapping._dtLeaveType.NewRow()
                drRow("leavetypemappingunkid") = objLeaveTypeMapping.GetLeaveTypeMappingUnkId(mintApproverunkid, CInt(dr("leavetypeunkid")))

                'Pinkal (06-Apr-2013) -- Start
                'Enhancement : TRA Changes

                If mintApproverunkid > 0 AndAlso mdtLeaveTypeMapping Is Nothing Then
                    drRow("ischecked") = objLeaveTypeMapping.isExist(mintApproverunkid, CInt(dr("leavetypeunkid")))
                Else
                    If dsFill.Tables(0).Columns.Contains("ischecked") Then
                        drRow("ischecked") = dr("ischecked")
                    Else
                        drRow("ischecked") = False
                    End If
                End If

                'Pinkal (06-Apr-2013) -- End 

                drRow("approverunkid") = mintApproverunkid
                drRow("leavetypeunkid") = dr("leavetypeunkid").ToString()
                drRow("leavetypecode") = dr("leavetypecode").ToString()
                drRow("leavename") = dr("leavename").ToString()
                drRow("ispaid") = dr("ispaid").ToString()
                objLeaveTypeMapping._dtLeaveType.Rows.Add(drRow)
            Next


            dgLeaveType.AutoGenerateColumns = False
            dgLeaveType.DataSource = objLeaveTypeMapping._dtLeaveType
            objdgcolhSelect.DataPropertyName = "ischecked"
            dgcolhLeaveCode.DataPropertyName = "leavetypecode"
            dgcolhLeaveType.DataPropertyName = "leavename"
            dgcolhPaidLeave.DataPropertyName = "ispaid"
            objdgcolhLeaveTypeMappingunkid.DataPropertyName = "leavetypemappingunkid"

            SetCheckBoxValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillProvider", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objLeaveTypeMapping._Approverunkid = mintApproverunkid
            objLeaveTypeMapping._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = objLeaveTypeMapping._dtLeaveType.Select("ischecked = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgLeaveType.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgLeaveType.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmApprover_leavetypeMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeaveTypeMapping = New clsapprover_leavetype_mapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            GetApproverInfo()
            FillLeaveType()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_leavetypeMapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApprover_leavetypeMapping_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_leavetypeMapping_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApprover_leavetypeMapping_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_leavetypeMapping_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApprover_leavetypeMapping_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeaveTypeMapping = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsapprover_leavetype_mapping.SetMessages()
            'objfrm._Other_ModuleNames = "clsapprover_leavetype_mapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            Dim drRow As DataRow() = objLeaveTypeMapping._dtLeaveType.Select("ischecked = true")
            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Type is compulsory information.Please Select one Leave Type."), CType(MsgBoxStyle.Information + MsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgLeaveType.Select()
                Exit Sub
            End If

            SetValue()


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objLeaveTypeMapping._FormName = mstrModuleName
            objLeaveTypeMapping._LoginEmployeeunkid = 0
            objLeaveTypeMapping._ClientIP = getIP()
            objLeaveTypeMapping._HostName = getHostName()
            objLeaveTypeMapping._FromWeb = False
            objLeaveTypeMapping._AuditUserId = User._Object._Userunkid
objLeaveTypeMapping._CompanyUnkid = Company._Object._Companyunkid
            objLeaveTypeMapping._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnFlag = objLeaveTypeMapping.Insert()

            If blnFlag = False And objLeaveTypeMapping._Message <> "" Then
                eZeeMsgBox.Show(objLeaveTypeMapping._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


    'Pinkal (06-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try

            If objLeaveTypeMapping._dtLeaveType Is Nothing Then Exit Sub

            Dim drRow As DataRow() = objLeaveTypeMapping._dtLeaveType.Select("ischecked = true")
            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Type is compulsory information.Please Select one Leave Type."), CType(MsgBoxStyle.Information + MsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgLeaveType.Select()
                Exit Sub
            End If
            _dtLeaveTypeMapping = objLeaveTypeMapping._dtLeaveType
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (06-Apr-2013) -- End

#End Region

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try

            If objLeaveTypeMapping._dtLeaveType.Rows.Count > 0 Then
                For Each dr As DataRow In objLeaveTypeMapping._dtLeaveType.Rows
                    RemoveHandler dgLeaveType.CellContentClick, AddressOf dgLeaveType_CellContentClick
                    dr("ischecked") = chkSelectAll.Checked
                    AddHandler dgLeaveType.CellContentClick, AddressOf dgLeaveType_CellContentClick
                Next
                objLeaveTypeMapping._dtLeaveType.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGridView Event"

    Private Sub dgLeaveType_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgLeaveType.CellContentClick, dgLeaveType.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhSelect.Index Then

                If dgLeaveType.IsCurrentCellDirty Then
                    dgLeaveType.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    objLeaveTypeMapping._dtLeaveType.AcceptChanges()
                End If

                SetCheckBoxValue()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLeaveType_CellContentClick", mstrModuleName)
        End Try

    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbLeavedetail.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeavedetail.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbLeavedetail.Text = Language._Object.getCaption(Me.gbLeavedetail.Name, Me.gbLeavedetail.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.lblApproverLevel.Text = Language._Object.getCaption(Me.lblApproverLevel.Name, Me.lblApproverLevel.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.dgcolhLeaveCode.HeaderText = Language._Object.getCaption(Me.dgcolhLeaveCode.Name, Me.dgcolhLeaveCode.HeaderText)
            Me.dgcolhLeaveType.HeaderText = Language._Object.getCaption(Me.dgcolhLeaveType.Name, Me.dgcolhLeaveType.HeaderText)
            Me.dgcolhPaidLeave.HeaderText = Language._Object.getCaption(Me.dgcolhPaidLeave.Name, Me.dgcolhPaidLeave.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Leave Type is compulsory information.Please Select one Leave Type.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
